/*
This file is created with lot of blood and sweat. For the same purpose Grunt could have been used but we shall live on the cutting edge so using Gulp. If you are interested in knowing why I advise you to watch these slides http://slides.com/contra/gulp but anyways for me the real joy is ability to execute asyncronously where ever possible in terms of executing task and more nodejs-ish flavour.

p.s I shall remove this big comment block after some time ;-)
*/
var gulp          = require('gulp'),
    minifyCSS     = require('gulp-minify-css');
    rjs           = require('gulp-requirejs'),
    uglify        = require('gulp-uglify'),
    concat        = require('gulp-concat'),
    handlebars    = require('gulp-handlebars'),
    // dependency for handlebars to create namespaces
    declare       = require('gulp-declare'),
    defineModule  = require('gulp-define-module'),
    combineCSS    = require('combine-css'),
    jshint        = require('gulp-jshint'),
    stylish       = require('jshint-stylish'),
    taskListing = require('gulp-task-listing'),
    prettify = require('gulp-prettify'),
    cleanhtml = require('gulp-cleanhtml'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass');

var jsLibPath = 'webapp/content/js/lib/';

var mobileJSBasePath      = 'webapp/content/js/m/',
    mobileBaseJSFile      = 'm.js',
    mobileBaseJSFileName  = mobileBaseJSFile.split('.')[0],
    mobileDistPath        = mobileJSBasePath + 'dist',
    mobileDistCssPath     = 'webapp/content/css/m/dist',
    mobiletemplatesPath   = 'webapp/content/templates/mobile/';

// Desktop settings
var desktopBase               = 'webapp/content/js/singlepage',
    websiteTemplatesPath  = 'webapp/content/templates/web/';

// desktop site related variables
var desktopBaseJsPath           = 'webapp/content/js/singlepage/custom/',
    desktopDistJsPath           = 'webapp/content/js/singlepage/dist/',
    desktopCssPath              = 'webapp/content/css/singlepage/',
    desktopCssLibPath           = 'webapp/content/css/lib/',
    desktopDistCssPath          = 'webapp/content/css/singlepage/dist';

var mobileCssFiles = [
  'webapp/content/css/m/fonts.css',
  'webapp/content/css/m/reset.css',
  'webapp/content/css/m/base.css',
  'webapp/content/css/m/modules.css',
  'webapp/content/css/m/pages.css',
  'webapp/content/css/m/keyframes.css',
  'webapp/content/css/m/mediaqueries.css'
];

var desktopCssFiles = [
  desktopCssPath      + 'b.css',
  desktopCssLibPath   + 'selectBoxIt.css',
  desktopCssPath      + 'freecharge.css',
  desktopCssLibPath   + 'throbber.css',
  desktopCssLibPath   + 'jquery.nouislider.css',
  desktopCssLibPath   + 'progressbutton.css'
];

var desktopTplPath = 'webapp/content/templates/web/new'

var uglifyConfigs = {
  mangle           : true,
  compress         : {
      drop_console : true
  }
};

var libFiles = [
  jsLibPath + 'fingerprint.min.js',
  jsLibPath + 'jquery.2.1.0.js',
  jsLibPath + 'almond.js',
  jsLibPath + 'parsley.custom.validation.js',
  jsLibPath + 'handlebars.runtime-v1.3.0.js',
  jsLibPath + 'iscroll.js'
  // jsLibPath + 'scriptsinlocal.js'
];

var libFilesDesktop = [
  jsLibPath + 'jquery.2.1.0.js',
  jsLibPath + 'stapes.min.js',
  jsLibPath + 'path.js',
  // jsLibPath + 'mustache.js',
  // jsLibPath + 'jquery.mustache.js',
  jsLibPath + 'Pace.min.js',
  jsLibPath + 'bootstrap.3.0.3.min.js',
  jsLibPath + 'jquery-ui-1.10.3.custom.min.js',
  jsLibPath + 'jquery.selectBoxIt.min.js',
  jsLibPath + 'jquery.knob.js',
  jsLibPath + 'underscore.min.js',
  jsLibPath + 'jquery.imageScroll.min.js',
  jsLibPath + 'parsley.min.js',
  jsLibPath + 'parsley.custom.validation.js',
  jsLibPath + 'jquery.nouislider.min.js',
  jsLibPath + 'jquery.isotope.min.js',
  jsLibPath + 'fingerprint.min.js',
  jsLibPath + 'bootstrap-datepicker.min.js',
  jsLibPath + 'placeholders.js',
  jsLibPath + 'handlebars.runtime-v1.3.0.js'
];

var jsFilesDesktop = [
  desktopBaseJsPath + 'AppView.js',
  desktopBaseJsPath + 'AppModel.js',
  desktopBaseJsPath + 'AppController.js',
  desktopBaseJsPath + 'AppUtil.js',
  desktopBaseJsPath + 'AppRoutes.js'
];

// task for building require files
gulp.task('buildmobilerequire', function() {
  return rjs({
      mainConfigFile : mobileJSBasePath + mobileBaseJSFile,
      baseUrl : mobileJSBasePath,
      name: mobileBaseJSFileName,
      out: mobileBaseJSFileName + '.' + 'min.js',
      removeCombined: true,
      findNestedDependencies: true
  })
  .pipe(uglify(uglifyConfigs))
  .pipe(gulp.dest(mobileDistPath));
});

// task to build external libraries
gulp.task('buildmobilelibs', function(){
  return gulp.src(libFiles)
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.lib.min.js'))
    .pipe(gulp.dest(mobileDistPath));
});

// task to compile mobile handlebar templates
gulp.task('buildmobiletemplates', function(){
  return gulp.src(['webapp/content/templates/mobile/*.tpl'])
    .pipe(handlebars()) // returns a bare function
    .pipe(defineModule('plain'))
    .pipe(declare({
      namespace: 'mobiletemplates'
    }))
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.templates.min.js'))
    .pipe(gulp.dest(mobileDistPath));
});

gulp.task('buildmobilepartials',['buildmobiletemplates'],function() {
  return gulp.src(['webapp/content/templates/mobile/partials/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'mobilepartials'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('all.partials.min.js'))
  .pipe(gulp.dest(mobileDistPath));
});

gulp.task('combinemobiletemplates',['buildmobilepartials'], function(){
  return gulp.src([mobileDistPath+'/all.templates.min.js',mobileDistPath+'/all.partials.min.js'])
  .pipe(concat('all.templates.min.js'))
  .pipe(gulp.dest(mobileDistPath));
});

//JSHint tasks
gulp.task('jshintmobile', function(cb){
      gulp.src(mobileJSBasePath + '*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'));
});

gulp.task('jshintdesktop', function(cb){
      gulp.src(jsFilesDesktop)
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'));
});

gulp.task('buildmobilecss', function() {
  gulp.src(mobileCssFiles)
    .pipe(minifyCSS())
    .pipe(concat('all.css'))
    .pipe(gulp.dest(mobileDistCssPath));
});

gulp.task('combine', function() {
  gulp.src(mobileCssFiles)
    .pipe(concat('all.css'))
    .pipe(gulp.dest(mobileDistCssPath));
});

gulp.task('watchcss', function () {
  gulp.watch('webapp/content/css/m/*.css', ['combine']);
});

gulp.task('watchmobile', function(){
  gulp.watch(mobiletemplatesPath+'/**/*.tpl', ['combinemobiletemplates']);
  // gulp.watch(mobileJSBasePath + '*.js', ['buildrequire']);
});

gulp.task('watchrequire', function(){
  gulp.watch(mobileJSBasePath + '*.js', ['buildrequire']);
});

// task to build desktop external libraries
gulp.task('builddesktoplibsprod', function(){
  console.log("Building desktop libs for prod");
  return gulp.src(libFilesDesktop)
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.lib.min.js'))
    .pipe(gulp.dest(desktopDistJsPath));
});

// task to build desktop js
gulp.task('builddesktopjsprod', function(){
  console.log("Building desktop js files for prod");
  return gulp.src(jsFilesDesktop)
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest(desktopDistJsPath));
});

gulp.task('builddesktopjsdev', function(){
  console.log("Building desktop js files for prod");
  return gulp.src(jsFilesDesktop)
    .pipe(concat('all.min.js'))
    .pipe(gulp.dest(desktopDistJsPath));
});

// sass
gulp.task('sass', function() {
  return gulp.src( desktopCssPath + 'scss/**/*.scss')
      .pipe(sass({errLogToConsole: true}))
      .pipe(minifyCSS())
      .pipe(concat('all.css'))
      .pipe(gulp.dest(desktopDistCssPath));
});

// task to build desktop css
gulp.task('builddesktopcss', function() {
  gulp.src(desktopCssFiles)
    .pipe(minifyCSS())
    .pipe(concat('all.old.css'))
    .pipe(gulp.dest(desktopDistCssPath));
});

// task to copile handlebar web templates
gulp.task('buildDesktopTemplates',function() {
  return gulp.src(['webapp/content/templates/web/new/home/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'webtemplates'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('all-web-templates.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('buildDesktopPartials',function() {
  return gulp.src(['webapp/content/templates/web/new/partials/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'webpartials'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('all-web-partials.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('buildBourbonTemplates',function() {
  return gulp.src(['webapp/content/templates/web/new/bourbon/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'bourbonTemplates'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('bourbon-tpl.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('buildSuccessPage',function() {
  return gulp.src(['webapp/content/templates/web/new/rechargeSuccess/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'successPage'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('successPage.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('prettify-desktop-templates', function() {
  gulp.src(['webapp/content/templates/web/new/home/*.tpl'])
    .pipe(prettify({indent_size: 2}))
    .pipe(gulp.dest('webapp/content/templates/web/new/home/'))
});

gulp.task('prettify-desktop-partials', function() {
  gulp.src(['webapp/content/templates/web/new/partials/*.tpl'])
    .pipe(prettify({indent_size: 2}))
    .pipe(gulp.dest('webapp/content/templates/web/partials/home/'))
});

// watch tasks for desktop [livereload], also watch changes in templates, js or css files
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch( 'webapp/content/templates/web/new/home/*.tpl', ['buildDesktopTemplates'] );
  gulp.watch( 'webapp/content/templates/web/new/partials/*.tpl', ['buildDesktopPartials'] );
  gulp.watch( 'webapp/content/templates/web/new/rechargeSuccess/*.tpl', ['buildSuccessPage'] );
  gulp.watch( desktopCssPath + 'scss/**/*.scss', ['sass']);
  gulp.watch( desktopCssFiles, ['builddesktopcss'] );
  gulp.watch( desktopBaseJsPath + '*.js', ['builddesktopjsprod'] );
//  gulp.watch( desktopDistJsPath + '*.js', ['builddesktopjsprod'] ).on('change', livereload.changed);
//  gulp.watch( desktopDistJsPath + '*.js', ['buildDesktopTemplates'] ).on('change', livereload.changed);
//  gulp.watch( desktopDistJsPath + '*.js', ['buildDesktopPartials'] ).on('change', livereload.changed);
  gulp.watch( desktopCssPath + 'scss/**/*.scss').on('change', livereload.changed);
  gulp.watch( desktopDistCssPath + 'css/*.css', ['builddesktopcss']).on('change', livereload.changed);
});

/***********************************************************
    new tasks for building new freecharge related files 
*/
var desktopBaseJsPathNew = 'webapp/content/js/experiment/custom/';
var jsFilesDesktopNew = [
  desktopBaseJsPathNew + 'microlibs.js',
  desktopBaseJsPathNew + 'PlansView.js',
  desktopBaseJsPathNew + 'HomeView.js',
  desktopBaseJsPathNew + 'PaymentView.js',
  desktopBaseJsPathNew + 'CouponsView.js',
  desktopBaseJsPathNew + 'TurboView.js',
  desktopBaseJsPathNew + 'AppView.js',
  desktopBaseJsPathNew + 'AppModel.js',
  desktopBaseJsPathNew + 'AppController.js',
  desktopBaseJsPathNew + 'AppUtil.js',
  desktopBaseJsPathNew + 'AppRoutes.js',
  desktopBaseJsPathNew + 'SigninView.js'
];

var libFilesDesktopNew = [
  jsLibPath + 'jquery.2.1.0.js',
  jsLibPath + 'stapes.min.js',
  jsLibPath + 'path.js',
  jsLibPath + 'Pace.min.js',
  jsLibPath + 'carousel.min.js',
  jsLibPath + 'jquery.knob.js',
  jsLibPath + 'underscore.min.js',
  jsLibPath + 'parsley.min.js',
  jsLibPath + 'parsley.custom.validation.js',
  jsLibPath + 'fingerprint.min.js',
  jsLibPath + 'handlebars.runtime-v1.3.0.js',
  jsLibPath + 'awesome-select.min.js',
  jsLibPath + 'sweet-alert.min.js'
];

var desktopCssPathNew = 'webapp/content/css/experiment/';
var desktopCssFilesNew = [
  desktopCssPathNew   + 'b.css',
  desktopCssLibPath   + 'selectBoxIt.css',
  desktopCssPathNew   + 'freecharge.css',
  desktopCssLibPath   + 'throbber.css',
  desktopCssLibPath   + 'jquery.nouislider.css',
  desktopCssLibPath   + 'progressbutton.css'
];

gulp.task('builddesktopjsprodnew', function(){
  console.log("Building desktop js files for prod");
  return gulp.src(jsFilesDesktopNew)
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.new.min.js'))
    .pipe(gulp.dest(desktopDistJsPath));
});

// task to build desktop external libraries
gulp.task('builddesktoplibsprodnew', function(){
  console.log("Building desktop libs for prod");
  return gulp.src(libFilesDesktopNew)
    .pipe(uglify(uglifyConfigs))
    .pipe(concat('all.new.lib.min.js'))
    .pipe(gulp.dest(desktopDistJsPath));
});

// task to build desktop css
gulp.task('builddesktopcssnew', function() {
  gulp.src(desktopCssFilesNew)
    .pipe(minifyCSS())
    .pipe(concat('all.new.css'))
    .pipe(gulp.dest(desktopDistCssPath));
});

gulp.task('jshintdesktopnew', function(cb){
      gulp.src(jsFilesDesktopNew)
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'));
});

gulp.task('compiledesktopsassnew', function () {
    return gulp.src('webapp/content/css/experiment/scss/new.scss')
          .pipe(sass())
          .pipe(minifyCSS())
          // .pipe(csslint())
          // .pipe(csslint.reporter())
          .pipe(gulp.dest('webapp/content/css/experiment/css/'));
});

// task to compile handlebar web templates
gulp.task('buildDesktopTemplatesNew',function() {
  return gulp.src([
    'webapp/content/templates/web/experiment/home/*.tpl',
    'webapp/content/templates/web/experiment/components/*.tpl',
    'webapp/content/templates/web/experiment/coupons/*.tpl',
    'webapp/content/templates/web/experiment/payments/*.tpl',
    'webapp/content/templates/web/experiment/landing/*.tpl',
    'webapp/content/templates/web/experiment/common/*.tpl'
  ])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'webtemplates'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('all-new-web-templates.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('buildDesktopPartialsNew',function() {
  return gulp.src(['webapp/content/templates/web/experiment/partials/*.tpl','webapp/content/templates/web/experiment/partials/coupons/*.tpl','webapp/content/templates/web/experiment/partials/payments/*.tpl','webapp/content/templates/web/experiment/partials/landing/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'webpartials'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('all-new-web-partials.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('buildSuccessPageNew',function() {
  return gulp.src(['webapp/content/templates/web/experiment/rechargeSuccess/*.tpl'])
  .pipe(cleanhtml())
  .pipe(handlebars())
  .pipe(defineModule('plain'))
  .pipe(declare({
      namespace: 'successPage'
    }))
  .pipe(uglify(uglifyConfigs))
  .pipe(concat('successPageNew.js'))
  .pipe(gulp.dest(desktopBase+'/dist/'));
});

gulp.task('build-desktop-new', ['jshintdesktopnew','builddesktopjsprodnew','builddesktopcssnew','buildDesktopTemplatesNew','buildDesktopPartialsNew','buildSuccessPageNew','compiledesktopsassnew','builddesktoplibsprodnew']);

// ************************************************* //

// Add a task to render the output
gulp.task('help', taskListing);
gulp.task('build-mobile', ['buildmobilerequire','buildmobilecss','buildmobiletemplates','buildmobilepartials','combinemobiletemplates']);
gulp.task('build-desktop',['jshintdesktop','builddesktopjsprod','builddesktopcss','buildDesktopTemplates','buildDesktopPartials','buildBourbonTemplates','buildSuccessPage','sass','builddesktoplibsprod']);
