package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.exception.CouponExpireException;
import com.freecharge.api.coupon.service.exception.CouponOutOfStockException;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CouponCartService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.service.Wallet.FundDestination;

import flexjson.JSONDeserializer;

public class HyperLocalCouponActivity extends DopConsumerBase {
	private Logger logger = LoggingFactory.getLogger(getClass());
	private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
	private Map<String, String> stateMap = new ConcurrentHashMap<String, String>();

	@Autowired
	private KestrelWrapper kestrelWrapper;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private PricingService pricingService;

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private RechargeStatusIdempotencyCache idempotencyMap;
	
	@Autowired
	private CouponCartService couponCartService;

	public static final String FAILURE_REASON_FIELD_NAME = "failureReason";
	public static final String COUPON_OUT_OF_STOCK_FIELD_NAME = "couponOutOfStock";
	public static final String COUPON_EXPIRED_FIELD_NAME = "couponExpired";
	public static final String IS_REFUNDED_FIELD_NAME = "isRefunded";

	public static final String REASON_COUPON_OF_STOCK = "couponOutOfStock";
	public static final String REASON_COUPON_EXPIRED = "couponExpired";
	public static final String REASON_SOMETHING_WRONG = "someThingWrong";

	@Override
	public void consume(Object payload) {
		logger.info("Starting HCoupon fulfillment for " + payload);
		String threadName = Thread.currentThread().getName() + "-"
				+ Thread.currentThread().getId();
		String orderId = getOrderId(payload);
		if (orderId == null) {
			logger.error("Got null order id, not doing fulfilment paylod: "
					+ payload);
			return;
		}
		logger.info("Doing Fulfillment for orderId: " + orderId);
		insertInStateMap(threadName, orderId);
		try {
			paymentTransactionService.assertPaymentSuccessAndDebitWallet(
					orderId, FundDestination.HCOUPON,
					RechargeConstants.HCOUPON_TYPE);
		} catch (DuplicateRequestException e) {
			logger.error(
					"Got failure response from paymentTransactionService  for order ID ["
							+ orderId + "]. Not doing any fulfillment");
			return;
		}
		doHCouponFulfilment(orderId);
		removeFromStateMap(threadName);
		idempotencyMap.markDequeue(orderId, this.getQueueName());
		logger.info("Completed HCoupon fulfillment for " + payload
				+ " orderId: " + orderId);
	}

	private String getOrderId(Object payload) {
		Map<String, String> queueItem = jsonDeserializer
				.deserialize((String) payload);
		String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
		return orderId;
	}

	private void removeFromStateMap(String threadName) {
		stateMap.remove(threadName);
	}

	private void insertInStateMap(String threadName, String orderId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sDate = sdf.format(new Date());
		stateMap.put(threadName,
				sDate + " doing hcoupon fulfillment for orderId=" + orderId);
	}

	private void doHCouponFulfilment(String orderId) {
		boolean isRefunded = false;
		String reason = null;
		Integer couponId = null;
		boolean isSuccess = false;
		try {
			List<CouponCartItem> couponCartItems = couponCartService.getCouponCartItemsForOrderId(orderId);
			Integer userId =orderService.getUserIdForOrder(orderId);
			couponServiceProxy.doHCouponFulfilment(couponCartItems,orderId,userId);
			updateUsertTransactionHistory(orderId,
					RechargeConstants.SUCCESS_RESPONSE_CODE);
			isSuccess = true;
			logger.info("Order success orderId: " + orderId);
			logger.info("Starting email send process for orderId: " + orderId);
			// couponNotificationService.sendHCouponSuccessNotifications(orderId);
			logger.info("Success email sent completed for orderId: " + orderId);
		} catch (InvalidParameterException e) {
			logger.error("InvalidParameterException Parameter name: "
					+ e.getParameterName() + " Value: " + e.getReceivedValue()
					+ "  Message: " + e.getMessage());
			isRefunded = handleHCouponRefund(orderId,
					REASON_SOMETHING_WRONG, null);
			reason = REASON_SOMETHING_WRONG;
		} catch (CouponOutOfStockException e) {
			couponId = e.getCouponId();
			reason = REASON_COUPON_OF_STOCK;
			isRefunded = handleHCouponRefund(orderId, reason, couponId);
		} catch (CouponExpireException e) {
			couponId = e.getCouponId();
			reason = REASON_COUPON_EXPIRED;
			isRefunded = handleHCouponRefund(orderId, reason, couponId);
		} catch (Exception e) {
			logger.error(
					"Unknown error in blocking coupons for orderId: " + orderId,
					e);
			reason = REASON_SOMETHING_WRONG;
			isRefunded = handleHCouponRefund(orderId, reason, null);
		}
		couponServiceProxy.saveOrderStatus(orderId, isSuccess, reason,
				couponId, isRefunded);
		logger.info("Saved coupon order status successfully for orderId: "
				+ orderId);
	}

	private boolean handleHCouponRefund(String orderId, String reason,
			Integer couponId) {
		logger.error("Got failure response from CouponService  for order ID ["
				+ orderId + "]. Not doing any further HCoupon fulfillment");
		boolean isRefunded = false;
		try {
			doRefundForHcouponOrder(orderId);
			isRefunded = true;
			logger.info("Order refunded for orderId: " + orderId);
			logger.info("Starting failure email send process for orderId: "
					+ orderId);
//			couponNotificationService.sendHCouponFailureNotifications(orderId);
			logger.info("Failure email sent completed for orderId: " + orderId);
		} catch (Exception e) {
			logger.error("Error refunding for orderId : " + orderId, e);
		}
		if (isRefunded) {
			logger.warn("Refund successful for orderId: " + orderId
					+ " Updating refund code.");
			updateUsertTransactionHistory(orderId,
					RechargeConstants.HCOUPON_REFUND_PROCESS_CODE);
		} else {
			logger.error("Refund not successful for orderId: " + orderId
					+ " Updating failure code.");
			updateUsertTransactionHistory(orderId,
					RechargeConstants.HCOUPON_FAILURE_PROCESS_CODE);
		}
		return isRefunded;
	}

	private void updateUsertTransactionHistory(String orderId, String status) {
		Integer userId = orderService.getUserIdForOrder(orderId);
		Float amountPaid = pricingService.getPayableAmountByOrderId(orderId)
				.floatValue();
		Map<String, Object> userDetail = new HashMap<>();
		userDetail.put("amount", amountPaid);
		userDetail.put("mobileno", "");
		userDetail.put("productType", FCConstants.PRODUCT_TYPE_HCOUPON);
		userDetail.put("operatorName", "");
		userDetail.put("circleName", "");
		userDetail.put("userId", userId);
		userTransactionHistoryService.createOrUpdateRechargeHistory(orderId,
				userDetail, status);
	}

	private void doRefundForHcouponOrder(String orderId) {
		logger.error("Proceeding with refund for hcoupon orderId: " + orderId);
		// Do refund here
		PaymentTransaction paymentTransaction = paymentTransactionService
				.getSuccessfulPaymentTransactionByOrderId(orderId);

		if (paymentTransaction != null) {
			kestrelWrapper.enqueueForHCouponFailure(
					paymentTransaction.getMerchantTxnId());
			logger.info("Successfully enqueued in refund queue for orderId: "
					+ orderId + " for HCoupon.");
		} else {
			logger.error("Could not find paymentTransaction for orderId: "
					+ orderId + " while doing refund for HCoupon.");
		}
	}
}
