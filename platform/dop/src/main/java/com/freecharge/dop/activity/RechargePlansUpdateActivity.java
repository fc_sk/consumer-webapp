package com.freecharge.dop.activity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.dao.jdbc.IreffPlan;
import com.freecharge.productdata.dao.jdbc.IreffPlanDAO;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.productdata.services.RechargePlanService.EmailAction;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.bean.RechargePlanAlertBean;
import com.freecharge.sns.bean.RechargePlanAlertBean.PlanAction;
import com.mysql.jdbc.StringUtils;

import flexjson.JSONDeserializer;

/*
 * Consumer to add/remove/update the recharge plans.
 */

public class RechargePlansUpdateActivity extends DopConsumerBase {

    private Logger                                logger             = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, Object>> deserializer       = new JSONDeserializer<>();

    ObjectMapper                                  objectMapper       = new ObjectMapper();

    @Autowired
    private RechargePlanService                   rechargePlanService;

    @Autowired
    private IreffPlanDAO                          ireffPlanDAO;

    private static final Map<String, String>      IREFF_CATEGORY_MAP = FCUtil.createMap("topup", "Topup", "other",
                                                                             "Other", "3g", "3G", "data", "Data/2G");

    private RechargePlanAlertBean getAlertBean(Object payLoad) {
        try {
            Map<String, Object> queueItem = deserializer.deserialize((String) payLoad);

            Map<String, String> rechargeMessageMap = (Map<String, String>) queueItem.get("message");
            String rechargePlanMessage = objectMapper.writeValueAsString(rechargeMessageMap);

            RechargePlanAlertBean alertBean = objectMapper.readValue(rechargePlanMessage, RechargePlanAlertBean.class);
            logger.info(alertBean.toString());
            return alertBean;
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
        return null;
    }

    @Override
    public void consume(Object payLoad) {

        RechargePlanAlertBean alertBean = getAlertBean(payLoad);
        if (alertBean == null) {
            logger.error("Some error while de-serializing the data from payload " + payLoad);
            return;
        }
        logger.info("Started processing alert bean " + alertBean);

        int productId = FCConstants.productMasterMapReverse.get(alertBean.getProductType());
        
        if (alertBean.getPlanType().equals(RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL)) {
            if (!rechargePlanService.SPECIAL_OPERATOR_IDS.contains(alertBean.getOperatorId())) {
                logger.info("This case should not have happened for " + alertBean.toString() + " Not continuing with the plan for now.");
                return;
            }
        }
        
        // If there is an existing sticky plan > 7 days. Delete it
        deleteOldStickyPlan(alertBean, productId);

        if (PlanAction.ADD.getActionName().equals(alertBean.getActionType())) {

            IreffPlan ireffPlan = ireffPlanDAO.getActiveIreffPlan(alertBean.getCircleId(), alertBean.getOperatorId(),
                    alertBean.getRechargeAmount().floatValue(), productId, alertBean.getPlanType());
            
            deleteModerationPlanOnGettingIreffData(ireffPlan, alertBean, productId);

            /*
             * TODO: Need to add plan type here
             */
            RechargePlan existingPlan = rechargePlanService.getActiveRechargePlan(alertBean.getCircleId(),
                    alertBean.getOperatorId(), productId, alertBean.getRechargeAmount().floatValue());
            
            // if we find the active plan, update the status.
            if (existingPlan != null) {
                logger.info("Plan already exists in the DB for " + alertBean.toString());
                if (ireffPlan != null) {

                    boolean needToUpdatePlan = planHasOldDetails(existingPlan, ireffPlan, productId,
                            alertBean.isRecommended());
                    // if the plan details are different, disable the plan and
                    // create the new one.
                    if (needToUpdatePlan) {
                        logger.info(" Plan with id " + existingPlan.getRechargePlanId()
                                + " needs to be updated. Deleting the existing plan");
                        rechargePlanService.deleteNonStickyPlanFromDB(existingPlan);
                        logger.info("Adding the plan with updated details");
                        addIreffPlansInRechargePlanDB(ireffPlan, productId, alertBean.isRecommended());
                        return;
                    }

                    if (existingPlan.getIsRecommended() != alertBean.isRecommended()) {
                        logger.info("updating the recommend flag to " + alertBean.isRecommended()
                                + " for recharge plan id " + existingPlan.getRechargePlanId());
                        int rowsUpdated = rechargePlanService.updatePlanRecommendation(
                                existingPlan.getRechargePlanId(), alertBean.isRecommended());
                        if (rowsUpdated != 1) {
                            logger.error(" Some error while updating the recommend flag for recharge plan id "
                                    + existingPlan.getRechargePlanId());
                        }
                    }
                    return;
                } else {
                    // old plan.
                    boolean shouldDeletePlan = shouldDeleteExistingPlan(existingPlan);
                    
                    if (shouldDeletePlan) {
                        logger.info("No active ireff data found for the plan " + alertBean.toString());
                        rechargePlanService.deleteNonStickyPlanFromDB(existingPlan);
                        existingPlan = null;
                    } else {
                        logger.info("Ireff Plan is null, but not deleting existing plan " + existingPlan);
                        return;
                    }
                }
            }
            
            // Not in ireff. Add all the known details to the recharge plan and
            // push it to the moderation queue if it is not already present.
            boolean storeInMODQ = shouldPutInMODQ(ireffPlan, existingPlan, productId);
            if (storeInMODQ) {
                // NOTE: This is the temporary fix. Plans which are in
                // moderation queue *should not be* in the recharge plans table.

                RechargePlan existingRechargePlan = rechargePlanService.getRechargePlanForIreffId(alertBean
                        .getCircleId(), alertBean.getOperatorId(), productId, alertBean.getRechargeAmount()
                        .floatValue(), "MODQ");
                if (existingRechargePlan != null) {
                    logger.info("Plan already exists in moderation queue");
                    return;
                }
                
                RechargePlan rechargePlan = createPlanForModeration(alertBean, productId);
                Integer id = rechargePlanService.storeRechargePlan(rechargePlan);
                if (id == null) {
                    logger.error(" Some error in saving the recharge plans for: " + alertBean);
                    return;
                }
                rechargePlan.setRechargePlanId(id);
                rechargePlanService.addRechargePlanToModeration(rechargePlan, "Plan not found in Ireff!");
                logger.info(" Plan has been moved to moderation queue:  " + alertBean.toString());
                return;
            }

            // Update the ireff plans in the actual plan DB.
            addIreffPlansInRechargePlanDB(ireffPlan, productId, alertBean.isRecommended());
            logger.info("Ireff plan has added to the recharge plans database for " + alertBean.toString());
            return;
        }

        if (PlanAction.INVALID.getActionName().equals(alertBean.getActionType())) {
            // De-activate the plan in FC plans.
            rechargePlanService.deleteNonStickyRechargePlan(alertBean.getCircleId(), alertBean.getOperatorId(), productId,
                    alertBean.getRechargeAmount().floatValue());
            logger.info("Ireff plan has removed from the recharge plans database for " + alertBean.toString());
        }
        
        logger.info("Completed processing plans updation for " + alertBean);
    }
    
    private RechargePlan createPlanForModeration(RechargePlanAlertBean alertBean, int productId) {
        RechargePlan rechargePlan = new RechargePlan();
        rechargePlan.setAmount(BigDecimal.valueOf(alertBean.getRechargeAmount()));
        rechargePlan.setCircleMasterId(alertBean.getCircleId());
        rechargePlan.setOperatorMasterId(alertBean.getOperatorId());
        rechargePlan.setProductType(productId);
        rechargePlan.setIsRecommended(alertBean.isRecommended());
        // Plan will not be active, until it is enabled in the admin
        // panel.
        rechargePlan.setStatus(false);
        // Indicates the plan is in the moderation queue.
        rechargePlan.setIreffID("MODQ");
        rechargePlan.setCreatedTime(new Timestamp(new Date().getTime()));
        return rechargePlan;
    }

    private boolean shouldDeleteExistingPlan(RechargePlan existingPlan) {
        if (existingPlan.isSticky()) {
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        
        if (existingPlan.getIreffID() != null && existingPlan.getIreffID().equals("MODQ")
                && existingPlan.getCreatedTime() != null
                && existingPlan.getCreatedTime().compareTo(calendar.getTime()) > 0) {
            return false;
        }
        return true;
    }

    private void deleteOldStickyPlan(RechargePlanAlertBean alertBean, int productId) {
        List<RechargePlan> existingStickyPlans = rechargePlanService.getStickyRechargePlan(alertBean.getCircleId(),
                alertBean.getOperatorId(), productId, alertBean.getRechargeAmount().floatValue());
        if (existingStickyPlans == null) {
            return;
        }
        
        for (RechargePlan existingStickyPlan : existingStickyPlans) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -14);
            
            if (existingStickyPlan != null && existingStickyPlan.getCreatedTime() != null && existingStickyPlan.getCreatedTime().compareTo(calendar.getTime()) < 0) {
                logger.info("Deleting old sticky recharge plan from db " + existingStickyPlan.toString());
                rechargePlanService.forceDeleteFromDB(existingStickyPlan);
                rechargePlanService.sendEmail(existingStickyPlan, EmailAction.STICKY_PLAN_EXPIRED);
            }
        }
    }
    
    private void deleteModerationPlanOnGettingIreffData(IreffPlan ireffPlan, RechargePlanAlertBean alertBean, int productId) {
        if (ireffPlan == null) {
            return;
        }
        
        RechargePlan existingRechargePlan = rechargePlanService.getRechargePlanForIreffId(alertBean
                .getCircleId(), alertBean.getOperatorId(), productId, alertBean.getRechargeAmount()
                .floatValue(), "MODQ");
        if (existingRechargePlan == null) {
            return;
        }
        logger.info("Removing plans from moderation queue and DB as it is available in ireff");
        
        rechargePlanService.deletePlanFromModQ(existingRechargePlan.getRechargePlanId());
        rechargePlanService.forceDeleteFromDB(existingRechargePlan);
        rechargePlanService.sendEmail(existingRechargePlan, EmailAction.MODERATION_PLAN_DELETED_AS_DATA_AVAILABLE);
    }

    private boolean isEqual(String str1, String str2) {
        boolean same = (!StringUtils.isNullOrEmpty(str1) && !StringUtils.isNullOrEmpty(str2));
        if (!same)
            return false;
        return str1.equals(str2);
    }

    private boolean planHasOldDetails(RechargePlan existingPlan, IreffPlan ireffPlan, int productId, boolean recommended) {
        RechargePlan rechargePlanInIreff = convertIreffToRechargePlans(ireffPlan, productId, recommended);

        try {
            if (existingPlan.isSticky()) {
                return false;
            }
            
            if (existingPlan.getTalktime() == null || rechargePlanInIreff.getTalktime() == null
                    || existingPlan.getTalktime() != rechargePlanInIreff.getTalktime()) {
                return true;
            }
            if (!isEqual(existingPlan.getDescription(), rechargePlanInIreff.getDescription())) {
                return true;
            }
            if (!isEqual(existingPlan.getIreffID(), rechargePlanInIreff.getIreffID())) {
                return true;
            }
            if (!isEqual(existingPlan.getValidity(), rechargePlanInIreff.getValidity())) {
                return true;
            }
            return false;
        } catch (Exception e) {
            logger.error(" Some error in comparing the plan", e);
        }
        return false;
    }

    private boolean shouldPutInMODQ(IreffPlan ireffPlan, RechargePlan existingPlan, int productId) {
        if (ProductName.fromProductId(productId) != ProductName.Mobile) {
            return false;
        }
        
        if (ireffPlan != null) {
            return false;
        }
        if (existingPlan == null) {
            return true;
        }

        if (StringUtils.isNullOrEmpty(existingPlan.getIreffID())) {
            return true;
        }

        if (existingPlan.getIreffID().equals("MODQ")) {
            return false;
        }

        return true;
    }

    private void addIreffPlansInRechargePlanDB(IreffPlan ireffPlan, int productId, boolean isRecommended) {
        // Add the ireff plan to FC plans table.
        RechargePlan rechargePlan = convertIreffToRechargePlans(ireffPlan, productId, isRecommended);
        rechargePlanService.storeRechargePlan(rechargePlan);
    }

    private RechargePlan convertIreffToRechargePlans(IreffPlan ireffPlan, int productId, boolean isRecommended) {
        RechargePlan plan = new RechargePlan();
        plan.setAmount(BigDecimal.valueOf(ireffPlan.getAmount()));
        plan.setCircleMasterId(ireffPlan.getCircleId());
        plan.setCreatedTime(new Timestamp(new Date().getTime()));
        plan.setDescription(ireffPlan.getDetail());
        plan.setIreffID(ireffPlan.getIreffId());
        plan.setIsRecommended(isRecommended);
        plan.setName(IREFF_CATEGORY_MAP.get(ireffPlan.getCategory()));
        plan.setOperatorMasterId(ireffPlan.getOperatorId());
        plan.setProductType(productId);
        plan.setStatus(true);
        plan.setTalktime(BigDecimal.valueOf(ireffPlan.getTalktime()));
        plan.setValidity(ireffPlan.getValidity());
        plan.setSticky(false);
        return plan;
    }
}
