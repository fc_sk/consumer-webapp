package com.freecharge.dop.activity;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.InService.INRESPONSE;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.autorefund.AutoRefundPaymentGatewayFailures;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.services.UserTransactionHistoryService;

import flexjson.JSONDeserializer;

/**
 * Given a payment transaction this activity syncs its
 * status from the Payment Gateway and updates it in our
 * system.
 * @author arun
 *
 */
public class PaymentSyncActivity extends DopConsumerBase {
    Logger logger = Logger.getLogger(PaymentSyncActivity.class);

    private JSONDeserializer<Map<String, String>> deserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private PaymentTransactionService paymentService;
    
    @Autowired
    private PaymentRefund paymentRefund;

    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
    
    @Autowired
    private InReadDAO inReadDAO;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private AutoRefundPaymentGatewayFailures autoRefundPaymentGatewayFailures;
    
    @Override
    public void consume(Object payLoad) {
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();   
        logger.info("Processing paySync request for " + payLoad);

        Map<String, String> refundDataMap = deserializer.deserialize((String) payLoad);
        
        String activityType = refundDataMap.get(FCConstants.ACTIVITY_TYPE).toString();
        String txnId = refundDataMap.get(PaymentConstants.PAY_TXN_ID).toString();
        String dequeue = txnId + "_" + activityType;
        if (activityType.equals(PaymentConstants.IS_SWEEP)) {
            Integer payTransactionId = Integer.parseInt(txnId);
            stateMap.put(threadName, "doing paySync for mTxnId=" + payTransactionId);
            PaymentTransaction transactionToSync = paymentService.getPaymentTransactionById(payTransactionId);
            if (transactionToSync.getIsSuccessful() != null && transactionToSync.getIsSuccessful()) {
                logger.error("Txn is already successful");
            } else {
                autoRefundPaymentGatewayFailures.paymentSyncTxn(transactionToSync,false);
            }
            stateMap.remove(threadName);
            idempotencyMap.markDequeue(dequeue, this.getQueueName());
        } else if (activityType.equals(PaymentConstants.IS_REFUND)) {
            String affiliateTransId = txnId;
            stateMap.put(threadName, "doing paySync for affiliateTransId=" + affiliateTransId);
            InResponse inResponse = inReadDAO.getInResponseByOrderID(affiliateTransId);
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(affiliateTransId);
            
            if(inResponse!=null) {
                INRESPONSE statusAtIn = null;
                
                try {
                    statusAtIn = inService.getStatusAtIn(inResponse, userTransactionHistory);
                } catch (IOException e) {
                    logger.info("IOException while trying to fetch status from InResponse");
                }
        
                switch (statusAtIn) {
                    case FAILURE:
            
                        logger.info("Got failure for recharge refunding for order ID : " + affiliateTransId);
                        Set<String> refundOrderIds = new HashSet<String>(Arrays.asList(affiliateTransId));
                        Set<String> merchantOrdersToRefund = getMerchantOrderIds(refundOrderIds);
                        paymentRefund.doRefund(merchantOrdersToRefund, PaymentConstants.RECHARGE_FAILURE);
                        break;
            
                    case UNKNOWN:
                        logger.info("Got unknown recharge status for order ID : " + affiliateTransId);
            
                    default: break;
                }
            }
            stateMap.remove(threadName);
            idempotencyMap.markDequeue(dequeue, this.getQueueName());
        }
        logger.info(String.format("Done with paySync activity for txnId ID: %s, activityType: %s", txnId, activityType));
    }
    
    public Set<String> getMerchantOrderIds(Collection<String> orderIds) {
        Set<String> merchantOrderIds = new HashSet<String>();
        for (String orderId : orderIds) {
            List<PaymentTransaction> paymentTransactions = paymentService.getPaymentTransactionsByOrderId(orderId);

            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                merchantOrderIds.add(paymentTransaction.getMerchantTxnId());
            }
        }
        return merchantOrderIds;
    }
}
