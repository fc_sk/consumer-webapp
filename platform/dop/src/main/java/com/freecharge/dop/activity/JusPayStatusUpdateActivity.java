package com.freecharge.dop.activity;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.services.PaymentType.PaymentTypeName;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import flexjson.JSONDeserializer;

/**
 * Consumer to post payment success status
 * to JusPay web-hook.
 * @author arun
 *
 */
public class JusPayStatusUpdateActivity extends DopConsumerBase {

    @Autowired
    private FCProperties fcProperties;

    private Logger logger = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> deserializer = new JSONDeserializer<>();

    ObjectMapper objectMapper = new ObjectMapper();

    List<String> validPaymentTypeList = Arrays.asList(
            "Credit Card", "Debit Card", "Net Banking",
            "Cash Card", "ATM Card", "FC Balance", 
            "HDFC Bank Debit Card", "ZeroPay"
            );

    private void sendPost(String postData, String orderId) throws IOException {
        String url = fcProperties.getJusPayPaymentStatsUrl();
        HttpClient client = new HttpClient();
        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();

        connectionParams.setConnectionTimeout(fcProperties.getJusPayPaymentStatsConnectTimeout());
        connectionParams.setSoTimeout(fcProperties.getJusPayPaymentStatsReadTimeout());
        client.getHttpConnectionManager().setParams(connectionParams);

        PostMethod postMethod = new PostMethod(url);

        try {
            StringRequestEntity requestEntity = new StringRequestEntity(postData, "application/json", "UTF-8");
            int responseCode;
            String response = "";
            try {
                postMethod.setRequestEntity(requestEntity);
                responseCode = client.executeMethod(postMethod);
                response = postMethod.getResponseBodyAsString();

            } catch (IOException e) {
                logger.error("Some error while sending data to jusPay for order id " + orderId, e);
                return;
            }

            if (responseCode != HttpStatus.SC_OK) {
                logger.error("Response code " + responseCode + " while posting payment success data to jusPay for order id " + orderId);
                return;
            }

            logger.info("Response data from jusPay is : " + String.format(response));

            if (!response.contains(("SUCCESS"))) {
                logger.error("Some error while sending data to jusPay for order id " + orderId);
                return;
            }

            logger.info("Successfully posted data to jusPay for order id " + orderId);
        } finally {
            postMethod.releaseConnection();
        }
    }

    @Override
    public void consume(Object payLoad) {

        /*
         * {
  "Type" : "Notification",
  "MessageId" : "9087698f-4ae0-590d-8801-24e82af97795",
  "TopicArn" : "arn:aws:sns:ap-southeast-1:695210568016:PaymentAlerts",
  "Message" : "{\"message\" : {\n  \"messageVersion\" : \"1.0\",\n  \"timeStamp\" : 1424842201303,\n  \"messageType\" : \"PaymentSuccess\",\n  \"userId\" : 6464950,\n  \"orderId\" : \"FCVA150225135206190\",\n  \"channel\" : \"Android\",\n  \"product\" : \"Mobile\",\n  \"amount\" : 100.0,\n  \"paymentType\" : \"Net Banking\",\n  \"cardNature\" : null,\n  \"cardType\" : null,\n  \"cardBin\" : null,\n  \"cardIssuingBank\" : null,\n  \"paymentPlan\" : {\n    \"pg\" : 100.0,\n    \"credits\" : 0.0\n  },\n  \"netBankingName\" : \"SBIN\"\n}}",
  "Timestamp" : "2015-02-25T05:30:01.330Z",
  "SignatureVersion" : "1",
  "Signature" : "CTo+FhbrQjvoLSzwhzYsu40gmtIMDz1Psi4gek2o5NyULtfZmPb/9piF79xmqqJi70oPfluferktFB7yfNC71zk9ciD6RX8+K28RHcp4QCIbG3LvvdkQmbNW/2y2ofqfVuHY3Se1AmbDUNdVVurFot/BsqoiaLeA2r8TFNqON8VrJADel8lL/AAomcRwdvlddHdbO1nSD9XxEvmVLzBoVoE+wiy4l7nJLmslkZvTzyAgI6QzEDIg9UvcrmQgH+T0w/HbKZf65pQg8bR1oeTsdhXW3XdZVljGRnjEu45t/xD54uJvqTZvmBx+lA9ihbI3KGfsPjiKO9FVUTJLbEkpOg==",
  "SigningCertURL" : "https://sns.ap-southeast-1.amazonaws.com/SimpleNotificationService-d6d679a1d18e95c2f9ffcf11f4f9e198.pem",
  "UnsubscribeURL" : "https://sns.ap-southeast-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-1:695210568016:PaymentAlerts:bc1539ad-f624-4352-9a54-0a1753be4cdd"
}
         */

        Map<String, String> queueItem = deserializer.deserialize((String)payLoad);
        String queueMessage = queueItem.get("Message");
        logger.info("Message :" + queueMessage );

        try {
            Map<String, Object> paymentLoad = objectMapper.readValue(queueMessage.getBytes(), Map.class);
            Map<String, String> paymentMessageMap = (Map<String, String>) paymentLoad.get("message");
            String paymentMessage = objectMapper.writeValueAsString(paymentMessageMap);

            /**
             * {"data":[{"payment_instrument_group":"CARD","merchant_id":"fcbrowser","client_id":"fcbrowser_android","order_id":"FCTA141231112205308","payment_status":"CANCELLED"}]}
             */

            PaymentAlertBean paymentBean = objectMapper.readValue(paymentMessage, PaymentAlertBean.class);

            String messageType = paymentBean.getMessageType();
            // Send only the payment success data to juspay
            if (!PaymentStatus.PAYMENT_SUCCESS.getStatus().equals(messageType))
                return;

            if (!isValidPaymentType(paymentBean.getPaymentType(), paymentBean))
                return;

            String jusPayPostData = getJusPayPostData(paymentBean);
            logger.info("JusPay PostData: " + String.format(jusPayPostData));
            sendPost(jusPayPostData, paymentBean.getOrderId());

        } catch (IOException e) {
            logger.error("Error while parsing payload", e);
        }
    }

    @SuppressWarnings("unchecked")
    private String getJusPayPostData(PaymentAlertBean paymentBean) {
        JSONObject jo = new JSONObject();
        jo.put("merchant_id", "fcbrowser");
        jo.put("client_id", "fcbrowser_android");
        jo.put("payment_status", "SUCCESS");
        jo.put("order_id", paymentBean.getOrderId());

        String paymentTypeUsed = null;
        String cardType = null;
        switch (fromDisplayString(paymentBean.getPaymentType())) {
            case ATM_CARD:
            case CASH_CARD:
                paymentTypeUsed = "CARD";
                break;

            case CREDIT_CARD:
                paymentTypeUsed = "CARD";
                cardType =  "CREDIT";
                break;

            case DEBIT_CARD:
            case HDFC_DEBIT_CARD:
                paymentTypeUsed = "CARD";
                cardType = "DEBIT";
                break;

            case NET_BANKING:
                paymentTypeUsed = "NETBANKING";
                break;

            case FC_BALANCE:
            case ZERO_PAY:
            default: throw new IllegalArgumentException("Invalid payment type to send the data to jusPay");
        }

        jo.put("payment_instrument_group", paymentTypeUsed);
        if (cardType != null) {
            jo.put("card_type", cardType);
        }

        JSONArray ja = new JSONArray();
        ja.add(jo);

        JSONObject toReturn = new JSONObject();
        toReturn.put("data", ja);

        return toReturn.toString();
    }

    private PaymentTypeName fromDisplayString(String displayString) {
        switch (displayString) {
            case "Credit Card": return PaymentTypeName.CREDIT_CARD;
            case "Debit Card": return PaymentTypeName.DEBIT_CARD;
            case "Net Banking": return PaymentTypeName.NET_BANKING;
            case "Cash Card": return PaymentTypeName.CASH_CARD;
            case "ATM Card": return PaymentTypeName.ATM_CARD;
            case "FC Balance": return PaymentTypeName.FC_BALANCE;
            case "HDFC Bank Debit Card": return PaymentTypeName.HDFC_DEBIT_CARD;
            case "ZeroPay": return PaymentTypeName.ZERO_PAY;

            default: throw new IllegalArgumentException("Unknown payment type");
        }
    }

    private boolean isValidPaymentType(String paymentType, PaymentAlertBean paymentBean) {
        boolean Valid =  validPaymentTypeList.contains(paymentType);
        if (!Valid) {
            logger.error("Unknown payment type: " + String.valueOf(paymentBean.getPaymentType()) + " for order id " + paymentBean.getOrderId() );
            return false;
        }

        PaymentTypeName paymentTypeName = fromDisplayString(paymentBean.getPaymentType());
        if (paymentTypeName == PaymentTypeName.ZERO_PAY ||
                paymentTypeName == PaymentTypeName.FC_BALANCE) {
            return false;
        }
        return true;
    }
}
