package com.freecharge.dop.activity;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.freecharge.common.framework.properties.AppConfigService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.wallet.WalletWrapper;

import flexjson.JSONDeserializer;

public class FailureFulfilmentActivity extends DopConsumerBase {
    private Logger                                logger           = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private UserTransactionHistoryService         userTransactionHistoryService;

    @Autowired
    private PaymentTransactionService             paymentTransactionService;

    @Autowired
    private InService                             inService;

    @Autowired
    KestrelWrapper                                kestrelWrapper;

    @Autowired
    BillPaymentService                            billPaymentService;

    @Autowired
    private OrderIdSlaveDAO                       orderIdDAO;

    @Autowired
    PaymentPlanService                            paymentPlanService;

    @Autowired
    WalletWrapper                                 walletWrapper;

    @Autowired
    PricingService                                pricingService;

    @Autowired
    PaymentRefund                                 paymentRefund;

    @Autowired
    AppConfigService                              appConfigService;

    @Override
    public void consume(Object payLoad) {
        logger.info("Processing Failed Fulfilment request for " + payLoad);

        Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);
        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);

        UserTransactionHistory uth = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);

        boolean isRefundable = FCUtil.isRefundableUserTxnForPaySuccess(uth.getTransactionStatus());
        logger.info("Transaction status for order " + orderId + " is " + uth.getTransactionStatus());
        if (!isRefundable) {
            logger.info("Transaction status for order " + orderId + " is " + uth.getTransactionStatus()
                    + " ( Non Refundable ). So not proceeding.");
            return;
        }
        PaymentPlan paymentPlan = null;
        try {
            paymentPlan = paymentPlanService.getPaymentPlan(orderId);
        } catch (PaymentPlanNotFoundException e1) {
            logger.error("Unable to find payment plan for " + orderId, e1);
        }
        if (paymentPlan == null) {
            return;
        }
        List<PaymentTransaction> successPaymentTxn = paymentTransactionService
                .getSuccessfulPaymentTransactionsByOrderId(orderId);

        // Adding a check in case the order is already processed by new refund flow
        if (appConfigService.isNewFailureFulfilmentEnabled() &&
                paymentRefund.isOrderRefundedByNewRefundFlow(orderId, successPaymentTxn)) {
            logger.info("The order " + orderId + " is acted on by new refund system, so returning");
            return;
        }

        if (successPaymentTxn == null || successPaymentTxn.isEmpty()) {
            logger.error("No Successful payment for orderId " + orderId + ". Updating UTH status to payment failure.");
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            return;
        }

        if (paymentPlan.isPartialPayment() && successPaymentTxn.size() < 2) {
            logger.info("Partial payment with some payment failure for " + orderId);
            String mtxnId = successPaymentTxn.get(0).getMerchantTxnId();
            List<PaymentTransaction> allPaymentTxn = paymentTransactionService
                    .getPaymentTransactionByMerchantOrderId(mtxnId);
            for (PaymentTransaction paymentTransaction : allPaymentTxn) {
                if (paymentTransaction.getIsSuccessful() == null || !paymentTransaction.getIsSuccessful()) {
                    paymentTransactionService.syncfromPGData(paymentTransaction, false);
                    successPaymentTxn = paymentTransactionService.getSuccessfulPaymentTransactionsByOrderId(orderId);
                }
            }
        }
        
        List<PaymentRefundTransaction> refundList = paymentRefund.fetchAndUpdateRefunds(successPaymentTxn);
        
        boolean isPaymentPlanIssue = paymentPlanService.isPaymentPlanMismatch(orderId, successPaymentTxn);

        if (isPaymentPlanIssue) {
            logger.error("OrderId "
                    + orderId
                    + " has a payment plan related issue. Updating UTH status to TRANSACTION_STATUS_INVALID_PAYMENT_PLAN.");
            userTransactionHistoryService.updateStatus(orderId,
                    PaymentConstants.TRANSACTION_STATUS_INVALID_PAYMENT_PLAN);
        }
        
        try {
            validateAndProcessActualRefunds(orderId, successPaymentTxn, refundList);
        } catch (PaymentPlanNotFoundException e) {
            logger.error("Payment plan not found for Order " + orderId, e);
            userTransactionHistoryService.updateStatus(orderId,
                    PaymentConstants.TRANSACTION_STATUS_INVALID_PAYMENT_PLAN);
        } catch (Exception e) {
            logger.error("Failed to process/update refunds for order " + orderId, e);
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_FAILED);
        }
    }

    private void validateAndProcessActualRefunds(String orderId, List<PaymentTransaction> successPaymentTxns,
            List<PaymentRefundTransaction> refundList)
            throws NumberFormatException, Exception {
        Double amountToBeRefunded = paymentRefund.getTotalAmountToBeRefunded(successPaymentTxns);
        Double totalRefundedAmount = FCUtil.calculateTotalRefunds(refundList);

        logger.info("AmountToBeRefunded = " + amountToBeRefunded + ", ActualAmountRefunded : " + totalRefundedAmount
                + " for orderId " + orderId);

        if (amountToBeRefunded.compareTo(totalRefundedAmount) == 0) {
            logger.info("All refunds done for " + orderId + ". Marking UTH as refund done");
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_DONE);
            return;
        } else {
            logger.info("Amount to be Refunded for " + orderId + " is " + amountToBeRefunded + ", Actual Refund is : "
                    + totalRefundedAmount);
            if (amountToBeRefunded.compareTo(totalRefundedAmount) > 0) {
                logger.info("Need to refund rest of the amount to the customer. Balance: "
                        + (amountToBeRefunded - totalRefundedAmount));
                logger.info("Attempting a manual refund here for " + orderId);
                Set<String> mtxnIds = getMtxnIds(successPaymentTxns);
                Set<String> oldMtxnIds = new HashSet<String>();
                if(kestrelWrapper.shouldEnqueueForNewRefund()) {
                    for(String mtxnId : mtxnIds) {
                        if(paymentRefund.isEligibleForNewRefund(mtxnId)) {
                            Double refundAmount = paymentRefund.calculateRefundAmountForMtxnId(mtxnId);
                            kestrelWrapper.enqueueNewRefundWithParams(mtxnId, PaymentConstants.RECHARGE_FAILURE,
                                    refundAmount);
                            logger.info("Enqueued mtxnId: " + mtxnId + " for new refund flow");
                        } else {
                            oldMtxnIds.add(mtxnId);
                        }
                    }
                    logger.info("All refunds done via new refund system for " + orderId + ". Marking UTH as refund done");
                    userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_DONE);
                } else {
                    paymentRefund.doRefund(mtxnIds, PaymentConstants.RECHARGE_FAILURE);
                    validateAndUpdateLatestRefundStatus(orderId, amountToBeRefunded, successPaymentTxns);
                }
                if(!FCUtil.isEmpty(oldMtxnIds)) {
                    paymentRefund.doRefund(oldMtxnIds, PaymentConstants.RECHARGE_FAILURE);
                    validateAndUpdateLatestRefundStatus(orderId, amountToBeRefunded, successPaymentTxns);
                }
            } else {
                logger.error("Refunded excess amount to the customer. Difference: "
                        + (amountToBeRefunded - totalRefundedAmount));
                userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_EXCESS_REFUND);
                return;
            }
        }

    }

    private void validateAndUpdateLatestRefundStatus(String orderId, Double amountToBeRefunded,
            List<PaymentTransaction> successPaymentTxns) {
        List<PaymentRefundTransaction> refunds = paymentRefund.fetchAndUpdateRefunds(successPaymentTxns);
        Double totalRefundedAmount = FCUtil.calculateTotalRefunds(refunds);

        if (amountToBeRefunded.compareTo(totalRefundedAmount) == 0) {
            logger.info("All refunds done for " + orderId + ". Marking UTH as refund done");
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_DONE);
        } else {
            logger.info("Refund not completed for " + orderId + ". Marking UTH as refund failed");
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_FAILED);
        }
    }

    private Set<String> getMtxnIds(List<PaymentTransaction> successPaymentTxns) {
        Set<String> mtxns = new HashSet<>();
        for (PaymentTransaction paymentTransaction : successPaymentTxns) {
            mtxns.add(paymentTransaction.getMerchantTxnId());
        }
        return mtxns;
    }
}
