package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;

import flexjson.JSONDeserializer;

/**
 * A wrapper activity that consumes order IDs to be fulfilled and delegates the actual
 * fulfillment to {@link FulfillmentService}.
 * @author arun
 *
 */
public class FulfillmentActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
    private Map<String, String> stateMap = new ConcurrentHashMap<String, String>();
    
    @Autowired
    private InService inService;
    
    @Autowired
    protected MetricsClient metricsClient;

    @Autowired
    private FulfillmentService fulfillmentService;
    
    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
   
    @Override
    public void consume(Object payLoad) {
        logger.info("Processing fulfillment request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String)payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
        Boolean shouldSendSMS = Boolean.valueOf(queueItem.get(PaymentConstants.SHOULD_SEND_SMS_KEY));
        Boolean isRetrigger = Boolean.valueOf(queueItem.get(FCConstants.IS_RETRIGGER));
        long startTimeInTxnData = System.currentTimeMillis();
        InTransactionData inTransactionData = inService.getInTransactionData(orderId);
        long endTimeInTxnData = System.currentTimeMillis();
        metricsClient.recordLatency("Fulfillment.inTransactionData.", "Latency", endTimeInTxnData - startTimeInTxnData);
        
        if (inTransactionData == null) {
            logger.error("Got null InTransaction data for order ID [" + orderId + "]. Not doing any fulfillment");
            return;
        }
        
        InResponse inResponse = inTransactionData.getInResponse();
        
        if (inResponse == null) {
            logger.error("Got null InResponse for order ID [" + orderId + "]. Not doing any fulfillment");
            return;
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sDate= sdf.format(new Date());

        stateMap.put(threadName, sDate + " doing fulfillment for orderId=" + orderId);
        try {
            fulfillmentService.doFulfillment(orderId, shouldSendSMS, isRetrigger, false);
        } catch (Exception e) {
            logger.error("Caught Exception on doing fulfilment for order : " + orderId, e);
        } finally {
            stateMap.remove(threadName);
            idempotencyMap.markDequeue(orderId, this.getQueueName());
        }
    }

    public String getState() {
        String ret = "";
        int count = 0;
        for (Map.Entry<String, String> entry : stateMap.entrySet()) {
            ret = ret + entry.getKey() + ": " + entry.getValue() + "\n";
            count = count+1;
        }
        return ret + "Count=" + count;
    }
}