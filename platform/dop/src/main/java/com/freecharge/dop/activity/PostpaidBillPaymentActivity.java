package com.freecharge.dop.activity;


public class PostpaidBillPaymentActivity extends BillPaymentActivity {
    @Override
    protected void fulfillOrderId(String orderId) {
        /**
         * Idempotency check.
         */
        if (billPaymentService.validatePostpaidRetry(orderId)) {
            billPaymentService.fulfillPostpaidSynchronously(orderId);
        } else {
            logger.info("Billpayment status for Order ID: [" + orderId + "] is in some unknown state not going ahead with postpaid payment");
        }
    }

    @Override
    protected String getActivityName() {
        return "PostPaid";
    }
    
}
