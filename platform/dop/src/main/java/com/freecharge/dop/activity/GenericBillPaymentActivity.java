package com.freecharge.dop.activity;

public class GenericBillPaymentActivity extends BillPaymentActivity {

    @Override
    protected void fulfillOrderId(String orderId) {
        if (billPaymentService.validateBillPayRetry(orderId)) {
            billPaymentService.fulfillBillpaymentSynchronously(orderId);
        } else {
            logger.info("BillTransaction for Order ID: [" + orderId + "] is in some unknown state not going ahead with generic bill payment");
        }
    }

    @Override
    protected String getActivityName() {
        return "GenericBillPayment";
    }

}
