package com.freecharge.dop.framework;


/**
 * All DOP consumers should implement this interface.
 */
public interface DopConsumer {
    /**
     * Set the queue consumer wait period. If the queue is empty, this number represents how long the queue waits for an item to be available before returning.
     * @param consumerWaitPeriod
     */
    public void setConsumerWaitPeriod(long consumerWaitPeriod);
    public long getConsumerWaitPeriod();
    public void setNoOfConsumers(int noOfConsumers);
    public int getNoOfConsumers();
    public void setQueueName(String queueName);
    public String getQueueName();
    public String getState();
    public void consume(Object object);
    public int getNoOfPollers();
    public void setNoOfPollers(int noOfPollers);
    public void setNoOfSQSPollers(int noOfSqsPollers);
    public int getNoOfSQSPollers();
}
