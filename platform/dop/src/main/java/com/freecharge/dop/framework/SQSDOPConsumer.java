package com.freecharge.dop.framework;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.ListQueuesRequest;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.dop.framework.exception.QueuePollerException;
import com.freecharge.platform.QueueConstants;

import flexjson.JSONDeserializer;

/**
 * Similar to {@link KestrelDopConsumer} but this one
 * uses AWS-SQS for queue instead of Kestrel.
 * @author arun
 *
 */
public class SQSDOPConsumer extends QueuePoller {
    private String messageToDelete;
    
    private AmazonSQSClient sqsClient;
    
    private AppConfigService appConfig;
    
    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
    
    private final int MINIMUM_POLL_WAIT_TIME = 15;
    private final int RANDOMIZED_POLL_WAIT_PERIOD = 5;
    
    public void setSqsClient(AmazonSQSClient sqsClient) {
        this.sqsClient = sqsClient;
    }
    
    public void setAppConfig(AppConfigService appConfig) {
        this.appConfig = appConfig;
    }
    
    @Override
    protected Object pollForPayLoad() throws QueuePollerException {
        long start =  System.currentTimeMillis();
        final String queueName = getQueueEndPoint();
        logger.info("queueName for consumer" + queueName);
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueName);
        
        receiveMessageRequest.setWaitTimeSeconds(RandomUtils.nextInt(RANDOMIZED_POLL_WAIT_PERIOD) + MINIMUM_POLL_WAIT_TIME);
        receiveMessageRequest.withMaxNumberOfMessages(1); 
        receiveMessageRequest.withVisibilityTimeout(120); // TODO: Config; this is in seconds.
        
        try {
            ReceiveMessageResult response = sqsClient.receiveMessage(receiveMessageRequest);
            final long endTime = System.currentTimeMillis();
            
            metricsClient.recordLatency("SQS", "Read.Latency" + dopConsumer.getQueueName() + ".Success" , endTime - start);
            metricsClient.recordEvent("SQS", "Read." + dopConsumer.getQueueName(), "Success");
            
            final List<Message> messageList = response.getMessages();
            
            if (messageList.isEmpty()) {
                metricsClient.recordEvent("SQS", "Read.PayLoad" + dopConsumer.getQueueName(), "EmptyPacket");
                metricsClient.recordLatency("SQS", "Read.PayLoad.Latency" + dopConsumer.getQueueName() + ".EmptyPacket" , endTime - start);
                return null;
            }
            
            metricsClient.recordLatency("SQS", "Read.PayLoad.Latency" + dopConsumer.getQueueName() + ".Success" , endTime - start);
            metricsClient.recordEvent("SQS", "Read.PayLoad" + dopConsumer.getQueueName(), "Success");
            
            
            Message message  = messageList.get(0);
            this.messageToDelete = message.getReceiptHandle();
            
            final String messageBody = message.getBody();
            logger.info("SQSDOPConsumer pollForPayLoad queueName : " + queueName + " response recieved: " + messageBody);
            if (StringUtils.isNotBlank(messageBody)) {
                Map<String, String> dataMap = jsonDeserializer.deserialize(messageBody);
                
                String entryTimeString = dataMap.get(QueueConstants.ENTRY_TIME);
                
                if (StringUtils.isNotBlank(entryTimeString)) {
                    long entryTime = Long.parseLong(entryTimeString);
                    metricsClient.recordLatency("SQS", "Queue.WaitTime." + dopConsumer.getQueueName(), endTime - entryTime);
                }
            }
            
            return messageBody;
        } catch (Exception e) {
            logger.error("Exception whie reading from SQS", e);
            metricsClient.recordLatency("SQS", "Read.Latency" + dopConsumer.getQueueName() + ".Error" , System.currentTimeMillis() - start);
            metricsClient.recordEvent("SQS", "Read." + dopConsumer.getQueueName(), "Error");
            throw new QueuePollerException(e);
        }
    }

    @Override
    protected void deQueue() {
        if (this.messageToDelete != null) {
            DeleteMessageRequest delegeRequest = new DeleteMessageRequest();
            delegeRequest.setQueueUrl(getQueueEndPoint());
            delegeRequest.setReceiptHandle(this.messageToDelete);

            long start = System.currentTimeMillis();
            
            try {
                sqsClient.deleteMessage(delegeRequest);
                logger.info("Deleted : ### "+ delegeRequest);

                metricsClient.recordLatency("SQS", "Dequeue.Latency" + dopConsumer.getQueueName() + ".Success" , System.currentTimeMillis() - start);
                metricsClient.recordEvent("SQS", "Dequeue." + dopConsumer.getQueueName(), "Success");
            } catch (Exception e) {
                logger.error("Exception Deleting : ### "+ delegeRequest);
                logger.error("Error while deleting message", e);
                metricsClient.recordLatency("SQS", "Dequeue.Latency" + dopConsumer.getQueueName() + ".Error" , System.currentTimeMillis() - start);
                metricsClient.recordEvent("SQS", "Dequeue." + dopConsumer.getQueueName(), "Error");
            }
        }
    }

    @Override
    protected void handleEmptyPayLoad() {
        logger.debug("No payload. Sleeping for " + fcProperties.getSQSPollSleepPeriodSeconds() + " seconds for queue: " + getQueueEndPoint());
        
        if (fcProperties.getSQSPollSleepPeriodSeconds() > 0) {
            interruptibleSleep(fcProperties.getSQSPollSleepPeriodSeconds(), TimeUnit.SECONDS);
        }
    }

    @Override
    protected void init() {
        if (StringUtils.isBlank(appConfig.getStackID())) {
            throw new IllegalStateException("Stack ID can not be null/empty");
        }

        if (!doesQueueExist()) {
            logger.info("Queue Does Not Exist : " + getStackSpecificQueueName());

            createQueue();
        }
    }

    private void createQueue() {
        CreateQueueRequest cq = new CreateQueueRequest();
        cq.setQueueName(getStackSpecificQueueName());
        
        CreateQueueResult queue = sqsClient.createQueue(cq);
        
        logger.info("Created queue: " + queue.getQueueUrl());
    }

    private boolean doesQueueExist() {
        ListQueuesRequest queueList = new ListQueuesRequest(getStackSpecificQueuePrefix());
        ListQueuesResult queues = sqsClient.listQueues(queueList);

        for (String queueUrl : queues.getQueueUrls()) {
        	logger.info("SQSDOPConsumer doesQueueExist queueUrl: " + queueUrl);
            if (StringUtils.equals(getQueueEndPoint(), queueUrl)) {
                logger.info("Queue Exists : " + getStackSpecificQueueName() + " : " + queueUrl);
                //return true;
            }
        }
        
        return true;
    }
    
    private String getStackSpecificQueueName() {
        return appConfig.getStackID() + "_" + dopConsumer.getQueueName();
    }

    private String getStackSpecificQueuePrefix() {
        return appConfig.getStackID() + "_";
    }

    private String getQueueEndPoint() {
        final String queueName =  fcProperties.getConsumerSQSQueueMumbaiEndPoint() + "/" + getStackSpecificQueueName();
        return queueName;
    }
}
