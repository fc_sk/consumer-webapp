package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.POSTFulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;

import flexjson.JSONDeserializer;
/**
 * A wrapper activity that consumes order IDs for notifications Retry
 * 
 * @author Ruchika
 *
 */
public class SkippedFFTaskActivity extends DopConsumerBase {
	private Logger logger = LoggingFactory.getLogger(getClass());

	private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
	private Map<String, String> stateMap = new ConcurrentHashMap<String, String>();

	@Autowired
	private POSTFulfillmentService postFulfillmentService;

	@Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
	
	// NUll check pending
	@Override
	public void consume(Object payLoad) {
		logger.info("Processing skippedTask fulfillment request for " + payLoad);
		String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
		Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);
		String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);			
		try {
			postFulfillmentService.proccessSkippedRecordListForFF(orderId);
		} catch (Exception ex) {
			logger.error("Exception in SkippedFFTask : " + ex + ", For ORDER_ID : " + orderId);
		}finally {
            idempotencyMap.markDequeue(orderId, this.getQueueName());
		}
	}

		
	}

