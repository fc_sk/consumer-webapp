package com.freecharge.dop.activity;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;

import flexjson.JSONDeserializer;

public class RefundActivity extends DopConsumerBase {
    Logger logger = Logger.getLogger(RefundActivity.class);

    private JSONDeserializer<Map<String, Object>> refundDataDeserializer = new JSONDeserializer<Map<String, Object>>();

    @Autowired
    private PaymentRefund paymentRefund;
    
    @Autowired
    private MetricsClient metricsClient;

    private void doRefund(String mtxnId, String refundType) {
        logger.info("About to Refund for order ID : " + mtxnId);
        Set<String> orderSet = new HashSet<String>();
        orderSet.add(mtxnId);
        
        try {
            paymentRefund.doRefund(orderSet, refundType);
        } catch(Exception e) {
            logger.error("Something bad happened while refunding for mtxn ID: [" + mtxnId + "]. Refund Type: [" + refundType + "]", e);
        }
        
        logger.info("Done with refund activity for order ID: " + mtxnId);
    }

    @Override
    public void consume(Object refundInitiateDatum) {
        // get Thread name and put in hashmap
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();   
        logger.info("Processing refund request for " + refundInitiateDatum);

        Map<String, Object> refundDataMap = refundDataDeserializer.deserialize((String) refundInitiateDatum);

        String mtxnId = (String) refundDataMap.get(PaymentConstants.ORDER_ID_KEY);
        String refundType = (String) refundDataMap.get(PaymentConstants.REFUND_TYPE_KEY);
        
        stateMap.put(threadName, "doing refund for mTxnId=" + mtxnId + ", refundType=" + refundType);
        doRefund(mtxnId, refundType);
        
        metricsClient.recordLatency("UserSession", "RefundComplete", mtxnId, MetricsClient.EventName.RechargeFailure);
        
        stateMap.remove(threadName);
    }
}
