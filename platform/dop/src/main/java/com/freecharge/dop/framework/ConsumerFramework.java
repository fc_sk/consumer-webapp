package com.freecharge.dop.framework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.dop.activity.DopConsumerBase;
import com.freecharge.dop.framework.telnet.TelnetServer;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.freecharge.common.comm.google.GoogleOAuthService;

/**

            ConsumerFramework
   +------------------------------------+
   |                                    |
   |  - get all beans of type DopConsumer
   |  - create a KestrelDopConsumer for |         KestrelDopConsumer
   |    each DopConsumer                | +-----------------------------+
   |  - start KestrelDopConsumer in a   | |  contains                   |
   |    Thread                          | |   - DopConsumer             |
   |  - Put all threads in the instances| |   - ExecutorService         |
   |    map                             | |                             |
   |                                    | +-----------------------------+
   |                                    |
   +------------------------------------+

            +---------------+
            |  DopConsumer  |
            +-------+-------+
                    |
           +--------+---------+
           |  DopConsumerBase |
           +--------/-------\-+
                   /         \
                  /           \
 +---------------/--+       +--\-------------+
 | RechargeActivity |       | RefundActivity |
 +------------------+       +----------------+

*/


/**
 * This is the class that bootstraps the whole kestrel consumer framework.
 */
public class ConsumerFramework implements TelnetCommunicator {
    public static final String MALFORMED_QUERY = "Malformed query";
    final Map<String, Thread> instances = new HashMap<String, Thread>();
    private Logger logger = LoggingFactory.getLogger(getClass());
    final List<QueuePoller> consumers = new LinkedList<>();
    private final String consumerPropertiesFile = "consumer.properties";

    public static void main(String[] args) throws IOException {
        ConsumerFramework consumerFramework = new ConsumerFramework();
        consumerFramework.execute();
    }

    public void execute() throws IOException {
        logger.info("Initializing consumers");
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        MetricsClient metricsClient = ctx.getBean(MetricsClient.class);
        final AmazonSQSClient sqsClient = (AmazonSQSClient) ctx.getBean("consumerSQSClient");
        
        logger.info("Spring context initialization done");

        FCProperties fcProperties = ((FCProperties) ctx.getBean(FCProperties.getBeanName()));
        AppConfigService appConfig = ctx.getBean(AppConfigService.class);
        
        fcProperties.updateProperties(this.consumerPropertiesFile);

        Map<String, DopConsumerBase> map = ctx.getBeansOfType(DopConsumerBase.class);
        //final List<KestrelDopConsumer> consumers = new LinkedList<KestrelDopConsumer>();
        final StopSwitch stopSwitch = new StopSwitch();
        
        List<String> enabledConsumers = getEnabledConsumers(fcProperties);
        
        boolean isAllConsumersEnabled = true;
        if (!enabledConsumers.isEmpty() && !enabledConsumers.contains("ALL")) {
            isAllConsumersEnabled = false;
        }

        for (DopConsumerBase dopConsumer : map.values()) {
            logger.info(String.format("Initializing consumer %s - start", dopConsumer.getClass().getSimpleName()));
            if (!isAllConsumersEnabled) {
                String queueName = dopConsumer.getQueueName();
                if (!enabledConsumers.contains(queueName)) {
                    logger.info("Skipping " + queueName + " as it is not enabled in this machine");
                    continue;
                } else {
                    logger.info("Consumer Queue " + queueName + " is enabled in this machine");
                }
            }
            final ExecutorService consumerThreadPool = createConsumerThreadPool(dopConsumer);
            final Semaphore workerPermit = new Semaphore(dopConsumer.getNoOfConsumers());

            for (int i = 0; i < dopConsumer.getNoOfSQSPollers(); i++) {
                QueuePoller sqsDopConsumer = createSQSConsumer(sqsClient, metricsClient, stopSwitch, dopConsumer, consumerThreadPool, workerPermit, fcProperties, appConfig);
                consumers.add(sqsDopConsumer);
                Thread sqsPoller = new Thread(sqsDopConsumer);
                sqsPoller.setName("SQSPollerThraed-" + dopConsumer.getQueueName() + "-" + i);
                instances.put(dopConsumer.getClass().getSimpleName(), sqsPoller);
                sqsPoller.start();
            }
            
            logger.info(String.format("Initializing consumer %s - end", dopConsumer.getClass().getSimpleName()));
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                final CountDownLatch countDownLatch = new CountDownLatch(consumers.size());

                logger.info("Consumer framework shut down - start");
                stopSwitch.setStop(true);

                for (final QueuePoller consumer : consumers) {
                    new Thread() {
                        public void run() {
                            logger.info(String.format("Shutting down consumer %s - start", getConsumerName(consumer)));
                            consumer.shutDown();
                            try {
                                logger.info(String.format("Awaiting %s consumer shut down", getConsumerName(consumer)));
                                //This is an arbitrarily chosen time, not a scientific one.
                                consumer.awaitShutdown(5, TimeUnit.MINUTES);
                                
                                if (consumer.getDopConsumer().getPendingObjectsCount() != 0) {
                                    logger.error(consumer.getDopConsumer().getQueueName() + " queue still had "
                                            + consumer.getDopConsumer().getPendingObjectsCount()
                                            + " items at the time killing");
                                }
                            } catch (InterruptedException e) {
                                logger.error(e);
                            } finally {
                                countDownLatch.countDown();
                            }
                            logger.info(String.format("Shutting down consumer %s - end", getConsumerName(consumer)));
                        }
                    }.start();
                }

                try {
                    logger.info("Waiting for all consumers to shut down");
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    logger.error(e);
                    Thread.currentThread().interrupt();
                }
                logger.info("Shutting down SQSClient");
                sqsClient.shutdown();
                logger.info("Consumer framework shut down - end");
            }
        });

        new TelnetServer(fcProperties.getConsumerFrameworkTelnetPort(), this).run();

        this.deamonize();
    }

    private List<String> getEnabledConsumers(FCProperties fcProperties) {
        String activeConsumers = fcProperties.getActiveConsumer();
        if (StringUtils.isEmpty(activeConsumers)) {
            return new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(activeConsumers.split(",")));
    }

    private ExecutorService createConsumerThreadPool(DopConsumerBase dopConsumer) {
        String consumerThreadName = "ConsumerThread-" + dopConsumer.getQueueName() + "-%d";
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat(consumerThreadName).build();
        final ExecutorService consumerThreadPool = Executors.newCachedThreadPool(namedThreadFactory);
        ((ThreadPoolExecutor) consumerThreadPool).setRejectedExecutionHandler(new CallerRunsPolicy());
        ((ThreadPoolExecutor) consumerThreadPool).setMaximumPoolSize(dopConsumer.getNoOfConsumers());
        
        return consumerThreadPool;
    }

    private QueuePoller createSQSConsumer(AmazonSQSClient sqsClient, MetricsClient metricsClient, 
            final StopSwitch stopSwitch, DopConsumerBase dopConsumer, 
            ExecutorService consumerThreadPool, Semaphore workerPermit, 
            FCProperties fcProperties, AppConfigService appConfig) {
        SQSDOPConsumer sqsDopConsumer = new SQSDOPConsumer();
        
        sqsDopConsumer.setSqsClient(sqsClient);
        sqsDopConsumer.setDopConsumer(dopConsumer);
        sqsDopConsumer.setStopSwitch(stopSwitch);
        sqsDopConsumer.setExecutorService(consumerThreadPool);
        sqsDopConsumer.setWorkerPermit(workerPermit);
        sqsDopConsumer.setMetricsClient(metricsClient);
        sqsDopConsumer.setFcProperties(fcProperties);
        sqsDopConsumer.setAppConfig(appConfig);
        
        return sqsDopConsumer;
    }

    private String getConsumerName(QueuePoller kestrelDopConsumer) {
        return kestrelDopConsumer.getDopConsumer().getClass().getSimpleName();
    }

    private static boolean isStatQuery(String query) {
        return query.startsWith("STAT ");
    }

    private static boolean isConsumersListQuery(String query) {
        return query.startsWith("CONSUMERS LIST");
    }  
    
    private static boolean isConsumerStatQuery(String query) {
        return query.startsWith("CONSUMERSTAT ");
    }  
    
    private static boolean isInstancesListQuery(String query) {
        return query.startsWith("INSTANCES LIST");
    }  
    
    private static String getConsumerName(String query) {
        String[] splits = query.split("\\s+");

        if (splits.length != 2) {
            return MALFORMED_QUERY;
        }

        return splits[1].trim();
    }

    @Override
    public String getResponse(String command) {
        if (isStatQuery(command)) {
            String consumerName = getConsumerName(command);
            if (MALFORMED_QUERY.equals(consumerName)) {
                return MALFORMED_QUERY;
            }

            Thread thread = instances.get(consumerName);
            if (thread != null) {
                return String.valueOf(thread.isAlive());
            } else {
                return String .valueOf(false);
            }
        }  else if (isConsumersListQuery(command)) {
            // Go over the consumers List
            String ret = "";
            int count = 0;
            for (QueuePoller kdc : consumers) {
               ret = ret + "queue=" + kdc.getDopConsumer().getQueueName() + ", ";
               ret = ret + "numConsumers=" + kdc.getDopConsumer().getNoOfConsumers() + ", ";
               ret = ret + "\n";
               count = count + 1;
            }
            return ret + "Count=" + count;
        } else if (isConsumerStatQuery(command)) {
            String queueName = getConsumerName(command);
            if (MALFORMED_QUERY.equals(queueName)) {
                return MALFORMED_QUERY;
            }
            
            String ret = "No KestrelDopConsumer with queue=" + queueName;
            for (QueuePoller kdc : consumers) {
                String qName = kdc.getDopConsumer().getQueueName();
                if (qName.equals(queueName)) {
                    ret = kdc.getDopConsumer().getState();
                }
            }
            return ret;
        } else if (isInstancesListQuery(command)) {
            String ret = "";
            int count = 0;
            for (Map.Entry<String, Thread> entry : instances.entrySet()) {
                Thread thread = entry.getValue();
                ret = ret + entry.getKey() + " " + String.valueOf(thread.isAlive()) + "\n";
                count = count+1;
            }
            return ret + "Count=" + count;
        } 
        
        return "Unknown query";
    }

    /**
     * When this process starts, the pid of this process will be passed in as a system property daemon.pidfile.
     * @return The pid file representing this process.
     */
    private File getPidFile() {
        return new File(System.getProperty("consumers.pidfile"));
    }

    /**
     * Daemonize the java processing running. Idea taken from - http://barelyenough.org/blog/2005/03/java-daemon/
     */
    private void deamonize() {
        getPidFile().deleteOnExit();
        System.out.close();
        System.err.close();
    }

    /**
     * This is the global variable that is used to kill the consumer framework. This gets passed to each of the consumers, and the consumers check this
     *  variable to decide whether to continue processing elements in the queue or stop.
     */
    public class StopSwitch {
        private boolean stop = false;

        public synchronized boolean isStop() {
            return stop;
        }

        public synchronized void setStop(boolean stop) {
            this.stop = stop;
        }
    }

    public static class TestStatInfrastructureUtils {
        @Test
        public void testIsStatQuery() {
            assertTrue("Happy case for testing whether a string is a consumer stat query.", isStatQuery("STAT foo"));
            assertFalse("Failure case for testing whether a string is a consumer stat query. Without space scenario.", isStatQuery("STATfoo"));
            assertFalse("Failure case for testing whether a string is a consumer stat query. Small case scenario.", isStatQuery("stat foo"));
        }

        @Test
        public void testGetConsumerName() {
            assertEquals("Malformed query case for getting consumer name from stats query.", MALFORMED_QUERY, getConsumerName("STATsomeConsumer"));
            assertEquals("Happy case for getting consumer name from stats query.", "someConsumer", getConsumerName("STAT someConsumer"));
        }
    }
}
