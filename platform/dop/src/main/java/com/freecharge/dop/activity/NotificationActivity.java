package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.comm.notification.GCMNotificationService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;

import flexjson.JSONDeserializer;

public class NotificationActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    MetricsClient metricsClient;

    @Autowired
    private GCMNotificationService gcmNotificationService;

    public static final String CONSUMER = "consumer";
    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    private void sendNotification(Map<String, String> postData) {
        gcmNotificationService.sendNotification(postData);
    }

    @Override
    public void consume(Object payLoad) {

        Map<String, String> notificationDataMap = new HashMap<String, String>();
        try {
            long startTime = System.currentTimeMillis();
            // get Thread name and put in stateMap
            String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
            logger.info("Notification sending starts for " + payLoad + " with threadName : " + threadName);

            notificationDataMap = jsonDeserializer.deserialize((String) payLoad);

            if (!notificationDataMap.containsKey(GCMNotificationService.USER_EMAIL)
                    || FCUtil.isEmpty(notificationDataMap.get(GCMNotificationService.USER_EMAIL))) {
                logger.warn("Notification not sent for blank email with url : "
                        + notificationDataMap.get(GCMNotificationService.URL) + " with threadName : " + threadName);
                return;
            }

            String emailId = String.valueOf(notificationDataMap.get(GCMNotificationService.USER_EMAIL));
            String url = String.valueOf(notificationDataMap.get(GCMNotificationService.URL));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sDate= sdf.format(new Date());

            stateMap.put(threadName, sDate + " Sending notification on email : " + emailId + " with url : " + url);
            sendNotification(notificationDataMap);
            stateMap.remove(threadName);

            logger.info("Notification sending finished for email : " + emailId + " with url : " + url + " with threadName : " + threadName);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("Notifications." + notificationDataMap.get(FCConstants.FC_CHANNEL),
                    CONSUMER + "." + notificationDataMap.get(GCMNotificationService.EVENT_NAME), endTime - startTime);

        } catch (Exception e) {
            logger.error("Notification Sending Failure for notificationDataMap : " + notificationDataMap, e);
            metricsClient.recordEvent("Notifications." + notificationDataMap.get(FCConstants.FC_CHANNEL),
                    FCConstants.FAIL, notificationDataMap.get(GCMNotificationService.EVENT_NAME));
        }
    }

}
