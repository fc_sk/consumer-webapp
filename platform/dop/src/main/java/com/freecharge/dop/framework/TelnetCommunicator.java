package com.freecharge.dop.framework;

public interface TelnetCommunicator {
    public String getResponse(String command);
}
