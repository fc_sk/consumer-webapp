package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.RechargeRetryStatusDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.PlanLevelRetryConfig;
import com.freecharge.app.domain.entity.RechargeRetryStatus;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.util.RechargeConstants;

import flexjson.JSONDeserializer;

public class RechargeRetryActivity extends DopConsumerBase {
    private Logger                                logger           = LoggingFactory.getLogger(getClass());

    @Autowired
    private RechargeInitiateService               rechargeInitiateService;

    @Autowired
    private TxnFulFilmentService                  txnFulFilmentService;

    @Autowired
    private OrderIdWriteDAO                       orderIdDao;

    @Autowired
    private InService                             inService;

    @Autowired
    private PaymentTransactionService             transactionService;

    @Autowired
    private RechargeStatusIdempotencyCache        idempotencyMap;

    @Autowired
    private RechargeRetryConfigReadThroughCache   rechargeRetryCachedService;

    @Autowired
    private OperatorCircleService                 operatorCircleService;

    @Autowired
    AggregatorFactory                             aggregatorFactory;

    @Autowired
    RechargeRetryStatusDao                        dao;

    @Autowired
    private BillPaymentService                    billPaymentService;

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    private void doRecharge(OrderId orderToWork) {
        logger.info("About to recharge for order ID : " + orderToWork.getOrderId());
        rechargeInitiateService.doSyncRecharge(orderToWork.getOrderId(), null);
        logger.info("Done with recharge for order ID : " + orderToWork.getOrderId());
        logger.info("Done with recharge activity for order ID: " + orderToWork.getOrderId());
    }

    @Override
    public void consume(Object payLoad) {
        logger.info("Processing recharge retry request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);

        logger.debug("Order id is:" + orderId);

        List<OrderId> orderIdList = orderIdDao.getByOrderId(orderId);

        logger.debug("Order id list is:" + orderIdList);

        if (orderIdList == null || orderIdList.isEmpty()) {
            logger.error("No order found for Order ID : [" + orderId + "]. Nothing to be done returning");
            return;
        }

        if (orderIdList.size() > 1) {
            logger.error("Something bad, found multiple orders for Order ID : [" + orderId + "]. Working the first one");
        }

        OrderId orderToWork = orderIdList.get(0);

        logger.info("Order id to work with:" + orderToWork);

        if(orderToWork.isMoreThanThreeHoursOld()){
            logger.info("Skipping Order id :" + orderId + " as it is more than three hours old.");
            // skipping as it might have been refunded
            return;
        }

        if (!rechargeInitiateService.shouldProceedWithRecharge(orderId)) {
            logger.error("Refund already done for order " + orderId + ". Skipping recharge.");
            return;
        }
        /**
         * Idempotency check.
         */
        InTransactionData inTransaction = inService.getInTransactionData(orderId);

        logger.debug("In transaction data:" + inTransaction);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sDate = sdf.format(new Date());
        try {
                
                InRechargeRetryMap inAggregatorRetryMap = (InRechargeRetryMap) rechargeRetryCachedService
                        .getRechargeRetryOrderId(RechargeConstants.AGGREGATOR_RETRY_CACHE_PREFIX + orderToWork.getOrderId());
                if (inAggregatorRetryMap != null) {
                    stateMap.put(threadName, sDate + " doing aggregator retry for orderId=" + orderToWork.getOrderId());
                    rechargeRetryCachedService.deletRechargeRetryOrderId(RechargeConstants.AGGREGATOR_RETRY_CACHE_PREFIX + orderToWork.getOrderId());
                    rechargeInitiateService.doSyncRecharge(orderToWork.getOrderId(), inAggregatorRetryMap);
                    return;
                } 
               
                InRechargeRetryMap inOperatorRetryMap = (InRechargeRetryMap) rechargeRetryCachedService
                        .getRechargeRetryOrderId("OperatorRetry-" + orderToWork.getOrderId());
                if (inOperatorRetryMap != null){
                    logger.info("Operator Retry for Order " + orderId);
                    stateMap.put(threadName, sDate + " doing operator retry for orderId=" + orderToWork.getOrderId());
                    rechargeRetryCachedService.deletRechargeRetryOrderId("OperatorRetry-" + orderToWork.getOrderId());
                    idempotencyMap.markDequeue(orderId, this.getQueueName());
                    doProcessOperatorRetry(orderToWork.getOrderId(), inOperatorRetryMap);
                    return;
                }
                
                PlanLevelRetryConfig planRetryConfig = (PlanLevelRetryConfig) rechargeRetryCachedService.getPlanLevelRetryData(orderId);
                if(planRetryConfig != null){
                	stateMap.put(threadName, sDate + " doing plan level retry for orderId=" + orderToWork.getOrderId());
                    rechargeInitiateService.doSyncRecharge(orderToWork.getOrderId(), null);
                    idempotencyMap.markDequeue(orderId, this.getQueueName());
                    return;
                }
                //regular status check retry for underprocess recharges
                logger.info("Scheduled Retry for Order " + orderId);
                stateMap.put(threadName, sDate + " doing scheduled retry for orderId=" + orderToWork.getOrderId());
                Object cachedRetryStatus = rechargeRetryCachedService.getRechargeRetryOrderId("Retrying-"
                        + orderToWork.getOrderId());
                if (inTransaction.isTransactionSuccessful()) {
    
                    logger.warn("Recharge status was successful for order id " + orderId);
    
                    rechargeRetryCachedService.deletRechargeRetryOrderId("Retrying-" + orderToWork.getOrderId());
                    RechargeRetryStatus rechargeRetryStatus = dao.getRechargeRetryStatus(orderId);
                    if (rechargeRetryStatus != null) {
                        updateRechargeretryStatus(rechargeRetryStatus, true);
                    }
                }
    
                Boolean retryStatus = statusCheckForOrderId(inTransaction);
    
                if ((!inTransaction.isTransactionSuccessful()
                        && cachedRetryStatus != null
                        && retryStatus
                        && !inTransaction.getInResponse().getAggrName()
                                .equalsIgnoreCase(RechargeConstants.AGGR_NAME_OXIGEN))) {
                    doRecharge(orderToWork);
                }
        } catch (Exception e) {
            logger.error("Caught exception on trying to do a recharge retry for order " + orderId,e);
        } finally {
            idempotencyMap.markDequeue(orderId, this.getQueueName());
            stateMap.remove(threadName);
            logger.info("Done with recharge retry activity for order ID: " + orderToWork.getOrderId());
        }
    }

    private void doProcessOperatorRetry(String orderId, InRechargeRetryMap inRechargeRetryMap) {
        // Check if Retry operator is postpaid or prepaid
        int productId = Integer.parseInt(inRechargeRetryMap.getRetryProductId());

        if (FCUtil.isRechargeProductType(FCConstants.productMasterMap.get(productId))) {
            logger.info("Doing prepaid retry recharge for order : " + orderId + " operator : "
                    + inRechargeRetryMap.getRetryOperatorName());
            rechargeInitiateService.doSyncRecharge(orderId, inRechargeRetryMap);
        } else if (productId == FCConstants.PRODUCT_ID_BILL_MOBILE) {
            logger.info("Doing postpaid retry recharge for order : " + orderId + " operator : "
                    + inRechargeRetryMap.getRetryOperatorName());
            billPaymentService.fulfillPostpaidRetry(orderId, inRechargeRetryMap);
        } else {
            logger.error("ProductId for Order " + orderId + " is " + productId);
        }
    }

    private Boolean statusCheckForOrderId(InTransactionData inTransaction) {
        Boolean retryStatus = false;
        try {
            AggregatorInterface aggregatorInterface = aggregatorFactory.getAggregator(inTransaction.getInResponse()
                    .getAggrName());

            InResponse inResponse = inTransaction.getInResponse();

            InRequest inRequest = inTransaction.getInRequest();

            RechargeRetryStatus rechargeRetryStatus = dao.getRechargeRetryStatus(inRequest.getAffiliateTransId());

            RechargeDo rechargeDo = createRechargeDO(inResponse);

            AggregatorResponseDo aggregatorResponseDo = aggregatorInterface
                    .getTransactionStatus(rechargeDo, inResponse);

            logger.info("status check api response String for order id " + inRequest.getAffiliateTransId()
                    + " for retry attempt " + inResponse.getRetryNumber() + " is " + aggregatorResponseDo.toString());

            if (aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(
                    RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE)
                    || aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(
                            RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE)) {

                updateInResponse(inResponse, aggregatorResponseDo);

                if (rechargeRetryStatus != null) {
                    updateRechargeretryStatus(rechargeRetryStatus, true);
                }
                return retryStatus = false;
            }

            if (aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(inResponse.getAggrRespCode())) {
                return retryStatus = true;
            }

            if (aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(
                    RechargeConstants.EURONET_TRANSACTION_NOT_FOUND_RESPONSE_CODE)) {
                return retryStatus = true;
            }
            updateInResponse(inResponse, aggregatorResponseDo);

            if (rechargeRetryStatus != null) {
                updateRechargeretryStatus(rechargeRetryStatus, true);
            }

        } catch (Exception exception) {
            logger.error("Exception raised for orderId " + inTransaction.getInRequest().getAffiliateTransId()
                    + "for retry count " + inTransaction.getInResponse().getRetryNumber(), exception);
        }
        return retryStatus;
    }

    private RechargeDo createRechargeDO(InResponse inResponse) {
        InRequest inRequest = inService.getInRequest(inResponse.getRequestId());

        OperatorMaster operatorMaster = operatorCircleService.getOperator(inRequest.getOperator());
        CircleMaster circleMaster = operatorCircleService.getCircle(inRequest.getCircle());

        RechargeDo rechargeDo = new RechargeDo();
        rechargeDo.setAffiliateid(inRequest.getAffiliateId());
        rechargeDo.setAffiliateTransId(inRequest.getAffiliateTransId());
        rechargeDo.setAmount(inRequest.getAmount());
        rechargeDo.setCircleOperatorMapping(operatorCircleService.getCircleOperatorMapping(
                operatorMaster.getOperatorMasterId(), circleMaster.getCircleMasterId()));
        rechargeDo.setMobileNo(inRequest.getSubscriberNumber());
        rechargeDo.setProductType(inRequest.getProductType());
        rechargeDo.setInRequestId(inRequest.getRequestId());
        rechargeDo.setTransactionDate(inRequest.getCreatedTime());
        rechargeDo.setOrderId(inRequest.getAffiliateTransId());
        return rechargeDo;
    }

    private void updateInResponse(InResponse inResponse, AggregatorResponseDo aggregatorResponseDo) {
        String aggregatorRespCode = null;
        String inResponseCode = null;

        List<InErrorcodeMap> inErrorCodeMapList = inService.getErrorCodeMap(aggregatorResponseDo.getAggrResponseCode(),
                inResponse.getAggrName());

        if (inErrorCodeMapList != null && inErrorCodeMapList.size() > 0) {
            inResponseCode = inErrorCodeMapList.get(0).getInRespCode();
            aggregatorRespCode = inErrorCodeMapList.get(0).getAggrRespCode();
        } else {
            aggregatorRespCode = aggregatorResponseDo.getAggrResponseCode();
            inResponseCode = aggregatorResponseDo.getAggrResponseCode();
        }
        inResponse.setInRespCode(inResponseCode);
        inResponse.setAggrRespCode(aggregatorRespCode);
        if (!FCUtil.isEmpty(aggregatorResponseDo.getAggrReferenceNo())) {
            inResponse.setAggrTransId(aggregatorResponseDo.getAggrReferenceNo());
        }
        if (!FCUtil.isEmpty(aggregatorResponseDo.getAggrOprRefernceNo())) {
            inResponse.setOprTransId(aggregatorResponseDo.getAggrOprRefernceNo());
        }
        inResponse = inService.update(inResponse);
    }

    private void updateRechargeretryStatus(RechargeRetryStatus rechargeRetryStatus, Boolean isRetryComplete) {
        rechargeRetryStatus.setRetryComplete(isRetryComplete);
        dao.updateRechargeRetryStatus(rechargeRetryStatus);
    }
}
