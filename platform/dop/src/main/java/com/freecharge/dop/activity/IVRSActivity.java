package com.freecharge.dop.activity;

import com.freecharge.common.comm.ivrs.IVRSService;
import com.freecharge.common.comm.ivrs.helper.IVRSFactory;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import flexjson.JSONDeserializer;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Used to make an outgoing IVR call to a phone number.
 */

public class IVRSActivity extends DopConsumerBase {

    private Logger logger = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private IVRSFactory ivrsFactory;

    @Override
    public void consume(Object payLoad) {
        Map<String, String> ivrDataMap;
        try {
            long startTime = System.currentTimeMillis();
            ivrDataMap = jsonDeserializer.deserialize((String) payLoad);
            String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
            String phoneNumber = ivrDataMap.get(FCConstants.PHONE_NUMBER);
            logger.info("IVRS Activity for " + payLoad + " with threadName : " + threadName);
            if(FCUtil.isEmpty(phoneNumber)) {
                logger.error("Trying to make IVR call without a phone number. Payload: " + payLoad);
                return;
            }
            String IVRSClient = ivrDataMap.get("IVRSClient");
            IVRSService ivrsService = ivrsFactory.getIVRSClient(IVRSClient);
            logger.info("Calling " + phoneNumber + " with threadName : " + threadName);
            ivrsService.makeCall(ivrDataMap);
            logger.info("Successfully called  " + phoneNumber + " with threadName : " + threadName);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("IVRS_CONSUMER", ivrDataMap.get(FCConstants.IVRSClient), endTime - startTime);
        } catch (Exception e) {
            logger.error("Error while making IVRS call." + payLoad, e);
        }
    }
}
