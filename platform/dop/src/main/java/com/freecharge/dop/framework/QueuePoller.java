package com.freecharge.dop.framework;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.dop.activity.DopConsumerBase;
import com.freecharge.dop.framework.exception.QueuePollerException;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.base.Preconditions;

import flexjson.JSONDeserializer;

public abstract class QueuePoller implements Runnable {
    protected Logger logger = LoggingFactory.getLogger(getClass());

    protected MetricsClient metricsClient;

    protected DopConsumerBase dopConsumer;

    private ConsumerFramework.StopSwitch stopSwitch;

    private ExecutorService executorService;
    
    /*
     * We use semaphores to block until 
     * a thread is available in the 
     * executor pool
     */
    private Semaphore workerPermit;
    
    protected FCProperties fcProperties;
    
    public void setFcProperties(FCProperties fcProperties) {
        this.fcProperties = fcProperties;
    }

    public final void setMetricsClient(MetricsClient metricsClient) {
        this.metricsClient = metricsClient;
    }
    
    public final void setWorkerPermit(Semaphore workerPermit) {
        this.workerPermit = workerPermit;
    }
    
    public final void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public final void setStopSwitch(ConsumerFramework.StopSwitch stopSwitch) {
        this.stopSwitch = stopSwitch;
    }

    public final DopConsumerBase getDopConsumer() {
        return dopConsumer;
    }

    public final void setDopConsumer(DopConsumerBase dopConsumer) {
        this.dopConsumer = dopConsumer;
    }
    @Override
    public void run() {
        Preconditions.checkState(dopConsumer.getNoOfConsumers() != 0, "No of consumers cannot be 0 in DOP consumer " + dopConsumer.getClass().getSimpleName());
        Preconditions.checkState(StringUtils.isNotEmpty(dopConsumer.getQueueName()), "Queue name cannot be empty in DOP consumer " + dopConsumer.getClass().getSimpleName());
        
        this.init();

        while (!stopSwitch.isStop()) { //Check for the global stop command
            long pollStartTime = System.currentTimeMillis();
            try {
                try {
                    workerPermit.acquire();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    logger.error(e);
                    continue;
                }

                final Object payLoad;
                
                try {
                    payLoad = pollForPayLoad();
                } catch (Exception e) {
                    workerPermit.release();
                    metricsClient.recordEvent("DOP.Poller", "Read." + dopConsumer.getQueueName(), "Error");
                    logAndSleep(e);
                    continue;
                }

                if (payLoad == null) {
                    workerPermit.release();
                    handleEmptyPayLoad();
                    continue;
                }

                executorService.submit(
                        new Thread() {
                            public void run() {
                                try {
                                    long startTime = System.currentTimeMillis();
                                    putMDC(payLoad);
                                    dopConsumer.consume(payLoad);
                                    long endTime = System.currentTimeMillis();
                                    metricsClient.recordLatency("Consumer", dopConsumer.getQueueName(), endTime - startTime);
                                    metricsClient.recordEvent("Consumer", dopConsumer.getQueueName(), "Execution");
                                } catch (Exception e) {
                                    metricsClient.recordEvent("Consumer", dopConsumer.getQueueName(), "ConsumeException");
                                    logger.error("Error while processing consumers", e);
                                } finally {
                                    workerPermit.release();
                                    MDC.remove(PaymentConstants.ORDER_ID_KEY);
                                }
                            }

                            private void putMDC(final Object payLoad) {
                                try {
                                    final JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
                                    Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);
                                    String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
                                    MDC.put(PaymentConstants.ORDER_ID_KEY, orderId);
                                } catch (Exception ex) {
                                    // dont do anything , this may be due to parse error when 
                                    // payload may not have order id
                                }
                            }
                        }
                );
                
                this.deQueue();
                metricsClient.recordEvent("Consumer.Poller", dopConsumer.getQueueName(), "Success");
            } catch (Exception e) {
                metricsClient.recordEvent("Consumer.Poller", dopConsumer.getQueueName(), "Exception");
            } finally {
                metricsClient.recordLatency("Consumer.Poller", dopConsumer.getQueueName(), System.currentTimeMillis() - pollStartTime);
            }
        }
    }

    private void logAndSleep(Exception e) {
        logger.error(e);
        logger.info("Waiting for a minute before attempting to poll");
        interruptibleSleep(1, TimeUnit.MINUTES);
    }

    protected void interruptibleSleep(long sleepDuration, TimeUnit timeUnit) {
        try {
            // TODO: Read this from config
            timeUnit.sleep(sleepDuration);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            logger.error(ie);
        }
    }
    
    protected abstract Object pollForPayLoad() throws QueuePollerException;
    protected abstract void deQueue();
    protected abstract void handleEmptyPayLoad();
    protected abstract void init();
    
    public void shutDown() {
        executorService.shutdown();
    }

    public void awaitShutdown(long timeOut, TimeUnit timeUnit) throws InterruptedException {
        executorService.awaitTermination(timeOut, timeUnit);
    }
}
