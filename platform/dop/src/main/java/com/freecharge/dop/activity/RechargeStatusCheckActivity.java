package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.QueueConstants;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

import flexjson.JSONDeserializer;

/**
 * This activity checks the recharge status for a given order ID and initiates
 * fulfillment if it gets terminal status (i.e., success or failure). This
 * essentially is a think wrapper around {@link RechargeSchedulerService} in
 * order to parallelize status checks. This also helps in following, but not
 * limited to, cases.
 * 
 * <ul>
 * <li>In certain cases you may want to initiate status check as soon as you get
 * under process from aggregator. You just enqueue that order ID for status
 * check and you are done. This way the recharge consumer thread won't be
 * delayed with status check call and is also isolated from it's failures/timeouts etc.,</li>
 * <li>You may want to reinitiate a status check for reversal cases. You just enqueue them into
 * this queue and you are done.</li>
 * </ul>
 * 
 * An important pre-condition is that recharge must have been initiated.
 * 
 * @author arun
 * 
 */
public class RechargeStatusCheckActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
    
    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private RechargeSchedulerService rechargeSchedulerService;
    
    @Autowired
    private FulfillmentScheduleService fulfillmentScheduleService;
    
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    @Override
    public void consume(Object payLoad) {
        logger.info("Processing status check request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String)payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
        String statusCheckTypeString = queueItem.get(QueueConstants.STATUS_CHECK_TYPE);

        StatusCheckType statusCheckType = StatusCheckType.Relaxed;
        try {
        	if(fulfillmentScheduleService.isInScheduledState(orderId)) {
        		logger.info("In schedule state so skipping status check");
        		return;
        	}
        }catch(Exception ex) {
    		logger.error("Exception checcking schedule state : "+ex);
        }
        if (StringUtils.isNotBlank(statusCheckTypeString)) {
            statusCheckType = StatusCheckType.valueOf(statusCheckTypeString);
        }
        
        InTransactionData inTransaction = inService.getLatestInTransactionData(orderId);
        
        if (inTransaction == null || inTransaction.getInRequest() == null || inTransaction.getInResponse() == null) {
            logger.error("No recharge transaction data found for [" + orderId + "]. Not initiating status check");
            cleanUp(payLoad, threadName, orderId);
            return;
        }
        
        InResponse initialResponse = inTransaction.getInResponse();
        
        if (!RechargeConstants.IN_UNDER_PROCESS_CODE.equals(initialResponse.getInRespCode())) {
            logger.error("Recharge is NOT in under-process state not going ahead. Response code : " + initialResponse.getInRespCode());
            cleanUp(payLoad, threadName, orderId);
            return;
        }
        
        if (RechargeConstants.AGGR_NAME_SUVIDHAA.equals(initialResponse.getAggrName())) {
            logger.info("Aggregator is suvidhaa; temporarily disabled");
            cleanUp(payLoad, threadName, orderId);
            return;
        }

        String sDate= sdf.format(new Date());
        
        stateMap.put(threadName, sDate + " doing status check for orderId [" + orderId + "]");

        logger.info("About to initiate status check for [" + orderId + "]");
        rechargeSchedulerService.checkStatusAndUpdate(orderId, statusCheckType, initialResponse,false);
        logger.info("Done with status check for [" + orderId + "]");
        
        cleanUp(payLoad, threadName, orderId);
    }

    private void cleanUp(Object payLoad, String threadName, String orderId) {
        idempotencyMap.markDequeue(orderId, this.getQueueName());
        logger.info("Done with processing status check request for " + payLoad);
        stateMap.remove(threadName);
    }

}
