package com.freecharge.dop.activity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.autorefund.ForceRefundService;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

import flexjson.JSONDeserializer;

public class UnderProcessUpdateActivity extends DopConsumerBase {
    private Logger                                logger           = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private UserTransactionHistoryService         userTransactionHistoryService;

    @Autowired
    private PaymentTransactionService             paymentTransactionService;

    @Autowired
    private InService                             inService;

    @Autowired
    KestrelWrapper                                kestrelWrapper;

    @Autowired
    BillPaymentService                            billPaymentService;

    @Autowired
    private OrderIdSlaveDAO                       orderIdDAO;

    @Autowired
    PgReadDAO                                     pgReadDAO;

    @Autowired
    PaymentPlanService                            paymentPlanService;

    @Autowired
    PricingService                                pricingService;

    @Autowired
    PaymentRefund                                 paymentRefund;
    
    @Autowired
    private ForceRefundService                    forceRefundService;
    
    @Autowired
    private RechargeSchedulerService				rechargeSchedulerService;
    
  
    @Override
    public void consume(Object payLoad) {
        logger.info("Processing UP request for " + payLoad);

        Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
        UserTransactionHistory uth = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);

        List<PaymentTransaction> successPaymentTxn = paymentTransactionService
                .getSuccessfulPaymentTransactionsByOrderId(orderId);
        if (successPaymentTxn == null || successPaymentTxn.isEmpty()) {
            logger.error("No Successful payment for orderId " + orderId + ". Updating UTH status to payment failure.");
            userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            return;
        }
        if (forceRefundService.forceRefundUnderProcess(uth)) {
            return;
        }
        boolean isRefundable = FCUtil.isRefundableUserTxnForPaySuccess(uth.getTransactionStatus());
        logger.info("Transaction status for orderId " + orderId + " is " + uth.getTransactionStatus());
        
        if (isRefundable) {
            logger.info("Enqueueing for refund of " + orderId);
            kestrelWrapper.enqueueForFailureFulfilment(orderId);
            return;
        } else if (!uth.getTransactionStatus().equals("08") && !uth.getTransactionStatus().equals("-1")) {
            logger.info("Transaction status for order " + orderId + " is " + uth.getTransactionStatus());
            try {
                paymentRefund.isRefundDone(successPaymentTxn);
            } catch (Exception e) {
                logger.error("Exception on checking refund for " + orderId + " with txn status " + uth.getTransactionStatus(), e);
            }
            return;
        }

        try {
            paymentRefund.isRefundDone(successPaymentTxn);
        } catch (Exception e) {
            logger.error("Exception on checking refund for " + orderId, e);
            return;
        }

        String productType = uth.getProductType();

        if (FCUtil.isRechargeProductType(productType)) {
            logger.info("Product type is recharge for order " + orderId);
            handleRechargeCase(orderId, productType);
        } else if (FCUtil.isBillPostpaidType(productType)) {
            logger.info("Product type is postpaid for order " + orderId);
            handlePostpaidCase(orderId, productType);
        } else if (FCUtil.isUtilityPaymentType(productType)) {
            logger.info("Product type is utility for order " + orderId);
            handleUtilityCase(orderId, productType);
        } else {
            logger.error("Unknown product type for order " + orderId + " productType " + productType);
        }
    }

    
    private void handleUtilityCase(String orderId, String productType) {
        logger.info("Enqueue for status check as product type is utility " + orderId + ", prductType " + productType);
        String billId = orderIdDAO.getBillIdForOrderId(orderId, productType);
        String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
        logger.info("BillId : " + billId + ", lookupId " + lookupId + ", OrderId " + orderId);
        kestrelWrapper.enqueueForBillPayStatusCheck(billId, lookupId, StatusCheckType.Relaxed);
    }

    private void handleRechargeCase(String orderId, String productType) {
        InResponse inResponse = inService.getInResponseByOrderID(orderId);

        //prepaid no recharge attempt
        if (inResponse == null) {
            logger.info("No Recharge Attempt for Order " + orderId);
            kestrelWrapper.enqueueForFailureFulfilment(orderId);
            return;
        }
        
        //prepaid Startup issue status check 
        if ((inResponse.getInRespCode() != null && inResponse.getInRespCode().equals("-1") && inResponse.isMoreThanThreeHoursOld())) {
        	try {
            	logger.info("##### Startup Refund Fix Block ##### for Order " + orderId);
                rechargeSchedulerService.checkStatusAndUpdate(orderId, StatusCheckType.Relaxed, inResponse,true);
            	logger.info("##### Startup Refund Fix Block executed ##### for Order " + orderId);
        	}catch(Exception ex) {
            	logger.error("##### Exception in Refund Fix Block ##### " + ex);
        	}
            return;
        }
        
        //prepaid status check 
        String agResp = inResponse.getInRespCode();
        if ((agResp != null && (agResp.equals("08")))) {
            logger.info("Skipping under process prepaid OrderId - " + orderId);
            return;
        }

        //postpaid status check (product retry)
        BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
        if (billPaymentStatus != null && billPaymentStatus.getSuccess() == null) {
            logger.info("Skipping under process postpaid OrderId - " + orderId);
            return;
        }

        //prepaid success/fail , fulfillment
        logger.info("Final status for orderId " + orderId + ". Enqueue for fulfilment.");
        enqueueFulfilment(orderId);
    }

    private void handlePostpaidCase(String orderId, String productType) {
        BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
        
        //postpaid no recharge attempt
        if (billPaymentStatus == null) {
            logger.info("No postpaid payment attempt for Order " + orderId);
            kestrelWrapper.enqueueForFailureFulfilment(orderId);
            return;
        }
        
        //postpaid pending recharge 
        Boolean status = billPaymentStatus.getSuccess();
        if (status == null) {
            logger.info("Skipping under process postpaid OrderId - " + orderId);
            return;
        }
        
        //prepaid pending recharge - product retry 
        InResponse inResponse = inService.getInResponseByOrderID(orderId);
        if (inResponse != null && inResponse.getInRespCode() != null && inResponse.getInRespCode().equals("08")) {
            logger.info("Skipping under process prepaid OrderId - " + orderId);
            return;
        }
        
        //postpaid success/fail fulfillment 
        logger.info("Final status for postpaid orderId " + orderId + ". Enqueue for fulfilment.");
        billPaymentService.doFullfilment(orderId);

    }

    private void enqueueFulfilment(String orderId) {
        Map<String, String> fulfillmentRequest = new HashMap<>();
        fulfillmentRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
        fulfillmentRequest.put(PaymentConstants.SHOULD_SEND_SMS_KEY, Boolean.TRUE.toString());
        kestrelWrapper.enqueueFulfillment(fulfillmentRequest);
    }
}
