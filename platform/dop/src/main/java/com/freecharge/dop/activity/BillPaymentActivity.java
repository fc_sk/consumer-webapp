package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.TimeBarrierService;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.FulfillmentIdempotencyCache;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;

import flexjson.JSONDeserializer;

/**
 * This is a base class for fulfilling *all* kinds of bill payments. It does
 * the book keeping and then hands over the task of fulfillment to appropriate
 * sub-class activities aas defined in kestrelConsumerContext.
 * @author arun
 *
 */
public abstract class BillPaymentActivity extends DopConsumerBase {
	protected Logger logger = LoggingFactory.getLogger(getClass());

	private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
	private Map<String, String> stateMap = new ConcurrentHashMap<String, String>();

	@Autowired
    private OrderIdWriteDAO orderIdDao;

	@Autowired
    protected BillPaymentService billPaymentService;
	
	@Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
	
	@Autowired
    TimeBarrierService timeBarrierService;
	
	@Autowired
    MetricsClient metricsClient;
	
	@Autowired
	RechargeInitiateService rechargeInitiateService;
	 
    @Autowired
    RechargeRetryConfigReadThroughCache rechargeCache;
    
    @Autowired
  	private FulfillmentIdempotencyCache fulfillmentIdempotencyCache;


	@Override
	public String getState() {
		String ret = "";
        int count = 0;
        for (Map.Entry<String, String> entry : stateMap.entrySet()) {
            ret = ret + entry.getKey() + ": " + entry.getValue() + "\n";
            count = count+1;
        }
        return ret + "Count=" + count;
	}

	@Override
	public void consume(Object payLoad) {
        logger.info("Processing " + getActivityName() + " bill payment request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String)payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);

        logger.info("Order id is:" + orderId);
        
      //We need to check for a lob3 flag here so that we can set it after fulfillment
        //We will set it to the cache value
        Boolean lob3Flag = Boolean.valueOf(queueItem.get(PaymentConstants.NODAL_FLAG));
        logger.info("Order id is:" + orderId);
        logger.info("LOB3 flag value is:" + lob3Flag);
        
        rechargeCache.setLOBKey(orderId, lob3Flag);

        List<OrderId> orderIdList = orderIdDao.getByOrderId(orderId);

        logger.debug("Order id list is:" + orderIdList);

        if(orderIdList == null || orderIdList.isEmpty()) {
            logger.error("No order found for Order ID : [" + orderId + "]. Nothing to be done returning");
            return;
        }

        if(orderIdList.size() > 1) {
            logger.error("Something bad, found multiple orders for Order ID : [" + orderId + "]. Working the first one");
        }

        OrderId orderToWork = orderIdList.get(0);

        logger.debug("Order id to work with:" + orderToWork);
        
        
        if(orderToWork.isMoreThanThreeHoursOld()){
            // skipping as it might have been refunded
            logger.info("Skipping Order id :" + orderId + " as it is more than three hours old.");
            return;
        }
        
        if(fulfillmentIdempotencyCache.isDuplicateRequest(orderId)) {
        	logger.info("Duplicate Enqueue From Payment for OrderId for BillPayment "+orderToWork);
        	return;
        }
        

        if (!timeBarrierService.putTimeBarrier(orderId, 5000)) {
            logger.info("Recharge already in progress not going ahead for order ID: [" + orderId + "]");
            metricsClient.recordEvent("BillPayment", "Consumer", "RechargeInProgress");
            return;
        }
        
        if (!rechargeInitiateService.shouldProceedWithRecharge(orderId)) {
            logger.error("Refund already done for order " + orderId + ". Skipping recharge.");
            return;
        }
        
        try {
        	registerToStateMap(threadName, orderToWork);
        	this.fulfillOrderId(orderId);
        } finally {
            idempotencyMap.markDequeue(orderId, this.getQueueName());
            stateMap.remove(threadName);
        }
	}

    private void registerToStateMap(String threadName, OrderId orderToWork) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sDate= sdf.format(new Date());
        stateMap.put(threadName, "[" + sDate + "] doing " + getActivityName() + "for orderId [" + orderToWork.getOrderId() + "]");
    }
    
    protected abstract void fulfillOrderId(String orderId);
    protected abstract String getActivityName();
}
