package com.freecharge.dop.activity;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.dop.activity.DopConsumerBase;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.services.UserTransactionHistoryService;

import flexjson.JSONDeserializer;

/**
 * This activity checks the Bill Pay status for a given bill ID and initiates
 * fulfillment if it gets terminal status (i.e., success or failure).
 * 
 * @author Korath Paul
 */
public class BillPayStatusCheckActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());
    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
    
    @Autowired
    private BillPaymentService billPaymentService;
    
    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Override
    public void consume(Object payLoad) {
        logger.info("Processing billpay status check request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);

        String billId = queueItem.get(PaymentConstants.BILL_ID_KEY);
        String lookupId = queueItem.get(PaymentConstants.LOOKUP_ID_KEY);
        String orderID = orderIdReadDAO.getOrderIdForLookUpId(lookupId);
        try {
            
            UserTransactionHistory txnHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderID);
            if (txnHistory.getTransactionStatus() != null && !txnHistory.getTransactionStatus().equals("08")) {
                logger.info("Transaction is already successful for orderId: " + orderID);
                return;
            }
            FulfillBillResponse fulfillBillResponse = billPayServiceRemote.executeBillPayStatusCheck(billId);
            if (fulfillBillResponse.getResponseStatus() != FulfillBillResponse.Status.UnderProcess) {
                logger.info("Status changed for billId " + billId + " from Under process to " + fulfillBillResponse.getResponseStatus().name());
                billPaymentService.doFullfilment(orderID);
            }
            logger.info("Done with status check for bill payment for " + payLoad);
        } catch (Exception exception) {
            logger.error("Exception raised for order id" +  orderID, exception);
        } finally {
            cleanUp(payLoad, threadName, billId);
        }
        logger.info("Done with status check for [" + orderID + "]");
    }

    private void cleanUp(Object payLoad, String threadName, String orderId) {
        idempotencyMap.markDequeue(orderId, this.getQueueName());
        logger.info("Done with processing status check request for " + payLoad);
        stateMap.remove(threadName);
    }
    

}
