package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.TimeBarrierService;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.FulfillmentIdempotencyCache;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;

import flexjson.JSONDeserializer;

public class RechargeActivity extends DopConsumerBase {
	private Logger logger = LoggingFactory.getLogger(getClass());
	
    @Autowired
    private RechargeInitiateService rechargeInitiateService;
    
    @Autowired
    private TxnFulFilmentService txnFulFilmentService;
    
    @Autowired
    private OrderIdWriteDAO orderIdDao;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private PaymentTransactionService transactionService;

    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
    
    @Autowired
    TimeBarrierService timeBarrierService;
    
    @Autowired
    MetricsClient metricsClient;
    
    @Autowired
	private FulfillmentIdempotencyCache fulfillmentIdempotencyCache;

    
    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
    
	private void doRecharge(OrderId orderToWork) {
		rechargeInitiateService.doSyncRecharge(orderToWork.getOrderId(), null);
	}

    private void recreateSession(OrderId orderToWork) {
        String sessionId = orderToWork.getSessionId();
        String orderIdKey = orderToWork.getOrderId() + "_txn_id";
        String orderId = orderToWork.getOrderId();
        
		logger.info("Recreating session with ID [" + sessionId + "]");

		Map<String, Object> sessionMap = new ConcurrentHashMap<>();
		updateSessionWithPaymentTransactionId(sessionMap, orderIdKey, orderId);
            
        FreechargeSession freechargeSession = new FreechargeSession(sessionId, "", "");
        freechargeSession.setSessionData(sessionMap);
		
		FreechargeContext fcContext = new FreechargeContext();
		fcContext.setFreechargeSession(freechargeSession);
		fcContext.setLookupId(orderToWork.getLookupId());
		fcContext.setOrderId(orderToWork.getOrderId());
		fcContext.setSessionId(orderToWork.getSessionId());
		
		FreechargeContextDirectory.set(fcContext);
    }

    /**
     * Go over payment transactions for the order ID and 
     * update the session with transaction ID that succeeded.
     *  
     */
    private void updateSessionWithPaymentTransactionId(Map<String, Object> sessionMap, String orderIdKey, String orderId) {
        List<PaymentTransaction> transactionList = transactionService.getPaymentTransactionsByOrderId(orderId);
        
        Integer paymentTransactionId = null;
        boolean foundPG = false;
        
        for (PaymentTransaction paymentTransaction : transactionList) {
            if (paymentTransaction.getIsSuccessful() && !PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentTransaction.getPaymentGateway())) {
                /*
                 * Dude has paid using non-wallet payment method
                 */
                foundPG = true;
                paymentTransactionId = paymentTransaction.getPaymentTxnId();
            }
        }
        
        /*
         * Dude has paid using wallet 
         */
        if (!foundPG) {
            for (PaymentTransaction paymentTransaction : transactionList) {
                if (paymentTransaction.getIsSuccessful() && PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentTransaction.getPaymentGateway())) {
                    paymentTransactionId = paymentTransaction.getPaymentTxnId();
                }
            }
        }
        
        logger.info("Adding [key: " + orderIdKey + ", value: " + paymentTransactionId + "] to session map in memory");
        sessionMap.put(orderIdKey, paymentTransactionId);
    }

    @Override
    public void consume(Object payLoad) {
        logger.info("Processing recharge request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String)payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);
        logger.debug("Order id is:" + orderId);
        
        List<OrderId> orderIdList = orderIdDao.getByOrderId(orderId);

        logger.debug("Order id list is:" + orderIdList);

        if(orderIdList == null || orderIdList.isEmpty()) {
            logger.error("No order found for Order ID : [" + orderId + "]. Nothing to be done returning");
            return;
        }

        if(orderIdList.size() > 1) {
            logger.error("Something bad, found multiple orders for Order ID : [" + orderId + "]. Working the first one");
        }

        OrderId orderToWork = orderIdList.get(0);

        logger.debug("Order id to work with:" + orderToWork);
        
        if(orderToWork.isMoreThanThreeHoursOld()){ //If recharge happened more than 3 hours back as we are retryinng in every 15 mins(need to check)
            // skipping as it might have been refunded
            logger.info("Skipping Order id :" + orderId + " as it is more than three hours old.");
            return;
        }
        if(fulfillmentIdempotencyCache.isDuplicateRequest(orderId)) {
        	logger.info("Duplicate Enqueue From Payment for OrderId "+orderToWork);
        	return;
        }
        
        if (!timeBarrierService.putTimeBarrier(orderId, 5000)) { //If recharge is under process
            logger.info("Recharge already in progress not going ahead for order ID: [" + orderId + "]");
            metricsClient.recordEvent("Recharge", "Consumer", "RechargeInProgress");
            return;
        }

        if (!rechargeInitiateService.shouldProceedWithRecharge(orderId)) {
            logger.error("Refund already done for order " + orderId + ". Skipping recharge.");
            return;
        }
        
        /**
         * Idempotency check.
         */
        if(inService.validateForRechargeRetry(orderId)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sDate= sdf.format(new Date());
            stateMap.put(threadName, sDate + " doing recharge for orderId=" + orderToWork.getOrderId());
            logger.info("Putting in stateMap: " + threadName + "->" + sDate + " doing recharge for orderId=" + orderToWork.getOrderId());
            logger.info("No record for IN request for Order ID [" + orderId + "] so going ahead with recharge");
            doRecharge(orderToWork);
            logger.info("Removing from stateMap: " + threadName);
            stateMap.remove(threadName);
        } else {
            logger.info("IN transaction for Order ID: [" + orderId + "] is in some unknown state not going ahead with recharge");
        }
        
        idempotencyMap.markDequeue(orderId, this.getQueueName());
        logger.info("Done with recharge activity for order ID: " + orderToWork.getOrderId());
    }
}
