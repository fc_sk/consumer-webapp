package com.freecharge.dop.framework.exception;

public class QueuePollerException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public QueuePollerException(Exception cause) {
        super(cause);
    }

}
