package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.infrastructure.billpay.beans.BillpayUnknownTransaction;
import com.freecharge.notification.email.SendEmailRequest;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.web.webdo.BillpayUnknownStatus;
import com.freecharge.order.service.dao.BillpayUnknownStatusDao;

import flexjson.JSONDeserializer;

/**
 * A wrapper activity that consumes order IDs for Processing of Unknown status from billpay
 * 
 * @author Anant 
 *
 */
public class UnknownStatusProcessActivity extends DopConsumerBase {
	private Logger logger = LoggingFactory.getLogger(getClass());

	private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();
	private Map<String, String> stateMap = new ConcurrentHashMap<String, String>();

	@Autowired
	private FulfillmentService fulfillmentService;
	
	@Autowired
	private BillpayUnknownStatusDao billpayUnknownStatusDao;

	@Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;

	@Autowired
	private UserTransactionHistoryDAO userTransactionHistoryDAO;

	@Override
	public void consume(Object payLoad) {
		logger.info("Processing UnknownStatusProcessActivity retry request for " + payLoad);
		String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
		
		try {
			ObjectMapper mapper = new ObjectMapper();		
			BillpayUnknownTransaction unknownTxn = mapper.readValue(String.valueOf(payLoad).getBytes(), BillpayUnknownTransaction.class);
			String billId = unknownTxn.getBillId();
			BillpayUnknownStatus entry = new BillpayUnknownStatus();
			entry.setAmount(unknownTxn.getAmount());
			entry.setAuthenticator(unknownTxn.getCustomerAccountNumber());
			entry.setBillId(unknownTxn.getBillId());
			entry.setIsActive(true);
			entry.setOrderId(unknownTxn.getOrderId());
			entry.setStatus("unknown");
			entry.setTxnDate(new SimpleDateFormat("MM-dd-yyyy").format(new Date()));
			entry.setAgTxnId(unknownTxn.getAgTxnId());
			billpayUnknownStatusDao.insert(entry);
			if(unknownTxn.getOrderId()!=null && !"".equals(unknownTxn.getOrderId())) {
			   userTransactionHistoryDAO.updateStatus(unknownTxn.getOrderId(), PaymentConstants.FULFILLMENT_UNKNOWN_STATUS);
			}else {
				logger.error("orderId missing");
			}
		} catch (Exception ex) {
			logger.error("Exception : " + ex);
		}
	}

}