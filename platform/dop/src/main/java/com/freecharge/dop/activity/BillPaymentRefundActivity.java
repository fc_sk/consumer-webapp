package com.freecharge.dop.activity;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.RechargeStatus;
import com.freecharge.recharge.RechargeStatusService;

import flexjson.JSONDeserializer;

public class BillPaymentRefundActivity extends DopConsumerBase {
    Logger logger = Logger.getLogger(BillPaymentRefundActivity.class);

    private JSONDeserializer<Map<String, Object>> refundDataDeserializer = new JSONDeserializer<Map<String, Object>>();

    @Autowired
    private PaymentRefund paymentRefund;
    
    @Autowired
    private RechargeStatusService reshargeStatusService;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    private void doRefund(String mtxnId, String refundType) {
        logger.info("About to Bill payment Refund for order ID : " + mtxnId);
        Set<String> orderSet = new HashSet<String>();
        orderSet.add(mtxnId);
        
        try {
            paymentRefund.doBillPaymentRefund(orderSet, refundType);
        } catch(Exception e) {
            logger.error("Something bad happened while refunding for mtxn ID: [" + mtxnId + "]. Refund Type: [" + refundType + "]", e);
        }
        logger.info("Done with refund activity for order ID: " + mtxnId);
    }

    @Override
    public void consume(Object refundInitiateDatum) {
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();   
        logger.info("Processing refund request for " + refundInitiateDatum);

        Map<String, Object> refundDataMap = refundDataDeserializer.deserialize((String) refundInitiateDatum);

        String mtxnId = (String) refundDataMap.get(PaymentConstants.ORDER_ID_KEY);
        String refundType = (String) refundDataMap.get(PaymentConstants.REFUND_TYPE_KEY);
        PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(mtxnId);
        
        RechargeStatus rechargeStatus = reshargeStatusService.getRechargeStatus(paymentTransaction.getOrderId());
        if(rechargeStatus.equals(RechargeStatus.RECHARGE_FAILED)){
        stateMap.put(threadName, "doing refund for mTxnId=" + mtxnId + ", refundType=" + refundType);
        doRefund(mtxnId, refundType);
        stateMap.remove(threadName);
        }
    }
}
