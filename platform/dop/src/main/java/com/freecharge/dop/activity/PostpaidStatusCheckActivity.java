package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.batch.job.BillPaymentStatusCheckJob;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.common.BillPaymentFactory;
import com.freecharge.freebill.common.BillPaymentStatusCheck;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.statuscheckservice.BillPaymentStatusCheckResponse;
import com.freecharge.freebill.statuscheckservice.IBIllPaymentStatusCheckInterface;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;

import flexjson.JSONDeserializer;

/**
 * This activity checks the Postpaid status for a given order ID and initiates
 * fulfillment if it gets terminal status (i.e., success or failure). This
 * essentially is a think wrapper around {@link BillPaymentStatusCheckJob} in
 * order to parallelize status checks. This also helps in following, but not
 * limited to, cases.
 * 
 * @author Vipin Goyal
 */
public class PostpaidStatusCheckActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;

    @Autowired
    private BillPaymentFactory billPaymentFactory;

    @Autowired
    private BillWriteDAO billWriteDAO;

    @Autowired
    private BillReadDAO billReadDAO;

    @Autowired
    private BillPaymentStatusCheck billPaymentStatusCheck;

    @Autowired
    private FulfillmentService fulfillmentService;
    
    @Autowired
    private FulfillmentScheduleService fulfillmentScheduleService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void consume(Object payLoad) {
        logger.info("Processing postpaid status check request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();

        Map<String, String> queueItem = jsonDeserializer.deserialize((String) payLoad);

        String orderId = queueItem.get(PaymentConstants.ORDER_ID_KEY);

        try {
        	try {
            	if(fulfillmentScheduleService.isInScheduledState(orderId)) {
            		logger.info("In schedule state so skipping status check");
            		return;
            	}
            }catch(Exception ex) {
        		logger.error("Exception checcking schedule state : "+ex);
            }
            BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderId);

            String billPaymentGateway = billPaymentStatus.getBillPaymentGateway();

            if (billPaymentStatus.getSuccess() != null && billPaymentStatus.getSuccess()) {
                logger.info("Not going ahead with status check for orderID : " + orderId + " as is already successful");
                cleanUp(payLoad, threadName, orderId);
                return;
            }
            IBIllPaymentStatusCheckInterface statusCheckInterface = billPaymentFactory
                    .getBillPaymentStatusCheckGateway(billPaymentGateway);

            if (statusCheckInterface != null) {
                logger.info("About to initiate status check for [" + orderId + "]");
                BillPaymentStatusCheckResponse billPaymentStatusCheckResponse = statusCheckInterface
                        .submitBillPaymentStatusCheckRequest(orderId, billPaymentStatus);
                logger.info("submitted BillPaymentStatusCheck for status check request order ID " + orderId);
                if (billPaymentStatusCheckResponse != null) {
                    updateDataBaseForBillPaymentStatusResponse(billPaymentStatusCheckResponse, billPaymentStatus);
                } else {
                    logger.info("billPaymentStatusCheckResponse is null for order id " + orderId);
                }
            } else {
                logger.info("IBIllPaymentStatusCheckInterface factory is not working for order id " + orderId);
            }

        } catch (Exception exception) {
            logger.error("Exception raised for order id" +  orderId, exception);
            cleanUp(payLoad, threadName, orderId);
            return;
        }
        String sDate = sdf.format(new Date());

        stateMap.put(threadName, sDate + " doing status check for orderId [" + orderId + "]");

        logger.info("Done with status check for [" + orderId + "]");

        cleanUp(payLoad, threadName, orderId);
    }

    private void updateDataBaseForBillPaymentStatusResponse(
            BillPaymentStatusCheckResponse billPaymentStatusCheckResponse, BillPaymentStatus billPaymentStatus) {
        billPaymentStatusCheck.updateStatusAndIntitiateFulfilment(billPaymentStatusCheckResponse,
                billPaymentStatus);
    }

    private void cleanUp(Object payLoad, String threadName, String orderId) {
        idempotencyMap.markDequeue(orderId, this.getQueueName());
        logger.info("Done with processing status check request for " + payLoad);
        stateMap.remove(threadName);
    }
}
