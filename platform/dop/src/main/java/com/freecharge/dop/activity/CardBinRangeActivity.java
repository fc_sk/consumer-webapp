package com.freecharge.dop.activity;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.payment.dao.CardBinRangeDAO;
import com.freecharge.payment.services.binrange.BinList;
import com.freecharge.payment.services.binrange.BinListService;
import com.freecharge.payment.services.binrange.CardBinRange;
import com.freecharge.payment.services.binrange.CardBinRangeService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;

import flexjson.JSONDeserializer;

/**
 * Given a cardBin this activity checks binlist.net api and updated the card bin
 * range table
 * 
 * @author korath
 * 
 */
public class CardBinRangeActivity extends DopConsumerBase {
    Logger                                        logger       = Logger.getLogger(CardBinRangeActivity.class);

    private JSONDeserializer<Map<String, String>> deserializer = new JSONDeserializer<Map<String, String>>();

    @Autowired
    private MetricsClient                         metricsClient;

    @Autowired
    private BinListService                        binListService;

    @Autowired
    private CardBinRangeService                   cardBinRangeService;
    
    @Autowired
    private CardBinRangeDAO binRangeDao;

    @Override
    public void consume(Object payLoad) {
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
        logger.info("Processing cardBinRange insert for " + payLoad);
        String binHead = null;
        String binTail = null;
        Boolean isPaySuccessful = null;
        try {
            Map<String, String> cardBinDataMap = deserializer.deserialize((String) payLoad);
            binHead = cardBinDataMap.get(PaymentConstants.CARD_BIN_HEAD).toString();
            stateMap.put(threadName, "Processing cardBinRange insert for bin " + binHead);
            binTail = cardBinDataMap.get(PaymentConstants.CARD_BIN_TAIL).toString();
            isPaySuccessful = Boolean.valueOf(cardBinDataMap.get(PaymentConstants.PAYMENT_STATUS).toString());
            if (isPaySuccessful) {
                metricsClient.recordEvent("Payment", "Binlist.cardBinHead." + binHead, "Success");
                List<CardBinRange> cardBinRangeList = binRangeDao.getCardBinRanges(binHead, binTail);
                
                if (cardBinRangeList.isEmpty()) {
                    metricsClient.recordEvent("Payment", "Binlist.NoDataInDB.", binHead);
                    logger.info("Checking the binlist API for bin head " + binHead);
                    
                    BinList binListData = binListService.getBinListData(binHead);
                    
                    if (binListData == null) {
                        metricsClient.recordEvent("Payment", "Binlist.NoDataInAPI.", binHead);
                    } else {
                        cardBinRangeService.updateCardBinRange(binHead, binTail, binListData, cardBinRangeList);
                    }
                } else {
                    metricsClient.recordEvent("Payment", "Binlist.DataInDB.", binHead);
                }
            } else {
                metricsClient.recordEvent("Payment", "Binlist.cardBinHead." + binHead, "Failure");
            }
        } catch (Exception e) {
            logger.error("Exception trying to update card_bin_range table.", e);
            metricsClient.recordEvent("Payment", "Binlist", "Exception");
        }
        stateMap.remove(threadName);
        logger.info("Done with cardBinRange insert for bin " + binHead + " and paymentStatus " + isPaySuccessful);
    }
}
