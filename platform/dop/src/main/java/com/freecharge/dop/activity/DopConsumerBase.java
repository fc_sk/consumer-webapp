package com.freecharge.dop.activity;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.freecharge.dop.framework.DopConsumer;

/**
 * Base class for all the consumers. Exposes methods using which
 * one can configure various consumer specific parameters.
 * @author arun
 *
 */
public abstract class DopConsumerBase implements DopConsumer {
    private long consumerWaitPeriod;
    private int noOfConsumers;
    private int noOfPollers;
    private int noOfSQSPollers;
    private String queueName;
    protected Map<String, String> stateMap = new ConcurrentHashMap<String, String>();

    @Override
    public void setConsumerWaitPeriod(long consumerWaitPeriod) {
        this.consumerWaitPeriod = consumerWaitPeriod;
    }

    @Override
    public long getConsumerWaitPeriod() {
        return this.consumerWaitPeriod;
    }

    @Override
    public void setNoOfConsumers(int noOfConsumers) {
        this.noOfConsumers = noOfConsumers;
    }
    
    @Override
    public void setNoOfPollers(int noOfPollers) {
        this.noOfPollers = noOfPollers;
    }

    @Override
    public int getNoOfConsumers() {
        return noOfConsumers;
    }

    @Override
    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @Override
    public String getQueueName() {
        return queueName;
    }
    
    @Override
    public int getNoOfPollers() {
        return this.noOfPollers;
    }
    
    @Override
    public int getNoOfSQSPollers() {
        return this.noOfSQSPollers;
    }
    
    @Override
    public void setNoOfSQSPollers(int noOfSQSPollers) {
        this.noOfSQSPollers = noOfSQSPollers;
    }
    
    public String getState() {
        String ret = "";
        int count = 0;
        for (Map.Entry<String, String> entry : stateMap.entrySet()) {
            ret = ret + entry.getKey() + ": " + entry.getValue() + "\n";
            count = count+1;
        }
        return ret + "Count=" + count;
    }
    
    public int getPendingObjectsCount() {
        return stateMap.size();
    }
}
