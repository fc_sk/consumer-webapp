package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundFulfillment;
import com.freecharge.platform.metrics.MetricsClient;

import flexjson.JSONDeserializer;

/**
 *  This class consumes FREEFUND_QUEUE item and takes care of freefund fulfillment as per Transaction status (Success / Failure)  
 */
public class FreefundActivity extends DopConsumerBase {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    MetricsClient metricsClient;

    @Autowired
    private FreefundFulfillment freefundFulfillment;

    public final static String EXCEPTION = "Exception";
    public static final String FREEFUND_CONSUMER = "FreefundConsumer";
    
    private JSONDeserializer<Map<String, String>> jsonDeserializer = new JSONDeserializer<Map<String, String>>();

    @Override
    public void consume(Object payLoad) {

        Map<String, String> dataMap = new HashMap<String, String>();
        String inResponse = null;
        try {
            long startTime = System.currentTimeMillis();
            // get Thread name and put in stateMap
            String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
            logger.info("Freefund processing starts for " + payLoad + " with threadName : " + threadName);

            dataMap = jsonDeserializer.deserialize((String) payLoad);

            if(!validateInput(dataMap)) {
                logger.error("Invalid input. Freefund processing not initialized for orderId : "
                        + FreefundFulfillment.ORDER_ID + ", lookupId : " 
                        + FreefundFulfillment.LOOKUP_ID + ", inResponse : " 
                        + FreefundFulfillment.IN_RESPONSE);
                return;
            }

            String orderId = String.valueOf(dataMap.get(FreefundFulfillment.ORDER_ID));
            String lookupId = String.valueOf(dataMap.get(FreefundFulfillment.LOOKUP_ID));
            inResponse = String.valueOf(dataMap.get(FreefundFulfillment.IN_RESPONSE));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String sDate= sdf.format(new Date());

            stateMap.put(threadName, sDate + " freefund processing for orderId : " + orderId + ", lookupId : " 
                    + lookupId + ", inResponse : " + inResponse);
            freefundFulfillment.proessFreefund(orderId, lookupId, inResponse);
            stateMap.remove(threadName);

            logger.info("Freefund processing finished for orderId : " + orderId + ", lookupId : " + lookupId 
                    + ", inResponse : " + inResponse + " with threadName : " + threadName);

            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency(FREEFUND_CONSUMER, FreefundFulfillment.IN_RESPONSE + "." 
                    + inResponse, endTime - startTime);

        } catch (Exception e) {
            logger.error("Freefund processing failure for dataMap : " + dataMap, e);
            metricsClient.recordEvent(FREEFUND_CONSUMER, FreefundFulfillment.IN_RESPONSE + "." + inResponse, 
                    EXCEPTION);
        }
    }

    private boolean validateInput(Map<String, String> dataMap){
        if (!dataMap.containsKey(FreefundFulfillment.ORDER_ID)
                || !dataMap.containsKey(FreefundFulfillment.LOOKUP_ID)
                || !dataMap.containsKey(FreefundFulfillment.IN_RESPONSE) 
                || FCUtil.isEmpty(dataMap.get(FreefundFulfillment.ORDER_ID))
                || FCUtil.isEmpty(dataMap.get(FreefundFulfillment.LOOKUP_ID))) {            
            return false;
        }        
        return true;
    }

}
