package com.freecharge.dop.activity;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.comm.sms.DelayedUnderProcessSmsService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.DelayedUnderProcessSmsDetails;

public class DelayUnderProcessSmsActivity extends DopConsumerBase{
	private static Logger logger = LoggingFactory.getLogger(DelayUnderProcessSmsActivity.class);
	
	@Autowired
	private DelayedUnderProcessSmsService delayedUnderProcessSmsService;
	
	@Override
    public void consume(Object payLoad) {
		try {
			ObjectMapper mapper = new ObjectMapper();		
			DelayedUnderProcessSmsDetails delayedSmsObject = mapper.readValue(payLoad.toString(), DelayedUnderProcessSmsDetails.class);
			delayedUnderProcessSmsService.sendUpSms(delayedSmsObject);
		} catch (Exception e) {
			logger.error("Exception caught", e);
		}
	}

}
