package com.freecharge.dop.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis.utils.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.RewardRechargeDAO;
import com.freecharge.app.domain.entity.RewardRecharge;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.TimeBarrierService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.services.IRechargeRetryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.gson.Gson;

public class RewardRechargeActivity extends DopConsumerBase {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private KestrelWrapper kestrelWrapper;

    @Autowired
    private RechargeInitiateService rechargeInitiateService;

    @Autowired
    private InService inService;

    @Autowired
    private TimeBarrierService timeBarrierService;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private IRechargeRetryService rechargeRetryService;

    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;

    @Override
    public void consume(Object payLoad) {

        logger.info("Processing reward recharge request for " + payLoad);
        String threadName = Thread.currentThread().getName() + "-" + Thread.currentThread().getId();
        RechargeRequestWebDo rechargeRequestWebDo = getRechargeRequest(payLoad);
        if (rechargeRequestWebDo == null || StringUtils.isEmpty(rechargeRequestWebDo.getOrderId())) {
            logger.info("Unable to parse reward recharge request for " + payLoad);
            return;
        }
        String orderId = rechargeRequestWebDo.getOrderId();
        boolean retryCreated = false;
        boolean isTransSuccessful = false;
        try {
            if (!timeBarrierService.putTimeBarrier(orderId, 5000)) {
                logger.info("Reward Recharge already in progress not going ahead for order ID: [" + orderId + "]");
                metricsClient.recordEvent("RewardRecharge", "Consumer", "RechargeInProgress");
                return;
            }
            RewardRecharge rewardRecharge = rechargeInitiateService.getRewardRecharge(orderId);
            if (rewardRecharge == null) {
                rewardRecharge = new RewardRecharge();
                rewardRecharge.setAmount(rechargeRequestWebDo.getAmount());
                rewardRecharge.setCircleId(rechargeRequestWebDo.getCircleId());
                rewardRecharge.setOperatorId(rechargeRequestWebDo.getOperatorId());
                rewardRecharge.setServiceNumber(rechargeRequestWebDo.getMobileNumber());
                rewardRecharge.setSourceOrderId(rechargeRequestWebDo.getTxnReferenceNumber());
                rewardRecharge.setOrderId(orderId);
                rewardRecharge.setUserId(rechargeRequestWebDo.getUserId());
                rechargeInitiateService.createRewardRecharge(rewardRecharge);
            }

            logger.debug("Processing Reward Recharge OrderId :" + orderId);

            /**
             * Idempotency check.
             */
            if (inService.validateForRechargeRetry(orderId)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String sDate = sdf.format(new Date());
                stateMap.put(threadName, sDate + " doing recharge for orderId=" + orderId);
                logger.info(
                        "Putting in stateMap: " + threadName + "->" + sDate + " doing recharge for orderId=" + orderId);
                logger.info("No record for IN request for Order ID [" + orderId + "] so going ahead with recharge");
                InResponse inResponse = rechargeInitiateService.doRewardRecharge(rewardRecharge);
                if (inResponse == null || RechargeConstants.IN_UNDER_PROCESS_CODE.equals(inResponse.getInRespCode())) {
                    logger.info("In response is null for Order Id [" + orderId + "]");
                    return;
                }
                isTransSuccessful = isTransactionSuccessful(inResponse);
                if (!isTransSuccessful) {
                    retryCreated = rechargeRetryService.checkAndScheduleForRetry(orderId,null);
                    logger.info("Recharge retry created for orderId -" + orderId + " : " + retryCreated);
                }
                if (isTransSuccessful || !retryCreated) {
                    enqueueRewardResponse(rechargeRequestWebDo, orderId, isTransSuccessful);
                }
                logger.info("Removing from stateMap: " + threadName);
                stateMap.remove(threadName);
            } else {
                logger.info("IN transaction for Order ID: [" + orderId
                        + "] is in some unknown state not going ahead with recharge");
            }
            logger.info("Done with recharge activity for order ID: " + orderId);
        } catch (Exception ex) {
            logger.error("Error while processing orderId: " + orderId, ex);
            enqueueRewardResponse(rechargeRequestWebDo, orderId, isTransSuccessful);
        } finally {
            idempotencyMap.markDequeue(orderId, this.getQueueName());
        }
    }

    private void enqueueRewardResponse(RechargeRequestWebDo rechargeRequestWebDo, String orderId,
            boolean isTransSuccessful) {
        logger.info("Enqueuing response for order ID: " + orderId + " with status:" + isTransSuccessful);
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put("success", Boolean.toString(isTransSuccessful));
        responseMap.put("txnReferenceNumber", rechargeRequestWebDo.getTxnReferenceNumber());
        responseMap.put("requestId", orderId);
        kestrelWrapper.enqueueRewardRechargeResponse(responseMap);
    }

    private RechargeRequestWebDo getRechargeRequest(Object payLoad) {
        RechargeRequestWebDo rechargeRequestWebDo = null;
        try {
            rechargeRequestWebDo = new Gson().fromJson((String) payLoad, RechargeRequestWebDo.class);
        } catch (Exception ex) {
            logger.error("Error parsing recharge request :" + payLoad);
        }
        return rechargeRequestWebDo;
    }

    private boolean isTransactionSuccessful(InResponse inResponse) {
        return inResponse != null && inResponse.isSuccessful();
    }
}
