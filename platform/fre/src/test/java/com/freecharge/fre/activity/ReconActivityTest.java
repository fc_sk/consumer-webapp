package com.freecharge.fre.activity;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.fre.activity.ReconActivity;

import flexjson.JSONSerializer;

public class ReconActivityTest {

    JSONSerializer serializer = new JSONSerializer();

    private static interface PayURecordHeaders {
        final String MTXN_ID = "Transaction ID";
        final String TRANSACTION_AMOUNT = "Amount";
        final String TRANSACTION_STATUS = "Status";
        final String TRANSACTION_TYPE = "Requested Action";
    }

    private final CSVFormat payUFormat = CSVFormat.EXCEL.toBuilder().withHeader(
            "Request ID", "Date", "PayU ID", "Customer Name",
            PayURecordHeaders.TRANSACTION_AMOUNT, PayURecordHeaders.TRANSACTION_TYPE,
            PayURecordHeaders.TRANSACTION_STATUS, "Bank Name",
            "Requested Amount", "Discount", "Merchant Name", PayURecordHeaders.MTXN_ID,
            "Payment Gateway", "Bank Reference No", "Last name", "Customer Email",
            "Customer IP Address", "Card Number", "Field 2", "Payment Type", "Net Amount",
            "Service Fee", "Service Tax", "Merchant UTR", "Token", "Settlement",
            "Offer Key", "PG MID", "International/Domestic", "Offer Failure Reason", "Offer Type"
            ).build();


    @Test
    public void test() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        ReconActivity reconActivity = ctx.getBean(ReconActivity.class);
        
        Map<String, String> reconMap = new HashMap<String, String>();
        reconMap.put("reportFileName", "prod/uploads/PayuSample.csv");
        
        reconActivity.consume(serializer.serialize(reconMap));
    }

}
