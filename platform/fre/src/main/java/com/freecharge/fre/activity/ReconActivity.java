package com.freecharge.fre.activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonServiceException.ErrorType;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.dop.activity.DopConsumerBase;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletTransaction;
import flexjson.JSONDeserializer;

public class ReconActivity extends DopConsumerBase {

    @Autowired
    PaymentTransactionService paymentTransactionService;

    @Autowired
    WalletService walletService;

    @Autowired
    FCProperties fcProperties;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AmazonS3 s3Client;

    private Logger logger = LoggingFactory.getLogger(getClass());

    JSONDeserializer<Map<String, String>> deserializer = new JSONDeserializer<Map<String, String>>();

    private enum ReconcileStatus {
        Success, Failure, NA;
    }

    private enum WalletCreditStatus {
        Success, Failure, NA;
    }

    private final CSVFormat iciciFormat = CSVFormat.EXCEL
            .toBuilder()
            .withHeader("Serial No.", "Transaction Ref. No.", "Merchant Id", "Terminal Id", "Channel Type", ICICIRecordHeaders.TRANSACTION_AMOUNT, "Surcharge Indicator", "Surcharge Amount",
                    "Transaction Alfa Currency Code", "Transaction Prev Txn Ref No.", "Transaction DTTM", ICICIRecordHeaders.TRANSACTION_TYPE, ICICIRecordHeaders.TRANSACTION_STATUS, "Sub Reason",
                    "Transaction RRN No", "Transaction Auth ID", "Transaction CVResp Code", ICICIRecordHeaders.MTXN_ID, "Transaction Invoice No.", "Merchant Txn. Id",
                    "Transaction Merchant Batch No.", "Transaction Bill Address line1", "Transaction Bill Address line2", "Transaction Bill Address line3", "Transaction Bill Address City",
                    "Transaction Bill Address state", "Transaction Bill Address Zip Code", "Transaction Ship Address line1", "Transaction Ship Address line2", "Transaction Ship Address line3",
                    "Transaction Ship Address City", "Transaction Ship Address state", "Transaction Ship Address Zip Code", "Transaction Ext Data 1", "Transaction Ext Data 2",
                    "Transaction Ext Data 3", "Transaction Ext Data 4", "Transaction Ext Data 5", "Transaction Chage Back Status", "Transaction Chage Back Reason Desc",
                    "Transaction Chage Back Initi Date", "Transaction Chage Back Reason Code", "Transaction Chage Back Amount", "Transaction Chage Back ARN", "Transaction Card No.",
                    "Transaction Card Type", "Transaction VBV CAVV", "Transaction VBV Status", "Transaction VBV XID", "Transaction VBV ECI", "Transaction VBV IP Address").build();

    private final CSVFormat payUFormat = CSVFormat.EXCEL
            .toBuilder()
            .withHeader("Request ID", "Date", "PayU ID", "Customer Name", PayURecordHeaders.TRANSACTION_AMOUNT, PayURecordHeaders.TRANSACTION_TYPE, PayURecordHeaders.TRANSACTION_STATUS, "Bank Name",
                    "Requested Amount", "Discount", "Merchant Name", PayURecordHeaders.MTXN_ID, "Payment Gateway", "Bank Reference No", "Last name", "Customer Email", "Customer IP Address",
                    "Card Number", "Field 2", "Payment Type", "Net Amount", "Service Fee", "Service Tax", "Merchant UTR", "Token", "Settlement", "Offer Key", "PG MID", "International/Domestic",
                    "Offer Failure Reason", "Offer Type").build();

    private final CSVFormat billDeskFormat = CSVFormat.EXCEL
            .toBuilder()
            .withHeader("Biller Id", "Bank Id", "Bank Ref. No.", "PGI Ref. No.", billDeskRecordHeaders.MTXN_ID, "Ref. 2", "Ref. 3", "Ref. 4", "Ref. 5", "Ref. 6", "Ref. 7", "Ref. 8", "Filler",
                    "Date of Txn", "Settlement Date", billDeskRecordHeaders.TRANSACTION_AMOUNT, "Charges (Rs.Ps)", "S Tax (Rs Ps)", "Net Amount(Rs.Ps)").build();

    private final int PAYU_COLUMN_SIZE = 31;

    private final int BILLDESK_COLUMN_SIZE = 19;

    private final int ICICI_COLUMN_SIZE = 51;

    private final int S3_GET_MAX_TRIES = 3;

    private static interface ReconRequestParameter {
        final String RECON_TYPE = "reconType";
        final String PG_NAME = "pgName";
        final String REPORT_FILE = "reportFileName";
        final String COMPLETION_MAIL_RECIPIENT = "completionEmailRecipient";
    }

    private static interface ICICIRecordHeaders {
        final String MTXN_ID = "Transaction Mrt Order Ref No.";
        final String TRANSACTION_AMOUNT = "Transaction Amount";
        final String TRANSACTION_STATUS = "Transaction Status";
        final String TRANSACTION_TYPE = "Transaction Type";
    }

    private static interface PayURecordHeaders {
        final String MTXN_ID = "Transaction ID";
        final String TRANSACTION_AMOUNT = "Amount";
        final String TRANSACTION_STATUS = "Status";
        final String TRANSACTION_TYPE = "Requested Action";
    }

    private static interface billDeskRecordHeaders {
        final String MTXN_ID = "Ref. 1";
        final String TRANSACTION_AMOUNT = "Gross Amount(Rs.Ps)";
        final String TRANSACTION_TYPE = "Requested Action";
    }

    @Override
    public void consume(Object reconRequestObject) {
        logger.info("Processing reconciliation request for " + reconRequestObject);

        Map<String, String> reconRequest = deserializer.deserialize((String) reconRequestObject);

        String reconType = reconRequest.get(ReconRequestParameter.RECON_TYPE);
        String reportFile = reconRequest.get(ReconRequestParameter.REPORT_FILE);
        String pgName = reconRequest.get(ReconRequestParameter.PG_NAME);
        String completionEmailRecipient = reconRequest.get(ReconRequestParameter.COMPLETION_MAIL_RECIPIENT);
        String outfileName = "/tmp/reconciled.csv";
        try {
            final String reconBucket = fcProperties.getReconBucket();

            S3Object s3Object = s3GetWithRetries(reportFile, s3Client, reconBucket);
            if ("BillDesk".equals(pgName)) {
                CSVParser csvp = new CSVParser(new BufferedReader(new InputStreamReader(s3Object.getObjectContent())), billDeskFormat);
                billDeskReconActivity(csvp, outfileName);
            } else {
                if ("payU".equals(pgName)) {
                    CSVParser csvp = new CSVParser(new BufferedReader(new InputStreamReader(s3Object.getObjectContent())), payUFormat);
                    payUReconActivity(csvp, outfileName);
                } else {
                    if ("icici".equals(pgName)) {
                        CSVParser csvp = new CSVParser(new BufferedReader(new InputStreamReader(s3Object.getObjectContent())), iciciFormat);
                        iciciReconActivity(csvp, outfileName);
                    } else {
                        throw new IllegalStateException();
                    }
                }
            }
            String[] inputFilePath = reportFile.split("/");
            String outFileSuffix = inputFilePath[inputFilePath.length - 1].split("\\.")[0];
            String s3OutfileKey = fcProperties.getReconReportsDirectory() + "/" + outFileSuffix + "-result.csv";

            s3Client.putObject(reconBucket, s3OutfileKey, new File(outfileName));

            EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
            emailBusinessDO.setSubject("File successfully uploaded to : " + s3OutfileKey);
            emailBusinessDO.setTo("vipin.goyal@freecharge.com");
            emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.RECON_SUCCESS_FROM));
            emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.RECON_SUCCESS_REPLYTO));
            emailBusinessDO.setCc(fcProperties.getProperty(FCConstants.RECON_SUCCESS_CC));
            emailBusinessDO.setBcc(fcProperties.getProperty(FCConstants.RECON_SUCCESS_BCC));
            emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());

            String s3Key = URLEncoder.encode(s3OutfileKey, "UTF-8");

            String downLoadLink = fcProperties.getAppRootUrl() + "/admin/payment/getreconfile.htm?" + "keyName=" + s3Key;

            emailBusinessDO.setContent("File successfully uploaded to : " + downLoadLink);

            emailService.sendNonVelocityEmail(emailBusinessDO);

            logger.info("Wrote the report to file: [" + outfileName + "]");
        } catch (IOException e) {
            logger.error("Something went wrong while reading from S3", e);
        } catch (Exception e) {
            logger.error("Something went wrong in Recon activity", e);
        }

        logger.info("Done with processing reconciliation request for " + reconRequestObject);
    }

    private S3Object s3GetWithRetries(String reportFile, AmazonS3 s3, final String reconBucket) {
        AmazonClientException clientException = null;

        for (int tryNumber = 0; tryNumber < S3_GET_MAX_TRIES; tryNumber++) {
            try {
                S3Object s3Object = s3Client.getObject(reconBucket, reportFile);
                return s3Object;
            } catch (AmazonServiceException ase) {
                if (ase.getErrorType() == ErrorType.Service) {
                    clientException = ase;
                    logger.info("Got amazon service exception trying again. Trial number: " + tryNumber, ase);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException("Got interrupted while retrying for s3get", e);
                    }
                } else {
                    throw new RuntimeException("Got client side exception whlie calling S3, aborting.", ase);
                }
            }
        }

        throw new RuntimeException("Unable to fetch from s3 after retries", clientException);
    }

    private void writeToPayUFile(FileWriter outFile, CSVRecord csvRecord, ReconcileStatus reconcileStatus, WalletCreditStatus walletCreditStatus) throws IOException {
        for (int currentPos = 0; currentPos < PAYU_COLUMN_SIZE; currentPos++) {
            outFile.write("\"" + csvRecord.get(currentPos) + "\",");
        }

        outFile.write("\"" + reconcileStatus.toString() + "\",\"" + walletCreditStatus.toString() + "\"");

        outFile.write("\n");
    }

    private void writeToBillDeskFile(FileWriter outFile, CSVRecord csvRecord, ReconcileStatus reconcileStatus, WalletCreditStatus walletCreditStatus) throws IOException {
        for (int currentPos = 0; currentPos < BILLDESK_COLUMN_SIZE; currentPos++) {
            outFile.write("\"" + csvRecord.get(currentPos) + "\",");
        }

        outFile.write("\"" + reconcileStatus.toString() + "\",\"" + walletCreditStatus.toString() + "\"");

        outFile.write("\n");
    }

    private WalletCreditStatus getWalletReconcileStatus(String merchantTransactionId) {
        List<WalletTransaction> walletTransactions = walletService.findTransaction(merchantTransactionId, TransactionType.DEPOSIT);

        WalletCreditStatus walletCreditStatus = WalletCreditStatus.Failure;

        if (walletTransactions != null) {
            for (WalletTransaction walletTransaction : walletTransactions) {
                if (walletTransaction.getOrderId().equals(merchantTransactionId)) {
                    walletCreditStatus = WalletCreditStatus.Success;
                    break;
                }
            }
        }
        return walletCreditStatus;
    }

    private ReconcileStatus getPaymentReconcileStatus(CSVRecord csvRecord, List<PaymentTransaction> transactionList, String paymentGatewayCode) {
        ReconcileStatus reconcileStatus = ReconcileStatus.Failure;

        try {
            if (transactionList != null) {
                for (PaymentTransaction paymentTransaction : transactionList) {
                    String paymentGateway = paymentTransaction.getPaymentGateway();

                    if (PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE.equals(paymentGatewayCode)) {
                        if (isPayUEqual(paymentTransaction, csvRecord)) {
                            reconcileStatus = ReconcileStatus.Success;
                            break;
                        }
                    }
                    if (PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE.equals(paymentGatewayCode)) {
                        if (isBillDeskEqual(paymentTransaction, csvRecord)) {
                            reconcileStatus = ReconcileStatus.Success;
                            break;
                        }
                    }
                    if (PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE.equals(paymentGatewayCode)) {
                        if (isIciciEqual(paymentTransaction, csvRecord)) {
                            reconcileStatus = ReconcileStatus.Success;
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Exception occured in getPaymentReconcileStatus method");
            return ReconcileStatus.NA;
        }
        return reconcileStatus;
    }

    private boolean isPayUEqual(PaymentTransaction paymentTransaction, CSVRecord csvRecord) throws Exception {
        String transactionAmount = csvRecord.get(PayURecordHeaders.TRANSACTION_AMOUNT);
        String transactionStatus = csvRecord.get(PayURecordHeaders.TRANSACTION_STATUS);
        try {
            Double amount = paymentTransaction.getAmount();
            Boolean isSuccessful = paymentTransaction.getIsSuccessful();

            if (Double.parseDouble(transactionAmount) != amount.doubleValue()) {
                return false;
            }

            if (isSuccessful) {
                if (!"SUCCESS".equalsIgnoreCase(transactionStatus)) {
                    return false;
                }
            } else {
                if ("SUCCESS".equalsIgnoreCase(transactionStatus)) {
                    return false;
                }
            }
        } catch (Exception e) {
            logger.info("Exception occured in isPayUEqual method");
            throw new Exception();
        }

        return true;
    }

    private boolean isBillDeskEqual(PaymentTransaction paymentTransaction, CSVRecord csvRecord) throws Exception {
        String transactionAmount = csvRecord.get(billDeskRecordHeaders.TRANSACTION_AMOUNT);
        try {
            Double amount = paymentTransaction.getAmount();
            Boolean isSuccessful = paymentTransaction.getIsSuccessful();

            if (Double.parseDouble(transactionAmount) != amount.doubleValue()) {
                return false;
            }

            if (isSuccessful) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.info("Exception occured in isBillDeskEqual method");
            throw new Exception();
        }

    }

    private boolean isIciciEqual(PaymentTransaction paymentTransaction, CSVRecord csvRecord) throws Exception {
        String transactionAmount = csvRecord.get(ICICIRecordHeaders.TRANSACTION_AMOUNT);
        try {
            Double amount = paymentTransaction.getAmount();
            Boolean isSuccessful = paymentTransaction.getIsSuccessful();

            if (Double.parseDouble(transactionAmount) != amount.doubleValue()) {
                return false;
            }

            if (isSuccessful) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.info("Exception occured in isIciciEqual method");
            throw new Exception();
        }

    }

    private void billDeskReconActivity(CSVParser csvp, String outfileName) {
        String merchantTransactionId = null;
        CSVRecord csvRecord = null;
        ReconcileStatus reconcileStatus = null;
        WalletCreditStatus walletCreditStatus = null;
        try {
            FileWriter outFile = new FileWriter(outfileName);
            Iterator<CSVRecord> csvIterator = csvp.iterator();
            try {
                while (csvIterator.hasNext()) {
                    csvRecord = csvIterator.next();
                    System.out.println(csvRecord.toString());
                    String outLine = csvRecord.toString().split("\\[")[1].split("\\]")[0];

                    if (csvRecord.get(billDeskRecordHeaders.MTXN_ID).equals(billDeskRecordHeaders.MTXN_ID)) {
                        outFile.write(outLine + "," + "Reconcile Status" + "," + "Wallet Credit Status");
                        outFile.write("\n");
                        continue;
                    }

                    logger.info("CSV Record:[ " + csvRecord + "]");

                    merchantTransactionId = csvRecord.get(billDeskRecordHeaders.MTXN_ID);

                    List<PaymentTransaction> transactionList = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantTransactionId);

                    reconcileStatus = getPaymentReconcileStatus(csvRecord, transactionList, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
                    walletCreditStatus = getWalletReconcileStatus(merchantTransactionId);
                    writeToBillDeskFile(outFile, csvRecord, reconcileStatus, walletCreditStatus);

                    logger.info("Fetching MtxnID:[" + merchantTransactionId + "]");
                }
            } catch (Exception e) {
                logger.error("exception occured during build desk recon process for mtxn Id : [ " + merchantTransactionId + "]");
                writeToPayUFile(outFile, csvRecord, ReconcileStatus.NA, walletCreditStatus);
            }

            outFile.write("\n");
            outFile.flush();
            outFile.close();
        } catch (IOException e) {
            logger.info("handling IO exception for File Object");
        }
    }

    private void payUReconActivity(CSVParser csvp, String outfileName) {
        String merchantTransactionId = null;
        CSVRecord csvRecord = null;
        ReconcileStatus reconcileStatus = null;
        WalletCreditStatus walletCreditStatus = null;
        try {
            FileWriter outFile = new FileWriter(outfileName);

            Iterator<CSVRecord> csvIterator = csvp.iterator();
            try {
                while (csvIterator.hasNext()) {
                    csvRecord = csvIterator.next();

                    String outLine = csvRecord.toString().split("\\[")[1].split("\\]")[0];

                    if (csvRecord.get(PayURecordHeaders.MTXN_ID).equals(PayURecordHeaders.MTXN_ID)) {
                        outFile.write(outLine + "," + "Reconcile Status" + "," + "Wallet Credit Status");
                        outFile.write("\n");
                        continue;
                    }

                    logger.info("CSV Record:[ " + csvRecord + "]");

                    merchantTransactionId = csvRecord.get(PayURecordHeaders.MTXN_ID);
                    String transactionType = csvRecord.get(PayURecordHeaders.TRANSACTION_TYPE);

                    if ("capture".equals(transactionType)) {
                        List<PaymentTransaction> transactionList = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantTransactionId);
                        reconcileStatus = getPaymentReconcileStatus(csvRecord, transactionList, PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
                        walletCreditStatus = getWalletReconcileStatus(merchantTransactionId);
                        writeToPayUFile(outFile, csvRecord, reconcileStatus, walletCreditStatus);
                    } else {
                        writeToPayUFile(outFile, csvRecord, ReconcileStatus.NA, WalletCreditStatus.NA);
                    }

                    logger.info("Fetching MtxnID:[" + merchantTransactionId + "]");
                }
            } catch (Exception e) {
                logger.error("exception occured during PayU recon process for mtxn Id : [ " + merchantTransactionId + "]");
                writeToPayUFile(outFile, csvRecord, ReconcileStatus.NA, walletCreditStatus);
            }

            outFile.write("\n");
            outFile.flush();
            outFile.close();
        } catch (IOException e) {
            logger.info("handling IO exception for File Object");
        }
    }

    private void iciciReconActivity(CSVParser csvp, String outfileName) {
        String merchantTransactionId = null;
        CSVRecord csvRecord = null;
        ReconcileStatus reconcileStatus = null;
        WalletCreditStatus walletCreditStatus = null;
        try {
            FileWriter outFile = new FileWriter(outfileName);
            Iterator<CSVRecord> csvIterator = csvp.iterator();
            while (csvIterator.hasNext()) {

                try {
                    csvRecord = csvIterator.next();

                    String outLine = csvRecord.toString().split("\\[")[1].split("\\]")[0];

                    if (csvRecord.get(ICICIRecordHeaders.MTXN_ID).equals(ICICIRecordHeaders.MTXN_ID)) {
                        outFile.write(outLine + "," + "Reconcile Status" + "," + "Wallet Credit Status");
                        outFile.write("\n");
                        continue;
                    }

                    logger.info("CSV Record:[ " + csvRecord + "]");

                    merchantTransactionId = csvRecord.get(ICICIRecordHeaders.MTXN_ID);
                    List<PaymentTransaction> transactionList = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantTransactionId);
                    reconcileStatus = getPaymentReconcileStatus(csvRecord, transactionList, PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);
                    walletCreditStatus = getWalletReconcileStatus(merchantTransactionId);

                    writeToPayUFile(outFile, csvRecord, reconcileStatus, walletCreditStatus);

                    logger.info("Fetching MtxnID:[" + merchantTransactionId + "]");
                } catch (Exception e) {
                    logger.error("exception occured during icici recon for mtxn Id : [ " + merchantTransactionId + "]");
                    writeToPayUFile(outFile, csvRecord, ReconcileStatus.NA, walletCreditStatus);
                }
            }
            outFile.write("\n");
            outFile.flush();
            outFile.close();
        } catch (IOException e) {
            logger.info("handling IO exception for File Object");
        }
    }
 
    @Override
    public String getState() {
        return "Some state for ReconActivity";
    }
}