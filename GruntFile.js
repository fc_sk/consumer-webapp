module.exports = function(grunt) {

  /* @swapnil files required for mobile build including library files */
  var mobileBuildJSFiles = [
    'webapp/content/js/lib/jquery.2.1.0.js',
    // 'webapp/content/js/lib/require.js',
    'webapp/content/js/lib/almond.js',
    'webapp/content/js/lib/parsley.custom.validation.js',
    'webapp/content/js/lib/handlebars.runtime-v1.3.0.js',
    'webapp/content/templates/mobile/prepaidmobile.js',
    'webapp/content/js/m/dist/m.min.js',
    'webapp/content/templates/mobile/dist/all.templates.js'
  ];
  // Project configuration.
  grunt.initConfig({
    jshint: {
      "globals": { "$": false },
      ignore_warning: {
        options: {
          '-W015' : true,
          '-W099' : true,
          '-W014' : true  
        },
        src: ['webapp/content/js/mobile/**/*.js'],
      },
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
      },
      files: {
        src: ['webapp/content/js/mobile/**/*.js']
      }
    },

    uglify : {

      options: {
        mangle    : true,
        compress  : true
      },
      target: {
        files: {
          'webapp/content/js/mobile/mobile.recharge.js': ['webapp/content/js/mobile/mobile.recharge.js'],
          'webapp/content/js/mobile/mobile.cart.js': ['webapp/content/js/mobile/mobile.cart.js'],
          'webapp/content/js/mobile/mobile.custom.js': ['webapp/content/js/jquery-1.9.1.min.js','webapp/content/js/mobile/mobile.custom.js'],
          'webapp/content/js/mobile/mobile.payments.js': ['webapp/content/js/mobile/mobile.payments.js'],
          'webapp/content/js/mobile/mobile.coupons.js': ['webapp/content/js/mobile/mobile.coupons.js'],
          'webapp/content/js/mobile/mobile.address.js': ['webapp/content/js/mobile/mobile.address.js'],
          'webapp/content/js/mobile/mobile.account.js': ['webapp/content/js/mobile/mobile.account.js']
        }
      },
      // uglify for mobile
      mobile:{
          files: {
              'webapp/content/js/m/dist/m.all.min.js': mobileBuildJSFiles
          }
      }
    },

    cssmin : {
      combine: {
        files: {
          'webapp/content/css/m/main.min.css': ['webapp/content/css/m/normalize.css', 'webapp/content/css/m/main.css'],
          'webapp/content/css/pepsi/m-style.min.css': ['webapp/content/css/pepsi/m-style.css']
        }
      }
    },

    imagemin: {                          // Task
      dist: {                            // Target
        options: {                       // Target options
          optimizationLevel: 7
        },
        files: {                         // Dictionary of files
          'webapp/content/images/m/mobileNew_1.png': 'webapp/content/images/m/mobileNew.png' // 'destination': 'source'
        }
      }
    },
    /* @swapnil require js concatenation for mobile */
    requirejs: {
      compile: {
        options: {
          mainConfigFile : "webapp/content/js/m/m.js",
          baseUrl : "webapp/content/js/m",
          name: "m",
          out: "webapp/content/js/m/dist/m.min.js",
          removeCombined: true,
          findNestedDependencies: true
        }
      }
    },
    /* Handlebars template compilation */
    handlebars: {
      compile: {
        options: {
          namespace: "mobiletemplates"
        },
        files:{
        'webapp/content/templates/mobile/dist/all.grunt.templates.js' : ['webapp/content/templates/mobile/*.htm']
        }
      }
    },

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-handlebars');
  grunt.loadNpmTasks('grunt-contrib-concat');
  
  // Default task(s).
  grunt.registerTask('default', ['uglify','cssmin']);
  grunt.registerTask('templates',['handlebars']);
  grunt.registerTask('mobile',['requirejs','handlebars','uglify:mobile']);
};