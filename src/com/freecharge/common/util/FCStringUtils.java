package com.freecharge.common.util;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.mongodb.DBObject;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class FCStringUtils {

    public static boolean isBlank(String str)
    {
        int strLen;
        if(str == null || (strLen = str.length()) == 0)
            return true;
        for(int i = 0; i < strLen; i++)
            if(!Character.isWhitespace(str.charAt(i)))
                return false;

        return true;
    }
    
    public static boolean isNotBlank(String str)
    {
        return !isBlank(str);
    }

    public static List<String> getStringList(String propertyValue, String delimiter) {

    	StringTokenizer stringtokens = new StringTokenizer(propertyValue, delimiter);
    	List<String> stringList = new ArrayList<String>();

    	while(stringtokens.hasMoreTokens()) {
    		stringList.add(stringtokens.nextToken());
    	}
    	return stringList;
    }

    public static String join(List<String> terms, String delimiter){
        if (terms==null || terms.size()<1){
            return null;
        }
        String res = "";
        for (String t : terms){
            res+=delimiter+t;
        }
        return res.substring(delimiter.length());
    }

    public static JSONObject jsonToMap(String jsonString){
        JSONParser parser = new JSONParser();
        try {
            return (JSONObject) parser.parse(jsonString);
        }catch (ParseException pe) {
            logger.error(String.format("Parsing Error - json string: %s", jsonString), pe);
            return null;
        }
    }
    
    public boolean checkForDuplicateInDBObjectList(String string,List<DBObject> list,String keyName){
		for(DBObject str:list)
		{
			if(str!=null&&str.get(keyName)!=null){
				if(string.equalsIgnoreCase(String.valueOf(str.get(keyName))))
				{
					return true;
				}
			}
		}
		return(false);
	}

    private static final Logger logger = LoggingFactory.getLogger(FCStringUtils.class);
}
