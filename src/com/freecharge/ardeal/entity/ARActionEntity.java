package com.freecharge.ardeal.entity;

import com.freecharge.common.util.FCStringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.json.simple.JSONObject;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ARActionEntity implements java.io.Serializable {
    private static final long serialVersionUID = 103062904052810157L;

    private Long id;
    @NotBlank(message = "AR Action name is required.")
    private String name;
    @NotNull
    private Long fkARActionTypeId;
    private String params;
    private Date createdOn;
    private Date updatedOn;
    private String emailTemplate;

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFkARActionTypeId() {
        return fkARActionTypeId;
    }

    public void setFkARActionTypeId(Long fkARActionTypeId) {
        this.fkARActionTypeId = fkARActionTypeId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getParams() {
        return params;
    }
    public void setParams(String params) {
        this.params = params;
    }
    
    public Date getCreatedOn() {
        return createdOn;
    }
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    public Date getUpdatedOn() {
        return updatedOn;
    }
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
    
    public String toString() {
        return "[ARActionEntity: id=" + id + ", name=" + name + ", fkARActionTypeId=" + fkARActionTypeId + ", params=" + params + "]";
    }

    public Map<String, Object> asMap(){
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("id", this.id);
        res.put("name", this.name);
        res.put("params", this.params);
        res.put("fkARActionTypeId", this.fkARActionTypeId);
        res.put("createdOn", this.createdOn.toString());
        res.put("updatedOn", this.updatedOn.toString());
        return res;
    }

    public JSONObject getParamsAsJson(){
        return FCStringUtils.jsonToMap(this.params);
    }
}
