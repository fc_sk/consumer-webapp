package com.freecharge.ardeal.action;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 2/6/14
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractARActivityDao {
    public abstract Boolean isAlreadyRewardedForAction(Long actionId, Long userId);
    public abstract void updateThreadId(Long activityId, Long threadId);
    public abstract void recordRewardedActivity(Long activityThreadId, Long userId, Long actionId);
}
