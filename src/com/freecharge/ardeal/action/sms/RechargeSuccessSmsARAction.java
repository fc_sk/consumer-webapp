package com.freecharge.ardeal.action.sms;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.freecharge.ardeal.entity.ARActionEntity;
import com.freecharge.ardeal.entity.ARActionTypeEntity;
import com.freecharge.ardeal.entity.ARDealEntity;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/27/13
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("rechargeSuccessSmsAction")
public class RechargeSuccessSmsARAction extends SendSmsARAction implements ApplicationListener<RechargeSuccessEvent> {
    private static final String actionTypeName = "rechargeSuccessSmsAction";

    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {
        String orderId = event.getOrderId();
        String number = event.getNumber();
        Integer userId = this.orderIdSlaveDAO.getUserIdFromOrderId(orderId);
        List<ARDealEntity> ardeals = this.ardealManager.getARDealsForUser(userId.longValue(), event.getLookupId());
        logger.info("Found_ArDeals number: "+number+" deals: "+(ardeals!=null?ardeals.size():null));
        if(ardeals!=null && ardeals.size() > 0){
            //Check if actionType in deal is rechargeSuccessSmsAction
            for (ARDealEntity ardeal : ardeals){
                ARActionTypeEntity actionTypeEntity = this.ardealDao.getARActionTypeByARDeal(ardeal);
                logger.debug("action_type: "+actionTypeEntity.getClassIdent());
                if (actionTypeEntity.getClassIdent().equals(actionTypeName)){
                    ARActionEntity actionEntity = this.ardealDao.getARActionById(ardeal.getFkActionId());

                    //Check that this action has not been performed before.
                    if(this.smsSentCount(actionEntity.getId(), number) > 0){
                        //Action already done.
                        logger.info("Sms already has been sent so do nothing. number: "+number);
                        return;
                    }
                    String message = (String) actionEntity.getParamsAsJson().get("message");
                    logger.debug("sending_sms: "+number);
                    Boolean res = this.sendSms(message, number);
                    this.recordMetric("rs");
                    String result = "FAILED";
                    logger.info("sms_sending_state: "+res+" for_number: "+number+" lookupid: "+event.getLookupId());
                    if (res){
                        result = "SENT";
                    }
                    //SMS sent. Making an entry with the activity and sms state in activity table
                    logger.info("adding_sms_activity: "+number+" for_user: "+userId+" for_state: "+result);
                    this.smsARActivityDao.addSmsActivity(actionEntity.getId(), userId.longValue(), orderId,
                            "RS", result, number);
                    break; //To avoid sending messages for all ardeals with this actionType
                }
            }
        }
    }
    
    public List<SMSARActivityEntity> getActivityByUser(Long userId, Long actionId){
    	return smsARActivityDao.getUserActivity(userId, actionId);
    }
    
    /**
     * This table doesn't have lookupId, hence returning null 
     * */
    @Override
	public String getLookupId(Long actionId, Long userId) {
		return null;
	}
    
   
}
