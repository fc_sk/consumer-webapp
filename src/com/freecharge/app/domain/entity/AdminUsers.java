package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

public class AdminUsers implements java.io.Serializable {
	
	private Integer adminUserID;
	private String  adminUserEmail;
	private String  adminUserName;
    private String  adminUserPassword;
	private Boolean adminUserActiveStatus;
	private String debitAmount;
	private String orderId;
	
	public String getAdminUserName() {
        return adminUserName;
    }

    public void setAdminUserName(String adminUserName) {
        this.adminUserName = adminUserName;
    }
	public Integer getAdminUserID() {
		return adminUserID;
	}

	public void setAdminUserID(Integer adminUserID) {
		this.adminUserID = adminUserID;
	}

	
	public String getAdminUserEmail() {
		return adminUserEmail;
	}

	public void setAdminUserEmail(String adminUserName) {
		this.adminUserEmail = adminUserName;
	}

	
	public String getAdminUserPassword() {
		return adminUserPassword;
	}

	public void setAdminUserPassword(String adminUserPassword) {
		this.adminUserPassword = adminUserPassword;
	}

	
	public Boolean getAdminUserActiveStatus() {
		return adminUserActiveStatus;
	}

	public void setAdminUserActiveStatus(Boolean adminUserActiveStatus) {
		this.adminUserActiveStatus = adminUserActiveStatus;
	}
	public String getDebitAmount() {
        return debitAmount;
    }


    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }


    public String getOrderId() {
        return orderId;
    }


    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public AdminUsers() {
    
    }
	
}
