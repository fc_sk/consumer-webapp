package com.freecharge.growthevent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.exception.MemcachedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.UserService;
import com.freecharge.ardeal.ARActivityTypeEnum;
import com.freecharge.ardeal.IARDealDao;
import com.freecharge.ardeal.action.promooptin.PromoOptInARAction;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.fraud.FreefundFraudConstant;
import com.freecharge.growthevent.service.EventCaptureService;
import com.freecharge.growthevent.service.EventCaptureService.EventType;
import com.freecharge.growthevent.service.GrowthActivityFailureMsgService;
import com.freecharge.growthevent.service.GrowthEventService;
import com.freecharge.growthevent.service.UserServiceProxy;

public class RsXOnAppInstallTest {
    @Autowired
    private static PromoOptInARAction promoOptInARAction;

    @Autowired
    private static UserServiceProxy userServiceProxy;

    @Autowired
    private static IARDealDao ardealDao;

    @Autowired
    private static EventCaptureService eventCaptureService;

    @Autowired
    private static GrowthActivityFailureMsgService failureMsgRepo;

    @Autowired
    private static GrowthEventService growthEventService;

    public static void main(String[] args) throws TimeoutException, InterruptedException, MemcachedException {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        ardealDao = (IARDealDao) ctx.getBean("ardealDBDao");
        userServiceProxy = (UserServiceProxy) ctx.getBean("userServiceProxy");
        failureMsgRepo = (GrowthActivityFailureMsgService) ctx.getBean("growthActivityFailureMsgService");
        eventCaptureService = (EventCaptureService) ctx.getBean("eventCaptureService");
        growthEventService = (GrowthEventService) ctx.getBean("growthEventService");

        testPromocodeReward();
        testSaveFailureReason();
        testEventReward();
    }

    private static void testPromocodeReward() {
        Users user = userServiceProxy.getUserByUserId(6);
        System.out.println("Recording Activity");

        growthEventService.processGrowthReward(1l, user.getUserId(), ARActivityTypeEnum.GROWTH, null, null);
    }

    private static void testSaveFailureReason() {
        failureMsgRepo.saveFailureReason(6l, 1l, "Sample Failure Reason", "FCV123213", "12334");
    }

    private static void testEventReward() {
        Map<String, Object> userInfo = new HashMap<String, Object>();
        userInfo.put(FCConstants.GROWTH_EVENT_USER_EMAIL, "jyothi.v462@gmail.com");
        userInfo.put(FreefundFraudConstant.TXN_CHANNEL, "ANDROID");
        userInfo.put(FCConstants.IMEI_NUMBER, "someIMEINumber");
        userInfo.put(FreefundFraudConstant.FINGER_PRINT, "someFingerPrint");
        eventCaptureService.captureEvent(EventType.REGISTRATION, userInfo);
    }
}
