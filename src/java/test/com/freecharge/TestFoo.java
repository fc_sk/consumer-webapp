package com.freecharge;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.freecharge.payment.services.PaymentCard;
import com.freecharge.payment.util.PaymentConstants;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public class TestFoo {
    private List<String> idempotencyExclusion = ImmutableList.of(PaymentConstants.ADMIN_CASH_BACK, PaymentConstants.MANUAL_REFUND);
    
    public void test() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-d HH:mm:ss,S");
//        System.out.println(df.format(new Date(System.currentTimeMillis())));
        
//        System.out.println(idempotencyExclusion.contains(null));
    }
    
    public void testC() {
        List<PaymentCard> cardList = new ArrayList<>();
        
        List<String> rawResponseList = Arrays.asList("|phone:9999999999|addedon:2014-04-03 09:10:09|payment_source:payu|version:2|mode:DC|amount:20.00|field4:3056443100940930|field3:3056443100940930|field2:540025|field9:SUCCESS|bankcode:VISA|field1:409348230433|key:oi3Liz|unmappedstatus:captured|issuing_bank:HDFC |net_amount_debit:20|txnid:FCVW14040329521187|PG_TYPE:HDFCPG|error:E000|cardnum:421424XXXXXX3811|status:success|hash:01ddf1a1294e39ec2145d6bafc462507e91786ba96c716d7c55af3f02aadc643ff114e1b5e76cda83375a9ed5080bf598830f816b4f77b14172b9b06b16a110d|error_Message:No Error|cardhash:c10ea069eb4088193178eddc1d78dfba601df9d2c484dbe1ea09f69b8c9e705e|firstname:NA|name_on_card:Arunkumar S Jadhav|productinfo:1|checksumResult:true|discount:0.00|bank_ref_num:3056443100940930|card_type:VISA|email:NA|mihpayid:89718877");
        
        for (String rawResponse : rawResponseList) {
            List<String> responseList = Lists.newArrayList((Splitter.on("|").split(rawResponse)));
            final PaymentCard card = new PaymentCard();
            
            for (String responseKV : responseList) {
                if (StringUtils.contains(responseKV, "cardnum") || StringUtils.contains(responseKV, "card_no")) {
                    String cardBin = extractData(responseKV);
                    if (StringUtils.isNotBlank(cardBin)) {
                        card.setCardBin(cardBin);
                    }
                } else if (StringUtils.contains(responseKV, "cardhash")) {
                    String cardHash = extractData(responseKV);
                    if (StringUtils.isNotBlank(cardHash)) {
                        card.setCardHash(cardHash);
                    }
                }
            }
            
            if (!card.isEmpty()) {
                cardList.add(card);
            }

        }
        

        
        for (PaymentCard paymentCard : cardList) {    
            System.out.println(paymentCard.getCardHash());
            System.out.println(paymentCard.getCardBin());
        }
    }
    
    private String extractData(String responseKV) {
        String cardBin = null;
        String[] cardData = responseKV.split(":");
        if (cardData != null && cardData.length == 2) {
            cardBin = cardData[1];
        }
        return cardBin;
    }
    
    public void testDynamoPut() throws Exception {
        AmazonDynamoDBAsync dynamoClient = new AmazonDynamoDBAsyncClient();
        dynamoClient.setEndpoint("https://dynamodb.ap-southeast-1.amazonaws.com");
        dynamoClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        
        Map<String, AttributeValue> item = new HashMap<>();
        item.put("serviceNumber", new AttributeValue("9342138044"));
        item.put("oi", new AttributeValue("88"));
        item.put("ci", new AttributeValue("99"));
        
        Future<PutItemResult> putRequest = dynamoClient.putItemAsync(new PutItemRequest("mnpMap", item));
        
        while (!putRequest.isDone()) {
            TimeUnit.SECONDS.sleep(10);
        }
    }
    
    public void testDynamoGet() throws Exception {
        AmazonDynamoDBAsync dynamoClient = new AmazonDynamoDBAsyncClient();
        dynamoClient.setEndpoint("https://dynamodb.ap-southeast-1.amazonaws.com");
        dynamoClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        
        GetItemRequest gir = new GetItemRequest("mnpMap", ImmutableMap.of("serviceNumber", new AttributeValue("9342138044")));
        
        GetItemResult getResponse = dynamoClient.getItem(gir);
        
        final Map<String, AttributeValue> item = getResponse.getItem();
        if (item != null) {
            for (Entry<String, AttributeValue> itemRow : item.entrySet()) {
                System.out.println(itemRow.getKey() + " : " + itemRow.getValue().getS());
            }
        }
    }
    
    @Test
    public void testFromJson() throws Exception {
        String jsonString = "{\"data\":{ \"token\": \"B5Rjdn9SlHoFo2kHLSJI68cwCOWI2nsobLoxlkhPgVpU3HOGFPXGHLGIr1bYNXivGvnHUuxZ_mfStUgcKgV6Uz9Bhw3NTaeI93op1XywlJM\", \"tokenExpiry\": \"2015-09-02T13:52:09\", \"globalToken\": \"XvC2OJLUDQ1kgP8doXeqgQvSZhaM24X0gsCwcw5i0rHiTbNTAXDmONiNM6RcikC_\", \"globalTokenExpiry\": \"2015-09-02T13:52:09\", \"accountOwner\": \"SD\", \"emailId\": \"foozzooesdf@gmail.com\", \"userId\": \"599721\", \"sdUserId\": 599721, \"fcUserId\": 0, \"mobileNumber\": null, \"firstName\": \"AB\", \"middleName\": \"C\", \"lastName\": \"D\", \"displayName\": \"ABC\", \"gender\": null, \"dob\": null, \"languagePref\": null, \"mobileVerified\": false }}";
        
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<HashMap<String, HashMap<String, Object>>> typeRef = new TypeReference<HashMap<String, HashMap<String, Object>>>() {};
        
        Map<String, HashMap<String, String>> responseMap = mapper.readValue(jsonString, typeRef);
        
        Map<String, String> responseData = responseMap.get("data");
        System.out.println(responseData.get("token"));
    }
    
    public void testJson() throws Exception {
        JSONObject jo = new JSONObject();
        jo.put("merchant_id", "fcbrowser");
        jo.put("client_id", "fcbrowser_android");
        
        JSONArray ja = new JSONArray();
        ja.add(jo);
        
        JSONObject toReturn = new JSONObject();
        toReturn.put("data", ja);

        System.out.println(toReturn.toString());
    }
    
    public void testHmac() throws Exception {
        String secret = "mySecret";
        String message = "4444333322221111";

        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        String hash = new String(Base64.encodeBase64(sha256_HMAC.doFinal(message.getBytes())));
        System.out.println(hash);
    }
}
