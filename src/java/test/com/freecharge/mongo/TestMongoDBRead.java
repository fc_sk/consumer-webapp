package com.freecharge.mongo;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.session.persistence.SessionRepository;
import com.freecharge.mongo.repos.UserRepository;
import com.mongodb.DBObject;

/**
 * Read data from mongo db.
 * @author shirish
 *
 */
public class TestMongoDBRead {
	
	
	@Test
	public void testMongoRead() {
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        UserRepository userRepository = ((UserRepository) ctx.getBean("userRepository"));
        DBObject userDBObject = userRepository.getUserByUserId(184l);
        System.out.println("Read: " + userDBObject);
        
    }
	
	@Test
	public void testMongoReadSession() {
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        SessionRepository userRepository = ((SessionRepository) ctx.getBean("sessionRepository"));
        DBObject userDBObject = userRepository.getSessionBySessionId("");
        System.out.println("Read: " + userDBObject);
        
    }
}
