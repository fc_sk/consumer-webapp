package com.freecharge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.api.coupon.service.IGeoIPService;
import com.freecharge.common.util.SpringBeanLocator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:batchContext.xml")
@ActiveProfiles("prod")
public class TestGeoIPService implements ApplicationContextAware{
	@Autowired
	@Qualifier("FastMaxMindGeoIPService")
	private IGeoIPService geoIpService;
	
	@Test
	public void testGetCity(){
		String cityName = geoIpService.getCityName("1.38.29.115");
		System.out.println("City Name: " + cityName);
	}
	
	@Override
    public void setApplicationContext(ApplicationContext context)
            throws BeansException
    {
		SpringBeanLocator.getInstance().setApplicationContext(context);
    }
}
	