package com.freecharge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.api.coupon.service.CouponRankService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:batchContext.xml")
@ActiveProfiles("prod")
public class TestCouponRankService {
	@Autowired
	private CouponRankService rankService;
	
	@Test
	public void testFulfilment(){
		rankService.updateCouponRankDataForDuration(1);
	}
}
	