package com.freecharge;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.recharge.services.MetroReportService;
import com.freecharge.rest.model.MetroReportData;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:batchContext.xml")
@ActiveProfiles("dev")
public class TestMetroReportService {
	@Autowired
	private MetroReportService metroReportService;
	
	@Test
	public void testFromToDateSearch(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/dd/MM");
		String fromDateStr = "2015/01/08";
		String toDateStr = "2015/14/08";
		try {
			List<MetroReportData> data = metroReportService.searchMetroTransactionHistory(formatter.parse(toDateStr), formatter.parse(fromDateStr), null, null, null);
			System.out.println(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFromToDateAndOrderIdSearch(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/dd/MM");
		String fromDateStr = "2015/01/08";
		String toDateStr = "2015/14/08";
		String orderId = "FCZW150812215708905";
		try {
			List<MetroReportData> data = metroReportService.searchMetroTransactionHistory(formatter.parse(toDateStr), formatter.parse(fromDateStr), "21", true, orderId);
			System.out.println(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
}
	