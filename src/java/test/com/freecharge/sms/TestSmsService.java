package com.freecharge.sms;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;

/**
 * Truncate SMS gateway name in common.properties>smssenderpriority property to test out gateways.
 * @author shirish
 *
 */
public class TestSmsService {
	
	@Test
	public void testRefundSendSms()
	{
		sendRefundSms("testorderid", "123", "8867899682");
        System.out.println("Check if the number got an SMS -----------------------------");
	}
	
	@Test
	public void testRechargeUnderProcessSms()
	{
		sendRUPSms("testorderid", "9741803053");
        System.out.println("Check if the number got an SMS -----------------------------");
	}
	
	private void sendRefundSms(String orderId, String amount, String no) {
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        Map<String, String> params = new HashMap<String, String>();
        params.put("orderId", orderId);
        params.put("refundAmount", amount);
        smsBusinessDO.setReceiverNumber(no);
        smsBusinessDO.setTemplateName("templates/sms/refund.vm");
        smsBusinessDO.setVariableValues(params);
        
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        SMSService smsService = ((SMSService) ctx.getBean("SMSService"));
        smsService.sendSMS(smsBusinessDO);
    }
	
	private void sendRUPSms(String orderId, String no) {
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        Map<String, String> params = new HashMap<String, String>();
        params.put("orderId", orderId);
        smsBusinessDO.setReceiverNumber(no);
        smsBusinessDO.setTemplateName("templates/sms/rechargeunderprocess.vm");
        smsBusinessDO.setVariableValues(params);
        
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        SMSService smsService = ((SMSService) ctx.getBean("SMSService"));
        smsService.sendSMS(smsBusinessDO);
    }
}
