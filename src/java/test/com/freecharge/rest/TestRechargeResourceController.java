package com.freecharge.rest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

public class TestRechargeResourceController {

    public void testsaveRechargeInfo() throws Exception {
        HttpClient client = new HttpClient();

        NameValuePair[] saveRecharge = {
                new NameValuePair("serviceNumber", ""),
                new NameValuePair("operator", "10"),
                new NameValuePair("productType", "V"),
                new NameValuePair("amount", ""),
            };
        
        PostMethod postMethod = new PostMethod("http://localhost:8080/rest/recharge/save");
        postMethod.setRequestBody(saveRecharge);
        
        client.executeMethod(postMethod);
        
        String saveResponse = postMethod.getResponseBodyAsString();
        
        System.out.println(saveResponse);
    }
}
