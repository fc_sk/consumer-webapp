package com.freecharge.rest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Test;

public class TestBillPayResourceController {

    @Test
    public void testCreateBill() throws Exception {
        HttpClient client = new HttpClient();

        NameValuePair[] billPayRequest = {
                new NameValuePair("serviceProviderID", "BSES"),
                new NameValuePair("billAmount", "10"),
                new NameValuePair("consumerAccountNumber", "11233"),
                new NameValuePair("billingUnit", "aabcd"),
                new NameValuePair("processingCycle", "12")
            };
        
        PostMethod postMethod = new PostMethod("http://localhost:8080/rest/bill/create");
        postMethod.setRequestBody(billPayRequest);
        
        client.executeMethod(postMethod);
        
        String saveResponse = postMethod.getResponseBodyAsString();
        
        System.out.println(saveResponse);
    }
    
    @Test
    public void testCreateMobileBill() throws Exception {
        PostMethod postMethod = new PostMethod("http://localhost:8080/rest/bill/postpaid/mobile/9342138044/operator/56/amount/20");
        HttpClient client = new HttpClient();
        client.executeMethod(postMethod);
        
        
        String saveResponse = postMethod.getResponseBodyAsString();
        
        System.out.println(saveResponse);
    }
    
    @Test
    public void testCreatedRecharge() throws Exception {
        NameValuePair[] saveRequest = {
                new NameValuePair("serviceNumber", "91342138044"),
                new NameValuePair("operatorName", "10"),
                new NameValuePair("circleName", "10"),
                new NameValuePair("amount", "100"),
                new NameValuePair("productType", "V"),
                new NameValuePair("operator", "10"),
                new NameValuePair("rechargePlanType", "topup")
            };

        PostMethod postMethod = new PostMethod("http://localhost:8080/rest/recharge/save");
        postMethod.setRequestBody(saveRequest);
        
        HttpClient client = new HttpClient();
        client.executeMethod(postMethod);
        
        String saveResponse = postMethod.getResponseBodyAsString();
        
        System.out.println(saveResponse);
    }
    
    public void testCreateCartItems() throws Exception {
        HttpClient client = new HttpClient();

        NameValuePair[] couponSaveRequest = {
                new NameValuePair("type", "coupon"),
                new NameValuePair("itemIdArray", ""),
                new NameValuePair("lookupID", "18f7c615-ba8e-478c-ae43-46b6134b9018"),
            };
        
        PostMethod postMethod = new PostMethod("http://localhost:8080/rest/coupon/optin/save");
        postMethod.setRequestBody(couponSaveRequest);
        
        client.executeMethod(postMethod);
        
        String saveResponse = postMethod.getResponseBodyAsString();
        
        System.out.println(saveResponse);
    }
}