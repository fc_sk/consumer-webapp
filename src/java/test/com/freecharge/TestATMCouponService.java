package com.freecharge;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.app.service.AffiliateCouponService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.rest.model.AffiliateCouponsRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:batchContext.xml")
@ActiveProfiles("prod")
public class TestATMCouponService implements ApplicationContextAware{
	@Autowired
	private AffiliateCouponService atmCouponService;
	
	@Test
	public void testSaveATMCouponsOrder(){
		Map<String, String> affiliateRequestData = new HashMap<String, String>();
		affiliateRequestData.put(FCConstants.KEY_COUPON_MERCHANT_ID, "123");
		affiliateRequestData.put(FCConstants.ATM_ID, "testAtmsdafasd");
		
		
		AffiliateCouponsRequest request = new AffiliateCouponsRequest();
		request.setCouponAffiliateId(FCConstants.AFFILIATE_ID_ATM);
		request.setAffiliateRequestData(affiliateRequestData);
		request.setAffiliateOrderId("testAOid");
		atmCouponService.saveAffiliateCouponsOrder(request);
	}
	
	@Override
    public void setApplicationContext(ApplicationContext context)
            throws BeansException
    {
		SpringBeanLocator.getInstance().setApplicationContext(context);
    }
}
	