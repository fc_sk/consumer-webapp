package com.freecharge.dop.activity;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.payment.util.PaymentConstants;

import flexjson.JSONSerializer;

public class TestFulfilmentActivity {
	private JSONSerializer jsonSerializer = new JSONSerializer();
	@Test
	public void testFulfilment(){
		ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
		FulfillmentActivity activity = ctx.getBean(FulfillmentActivity.class);
		Map<String, String> fulfillmentRequest = new HashMap<>();
		fulfillmentRequest.put(PaymentConstants.ORDER_ID_KEY, "FCVW13121120104599");
		fulfillmentRequest.put(PaymentConstants.SHOULD_SEND_SMS_KEY,
		        Boolean.FALSE.toString());
		activity.consume(jsonSerializer.serialize(fulfillmentRequest));
	}
}
