package com.freecharge.payment.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Test;

import com.freecharge.payment.services.PayUGatewayHandler.Constants;
import com.freecharge.payment.services.PayUGatewayHandler.WebServiceParameters;

import flexjson.JSONDeserializer;

public class TestPayUWSCalls {
    private static final String HASH_SALT = "v1ck1cHv";
    private static final String MERCHANT_KEY = "oi3Liz";
    private static final String PAYU_ENDPOINT = "https://localhost:8080/merchant/postservice.php?form=2";
    
    JSONDeserializer<Map<String, Object>> deserializer = new JSONDeserializer<Map<String,Object>>();
    
    @Test
    public void testCheckOrderID() throws Exception {
        String merchantKey = MERCHANT_KEY;
        String salt = HASH_SALT;
        String merchantTransactionId = "a83a7420ccbe572c47f8";
        
        /*
         * {status=1,
         * transaction_details={a83a7420ccbe572c47f8=[{unmappedstatus=captured,
         * addedon=2013-09-10 15:55:09, PG_TYPE=AXIS, status=success, card_no=,
         * name_on_card=null, disc=0.00, mode=CC, bank_ref_num=aabbcc, udf2=,
         * amt=100.00, request_id=, Settled_At=0000-00-00 00:00:00,
         * Merchant_UTR=null, mihpayid=403993715508430104},
         * {unmappedstatus=captured, addedon=2013-09-10 15:55:28, PG_TYPE=AXIS,
         * status=success, card_no=412345XXXXXX2356, name_on_card=tetetete,
         * disc=0.00, mode=CC, bank_ref_num=abccdd, udf2=, amt=100.00,
         * request_id=, Settled_At=0000-00-00 00:00:00, Merchant_UTR=null,
         * mihpayid=403993715508430106}, {unmappedstatus=captured,
         * addedon=2013-09-10 16:01:57, PG_TYPE=AXIS, status=success,
         * card_no=512345XXXXXX2346, name_on_card=testet, disc=0.00, mode=CC,
         * bank_ref_num=2000090000, udf2=, amt=100.00, request_id=,
         * Settled_At=0000-00-00 00:00:00, Merchant_UTR=null,
         * mihpayid=403993715508430160}]}, msg=3 out of 1 Transactions Fetched
         * Successfully}
         */

        String hash = calculateHash(merchantKey
                + Constants.HASH_STRING_DELIMITER
                + Constants.PAYMENT_STATUS_METHOD
                + Constants.HASH_STRING_DELIMITER + merchantTransactionId
                + Constants.HASH_STRING_DELIMITER + salt, "SHA-512");
        
        NameValuePair[] statusCheckSubmitData = {
                new NameValuePair(WebServiceParameters.METHOD_NAME, Constants.PAYMENT_STATUS_METHOD),
                new NameValuePair(WebServiceParameters.MERCHANT_KEY_PARAMETER, merchantKey),
                new NameValuePair(WebServiceParameters.MERCHANT_TXN_ID_PARAMETER, merchantTransactionId),
                new NameValuePair(WebServiceParameters.HASH_PARAMETER, hash),
            };
        
        HttpClient client = new HttpClient();
        PostMethod postMethod = new PostMethod(PAYU_ENDPOINT);
        
        postMethod.setRequestBody(statusCheckSubmitData);

        int postStatus = client.executeMethod(postMethod);
        
        String paymentResponse = postMethod.getResponseBodyAsString();
        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
            
            System.out.println(paymentResponse);
            Map<String, Object> deserialize = deserializer.deserialize(paymentResponse);

            System.out.println(deserialize);
            
            Map<String, String> txnDetails = (Map<String, String>) deserialize.get("transaction_details");
            
//            System.out.println(txnDetails);
        } else {
            System.out.println("Error!!! " + postStatus);
            System.out.println(paymentResponse);
        }
    }

//    @Test
//    public void testStatusCheckPost() throws Exception {
//        HttpClient client = new HttpClient();
//        
//        PostMethod postMethod = new PostMethod(PAYU_ENDPOINT);
//        
//        String command = "check_payment";
//        String txnId = "403993715507747260";
//        String merchantKey = MERCHANT_KEY;
//        String salt = HASH_SALT;
//        
//        NameValuePair[] data = {
//                new NameValuePair("command", command),
//                new NameValuePair("var1", txnId),
//                new NameValuePair("key", merchantKey),
//                new NameValuePair("hash", calculateHash(merchantKey+"|" + command + "|" + txnId + "|" + salt, "SHA-512")),
//            };
//        
//        postMethod.setRequestBody(data);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        String paymentResponse = postMethod.getResponseBodyAsString();
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
//            
//            Map<String, Object> deserialize = deserializer.deserialize(paymentResponse);
//
//            System.out.println(deserialize);
//            
//            Map<String, String> txnDetails = (Map<String, String>) deserialize.get("transaction_details");
//            
//            System.out.println(txnDetails);
//        } else {
//            System.out.println("Error!!! " + postStatus);
//            System.out.println(paymentResponse);
//        }
//    }
//    
//    @Test
//    public void testRefundWSCall() throws Exception {
//        HttpClient client = new HttpClient();
//        
//        PostMethod postMethod = new PostMethod(PAYU_ENDPOINT);
//        
//        String command = "cancel_refund_transaction";
//        String txnId = "403993715507747260";
//        String merchantKey = MERCHANT_KEY;
//        String salt = HASH_SALT;
//        
//        String tokenId = "123488x1c";
//        String amount = "10.00";
//        
//        NameValuePair[] data = {
//                new NameValuePair("command", command),
//                new NameValuePair("var1", txnId),
//                new NameValuePair("var2", tokenId),
//                new NameValuePair("var3", amount),
//                new NameValuePair("key", merchantKey),
//                new NameValuePair("hash", calculateHash(merchantKey+"|" + command + "|" + txnId + "|" + salt, "SHA-512")),
//            };
//        
//        postMethod.setRequestBody(data);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        String paymentResponse = postMethod.getResponseBodyAsString();
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
//            
//            Map<String, Object> deserialize = deserializer.deserialize(paymentResponse);
//            
//            System.out.println(deserialize);
//            
//            Map<String, String> txnDetails = (Map<String, String>) deserialize.get("transaction_details");
//            
//            System.out.println(txnDetails);
//        } else {
//            System.out.println("Error!!! " + postStatus);
//            System.out.println(paymentResponse);
//        }
//    }
//    
//    @Test
//    public void testCheckStatus() throws Exception {
//        HttpClient client = new HttpClient();
//        
//        PostMethod postMethod = new PostMethod(PAYU_ENDPOINT);
//        
//        String command = "check_action_status";
//        String txnId = "32478";
//        String merchantKey = MERCHANT_KEY;
//        String salt = HASH_SALT;
//        
//        NameValuePair[] data = {
//                new NameValuePair("command", command),
//                new NameValuePair("var1", txnId),
//                new NameValuePair("key", merchantKey),
//                new NameValuePair("hash", calculateHash(merchantKey+"|" + command + "|" + txnId + "|" + salt, "SHA-512")),
//            };
//        
//        postMethod.setRequestBody(data);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        String paymentResponse = postMethod.getResponseBodyAsString();
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
//            
//            Map<String, Object> deserialize = deserializer.deserialize(paymentResponse);
//            
//            Map<String, String> txnDetails = (Map<String, String>) deserialize.get("transaction_details");
//            
//            System.out.println(txnDetails);
//        } else {
//            System.out.println("Error!!! " + postStatus);
//            System.out.println(paymentResponse);
//        }
//    }
//    
//    @Test
//    public void testSeamless() throws Exception {
//        /*
//         * if(
//          empty($posted['key'])
//          || empty($posted['txnid'])
//          || empty($posted['amount'])
//          || empty($posted['firstname'])
//          || empty($posted['email'])
//          || empty($posted['phone'])
//          || empty($posted['productinfo'])
//          || empty($posted['surl'])
//          || empty($posted['furl'])
//          || empty($posted['CCNAME'])
//          || empty($posted['CCNUM'])
//          || empty($posted['CCVV'])
//          || empty($posted['CCEXPMON'])
//          || empty($posted['CCEXPYR'])
//          || empty($posted['pg'])
//          || empty($posted['bankcode'])
//  
//         */
//        
//        HttpClient client = new HttpClient();
//        
//        PostMethod postMethod = new PostMethod("https://test.payu.in/_payment");
//        
//        String txnId = RandomStringUtils.randomAlphanumeric(10);
//        String merchantKey = MERCHANT_KEY;
//        String salt = HASH_SALT;
//        String amount = "128.00";
//        String firstName = "Arunkumar";
//        String email = "email";
//        String phone = "phone";
//        String productInfo = "MobileRecharge";
//        String url = "http://localhost:8080/handleresponse";
//        String ccName = "Arunkumar S Jadhav";
//        String ccNum = "5123456789012346";
//        String ccvv = "111";
//        String expMon = "05";
//        String expYear = "2013";
//        String pg = "CC";
//        String bankCode = "CC";
//        
//        String hash = calculateHash(merchantKey+"|" + txnId + "|" + amount + "|" + productInfo + "|" + firstName + "|" + email + "|||||||||||" + salt, "SHA-512");
//        
//        System.out.println(hash);
//        
//        NameValuePair[] data = {
//                new NameValuePair("key", merchantKey),
//                new NameValuePair("txnid", txnId),
//                new NameValuePair("amount", amount),
//                new NameValuePair("firstname", firstName),
//                
//                new NameValuePair("email", email),
//                new NameValuePair("phone", phone),
//                new NameValuePair("productInfo", productInfo),
//                new NameValuePair("surl", url),
//                new NameValuePair("furl", url),
//
//                new NameValuePair("CCNAME", ccName),
//                new NameValuePair("CCNUM", ccNum),
//                new NameValuePair("CCVV", ccvv),
//                
//                new NameValuePair("CCEXPMON", expMon),
//                new NameValuePair("CCEXPYR", expYear),
//                new NameValuePair("pg", pg),
//                new NameValuePair("bankcode", bankCode),
//                
//
//                /*
//                 * key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10
//                 */
//                
//                new NameValuePair("hash", hash),
//            };
//        
//        postMethod.setRequestBody(data);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        String paymentResponse = postMethod.getResponseBodyAsString();
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
//            
//            Map<String, Object> deserialize = deserializer.deserialize(paymentResponse);
//            
//            Map<String, String> txnDetails = (Map<String, String>) deserialize.get("transaction_details");
//            
//            System.out.println(txnDetails);
//        } else {
//            System.out.println("Error!!! " + postStatus);
//            System.out.println(paymentResponse);
//            
//            Header[] headers = postMethod.getResponseHeaders();
//            for (Header header : headers) {
//                System.out.println("Name: " + header.getName() + ", value : " + header.getValue());
//                
//            }
//        }
//    }
//    
//    @Test
//    public void testAxis() throws Exception {
//        HttpClient client = new HttpClient();
//        
//        PostMethod postMethod = new PostMethod("https://migs.mastercard.com.au/vpcpay");
//
//        
//        NameValuePair submitData[] = {
//                new NameValuePair("Title" ,"PHP VPC 2.5-Party"),
//                new NameValuePair("vpc_AccessCode" ,"BDEF182C"),
//                new NameValuePair("vpc_Amount" ,"12800"),
//                new NameValuePair("vpc_CardExp" ,"1305"),
//                new NameValuePair("vpc_CardNum" ,"5123456789012346"),
//                new NameValuePair("vpc_CardSecurityCode" ,"111"),
//                new NameValuePair("vpc_Command" ,"pay"),
//                new NameValuePair("vpc_Locale" ,"en"),
//                new NameValuePair("vpc_MerchTxnRef" ,"403993715507749750"),
//                new NameValuePair("vpc_Merchant" ,"TESTIBIBOWEB"),
//                new NameValuePair("vpc_OrderInfo" ,"MobileRecharge"),
//                new NameValuePair("vpc_ReturnURL" ,"https://test.payu.in/_axis_response.php"),
//                new NameValuePair("vpc_TicketNo" ,""),
//                new NameValuePair("vpc_Version" ,"1"),
//                new NameValuePair("vpc_card" ,"MASTERCARD"),
//                new NameValuePair("vpc_gateway" ,"ssl"),
//                new NameValuePair("vpc_SecureHash" ,"FCF4B5E70D050F19580A802C1B909662")
//        };
//        
//        postMethod.setRequestBody(submitData);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        String paymentResponse = postMethod.getResponseBodyAsString();
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(paymentResponse);
//        } else {
//            System.out.println("Error! " + postStatus);
//            System.out.println(paymentResponse);
//            
//            Header[] headers = postMethod.getResponseHeaders();
//            
//            for (Header header : headers) {
//                System.out.println(header.getName() + ":" + header.getValue());
//            }
//        }
//
//    }
    
    private String calculateHash(String stringToHash, String hashType)
            throws NoSuchAlgorithmException {
        byte[] hashseq = stringToHash.getBytes();

        StringBuffer hexString = new StringBuffer();

        MessageDigest algorithm = MessageDigest.getInstance(hashType);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();

        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);

            if (hex.length() == 1) {
                hexString.append("0");
            }

            hexString.append(hex);
        }

        return hexString.toString();
    }
    
    public static boolean Check(String ccNumber) {
        int sum = 0;
        boolean alternate = false;
        for (int i = ccNumber.length() - 1; i >= 0; i--) {
            int n = Integer.parseInt(ccNumber.substring(i, i + 1));
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }
}
