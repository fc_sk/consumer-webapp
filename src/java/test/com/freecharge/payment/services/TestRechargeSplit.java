package com.freecharge.payment.services;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;

public class TestRechargeSplit {

    @Test
    public void testSplit() {
        ConcurrentMap<String, Integer> euList = new ConcurrentHashMap<>(); 
        final int total = 100000;
        
        final String EU = "EU";
        final String OXIGEN = "OX";
        final String SUVIDHA = "SU";

        euList.put(EU, 0);
        euList.put(OXIGEN, 0);
        euList.put(SUVIDHA, 0);
        
        ConcurrentMap<String, Integer> distList = new ConcurrentHashMap<>();
        distList.put(EU, 21);
        distList.put(OXIGEN, 33);
        distList.put(SUVIDHA, 46);
        
        for(int i = 0; i < total; i++) {
            final int nextInt = RandomUtils.nextInt(9999);
            if (nextInt < distList.get(EU) * 100) {
                Integer current = euList.get(EU);
                euList.put(EU, current + 1);
            } else if (nextInt < (distList.get(EU) + distList.get(OXIGEN)) * 100) {
                Integer current = euList.get(OXIGEN);
                euList.put(OXIGEN, current + 1);
            } else {
                Integer current = euList.get(SUVIDHA);
                euList.put(SUVIDHA, current + 1);
            }
        }
        
        for (String ag : euList.keySet()) {
            System.out.println(ag + ":" + euList.get(ag) + "(" + distList.get(ag) + ")");
        }
    }
}
