package com.freecharge.payment.services;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;

public class TestPayUStatus {

    @Test
    public void testPaymentStatus() throws FCPaymentGatewayException, Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        IGatewayHandler payuPG = (IGatewayHandler) ctx.getBean("payUGatewayHandler");
        
        PaymentQueryBusinessDO queryDO = new PaymentQueryBusinessDO();
        queryDO.setMerchantTxnId("FCVW1301283477797");
        
        PaymentQueryBusinessDO paymentStatus = payuPG.paymentStatus(queryDO, false);
            
        System.out.println(paymentStatus);
        
        ctx.close();
    }

}
