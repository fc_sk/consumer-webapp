package com.freecharge.payment.services;


public class TestBDS2SResponseHandling {

//	@Test
//	public void testS2SHandling() throws Exception {
//        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
//        
//        BillDeskGatewayHandler bdGatewayHandler = ctx.getBean(BillDeskGatewayHandler.class);
//        
//        Map<String, String> requestMapping = new HashMap<String, String>();
//        requestMapping.put("msg", "FREECHARGE|FCVW12112815254291|MSPG2860450605|112112839630331|269.00|SPG|NA|NA|INR|DIRECT|NA|NA|NA|28-11-2012 18:17:44|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|1236123906");
//        
//    	PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
//    	paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
//    	paymentResponseDO.setRequestMap(requestMapping);
//    	paymentResponseDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));
//    	
//    	bdGatewayHandler.handleS2SPaymentResponse(paymentResponseDO);
//        
//        ctx.close();
//	}
	
//	@Test
//	public void testS2SE2EFeature() throws Exception {
//		HttpClient client = new HttpClient();
//		
//		String qaS2SURL = "http://qa.freecharge.com/payment/s2s/bd";
//		final String MSG_QA = "FREECHARGE|FCVW12111900088501|MCIT2853938013|102961-481195|00000029.00|CIT|22419617|NA|INR|DIRECTD|NA|NA|NA|19-11-2012 21:02:15|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|3887294810";
//
//		String prodS2SURL = "http://www.freecharge.in/payment/s2s/bd";
//		final String MSG_PROD = "FREECHARGE|FCVW12112815254291|MSPG2860450605|112112839630331|269.00|SPG|NA|NA|INR|DIRECT|NA|NA|NA|28-11-2012 18:17:44|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|1236123906";
//		
//		PostMethod postMethod = new PostMethod(qaS2SURL);
//		
//		NameValuePair[] data = {
//			new NameValuePair("msg", MSG_QA)	
//		};
//		
//		postMethod.setRequestBody(data);
//		
//		int postStatus = client.executeMethod(postMethod);
//		
//		if(HttpStatus.SC_OK == postStatus) {
//			System.out.println(postMethod.getResponseBodyAsString());
//		} else {
//			System.out.println("Error!!! " + postStatus);
//			System.out.println(postMethod.getResponseBodyAsString());
//		}
//	}
//	
//	@Test
//	public void testS2SGet() throws Exception {
//	    HttpClient client = new HttpClient();
//	    
//        String qaS2SURL = "https://www.freecharge.in/payment/s2s/bd";
//        final String MSG_QA = "FREECHARGE|FCVW12111900088501|MCIT2853938013|102961-481195|00000029.00|CIT|22419617|NA|INR|DIRECTD|NA|NA|NA|19-11-2012 21:02:15|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|3887294810";
//        String qaEncodedString = URLEncoder.encode(MSG_QA, "UTF-8");
//        String qaS2SGetURL = qaS2SURL + "?msg=" + qaEncodedString;
//
//        String prodS2SURL = "http://www.freecharge.in/payment/s2s/bd";
//        final String MSG_PROD = "FREECHARGE|FCVW12112815254291|MSPG2860450605|112112839630331|269.00|SPG|NA|NA|INR|DIRECT|NA|NA|NA|28-11-2012 18:17:44|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|1236123906";
//        String prodEncodedString = URLEncoder.encode(MSG_PROD, "UTF-8");
//        String prodS2SGetURL = prodS2SURL + "?msg=" + prodEncodedString;
//        
//        String urlToGet = qaS2SGetURL;
//        
//        System.out.println(urlToGet);
//        GetMethod getMethod = new GetMethod(urlToGet);
//        
//        int getStatus = client.executeMethod(getMethod);
//
//        if(HttpStatus.SC_OK == getStatus) {
//            System.out.println(getMethod.getResponseBodyAsString());
//        } else {
//            System.out.println("Error!!! " + getStatus);
//            System.out.println(getMethod.getResponseBodyAsString());
//        }
//	}
    
//	@Test
//    public void testPayUS2SE2EFeature() throws Exception {
//        HttpClient client = new HttpClient();
//        
//        String qaS2SURL = "https://www.freecharge.in/rest/payment/callback/payu";
//
//        String prodS2SURL = "http://www.freecharge.in/rest/payment/callback/payu";
//        
//        PostMethod postMethod = new PostMethod(qaS2SURL);
//        
//        NameValuePair[] data = {
//                new NameValuePair("unmappedstatus", "success"),
//                new NameValuePair("phone", "9999999999"),
//                new NameValuePair("txnid", "FCTW14050532942410"),
//                new NameValuePair("hash", "b3747fb66e5235c6ba75bbc4acd9fab7d4077ec4d8aa141cb2649a4ece511e3a5a2f6f81bf2fe59c1b463fe330497667ea5d1face5f9d00832acee4b0c232ea2"),
//                new NameValuePair("status", "success"),
//                new NameValuePair("curl", "https://www.freecharge.in/m/payment/response/handlepayuresponse"),
//                new NameValuePair("firstname", "NA"),
//                new NameValuePair("card_no", "402985XXXXXX7864"),
//                new NameValuePair("furl", "https://www.freecharge.in/m/payment/response/handlepayuresponse"),
//                new NameValuePair("productinfo", "1"),
//                new NameValuePair("mode", "DC"),
//                new NameValuePair("amount", "155.00"),
//                new NameValuePair("field4", "1907998341041260"),
//                new NameValuePair("field3", "1907998341041260"),
//                new NameValuePair("field2", "103457"),
//                new NameValuePair("field9", "SUCCESS"),
//                new NameValuePair("email", "NA"),
//                new NameValuePair("mihpayid", "100684981"),
//                new NameValuePair("surl", "https://www.freecharge.in/m/payment/response/handlepayuresponse"),
//                new NameValuePair("card_hash", "f3d393e6953112005c3a9e8b59a239f60793ab3ecd23b2b3d1cdff87d468f2a0"),
//                new NameValuePair("field1", "412673651189"),
//                new NameValuePair("bank_ref_no", "1907998341041260"),
//                new NameValuePair("key", "oi3Liz"),
//        };
//        
//        postMethod.setRequestBody(data);
//        
//        int postStatus = client.executeMethod(postMethod);
//        
//        if(HttpStatus.SC_OK == postStatus) {
//            System.out.println(postMethod.getResponseBodyAsString());
//        } else {
//            System.out.println("Error!!! " + postStatus);
//            System.out.println(postMethod.getResponseBodyAsString());
//        }
//    }
}
