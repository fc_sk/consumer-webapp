package com.freecharge.payment.services;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.handlingcharge.PricingService;

public class TestRefund {

    @Test
    public void testRefundAmount() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PricingService pricingService = ctx.getBean(PricingService.class);
        System.out.println(pricingService.getRefundAmountForOrderId("FCVW1305220000536"));
    }

}
