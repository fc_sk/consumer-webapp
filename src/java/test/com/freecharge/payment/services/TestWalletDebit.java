package com.freecharge.payment.services;

import java.math.BigDecimal;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.util.Amount;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.WalletCreditRequest;

public class TestWalletDebit {

    @Test
    public void testDebit() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        WalletService walletService = ctx.getBean(WalletService.class);
        
        WalletCreditRequest cr = new WalletCreditRequest();
        cr.setCallerReference("23AABBFCCDDXXEE");
        cr.setFundSource("REFUND");
        cr.setMtxnId("");
        cr.setUserID(9);
        cr.setTransactionType("deposit");
        cr.setTxnAmount(new Amount(new BigDecimal(10)));
        
        walletService.credit(cr);
        
        ctx.close();
    }
}
