package com.freecharge.payment.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.StringUtils;

public class TestReclaim {

    @Test
    public void testOutputMoney() throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentTransactionService paymentTransactionService = ctx.getBean(PaymentTransactionService.class);

        File outFile = new File("/home/arun/devel/ops/refund/FailedToReclaimDetails.csv");
        outFile.createNewFile();
        PrintStream os = new PrintStream(outFile);

        BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/arun/devel/ops/refund/FailedToReclaimOrderIDs.csv"));
        String line;
        
        while ((line = bufferedReader.readLine()) != null) {
            String mtxnId = StringUtils.trim(line);
            List<PaymentTransaction> paymentTransactionList = paymentTransactionService.getPaymentTransactionByMerchantOrderId(mtxnId);
            
            for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                if (paymentTransaction.getIsSuccessful() && !paymentTransaction.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)){
                    os.println(paymentTransaction.getOrderId() + "," + paymentTransaction.getMerchantTxnId() + "," + paymentTransaction.getAmount());
                }
            }
        }
        
        os.close();
        bufferedReader.close();
    }

}
