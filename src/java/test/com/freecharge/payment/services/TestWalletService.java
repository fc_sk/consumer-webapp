package com.freecharge.payment.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.util.Amount;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.StringUtils;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletCreditRequest;

public class TestWalletService {

    @Test
    public void testWalletService() throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentTransactionService paymentTransactionService = ctx.getBean(PaymentTransactionService.class);
        
        BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/arun/devel/ops/refund/UniqToFix.csv"));
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            String mtxnId = StringUtils.trim(line);

            PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(mtxnId);

            PaymentRequestBusinessDO walletPayDO = new PaymentRequestBusinessDO();
            walletPayDO.setAmount(Double.toString(paymentTransaction.getAmount().doubleValue() - 30));
            walletPayDO.setOrderId(paymentTransaction.getOrderId());
            String walletReference = paymentTransaction.getMerchantTxnId();
            
            try {
//                paymentTransactionService.makeWalletPayment(walletPayDO, walletReference, paymentTransaction.getMerchantTxnId());
                System.out.println("Success for :[" + paymentTransaction.getMerchantTxnId() + "]");
            } catch (Exception e) {
                System.out.println("Failure for : [" + paymentTransaction.getMerchantTxnId() + "]");
            }

        }
        
        bufferedReader.close();
        ctx.close();
    }
    
    @Test
    public void testWalletMiscCredit() throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentTransactionService paymentTransactionService = ctx.getBean(PaymentTransactionService.class);
        WalletService walletService = ctx.getBean(WalletService.class);
        CartService cartService = ctx.getBean(CartService.class);
        UsersDAO usersDAO = ctx.getBean(UsersDAO.class);
        
        List<String> orderIdListToAdjust = Arrays.asList("");
        
        for (String orderIdToFix : orderIdListToAdjust) {
            adjustWalletCredit(paymentTransactionService, walletService, cartService, usersDAO, orderIdToFix);
        }
        
        ctx.close();
    }

    private void adjustWalletCredit(PaymentTransactionService paymentTransactionService, WalletService walletService, CartService cartService, UsersDAO usersDAO, String orderIdToAdjust) {
        List<PaymentTransaction> transactions = paymentTransactionService.getPaymentTransactionsByOrderId(orderIdToAdjust);
        for (PaymentTransaction paymentTransaction : transactions) {
            if (paymentTransaction.getIsSuccessful()) {
                if (PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE.equals(paymentTransaction.getPaymentGateway())) {

                    Users user = getUserForOrderID(paymentTransaction.getOrderId(), cartService, usersDAO);
                    String callerReference = paymentTransaction.getMerchantTxnId() + PaymentConstants.PAYMENT_ADJUSTMENT;
                    WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
                    walletCreditRequest.setUserID(user.getUserId());
                    walletCreditRequest.setTxnAmount(new Amount("30"));
                    walletCreditRequest.setMtxnId(paymentTransaction.getMerchantTxnId());
                    walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
                    walletCreditRequest.setCallerReference(callerReference);
                    walletCreditRequest.setFundSource(Wallet.FundSource.RECHARGE_FUND.toString());
                    walletService.credit(walletCreditRequest);
                
                }
            }
        }
    }
    
    protected Users getUserForOrderID(String orderId, CartService cartService, UsersDAO usersDAO) {
        CartBusinessDo cart = cartService.getCartByOrderId(orderId);
        return usersDAO.findByID(cart.getUserId());
    }
}