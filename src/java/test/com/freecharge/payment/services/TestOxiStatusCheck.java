package com.freecharge.payment.services;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.junit.Test;

public class TestOxiStatusCheck {

	@Test
	public void testOxiStatus() throws HttpException, IOException {
        Protocol.registerProtocol("https", new Protocol("https", new EasySSLProtocolSocketFactory(), 443));

		HttpClient client = new HttpClient();
        
        String statusCheckUrl = "https://oximall.com/EnquiryApi/default.aspx";
		PostMethod method = new PostMethod(statusCheckUrl);
		
        String merchantRefNo = "Checkstatus," + "null";

        method.setParameter("Transid", "0_1180849");
        method.setParameter("merchantrefno", merchantRefNo);
        method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");

        int statusCode = client.executeMethod(method);
        
        System.out.println(statusCode);
        
        System.out.println(method.getResponseBodyAsString());
	}

}
