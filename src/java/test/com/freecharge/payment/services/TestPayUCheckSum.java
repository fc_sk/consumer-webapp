package com.freecharge.payment.services;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.freecharge.payment.services.PayUGatewayHandler.RequestParameters;

public class TestPayUCheckSum {

    @Test
    public void testChecksum() throws Exception {
        Map<String, Object> paymentRequestMap = new HashMap<String, Object>();
        paymentRequestMap.put(RequestParameters.MERCHANT_ID, "C0Dr8m");
        paymentRequestMap.put(RequestParameters.PAYMENT_REFERENCE_ID, "FCVW1212070009171");
        paymentRequestMap.put(RequestParameters.AMOUNT, "110.0");
        paymentRequestMap.put(RequestParameters.PRODUCT_DESCRIPTION, "1");
        // No special characters allowed in the first name so getting rid of space.
        paymentRequestMap.put(RequestParameters.FIRST_NAME, "Arunkumar");
        paymentRequestMap.put(RequestParameters.CUSTOMER_EMAIL, "arunkumar.jadhav@gmail.com");
        paymentRequestMap.put(RequestParameters.API_VERSION, "1");
        String calculated = getRequestHash(paymentRequestMap);
        paymentRequestMap.put(RequestParameters.CHECKSUM, calculated);
  
        System.out.println(calculated);
        String expected = "417e0d0ff4ae3d843924d4deca31f76f23915d30585169037c63b26e60f2acc6b493c497925b3f61c6ebb6e2c8e935d570562755d58dc17cf700ade1fc78c6c9";

        System.out.println("Calculated:" + calculated);
        System.out.println("Expected:" + expected);
        
        Assert.assertEquals(expected, calculated);
        
        System.out.println(calculateHash("512345XXXXXX2346", "SHA-512"));
        System.out.println("766f0227cc4b4c5f773a04cb31d8d1c5be071dd8d08fe365ecf5e2e5c947546d");
    }
    
    /*
     * mihpayid:403993715507747260
mode:CC
status:success
unmappedstatus:captured
key:C0Dr8m
txnid:FCVW1212110009289
amount:110.00
discount:0.00
productinfo:1
firstname:Arunkumar
lastname:
address1:
address2:
city:
state:
country:
zipcode:
email:arunkumar.jadhav@gmail.com
phone:9342138044
udf1:
udf2:
udf3:
udf4:
udf5:
udf6:
udf7:
udf8:
udf9:
udf10:
hash:d003f10d0c0b77446a4d1c1f2c1776056f0b7e51e0b60a9c84217453dcc9b6d0533ee9f8701cce8de1a0b40eca1a8e86088e3bf0fbca01f538783bd1e8bbbeea
field1:2000022742
field2:234623431820
field3:20121211
field4:MC
field5:431820
field6:00
field7:0
field8:3DS
PG_TYPE:AXIS
bank_ref_num:2000022742
bankcode:CC
error:E000
cardnum:512345XXXXXX2346
cardhash:766f0227cc4b4c5f773a04cb31d8d1c5be071dd8d08fe365ecf5e2e5c947546d
     */
    
    private String getRequestHash(Map<String, Object> paymentRequestMap) throws Exception {
        /*
         * "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
         */
        
        String stringToHash = paymentRequestMap.get(RequestParameters.MERCHANT_ID) + "|"
                + paymentRequestMap.get(RequestParameters.PAYMENT_REFERENCE_ID) + "|"
                + paymentRequestMap.get(RequestParameters.AMOUNT) + "|"
                + paymentRequestMap.get(RequestParameters.PRODUCT_DESCRIPTION) + "|"
                + paymentRequestMap.get(RequestParameters.FIRST_NAME) + "|"
                + paymentRequestMap.get(RequestParameters.CUSTOMER_EMAIL) + "||||||||||";
        
        stringToHash = stringToHash + "|3sf0jURk";
        
        System.out.println(stringToHash);
                
        return calculateHash(stringToHash, "SHA-512");
    }
    
    private String calculateHash(String stringToHash, String hashType) throws Exception {
        byte[] hashseq = stringToHash.getBytes();
        
        StringBuffer hexString = new StringBuffer();
        
        MessageDigest algorithm = MessageDigest.getInstance(hashType);
        algorithm.reset();
        algorithm.update(hashseq);
        byte messageDigest[] = algorithm.digest();

        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xFF & messageDigest[i]);

            if (hex.length() == 1) {
                hexString.append("0");
            }

            hexString.append(hex);
        }

        return hexString.toString();
    }
}
