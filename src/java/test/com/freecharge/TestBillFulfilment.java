package com.freecharge;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.comm.fulfillment.FulfillmentService;

public class TestBillFulfilment {
	@Test
	public void testFulfilment(){
		ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
		FulfillmentService activity = ctx.getBean(FulfillmentService.class);
		activity.doFulfillment("FCMW13121620448817", false, false, false);
	}
}
	