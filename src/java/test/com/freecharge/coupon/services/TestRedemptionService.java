package com.freecharge.coupon.services;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.api.coupon.service.CouponRedemptionRequest;
import com.freecharge.api.coupon.service.CouponRedemptionResponse;
import com.freecharge.api.coupon.service.CouponRedemptionResponse.RedemptionStatus;
import com.freecharge.api.coupon.service.CouponRedemptionService;
import com.freecharge.api.coupon.service.RedemptionErrorInfo.ErrorCode;

public class TestRedemptionService {
    ClassPathXmlApplicationContext ctx;    
    @Test
    public void testRedeemedCode() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse invalidMerchantIdResponse = invalidMerchantId(rService);
        
        Assert.assertEquals(RedemptionStatus.Failure, invalidMerchantIdResponse.getRedemptionStatus());
        Assert.assertEquals(ErrorCode.RedeemedCode, invalidMerchantIdResponse.getErrorInfo().getErrorCode());
    }
    
    @Test
    public void testInitializedCode() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse successResponse = initializedValidCode(rService);

        Assert.assertEquals(RedemptionStatus.Success, successResponse.getRedemptionStatus());
    }
    
    @Test
    public void testRedemptionSuccess() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse successResponse = redemptionSuccess(rService);

        Assert.assertEquals(RedemptionStatus.Success, successResponse.getRedemptionStatus());
    }

    @Test
    public void testOneMoreRedeemedCode() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse redeemedCodeResponse = redeemedCode(rService);

        Assert.assertEquals(RedemptionStatus.Failure, redeemedCodeResponse.getRedemptionStatus());
        Assert.assertEquals(ErrorCode.RedeemedCode, redeemedCodeResponse.getErrorInfo().getErrorCode());
    }

    @Test
    public void testAvailableExpiredCode() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse redeemedCodeResponse = availableExpiredCode(rService);

        Assert.assertEquals(RedemptionStatus.Failure, redeemedCodeResponse.getRedemptionStatus());
        Assert.assertEquals(ErrorCode.RedeemedCode, redeemedCodeResponse.getErrorInfo().getErrorCode());
    }

    @Test
    public void testInvalidCode() {
        CouponRedemptionService rService = ctx.getBean(CouponRedemptionService.class);
        
        CouponRedemptionResponse invalidCodeResponse = invalidCode(rService);

        Assert.assertEquals(RedemptionStatus.Failure, invalidCodeResponse.getRedemptionStatus());
        Assert.assertEquals(ErrorCode.InvalidCode, invalidCodeResponse.getErrorInfo().getErrorCode());
    }

    private CouponRedemptionResponse invalidMerchantId(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC11223344");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCDF");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }
    
    private CouponRedemptionResponse initializedValidCode(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC11223344");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCDD");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }

    private CouponRedemptionResponse redemptionSuccess(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC11223344");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCDD");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }
    
    private CouponRedemptionResponse redeemedCode(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC11223355");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCDD");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }
    
    private CouponRedemptionResponse availableExpiredCode(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC11223366");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCFF");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }
    
    private CouponRedemptionResponse invalidCode(CouponRedemptionService rService) {
        CouponRedemptionRequest request = new CouponRedemptionRequest();
        request.setCouponCode("FC21223366");
        request.setState("Gujarat");
        request.setMerchantId("xyzab");
        request.setRequestId("AABBCCFF");
        
        CouponRedemptionResponse response = rService.redeemCoupon(request);
        
        return response;
    }
    
    @Before
    public void setup() {
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
    }
    
    @After
    public void tearDown() {
        ctx.close();
    }
}
