package com.freecharge;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.dop.framework.DopConsumer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
@ActiveProfiles("dev")
public class TestHyperLocalCouponActivity implements ApplicationContextAware {
	private ApplicationContext context;
	private DopConsumer hyperLocalCouponActivity;
	
	@Before
	public void init(){
		hyperLocalCouponActivity = (DopConsumer) context.getBean("hyperLocalCouponActivity");
	}
	
	@Test
	public void testHCouponFulfilment(){
		hyperLocalCouponActivity.consume("{'entryTime':'1422440471079','orderId':'FCHA1501290000391'}");
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		context = arg0;
	}
}
	