package com.freecharge.testforkjoin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Hours;
import org.joda.time.Interval;
import org.junit.Test;

import com.freecharge.payment.autorefund.RefundUtil;

public class TestGetHours {

    @Test
    public void test() throws Exception {
        Date fromDate = RefundUtil.getDate("2013-08-01 13:00:00");
        Date toDate = RefundUtil.getDate("2013-08-06 20:00:00");
        
        System.out.println(getHoursBetween(fromDate, toDate));
        
        List<Interval> durationList = splitDuration(fromDate, toDate);
        
        for (Interval duration : durationList) {
            System.out.println(duration.getStart() + " - " + duration.getEnd());
        }
        
    }
    
    private int getHoursBetween(Date startDate, Date endDate) {
        DateTime st = new DateTime(startDate.getTime());
        DateTime et = new DateTime(endDate.getTime());
        
        return Hours.hoursBetween(st, et).getHours();
    }
    
    private List<Interval> splitDuration(Date startDate, Date endDate) {
        DateTime start = new DateTime(startDate.getTime());
        DateTime end = new DateTime(endDate.getTime());
        
        if (end.isBefore(start)) {
            throw new IllegalArgumentException("End is before start. Start [" + start + "]. End [" + end + "]");
        }
        
        long startMillis = start.getMillis();
        long endMillis = start.getMillis();
        
        List<Interval> list = new ArrayList<Interval>();

        while(true) {
            endMillis = startMillis + DateTimeConstants.MILLIS_PER_HOUR * 2;
            
            final Interval currentInterval = new Interval(startMillis, endMillis);
            
            if (currentInterval.getStart().isAfter(end)) {
                break;
            }
            
            if (currentInterval.getEnd().isAfter(end)) {
                if (startMillis < end.getMillis()) {
                    list.add(new Interval(startMillis, end.getMillis()));
                }
                
                break;
            } else {
                list.add(currentInterval);
            }
            
            startMillis = endMillis;
        }

        return list;
    }
}
