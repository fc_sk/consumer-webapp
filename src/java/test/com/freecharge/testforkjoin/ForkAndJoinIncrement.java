package com.freecharge.testforkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class ForkAndJoinIncrement {
    private static final ForkJoinPool fjPool = new ForkJoinPool(1);
    
    
    private static class IncrementTask extends RecursiveAction {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        
        private final int[] toIncrement;
        private final int high;
        private final int low;
        
        private final int THRESHOLD = 100;
        
        public IncrementTask(int[] toIncrement, int low, int high) {
            this.toIncrement = toIncrement;
            this.low = low;
            this.high = high;
        }

        @Override
        protected void compute() {
            if (this.high - this.low < THRESHOLD) {
                for (int current = low; current < high; current++) {
                    toIncrement[current] += 10;
                }
            } else {
                int mid = (low + high) >>> 1;
                invokeAll(new IncrementTask(toIncrement, low, mid), new IncrementTask(toIncrement, mid, high));
            }
        }
    }
    
    public static void main(String[] args) {
        int[] sampleList = new int[200];
        
        for (int current = 0; current < sampleList.length; current++) {
            sampleList[current] = current;
        }
        
        
        fjPool.invoke(new IncrementTask(sampleList, 0, sampleList.length));
        
        for (Integer sampleElement : sampleList) {
            System.out.println(sampleElement);
        }
    }
}
