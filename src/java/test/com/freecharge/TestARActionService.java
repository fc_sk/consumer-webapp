package com.freecharge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.ardeal.ARDealConstants;
import com.freecharge.ardeal.ARDealDBDao;
import com.freecharge.ardeal.action.promooptin.PromoOptInARAction;
import com.freecharge.ardeal.entity.ARActionEntity;
import com.freecharge.ardeal.entity.ARDealEntity;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.growthevent.service.UserServiceProxy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
@ActiveProfiles("dev")
public class TestARActionService {
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private PromoOptInARAction actionService;
	
	@Autowired
	private ARDealDBDao ardealDao;
	
	@Autowired
	private UsersDAO userDao;
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	@Test
	public void testOptInAction(){
		SpringBeanLocator.getInstance().setApplicationContext(applicationContext);
		ARDealEntity arDealEntity = this.ardealDao.getARDealById(3);
		ARActionEntity arActionEntity = this.ardealDao.getARActionById(arDealEntity.getFkActionId());
		Users user = userServiceProxy.getUserByEmailId("shirish.surti@freecharge.com");
		actionService.recordActivity(arActionEntity,"testsession","testlid", user, 1l, ARDealConstants.OPTIN);
		System.out.println("Done");
	}
	
	@Test
	public void testOptInActionForOrderId(){
		SpringBeanLocator.getInstance().setApplicationContext(applicationContext);
		actionService.triggerOptinActivityForOrder("FCVW1307230000001", 3l);
	}
}
	