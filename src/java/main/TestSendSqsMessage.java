import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.freecharge.common.comm.fulfillment.FulfillmentService;

@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan(basePackages = {"com.freecharge.common"})
@ActiveProfiles("dev")
public class TestSendSqsMessage {

	@Autowired
	private FulfillmentService fulfillmentService;
    @Test
    public void testSendSqsMessage() {
    	String orderId = "FCMW15062248376970";
        fulfillmentService.storeMetric(orderId);
    }
}
