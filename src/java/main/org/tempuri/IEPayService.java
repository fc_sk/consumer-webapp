/**
 * IEPayService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IEPayService extends java.rmi.Remote {
    public java.lang.String sendEuroVasRequest(java.lang.String request) throws java.rmi.RemoteException;
}
