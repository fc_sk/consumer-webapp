package org.tempuri;

public class IEPayServiceProxy implements org.tempuri.IEPayService {
  private String _endpoint = null;
  private org.tempuri.IEPayService iEPayService = null;
  
  public IEPayServiceProxy() {
    _initIEPayServiceProxy();
  }
  
  public IEPayServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIEPayServiceProxy();
  }
  
  private void _initIEPayServiceProxy() {
    try {
      iEPayService = (new org.tempuri.Service1Locator()).getBasicHttpBinding_IEPayService();
      if (iEPayService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iEPayService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iEPayService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iEPayService != null)
      ((javax.xml.rpc.Stub)iEPayService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.IEPayService getIEPayService() {
    if (iEPayService == null)
      _initIEPayServiceProxy();
    return iEPayService;
  }
  
  public java.lang.String sendEuroVasRequest(java.lang.String request) throws java.rmi.RemoteException{
    if (iEPayService == null)
      _initIEPayServiceProxy();
    return iEPayService.sendEuroVasRequest(request);
  }
  
  
}