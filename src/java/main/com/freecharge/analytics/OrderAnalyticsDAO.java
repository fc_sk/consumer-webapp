package com.freecharge.analytics;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public class OrderAnalyticsDAO {

	NamedParameterJdbcTemplate jt;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jt = new NamedParameterJdbcTemplate(dataSource);
	}

//	public static final String SQL_CART_ITEMS_BY_ORDER_ID =
//			"SELECT DISTINCT" +
//			"  pm.product_name, thp.operator_name, citems.entity_id, citems.display_label, citems.price AS unit_price," +
//			"  cs.campaign_name, cs.coupon_type, tcs.quantity AS coupon_count" +
//			" FROM cart c" +
//			"  JOIN cart_items citems ON citems.fk_cart_id=c.cart_id" +
//			"  JOIN product_master pm ON pm.product_master_id=citems.fl_product_master_id" +
//			"  JOIN order_id oid      ON oid.lookup_id=c.lookup_id" +
//			"  JOIN txn_home_page thp ON thp.lookup_id=c.lookup_id" +
//			"  LEFT JOIN txn_cross_sell tcs  ON tcs.txn_home_page_id = thp.id AND product_name='Coupons'" +
//			"  LEFT JOIN coupon_store cs     ON cs.coupon_store_Id = tcs.coupon_id" + // citems.fk_item_id
//			" WHERE (tcs.coupon_id IS NULL OR tcs.coupon_id <> 0) AND" +
//			"  (tcs.is_deleted IS NULL OR tcs.is_deleted = 0) AND" +
//			"  (NOT citems.is_deleted) AND oid.order_id = :order_id";
	
	public static final String SQL_CART_ITEMS_BY_ORDER_ID =
			"SELECT DISTINCT" +
			"  pm.product_name, thp.operator_name, citems.entity_id, citems.display_label, citems.price AS unit_price," +
			"  tcs.coupon_id AS coupon_id, tcs.quantity AS coupon_count" +
			" FROM cart c" +
			"  JOIN cart_items citems ON citems.fk_cart_id=c.cart_id" +
			"  JOIN product_master pm ON pm.product_master_id=citems.fl_product_master_id" +
			"  JOIN order_id oid      ON oid.lookup_id=c.lookup_id" +
			"  JOIN txn_home_page thp ON thp.lookup_id=c.lookup_id" +
			"  LEFT JOIN txn_cross_sell tcs  ON tcs.txn_home_page_id = thp.id AND product_name='Coupons'" +
			" WHERE (tcs.coupon_id IS NULL OR tcs.coupon_id <> 0) AND" +
			"  (tcs.is_deleted IS NULL OR tcs.is_deleted = 0) AND" +
			"  (NOT citems.is_deleted) AND oid.order_id = :order_id";

	public List<CartItemDetails> findCartItemDetailsByOrderID(String orderID) {
		List<CartItemDetails> cartItemsDetails = new ArrayList<CartItemDetails>();
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//		paramMap.put("order_id", orderID);
//		
//		List<Map<String,Object>> results=jt.queryForList(SQL_CART_ITEMS_BY_ORDER_ID, paramMap);
//		List<Integer> couponStoreIds= new ArrayList<Integer>();
//		for(Map<String,Object> result:results){
//			if(result.get("coupon_id")!=null){
//				couponStoreIds.add((Integer)result.get("coupon_id"));
//			}
//		}
//		Map<Integer,Map<String,String>> couponMapResult = new HashMap<Integer,Map<String,String>>();
//		if(couponStoreIds!=null && couponStoreIds.size()!=0) {
//			couponMapResult=couponStoreReadDAO.getCouponIdVsCouponMap(couponStoreIds);
//		}
//		for(Map<String,Object> result:results) {
//			CartItemDetails cid = new CartItemDetails();
//			cartItemsDetails.add(cid);
//			String productName = (String)result.get("product_name");
//			cid.setProductName(productName);
//			String sku = productName;
//			// display label
//			String displayLabel = (String) result.get("display_label");
//			cid.setProductName(productName);
//			int quantity = 1;
//			double unitPrice = 0;
//			if (displayLabel==null) { // displayLabel is NULL for coupons
//				if(result.get("coupon_id")!=null){
//					Integer couponId = (Integer)result.get("coupon_id");
//					Map<String,String> couponResult = couponMapResult.get(couponId);
//					if(couponResult!=null){
//						displayLabel = couponResult.get("campaign_name")+ "-" + couponResult.get("coupon_type");
//						sku += "-" + displayLabel;
//					}
//					quantity = (Integer)result.get("coupon_count");
//				}
//			} else {
//                final String entityID = "" + (String)result.get("entity_id");
//				sku += "-" + (entityID.equalsIgnoreCase("Crosssell")?
//                  displayLabel: (String)result.get("operator_name"));
//				unitPrice = (Double)result.get("unit_price");
//			}
//			cid.setDisplayLabel(displayLabel);
//			// quantity
//			cid.setQuantity(quantity);
//			// unit price
//			cid.setUnitPrice(unitPrice);
//			// stock keeping unit
//			sku += "-" + unitPrice;
//			cid.setStockKeepingUnit(sku);
//
//		}
		return cartItemsDetails;
		
//		return jt.query(SQL_CART_ITEMS_BY_ORDER_ID, paramMap, new RowMapper<CartItemDetails>() {
//			@Override
//			public CartItemDetails mapRow(ResultSet rs, int rowNum)
//					throws SQLException {
//				CartItemDetails cid = new CartItemDetails();
//				// product name
//				String productName = rs.getString("product_name");
//				cid.setProductName(productName);
//				String sku = productName;
//				// display label
//				String displayLabel = rs.getString("display_label");
//				cid.setProductName(productName);
//				int quantity = 1;
//				double unitPrice = 0;
//				if (displayLabel==null) { // displayLabel is NULL for coupons
//					displayLabel = rs.getString("campaign_name") + "-" + rs.getString("coupon_type");
//					sku += "-" + displayLabel;
//					quantity = rs.getInt("coupon_count");
//				} else {
//                    final String entityID = "" + rs.getString("entity_id");
//					sku += "-" + (entityID.equalsIgnoreCase("Crosssell")?
//                      displayLabel: rs.getString("operator_name"));
//					unitPrice = rs.getDouble("unit_price");
//				}
//				cid.setDisplayLabel(displayLabel);
//				// quantity
//				cid.setQuantity(quantity);
//				// unit price
//				cid.setUnitPrice(unitPrice);
//				// stock keeping unit
//				sku += "-" + unitPrice;
//				cid.setStockKeepingUnit(sku);
//
//				return cid;
//			}
//		});
	}

	public static final String SQL_CITY_BY_ORDER_ID =
			"SELECT IFNULL((" +
			"SELECT distinct(city_name)" +
			" FROM city_master cm" +
			"  JOIN txn_fulfilment tf ON cm.city_master_id = tf.delivery_city" +
			"  JOIN txn_home_page thp ON tf.txn_home_page_id = thp.id" +
			"  JOIN order_id oid      ON thp.lookup_id = oid.lookup_id" +
			" WHERE oid.order_id = :order_id)," +
			" 'UNKNOWN')";


	public String findCityByOrderID(String orderID) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("order_id", orderID);
		return jt.queryForObject(SQL_CITY_BY_ORDER_ID, paramMap, String.class);
	}

}
