package com.freecharge.analytics;

import com.freecharge.common.framework.basedo.AbstractDO;

public class CartItemDetails extends AbstractDO {

	private String stockKeepingUnit;
	private String productName;
	private String displayLabel;
	private double unitPrice;
	private int    quantity;


	public String getStockKeepingUnit() {
		return stockKeepingUnit;
	}
	public void setStockKeepingUnit(String stockKeepingUnit) {
		this.stockKeepingUnit = stockKeepingUnit;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDisplayLabel() {
		return displayLabel;
	}
	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
