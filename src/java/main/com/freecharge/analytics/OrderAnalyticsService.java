package com.freecharge.analytics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderAnalyticsService {
    @Autowired
    OrderAnalyticsDAO analyticsDAO;

    public OrderDetails findOrderDetails(String orderID, final Integer userId) {
        //Coupon and Freefund details
        List<CartItemDetails> cids = analyticsDAO.findCartItemDetailsByOrderID(orderID);
        List<CartItemDetails> result = new ArrayList<CartItemDetails>();
        OrderDetails od = new OrderDetails();
        for (CartItemDetails each : cids) {
            if ("Handling Charges".equalsIgnoreCase(each.getDisplayLabel())) {
                od.setShippingCharges(each.getUnitPrice() * each.getQuantity());
            } else {
                result.add(each);
            }
        }
        od.setCartItemList(result);
        od.setCity(analyticsDAO.findCityByOrderID(orderID));
        return od;
    }

}
