package com.freecharge.analytics;

import java.util.List;

import com.freecharge.common.framework.basedo.AbstractDO;

public class OrderDetails extends AbstractDO {

	private String city;

	private List<CartItemDetails> cartItemList;

	private double shippingCharges;

	public OrderDetails() {
		// default constructor is required for a JavaBean
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<CartItemDetails> getCartItemList() {
		return cartItemList;
	}

	public void setCartItemList(List<CartItemDetails> cartItemList) {
		this.cartItemList = cartItemList;
	}

	public double getShippingCharges() {
		return shippingCharges;
	}

	public void setShippingCharges(double shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

}
