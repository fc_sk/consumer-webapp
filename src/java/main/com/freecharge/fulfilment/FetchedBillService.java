package com.freecharge.fulfilment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.rest.common.FetchedBill;

@Service
public class FetchedBillService {
	private final Logger          logger = Logger.getLogger(getClass());
	
	@Autowired
	private FetchedBillDao fetchedBillDao;
	
	public Boolean insertFetchedBill(FetchedBill fetchedBill) {
		return fetchedBillDao.insertFetchedBill(fetchedBill);
	}
	
	public FetchedBill getFetchedBillBySessionId(String sessionId) {
		logger.info("request comes for fetched bill for sessionId: " + sessionId);
		return fetchedBillDao.getFetchedBillBySessionId(sessionId);
	}
	
	public FetchedBill getFetchedBill(String sessionId, Integer billerId, String accountId) throws Exception {
		logger.info("request comes for fetched bill for sessionId: " + sessionId + "billerId : " + billerId + " accountId: " + accountId);
		try {
			return fetchedBillDao.getFetchedBill(sessionId, billerId, accountId);
		} catch (Exception e) {
			throw e;
		}
	}

}
