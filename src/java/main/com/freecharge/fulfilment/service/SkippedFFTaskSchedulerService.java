package com.freecharge.fulfilment.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dao.PostFulfillmentTaskDao;
import com.freecharge.web.webdo.PostFFTaskModel;

@Service
public class SkippedFFTaskSchedulerService {

	private static final Logger LOGGER = LoggingFactory.getLogger(SkippedFFTaskSchedulerService.class);
	
    private static final int FIFTEEN_MINUTES = 900;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private PostFulfillmentTaskDao postFulfillmentTaskDao;
    
    // In PropertyUtil class
    @Value("${skipped.ff.retry.count}")
    private int skippedFFRetryCount;

	public void pushSkippedFFRecords(Integer m1, Integer m2) {
		Date now = new Date();
		Date to = DateUtils.addMinutes(now, -m1);
		Date from = DateUtils.addMinutes(to, -m2);
		LOGGER.info("checking recharge status from " + from + " to " + to);

		processSkippedFFRecords(from, to, "Pending." + m2.toString());

	}

	public void processSkippedFFRecords(Date from, Date to, String string) {
		List<PostFFTaskModel> postFFTaskList = postFulfillmentTaskDao.getSkippedTasksInDateRange(from, to,skippedFFRetryCount);
        if(CollectionUtils.isEmpty(postFFTaskList)) {
        	LOGGER.info("Empty task List");
        	return;
        }
        //metricsClient.recordCount("Recharge", metricName, orderIdList.size());        
        for (PostFFTaskModel postFFTask : postFFTaskList) {
				kestrelWrapper.enqueueForFFSkippedTask(postFFTask.getOrderId());
        }

		
	}
	
	public void enqueue(String orderId) {
		LOGGER.info("enqueuing : "+orderId);
		kestrelWrapper.enqueueForFFSkippedTask(orderId);
		LOGGER.info("enqueued : "+orderId);


	}


}
