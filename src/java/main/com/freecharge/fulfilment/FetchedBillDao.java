package com.freecharge.fulfilment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.rest.common.FetchedBill;

@Component
public class FetchedBillDao {
	private final Logger          logger = Logger.getLogger(getClass());
	
	private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert sji;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        sji = new SimpleJdbcInsert(dataSource).withTableName("fetched_bill")
                .usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }
    
    public Boolean insertFetchedBill(FetchedBill fetchedBill) {
    	if(fetchedBill == null || fetchedBill.getSessionId().equals("-1")){
    		logger.error("Invalid entry cannot be made into DB");
    		return false;
    	}
    	
        try {
			Map<String, Object> params = new HashMap<>();
			params.put("session_id", fetchedBill.getSessionId());
			params.put("biller_id", fetchedBill.getBillerId());
			params.put("bill_amount", fetchedBill.getBillAmount());
			params.put("account_id", fetchedBill.getAccountId());
			
			sji.execute(params);
		} catch (Exception e) {
			logger.error("Exception caught while inserting fetchedbill details",e );
			return false;
		}
        return true;
    }
    
    public FetchedBill getFetchedBill(String sessionId, Integer billerId, String accountId) throws Exception{
        String sql = "select id,session_id, biller_id, bill_amount, account_id from fetched_bill "
        		+ "where session_id = :sessionId and biller_id = :billerId and account_id = :accountId order by n_created desc";
        Map<String, Object> params = new HashMap<>();
        params.put("sessionId", sessionId);
        params.put("billerId", billerId);
        params.put("accountId", accountId);
        try {
            List<FetchedBill> fetchedBillList = this.jt.query(sql, params, new RowMapper<FetchedBill>() {
            	@Override
        		public FetchedBill mapRow(ResultSet rs, int rowNum) throws SQLException{
            		FetchedBill fetchedBill = new FetchedBill();
        			fetchedBill.setId(rs.getInt("id"));
        			fetchedBill.setSessionId(rs.getString("session_id"));
        			fetchedBill.setBillerId(rs.getInt("biller_id"));
        			fetchedBill.setBillAmount(rs.getFloat("bill_amount"));
        			fetchedBill.setAccountId(rs.getString("account_id"));
        			return fetchedBill;
            	}
            });
            if(fetchedBillList == null || fetchedBillList.size() <1) {
            	logger.info("Nothing found for sessionId: " + sessionId + " billerId :"
            		+ billerId + " accountId: "+ accountId);
            }else {
            	return fetchedBillList.get(0);
            }
        }catch (Exception e){
            logger.error("Exception caught while getting bill amount for sessionId: " + sessionId + " billerId :"
            		+ billerId + " accountId: "+ accountId,e);
            throw new Exception("Exception caught whlie quering for fetchedBill");
        }
        return null;
    }
    
    public FetchedBill getFetchedBillBySessionId(String sessionId) {
        String sql = "select id,session_id, biller_id, bill_amount, account_id from fetched_bill "
        		+ "where session_id = :sessionId ";
        Map<String, Object> params = new HashMap<>();
        params.put("sessionId", sessionId);
        try {
            List<FetchedBill> fetchedBillList = this.jt.query(sql, params, new RowMapper<FetchedBill>() {
            	@Override
        		public FetchedBill mapRow(ResultSet rs, int rowNum) throws SQLException{
            		FetchedBill fetchedBill = new FetchedBill();
        			fetchedBill.setId(rs.getInt("id"));
        			fetchedBill.setSessionId(rs.getString("session_id"));
        			fetchedBill.setBillerId(rs.getInt("biller_id"));
        			fetchedBill.setBillAmount(rs.getFloat("bill_amount"));
        			fetchedBill.setAccountId(rs.getString("account_id"));
        			return fetchedBill;
            	}
            });
            if(fetchedBillList == null || fetchedBillList.size() <1) {
            	logger.error("Nothing found for sessionId: " + sessionId );
            }else {
            	return fetchedBillList.get(0);
            }
        }catch (Exception e){
            logger.error("Exception caught while getting bill amount for sessionId: " + sessionId,e);
        }
        return null;
    }
}
