package com.freecharge.fulfilment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.web.util.ViewConstants;

import flexjson.JSONSerializer;

@Controller
@RequestMapping("/admin/*")
public class FulfilmentAdminController extends BaseAdminController {
    private Logger logger = LoggingFactory.getLogger(getClass());
    public static final String ADMIN_FULFILMENT_LINK = "/admin/fulfilment";

    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private FulfillmentService   fulfillmentService;

    @Autowired
    private AdminHelper              adminHelper;
    
    private JSONSerializer jsonSerializer = new JSONSerializer();

    @RequestMapping(value = "fulfilment", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView viewAdminFulfilment(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FULFILMENT)) {
            get403HtmlResponse(response);
            return null;
        }
        return new ModelAndView(ViewConstants.ADMIN_FULFILMENT_VIEW);
    }

    @RequestMapping(value = "fulfilment", method = RequestMethod.POST)
    @NoLogin
    public ModelAndView doFulfilment(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FULFILMENT)) {
            get403HtmlResponse(response);
            return null;
        }
        String orderId = request.getParameter("orderId").trim();
        String message = "";
        if (orderId != null && !orderId.isEmpty()) {
            try {
                enqueueOrderIdForFulfilment(orderId);
                message = "Successfully enqueued fulfilment request for order id: " + orderId;
            } catch (Exception e) {
                logger.error("Error enqueuing fulfilment request for order id: " + orderId, e);
                message = "Fulfilment request could not be placed. Please contact tech team.";
            }
        } else {
            message = "Invalid Order Id.";
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("message", message);
        return new ModelAndView(ViewConstants.ADMIN_FULFILMENT_VIEW, params);
    }

    private void enqueueOrderIdForFulfilment(String orderId) {
        Map<String, String> fulfillmentRequest = new HashMap<>();
        fulfillmentRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
        fulfillmentRequest.put(PaymentConstants.SHOULD_SEND_SMS_KEY, Boolean.TRUE.toString());
        fulfillmentRequest.put(FCConstants.IS_RETRIGGER, Boolean.TRUE.toString());
        if (orderId.startsWith(PaymentConstants.BILL_ORDERID_START_STRING)) {
            fulfillmentService.doFulfillment(orderId, true, true, false);
        } else {
            kestrelWrapper.enqueueFulfillment(fulfillmentRequest);

        }
    }

    @RequestMapping(value = "fulfilmentmultiple", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView viewAdminFulfilmentMultiple(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FULFILMENT)) {
            get403HtmlResponse(response);
            return null;
        }
        return new ModelAndView(ViewConstants.ADMIN_FULFILMENT_VIEW);
    }

    @RequestMapping(value = "fulfilmentmultiple", method = RequestMethod.POST)
    @NoLogin
    public ModelAndView doFulfilmentMultiple(@RequestParam("uploadFile") MultipartFile file,
            @RequestParam(required = false, value = "ignoreFirst") boolean ignoreFirst,
            @ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        BufferedReader br = new BufferedReader(new StringReader(new String(file.getBytes())));
        String orderId;
        int n = 0;
        List<String> orderIdList = new ArrayList<String>();
        while ((orderId = br.readLine()) != null) {
            n++;
            if (!(n == 1 && ignoreFirst)) {
                try {
                    logger.info("Started processing fulfilment from admin-tool of orderid :" + orderId);
                    enqueueOrderIdForFulfilment(orderId);
                    orderIdList.add(orderId);
                    logger.info("Successfully finished fulfilment from admin-tool of orderid :" + orderId);
                } catch (Exception ex) {
                    logger.error("Exception while processing fulfilment process from admin tool, orderId : " + orderId, ex);
                }
            }
        }
        if (n == 0) {
            throw new IllegalArgumentException("Please upload a non-empty file");
        }
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put("multiplemessage", orderIdList);
        /*Sending email with successfully processed orderids*/
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
        resultObjectMap.put(dateFormat.format(new Date()), orderIdList);
        adminHelper.sendEmailOnAdminToolAction(resultObjectMap, "Fulfilment tool", getCurrentAdminUser(request), FCConstants.ADMIN_TOOL_ACTION_ALERT);
        return new ModelAndView(ViewConstants.ADMIN_FULFILMENT_VIEW, params);
    }
}
