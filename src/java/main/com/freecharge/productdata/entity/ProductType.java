package com.freecharge.productdata.entity;

public enum ProductType {
	MOBILE("Mobile"), DTH("DTH"), DATACARD("Data Card") ;
	
	private String name;
	
	private ProductType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
