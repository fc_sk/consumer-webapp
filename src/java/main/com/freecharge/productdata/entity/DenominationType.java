package com.freecharge.productdata.entity;

public enum DenominationType {
	
	
	FIXED_DENOMINATION("Fixed"), VARIABLE_DENOMINATION("Variable");
	
	private String name;
	
	private DenominationType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
