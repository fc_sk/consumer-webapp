package com.freecharge.productdata.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.framework.basedo.AbstractDO;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 29/9/12
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class RechargePlan extends AbstractDO implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    public static final String RECHARGE_PLAN_ID_DB_COLUMN = "recharge_plan_id";
    public static final String RECHARGE_PLAN_NAME_COLUMN = "name";
    public static final String PRODUCT_TYPE_COLUMN = "fk_product_master_id";
    public static final String DENOMINATION_TYPE_COLUMN = "denomination_type";
    public static final String FK_OPERATOR_MASTER_ID_DB_COLUMN = "fk_operator_master_id";
    public static final String FK_CIRCLE_MASTER_ID_DB_COLUMN = "fk_circle_master_id";
    public static final String AMOUNT_DB_COLUMN = "amount";
    public static final String MIN_AMOUNT_DB_COLUMN = "min_amount";
    public static final String MAX_AMOUNT_DB_COLUMN = "max_amount";
    public static final String TALKTIME_DB_COLUMN = "talktime";
    public static final String VALIDITY_DB_COLUMN = "validity";
    public static final String DESCRIPTION_DB_COLUMN = "description";
    public static final String STATUS_DB_COLUMN = "status";
    public static final String IS_STICKY_DB_COLUMN = "is_sticky";
    public static final String IREFF_ID_DB_COLUMN = "ireff_id";
    public static final String DATAWEAVE_ID_DB_COLUMN = "dataweave_id";
    public static final String IREFF_CREATED_TIME = "created_time";
    
    private int rechargePlanId;
    private String name;
    private int productType;
    private String denominationType;
    private int operatorMasterId;
    private int circleMasterId;
    private BigDecimal amount;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private BigDecimal talktime;
    private String validity;
    private String description;
    private boolean status = true;
    private boolean sticky = false;
    private String ireffID;
    private boolean isRecommended = false;
    private Timestamp createdTime;
    
    
    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public int getRechargePlanId() {
        return rechargePlanId;
    }

    public void setRechargePlanId(int rechargePlanId) {
        this.rechargePlanId = rechargePlanId;
    }

    public int getOperatorMasterId() {
        return operatorMasterId;
    }

    public void setOperatorMasterId(int operatorMasterId) {
        this.operatorMasterId = operatorMasterId;
    }

    public int getCircleMasterId() {
        return circleMasterId;
    }

    public void setCircleMasterId(int circleMasterId) {
        this.circleMasterId = circleMasterId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProductType() {
		return productType;
	}

	public void setProductType(int product_type) {
		this.productType = product_type;
	}

	public String getDenominationType() {
		return denominationType;
	}

	public void setDenominationType(String denomination_type) {
		this.denominationType = denomination_type;
	}

	public BigDecimal getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal min_amount) {
		this.minAmount = min_amount;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal max_amount) {
		this.maxAmount = max_amount;
	}

	public BigDecimal getTalktime() {
		return talktime;
	}

	public void setTalktime(BigDecimal talktime) {
		this.talktime = talktime;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isSticky() {
		return sticky;
	}

	public void setSticky(boolean sticky) {
		this.sticky = sticky;
	}

	public String getIreffID() {
		return ireffID;
	}

	public void setIreffID(String ireffID) {
		this.ireffID = ireffID;
	}
	
	public boolean getIsRecommended() {
		return isRecommended;
	}

	public void setIsRecommended(boolean isRecommended) {
		this.isRecommended = isRecommended;
	}
	
	public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(FK_CIRCLE_MASTER_ID_DB_COLUMN, getCircleMasterId());
        mapSqlParameterSource.addValue(FK_OPERATOR_MASTER_ID_DB_COLUMN, getOperatorMasterId());
        mapSqlParameterSource.addValue(AMOUNT_DB_COLUMN, amount);
        mapSqlParameterSource.addValue(RECHARGE_PLAN_NAME_COLUMN, name);
        mapSqlParameterSource.addValue(PRODUCT_TYPE_COLUMN, productType);
        mapSqlParameterSource.addValue(DENOMINATION_TYPE_COLUMN, denominationType);
        mapSqlParameterSource.addValue(MIN_AMOUNT_DB_COLUMN, minAmount);
        mapSqlParameterSource.addValue(MAX_AMOUNT_DB_COLUMN, maxAmount);
        mapSqlParameterSource.addValue(TALKTIME_DB_COLUMN, talktime);
        mapSqlParameterSource.addValue(VALIDITY_DB_COLUMN, validity);
        mapSqlParameterSource.addValue(DESCRIPTION_DB_COLUMN, description);
        mapSqlParameterSource.addValue(STATUS_DB_COLUMN, status);
        mapSqlParameterSource.addValue(IS_STICKY_DB_COLUMN, sticky);
        mapSqlParameterSource.addValue(IREFF_ID_DB_COLUMN, ireffID);
        mapSqlParameterSource.addValue(DATAWEAVE_ID_DB_COLUMN, isRecommended);
        mapSqlParameterSource.addValue(IREFF_CREATED_TIME, createdTime);
        return mapSqlParameterSource;
    }
	
	public MapSqlParameterSource getMapSqlParameterSourceForUpdate() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(RECHARGE_PLAN_ID_DB_COLUMN, rechargePlanId);
        mapSqlParameterSource.addValue(FK_CIRCLE_MASTER_ID_DB_COLUMN, circleMasterId);
        mapSqlParameterSource.addValue(FK_OPERATOR_MASTER_ID_DB_COLUMN, operatorMasterId);
        mapSqlParameterSource.addValue(AMOUNT_DB_COLUMN, amount);
        mapSqlParameterSource.addValue(RECHARGE_PLAN_NAME_COLUMN, name);
        mapSqlParameterSource.addValue(PRODUCT_TYPE_COLUMN, productType);
        mapSqlParameterSource.addValue(DENOMINATION_TYPE_COLUMN, denominationType);
        mapSqlParameterSource.addValue(MIN_AMOUNT_DB_COLUMN, minAmount);
        mapSqlParameterSource.addValue(MAX_AMOUNT_DB_COLUMN, maxAmount);
        mapSqlParameterSource.addValue(TALKTIME_DB_COLUMN, talktime);
        mapSqlParameterSource.addValue(VALIDITY_DB_COLUMN, validity);
        mapSqlParameterSource.addValue(DESCRIPTION_DB_COLUMN, description);
        mapSqlParameterSource.addValue(STATUS_DB_COLUMN, status);
        mapSqlParameterSource.addValue(IS_STICKY_DB_COLUMN, sticky);
        mapSqlParameterSource.addValue(IREFF_ID_DB_COLUMN, ireffID);
        mapSqlParameterSource.addValue(DATAWEAVE_ID_DB_COLUMN, isRecommended);
        mapSqlParameterSource.addValue(IREFF_CREATED_TIME, createdTime);
        return mapSqlParameterSource;
    }

	@Override
	public String toString() {
		return "RechargePlan{" + "operatorMasterId=" + operatorMasterId + ", circleMasterId=" + circleMasterId
				+ ", amount=" + amount.intValue() + ", name=" + name + ", product_type=" + productType
				+ ", denomination_type=" + denominationType + '}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + circleMasterId;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + operatorMasterId;
		result = prime * result + productType;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RechargePlan other = (RechargePlan) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (circleMasterId != other.circleMasterId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (operatorMasterId != other.operatorMasterId)
			return false;
		if (productType != other.productType)
			return false;
		return true;
	}
    
    
}
