package com.freecharge.productdata.entity;

public enum DenominationActiveType {
	ACTIVE_YES("Active"), ACTIVE_NO("Inactive");
	
	private String name;
	
	private DenominationActiveType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
