package com.freecharge.productdata.entity;

public enum DenominationStatusType {
	STATUS_YES("Enabled"), STATUS_NO("Disabled");
	
	private String name;
	private DenominationStatusType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
