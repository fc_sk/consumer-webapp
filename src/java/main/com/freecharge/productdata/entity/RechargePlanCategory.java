package com.freecharge.productdata.entity;

import com.freecharge.common.framework.basedo.AbstractDO;

/**
 * @param args
 */

public class RechargePlanCategory extends AbstractDO {

    private String name;
    private String displayName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
