package com.freecharge.productdata.entity;

public enum RechargePlanType {
	FULL_TALK_TIME_PLAN("Full Talk Time Plan"), SMS_PLAN("SMS Plan"), REGULAR_RECHARGE_PLAN("Regular Plan");
	
	private String name;
	
	private RechargePlanType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}
