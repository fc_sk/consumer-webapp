package com.freecharge.productdata.entity;

import java.util.Date;
import java.util.List;

public class IreffPlanData {

    private String       id;
    private String       category;
    private List<String> detail;
    private Double       price;
    private Date         updated;
    private String       validity;
    private String       service;
    private String       sourceUri;
    private String       circle;
    private Double       talktime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "IreffPlan [id=" + id + ", category=" + category + ", detail=" + detail + ", price=" + price
                + ", update=" + updated + ", validity=" + validity + ", service=" + service + ", sourceUri="
                + sourceUri + ", circle=" + circle + ", talktime=" + talktime + "]";
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getDetail() {
        return detail;
    }

    public void setDetail(List<String> detail) {
        this.detail.addAll(detail);
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getUpdate() {
        return updated;
    }

    public void setUpdate(Date update) {
        this.updated = update;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSourceUri() {
        return sourceUri;
    }

    public void setSourceUri(String sourceUri) {
        this.sourceUri = sourceUri;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public Double getTalktime() {
        return talktime;
    }

    public void setTalktime(Double talktime) {
        this.talktime = talktime;
    }

}
