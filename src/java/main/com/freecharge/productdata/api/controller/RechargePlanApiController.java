package com.freecharge.productdata.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/api/rechargeplans/*")
public class RechargePlanApiController {
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private OperatorCircleService operatorCircleService;
	
	@Autowired
	private RechargePlanService rechargePlanService;

	public static final String RECHARGE_PLANS_URL = "/admin/rechargeplans/home.htm";

	@RequestMapping(value = "home", method = RequestMethod.GET)
	@NoLogin
    public String viewHome(@RequestParam Map<String, String> mapping, HttpServletResponse response, Model model) throws Exception {
		String dataType = mapping.get("dataType");
		int operatorId = mapping.get("operator") != null ? Integer.parseInt(mapping.get("operator")) : -1;
		int productId = mapping.get("product") != null ? Integer.parseInt(mapping.get("product")) : -1;
		String rechargePlanName = mapping.get("rechargePlanName");
		int circleId = -1;
		int allCircleId = -1;
		List<RechargePlan> rechargePlans = null;
		List<RechargePlan> allRechargePlans = null;
		
		CircleMaster circleMaster = operatorCircleService.getCircle("ALL");
		allCircleId = circleMaster.getCircleMasterId();
		allRechargePlans = rechargePlanService.getRechargePlans(allCircleId, operatorId, productId);
		
		if(mapping.get("circle") != null  && "all".equalsIgnoreCase(mapping.get("circle"))) {
			circleId = allCircleId;
		} else {
			circleId = mapping.get("circle") != null ? Integer.parseInt(mapping.get("circle")) : -1;
		}		
		
		if(circleId != allCircleId) {
			rechargePlans = rechargePlanService.getRechargePlans(circleId, operatorId, productId, rechargePlanName);
		}
		if(allRechargePlans == null) 
			allRechargePlans = new ArrayList<RechargePlan>();
		if(rechargePlans != null)
			allRechargePlans.addAll(rechargePlans);
		if(allRechargePlans.size() > 0){
			model.addAttribute(WebConstants.ALL_RECHARGE_PLANS, allRechargePlans);
		}
		if("html".equals(dataType)) {
			return "home/rechargeplans";
		}
    	return "jsonView";
    }

}
