package com.freecharge.productdata.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.hibernate.envers.tools.Pair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.batch.job.RechargePlanDailyMapper;
import com.freecharge.batch.job.RechargePlanIngestJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashMapIgnoreCase;
import com.freecharge.productdata.entity.DenominationType;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.entity.RechargePlanCategory;
import com.google.common.collect.ImmutableList;

@Service
public class IreffRechargeDataImportService {

    private static final Logger LOGGER = LoggingFactory.getLogger(IreffRechargeDataImportService.class);

    public static final String IREFF_ID   = "id";
    public static final String DETAIL     = "detail";
    public static final String CATEGORY   = "category";
    public static final String KEYWORDS   = "keywords";
    public static final String PRICE      = "price";
    public static final String UPDATED    = "updated";
    public static final String SERVICE    = "service";
    public static final String SOURCE_URI = "sourceUri";
    public static final String CIRCLE     = "circle";
    public static final String TALKTIME   = "talktime";
    public static final String VALIDITY   = "validity";
    public static final String NAME       = "name";
    public static final String DISPLAYNAME   = "displayName";
    public static final String DELIMITER   = " | ";

    public static final String TOPUP = "Topup";
    public static final String DATA2G = "Data/2G";
    public static final String DATA3G = "3G";
    public static final String OTHER = "Other";

    public static final String EMPTY_DESC_TOPUP_FULL = "Full Talktime";
    public static final String EMPTY_DESC_TOPUP_REGULAR = "Talktime of Rs. %s on a recharge of Rs. %s";

    public static final String MODERATION_CAUSE_PLAN_ID = "Similar Plans have same Plan ID.";
    public static final String MODERATION_CAUSE_TALKTIME = "Similar Plans with different Talktime";

    // The maximum recharge amount till which recharge plan status is true
    public static final int MAX_AMOUNT = 1000;
    public static final int MIN_AMOUNT = 10;

    public static final ImmutableList<Integer> specialOperatorIds        = ImmutableList
                                                                                 .of(3, 5, 9, 13, 24, 30, 43, 54);

    /* ---- Example Ireff JSON response ----
     * {"detail":"Local A2A: 5 Min Valid for 1 Day",
     * "category":"topup",
     * "keywords":"topup, local",
     * "price":68,
     * "updated":"2013-05-13 20:35:37.0",
     * "service":"Aircel",
     * "sourceUri":"http://www.aircel.com",
     * "circle":"Assam",
     * "talktime":68}
     */

    @Autowired
    private RechargePlanService                rechargePlanService;

    @Autowired
    private RechargePlanIngestJobUtil          rechargePlanIngestJobUtil;

    @Autowired
    @Qualifier("ireffConnectionManager")
    private MultiThreadedHttpConnectionManager connectionManager;

    private static final String IREFF_FC_KEY = "0e49fde47a63776ee63ced856b581c47191d17f4";

    /*
     * Eventually looks like: http://api.ireff.in/partner/v1/plans?key=KEY&circle=CIRCLE&service=SERVICE
     */
    private static final String IREFF_URL_FORMAT = "http://api.ireff.in/partner/v1/plans?key=%s&circle=%s&service=%s";

    private static final String IREFF_CATEGORIES_URL = "http://api.ireff.in/partner/v1/categories?key=%s";

    private static String getIreffURL(final String ireffCircle, final String ireffService) {
        return String.format(IREFF_URL_FORMAT, IREFF_FC_KEY, ireffCircle, ireffService);
    }

    @Transactional
    public void ingest(final List<RechargePlan> plans, final int circleMasterID, final int operatorMasterID) {
        // Inserts plans which are new - Incremental insert
        rechargePlanService.insertRechargePlansWithModeration(plans);
    }

    private static String getIreffCategoriesUrl() {
        return String.format(IREFF_CATEGORIES_URL, IREFF_FC_KEY);
    }

    // Returns the Categories to be used in the DB
    public List<String> getDBCategoryList() {
        List<String> myCategoryList = new ArrayList<>();
        myCategoryList.add(TOPUP);
        myCategoryList.add(DATA2G);
        myCategoryList.add(DATA3G);
        myCategoryList.add(OTHER);
        return myCategoryList;
    }

    // Maps Data from Ireff Category API "name"=>"displayName"
    public Map<String, String> setRechargePlanCategoryMap(final Reader rechargePlanCategoryReader)
            throws IOException, ParseException {
        Object deserializedRechargePlanCategory = new JSONParser().parse(rechargePlanCategoryReader);

        JSONArray rechargePlanCategoryArray = (JSONArray) deserializedRechargePlanCategory;
        List<RechargePlanCategory> rechargePlanCategoryList = new ArrayList<>(
                rechargePlanCategoryArray.size());
        for (Object each : rechargePlanCategoryArray) {
            JSONObject row = (JSONObject) each;
            RechargePlanCategory rpc = new RechargePlanCategory();
            rpc.setName((String) row.get(NAME));
            rpc.setDisplayName((String) row.get(DISPLAYNAME));
            rechargePlanCategoryList.add(rpc);
        }
        Map<String, String> rpcMap = new HashMap<>();
        if (rechargePlanCategoryList != null && rechargePlanCategoryList.size() > 0) {
            for (RechargePlanCategory rpc : rechargePlanCategoryList) {
                rpcMap.put(rpc.getName(), rpc.getDisplayName());
            }
        }
        return rpcMap;
    }

    // Function to parse the API data and return the Recharge Plans
    public List<RechargePlan> parse(final Reader rechargePlanReader, final int circleMasterID,
            final String ireffCircleName, final int operatorMasterID, final String ireffServiceName,
            final List<String> myCategoryList, final Map<String, String> rpcMap, final String fcOperator) {
        boolean isSpecialOperator = false;
        if(specialOperatorIds.contains(operatorMasterID)){
            isSpecialOperator = true;
        }
        Object deserializedRechargePlan = null;
        try {
            deserializedRechargePlan = new JSONParser().parse(rechargePlanReader);
            JSONArray array = (JSONArray) deserializedRechargePlan;
            List<RechargePlan> result = new ArrayList<>();

            for (Object each : array) {
                if (!(each instanceof JSONObject)) {
                    throw new RuntimeException("Expected a map element, but found " + each);
                }
                JSONObject row = (JSONObject) each;
                if (!ireffCircleName.equals(row.get(CIRCLE))) {
                    String msg = "Expected circle name in JSON response to be '%s', but found '%s'";
                    throw new IllegalStateException(String.format(msg, ireffCircleName, row.get(CIRCLE)));
                }
                if (!ireffServiceName.equals(row.get(SERVICE))) {
                    String msg = "Expected service name in JSON response to be '%s', but found '%s'";
                    throw new IllegalStateException(String.format(msg, ireffServiceName, row.get(SERVICE)));
                }
                RechargePlan rp = new RechargePlan();
                final String exceptionFormat = "Encountered %s while ingesting " + row.toString() + " for circle="
                        + ireffCircleName + ", service=" + ireffServiceName;
                try {
                    String categoryName = (String) row.get(CATEGORY);
                    if (categoryName != null && !categoryName.isEmpty()) {
                        String category = rpcMap.get(categoryName);
                        if (myCategoryList.contains(category)) {
                            rp.setName(category);
                        } else {
                            rp.setName(OTHER);
                        }
                    } else {
                        rp.setName(OTHER);
                    }

                    rp.setAmount(new BigDecimal("" + row.get(PRICE)));
                    final String talktime = row.get(TALKTIME) == null ? "0" : ("" + row.get(TALKTIME));
                    rp.setTalktime(new BigDecimal(talktime));
                    final String validity = (row.get(VALIDITY) == null) ? ("N.A") : ((String) row.get(VALIDITY));
                    rp.setValidity(validity);
                    rp.setDenominationType(DenominationType.FIXED_DENOMINATION.getName());
                    rp.setProductType(ProductMaster.ProductName.Mobile.getProductId());
                    rp.setCircleMasterId(circleMasterID);
                    rp.setOperatorMasterId(operatorMasterID);
                    rp.setIreffID("" + row.get(IREFF_ID));
                    rp.setIsRecommended(false);
                    rp.setCreatedTime(new Timestamp(new Date().getTime()));

                    JSONArray descriptions = null;
                    descriptions = (JSONArray) row.get(DETAIL);

                    // Setting Descriptions for empty descriptions from api
                    if (descriptions == null || descriptions.isEmpty() || descriptions.get(0).equals("")) {
                        if (rp.getName().equals(TOPUP)) {
                            if (rp.getTalktime().compareTo(rp.getAmount()) >= 0) {
                                rp.setDescription(rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                                        EMPTY_DESC_TOPUP_FULL, rp.getName()));
                            } else {
                                rp.setDescription(rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                                        String.format(EMPTY_DESC_TOPUP_REGULAR, row.get(TALKTIME), row.get(PRICE)),
                                        rp.getName()));
                            }
                        } else {
                            rp.setDescription("");
                        }
                    } else {
                        rp.setDescription(rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                                (String) descriptions.get(0), rp.getName()));
                        if (descriptions != null && descriptions.size() >= 2) {
                            rp.setDescription(rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                                    FCUtil.delimitedString(DELIMITER, descriptions), rp.getName()));
                        }
                    }

                    // Do not process recharge amounts > maximum allowable topup
                    if (Double.parseDouble(row.get(PRICE).toString()) > MAX_AMOUNT
                            || Double.parseDouble(row.get(PRICE).toString()) < MIN_AMOUNT) {
                        rp.setStatus(false);
                    }

                    // Check if a duplicate plan exist for same category
                    if (!rechargePlanIngestJobUtil.checkIfDuplicate(rp, result)) {
                        if(!isSpecialOperator) {
                            result.add(rp);
                        } else {
                            // For special operators remove Topup plan if it is a special recharge.
                            result.add(rp);
                            List<RechargePlan> rechargePlans = rechargePlanIngestJobUtil.getAllInvalidPlansFromList(rp,
                                    result);
                            if (rechargePlans.size() > 1) {
                                for (RechargePlan rechargePlan : rechargePlans) {
                                    if (rechargePlan.getName().equals(TOPUP)) {
                                        result.remove(rechargePlan);
                                    }
                                }
                            }
                        }
                    }

                } catch (NumberFormatException nfe) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "NumberFormatException"), nfe);
                } catch (ClassCastException cce) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "ClassCastException"), cce);
                } catch (NullPointerException npe) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "NullPointerException"), npe);
                }
            }
            return result;
        } catch (ClassCastException e) {
            throw new RuntimeException("Expected JSON array, but found " + deserializedRechargePlan, e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    // Checks if a recharge plan in DB is still active in the API.
    public boolean checkPlanActiveInAPI(final List<RechargePlan> plansAPI, final String id, final String name) {
        for (RechargePlan plan : plansAPI) {
            if (plan.getIreffID().equals(id) && plan.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    // Deletes recharge plans which exists in DB but not in API call.
    public List<RechargePlan> getInactivePlans(final List<RechargePlan> plansAPI,
            final List<RechargePlan> allPlansDB) {
        List<RechargePlan> inactivePlans = new ArrayList<>();
        for (RechargePlan row : allPlansDB) {
            if (row.getIreffID() != null && !checkPlanActiveInAPI(plansAPI, row.getIreffID(), row.getName())) {
                // It is an Ireff plan data and it is not present in the current
                // call to Ireff api
                // Delete it from the DB

                if (!row.isSticky()) {
                    inactivePlans.add(row);
                    LOGGER.info("Recharge Plan given by API has been removed by API. So deleting plan from DB."
                            + row.toString());
                }
                if (row.isSticky() && row.getStatus()) {
                    LOGGER.info("Conflicts - A sticky Plan Not in API is enabled. " + row.toString());
                }
            }
        }
        return inactivePlans;
    }

    // Main function to update the Recharge Plans DB
    public void fetchUpdate(final Boolean isPopular) throws HttpException, Exception {
        // get circle name - ID map
        List<CircleMaster> circles = rechargePlanService.getCircles();
        HashMapIgnoreCase<Integer> circleMap = rechargePlanIngestJobUtil.getCircleMap(circles);

        // get operator name - ID map
        List<OperatorMaster> operators = rechargePlanService.getAllOperators();
        HashMapIgnoreCase<Integer> operatorMap = rechargePlanIngestJobUtil.getOperatorMap(operators);

        // Fetching the Ireff categories list
        String ireffCategoriesURL = null;
        ireffCategoriesURL = getIreffCategoriesUrl();
        String ireffRechargePlanCategoryString = getIreffPlans(ireffCategoriesURL);
        Map<String, String> rpcMap = setRechargePlanCategoryMap(new StringReader(ireffRechargePlanCategoryString));
        List<String> myCategoryList = getDBCategoryList();

        // Stores plans to be inserted to db
        List<RechargePlan> dbIngestPlans = new ArrayList<>();
        // Stores plans to be deleted from db
        List<RechargePlan> plansToBeDeleted = new ArrayList<>();
        // Stores plans which require moderation
        List<RechargePlanModerationCausePair> plansToBeModerated = new ArrayList<>();
        // Contains the popular circle-operator pair.
        List<Pair<String, String>> popularList = RechargePlanDailyMapper.getDailyRunOperatorCirclePair();

        // Loop through all circles
        for (String fcCircle : CIRCLE_IREFF_LOOKUP.keySet()) {
            Integer circleIDObject = circleMap.get(fcCircle);
            if (circleIDObject == null) {
                continue;
            }
            int circleID = (int) circleIDObject;
            String ireffCircle = CIRCLE_IREFF_LOOKUP.get(fcCircle);
            String ireffURL = null;
            //
            for (String fcOperator : SERVICE_IREFF_LOOKUP.keySet()) {
                plansToBeDeleted.clear();
                plansToBeModerated.clear();
                // Based on the input popular/non-popular process or ignore
                // circle-operator pair
                Pair<String, String> circleOperatorPair = new Pair<>(fcCircle, fcOperator);
                if (isPopular != null) {
                    if (isPopular.equals(true)) {
                        if (!popularList.contains(circleOperatorPair)) {
                            continue;
                        }
                    } else {
                        if (popularList.contains(circleOperatorPair)) {
                            continue;
                        }
                    }
                }

                Integer operatorIDObject = operatorMap.get(fcOperator);
                /*
                 * Temporary code - Do not update 
                 * 1. Idea - Punjab 
                 * 2. BSNL - Punjab
                 * 3. BSNL- Kerala
                 * 4. Vodafone- Mumbai
                 * 5. Airtel - Karnataka
                 * 6. Vodafone - Andhra Pradesh
                 * 7. Aircel - Delhi
                 * 8. Idea - Maharashtra 
                 * 9. Reliance GSM - Delhi
                 */

                if (operatorIDObject == null || (circleIDObject == 15 && operatorIDObject == 14)
                        || (circleIDObject == 18 && operatorIDObject == 6)
                        || (circleIDObject == 18 && operatorIDObject == 3)
                        || (circleIDObject == 11 && operatorIDObject == 3)
                        || (circleIDObject == 10 && operatorIDObject == 2)
                        || (circleIDObject == 1 && operatorIDObject == 14)
                        || (circleIDObject == 6 && operatorIDObject == 1)
                        || (circleIDObject == 14 && operatorIDObject == 6)
                        || (circleIDObject == 6 && operatorIDObject == 11)) {
                    continue;
                }

                int operatorID = (int) operatorIDObject;
                String ireffService = SERVICE_IREFF_LOOKUP.get(fcOperator);
                ireffURL = getIreffURL(ireffCircle, ireffService);

                String ireffRechargePlanResponseString = null;
                try {
                    ireffRechargePlanResponseString = getIreffPlans(ireffURL);
                } catch (Exception e) {
                    LOGGER.warn(String.format("Unable to fetch API data for Operator : %s and Circle : %s", fcOperator,
                            fcCircle), e);
                    continue;
                }

                if (ireffRechargePlanResponseString != null && ireffRechargePlanCategoryString != null) {
                    List<RechargePlan> plansAPI = parse(new StringReader(ireffRechargePlanResponseString), circleID,
                            ireffCircle, operatorID, ireffService, myCategoryList, rpcMap, fcOperator);
                    dbIngestPlans.clear();
                    dbIngestPlans.addAll(plansAPI);

                    if (plansAPI != null && !plansAPI.isEmpty()) {
                        List<RechargePlan> allPlansDB = rechargePlanService.getRechargePlans(circleID, operatorID);

                        // Check if a plan has been removed by the operator
                        List<RechargePlan> inactivePlans = getInactivePlans(plansAPI, allPlansDB);
                        if (!inactivePlans.isEmpty()) {
                            plansToBeDeleted.addAll(inactivePlans);
                        }

                        allPlansDB.removeAll(plansToBeDeleted);

                        for (RechargePlan planAPI : plansAPI) {
                            RechargePlan similarPlanFromDB = rechargePlanService.getExistingSimilarPlan(planAPI, allPlansDB);
                            if (similarPlanFromDB != null) {
                                // Do not add new plans if the existing plan is
                                // sticky
                                if (similarPlanFromDB.isSticky()) {
                                    dbIngestPlans.remove(planAPI);
                                    if (!similarPlanFromDB.getStatus()) {
                                        // Log Sticky plans with a disabled status.
                                        LOGGER.warn(String.format("Conflicts - Similar Plan is sticky and disabled. "
                                                + "Plan Id: %s, Circle : %s, Operator : %s, Amount : %s, Name : %s ",
                                                similarPlanFromDB.getRechargePlanId(), fcCircle, fcOperator,
                                                similarPlanFromDB.getAmount(), similarPlanFromDB.getName()));
                                    }
                                    continue;
                                }
                                if (similarPlanFromDB.getIreffID() != null) {
                                    // Just update the data if there is a change
                                    // in data provided by by API for same id
                                    if (similarPlanFromDB.getIreffID().equals(planAPI.getIreffID())) {
                                        if (similarPlanFromDB.getStatus() != planAPI.getStatus()) {
                                            try {
                                                rechargePlanService.getRechargePlanInModeration(similarPlanFromDB
                                                        .getRechargePlanId());
                                            } catch (Exception e) {
                                                similarPlanFromDB.setStatus(planAPI.getStatus());
                                                rechargePlanService.updateRechargePlanStatus(similarPlanFromDB);
                                            }
                                        }
                                        dbIngestPlans.remove(planAPI);
                                        planAPI.setRechargePlanId(similarPlanFromDB.getRechargePlanId());
                                        if (!planAPI.toString().equals(similarPlanFromDB.toString())) {
                                            rechargePlanService.updateRechargePlan(planAPI);
                                        }
                                    } else {
                                        // If a similar plan is having a
                                        // different Id
                                        if (!plansToBeDeleted.contains(similarPlanFromDB)) {
                                            plansToBeModerated.add(new RechargePlanModerationCausePair(planAPI,
                                                    MODERATION_CAUSE_PLAN_ID));
                                            plansToBeModerated.add(new RechargePlanModerationCausePair(
                                                    similarPlanFromDB, MODERATION_CAUSE_PLAN_ID));
                                            allPlansDB.add(planAPI);
                                            dbIngestPlans.remove(planAPI);
                                        }
                                    }
                                    continue;
                                }
                                // Check if two plans give the same talktime
                                if (!rechargePlanIngestJobUtil.sameTalktime(similarPlanFromDB, planAPI)
                                        && !plansToBeDeleted.contains(similarPlanFromDB)) {
                                    // Push to db and mark status as false
                                    // Push the two plans to moderation queue
                                    plansToBeModerated.add(new RechargePlanModerationCausePair(planAPI,
                                            MODERATION_CAUSE_TALKTIME));
                                    plansToBeModerated.add(new RechargePlanModerationCausePair(similarPlanFromDB,
                                            MODERATION_CAUSE_TALKTIME));
                                    allPlansDB.add(planAPI);
                                    dbIngestPlans.remove(planAPI);
                                    continue;
                                }
                                // Update plan in db with plan having more
                                // description
                                int descDBLength = rechargePlanIngestJobUtil.getDescriptionLength(similarPlanFromDB);
                                int descAPILength = rechargePlanIngestJobUtil.getDescriptionLength(planAPI);
                                if (descDBLength < descAPILength) {
                                    // Check if planAPI can be inserted to the
                                    // DB after deletion of planFromDB
                                    plansToBeDeleted.add(similarPlanFromDB);
                                    allPlansDB.add(planAPI);
                                    continue;
                                }
                                // Same plan found in DB. Remove plan from the
                                // plans to be inserted to DB
                                dbIngestPlans.remove(planAPI);
                            } else {
                                allPlansDB.add(planAPI);
                            }
                        }

                        if (!plansToBeModerated.isEmpty()) {
                            List<RechargePlan> plansAddedToDB = rechargePlanIngestJobUtil
                                    .processForModeration(plansToBeModerated);
                            dbIngestPlans.removeAll(plansAddedToDB);
                        }

                        if (!plansToBeDeleted.isEmpty()) {
                            rechargePlanService.deleteNonStickyPlanFromDB(plansToBeDeleted);
                            LOGGER.info(String.format(
                                    "Removed all non-sticky invalid recharge plans from DB for circle=%s, operator=%s",
                                    fcCircle, fcOperator));
                            allPlansDB.removeAll(plansToBeDeleted);
                        }

                        // Push all the Full TT recharges to moderation queue
                        // for popular Circles
                        if (popularList.contains(circleOperatorPair)) {
                            List<RechargePlanModerationCausePair> fullTTPlansInDB = rechargePlanIngestJobUtil
                                    .getFullTTPlansToBeModerated(allPlansDB);
                            List<RechargePlan> plansAddedToDB = rechargePlanIngestJobUtil
                                    .processForModeration(fullTTPlansInDB);
                            dbIngestPlans.removeAll(plansAddedToDB);
                        }

                        if (!dbIngestPlans.isEmpty()) {
                            ingest(dbIngestPlans, circleID, operatorID);
                            LOGGER.info(String.format("Ingested recharge plans for circle=%s, operator=%s", fcCircle,
                                    fcOperator));
                        }
                    }
                } else {
                    LOGGER.error("Expected HTTP response code 200 for Ireff URL: " + ireffURL);
                }
            }
        }
    }

    private static final Map<String, String> CIRCLE_IREFF_LOOKUP  = FCUtil.createMap(
            // Freecharge name, Ireff long name, //Ireff short code
            "Andhra Pradesh",   "AndhraPradesh",     //"AP",
            "Assam",            "Assam",             //"AS",
            "Bihar",            "BiharJharkhand",    //"BR",
            //"Chattisgarh",
            "Chennai",          "Chennai",           //"Chennai",
            "Delhi",            "Delhi",             //"Delhi",
            "Gujarat",          "Gujarat",           //"GJ",
            "Haryana",          "Haryana",           //"HR",
            "Himachal Pradesh", "HimachalPradesh",   //"HP",
            "Karnataka",        "Karnataka",         //"KA",
            "Kerala",           "Kerala",            //"KL",
            "Kolkata",          "Kolkata",           //"Kolkata",
            "Madhya Pradesh",   "MadhyaPradesh",     //"MP",
            "Maharashtra",      "Maharashtra",       //"MH",
            "Mumbai",           "Mumbai",            //"Mumbai",
            "North East",       "NorthEast",         //"NE",
            "Orissa",           "Orissa",            //"OR",
            "Punjab",           "Punjab",            //"PB",
            "Rajasthan",        "Rajasthan",         //"RJ",
            "Tamil Nadu",       "TamilNadu",         //"TN",
            "Uttar Pradesh (E)", "UttarPradeshEast", //"UPE",
            "Uttar Pradesh (W)", "UttarPradeshWest", //"UPW",
            "West Bengal",       "WestBengal",       //"WB"
            //"Uttar Pradesh"
            //"ALL"
            "JK",                "JammuKashmir"      //"JK",
            //"UTTARANCHAL"
            // ----------
            );

    private static final Map<String, String> SERVICE_IREFF_LOOKUP = FCUtil.createMap(
            // Freecharge operator code, Ireff service name
            "Aircel",                    "Aircel",
            "Airtel",                    "Airtel",
            "BSNL",                      "BSNL",
            "Tata Docomo GSM",           "Docomo",
            "Idea",                      "Idea",
            "Loop",                      "Loop",
            "MTNL Delhi",                "MTNL",
            "MTNL Mumbai",               "MTNL",
            "Reliance-CDMA",             "RelianceCDMA",
            "Reliance-GSM",              "RelianceGSM",
            "Tata Indicom",              "Indicom",
            "Tata Docomo CDMA",          "Indicom",
            "Uninor",                    "Uninor",
            "Vodafone",                  "Vodafone",
            "Virgin GSM",                "VirginGSM",
            "Virgin CDMA",               "VirginCDMA",
            "T24",                       "T24",
            "MTS",                       "MTS",
            "Videocon Mobile",           "Videocon"
            //"Indicom-Walky",             "VirginCDMA",
            //"Dish Tv",
            //"Big Tv",
            //"Airtel Tv",
            //"Sun Tv",
            //"Tata Sky",
            //"Videocon Tv",
            //"Tata Photon",
            //"Reliance Netconnect",
            //"VIRGIN",
            //"MTS MBrowse",
            //"MTS Mblaze",
            // -----------
            // Unused Ireff entries are below
            // -----------
            // Unmapped Ireff entries
            //"T24",
            //"VirginGSM"
            );

    private String getIreffPlans(final String ireffURL) throws Exception {
        try {
            String body = null;
            HttpClient client = new HttpClient(connectionManager);
            HttpMethod txn = new GetMethod(ireffURL);
            LOGGER.info("Making request to Ireff URL: " + ireffURL);
            int code = client.executeMethod(txn);
            LOGGER.info("Ireff request returned HTTP response code " + code);
            if (code == HttpServletResponse.SC_OK) {
                body = txn.getResponseBodyAsString();
            }
            return body;
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get plans using API call : %s", ireffURL), e);
            throw e;
        }
    }
}
