package com.freecharge.productdata.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.DimensionFilter;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.cloudwatch.model.ListMetricsRequest;
import com.amazonaws.services.cloudwatch.model.ListMetricsResult;
import com.amazonaws.services.cloudwatch.model.Metric;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.amazonaws.services.cloudwatch.model.Statistic;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.sns.RechargePlanSNSService;
import com.freecharge.sns.bean.RechargePlanAlertBean;
import com.freecharge.sns.bean.RechargePlanAlertBean.PlanAction;

@Service
public class DenomSuccessUpdaterService {

    @Autowired
    private RechargePlanService               rechargePlanService;

    @Autowired
    private AmazonCloudWatch                  cloudWatch;

    @Autowired
    private AmazonDynamoDBAsync               dynamoClient;

    @Autowired
    private RechargePlanSNSService            rechargePlanSNSService;

    @Autowired
    private FCProperties                      fcProperties;

    private static final Logger               logger               = LoggingFactory
                                                                           .getLogger(DenomSuccessUpdaterService.class);

    private static final String               DIM_OPERATOR         = "operator";
    private static final String               DIM_CIRCLE           = "circle";
    private static final String               DIM_PRODUCT          = "product";
    private static final String               DIM_PLANTYPE         = "planType";

    private static final String               DELIMITER            = ":";
    private static final String               VALID_DENOM          = "Valid";
    private static final String               INVALID_DENOM        = "Invalid";

    private static final String               DYNAMO_TABLE_NAME    = "denom_success_data";
    private static final String               DYNAMO_KEY_FIELD     = "key";
    private static final String               DYNAMO_SUCCESS_FIELD = "successRate";

    private final List<RechargePlanAlertBean> plansToBeAdded       = new LinkedList<RechargePlanAlertBean>();

    public class SuccessRateComparator implements Comparator<RechargePlanAlertBean> {

        @Override
        public int compare(RechargePlanAlertBean o1, RechargePlanAlertBean o2) {
            return o2.getSucessRate().compareTo(o1.getSucessRate());
        }
    }

    public class TxnCountComparator implements Comparator<RechargePlanAlertBean> {

        @Override
        public int compare(RechargePlanAlertBean o1, RechargePlanAlertBean o2) {
            return o2.getTxnCount().compareTo(o1.getTxnCount());
        }
    }

    /*
     * A function to calculate the denomination success rate for all the plans
     * used. The steps are as follows. 1. For each operator and circle, do the
     * following; a. Get all the denominations from the cloudwatch. b. Calculate
     * the success rate using the following formula.
     */
    public void calculateDenomSuccessRates() {
        // Get all the operators and the circle.
        List<CircleMaster> circles = rechargePlanService.getCircles();
        List<OperatorMaster> operators = rechargePlanService.getAllOperators();
        
        // First get the data for the last 'n' hours.
        final DateTime startTime = DateTime.now().minusHours(fcProperties.getRechargeDenomWindowHours());
        final DateTime endTime = DateTime.now();

        for (CircleMaster currCirlce : circles) {
            for (OperatorMaster currOperator : operators) {
                // clear the plans.
                plansToBeAdded.clear();
                logger.info(" Operator : " + currOperator.getName() + " Circle : " + currCirlce.getName());
                findAndUpdateSuccessRate(currCirlce.getCircleMasterId(), currOperator.getOperatorMasterId(), startTime, endTime);
                findPlansToRecommnedAndNotify(currCirlce, currOperator);
            }
        }
    }

    /*
     * This function will traverse over all the plans to be added for this
     * operator-cirlce and picks the recommended plan based on the following
     * criteria.
     */
    private void findPlansToRecommnedAndNotify(CircleMaster circleMaster, OperatorMaster operatorMaster) {
        if (plansToBeAdded.isEmpty()) {
            logger.error("No plans to be added for the operator: " + operatorMaster.getName() + " and circle "
                    + circleMaster.getName());
            return;
        }
        List<RechargePlanAlertBean> txnCountList = new LinkedList<RechargePlanAlertBean>();
        List<RechargePlanAlertBean> succRateList = new LinkedList<RechargePlanAlertBean>();
        txnCountList.addAll(plansToBeAdded);
        succRateList.addAll(plansToBeAdded);
        Collections.sort(succRateList, new SuccessRateComparator());
        Collections.sort(txnCountList, new TxnCountComparator());
        for (int i = 0; i < txnCountList.size(); ++i) {
            logger.info("Txn size: " + txnCountList.get(i).getTxnCount());
        }
        for (int i = 0; i < succRateList.size(); ++i) {
            logger.info("Success rate : " + succRateList.get(i).getSucessRate());
        }

        int numTxnsToRetain = (int) (txnCountList.size() * (fcProperties.getNumPercentPlansToRecommend() / 100f));
        succRateList.retainAll(txnCountList.subList(0, numTxnsToRetain * 2));
        succRateList = succRateList.subList(0, numTxnsToRetain);
        logger.info(succRateList.size() + " plans are recommended out of " + plansToBeAdded.size()
                + " for the operator: " + operatorMaster.getName() + " and circle " + circleMaster.getName());
        plansToBeAdded.removeAll(succRateList);

        for (RechargePlanAlertBean recommendedPlan : succRateList) {
            recommendedPlan.setRecommended(true);
            this.notifyPlanStatus(recommendedPlan, PlanAction.ADD);
        }

        for (RechargePlanAlertBean plan : plansToBeAdded) {
            plan.setRecommended(false);
            this.notifyPlanStatus(plan, PlanAction.ADD);
        }
    }

    /*
     * Get and update all the denominations for the currCircle and currOperator
     */
    private void findAndUpdateSuccessRate(final Integer currCirlceId, final Integer currOperatorId, DateTime startTime, DateTime endTime) {

        // For each product type
        for (String currProduct : FCConstants.PRODUCT_IDS_FOR_RECHARGE_PLANS) {

            for (String currPlanType : FCConstants.RECHARGE_PLAN_TYPES) {
                // Get all the denominations available in cloudwatch
                List<Double> denoms = getAllMetricsForProduct(currCirlceId, currOperatorId, currProduct, currPlanType);
                if (denoms == null || denoms.isEmpty()) {
                    logger.error("Got empty list of denominations for operator " + currOperatorId + " , circle "
                            + currCirlceId + " , product " + currProduct + " , plan type " + currPlanType);
                    continue;
                }
                updateSuccessRateForDenoms(denoms, currCirlceId, currOperatorId, currProduct, currPlanType, startTime, endTime);
            }
        }
    }

    private void updateSuccessRateForDenoms(List<Double> denoms, Integer currCirlceId, Integer currOperatorId,
            String currProduct, String planType, DateTime startTime, DateTime endTime) {

        try {
            for (double amount : denoms) {
                Object[] denomData = getStatsForDenom(amount, currCirlceId, currOperatorId, currProduct, planType, startTime, endTime);
                float successRate = (float) denomData[1];
                float rechargeAttempts = (float) denomData[0];
                long totalCount = (long) denomData[2];
                logger.info(" Amount : " + amount + "  success Rate: " + successRate + " Effective recharge attempts: "
                        + rechargeAttempts);

                // Update the success rate in Dynamo.
                String key = currOperatorId.toString() + "_" + currCirlceId.toString() + "_" + currProduct + "_"
                        + planType + "_" + amount;
                updateSuccessRateInDynamo(key, successRate);

                RechargePlanAlertBean alertBean = new RechargePlanAlertBean();
                alertBean.setOperatorId(currOperatorId);
                alertBean.setCircleId(currCirlceId);
                alertBean.setRechargeAmount(amount);
                alertBean.setPlanType(planType);
                alertBean.setProductType(currProduct);
                alertBean.setSucessRate(successRate);
                alertBean.setTxnCount(totalCount);
                alertBean.setRecommended(false);

                float minSuccessRate = ((float) fcProperties.getRechargeDenomMinSuccessRate()) / 100;
                float minRechargeAttempts = fcProperties.getRechargeDenomMinAttempts();

                if (successRate < minSuccessRate) {
                    // Low success rate. Send the invalid plan notification
                    logger.info(" Not enough success rate for amount " + amount + ", notify to invalidate from plan DB");
                    notifyPlanStatus(alertBean, PlanAction.INVALID);
                    continue;
                }

                // Don't add the plans with low recharge attempts.
                if (rechargeAttempts < minRechargeAttempts) {
                    continue;
                }

                // Plan has both high success rate and the decent number of recharge attempts.
                // Add it.
                logger.info("Add plan with amount: " + amount + " to plan DB");
                plansToBeAdded.add(alertBean);
                continue;
            }
        } catch (Exception e) {
            logger.error("Some error while updating the success rate ", e);
        }
    }

    private void notifyPlanStatus(RechargePlanAlertBean alertBean, PlanAction action) {
        String message;
        try {
            alertBean.setActionType(action.getActionName());
            message = FCUtil.getJsonString(alertBean);
            rechargePlanSNSService.publish("{\"message\" : " + message + "}");
        } catch (Exception e) {
            logger.error(" Error while notifying the invalid plan ", e);
        }
    }

    private void updateSuccessRateInDynamo(String key, Float successRate) {
        Map<String, AttributeValue> updateItems = new HashMap<String, AttributeValue>();
        updateItems.put(DYNAMO_KEY_FIELD, new AttributeValue(key));
        updateItems.put(DYNAMO_SUCCESS_FIELD, new AttributeValue().withS(successRate.toString()));
        PutItemRequest putItemRequest = new PutItemRequest(DYNAMO_TABLE_NAME, updateItems);
        logger.info("Updating the success rate to " + successRate + " for key " + key);
        dynamoClient.putItem(putItemRequest);
    }

    private Object[] getStatsForDenom(Double rechargeAmount, Integer currCirlceId, Integer currOperatorId,
            String currProduct, String currPlanType, DateTime startTime, DateTime endTime) {
        List<Dimension> dimensions = new ArrayList<Dimension>();
        dimensions.add(new Dimension().withName(DIM_OPERATOR).withValue(currOperatorId.toString()));
        dimensions.add(new Dimension().withName(DIM_CIRCLE).withValue(currCirlceId.toString()));
        dimensions.add(new Dimension().withName(DIM_PRODUCT).withValue(currProduct));
        dimensions.add(new Dimension().withName(DIM_PLANTYPE).withValue(currPlanType));
        final String validMetricName = VALID_DENOM + DELIMITER + rechargeAmount.toString();
        final String invalidMetricName = INVALID_DENOM + DELIMITER + rechargeAmount.toString();

        // This array holds count of total transactions happened for
        // 'rechargeAmount' in last n days where n <=MAX_DAYS.
        List<Integer> totalRechargesList = new ArrayList<Integer>();
        // success rate of last n days.
        List<Float> successRateList = new ArrayList<Float>();

        Float effectiveReachargeAttempts = 0f;
        Long numTxns = 0l;
        int numDays = 0;

        int decayRate = fcProperties.getRechargeDenomDecayRate();
        float currDecayRate = decayRate;

        int maxDaysToCheck = fcProperties.getRechargeDenomMaxDays();
        // Collect the sufficient recharges attempts count.
        while (numDays < maxDaysToCheck && effectiveReachargeAttempts < fcProperties.getRechargeDenomMinAttempts()) {
            Integer validMetricCount = getStatsForMetric(dimensions, validMetricName, startTime.toDate(),
                    endTime.toDate());
            Integer invalidMetricCount = getStatsForMetric(dimensions, invalidMetricName, startTime.toDate(),
                    endTime.toDate());
            if (validMetricCount == null || invalidMetricCount == null) {
                logger.error(" Could not find the stats for amount " + rechargeAmount);
                return null;
            }
            int total = validMetricCount + invalidMetricCount;
            numTxns += total;
            totalRechargesList.add(total);
            if (total != 0) {
                successRateList.add((float) validMetricCount / (total));
            } else {
                successRateList.add(0f);
            }
            endTime = startTime;
            startTime = startTime.minusDays(/* Day count = */1);

            numDays++;
            effectiveReachargeAttempts += 2 * (total * currDecayRate);
            currDecayRate /= decayRate;
        }
        // Calculate the success rate and return it.
        Float finalSuccessRate = cacluateEffectiveSuccessRate(successRateList, totalRechargesList);
        return new Object[] { effectiveReachargeAttempts, finalSuccessRate, numTxns };
    }

    private float cacluateEffectiveSuccessRate(List<Float> successRateList, List<Integer> totalRechargesList) {
        int decayRate = fcProperties.getRechargeDenomDecayRate();
        int currDecayRate = decayRate;
        float effectiveSuccessAttempts = 0f;
        float effectiveTotalAttempts = 0f;
        for (int i = 0; i < successRateList.size(); ++i) {
            effectiveSuccessAttempts += currDecayRate * totalRechargesList.get(i) * successRateList.get(i);
            effectiveTotalAttempts += currDecayRate * totalRechargesList.get(i);
            currDecayRate /= decayRate;
        }
        if (effectiveTotalAttempts == 0) {
            return 0;
        }
        return effectiveSuccessAttempts / effectiveTotalAttempts;
    }

    private Integer getStatsForMetric(List<Dimension> dimensions, String metricName, Date startTime, Date endTime) {

        try {
            GetMetricStatisticsRequest metricsRequest = new GetMetricStatisticsRequest();
            metricsRequest.setDimensions(dimensions);
            metricsRequest.setMetricName(metricName);
            List<String> statistics = new ArrayList<String>();
            statistics.add(Statistic.Sum.toString());
            metricsRequest.setStatistics(statistics);
            metricsRequest.setNamespace(fcProperties.getMode());
            metricsRequest.setStartTime(startTime);
            metricsRequest.setEndTime(endTime);
            int secondsBetween = (int) (endTime.getTime() - startTime.getTime()) / 1000;
            metricsRequest.setPeriod(secondsBetween);
            metricsRequest.setUnit(StandardUnit.Count);
            GetMetricStatisticsResult statisticsResult = cloudWatch.getMetricStatistics(metricsRequest);
            List<Datapoint> datapoints = statisticsResult.getDatapoints();
            if (datapoints.isEmpty())
                return 0;
            // this cast is safe as we know that the count cannot exceed the int
            // limit
            return (int) datapoints.get(0).getSum().doubleValue();
        } catch (Exception e) {
            logger.error("Error while getting data from cloudwatch", e);
        }
        return null;
    }

    private List<Double> getDenominationListFromMetrics(List<Metric> metrics) {

        List<Double> denoms = new ArrayList<Double>();
        for (Metric currMetric : metrics) {
            String amount = currMetric.getMetricName().split(DELIMITER)[1];
            if (!denoms.contains(Double.parseDouble(amount))) {
                denoms.add(Double.parseDouble(amount));
            }
        }
        return denoms;
    }

    // Return all the denominations present in the cloudwatch.

    private List<Double> getAllMetricsForProduct(Integer currCirlceId, Integer currOperatorId, String currProduct,
            String currPlanType) {

        try {
            List<DimensionFilter> dimensions = new ArrayList<DimensionFilter>();
            dimensions.add(new DimensionFilter().withName(DIM_OPERATOR).withValue(currOperatorId.toString()));
            dimensions.add(new DimensionFilter().withName(DIM_CIRCLE).withValue(currCirlceId.toString()));
            dimensions.add(new DimensionFilter().withName(DIM_PRODUCT).withValue(currProduct));
            dimensions.add(new DimensionFilter().withName(DIM_PLANTYPE).withValue(currPlanType));
            ListMetricsRequest metricsRequest = new ListMetricsRequest();
            metricsRequest.setNamespace(fcProperties.getMode());
            metricsRequest.setDimensions(dimensions);
            ListMetricsResult metricsResult = cloudWatch.listMetrics(metricsRequest);

            return getDenominationListFromMetrics(metricsResult.getMetrics());
        } catch (Exception e) {
            logger.error("Some error while getting the metrcis list from cloudwatch", e);
        }
        return null;
    }
}