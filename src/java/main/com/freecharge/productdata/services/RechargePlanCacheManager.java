package com.freecharge.productdata.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.cache.CacheManager;
import com.freecharge.productdata.dao.jdbc.RechargePlanReadDAO;
import com.freecharge.productdata.entity.RechargePlan;

@Repository("rechargePlanCacheManager")
public class RechargePlanCacheManager extends CacheManager {

    @Autowired
    private RechargePlanReadDAO rechargePlanReadDAO;

    String rechargePlanCacheKeyPrefix = "rechargeplan";
    String rechargePlanAllCircleCacheKeyPrefix = "rechargeplanAllCircle";
    String rechargePlanProductWiseCacheKeyPrefix = "rechargePlanProductWise";

    @SuppressWarnings("unchecked")
    public List<RechargePlan> getRechargePlansAllCircle(int operatorId) {

        List<RechargePlan> rechargePlans = null;
        String cacheKey = rechargePlanAllCircleCacheKeyPrefix + "_" + operatorId;

        Object obj = get(cacheKey);

        if (obj != null) {
            rechargePlans = (List<RechargePlan>) obj;
        }

        if (rechargePlans == null || rechargePlans.size() == 0) {
            rechargePlans = rechargePlanReadDAO.getRechargePlansAllCircle(operatorId);
            if (rechargePlans != null && rechargePlans.size() > 0) {
                setRechargePlansAllCircleInCache(operatorId, rechargePlans);
            }
        }
        return rechargePlans;
    }

    public void setRechargePlansAllCircleInCache(int operatorId, List<RechargePlan> rechargePlanObj) {
        String cacheKey = rechargePlanAllCircleCacheKeyPrefix + "_" + operatorId;
        set(cacheKey, rechargePlanObj, 60, TimeUnit.MINUTES);
    }
    
    @SuppressWarnings("unchecked")
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId) {

        List<RechargePlan> rechargePlans = null;
        String cacheKey = rechargePlanCacheKeyPrefix + "_" + circleId + "_" + operatorId;

        Object obj = get(cacheKey);

        if (obj != null) {
            rechargePlans = (List<RechargePlan>) obj;
        }

        if (rechargePlans == null || rechargePlans.size() == 0) {
            rechargePlans = rechargePlanReadDAO.getRechargePlans(circleId, operatorId);
            if (rechargePlans != null && rechargePlans.size() > 0) {
                setRechargePlansCache(circleId, operatorId, rechargePlans);
            }
        }
        return rechargePlans;
    }
    
    public void setRechargePlansForProductCache(int circleId, int operatorId, int productId, List<RechargePlan> rechargePlanObj) {
        String cacheKey = rechargePlanProductWiseCacheKeyPrefix + "_" + circleId + "_" + operatorId + "_" + productId;
        set(cacheKey, rechargePlanObj, 60, TimeUnit.MINUTES);
    }
    
    @SuppressWarnings("unchecked")
    public List<RechargePlan> getRechargePlansForProduct(int circleId, int operatorId, int productId) {

        List<RechargePlan> rechargePlans = null;
        String cacheKey = rechargePlanProductWiseCacheKeyPrefix + "_" + circleId + "_" + operatorId + "_" + productId;

        Object obj = get(cacheKey);

        if (obj != null) {
            rechargePlans = (List<RechargePlan>) obj;
        }

        if (rechargePlans == null || rechargePlans.size() == 0) {
            rechargePlans = rechargePlanReadDAO.getRechargePlans(circleId, operatorId, productId);
            if (rechargePlans != null && rechargePlans.size() > 0) {
                setRechargePlansForProductCache(circleId, operatorId, productId, rechargePlans);
            }
        }
        return rechargePlans;
    }
    
    public void setRechargePlansCache(int circleId, int operatorId, List<RechargePlan> rechargePlanObj) {
        String cacheKey = rechargePlanCacheKeyPrefix + "_" + circleId + "_" + operatorId;
        set(cacheKey, rechargePlanObj, 60, TimeUnit.MINUTES);
    }
    
    public void clearPlansCache(int circleId, int operatorId, int productId) {
        String cacheKey = rechargePlanCacheKeyPrefix + "_" + circleId + "_" + operatorId;
        delete(cacheKey);
        cacheKey = rechargePlanProductWiseCacheKeyPrefix + "_" + circleId + "_" + operatorId + "_" + productId;
        delete(cacheKey);
        cacheKey = rechargePlanAllCircleCacheKeyPrefix + "_" + operatorId;
        delete(cacheKey);
    }
    
    
    public List<RechargePlan> getRechargePlansForOperatorWithProduct(int operatorId,int productId) {
        List<RechargePlan> rechargePlans = getRechargePlansAllCircle(operatorId);
        List<RechargePlan> rechargePlansForProduct = new ArrayList<RechargePlan>();
        if(rechargePlans != null && rechargePlans.size() > 0){
            for(RechargePlan plan : rechargePlans){
                if(plan.getStatus() && plan.getProductType() == productId && StringUtils.isNotEmpty(plan.getName())){
                    rechargePlansForProduct.add(plan);
                }
            }
        }
        return rechargePlansForProduct;
    }
    
    public List<RechargePlan> getRechargePlansForAmount(int circleId,int operatorId,int productId,float amount) {
        List<RechargePlan> rechargePlans = getRechargePlansForProduct(circleId, operatorId, productId);
        List<RechargePlan> rechargePlansForAmount = new ArrayList<RechargePlan>();
        if(rechargePlans != null && rechargePlans.size() > 0){
            for(RechargePlan plan : rechargePlans){
                if(plan.getStatus() && plan.getAmount() != null && new BigDecimal(amount).compareTo(plan.getAmount()) == 0){
                    rechargePlansForAmount.add(plan);
                }
            }
        }
        return rechargePlansForAmount;
    }
    
    public List<RechargePlan> getRechargePlansWithName(int circleId,int operatorId,int productId,String planName) {
        List<RechargePlan> rechargePlans = getRechargePlansForProduct(circleId, operatorId, productId);
        List<RechargePlan> rechargePlansWithName = new ArrayList<RechargePlan>();
        if(rechargePlans != null && rechargePlans.size() > 0){
            for(RechargePlan plan : rechargePlans){
                if(plan.getStatus() && planName.equalsIgnoreCase(plan.getName())){
                    rechargePlansWithName.add(plan);
                }
            }
        }
        return rechargePlansWithName;
    }

}
