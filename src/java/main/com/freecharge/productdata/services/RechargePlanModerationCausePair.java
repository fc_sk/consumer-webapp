package com.freecharge.productdata.services;
import com.freecharge.productdata.entity.RechargePlan;

public class RechargePlanModerationCausePair {
    private RechargePlan rechargePlan;
    private String       cause;

    public RechargePlanModerationCausePair(RechargePlan rechargePlan, String cause) {
        this.rechargePlan = rechargePlan;
        this.cause = cause;
    }

    public RechargePlan getRechargePlan() {
        return rechargePlan;
    }

    public String getCause() {
        return cause;
    }

    public void setRechargePlan(RechargePlan rechargePlan) {
        this.rechargePlan = rechargePlan;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}