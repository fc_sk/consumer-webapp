package com.freecharge.productdata.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.batch.job.RechargePlanIngestJobUtil;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mongo.repos.RechargePlanModerationRepository;
import com.freecharge.productdata.dao.jdbc.InRechargePlanReadDAO;
import com.freecharge.productdata.dao.jdbc.InRechargePlanWriteDAO;
import com.freecharge.productdata.dao.jdbc.RechargePlanReadDAO;
import com.freecharge.productdata.dao.jdbc.RechargePlanWriteDAO;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.rest.rechargeplans.RechargePlanBusinessDO;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 29/9/12 Time: 4:25 PM To change
 * this template use File | Settings | File Templates.
 */
@Component("rechargePlanService")
public class RechargePlanService {
    private static final Logger              LOGGER = LoggingFactory.getLogger(RechargePlanService.class);
    private static final String TOPUP                                   = "Topup";
    private static final String DATA2G                                  = "Data/2G";
    private static final String DATA3G                                  = "3G";
    private static final String OTHER                                   = "Other";

    @Autowired
    private RechargePlanReadDAO              rechargePlanReadDAO;
    @Autowired
    private RechargePlanWriteDAO             rechargePlanWriteDAO;
    @Autowired
    private OperatorCircleReadDAO            operatorCircleReadDAO;

    @Autowired
    private InRechargePlanWriteDAO inRechargePlanWriteDAO;

    @Autowired
    private InRechargePlanReadDAO inRechargePlanReadDAO;

    @Autowired
    private RechargePlanModerationRepository rechargePlanModerationRepository;
    @Autowired
    private RechargePlanIngestJobUtil          rechargePlanIngestJobUtil;
    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private RechargePlanCacheManager rechargePlanCacheManager;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private EmailService emailService;

    /*
    1. BSNL - 3
    2. Tata Docomo GSM - 5
    3. MTNL Delhi - 9
    4. Uninor - 13
    5. Videocon Mobile - 24
    6. Virgin GSM - 30
    7. T24 - 43
    8. MTNL Mumbai - 54
     */
    public final ImmutableList<Integer> SPECIAL_OPERATOR_IDS       = ImmutableList
            .of(3, 5, 9, 13, 24, 30, 43, 54);

    private static final String MODERATION_CAUSE_ID_STICKY = "Plan ID found sticky on Insert.";
    private static final int DISABLE = 0;

    public List<RechargePlan> getRechargePlans(final int circleId, final int operatorId, final int productId) {
        return rechargePlanCacheManager.getRechargePlansForProduct(circleId, operatorId, productId);
    }

    public List<RechargePlan> getRechargePlanList(final int circleId, final int operatorId, final int productId, final float amount, final String name) {
    	List<RechargePlan> rechargePlans = rechargePlanReadDAO.getRechargePlans(circleId, operatorId, productId, amount);	
        List<RechargePlan> rechargePlanList = new ArrayList<>();
        for (RechargePlan rechargePlan : rechargePlans) {
            if (name.equals(rechargePlan.getName())) {
            	rechargePlanList.add(rechargePlan);
            }
        }
        return rechargePlanList;
    }
    
    public List<RechargePlan> getRechargePlansForOperator(final int operatorId, final int productId) {
        return rechargePlanCacheManager.getRechargePlansForOperatorWithProduct(operatorId, productId);
    }

    public List<RechargePlanBusinessDO> getRechargePlansBizDO(final int circleId, final int operatorId,
            final int productId) {
        List<RechargePlan> rechargePlanList = rechargePlanCacheManager.getRechargePlansForProduct(circleId, operatorId, productId);

        List<RechargePlanBusinessDO> rechargePlanBizDoList = typeConverter(rechargePlanList);

        return rechargePlanBizDoList;
    }

    public List<RechargePlanBusinessDO> getRechargePlan(final int circleId, final int operatorId, final int productId,
            final float amount) {
        List<RechargePlan> rechargePlanList = rechargePlanCacheManager.getRechargePlansForAmount(circleId, operatorId, productId,
                amount);

        List<RechargePlanBusinessDO> rechargePlanBizDoList = typeConverter(rechargePlanList);

        return rechargePlanBizDoList;
    }

    private List<RechargePlanBusinessDO> typeConverter(final List<RechargePlan> rechargePlanList) {
        List<RechargePlanBusinessDO> rechargePlanBizDoList = new ArrayList<>();
        Set<String> visited = new HashSet<>();

        if (rechargePlanList != null) {
            for (RechargePlan rechargePlan : rechargePlanList) {
                if (!visited.contains(rechargePlan.getIreffID())) {
                    RechargePlanBusinessDO listElement = typeConverter(rechargePlan);

                    rechargePlanBizDoList.add(listElement);

                    visited.add(rechargePlan.getIreffID());
                }
            }
        }
        return rechargePlanBizDoList;
    }

    private RechargePlanBusinessDO typeConverter(final RechargePlan from) {
        RechargePlanBusinessDO to = new RechargePlanBusinessDO();

        to.setAmount(from.getAmount());
        to.setCircleMasterId(from.getCircleMasterId());
        to.setDenominationType(from.getDenominationType());
        to.setDescription(from.getDescription());
        to.setMaxAmount(from.getMaxAmount());
        to.setMinAmount(from.getMinAmount());
        to.setName(from.getName());
        to.setOperatorMasterId(from.getOperatorMasterId());
        to.setProductType(from.getProductType());
        to.setTalkTime(from.getTalktime());
        to.setValidity(from.getValidity());
        to.setRechargePlanId(from.getRechargePlanId());

        if (from.getTalktime().compareTo(from.getAmount()) >= 0) {
            to.setIsFullTalkTime(true);
        }

        return to;
    }

    public List<RechargePlan> getRechargePlans(final int circleId, final int operatorId, final int productId,
            final String rechargePlanName) {
        return rechargePlanCacheManager.getRechargePlansWithName(circleId, operatorId, productId, rechargePlanName);
    }

    public List<RechargePlan> getRechargePlans(final int circleId, final int operatorId) {
        return rechargePlanCacheManager.getRechargePlans(circleId, operatorId);
    }

    public List<RechargePlan> getRechargePlansAllCircle(final int operatorId) {
        return rechargePlanCacheManager.getRechargePlansAllCircle(operatorId);
    }

    public RechargePlan getRechargePlan(final int rechargePlanId) {
        return rechargePlanReadDAO.getRechargePlan(rechargePlanId);
    }
    // Checks if a recharge plan similar to 'rechargePlan' is set as sticky in
    // the plans from db
    public boolean checkIfStickyPlanExists(final RechargePlan rechargePlan) {
        return this.rechargePlanReadDAO.checkIfStickyPlanExists(rechargePlan);
    }
    // Checks if a recharge plan similar to 'rechargePlan' is set as sticky in
    // 'plans'
    public boolean checkIfStickyPlanExists(final RechargePlan rechargePlan, final List<RechargePlan> plans) {
        for (RechargePlan rp : plans) {
            if (rp.isSticky()) {
                if (rp.getIreffID() != null && rechargePlan.getIreffID() != null
                        && rechargePlan.getIreffID().equals(rp.getIreffID())) {
                    return true;
                }
            }
        }
        return false;
    }

    public RechargePlan insertRechargePlan(final RechargePlan rechargePlan) throws RuntimeException {
        validate(rechargePlan);
        RechargePlan plan = this.rechargePlanWriteDAO.insertechargePlan(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }
    
    public RechargePlan insertRechargePlanWithoutValidation(final RechargePlan rechargePlan) throws RuntimeException {
    	int circleId = rechargePlan.getCircleMasterId();
        if (operatorCircleReadDAO.getCircle(circleId) == null) {
            throw new IllegalStateException("Invalid Circle Id - NULL.");
        }

        int operatorId = rechargePlan.getOperatorMasterId();
        if (operatorCircleReadDAO.getOperator(operatorId) == null) {
            throw new IllegalStateException("Invalid Operator ID - NULL");
        }
        RechargePlan plan = this.rechargePlanWriteDAO.insertechargePlan(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }
    

    public void insertRechargePlansWithModeration(final List<RechargePlan> allPlans) {
        List<String> stickyIreffIDs = rechargePlanReadDAO.getAllStickyIreffIds();
        List<RechargePlan> plansToBeAdded = new ArrayList<>();
        plansToBeAdded.addAll(allPlans);

        final List<RechargePlan> ambiguousPlans = new ArrayList<RechargePlan>();
        // eliminate sticky plans so that we insert only non-sticky ones
        for (RechargePlan each : allPlans) {
            if (each.getIreffID() != null && stickyIreffIDs.contains(each.getIreffID())) {
                ambiguousPlans.add(each);
                ambiguousPlans.addAll(rechargePlanReadDAO.getRechargePlansFromIreffId(each.getIreffID()));
            }
        }
        List<RechargePlanModerationCausePair> rechargePlanModerationCausePair = new ArrayList<>();
        for (RechargePlan rechargePlan : ambiguousPlans) {
            RechargePlanModerationCausePair rechargePlanCause = new RechargePlanModerationCausePair(rechargePlan,
                    MODERATION_CAUSE_ID_STICKY);
            rechargePlanModerationCausePair.add(rechargePlanCause);
        }
        List<RechargePlan> plansInsertedToDB = rechargePlanIngestJobUtil
                .processForModeration(rechargePlanModerationCausePair);
        plansToBeAdded.removeAll(plansInsertedToDB);
        this.rechargePlanWriteDAO.insertRechargePlans(plansToBeAdded);
        clearRechargePlansCache(plansToBeAdded);
    }

    public void insertRechargePlans(final List<RechargePlan> allPlans) {
        this.rechargePlanWriteDAO.insertRechargePlans(allPlans);
        clearRechargePlansCache(allPlans);
    }

    public void insertRechargePlanAllCircles(final RechargePlan rechargePlan) {
        List<CircleMaster> circleMasters = operatorCircleService.getCircles();
        List<RechargePlan> plans = new ArrayList<RechargePlan>();

        for (CircleMaster circleMaster : circleMasters) {
            if(circleMaster.getIsActive()) {
                RechargePlan rp = new RechargePlan();
                rp.setRechargePlanId(0);
                rp.setName(rechargePlan.getName());
                rp.setProductType(rechargePlan.getProductType());
                rp.setDenominationType(rechargePlan.getDenominationType());
                rp.setDescription(rechargePlan.getDescription());
                rp.setAmount(rechargePlan.getAmount());
                rp.setMinAmount(rechargePlan.getMinAmount());
                rp.setMaxAmount(rechargePlan.getMaxAmount());
                rp.setTalktime(rechargePlan.getTalktime());
                rp.setValidity(rechargePlan.getValidity());
                rp.setSticky(rechargePlan.isSticky());
                rp.setCircleMasterId(circleMaster.getCircleMasterId());
                rp.setOperatorMasterId(rechargePlan.getOperatorMasterId());
                plans.add(rp);
            }
        }

        for (RechargePlan plan : plans) {
            try {
                validate(plan);
                this.rechargePlanWriteDAO.insertechargePlan(plan);
                clearRechargePlansCache(plan);
            } catch (Exception e) {
                // Plan already exists and will be updated.
            }
        }
    }

    public RechargePlan updateRechargePlan(final RechargePlan rechargePlan) {
        RechargePlan plan = this.rechargePlanWriteDAO.updatePlan(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public RechargePlan forceUpdateRechargePlan(final RechargePlan rechargePlan)  {
        RechargePlan plan = this.rechargePlanWriteDAO.forceUpdatePlan(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public RechargePlan forceUpdateDescription(final RechargePlan rechargePlan)  {
        RechargePlan plan = this.rechargePlanWriteDAO.forceUpdateDescription(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public RechargePlan updateRechargePlanAllCircle(final RechargePlan rechargePlan)  {
        RechargePlan plan = this.rechargePlanWriteDAO.updatePlanAllCircle(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public RechargePlan updateRechargePlanStatus(final RechargePlan rechargePlan)  {
        RechargePlan plan = this.rechargePlanWriteDAO.updateStatus(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public RechargePlan updateRechargePlanRecomended(final RechargePlan rechargePlan)  {
        RechargePlan plan = this.rechargePlanWriteDAO.updateRecomended(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public List<RechargePlan> updateRechargePlanRecomended(final List<RechargePlan> rechargePlans)  {
        List<RechargePlan> plans = this.rechargePlanWriteDAO.updateRecomended(rechargePlans);
        clearRechargePlansCache(rechargePlans);
        return plans;
    }

    public List<RechargePlan> updateRechargePlanStatus(final List<RechargePlan> rechargePlans) {
        List<RechargePlan> plans = this.rechargePlanWriteDAO.updateStatus(rechargePlans);
        clearRechargePlansCache(rechargePlans);
        return plans;
    }

    public RechargePlan updateRechargePlanSticky(final RechargePlan rechargePlan) {
        RechargePlan plan = this.rechargePlanWriteDAO.updateSticky(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan;
    }

    public List<RechargePlan> updateRechargePlanSticky(final List<RechargePlan> rechargePlans) {
        List<RechargePlan> plans = this.rechargePlanWriteDAO.updateSticky(rechargePlans);
        clearRechargePlansCache(rechargePlans);
        return plans;
    }

    public void deleteNonStickyPlanFromDB(final RechargePlan rechargePlan) {
        this.rechargePlanWriteDAO.deleteNonStickyPlanFromDB(rechargePlan);
        clearRechargePlansCache(rechargePlan);
    }
    
    public void forceDeleteFromDB(final RechargePlan rechargePlan) {
        this.rechargePlanWriteDAO.forceDeleteFromDB(rechargePlan);
        clearRechargePlansCache(rechargePlan);
    }

    public void deleteNonStickyPlanFromDB(final List<RechargePlan> rechargePlans) {
        this.rechargePlanWriteDAO.deleteNonStickyPlanFromDB(rechargePlans);
        clearRechargePlansCache(rechargePlans);
    }

    public void deleteStickyDisabledPlanFromDB(final List<RechargePlan> rechargePlans) {
        this.rechargePlanWriteDAO.deleteStickyDisabledPlanFromDB(rechargePlans);
        clearRechargePlansCache(rechargePlans);
    }

    private void validate(final RechargePlan rechargePlan) throws RuntimeException {
        int circleId = rechargePlan.getCircleMasterId();
        if (operatorCircleReadDAO.getCircle(circleId) == null) {
            throw new IllegalStateException("Invalid Circle Id - NULL.");
        }

        int operatorId = rechargePlan.getOperatorMasterId();
        if (operatorCircleReadDAO.getOperator(operatorId) == null) {
            throw new IllegalStateException("Invalid Operator ID - NULL");
        }

        if (doesAmountNameExist(rechargePlan)) {
            throw new IllegalStateException(String.format("Amount-Name pair ( %s, %s) already exists.",
                    rechargePlan.getAmount(), rechargePlan.getName()));
        }
    }

    public boolean doesAmountNameExist(final RechargePlan rechargePlan) {
        int circleId = rechargePlan.getCircleMasterId();
        int operatorId = rechargePlan.getOperatorMasterId();
        List<RechargePlan> rechargePlans = this.rechargePlanReadDAO.getRechargePlans(circleId, operatorId);
        BigDecimal amount = rechargePlan.getAmount();
        String name = rechargePlan.getName();
        for (RechargePlan existingRechargePlan : rechargePlans) {
            BigDecimal existingRechargePlanAmount = existingRechargePlan.getAmount();
            if (FCUtil.areEqual(existingRechargePlanAmount, amount) && existingRechargePlan.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public RechargePlanReadDAO getRechargePlanReadDAO() {
        return rechargePlanReadDAO;
    }

    public void setRechargePlanReadDAO(final RechargePlanReadDAO rechargePlanReadDAO) {
        this.rechargePlanReadDAO = rechargePlanReadDAO;
    }

    public RechargePlanWriteDAO getRechargePlanWriteDAO() {
        return rechargePlanWriteDAO;
    }

    public void setRechargePlanWriteDAO(final RechargePlanWriteDAO rechargePlanWriteDAO) {
        this.rechargePlanWriteDAO = rechargePlanWriteDAO;
    }

    public OperatorCircleReadDAO getOperatorCircleReadDAO() {
        return operatorCircleReadDAO;
    }

    public void setOperatorCircleReadDAO(final OperatorCircleReadDAO operatorCircleReadDAO) {
        this.operatorCircleReadDAO = operatorCircleReadDAO;
    }

    public List<CircleMaster> getCircles() {
        return this.operatorCircleReadDAO.getCircles();
    }

    public List<OperatorMaster> getAllOperators() {
        return operatorCircleReadDAO.getOperators();
    }

    public OperatorMaster getOperator(int operatorId) {
        return operatorCircleReadDAO.getOperator(operatorId);
    }

    public List<String> getAllRechargePlanNames() {
        return rechargePlanReadDAO.getAllRechargePlanNames();
    }

    // Gets all the recharge plans from the moderation queue
    public List<DBObject> getAllRechargePlansInModeration() throws RuntimeException {
        List<DBObject> rechargePlanList = rechargePlanModerationRepository.getAllRechargePlan();
        return rechargePlanList;
    }

    public List<String> getDuplicatePlansInModeration(final BigDecimal amount, final String name, final int circleId,
            final int operatorId) throws RuntimeException {
        List<DBObject> rechargePlans = rechargePlanModerationRepository.getAllRechargePlan();
        List<String> planIds = new ArrayList<>();
        for (DBObject rechObject : rechargePlans) {
            if (FCUtil.areEqual(amount, new BigDecimal(rechObject.get(RechargePlanModerationRepository.AMOUNT)
                    .toString()))
                    && name.equals(rechObject.get(RechargePlanModerationRepository.NAME))
                    && rechObject.get(RechargePlanModerationRepository.CIRCLE).equals(circleId)
                    && rechObject.get(RechargePlanModerationRepository.OPERATOR).equals(operatorId)) {
                planIds.add(rechObject.get(RechargePlanModerationRepository.ID).toString());
            }
        }
        return planIds;
    }

    //Checks if plan id - name pair exists in moderation queue.
    public boolean checkPlanExistsInModeration(final RechargePlan rechargePlan) throws RuntimeException {
        List<DBObject> plansList = getAllRechargePlansInModeration();
        for (DBObject plan : plansList) {
            if (rechargePlan.getIreffID() != null
                    && plan.get(RechargePlanModerationRepository.SOURCE).toString().contains(rechargePlan.getIreffID())
                    && plan.get(RechargePlanModerationRepository.NAME).toString().equals(rechargePlan.getName())) {
                return true;
            }
        }
        return false;
    }

    // Get the recharge plan from the moderation queue with the given planID
    public DBObject getRechargePlanInModeration(final int planID) throws RuntimeException {
        DBObject rechargePlanObject = null;
        try {
            rechargePlanObject = rechargePlanModerationRepository.getRechargePlan(planID);
        } catch (RuntimeException e) {
            LOGGER.info("Plan does not exist in moderation queue ", e);
            throw e;
        }
        return rechargePlanObject;
    }

    // Get the recharge plan from the moderation queue with the given planID
    public boolean isRechargePlanInModeration(final int planID) {
        try {
            rechargePlanModerationRepository.getRechargePlan(planID);
        } catch (IllegalStateException e) {
            LOGGER.info("Plan does not exist in moderation queue ", e);
            return false;
        }
        return true;
    }

    // Deletes a recharge plan from the moderation queue and updates sticky identified by planID
    public void deleteRechargePlanFromModeration(final int planID, final boolean updateSticky) throws RuntimeException {
        rechargePlanModerationRepository.removeRechargePlan(planID);
        RechargePlan rechargePlan = rechargePlanReadDAO.getRechargePlan(planID);
        if (updateSticky) {
            rechargePlan.setSticky(true);
            rechargePlan.setCreatedTime(new Timestamp(new Date().getTime()));
            updateRechargePlanSticky(rechargePlan);
        }
    }
    
    public void deleteRechargePlanFromModeration(final int planID) {
        rechargePlanModerationRepository.removeRechargePlan(planID);
    }
    
    // Adds a recharge plan 'rechargePlan' to the moderation queue
    public boolean addRechargePlanToModeration(final RechargePlan rechargePlan, final String cause)
            throws RuntimeException {
        return rechargePlanModerationRepository.addRechargePlan(rechargePlan, cause);
    }

    // Updates an existing recharge plan in moderation
    public void saveRechargePlanToModeration(final RechargePlan bean) throws RuntimeException {
        BasicDBObject userSets = new BasicDBObject();
        BasicDBObject fieldSets = new BasicDBObject();
        fieldSets.put(RechargePlanModerationRepository.AMOUNT, bean.getAmount().toString());
        fieldSets.put(RechargePlanModerationRepository.TALKTIME, (bean.getTalktime() != null) ? bean.getTalktime().toString() : "");
        fieldSets.put(RechargePlanModerationRepository.NAME, bean.getName());
        fieldSets.put(RechargePlanModerationRepository.OPERATOR, bean.getOperatorMasterId());
        fieldSets.put(RechargePlanModerationRepository.CIRCLE, bean.getCircleMasterId());
        fieldSets.put(RechargePlanModerationRepository.VALIDITY, bean.getValidity());
        fieldSets.put(RechargePlanModerationRepository.DESC, bean.getDescription());
        userSets.put("$set", fieldSets);

        BasicDBObject searchQuery = new BasicDBObject().append(RechargePlanModerationRepository.ID,
                bean.getRechargePlanId());

        rechargePlanModerationRepository.updateRechargePlan(searchQuery, userSets);
    }

    // Updates the recharge plan in DB
    public void saveRechargeFromModerationToDB(final RechargePlan bean) throws RuntimeException {
        BigDecimal amount = bean.getAmount();
        BigDecimal talktime = bean.getTalktime();

        int rechargePlanId = bean.getRechargePlanId();
        RechargePlan rechargePlan = new RechargePlan();

        rechargePlan = getRechargePlan(rechargePlanId);
        if (rechargePlan == null) {
            throw new IllegalStateException(String.format("Recharge plan with plan id %s not found", rechargePlanId));
        }

        rechargePlan.setName(bean.getName());
        rechargePlan.setDescription(bean.getDescription());
        rechargePlan.setValidity(bean.getValidity());
        rechargePlan.setAmount(amount);
        rechargePlan.setCircleMasterId(bean.getCircleMasterId());
        rechargePlan.setOperatorMasterId(bean.getOperatorMasterId());
        rechargePlan.setTalktime(talktime);
        rechargePlan.setCreatedTime(new Timestamp(new Date().getTime()));
        rechargePlan.setStatus(true);
        forceUpdateRechargePlan(rechargePlan);
        updateRechargePlanStatus(rechargePlan);
    }

    // Checks if a plan similar to planAPI exists in DB.
    // If plan Exists Return the plan, else return null
    // Ignores sticky plans
    public RechargePlan getExistingSimilarPlan(final RechargePlan planAPI, final List<RechargePlan> allPlanDB) {
        List<RechargePlan> rp = new ArrayList<RechargePlan>();

        for (RechargePlan rechargePlan : allPlanDB) {
            if(!FCUtil.isEmpty(rechargePlan.getName())) {
                if (rechargePlan.getProductType() == planAPI.getProductType()
                        && rechargePlan.getAmount().intValue() == planAPI.getAmount().intValue()
                        && rechargePlan.getName().equals(planAPI.getName())
                        && rechargePlan.getOperatorMasterId() == planAPI.getOperatorMasterId()
                        && rechargePlan.getCircleMasterId() == planAPI.getCircleMasterId()) {
                    rp.add(rechargePlan);
                }
            }
        }

        if (!FCUtil.isEmpty(rp)) {
            // No need to insert planAPI to db if more than one similar active
            // plan Exists. This case should never come
            if (rp.size() > 1) {
                LOGGER.error(String
                        .format("More than one similar recharge plans exists for Circle: %s Operator : %s Amount : %s Category : %s",
                                planAPI.getCircleMasterId(), planAPI.getOperatorMasterId(), planAPI.getAmount(),
                                planAPI.getName()));
                for (RechargePlan rechargePlan : rp) {
                    planAPI.setRechargePlanId(rechargePlan.getRechargePlanId());
                    if (planAPI.toString().equals(rechargePlan.toString())) {
                        return rechargePlan;
                    }
                }
            }
            return rp.get(0);
        }
        return null;
    }

    public void disableAllPlansExcept(final List<RechargePlan> rechargePlans, final Integer operatorId,
            final Integer circleId) {
        rechargePlanWriteDAO.updateStatusAllPlansExcept(rechargePlans, operatorId.toString(), circleId.toString(),
                DISABLE);
    }

    public void resetRecommendedExcept(final List<RechargePlan> rechargePlans, final Integer operatorId,
            final Integer circleId) {
        rechargePlanWriteDAO.updateRecommendedAllPlansExcept(rechargePlans, operatorId.toString(), circleId.toString(),
                DISABLE);
    }

    public List<RechargePlan> getFilteredRecomendedPlans(List<RechargePlan> recommendedPlans) {
        List<RechargePlan> filteredRecommendedPlans = new ArrayList<>();
        filteredRecommendedPlans.addAll(recommendedPlans);
        for (RechargePlan rechargePlan: recommendedPlans) {
            if (isDuplicateRecommendedPlan(rechargePlan, filteredRecommendedPlans)) {
                filteredRecommendedPlans.remove(rechargePlan);
            }
        }
        return filteredRecommendedPlans;
    }

    private boolean isDuplicateRecommendedPlan(RechargePlan rechargePlan, List<RechargePlan> filteredRecommendedPlans) {
        for (RechargePlan rp: filteredRecommendedPlans) {
            if (!rp.equals(rechargePlan) && rp.getAmount().intValue() == rechargePlan.getAmount().intValue()) {
                if (rp == null || rp.getName() == null || rechargePlan.getName() == null) {
                    return false;
                }
                if ((rp.getName().equals(OTHER) || rp.getName().equals(DATA2G) || rp.getName().equals(DATA3G)) && rechargePlan.getName().equals(TOPUP)) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public void deletePlansNotInAPI(final List<RechargePlan> plansFromAPI, final List<RechargePlan> allPlanDB) {
        Boolean isPlanDelete;
        List<RechargePlan> listOfDisabledDbPlans = new ArrayList<RechargePlan>();
        for (RechargePlan rechargePlanDb : allPlanDB) {
            isPlanDelete = false;
            if (rechargePlanDb.isSticky() == true) {
                continue;
            }
            String planName = rechargePlanDb.getName();
            BigDecimal amount = rechargePlanDb.getAmount();
            for (RechargePlan rechargePlanApi : plansFromAPI) {
                try {
                    if (planName.equalsIgnoreCase(rechargePlanApi.getName())
                            && amount.intValue() == rechargePlanApi.getAmount().intValue()) {
                        isPlanDelete = false;
                        break;
                    } else {
                        isPlanDelete = true;
                    }
                } catch (NullPointerException e) {
                    LOGGER.error("NPE while deleting plans not in API. This is ok as they are plans inserted to moderation queue");
                }
            }
            if (isPlanDelete) {
                listOfDisabledDbPlans.add(rechargePlanDb);
            }
        }
        deleteNonStickyPlanFromDB(listOfDisabledDbPlans);
    }

    private void clearRechargePlansCache(List<RechargePlan> rechargePlans) {
        if (!FCUtil.isEmpty(rechargePlans)) {
            RechargePlan rechargePlan = rechargePlans.get(0);
            rechargePlanCacheManager.clearPlansCache(rechargePlan.getCircleMasterId(), rechargePlan.getOperatorMasterId(), rechargePlan.getProductType());
        }
    }

    private void clearRechargePlansCache(RechargePlan rechargePlan) {
        rechargePlanCacheManager.clearPlansCache(rechargePlan.getCircleMasterId(), rechargePlan.getOperatorMasterId(), rechargePlan.getProductType());
    }

    public int markPlanStatusAsInvalid(int circleId, int operatorId, int productId, float amount) {
        return inRechargePlanWriteDAO.updateStatus(circleId, operatorId, productId, amount, false);
    }
    
    public void deleteNonStickyRechargePlan(int circleId, int operatorId, int productId, float amount) {

        List<RechargePlan> plans = rechargePlanReadDAO.getRechargePlans(circleId, operatorId, productId, amount);

        if (plans == null || plans.isEmpty()) {
            return;
        }

        for (RechargePlan plan : plans) {
            if (isModQPlan(plan) && !plan.isSticky()) {
                deletePlanFromModQ(plan.getRechargePlanId());
            }
        }

        rechargePlanWriteDAO.deleteNonStickyRechargePlan(circleId, operatorId, productId, amount);
    }
    
    public void deletePlanFromModQ(int rechargePlanId) {
        try {
            deleteRechargePlanFromModeration(rechargePlanId);
        } catch (Exception e) {
            LOGGER.info("Plan not in moderation queue for planId " + rechargePlanId);
        }
    }

    private boolean isModQPlan(RechargePlan plan) {
        if (plan != null && plan.getIreffID() != null && plan.getIreffID().equals("MODQ")) {
            return true;
        }
        return false;
    }

    public boolean hasActiveRechargePlan(int circleId, int operatorId, int productId, float amount) {
        return inRechargePlanReadDAO.hasActiveRechargePlan(circleId, operatorId, productId, amount);
    }

    public RechargePlan getActiveRechargePlan(int circleId, int operatorId, int productId, float amount) {
        /*List<RechargePlan> inActiveRechargePlans =inRechargePlanReadDAO.getInActiveRechargePlans(amount, circleId, operatorId,
                productId);
        if (!FCUtil.isEmpty(inActiveRechargePlans)) {
            for (RechargePlan inActiveRechargePlan : inActiveRechargePlans) {
                if (inActiveRechargePlan.getIreffID() != null && !inActiveRechargePlan.getIreffID().equals("MODQ")) {
                    this.deleteNonStickyPlanFromDB(inActiveRechargePlan);
                }
            }
        }*/
        
        List<RechargePlan> inRechargePlans = inRechargePlanReadDAO.getActiveRechargePlan(amount, circleId, operatorId,
                productId);

        if (FCUtil.isEmpty(inRechargePlans)) {
            return null;
        }

        if (inRechargePlans.size() == 1) {
            return inRechargePlans.get(0);
        }
        LOGGER.error("Mutliple plans found for the amount " + amount + " , circle " + circleId + ", operator "
                + operatorId + ", product id " + productId + " Marking duplicate plans as inactive.");

        RechargePlan plan = inRechargePlans.remove(0);
        for (RechargePlan each : inRechargePlans) {
            this.deleteNonStickyPlanFromDB(each);
        }
        return plan;
    }

    public RechargePlan getRechargePlanFor(int circleId, int operatorId, int productId, float amount, String ireffId) {
        return inRechargePlanReadDAO.getRechargePlan(amount, circleId, operatorId, productId);
    }

    public RechargePlan getRechargePlanForIreffId(int circleId, int operatorId, int productId, float amount, String ireffId) {
        List<RechargePlan> inRechargePlans = inRechargePlanReadDAO.getRechargePlanForIreffId(amount, circleId, operatorId,
                productId, ireffId);

        if (FCUtil.isEmpty(inRechargePlans)) {
            return null;
        }

        if (inRechargePlans.size() == 1) {
            return inRechargePlans.get(0);
        }
        LOGGER.error("Mutliple plans found for the amount " + amount + " , circle " + circleId + ", operator "
                + operatorId + ", product id " + productId + " Marking duplicate plans as inactive.");

        RechargePlan plan = inRechargePlans.remove(0);
        for (RechargePlan each : inRechargePlans) {
            each.setStatus(false);
            LOGGER.info ("Removing the moderation plan with id " + each.getRechargePlanId());
            this.deleteNonStickyPlanFromDB(each);
        }
        return plan;
    }
    
    public List<RechargePlan> getStickyRechargePlan(int circleId, int operatorId, int productId, float amount) {
        return inRechargePlanReadDAO.getStickyRechargePlan(amount, circleId, operatorId, productId);
    }

    public void storeRechargePlans(List<RechargePlan> rechargePlans) {
        inRechargePlanWriteDAO.insertRechargePlans(rechargePlans);
        clearRechargePlansCache(rechargePlans);
    }

    public Integer storeRechargePlan(RechargePlan rechargePlan) {
        RechargePlan  plan = rechargePlanWriteDAO.insertechargePlan(rechargePlan);
        clearRechargePlansCache(rechargePlan);
        return plan.getRechargePlanId();
    }

    public int updatePlanRecommendation(int planId, boolean IsRecommended) {
        return inRechargePlanWriteDAO.updatePlanRecommendation(planId, IsRecommended);
    }
    
    public enum EmailAction {
        STICKY_PLAN_EXPIRED, MODERATION_PLAN_DELETED_AS_DATA_AVAILABLE, MODERATION_PLAN_DELETED_AS_PLAN_INVALID
    }
    
    public void sendEmail(RechargePlan rechargePlan, EmailAction emailAction) {
        if (fcProperties.isProdMode()) {
            EmailBusinessDO ebdo = new EmailBusinessDO();
            ebdo.setFrom(fcProperties.getProperty(FCConstants.RECHARGE_PLAN_EMAIL_NOTIFICATION_FROM));
            ebdo.setReplyTo(fcProperties.getProperty(FCConstants.RECHARGE_PLAN_EMAIL_NOTIFICATION_TO));
            ebdo.setTo(fcProperties.getProperty(FCConstants.RECHARGE_PLAN_EMAIL_NOTIFICATION_TO));
            ebdo.setSubject(String.format("Recharge Plan: (%s) Operator- %s, Circle- %s, Amount- %s, Category- %s",
                    emailAction.name(), rechargePlan.getOperatorMasterId(), rechargePlan.getCircleMasterId(),
                    rechargePlan.getAmount(), rechargePlan.getName()));
            String br = "<br>\n";
            String emailContent = "Plan action : " + emailAction.name() + br + new Gson().toJson(rechargePlan);
            ebdo.setContent(emailContent.replace("\n", ""));
            emailService.sendNonVelocityEmail(ebdo);
        }
    }
    
}