package com.freecharge.productdata.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.batch.job.RechargePlanIngestJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.dao.jdbc.IreffPlan;
import com.freecharge.productdata.dao.jdbc.IreffPlanDAO;
import com.freecharge.productdata.dao.jdbc.IreffPlanReadDAO;
import com.freecharge.productdata.entity.IreffPlanData;
import com.freecharge.productdata.util.IreffConstants;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class IreffDataImportService {
    private static final Logger                logger                 = LoggingFactory
                                                                              .getLogger(IreffDataImportService.class);

    private static final String                IREFF_URL_FORMAT       = "http://api.ireff.in/partner/v1/plans?key=%s&circle=%s&service=%s";
    private static final String                IREFF_CATEGORIES_URL   = "http://api.ireff.in/partner/v1/categories?key=%s";

    private static final String                IREFF_FC_KEY           = "0e49fde47a63776ee63ced856b581c47191d17f4";
    private static final String                IREFF_PLAN_IGNORE_TEXT = "Available only on self activation";

    @Autowired
    @Qualifier("ireffConnectionManager")
    private MultiThreadedHttpConnectionManager connectionManager;

    @Autowired
    private AmazonDynamoDBAsync                dynamoClient;

    @Autowired
    private RechargePlanService                rechargePlanService;

    @Autowired
    private RechargePlanIngestJobUtil          rechargePlanIngestJobUtil;

    @Autowired
    private IreffPlanDAO                       ireffPlanDAO;
    
    @Autowired
    private IreffPlanReadDAO                   ireffPlanReadDAO;     

    // This function is used to update the ireff plans in the dynamo DB repo by
    // quering the data from ireff.
    public void updateIreffRepo() {
        // get circle name - ID map
        List<CircleMaster> circles = rechargePlanService.getCircles();
        // get operator name - ID map
        List<OperatorMaster> operators = rechargePlanService.getAllOperators();

        for (CircleMaster circleMaster : circles) {

            String ireffCircle = IreffConstants.CIRCLE_IREFF_LOOKUP.get(circleMaster.getName());
            if (StringUtils.isEmpty(ireffCircle)) {
                logger.error("ireff Circle is empty for the circle: " + circleMaster.getName());
                continue;
            }

            for (OperatorMaster operatorMaster : operators) {
                String ireffService = IreffConstants.SERVICE_IREFF_LOOKUP.get(operatorMaster.getName());

                if (StringUtils.isEmpty(ireffService)) {
                    logger.error("ireff Service is empty for the operator: " + operatorMaster.getName());
                    continue;
                }

                logger.info("Getting ireff plan list for circle: " + ireffCircle + " and service: " + ireffService);
                List<IreffPlanData> ireffDataList = getIreffPlanData(ireffCircle, ireffService);
                if (ireffDataList == null || ireffDataList.isEmpty()) {
                    continue;
                }
                logger.info("updating the fetched ireff plans in DB");
                // modify the plans as needed and put the data into DB.
                modifyAndUpdateIreffPlanData(ireffDataList, operatorMaster, circleMaster.getCircleMasterId());
            }
        }
    }

    private String getDescriptionData(final IreffPlanData rp, final String fcOperator) {

        final List<String> descriptions = rp.getDetail();
        if (descriptions == null || descriptions.isEmpty() || descriptions.get(0).equals("")) {
            if (rp.getCategory().equals(IreffConstants.CATEGORY_TOPUP)) {
                if (rp.getTalktime().compareTo(rp.getPrice()) >= 0) {
                    return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                            IreffConstants.EMPTY_DESC_TOPUP_FULL, rp.getCategory());
                } else {
                    return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                            String.format(IreffConstants.EMPTY_DESC_TOPUP_REGULAR, rp.getTalktime(), rp.getPrice()),
                            rp.getCategory());
                }
            } else {
                return "";
            }
        } else {
            if (descriptions != null && descriptions.size() >= 2) {
                return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                        FCUtil.delimitedString("|", descriptions), rp.getCategory());
            }
            return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator, (String) descriptions.get(0),
                    rp.getCategory());
        }
    }

    /*
     * Modify the plan ireff plan to data as needed.
     */
    private void modifyAndUpdateIreffPlanData(List<IreffPlanData> ireffDataList, OperatorMaster operatorMaster,
            int circleId) {

        for (IreffPlanData plan : ireffDataList) {

            try {

                // Do not store the plans which contains the mentioned text.
                if (!FCUtil.isEmpty(plan.getDetail()) && plan.getDetail().contains(IREFF_PLAN_IGNORE_TEXT)) {
                    continue;
                }
                IreffPlan ireffPlan = new IreffPlan();
                ireffPlan.setCircleId(circleId);
                ireffPlan.setOperatorId(operatorMaster.getOperatorMasterId());
                String category = plan.getCategory();
                ireffPlan.setCategory(category);

                // mark the category as "Other" for "Local", "SMS" etc,...
                // We only need to categorize the plans in
                // IreffConstants.validCategoryList.
                if (!IreffConstants.validCategoryList.contains(category)) {
                    ireffPlan.setCategory(IreffConstants.CATEGORY_OTHER);
                }
                ireffPlan.setIreffId(plan.getId());
                ireffPlan.setAmount(plan.getPrice());
                if (plan.getTalktime() != null) {
                    ireffPlan.setTalktime(plan.getTalktime());
                } else {
                    ireffPlan.setTalktime(0.0d);
                }
                ireffPlan.setValidity(plan.getValidity());
                ireffPlan.setDetail(getDescriptionData(plan, operatorMaster.getName()));
                ireffPlan.setSourceUri(plan.getSourceUri());

                // update DB
                this.updateIreffPlan(ireffPlan);
            } catch (Exception e) {
                logger.error("Some error while updating the ireff plan", e);
            }
        }
    }

    private boolean hasSimilarsPlanDetails(IreffPlan existingPlan, IreffPlan newIreffPlan) {
        if (!existingPlan.getTalktime().equals(newIreffPlan.getTalktime())) {
            return false;
        }
        if (existingPlan.getValidity() != null && !existingPlan.getValidity().equals(newIreffPlan.getValidity())) {
            return false;
        }

        if (existingPlan.getDetail() != null && !existingPlan.getDetail().equals(newIreffPlan.getDetail())) {
            return false;
        }
        return true;
    }

    /*
     * This function updates the updated time if the plan already exists, else
     * insert the plan.
     */
    public void updateIreffPlan(IreffPlan newIreffPlan) {
        try {
            IreffPlan existingPlan = ireffPlanReadDAO.getActiveIreffPlansForAmount(newIreffPlan);

            // Add plan to DB.
            if (existingPlan == null) {
                ireffPlanDAO.insertIreffPlan(newIreffPlan);
                return;
            }

            logger.info("similar plan with plan_id " + existingPlan.getPlanId() + " exists for the same amount: "
                    + newIreffPlan.getAmount() + "  , operator = " + newIreffPlan.getOperatorId() + " and circle = "
                    + newIreffPlan.getCircleId());

            // Plan has not changed; No need to update.
            if (this.hasSimilarsPlanDetails(existingPlan, newIreffPlan)) {
                ireffPlanDAO.updateTime(existingPlan);
                return;
            }

            ireffPlanDAO.disableIreffPlan(existingPlan.getPlanId());
            ireffPlanDAO.insertIreffPlan(newIreffPlan);
        } catch (Exception e) {
            logger.error("Some error while updating the plan ", e);
        }
    }

    /*
     * Gets data from the ireff url and return the parsed data as IreffPlanData.
     */
    private List<IreffPlanData> getIreffPlanData(String ireffCircle, String ireffService) {

        // First, get the data from URL.
        String ireffURL = String.format(IREFF_URL_FORMAT, IREFF_FC_KEY, ireffCircle, ireffService);
        String rawIreffPlanData = getDataFromIreffUrl(ireffURL);
        if (StringUtils.isEmpty(rawIreffPlanData)) {
            logger.error("Some error while getting data from ireff for the circle: " + ireffCircle + " and service: "
                    + ireffService);
            return null;
        }

        if (rawIreffPlanData.equals("[]")) {
            logger.error("No plans returned from ireff for the circle: " + ireffCircle + " and service: "
                    + ireffService);
            return null;
        }

        // Convert the raw data into IreffPlanData list and return.
        // Get the json for the date format used in ireff.
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
        Type ireffPlanListtype = new TypeToken<List<IreffPlanData>>() {
        }.getType();
        List<IreffPlanData> ireffDataList = gson.fromJson(rawIreffPlanData, ireffPlanListtype);
        return ireffDataList;
    }

    private String getDataFromIreffUrl(String ireffURL) {
        try {
            String body = null;
            HttpClient client = new HttpClient(connectionManager);
            HttpMethod txn = new GetMethod(ireffURL);
            logger.info("Making request to Ireff URL: " + ireffURL);
            int code = client.executeMethod(txn);
            logger.info("Ireff request returned HTTP response code " + code);
            if (code == HttpServletResponse.SC_OK) {
                body = txn.getResponseBodyAsString();
            }
            return body;
        } catch (RuntimeException | IOException e) {
            logger.error(String.format("Failed to get plans using API call : %s", ireffURL), e);
        }
        return null;
    }

}