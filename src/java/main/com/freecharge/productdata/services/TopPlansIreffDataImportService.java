package com.freecharge.productdata.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.batch.job.RechargePlanIngestJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashMapIgnoreCase;
import com.freecharge.common.util.SuccessDenominationCount;
import com.freecharge.common.util.SuccessDenominationType;
import com.freecharge.productdata.entity.DenominationType;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.recharge.collector.RechargeMetricService;

@Service
public class TopPlansIreffDataImportService {
    private static final Logger LOGGER = LoggingFactory.getLogger(TopPlansIreffDataImportService.class);

    private static final String IREFF_ID                                = "id";
    private static final String DETAIL                                  = "detail";
    private static final String CATEGORY                                = "category";
    private static final String PRICE                                   = "price";
    private static final String SERVICE                                 = "service";
    private static final String CIRCLE                                  = "circle";
    private static final String TALKTIME                                = "talktime";
    private static final String VALIDITY                                = "validity";
    private static final String EMPTY_DESC_TOPUP_FULL                   = "Full Talktime";
    private static final String EMPTY_DESC_TOPUP_REGULAR                = "Talktime of Rs. %s on a recharge of Rs. %s";
    private static final String TOPUP                                   = "Topup";
    private static final String DATA2G                                  = "Data/2G";
    private static final String DATA3G                                  = "3G";
    private static final String OTHER                                   = "Other";
    private static final String DELIMITER                               = " | ";

    private static final String IREFF_FC_KEY                            = "0e49fde47a63776ee63ced856b581c47191d17f4";

    /*
     * Eventually looks like:
     * http://api.ireff.in/partner/v1/plans?key=KEY&circle
     * =CIRCLE&service=SERVICE
     */
    private static final String IREFF_URL_FORMAT = "http://api.ireff.in/partner/v1/plans?key=%s&circle=%s&service=%s";
    private static final String IREFF_CATEGORIES_URL = "http://api.ireff.in/partner/v1/categories?key=%s";

    // The maximum recharge amount till which recharge plan status is true
    private static final int                   MAX_AMOUNT                              = 1000;
    private static final int                   MIN_AMOUNT                              = 10;
    private static final int                   CIRCLE_ID_ALL                           = 33;
    private static final int                   RECOMMENDED_THRESHOLD                   = 30;

    private static final int                   MIN_SUCCESS_DENOMINATION_RECHARGE_COUNT = 30;
    private static final Double                BENCHMARK_SUCCESS_RATE                  = 70d;

    /*
     * ---- Example Ireff JSON response ----
     * {"detail":"Local A2A: 5 Min Valid for 1 Day", "category":"topup",
     * "keywords":"topup, local", "price":68, "updated":"2013-05-13 20:35:37.0",
     * "service":"Aircel", "sourceUri":"http://www.aircel.com",
     * "circle":"Assam", "talktime":68}
     */

    @Autowired
    private RechargePlanService                 rechargePlanService;

    @Autowired
    private RechargePlanIngestJobUtil           rechargePlanIngestJobUtil;

    @Autowired
    private RechargeMetricService               rechargeMetricService;

    @Autowired
    @Qualifier("ireffConnectionManager")
    private MultiThreadedHttpConnectionManager  connectionManager;

    private static String getIreffURL(final String ireffCircle, final String ireffService) {
        return String.format(IREFF_URL_FORMAT, IREFF_FC_KEY, ireffCircle, ireffService);
    }

    @Transactional
    private void ingest(final List<RechargePlan> plans) {
        // Inserts plans which are new - Incremental insert
        rechargePlanService.insertRechargePlans(plans);
    }

    private static String getIreffCategoriesUrl() {
        return String.format(IREFF_CATEGORIES_URL, IREFF_FC_KEY);
    }

    // Returns the Categories to be used in the DB
    private static final List<String> getDBCategoryList() {
        List<String> myCategoryList = new ArrayList<>();
        myCategoryList.add(TOPUP);
        myCategoryList.add(DATA2G);
        myCategoryList.add(DATA3G);
        myCategoryList.add(OTHER);
        return myCategoryList;
    }

    // Function to parse the API data and return the Recharge Plans
    private List<RechargePlan> parse(final String rechargePlanString, final int circleMasterID,
            final String ireffCircleName, final int operatorMasterID, final String ireffServiceName,
            final Map<String, String> rpcMap) throws IOException, ParseException {
        Reader rechargePlanReader = new StringReader(rechargePlanString);
        final List<String> myCategoryList = getDBCategoryList();
        boolean isSpecialOperator = false;
        if (rechargePlanService.SPECIAL_OPERATOR_IDS.contains(operatorMasterID)) {
            isSpecialOperator = true;
        }
        OperatorMaster operatorMaster = rechargePlanService.getOperator(operatorMasterID);
        String fcOperator = operatorMaster.getName();
        Object deserializedRechargePlan = null;
        try {
            deserializedRechargePlan = new JSONParser().parse(rechargePlanReader);
            JSONArray array = (JSONArray) deserializedRechargePlan;
            List<RechargePlan> result = new ArrayList<>();

            for (Object each : array) {
                JSONObject row = validateAndGetRow(each, ireffCircleName, ireffServiceName);
                RechargePlan rp = new RechargePlan();
                final String exceptionFormat = "Encountered %s while parsing " + row.toString() + " for circle="
                        + ireffCircleName + ", service=" + ireffServiceName;
                try {
                    String categoryName = (String) row.get(CATEGORY);
                    rp.setName(getCategoryName(categoryName, rpcMap, myCategoryList));

                    rp.setAmount(new BigDecimal("" + row.get(PRICE)));
                    final String talktime = row.get(TALKTIME) == null ? "0" : ("" + row.get(TALKTIME));
                    rp.setTalktime(new BigDecimal(talktime));
                    final String validity = (row.get(VALIDITY) == null) ? ("N.A") : ((String) row.get(VALIDITY));
                    rp.setValidity(validity);
                    rp.setDenominationType(DenominationType.FIXED_DENOMINATION.getName());
                    rp.setProductType(ProductMaster.ProductName.Mobile.getProductId());
                    rp.setCircleMasterId(circleMasterID);
                    rp.setOperatorMasterId(operatorMasterID);
                    rp.setIreffID("" + row.get(IREFF_ID));
                    rp.setIsRecommended(false);
                    rp.setCreatedTime(new Timestamp(new Date().getTime()));

                    JSONArray descriptions = null;
                    descriptions = (JSONArray) row.get(DETAIL);
                    // Setting Descriptions for empty descriptions from api
                    rp.setDescription(getDescriptionData(rp, descriptions, fcOperator));
                    // Do not process recharge amounts > maximum allowable topup
                    if (Double.parseDouble(row.get(PRICE).toString()) > MAX_AMOUNT
                            || Double.parseDouble(row.get(PRICE).toString()) < MIN_AMOUNT) {
                        rp.setStatus(false);
                    }
                    // Check if a duplicate plan exist for same category
                    if (!rechargePlanIngestJobUtil.checkIfDuplicate(rp, result)) {
                        if (!isSpecialOperator) {
                            result.add(rp);
                        } else {
                            // For special operators remove Topup plan if it is a special recharge.
                            result.add(rp);
                            result.removeAll(getPlansTobeRemoved(rp, result));
                        }
                    }
                } catch (NumberFormatException nfe) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "NumberFormatException"), nfe);
                } catch (ClassCastException cce) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "ClassCastException"), cce);
                } catch (NullPointerException npe) {
                    throw new IllegalArgumentException(String.format(exceptionFormat, "NullPointerException"), npe);
                }
            }
            return result;
        } catch (RuntimeException e) {
            LOGGER.error("Encountered RuntimeException while parsing Ireff data.", e);
            throw e;
        }
    }
    
    private JSONObject validateAndGetRow(Object each, String ireffCircleName, String ireffServiceName)
            throws IllegalStateException {
        if (!(each instanceof JSONObject)) {
            throw new RuntimeException("Expected a map element, but found " + each);
        }
        JSONObject row = (JSONObject) each;
        if (!ireffCircleName.equals(row.get(CIRCLE))) {
            String msg = "Expected circle name in JSON response to be '%s', but found '%s'";
            throw new IllegalStateException(String.format(msg, ireffCircleName, row.get(CIRCLE)));
        }
        if (!ireffServiceName.equals(row.get(SERVICE))) {
            String msg = "Expected service name in JSON response to be '%s', but found '%s'";
            throw new IllegalStateException(String.format(msg, ireffServiceName, row.get(SERVICE)));
        }
        return row;
    }
    
    private String getCategoryName(final String categoryName, final Map<String, String> rpcMap,
            final List<String> myCategoryList) {
        if (categoryName != null && !categoryName.isEmpty()) {
            String category = rpcMap.get(categoryName);
            if (myCategoryList.contains(category)) {
                return category;
            } else {
                return OTHER;
            }
        } else {
            return OTHER;
        }
    }

    private List<RechargePlan> getPlansTobeRemoved(final RechargePlan rp, final List<RechargePlan> result) {
        List<RechargePlan> rechargePlans = rechargePlanIngestJobUtil.getAllInvalidPlansFromList(rp, result);
        List<RechargePlan> plansToBeRemoved = new ArrayList<>();
        if (rechargePlans.size() > 1) {
            for (RechargePlan rechargePlan : rechargePlans) {
                if (rechargePlan.getName().equals(TOPUP)) {
                    plansToBeRemoved.add(rechargePlan);
                }
            }
        }
        return plansToBeRemoved;
    }

    private String getDescriptionData(final RechargePlan rp, final JSONArray descriptions, final String fcOperator) {
        if (descriptions == null || descriptions.isEmpty() || descriptions.get(0).equals("")) {
            if (rp.getName().equals(TOPUP)) {
                if (rp.getTalktime().compareTo(rp.getAmount()) >= 0) {
                    return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator, EMPTY_DESC_TOPUP_FULL,
                            rp.getName());
                } else {
                    return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                            String.format(EMPTY_DESC_TOPUP_REGULAR, rp.getTalktime(), rp.getAmount()),rp.getName());
                }
            } else {
                return "";
            }
        } else {
            if (descriptions != null && descriptions.size() >= 2) {
                return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator,
                        FCUtil.delimitedString(DELIMITER, descriptions), rp.getName());
            }
            return rechargePlanIngestJobUtil.appendOperatorToDesc(fcOperator, (String) descriptions.get(0),
                    rp.getName());
        }
    }

    private List<SuccessDenominationType> getTopValidDenominationTypes(final SuccessDenominationCount[] topRechargeDenominations,
            final Integer circleId, final Integer operatorId) {
        List<SuccessDenominationType> topValidDenominationTypes = new ArrayList<>();
        // Checks if the denomination is a part of an invalidDenomination.
        for (int i = 0; i < topRechargeDenominations.length; i++) {
            BigDecimal amount = topRechargeDenominations[i].getAmount();
            Double count = topRechargeDenominations[i].getCount();
            boolean isSpecial = topRechargeDenominations[i].isSpecial();
            if (count < MIN_SUCCESS_DENOMINATION_RECHARGE_COUNT) {
                break;
            }
            String rechargeType = FCConstants.RECHARGE_TYPE_TOPUP;
            if (topRechargeDenominations[i].isSpecial()) {
                rechargeType = FCConstants.RECHARGE_TYPE_SPECIAL;
            }
            if (rechargeMetricService.isInvalidDenomination(operatorId.toString(), circleId.toString(),
                    amount.doubleValue(), BENCHMARK_SUCCESS_RATE, rechargeType)) {
                continue;
            }
            SuccessDenominationType successDenominationType = new SuccessDenominationType();
            successDenominationType.setAmount(BigDecimal.valueOf(amount.intValue()));
            successDenominationType.setSpecial(isSpecial);
            topValidDenominationTypes.add(successDenominationType);
        }
        return topValidDenominationTypes;
    }

    // Main function to update the Recharge Plans DB
    public void fetchUpdate(final Boolean isPopular) throws RuntimeException, IOException, ParseException {
        // get circle name - ID map
        List<CircleMaster> circles = rechargePlanService.getCircles();
        HashMapIgnoreCase<Integer> circleMap = rechargePlanIngestJobUtil.getCircleMap(circles);

        // get operator name - ID map
        List<OperatorMaster> operators = rechargePlanService.getAllOperators();
        HashMapIgnoreCase<Integer> operatorMap = rechargePlanIngestJobUtil.getOperatorMap(operators);

        // Fetching the Ireff categories list
        String ireffCategoriesURL = null;
        ireffCategoriesURL = getIreffCategoriesUrl();
        String ireffRechargePlanCategoryString = getIreffPlans(ireffCategoriesURL);
        Map<String, String> rpcMap = rechargePlanIngestJobUtil
                .setRechargePlanCategoryMap(ireffRechargePlanCategoryString);

        for (CircleMaster fcCircleMaster : circles) {
            String fcCircle = fcCircleMaster.getName();
            String ireffCircle = CIRCLE_IREFF_LOOKUP.get(fcCircle);
            Integer circleId = circleMap.get(fcCircle);
            if (circleId == null || circleId == CIRCLE_ID_ALL) {
                continue;
            }
            for (OperatorMaster fcOperatorMaster : operators) {
                // Stores top plans available in Ireff
                List<RechargePlan> ireffPlans = new ArrayList<>();
                // Stores plans in rechargePlansCreated and not in DB (Pushed to
                // moderation queue)
                List<RechargePlanModerationCausePair> moderationCausePairs = new ArrayList<>();
                // Stores all plans in top denominations that need to be active.
                List<RechargePlan> allPlansToBeActive = new ArrayList<>();
                // Stores new plans in top denomination to be inserted to the
                // db.
                List<RechargePlan> dbIngestPlans = new ArrayList<>();
                // Stores recommended plans
                List<RechargePlan> recommendedPlans = new ArrayList<>();

                String fcOperator = fcOperatorMaster.getOperatorCode();
                String ireffOperator = SERVICE_IREFF_LOOKUP.get(fcOperator);
                Integer operatorId = operatorMap.get(fcOperator);
                if (operatorId == null) {
                    continue;
                }
                LOGGER.info(String.format("\n\n=========================== %s - %s ===========================",
                        fcOperator, fcCircle));
                LOGGER.info(String.format("Fetching top denominations for operator: %s and circle: %s.", fcOperator,
                        fcCircle));
                // Fetching the top recharge denominations (SORTED by count) for
                // a circle-operator from redis database.
                SuccessDenominationCount[] topRechargeDenominations = rechargeMetricService
                        .getTopRechargeDenominations(operatorId, circleId);
                /* if (topRechargeDenominations.length > 0) { */
                List<SuccessDenominationType> topValidDenominationTypes = null;
                if (topRechargeDenominations.length > 0) {
                    LOGGER.info("Filtering out top denominations which are invalid.");
                    topValidDenominationTypes = getTopValidDenominationTypes(topRechargeDenominations, circleId,
                            operatorId);

                }
                // Now `topValidDenominationTypes` - Contains the top valid
                // denominations and their rechargeType (Special/Topup)
                /* if (!FCUtil.isEmpty(topValidDenominationTypes)) { */
                LOGGER.info(String.format("Fetching all plans from database for operator: %s and circle: %s",
                        fcOperator, fcCircle));
                // Getting all the plans in DB for a circle and operator
                List<RechargePlan> allPlansDB = rechargePlanService.getRechargePlans(circleId, operatorId);
                String ireffRechargePlanResponseString = null;
                if (!FCUtil.isEmpty(ireffCircle) && !FCUtil.isEmpty(ireffOperator)) {
                    try {
                        LOGGER.info(String.format("Fetching plans from ireff for operator: %s and circle: %s",
                                fcOperator, fcCircle));
                        String ireffURL = getIreffURL(ireffCircle, ireffOperator);
                        ireffRechargePlanResponseString = getIreffPlans(ireffURL);
                    } catch (Exception e) {
                        LOGGER.warn(String.format("Unable to fetch API data for Operator : %s and Circle : %s",
                                fcOperator, fcCircle), e);
                    }
                } else {
                    LOGGER.warn(String.format("No Ireff operator/circle for operator: %s and circle: %s", fcOperator,
                            fcCircle));
                }
                List<RechargePlan> plansFromAPI = new ArrayList<>();
                if (!FCUtil.isEmpty(ireffRechargePlanResponseString)) {
                    // Ireff has data for operator - circle. Parse to get
                    // all the plans.
                    LOGGER.info("Parsing recharge plans from Ireff.");
                    plansFromAPI = parse(ireffRechargePlanResponseString, circleId, ireffCircle, operatorId,
                            ireffOperator, rpcMap);
                }
                int count = 0;
                // Loop through topDenomination and check if plan is
                // there in Ireff.
                if (topValidDenominationTypes != null && topValidDenominationTypes.size() > 0) {
                    for (SuccessDenominationType topAmountType : topValidDenominationTypes) {

                        if (rechargeMetricService.isAirtelInvalidDenomination(fcOperator, fcCircle, topAmountType
                                .getAmount().doubleValue())
                                || rechargeMetricService.isAirtelInvalidDenominationCheck(fcOperator, fcCircle,
                                        topAmountType.getAmount().doubleValue())) {
                            LOGGER.info(String.format(
                                    "Amount : %s, Operator: %s , Circle: %s is evaulated invalid. Hence skipping",
                                    topAmountType.getAmount(), fcOperator, fcCircle));
                            continue;
                        }

                        LOGGER.info(String.format("Checking if amount : %s, rechargeType: %s exist in ireff",
                                topAmountType.getAmount(), topAmountType.isSpecial()));
                        // Fetching similar plans in the API for an amount
                        List<RechargePlan> rechargePlansFromAPI = rechargePlanIngestJobUtil.getSimilarPlansInAPI(
                                plansFromAPI, topAmountType.getAmount(), topAmountType.isSpecial(), operatorId);
                        // No similar plans found
                        if (FCUtil.isEmpty(rechargePlansFromAPI)) {
                            LOGGER.info("No similar plans found. Checking if plan in already in database.");

                            RechargePlan rechargePlanCreated = rechargePlanIngestJobUtil.createRechargePlanFromAmount(
                                    operatorId, circleId, topAmountType.getAmount(), count < RECOMMENDED_THRESHOLD);
                            // Checks plans availability in database. If not
                            // available adds to moderation queue.
                            RechargePlanModerationCausePair rechargePlanModerationCausePair = rechargePlanIngestJobUtil
                                    .getRechargePlanToBeModerated(rechargePlanCreated, allPlansDB,
                                            topAmountType.isSpecial());
                            if (rechargePlanModerationCausePair != null) {
                                LOGGER.info("Not in database. Plan is to be moderated. " + rechargePlanCreated);
                                moderationCausePairs.add(rechargePlanModerationCausePair);
                                recommendedPlans.add(rechargePlanModerationCausePair.getRechargePlan());
                            } else {
                                LOGGER.info("Similar plan exist in database.");
                                // Similar plan is found in database. Add it
                                // to the to be active plans list.
                                List<RechargePlan> similarPlansInDb = rechargePlanIngestJobUtil
                                        .getSimilarExistingPlans(rechargePlanCreated, allPlansDB,
                                                topAmountType.isSpecial());
                                allPlansToBeActive.addAll(similarPlansInDb);
                                if (count < RECOMMENDED_THRESHOLD) {
                                    recommendedPlans.addAll(similarPlansInDb);
                                    rechargePlanIngestJobUtil.setRecomendedPlans(similarPlansInDb);
                                }
                                // Enable any plan existing in database and not
                                // in moderation queue
                                rechargePlanIngestJobUtil.enablePlansNotInModeration(similarPlansInDb);
                            }
                        } else {
                            LOGGER.info("Similar plans exists in Ireff.");
                            // A similar plan exists in Ireff.
                            for (RechargePlan rechargePlan : rechargePlansFromAPI) {
                                if (count < RECOMMENDED_THRESHOLD) {
                                    rechargePlan.setIsRecommended(true);
                                    recommendedPlans.add(rechargePlan);
                                }
                            }
                            ireffPlans.addAll(rechargePlansFromAPI);
                        }
                        count++;
                    }
                }
                // deleting the plans from the database which are not present in
                // ireff api and having sticky status 0

                rechargePlanService.deletePlansNotInAPI(plansFromAPI, allPlansDB);

                // Checks if the ireffPlans already exists in database.
                for (RechargePlan rechargePlan : plansFromAPI) {

                    String rechargeType = FCConstants.RECHARGE_TYPE_TOPUP;
                    if (rechargePlanService.SPECIAL_OPERATOR_IDS.contains(operatorId) && !rechargePlan.getName().equals(TOPUP)) {
                        rechargeType = FCConstants.RECHARGE_TYPE_SPECIAL;
                    }

                    if (rechargeMetricService.isInvalidDenomination(operatorId.toString(), circleId.toString(),
                            new Double(rechargePlan.getAmount().doubleValue()), BENCHMARK_SUCCESS_RATE, rechargeType)
                            || rechargeMetricService.isAirtelInvalidDenomination(fcOperator, fcCircle, rechargePlan
                                    .getAmount().doubleValue())) {
                        LOGGER.info(String.format(
                                "Amount : %s, Operator: %s , Circle: %s is evaulated invalid. Hence skipping",
                                rechargePlan.getAmount(), fcOperator, fcCircle));
                        continue;
                    }
                    LOGGER.info("Checking if ireff plans already exists in database.");
                    RechargePlan similarPlanInDB = rechargePlanService.getExistingSimilarPlan(rechargePlan, allPlansDB);
                    if (similarPlanInDB != null) {
                        LOGGER.info("Plan exists in database.");
                        rechargePlan.setRechargePlanId(similarPlanInDB.getRechargePlanId());
                        if (!similarPlanInDB.isSticky() && !rechargePlan.toString().equals(similarPlanInDB.toString())) {
                            LOGGER.info("Updating plan in database with latest ireff plan data.");
                            rechargePlanService.forceUpdateRechargePlan(rechargePlan);
                        }
                        if (!similarPlanInDB.isSticky() && similarPlanInDB.getStatus() == false) {
                            LOGGER.info("Enabling the plan in database as it was disabled.");
                            rechargePlanService.updateRechargePlanStatus(rechargePlan);
                        }
                        if (!similarPlanInDB.isSticky()
                                && similarPlanInDB.getIsRecommended() != rechargePlan.getIsRecommended()) {
                            LOGGER.info("Enabling the plan in database as it was disabled.");
                            rechargePlanService.updateRechargePlanRecomended(rechargePlan);
                        }
                        allPlansToBeActive.add(similarPlanInDB);
                    } else {
                        // New Plans to be added to DB
                        LOGGER.info("Plan not found in databse. Need to insert it to database.");
                        dbIngestPlans.add(rechargePlan);
                    }
                }
                recommendedPlans = rechargePlanService.getFilteredRecomendedPlans(recommendedPlans);
                rechargePlanIngestJobUtil.processForModeration(moderationCausePairs);
                rechargePlanService.disableAllPlansExcept(allPlansToBeActive, operatorId, circleId);
                if (!FCUtil.isEmpty(dbIngestPlans)) {
                    ingest(dbIngestPlans);
                }
                rechargePlanService.resetRecommendedExcept(recommendedPlans, operatorId, circleId);
                /*
                 * } else { LOGGER.warn(String .format(
                 * "No valid denominations found for operator: %s and circle:%s after filters -"
                 * +
                 * " MIN_SUCCESS_DENOMINATION_RECHARGE_COUNT and invalidDenomination."
                 * , fcOperator, fcCircle));
                 * rechargePlanService.disableAllPlansExcept(allPlansToBeActive,
                 * operatorId, circleId);
                 * rechargePlanService.resetRecommendedExcept(recommendedPlans,
                 * operatorId, circleId); }
                 */
                /*
                 * } else { LOGGER.warn(String.format(
                 * "No valid denominations found for operator: %s and circle:%s."
                 * , fcOperator, fcCircle));
                 * rechargePlanService.disableAllPlansExcept(allPlansToBeActive,
                 * operatorId, circleId);
                 * rechargePlanService.resetRecommendedExcept(recommendedPlans,
                 * operatorId, circleId); }
                 */
            }
        }
    }

    private static final Map<String, String> CIRCLE_IREFF_LOOKUP  = FCUtil.createMap(
            // Freecharge name, Ireff long name, //Ireff short code
            "Andhra Pradesh",   "AndhraPradesh",     //"AP",
            "Assam",            "Assam",             //"AS",
            "Bihar",            "BiharJharkhand",    //"BR",
            //"Chattisgarh",
            "Chennai",          "Chennai",           //"Chennai",
            "Delhi",            "Delhi",             //"Delhi",
            "Gujarat",          "Gujarat",           //"GJ",
            "Haryana",          "Haryana",           //"HR",
            "Himachal Pradesh", "HimachalPradesh",   //"HP",
            "Karnataka",        "Karnataka",         //"KA",
            "Kerala",           "Kerala",            //"KL",
            "Kolkata",          "Kolkata",           //"Kolkata",
            "Madhya Pradesh",   "MadhyaPradesh",     //"MP",
            "Maharashtra",      "Maharashtra",       //"MH",
            "Mumbai",           "Mumbai",            //"Mumbai",
            "North East",       "NorthEast",         //"NE",
            "Orissa",           "Orissa",            //"OR",
            "Punjab",           "Punjab",            //"PB",
            "Rajasthan",        "Rajasthan",         //"RJ",
            "Tamil Nadu",       "TamilNadu",         //"TN",
            "Uttar Pradesh (E)", "UttarPradeshEast", //"UPE",
            "Uttar Pradesh (W)", "UttarPradeshWest", //"UPW",
            "West Bengal",       "WestBengal",       //"WB"
            //"Uttar Pradesh"
            //"ALL"
            "JK",                "JammuKashmir"      //"JK",
            //"UTTARANCHAL"
            // ----------
            );

    private static final Map<String, String> SERVICE_IREFF_LOOKUP = FCUtil.createMap(
            // Freecharge operator code, Ireff service name
            "Aircel",                    "Aircel",
            "Airtel",                    "Airtel",
            "BSNL",                      "BSNL",
            "Tata Docomo GSM",           "Docomo",
            "Idea",                      "Idea",
            "Loop",                      "Loop",
            "MTNL Delhi",                "MTNL",
            "MTNL Mumbai",               "MTNL",
            "Reliance-CDMA",             "RelianceCDMA",
            "Reliance-GSM",              "RelianceGSM",
            "Tata Indicom",              "Indicom",
            "Tata Docomo CDMA",          "Indicom",
            "Uninor",                    "Uninor",
            "Vodafone",                  "Vodafone",
            "Virgin GSM",                "VirginGSM",
            "Virgin CDMA",               "VirginCDMA",
            "T24",                       "T24",
            "MTS",                       "MTS",
            "Videocon Mobile",           "Videocon"
            //"Indicom-Walky",             "VirginCDMA",
            //"Dish Tv",
            //"Big Tv",
            //"Airtel Tv",
            //"Sun Tv",
            //"Tata Sky",
            //"Videocon Tv",
            //"Tata Photon",
            //"Reliance Netconnect",
            //"VIRGIN",
            //"MTS MBrowse",
            //"MTS Mblaze",
            // -----------
            // Unused Ireff entries are below
            // -----------
            // Unmapped Ireff entries
            //"T24",
            //"VirginGSM"
            );

    private String getIreffPlans(final String ireffURL) throws RuntimeException, IOException {
        try {
            String body = null;
            HttpClient client = new HttpClient(connectionManager);
            HttpMethod txn = new GetMethod(ireffURL);
            LOGGER.info("Making request to Ireff URL: " + ireffURL);
            int code = client.executeMethod(txn);
            LOGGER.info("Ireff request returned HTTP response code " + code);
            if (code == HttpServletResponse.SC_OK) {
                body = txn.getResponseBodyAsString();
            }
            return body;
        } catch (RuntimeException e) {
            LOGGER.error(String.format("Failed to get plans using API call : %s", ireffURL), e);
            throw e;
        }
    }
}