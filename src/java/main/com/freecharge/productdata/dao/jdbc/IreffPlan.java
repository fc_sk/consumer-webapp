package com.freecharge.productdata.dao.jdbc;

import java.util.Date;

public class IreffPlan {

    private long    planId;
    private String  ireffId;
    private String  category;
    private String  detail;
    private Double  amount;
    private Date    createdTime;
    private String  validity;
    private int     circleId;
    private String  sourceUri;
    private int     operatorId;
    private Double  talktime;
    private boolean isActive;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getValidity() {
        return validity;
    }

    @Override
    public String toString() {
        return "IreffPlan [planId=" + planId + ", ireffId=" + ireffId + ", category=" + category + ", detail=" + detail
                + ", amount=" + amount + ", createdTime=" + createdTime + ", validity=" + validity + ", circleId="
                + circleId + ", sourceUri=" + sourceUri + ", operatorId=" + operatorId + ", talktime=" + talktime
                + ", isActive=" + isActive + "]";
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getSourceUri() {
        return sourceUri;
    }

    public void setSourceUri(String sourceUri) {
        this.sourceUri = sourceUri;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getIreffId() {
        return ireffId;
    }

    public void setIreffId(String ireffId) {
        this.ireffId = ireffId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public Double getTalktime() {
        return talktime;
    }

    public void setTalktime(Double talktime) {
        this.talktime = talktime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

}
