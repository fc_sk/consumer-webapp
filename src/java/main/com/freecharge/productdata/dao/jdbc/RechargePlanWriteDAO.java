package com.freecharge.productdata.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.entity.RechargePlan;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 29/9/12 Time: 4:30 PM To change
 * this template use File | Settings | File Templates.
 */
@Component("rechargePlanWriteDAO")
@Transactional
public class RechargePlanWriteDAO {
    private static Logger logger = LoggingFactory.getLogger(RechargePlanWriteDAO.class);

    private static final String        CIRCLE_ID   = "circleId";
    private static final String        OPERATOR_ID = "operatorId";
    private static final String        STATUS      = "status";
    private static final String        PLAN_IDS    = "planIds";
    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertRechargePlan;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertRechargePlan = new SimpleJdbcInsert(dataSource).withTableName("in_recharge_plans").usingGeneratedKeyColumns(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, "n_last_updated", "n_created");
    }

    public RechargePlan insertechargePlan(final RechargePlan rechargePlan) throws RuntimeException {
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSource();
        Number primaryKey = this.insertRechargePlan.executeAndReturnKey(sqlParameterSource);
        rechargePlan.setRechargePlanId(primaryKey.intValue());
        return rechargePlan;
    }

    private static final String SQL_INSERT = "INSERT INTO in_recharge_plans"
        + " (name, fk_product_master_id, denomination_type, fk_operator_master_id, fk_circle_master_id, amount,"
        + " min_amount, max_amount, talktime, validity, description, status, is_sticky, ireff_id, created_time,"
        + " dataweave_id) VALUES (:name, :fk_product_master_id, :denomination_type, :fk_operator_master_id,"
        + " :fk_circle_master_id, :amount, :min_amount, :max_amount, :talktime, :validity, :description, :status,"
        + " :is_sticky, :ireff_id, :created_time, :dataweave_id)";

    public void insertRechargePlans(final List<RechargePlan> allPlans) {
        @SuppressWarnings("unchecked")
        Map<String, Object>[] batchValues = new HashMap[allPlans.size()];
        int i = 0;
        for (RechargePlan each : allPlans) {
            Map<String, Object> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_NAME_COLUMN, each.getName());
            params.put(RechargePlan.PRODUCT_TYPE_COLUMN, each.getProductType());
            params.put(RechargePlan.DENOMINATION_TYPE_COLUMN, each.getDenominationType());
            params.put(RechargePlan.FK_OPERATOR_MASTER_ID_DB_COLUMN, each.getOperatorMasterId());
            params.put(RechargePlan.FK_CIRCLE_MASTER_ID_DB_COLUMN, each.getCircleMasterId());
            params.put(RechargePlan.AMOUNT_DB_COLUMN, each.getAmount());
            params.put(RechargePlan.MIN_AMOUNT_DB_COLUMN, each.getMinAmount());
            params.put(RechargePlan.MAX_AMOUNT_DB_COLUMN, each.getMaxAmount());
            params.put(RechargePlan.TALKTIME_DB_COLUMN, each.getTalktime());
            params.put(RechargePlan.VALIDITY_DB_COLUMN, each.getValidity());
            params.put(RechargePlan.DESCRIPTION_DB_COLUMN, each.getDescription());
            params.put(RechargePlan.STATUS_DB_COLUMN, each.getStatus());
            params.put(RechargePlan.IS_STICKY_DB_COLUMN, each.isSticky());
            params.put(RechargePlan.IREFF_ID_DB_COLUMN, each.getIreffID());
            params.put(RechargePlan.DATAWEAVE_ID_DB_COLUMN, each.getIsRecommended());
            params.put(RechargePlan.IREFF_CREATED_TIME, each.getCreatedTime());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(SQL_INSERT, batchValues);
    }

    private static final String SQL_DELETE = "DELETE FROM in_recharge_plans"
            + " WHERE fk_circle_master_id = :fk_circle_master_id"
            + "   AND fk_operator_master_id = :fk_operator_master_id AND NOT is_sticky";

    public void deleteNonStickyPlans(final int circleMasterID, final int operatorMasterID) {
        Map<String, Integer> params = FCUtil.createMap("fk_circle_master_id", circleMasterID, "fk_operator_master_id",
                operatorMasterID);
        jdbcTemplate.update(SQL_DELETE, params);
    }

    public void deleteNonStickyPlanFromDB(final RechargePlan plan) {
        Map<String, Integer> params = FCUtil.createMap("recharge_plan_id", plan.getRechargePlanId());
        String sql = "DELETE FROM in_recharge_plans WHERE recharge_plan_id = :recharge_plan_id AND NOT is_sticky";
        jdbcTemplate.update(sql, params);
    }
    
    public void forceDeleteFromDB(final RechargePlan plan) {
        Map<String, Integer> params = FCUtil.createMap("recharge_plan_id", plan.getRechargePlanId());
        String sql = "DELETE FROM in_recharge_plans WHERE recharge_plan_id = :recharge_plan_id";
        jdbcTemplate.update(sql, params);
    }

    public void deleteNonStickyPlanFromDB(final List<RechargePlan> plans) {
        String sql = "DELETE FROM in_recharge_plans WHERE recharge_plan_id = :recharge_plan_id AND NOT is_sticky";
        @SuppressWarnings("unchecked")
        Map<String, Integer>[] batchValues = new HashMap[plans.size()];
        int i = 0;
        for (RechargePlan each : plans) {
            Map<String, Integer> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, each.getRechargePlanId());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sql, batchValues);
    }

    /*
     * This function is to delete plans in the list of plans which has a sticky
     * enabled and status disabled
     */
    public void deleteStickyDisabledPlanFromDB(final List<RechargePlan> plans) {
        String sql = "DELETE FROM in_recharge_plans WHERE recharge_plan_id = :recharge_plan_id AND is_sticky"
                        + "AND NOT status";
        @SuppressWarnings("unchecked")
        Map<String, Integer>[] batchValues = new HashMap[plans.size()];
        int i = 0;
        for (RechargePlan each : plans) {
            Map<String, Integer> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, each.getRechargePlanId());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sql, batchValues);
    }

    private void assertValidRechargePlanID(final RechargePlan rechargePlan) {
        if (rechargePlan == null || rechargePlan.getRechargePlanId() == 0) {
            throw new IllegalArgumentException("Invalid RechargePlan argument: " + rechargePlan == null ? "NULL"
                    : rechargePlan.toString());
        }
    }

    public RechargePlan updatePlan(final RechargePlan rechargePlan) {
        assertValidRechargePlanID(rechargePlan);
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set fk_circle_master_id = :fk_circle_master_id, fk_operator_master_id ="
                + " :fk_operator_master_id, amount = :amount, name = :name, fk_product_master_id ="
                + " :fk_product_master_id, denomination_type = :denomination_type,"
                + " min_amount = :min_amount, max_amount = :max_amount, talktime = :talktime, validity = :validity,"
                + " description = :description, created_time = :created_time where recharge_plan_id = :recharge_plan_id"
                + " and NOT is_sticky";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public RechargePlan forceUpdatePlan(final RechargePlan rechargePlan) {
        assertValidRechargePlanID(rechargePlan);
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set fk_circle_master_id = :fk_circle_master_id, fk_operator_master_id ="
                + " :fk_operator_master_id, amount = :amount, name = :name, fk_product_master_id ="
                + " :fk_product_master_id, denomination_type = :denomination_type,"
                + " min_amount = :min_amount, max_amount = :max_amount, talktime = :talktime, validity = :validity,"
                + " description = :description, created_time = :created_time where recharge_plan_id = :recharge_plan_id";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public RechargePlan forceUpdateDescription(final RechargePlan rechargePlan) {
        Map<String, ? extends Object> params = FCUtil.createMap("description", rechargePlan.getDescription(),
                "recharge_plan_id", rechargePlan.getRechargePlanId());

        String sql = "update in_recharge_plans set description = :description, created_time = CURDATE() where recharge_plan_id = :recharge_plan_id";

        int status = this.jdbcTemplate.update(sql, params);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }
        return rechargePlan;
    }

    public RechargePlan updatePlanAllCircle(final RechargePlan rechargePlan) {
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set talktime = :talktime, validity = :validity, description ="
                    + " :description, status = :status, is_sticky = :is_sticky, created_time = CURDATE() where fk_operator_master_id ="
                    + " :fk_operator_master_id and amount = :amount and name = :name and"
                    + " fk_product_master_id = :fk_product_master_id and denomination_type = :denomination_type";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public RechargePlan updateStatus(final RechargePlan rechargePlan) {
        assertValidRechargePlanID(rechargePlan);
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set status = :status, created_time = CURDATE() where"
                    + " recharge_plan_id = :recharge_plan_id";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public List<RechargePlan> updateStatus(final List<RechargePlan> rechargePlans) {
        String sql = "update in_recharge_plans set status = :status, created_time = CURDATE() where"
                    + " recharge_plan_id = :recharge_plan_id";

        @SuppressWarnings("unchecked")
        Map<String, Object>[] batchValues = new HashMap[rechargePlans.size()];
        int i = 0;
        for (RechargePlan each : rechargePlans) {
            assertValidRechargePlanID(each);
            Map<String, Object> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, each.getRechargePlanId());
            params.put(RechargePlan.STATUS_DB_COLUMN, each.getStatus());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sql, batchValues);
        return rechargePlans;
    }
    
    public List<RechargePlan> updateRecomended(final List<RechargePlan> rechargePlans) {
        String sql = "update in_recharge_plans set dataweave_id = :dataweave_id, created_time = :created_time where"
                    + " recharge_plan_id = :recharge_plan_id";

        @SuppressWarnings("unchecked")
        Map<String, Object>[] batchValues = new HashMap[rechargePlans.size()];
        int i = 0;
        for (RechargePlan each : rechargePlans) {
            assertValidRechargePlanID(each);
            Map<String, Object> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, each.getRechargePlanId());
            params.put(RechargePlan.DATAWEAVE_ID_DB_COLUMN, each.getIsRecommended());
            params.put(RechargePlan.IREFF_CREATED_TIME, each.getCreatedTime());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sql, batchValues);
        return rechargePlans;
    }

    public RechargePlan updateRecomended(final RechargePlan rechargePlan) {
        assertValidRechargePlanID(rechargePlan);
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set dataweave_id = :dataweave_id, created_time = :created_time where"
                    + " recharge_plan_id = :recharge_plan_id";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public RechargePlan updateSticky(final RechargePlan rechargePlan) {
        assertValidRechargePlanID(rechargePlan);
        SqlParameterSource sqlParameterSource = rechargePlan.getMapSqlParameterSourceForUpdate();

        String sql = "update in_recharge_plans set is_sticky = :is_sticky, created_time = :created_time where"
                    + " recharge_plan_id = :recharge_plan_id";

        int status = this.jdbcTemplate.update(sql, sqlParameterSource);

        if (status <= 0) {
            logger.error("No Rows affected on updating in_recharge_plans for id = " + rechargePlan.getRechargePlanId());
        }

        return rechargePlan;
    }

    public List<RechargePlan> updateSticky(final List<RechargePlan> rechargePlans) {
        String sql = "update in_recharge_plans set is_sticky = :is_sticky, created_time = :created_time where"
                    + " recharge_plan_id = :recharge_plan_id";

        @SuppressWarnings("unchecked")
        Map<String, Object>[] batchValues = new HashMap[rechargePlans.size()];
        int i = 0;
        for (RechargePlan each : rechargePlans) {
            assertValidRechargePlanID(each);
            Map<String, Object> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN, each.getRechargePlanId());
            params.put(RechargePlan.IS_STICKY_DB_COLUMN, each.isSticky());
            params.put(RechargePlan.IREFF_CREATED_TIME, each.getCreatedTime());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sql, batchValues);
        return rechargePlans;
    }

    public void updateStatusAllPlansExcept(final List<RechargePlan> rechargePlans, final String operatorId,
            final String circleId, final Integer status) {
        String sql = "update in_recharge_plans set status =:%s where fk_operator_master_id=:%s and fk_circle_master_id=:%s"
                    + " and recharge_plan_id NOT IN (:%s)";

        sql = String.format(sql, STATUS, OPERATOR_ID, CIRCLE_ID, PLAN_IDS);
        String planIdList = "";
        String prefix = "";
        for (RechargePlan each : rechargePlans) {
            planIdList = planIdList.concat(prefix);
            prefix = ",";
            planIdList = planIdList.concat(each.getRechargePlanId() + "");
        }
        if (FCUtil.isEmpty(planIdList)) {
            sql = sql.replace(":"+PLAN_IDS, "''");
        } else {
            sql = sql.replace(":"+PLAN_IDS, planIdList);
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put(STATUS, status.toString());
        paramMap.put(OPERATOR_ID, operatorId);
        paramMap.put(CIRCLE_ID, circleId);
        jdbcTemplate.update(sql, paramMap);
    }

    public void updateRecommendedAllPlansExcept(final List<RechargePlan> rechargePlans, final String operatorId,
            final String circleId, final Integer isRecomended) {
        String sql = "update in_recharge_plans set dataweave_id =:%s where fk_operator_master_id=:%s and fk_circle_master_id=:%s"
                    + " and recharge_plan_id NOT IN (:%s)";

        sql = String.format(sql, RechargePlan.DATAWEAVE_ID_DB_COLUMN, OPERATOR_ID, CIRCLE_ID, PLAN_IDS);
        String planIdList = "";
        String prefix = "";
        for (RechargePlan each : rechargePlans) {
            planIdList = planIdList.concat(prefix);
            prefix = ",";
            planIdList = planIdList.concat(each.getRechargePlanId() + "");
        }
        if (FCUtil.isEmpty(planIdList)) {
            sql = sql.replace(":"+PLAN_IDS, "''");
        } else {
            sql = sql.replace(":"+PLAN_IDS, planIdList);
        }
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put(RechargePlan.DATAWEAVE_ID_DB_COLUMN, isRecomended.toString());
        paramMap.put(OPERATOR_ID, operatorId);
        paramMap.put(CIRCLE_ID, circleId);
        jdbcTemplate.update(sql, paramMap);
    }

    private static final String SQL_DELETE_PLANS = "DELETE FROM in_recharge_plans"
            + " WHERE fk_circle_master_id = :fk_circle_master_id"
            + " AND fk_operator_master_id = :fk_operator_master_id AND amount= :amount"
            + " AND fk_product_master_id = :fk_product_master_id AND NOT is_sticky";
    public void deleteNonStickyRechargePlan(int circleId, int operatorId, int productId, float amount) {
        Map<String, ? extends Number> params = FCUtil.createMap("fk_circle_master_id", circleId, "fk_operator_master_id",
                operatorId, "amount", amount, "fk_product_master_id", productId);
        jdbcTemplate.update(SQL_DELETE_PLANS, params);
    }
}
