package com.freecharge.productdata.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.mapper.IreffPlanMapper;

@Component("ireffPlanReadDAO")
public class IreffPlanReadDAO {
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static Logger logger = LoggingFactory.getLogger(IreffPlanReadDAO.class);

    @Autowired
    public void setDataSource(final DataSource jdbcReadDataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcReadDataSource);
    }

    public IreffPlan getActiveIreffPlansForAmount(IreffPlan ireffPlan) {
        String sql = "select * from ireff_plans where amount = :amount and fk_operator_master_id = :operatorId and fk_circle_master_id = :circleId  and category = :category and is_active=1 order by created_time desc";
        Map<String, Object> params = new HashMap<>();
        params.put("amount", ireffPlan.getAmount());
        params.put("operatorId", ireffPlan.getOperatorId());
        params.put("category", ireffPlan.getCategory());
        params.put("circleId", ireffPlan.getCircleId());

        List<IreffPlan> plans = this.jdbcTemplate.query(sql, params, new IreffPlanMapper());
        if (plans != null && plans.size() > 0) {
            logger.info("Similar plans exist for the amount: " + ireffPlan.getAmount() + "  , operator = "
                    + ireffPlan.getOperatorId() + " , circle = " + ireffPlan.getCircleId() + " and category = "
                    + ireffPlan.getCategory());
            return plans.get(0);
        }
        return null;
    }

}
