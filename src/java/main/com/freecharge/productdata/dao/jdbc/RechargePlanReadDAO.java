package com.freecharge.productdata.dao.jdbc;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.mapper.RechargePlanMapper;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 29/9/12
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
@Component("rechargePlanReadDAO")
@Transactional
public class RechargePlanReadDAO {
    private static final String CIRCLE_ID = "circleId";
    private static final String OPERATOR_ID = "operatorId";
    private static final String PRODUCT_ID = "productId";
    private static final String AMOUNT = "amount";
    private static final String PLAN_NAME = "planName";
    private static final String RECHARGE_PLAN_ID = "rechargePlanId";
    private static final String IREFF_ID = "ireffID";
    
    private static final String SQL_FIND_STICKY_IREFF_ID_LIST = "SELECT ireff_id FROM in_recharge_plans WHERE is_sticky";
    
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource jdbcReadDataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcReadDataSource);
    }
    
    public List<String> getAllRechargePlanNames() {
    	String sql = "SELECT DISTINCT name from in_recharge_plans ORDER BY name";
    	return jdbcTemplate.queryForList(sql, Collections.EMPTY_MAP, String.class);
    }
    
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId, int productId) {
        String sql = "select * from in_recharge_plans where fk_circle_master_id = :%s and fk_operator_master_id=:%s and fk_product_master_id=:%s and status=true and name is not NULL order by name, amount asc";
        

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID, PRODUCT_ID);

        Map<String, Object> params = new HashMap<>();

        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);

        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlansForOperator(int operatorId, int productId) {
        String sql = "select * from in_recharge_plans where fk_operator_master_id=:%s and fk_product_master_id=:%s and status=true and name is not NULL order by name, amount asc";
        

        sql = String.format(sql, OPERATOR_ID, PRODUCT_ID);

        Map<String, Object> params = new HashMap<>();

        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);

        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId, int productId,float amount) {
        String sql = "select * from in_recharge_plans " +
        		     "where fk_circle_master_id = :%s and fk_operator_master_id=:%s " +
        		     "and fk_product_master_id=:%s and status=true and amount=:%s ";

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID, PRODUCT_ID,AMOUNT);

        Map<String, Object> params = new HashMap<>();
        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);
        params.put(AMOUNT, amount);
        
        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId, int productId,float amount, String planName) {
        String sql = "select * from in_recharge_plans " +
        		     "where fk_circle_master_id = :%s and fk_operator_master_id=:%s " +
        		     "and fk_product_master_id=:%s and status=true and amount=:%s and name=:%s";

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID, PRODUCT_ID,AMOUNT, PLAN_NAME);

        Map<String, Object> params = new HashMap<>();
        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);
        params.put(AMOUNT, amount);
        params.put(PLAN_NAME, planName);
        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlansAnyStatus(int circleId, int operatorId, int productId,BigDecimal amount, String planName) {
        String sql = "select * from in_recharge_plans " +
        		     "where fk_circle_master_id = :%s and fk_operator_master_id=:%s " +
        		     "and fk_product_master_id=:%s and amount=:%s and name=:%s";

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID, PRODUCT_ID,AMOUNT, PLAN_NAME);

        Map<String, Object> params = new HashMap<>();
        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);
        params.put(AMOUNT, amount);
        params.put(PLAN_NAME, planName);
        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId,
			int productId, String rechargePlanName) {

    	String sql = "select * from in_recharge_plans where fk_circle_master_id = :%s and fk_operator_master_id=:%s and fk_product_master_id=:%s and name=:%s and status=true order by amount asc";

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID, PRODUCT_ID, PLAN_NAME);

        Map<String, Object> params = new HashMap<>();

        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);
        params.put(PRODUCT_ID, productId);
        params.put(PLAN_NAME, rechargePlanName);
        
        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
	}
    
    public List<RechargePlan> getRechargePlans(int circleId, int operatorId) {
        String sql = "select * from in_recharge_plans where fk_circle_master_id = :%s and fk_operator_master_id=:%s order by recharge_plan_id desc";

        sql = String.format(sql, CIRCLE_ID, OPERATOR_ID);

        Map<String, Object> params = new HashMap<>();

        params.put(CIRCLE_ID, circleId);
        params.put(OPERATOR_ID, operatorId);

        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public List<RechargePlan> getRechargePlansAllCircle(int operatorId) {
        String sql = "select * from in_recharge_plans where fk_operator_master_id=:%s order by recharge_plan_id desc";

        sql = String.format(sql, OPERATOR_ID);

        Map<String, Object> params = new HashMap<>();

        params.put(OPERATOR_ID, operatorId);

        return this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
    }
    
    public RechargePlan getRechargePlan(int rechargePlanId) {
        String sql = "select * from in_recharge_plans where recharge_plan_id = :%s ";

        sql = String.format(sql, RECHARGE_PLAN_ID);

        Map<String, Object> params = new HashMap<>();

        params.put(RECHARGE_PLAN_ID, rechargePlanId);
        
        List<RechargePlan> rechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        if(rechargePlans != null && rechargePlans.size() > 0)
        	return rechargePlans.get(0);
        else 
        	return null;
    }
    
    public boolean checkIfStickyPlanExists(RechargePlan rechargePlan) {
    	@SuppressWarnings("unchecked")
        List<String> ireffIDs = jdbcTemplate.queryForList(SQL_FIND_STICKY_IREFF_ID_LIST, Collections.EMPTY_MAP,
                String.class);
        if (rechargePlan.getIreffID() != null && !ireffIDs.contains(rechargePlan.getIreffID())) {
            return false;
        }
        return true;
    }
    
    public List<String> getAllStickyIreffIds() {
        @SuppressWarnings("unchecked")
        List<String> ireffIDs = jdbcTemplate.queryForList(SQL_FIND_STICKY_IREFF_ID_LIST, Collections.EMPTY_MAP,
                String.class);
        return ireffIDs;
    }

    public List<RechargePlan> getRechargePlansFromIreffId(String ireffId) {
        String sql = "select * from in_recharge_plans where ireff_id = :%s ";
        sql = String.format(sql, IREFF_ID);
        Map<String, Object> params = new HashMap<>();
        params.put(IREFF_ID, ireffId);
        List<RechargePlan> rechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return rechargePlans;
    }
}
