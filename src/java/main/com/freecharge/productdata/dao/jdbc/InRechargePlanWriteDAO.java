package com.freecharge.productdata.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.productdata.entity.RechargePlan;

@Component("inRechargePlanWriteDAO")
@Transactional
public class InRechargePlanWriteDAO {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public int updateStatus(int circleId, int operatorId, int productId, float amount, boolean status) {
        String sql = "update in_recharge_plans set status = :status, created_time = CURDATE() "
                + "where fk_circle_master_id = :circleId and fk_operator_master_id=:operatorId "
                + "and fk_product_master_id=:productId and amount=:amount AND NOT is_sticky";

        Map<String, Object> params = new HashMap<>();
        params.put("status", status);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);
        params.put("amount", amount);

        int updateCount = jdbcTemplate.update(sql, params);
        return updateCount;
    }

    public void insertRechargePlans(final List<RechargePlan> allPlans) {

        final String sqlInsert = "INSERT INTO in_recharge_plans"
                + " (name, fk_product_master_id, denomination_type, fk_operator_master_id, fk_circle_master_id, amount,"
                + " min_amount, max_amount, talktime, validity, description, status, is_sticky, ireff_id, created_time,"
                + " dataweave_id) VALUES (:name, :fk_product_master_id, :denomination_type, :fk_operator_master_id,"
                + " :fk_circle_master_id, :amount, :min_amount, :max_amount, :talktime, :validity, :description, :status,"
                + " :is_sticky, :ireff_id, :created_time, :dataweave_id)";

        @SuppressWarnings("unchecked")
        Map<String, Object>[] batchValues = new HashMap[allPlans.size()];
        int i = 0;
        for (RechargePlan each : allPlans) {
            Map<String, Object> params = new HashMap<>();
            params.put(RechargePlan.RECHARGE_PLAN_NAME_COLUMN, each.getName());
            params.put(RechargePlan.PRODUCT_TYPE_COLUMN, each.getProductType());
            params.put(RechargePlan.DENOMINATION_TYPE_COLUMN, each.getDenominationType());
            params.put(RechargePlan.FK_OPERATOR_MASTER_ID_DB_COLUMN, each.getOperatorMasterId());
            params.put(RechargePlan.FK_CIRCLE_MASTER_ID_DB_COLUMN, each.getCircleMasterId());
            params.put(RechargePlan.AMOUNT_DB_COLUMN, each.getAmount());
            params.put(RechargePlan.MIN_AMOUNT_DB_COLUMN, each.getMinAmount());
            params.put(RechargePlan.MAX_AMOUNT_DB_COLUMN, each.getMaxAmount());
            params.put(RechargePlan.TALKTIME_DB_COLUMN, each.getTalktime());
            params.put(RechargePlan.VALIDITY_DB_COLUMN, each.getValidity());
            params.put(RechargePlan.DESCRIPTION_DB_COLUMN, each.getDescription());
            params.put(RechargePlan.STATUS_DB_COLUMN, each.getStatus());
            params.put(RechargePlan.IS_STICKY_DB_COLUMN, each.isSticky());
            params.put(RechargePlan.IREFF_ID_DB_COLUMN, each.getIreffID());
            params.put(RechargePlan.DATAWEAVE_ID_DB_COLUMN, each.getIsRecommended());
            params.put(RechargePlan.IREFF_CREATED_TIME, each.getCreatedTime());
            batchValues[i++] = params;
        }
        jdbcTemplate.batchUpdate(sqlInsert, batchValues);
    }

    public int updatePlanRecommendation(int planId, boolean isRecommended) {
        String sql = "update in_recharge_plans set dataweave_id = :isRecommended, created_time = CURDATE() "
                + "where recharge_plan_id = :rechargePlanId";

        Map<String, Object> params = new HashMap<>();
        params.put("rechargePlanId", planId);
        params.put("isRecommended", isRecommended);

        int updateCount = jdbcTemplate.update(sql, params);
        return updateCount;
    }
}