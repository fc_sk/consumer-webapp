package com.freecharge.productdata.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.mapper.RechargePlanMapper;

@Component("inRechargePlanReadDAO")
@Transactional(readOnly=true)
public class InRechargePlanReadDAO { 

	private static Logger              logger = LoggingFactory.getLogger(InRechargePlanReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("jdbcSlaveDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public boolean hasActiveRechargePlan(int circleId, int operatorId, int productId, float amount) {
        String sql = "select recharge_plan_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and status=1";
        Map<String, Object> params = new HashMap<>();
        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        if (FCUtil.isEmpty(inRechargePlans)) {
            return false;
        }
        return true;
    }

    public RechargePlan getRechargePlan(double amount, int circleId, int operatorId, int productId) {
        String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId";
        Map<String, Object> params = new HashMap<>();

        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());

        if (FCUtil.isEmpty(inRechargePlans)) {
            return null;
        }
        if (inRechargePlans.size() > 1) {
            logger.error("Mutliple plans found for the amount " + amount + " , circle " + circleId + ", operator "
                    + operatorId + ", product id " + productId);
        }

        return inRechargePlans.get(0);
    }

    public List<RechargePlan> getRechargePlanForIreffId(double amount, int circleId, int operatorId, int productId,
            String ireffId) {
        String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and ireff_id = :ireffId";
        Map<String, Object> params = new HashMap<>();

        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);
        params.put("ireffId", ireffId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return inRechargePlans;
    }
    
    public List<RechargePlan> getStickyRechargePlan(double amount, int circleId, int operatorId, int productId) {
        String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and is_sticky=true";
        Map<String, Object> params = new HashMap<>();

        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return inRechargePlans;
    }

    public List<RechargePlan> getActiveRechargePlan(double amount, int circleId, int operatorId, int productId) {
        String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and status=1";
        Map<String, Object> params = new HashMap<>();

        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return inRechargePlans;
    }
    
    public List<RechargePlan> getInActiveRechargePlans(double amount, int circleId, int operatorId, int productId) {
        String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where amount = :amount and fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and status=0";
        Map<String, Object> params = new HashMap<>();

        params.put("amount", amount);
        params.put("circleId", circleId);
        params.put("operatorId", operatorId);
        params.put("productId", productId);

        List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return inRechargePlans;
    }
    
    public List<RechargePlan> getActivePlansOperatorCircle(int operatorId,int circleId, Integer productId){
    	String sql = "select recharge_plan_id, name, amount, min_amount, max_amount, talktime, validity, fk_product_master_id, "
                + "denomination_type, description, fk_operator_master_id, fk_circle_master_id, status, is_sticky, ireff_id, created_time, "
                + "dataweave_id from in_recharge_plans where fk_operator_master_id = :operatorId and "
                + " fk_circle_master_id = :circleId and fk_product_master_id = :productId and status=1";
    	Map<String, Object> params = new HashMap<>();
    	
    	params.put("operatorId", operatorId);
    	params.put("circleId", circleId);
    	params.put("productId", productId);
    	
    	List<RechargePlan> inRechargePlans = this.jdbcTemplate.query(sql, params, new RechargePlanMapper());
        return inRechargePlans;
    }
}