package com.freecharge.productdata.dao.jdbc;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.mapper.IreffPlanMapper;
import com.freecharge.productdata.util.IreffConstants;
import com.freecharge.recharge.util.RechargeConstants;

@Component("ireffPlanDAO")
public class IreffPlanDAO {
    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert           insertIreffPlan;

    private static Logger              logger      = LoggingFactory.getLogger(IreffPlanDAO.class);

    private static final int           WINDOW_DAYS = 7;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertIreffPlan = new SimpleJdbcInsert(dataSource).withTableName("ireff_plans").usingGeneratedKeyColumns(
                "plan_id");
    }

    public String insertIreffPlan(IreffPlan ireffPlan) {

        logger.info(" Adding new plan with ireff id " + ireffPlan.getIreffId());
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("category", ireffPlan.getCategory());
        insertParams.put("amount", ireffPlan.getAmount());
        insertParams.put("talktime", ireffPlan.getTalktime());
        insertParams.put("validity", ireffPlan.getValidity());
        insertParams.put("description", ireffPlan.getDetail());
        insertParams.put("fk_operator_master_id", ireffPlan.getOperatorId());
        insertParams.put("fk_circle_master_id", ireffPlan.getCircleId());
        insertParams.put("ireff_id", ireffPlan.getIreffId());
        insertParams.put("created_time", DateTime.now().toDate());
        insertParams.put("source_uri", ireffPlan.getSourceUri());
        insertParams.put("is_active", true);
        Number primaryKey = insertIreffPlan.executeAndReturnKey(insertParams);

        if (primaryKey.intValue() == 0) {
            logger.error(" Some errior while stroing the ireff plan with id " + ireffPlan.getIreffId());
            return null;
        }
        return primaryKey.toString();

    }

    /*
     * Only update to the existing plan is to mark the plan as inactive.
     */
    public void disableIreffPlan(long planId) {

        logger.info("Marking plan with id " + planId + " as inactive");
        String sql = "update ireff_plans set is_active = :active where plan_id = :planId";

        Map<String, Object> params = new HashMap<>();
        params.put("active", 0);
        params.put("planId", planId);

        int status = this.jdbcTemplate.update(sql, params);
        if (status <= 0) {
            logger.error("No Rows affected on updating ireff_plans for id = " + planId);
        }
    }

    public IreffPlan getActiveIreffPlansForAmount(IreffPlan ireffPlan) {
        String sql = "select * from ireff_plans where amount = :amount and fk_operator_master_id = :operatorId and fk_circle_master_id = :circleId  and category = :category and is_active=1 order by created_time desc";
        Map<String, Object> params = new HashMap<>();
        params.put("amount", ireffPlan.getAmount());
        params.put("operatorId", ireffPlan.getOperatorId());
        params.put("category", ireffPlan.getCategory());
        params.put("circleId", ireffPlan.getCircleId());

        List<IreffPlan> plans = this.jdbcTemplate.query(sql, params, new IreffPlanMapper());
        if (plans != null && plans.size() > 0) {
            logger.info("Similar plans exist for the amount: " + ireffPlan.getAmount() + "  , operator = "
                    + ireffPlan.getOperatorId() + " , circle = " + ireffPlan.getCircleId() + " and category = "
                    + ireffPlan.getCategory());
            return plans.get(0);
        }
        return null;
    }

    public IreffPlan getActiveIreffPlan(int circleId, int operatorId, float amount, int productId, String planType) {

        String sql = "select * from ireff_plans where amount = :amount and fk_operator_master_id = :operatorId "
                + "and fk_circle_master_id = :circleId  and is_active=1 and created_time > :createdTime ";
        Map<String, Object> params = new HashMap<>();

        // Do not choose the topup plans for the special or the data card
        // recharges.
        if ((planType.equals(RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL) && productId == 1)) {
            sql += " and category not in (:category) ";
            params.put("category", Arrays.asList(IreffConstants.CATEGORY_TOPUP));
        } else if (productId == ProductMaster.ProductName.DataCard.getProductId()) {
            sql += " and category in (:category) ";
            params.put("category", Arrays.asList(IreffConstants.CATEGORY_DATA2G, IreffConstants.CATEGORY_DATA3G));
        }
        
        sql += " order by created_time desc";

        params.put("amount", amount);
        params.put("operatorId", operatorId);
        params.put("circleId", circleId);
        params.put("createdTime", new Timestamp(DateTime.now().minusDays(WINDOW_DAYS).getMillis()));

        List<IreffPlan> plans = this.jdbcTemplate.query(sql, params, new IreffPlanMapper());

        String logString = "amount " + amount + " , circle " + circleId + ", operator " + operatorId + ", product id "
                + productId;
        if (FCUtil.isEmpty(plans)) {
            logger.error(" Could not find any ireff plan for " + logString);
            return null;
        }
        if (plans.size() > 1) {
            logger.error(" Multiple ireff plans found for " + logString);
        }
        return plans.get(0);
    }

    public void updateTime(IreffPlan existingPlan) {

        long planId = existingPlan.getPlanId();
        logger.info("Updating the time for plan with id " + planId);
        String sql = "update ireff_plans set created_time = :createdTime where plan_id = :planId";

        Map<String, Object> params = new HashMap<>();
        params.put("createdTime", new Timestamp(DateTime.now().getMillis()));
        params.put("planId", planId);

        int status = this.jdbcTemplate.update(sql, params);
        if (status <= 0) {
            logger.error("No Rows affected on updating ireff_plans for id = " + planId);
        }

    }
}
