package com.freecharge.productdata.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.jdbc.core.RowMapper;

import com.freecharge.productdata.entity.RechargePlan;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 29/9/12
 * Time: 1:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class RechargePlanMapper implements RowMapper<RechargePlan> {
    
    
    @Override
    public RechargePlan mapRow(ResultSet resultSet, int i) throws SQLException {
        final DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        final DateTime VodafoneOfferExpiryDay = formatter.parseDateTime("30/09/2014 22:00:00");
        final boolean IsVodaOfferValid = VodafoneOfferExpiryDay.isAfterNow();
        final int Vodafone = 14;
        
        RechargePlan rechargePlan = new RechargePlan();

        int rechargePlanId = resultSet.getInt(RechargePlan.RECHARGE_PLAN_ID_DB_COLUMN);
        rechargePlan.setRechargePlanId(rechargePlanId);

        int circleId = resultSet.getInt(RechargePlan.FK_CIRCLE_MASTER_ID_DB_COLUMN);
        rechargePlan.setCircleMasterId(circleId);

        int operatorId = resultSet.getInt(RechargePlan.FK_OPERATOR_MASTER_ID_DB_COLUMN);
        rechargePlan.setOperatorMasterId(operatorId);

        BigDecimal amount = resultSet.getBigDecimal(RechargePlan.AMOUNT_DB_COLUMN);
        rechargePlan.setAmount(amount);
        
        BigDecimal minAmount = resultSet.getBigDecimal(RechargePlan.MIN_AMOUNT_DB_COLUMN);
        rechargePlan.setMinAmount(minAmount);
        
        BigDecimal maxAmount = resultSet.getBigDecimal(RechargePlan.MAX_AMOUNT_DB_COLUMN);
        rechargePlan.setMaxAmount(maxAmount);
        
        BigDecimal talktime = resultSet.getBigDecimal(RechargePlan.TALKTIME_DB_COLUMN);
        rechargePlan.setTalktime(talktime);
        
        String validity = resultSet.getString(RechargePlan.VALIDITY_DB_COLUMN);
        rechargePlan.setValidity(validity);
        
        String name = resultSet.getString(RechargePlan.RECHARGE_PLAN_NAME_COLUMN);
        rechargePlan.setName(name);
        
        int productType = resultSet.getInt(RechargePlan.PRODUCT_TYPE_COLUMN);
        rechargePlan.setProductType(productType);
        
        String denominationType = resultSet.getString(RechargePlan.DENOMINATION_TYPE_COLUMN);
        rechargePlan.setDenominationType(denominationType);
        
        String description = resultSet.getString(RechargePlan.DESCRIPTION_DB_COLUMN);
        
        if (operatorId == Vodafone && IsVodaOfferValid) {
            description += ". Dial *599# one hour after recharge to avail benefits.";
        }
        
        rechargePlan.setDescription(description);
        
        Boolean status = resultSet.getBoolean(RechargePlan.STATUS_DB_COLUMN);
        rechargePlan.setStatus(status);
        
        Boolean sticky = resultSet.getBoolean(RechargePlan.IS_STICKY_DB_COLUMN);
        rechargePlan.setSticky(sticky);
        
        String ireffID = resultSet.getString(RechargePlan.IREFF_ID_DB_COLUMN);
        rechargePlan.setIreffID(ireffID);
        
        Boolean isRecommended = resultSet.getBoolean(RechargePlan.DATAWEAVE_ID_DB_COLUMN);
        rechargePlan.setIsRecommended(isRecommended);
        
        Timestamp createdTime = resultSet.getTimestamp(RechargePlan.IREFF_CREATED_TIME);
        rechargePlan.setCreatedTime(createdTime);
        
        return rechargePlan;
    }
}
