package com.freecharge.productdata.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.productdata.dao.jdbc.IreffPlan;

public class IreffPlanMapper implements RowMapper<IreffPlan> {

    @Override
    public IreffPlan mapRow(ResultSet resultSet, int i) throws SQLException {

        IreffPlan ireffPlan = new IreffPlan();
        ireffPlan.setPlanId(resultSet.getLong("plan_id"));
        ireffPlan.setCategory(resultSet.getString("category"));
        ireffPlan.setAmount(resultSet.getDouble("amount"));; 
        ireffPlan.setTalktime(resultSet.getDouble("talktime"));
        ireffPlan.setValidity(resultSet.getString("validity"));
        ireffPlan.setDetail(resultSet.getString("description")); 
        ireffPlan.setOperatorId(resultSet.getInt("fk_operator_master_id")); 
        ireffPlan.setCircleId(resultSet.getInt("fk_circle_master_id")); 
        ireffPlan.setIreffId(resultSet.getString("ireff_id")); 
        ireffPlan.setCreatedTime(resultSet.getDate("created_time")); 
        ireffPlan.setSourceUri(resultSet.getString("source_uri")); 
        ireffPlan.setActive(resultSet.getBoolean("is_active"));
        return ireffPlan;
    }

}
