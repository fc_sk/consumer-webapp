package com.freecharge.productdata.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.rechargeplans.plansdenomination.OperatorCircleBlock;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.OperatorCircleWebDO;
import com.mongodb.DBObject;

// Controller used to provide data for the Recharge Plan Moderation List
@Controller
@RequestMapping("/admin/rechargeplans/moderation/*")
public class RechargePlanModerationController {
    @Autowired
    private RechargePlanService rechargePlanService;
    
    @Autowired
    private OperatorCircleReadDAO operatorCircleReadDAO;
    
    @Autowired
    private OperatorCircleWriteDAO operatorCircleWriteDAO;
    
    public static String LOGGER_PREFIX = "MODERATED";

    private static final Logger LOGGER = LoggingFactory.getLogger(RechargePlanModerationController.class);

    // Function to get all recharge plan from mongodb
    @RequestMapping(value = "list", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody ResponseStatus getAllRechargePlansInModeration() {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            List<DBObject> response = rechargePlanService.getAllRechargePlansInModeration();
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult(response);
        } catch (Exception e) {
            LOGGER.error("Controller Failed : ", e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(e.toString());
        }
        return responseStatus;
    }

    // Function to fetch a plan from Mongo db
    @RequestMapping(value = "get/{planID}", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody ResponseStatus getRechargePlanInModeration(final @PathVariable int planID) {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            DBObject response = rechargePlanService.getRechargePlanInModeration(planID);
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult(response);
        } catch (Exception e) {
            LOGGER.error("Controller Failed : ", e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(e.toString());
        }
        return responseStatus;
    }

    // Function to get duplicate plans from db
    @RequestMapping(value = "duplicate/{amount}/{name}/{circleId}/{operatorId}", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody ResponseStatus getDuplicatePlans(final @PathVariable BigDecimal amount,
            final @PathVariable String name,
            final @PathVariable int circleId, final @PathVariable int operatorId) {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            String nameFinal = name;
            if(name.equals("2G")) {
                nameFinal = "Data/2G";
            }
            List<String> response = rechargePlanService.getDuplicatePlansInModeration(amount, nameFinal, circleId,
                    operatorId);
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult(response);
        } catch (Exception e) {
            LOGGER.error("Controller Failed : ", e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(e.toString());
        }
        return responseStatus;
    }

    // Function to delete a plan from Mongo DB
    @RequestMapping(value = "delete/{planID}", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody ResponseStatus deleteRechargePlanFromModeration(final @PathVariable int planID) {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            rechargePlanService.deleteRechargePlanFromModeration(planID, false);
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("Succesfully deleted plan id : " + planID + " from Moderation.");
            LOGGER.info(String.format(LOGGER_PREFIX + " Deleted planID : %s from moderation.",
                    planID));
        } catch (Exception e) {
            LOGGER.error("Controller Failed : ", e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(e.toString());
        }
        return responseStatus;
    }

    // Function to Save a plan to Mongo db
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @NoLogin
    public String saveRechargePlanToModeration(final RechargePlan bean, final Model model) {
        try {
            rechargePlanService.saveRechargePlanToModeration(bean);
            model.addAttribute(WebConstants.STATUS, "Saved");
            return "jsonView";
        } catch (Exception e) {
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Failed to save the plan to the Queue.");
            LOGGER.error("Exception occured while saving recharge plan to Queue", e);
            return "jsonView";
        }
    }

    // Function to push a plan to in_recharge_plans table
    @RequestMapping(value = "push", method = RequestMethod.POST)
    @NoLogin
    public String saveRechargePlanToDB(final RechargePlan bean, final Model model) {
        try {
            rechargePlanService.saveRechargeFromModerationToDB(bean);
            saveRechargePlanToModeration(bean, model);
            model.addAttribute(WebConstants.STATUS, FCConstants.SUCCESS);
            LOGGER.info(String
                    .format(LOGGER_PREFIX + " Pushed planID : %s, Operator : %s, Circle : %s, Category : %s, " +
                            "Amount : %s from moderation.",
                            bean.getRechargePlanId(), bean.getOperatorMasterId(), bean.getCircleMasterId(),
                            bean.getName(), bean.getAmount()));
            return "jsonView";
        } catch (Exception e) {
            model.addAttribute(WebConstants.STATUS, FCConstants.FAIL);
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Push to DB Failed." + e.toString());
            LOGGER.error("Exception occured while pushing recharge plan to DB.", e);
            return "jsonView";
        }
    } 
}
