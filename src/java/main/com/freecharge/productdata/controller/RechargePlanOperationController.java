package com.freecharge.productdata.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleWriteDAO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.rechargeplans.plansdenomination.OperatorCircleBlock;
import com.freecharge.rest.rechargeplans.plansdenomination.PlansDenomination;
import com.freecharge.rest.rechargeplans.plansdenomination.PlansDenominationSNSService;
import com.freecharge.rest.rechargeplans.plansdenomination.PlansDenominationService;

@Controller
@RequestMapping("/operations/rechargeplans/*")
public class RechargePlanOperationController {
	
    private static final Logger LOGGER = LoggingFactory.getLogger(RechargePlanOperationController.class);
	
    @Autowired
    private OperatorCircleReadDAO operatorCircleReadDAO;
    
    @Autowired
    private OperatorCircleWriteDAO operatorCircleWriteDAO;
    
    @Autowired
    private PlansDenominationSNSService plansDenominationSNSService;
    
    @Autowired
    private PlansDenominationService plansDenominationService;
    
    @Autowired
    private FCProperties fcProperties;
    
	
    @RequestMapping(value = "getopcrblock/{operatorId}/{circleId}", method = RequestMethod.GET, produces = "application/json")
    @Csrf(exclude=true)
    @NoLogin
    public @ResponseBody ModelMap getOperatorCircleBlock(@PathVariable int operatorId, @PathVariable int circleId, ModelMap modelMap) {
        OperatorCircleBlock operatorCircleBlock = operatorCircleReadDAO.getOperatorCircleBlock(operatorId, circleId);
        if(operatorCircleBlock != null){
        	operatorCircleBlock.setErrorCode("00");
        	modelMap.addAttribute("operatorCircleBlock", operatorCircleBlock);
        }
        else{
        	OperatorCircleBlock opcrBlock = new OperatorCircleBlock();
        	opcrBlock.setErrorCode("11004");
        	opcrBlock.setErrorMessage("Operator Circle Block details not present");
        	modelMap.addAttribute("operatorCircleBlock", opcrBlock);
        }
        return modelMap;
    }
    
    
    @RequestMapping(value = "setopcrblock/{operatorId}/{circleId}/{is_blocked}", method = RequestMethod.POST, produces = "application/json")
    @Csrf(exclude=true)
    @NoLogin
    public @ResponseBody ModelMap setOperatorCircleBlock(@PathVariable int operatorId, @PathVariable int circleId, @PathVariable Boolean is_blocked,
			ModelMap modelMap, HttpServletRequest request) {
    	String curUserEmail = null;
    	String accessKey = null;
    	String secretKey = null;
    	OperatorCircleBlock operatorCircleBlock = new OperatorCircleBlock();
		try {
			curUserEmail = (String) request.getHeader(FCConstants.ADMIN_CUR_USER_EMAIL);
			accessKey = (String) request.getHeader(FCConstants.OPERATIONS_ACCESS_KEY);
			secretKey = (String) request.getHeader(FCConstants.OPERATIONS_SECRET_KEY);
			LOGGER.info("The admin user email is : " + curUserEmail);
		} catch(Exception e){
    		LOGGER.error("An error occurred trying to fetch the validation keys from request", e);
    		operatorCircleBlock.setErrorCode("11001");
    		operatorCircleBlock.setErrorMessage("Invalid Request Parameters");
    		modelMap.put("operatorCircleBlock", operatorCircleBlock);
    		return modelMap;
    	}
		
		try {
			String fcAccessKey = fcProperties.getOperationsAccessKey();
			String fcSecretKey = fcProperties.getOperationsSecretKey();
			if (fcAccessKey.equalsIgnoreCase(accessKey) && fcSecretKey.equalsIgnoreCase(secretKey)) {
				if (operatorId >= 0) {
					OperatorCircleBlock operatorBlock = operatorCircleReadDAO.getOperatorCircleBlock(operatorId,
							circleId);
					if (operatorBlock != null) {
						operatorCircleWriteDAO.updateOperatorCircleBlock(operatorId, circleId, is_blocked,
								curUserEmail);
					} else {
						operatorCircleWriteDAO.insertOperatorCircleBlock(operatorId, circleId, is_blocked,
								curUserEmail);
					}
					operatorCircleBlock.setErrorCode("00");
					operatorCircleBlock.setErrorMessage("No Error");
					operatorCircleBlock.setOperatorId(operatorId);
					operatorCircleBlock.setCircleId(circleId);
					operatorCircleBlock.setIsBlocked(is_blocked);
					operatorCircleBlock.setAdminUserEmail(curUserEmail);
					modelMap.put("operatorCircleBlock", operatorCircleBlock);
				}
			}
			else{
				operatorCircleBlock.setErrorCode("11002");
				operatorCircleBlock.setErrorMessage("Bad Request.Please enter valid access credentials");
				modelMap.put("operatorCircleBlock", operatorCircleBlock);
			}
		} catch (Exception e) {
			LOGGER.error("An error occurred trying to update operator circle block data for Operator Id : ["
					+ operatorId + "] Circle Id : [" + circleId + "]", e);
			operatorCircleBlock.setErrorCode("11003");
			operatorCircleBlock.setErrorMessage("Something went wrong.An internal error occurred");
			modelMap.put("operatorCircleBlock", operatorCircleBlock);
		}
		return modelMap;
	}
    
    

    @RequestMapping(value = "changePlanValidity", method = RequestMethod.POST)
    @Csrf(exclude=true)
    @NoLogin
	public @ResponseBody ModelMap changePlanValidity(@RequestBody PlansDenomination plansDenomination,
			final HttpServletRequest request, final HttpServletResponse response, ModelMap model)
			throws InvalidParameterException {
		if (plansDenomination != null) {
			try {
				if (checkPlansDenomObject(plansDenomination)) {
					plansDenominationService.setPlanDenominationCache(plansDenomination);
					LOGGER.info("The operatorId is [" + plansDenomination.getOperatorId() + "] Circle Id is : ["
							+ plansDenomination.getCircleId() + "] Is Invalid value is : ["
							+ plansDenomination.getIsInvalid() + "] Amount is : [" + plansDenomination.getAmount()
							+ "] Product Id is :[" + plansDenomination.getProductId() + "]");
					if ("Y".equalsIgnoreCase(plansDenomination.getIsInvalid())) {
						LOGGER.info("Plan marked as Invalid. Sending to topic");
						plansDenominationSNSService
								.publish("{\"message\" : " + PlansDenomination.getJsonString(plansDenomination) + "}");
						LOGGER.info("Plans Denomination successfully pushed to SNS topic");
					}
					model.addAttribute(plansDenomination);
				}
			} catch (Exception e) {
				LOGGER.error("An error occurred trying to send notification for plan denomination change", e);
			}
		} else {
			LOGGER.error("No plans denomination object present.Doing nothing");
		}
		return model;
	}
    
    
    @RequestMapping(value = "checkPlanValidity/{operatorId}/{circleId}/{amount}/{productId}/{planType}", method = RequestMethod.GET)
    @Csrf(exclude=true)
    @NoLogin
    public @ResponseBody ModelMap getPlanValidity(@PathVariable final String operatorId, @PathVariable final String circleId, 
			@PathVariable final String amount, @PathVariable final String productId, @PathVariable String planType,
			ModelMap model, final HttpServletRequest request, final HttpServletResponse response)
			throws InvalidParameterException {
		PlansDenomination plansDenomination = plansDenominationService.getPlansDenominationDO(operatorId, circleId,
				amount, productId, "", null, null, planType);
		String isInvalidString = plansDenominationService.getInvalidDenominationCache(operatorId, circleId, amount,
				productId);
		plansDenomination.setIsInvalid(isInvalidString);
		if (isInvalidString.isEmpty()) {
			LOGGER.info("Plans Denomination not found in the new plans cache. Going for old cache check");
			boolean isInvalid = false;
			isInvalid = plansDenominationService.getExistingInvalidDenoCache(operatorId, circleId, amount, productId,
					planType);
			if (isInvalid) {
				plansDenomination.setIsInvalid("Y");
				LOGGER.info("Invalid plan found in old existing cache, setting the isInvalid value to Y");
			}
		}
		return model.addAttribute("plansDenomination", plansDenomination);
	}
    
    @RequestMapping(value = "invaliddenominations/uploadfile", method = RequestMethod.POST)
    @Csrf(exclude=true)
    @NoLogin
    public  ResponseEntity<ModelMap>  uploadDenominations(@RequestParam("invaliddenominationsuploadfile") final MultipartFile file, final ModelMap model,
    final HttpServletRequest request, final HttpServletResponse response) throws IOException  {
    	List<PlansDenomination> successUploadPlansDenominationList = new ArrayList<>();
    	Set<String> fileStringSet = FCUtil.getStringSetFromFile(file);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		for (String planLineString : fileStringSet) {
			if (FCUtil.isEmpty(planLineString)) {
				continue;
			}
			String[] split = planLineString.split("\\|");
			/*In each line : operatorId,circleId,amount,productId,isInvalid,expiryDate(MM/DD/YYYY)*/
			final String YES = "Y";
			final String NO	 = "N";
			
			String operatorId = null;
			String circleId = null;
			String amount = null;
			String productId = null;
			String isInvalid = null;
			Date expiryDate = null;
			int size = split.length;
			
			if (size > 0) {
				operatorId = split[0];
			}

			if (size > 1) {
				circleId = split[1];
			}
			
			if (size > 2) {
				amount = split[2];
			}

			if (size > 3) {
				productId = split[3];
			}
			
			if (size > 4) {
				String invalidString = split[4];
				if (YES.equalsIgnoreCase(invalidString) || NO.equalsIgnoreCase(invalidString)) {
					isInvalid = split[4];
				}
			}
			
			if (size > 5){
				Date parsedDate = null;
				try{
					parsedDate = dateFormatter.parse(split[5]);
				}
				catch(Exception e){
					LOGGER.error("An Exception occurred in parsing the date string");
				}
				expiryDate = parsedDate;
			}

			if (FCUtil.isEmpty(operatorId)
					|| FCUtil.isEmpty(circleId) || FCUtil.isEmpty(amount)
					|| FCUtil.isEmpty(productId) || FCUtil.isEmpty(isInvalid)) {
				continue;
			}
			PlansDenomination plansDenomination = plansDenominationService.getPlansDenominationDO(operatorId, circleId, amount, productId, isInvalid, expiryDate, null,null);
			try {
				plansDenominationService.setPlanDenominationCache(plansDenomination);
				if("Y".equalsIgnoreCase(isInvalid)){
					plansDenominationSNSService.publish("{\"message\" : " + PlansDenomination.getJsonString(plansDenomination) + "}");
				}
				successUploadPlansDenominationList.add(plansDenomination);
			} catch (Exception ex) {
				LOGGER.error("Exception while uploading bulk denomination plans for circleId : " + circleId + ", operatorid :" + operatorId +  ", amount :" + amount, ex);
			    continue;
			}    
		}
		model.addAttribute("plansDenominationList", successUploadPlansDenominationList);
		return new ResponseEntity<ModelMap>(model, HttpStatus.OK);
    }
    
    private boolean checkPlansDenomObject(PlansDenomination plansDenomination){
    	if(plansDenomination != null){
    		String invalidString = null;
    		if("Y".equalsIgnoreCase(plansDenomination.getIsInvalid()) ||"N".equalsIgnoreCase(plansDenomination.getIsInvalid())){
    			invalidString = plansDenomination.getIsInvalid();
    		}
    		if (FCUtil.isEmpty(plansDenomination.getOperatorId())
					|| FCUtil.isEmpty(plansDenomination.getCircleId()) || FCUtil.isEmpty(plansDenomination.getAmount())
					|| FCUtil.isEmpty(plansDenomination.getProductId()) || FCUtil.isEmpty(invalidString)) {
				return false;
			}
    	} else{
    		return false;
    	}
    	return true;
    }

}
