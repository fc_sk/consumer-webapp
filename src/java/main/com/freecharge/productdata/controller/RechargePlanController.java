package com.freecharge.productdata.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.entity.DenominationType;
import com.freecharge.productdata.entity.ProductType;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.mongodb.DBObject;

@Controller
@RequestMapping("/admin/rechargeplans/*")
public class RechargePlanController extends BaseAdminController {
    public static final String RECHARGE_PLAN_LINK="admin/rechargeplans/home";

    public static final String RECHARGE_MODERATOR_LINK="admin/rechargeplans/moderation.htm";

    private Logger logger=LoggingFactory.getLogger(getClass());
    
    public static final String RECHARGE_PLAN_ID = "recharge_plan_id";
    public static final String RECHARGE_PLAN_NAME = "name";
    public static final String PRODUCT = "product";
    public static final String DENOMINATION_TYPE = "denomination";
    public static final String OPERATOR = "operator";
    public static final String CIRCLE = "circle";
    public static final String AMOUNT = "amount";
    public static final String MIN_AMOUNT = "min_amount";
    public static final String MAX_AMOUNT = "max_amount";
    public static final String TALKTIME = "talktime";
    public static final String VALIDITY = "validity";
    public static final String DESCRIPTION = "description";
    public static final String STATUS = "status";
    public static final String STICKY = "sticky";
    
    public static final String ALL = "all";
    
    public static final int INVALID = -1;
    public static final int ALL_CIRCLES =0;
    
    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private RechargePlanService   rechargePlanService;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    @NoLogin
    public String viewHome(@RequestParam Map<String, String> mapping,
            @ModelAttribute("AdminUsers") AdminUsers adminUsers, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        if (!hasAccess(request, AdminComponent.RECHARGE_PLANS)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute(WebConstants.ALL_DENOMINATION_TYPES, DenominationType.values());
        model.addAttribute(WebConstants.ALL_PRODUCT_TYPES, ProductType.values());
        List<ProductMaster> productMasters = operatorCircleService.getProducts();
        List<CircleMaster> circleMasters = operatorCircleService.getCircles();
        List<String> nameMasters = operatorCircleService.getNames();
        List<Map<String, Object>> operatorMap = operatorCircleService.getAllOperators();

        model.addAttribute(WebConstants.ALL_OPERATORS, operatorMap);
        model.addAttribute(WebConstants.ALL_PRODUCTS, productMasters);
        model.addAttribute(WebConstants.ALL_CIRCLES, circleMasters);
        model.addAttribute(WebConstants.ALL_NAMES, nameMasters);

        int operatorId = mapping.get(OPERATOR) != null ? Integer.parseInt(mapping.get(OPERATOR)) : INVALID;
        List<RechargePlan> rechargePlans = null;
        if (mapping.get("circle") != null && mapping.get("circle").equals(ALL)) {
            rechargePlans = rechargePlanService.getRechargePlansAllCircle(operatorId);
        } else {
            int circleId = mapping.get(CIRCLE) != null ? Integer.parseInt(mapping.get(CIRCLE)) : INVALID;
            rechargePlans = rechargePlanService.getRechargePlans(circleId, operatorId);
        }

        if (rechargePlans != null && rechargePlans.size() > 0) {
            model.addAttribute(WebConstants.RECHARGE_PLANS_AVAILABLE, true);
            model.addAttribute(WebConstants.ALL_RECHARGE_PLANS, rechargePlans);
        }
        int rechargePlanId = INVALID;
        try {
            rechargePlanId = Integer.parseInt(request.getParameter(RECHARGE_PLAN_ID));
        } catch (Exception e) {

        }
        if (rechargePlanId != INVALID) {
            RechargePlan rechargePlan = rechargePlanService.getRechargePlan(rechargePlanId);
            if (rechargePlan != null)
                model.addAttribute(WebConstants.RECHARGE_PLAN, rechargePlan);
        }
        return ViewConstants.RECHARGE_PLANS_HOME_VIEW;

    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @NoLogin
    public String doSave(@RequestParam Map<String, String> mapping, Model model)  {
		
        BigDecimal amount = new BigDecimal(mapping.get(AMOUNT).trim().isEmpty() ? 0.0 : Double.parseDouble(mapping
                .get(AMOUNT).trim()));
        BigDecimal min_amount = new BigDecimal(mapping.get(MIN_AMOUNT).trim().isEmpty() ? 0.0 : Double.parseDouble(mapping
                .get(MIN_AMOUNT).trim()));
        BigDecimal max_amount = new BigDecimal(mapping.get(MAX_AMOUNT).trim().isEmpty() ? 0.0 : Double.parseDouble(mapping
                .get(MAX_AMOUNT).trim()));
        BigDecimal talktime = new BigDecimal(mapping.get(TALKTIME).trim().isEmpty() ? 0.0 : Double.parseDouble(mapping
                .get(TALKTIME).trim()));
		
        String validity = mapping.get(VALIDITY).trim();
		int circleId;
		if(mapping.get(CIRCLE) !=null && mapping.get(CIRCLE).equals(ALL)){
		    circleId = ALL_CIRCLES;
		}
		else {
		    circleId = Integer.parseInt(mapping.get(CIRCLE));
		}
		int operatorId = Integer.parseInt(mapping.get(OPERATOR));
        int productId = Integer.parseInt(mapping.get(PRODUCT));
        int rechargePlanId = INVALID;
        try{
        	rechargePlanId = Integer.parseInt(mapping.get(RECHARGE_PLAN_ID));
        } catch(Exception e) {
            logger.info("No Recharge Plan id provided.",e);
        }

        RechargePlan rechargePlan = new RechargePlan();
        if (rechargePlanId != INVALID) {
            rechargePlan = rechargePlanService.getRechargePlan(rechargePlanId);
            if (rechargePlan == null)
                rechargePlan = new RechargePlan();
        }


        rechargePlan.setName(mapping.get(RECHARGE_PLAN_NAME).trim());
        rechargePlan.setProductType(productId);
        rechargePlan.setDenominationType(mapping.get(DENOMINATION_TYPE).trim());
        rechargePlan.setDescription(mapping.get(DESCRIPTION).trim());
        rechargePlan.setAmount(amount);
        rechargePlan.setMinAmount(min_amount);
        rechargePlan.setMaxAmount(max_amount);
        rechargePlan.setTalktime(talktime);
        rechargePlan.setValidity(validity);
        rechargePlan.setCircleMasterId(circleId);
        rechargePlan.setOperatorMasterId(operatorId);
        rechargePlan.setCreatedTime(new Timestamp(new Date().getTime()));
        rechargePlan.setSticky(true);

        if(rechargePlanId != INVALID) {
            if(circleId == ALL_CIRCLES) {
                rechargePlanService.insertRechargePlanAllCircles(rechargePlan);
                rechargePlanService.updateRechargePlanAllCircle(rechargePlan);
                model.addAttribute(WebConstants.STATUS, "Updated");
                return "jsonView";
            }
            else {
                rechargePlanService.forceUpdateRechargePlan(rechargePlan);
                rechargePlanService.updateRechargePlanSticky(rechargePlan);
                model.addAttribute(WebConstants.STATUS, "Updated");
                return "jsonView";
            }
        } else {
        	try {
        	    if(circleId == ALL_CIRCLES) {
        	        rechargePlanService.insertRechargePlanAllCircles(rechargePlan);
                    rechargePlanService.updateRechargePlanAllCircle(rechargePlan);
                    model.addAttribute(WebConstants.STATUS, "Updated");
                    return "jsonView";
        	    }
        	    else{
        	        rechargePlan = rechargePlanService.insertRechargePlan(rechargePlan);
        	    }
    		} catch (Exception exception) {
    			model.addAttribute(WebConstants.STATUS, "Error");
    			model.addAttribute(WebConstants.ERROR_MESSAGE, "The recharge plan of this amount-name already exists.");
    			logger.error("Exception occured while saving recharge plan ",exception);
    			return "jsonView";
    		}
        }

        List<RechargePlan> rechargePlans = rechargePlanService.getRechargePlans(circleId, operatorId);
        if (rechargePlans != null && rechargePlans.size() > 0) {
            model.addAttribute(WebConstants.RECHARGE_PLANS_AVAILABLE, true);
            model.addAttribute(WebConstants.ALL_RECHARGE_PLANS, rechargePlans);
        }
        return ViewConstants.RECHARGE_PLANS_LIST_VIEW;
    }
    
    /*Upload bulk recharge plans*/
    @RequestMapping(value = "uploadfile", method = RequestMethod.POST)
    @NoLogin
    public String upload(@RequestParam("planuploadfile") final MultipartFile file, final Model model,
    final HttpServletRequest request, final HttpServletResponse response) throws IOException  {
    	Set<String> fileStringSet = FCUtil.getStringSetFromFile(file);
		List<RechargePlan> successUploadPlanList = new ArrayList<>();
		for (String planLineString : fileStringSet) {
			if (FCUtil.isEmpty(planLineString)) {
				continue;
			}
			String[] split = planLineString.split("\\|");
			/*In each line : Name,OperatorId,CircleId,amount,talktime,validity,description,productTypeId*/
			String name = null;
			String operatorId = null;
			String circleId = null;
			String amount = null;
			String talktime = null;	
			String validity = null;
			String description = null;
			String productId = null;
			
			int size = split.length;
			if (size > 0) {
				name = split[0];
			}

			if (size > 1) {
				operatorId = split[1];
			}
			
			if (size > 2) {
				circleId = split[2];
			}

			if (size > 3) {
				amount = split[3];
			}
			
			if (size > 4) {
				talktime = split[4];
			}

			if (size > 5) {
				validity = split[5];
			}

			if (size > 6) {
				description = split[6];
			}
			
			if (size > 7) {
				productId = split[7];
			}

			if (FCUtil.isEmpty(name) || FCUtil.isEmpty(operatorId)
					|| FCUtil.isEmpty(circleId) || FCUtil.isEmpty(amount)
					|| FCUtil.isEmpty(talktime) || FCUtil.isEmpty(validity)
					|| FCUtil.isEmpty(description) || FCUtil.isEmpty(productId)) {
				continue;
			}
			
			try {
				RechargePlan rechargePlan = new RechargePlan();
				rechargePlan.setName(name.trim());
				rechargePlan.setOperatorMasterId(Integer.parseInt(operatorId.trim()));
				rechargePlan.setCircleMasterId(Integer.parseInt(circleId.trim()));
				BigDecimal amountValue = new BigDecimal(amount.trim().isEmpty() ? 0.0 : Double.parseDouble(amount.trim()));
				rechargePlan.setAmount(amountValue);
				BigDecimal talktimeValue = new BigDecimal(talktime.trim().isEmpty() ? 0.0 : Double.parseDouble(talktime.trim()));
				rechargePlan.setTalktime(talktimeValue);				
				rechargePlan.setValidity(validity.trim());
				if (description.trim().length() > 304) {
					continue;
				}
				rechargePlan.setDescription(getTrimmedDescription(description));
				rechargePlan.setProductType(Integer.parseInt(productId));
				rechargePlan.setDenominationType("Fixed");
				rechargePlan.setMinAmount(new BigDecimal(0.0));
				rechargePlan.setMaxAmount(new BigDecimal(0.0));
				rechargePlan.setCreatedTime(new Timestamp(new Date().getTime()));
		        rechargePlan.setSticky(true);
		        List<RechargePlan> rechargePlanList = rechargePlanService.getRechargePlanList(Integer.parseInt(circleId.trim()), Integer.parseInt(operatorId.trim()), Integer.parseInt(productId), Float.parseFloat(amount), name.trim());
				if (!FCUtil.isEmpty(rechargePlanList)) {
					if (rechargePlanList.size() > 1) {
						logger.error("Multiple plan exist - circle :" + circleId + ", operatorId :" + operatorId + " , amount :"+ amount);
						throw new Exception();
					}
					RechargePlan plan = rechargePlanList.get(0);
		            plan.setStatus(false);
					rechargePlanService.updateRechargePlanStatus(rechargePlanList);
				}
				RechargePlan rechargePlanNew = rechargePlanService.insertRechargePlanWithoutValidation(rechargePlan);
				successUploadPlanList.add(rechargePlanNew);	
			} catch (Exception ex) {
				logger.error("Exception while uploading bulk rechargeplans for circleId : " + circleId + ", operatorid :" + operatorId +  ", amount :" + amount, ex);
			    continue;
			}    
		}
		
		if (FCUtil.isEmpty(successUploadPlanList)) {
			logger.info("No success upload");
		}
		model.addAttribute("excelRechargePlanData", successUploadPlanList);
		return "rechargePlanDataExcelSummary";
    }

    private String getTrimmedDescription(String description) {
		if (!FCUtil.isEmpty(description)) { 
			/*Linux uses \n for a new-line, Windows \r\n and old Mac \r for new line break*/
			return description.trim().replaceAll("\\r\\n|\\r|\\n", " ").replaceAll(",", " ").replaceAll("\\s+", " ");
		}
		return null;
	}
    
	@RequestMapping(value = "updateStatus", method = RequestMethod.POST)
    @NoLogin
    public String updateStatus(@RequestParam Map<String, String> mapping, Model model) {
        ArrayList<Integer> rechargePlanIdArray = new ArrayList<>(mapping.size());
        try {
            String rechargePlanIds = mapping.get(RECHARGE_PLAN_ID);
            String[] planIdArray = rechargePlanIds.split(",");
            for (String planId : planIdArray) {
                rechargePlanIdArray.add(Integer.parseInt(planId));
            }
        } catch (Exception e) {
            logger.error("Failed to parse the recharge plan ids.", e);
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Failed to parse the plan ids.");
            return "jsonView";
        }

        boolean status = true;
        try {
            status = Boolean.parseBoolean(mapping.get(STATUS));
        } catch (Exception e) {
            logger.error("Unable to parse the status from the POST Method.", e);
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Failed to parse the status of plans.");
            return "jsonView";
        }

        List<RechargePlan> rechargePlanList = new ArrayList<>();
        for (Integer rechargePlanId : rechargePlanIdArray) {
            if (rechargePlanId != INVALID) {
                RechargePlan rp = rechargePlanService.getRechargePlan(rechargePlanId);
                rp.setStatus(status);
                rp.setCreatedTime(new Timestamp(new Date().getTime()));
                rechargePlanList.add(rp);
            }
        }

        if (rechargePlanList == null || rechargePlanList.isEmpty()) {
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "The plan does not exist.");
            return "jsonView";
        }
        rechargePlanService.updateRechargePlanStatus(rechargePlanList);
        model.addAttribute(WebConstants.STATUS, "Updated");
        return "jsonView";
    }

    @RequestMapping(value = "updateSticky", method = RequestMethod.POST)
    @NoLogin
    public String updateSticky(@RequestParam Map<String, String> mapping, Model model) {
        ArrayList<Integer> rechargePlanIdArray = new ArrayList<>(mapping.size());
        try {
            String rechargePlanIds = mapping.get(RECHARGE_PLAN_ID);
            String[] planIdArray = rechargePlanIds.split(",");
            for (String planId : planIdArray) {
                rechargePlanIdArray.add(Integer.parseInt(planId));
            }
        } catch (Exception e) {
            logger.error("Failed to parse the recharge plan ids.", e);
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Failed to parse the plan ids.");
            return "jsonView";
        }

        boolean sticky = true;
        try {
            sticky = Boolean.parseBoolean(mapping.get(STICKY));
        } catch (Exception e) {
            logger.error("Unable to parse the status from the POST Method.", e);
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Failed to parse the status of plans.");
            return "jsonView";
        }

        List<RechargePlan> rechargePlanList = new ArrayList<>();
        for (Integer rechargePlanId : rechargePlanIdArray) {
            if (rechargePlanId != INVALID) {
                RechargePlan rp = rechargePlanService.getRechargePlan(rechargePlanId);
                rp.setSticky(sticky);
                rp.setCreatedTime(new Timestamp(new Date().getTime()));
                rechargePlanList.add(rp);
            }
        }
        if (rechargePlanList == null || rechargePlanList.isEmpty()) {
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "The plan does not exist.");
            return "jsonView";
        }
        rechargePlanService.updateRechargePlanSticky(rechargePlanList);
        model.addAttribute(WebConstants.STATUS, "Updated");
        return "jsonView";
    }
	/* functions for Recharge Plan Moderation Tool on Admin Panel By Rajani*/
	
	@RequestMapping(value = "moderation", method = RequestMethod.GET)
    public String getRechargePlanModeration(Model model) throws Exception {
	    List<DBObject> rechargePlansInModeration = rechargePlanService.getAllRechargePlansInModeration();
	    List<CircleMaster> circleMasters = operatorCircleService.getCircles();
        List<String> nameMasters = operatorCircleService.getNames();
        List<Map<String, Object>> operatorMap = operatorCircleService.getAllOperators();;
        model.addAttribute(WebConstants.ALL_OPERATORS, operatorMap);
        model.addAttribute(WebConstants.ALL_CIRCLES, circleMasters);
        model.addAttribute(WebConstants.ALL_NAMES, nameMasters);
        model.addAttribute(WebConstants.ALL_RECHARGE_PLANS_IN_MODERATION, rechargePlansInModeration);
        return ViewConstants.RECHARGE_PLANS_MODERATION_VIEW;
    }

    @RequestMapping(value = "moderation/edit", method = RequestMethod.GET)
    public String editRechargePlanModeration(@RequestParam Map<String, String> mapping, Model model) throws Exception {
        model.addAttribute("planId", mapping.get("_id"));
        return ViewConstants.EDIT_RECHARGE_PLANS_MODERATION;
    }
}
