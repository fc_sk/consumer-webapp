package com.freecharge.productdata.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.freecharge.common.util.FCUtil;

public class IreffConstants {

    
    public static final String             EMPTY_DESC_TOPUP_FULL    = "Full Talktime";
    public static final String             EMPTY_DESC_TOPUP_REGULAR = "Talktime of Rs. %s on a recharge of Rs. %s";
    public static final String              CATEGORY_TOPUP           = "topup";
    public static final String              CATEGORY_DATA2G          = "data";
    public static final String              CATEGORY_DATA3G          = "3g";
    public static final String              CATEGORY_OTHER           = "other";

    public static final List<String> validCategoryList = Arrays.asList(CATEGORY_TOPUP, CATEGORY_DATA2G, CATEGORY_DATA3G, CATEGORY_OTHER); 

    public static final Map<String, String> CIRCLE_IREFF_LOOKUP  = FCUtil.createMap(
            // Freecharge name, Ireff long name, //Ireff short code
            "Andhra Pradesh",   "AndhraPradesh",     //"AP",
            "Assam",            "Assam",             //"AS",
            "Bihar",            "BiharJharkhand",    //"BR",
            //"Chattisgarh",
            "Chennai",          "Chennai",           //"Chennai",
            "Delhi",            "Delhi",             //"Delhi",
            "Gujarat",          "Gujarat",           //"GJ",
            "Haryana",          "Haryana",           //"HR",
            "Himachal Pradesh", "HimachalPradesh",   //"HP",
            "Karnataka",        "Karnataka",         //"KA",
            "Kerala",           "Kerala",            //"KL",
            "Kolkata",          "Kolkata",           //"Kolkata",
            "Madhya Pradesh",   "MadhyaPradesh",     //"MP",
            "Maharashtra",      "Maharashtra",       //"MH",
            "Mumbai",           "Mumbai",            //"Mumbai",
            "North East",       "NorthEast",         //"NE",
            "Orissa",           "Orissa",            //"OR",
            "Punjab",           "Punjab",            //"PB",
            "Rajasthan",        "Rajasthan",         //"RJ",
            "Tamil Nadu",       "TamilNadu",         //"TN",
            "Uttar Pradesh (E)", "UttarPradeshEast", //"UPE",
            "Uttar Pradesh (W)", "UttarPradeshWest", //"UPW",
            "West Bengal",       "WestBengal",       //"WB"
            //"Uttar Pradesh"
            //"ALL"
            "JK",                "JammuKashmir"      //"JK",
            //"UTTARANCHAL"
            // ----------
            );

    public static final Map<String, String> SERVICE_IREFF_LOOKUP = FCUtil.createMap(
            // Freecharge operator code, Ireff service name
            "Aircel",                    "Aircel",
            "Airtel",                    "Airtel",
            "BSNL",                      "BSNL",
            "Tata Docomo GSM",           "Docomo",
            "Idea",                      "Idea",
            "Loop",                      "Loop",
            "MTNL Delhi",                "MTNL",
            "MTNL Mumbai",               "MTNL",
            "Reliance-CDMA",             "RelianceCDMA",
            "Reliance GSM",              "RelianceGSM",
            "Tata Indicom",              "Indicom",
            "Tata Docomo CDMA",          "Indicom",
            "Uninor",                    "Uninor",
            "Vodafone",                  "Vodafone",
            "Virgin GSM",                "VirginGSM",
            "Virgin CDMA",               "VirginCDMA",
            "T24",                       "T24",
            "MTS",                       "MTS",
            "Videocon Mobile",           "Videocon"
            //"Indicom-Walky",             "VirginCDMA",
            //"Dish Tv",
            //"Big Tv",
            //"Airtel Tv",
            //"Sun Tv",
            //"Tata Sky",
            //"Videocon Tv",
            //"Tata Photon",
            //"Reliance Netconnect",
            //"VIRGIN",
            //"MTS MBrowse",
            //"MTS Mblaze",
            // -----------
            // Unused Ireff entries are below
            // -----------
            // Unmapped Ireff entries
            //"T24",
            //"VirginGSM"
            );
}
