package com.freecharge.util;

import java.util.Calendar;

import com.freecharge.freefund.dos.entity.FreefundUsageHistory;
import com.google.gson.Gson;


public class JsonUtil {
    
    public static String getJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }
    
    public static Object getObject(String json, Class className) {
        Gson gson = new Gson();
        return gson.fromJson(json, className);
    }
    
    public static void main(String[] args) {
        FreefundUsageHistory freefundUsageHistory = new FreefundUsageHistory();
        freefundUsageHistory.setAction("APPLY");
        freefundUsageHistory.setCode("SDFSDFDSF");
        freefundUsageHistory.setFreefundClassId(876);
        Calendar cal = Calendar.getInstance();
        freefundUsageHistory.setTimestamp(cal.getTime());
        
        String json = getJson(freefundUsageHistory);
        System.out.println(json);
        
        FreefundUsageHistory fuh = (FreefundUsageHistory)getObject(json, FreefundUsageHistory.class);
        System.out.println(fuh.getCode());
    }
}
