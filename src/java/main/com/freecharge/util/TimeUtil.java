package com.freecharge.util;

import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This class would have date time utilities
 **/
public class TimeUtil {

    private static Logger logger = LoggingFactory.getLogger(TimeUtil.class);

    /**
     * This method converts time from one timezone to another
     * The time passed should be in the passed dateFormat.
     * The time returned would also be in the same format.
     * @param inputTz
     * @param outputTz
     * @param time in input timezone
     * @param pattern in which the date is passed and returned eg. "yyyy-MM-dd HH:mm:ss"
     * @throws UnableToParseDateException
     **/
    public static String convertTimeZone(String inputTz, String outputTz, String time, String pattern) {
        logger.debug("Time in " + inputTz + " : is to be converted to " + outputTz);
        DateTimeZone fromTimeZone = DateTimeZone.forID(inputTz);
        DateTimeZone toTimeZone = DateTimeZone.forID(outputTz);
        DateTimeFormatter f = DateTimeFormat.forPattern(pattern);
        DateTime date = null;
        String outoutTzDate = null;

        try {
            date = f.withZone(fromTimeZone).parseDateTime(time);
            outoutTzDate = f.withZone(toTimeZone).print(date);
        } catch (IllegalArgumentException e) {
            logger.error("Unable to parse date " + time + " to pattern " + pattern, e);
        }
        return outoutTzDate;
    }

    /**
     * This method converts time from one timezone to another
     * The time passed should be in the passed dateFormat.
     * The time returned would also be in the same format.
     * @param inputTz
     * @param outputTz
     * @param time in input timezone
     * @param inputPattern in which the date is passed eg. "yyyy-MM-dd HH:mm:ss"
     * @param outputPattern in which the date is returned eg. "yyyy-MM-dd HH:mm:ss"
     * @throws UnableToParseDateException
     **/
    public static String convertTimeZoneWithPattern(String inputTz, String outputTz, String time,
                                                    String inputPattern, String outputPattern) {
        logger.debug("Time in " + inputTz + "(" + time + ") is to be converted to " + outputTz);
        DateTimeZone fromTimeZone = DateTimeZone.forID(inputTz);
        DateTimeZone toTimeZone = DateTimeZone.forID(outputTz);
        DateTimeFormatter inputFormat = DateTimeFormat.forPattern(inputPattern);
        DateTimeFormatter outputFormat = DateTimeFormat.forPattern(outputPattern);
        DateTime date = null;
        String outoutTzDate = null;

        try {
            date = inputFormat.withZone(fromTimeZone).parseDateTime(time);
            outoutTzDate = outputFormat.withZone(toTimeZone).print(date);
        } catch (IllegalArgumentException e) {
            logger.error("Unable to parse date " + time, e);
        }
        return outoutTzDate;
    }

    /**
     * This method finds the difference between the two dates passed
     * @param fromTime
     * @param toTime
     * @return difference between the above times in minutes
     **/
    public static long getDiffInMinutes(Date fromTime, Date toTime) {
        logger.debug("Date 1: " + fromTime);
        logger.debug("Date 2 : " + toTime);

        if (null == fromTime || null == toTime) {
            throw new IllegalArgumentException("fromDate " + fromTime + " or toDate " + toTime + " is null");
        }

        long timeDiff = toTime.getTime() - fromTime.getTime();
        long diffMinutes = timeDiff / (60 * 1000);
        logger.debug("Diff In Minutes : " + diffMinutes);

        return diffMinutes;
    }

    /**
     * This method finds the difference between the time passed and the current time
     * @param date
     * @return time difference from current time in minutes
     **/
    public static long getDiffInMinutesFromCurrentTime(Date date) {
        return getDiffInMinutes(date, new Date());
    }
}
