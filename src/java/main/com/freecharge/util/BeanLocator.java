package com.freecharge.util;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.SpringBeanLocator;

public class BeanLocator {

	public static final String BEAN_NAME_APP_CONFIG_SERVICE = AppConfigService.class.getSimpleName();

	public static AppConfigService getAppConfigServiceBean() {
		return (AppConfigService) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_APP_CONFIG_SERVICE);
	}

	public static FCProperties getFCPropertiesBean() {
		return (FCProperties) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_FCPROPERTIES);
	}

}
