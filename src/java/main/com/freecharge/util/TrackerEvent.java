package com.freecharge.util;

import com.freecharge.common.util.FCUtil;
import com.freecharge.tracker.TrackerEventVo;

public enum TrackerEvent {

    SIGNUP_EMAIL_ATTEMPT ("signup_email_att"), // Event triggers when a user attempted to sign up via email
    SIGNUP_EMAIL_SUCCESS ("signup_email_suc"), // Event triggers post successful signup via email
    SIGNUP_FACEBOOK_ATTEMPT ("signup_fb_att"), // Event triggers when a user attempted to sign up via FB
    SIGNUP_FACEBOOK_SUCCESS ("signup_fb_suc"), // Event triggers post successful signup via FB
    SIGNUP_GOOGLE_ATTEMPT ("signup_gl_att"), // Event triggers when a user attempted to sign up via google
    SIGNUP_GOOGLE_SUCCESS ("signup_gl_suc"), // Event triggers post successful signup via google

    SIGNIN_EMAIL_ATTEMPT ("signin_email_att"), // Event triggers when a user attempted to sign in via email
    SIGNIN_EMAIL_SUCCESS ("signin_email_suc"), // Event triggers post successful signin via email
    SIGNIN_FACEBOOK_ATTEMPT ("signin_fb_att"), // Event triggers when a user attempted to sign in via FB
    SIGNIN_FACEBOOK_SUCCESS ("signin_fb_suc"), // Event triggers post successful signin via FB
    SIGNIN_GOOGLE_ATTEMPT ("signin_gl_att"), // Event triggers when a user attempted to signin via google
    SIGNIN_GOOGLE_SUCCESS ("signin_gl_suc"), // Event triggers post successful signin via google

    COUPON_ORDER_RANK ("coupon_order_rank"), // Event triggered when the coupons are ordered by rank
    COUPON_PRICED ("coupon_priced"), // Event triggered when the coupons are ordered by rank
    COUPON_RECOMMENDATION("recoc"), // Event triggered when the coupons are ordered by recommendation
    COUPON_RECOMMENDATION_NO_SHOW("nrecoc"), // Event triggered when the coupons are available for ordering by recommendations but not shown for AB Testing.
    COUPON_CITY_FILTER("city_filter"), // Event triggered when the coupons are ordered by city filter
    COUPON_CITY_FILTER_NO_SHOW("no_city_filter"), // Event triggered when the coupons are available for ordering by city filter but not shown for AB Testing.
    COUPON_DESKTOP_USER_IS_LOGGED_IN("clogiin"),  //// Event triggered when the user is logged when coupons are loaded on desktop site.
    COUPON_DESKTOP_CITY_FILTER_DONE("ccityf"),   // Event triggered when city filtering is done for a user while loading coupons on web.
    DONT_USE ("dontuse");

    public final String eventName;

    private TrackerEvent(String eventName) {
        if (eventName == null || eventName.length() > 20) {
            throw new IllegalArgumentException(
                    "Expected event name up to 20 characters, found: " + FCUtil.stringify(eventName));
        }
        this.eventName = eventName;
    }

    public TrackerEventVo createTrackingEvent() {
        TrackerEventVo trackerEventVo = new TrackerEventVo();
        trackerEventVo.setEventName(eventName);
        return trackerEventVo;
    }

}
