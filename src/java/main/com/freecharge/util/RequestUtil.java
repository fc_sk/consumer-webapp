package com.freecharge.util;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.service.UADetectorServiceFactory;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.common.base.Strings;

/**
 * User: abhi
 * Date: 23/7/13
 * Time: 12:45 PM
 */
public class RequestUtil {
    private static Logger logger = LoggingFactory.getLogger(RequestUtil.class);
    private static final String[] IP_HEADERS = { "True-Client-IP","X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR","HTTP_X_FORWARDED","HTTP_X_CLUSTER_CLIENT_IP","HTTP_CLIENT_IP","HTTP_FORWARDED_FOR","HTTP_FORWARDED","HTTP_VIA","REMOTE_ADDR" };
    public static final String UNKNOWN_IP_IDENTIFIER = "unknown";
    
    public static String getOSFromUserAgent(String userAgent) {
        String operatingSystem = "";
        ReadableUserAgent agent = UADetectorServiceFactory.getResourceModuleParser().parse(userAgent);

        if (agent != null) {
            operatingSystem = agent.getOperatingSystem().getName();
        }
        return operatingSystem;
    }

    public static String getBrowserFromUserAgent(String userAgent) {
        String browser = "";
        ReadableUserAgent agent = UADetectorServiceFactory.getResourceModuleParser().parse(userAgent);
        if (agent != null) {
            browser = agent.getName();
        }
        return browser;
    }

    public static String getUserAgent(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        if(userAgent==null || userAgent.isEmpty()) {
            userAgent= request.getHeader("user-agent");
        }
        return userAgent;    
    }

    public static String getClientIpAddr(HttpServletRequest request) {
        String forwardedForIpHeader = Strings.nullToEmpty(request.getHeader("X-Forwarded-For"));
        if ("".equals(forwardedForIpHeader)) {
            logger.error("Empty X-Forwarded-For header");
        } else {
            logger.debug("Ip header is - " + forwardedForIpHeader);
        }

        for (String ipHeader : IP_HEADERS) {
            String ip = Strings.nullToEmpty(request.getHeader(ipHeader));

            if ("".equals(ip) || UNKNOWN_IP_IDENTIFIER.equals(ip)) {
                continue;
            }
            logger.info("debugmsg: ipHeader: "+ipHeader+" ipvalue: "+ip);
            return ip;
        }
        logger.info("debugmsg: passing remoteaddr: "+request.getRemoteAddr());
        return request.getRemoteAddr();
    }
    
    public static String getRealClientIpAddress(HttpServletRequest request) {
        String commaSeparatedIps = Strings.nullToEmpty(getClientIpAddr(request));
        logger.info("debugmsg: commaSeparatedIps: "+commaSeparatedIps);
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedIps, ",");
        String ipAddress = null;
        if (tokenizer.hasMoreTokens()) {
            ipAddress = tokenizer.nextToken();
        }
        logger.info("debugmsg: finalipaddress passed: "+ipAddress);
        return ipAddress;
    }

}

