package com.freecharge.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomHttpClientBuilder {
	
	public static final String TLS_V_1_2 = "TLSv1.2";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomHttpClientBuilder.class);
	
	public static CloseableHttpClient getCustomHttpClient(String requestURL, int connectTimeout, int readTimeout) {
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectTimeout)
                .setSocketTimeout(readTimeout).build();
        HttpClientBuilder httpClientBuilder = HttpClients.custom().setDefaultRequestConfig(requestConfig);
        if (requestURL.startsWith("https")) {
            setSSLRelatedConfig(httpClientBuilder);
        }
        CloseableHttpClient httpClient = httpClientBuilder.build();
        return httpClient;
    }
	
	private static void setSSLRelatedConfig(HttpClientBuilder httpClientBuilder) {
        try {
            SSLContext sslContext = SSLContext.getInstance(TLS_V_1_2);
            sslContext.init(null, new TrustManager[] { new X509TrustManager() {

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }
            } }, new SecureRandom());

            httpClientBuilder.setSSLSocketFactory((LayeredConnectionSocketFactory) new SSLSocketFactory(sslContext));
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Exception executing https request", e);
        } catch (KeyManagementException e) {
            LOGGER.error("Exception executing https request", e);
        }
    }

}
