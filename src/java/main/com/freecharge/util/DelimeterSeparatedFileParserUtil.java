package com.freecharge.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DelimeterSeparatedFileParserUtil {
	
	public static final String COMMA_DELIMETER = ",";
	
	public static interface ExecuterFunction{
		public void execute(List<String> columnHeaders, List<List<String>> rowRecords);
	}

	private static Logger logger = Logger.getLogger(DelimeterSeparatedFileParserUtil.class);
	
	public static void readDataFromLinesAndExecute(File file, Integer recordsToReadPerExecution, String delimeter, ExecuterFunction executerFunction) throws Exception {
		FileReader fr = new FileReader(file);
		BufferedReader br = null;
		String lineData = "";
		try {
			br = new BufferedReader(fr);
			List<String> columnHeaders = getSplittedDataBy(br.readLine(), delimeter);
			logger.info("Read column : " + new ObjectMapper().writeValueAsString(columnHeaders));
			List<List<String>> rowRecords = new LinkedList<>();
			int rowCounter = 0;
			while ((lineData = br.readLine()) != null) {
				List<String> rowRecord = getSplittedDataBy(lineData, delimeter);
				rowRecords.add(rowRecord);
				rowCounter++;
				logger.info("Total rows read : " + rowCounter);
				if(rowCounter % recordsToReadPerExecution == 0) {
					executerFunction.execute(columnHeaders, rowRecords);
					rowRecords = new LinkedList<>();
				}
			}
			if(rowCounter % recordsToReadPerExecution != 0) {
				executerFunction.execute(columnHeaders, rowRecords);
			}
		} catch (Exception e) {
			logger.error("Exception caught while parsing file, ", e);
			throw e;
		}finally{
			br.close();
		}
	}
	
	private static final List<String> getSplittedDataBy(String data, String delimeter) {
		String[] tokens = data.split(delimeter);
		List<String> splittedData = new ArrayList<>(tokens.length);
		for(String token : tokens) {
			splittedData.add(token);
		}
		return splittedData;
	}
}
