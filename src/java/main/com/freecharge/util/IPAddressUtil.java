package com.freecharge.util;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 3/27/14
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class IPAddressUtil {
    /**
     * This method checks whether given IP is in Given range. IPv4.
     * Please check example below in main method
     * @param ip
     * @param ipRange - in cidr notation
     * @return
     */
    public static Boolean isIpInRange(String ip, String ipRange) throws UnknownHostException {
        //Process ip address
        InetAddress addr = InetAddress.getByName(ip);
        String ipBits = new BigInteger(1, addr.getAddress()).toString(2);

        //process ip range
        String ipRangeArr [] = ipRange.split("/");
        if (ipRangeArr.length!=2){
            throw new UnknownHostException("IP range is not valid. Please specify in cidr format.");
        }
        InetAddress rAddr = InetAddress.getByName(ipRangeArr[0]);
        String rangeBits = new BigInteger(1, rAddr.getAddress()).toString(2);
        Integer msbLength = Integer.parseInt(ipRangeArr[1]);
        return ipBits.substring(0, msbLength).equals(rangeBits.substring(0, msbLength));
    }

    public static void main(String ... args){
        String iprange = "192.168.12.0/23";
        String cip = "192.168.12.167";
        String wip = "192.168.14.167";

        try {
            System.out.println("This is in range: "+isIpInRange(cip, iprange));
            System.out.println("This is not in range: "+isIpInRange(wip, iprange));
        }catch (Exception e){

        }
    }
}