package com.freecharge.util;

import java.util.Arrays;


/**
 * Cyclic (rolling) representation of integers posing as an infinite sequence. A static helper method to interleave
 * integer weights (useful for splitting traffic for A/B testing, partial roll-out etc) is included.
 *
 */
public class RollingIntSequence {

    private volatile int index = 0;
    private final int[] ns;

    public RollingIntSequence(final int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            throw new IllegalArgumentException(
                    "Expected non-empty array of integers, but found " + (numbers == null ? "NULL"
                            : Arrays.toString(numbers)));
        }
        this.ns = numbers.clone();
    }

    /**
     * Return next integer from the sequence in a lock-free fashion (uses only a half barrier for `index`). Overlaps
     * in result are possible in n/Integer.MAX_VALUE (n = concurrency) cases when `index` approaches Integer.MAX_VALUE.
     * @return next integer
     */
    public int next() {
        if (index >= Integer.MAX_VALUE) {
            index = 0;
        }
        return ns[index++ % ns.length];
    }

    public static RollingIntSequence createInterleaved(int... ranges) {
        final int[] ns = interleave(ranges);
        if (ns.length == 1) {
            final int n = ns[0];
            return new RollingIntSequence(ns) {
                @Override
                public int next() {
                    return n;
                }
            };
        }
        return new RollingIntSequence(interleave(ranges));
    }

    private static int[] interleave(int[] ranges) {
        final int[] delvrd = new int[ranges.length];
        final double[] ratios = new double[ranges.length];
        int totalWeight = 0;
        for (int i = 0; i < ranges.length; i++) {
            totalWeight += ranges[i] <= 0 ? 0 : ranges[i];
        }
        final int[] result = new int[totalWeight];
        for (int i = 0; i < totalWeight; i++) {
            result[i] = next(ranges, delvrd, ratios);
        }
        return result;
    }

    public static final int NOT_FOUND = -1;

    private static int next(int[] ranges, int[] delvrd, double[] ratios) {
        final int lowestIndex = lowestRatioIndex(ranges, delvrd, ratios);
        if (lowestIndex != NOT_FOUND) {
            return nextInternal(lowestIndex, ranges, delvrd, ratios);
        }
        return NOT_FOUND;
    }

    private static int lowestRatioIndex(int[] ranges, int[] delvrd, double[] ratios) {
        int index = NOT_FOUND;
        double lowest = Double.MAX_VALUE;
        for (int i = 0; i < ranges.length; i++) {
            if (ratios[i] < lowest && delvrd[i] < ranges[i]) {
                lowest = ratios[i];
                index = i;
            }
        }
        return index;
    }

    private static int nextInternal(final int lowestIndex, int[] ranges, int[] delvrd, double[] ratios) {
        if (lowestIndex < 0) {
            return NOT_FOUND;
        }
        delvrd[lowestIndex]++;
        ratios[lowestIndex] = ((double) delvrd[lowestIndex]) / ranges[lowestIndex]; // cast to double for precision
        return lowestIndex;
    }

    public static void main(String[] args) {
        final RollingIntSequence s = RollingIntSequence.createInterleaved(5, 5, 190);
        final int n = args.length == 0 ? 50 : Integer.parseInt(args[0]);
        for (int i = 0; i < n; i++) {
            System.out.print(s.next() + "  ");
        }
    }

}
