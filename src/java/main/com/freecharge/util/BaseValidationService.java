package com.freecharge.util;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.common.CcsClientWrapper;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.rest.validators.ServiceNumberFormatValidator;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;

@Service
public class BaseValidationService {
	Logger logger = LoggingFactory.getLogger(BaseValidationService.class);
	
	private final String ERROR_MESSAGE = "errorMessage";
	private final String DEFAULT_ERROR_MSG = "Something went wrong. Please try after some time.";

	private final String CALL_STATUS = "callStatus";
	private final String FAILURE = "failure";
	private final String ERROR_CODE = "errorCode";

	@Autowired
	@Qualifier("ccsValidatiorClient")
	private CcsClientWrapper ccsValidatiorClient;

	@Autowired
	@Qualifier("ccsErrorMsgClient")
	private CcsClientWrapper ccsErrorMsgClient;

	public Map<String, String> processError(String errorCode, String errorMsg) {
		if (FCUtil.isEmpty(errorMsg)) {
			errorMsg = DEFAULT_ERROR_MSG;
		}

		return ImmutableMap.of(CALL_STATUS, FAILURE, ERROR_CODE, errorCode, ERROR_MESSAGE, errorMsg);
	}

	public Map<String, String> processError(ErrorCode errorCodeObj) {
		String errorMessage = ccsErrorMsgClient
				.getValue(FCConstants.CCS_ERROR_MSG_KEY + "." + errorCodeObj.getErrorNumberString());
		return processError(errorCodeObj.getErrorNumberString(), errorMessage);
	}

	public Map<String, String> processError(String errorCode) {
		String errorMessage = ccsErrorMsgClient.getValue(FCConstants.CCS_ERROR_MSG_KEY + "." + errorCode);
		return processError(errorCode, errorMessage);
	}
	
	public Map<String, String> validateRechargeProduct(ProductName productName, RechargeRequest rechargeRequest) {
		JsonObject mobileValidator = ccsValidatiorClient.getJsonValue(FCConstants.CCS_INPUT_VALIDATION_KEY + "." + productName.getProductType());
		ValidationParamBean validationParamBean = getValidationParam(rechargeRequest);
    	Map<String, String> validation = validateFromValidator(mobileValidator, validationParamBean);
    	return validation;
	}
	
	public Map<String, String> validatePostpaidProduct(String mobileNumber, String operatorID, String amount) {
		JsonObject ppValidator = ccsValidatiorClient.getJsonValue(FCConstants.CCS_INPUT_VALIDATION_KEY + "." + ProductName.MobilePostpaid.getProductType());
		ValidationParamBean validationParamBean = getValidationParam(mobileNumber, operatorID, amount);
    	Map<String, String> validation = validateFromValidator(ppValidator, validationParamBean);
    	return validation;
	}
	
	private ValidationParamBean getValidationParam(String mobileNumber, String operatorID, String amount) {
		ValidationParamBean validationParamBean = new ValidationParamBean();
		validationParamBean.setAmount(Float.parseFloat(amount));
		validationParamBean.setOperatorId(operatorID.trim());
		validationParamBean.setServiceNumber(mobileNumber);
		return validationParamBean;
	}

	private ValidationParamBean getValidationParam(RechargeRequest rechargeRequest) {
		ValidationParamBean validationParamBean = new ValidationParamBean();
		validationParamBean.setAmount(rechargeRequest.getAmount());
		validationParamBean.setOperatorId(rechargeRequest.getOperator().trim());
		validationParamBean.setServiceNumber(rechargeRequest.getServiceNumber());
		return validationParamBean;
	}

	private Map<String, String> validateFromValidator(JsonObject validator, ValidationParamBean validationParamBean) {
		try {
			if (validator == null) {
				return null;
			}
			String operatorId = validationParamBean.getOperatorId();
			JsonObject amountValidator = getSubValidator(validator, operatorId, "amt");
			if (amountValidator == null) {
				return null;
			}
			Float min = Float.parseFloat(amountValidator.get("min").getAsString());
			Float max = Float.parseFloat(amountValidator.get("max").getAsString());
			if (!AmountFormatValidator.isValid(validationParamBean.getAmount(), min, max)) {
				String errorCode = amountValidator.get("errorCode").getAsString();
				String errorMsg = amountValidator.get("errorMsg").getAsString();
				return processError(errorCode, errorMsg);
			}
			JsonObject numberValidator = getSubValidator(validator, operatorId, "num");
			if (numberValidator == null) {
				return null;
			}
			String numberRegex = numberValidator.get("regex").getAsString();
			if (!ServiceNumberFormatValidator.isValid(validationParamBean.getServiceNumber(), numberRegex)) {
				String errorCode = numberValidator.get("errorCode").getAsString();
				String errorMsg = numberValidator.get("errorMsg").getAsString();
				return processError(errorCode, errorMsg);
			}
		} catch (Exception e) {
			logger.error("[CRITICAL] Validation failed", e);
		}
		return null;
	}
	
	private JsonObject getSubValidator(JsonObject rootValidatorJson, String operatorId, String validatorName) {
		JsonObject validator = rootValidatorJson.getAsJsonObject(operatorId + "." + validatorName);
 		
		if (validator == null) {
			validator = rootValidatorJson.getAsJsonObject("*." + validatorName);
		}
		return validator;
	}
}
