package com.freecharge.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtil {
	public static Cookie getCookie(HttpServletRequest request, String cookieName) {
		if (request == null){
			return null;
		}
		request.getRequestURI();
		Cookie[] cookies = request.getCookies();
		if (cookies == null){
			return null;
		}
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookie.getName().equals(cookieName)) {
				return cookie;
			}
		}
		return null;
	}
	
	public static String getCookieValue(HttpServletRequest request, String cookieName) {
		Cookie c = getCookie(request, cookieName);
		String value = null;
		if (c != null){
			value = c.getValue();
		}
		return value;
	}
	
	public static Cookie getOrCreateEmptyCookie(HttpServletRequest request,
			HttpServletResponse response, String cookieName, int maxAge){
		Cookie c = getCookie(request, cookieName);
		request.getRequestURL();
		if(c == null){
			c = new Cookie(cookieName, "");
			c.setMaxAge(maxAge);
			c.setPath("/");
			response.addCookie(c);
		}
		return c;
	}
	
	public static void expireCookie(String cookieName, HttpServletRequest request, HttpServletResponse response){
		if (request == null){
			return;
		}
		Cookie cookie = getCookie(request, cookieName);
		if (cookie != null && response != null){
			cookie.setMaxAge(0);
			cookie.setPath("/");
			cookie.setValue("");
			response.addCookie(cookie);			
		}
	}
	
	public static void addAllCookies(HttpServletRequest request, HttpServletResponse response, int expiry, Cookie... cookies){
		if (cookies != null && response != null){
			for (Cookie c : cookies){
				/*Cookie existingCookie = getCookie(request, c.getName());
				if (existingCookie !=  null){
					expireCookie(existingCookie.getName(), request, response);
				}*/
				if (c != null){
					c.setMaxAge(expiry);
					c.setPath("/");
					response.addCookie(c);
				}
			}
				
		}
	}

    public static void setCookie(HttpServletResponse response, String name, String value, Integer expiry, boolean secure){
        Cookie c = new Cookie(name, value);
        c.setHttpOnly(true);
        c.setSecure(secure);
        c.setMaxAge(expiry);
        c.setPath("/");
        response.addCookie(c);
    }

    public static void setCookie(HttpServletResponse response, String name, String value, Integer expiry,
                                 boolean secure, String path, boolean httpOnly) {
        Cookie c = new Cookie(name, value);
        c.setMaxAge(expiry);
        c.setSecure(secure);
        c.setHttpOnly(httpOnly);
        c.setPath(path);
        response.addCookie(c);
    }
}
