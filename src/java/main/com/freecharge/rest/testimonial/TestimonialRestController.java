package com.freecharge.rest.testimonial;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.yaml.snakeyaml.Yaml;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCHttpUtil;
import com.freecharge.rest.model.ResponseStatus;

/**
 * The objective of TestimonialRestController class is to read the yaml file
 * which contains testimonials and return the JSON response. This JSON response
 * has been processed in javascript to generate the testimonials dynamically.
 * 
 * @author rajani
 * 
 */
@Controller
@RequestMapping("/rest/*")
public class TestimonialRestController {

    @Autowired
    private FCHttpUtil   fcHttpUtil;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "testimonials", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody
    ResponseStatus loadTestimonials(HttpServletResponse response) {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            /*
             * snake yaml is not thread safe so, creating its instance instead
             * of autowire
             */
            Yaml yaml = new Yaml();
            @SuppressWarnings("unchecked")
            List<Map<String, String>> testimonialData = yaml.loadAs(TestimonialRestController.class.getClassLoader()
                    .getResourceAsStream("testimonials/testimonials.yaml"), List.class);
            if (testimonialData != null) {
                responseStatus.setStatus("SUCCESS");
                responseStatus.setResult(testimonialData);
                fcHttpUtil.infiniteCache(response);
            } else {
                responseStatus.setStatus("FAIL");
            }
        } catch (RuntimeException re) {
            responseStatus.setStatus("FAIL");
            logger.error("Unable to read testimonial.yaml file:" + re);
        }
        
        return responseStatus;
    }
}
