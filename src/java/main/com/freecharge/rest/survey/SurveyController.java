package com.freecharge.rest.survey;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.caucho.hessian.client.HessianProxyFactory;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.LocationService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.gaptun.service.common.IGaptunRemoteService;
import com.freecharge.gaptun.service.common.Question;
import com.freecharge.web.util.WebConstants;

/**
 * Survey controller which provides web end points for reading and answering surveys.
 * Internally delegates calls to gaptun-service.
 * @author shirish
 *
 */

@Controller
@RequestMapping("/survey")
public class SurveyController {
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
	private FCProperties properties;
	
    @Autowired
    private LocationService locationService;

    private IGaptunRemoteService gaptunService;
    
    @Autowired
    private InService inService;
    
	@NoLogin
	@Csrf(exclude = true)
    @RequestMapping(value = "/answer", method = RequestMethod.POST)
    public @ResponseBody
    String answerSurveyForUser(@RequestParam String answerText , @RequestParam(defaultValue = "") String orderId,
                               HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model)
    		throws InvalidPathValueException {
		String email = FCSessionUtil.getLoggedInEmailId();
		if (email == null || email.trim().isEmpty()){
			return "User Not Logged In";
		}
		if(!validationFraudCheck()) {
			logger.info("User Not Able to Give Answer for EmailId: "+email);
			return "User Not Able to Give Answer";
		}
		String responseStr = "Unable to record answer";
		try{
			responseStr = getGaptunRemoteService().answerSurvey(email, answerText, orderId);
		} catch (Exception e){
			logger.error("Error answering surver: ", e);
		}
		return responseStr;
	}

	/*
	 * Checking user city condition
	 */
	public boolean checkSurveyCondition(String orderId) {
	    String cityNameListStr = "";
	    String cityName = null;
	    cityNameListStr = this.appConfig.getCityName();
		try {
	        InRequest inRequestObj =  inService.getInRequest(orderId);
	        if(inRequestObj != null) {
	            cityName = inRequestObj.getCircle();
	            try {
	                if (cityNameListStr!=null) {
	                    String [] cityIdList = cityNameListStr.split(",");
	                    if (cityIdList!=null && cityIdList.length>0){
	                        for (String cityNameval : cityIdList){
	                            if (cityNameval.equalsIgnoreCase(cityName)){
	                                logger.info("User city matched for orderId : "+orderId);
	                                return true;
	                            }
	                        }
	                    }
	                } 
	            }catch (Exception e){
	                logger.error("error while checking city condition", e);
	            }
	        }	
		} catch (Exception e) {
			logger.error("error while checking city condition", e);
		}
		return false;
	}
	
	@NoLogin
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody
    List<Question> getSurveyForUser(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) 
    		throws InvalidPathValueException {
		List<Question> questions = null;
		if (!appConfig.isSurveyEnabled()){
			return null;
		}
		String orderId = httpRequest.getParameter("orderId");
		if (orderId!=null && !checkSurveyCondition(orderId)) {
			return null;
		}
		IGaptunRemoteService service = getGaptunRemoteService();
		if (service == null){
			logger.error("Error connecting Gaptun, service=" + service);
			return new ArrayList<>();
		}
		String email = FCSessionUtil.getLoggedInEmailId();
		if (email == null || email.trim().isEmpty()){
			return new ArrayList<>();
		}
		if(!validationFraudCheck()) {
			logger.info("Not Able To View Survey Question for EmailId: "+email);
			return new ArrayList<>();
		}
		try{
		} catch (Exception e){
			logger.error(e);
		}
		questions = service.getSurvey(email);
		return questions;
	}
	
	private IGaptunRemoteService getGaptunRemoteService(){
		String url = properties.getProperty("gaptunServiceUrl");
		HessianProxyFactory factory = new HessianProxyFactory();
		if (gaptunService == null){
			try {
				gaptunService = (IGaptunRemoteService) factory.create(IGaptunRemoteService.class, url);
			} catch (MalformedURLException e) {
				logger.error("Error connecting to Gaptun." , e);
			}
		}
		return gaptunService;
	}
	
	private boolean validationFraudCheck() {
		if(FCSessionUtil.getCurrentSession()!=null && FCSessionUtil.getCurrentSession().getSessionData()!=null){
			Float orderAmount = (Float) FCSessionUtil.getCurrentSession().getSessionData().get(WebConstants.LAST_ORDER_AMOUNT);
			if(orderAmount!=null && orderAmount >= 50) {
				return true;
			}
		}
		return false;
	}
}
