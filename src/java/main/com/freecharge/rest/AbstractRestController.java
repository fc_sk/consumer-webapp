package com.freecharge.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.freecharge.api.error.Error;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidParametersException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.service.PaymentService;
import com.freecharge.common.framework.exception.DataNotFoundInLocalCacheException;
import com.freecharge.common.framework.exception.IdentityAuthorizedException;
import com.freecharge.common.framework.exception.MerchandisingException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.exception.InvalidOrderIDException;

public class AbstractRestController {
    Logger logger = LoggingFactory.getLogger(getClass());
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Error handleException(InvalidParameterException ex) {
		String parameterName = ex.getParameterName();
		String received = ex.getReceivedValue();
		StringBuilder messageBuffer = new StringBuilder("").append("Validation error value- ").append(received).append(" is invalid for ").append(parameterName);
		Error error = new Error(HttpStatus.BAD_REQUEST.toString(), ErrorCode.VALIDATION_ERROR, ex.getErrorMessage(), messageBuffer.toString(), RestConstants.REST_ERROR_SUPPORT_MESSAGE);
		return error;
	}
	
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Error handleException(InvalidPathValueException ex) {
		String requiredType = ex.getExpectedType();
		String path = ex.getPath();
		String received = ex.getReceivedValue();
		StringBuilder messageBuffer = new StringBuilder("").append("Expected type '").append(requiredType).append("' for ").append(path).append(" but received ").append(received);
		Error error = new Error(HttpStatus.BAD_REQUEST.toString(), ErrorCode.TYPE_MISMATCH, messageBuffer.toString(), messageBuffer.toString(), RestConstants.REST_ERROR_SUPPORT_MESSAGE);
		return error;
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Error handleException(InvalidOrderIDException ex) {
		String message = ex.getMessage();
		Error error = new Error(HttpStatus.BAD_REQUEST.toString(), ErrorCode.INVALID_ORDER_ID, message, message, RestConstants.REST_ERROR_SUPPORT_MESSAGE);
		return error;
	}
	
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody Error handleException(TypeMismatchException ex) {
		String requiredType = ex.getRequiredType().getName();
		String propertyName = ex.getPropertyName();
		StringBuilder messageBuffer = new StringBuilder("").append("Expected type '").append(requiredType).append("' for ").append(propertyName);
		StringBuilder devMessageBuffer = new StringBuilder("").append("Parameter type should be '").append(requiredType).append("' for parameter name ").append(propertyName);
		Error error = new Error(HttpStatus.BAD_REQUEST.toString(), ErrorCode.TYPE_MISMATCH, messageBuffer.toString(), devMessageBuffer.toString(), RestConstants.REST_ERROR_SUPPORT_MESSAGE);
		return error;
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public @ResponseBody Error handleException(Exception ex) {
		if (!(ex instanceof MerchandisingException)) {
			logger.error("Got un handled exception", ex);
		}
		StringBuilder messageBuffer = new StringBuilder("Requested resource not found on server.");
		StringBuilder devMessageBuffer = new StringBuilder("Requested resource not found on server. Server failed to complete the request.");
		Error error = new Error(HttpStatus.NOT_FOUND.toString(), ErrorCode.REQUESTED_RESOURCE_NOT_FOUND, messageBuffer.toString(), devMessageBuffer.toString(), RestConstants.REST_ERROR_SUPPORT_MESSAGE);
		return error;
	}
	
	/**
	 * Handling multiple exceptions(which are grouped into single exception).
	 * @param ex
	 */
    @ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody List<Error> handleException(InvalidParametersException ex) {
    	List<InvalidParameterException> invalidRestParameterExceptionList = ex.getInvalidRestParameterExceptionList();
		List<Error> errorList = new ArrayList<Error>();
			
		for (InvalidParameterException invalidRestParameterException : invalidRestParameterExceptionList) {
			StringBuilder messageBuffer = new StringBuilder("").append("Validation error value- ").append(invalidRestParameterException.getReceivedValue()).append(" is invalid for ").append(invalidRestParameterException.getParameterName()).append(" error msg :").append(invalidRestParameterException.getErrorMessage());
			Error error = new Error(HttpStatus.BAD_REQUEST.toString(), ErrorCode.VALIDATION_ERROR, invalidRestParameterException.getErrorMessage(), messageBuffer.toString(), RestConstants.REST_ERROR_SUPPORT_MESSAGE);
			errorList.add(error);
		}
		return errorList;
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody HashMap<String, Object> handleException(DataNotFoundInLocalCacheException ex) {
		logger.error("Data not found in local cache");
		HashMap<String, Object> errorMsg = new HashMap<>();
		errorMsg.put(PaymentService.ERROR_CODE, PaymentConstants.ERROR_DATA_NOT_FOUND_IN_LOCAL_CACHE);
		errorMsg.put(PaymentService.ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_DATA_NOT_FOUND_IN_LOCAL_CACHE));
		return errorMsg;
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public @ResponseBody HashMap<String, Object> handleException(IdentityAuthorizedException ex) {
		logger.error("UNAUTHORIZED Session");
		HashMap<String, Object> errorMsg = new HashMap<>();
		errorMsg.put(FCConstants.ERROR_CODE, FCConstants.UNAUTHORIZED_CODE);
		errorMsg.put(FCConstants.ERROR_MSG,FCConstants.UNAUTHORIZED_MSG);
		return errorMsg;
	}
}
