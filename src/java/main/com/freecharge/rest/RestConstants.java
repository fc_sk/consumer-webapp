package com.freecharge.rest;

public class RestConstants {
	public static final String REST_SUPPORT_EMAIL = "support@freecharge.com";
	public static final String REST_ERROR_SUPPORT_MESSAGE = "For clarifications please contact- " + REST_SUPPORT_EMAIL;
	
	public static final String TYPE_INTEGER = "INTEGER";
	public static final String TYPE_EMAIL = "EMAIL";
}
