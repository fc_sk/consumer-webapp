package com.freecharge.rest.admin;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.ISessionService;
import com.freecharge.rest.AbstractRestController;

/**
 * This is to expose protected resources to clients from within FreeCharge
 * firewall.
 * @author arun
 *
 */
@Controller
@RequestMapping("/protected/*")
public class ProtectedResourceController extends AbstractRestController {

    @Autowired
    private ISessionService sessionService;


    @RequestMapping(value = "rest/csrftoken/{sessionId}", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody Map<String, String> renderPrivateHomePage(@PathVariable String sessionId) {
        Map<String, String> responseMap = new HashMap<>();
    
        FreechargeSession freechargeSession = sessionService.retrieveSession(sessionId);
        
        if (freechargeSession == null || StringUtils.isBlank(freechargeSession.getCSRFToken())) {
            return ErrorCode.INVALID_SESSION_ID.restResponse();
        }
        
        responseMap.put("csrfToken", freechargeSession.getCSRFToken());
        
        return responseMap;
    }

}
