package com.freecharge.rest.operatoralert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.admin.controller.OperatorAlertAdminController;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.ResponseStatus;

@Controller
@RequestMapping("/*")
public class OperatorAlertRestController extends AbstractRestController {
    private Logger               logger      = LoggingFactory.getLogger(getClass());
    private static final boolean RESTRICT_USER_ON_HOME_PAGE = false;
    private static final int CREATE_ALERT_EMAIL = 1;
    private static final int DELETE_ALERT_EMAIL = 0;
    
    @Autowired
    private OperatorAlertService operatorAlertService;

    @Autowired
    private FCProperties          fcProperties;

    @NoLogin
    @RequestMapping(value = "/protected/rest/operatoralert/create", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus operatorAlert(@RequestParam final Map<String, String> mapping) {
        ResponseStatus responseStatus = new ResponseStatus();
        String operatorIdParamValue = mapping.get(OperatorAlertAdminController.OPERATOR_ID);
        String startDateTimeParamValue = mapping.get(OperatorAlertAdminController.START_DATE_TIME).trim();
        String endDateTimeParamValue = mapping.get(OperatorAlertAdminController.END_DATE_TIME).trim();
        String alertMessageParamValue = mapping.get(OperatorAlertAdminController.ALERT_MESSAGE).trim();
        String circleIdParamValue = mapping.get(OperatorAlertAdminController.CIRCLE_ID);
        Integer operatorId = 0;
        Integer circleId = 0;
        String emailContent = null;
     
        try {
               operatorId = Integer.parseInt(operatorIdParamValue);
               if (!circleIdParamValue.isEmpty()) {
                      circleId = Integer.parseInt(circleIdParamValue);
                      operatorAlertService.saveOperatorAlertId(operatorId, circleId, startDateTimeParamValue,
                                    endDateTimeParamValue, alertMessageParamValue, RESTRICT_USER_ON_HOME_PAGE);
               } else {
                      operatorAlertService.saveOperatorAlertId(operatorId, startDateTimeParamValue,
                                    endDateTimeParamValue, alertMessageParamValue, RESTRICT_USER_ON_HOME_PAGE);
               }
            emailContent = String.format("<br>\n%s<br>\n DownTime:<br>\nFrom: %s<br>\nTo: %s", alertMessageParamValue,
                    startDateTimeParamValue, endDateTimeParamValue);
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("Saved operatorAlert for operatorId=" + operatorIdParamValue);
            // Send email only for Prod mode
            if (fcProperties.isProdMode()) {
                operatorAlertService.sendEmail(CREATE_ALERT_EMAIL, operatorId, emailContent, null);
            }
        } catch (RuntimeException e) {
            logger.error("Error while Creating Operator Alert for operatorIdParamValue=" + operatorIdParamValue, e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Error while Creating Operator Alert for operatorIdParamValue = "
                    + operatorIdParamValue);
        }
        return responseStatus;
    }

    @NoLogin
    @RequestMapping(value = "/protected/rest/operatoralert/get/{operatorID}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus getAllOperatorAlert(final @PathVariable int operatorID) {
        ResponseStatus responseStatus = new ResponseStatus();
        List<OperatorAlert> operatorAlert = null;
        try {
            operatorAlert = operatorAlertService.getValidOperatorAlert(operatorID);
        } catch (RuntimeException e) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Failed to get the operator alert for operator ID : " + operatorID);
            logger.error("Failed to get the operator alert for operator ID : " + operatorID, e);
        }

        if (operatorAlert == null) {
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("No Downtime");
        } else {
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult(operatorAlert);
        }
        return responseStatus;
    }

    @NoLogin
    @RequestMapping(value = "/protected/rest/operatoralert/delete", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus deleteOperatorAlert(@RequestParam final Map<String, String> mapping) throws IOException {
        ResponseStatus responseStatus = new ResponseStatus();
        String emailContent = null;
        Date date = new Date();
        long score = date.getTime();
        Integer operatorID = Integer.parseInt(mapping.get(OperatorAlertAdminController.OPERATOR_ID));
        Integer circleID = 0; 
        List<OperatorAlert> operatorAlerts= new ArrayList<OperatorAlert>();
        circleID = Integer.parseInt(mapping.get(OperatorAlertAdminController.CIRCLE_ID));
        
        try {
            if (circleID!=0 || circleID!=null){
                   operatorAlerts = operatorAlertService.getAllOldOperatorAlerts(operatorID, circleID, score);
            } else {
                   operatorAlerts = operatorAlertService.getAllOldOperatorAlerts(operatorID, score);
            }
            for (OperatorAlert operatorAlert: operatorAlerts) {
                emailContent += String.format(
                        "<br>\nDown Time was From: %s <br>\nTo: %s <br>\nAlert Message was: %s<br>\n<br>\n",
                        operatorAlert.getStartDateTime(), operatorAlert.getEndDateTime(),
                        operatorAlert.getAlertMessage());
            }
            this.deleteAllOldOperatorAlert(operatorID, score);
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("Operator Alert Deleted Successfuly");
        } catch (RuntimeException e) {
            logger.error("Error while deleting Operator Alert for operatorIdParamValue=" + operatorID, e);
            emailContent = FCUtil.getStackTrace(e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Error while Creating Operator Alert for operatorIdParamValue = "
                    + operatorID);
        }
        if (fcProperties.isProdMode()) {
            operatorAlertService.sendEmail(DELETE_ALERT_EMAIL, (int)operatorID, emailContent, null);
        }
        return responseStatus;
    }
    
    private void deleteAllOldOperatorAlert(Integer operatorID, long score) {
           operatorAlertService.deleteAllOldOperatorAlert(operatorID, score);
    }
}
