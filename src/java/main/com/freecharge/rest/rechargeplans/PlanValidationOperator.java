package com.freecharge.rest.rechargeplans;

public class PlanValidationOperator {

	private Integer operatorId;
	private Integer productMasterId;
	private Boolean isPlanSelectionEnabled;

	public PlanValidationOperator(Integer operatorId, Integer productMasterId, Boolean isPlanSelectionEnabled) {
		this.operatorId = operatorId;
		this.productMasterId = productMasterId;
		this.isPlanSelectionEnabled = isPlanSelectionEnabled;
	}

	public Integer getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}

	public Integer getProductMasterId() {
		return productMasterId;
	}

	public void setProductMasterId(Integer productMasterId) {
		this.productMasterId = productMasterId;
	}

	public Boolean getIsPlanSelectionEnabled() {
		return isPlanSelectionEnabled;
	}

	public void setIsPlanSelectionEnabled(Boolean isPlanSelectionEnabled) {
		this.isPlanSelectionEnabled = isPlanSelectionEnabled;
	}

}
