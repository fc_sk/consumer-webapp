package com.freecharge.rest.rechargeplans.plansdenomination;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.freecharge.common.cache.CacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * @author fcaa17922
 * This cache holds the values for invalid plan denominations for the purpose of validation.
 *
 */
@Repository("plansDenominationCacheManager")
public class PlansDenominationCacheManager extends CacheManager {
    private static final String PLANS_DENOMINATION_CACHE_PREFIX = "plansDenominationCache";
    private static final Integer DEFAULT_EXPIRY_IN_DAYS = 7;
    
    private Logger   logger = LoggingFactory.getLogger(getClass());
    
    
    public void setPlansDenominationCache(String operatorId, String circleId,  String amount, String productId, String isInvalid, Date expiryDate) {
    	Calendar cal = Calendar.getInstance();
    	Date expireAt;
        String cacheKey = getCacheKey(operatorId, circleId, amount, productId);
        logger.info("The cache key is :["+cacheKey+"] Is invalid value is :["+isInvalid+"]");
        set(cacheKey, isInvalid);
        if(expiryDate == null){
        	cal.add(Calendar.DATE, DEFAULT_EXPIRY_IN_DAYS);
        	expireAt = cal.getTime();
        }
        else {
        	expireAt = expiryDate;
        }
        expireAt(cacheKey, expireAt);
        logger.info("Cache successfully updated with key ["+cacheKey+"] and isInvalid value as:["+isInvalid+"] and expiry date as ["+expireAt+"]");
    }
    
    
	public String getCacheKey(String operatorId, String circleId, String amount, String productId){
		return PLANS_DENOMINATION_CACHE_PREFIX + "_" + operatorId + "_" + circleId + "_" +  amount + "_" + productId;
	}

	
	public String getPlansDenominationCache(String operatorId, String circleId, String amount, String productId){
		String isInvalid = "";
		Object obj = get(getCacheKey(operatorId, circleId, amount, productId));  
		if(obj != null){			
			isInvalid = (String)obj;
		}
		return isInvalid;
	}
	
	 public void clearPlansDenominationCache(String operatorId, String circleId, String amount, String productId) {
	        String cacheKey = getCacheKey(operatorId, circleId, amount, productId);
	        delete(cacheKey);
	    }
	
	
}

