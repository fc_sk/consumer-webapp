package com.freecharge.rest.rechargeplans.plansdenomination;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.cache.RechargeResourceCache;
import com.freecharge.recharge.collector.InvalidDenominationCache;
import com.freecharge.recharge.collector.RechargeMetricService;

/**
 * @author fcaa17922
 * This class is used to interact with invalid denomination Plans as well as Operator circle block.
 *
 */
@Service
public class PlansDenominationService {

	private Logger   logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	PlansDenominationCacheManager plansDenominationCacheManager;
	
	@Autowired
	RechargeResourceCache rechargeResourceCache;
	
	@Autowired
	InvalidDenominationCache invalidDenoCache;
	
	@Autowired
	RechargeMetricService rechargeMetricService;
	
	@Autowired
	PlansDenominationDAO plansDenominationDAO;
	
	@Autowired
	OperatorCircleReadDAO operatorCircleReadDAO;
	

	public String getInvalidDenominationCache(String operatorId, String circleId, String amount, String productId) {
		return plansDenominationCacheManager.getPlansDenominationCache(operatorId, circleId, amount, productId);
	}

	public void setPlanDenominationCache(PlansDenomination plansDenomination) {
		plansDenominationCacheManager.setPlansDenominationCache(plansDenomination.getOperatorId(), plansDenomination.getCircleId(), 
				plansDenomination.getAmount(), plansDenomination.getProductId(), plansDenomination.getIsInvalid(), plansDenomination.getExpiryDate());
	}

	public void clearPlansDenominationCache(String operatorId, String circleId, String amount, String productId) {
		if (FCUtil.isEmpty(operatorId) || FCUtil.isEmpty(circleId) || FCUtil.isEmpty(amount)
				|| FCUtil.isEmpty(productId)) {
			return;
		}
		plansDenominationCacheManager.clearPlansDenominationCache(operatorId, circleId, amount, productId);
	}
	
	public PlansDenomination getPlansDenominationDO(String operatorId, String circleId, String amount, String productId, String isInvalid, Date expiryDate, String mobileNumber, String planType){
		PlansDenomination plansDenomination = new PlansDenomination();
		plansDenomination.setOperatorId(operatorId);
		plansDenomination.setCircleId(circleId);
		plansDenomination.setAmount(amount);
		plansDenomination.setProductId(productId);
		plansDenomination.setIsInvalid(isInvalid);
		plansDenomination.setExpiryDate(expiryDate);
		plansDenomination.setMobileNumber(mobileNumber);
		plansDenomination.setPlanType(planType);
		return plansDenomination;
	}
	
	public int insertInvalidPlanAttempts(PlansDenomination plansDenomination, String userId){
		return plansDenominationDAO.insertInvalidPlanAttempts(plansDenomination, userId);
	}
	
	public boolean isOperatorCircleBlocked(Integer operatorId, Integer circleId) {
		boolean isBlocked = false;
		logger.info("Operator circle block data being fetched for Operator Id : [" + operatorId + "] and Circle Id : ["
				+ circleId + "]");
		try {
			OperatorCircleBlock opBlock = operatorCircleReadDAO.getOperatorCircleBlock(operatorId, circleId);
			if (opBlock != null) {
				isBlocked = opBlock.getIsBlocked();
			}
			if (!isBlocked && circleId >= 0) {
				// check if it is blocked for all circles
				logger.info("No blocking found, going for ALL circle block check");
				OperatorCircleBlock allBlock = operatorCircleReadDAO.getOperatorCircleBlock(operatorId, -1);
				if (allBlock != null) {
					isBlocked = allBlock.getIsBlocked();
				}

			}
		} catch (Exception e) {
			logger.info("An exception occurred trying to fetch the operator circle block data", e);
		}
		return isBlocked;
	}
	
	public boolean getExistingInvalidDenoCache(String operatorId, String circleId, String amount, String productId, String planType){
		boolean isInvalid = false;
		try {
			if(invalidDenoCache.getValidatedInvalidDenomination(operatorId, circleId, Integer.valueOf(amount), planType)){
				return true;
			}
			if (rechargeResourceCache.isInvalidDenomination(operatorId, operatorId, Double.valueOf(amount), planType,
					productId)) {
				return true;
			}
			if ("2".equalsIgnoreCase(operatorId)) {
				return rechargeResourceCache.isAirtelInvalidDenomination(operatorId, circleId, Double.valueOf(amount));
			}
			if ("5".equalsIgnoreCase(operatorId)) {
				String complimentaryPlanType = getComplimentaryPlanTypeTataDocomo(planType);
				if(invalidDenoCache.getTDPlanValidity(circleId, Integer.valueOf(amount), planType) && invalidDenoCache.getTDPlanValidity(circleId, Integer.valueOf(amount), complimentaryPlanType)){
					return true;	
				}
				if(invalidDenoCache.getTDPlanValidity(circleId, Integer.valueOf(amount), planType)){
					return true;
				}
			}
			if("3".equalsIgnoreCase(operatorId)){
				return rechargeResourceCache.isBsnlInvalidDenomination(operatorId, circleId, Double.valueOf(amount), planType);
			}
		}
		catch(Exception e){
			logger.error("An error occurred trying to fetch the invalid denomination details", e);
			
		}
		return isInvalid;
	}
	
	 private String getComplimentaryPlanTypeTataDocomo(String rechargePlanType) {
	        if (!FCUtil.isEmpty(rechargePlanType)) {
	            if (rechargePlanType.equals("topup")) {
	                return "special";
	            } else {
	                return "topup";
	            }
	        }
	        return "special";
	    }

}
