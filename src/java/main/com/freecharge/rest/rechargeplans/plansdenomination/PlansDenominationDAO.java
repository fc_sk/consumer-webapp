package com.freecharge.rest.rechargeplans.plansdenomination;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;

@Component("plansDenominationDAO")
@Transactional
public class PlansDenominationDAO {
	private static Logger logger = LoggingFactory.getLogger(PlansDenominationDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public int insertInvalidPlanAttempts(PlansDenomination plansDenomination, String userId) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		logger.info("Request comes into plans denomination dao for inserting invalid attempts against the user id : ["
				+ userId + "]");
		String insertSql = "insert into invalid_plan_denomination_attempts(product_id, fk_operator_id, fk_circle_id, amount, mobile_number, user_id, "
				+ "created_on, plan_type) values ("
				+ ":product_id, :fk_operator_id, :fk_circle_id, :amount, :mobile_number, :user_id, :created_on, :plan_type);";

		Map<String, Object> insertParams = new HashMap<String, Object>();
		insertParams.put("product_id", Integer.valueOf(plansDenomination.getProductId()));
		insertParams.put("fk_operator_id", Integer.valueOf(plansDenomination.getOperatorId()));
		insertParams.put("fk_circle_id", Integer.valueOf(plansDenomination.getCircleId()));
		insertParams.put("amount", plansDenomination.getAmount());
		insertParams.put("mobile_number", plansDenomination.getMobileNumber());
		insertParams.put("user_id", userId);
		insertParams.put("created_on", dateFormat.format(calendar.getTime()));
		insertParams.put("plan_type", plansDenomination.getPlanType());
		return this.jdbcTemplate.update(insertSql, insertParams);
	}

}
