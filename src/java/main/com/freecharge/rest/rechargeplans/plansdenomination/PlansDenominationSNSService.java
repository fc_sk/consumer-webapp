package com.freecharge.rest.rechargeplans.plansdenomination;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sns.AmazonSNSProp;
import com.freecharge.sns.AmazonSNSService;

@Service("plansDenominationSnsService")
public class PlansDenominationSNSService extends AmazonSNSService {

	private Logger logger = LoggingFactory.getLogger("amazon.sns." + PlansDenominationSNSService.class.getName());

    @Autowired
    public PlansDenominationSNSService(@Qualifier("invalidPlanDenominationSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }
}
