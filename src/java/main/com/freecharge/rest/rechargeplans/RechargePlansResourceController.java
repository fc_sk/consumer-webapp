package com.freecharge.rest.rechargeplans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/recharge/plan/*")
public class RechargePlansResourceController extends AbstractRestController {

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private RechargePlanService   rechargePlanService;

    private final Logger          logger = LoggingFactory.getLogger(getClass());

    @NoSessionWrite // TODO: To be excluded from sessions completely.
    @NoLogin
    @RequestMapping(value = "{product}/{operator}/{circle}/{amount}/check", method = RequestMethod.GET)
    public @ResponseBody  Map<String, Object> viewHome(@PathVariable String operator,@PathVariable String product,@PathVariable String amount,
           @PathVariable String circle) throws InvalidPathValueException {
        int operatorId = operator != null ? Integer.parseInt(operator) : -1;
        int productId = product != null ? Integer.parseInt(product) : -1;
        float totalAmount = amount != null ? Float.parseFloat(amount) : -1;
        int circleId = -1;
        int allCircleId = -1;
        List<RechargePlanBusinessDO> requestedPlan = null;
        Map<String, Object> returnMap = new HashMap<>();
        
        try {
            CircleMaster circleMaster = operatorCircleService.getCircle("ALL");
            allCircleId = circleMaster.getCircleMasterId();

            if (circle != null && "all".equalsIgnoreCase(circle)) {
                circleId = allCircleId;
            } else {
                circleId = circle != null ? Integer.parseInt(circle) : -1;
            }

            requestedPlan = rechargePlanService.getRechargePlan(circleId, operatorId, productId, totalAmount);

            if (requestedPlan.isEmpty()) {
                returnMap.put("isValid", Boolean.FALSE);
            } else {
                returnMap.put("isValid", Boolean.TRUE);
                returnMap.put("requestedPlanDetails", requestedPlan);
            }
            
            List<RechargePlanBusinessDO> allPlans = rechargePlanService.getRechargePlansBizDO(circleId, operatorId, productId);
            returnMap.put("allPlanDetails", allPlans);
            
        } catch (Exception e) {
            logger.error("Error in getting Recharge Plan", e);
            return null;
        }

        return returnMap;
    }

}
