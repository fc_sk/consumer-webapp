package com.freecharge.rest.rechargeplans;

import java.math.BigDecimal;

public class RechargePlanBusinessDO {
    private String name;
    private int productType;
    private String denominationType;
    private int operatorMasterId;
    private int circleMasterId;
    private BigDecimal amount;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private BigDecimal talkTime;
    private String validity;
    private String description;
    private Boolean isFullTalkTime = false;
    private Integer rechargePlanId;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public int getProductType() {
        return productType;
    }
    public void setProductType(int productType) {
        this.productType = productType;
    }
    
    public String getDenominationType() {
        return denominationType;
    }
    public void setDenominationType(String denominationType) {
        this.denominationType = denominationType;
    }
    
    public int getOperatorMasterId() {
        return operatorMasterId;
    }
    public void setOperatorMasterId(int operatorMasterId) {
        this.operatorMasterId = operatorMasterId;
    }
    
    public int getCircleMasterId() {
        return circleMasterId;
    }
    public void setCircleMasterId(int circleMasterId) {
        this.circleMasterId = circleMasterId;
    }
    
    public BigDecimal getMinAmount() {
        return minAmount;
    }
    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }
    
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }
    
    public BigDecimal getTalkTime() {
        return talkTime;
    }
    public void setTalkTime(BigDecimal talkTime) {
        this.talkTime = talkTime;
    }
    
    public String getValidity() {
        return validity;
    }
    public void setValidity(String validity) {
        this.validity = validity;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Boolean getIsFullTalkTime() {
        return isFullTalkTime;
    }
    public void setIsFullTalkTime(Boolean isFullTalkTime) {
        this.isFullTalkTime = isFullTalkTime;
    }
	public Integer getRechargePlanId() {
		return rechargePlanId;
	}
	public void setRechargePlanId(Integer rechargePlanId) {
		this.rechargePlanId = rechargePlanId;
	}
    
    
}
