package com.freecharge.rest.appinstall;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
public class AppInstallTrackingController {
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@NoLogin
    @Csrf(exclude=true)
	@NoSessionWrite
    @RequestMapping(value = "/atrfo", method = RequestMethod.POST)
	public @ResponseBody String appTrackFirstOpen(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model){
		String result = "successs";
		logger.info(httpRequest.getParameterMap());
		return result;
	}
	
	@NoLogin
    @Csrf(exclude=true)
	@NoSessionWrite
    @RequestMapping(value = "/aattr", method = RequestMethod.POST)
	public @ResponseBody String appTrackAttribution(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model){
		String result = "successs";
		logger.info(httpRequest.getParameterMap());
		return result;
	}
}
