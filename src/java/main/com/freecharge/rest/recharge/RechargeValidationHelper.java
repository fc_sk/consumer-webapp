package com.freecharge.rest.recharge;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.util.BaseValidationService;

/**
 * This class acts as a validation helper for all the recharge related requests
 * 
 * @author fcaa17922
 *
 */
@Service
public class RechargeValidationHelper {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private BaseValidationService baseValidationService;

	@Autowired
	private OperatorCircleService operatorCircleService;


	/**
	 * Method used to validate the incoming request from UI
	 * and process the error correspondingly from ccs.
	 * @param rechargeRequest
	 * @param request
	 * @return Map 
	 * 			- validationMap
	 */
	public Map<String, String> validateRequest(RechargeRequest rechargeRequest, HttpServletRequest request) {
		logger.info("Request comes inside validate recharge");
		if (rechargeRequest == null) {
			return baseValidationService.processError(ErrorCode.RECHARGE_REQUEST_NOT_PRESENT);
		}
		final String serviceNumber = rechargeRequest.getServiceNumber();
		final String productType = rechargeRequest.getProductType();
		final String operator = rechargeRequest.getOperator();

		if (StringUtils.isBlank(serviceNumber)) {
			return baseValidationService.processError(ErrorCode.SERVICE_NUMBER_NOT_PRESENT);
		}

		if (!StringUtils.isNumeric(serviceNumber)) {
			return baseValidationService.processError(ErrorCode.INVALID_SERVICE_NUMBER_FORMAT);
		}

		if (!AmountFormatValidator.isValid(rechargeRequest.getAmount())) {
			return baseValidationService.processError(ErrorCode.INVALID_AMOUNT_FORMAT);
		}

		if (StringUtils.isBlank(rechargeRequest.getProductType())) {
			return baseValidationService.processError(ErrorCode.PRODUCT_TYPE_NOT_PRESENT);
		}

		if (StringUtils.isBlank(rechargeRequest.getOperator())) {
			return baseValidationService.processError(ErrorCode.OPERATOR_NOT_PRESENT);
		}

		final ProductName productName = ProductName.fromCode(productType);

		if (!operatorCircleService.isValidOperator(productName.getProductId(), operator)) {
			return baseValidationService.processError(ErrorCode.OPERATOR_NOT_PRESENT);
		}

		switch (productName) {
		case Mobile:
        case DTH:
        case DataCard:
        case PostPaidDataCard:
		case GoogleCredits:
			Map<String, String> validation = baseValidationService.validateRechargeProduct(productName,
					rechargeRequest);
			if (validation != null) {
				return validation;
			}
			break;
		default:
			break;
		}
		return null;
	}


	/**
	 * Used to check if an incoming request is from a new mobile app version > 16
	 * @param request
	 * @return boolean 
	 * 				- the boolean value
	 */
	@SuppressWarnings("unused")
	private boolean isNewMobileApp(HttpServletRequest request) {
		int version = MobileUtil.getMobileAppVersion(request);
		Boolean isNEWMobileAPP = MobileUtil.getMobileChannel(request) == 3 && version > 16;
		return isNEWMobileAPP ? true : false;
	}

}
