package com.freecharge.rest.recharge;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.LoggingConstants;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.recharge.businessDo.EuroValidationResponseDo;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.services.PlanValidatorService;
import com.freecharge.recharge.util.CallSourceType;
import com.freecharge.rest.model.RechargeRequest;
import com.google.common.collect.ImmutableMap;

/**
 * Created by yashveer on 26/9/16.
 */
@Service
public class InTransactionHelper {
    private static final String INTRANSACTION_VALIDATION_OPERATOR = "InTransactionValidationOperator";
    private static Logger logger = LoggingFactory.getLogger(InTransactionHelper.class);    
	
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss"); 

    @Autowired
    private PlanValidatorService planValidatorService;

    @Autowired
    private EuronetService euronetService;

    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private LogRequestService logRequestService;

    private final Map<String, ErrorCode> responseErrorCodeMap = ImmutableMap.<String, ErrorCode>builder()
             .put("AI", ErrorCode.INVALID_DENOMINATION)
            .put("EAI", ErrorCode.INVALID_DENOMINATION)
            .put("CI", ErrorCode.INVALID_MOBILE_NUMBER)
            .put("ECI", ErrorCode.INVALID_MOBILE_NUMBER)
            .put("EI", ErrorCode.UNKNOWN_ERROR)
            .put("ESM", ErrorCode.OPERATOR_DOWN)
            .build();

    public ErrorCode validate(RechargeRequest rechargeRequest, String rechargePlanType,
                                               String serviceNumber) {
        ErrorCode errorCode = ErrorCode.NO_ERROR;
        if (isInTransactionEnabled(rechargeRequest.getOperator())) {
            logger.info("InTransaction Validation enabled for operator - " + rechargeRequest.getOperator());
            RechargePlan rechargePlan = initRechargePlan(rechargeRequest, rechargePlanType);            
            Map<String,Object> inTransactionValidationDataMap = createValidationDataMap(rechargePlan,serviceNumber);
            EuroValidationResponseDo validationResponse = euronetService.validatePlan(rechargePlan, serviceNumber,
                    CallSourceType.RECHARGE_SAVE); 
            inTransactionValidationDataMap.put(LoggingConstants.RESPONSE_TIMESTAMP, sdf.format(new Date()));
            if(validationResponse !=null){
            	inTransactionValidationDataMap.put(LoggingConstants.RAW_REQUEST,validationResponse.getRawRequest());
                inTransactionValidationDataMap.put(LoggingConstants.RAW_RESPONSE,validationResponse.getResponseMessage());
                inTransactionValidationDataMap.put(LoggingConstants.RESPONSE_CODE,validationResponse.getResponseCode());
            } else{
            	return ErrorCode.UNKNOWN_ERROR;
            }
                       
            if (validationResponse != null && !planValidatorService.isNotFailure(validationResponse)) {
                // get correct error code
                if(responseErrorCodeMap.containsKey(validationResponse.getResponseCode())){
                    ErrorCode x = responseErrorCodeMap.get(validationResponse.getResponseCode());
                    if(FCConstants.errorCodeToMessageMap.get(x) != null){
                        x.setErrorMessage(FCConstants.errorCodeToMessageMap.get(x)[1]);
                    }                    
                    logRequestService.logInTransactionValidation(inTransactionValidationDataMap);
                    return x;
                }
                else{
                    // do not block if not present in the list of error codes
                    logger.info("Errorcode not handled. Giving it green flag. Errorcode:" + validationResponse.getResponseCode() + ",ServiceNumber:" + serviceNumber);
                    logRequestService.logInTransactionValidation(inTransactionValidationDataMap);
                    return ErrorCode.NO_ERROR;
                    //return ErrorCode.INVALID_DENOMINATION;
                }
            }
        }
        return errorCode;
    }	

	private Map<String, Object> createValidationDataMap(RechargePlan rechargePlan, String serviceNumber) {
		Map<String, Object> dataMap = logRequestService.initValidationLogDataMap(serviceNumber,
				rechargePlan.getOperatorMasterId(), rechargePlan.getCircleMasterId(), "euronet",
				LoggingConstants.ValidationCallType.IN_TRANSACTION_VALIDATION.getName());

		return dataMap;
	}

	private boolean isInTransactionEnabled(String operatorId) {
        JSONObject configJson = appConfigService.getCustomProp(appConfigService.RECHARGE_CUSTOM_CONFIG);

        if (configJson != null) {
            final String enabledEventListString = (String) configJson.get(INTRANSACTION_VALIDATION_OPERATOR);

            if (!StringUtils.isBlank(enabledEventListString)) {
                List<String> eventList = Arrays.asList(enabledEventListString.split(","));

                if (eventList.contains(operatorId)) {
                    return true;
                }

            }
        }

        return false;
    }

    private RechargePlan initRechargePlan(RechargeRequest rechargeRequest, String rechargePlanType) {
        RechargePlan rechargePlan = new RechargePlan();
        rechargePlan.setOperatorMasterId(Integer.valueOf(rechargeRequest.getOperator()));
        rechargePlan.setCircleMasterId(Integer.valueOf(rechargeRequest.getCircleName()));
        rechargePlan.setAmount(new BigDecimal(String.valueOf(rechargeRequest.getAmount())));
        // retained for datacard validation check
        if(ProductMaster.ProductName.DataCard.getProductType().equals(rechargeRequest.getProductType())){
            rechargePlan.setName("special");
        }
        else{
            rechargePlan.setName(rechargePlanType);
        }
        if(rechargeRequest.getProductType()!=null){
            rechargePlan.setProductType(ProductMaster.ProductName.fromPrimaryProductType(rechargeRequest.getProductType()).getProductId());
        }
        return rechargePlan;
    }
}
