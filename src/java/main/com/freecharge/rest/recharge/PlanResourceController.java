package com.freecharge.rest.recharge;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidParametersException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/rest/*")
public class PlanResourceController  extends AbstractRestController {
    @Autowired
    private OperatorCircleService operatorCircleService;
    
    @Autowired
    private RechargePlanService rechargePlanService;
    
    @Autowired
    protected MetricsClient             metricsClient;
    
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    /**
     * Getting recharge plans based on operator code,circle name,recharge plan,product name
     * @param operatorCode
     * @param circleName
     * @param rechargePlanName
     * @param productName
     * @param model
     * @return
     * @throws InvalidRestParametersException
     */
    @NoLogin
    @NoSessionWrite // TODO: This should be excluded from sessions altogether.
    @RequestMapping(value="/plans/{operatorCode}/{circleName}/{rechargePlanName}/{productName}", method=RequestMethod.GET)
    public String getPopularRechargePlans(@PathVariable String operatorCode, @PathVariable String circleName, @PathVariable String rechargePlanName,  @PathVariable String productName, ModelMap model) throws InvalidParametersException {
        
        validateParameters(operatorCode,circleName,rechargePlanName,productName);
        
        OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorCode);
        int operatorId = operatorMaster.getOperatorMasterId();
        
        CircleMaster circleMaster1 = operatorCircleService.getCircle(circleName);
         
        int productId = ProductMaster.ProductName.valueOf(productName).getProductId();
        
        int circleId = -1;
        int allCircleId = -1;
        List<RechargePlan> rechargePlans = null;
        List<RechargePlan> allRechargePlans = null;
        
        CircleMaster circleMaster = operatorCircleService.getCircle("ALL");
        allCircleId = circleMaster.getCircleMasterId();
        allRechargePlans = rechargePlanService.getRechargePlans(allCircleId, operatorId, productId);
        
        if( circleMaster1.getCircleMasterId() != null  && "all".equalsIgnoreCase(circleMaster1.getName())) {
            circleId = allCircleId;
        } else {
            circleId = circleMaster1.getCircleMasterId() != null ? circleMaster1.getCircleMasterId() : -1;
        }       
        
        if(circleId != allCircleId) {
            rechargePlans = rechargePlanService.getRechargePlans(circleId, operatorId, productId, rechargePlanName);
        }
        if(allRechargePlans == null) 
            allRechargePlans = new ArrayList<RechargePlan>();
        if(rechargePlans != null)
            allRechargePlans.addAll(rechargePlans);
        
        if(allRechargePlans.size() > 0){
            model.addAttribute(WebConstants.ALL_RECHARGE_PLANS, allRechargePlans);
        }
        
        return "jsonView";
    }

    //Validation
    private void validateParameters(String operatorCode, String circleName, String rechargePlanName, String productName) throws InvalidParametersException {
        List<InvalidParameterException> InvalidParameterExceptionList = new ArrayList<InvalidParameterException>();

        if (operatorCode != null) {
            if (operatorCircleService.getOperator(operatorCode) == null) {
                InvalidParameterExceptionList.add(new InvalidParameterException("operatorCode", operatorCode,"Invalid operator code", ValidationErrorCode.INVALID_OPERATOR_CODE));
            }
        }
        
        if (circleName != null) {
            if (operatorCircleService.getCircle(circleName) == null) {
                InvalidParameterExceptionList.add(new InvalidParameterException("circleName", circleName,"Invalid circle name", ValidationErrorCode.INVALID_CIRCLE_NAME));
            }
        }

        if (rechargePlanName != null) {
            List<String> rechargePlanNameList = rechargePlanService.getAllRechargePlanNames();
            if (!(rechargePlanNameList.contains(rechargePlanName))) {
                InvalidParameterExceptionList.add(new InvalidParameterException("rechargePlanName", rechargePlanName,"Invalid recharge plan name", ValidationErrorCode.INVALID_RECHARGE_PLAN));
            }
        }
        
        if (productName != null) {
            if (!(ProductMaster.ProductName.isValidProductName(productName))) {
                InvalidParameterExceptionList.add(new InvalidParameterException("productName", productName,"Valid product name is required", ValidationErrorCode.INVALID_PRODUCT_MASTER_ID));
            }
        }
        
        if (InvalidParameterExceptionList.size() > 0) {
            throw new InvalidParametersException(InvalidParameterExceptionList);
        }
    }
    
    /**
     * Getting all Recharge plan names.
     */
    @NoSessionWrite
    @NoLogin
    @RequestMapping(value="/plans/planNames", method=RequestMethod.GET)
    public @ResponseBody List<String> getPopularRechargePlanNames(ModelMap model) throws InvalidPathValueException {
        List<String> rechargePlanNameList = rechargePlanService.getAllRechargePlanNames();
        return rechargePlanNameList;
    }
    
    //Getting recharge plans based on operatorName,circleName and productName
    @NoLogin
    @NoSessionWrite // TODO: This should be excluded from sessions altogether.
    @RequestMapping(value="/plans/{operatorId}/{circleId}/{productName}", method=RequestMethod.GET)
    public String getRechargePlans(@PathVariable int operatorId, @PathVariable int circleId, @PathVariable String productName, ModelMap model) throws InvalidParametersException {
        validateRechargeParameters(operatorId,circleId,productName);
        CircleMaster circleMaster = operatorCircleService.getCircle(circleId);
        OperatorMaster operatorMaster= operatorCircleService.getOperator(operatorId);
        int productId = -1;
        for (ProductMaster.ProductName productNameObject : ProductMaster.ProductName.values()) {
            if ((productNameObject.name()).equals(productName)) {
                productId = productNameObject.getProductId();
            }
        }
       
        List<RechargePlan>  rechargePlans = rechargePlanService.getRechargePlans(circleId, operatorId, productId);
    
        if (rechargePlans != null && rechargePlans.size() > 0) {
            model.addAttribute(WebConstants.RECHARGE_PLANS, rechargePlans);
            metricsClient.recordEvent("RechargePlansSize", String.format("%s_%s_%s", productName, operatorMaster.getOperatorCode(), circleMaster.getName()), Integer.toString(rechargePlans.size()));
        } else {
            metricsClient.recordCount("RechargePlansZero", String.format("%s_%s_%s", productName, operatorMaster.getOperatorCode(), circleMaster.getName()), 1);
        }
        return "jsonView";
    }
    
  //Getting recharge plans based on operatorName,circleName and productName
    @NoLogin
    @NoSessionWrite // TODO: This should be excluded from sessions altogether.
    @RequestMapping(value="/plans/{operatorId}/{productName}", method=RequestMethod.GET)
    public String getRechargePlansForOperator(@PathVariable int operatorId, @PathVariable String productName, ModelMap model) throws InvalidParametersException {
        List<InvalidParameterException> InvalidParameterExceptionList = new ArrayList<InvalidParameterException>();
        OperatorMaster operatorMaster= operatorCircleService.getOperator(operatorId);
        if(operatorMaster==null)
        {
            InvalidParameterExceptionList.add(new InvalidParameterException("operatorId", ""+operatorId,"Invalid operatorId", ValidationErrorCode.INVALID_OPERATOR_ID));
            throw new InvalidParametersException(InvalidParameterExceptionList);
        }
        int productId = -1;
        for (ProductMaster.ProductName productNameObject : ProductMaster.ProductName.values()) {
            if ((productNameObject.name()).equals(productName)) {
                productId = productNameObject.getProductId();
            }
        }
       
        List<RechargePlan>  rechargePlans = rechargePlanService.getRechargePlansForOperator(operatorId, productId);
    
        if (rechargePlans != null && rechargePlans.size() > 0) {
            model.addAttribute(WebConstants.RECHARGE_PLANS, rechargePlans);
            metricsClient.recordEvent("RechargePlansSize", String.format("%s_%s", productName, operatorMaster.getOperatorCode()), Integer.toString(rechargePlans.size()));
        } else {
            metricsClient.recordCount("RechargePlansZero", String.format("%s_%s", productName, operatorMaster.getOperatorCode()), 1);
        }
        return "jsonView";
    }

    private void validateRechargeParameters(int operatorId, int circleId, String productName) throws InvalidParametersException {
        // TODO Auto-generated method stub
        List<InvalidParameterException> InvalidParameterExceptionList = new ArrayList<InvalidParameterException>();
        
        
        if(operatorCircleService.getCircle(circleId)==null)
        {
            InvalidParameterExceptionList.add(new InvalidParameterException("circleId", ""+circleId,"Invalid circleId", ValidationErrorCode.INVALID_CIRCLE_ID));
        }
        
        if(operatorCircleService.getOperator(operatorId)==null)
        {
            InvalidParameterExceptionList.add(new InvalidParameterException("operatorId", ""+operatorId,"Invalid operatorId", ValidationErrorCode.INVALID_OPERATOR_ID));
        }
        
        if (productName != null) {
            if (!(ProductMaster.ProductName.isValidProductName(productName))) {
                InvalidParameterExceptionList.add(new InvalidParameterException("productName", productName,"Valid product name is required", ValidationErrorCode.INVALID_PRODUCT_MASTER_ID));
            }
        }
        
        if (InvalidParameterExceptionList.size() > 0) {
            throw new InvalidParametersException(InvalidParameterExceptionList);
        }
        
    }
}