package com.freecharge.rest.recharge;

public class RechargeNotificationBean {
    private String usermail;
    private Integer shopno = 21;
    private Integer locationid = 1;
    private String messagetitle;
    private String messageurl = "launch:com.freecharge.android:AddCashBack";
    private String messagedesc;
    private String messageaction = "launch";
    private String lmd = "null";
    private String lma = "null";
    private String rmd = "null";
    private String rma = "null";
    private String eventName = "cashback";
    
    public String getUsermail() {
        return usermail;
    }
    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }
    public Integer getShopno() {
        return shopno;
    }
    public void setShopno(Integer shopno) {
        this.shopno = shopno;
    }
    public Integer getLocationid() {
        return locationid;
    }
    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }
    public String getMessagetitle() {
        return messagetitle;
    }
    public void setMessagetitle(String messagetitle) {
        this.messagetitle = messagetitle;
    }
    public String getMessageurl() {
        return messageurl;
    }
    public void setMessageurl(String messageurl) {
        this.messageurl = messageurl;
    }
    public String getMessagedesc() {
        return messagedesc;
    }
    public void setMessagedesc(String messagedesc) {
        this.messagedesc = messagedesc;
    }
    public String getMessageaction() {
        return messageaction;
    }
    public void setMessageaction(String messageaction) {
        this.messageaction = messageaction;
    }
    public String getLmd() {
        return lmd;
    }
    public void setLmd(String lmd) {
        this.lmd = lmd;
    }
    public String getLma() {
        return lma;
    }
    public void setLma(String lma) {
        this.lma = lma;
    }
    public String getRmd() {
        return rmd;
    }
    public void setRmd(String rmd) {
        this.rmd = rmd;
    }
    public String getRma() {
        return rma;
    }
    public void setRma(String rma) {
        this.rma = rma;
    }
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
