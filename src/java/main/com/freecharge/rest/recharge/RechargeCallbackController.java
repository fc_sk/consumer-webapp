package com.freecharge.rest.recharge;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.jdbc.EuronetCallbackRequest;
import com.freecharge.app.domain.entity.jdbc.EuronetCallbackResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.EuronetCallbackService;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.AbstractRestController;

@Controller
@RequestMapping("/rest/recharge/callback/*")
public class RechargeCallbackController extends AbstractRestController {
    private final Logger             logger                                  = LoggingFactory.getLogger(getClass());
   
  
    @Autowired
    private EuronetCallbackService   euroCallbackService;

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "handleEuronetResponse", method = RequestMethod.GET)
    public @ResponseBody
    EuronetCallbackResponse handleEuronetRechargeStatus(EuronetCallbackRequest euronetCallbackRequest,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
            throws InvalidParameterException {
        return euroCallbackService.handleStatusCheck(euronetCallbackRequest);    	
    }
    
}
