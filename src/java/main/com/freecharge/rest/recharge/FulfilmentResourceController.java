package com.freecharge.rest.recharge;

import com.freecharge.common.framework.annotations.NoLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.Authenticate;

@Controller
@RequestMapping("/rest/fulfilment/*")
public class FulfilmentResourceController extends AbstractRestController {

    @Autowired
    FulfilmentResourceService fulfilmentResourceService;

    @NoLogin
    @RequestMapping(value = "mtxn/details", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody FulfilmentResponse getDataFromMtxnId(@RequestParam("mtxnId") String mtxnId) {
        try {
            return fulfilmentResourceService.getFulfilmentResponseForMtxn(mtxnId);
        } catch (Exception e) {
            return new FulfilmentResponse(FulfilmentConstants.FULFILMENT_STATUS_GENERIC_EXCEPTION,
                    "Server side exception");
        }
    }

    @NoLogin
    @RequestMapping(value = "order/details", method = RequestMethod.GET, produces = "application/json")
    @Authenticate(api="FULFILMENT_DATA_ORDER")
    public @ResponseBody FulfilmentResponse getDataFromOrderId(@RequestParam("orderId") String orderId) {
        try {
            return fulfilmentResourceService.getFulfilmentResponseForOrder(orderId);
        } catch (Exception e) {
            return new FulfilmentResponse(FulfilmentConstants.FULFILMENT_STATUS_GENERIC_EXCEPTION,
                    "Server side exception");
        }
    }

    @NoLogin
    @RequestMapping(value = "mtxn/amount", method = RequestMethod.GET, produces = "application/json")
    @Authenticate(api="FULFILMENT_DATA_ORDER")
    public @ResponseBody FulfilmentAmount getFulfilmentAmountForMtxn(@RequestParam("mtxnId") String mtxnId) {
        try {
            return fulfilmentResourceService.getFulfilmentAmountForMtxn(mtxnId);
        } catch (Exception e) {
            return new FulfilmentAmount(FulfilmentConstants.FULFILMENT_STATUS_GENERIC_EXCEPTION,
                    "Server side exception");
        }
    }
}
