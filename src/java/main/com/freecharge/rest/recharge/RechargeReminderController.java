package com.freecharge.rest.recharge;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.payment.dao.RemindRechargeDAO;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.model.AbstractRestResponse;
import com.freecharge.rest.recharge.RemindRecharge.ReminderAction;

@Controller
public class RechargeReminderController {

    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    RemindRechargeDAO             remindRechargeDAO;

    @RequestMapping(value = "/rest/recharge/remind", method = RequestMethod.POST)
    public @ResponseBody AbstractRestResponse registerReminder(RemindInput remindInput) {
        AbstractRestResponse response = new AbstractRestResponse();
        try {
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                    .findUserTransactionHistoryByOrderId(remindInput.getOrderId());
            RemindRecharge remindRecharge = new RemindRecharge();
            remindRecharge.setInsertedOn(new Date());
            remindRecharge.setOperatorName(userTransactionHistory.getServiceProvider());
            remindRecharge.setRechargeAmount(userTransactionHistory.getTransactionAmount());
            remindRecharge.setRechargeNo(userTransactionHistory.getSubscriberIdentificationNumber());
            remindRecharge.setUserId(userTransactionHistory.getFkUserId());
            remindRecharge.setPattern(Integer.parseInt(remindInput.getRemindInDays()));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, Integer.parseInt(remindInput.getRemindInDays()));
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.SECOND, 00);
            remindRecharge.setRemindOn(calendar.getTime());
            remindRecharge.setActive(true);
            remindRecharge.setOrderId(remindInput.getOrderId());
            remindRecharge.setReminderAction(ReminderAction.valueOf(remindInput.getAction()));
            remindRechargeDAO.insertRemindRecharge(remindRecharge);
            response.setSuccessResponse();
        } catch (Exception e) {
            response.setFailureResponse();
            response.setErrorMessage("Failed to register the remind alert!");
        }
        return response;
    }
}
