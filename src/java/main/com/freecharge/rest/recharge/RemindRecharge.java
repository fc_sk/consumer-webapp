package com.freecharge.rest.recharge;

import java.util.Date;

public class RemindRecharge {

	private Long reminderId;

	private Integer userId;

	private String rechargeNo;

	private String operatorName;

	private Date remindOn;

	private Double rechargeAmount;

	private Date insertedOn;

	private boolean isActive;

	private String orderId;

	private Integer pattern;
	
	private Integer planId;

	private ReminderAction reminderAction;

	public Long getReminderId() {
		return reminderId;
	}

	public void setReminderId(Long reminderId) {
		this.reminderId = reminderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRechargeNo() {
		return rechargeNo;
	}

	public void setRechargeNo(String rechargeNo) {
		this.rechargeNo = rechargeNo;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public Date getRemindOn() {
		return remindOn;
	}

	public void setRemindOn(Date remindOn) {
		this.remindOn = remindOn;
	}

	public Double getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(Double rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public Date getInsertedOn() {
		return insertedOn;
	}

	public void setInsertedOn(Date insertedOn) {
		this.insertedOn = insertedOn;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public ReminderAction getReminderAction() {
		return reminderAction;
	}

	public void setReminderAction(ReminderAction reminderAction) {
		this.reminderAction = reminderAction;
	}

	public Integer getPattern() {
		return pattern;
	}

	public void setPattern(Integer pattern) {
		this.pattern = pattern;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}



	public enum ReminderAction {

		REMINDER, RECHARGE;
	}
}
