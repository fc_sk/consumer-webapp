package com.freecharge.rest.recharge;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.cartcheckout.CreateCartTaskRequest;
import com.freecharge.cartcheckout.constants;
import com.freecharge.common.client.CheckoutDetailsRequestParams;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.client.CreateCartRequest;
import com.freecharge.common.comm.google.GooglePostTxnService;
import com.freecharge.postTxn.google.dto.GoogleInstallRefererRequest;
import com.freecharge.postTxn.google.response.GoogleInstallRefererResponse;
import com.freecharge.postTxn.google.response.GoogleReError;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.payments.ts.TaskScheduler;
import com.snapdeal.payments.ts.dto.TaskDTO;
import com.snapdeal.payments.ts.exception.DuplicateTaskException;
import com.snapdeal.payments.ts.exception.InvalidTaskTypeException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.analytics.CartItemDetails;
import com.freecharge.analytics.OrderDetails;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.dao.ICustomRechargePlansTxnTrackerWriteDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.AutoPayAppService;
import com.freecharge.app.service.AutoPayAppService.AutopayAPIVersion;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.PostRechargeService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.UserService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.DTHValidationConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.google.GoogleCreditsService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.service.helper.SessionHelper;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dao.AppVersionOrderDAO;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dao.RemindRechargeDAO;
import com.freecharge.payment.dos.entity.AppVersionOrder;
import com.freecharge.payment.dos.entity.AppVersionOrderRequest;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.cache.RechargeResourceCache;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.recharge.businessDo.DTHValidationResponse;
import com.freecharge.recharge.businessDo.EuroDTHValidationResponseDo;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.businessDo.RechargeResponseWebDo;
import com.freecharge.recharge.collector.InvalidDenominationCache;
import com.freecharge.recharge.collector.RechargeDenominationStat;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleExecutorService;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.services.OperatorUptimeCommService;
import com.freecharge.recharge.services.PlanValidatorService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.services.ValidationService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.recharge.RemindRecharge.ReminderAction;
import com.freecharge.rest.rechargeplans.plansdenomination.PlansDenomination;
import com.freecharge.rest.rechargeplans.plansdenomination.PlansDenominationService;
import com.freecharge.rest.user.bean.FailureRechargeInfo;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.rest.validators.CircleLevelValidation;
import com.freecharge.rest.validators.ServiceNumberFormatValidator;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.BaseValidationService;
import com.freecharge.validation.DO.ValidationResponse;
import com.freecharge.validation.services.CommonValidationService;
import com.freecharge.wallet.exception.OneCheckWalletException;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.webdo.BBPSDetails;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.FeeCharge;
import com.freecharge.web.webdo.PostRechargeDetailsWebDo;
import com.freecharge.web.webdo.RechargeInitiateWebDo;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.client.impl.UserServiceClientImpl;
import com.snapdeal.ims.dto.ClientConfig;
import com.snapdeal.ims.request.GetUserByIdRequest;
import com.snapdeal.ims.response.GetUserResponse;
import com.google.gson.Gson;


@Controller
@RequestMapping("/rest/recharge/*")
public class RechargeResourceController extends AbstractRestController {

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    private RechargeInitiateService rechargeInitiateService;

    @Autowired
    private RechargeMetricService rechargeMetricService;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private Tracker tracker;

    @Autowired
    private UserService userService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;

    @Autowired
    private BinOfferService binOfferService;

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private ConvinenceFeeDetailsDao convinenceFeeDetailsDao;

    @Autowired
    private SessionHelper sessionHelper;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private OperatorAlertService alertService;

    @Autowired
    private OperatorUptimeCommService inactiveOperatorService;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private FulfillmentScheduleExecutorService scheduleExecutor;

    @Autowired
    private RechargePattern rechargePattern;

    @Autowired
    private RemindRechargeDAO remindRechargeDAO;

    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Autowired
    private BBPSDetailsDao bbpsDetailsDao;

    @Autowired
    private RechargePlanService rechargePlanService;

    @Autowired
    private RechargeResourceCache rechargeCache;

    @Autowired
    private InvalidDenominationCache denominationCache;

    @Autowired
    private EuronetService euronetService;

    @Autowired
    private PostRechargeService postRechargeService;

    @Autowired
    private BaseValidationService baseValidationService;

    @Autowired
    private InTransactionHelper inTransactionHelper;

    @Autowired
    private AutoPayAppService autoPayAppService;

    @Autowired
    private PlansDenominationService plansDenominationService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private GoogleCreditsService googleCreditsService;

    private IUserServiceClient userClient;

    @Autowired
    private KpayPaymentMetaDAO kpayPaymentMetaDAO;


    @Autowired
    private AppVersionOrderDAO appVersionOrderDAO;

    @Autowired
    private PlanValidatorService planValidatorService;

    @Autowired
    private CommonValidationService commonValidationService;

    @Autowired
    private ICustomRechargePlansTxnTrackerWriteDAO customRechargePlansTxnTrackerWriteDAO;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    CartCheckoutServiceClientImpl cartClient;


    @Autowired
    GooglePostTxnService googlePostTxnService;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    private final int MOBILE_NUMBER_LENGTH = 10;
    private final String CALL_STATUS = "callStatus";
    private final String FAILURE = "failure";
    private final String SUCCESS = "success";
    private final String ERROR_CODE = "errorCode";
    private final String ERROR_MSG = "errorMessage";
    private final String LOOKUP_ID = "lookupId";
    private final String HOME_PAGE_ID = "txnHomePgID";

    private final Map<String, String> INTERNAL_ERROR = ImmutableMap.of(
            CALL_STATUS, FAILURE,
            ERROR_CODE, ErrorCode.INTERNAL_SERVER_ERROR.getErrorNumberString()
    );

    private final String VODAFONE = "Vodafone";
    private final String AIRCEL = "Aircel";
    private final String IDEA = "Idea";
    private final String BSNL = "BSNL";

    private final int APP_VERSION_HANDLING_SERVER_VALIDATION = 21;

    private final int APP_VERSION_HANDLING_EG_ERROR = 22;

    @PostConstruct
    public void init() throws OneCheckWalletException {
        try {
            com.snapdeal.ims.utils.ClientDetails.init(fcProperties.getProperty(FCProperties.IMS_CLIENT_URL),
                    fcProperties.getProperty(FCProperties.IMS_CLIENT_PORT),
                    fcProperties.getProperty(FCProperties.IMS_SLAVE_URL),
                    fcProperties.getProperty(FCProperties.IMS_SLAVE_PORT),
                    fcProperties.getProperty(FCProperties.IMS_CLIENT_KEY),
                    fcProperties.getProperty(FCProperties.IMS_CLIENT_ID),
                    fcProperties.getIntProperty(FCProperties.IMS_CLIENT_TIMEOUT));

            userClient = new UserServiceClientImpl();
        } catch (Exception e) {
            logger.error("Exception while initializing IMS Client Config", e);
        }
    }

    @RequestMapping(value = "saveInKpayDb", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude = true)
    public @ResponseBody
    Boolean saveInKpayDb(@RequestBody KlickpayPaymentMeta requestObj, HttpServletRequest request) {
        try {
            kpayPaymentMetaDAO.insert(requestObj);
        } catch (Exception e) {
            logger.error("Exception caught", e);
            return false;
        }
        return true;
    }

    @RequestMapping(value = "readFromKpayDb", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude = true)
    public @ResponseBody
    List<KlickpayPaymentMeta> readFromKpayDb(HttpServletRequest request) {
        try {
            String mTxnId = request.getParameter("mTxnId");
            return kpayPaymentMetaDAO.getKpayPaymentMeta(mTxnId);
        } catch (Exception e) {
            logger.error("Exception caught", e);
            return null;
        }
    }

    @RequestMapping(value = "saveInAppVersionDb", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude = true)
    public @ResponseBody
    Boolean saveInAppVersionDb(@RequestBody AppVersionOrderRequest requestObj, HttpServletRequest request) {
        try {
            AppVersionOrder requestObj1 = new AppVersionOrder(requestObj.getOrderId(), requestObj.getAppVersion(), requestObj.getCreatedTime());
            appVersionOrderDAO.insertAppVersionOrder(requestObj1);
        } catch (Exception e) {
            logger.error("Exception caught", e);
            return false;
        }
        return true;
    }

    @RequestMapping(value = "readFromAppVersionDb", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude = true)
    public @ResponseBody
    Integer readFromInAppVersionDb(HttpServletRequest request) {
        try {
            String orderId = request.getParameter("orderId");
            return orderIdSlaveDAO.getMobileAppVersion(orderId);
        } catch (Exception e) {
            logger.error("Exception caught", e);
            return null;
        }
    }


    @RequestMapping(value = "validateNumber/{productType}/{operatorId}/{circleId}/{serviceNumber:.+}", method = RequestMethod.GET)
    public @ResponseBody
    ModelMap validateNumber(@PathVariable final String productType, @PathVariable final Integer operatorId,
                            @PathVariable final String circleId, @PathVariable final String serviceNumber, final ModelMap model) throws InvalidParameterException {

        DTHValidationResponse dthValidationResponse = new DTHValidationResponse(DTHValidationConstants.NO_ERROR, DTHValidationConstants.VALID_DTH_NUMBER_RESPONSE_MESSAGE);
        final List<String> invalidCodes = ImmutableList.of("ECI", "CI", "EMI");
        final int defaultConnectionTimeout = 2000;
        if (appConfigService.isDTHNumberValidationEnabled()) {
            EuroDTHValidationResponseDo euroDTHValidationResponse = euronetService.validateNumber(productType, operatorId, circleId, serviceNumber, defaultConnectionTimeout, 0f);
            if (euroDTHValidationResponse != null) {
                if (invalidCodes.contains(euroDTHValidationResponse.getResponseCode())) {
                    dthValidationResponse.setResponseCode(DTHValidationConstants.INVALID_DTH_NUMBER_RESPONSE_CODE);
                    dthValidationResponse.setResponseMessage(DTHValidationConstants.INVALID_DTH_NUMBER_RESPONSE_MESSAGE);
                }
            }
        }
        return model.addAttribute("dthValidationResponse", dthValidationResponse);
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @NoLogin
    public @ResponseBody
    Map<String, String> saveRechargeInfo(RechargeRequest rechargeRequest,
                                         BindingResult bindingResult, HttpServletRequest request) {
        return saveRechargeInfoNew(rechargeRequest, bindingResult, request);
    }

    @RequestMapping(value = "v2/save", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> saveRechargeInfoV2(RechargeRequest rechargeRequest,
                                           BindingResult bindingResult, HttpServletRequest request) {

        Map<String, String> errorResult = validate(rechargeRequest, bindingResult, request);

        if (errorResult != null && !errorResult.isEmpty()) {
            return errorResult;
        }

        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();

        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        final String planType = rechargeRequest.getRechargePlanType();
        int circleId = -1;
        try {
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case
            // parsing fails. And circleId remains as -1, which is wht we want
        }

        try {
            Users loggedInUser = userServiceProxy.getLoggedInUser();

            TxnHomePageBusinessDO thpDO = createTxnHomePageBusinessDO(serviceNumber, productType, rechargeAmount,
                    operator, circleName, planType);

            logger.info("About to create log for lookupid: " + thpDO.getLookupId());

            TxnHomePageBO homePageBO = txnHomePageService.saveInfoAndCreateCart(thpDO,
                    loggedInUser.getUserId().toString(), loggedInUser.getEmail(), planType, rechargeRequest.getPlanId(), rechargeRequest.getPlanCategory());

            sessionHelper.saveDataInSession(thpDO);

            saveInCampaignService(serviceNumber, productType, rechargeAmount, operator, circleId, thpDO.getSessionId(),
                    thpDO.getLookupId());

            return successResponse(homePageBO.getCartId(), homePageBO.getHomePageId());
        } catch (Exception e) {
            logger.error("Exception while saving recharge", e);
            metricsClient.recordEvent("Recharge.Controller", "SaveCall", "Exception");
            return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
        }
    }

    @RequestMapping(value = "v3/save", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String> saveRechargeInfoV3(RechargeRequest rechargeRequest, BindingResult bindingResult, HttpServletRequest request) {

        Map<String, String> errorResult = validateV3(rechargeRequest, bindingResult, request);

        logger.info("Hodor : v3/save request received:" + rechargeRequest.toString());

        if (errorResult != null && !errorResult.isEmpty()) {
            return errorResult;
        }

        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();

        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        final String planType = rechargeRequest.getRechargePlanType();
        int circleId = -1;
        try {
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case parsing fails. And circleId remains as -1, which is wht we want
        }

        try {

            Users loggedInUser = userServiceProxy.getLoggedInUser();

            TxnHomePageBusinessDO thpDO = createTxnHomePageBusinessDO(serviceNumber, productType, rechargeAmount, operator, circleName, planType);

            logger.info("About to create log for lookupid: " + thpDO.getLookupId());

            TxnHomePageBO homePageBO = txnHomePageService.saveInfoAndCreateCart(thpDO, loggedInUser.getUserId().toString(), loggedInUser.getEmail(), planType, rechargeRequest.getPlanId(), rechargeRequest.getPlanCategory());

            sessionHelper.saveDataInSession(thpDO);

            saveInCampaignService(serviceNumber, productType, rechargeAmount, operator, circleId, thpDO.getSessionId(), thpDO.getLookupId());

            saveCustomRechargePlanTxnDetails(rechargeRequest, thpDO);

            CreateCartRequest createCartRequest = new CreateCartTaskRequest();
            createCartRequest.setCategoryName(FCConstants.productTypeToNameMap.get(productType));
            createCartRequest.setLookupId(thpDO.getLookupId());
            createCartRequest.setServiceNumber(serviceNumber);
            createCartRequest.setProductType(productType);
            createCartRequest.setAmount(rechargeAmount);
            createCartRequest.setPlanType(planType);
            createCartRequest.setCircleName(circleName);
            logger.info("Setting userid for cart request "+ FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
            createCartRequest.setUserId((String) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
            logger.info("Created cart request " + createCartRequest.toString());	
            logger.info("calling cart checkout service to create cart for lookupId: " + createCartRequest.getLookupId());
            try {
                CheckoutResponse checkoutResponse = cartClient.createCart(createCartRequest);
                logger.info("cart id returned from checkout: " + checkoutResponse.getCheckoutId());
            } catch (Exception exp) {
                logger.info("Unable to create cart in cartcheckout. Scheduling task scheduler for lookupid: " + createCartRequest.getLookupId());
                CreateCartTaskRequest createCartTaskRequest = (CreateCartTaskRequest) createCartRequest;
                String taskId = generateTaskId();
                createCartTaskRequest.setTaskId(taskId);
                //check why taskdto should be made as final
                TaskDTO taskDTO = new TaskDTO();
                taskDTO.setRequest(createCartTaskRequest);
                taskDTO.setTaskType(constants.CREATE_CART_TASK_TYPE);

                taskDTO.setCurrentScheduleTime(new Date());
                try {
                    taskScheduler.submitTask(taskDTO);
                } catch (DuplicateTaskException e) {

                    final String exceptionMsg =
                            String.format("DuplicateTaskException while creating cart, "
                                    + "taskId: %s", taskId);
                    logger.error(exceptionMsg, e);
                } catch (InvalidTaskTypeException e) {

                    String exceptionMsg =
                            String.format(
                                    "InvalidTaskTypeException while creating IMSWalletTask, "
                                            + " taskId: %s", taskId);
                    logger.error(exceptionMsg, e);
                    throw e;
                }
            }
            return successResponse(homePageBO.getCartId(), homePageBO.getHomePageId());
        } catch (Exception e) {
            logger.error("Exception while saving recharge", e);
            metricsClient.recordEvent("Recharge.Controller", "SaveCall", "Exception");
            return baseValidationService.processError(ErrorCode.INTERNAL_SERVER_ERROR);
        }

    }

    private void saveCustomRechargePlanTxnDetails(RechargeRequest rechargeRequest, TxnHomePageBusinessDO thpDO) {
        if (StringUtils.isNotBlank(rechargeRequest.getFulfillmentIdf())) {
            CustomRechargePlanTxnTracker customRechargePlanTxnTracker = new CustomRechargePlanTxnTracker();
            customRechargePlanTxnTracker.setLookupId(thpDO.getLookupId());
            customRechargePlanTxnTracker.setAmount(rechargeRequest.getAmount());
            customRechargePlanTxnTracker.setFulfillmentIdf(rechargeRequest.getFulfillmentIdf());
            try {
                customRechargePlansTxnTrackerWriteDAO.savePlanDetails(customRechargePlanTxnTracker);
                logger.info(String.format("Saved custom recharge plan details for look up id : %s with fulfillmentIdf : %s", thpDO.getLookupId(), rechargeRequest.getFulfillmentIdf()));
            } catch (Exception e) {
                logger.error("Unable to save custom recharge plan details because of error : ", e);
            }
        } else {
            logger.info(String.format("Not saving any custom recharge plan details for look up id %s", thpDO.getLookupId()));
        }
    }

    private Map<String, String> validate(RechargeRequest rechargeRequest, BindingResult bindingResult, HttpServletRequest request) {
        logRequest(rechargeRequest);

        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "recharge", "click");

        ErrorCode validationResult = validateRequest(rechargeRequest, request);
        if (validationResult != ErrorCode.NO_ERROR) {
            return validationResult.restResponse();
        }
        return validateRecharge(rechargeRequest, bindingResult, request);
    }

    private Map<String, String> validateV3(RechargeRequest rechargeRequest, BindingResult bindingResult, HttpServletRequest request) {
        logRequest(rechargeRequest);
        Map<String, String> errorResult = validateRequestV3(rechargeRequest, request);
        if (errorResult != null && !errorResult.isEmpty()) {
            return errorResult;
        }
        return validateRechargeV3(rechargeRequest, bindingResult, request);
    }

    private ClientConfig getClientConfigForIms() {
        ClientConfig config = new ClientConfig();
        config.setApiTimeOut(fcProperties.getIntProperty(FCProperties.IMS_CLIENT_TIMEOUT));
        config.setClientId(fcProperties.getProperty(FCProperties.IMS_CLIENT_ID));
        config.setClientKey(fcProperties.getProperty(FCProperties.IMS_CLIENT_KEY));
        return config;
    }

    private Map<String, String> validateRechargeV3(RechargeRequest rechargeRequest, BindingResult bindingResult,
                                                   HttpServletRequest request) {
        Map<String, String> validation = validateRecharge(rechargeRequest, bindingResult, request);
        
        if (validation != null) {
        	logger.info("error code : " + validation.get(ERROR_CODE));
            return baseValidationService.processError(validation.get(ERROR_CODE));
        }
        return null;
    }

    private Map<String, String> validateRequestV3(RechargeRequest rechargeRequest, HttpServletRequest request) {
        if (rechargeRequest == null) {
            return baseValidationService.processError(ErrorCode.RECHARGE_REQUEST_NOT_PRESENT);
        }
        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final String operator = rechargeRequest.getOperator();

        if (StringUtils.isBlank(serviceNumber)) {
            return baseValidationService.processError(ErrorCode.SERVICE_NUMBER_NOT_PRESENT);
        }

        if (!StringUtils.isNumeric(serviceNumber)) {
            return baseValidationService.processError(ErrorCode.INVALID_SERVICE_NUMBER_FORMAT);
        }

        if (!AmountFormatValidator.isValid(rechargeRequest.getAmount())) {
            return baseValidationService.processError(ErrorCode.INVALID_AMOUNT_FORMAT);
        }

        if (StringUtils.isBlank(rechargeRequest.getProductType())) {
            return baseValidationService.processError(ErrorCode.PRODUCT_TYPE_NOT_PRESENT);
        }

        if (StringUtils.isBlank(rechargeRequest.getOperator())) {
            return baseValidationService.processError(ErrorCode.OPERATOR_NOT_PRESENT);
        }

        final ProductName productName = ProductName.fromCode(productType);

        if (!operatorCircleService.isValidOperator(productName.getProductId(), operator)) {
            return baseValidationService.processError(ErrorCode.OPERATOR_NOT_PRESENT);
        }

        switch (productName) {
            case Mobile:
            case DTH:
            case DataCard:
            case PostPaidDataCard:
                Map<String, String> validation = baseValidationService.validateRechargeProduct(productName, rechargeRequest);
                if (validation != null) {
                    return validation;
                }
                break;
            default:
                break;
        }
        return null;
    }

    private Map<String, String> validateRecharge(RechargeRequest rechargeRequest, BindingResult bindingResult, HttpServletRequest request) {
        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();
        logger.info("reached into validate recharge");
        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        final String planType = rechargeRequest.getRechargePlanType();
        int circleId = -1;
        try {
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case
            // parsing fails. And circleId remains as -1, which is wht we want
        }
        SuccessfulRechargeInfo previousRecharge = userService.getLastSuccessfulRechargeByNumber(serviceNumber);

        if (!isNewMobileApp(request)) {
            if (appConfigService.isRepeatRechargeBlockingEnabled()) {
                if (previousRecharge != null) {
                    final String previousRechargedOperator = previousRecharge.getOperatorCode();
                    metricsClient.recordEvent("Recharge.Block", rechargeRequest.getOperatorName(), circleName);
                    if (!StringUtils.isBlank(previousRechargedOperator)) {
                        if (VODAFONE.equals(previousRechargedOperator) || AIRCEL.equals(previousRechargedOperator)) {
                            return ErrorCode.REPEAT_TRANSACTION_FIVE_MINUTES.restResponse();
                        } else if (IDEA.equals(previousRechargedOperator)) {
                            return ErrorCode.REPEAT_TRANSACTION_EIGHT_MINUTES.restResponse();
                        } else if (BSNL.equals(previousRechargedOperator)
                                && checkBsnlSuccessfulBlockageCriteria(rechargeRequest, previousRecharge)) {
                            return ErrorCode.REPEAT_TRANSACTION_EIGHT_MINUTES.restResponse();
                        } else {
                            return ErrorCode.REPEAT_TRANSACTION_FIVE_MINUTES.restResponse();
                        }
                    }
                    return ErrorCode.REPEAT_TRANSACTION_FIVE_MINUTES.restResponse();
                }
            }
        }
        logger.info("Reached here");
        FailureRechargeInfo previousFailureRecharge = userService.getLastFailureRechargeByNumber(serviceNumber);

        if (!isNewMobileApp(request)) {
            if (previousFailureRecharge != null) {
                final String previousRechargedOperator = previousFailureRecharge.getOperatorCode();
                metricsClient.recordEvent("Recharge.Block", rechargeRequest.getOperatorName(), circleName);
                if (!StringUtils.isBlank(previousRechargedOperator)) {
                    if (BSNL.equals(previousRechargedOperator)
                            && checkBsnlFailureBlockageCriteria(rechargeRequest, previousFailureRecharge)) {
                        return ErrorCode.REPEAT_TRANSACTION_EIGHT_MINUTES.restResponse();
                    }
                }
            }
        }
        String rechargePlanType = RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        if (!StringUtils.isBlank(rechargeRequest.getRechargePlanType())) {
            rechargePlanType = rechargeRequest.getRechargePlanType();
        }
        logger.info("Reached here");
        if (appConfigService.isInTransactionValidationEnabled()) {
            ErrorCode planValidation = inTransactionHelper.validate(rechargeRequest, rechargePlanType, serviceNumber);
            if (planValidation != ErrorCode.NO_ERROR) {
                return failureResponse(planValidation.getErrorNumberString(), planValidation.getErrorMessage());
            }
        }
        logger.info("Reached here");
        if (appConfigService.isInvalidNumberBlockingEnabled()) {
            logger.info("Invalid Number Blocking Enabled:" + serviceNumber);
            if (rechargeMetricService.isInvalidNumber(serviceNumber)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidNumber", serviceNumber);
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidNumber");
                return ErrorCode.SERVICE_NUMBER_NOT_PRESENT.restResponse();
            }
        }
        logger.info("Reached here");
        if (rechargeCache.isPermanentFailure(productType, operator, serviceNumber)) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "PermanentFailure");
                return ErrorCode.RECHARGE_PERMANENT_FAILURE.restResponse();
            }
        }
        logger.info("Reached here");
        if (rechargeCache.isEGFailureNumber(serviceNumber, operator, rechargeAmount.doubleValue())) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_EG_ERROR)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "EG");
                return ErrorCode.RECHARGE_DEN_OR_OPERATOR.restResponse();
            }
        }
        Integer circleIdForOperatorBlocking = circleId;
        if (productType.equals("F")) {
            circleIdForOperatorBlocking = Integer.valueOf(RechargeConstants.RECHARGE_CIRCLE_FOR_ALL);
        }
        logger.info("Reached here");
        if (alertService.isOperatorInAlert(Integer.parseInt(operator), circleIdForOperatorBlocking)) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "OperatorAlert");
                Users loggedInUser = userServiceProxy.getLoggedInUser();
                logger.info("Operator Down. Storing information for " + loggedInUser.getEmail());
                inactiveOperatorService.storeImpactedUser(loggedInUser.getEmail(), loggedInUser.getMobileNo(), rechargeRequest);
                return ErrorCode.OPERATOR_DOWN.restResponse();
            }
        }
        logger.info("Reached here");
        if (alertService.isOperatorInAlert(Integer.parseInt(operator))) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "OperatorAlert");
                Users loggedInUser = userServiceProxy.getLoggedInUser();
                inactiveOperatorService.storeImpactedUser(loggedInUser.getEmail(), loggedInUser.getMobileNo(), rechargeRequest);
                return ErrorCode.OPERATOR_DOWN.restResponse();
            }
        }

        logger.info("Reached here");
        ErrorCode errorCode = CircleLevelValidation.validate(rechargeRequest);
        if (errorCode != ErrorCode.NO_ERROR) {
            return errorCode.restResponse();
        }
        logger.info("Reached here");
        if (appConfigService.isDataCardCircleBlockingEnabled()) {
            logger.info("Data Card Operator Circle Block enabled. Going for plan validation");
            if (isOperatorCircleBlockedForDataCard(rechargeRequest)) {
                try {
                    int operatorId = Integer.parseInt(operator);
                    Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
                    RechargePlan rechargePlan = rechargePlanService.getActiveRechargePlan(circleId, operatorId, productId, rechargeAmount);
                    if (rechargePlan == null) {
                        logger.error("No active recharge plan found, returning an error");
                        insertInvalidPlanAttempts(rechargeRequest);
                        return ErrorCode.INVALID_PLAN_TYPE.restResponse();
                    }
                } catch (Exception e) {
                    logger.error("An error occurred in parsing the operator id, product id and amount", e);
                }
            }
        }
        logger.info("Reached here");
        if (RechargeConstants.DTH_PRODUCT_TYPE.equals(productType)) {
        	logger.info("DTH product type with rechargeRequest: " + rechargeRequest.toString());
            if (appConfigService.isDTHNumberValidationEnabled()) {
            	logger.info("DTH validation enabled");
                try {
                    String circleIdString = String.valueOf(circleId);
                    DTHValidationResponse response = validationService.validateNumber(productType, operator,
                            circleIdString, serviceNumber, rechargeRequest.getAmount());
                    if (!DTHValidationConstants.NO_ERROR.equals(response.getResponseCode())) {
                        return failureResponse(response.getResponseCode(), response.getResponseMessage());
                    }
                } catch (Exception e) {
                    logger.error("An error occurred trying to validate the DTH number");
                }
            }

            if (appConfigService.isDTHPlanValidationEnabled()) {
                try {
                    if (planValidatorService.isOpEligibleForDTHPlanValidation(operator)) {
                        if (!planValidatorService.isValidDTHPlan(operator, serviceNumber, rechargeAmount)) {
                            return ErrorCode.INVALID_DENOMINATION.restResponse();
                        }
                    }

                } catch (Exception e) {
                    logger.error("An error occurred trying to validate the sun tv plans", e);
                }
            }
        }
        logger.info("Reached here");
        //check for JIO plan validation enabled)
        if (appConfigService.isJIONumberValidationEnabled()) {
            logger.info("JIO plan validation enabled");
            if (RechargeConstants.JIO_OP_ID.equals(operator) || RechargeConstants.JIO_DATACARD_OP_ID.equals(operator)) {
                ValidationResponse validationResponse = commonValidationService.validateNumber(serviceNumber, String.valueOf(rechargeAmount));       
                logger.info("JIO serviceNumber::"+serviceNumber+ "Response: "+ validationResponse.toString());
                if (validationResponse != null && !RechargeConstants.SUCCESS_RESPONSE_CODE.equals(validationResponse.getResponseCode())) {
                    return failureResponse(validationResponse.getResponseCode(), validationResponse.getResponseMessage());
                }
            }
        }

        ErrorCode invalidDenom = invalidDenomValidationV2(rechargeRequest, request, serviceNumber, productType, rechargeAmount, operator,
                circleName, planType, circleId, rechargePlanType);

        if (invalidDenom != ErrorCode.NO_ERROR) {
            insertInvalidPlanAttempts(rechargeRequest);
            return invalidDenom.restResponse();
        }

        return null;
    }

    private ErrorCode invalidDenomValidationV2(RechargeRequest rechargeRequest, HttpServletRequest request,
                                               final String serviceNumber, final String productType, final Float rechargeAmount, final String operator,
                                               final String circleName, final String planType, int circleId, String rechargePlanType) {
        String isInvalid = validateInvalidPlanDenominations(rechargeRequest);
        if ("Y".equalsIgnoreCase(isInvalid)) {
            return ErrorCode.INVALID_DENOMINATION;
        } else if ("N".equalsIgnoreCase(isInvalid)) {
            return ErrorCode.NO_ERROR;
        } else {
            if (appConfigService.isPlanValidationEnabled()) {
                if (!planValidatorService.isPlanValidThroughValidationAPI(serviceNumber, productType, rechargeAmount, operator, circleId, rechargePlanType)) {
                    return ErrorCode.INVALID_DENOMINATION;
                }

                if (operator.equals("5")) {
                    String complimentaryPlanType = getComplimentaryPlanType(rechargePlanType);
                    logger.info("planType:" + rechargePlanType + " compPlanType:" + complimentaryPlanType);
                    if (denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(), rechargePlanType)
                            && denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(),
                            complimentaryPlanType)) {
                        if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                    String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                            return ErrorCode.INVALID_DENOMINATION;
                        }
                    }
                    if (denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(), rechargePlanType)) {
                        if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                    String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                            return ErrorCode.INVALID_PLAN_TYPE;
                        }
                    }
                } else {
                    if (denominationCache.getValidatedInvalidDenomination(operator, circleName, rechargeAmount.intValue(),
                            rechargePlanType)) {
                        metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                        metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                        return ErrorCode.INVALID_DENOMINATION;
                    }
                }

            }

            if (rechargeCache.isInvalidDenominationNumber(productType, serviceNumber, operator, rechargeAmount.doubleValue(), rechargePlanType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber", String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(), rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeCache.isInvalidDenomination(operator, circleName, rechargeAmount.doubleValue(), rechargePlanType, productType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenomination", String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(), rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeCache.isAirtelInvalidDenomination(operator, circleName, rechargeAmount.doubleValue())) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.Airtel.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.AirtelInvalidDenomination", String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(), rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeCache.isAirtelInvalidDenominationCheck(operator, circleName, rechargeAmount.doubleValue())) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.Airtel.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.AirtelInvalidDenomination", String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(), rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeCache.isBsnlInvalidDenomination(operator, circleName, rechargeAmount.doubleValue(), planType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.BSNL.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.BSNLlInvalidDenomination", String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(), rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }
        }
        return ErrorCode.NO_ERROR;
    }

    private String validateInvalidPlanDenominations(RechargeRequest rechargeRequest) {
        String isInvalid = "";
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();
        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        final String planType = rechargeRequest.getRechargePlanType();
        try {
            Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
            logger.info("Operator : [" + operator + "] Circle Id : [" + circleName + "] Recharge Amount : ["
                    + String.valueOf(rechargeAmount) + "] Product Type : [" + productId + "] Plan Type : " + planType
                    + "]");
            if (appConfigService.isInvalidPlansDenominationValidationEnabled()) {
                // If plan denomination validation enabled do a check against the cache to see if the plan is present
                isInvalid = plansDenominationService.getInvalidDenominationCache(operator, circleName,
                        String.valueOf(rechargeAmount.intValue()), String.valueOf(productId));
            } else {
                logger.info(
                        "Invalid Plans Denomination Validation not enabled.Going forward with the validation from old flow");
            }
        } catch (Exception e) {
            logger.info("An error occurred trying to fetch invalid plan denominations.");
        }
        return isInvalid;
    }

    private void insertInvalidPlanAttempts(RechargeRequest rechargeRequest) {
        Integer userId = userServiceProxy.getLoggedInUser().getUserId();
        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();
        final String planType = rechargeRequest.getRechargePlanType();
        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        int circleId = -1;
        try {
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case parsing fails. And circleId remains as -1, which is wht we want
        }
        try {
            logger.info("Invalid plans attempt. Saving to database");
            Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
            PlansDenomination plansDenomination = plansDenominationService.getPlansDenominationDO(operator, String.valueOf(circleId),
                    String.valueOf(rechargeAmount), String.valueOf(productId), null, null, serviceNumber, planType);

            int updated = plansDenominationService.insertInvalidPlanAttempts(plansDenomination, String.valueOf(userId));
            if (updated != 1) {
                logger.error("An error occurred while inserting invalid attempts to database");
            }
        } catch (Exception e) {
            logger.error("An error occurred trying to insert invalid plan attempt to database", e);
        }

    }

    private boolean isOperatorCircleBlockedForDataCard(RechargeRequest rechargeRequest) {
        boolean isBlocked = false;
        final String productType = rechargeRequest.getProductType();
        final String circleName = rechargeRequest.getCircleName();
        final String operator = rechargeRequest.getOperator();
        int operatorId = -1;
        int circleId = -1;
        try {
            operatorId = Integer.parseInt(operator);
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case parsing fails. And circleId remains as -1, which is wht we want
        }
        try {
            Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
            //validate for datacard
            if (productId != 3) {
                return isBlocked;
            } else {
                isBlocked = plansDenominationService.isOperatorCircleBlocked(operatorId, circleId);
            }
        } catch (Exception e) {
            logger.error("An error occurred trying to fetch data card plan block");
        }
        return isBlocked;
    }

    private Map<String, String> saveRechargeInfoNew(RechargeRequest rechargeRequest, BindingResult bindingResult,
                                                    HttpServletRequest request) {
        logRequest(rechargeRequest);

        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "recharge", "click");

        ErrorCode validationResult = validateRequest(rechargeRequest, request);

        if (validationResult != ErrorCode.NO_ERROR) {
            return validationResult.restResponse();
        }

        final String serviceNumber = rechargeRequest.getServiceNumber();
        final String productType = rechargeRequest.getProductType();
        final Float rechargeAmount = rechargeRequest.getAmount();

        final String operator = rechargeRequest.getOperator();
        final String circleName = rechargeRequest.getCircleName();
        final String planType = rechargeRequest.getRechargePlanType();
        int circleId = -1;
        try {
            circleId = Integer.parseInt(circleName);
        } catch (Exception e) {
            // Sometimes, circleName may come as 'ALL' from UI, in this case
            // parsing fails. And circleId remains as -1, which is wht we want
        }

        String rechargePlanType = RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;

        if (appConfigService.isInTransactionValidationEnabled()) {
            ErrorCode planValidation = inTransactionHelper.validate(rechargeRequest, rechargePlanType, serviceNumber);
            if (planValidation != ErrorCode.NO_ERROR) {
                return planValidation.restResponse();
            }
        }

        if (!StringUtils.isBlank(rechargeRequest.getRechargePlanType())) {
            rechargePlanType = rechargeRequest.getRechargePlanType();
        }

        if (appConfigService.isInvalidNumberBlockingEnabled()) {
            logger.info("Invalid Number Blocking Enabled:" + serviceNumber);
            if (rechargeMetricService.isInvalidNumber(serviceNumber)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidNumber", serviceNumber);
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidNumber");
                return ErrorCode.SERVICE_NUMBER_NOT_PRESENT.restResponse();
            }
        }

        if (rechargeMetricService.isPermanentFailure(productType, operator, serviceNumber)) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "PermanentFailure");
                return ErrorCode.RECHARGE_PERMANENT_FAILURE.restResponse();
            }
        }

        if (rechargeMetricService.isEGFailureNumber(serviceNumber, operator, rechargeAmount.doubleValue())) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_EG_ERROR)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "EG");
                return ErrorCode.RECHARGE_DEN_OR_OPERATOR.restResponse();
            }
        }

        if (alertService.isOperatorInAlert(Integer.parseInt(operator), circleId)) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "OperatorAlert");
                return ErrorCode.OPERATOR_DOWN.restResponse();
            }
        }

        if (alertService.isOperatorInAlert(Integer.parseInt(operator))) {
            if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                metricsClient.recordEvent("Recharge", "Save.ServerBlock", "OperatorAlert");
                return ErrorCode.OPERATOR_DOWN.restResponse();
            }
        }


        ErrorCode errorCode = CircleLevelValidation.validate(rechargeRequest);
        if (errorCode != ErrorCode.NO_ERROR) {
            return errorCode.restResponse();
        }

        if (appConfigService.isDataCardCircleBlockingEnabled()) {
            logger.info("Data Card Operator Circle Block enabled. Going for plan validation");
            if (isOperatorCircleBlockedForDataCard(rechargeRequest)) {
                try {
                    int operatorId = Integer.parseInt(operator);
                    Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
                    RechargePlan rechargePlan = rechargePlanService.getActiveRechargePlan(circleId, operatorId, productId, rechargeAmount);
                    if (rechargePlan == null) {
                        logger.error("No active recharge plan found, returning an error");
                        insertInvalidPlanAttempts(rechargeRequest);
                        return ErrorCode.INVALID_PLAN_TYPE.restResponse();
                    }
                } catch (Exception e) {
                    logger.error("An error occurred in parsing the operator id, product id and amount", e);
                }
            }
        }

        ErrorCode invalidDenom = invalidDenomValidation(rechargeRequest, request, serviceNumber, productType, rechargeAmount, operator,
                circleName, planType, circleId, rechargePlanType);

        if (invalidDenom != ErrorCode.NO_ERROR) {
            insertInvalidPlanAttempts(rechargeRequest);
            return invalidDenom.restResponse();
        }

        return createHomePage(serviceNumber, productType, rechargeAmount, operator, circleName, circleId,
                rechargePlanType);
    }

    private ErrorCode invalidDenomValidation(RechargeRequest rechargeRequest, HttpServletRequest request,
                                             final String serviceNumber, final String productType, final Float rechargeAmount, final String operator,
                                             final String circleName, final String planType, int circleId, String rechargePlanType) {
        String isInvalid = validateInvalidPlanDenominations(rechargeRequest);
        if ("Y".equalsIgnoreCase(isInvalid)) {
            return ErrorCode.INVALID_DENOMINATION;
        } else if ("N".equalsIgnoreCase(isInvalid)) {
            return ErrorCode.NO_ERROR;
        } else {
            if (appConfigService.isPlanValidationEnabled()) {
                if (operator.equals("5")) {
                    String complimentaryPlanType = getComplimentaryPlanType(rechargePlanType);
                    logger.info("planType:" + rechargePlanType + " compPlanType:" + complimentaryPlanType);
                    if (denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(), rechargePlanType)
                            && denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(),
                            complimentaryPlanType)) {
                        if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                    String.valueOf(operator) + String.valueOf(circleId)
                                            + String.valueOf(rechargeAmount));
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                            return ErrorCode.INVALID_DENOMINATION;
                        }
                    }
                    if (denominationCache.getTDPlanValidity(circleName, rechargeAmount.intValue(), rechargePlanType)) {
                        if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                    String.valueOf(operator) + String.valueOf(circleId)
                                            + String.valueOf(rechargeAmount));
                            metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                            return ErrorCode.INVALID_PLAN_TYPE;
                        }
                    }
                } else {
                    if (denominationCache.getValidatedInvalidDenomination(operator, circleName,
                            rechargeAmount.intValue(), rechargePlanType)) {
                        metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                                String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                        metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                        return ErrorCode.INVALID_DENOMINATION;
                    }
                }

            }

            if (rechargeMetricService.isInvalidDenominationNumber(productType, serviceNumber, operator,
                    rechargeAmount.doubleValue(), rechargePlanType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenominationNumber",
                            String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenominationNumber");
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(),
                            rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeMetricService.isInvalidDenomination(operator, circleName, rechargeAmount.doubleValue(),
                    rechargePlanType, productType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.InvalidDenomination",
                            String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(),
                            rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeMetricService.isAirtelInvalidDenomination(operator, circleName, rechargeAmount.doubleValue())) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.Airtel.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.AirtelInvalidDenomination",
                            String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(),
                            rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeMetricService.isAirtelInvalidDenominationCheck(operator, circleName,
                    rechargeAmount.doubleValue())) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.Airtel.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.AirtelInvalidDenomination",
                            String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(),
                            rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }

            if (rechargeMetricService.isBsnlInvalidDenomination(operator, circleName, rechargeAmount.doubleValue(),
                    planType)) {
                if (isErrorCodeSupported(request, APP_VERSION_HANDLING_SERVER_VALIDATION)) {
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.BSNL.", "InvalidDenomination");
                    metricsClient.recordEvent("Recharge", "Save.ServerBlock.BSNLlInvalidDenomination",
                            String.valueOf(operator) + String.valueOf(circleId) + String.valueOf(rechargeAmount));
                    logInvalidDenomination(serviceNumber, operator, circleId, rechargeAmount.doubleValue(),
                            rechargePlanType);
                    return ErrorCode.INVALID_DENOMINATION;
                }
            }
        }
        return ErrorCode.NO_ERROR;
    }

    private void logInvalidDenomination(String serviceNumber, String operator, int circleId, double amount,
                                        String rechargePlanType) {
        try {
            logger.info("Invalid Denomination Block for ,serviceNumber:" + serviceNumber + ",operator:" + operator
                    + ",circle:" + String.valueOf(circleId) + ",amount:" + String.valueOf(amount));
        } catch (Exception e) {
            logger.error("Exception caught in logging InvalidDenomination ", e);
        }
    }

    private String getComplimentaryPlanType(String rechargePlanType) {
        if (!FCUtil.isEmpty(rechargePlanType)) {
            if (rechargePlanType.equals("topup")) {
                return "special";
            } else {
                return "topup";
            }
        }
        return "special";
    }

    private Map<String, String> createHomePage(final String serviceNumber, final String productType,
                                               final Float rechargeAmount, final String operator, final String circleName, int circleId,
                                               String rechargePlanType) {
        try {
            TxnHomePageBusinessDO thpDO = createTxnHomePageBusinessDO(serviceNumber, productType, rechargeAmount,
                    operator, circleName, rechargePlanType);
            txnHomePageService.saveTransactionInfo(thpDO);
            sessionHelper.saveDataInSession(thpDO);

            saveInCampaignService(serviceNumber, productType, rechargeAmount, operator, circleId, thpDO.getSessionId(),
                    thpDO.getLookupId());

            logger.debug("putting lookupid in session HomeController  " + thpDO.getLookupId());

            if (thpDO.getServiceStatus().equalsIgnoreCase(FCConstants.FAIL)) {
                logger.error("Get Transaction service status Fail");
                return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
            }

            return successResponse(thpDO.getLookupId(), thpDO.getTxnHomePageId().toString());
        } catch (Exception exp) {
            logger.error("exception raised while saving the recharge information in db ", exp);
            return INTERNAL_ERROR;
        }
    }

    private void saveInCampaignService(final String serviceNumber, final String productType, final Float rechargeAmount,
                                       final String operator, int circleId, String sessionId, String lookupId) {
        Integer productId = ProductName.fromPrimaryProductType(productType).getProductId();
        RechargePlan rechargePlan = rechargePlanService.getActiveRechargePlan(circleId, Integer.parseInt(operator),
                productId, rechargeAmount);
        String rechargePlanName;
        Integer rechargePlanId;
        Map<String, Object> transactionInfo = new HashMap<>();
        if (rechargePlan != null) {
            rechargePlanName = rechargePlan.getName();
            rechargePlanId = rechargePlan.getRechargePlanId();
            transactionInfo.put(InTxnDataConstants.RECHARGE_PLAN, rechargePlanName);
            transactionInfo.put(InTxnDataConstants.RECHARGE_PLAN_ID, rechargePlanId);
        }

        transactionInfo.put(InTxnDataConstants.RECHARGE_AMOUNT, rechargeAmount);
        String transInfoCirName = operatorCircleService.getCircle(circleId) == null ? ""
                : operatorCircleService.getCircle(circleId).getName();
        transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, transInfoCirName);
        String transInfoOpName = operatorCircleService.getOperator(Integer.parseInt(operator)) == null ? ""
                : operatorCircleService.getOperator(Integer.parseInt(operator)).getName();
        transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, transInfoOpName);
        transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, circleId);
        transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, Integer.parseInt(operator));
        transactionInfo.put(InTxnDataConstants.PRODUCT_TYPE, FCConstants.productTypeToNameMap.get(productType));
        transactionInfo.put(InTxnDataConstants.SERVICE_NUMBER, serviceNumber);
        transactionInfo.put(InTxnDataConstants.SESSION_ID, sessionId);
        try {
            campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID,
                    transactionInfo);
        } catch (Exception e) {
            logger.error("[Error occurred while saving intxndata] for session: " + sessionId, e);
        }
    }

    private TxnHomePageBusinessDO createTxnHomePageBusinessDO(final String serviceNumber, final String productType,
                                                              final Float rechargeAmount, final String operator, final String circleName, String rechargePlanType) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String lookupID = FCUtil.getLookupID();
        tracker.trackLookupId(lookupID);
        String sessionId = fs.getUuid();
        fs.setRechargePlanType(rechargePlanType);

        TxnHomePageBusinessDO thpDO = new TxnHomePageBusinessDO();
        thpDO.setProductType(productType);
        thpDO.setServiceNumber(serviceNumber);
        thpDO.setAmount(rechargeAmount);
        thpDO.setRechargePlanType(rechargePlanType);
        thpDO.setOperatorName(operator);
        thpDO.setCircleName(circleName);
        thpDO.setLookupId(lookupID);
        thpDO.setSessionId(sessionId);
        return thpDO;
    }

    private void logRequest(RechargeRequest request) {
        String productType = String.valueOf(request.getProductType());
        String operator = String.valueOf(request.getOperator());
        String circle = String.valueOf(request.getCircleName());
        String planType = String.valueOf(request.getRechargePlanType());
        String serviceNumber = String.valueOf(request.getServiceNumber());
        String amount = String.valueOf(request.getAmount());

        logger.info("Product: [" + productType + "] " + "Operator: [" + operator + "] " + "Circle: [" + circle + "] "
                + "RechargeType: [" + planType + "] " + "ServiceNumber: [" + serviceNumber + "] " + "Amount: [" + amount
                + "] ");
    }

    private Map<String, String> successResponse(String lookupID, String txnHomePgID) {
        return ImmutableMap.of(CALL_STATUS, SUCCESS, LOOKUP_ID, lookupID, HOME_PAGE_ID, txnHomePgID);
    }

    @NoSessionWrite
    @RequestMapping(value = "/{orderId}/status", method = RequestMethod.GET)
    public String handleRecharge(@PathVariable String orderId, HttpServletResponse response, Model model) {
        if (StringUtils.isBlank(orderId)) {
            return "jsonView";
        }

        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);

        if (loggedInUserId.intValue() != orderOwner.intValue()) {
            return "jsonView";
        }

        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);

        OrderDetails od = new OrderDetails();
        List<CartItemDetails> cartItemList = new ArrayList<>();
        od.setCartItemList(cartItemList);

        List<CartItemsBusinessDO> cartItemsBusinessDOs = null;
        boolean hasBinOffer = false;
        boolean hasBinOfferReward = false;
        String binOfferBannerImage = "";

        boolean hasFreefundOffer = false;
        Map<String, String> redeemedFreefund = new HashMap<String, String>();

        try {
            cartItemsBusinessDOs = orderIdSlaveDAO.getCartItemsByOrderId(orderId);
        } catch (Exception e) {
            logger.info("Retrieving Cart Items failed for OrderID : " + orderId, e);
        }
        if (cartItemsBusinessDOs != null) {
            for (CartItemsBusinessDO cartItemsBusinessDO : cartItemsBusinessDOs) {
                String cartItemsEntityId = cartItemsBusinessDO.getEntityId();
                int productMasterId = cartItemsBusinessDO.getProductMasterId();

                if (cartItemsEntityId != null) {

                    if (cartItemsEntityId.equals(FCConstants.FFREEFUND_PRODUCT_TYPE)) {
                        hasFreefundOffer = (productMasterId == FCConstants.PRODUCT_ID_FREEFUND_CASHBACK);

                        if (!hasFreefundOffer) {
                            hasBinOffer = (productMasterId == FCConstants.PRODUCT_ID_FREEFUND_BINOFFER);
                        }
                    }

                }

                if (hasFreefundOffer) {
                    try {

                        FreefundCoupon freefundCoupon = freefundService
                                .getFreefundCoupon((long) cartItemsBusinessDO.getItemId());
                        if (freefundCoupon != null) {
                            FreefundClass freefundClass = freefundService
                                    .getFreefundClass(freefundCoupon.getFreefundCode());

                            if (freefundClass != null) {

                                redeemedFreefund.put("offerName", freefundClass.getName());
                                Map dataMap = FCStringUtils.jsonToMap(freefundClass.getDatamap());
                                redeemedFreefund.put(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME,
                                        (String) dataMap.get(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME));
                            }

                        }
                    } catch (Exception e) {
                        logger.error("Retrieving Freefund details failed for OrderID : " + orderId, e);
                    }

                } else if (hasBinOffer) {

                    try {
                        BinOffer binOffer = binOfferService.getBinOffer(cartItemsBusinessDO.getItemId());
                        if (binOffer != null) {
                            hasBinOfferReward = BinOffer.OfferType.valueOf(binOffer.getOfferType())
                                    .equals(BinOffer.OfferType.REWARD);
                            if (hasBinOfferReward) {
                                binOfferBannerImage = binOffer.getBannerImage();
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Retrieving binOffer details failed for OrderID : " + orderId, e);
                    }
                }
            }
        }

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        dataMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, true);
        dataMap.put(PaymentConstants.AMOUNT, Math.round(rechargeInitiateWebDo.getAmount()));

        model.addAttribute("orderDetails", od);
        model.addAttribute("riwd", rechargeInitiateWebDo);
        model.addAttribute("resultMap", dataMap);
        model.addAttribute("product", rechargeDetails.getProductName());
        model.addAttribute("operator", rechargeDetails.getOperatorName());
        model.addAttribute("circle", rechargeDetails.getCircleName());
        model.addAttribute("hasBinOffer", hasBinOffer);
        model.addAttribute("hasBinOfferReward", hasBinOfferReward);
        model.addAttribute("binOfferBannerImage", binOfferBannerImage);
        model.addAttribute("hasFreefundOffer", hasFreefundOffer);
        model.addAttribute("redeemedFreefund", redeemedFreefund);

        /* required attributes for final page in JSON response added by AMAN */
        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());

        metricResponseCode(rechargeInitiateWebDo);

        try {
            if (appConfigService.isRechargeReminderEnabled()) {
                logger.info("Predicting next recharge date for " + orderId);
                RemindRecharge remindRecharge = new RemindRecharge();
                Double rechargeAmount = Double.parseDouble(rechargeDetails.getAmount());
                String productName = rechargeDetails.getProductName();
                rechargePattern.getNextRechargeDate(remindRecharge, cartItemsBusinessDOs,
                        Integer.parseInt(rechargeDetails.getOperatorMasterId()),
                        Integer.parseInt(rechargeDetails.getCircleId()), rechargeAmount.floatValue(),
                        FCConstants.productMasterMapReverse.get(FCConstants.productNameToTypeMap.get(productName)));
                if (remindRecharge.getRemindOn() != null) {
                    logger.info("Next recharge date for " + rechargeDetails.getOrderId() + " : "
                            + remindRecharge.getRemindOn());
                    logger.info("Recharge reminder enabled. Predicting next recharge date for "
                            + rechargeDetails.getOrderId());
                    remindRecharge.setActive(true);
                    remindRecharge.setInsertedOn(new Date());
                    remindRecharge.setOperatorName(rechargeDetails.getOperatorName());
                    remindRecharge.setOrderId(orderId);
                    remindRecharge.setRechargeAmount(Double.parseDouble(rechargeDetails.getAmount()));
                    remindRecharge.setRechargeNo(rechargeDetails.getServiceNumber());
                    remindRecharge.setReminderAction(ReminderAction.REMINDER);
                    remindRecharge.setRemindOn(remindRecharge.getRemindOn());
                    remindRecharge.setUserId(loggedInUserId);
                    remindRecharge.setPattern(0);
                    remindRechargeDAO.insertRemindRecharge(remindRecharge);
                }
            } else {
                logger.info("Failed to predict next recharge date for " + rechargeDetails.getOrderId());
            }
        } catch (Exception e) {
            logger.error("Exception determining next recharge date for " + rechargeDetails.getOrderId(), e);
        }

        return "jsonView";
    }

    @NoSessionWrite
    @RequestMapping(value = "/{orderId}/v2/status", method = RequestMethod.GET)
    public String handleRechargeNew(@PathVariable String orderId, HttpServletResponse response, Model model) {
        if (StringUtils.isBlank(orderId)) {
            return "jsonView";
        }

        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);

        if (loggedInUserId.intValue() != orderOwner.intValue()) {
            return "jsonView";
        }
        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        PostRechargeDetailsWebDo postRechargeDetails = postRechargeService.getPostRechargeDetails(orderId,
                rechargeInitiateWebDo, rechargeDetails, false);

        model.addAttribute("riwd", rechargeInitiateWebDo);

        /* required attributes for final page in JSON response added by AMAN */
        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());
        model.addAttribute("postRechargeDetails", postRechargeDetails);

        metricResponseCode(rechargeInitiateWebDo);

        return "jsonView";
    }

    @NoSessionWrite
    @RequestMapping(value = "/{orderId}/v3/status", method = RequestMethod.GET)
    public String handleRechargeWithAutoPayData(@PathVariable String orderId, HttpServletResponse response, Model model, HttpServletRequest httpRequest) {
//        if (StringUtils.isBlank(orderId)) {
//            return "jsonView";
//        }
//
//        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
//        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
//
//        if (loggedInUserId.intValue() != orderOwner.intValue()) {
//            return "jsonView";
//        }
//        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
//        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
//        PostRechargeDetailsWebDo postRechargeDetails = postRechargeService.getPostRechargeDetails(orderId,
//                rechargeInitiateWebDo, rechargeDetails);
//
//        model.addAttribute("riwd", rechargeInitiateWebDo);
//        Map<String,Object> autoPayMap = autoPayAppService.getAutopayMetadata(orderId,rechargeDetails,rechargeInitiateWebDo, loggedInUserId ,AutopayAPIVersion.VERSION_1);
//        if (!CollectionUtils.isEmpty(autoPayMap)) {
//            model.addAttribute("autopay", autoPayMap);
//        }
//
//        /* required attributes for final page in JSON response added by AMAN */
//        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
//        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());
//        model.addAttribute("postRechargeDetails", postRechargeDetails);
//
//        metricResponseCode(rechargeInitiateWebDo);
//
//		return "jsonView";


        //Copying v4 code here.
        if (StringUtils.isBlank(orderId)) {
            return "jsonView";
        }

        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);

        if (loggedInUserId.intValue() != orderOwner.intValue()) {
            return "jsonView";
        }

        String fcVersionStr = httpRequest.getParameter("fcversion");
        String fcChannelStr = httpRequest.getParameter("fcChannel");
        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        FeeCharge feeCharge = convinenceFeeDetailsDao.getFeeDetails(rechargeInitiateWebDo.getLookupID());
        BigDecimal convenienceFee = new BigDecimal(0);
        if (appConfigService.isConvenienceFeeEnabled() && feeCharge != null) {
            convenienceFee = feeCharge.getFeeAmount();
        }

        rechargeInitiateWebDo.setAmount(rechargeInitiateWebDo.getAmount() + convenienceFee.doubleValue());
        String rechargeDetailsAmount = String.valueOf(rechargeInitiateWebDo.getAmount());
        rechargeDetails.setAmount(rechargeDetailsAmount);
        int fcVersion = -1;
        int fcChannel = -1;
        if (fcVersionStr != null && fcChannelStr != null && fcVersionStr != "" && fcChannelStr != "") {
            fcVersion = Integer.valueOf(fcVersionStr);
            fcChannel = Integer.valueOf(fcChannelStr);
        }
        PostRechargeDetailsWebDo postRechargeDetails = new PostRechargeDetailsWebDo();
        Boolean deeplinkEnabled = false;
        if (fcChannel == 3 && fcVersion >= 123) {
            deeplinkEnabled = true;
        }
        postRechargeDetails = postRechargeService.getPostRechargeDetails(orderId,
                rechargeInitiateWebDo, rechargeDetails, deeplinkEnabled);


        setInvoiceFlag(rechargeInitiateWebDo, rechargeDetails);
        model.addAttribute("riwd", rechargeInitiateWebDo);
        Map<String, Object> autoPayMap = autoPayAppService.getAutopayMetadata(orderId, rechargeDetails, rechargeInitiateWebDo, loggedInUserId, AutopayAPIVersion.VERSION_2);
        if (!CollectionUtils.isEmpty(autoPayMap)) {
            model.addAttribute("autopay", autoPayMap);
        }
        /* required attributes for final page in JSON response added by AMAN */
        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());
        model.addAttribute("postRechargeDetails", postRechargeDetails);
        Map<String, Object> googleCreditsMap = googleCreditsService.getGoogleCreditsPostRechargeData(orderId, rechargeDetails, rechargeInitiateWebDo);
        if (!CollectionUtils.isEmpty(googleCreditsMap)) {
            model.addAttribute("gcprd", googleCreditsMap);
        }

        metricResponseCode(rechargeInitiateWebDo);

        return "jsonView";

    }

    @NoSessionWrite
    @RequestMapping(value = "/{orderId}/v4/status", method = RequestMethod.GET)
    public String handleRechargeWithAutoPayDataV2(@PathVariable String orderId, HttpServletResponse response, Model model, HttpServletRequest httpRequest) {
        if (StringUtils.isBlank(orderId)) {
            return "jsonView";
        }
        logger.info("Here is status page for orderId: " + orderId);
        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);

        if (loggedInUserId.intValue() != orderOwner.intValue()) {
            return "jsonView";
        }

        String fcVersionStr = httpRequest.getParameter("fcversion");
        String fcChannelStr = httpRequest.getParameter("fcChannel");
        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        logger.info("Before calling transaction history");
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        FeeCharge feeCharge = convinenceFeeDetailsDao.getFeeDetails(rechargeInitiateWebDo.getLookupID());
        BigDecimal convenienceFee = new BigDecimal(0);
        if (appConfigService.isConvenienceFeeEnabled() && feeCharge != null) {
            convenienceFee = feeCharge.getFeeAmount();
        }

        rechargeInitiateWebDo.setAmount(rechargeInitiateWebDo.getAmount() + convenienceFee.doubleValue());
        String rechargeDetailsAmount = String.valueOf(rechargeInitiateWebDo.getAmount());
        rechargeDetails.setAmount(rechargeDetailsAmount);
        int fcVersion = -1;
        int fcChannel = -1;
        if (fcVersionStr != null && fcChannelStr != null && fcVersionStr != "" && fcChannelStr != "") {
            fcVersion = Integer.valueOf(fcVersionStr);
            fcChannel = Integer.valueOf(fcChannelStr);
        }
        PostRechargeDetailsWebDo postRechargeDetails = new PostRechargeDetailsWebDo();
        Boolean deeplinkEnabled = false;
        if (fcChannel == 3 && fcVersion >= 123) {
            deeplinkEnabled = true;
        }
        postRechargeDetails = postRechargeService.getPostRechargeDetails(orderId,
                rechargeInitiateWebDo, rechargeDetails, deeplinkEnabled);


        setInvoiceFlag(rechargeInitiateWebDo, rechargeDetails);
        model.addAttribute("riwd", rechargeInitiateWebDo);
        //        Map<String,Object> autoPayMap = autoPayAppService.getAutopayMetadata(orderId,rechargeDetails,rechargeInitiateWebDo, loggedInUserId ,AutopayAPIVersion.VERSION_2);
        //        if (!CollectionUtils.isEmpty(autoPayMap)) {
        //            model.addAttribute("autopay", autoPayMap);
        //        }

        BBPSDetails bbpsDetails = new BBPSDetails();
        Boolean isBBPSEnabled = true;
        BBPSTransactionDetails bbpsTxnDetails = bbpsDetailsDao.getBBPSDetailsForLookupId(rechargeInitiateWebDo.getLookupID());
        if (bbpsTxnDetails != null) {
            bbpsDetails.setCustomerAccountNumber(bbpsTxnDetails.getCustomerAccountNumber());
            bbpsDetails.setCustomerName(bbpsTxnDetails.getCustomerName());
            bbpsDetails.setDueDate(bbpsTxnDetails.getBillDueDate());
            bbpsDetails.setBbpsReferenceNumber(bbpsTxnDetails.getBbpsReferenceNumber());
        }

        logger.info(" BBPS details: " + bbpsDetails.toString() + " for orderId: " + orderId);


        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());
        model.addAttribute("postRechargeDetails", postRechargeDetails);
        model.addAttribute("bbpsDetails", bbpsDetails);
        Map<String, Object> googleCreditsMap = googleCreditsService.getGoogleCreditsPostRechargeData(orderId, rechargeDetails, rechargeInitiateWebDo);
        if (!CollectionUtils.isEmpty(googleCreditsMap)) {
            model.addAttribute("gcprd", googleCreditsMap);
        }
        logger.info("Existing v4 status call for orderId: " + orderId);
        metricResponseCode(rechargeInitiateWebDo);

        return "jsonView";
    }


    @NoSessionWrite
    @RequestMapping(value = "/{orderId}/v5/status", method = RequestMethod.GET)
    public String handleRechargeWithAutoPayDataV5(@PathVariable String orderId, HttpServletResponse response, Model model, HttpServletRequest httpRequest) {
        if (StringUtils.isBlank(orderId)) {
            return "jsonView";
        }
        logger.info("Here is status page");
        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);

        if (loggedInUserId.intValue() != orderOwner.intValue()) {
            return "jsonView";
        }

        String fcVersionStr = httpRequest.getParameter("fcversion");
        String fcChannelStr = httpRequest.getParameter("fcChannel");
        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        logger.info("Before calling transaction history");
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        FeeCharge feeCharge = convinenceFeeDetailsDao.getFeeDetails(rechargeInitiateWebDo.getLookupID());
        BigDecimal convenienceFee = new BigDecimal(0);
        if (appConfigService.isConvenienceFeeEnabled() && feeCharge != null) {
            convenienceFee = feeCharge.getFeeAmount();
        }

        rechargeInitiateWebDo.setAmount(rechargeInitiateWebDo.getAmount() + convenienceFee.doubleValue());
        String rechargeDetailsAmount = String.valueOf(rechargeInitiateWebDo.getAmount());
        rechargeDetails.setAmount(rechargeDetailsAmount);
        int fcVersion = -1;
        int fcChannel = -1;
        if (fcVersionStr != null && fcChannelStr != null && fcVersionStr != "" && fcChannelStr != "") {
            fcVersion = Integer.valueOf(fcVersionStr);
            fcChannel = Integer.valueOf(fcChannelStr);
        }
        PostRechargeDetailsWebDo postRechargeDetails = new PostRechargeDetailsWebDo();
        Boolean deeplinkEnabled = false;
        if (fcChannel == 3 && fcVersion >= 123) {
            deeplinkEnabled = true;
        }
        postRechargeDetails = postRechargeService.getPostRechargeDetails(orderId,
                rechargeInitiateWebDo, rechargeDetails, deeplinkEnabled);


        setInvoiceFlag(rechargeInitiateWebDo, rechargeDetails);
        model.addAttribute("riwd", rechargeInitiateWebDo);
        //        Map<String,Object> autoPayMap = autoPayAppService.getAutopayMetadata(orderId,rechargeDetails,rechargeInitiateWebDo, loggedInUserId ,AutopayAPIVersion.VERSION_2);
        //        if (!CollectionUtils.isEmpty(autoPayMap)) {
        //            model.addAttribute("autopay", autoPayMap);
        //        }

        BBPSDetails bbpsDetails = new BBPSDetails();
        Boolean isBBPSEnabled = true;
        if (isBBPSEnabled) {
//    		BBPSTransactionDetails bbpsTxnDetails = bbpsDetailsDao.getBBPSDetails(rechargeInitiateWebDo.getLookupID());
//    		if(bbpsTxnDetails != null) {
            bbpsDetails.setCustomerAccountNumber("123123123123");
            bbpsDetails.setCustomerName("Testing123");
            bbpsDetails.setDueDate("1514359680");
            bbpsDetails.setBbpsReferenceNumber("BBPS123");
//    		}
            //        	UserTransactionHistory userTxnHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
            //        	Gson serializer = new Gson();
            //        	if(userTxnHistory.getMetadata() != null) {
            //        		Map<String, Object> metadataMap = serializer.fromJson(userTxnHistory.getMetadata(), HashMap.class);
            //        		if(metadataMap.get("BBPSReferenceNumber") != null) {
            //        			bbpsReferenceNumber = String.valueOf(metadataMap.get("BBPSReferenceNumber"));
            //        		}
            //        	}

            logger.info(" BBPS details: " + bbpsDetails.toString());
        }


        model.addAttribute("operatorLogo", rechargeDetails.getOperatorImageURL());
        model.addAttribute("rechargeDate", rechargeDetails.getRechargedOn());
        model.addAttribute("postRechargeDetails", postRechargeDetails);
        model.addAttribute("bbpsDetails", bbpsDetails);
        Map<String, Object> googleCreditsMap = googleCreditsService.getGoogleCreditsPostRechargeData(orderId, rechargeDetails, rechargeInitiateWebDo);
        if (!CollectionUtils.isEmpty(googleCreditsMap)) {
            model.addAttribute("gcprd", googleCreditsMap);
        }
        logger.info("Existing v4 status call");
        metricResponseCode(rechargeInitiateWebDo);

        return "jsonView";
    }

    private void setInvoiceFlag(RechargeInitiateWebDo rechargeInitiateWebDo, RechargeDetails rechargeDetails) {
        try {
            logger.info("Enter into setInvoiceFlag for rechargeDetails " + rechargeDetails);
            ProductName productName = ProductName.fromCode(FCConstants.productNameToTypeMap.get(rechargeDetails.getProductName()));
            String txnType = FCConstants.productNameToTxnTypeMap.get(rechargeDetails.getProductName());
            rechargeInitiateWebDo.setTxnType(txnType);

            Calendar gstStart = Calendar.getInstance();
            gstStart.set(2017, 6, 1, 0, 0);
            Calendar convenienceFeeDisableDate = Calendar.getInstance();
            convenienceFeeDisableDate.set(2017, 7, 1, 12, 56);

            JSONObject json = appConfigService.getCustomProp(AppConfigService.MERCHANT_ID_MAPPING);
            if (rechargeDetails.getRechargedOn().after(gstStart.getTime())) {
                if (productName == ProductName.Mobile || productName == ProductName.DTH) {
                    rechargeInitiateWebDo.setInvoice(true);
                }
                if (productName.isUtilityPaymentProduct() && rechargeDetails.getRechargedOn().before(convenienceFeeDisableDate.getTime())) {
                    if (json.containsKey(rechargeDetails.getOperatorMasterId())) {
                        rechargeInitiateWebDo.setInvoice(true);
                    }
                }
            }
            logger.info("Exit from setInvoiceFlag for rechargeInitiateWebDo " + rechargeInitiateWebDo);
        } catch (Exception e) {
            logger.error("New Invoice Flag set Error");
        }
    }

    private void metricResponseCode(RechargeInitiateWebDo rechargeInitiateWebDo) {
        if (rechargeInitiateWebDo != null) {
            if (StringUtils.isNotBlank(rechargeInitiateWebDo.getRechargeResponse())) {
                if (StringUtils.isNotBlank(rechargeInitiateWebDo.getOperator())) {
                    metricsClient.recordEvent("Recharge", "FirstResponse",
                            rechargeInitiateWebDo.getOperator() + "." + rechargeInitiateWebDo.getRechargeResponse());
                }
            }
        }
    }

    @NoLogin
    @RequestMapping(value = "/update")
    @Csrf(exclude = true)
    public void handleStatusUpdate(HttpServletRequest request, HttpServletResponse response) {
        /*
         *
         * MerchantID=4&TransID=63738& ResponseCode=0&ResponseMessage=Success&
         * OxigenTransID=000000727282&
         * IntegratorID=37373&RequestTime=20111112121212&CompletionTime=
         * 20111113131211&OperatorTranID=2018291892
         *
         * Code=1&Message=success&CustomerMessage=successfull&RequestID=112233&
         * Amount=50&Balance=5000&TransactionNumber=030303038484848
         */
        @SuppressWarnings("unused")
        List<String> paramList = Arrays.asList("MerchantID", "TransID", "ResponseCode", "ResponseMessage",
                "OxigenTransID", "IntegratorID", "RequestTime", "CompletionTime", "OperatorTranID", "Code", "Message",
                "CustomerMessage", "RequestID", "Amount", "Balance", "TransactionNumber");

        StringBuffer logStatement = new StringBuffer("");

        for (Enumeration<String> paramArray = request.getParameterNames(); paramArray.hasMoreElements(); ) {
            String key = paramArray.nextElement();

            if (key.equals("RequestTime") || key.equals("CompletionTime")) {
                Long time = Long.parseLong(request.getParameter(key));

                logStatement.append(key + ":" + String.valueOf(new Date(time * 1000)) + ",");
            } else {
                logStatement.append(key + ":" + String.valueOf(request.getParameter(key)) + ",");
            }
        }

        logger.info("Asynchronous response from Oxigen : " + logStatement);
    }

    @RequestMapping(value = "denomination/{operatorId}/{circleId}/{rechargeAmount}/{rechargeType}/status", method = RequestMethod.GET)
    @NoSession
    @NoLogin
    public @ResponseBody
    ModelMap getRechargeAlertByOperatorAndAmount(@PathVariable final long operatorId,
                                                 @PathVariable final int circleId, @PathVariable final Float rechargeAmount,
                                                 @PathVariable final String rechargeType, final ModelMap model) throws InvalidParameterException {
        RechargeDenominationStat rechargeDenominationStat = rechargeMetricService
                .getRechargeMetricsBasedOnRechargeAmount(operatorId, circleId, rechargeAmount, rechargeType,
                        ProductMaster.ProductName.Mobile.getProductType());
        if (rechargeDenominationStat != null) {
            model.addAttribute("rechargeDenominationStat", rechargeDenominationStat);
        }
        if (rechargeDenominationStat.getSuccessRate() < 70) {
            String circle = operatorCircleService.getCircle(circleId).getName();
            String operator = operatorCircleService.getOperator((int) operatorId).getName();
            Integer amount = Math.round(rechargeAmount);
            metricsClient.recordEvent("Recharge.InvalidDenomination", operator + "." + circle, amount.toString());
        }
        return model;
    }

    @RequestMapping(value = "denomination/{operatorId}/{circleId}/{rechargeAmount}/{rechargeType}/{productType}/status", method = RequestMethod.GET)
    @NoSession
    @NoLogin
    public @ResponseBody
    ModelMap getRechargeAlertByOperatorAndAmountPerProduct(@PathVariable final long operatorId,
                                                           @PathVariable final int circleId, @PathVariable final Float rechargeAmount,
                                                           @PathVariable final String rechargeType, @PathVariable final String productType, final ModelMap model)
            throws InvalidParameterException {
        RechargeDenominationStat rechargeDenominationStat = rechargeMetricService
                .getRechargeMetricsBasedOnRechargeAmount(operatorId, circleId, rechargeAmount, rechargeType,
                        productType);
        if (rechargeDenominationStat != null) {
            model.addAttribute("rechargeDenominationStat", rechargeDenominationStat);
        }
        if (rechargeDenominationStat.getSuccessRate() < 70) {
            String circle = operatorCircleService.getCircle(circleId).getName();
            String operator = operatorCircleService.getOperator((int) operatorId).getName();
            Integer amount = Math.round(rechargeAmount);
            metricsClient.recordEvent("Recharge.InvalidDenomination", operator + "." + circle, amount.toString());
        }
        return model;
    }

    @RequestMapping(value = "schedule/{orderId}/execute", method = RequestMethod.POST)
    @NoSessionWrite
    public @ResponseBody
    Map<String, String> executeSchedule(@PathVariable final String orderId) {
        if (StringUtils.isBlank(orderId)) {
            return ErrorCode.INVALID_ORDER_ID.restResponse();
        }

        if (!userServiceProxy.validateOrderOwnership(orderId)) {
            return ErrorCode.INVALID_ORDER_ID.restResponse();
        }

        boolean executed = scheduleExecutor.executeNow(orderId);

        if (executed) {
            return ErrorCode.NO_ERROR.restResponse();
        }

        return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
    }

    private Map<String, String> failureResponse(String errorCode, String errorMessage) {
        Map<String, String> map = new HashMap<>();
        map.put(CALL_STATUS, FAILURE);
        if (errorCode != null && !errorCode.isEmpty()) {
            map.put(ERROR_CODE, errorCode);
        }
        if (errorMessage != null && !errorMessage.isEmpty()) {
            map.put(ERROR_MSG, errorMessage);
        }
        return map;
    }

    private ErrorCode validateRequest(RechargeRequest rechargeRequest, HttpServletRequest httpRequest) {
        if (rechargeRequest == null) {
            return ErrorCode.RECHARGE_REQUEST_NOT_PRESENT;
        }

        String serviceNumber = rechargeRequest.getServiceNumber();

        if (StringUtils.isBlank(serviceNumber)) {
            return ErrorCode.SERVICE_NUMBER_NOT_PRESENT;
        }

        if (!StringUtils.isNumeric(serviceNumber)) {
            return ErrorCode.INVALID_SERVICE_NUMBER_FORMAT;
        }

        if (StringUtils.isBlank(rechargeRequest.getProductType())) {
            return ErrorCode.PRODUCT_TYPE_NOT_PRESENT;
        }

        if (StringUtils.isBlank(rechargeRequest.getOperator())) {
            return ErrorCode.OPERATOR_NOT_PRESENT;
        }

        final ProductName productType = ProductName.fromCode(rechargeRequest.getProductType());

        if (!operatorCircleService.isValidOperator(productType.getProductId(), rechargeRequest.getOperator())) {
            return ErrorCode.OPERATOR_NOT_PRESENT;
        }

        switch (productType) {
            case Mobile:
                final ErrorCode isServiceNumberValid = ServiceNumberFormatValidator.validate(serviceNumber,
                        MOBILE_NUMBER_LENGTH);
                if (isServiceNumberValid != ErrorCode.NO_ERROR) {
                    return isServiceNumberValid;
                }

                if (!isNewMobileApp(httpRequest)) {
                    if (StringUtils.isBlank(rechargeRequest.getCircleName())) {
                        return ErrorCode.CIRCLE_NOT_PRESENT;
                    }
                }

                break;

            default:
                // TODO: Add validation for other products (DTH etc.,)

                break;
        }

        return AmountFormatValidator.validate(rechargeRequest.getAmount());
    }

    private boolean isErrorCodeSupported(HttpServletRequest request, int supportedAppVersion) {
        final int appVersion = MobileUtil.getMobileAppVersion(request);
        final boolean isAppChannel = MobileUtil.getMobileChannel(request) == 3;

        if (!isAppChannel) {
            return true;
        }

        Boolean isAppVersionSupported = appVersion >= supportedAppVersion;

        return isAppVersionSupported;
    }

    private boolean isNewMobileApp(HttpServletRequest request) {
        int version = MobileUtil.getMobileAppVersion(request);
        Boolean isNEWMobileAPP = MobileUtil.getMobileChannel(request) == 3 && version > 16;
        return isNEWMobileAPP ? true : false;
    }

    private Boolean checkBsnlSuccessfulBlockageCriteria(RechargeRequest rechargeRequest,
                                                        SuccessfulRechargeInfo previousRecharge) {
        if (rechargeRequest.getRechargePlanType() != null && previousRecharge.getPlanType() != null
                && rechargeRequest.getRechargePlanType().equalsIgnoreCase(previousRecharge.getPlanType())
                && rechargeRequest.getAmount() == previousRecharge.getAmount().getAmount().floatValue()) {
            return true;
        }
        return false;
    }

    private Boolean checkBsnlFailureBlockageCriteria(RechargeRequest rechargeRequest,
                                                     FailureRechargeInfo previousRecharge) {
        if (rechargeRequest.getRechargePlanType() != null && previousRecharge.getPlanType() != null
                && rechargeRequest.getRechargePlanType().equalsIgnoreCase(previousRecharge.getPlanType())
                && rechargeRequest.getAmount() == previousRecharge.getAmount().getAmount().floatValue()) {
            return true;
        }
        return false;
    }

    @Csrf(exclude = true)
    @NoLogin
    @NoSessionWrite
    @Authenticate(api = "autoPay.recharge")
    @RequestMapping(value = "autoPayRecharge", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    RechargeResponseWebDo doAutoPayRecharge(@RequestBody RechargeRequestWebDo rechargeRequestWebDo,
                                            HttpServletRequest request, HttpServletResponse response) {
        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        logger.info("Request for autopay received for mobileNumber: " + rechargeRequestWebDo.getMobileNumber() + " for amount: " + rechargeRequestWebDo.getAmount() + " "
                + " is: " + rechargeRequestWebDo.toString());
        try {
            if (rechargeRequestWebDo.getLastPaidDate() != null) {
                List<UserTransactionHistory> txnHistory = userTransactionHistoryService
                        .getSuccessfulTransactionHistoryByCombination(rechargeRequestWebDo.getUserId(),
                                rechargeRequestWebDo.getMobileNumber(),
                                rechargeRequestWebDo.getLastPaidDate().plusDays(1).toDate(), new LocalDate().toDate(),
                                rechargeRequestWebDo.getAmount().floatValue());
                if (txnHistory != null && txnHistory.size() > 0) {
                    logger.info("Success refill present with order Id :" + txnHistory.get(0).getOrderId());
                    rechargeResponseWebDo.setStatusCode(AutoPayAppService.SUCCESSFUL_REFILL_PRESENT);
                    try {
                        int latestTxnIndex = txnHistory.size() - 1;
                        UserTransactionHistory latestTransaction = txnHistory.get(latestTxnIndex);
                        rechargeResponseWebDo.setAutopayRefillTransactionDate(latestTransaction.getCreatedOn());
                    } catch (Exception e) {
                        logger.error("Exception caught is ", e);
                    }
                    return rechargeResponseWebDo;
                }
            }
            String errorCode = autoPayAppService.validateBalance(rechargeRequestWebDo.getAmount(), rechargeRequestWebDo.getUserId());
            if (errorCode != null) {
                rechargeResponseWebDo.setStatusCode(errorCode);
                logger.info("Error code received for validateBalance : " + errorCode + " for mobileNumber: " + rechargeRequestWebDo.getMobileNumber());
                return rechargeResponseWebDo;
            }

            request.setAttribute(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY,
                    String.valueOf(FCConstants.CHANNEL_ID_AUTO_PAY));
            sessionManager.generateMockSessionForAutopay(rechargeRequestWebDo.getUserId(), request, response);
            rechargeInitiateService.doTurboRecharge(rechargeRequestWebDo, rechargeResponseWebDo,
                    new BeanPropertyBindingResult(new Object(), ""), request, response);
            if (rechargeResponseWebDo != null && rechargeResponseWebDo.getOrderId() != null) {
                RechargeDetails details = userTransactionHistoryService
                        .getRechargeDetailsByOrderID(rechargeResponseWebDo.getOrderId());
                rechargeResponseWebDo.setStatusCode(details.getTransactionStatusCode());
            }
        } catch (Exception ex) {
            logger.error("Error doing autopay recharge for service number:" + rechargeRequestWebDo.getMobileNumber(),
                    ex);
            rechargeResponseWebDo.setStatusCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
        }
        logger.info("Transaction status for autopay request for mobileNumber: " + rechargeRequestWebDo.getMobileNumber() + " is: " + rechargeResponseWebDo.getStatusCode());
        return rechargeResponseWebDo;
    }

    @Csrf(exclude = true)
    @NoLogin
    @NoSessionWrite
    @Authenticate(api = "autoPay.recharge")
    @RequestMapping(value = "autoPay/{requestId}/status", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    RechargeResponseWebDo getAutoPayRequestStatus(@PathVariable String requestId) {
        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        String lookupId = autoPayAppService.getLookupIdFromRequestId(requestId);
        if (FCUtil.isEmpty(lookupId)) {
            logger.info(String.format("LookupId not generated for requestId=%s", requestId));
            rechargeResponseWebDo.setStatusCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
            return rechargeResponseWebDo;
        }
        String orderId = orderIdSlaveDAO.getOrderIdForLookUpId(lookupId);
        if (FCUtil.isEmpty(lookupId)) {
            logger.info(String.format("Order not generated for requestId=%s and lookupId = %s", requestId, lookupId));
            rechargeResponseWebDo.setStatusCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
            return rechargeResponseWebDo;
        }
        rechargeResponseWebDo.setOrderId(orderId);
        if (orderId != null) {
            RechargeDetails details = userTransactionHistoryService.getRechargeDetailsByOrderID(rechargeResponseWebDo.getOrderId());
            rechargeResponseWebDo.setStatusCode(details.getTransactionStatusCode());
        }
        return rechargeResponseWebDo;
    }

    @Csrf(exclude = true)
    @NoLogin
    @NoSessionWrite
    @Authenticate(api = "autoPay.recharge")
    @RequestMapping(value = "autoPayRecharge/{orderId}/status", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    RechargeResponseWebDo getAutoPayRechargeStatus(@PathVariable String orderId) {
        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        rechargeResponseWebDo.setOrderId(orderId);
        if (orderId != null) {
            RechargeDetails details = userTransactionHistoryService.getRechargeDetailsByOrderID(rechargeResponseWebDo.getOrderId());
            rechargeResponseWebDo.setStatusCode(details.getTransactionStatusCode());
        }
        return rechargeResponseWebDo;
    }

    @Csrf(exclude = true)
    @NoLogin
    @NoSessionWrite
    @Authenticate(api = "autoPay.recharge")
    @RequestMapping(value = "autoPayRecharge/getRechargeDetails/{orderId}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    RechargeDetails getRechargeMetadata(@PathVariable String orderId) {
        RechargeDetails details = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        Users user = userService.getUserFromOrderId(orderId);
        GetUserByIdRequest getuserbyidrequest = new GetUserByIdRequest();
        getuserbyidrequest.setUserId(String.valueOf(user.getUserId()));
        getuserbyidrequest.setUserAgent(null);
        getuserbyidrequest.setUserMachineIdentifier(null);
        getuserbyidrequest.setClientConfig(getClientConfigForIms());

        try {
            GetUserResponse userResponse = userClient.getUserById(getuserbyidrequest);
            details.setUserEmailId(userResponse.getUserDetails().getEmailId());
            details.setUserName(userResponse.getUserDetails().getFirstName());
        } catch (Exception e) {
            logger.error("Exception caught while getting userDetails from IMS for orderId: " + orderId, e);
            details.setUserEmailId(null);
            details.setUserName(null);
        }
        details.setUserMobileNumber(user.getMobileNo());
        return details;
    }


    @Csrf(exclude = true)
    @NoLogin
    @RequestMapping(value = "installReferer",method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    GoogleInstallRefererResponse installReferer(@RequestBody GoogleInstallRefererRequest googleInstallRefererRequest, HttpServletRequest httpRequest) {

        GoogleInstallRefererResponse googleInstallRefererResponse = null;

        try {
            String fcChannelString = httpRequest.getParameter("fcChannel");

            logger.info("Google Install Referer request to installReference Service: " + googleInstallRefererRequest);
            googleInstallRefererResponse = googlePostTxnService.postInstallReference(googleInstallRefererRequest, fcChannelString);
        } catch (Exception e){
            GoogleReError googleReError = new GoogleReError("GGLRE-113", "Something went wrong, Please try again later");
            googleInstallRefererResponse=new GoogleInstallRefererResponse();
            googleInstallRefererResponse.setError(googleReError);
            logger.error("Exception is ", e);
        }

        return googleInstallRefererResponse;

    }

    private String generateTaskId() {
        return UUID.randomUUID().toString() + new Timestamp(System.currentTimeMillis());
    }


}
