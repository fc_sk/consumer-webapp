package com.freecharge.rest.recharge;

public class FulfilmentConstants {
    public static String FULFILMENT_STATUS_SUCCESS_CODE      = "E-000";
    public static String FULFILMENT_STATUS_INVALID_INPUT     = "E-001";
    public static String FULFILMENT_STATUS_GENERIC_EXCEPTION = "E-009";
}
