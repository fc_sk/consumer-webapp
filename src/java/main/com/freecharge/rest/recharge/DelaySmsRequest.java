package com.freecharge.rest.recharge;

import java.io.Serializable;
import java.util.Map;

import com.freecharge.common.comm.fulfillment.UserDetails;

public class DelaySmsRequest implements Serializable{

	private Integer smsTemplateId;
	private String profileNumber;
	private String rechargeNumber;
	Map<String,String> templateDataMap;
	Boolean isSelfRecharge;
	private UserDetails userDetailsObj;
	
	public Integer getSmsTemplateId() {
		return smsTemplateId;
	}
	public void setSmsTemplateId(Integer smsTemplateId) {
		this.smsTemplateId = smsTemplateId;
	}
	public String getProfileNumber() {
		return profileNumber;
	}
	public void setProfileNumber(String profileNumber) {
		this.profileNumber = profileNumber;
	}
	public String getRechargeNumber() {
		return rechargeNumber;
	}
	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}
	public Map<String, String> getTemplateDataMap() {
		return templateDataMap;
	}
	public void setTemplateDataMap(Map<String, String> templateDataMap) {
		this.templateDataMap = templateDataMap;
	}
	public Boolean getIsSelfRecharge() {
		return isSelfRecharge;
	}
	public void setIsSelfRecharge(Boolean isSelfRecharge) {
		this.isSelfRecharge = isSelfRecharge;
	}
	public UserDetails getUserDetailsObj() {
		return userDetailsObj;
	}
	public void setUserDetailsObj(UserDetails userDetailsObj) {
		this.userDetailsObj = userDetailsObj;
	}
	
}
