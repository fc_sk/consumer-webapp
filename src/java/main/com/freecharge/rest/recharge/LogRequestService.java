/**
 * Sayak
 */
package com.freecharge.rest.recharge;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.CommonLogDetails;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.LoggingConstants;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;
import com.google.gson.Gson;

@Service
public class LogRequestService {

	@Autowired
	private FulfillmentScheduleService fulfillmentScheduleService;

	@Autowired
	private RechargeRetryConfigReadThroughCache rechargeRetryConfigCache;

	private static Logger logger = LoggingFactory.getLogger(LogRequestService.class);

	private static Logger validationLogger = LoggingFactory
			.getLogger(LoggingFactory.getTPTValidationLoggerName(LogRequestService.class.getName()));

	private static Logger scheduleInfoLogger = LoggingFactory
			.getLogger(LoggingFactory.getScheduleInfoLoggerName(LogRequestService.class.getName()));

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	public void logInTransactionValidation(Map<String, Object> inTransactionValidationDataMap) {
		try {
			CommonLogDetails validationDetails = FCUtil.createLoggingObject(LoggingConstants.TPT_VALIDATION_DATABASE,
					LoggingConstants.TPT_VALIDATION_TABLE, LoggingConstants.DBActionType.INSERT.toString(),
					inTransactionValidationDataMap);
			Gson gosGson = new Gson();
			String data = gosGson.toJson(validationDetails);
			logger.info("Data  logged is " + data);
			validationLogger.info(data);
		} catch (Exception e) {
			logger.error("Exception caught is ", e);
		}

	}

	public Map<String, Object> initValidationLogDataMap(String mobilNumber, Integer operatorId, Integer circleId,
			String aggrName, Integer callId) {
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put(LoggingConstants.OPERATOR_ID, operatorId);
		dataMap.put(LoggingConstants.CIRCLE_ID, circleId);
		dataMap.put(LoggingConstants.SERVICE_NUMBER, mobilNumber);
		dataMap.put(LoggingConstants.AGGR_NAME, aggrName);
		dataMap.put(LoggingConstants.CALL_ID, callId);
		dataMap.put(LoggingConstants.REQUEST_TIMESTAMP, sdf.format(new Date()));
		dataMap.put(LoggingConstants.RAW_REQUEST, null);
		dataMap.put(LoggingConstants.RAW_RESPONSE, null);
		dataMap.put(LoggingConstants.RESPONSE_CODE, null);
		dataMap.put(LoggingConstants.RESPONSE_TIMESTAMP, null);
		return dataMap;
	}

	public void cacheScheduleInfo(String orderId) {
		logger.info("Caching ScheduleInfo for orderId :" + orderId);
		try {
			FulfillmentScheduleDO fulfillmentScheduleDO = fulfillmentScheduleService.getScheduledRecharge(orderId);
			if (fulfillmentScheduleDO != null) {
				rechargeRetryConfigCache.setScheduleInfo(fulfillmentScheduleDO, orderId);
			}
		} catch (Exception e) {
			logger.error("Exception caught in caching ScheduleInfo ", e);
		}

	}

	public void logScheduleInfoIfPresent(String orderId, InResponse inResponse, Integer operatorId, Integer circleId,
			String mobileNo) {
		try {
			FulfillmentScheduleDO fulfillmentScheduleDO = rechargeRetryConfigCache.getScheduleInfo(orderId);
			if (fulfillmentScheduleDO != null) {
				rechargeRetryConfigCache.deleteScheduleInfo(orderId);
				Map<String, Object> dataMap = initScheduleInfoDataMap(fulfillmentScheduleDO, inResponse, orderId,
						operatorId, circleId, mobileNo);
				logAttemptStatusAfterSchedule(dataMap);
			}
		} catch (Exception e) {
			logger.error("Caught exception is ", e);
		}

	}

	private void logAttemptStatusAfterSchedule(Map<String, Object> dataMap) {
		try {
			CommonLogDetails scheduleDetails = FCUtil.createLoggingObject(
					LoggingConstants.SCHEDULE_INFO_LOGGER_DATABASE, LoggingConstants.SCHEDULE_INFO_LOGGER_TABLE,
					LoggingConstants.DBActionType.INSERT.toString(), dataMap);
			Gson gosGson = new Gson();
			String data = gosGson.toJson(scheduleDetails);
			logger.info("Data  logged is " + data);
			scheduleInfoLogger.info(data);
		} catch (Exception e) {
			logger.error("Exception caught is ", e);
		}

	}

	private Map<String, Object> initScheduleInfoDataMap(FulfillmentScheduleDO fulfillmentScheduleDO,
			InResponse inResponse, String orderId, Integer operatorId, Integer circleId, String mobileNo) {
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put(LoggingConstants.ORDER_ID, orderId);
		dataMap.put(LoggingConstants.SCHEDULE_CREATION_TS, sdf.format(fulfillmentScheduleDO.getCreatedTime()));
		dataMap.put(LoggingConstants.RETRY_TS, sdf.format(new Date()));
		dataMap.put(LoggingConstants.AG_NAME, inResponse.getAggrName());
		dataMap.put(LoggingConstants.OPERATOR_ID, operatorId);
		dataMap.put(LoggingConstants.CIRCLE_ID, circleId);
		dataMap.put(LoggingConstants.SERVICE_NUMBER, mobileNo);
		dataMap.put(LoggingConstants.STATUS, inResponse.getAggrRespCode());
		return dataMap;
	}

}
