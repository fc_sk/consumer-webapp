package com.freecharge.rest.recharge;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidParametersException;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.mobile.service.MobileOperatorCircleService;
import com.freecharge.mobile.web.view.OperatorCircle;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSession;

@Controller
@RequestMapping("/rest/*")
public class AutoSuggestResourceController extends AbstractRestController {
	@Autowired
	private MobileOperatorCircleService mobileOperatorCircleService;
	
    @Autowired
    private OperatorAlertService alertService;
    
	/**
	 * Getting Operator circle by rechargeTypeName and number
	 * @param rechargeType
	 * @param number
	 * @param model
	 * @return
	 * @throws InvalidRestParametersException
	 */
    @NoSession
    @NoLogin
	@RequestMapping(value="/autosuggest/circles/{rechargeType}/{number}", method=RequestMethod.GET)
	public @ResponseBody OperatorCircle getOperatorsForNumber(@PathVariable String rechargeType,@PathVariable String number, ModelMap model) throws InvalidParametersException {	
		validateParameters(rechargeType, number);					
		String rechargeTypeId = String.valueOf(ProductMaster.ProductName.valueOf(rechargeType).getProductId());
			
		OperatorCircleBusinessDO operatorCicleBDO = new OperatorCircleBusinessDO();
		operatorCicleBDO.setPrefix(number);
		operatorCicleBDO.setProductType(rechargeTypeId);	
		final OperatorCircle operatorBean = mobileOperatorCircleService.getOperatorCircle(operatorCicleBDO);
		
		operatorBean.setIsOperatorUp(Boolean.TRUE);
		
		String operatorId = operatorBean.getOperatorId();
		
		if (StringUtils.isNotBlank(operatorId)) {
		    if (StringUtils.isNumeric(operatorId)) {
		        if (alertService.isOperatorInAlert(Integer.parseInt(operatorId))) {
		            operatorBean.setIsOperatorUp(Boolean.FALSE);
		        }
		    }
		}
		
        return operatorBean;
	}
		
	private void validateParameters(String rechargeType, String number) throws InvalidParametersException {
		List<InvalidParameterException> invalidRestParameterExceptionList = new ArrayList<InvalidParameterException>();
			
		if (rechargeType == null) {
			invalidRestParameterExceptionList.add(new InvalidParameterException("rechargeType", rechargeType,"Recharge type is required", ValidationErrorCode.REQUIRED_PRODUCT_MASTER_ID));
		} else if (!(ProductMaster.ProductName.isValidProductName(rechargeType))) {
			invalidRestParameterExceptionList.add(new InvalidParameterException("rechargeType", rechargeType,"Valid recharge type is required", ValidationErrorCode.INVALID_PRODUCT_MASTER_ID));
		}
			
		if (number != null) {
			if (!(StringUtils.isNumeric(number))) {
				invalidRestParameterExceptionList.add(new InvalidParameterException("number", number, "Number field should be numeric", ValidationErrorCode.INVALID_NUMBER_FORMAT));				
			} 	
		}
		    
		if (invalidRestParameterExceptionList.size() > 0) {
			throw new InvalidParametersException(invalidRestParameterExceptionList);
		}
	}	
}
