package com.freecharge.rest.recharge;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/operator/*")
public class OperatorAlertController {

    @Autowired
    private OperatorAlertService operatorAlertService;

    @NoSessionWrite
    // TODO: This should be excluded from session altogether.
    @NoLogin
    @RequestMapping(value = "{operatorId}/{circle}/status", method = RequestMethod.GET)
    public @ResponseBody
    ModelMap getOperatorAlertById(@PathVariable final int operatorId, @PathVariable final String circle, final ModelMap model)
            throws InvalidParameterException {
        
        List<OperatorAlert> operatorAlerts = operatorAlertService.processAndGetOperatorAlert(operatorId, circle);
        if (!FCUtil.isEmpty(operatorAlerts)) {
            model.addAttribute("operatorAlert", operatorAlerts.get(0));
        }
        return model;
    }

    @NoSessionWrite
    @NoLogin
    @RequestMapping(value = "all/status", method = RequestMethod.GET)
    public @ResponseBody
    List<OperatorAlert> getAllOperatorAlert(final ModelMap model) throws InvalidParameterException {
        List<OperatorAlert> allOperatorAlertsList = operatorAlertService.getAllOperatorAlerts();

        if (allOperatorAlertsList.size() == 0 || allOperatorAlertsList == null) {
            model.addAttribute("noDowntime", "No Downtime for any operator");
        } else {
            model.addAttribute("operatorAlertList", allOperatorAlertsList);
        }
        return allOperatorAlertsList;
    }
}
