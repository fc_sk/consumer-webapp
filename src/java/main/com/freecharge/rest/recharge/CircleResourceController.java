package com.freecharge.rest.recharge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCircleMaster;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/*")
public class CircleResourceController extends AbstractRestController {
    @Autowired 
    private CommonService commonService;
    
	private OperatorCircleService operatorCircleService;
	@Autowired
	public void setOperatorCircleService(OperatorCircleService operatorCircleService) {
		this.operatorCircleService = operatorCircleService;
	}
	
	/**
	 * Getting all circles
	 */
	@NoLogin
	@RequestMapping(value="/circles", method=RequestMethod.GET)
	public @ResponseBody Map<String, List<CircleMaster>> getAllCircles(ModelMap model) throws InvalidPathValueException  {
	    Map<String, List<CircleMaster>> circleMap = new HashMap<String, List<CircleMaster>>(); 
	    circleMap.put("circles", operatorCircleService.getAllCircles());
		return circleMap;
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/circles/Bill", method=RequestMethod.GET)
    public @ResponseBody Map<String, List<BillPayCircleMaster>> getAllCirclesForBill() {
        List<BillPayCircleMaster> billPayOperatorList = commonService.getBillPayCircleMasterList();
        Map<String, List<BillPayCircleMaster>> operatorMap = new HashMap<String, List<BillPayCircleMaster>>(); 
        operatorMap.put("circles", billPayOperatorList);
        return operatorMap;
    }
}
