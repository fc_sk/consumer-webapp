package com.freecharge.rest.recharge;

import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.dao.RemindRechargeDAO;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.rechargeplans.RechargePlanBusinessDO;

@Component
public class RechargePattern {

	private static final Logger LOGGER = LoggerFactory.getLogger(RechargePattern.class);

	@Autowired
	UserTransactionHistoryService transactionHistoryService;

	@Autowired
	RemindRechargeDAO remindRechargeDAO;

	@Autowired
	RechargePlanService rechargePlanService;

	public class PatternData {
		private Integer mean, standardDev;
		private float initialMean;
		private double initialStandardDev;
		private boolean isSameAmount;
		private Integer[] diffArr;
		private float percent;
		private Integer[] valuesConsidered;

		public double getInitialStandardDev() {
			return initialStandardDev;
		}

		public void setInitialStandardDev(double initialStandardDev) {
			this.initialStandardDev = initialStandardDev;
		}

		public float getInitialMean() {
			return initialMean;
		}

		public void setInitialMean(float initialMean) {
			this.initialMean = initialMean;
		}

		public Integer[] getDiffArr() {
			return diffArr;
		}

		public void setDiffArr(Integer[] diffArr) {
			this.diffArr = diffArr;
		}

		public Integer getMean() {
			return mean;
		}

		public void setMean(Integer mean) {
			this.mean = mean;
		}

		public Integer getStandardDev() {
			return standardDev;
		}

		public void setStandardDev(Integer standardDev) {
			this.standardDev = standardDev;
		}

		public boolean isSameAmount() {
			return isSameAmount;
		}

		public void setSameAmount(boolean isSameAmount) {
			this.isSameAmount = isSameAmount;
		}

		public float getPercent() {
			return percent;
		}

		public void setPercent(float percent) {
			this.percent = percent;
		}

		public Integer[] getValuesConsidered() {
			return valuesConsidered;
		}

		public void setValuesConsidered(Integer[] valuesConsidered) {
			this.valuesConsidered = valuesConsidered;
		}

	}

	// Week, Weeks, Day, Days, Month, Months

	public void getNextRechargeDate(RemindRecharge remindRecharge,List<CartItemsBusinessDO> cartItemsBusinessDOs, Integer operatorId, Integer circleId,
			Float rechargeAmount, Integer product) {
		boolean isSpecial = false;
		for(CartItemsBusinessDO  cartItemsBusinessDO : cartItemsBusinessDOs){
			if(cartItemsBusinessDO.getEntityId() != null && cartItemsBusinessDO.getEntityId().equalsIgnoreCase(FCConstants.RECHARGE_TYPE_SPECIAL)){
				isSpecial = true;
				break;
			}
		}
		List<RechargePlanBusinessDO> rechargePlans = rechargePlanService
				.getRechargePlan(circleId, operatorId, product, rechargeAmount);
		if (rechargePlans != null) {
			for (RechargePlanBusinessDO planBusinessDO : rechargePlans) {
				if (rechargePlanService.SPECIAL_OPERATOR_IDS.contains(operatorId) && 
						isSpecial && planBusinessDO.getName() != null && planBusinessDO.getName().equals("Topup")) {
					continue;
				}
				String validity = planBusinessDO.getValidity();
				Pattern dayPattern = Pattern
						.compile("(.*?)(\\d{1,})(.*[[Ww]eeks?|[Dd]ays?|[Mm]onths?].*)");
				if (validity == null || dayPattern == null) {
				    return;
				}
				Matcher matcher = dayPattern.matcher(validity);
				try {
					if (matcher.matches()) {
						String validityIn = matcher.group(3).trim();
						Integer validityDays = null;
						if (validityIn.contains("week")
								|| validityIn.contains("Week")) {
							validityDays = Integer.parseInt(matcher.group(2)) * 7;
						} else if (validityIn.contains("Day")
								|| validityIn.contains("day")) {
							validityDays = Integer.parseInt(matcher.group(2));
						} else if (validityIn.contains("Month")
								|| validityIn.contains("month")) {
							validityDays = Integer.parseInt(matcher.group(2)) * 30;
						}

						if (validityDays != null && validityDays >= 1) {
							Calendar calendar = Calendar.getInstance();
							calendar.add(Calendar.DATE, validityDays);
							remindRecharge.setRemindOn(calendar.getTime());
							remindRecharge.setPlanId(planBusinessDO.getRechargePlanId());
							return;
						}
					}
				} catch (Exception exception) {
					LOGGER.error(
							"Exception determing the next recharge date using plan validity",
							exception);
				}

			}
		}
	}
}
