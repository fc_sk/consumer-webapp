package com.freecharge.rest.recharge;

import java.sql.Timestamp;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FulfilmentResponse extends AbstractFulfilmentRestResponse {
    public FulfilmentResponse(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    @JsonProperty("fulfilment_status")
    private FulfilmentStatus fulfilmentStatus;
    @JsonProperty("transaction_id")
    private Integer          transactionId;
    @JsonProperty("order_id")
    private String           orderId;
    @JsonProperty("fk_user_id")
    private Integer          fkUserId;
    @JsonProperty("transaction_amount")
    private Double           transactionAmount;
    @JsonProperty("subscriber_identification_number")
    private String           subscriberIdentificationNumber;
    @JsonProperty("product_type")
    private String           productType;
    @JsonProperty("service_provider")
    private String           serviceProvider;
    @JsonProperty("service_region")
    private String           serviceRegion;
    @JsonProperty("transaction_status")
    private String           transactionStatus;
    @JsonProperty("created_on")
    private Timestamp        createdOn;
    @JsonProperty("last_updated")
    private Timestamp        lastUpdated;
    @JsonProperty("metadata")
    private String           metadata;
    @JsonProperty("fulfilment_attempt_time")
    private Timestamp        fulfilmentAttemptTime;
    @JsonProperty("mtxn_id")
    private String           mtxnId;
    
    @JsonProperty("mob_merchant_id")
    private String mobMerchantId;
    
    public String getMobMerchantId() {
		return mobMerchantId;
	}

	public void setMobMerchantId(String mobMerchantId) {
		this.mobMerchantId = mobMerchantId;
	}

	public FulfilmentStatus getFulfilmentStatus() {
        return fulfilmentStatus;
    }

    public void setFulfilmentStatus(FulfilmentStatus fulfilmentStatus) {
        this.fulfilmentStatus = fulfilmentStatus;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(Integer fkUserId) {
        this.fkUserId = fkUserId;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getSubscriberIdentificationNumber() {
        return subscriberIdentificationNumber;
    }

    public void setSubscriberIdentificationNumber(String subscriberIdentificationNumber) {
        this.subscriberIdentificationNumber = subscriberIdentificationNumber;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getServiceRegion() {
        return serviceRegion;
    }

    public void setServiceRegion(String serviceRegion) {
        this.serviceRegion = serviceRegion;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Timestamp getFulfilmentAttemptTime() {
        return fulfilmentAttemptTime;
    }

    public void setFulfilmentAttemptTime(Timestamp fulfilmentAttemptTime) {
        this.fulfilmentAttemptTime = fulfilmentAttemptTime;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }
}
