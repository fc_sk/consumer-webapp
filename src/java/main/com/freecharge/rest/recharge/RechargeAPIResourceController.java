package com.freecharge.rest.recharge;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.businessDo.RechargeResponseWebDo;
import com.freecharge.recharge.businessDo.RechargeStatusRequestWebDo;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.web.interceptor.annotation.Authenticate;

@Controller
@RequestMapping("/rest/api/*")
public class RechargeAPIResourceController {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;

    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private RechargeInitiateService rechargeInitiateService;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private Environment env;

    private static final String INVALID_REQUEST       = "1000";
    private static final String RECHARGE_API_DISABLED = "1001";

    @RequestMapping(value = "recharge/status", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody RechargeResponseWebDo doRechargeStatusCheck(RechargeStatusRequestWebDo rechargeStatusRequest,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
                    throws InvalidPathValueException {

        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        if (!appConfig.isRechargeApiEnabled()) {
            rechargeResponseWebDo.setStatusCode(RECHARGE_API_DISABLED);
            return rechargeResponseWebDo;
        }
        String orderId = rechargeStatusRequest.getFreechargeTransactionId();
        if (FCUtil.isEmpty(orderId)) {
            rechargeResponseWebDo.setStatusCode(INVALID_REQUEST);
            return rechargeResponseWebDo;
        }
        Integer loggedInUserId = userServiceProxy.getLoggedInUser().getUserId();
        Integer orderOwner = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
        if (orderOwner == null || loggedInUserId.intValue() != orderOwner.intValue()) {
            rechargeResponseWebDo.setStatusCode(INVALID_REQUEST);
            return rechargeResponseWebDo;
        }
        UserTransactionHistory userTxnHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId);
        String status = RechargeConstants.IN_UNDER_PROCESS_CODE;
        String createdOn = "";
        if (userTxnHistory != null) {
            status = userTxnHistory.getTransactionStatus();
            createdOn = userTxnHistory.getCreatedOn().toString();
        }
        rechargeResponseWebDo.setStatusCode(status);
        rechargeResponseWebDo.setFreechargeTransactionId(orderId);
        rechargeResponseWebDo.setApiVersion(rechargeStatusRequest.getApiVersion());
        rechargeResponseWebDo.setOperatorTransactionId("");
        rechargeResponseWebDo.setRequestId(UUID.randomUUID().toString());
        rechargeResponseWebDo.setTransactionDate(createdOn);
        return rechargeResponseWebDo;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "recharge", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody RechargeResponseWebDo doRecharge(RechargeRequestWebDo rechargeRequestWebDo,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
                    throws InvalidPathValueException {
        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        if (!appConfig.isRechargeApiEnabled()) {
            rechargeResponseWebDo.setStatusCode(RECHARGE_API_DISABLED);
            logger.error("Recharge API is disabled in appConfig for request" + String.format(request.toString()));
            return rechargeResponseWebDo;
        }

        rechargeInitiateService.doTurboRecharge(rechargeRequestWebDo, rechargeResponseWebDo, bindingResult, request,
                response);
        return rechargeResponseWebDo;
    }

   
}
