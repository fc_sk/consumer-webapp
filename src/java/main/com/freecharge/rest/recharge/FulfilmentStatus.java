package com.freecharge.rest.recharge;

public enum FulfilmentStatus {
    PENDING, FAILED, SUCCESS, NOT_ATTEMPTED, UNKNOWN
}
