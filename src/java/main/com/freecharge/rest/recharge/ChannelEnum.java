package com.freecharge.rest.recharge;

/**
 * @author bhaskerghosh
 *
 */
public enum ChannelEnum {
	ANDROID("android"),
	APPLE("apple"),
	WINDOW("window");
	
	private String val;
	
	ChannelEnum(String val) {
		this.val = val;
	}
	
	public String getValue() {
		return val;
	}
}
