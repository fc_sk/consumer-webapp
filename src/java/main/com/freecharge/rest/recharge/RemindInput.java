package com.freecharge.rest.recharge;

public class RemindInput {
	private String orderId;

	private String remindInDays;

	private String action;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRemindInDays() {
		return remindInDays;
	}

	public void setRemindInDays(String remindInDays) {
		this.remindInDays = remindInDays;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}