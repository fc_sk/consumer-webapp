package com.freecharge.rest.recharge;

public class TxnHomePageBO {
    private final String cartId;
    private final String homePageId;
    
    public TxnHomePageBO(String cartId, String homePageId) {
        this.cartId = cartId;
        this.homePageId = homePageId;
    }
    
    public String getCartId() {
        return cartId;
    }
    
    public String getHomePageId() {
        return homePageId;
    }
}
