package com.freecharge.rest.recharge;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;

@Service
public class FulfilmentResourceService {

    @Autowired
    PaymentTransactionService     paymentTransactionService;

    @Autowired
    UserTransactionHistoryService transactionHistoryService;

    @Autowired
    InService                     inService;

    @Autowired
    private BillPaymentService    billPaymentService;

    @Autowired
    private OrderIdSlaveDAO       orderIdDao;

    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService       billPayServiceRemote;
    
    @Autowired
    private RechargeRetryConfigReadThroughCache rechargeCache;
    
    @Autowired
    private FCProperties                  fcProperties;
    
    private static Logger  logger         = LoggingFactory.getLogger(FulfilmentResourceService.class);

    private class FulfilmentData {
        private FulfilmentStatus fulfilmentStatus;
        private Timestamp        fulfilmentAttemptTime;

		public FulfilmentData(final FulfilmentStatus fulfilmentStatus, final Timestamp fulfilmentAttemptTime) {
            this.fulfilmentStatus = fulfilmentStatus;
            this.fulfilmentAttemptTime = fulfilmentAttemptTime;
        }

        public FulfilmentStatus getFulfilmentStatus() {
            return fulfilmentStatus;
        }

        public Timestamp getFulfilmentAttemptTime() {
            return fulfilmentAttemptTime;
        }
        
        public void setFulfilmentStatus(final FulfilmentStatus fulfilmentStatus){
        	this.fulfilmentStatus = fulfilmentStatus;
        }
    }

    public FulfilmentResponse getFulfilmentResponseForOrder(String orderId) {
        UserTransactionHistory uth = transactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
        if (uth == null) {
            return invalidRequestResponse();
        }
        String mtxnId = paymentTransactionService.getPaymentFulfilmentMtxnFromOrder(orderId);
        FulfilmentData fulfilmentData = getFinalFulfilmentStatusForOrder(orderId, uth.getProductType());
        String mobMerchantId = null;
        if (FCUtil.isUtilityPaymentType(uth.getProductType())) {
        	boolean lobKey = rechargeCache.getLOBKey(orderId);
        	if(lobKey){
        		mobMerchantId = fcProperties.getProperty(FCConstants.BILLDESK_MERCHANT_ID);
        	}
        }
        return createFulfilmentResponse(uth, fulfilmentData, mtxnId, mobMerchantId);
    }

    public FulfilmentResponse getFulfilmentResponseForMtxn(String mtxnId) {
        String orderId = paymentTransactionService.getOrderIdFromMtxn(mtxnId);
        if (orderId == null) {
            return invalidRequestResponse();
        }
        UserTransactionHistory uth = transactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
        if (uth == null) {
            return invalidRequestResponse();
        }
        String orderForMtxn = paymentTransactionService.getPaymentFulfilmentOrderFromMtxn(mtxnId);
        FulfilmentData fulfilmentData = null;
        if (FCUtil.isEmpty(orderForMtxn)) {
            fulfilmentData = new FulfilmentData(FulfilmentStatus.NOT_ATTEMPTED, null);
        } else {
            fulfilmentData = getFinalFulfilmentStatusForOrder(orderId, uth.getProductType());
        }
        sanitizeFulfillmentResponse(uth,fulfilmentData);
        String mobMerchantId = null;
        if (FCUtil.isUtilityPaymentType(uth.getProductType())) {
        	boolean lobKey = rechargeCache.getLOBKey(orderId);
        	if(lobKey){
        		mobMerchantId = fcProperties.getProperty(FCConstants.BILLDESK_MERCHANT_ID);
        	}
        }
        return createFulfilmentResponse(uth, fulfilmentData, mtxnId, mobMerchantId);
    }

	private void sanitizeFulfillmentResponse(UserTransactionHistory uth, FulfilmentData fulfilmentData) {
		if (uth != null && uth.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND)) {			
			fulfilmentData.setFulfilmentStatus(FulfilmentStatus.FAILED);			
		}
		
		if(uth != null && (PaymentConstants.FULFILLMENT_UNKNOWN_STATUS.equals(uth.getTransactionStatus())
				|| PaymentConstants.FULFILLMENT_PENDING_STATUS.equals(uth.getTransactionStatus()))) {
			fulfilmentData.setFulfilmentStatus(FulfilmentStatus.PENDING);			
		}

	}

	private FulfilmentResponse invalidRequestResponse() {
        return new FulfilmentResponse(FulfilmentConstants.FULFILMENT_STATUS_INVALID_INPUT,
                "Please provide a valid input");
    }

    private FulfilmentResponse createFulfilmentResponse(UserTransactionHistory uth, FulfilmentData fulfilmentData,
            String mtxnId, String mobMerchantId) {
        FulfilmentResponse fulfilmentResponse = new FulfilmentResponse(
                FulfilmentConstants.FULFILMENT_STATUS_SUCCESS_CODE, "");
        fulfilmentResponse.setCreatedOn(uth.getCreatedOn());
        fulfilmentResponse.setLastUpdated(uth.getLastUpdated());
        fulfilmentResponse.setFkUserId(uth.getFkUserId());
        fulfilmentResponse.setFulfilmentAttemptTime(fulfilmentData.getFulfilmentAttemptTime());
        logger.info("FF status "+fulfilmentData.getFulfilmentStatus());
        if(uth != null && (PaymentConstants.FULFILLMENT_UNKNOWN_STATUS.equals(uth.getTransactionStatus())
				|| PaymentConstants.FULFILLMENT_PENDING_STATUS.equals(uth.getTransactionStatus()))) {
			fulfilmentData.setFulfilmentStatus(FulfilmentStatus.PENDING);			
		}
        logger.info("FF status # "+fulfilmentData.getFulfilmentStatus());
        fulfilmentResponse.setFulfilmentStatus(fulfilmentData.getFulfilmentStatus());
        fulfilmentResponse.setMetadata(uth.getMetadata());
        fulfilmentResponse.setMtxnId(mtxnId);
        fulfilmentResponse.setOrderId(uth.getOrderId());
        fulfilmentResponse.setProductType(uth.getProductType());
        fulfilmentResponse.setServiceProvider(uth.getServiceProvider());
        fulfilmentResponse.setServiceRegion(uth.getServiceRegion());
        fulfilmentResponse.setSubscriberIdentificationNumber(uth.getSubscriberIdentificationNumber());
        fulfilmentResponse.setTransactionAmount(uth.getTransactionAmount());
        fulfilmentResponse.setTransactionId(uth.getTransactionId());
        fulfilmentResponse.setTransactionStatus(uth.getTransactionStatus());
        fulfilmentResponse.setMobMerchantId(mobMerchantId);
        return fulfilmentResponse;
    }

    private FulfilmentData getFinalFulfilmentStatusForOrder(String orderId, String productType) {
        if (FCUtil.isRechargeProductType(productType)) {
            InResponse inResp = inService.getInResponseByOrderID(orderId);
            if (inResp == null) {
                return new FulfilmentData(FulfilmentStatus.NOT_ATTEMPTED, null);
            }
            FulfilmentStatus fulfilmentStatus = inService.getFulfilmentStatus(inResp.getInRespCode());
            if (fulfilmentStatus == FulfilmentStatus.FAILED) {
                BillPaymentStatus bps = billPaymentService.findBillPaymentStatusByOrderId(orderId);
                if (bps == null) {
                    return new FulfilmentData(FulfilmentStatus.FAILED, inResp.getCreatedTime());
                } else {
                    String responseCode = bps.getSuccess() == null ? RechargeConstants.IN_UNDER_PROCESS_CODE : (bps
                            .getSuccess() == true ? "00" : "55");
                    return new FulfilmentData(inService.getFulfilmentStatus(responseCode), bps.getCreatedAt());
                }
            } else {
                return new FulfilmentData(fulfilmentStatus, inResp.getCreatedTime());
            }
        } else if (FCUtil.isBillPostpaidType(productType)) {
            BillPaymentStatus bps = billPaymentService.findBillPaymentStatusByOrderId(orderId);
            if (bps == null) {
                return new FulfilmentData(FulfilmentStatus.NOT_ATTEMPTED, null);
            }
            String responseCode = bps.getSuccess() == null ? RechargeConstants.IN_UNDER_PROCESS_CODE : (bps
                    .getSuccess() == true ? "00" : "55");
            FulfilmentStatus fulfilmentStatus = inService.getFulfilmentStatus(responseCode);
            if (fulfilmentStatus == FulfilmentStatus.FAILED) {
                InResponse inResp = inService.getInResponseByOrderID(orderId);
                if (inResp == null) {
                    return new FulfilmentData(FulfilmentStatus.FAILED, bps.getCreatedAt());
                } else {
                    return new FulfilmentData(inService.getFulfilmentStatus(inResp.getInRespCode()),
                            inResp.getCreatedTime());
                }
            } else {
                return new FulfilmentData(fulfilmentStatus, bps.getCreatedAt());
            }
        } else if (FCUtil.isUtilityPaymentType(productType)) {
			String billId = null;
			try {
				billId = orderIdDao.getBillIdForOrderId(orderId, productType);
			} catch (Exception e) {
				logger.error("An exception occurred trying to get the bill id from order id : " + orderId);
			}
            BillTransaction billTransaction = billPayServiceRemote.getBillTransaction(billId);
            if (billTransaction == null || "CREATED".equals(billTransaction.getTransactionStatus())) {
                return new FulfilmentData(FulfilmentStatus.NOT_ATTEMPTED, null);
            }
            return new FulfilmentData(inService.getFulfilmentStatus(billTransaction.getTransactionStatus()),
                    billTransaction.getUpdatedOn());
        }
        return new FulfilmentData(FulfilmentStatus.UNKNOWN, null);
    }

    public FulfilmentAmount getFulfilmentAmountForMtxn(String mtxnId) {
        // TODO Auto-generated method stub
        return null;
    }
}
