package com.freecharge.rest.recharge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.recharge.businessDo.AggregatorBalanceStatus;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.services.OxigenService;
import com.freecharge.rest.AbstractRestController;

@Controller
@RequestMapping("/protected/rest/*")
public class AggregatorAdminResourceController extends AbstractRestController {
    @Autowired
    private OxigenService oxigenService;
    
    @Autowired
    private EuronetService euronetService;

    @NoLogin
    @RequestMapping(value="/aggregator/oxigen/balance/get", method=RequestMethod.GET)
    public @ResponseBody String getOxigenWalletBalance() throws Exception {
        return oxigenService.getBalanceFromCache().toString();
    }
    
    @NoLogin
    @RequestMapping(value="/aggregator/oxigen/balance/status", method=RequestMethod.GET)
    public @ResponseBody AggregatorBalanceStatus getOxigenWalletBalanceStatus() throws Exception {
        return oxigenService.getBalanceStatus();
    }

    @NoLogin
    @RequestMapping(value="/aggregator/euronet/balance/get", method=RequestMethod.GET)
    public @ResponseBody String getEuronetWalletBalance() throws Exception {
        return euronetService.getBalanceFromCache().toString();
    }
    
    @NoLogin
    @RequestMapping(value="/aggregator/euronet/balance/status", method=RequestMethod.GET)
    public @ResponseBody AggregatorBalanceStatus getEuronetWalletBalanceStatus() throws Exception {
        return euronetService.getBalanceStatus();
    }
}