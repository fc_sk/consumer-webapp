package com.freecharge.rest.recharge;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.cartcheckout.CreateCartTaskRequest;
import com.freecharge.cartcheckout.constants;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.client.CreateCartRequest;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.payments.ts.TaskScheduler;
import com.snapdeal.payments.ts.dto.TaskDTO;
import com.snapdeal.payments.ts.exception.DuplicateTaskException;
import com.snapdeal.payments.ts.exception.InvalidTaskTypeException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.service.helper.SessionHelper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.BaseValidationService;
import com.freecharge.web.controller.HomeController;
import com.google.common.collect.ImmutableMap;

@Controller
@RequestMapping("/rest/recharge/credits/*")
public class CreditRechargeController extends AbstractRestController {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private MetricsClient metricsClient;

	@Autowired
	UserServiceProxy userServiceProxy;

	@Autowired
	private TxnHomePageService txnHomePageService;

	@Autowired
	private Tracker tracker;

	@Autowired
	private SessionHelper sessionHelper;
	
    @Autowired
    private OperatorCircleService operatorCircleService;
    
    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    private BaseValidationService baseValidationService;
    
    @Autowired
    private RechargeValidationHelper validationHelper;

	@Autowired
	private TaskScheduler taskScheduler;

	@Autowired
	CartCheckoutServiceClientImpl cartClient;
    
    
    private final String              CALL_STATUS                           = "callStatus";
    private final String              FAILURE                               = "failure";
    private final String              SUCCESS                               = "success";
    private final String              ERROR_CODE                            = "errorCode";
    private final String              ERROR_MSG                            = "errorMessage";
    private final String              LOOKUP_ID                             = "lookupId";
    private final String              HOME_PAGE_ID                          = "txnHomePgID";

	/**
	 * Credit save call
	 * @param rechargeRequest
	 * @param bindingResult
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> saveCredits(RechargeRequest rechargeRequest, BindingResult bindingResult,
			HttpServletRequest request) {
		logger.info("Request comes inside the save credits method");
		Map<String, String> errorResult = validateCreditRequest(rechargeRequest, bindingResult, request);

		if (errorResult != null && !errorResult.isEmpty()) {
			return errorResult;
		}

		final String serviceNumber = rechargeRequest.getServiceNumber();
		final String productType = rechargeRequest.getProductType();
		final Float rechargeAmount = rechargeRequest.getAmount();

		final String operator = rechargeRequest.getOperator();
		final String circleName = rechargeRequest.getCircleName();
		final String planType = rechargeRequest.getRechargePlanType();
		int circleId = -1;
		try {
			circleId = Integer.parseInt(circleName);
		} catch (Exception e) {
			// Sometimes, circleName may come as 'ALL' from UI, in this case
			// parsing fails. And circleId remains as -1, which is wht we want
		}
		try {
			Users loggedInUser = userServiceProxy.getLoggedInUser();
			TxnHomePageBusinessDO thpDO = createTxnHomePageBusinessDO(serviceNumber, productType, rechargeAmount,
					operator, circleName, planType);
			
			logger.info("About to create log for lookupid: " + thpDO.getLookupId());
			TxnHomePageBO homePageBO = txnHomePageService.saveInfoAndCreateCart(thpDO,
					loggedInUser.getUserId().toString(), loggedInUser.getEmail(), planType, rechargeRequest.getPlanId(), rechargeRequest.getPlanCategory());

			sessionHelper.saveDataInSession(thpDO);

			saveInCampaignService(serviceNumber, productType, rechargeAmount, operator, circleId, thpDO.getSessionId(),
					thpDO.getLookupId());

			CreateCartRequest createCartRequest = new CreateCartTaskRequest();
			createCartRequest.setCategoryName(FCConstants.productTypeToNameMap.get(productType));
			createCartRequest.setLookupId(thpDO.getLookupId());
			createCartRequest.setServiceNumber(serviceNumber);
			createCartRequest.setProductType(productType);
			createCartRequest.setAmount(rechargeAmount);
			createCartRequest.setPlanType(planType);
			createCartRequest.setCircleName(circleName);
			logger.info("Setting userid for cart request "+ FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
			createCartRequest.setUserId((String) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
			logger.info("Created cart request for googlecredits "+createCartRequest.toString());
			logger.info("calling cart checkout service to create cart for lookupId: "+ createCartRequest.getLookupId());
			try{
				CheckoutResponse checkoutResponse = cartClient.createCart(createCartRequest);
				logger.info("cart id returned from checkout: " + checkoutResponse.getCheckoutId());
			}
			catch(Exception exp){
				logger.info("Unable to create cart in cartcheckout. Scheduling task scheduler for lookupid: "+createCartRequest.getLookupId());
				CreateCartTaskRequest createCartTaskRequest = (CreateCartTaskRequest) createCartRequest;
				String taskId = generateTaskId();
				createCartTaskRequest.setTaskId(taskId);
				//check why taskdto should be made as final
				TaskDTO taskDTO = new TaskDTO();
				taskDTO.setRequest(createCartTaskRequest);
				taskDTO.setTaskType(constants.CREATE_CART_TASK_TYPE);

				taskDTO.setCurrentScheduleTime(new Date());
				try {
					taskScheduler.submitTask(taskDTO);
				} catch (DuplicateTaskException e) {

					final String exceptionMsg =
							String.format("DuplicateTaskException while creating cart, "
									+ "taskId: %s", taskId);
					logger.error(exceptionMsg, e);
				} catch (InvalidTaskTypeException e) {

					String exceptionMsg =
							String.format(
									"InvalidTaskTypeException while creating IMSWalletTask, "
											+ " taskId: %s", taskId);
					logger.error(exceptionMsg, e);
					throw e;
				}
			}

			return successResponse(homePageBO.getCartId(), homePageBO.getHomePageId());
		} catch (Exception e) {
			logger.error("Exception while saving recharge", e);
			metricsClient.recordEvent("CreditRecharge.Controller", "SaveCall", "Exception");
			return baseValidationService.processError(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}


	private Map<String, String> validateCreditRequest(RechargeRequest rechargeRequest, BindingResult bindingResult,
			HttpServletRequest request) {
		logRequest(rechargeRequest);
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "google.credits", "click");
		return validationHelper.validateRequest(rechargeRequest, request);
	}

	private void logRequest(RechargeRequest request) {
		String productType = String.valueOf(request.getProductType());
		String operator = String.valueOf(request.getOperator());
		String circle = String.valueOf(request.getCircleName());
		String planType = String.valueOf(request.getRechargePlanType());
		String serviceNumber = String.valueOf(request.getServiceNumber());
		String amount = String.valueOf(request.getAmount());

		logger.info("Product: [" + productType + "] " + "Operator: [" + operator + "] " + "Circle: [" + circle + "] "
				+ "RechargeType: [" + planType + "] " + "ServiceNumber: [" + serviceNumber + "] " + "Amount: [" + amount
				+ "] ");
	}

	private TxnHomePageBusinessDO createTxnHomePageBusinessDO(final String serviceNumber, final String productType,
			final Float rechargeAmount, final String operator, final String circleName, String rechargePlanType) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		String lookupID = FCUtil.getLookupID();
		tracker.trackLookupId(lookupID);
		String sessionId = fs.getUuid();
		fs.setRechargePlanType(rechargePlanType);

		TxnHomePageBusinessDO thpDO = new TxnHomePageBusinessDO();
		thpDO.setProductType(productType);
		thpDO.setServiceNumber(serviceNumber);
		thpDO.setAmount(rechargeAmount);
		thpDO.setRechargePlanType(rechargePlanType);
		thpDO.setOperatorName(operator);
		thpDO.setCircleName(circleName);
		thpDO.setLookupId(lookupID);
		thpDO.setSessionId(sessionId);
		return thpDO;
	}

	/**
	 * Used to save the data to campaign service
	 * @param serviceNumber
	 * @param productType
	 * @param rechargeAmount
	 * @param operator
	 * @param circleId
	 * @param sessionId
	 * @param lookupId
	 */
	private void saveInCampaignService(final String serviceNumber, final String productType, final Float rechargeAmount,
			final String operator, int circleId, String sessionId, String lookupId) {
		
		Map<String, Object> transactionInfo = new HashMap<>();
		transactionInfo.put(InTxnDataConstants.RECHARGE_AMOUNT, rechargeAmount);
		String transInfoCirName = operatorCircleService.getCircle(circleId) == null ? ""
				: operatorCircleService.getCircle(circleId).getName();
		transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, transInfoCirName);
		String transInfoOpName = operatorCircleService.getOperator(Integer.parseInt(operator)) == null ? ""
				: operatorCircleService.getOperator(Integer.parseInt(operator)).getName();
		transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, transInfoOpName);
		transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, circleId);
		transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, Integer.parseInt(operator));
		transactionInfo.put(InTxnDataConstants.PRODUCT_TYPE, FCConstants.productTypeToNameMap.get(productType));
		transactionInfo.put(InTxnDataConstants.SERVICE_NUMBER, serviceNumber);
		transactionInfo.put(InTxnDataConstants.SESSION_ID, sessionId);
		try {
			campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID,
					transactionInfo);
		} catch (Exception e) {
			logger.error("[Error occurred while saving intxndata] for session: " + sessionId, e);
		}
	}

	/**
	 * Creates the success response map to send back to UI
	 * @param lookupID
	 * @param txnHomePgID
	 * @return Map -
	 * 			return successMap
	 */
	private Map<String, String> successResponse(String lookupID, String txnHomePgID) {
		return ImmutableMap.of(CALL_STATUS, SUCCESS, LOOKUP_ID, lookupID, HOME_PAGE_ID, txnHomePgID);
	}

	private String generateTaskId(){
		return UUID.randomUUID().toString() + new Timestamp(System.currentTimeMillis());
	}
}
