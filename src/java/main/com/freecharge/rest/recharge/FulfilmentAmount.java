package com.freecharge.rest.recharge;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FulfilmentAmount extends AbstractFulfilmentRestResponse {
    public FulfilmentAmount(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    private BigDecimal amount;
    
    
}
