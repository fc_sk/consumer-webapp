package com.freecharge.rest.recharge;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.PgAlert;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.recharge.services.PgAlertService;
import com.freecharge.rest.model.ResponseStatus;

@Controller
@RequestMapping("/rest/pgalert/*")
public class PgAlertController {
    private Logger                    logger = LoggingFactory.getLogger(getClass());
    @Autowired
    PgAlertService pgAlertService;
    
    @NoLogin
    @RequestMapping(value = "{pgId}/status", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus getpgAlertById(@PathVariable Long pgId, ModelMap model) throws InvalidParameterException {
        ResponseStatus responseStatus = new ResponseStatus();
        PgAlert pgAlert;
        try {
            pgAlert = pgAlertService.getPgAlertId(pgId);
        } catch (RuntimeException e) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Failed to get the Payment Gateway Alert for pgID ." + pgId);
            logger.error("Failed to get the Payment Gateway Alert for pgID " + pgId, e);
            return responseStatus;
        }
        responseStatus.setStatus(FCConstants.SUCCESS);
        responseStatus.setResult(pgAlert);
        return responseStatus;
    }
    
    @NoLogin
    @RequestMapping(value = "all/status", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus getAllPgAlert(ModelMap model)
            throws InvalidParameterException {
        ResponseStatus responseStatus = new ResponseStatus();
        List<PgAlert> allPgAlertsList;
        try {
            allPgAlertsList = pgAlertService.getAllPgAlerts();
        } catch (RuntimeException e) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Failed to get the Payment Gateway Alert Status.");
            logger.error("Failed to get the Payment Gateway Alert Status", e);
            return responseStatus;
        }
        responseStatus.setStatus(FCConstants.SUCCESS);
        responseStatus.setResult(allPgAlertsList);
        return responseStatus;
    }

    @NoLogin
    @RequestMapping(value = "all/active", method = RequestMethod.GET)
    public @ResponseBody ResponseStatus getAllActivePgAlerts(ModelMap model) throws InvalidParameterException {
        ResponseStatus responseStatus = new ResponseStatus();
        List<PgAlert> allPgAlertsList;
        try {
            allPgAlertsList = pgAlertService.getAllActivePgAlerts();
        } catch (RuntimeException e) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Failed to get the Payment Gateway Alert.");
            logger.error("Failed to get the Payment Gateway Alert", e);
            return responseStatus;
        }
        responseStatus.setStatus(FCConstants.SUCCESS);
        responseStatus.setResult(allPgAlertsList);
        return responseStatus;
    }
}
