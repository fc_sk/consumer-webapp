package com.freecharge.rest.recharge;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.autopay.clientdo.AutoPaySchedule;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.rest.RestValidationHelper;
import com.freecharge.web.service.CommonHttpService;
import com.freecharge.web.service.CommonHttpService.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Controller
@RequestMapping(AutoPayScheduleController.REST_RECHARGE_AUTOPAY)
public class AutoPayScheduleController {

    static final String REST_RECHARGE_AUTOPAY = "/rest/recharge/autopay";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private CommonHttpService commonHttpService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private RestValidationHelper validationHelper;

    @RequestMapping(value = "/createSchedule", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Map<String, String> createSchedule(@RequestBody AutoPaySchedule schedule,
            final HttpServletRequest request) throws HTTPException, IOException {
    	validationHelper.validateOrderIdBelongsToUser(schedule.getOrderId());
        Users user = userServiceProxy.getLoggedInUser();
        schedule.setUserId(user.getUserId());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, ""),
                new Gson().toJson(schedule), Boolean.FALSE, Integer.valueOf(1000), headers);
        logger.info("Response :" + response.getResponseBody());
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }
    
    @RequestMapping(value = "v2/createSchedule", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody Map<String, String> createScheduleV2(@RequestBody AutoPaySchedule schedule,
            final HttpServletRequest request) throws HTTPException, IOException {
    	validationHelper.validateOrderIdBelongsToUser(schedule.getOrderId());
        Users user = userServiceProxy.getLoggedInUser();
        schedule.setUserId(user.getUserId());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, ""),
                new Gson().toJson(schedule), Boolean.FALSE, Integer.valueOf(1000), headers);
        logger.info("Response :" + response.getResponseBody());
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }


    @RequestMapping(value = "/getActiveSchedules", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, String> getActiveSchedules(final HttpServletRequest request)
			throws HTTPException, IOException {
		Integer channelId = FCUtil.getChannelFromRequest(request);
		// Users user = userServiceProxy.getLoggedInUser();
		// int userId = user.getUserId();
		/**
		 * HttpResponse response =
		 * commonHttpService.fireGetRequestWithTimeOut(fcProperties.getAutoPayBaseUrl()
		 * + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, "") + "/" +
		 * userId, new HashMap<String, String>(), 2000); Type type = new
		 * TypeToken<List<AutoPaySchedule>>() { }.getType(); return new
		 * Gson().fromJson(response.getResponseBody(), type);
		 */
		Map<String, String> returnMap = new HashMap<String, String>();
		if (channelId == 5) {
			returnMap.put("errorMessage",
					"Autopay is temporarily unavailable due to technical reasons. Please try again later");
			return returnMap;
		} else {
			return null;
		}
		// Returning null for testing purpose as autopay is going to close //new
		// Gson().fromJson(response.getResponseBody(), type);
	}

    @RequestMapping(value = "/getAllSchedules", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<AutoPaySchedule> getAllSchedules(final HttpServletRequest request)
            throws HTTPException, IOException {
        Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        HttpResponse response = commonHttpService.fireGetRequestWithTimeOut(fcProperties.getAutoPayBaseUrl()
                + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, "") + "/" + userId,
                new HashMap<String, String>(), 2000);
        Type type = new TypeToken<List<AutoPaySchedule>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }

    @RequestMapping(value = "/deactivateSchedule/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> deactivateAutoPaySchedule(@PathVariable Integer scheduleId,
            final HttpServletRequest request) throws HTTPException, IOException {
    	Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, "") + "/" + userId, "", Boolean.FALSE, Integer.valueOf(1000),
                new HashMap<String, String>());
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }

    @RequestMapping(value = "/activateSchedule/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> activateAutoPaySchedule(@PathVariable Integer scheduleId,
            final HttpServletRequest request) throws HTTPException, IOException {
    	Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY , "") + "/" + userId, "",
                Boolean.FALSE, Integer.valueOf(1000), new HashMap<String, String>());
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }

    @RequestMapping(value = "/deactivateNotifications/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> deactivateNotifications(@PathVariable Integer scheduleId,
            final HttpServletRequest request) throws HTTPException, IOException {
    	Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, "") + "/" + userId, "",
                Boolean.FALSE, Integer.valueOf(1000), new HashMap<String, String>());
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }

    @RequestMapping(value = "/activateNotifications/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> activateNotifications(@PathVariable Integer scheduleId,
            final HttpServletRequest request) throws HTTPException, IOException {
    	Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
                fcProperties.getAutoPayBaseUrl() + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, "") + "/" + userId, "",
                Boolean.FALSE, Integer.valueOf(1000), new HashMap<String, String>());
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(response.getResponseBody(), type);
    }

    @ExceptionHandler(value = { Exception.class, HTTPException.class, IOException.class })
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Map<String, String> processGenericException(Exception ex, final HttpServletRequest request) {
        logger.error("Error hitting autopay API : " + request.getRequestURI().replace(REST_RECHARGE_AUTOPAY, ""), ex);
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put("errorCode", "AP007");
        responseMap.put("errorMessage", "Internal server error.");
        return responseMap;
    }

}
