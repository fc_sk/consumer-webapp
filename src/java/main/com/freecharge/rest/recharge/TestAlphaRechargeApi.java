package com.freecharge.rest.recharge;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.freecharge.common.framework.logging.LoggingFactory;

import flexjson.JSONDeserializer;

public class TestAlphaRechargeApi {
    String                                RECHARGE_URL_END_POINT = "https://www.freecharge.in/rest/api/recharge";
//    String                                RECHARGE_URL_END_POINT = "http://localhost:8080/rest/api/recharge";

    private final Logger                  logger              = LoggingFactory.getLogger(getClass());

    JSONDeserializer<Map<String, Object>> deserializer        = new JSONDeserializer<Map<String, Object>>();

    @Test
    public void testSampleRecharge() throws Exception {
        String csrfRequestIdentifier = "dcd3b43a-6cc6-43d5-beb9-fcf1305c5851";
//        String csrfRequestIdentifier = "c8d69912-5cd4-4e32-8022-7558b4a7fdd2";
//        String mobileNumber = "9738042066";
        String mobileNumber = "9999669859";
        Integer serviceType = 1;
        Integer operatorId = 1;
        Double amount = 10.0;
        String txnReferenceNumber = "xyz";
        String apiVersion = "1.0";
        String sessionId = "68987847-2a3c-49f5-a786-de5c66431ef1";
//        String sessionId = "1477b4c8-b64c-41da-84ac-64d45e91c1ba";
        NameValuePair[] rechargePostData = createRechargeSubmitData(mobileNumber, serviceType, operatorId, amount,
                txnReferenceNumber, apiVersion);
        Map<String, Object> rechargeApiResponse = makeWSCallToRechargeApi(rechargePostData, csrfRequestIdentifier,
                sessionId);
        System.out.println(rechargeApiResponse);
    }

    private NameValuePair[] createRechargeSubmitData(String mobileNumber, Integer serviceType, Integer operatorId,
            Double amount, String txnReferenceNumber, String apiVersion) {

        NameValuePair[] statusCheckSubmitData = { new NameValuePair("mobileNumber", mobileNumber),
                new NameValuePair("serviceType", serviceType.toString()),
                new NameValuePair("operatorId", operatorId.toString()), new NameValuePair("amount", amount.toString()),
                new NameValuePair("txnReferenceNumber", txnReferenceNumber),
                new NameValuePair("apiVersion", apiVersion), };

        return statusCheckSubmitData;
    }

    private Map<String, Object> makeWSCallToRechargeApi(NameValuePair[] rechargePostData, String csrfRequestIdentifier,
            String sessionId) {
        HttpClient client = new HttpClient();
        PostMethod postMethod = new PostMethod(RECHARGE_URL_END_POINT);

        try {
            int getStatus;
            postMethod.setRequestBody(rechargePostData);
            postMethod.setRequestHeader("csrfRequestIdentifier", csrfRequestIdentifier);
            postMethod.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            postMethod.setRequestHeader("Cookie", "app_fc=" + sessionId);
            String operatorMapResponse;
            try {
                getStatus = client.executeMethod(postMethod);
                operatorMapResponse = postMethod.getResponseBodyAsString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            logger.info("Recharge API web Response : " + operatorMapResponse);

            if (HttpStatus.SC_OK == getStatus) {
                Map<String, Object> wsResponse = deserializer.deserialize(operatorMapResponse);
                return wsResponse;
            } else {
                throw new RuntimeException("Error while calling Recharge API web end point : [" + RECHARGE_URL_END_POINT
                        + "]" + "Got HTTP failure HTTP Code [" + getStatus + "]. Post Response [" + operatorMapResponse
                        + "]");
            }
        } finally {
            postMethod.releaseConnection();
        }
    }
}
