package com.freecharge.rest.recharge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.entity.SessionMaster;
import com.freecharge.common.framework.session.persistence.SessionDaoRedis;
import com.freecharge.common.framework.session.service.CookieHandler;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freebill.service.BillPaymentService;
//import com.freecharge.freefund.services.CampaignssResult;
import com.freecharge.freefund.services.FreefundFulfillment;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCategoryMaster;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.beans.BBPSBillTransactionDetails;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.webdo.BBPSTransactionDetails;

@Controller
@RequestMapping("/rest/*")
public class OperatorResourceController extends AbstractRestController {
    
    private static final Logger logger = LoggingFactory.getLogger(OperatorResourceController.class);
    
	@Autowired 
	private CommonService commonService;
	
    @Autowired
    OperatorCircleService prefixService;
    
    @Autowired
    OperatorAlertService operatorAlertService;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private BillPaymentService billPaymentService;
    
//    @Autowired
//    private CampaignServiceClient campaignServiceClient;
    
//    @Autowired
//    private InExcelReader 				  inExcelReader;
//    
    @Autowired
    private HomeService                       homeService;
    
    @Autowired
    private FulfillmentService fulfillmentService;
    
    @Autowired
    private FreefundFulfillment freefundFulfillment;
    
    @Autowired
	@Qualifier("sessionDaoRedis")
	private  SessionDaoRedis sessionDaoRedis;
    
    @Autowired
    private CookieHandler cookieHandler;
    
    @Autowired
    private RechargeInitiateService rechargeInitiateService;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value="/campaign/promocode/backfill", method=RequestMethod.GET)
    public @ResponseBody Boolean campaignBackfill(HttpServletRequest request) {
		logger.info("PromocodeBackFill reached at controller promocode");
    	freefundFulfillment.campaignBackfill();
    	return true;
	}
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value="/campaign/nonpromocode/backfill", method=RequestMethod.GET)
    public @ResponseBody Boolean nonPromocodeCampaignBackfill(HttpServletRequest request) {
    	logger.info("PromocodeBackFill reached at controller nonpromocode");
    	freefundFulfillment.nonPromocodeCampaignBackfill();
    	return true;
	}
    
    @NoLogin
    @NoSessionWrite
    @Csrf(exclude= true)
    @RequestMapping(value = "/campaign/promocode/backfill-data", method = RequestMethod.POST, consumes = {MediaType.TEXT_PLAIN_VALUE})
	@ResponseBody
	public Boolean promocodeCampaignBackfillBulk(@RequestBody String requestDataStr) {
    	List<Map<String, Object>> requestData;
		try {
			requestData = new ObjectMapper().readValue(requestDataStr, new TypeReference<List<Map<String, Object>>>(){});
			logger.info("PromocodeBackFillBulkData reached at controller promocode");
			freefundFulfillment.campaignBackfill(requestData);
	    	return true;
		} catch (IOException e) {
			logger.error("Unable to parse requestbody : " + requestDataStr + " because of exception :",e);
			return false;
		}
	}
    
    @NoLogin
    @NoSessionWrite
    @Csrf(exclude= true)
    @RequestMapping(value = "/campaign/nonpromocode/backfill-data", method = RequestMethod.POST, consumes = {MediaType.TEXT_PLAIN_VALUE})
	@ResponseBody
	public Boolean nonPromocodeCampaignBackfillBulk(@RequestBody String requestDataStr) {
    	List<Map<String, Object>> requestData;
		try {
			requestData = new ObjectMapper().readValue(requestDataStr, new TypeReference<List<Map<String, Object>>>(){});
			logger.info("NonPromocodeBackFillBulkData reached at controller promocode");
	    	freefundFulfillment.nonPromocodeCampaignBackfill(requestData);
	    	return true;
		} catch (IOException e) {
			logger.error("Unable to parse requestbody : " + requestDataStr + " because of exception :",e);
			return false;
		}
	}
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value="/campaign/promocode/backfill-data-from-file", method=RequestMethod.GET)
    public @ResponseBody Boolean campaignBackfillDataFromFile(HttpServletRequest request) {
		logger.info("PromocodeBackFillDataFromFile reached at controller promocode");
    	freefundFulfillment.campaignBackfillFromFile();
    	return true;
	}
    
//    public static UserDetails getUserDetails(Map<String, Object> userDetail, String orderId) {
//
//        UserDetails userDetailsObj = new UserDetails();
//        userDetailsObj.setUserEmail((String) userDetail.get("email"));
//        userDetailsObj.setUserMobileNo((String) userDetail.get("mobileno"));
//        userDetailsObj.setUserFirstName((String) userDetail.get("firstname"));
//        userDetailsObj.setAmount(userDetail.get("amount").toString());
//        userDetailsObj.setLookupId((String) userDetail.get("lookupId"));
//        userDetailsObj.setOperatorName((String) userDetail.get("operatorName"));
//        userDetailsObj.setProductType((String) userDetail.get("productType"));
//        userDetailsObj.setChannelId(userDetail.get("channelId") != null ? (Integer) userDetail.get("channelId") : 1);
//        userDetailsObj.setCreatedAt(userDetail.get("createdAt").toString());
//        userDetailsObj.setTxnfulfilmentid(userDetail.get("txnfulfilmentid").toString());
//        userDetailsObj.setCircleName(userDetail.get("circleName") != null ? (String) userDetail.get("circleName") : "");
//        userDetailsObj.setBillId(userDetail.get("billId") != null ? (String) userDetail.get("billId") : "");
//        userDetailsObj.setOrderId(orderId);
//        userDetailsObj.setCircleMasterId(userDetail.get("circleMasterId") != null ? (Integer) userDetail.get("circleMasterId") : -1);
//        userDetailsObj.setOperatorMasterId(userDetail.get("operatorMasterId") != null ? (Integer) userDetail.get("operatorMasterId") : -1);
//        userDetailsObj.setOperatorUIName(userDetail.get("operatorUIName") != null ? (String) userDetail.get("operatorUIName") : "");
//        userDetailsObj.setCircleUIName(userDetail.get("circleUIName") != null ? (String) userDetail.get("circleUIName") : "");
//        return userDetailsObj;
//    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/getOrderDetails/{reconId}", method = RequestMethod.GET)
    public @ResponseBody String fetchOrderDetailByReconId(@PathVariable String reconId, HttpServletResponse response, Model model, HttpServletRequest httpRequest) {
    	Map<String, String> result = rechargeInitiateService.getOrderDetailByReconId(reconId);
    	logger.info("Recevied request for get order details for recon Id : " + reconId);
    	if(result != null) {
    		return result.get("OrderId");
    	}
    	
    	return "Unable to find corresponding orderId for reconId:"+reconId;
    }
       
	@NoLogin
    @NoSessionWrite
	@RequestMapping(value="/operators/{rechargeType}", method=RequestMethod.GET)
	public @ResponseBody Map<String, List<OperatorMaster>> getAllOperatorsForType(@PathVariable String rechargeType, ModelMap model) throws InvalidParameterException {
				
		if (rechargeType == null) {
			throw new InvalidParameterException("rechargeType", rechargeType,"Recharge type is required", ValidationErrorCode.REQUIRED_PRODUCT_MASTER_ID);
		} else if (!(ProductMaster.ProductName.isValidProductName(rechargeType))) {
			throw new InvalidParameterException("rechargeType", rechargeType,"Valid recharge type is required", ValidationErrorCode.INVALID_PRODUCT_MASTER_ID);
		}
			
		int rechargeTypeId = ProductMaster.ProductName.valueOf(rechargeType).getProductId();
		
		List<OperatorMaster> operatorList = null;		
		if (rechargeTypeId == ProductMaster.ProductName.Mobile.getProductId()) {
			operatorList = commonService.getMobileOperatorMasterList();
		} else if (rechargeTypeId == ProductMaster.ProductName.DTH.getProductId()) {
			operatorList = commonService.getDTHOperatorMasterList();
		} else if (rechargeTypeId == ProductMaster.ProductName.DataCard.getProductId()) {
			operatorList = commonService.getDatacardOperatorMasterList();
		} else if (rechargeTypeId == ProductMaster.ProductName.PostPaidDataCard.getProductId()) {
            operatorList = commonService.getPostPaidDatacardOperatorMasterList();
        } else if (rechargeTypeId == ProductMaster.ProductName.MobilePostpaid.getProductId()) {
		    operatorList = commonService.getPostpaidMobileOperatorMasterList();
		}
		
		 Map<String, List<OperatorMaster>> operatorMap = new HashMap<String, List<OperatorMaster>>(); 
		 operatorMap.put("operators", operatorList);
		
		return operatorMap;
	}
	
//	@Csrf(exclude = true)
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/operators/Bill", method=RequestMethod.GET)
    public @ResponseBody Map<String, List<BillerDetails>> getAllOperatorsForType(HttpServletRequest request) {
	    String locationId = request.getParameter("location");
	    //put a toggle check here for switching to new flow
	    List<BillerDetails> billPayOperatorList = new ArrayList<BillerDetails>();
	    Integer channelId = FCUtil.getChannelFromRequest(request);
	    Integer versionId = MobileUtil.getMobileAppVersionFromRequest(request);
	    
	    if(appConfigService.isBillpayOperatorChannelBlockingEnabled()){
	    	 billPayOperatorList = commonService.getBillPayOperatorMasterListWithChannelBlocking(locationId, channelId, versionId);
	    }else{
	    		 billPayOperatorList = commonService.getBillPayOperatorMasterList(locationId);
	    }
        Map<String, List<BillerDetails>> operatorMap = new HashMap<String, List<BillerDetails>>(); 
        operatorMap.put("operators", billPayOperatorList);
        return operatorMap;
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/operators/Metro", method=RequestMethod.GET)
    public @ResponseBody Map<String, List<BillPayOperatorMaster>> getOperatorsForProductId(HttpServletRequest request) {
        List<BillPayOperatorMaster> billPayOperatorList = commonService.getOperatorsForProductId(ProductName.Metro.getProductId());
        Map<String, List<BillPayOperatorMaster>> operatorMap = new HashMap<String, List<BillPayOperatorMaster>>(); 
        operatorMap.put("operators", billPayOperatorList);
        return operatorMap;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value= {"/apple/association",".well-known/apple-app-site-association"}, method=RequestMethod.GET)
    public @ResponseBody String getAppleSiteAssociation(HttpServletRequest request) {
		return appConfigService.getAppDeepLinkSiteAssociation(ChannelEnum.APPLE.getValue());
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/window/association", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getWindowSiteAssociation(HttpServletRequest request) {
		return appConfigService.getAppDeepLinkSiteAssociation(ChannelEnum.WINDOW.getValue());
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value=".well-known/assetlinks.json", method=RequestMethod.GET, produces = "application/json") 
	public @ResponseBody String getAndroidSiteAssociation(HttpServletRequest request) {
		return appConfigService.getAppDeepLinkSiteAssociation(ChannelEnum.ANDROID.getValue());
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/category/Bill", method=RequestMethod.GET)
    public @ResponseBody Map<String, List<BillPayCategoryMaster>> getAllCategoryForType() {
        List<BillPayCategoryMaster> billPayCategoryList = commonService.getBillPayCategoryMasterList();
        Map<String, List<BillPayCategoryMaster>> categoryMap = new HashMap<String, List<BillPayCategoryMaster>>(); 
        categoryMap.put("category", billPayCategoryList);
        return categoryMap;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/validators/Bill", method=RequestMethod.GET)
    public @ResponseBody Map<String, BillPayValidator> getAllBillValidators() {
	    Map<String, BillPayValidator> billPayValidatorList = commonService.getBillPayValidatorList();
        return billPayValidatorList;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value="/validators/{operatorId}/{circleId}/Bill", method=RequestMethod.GET)
    public @ResponseBody BillPayValidator getBillValidators(@PathVariable Integer operatorId, @PathVariable Integer circleId) {
        BillPayValidator billPayValidator = commonService.getBillPayValidatorForOperator(operatorId, circleId);
        return billPayValidator;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/operators/mapping/{productType}/{serviceNumber}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> getServiceProvider(@PathVariable String productType, @PathVariable String serviceNumber) {
        return getOperatorCircle(productType, serviceNumber);
    }
	
    private Map<String, Object> getOperatorCircle(String productType, String serviceNumber) {
        Map<String, Object> returnData = new HashMap<>();

        String productId = Integer.toString(ProductName.fromPrimaryProductType(productType).getProductId());

        List<Map<String, Object>> mnpData = prefixService.getMNPOperator(serviceNumber, productId);
        List<OperatorAlert> operatorAlerts = null;
        if (!FCUtil.isEmpty(mnpData)) {
            String operatorId = mnpData.get(0).get("operatorMasterId") == null ? null
                    : mnpData.get(0).get("operatorMasterId").toString();

            boolean operatorExcluded = prefixService.isExcludedForAutoDetection(operatorId);

            if (operatorExcluded) {
                returnData.put("prefixData", new ArrayList<>());
                return returnData;
            }

            String circleId = mnpData.get(0).get("circleMasterId") == null ? null
                    : mnpData.get(0).get("circleMasterId").toString();
            if (operatorId != null && circleId != null) {
                operatorAlerts = operatorAlertService.processAndGetOperatorAlert(Integer.parseInt(operatorId),
                        circleId);
            }
           }
        if (!FCUtil.isEmpty(operatorAlerts)) {
            returnData.put("operatorAlert", operatorAlerts.get(0));
        }

        returnData.put("prefixData", mnpData);
        return returnData;
    }

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/operators/mapping/v2/{serviceNumber}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> getProductPredictionOld(@PathVariable String serviceNumber) {
        Map<String, Object> returnData = new HashMap<>();

        if (isValidServiceNumber(serviceNumber)) {
            logger.info(serviceNumber + " is a valid Service Number");
            Map<String, Object> productPredictorData = prefixService.getProductTypeData(serviceNumber,ProductMaster.ProductName.Mobile.getProductType());
            if (!FCUtil.isEmpty(productPredictorData)) {
                List<OperatorAlert> operatorAlerts = null;
                String operatorId=productPredictorData.get("operatorId").toString();
                String circleId=productPredictorData.get("circleId").toString();
                if (operatorId != null && circleId != null) {
                    operatorAlerts = operatorAlertService
                            .processAndGetOperatorAlert(Integer.parseInt(operatorId), circleId);
               }
                if (!FCUtil.isEmpty(operatorAlerts)) {
                    productPredictorData.put("operatorAlert", operatorAlerts.get(0));
                }
                return productPredictorData;
            } else {
                returnData.put("serviceNumber", serviceNumber);
                returnData.put("productType", "UNKNOWN");
                Map<String, Object> prePaidResponse = getOperatorCircle("V", serviceNumber);
                Map<String, Object> postPaidResponse = getOperatorCircle("M", serviceNumber);
                if (!FCUtil.isEmpty(prePaidResponse)) {
                    returnData.put("prepaid", parseOperatorAndCircleData(prePaidResponse));
                }
                if (!FCUtil.isEmpty(postPaidResponse)) {
                    returnData.put("postpaid", parseOperatorAndCircleData(postPaidResponse));
                }

                return returnData;
            }
        } else {
            returnData.put("ERROR", ErrorCode.INVALID_SERVICE_NUMBER_FORMAT.restResponse());
        }

        return returnData;

    }

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/operators/mapping/v3/{serviceNumber}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> getProductPrediction(@PathVariable String serviceNumber) {
        Map<String, Object> returnData = new HashMap<>();
        if (isValidServiceNumber(serviceNumber)) {
        	returnData = getProductPredictionMap(serviceNumber,returnData,RechargeConstants.MOBILE_PRODUCT_TYPE);            
        } else {
            returnData.put("ERROR", ErrorCode.INVALID_SERVICE_NUMBER_FORMAT.restResponse());
        }
        return returnData;

    }
    
    private Map<String, Object> getProductPredictionMap(String serviceNumber, Map<String, Object> returnData,String productType) {
    	logger.info(serviceNumber + " is a valid Service Number");
        Map<String, Object> productPredictorData = prefixService.getProductTypeData(serviceNumber,productType);
        if (!FCUtil.isEmpty(productPredictorData)) {
            productType = productPredictorData.get("productType").toString();
            returnData.put("serviceNumber", serviceNumber);
            returnData.put("productType", productType);
            List<OperatorAlert> operatorAlerts = null;
            String operatorId = productPredictorData.get("operatorId").toString();
            String circleId = productPredictorData.get("circleId").toString();
            if (operatorId != null && circleId != null) {
                operatorAlerts = operatorAlertService.processAndGetOperatorAlert(Integer.parseInt(operatorId),
                        circleId);
                addCircleOperatorData(returnData, serviceNumber, productType, operatorId, circleId);
            }
            if (!FCUtil.isEmpty(operatorAlerts)) {
                productPredictorData.put("operatorAlert", operatorAlerts.get(0));
            }
            return returnData;
        } else {
            returnData.put("serviceNumber", serviceNumber);
            returnData.put("productType", "UNKNOWN");
            addPrefixData(returnData, serviceNumber,productType);

            return returnData;
        }
	}

	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/operators/mapping/datacard/{serviceNumber}", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getProductPredictionDataCard(@PathVariable String serviceNumber) {
		Map<String, Object> returnData = new HashMap<>();

		if (isValidServiceNumber(serviceNumber)) {
			returnData = getProductPredictionMap(serviceNumber, returnData,RechargeConstants.DATA_CARD_PRODUCT_TYPE);
		} else {
			returnData.put("ERROR", ErrorCode.INVALID_SERVICE_NUMBER_FORMAT.restResponse());
		}

		return returnData;

	}


	private void addCircleOperatorData(Map<String, Object> returnData, String serviceNumber, String productType,
			String operatorId, String circleId) {

		Map<String, Object> operatorCircleData = new HashMap<String, Object>();
		operatorCircleData.put("operatorId", operatorId);
		operatorCircleData.put("circleId", circleId);
		if (FCUtil.isMobileRecharge(productType)) {
			if (productType.equals("V")) {
				returnData.put("prepaid", operatorCircleData);
				Map<String, Object> postPaidResponse = getOperatorCircle("M", serviceNumber);
				returnData.put("postpaid", parseOperatorAndCircleData(postPaidResponse));
			}
			if (productType.equals("M")) {
				returnData.put("postpaid", operatorCircleData);
				Map<String, Object> prePaidResponse = getOperatorCircle("V", serviceNumber);
				returnData.put("prepaid", parseOperatorAndCircleData(prePaidResponse));
			}
		} else if (FCUtil.isDataCard(productType)) {
			if (productType.equals("C")) {
				returnData.put("prepaid", operatorCircleData);
				Map<String, Object> postPaidResponse = getOperatorCircle("F", serviceNumber);
				returnData.put("postpaid", parseOperatorAndCircleData(postPaidResponse));
			}
			if (productType.equals("F")) {
				returnData.put("postpaid", operatorCircleData);
				Map<String, Object> prePaidResponse = getOperatorCircle("C", serviceNumber);
				returnData.put("prepaid", parseOperatorAndCircleData(prePaidResponse));
			}
		}
	}

	private void addPrefixData(Map<String, Object> returnData, String serviceNumber, String productType) {
		Map<String, Object> prePaidResponse = null;
		Map<String, Object> postPaidResponse = null;

		if (FCUtil.isMobileRecharge(productType)) {
			prePaidResponse = getOperatorCircle("V", serviceNumber);
			postPaidResponse = getOperatorCircle("M", serviceNumber);
			if (!FCUtil.isEmpty(prePaidResponse)) {
				returnData.put("prepaid", parseOperatorAndCircleData(prePaidResponse));
			}
			if (!FCUtil.isEmpty(postPaidResponse)) {
				returnData.put("postpaid", parseOperatorAndCircleData(postPaidResponse));
			}
		}
		if (FCUtil.isDataCard(productType)) {
			prePaidResponse = getOperatorCircle("C", serviceNumber);
			postPaidResponse = getOperatorCircle("F", serviceNumber);
			if (!FCUtil.isEmpty(prePaidResponse)) {
				returnData.put("prepaid", parseOperatorAndCircleData(prePaidResponse));
			}
			if (!FCUtil.isEmpty(postPaidResponse)) {
				returnData.put("postpaid", parseOperatorAndCircleData(postPaidResponse));
			}
		}

	}

    private Map<String, Object> parseOperatorAndCircleData(Map<String, Object> prePaidResponse) {
        Map<String, Object> responseData = new HashMap<>();
        
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> prefixData = (List<Map<String, Object>>)prePaidResponse.get("prefixData");
        if (FCUtil.isEmpty(prefixData)) {
            return responseData;
        }
        
        Map<String, Object> data = prefixData.get(0);
        
        String operatorId = data.get("operatorMasterId") == null ? null
                : data.get("operatorMasterId").toString();
        String circleId = data.get("circleMasterId") == null ? null
                : data.get("circleMasterId").toString();
        
        responseData.put("operatorId", operatorId);
        responseData.put("circleId", circleId);
        
        return responseData;
    }

    private boolean isValidServiceNumber(String serviceNumber) {

        String regex = "(9|8|7|6){1}\\d{9}$";
        Pattern validpattern = Pattern.compile(regex);
        Matcher matcher = validpattern.matcher(serviceNumber);
        if (matcher.find())
            return true;
        return false;
    }
    
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value="/operators/Bill/{operatorId}", method=RequestMethod.GET)
    public @ResponseBody Map<String, Object> getAllOperatorsForType(@PathVariable String operatorId, HttpServletRequest request) {
	    String locationId = request.getParameter("location");
	    List<BillerDetails> billPayOperatorList = commonService.getBillPayOperatorMasterList(locationId);
	    Map<String, Object> downTimeMap = new HashMap<String, Object>(); 
	    for(BillerDetails billpayOperator : billPayOperatorList){
	    	if(operatorId.equalsIgnoreCase(String.valueOf(billpayOperator.getOperatorMasterId()))){
	    		downTimeMap.put("downTimeStatus", billpayOperator.getBillerDownTimeStatus());
	    		downTimeMap.put("downTimeMessage", billpayOperator.getStatusMessage());
	    		break;
	    	}
	    }
        return downTimeMap;
	}
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value="/sessionMaster", method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSessionMaster(HttpServletRequest request, HttpServletResponse response)
			throws JsonGenerationException, JsonMappingException, IOException {
		String sessionId = cookieHandler.checkAndSetCookies(request, response, false);
		if (null != sessionId) {
			try {
				SessionMaster sessionMaster = sessionDaoRedis.retrieveSession(sessionId);
				if(null == sessionMaster) {
					return "sessionMaster is Null getting from sessionDaoRedis and sessionId : "+sessionId ;
				}
				return new ObjectMapper().writeValueAsString(sessionMaster);
			} catch (Exception e) {
				logger.error("gettion error while convert sessionMaster through ObjectMapper  ", e);
			}
		}
		return "sessionId is Null getting from cookieHandler";
	}
}