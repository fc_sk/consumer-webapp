package com.freecharge.rest;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.growthevent.service.UserServiceProxy;

@Component
public class RestControllerHelper {

	private Logger logger = LoggingFactory.getLogger(getClass());
	@Autowired
    private TxnHomePageDAO homePageDao;
	
	@Autowired
    private UserServiceProxy userServiceProxy;
	
	
	public boolean isLookupIdValid(String lookUpId) {
		FreechargeContext fcContext = FreechargeContextDirectory.get();
        String sessionId = fcContext.getSessionId();
        boolean isValidLookupId = false;
        List<TxnHomePage> homePageList = homePageDao.findBySessionId(sessionId);
        logger.info("Size of homePageList :"+ homePageList.size());
        if (homePageList != null) {
            for (TxnHomePage txnHomePage : homePageList) {
                if (areLookupIdsValidAndEqual(txnHomePage.getLookupId(), lookUpId)) {
                    isValidLookupId = true;
                    break;
                }
            }
        }
        
        return isValidLookupId;
    }
	public boolean isLookupIdValid(Integer userId,String email) {
		boolean isValidLookupId = false;
        Users user = userServiceProxy.getUserByUserId(userId) ;
        logger.info("UserEMail: " +user.getEmail() +" email: " + email );
        if(email.equals(user.getEmail())) {
        	return true;
        }
        return isValidLookupId;
    }
	

	private boolean areLookupIdsValidAndEqual(String lookupIdFromSession, String lookUpId) {
        if (lookupIdFromSession == null || lookUpId == null) {
            return false;
        }
        
        if (lookupIdFromSession.isEmpty() || lookUpId.isEmpty()) {
            return false;
            
        }
        
        return lookupIdFromSession.equals(lookUpId);
    }
    
}