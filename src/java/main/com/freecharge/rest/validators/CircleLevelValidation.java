package com.freecharge.rest.validators;

import java.util.List;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.rest.model.RechargeRequest;
import com.google.common.collect.ImmutableList;

public class CircleLevelValidation {

    static List<String> ideaOperatorIdempotencyInclusion = ImmutableList.of("6");
    
    static List<String> ideaCircleIdempotencyInclusion = ImmutableList.of("34");
    
    static List<String> videoconOperatorIdempotencyExclusion = ImmutableList.of("24");
    
    static List<String> videoconCircleIdempotencyExclusion = ImmutableList.of("18");
    
    static List<String> bsnlOperatorIdempotencyInclusion = ImmutableList.of("3");
    
    static List<String> bsnlCircleIdempotencyInclusion = ImmutableList.of("6","15");
    
    static List<String> tataDocomoOperatorIdempotencyInclusion = ImmutableList.of("5","42");
    
    static List<String> tataDocomoCircleIdempotencyInclusion = ImmutableList.of("2","16");
    
    static List<String> relianceCDMAOperatorInclusion = ImmutableList.of("10");	
    
    static List<String> relianceCDMACircleExclusion = ImmutableList.of("20","5","1","11","19");	
    
    static List<String> tataDocomoGSMDatacardOperatorInclusion = ImmutableList.of("52");
    
    static List<String> tataDocomoGSMDatacardCircleInclusion = ImmutableList.of("6");
    
    public static ErrorCode validate(RechargeRequest rechargeRequest) {

        if (ideaOperatorIdempotencyInclusion.contains(rechargeRequest.getOperator())
                && ideaCircleIdempotencyInclusion.contains(rechargeRequest.getCircleName())) {
            return ErrorCode.RECHARGE_IDEA_CIRCLELEVEL_BLOCK;
        }
        
        if(videoconOperatorIdempotencyExclusion.contains(rechargeRequest.getOperator())
        		&& !videoconCircleIdempotencyExclusion.contains(rechargeRequest.getCircleName())){
        	return ErrorCode.CIRCLE_NOT_ALLOWED;
        }
        
        if(bsnlOperatorIdempotencyInclusion.contains(rechargeRequest.getOperator())
        		&& bsnlCircleIdempotencyInclusion.contains(rechargeRequest.getCircleName())){
        	return ErrorCode.RECHARGE_BSNL_CIRCLELEVEL_BLOCK;
        }
        
        if(tataDocomoOperatorIdempotencyInclusion.contains(rechargeRequest.getOperator())
        		&& tataDocomoCircleIdempotencyInclusion.contains(rechargeRequest.getCircleName())){
        	return ErrorCode.CIRCLE_NOT_ALLOWED;
        }

	/*	if (relianceCDMAOperatorInclusion.contains(rechargeRequest.getOperator())
				&& !relianceCDMACircleExclusion.contains(rechargeRequest.getCircleName())) {
			return ErrorCode.CIRCLE_NOT_ALLOWED;
		}*/
		
		if (tataDocomoGSMDatacardOperatorInclusion.contains(rechargeRequest.getOperator())
				&& tataDocomoGSMDatacardCircleInclusion.contains(rechargeRequest.getCircleName())) {
			return ErrorCode.CIRCLE_NOT_ALLOWED;
		}

        return ErrorCode.NO_ERROR;
    }

}
