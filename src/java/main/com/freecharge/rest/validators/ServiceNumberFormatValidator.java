package com.freecharge.rest.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.freecharge.api.error.ErrorCode;

public class ServiceNumberFormatValidator {
    public static ErrorCode validate(String serviceNumber, int exactLength) {
        if (StringUtils.isBlank(serviceNumber)) {
            return ErrorCode.SERVICE_NUMBER_NOT_PRESENT;
        }

        if (!StringUtils.isNumeric(serviceNumber.trim())) {
            return ErrorCode.INVALID_SERVICE_NUMBER_FORMAT;
        }
        
        if (serviceNumber.trim().length() != exactLength) {
            return ErrorCode.INVALID_SERVICE_NUMBER_LENGTH;
        }
        
        return ErrorCode.NO_ERROR;
    }

	public static boolean isValid(String serviceNumber, String numberRegex) {
		Pattern regexPattern = Pattern.compile(numberRegex);
		Matcher matcher = regexPattern.matcher(serviceNumber);
		return matcher.find();
	}
}
