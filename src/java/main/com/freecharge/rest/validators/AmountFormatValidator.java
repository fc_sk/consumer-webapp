package com.freecharge.rest.validators;

import org.apache.commons.lang.StringUtils;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.common.util.FCConstants;

public class AmountFormatValidator {
    public static ErrorCode validate(String amount) {
    	ErrorCode errorCode;
    	if (StringUtils.isBlank(amount)) {
    		errorCode = ErrorCode.AMOUNT_NOT_PRESENT;
    		errorCode.setErrorMessage(FCConstants.errorCodeToMessageMap.get(errorCode)[0]);
            return errorCode;
        }
        
        if (!StringUtils.isNumeric(amount) || Double.parseDouble(amount) < 10) {
        	errorCode = ErrorCode.INVALID_AMOUNT_FORMAT;
    		errorCode.setErrorMessage(FCConstants.errorCodeToMessageMap.get(errorCode)[1]);
            return errorCode;
        }
        
        return ErrorCode.NO_ERROR;
    }
    
    public static ErrorCode validate(Float amount) {
    	ErrorCode errorCode;
        if (amount == null) {
        	errorCode = ErrorCode.AMOUNT_NOT_PRESENT;
    		errorCode.setErrorMessage(FCConstants.errorCodeToMessageMap.get(errorCode)[0]);
            return errorCode;
        }
        
        if (Float.isNaN(amount) || amount < 10) {
        	errorCode = ErrorCode.INVALID_AMOUNT_FORMAT;
    		errorCode.setErrorMessage(FCConstants.errorCodeToMessageMap.get(errorCode)[1]);
            return errorCode;
        }
        return ErrorCode.NO_ERROR;
    }
    
    
	public static boolean isValid(Float amount, Float min, Float max) {
		if (amount == null || Float.isNaN(amount) || amount < min || amount > max || amount <= 0) {
			return false;
		}
		return true;
	}
	
	public static boolean isValid(Float amount) {
		if (amount == null || Float.isNaN(amount) || amount <= 0) {
			return false;
		}
		return true;
	}
}
