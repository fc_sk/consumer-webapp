package com.freecharge.rest.freefund;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/rest/freefund/*")
public class FreefundResourceController {
    
  /*  @Autowired
    private UserService userService;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private FreefundService freeFundService = new FreefundService();
    
    
    private final Logger LOGGER = LoggingFactory.getLogger(getClass());
   
    @RequestMapping(value = "/redeem", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseStatus freefundRedeemToWallet(FreefundRequestWebDO freeFundRequestWebDO) {
        ResponseStatus responseStatus = new ResponseStatus();
        com.freecharge.app.domain.entity.jdbc.Users user = userService.getLoggedInUser();
        if (user == null) {
            LOGGER.info("logged in user not found");
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("userNotFound");
            return responseStatus;
        }

        FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
        freeFundRequestBusinessDO.setEmail(user.getEmail());
        freeFundRequestBusinessDO.setCouponCode(StringUtils.trimToEmpty(freeFundRequestWebDO.getCouponCode()));
        freeFundRequestBusinessDO.setServiceNumber(user.getMobileNo());

        if (StringUtils.isEmpty(freeFundRequestBusinessDO.getCouponCode())) {
            LOGGER.info("coupon validation failure");
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Incorrect freefund code");
            return responseStatus;
        }
        
        freeFundService.validateAndRedeemFreefund(freeFundRequestWebDO, responseStatus, user, freeFundRequestBusinessDO);
        return responseStatus;
    }

  */

}
