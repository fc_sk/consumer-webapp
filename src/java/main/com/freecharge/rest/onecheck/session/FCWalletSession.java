package com.freecharge.rest.onecheck.session;

public class FCWalletSession {
    private final String sessionId;
    private final String userToken;
    
    public FCWalletSession(String sessionId, String userToken) {
        this.sessionId = sessionId;
        this.userToken = userToken;
    }
    
    public String getSessionId() {
        return sessionId;
    }
    
    public String getUserToken() {
        return userToken;
    }
}
