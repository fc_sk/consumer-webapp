package com.freecharge.rest.onecheck.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("fcWalletSessionService")
public class FCWalletSessionService {
    @Autowired
    FCWalletSessionRepository sessionRepository;
    
    public void saveSession(FCWalletSession session) {
        sessionRepository.saveSession(session);
    }
    
    public FCWalletSession retrieveSession(String sessionId) {
        return sessionRepository.retrieveSession(sessionId);
    }
    
    public void deleteSession(String sessionId) {
        sessionRepository.deleteSession(sessionId);
    }
}
