package com.freecharge.rest.onecheck.session;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.MetricsBaseMongoRepository;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("fcWalletSessionRepository")
public class FCWalletSessionRepository extends MetricsBaseMongoRepository {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    private final String SESSIONID = "session_id";
    private final String SESSION_OBJECT = "session_object";
    public static final String SESSION_START_TIME_FIELD_NAME = "fcw_session_start";
    
    @Value("${fcw.session.expire.time}")
    private String sessionExpireTime;

    @Autowired
    protected FCWalletSessionRepository(final DB mongoDB, final MetricsClient metricsClient) {
        
        super("fcWalletSession", mongoDB, metricsClient);
    }
    
    public void saveSession(FCWalletSession walletSession) {
        Map<String, Object> sessionMap = new HashMap<>();
        sessionMap.put("token", walletSession.getUserToken());
        
        String sessionJson = new Gson().toJson(sessionMap);
        
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(SESSIONID, walletSession.getSessionId());
        objectMap.put(SESSION_OBJECT, sessionJson);
        
        this.insertDocument(new BasicDBObject(objectMap));
    }
    
    public FCWalletSession retrieveSession(String sessionId) {
        BasicDBObject query = new BasicDBObject().append(SESSIONID, sessionId);
        
        List<DBObject> sessions = this.findDocuments(query, null, null);
        
        if (sessions == null || sessions.isEmpty()) {
            return null;
        }
        
        if (sessions.size() != 1) {
            throw new IllegalStateException("More than one session found for sessio Id: " + sessionId);
        }
        
        DBObject sessionObject = sessions.get(0);
        
        // being paranoid.
        if (sessionObject == null) {
            return null;
        }
        
        String sessionJson = String.valueOf(sessionObject.get(SESSION_OBJECT));
        
        if (StringUtils.isBlank(sessionJson)) {
            return null;
        }
        
        Map<String, Object> sessionMap = new Gson().fromJson(sessionJson, Map.class);
        
        if (sessionMap == null || sessionMap.isEmpty()) {
            return null;
        }
        
        String sessionToken = String.valueOf(sessionMap.get("token"));
        
        if (StringUtils.isBlank(sessionToken)) {
            return null;
        }
        
        return new FCWalletSession(sessionId, sessionToken);
    }
    
    public void deleteSession(String sessionId) {
        BasicDBObject query = new BasicDBObject().append(SESSIONID, sessionId);
        
        this.deleteDocument(query);
    }
    
    

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        logger.info("Ensuring user_id index.");
        collection.ensureIndex(SESSIONID);
        logger.info("Ensuring index done.");
        
        // Session start TTL index for session expiry.
        logger.info("Ensuring session_start ttl index.");
        BasicDBObject sessionStartIndex = new BasicDBObject();
        sessionStartIndex.put(SESSION_START_TIME_FIELD_NAME, 1);
        BasicDBObject sessionStartTtl = new BasicDBObject();
        int expiryDays = Integer.parseInt(sessionExpireTime);
        sessionStartTtl.put("expireAfterSeconds", (expiryDays * 86400)); // 24 * 60 * 60 = 86400 seconds
        collection.ensureIndex(sessionStartIndex, sessionStartTtl);
        logger.info("Ensuring index done.");
    }
}