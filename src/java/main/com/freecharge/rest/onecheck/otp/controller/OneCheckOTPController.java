package com.freecharge.rest.onecheck.otp.controller;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.onecheck.model.OTPGenerateRequestDO;
import com.freecharge.rest.onecheck.model.OTPValidateRequestDO;
import com.freecharge.rest.onecheck.service.OneCheckOTPService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/otp/v1/*")
public class OneCheckOTPController {

    @Autowired
    private OneCheckOTPService oneCheckOtpService;

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "generate", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String generateOtp(@RequestBody OTPGenerateRequestDO otpGenerateRequestDO,
            HttpServletRequest request) {
        return oneCheckOtpService.generateOtp(otpGenerateRequestDO, request);
    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "validate", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String validateOtp(@RequestBody OTPValidateRequestDO otpValidateRequestDO,
            HttpServletRequest request) {
        return oneCheckOtpService.validateOtp(otpValidateRequestDO, request);
    }

}
