package com.freecharge.rest.onecheck.util;

public class OneCheckConstants {

    public static final String OTP_GENERATE_URL                = "otp.generate.url";
    public static final String TOKEN                           = "token";
    public static final String USER_MACHINE_REQUEST_IDENTIFIER = "userMachineIdentifier";
    public static final String USER_AGENT                      = "userAgent";
    public static final String CONTENT_TYPE                    = "Content-Type";
    public static final String ACCEPT                          = "Accept";

    public static final String CLICKPAY_MERCHANT                   = "CLICKPAY";
    public static final int    API_TIMEOUT                         = 10000;
    public static final String OTP_VALIDATE_URL                    = "otp.validate.url";
    public static final String WALLET_BALANCE_URL                  = "wallet.balance.url";
    public static final String WALLET_BALANCE_LIMITS_URL           = "wallet.balance.limits.url";
    public static final String VERIFY_AND_WITHDRAW_URL             = "verify.and.withdraw.url";
    public static final String ADD_BANK_ACCOUNT_URL                = "add.bank.account.url";
    public static final String GET_BANK_ACCOUNTS_URL               = "bank.accounts.url";
    public static final String UPDATE_BANK_ACCOUNT_URL             = "update.bank.account.url";
    public static final String GET_USER_ADDRESSES_URL              = "get.user.addresses.url";
    public static final String ADD_ADDRESS_URL                     = "add.address.url";
    public static final String DELETE_ADDRESS_URL                  = "delete.address.url";
    public static final String INIT_CREDIT_IN_URL                  = "credit.init.url";
    public static final String LIST_CARDS_URL                      = "cards.list.url";
    public static final String DEL_CARD_URL                        = "cards.delete.url";
    public static final String GET_PAYMENT_MODES_URL               = "payments.mode.url";
    public static final String ONE_CHECK_TXN_DETAILS_URL           = "one.check.txn.details.url";
    public static final String ONE_CHECK_CASHBACK_TRANSACTION_TYPE = "CREDIT_CASHBACK";
    public static final String ONE_CHECK_REFUND_TRANSACTION_TYPE   = "CREDIT_REFUND";
    public static final String GET_SEED_URL                        = "seed.get.url";
    public static final String GET_COUNTER_URL                     = "seed.counter.url";
    public static final String ONE_CHECK_FREECHARGE_MERCHANT       = "FreeCharge";
    
	public static final String P2P_COMMENT                         = "comment";
	public static final String P2P_REQUEST_TIMESTAMP  		       = "request_timestamp";
	public static final String P2P_OTHER_PARTY_CONTACT_NAME        = "party_contact_name";
	public static final String P2P_OTHER_PARTY_MOBILE              = "party_mobile";
	public static final String P2P_OTHER_PARTY_TAG                 = "party_tag";

    public static final String EVENT_CONTEXT_TRANSACTION_P2P_REQUEST_MONEY = "P2P_REQUEST_MONEY";
    public static final String EVENT_CONTEXT_TRANSACTION_P2P_SEND_MONEY = "P2P_SEND_MONEY";
    public static final String IS_MONEY_RECEIVED = "is_money_received";
    public static final String TRANSACTION_DESCRIPTION = "description";
}
