package com.freecharge.rest.onecheck.util;

import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse;
import com.freecharge.rest.onecheck.model.WalletResponseDo;
import java.util.Map;

public class WalletErrorCodeUtil {

    private static final String ERROR_CODE_KEY = "errorCode";
    private static final String ERROR_MSG_KEY  = "errorMessage";

    public static void setErrorCodeForAPIError(Map<String, Object> errorResponseMap, WalletResponseDo responseDo) {
        // TODO: Map the error codes to the FCWalletErrorCodes.
        switch ((String) errorResponseMap.get(ERROR_CODE_KEY)) {
            case "OC-2000" :
                responseDo.setStatusCode(FCWalletErrorCode.E03.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E03.getMessage());
                break;
            case "OC-2001" :
                responseDo.setStatusCode(FCWalletErrorCode.E04.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E04.getMessage());
                break;
            case "OC-2002" :
                responseDo.setStatusCode(FCWalletErrorCode.E05.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E05.getMessage());
                break;
            case "OC-2004" :
                responseDo.setStatusCode(FCWalletErrorCode.E06.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E06.getMessage());
                break;
            case "OC-2008" :
                responseDo.setStatusCode(FCWalletErrorCode.E07.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E07.getMessage());
                break;
            case "OC-1030" :
            case "OC-2009" :
                responseDo.setStatusCode(FCWalletErrorCode.E08.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E08.getMessage());
                break;
            case "OC-2003" :
                responseDo.setStatusCode(FCWalletErrorCode.E09.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E09.getMessage());
                break;
            case "OC-2005" :
                responseDo.setStatusCode(FCWalletErrorCode.E10.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E10.getMessage());
                break;
            case "OC-2006" :
                responseDo.setStatusCode(FCWalletErrorCode.E11.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E11.getMessage());
                break;
            case "OC-2007" :
                responseDo.setStatusCode(FCWalletErrorCode.E10.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E10.getMessage());
                break;
            case "400" :
                responseDo.setStatusCode(FCWalletErrorCode.E12.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E12.getMessage());
                break;
            case "OC-2011" :
                responseDo.setStatusCode(FCWalletErrorCode.E13.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E13.getMessage());
                break;
            case "ER-2104":
                responseDo.setStatusCode(FCWalletErrorCode.E16.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E16.getMessage());
                break;
            case "OC-6000":
            	responseDo.setStatusCode(FCWalletErrorCode.E35.name());
            	responseDo.setErrorMessage(FCWalletErrorCode.E35.getMessage());
            case "OC_6004":
            	responseDo.setStatusCode(FCWalletErrorCode.E36.name());
            	responseDo.setErrorMessage(FCWalletErrorCode.E36.getMessage());
            	break;
            default:
                responseDo.setStatusCode(FCWalletErrorCode.E02.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E02.getMessage());
                break;
        }
    }

    public static void setWalletStatusCode(OneCheckRawResponse oneCheckRawResponse, WalletResponseDo responseDo) {
        switch (oneCheckRawResponse.getCallStatus()) {
            case Success:
                responseDo.setStatusCode(FCWalletErrorCode.E00.name());
                responseDo.setErrorMessage(FCWalletErrorCode.E00.getMessage());
                break;

            case InternalError:
            case HttpError:
                responseDo.setStatusCode(FCWalletErrorCode.INTERNAL_ERROR.name());
                responseDo.setErrorMessage(FCWalletErrorCode.INTERNAL_ERROR.getMessage());
                break;

            case APIError:
                setErrorCodeForAPIError(oneCheckRawResponse.getResponseMap(), responseDo);
                break;

            default:
                throw new IllegalStateException("Invalid callstatus code " + oneCheckRawResponse.getCallStatus().name());
        }
    }
}
