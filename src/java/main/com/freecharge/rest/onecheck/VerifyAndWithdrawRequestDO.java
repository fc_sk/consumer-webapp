package com.freecharge.rest.onecheck;

import java.util.UUID;

import org.codehaus.jackson.annotate.JsonProperty;

import com.freecharge.rest.onecheck.model.DeviceInformation;


public class VerifyAndWithdrawRequestDO {
    private String otpId;
    private String otp;
    private String accountToken;
    private String amount;
    
    @JsonProperty("idempotencyId")
    private String idempotencyId = UUID.randomUUID().toString();
    
    @JsonProperty("otpPurpose")
    private String otpPurpose = "MONEY_OUT";
    
    @JsonProperty("device_info")
    private DeviceInformation deviceInformation;
    
    @JsonProperty("visitId")
    private String visitId;
    
    public String getIdempotencyId() {
        return idempotencyId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpId() {
        return otpId;
    }

    public void setOtpId(String otpId) {
        this.otpId = otpId;
    }

    public String getOtpPurpose() {
        return otpPurpose;
    }
    
    public void setIdempotencyId(String idempotencyId) {
        this.idempotencyId = idempotencyId;
    }
    
    public void setOtpPurpose(String otpPurpose) {
        this.otpPurpose = otpPurpose;
    }
    
    public DeviceInformation getDeviceInformation() {
		return deviceInformation;
	}

	public void setDeviceInformation(DeviceInformation deviceInformation) {
		this.deviceInformation = deviceInformation;
	}

	public String getVisitId() {
		return visitId;
	}

	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}

}
