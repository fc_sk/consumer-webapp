package com.freecharge.rest.onecheck.address.controller;

import com.amazonaws.services.codedeploy.model.ErrorCode;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.onecheck.model.Address;
import com.freecharge.rest.onecheck.model.DeleteAddressRequestDO;
import com.freecharge.rest.onecheck.service.OneCheckAddressService;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/user/address/v1/*")
public class OneCheckAddressController {

    @Autowired
    private OneCheckAddressService oneCheckAddressService;

    @NoLogin
    @Csrf(exclude=true)
    @RequestMapping(value = "getaddresses", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getAddress(HttpServletRequest request) {
        return oneCheckAddressService.getUserAddresses(request);
    }

	@NoLogin
	@Csrf(exclude = true)
	@RequestMapping(value = "addaddress", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String addAddress(@RequestBody Address address, HttpServletRequest request) {
		String landLineNumber = "";
		String address2 = "";
		if (!FCUtil.isValidLandLineFormat(address.getLandline())) {
			address.setLandline(landLineNumber);
		}
		if (!FCUtil.isWelFormedFieldValue(address.getAddress2())) {
			address.setAddress2(address2);
		}

		if (FCUtil.isWelFormedName(address.getName()) && FCUtil.isWelFormedFieldValue(address.getAddress1())
				&& FCUtil.isWelFormedFieldValue(address.getCity()) && FCUtil.isWelFormedFieldValue(address.getState())
				&& FCUtil.isMobileNoFormat(address.getMobile()) && FCUtil.isPostalCodeFormat(address.getPostalCode())) {
			return oneCheckAddressService.addAddress(address, request);
		} else {
			Map<String, String> errorMap = new HashMap<>();
			errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
			errorMap.put("errorMessage", "Failed to save address.");
			return new Gson().toJson(errorMap);
		}
	}

    @NoLogin
    @Csrf(exclude=true)
    @RequestMapping(value = "deleteaddress", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String deleteAddress(@RequestBody DeleteAddressRequestDO deleteAddressRequestDO,
            HttpServletRequest request) {
        return oneCheckAddressService.deleteAddress(deleteAddressRequestDO, request);
    }

}
