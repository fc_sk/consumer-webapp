package com.freecharge.rest.onecheck.model;

public enum PaymentType {
    Card("1"),
    SavedCard("2"),
    NetBanking("3"),
    UnkownPaymentType("4");
    
    private final String paymentTypeCode;
    
    private PaymentType(String code) {
        this.paymentTypeCode = code;
    }
    
    public static PaymentType fromCode(String code) {
        switch (code) {
            case "1": return Card;
            case "2": return SavedCard;
            case "3": return NetBanking;
            default: return UnkownPaymentType;
        }
    }
    
}
