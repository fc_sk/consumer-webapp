package com.freecharge.rest.onecheck.model;

public class CustomerCard {
    private String cardToken;
    private String cardFingerprint;
    private String cardReference;
    private String cardMask;
    private String nickName;
    private String cardIssuer;
    private String cardBrand;
    private String cardNumber;
    private boolean isDebitAtmEnabled;
    private Boolean isAllowedForPurpose;
    private String notAllowedReason;
    private String cardType;

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getCardFingerprint() {
        return this.cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public String getCardReference() {
        return cardReference;
    }

    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(String cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getCardMask() {
        return cardMask;
    }

    public void setCardMask(String cardMask) {
        this.cardMask = cardMask;
    }

    public boolean isDebitAtmEnabled() {
        return this.isDebitAtmEnabled;
    }

    public void setDebitAtmEnabled(boolean isDebitAtmEnabled) {
        this.isDebitAtmEnabled = isDebitAtmEnabled;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

   public Boolean isAllowedForPurpose() {
      return isAllowedForPurpose;
   }

   public void setIsAllowedForPurpose(Boolean isAllowedForPurpose) {
      this.isAllowedForPurpose = isAllowedForPurpose;
   }

   public String getNotAllowedReason() {
      return notAllowedReason;
   }

   public void setNotAllowedReason(String notAllowedReason) {
      this.notAllowedReason = notAllowedReason;
   }
   
   public String getCardType() {
      return cardType;
   }
   
   public void setCardType(String cardType) {
      this.cardType = cardType;
   }
}
