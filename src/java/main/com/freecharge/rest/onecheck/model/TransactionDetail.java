package com.freecharge.rest.onecheck.model;

import java.util.Date;

/**
 * Created by vishal on 17/11/15.
 */
public class TransactionDetail {
    private String context;
    private String status;
    private String createTime;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

	@Override
	public String toString() {
		return "TransactionDetail [context=" + context + ", status=" + status + ", createTime=" + createTime + "]";
	}
    
    
}
