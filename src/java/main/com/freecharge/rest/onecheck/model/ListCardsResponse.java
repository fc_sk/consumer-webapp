package com.freecharge.rest.onecheck.model;

import java.util.List;

public class ListCardsResponse extends WalletResponseDo {
    private List<CustomerCard> cards;

    public List<CustomerCard> getCards() {
        return cards;
    }

    public void setCards(List<CustomerCard> cards) {
        this.cards = cards;
    }
}
