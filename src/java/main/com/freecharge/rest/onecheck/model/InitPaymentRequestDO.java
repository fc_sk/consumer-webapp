package com.freecharge.rest.onecheck.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class InitPaymentRequestDO {
    @JsonProperty("txn_amount")
    private Double txnAmount;

    @JsonProperty("req_channel")
    private String reqChannel;

    @JsonProperty("source")
    private String requestSource;

    @JsonProperty("product_info")
    private final String productInfo = "CREDIT_WALLET";

    @JsonProperty("currency")
    private final String currency = "INR";
    
    @JsonProperty("redirect_url")
    private String redirectUrl;

    @JsonProperty("failure_url")
    private String failureUrl;
    
    @JsonProperty("payment_details")
    private PaymentDetails paymentDetails;
    
    @JsonProperty("device_info")
    private DeviceInformation deviceInformation;
     
    @JsonProperty("user_id")
    private String userId;
    
    @JsonProperty("timestamp")
    private long timeStamp;

    @JsonProperty("session_id")
	private String sessionId;
    
    @JsonProperty("customer_name")
	private String customerName;
    
    @JsonProperty("is_promo_code")
    private boolean isPromoCode;
    
    @JsonProperty("visitId")
    private String visitId;

    @JsonProperty("isGVLoad")
    private boolean GVLoadMoneyRequest;

	public boolean isPromoCode() {
		return isPromoCode;
	}

	public void setPromoCode(boolean isPromoCode) {
		this.isPromoCode = isPromoCode;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public DeviceInformation getDeviceInformation() {
		return deviceInformation;
	}

	public void setDeviceInformation(DeviceInformation deviceInformation) {
		this.deviceInformation = deviceInformation;
	}

    public String getCurrency() {
        return currency;
    }
    
    public String getFailureUrl() {
        return failureUrl;
    }
    public void setFailureUrl(String failureUrl) {
        this.failureUrl = failureUrl;
    }
    
    public String getRedirectUrl() {
        return redirectUrl;
    }
    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
    
    public String getProductInfo() {
        return productInfo;
    }
    
    public Double getTxnAmount() {
        return txnAmount;
    }
    
    public void setTxnAmount(Double txnAmount) {
        this.txnAmount = txnAmount;
    }
    
    public String getReqChannel() {
        return reqChannel;
    }
    
    public void setReqChannel(String reqChannel) {
        this.reqChannel = reqChannel;
    }
    
    public String getSource() {
        return requestSource;
    }
    public void setSource(String source) {
        this.requestSource = source;
    }
    
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public String getSessionId() {
        return sessionId;
    }
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCustomerName() {
         return customerName;	
    }
    
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getVisitId() {
		return visitId;
	}

	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	
	public boolean isGVLoadMoneyRequest() {
      return GVLoadMoneyRequest;
   }
	
	public void setIsGVLoadMoneyRequest(boolean isGVLoadMoneyRequest) {
      GVLoadMoneyRequest = isGVLoadMoneyRequest;
   }
}
