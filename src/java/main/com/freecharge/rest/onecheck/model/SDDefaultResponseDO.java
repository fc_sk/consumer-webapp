package com.freecharge.rest.onecheck.model;


import java.util.Map;

public class SDDefaultResponseDO {
    private Map<String, Object> error;
    private Map<String, Object> data;

    public Map<String, Object> getError() {
        return error;
    }

    public void setError(Map<String, Object> error) {
        this.error = error;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
