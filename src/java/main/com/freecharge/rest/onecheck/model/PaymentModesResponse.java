package com.freecharge.rest.onecheck.model;

import java.util.List;

public class PaymentModesResponse extends WalletResponseDo {

    private String             customerId;
    private List<CustomerCard> cards;
    private List<String>       netBankingModes;

    public String getCustomerId() {
        return customerId;
    }

    public List<CustomerCard> getCards() {
        return cards;
    }

    public List<String> getNetBankingModes() {
        return netBankingModes;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setCards(List<CustomerCard> cards) {
        this.cards = cards;
    }

    public void setNetBankingModes(List<String> netBankingModes) {
        this.netBankingModes = netBankingModes;
    }
}
