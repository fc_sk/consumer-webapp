package com.freecharge.rest.onecheck.model;

public class GetSeedRequestDO {
    private String imei;
    private String appVersion;
    private String osVersion;
    private String os;
    private String advId;
    private String imsi;
    
    public String getImei() {
        return imei;
    }
    public void setImei(String imei) {
        this.imei = imei;
    }
    
    public String getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    
    public String getOsVersion() {
        return osVersion;
    }
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }
    
    public String getOs() {
        return os;
    }
    public void setOs(String os) {
        this.os = os;
    }
    
    public String getAdvId() {
        return advId;
    }
    public void setAdvId(String advId) {
        this.advId = advId;
    }
    
    public String getImsi() {
        return imsi;
    }
    public void setImsi(String imsi) {
        this.imsi = imsi;
    }
}
