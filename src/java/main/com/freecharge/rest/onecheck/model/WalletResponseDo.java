package com.freecharge.rest.onecheck.model;

public class WalletResponseDo {
    private String statusCode;
    private String errorMessage;
    private String transactionId;

    public String getStatusCode() {
        return statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void settransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String gettransactionId() {
        return transactionId;
    }
}
