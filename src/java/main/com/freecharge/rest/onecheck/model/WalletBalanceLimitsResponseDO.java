package com.freecharge.rest.onecheck.model;


public class WalletBalanceLimitsResponseDO {
    private String transferLimit;
    private String balanceLimit;

    public String getTransferLimit() {
        return transferLimit;
    }

    public void setTransferLimit(String transferLimit) {
        this.transferLimit = transferLimit;
    }

    public String getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(String balanceLimit) {
        this.balanceLimit = balanceLimit;
    }
}
