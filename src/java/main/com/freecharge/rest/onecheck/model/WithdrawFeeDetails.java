package com.freecharge.rest.onecheck.model;

public class WithdrawFeeDetails {
	private WithdrawFee withdrawFee;
	private WithdrawAmount withdrawAmount;
	
	public WithdrawFee getWithdrawFee() {
		return withdrawFee;
	}
	public void setWithdrawFee(WithdrawFee withdrawFee) {
		this.withdrawFee = withdrawFee;
	}
	public WithdrawAmount getWithdrawAmount() {
		return withdrawAmount;
	}
	public void setWithdrawAmount(WithdrawAmount withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}
	
	@Override
	public String toString() {
		return "WithdrawFeeDetails [withdrawFee=" + withdrawFee + ", withdrawAmount=" + withdrawAmount + "]";
	}
	
}
