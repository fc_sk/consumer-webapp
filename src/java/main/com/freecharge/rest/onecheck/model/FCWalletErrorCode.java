package com.freecharge.rest.onecheck.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;


/**
 * Enum encapsulating error codes specific to FC Wallet API.
 * 
 * @author arun
 *
 */
public enum FCWalletErrorCode {

    E00("Success"), 
    INTERNAL_ERROR("Internal error"), 
    E02("API Error"), 
    E03("Wallet request Error"),
    E04("Input Validation Error"), 
    E05("Invalid/Blocked User"), 
    E06("Insufficient funds"),
    E07("OTP verification failed"), 
    E08("Unauthorised action"), 
    E09("Wallet limit exceeded"), 
    E10("Voucher not found"),
    E11("Invalid date range"), 
    E12("User has already 2 accounts associated with his identity"), 
    E13("Bank account already added"),
    E14("Card number is absent"),
    E15("Saved card token is absent"),
    E16("Net banking code is absent"),
    E17("Token is absent"),
    E18("Global token is expired"), 
    E19("Requested useraddressId does not exist"),
    E20("Minimum amount for bank transfer is Rs.100. Please enter an amount greater than that."),
    E21("Please enter valid OTP or regenerate the OTP by going back"),
    E22("OTP ID is invalid"),
    E23("Account number is absent"),
    E24("Bank name is absent"),
    E25("IFSC code is absent"),
    E26("Nick name is absent"),
    E27("Instrument not found in Database"),
    E28("Account has already been used by 2 users"),
    E29("Unknown payment type"),
	E30("Invalid bank account number"),
	E31("Invalid IFSC code"),
    E32("Invalid Amount"),
    E33("This payment method is not accepted for adding cash to wallet. Please use an alternate payment method."),
    E34("Card Validation failure"),
	E35("Internal Server Error in Payments Service"),
	E36("User is not allowed to perform this transaction"), 
	E37("You cannot exceed the maximum allowed wallet balance"),
	E38("We encountered an error in processing the request. Please try again"),
	E39("Invalid Short Name"),
	E40("Invalid Bank Name"),
    E41("Withdrawal facility will be available 48 hours after the addition of bank account");
	
    private final String message;

    FCWalletErrorCode(String msg) {
        this.message = msg;
    }

    public String getMessage() {
        return this.message;
    }
    
    public WalletResponseDo toWalletResponse() {
        WalletResponseDo errorResponse = new WalletResponseDo();
        errorResponse.setStatusCode(this.name());
        errorResponse.setErrorMessage(this.message);
        
        return errorResponse;
    }
    
    public Map<String, String> toErrorMap() {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("statusCode", this.name());
        errorMap.put("errorMessage", this.message);
        
        return errorMap;
        
    }
    
    public static FCWalletErrorCode fromOCErrorCode(String ocErrorCode) {
        switch (ocErrorCode) {
            case "OC-2000" : return E03;
            case "OC-2001" : return E04;
            case "OC-2002" : return E05;
            case "OC-2004" : return E06;
            case "OC-2008" : return E07;
            case "OC-1030" :
            case "ER-2104" :
            case "OC-2009" : return E08;
            case "OC-2003" : return E09;
            case "OC-2005" : return E10;
            case "OC-2006" : return E11;
            case "OC-2007" : return E10;
            case "400"     : return E12;
            case "OC-2011" : return E13;
            case "ER-4110" : return E17;
            case "ER-2103" : return E18;
            case "508"     : return E19;
            case "ER-5106" : return E21;
            case "ER-5108" : return E22;
            case "403"     : return E27;
            case "449"	   : return E41;
            default        : return E02;
        }
    }
    
    public static FCWalletErrorCode fromOCAPIErrorCode(String ocErrorCode, String api) {
        // Ugly if/else for to handle API specific error code 400.
        if (StringUtils.equals("VerifyAndWithdraw", api)) {
            switch (ocErrorCode) {
                case "400"  : return E20;
                default     : return fromOCErrorCode(ocErrorCode);
            }
        } else if (StringUtils.equals("addBankAccount", api)) {
            switch (ocErrorCode) {
                case "400"  : return E28;
                default     : return fromOCErrorCode(ocErrorCode);
            }
        }
        
        return INTERNAL_ERROR;
    }
    
    public static String fromOCErrorCodeToJson(String ocErrorCode) {
        return fromOCErrorCode(ocErrorCode).toJson();
    }
    
    public String toJson() {
        return new Gson().toJson(this.toErrorMap());
    }
}
