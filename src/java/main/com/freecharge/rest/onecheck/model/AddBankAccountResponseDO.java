package com.freecharge.rest.onecheck.model;


public class AddBankAccountResponseDO {
    private String accountToken;

    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }
}
