package com.freecharge.rest.onecheck.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class PaymentDetails {

	@JsonProperty("cardHash")
	private String cardHash;
	
	@JsonProperty("cardBin")
	private String cardBin;
	
	@JsonProperty("@class")
	private String name;
	
	@JsonProperty("paymentMode")
	private String paymentMode;
	
	@JsonProperty("paymentIdentifier")
	private String paymentIdentifier;
	
	public String getPaymentIdentifier() {
		return paymentIdentifier;
	}
	public void setPaymentIdentifier(String paymentIdentifier) {
		this.paymentIdentifier = paymentIdentifier;
	}
	public String getCardHash() {
		return cardHash;
	}
	public void setCardHash(String cardHash) {
		this.cardHash = cardHash;
	}
	public String getCardBin() {
		return cardBin;
	}
	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	} 
	
}
	

