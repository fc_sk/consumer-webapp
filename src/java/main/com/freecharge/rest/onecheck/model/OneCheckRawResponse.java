package com.freecharge.rest.onecheck.model;

import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.onecheck.util.OneCheckConstants;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONObject;

public class OneCheckRawResponse {
    private static TypeReference<HashMap<String, HashMap<String, Object>>> typeRef = new TypeReference<HashMap<String, HashMap<String, Object>>>() {
    };
    private static ObjectMapper                                            mapper  = new ObjectMapper();

    public static enum CallStatus {
        Success, APIError, InternalError, HttpError
    }

    private Map<String, Object> responseMap;
    private int                 httpStatus;
    private CallStatus          callStatus;

    private OneCheckRawResponse() {
    }

    /**
     * Response is in the form of “data”: {...}
     *
     * @param parsableResponse
     * @return
     */
    public static OneCheckRawResponse successfulResponse(String parsableResponse) {
        try {
            Map<String, HashMap<String, Object>> responseMap = mapper.readValue(parsableResponse, typeRef);

            OneCheckRawResponse responseObject = new OneCheckRawResponse();
            responseObject.httpStatus = HttpStatus.SC_OK;

            responseObject.responseMap = responseMap.get("data");
            if (!FCUtil.isEmpty(responseObject.responseMap)) {
                responseObject.callStatus = CallStatus.Success;
                return responseObject;
            }
            responseObject.responseMap = responseMap.get("error");
            responseObject.callStatus = CallStatus.APIError;
            return responseObject;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static OneCheckRawResponse httpErrorResponse(int httpStatus) {
        OneCheckRawResponse errorResponse = new OneCheckRawResponse();
        errorResponse.httpStatus = httpStatus;
        errorResponse.callStatus = CallStatus.HttpError;

        return errorResponse;
    }

    public static OneCheckRawResponse internalErrorResponse() {
        OneCheckRawResponse errorResponse = new OneCheckRawResponse();
        errorResponse.callStatus = CallStatus.InternalError;

        return errorResponse;
    }

    public static OneCheckRawResponse mockResponse(final String apiName) {
        String response = "";
        switch (apiName) {
            case OneCheckConstants.INIT_CREDIT_IN_URL:
                Map<String, Object> responseMap = new HashMap<String, Object>();
                responseMap.put("pg_url", "https://fcpay.freecharge.in");
                responseMap.put("checksum", "abcde");
                responseMap.put("currency", "INR");
                responseMap.put("txn_id", "27365792695");
                responseMap.put("redirect_url", "https://freecharge-test.in");
                responseMap.put("txn_amount", (Double) 10.0);
                responseMap.put("req_channel", "WEB");
                Map<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("data", new JSONObject(responseMap));
                return OneCheckRawResponse.successfulResponse(new JSONObject(dataMap).toString());

            case OneCheckConstants.LIST_CARDS_URL:
                response = "{  \"data\": {  \"customerId\": \"13\","
                        + "\"cards\": [    {\"card_token\": \"685be0d8-555f-4641-916e-898235c54573\","
                        + "   \"card_reference\": \"55d7fb028624b02fdfa8d6145fbee122\",     "
                        + "  \"card_fingerprint\": \"\",     \"card_mask\": \"4572-XXXXXXXX-0372\",   \"card_isin\": \"457274\","
                        + "   \"card_type\": \"DEBIT\",\"card_issuer\": "
                        + " \"ICICI BANK\", \"card_brand\": \"VISA\",\"nickname\": \"null\","
                        + "   \"expired\": null       },      {\"card_token\": \"d3828507-01c3-498e-872c-20149c35b4df\",   "
                        + "   \"card_reference\": \"6c0ffe8e50ae605d26cb6882f4d7d56f\",\"card_fingerprint\": \"\",   "
                        + "  \"card_mask\": \"4363-XXXXXXXX-7052\","
                        + "      \"card_isin\": \"436303\",      \"card_type\": \"DEBIT\",\"card_issuer\": \"HDFC BANK\",     "
                        + "    \"card_brand\": \"VISA\",\"nickname\": \"null\",  \"expired\": null       }       ]  }}";
                return OneCheckRawResponse.successfulResponse(response);

            case OneCheckConstants.DEL_CARD_URL:
                response = "{\"data\": {\"card_reference\":\"a1a26720-069e-4e7f-9f0b-8d3e4207608b\",  "
                        + "\"card_token\":\"a1a26720-069e-4e7f-9f0b-8d3e4207608b\", \"deleted\":true}}";
                return OneCheckRawResponse.successfulResponse(response);

            case OneCheckConstants.GET_PAYMENT_MODES_URL:
                response = "{ \"error\": null, \"data\": {  \"customer_id\": \"1234\", "
                        + " \"cards\": [{ \"card_token\": \"986537997321\", "
                        + " \"card_reference\": \"930sdksdf\", \"card_fingerprint\": "
                        + " \"9ksf83ls-dk230\", \"card_mask\": \"4171-XXXX-XXXX-0897\", "
                        + " \"card_isin\": \"ASD0L3\", \"card_type\": \"DEBIT\", \"card_issuer\": "
                        + " \"ICICI\", \"card_brand\": \"VISA\", \"nickname\":  "
                        + "\"Joe\", \"expired\": \"06/26\"}  ],  \"netbanking_modes\": "
                        + " [\"ICIC\",\"HDFC\",\"AXIS\"  ] }}";
                return OneCheckRawResponse.successfulResponse(response);

            case OneCheckConstants.ONE_CHECK_TXN_DETAILS_URL:
                response = "{\"error\": null, \"data\": {\"transactionid\": \"OC-229418086-1444117827201\", "
                        + " \"details\": [{\"context\": \"PG_RESPONSE_HANDLING\", "
                        + " \"status\": \"FAILED TO GET TASK DETAILS\", "
                        + " \"createTime\": \"06/10/2015 13:23:18\"}]}}";
                return OneCheckRawResponse.successfulResponse(response);

            case OneCheckConstants.GET_BANK_ACCOUNTS_URL:
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "    \"userAccounts\": [" +
                        "      {" +
                        "        \"accountToken\": \"7ca68812-1261-4920-8c88-685d96bac76f\"," +
                        "        \"shortName\": \"acvava\"," +
                        "        \"accountNumber\": \"0023019227\"," +
                        "        \"bankName\": \"HDFC\"," +
                        "        \"ifsc\": \"HDFC00000077\"," +
                        "        \"addedOn\": 1437071400000," +
                        "        \"status\": \"OPEN\"," +
                        "        \"vrfnRefId\": null," +
                        "        \"statusModifiedDate\": null," +
                        "        \"verified\": false" +
                        "      }," +
                        "      {" +
                        "        \"accountToken\": \"fa51681e-b9b1-4ab9-8aea-3cd70512ca4b\"," +
                        "        \"shortName\": \"adasdad\"," +
                        "        \"accountNumber\": \"023423400001928227\"," +
                        "        \"bankName\": \"HDFC\"," +
                        "        \"ifsc\": \"HDFC00000077\"," +
                        "        \"addedOn\": 1438021800000," +
                        "        \"status\": \"OPEN\"," +
                        "        \"vrfnRefId\": null," +
                        "        \"statusModifiedDate\": null," +
                        "        \"verified\": false" +
                        "      }" +
                        "    ]" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.UPDATE_BANK_ACCOUNT_URL:
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "    \"bankAccountDetails\": {" +
                        "      \"accountToken\": \"6981ba35-273d-4317-a507-6a86906b0572\"," +
                        "      \"shortName\": \"acvava\"," +
                        "      \"accountNumber\": \"0023019227\"," +
                        "      \"bankName\": \"HDFC\"," +
                        "      \"ifsc\": \"HDFC00000077\"," +
                        "      \"addedOn\": 1438108200000," +
                        "      \"status\": \"OPEN\"," +
                        "      \"vrfnRefId\": null," +
                        "      \"statusModifiedDate\": null," +
                        "      \"verified\": false" +
                        "    }" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.GET_USER_ADDRESSES_URL :
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "    \"addresses\": [" +
                        "      {" +
                        "        \"id\": 5533," +
                        "        \"userId\": 567804," +
                        "        \"name\": \"my address1\"," +
                        "        \"address1\": \"addrss 11\"," +
                        "        \"address2\": \"address 21\"," +
                        "        \"city\": \"noida\"," +
                        "        \"state\": \"up\"," +
                        "        \"country\": \"india\"," +
                        "        \"postalCode\": \"201301\"," +
                        "        \"mobile\": \"9999999999\"," +
                        "        \"landline\": \"92304324\"," +
                        "        \"created\": 1435892887000," +
                        "        \"updated\": 1435892887000," +
                        "        \"addressTag\": \"home\"," +
                        "        \"isDefault\": false," +
                        "        \"isActive\": true," +
                        "        \"status\": \"UNKNOWN\"" +
                        "      }," +
                        "      {" +
                        "        \"id\": 5530," +
                        "        \"userId\": 567804," +
                        "        \"name\": \"my address\"," +
                        "        \"address1\": \"addrss 1\"," +
                        "        \"address2\": \"address 2\"," +
                        "        \"city\": \"noida\"," +
                        "        \"state\": \"up\"," +
                        "        \"country\": \"india\"," +
                        "        \"postalCode\": \"201301\"," +
                        "        \"mobile\": \"9999999999\"," +
                        "        \"landline\": \"92304324\"," +
                        "        \"created\": 1435846600000," +
                        "        \"updated\": 1435892823000," +
                        "        \"addressTag\": \"home\"," +
                        "        \"isDefault\": false," +
                        "        \"isActive\": true," +
                        "        \"status\": \"UNKNOWN\"" +
                        "      }" +
                        "    ]" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.ADD_ADDRESS_URL :
                response = "{" +
                        "    \"error\": null," +
                        "    \"data\": {" +
                        "        \"savedAddressId\": 5530" +
                        "    }" +
                        "}";
                OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.DELETE_ADDRESS_URL :
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "    \"message\": \"Successfully deleted address\"" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.OTP_GENERATE_URL :
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "    \"otpId\": \"303a83c7-cc50-432d-a3b5-4673cf48eb06\"" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.OTP_VALIDATE_URL :
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "  \"status\":\"Success\"," +
                        "   \"message\":\"verification done\"" +
                        "  }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.WALLET_BALANCE_URL:
                response = "{" +
                        "  \"error\": null," +
                        "  \"data\": {" +
                        "  \"totalBalance\":\"3425\"," +
                        "   \"voucherBalance\":\"1000\"," +
                        "   \"generalBalance\":\"2425\"" +
                        "   }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.WALLET_BALANCE_LIMITS_URL :
                response = "{" +
                        "    \"error\": null," +
                        "    \"data\": { " +
                        "        \"transferLimit\": 2000," +
                        "        \"balanceLimit\": 9000" +
                        "    }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            case OneCheckConstants.VERIFY_AND_WITHDRAW_URL:
                response = "{" +
                        "    \"error\": null," +
                        "    \"data\": {" +
                        "    \"transactionId\": \"STUB___TXN:2233323232408NXCCC765445456544\"" +
                        "    }" +
                        "}";
                return OneCheckRawResponse.successfulResponse(response);
            default:
                throw new IllegalStateException("Invalid API: " + apiName);

        }
    }

    public CallStatus getCallStatus() {
        return callStatus;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public Map<String, Object> getResponseMap() {
        return responseMap;
    }
}
