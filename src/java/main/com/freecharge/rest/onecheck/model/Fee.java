package com.freecharge.rest.onecheck.model;

import java.math.BigDecimal;

public class Fee {
	private String name;
	private BigDecimal amount;
	
	@Override
	public String toString() {
		return "Fee [name=" + name + ", amount=" + amount + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
