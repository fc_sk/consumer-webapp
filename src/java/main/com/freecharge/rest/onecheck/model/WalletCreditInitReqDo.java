package com.freecharge.rest.onecheck.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class WalletCreditInitReqDo {

    @JsonProperty("txn_amount")
    private Double txnAmount;

    @JsonProperty("req_channel")
    private String reqChannel;

    @JsonProperty("req_source")
    private String source;
    
    /*
     * Card = 1
     * Saved Card = 2
     * Net Banking = 3
     */
    @JsonProperty("payment_type")
    private String paymentType;
    
    @JsonProperty("payment_option")
    private String paymentOption;
    
    @JsonProperty("card_num")
    private String cardNumber;
    
    @JsonProperty("name_on_card")
    private String nameOnCard;
    
    @JsonProperty("card_exp_month")
    private String cardExpiryMonth;
    
    @JsonProperty("card_exp_year")
    private String cardExpiryYear;
    
    @JsonProperty("card_cvv")
    private String cardCvv;
    
    @JsonProperty("saved_card_token")
    private String savedCardToken;
    
    @JsonProperty("card_reference")
    private String cardReference;
    
    @JsonProperty("visitId")
    private String visitId;

    @JsonProperty("gv_load_money_request")
    private boolean gvLoadMoneyRequest;
    
    public Double getTxnAmount() {
        return txnAmount;
    }

    public String getSource() {
        return source;
    }

    public String getReqChannel() {
        return reqChannel;
    }

    public void setTxnAmount(Double txnAmount) {
        this.txnAmount = txnAmount;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setReqChannel(String reqChannel) {
        this.reqChannel = reqChannel;
    }
    
    public String getPaymentType() {
        return paymentType;
    }
    
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public String getPaymentOption() {
        return paymentOption;
    }
    
    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }
    
    public String getCardNumber() {
        return cardNumber;
    }
    
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getCardCvv() {
        return cardCvv;
    }
    
    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }
    
    public String getCardExpiryMonth() {
        return cardExpiryMonth;
    }
    
    public void setCardExpiryMonth(String cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }
    
    public String getCardExpiryYear() {
        return cardExpiryYear;
    }
    
    public void setCardExpiryYear(String cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }
    
    public String getSavedCardToken() {
        return savedCardToken;
    }
    
    public void setSavedCardToken(String savedCardToken) {
        this.savedCardToken = savedCardToken;
    }
    
    public String getNameOnCard() {
        return nameOnCard;
    }
    
    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
    
    public String getCardReference() {
        return cardReference;
    }
    
    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

	public String getVisitId() {
		return visitId;
	}

	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}

	public boolean isGvLoadMoneyRequest() {
      return gvLoadMoneyRequest;
   }
	
   public void setIsGVLoadMoneyRequest(boolean isGVLoadMoneyRequest) {
      this.gvLoadMoneyRequest = isGVLoadMoneyRequest;
      
   }
    
}
