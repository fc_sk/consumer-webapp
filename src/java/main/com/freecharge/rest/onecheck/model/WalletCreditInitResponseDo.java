package com.freecharge.rest.onecheck.model;

public class WalletCreditInitResponseDo extends WalletResponseDo {

    private String checksum;
    private String pgUrl;
    private String txnId;
    private String redirectUrl;
    private Double txnAmount;
    private String currency;
    private String reqChannel;
    private String merchantId;
    private String userId;

    public String getChecksum() {
        return checksum;
    }

    public String getPgUrl() {
        return pgUrl;
    }

    public String getTxnId() {
        return txnId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public Double getTxnAmount() {
        return txnAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getReqChannel() {
        return reqChannel;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public void setPgUrl(String pgUrl) {
        this.pgUrl = pgUrl;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setTxnAmount(Double txnAmount) {
        this.txnAmount = txnAmount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setReqChannel(String reqChannel) {
        this.reqChannel = reqChannel;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
