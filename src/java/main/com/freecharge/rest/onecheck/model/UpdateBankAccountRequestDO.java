package com.freecharge.rest.onecheck.model;


public class UpdateBankAccountRequestDO {
    private AccountDetailsDO bankAccountDetails;

    public AccountDetailsDO getBankAccountDetails() {
        return bankAccountDetails;
    }

    public void setBankAccountDetails(AccountDetailsDO bankAccountDetails) {
        this.bankAccountDetails = bankAccountDetails;
    }
}
