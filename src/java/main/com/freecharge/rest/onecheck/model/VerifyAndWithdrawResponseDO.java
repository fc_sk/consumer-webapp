package com.freecharge.rest.onecheck.model;


public class VerifyAndWithdrawResponseDO {
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
