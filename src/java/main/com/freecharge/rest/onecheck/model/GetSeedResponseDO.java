package com.freecharge.rest.onecheck.model;



public class GetSeedResponseDO {
    private String seed;
    private long validUpto;
    private boolean rsaEnabled;
    
    public String getSeed() {
        return seed;
    }
    public void setSeed(String seed) {
        this.seed = seed;
    }
    
    public long getValidUpto() {
        return validUpto;
    }
    public void setValidUpto(long validUpto) {
        this.validUpto = validUpto;
    }
    public boolean isRsaEnabled() {
        return rsaEnabled;
    }
    public void setRsaEnabled(boolean rsaEnabled) {
        this.rsaEnabled = rsaEnabled;
    }
}
