package com.freecharge.rest.onecheck.model;

import java.util.List;

/**
 * Created by vishal on 17/11/15.
 */
public class OneCheckTransactionDetails {
    private String                  transactionId;
    private String 					userId;
    private Double 					amount;
    private List<TransactionDetail> details;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<TransactionDetail> getDetails() {
        return details;
    }

    public void setDetails(List<TransactionDetail> details) {
        this.details = details;
    }
    
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "OneCheckTransactionDetails [transactionId=" + transactionId + ", userId=" + userId + ", amount="
				+ amount + ", details=" + details + "]";
	}
	
}
