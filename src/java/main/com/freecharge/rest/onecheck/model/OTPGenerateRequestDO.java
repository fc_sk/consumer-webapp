package com.freecharge.rest.onecheck.model;

import org.codehaus.jackson.annotate.JsonProperty;


public class OTPGenerateRequestDO {
    private String emailId;
    private String mobileNumber;

    @JsonProperty("purpose")
    private final String purpose = "MONEY_OUT";

    public String getPurpose() {
        return purpose;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
