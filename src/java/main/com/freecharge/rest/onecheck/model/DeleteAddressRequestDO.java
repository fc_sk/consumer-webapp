package com.freecharge.rest.onecheck.model;


public class DeleteAddressRequestDO {
    private String userAddressId;

    public String getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(String userAddressId) {
        this.userAddressId = userAddressId;
    }
}
