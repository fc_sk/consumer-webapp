package com.freecharge.rest.onecheck.model;

public class UpdateBankAccountResponseDO {
    private String accountToken;
    private String shortName;
    private String accountNumber;
    private String bankName;
    private String ifsc;
    
    public String getAccountToken() {
        return accountToken;
    }
    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }
    public String getShortName() {
        return shortName;
    }
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getBankName() {
        return bankName;
    }
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getIfsc() {
        return ifsc;
    }
    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }
}
