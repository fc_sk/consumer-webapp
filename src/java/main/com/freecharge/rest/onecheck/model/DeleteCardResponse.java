package com.freecharge.rest.onecheck.model;

public class DeleteCardResponse extends WalletResponseDo {

    private String  cardToken;
    private String  cardReference;
    private boolean deleted;

    public String getCardToken() {
        return cardToken;
    }

    public String getCardReference() {
        return cardReference;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public void setCardReference(String cardReference) {
        this.cardReference = cardReference;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
