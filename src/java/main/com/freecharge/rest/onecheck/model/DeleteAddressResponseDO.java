package com.freecharge.rest.onecheck.model;


public class DeleteAddressResponseDO {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
