package com.freecharge.rest.onecheck.model;


public class GetCounterResponseDO {
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
