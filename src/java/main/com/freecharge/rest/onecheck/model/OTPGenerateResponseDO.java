package com.freecharge.rest.onecheck.model;


public class OTPGenerateResponseDO {
    private String otpId;

    public String getOtpId() {
        return otpId;
    }

    public void setOtpId(String otpId) {
        this.otpId = otpId;
    }
}
