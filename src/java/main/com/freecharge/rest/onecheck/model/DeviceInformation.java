package com.freecharge.rest.onecheck.model;

import org.codehaus.jackson.annotate.JsonProperty;

public class DeviceInformation {
	
	@JsonProperty("osName")
	private String osName;
	
	@JsonProperty("referer")
	private String referer;
	
	@JsonProperty("host")
	private String host;
	
	@JsonProperty("ipAddress")
	private String ipAddress;
	
	@JsonProperty("imei")
	private String iMei;
	
	@JsonProperty("advertisementId")
	private String advertisementId;
	
	@JsonProperty("clientId")
	private String clientId;
	
	public String getiMei() {
		return iMei;
	}
	public void setiMei(String iMei) {
		this.iMei = iMei;
	}
	public String getOsName() {
		return osName;
	}
	public void setOsName(String osName) {
		this.osName = osName;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getAdvertisementId() {
		return advertisementId;
	}
	public void setAdvertisementId(String advertisementId) {
		this.advertisementId = advertisementId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
}
