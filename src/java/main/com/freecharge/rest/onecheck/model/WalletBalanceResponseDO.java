package com.freecharge.rest.onecheck.model;


public class WalletBalanceResponseDO {
    private String totalBalance;
    private String voucherBalance;
    private String generalBalance;
    private String withdrawLimit;

    public String getGeneralBalance() {
        return generalBalance;
    }

    public void setGeneralBalance(String generalBalance) {
        this.generalBalance = generalBalance;
    }

    public String getVoucherBalance() {
        return voucherBalance;
    }

    public void setVoucherBalance(String voucherBalance) {
        this.voucherBalance = voucherBalance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getWithdrawLimit() {
        return withdrawLimit;
    }

    public void setWithdrawLimit(String withdrawLimit) {
        this.withdrawLimit = withdrawLimit;
    }
}
