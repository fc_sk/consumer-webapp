package com.freecharge.rest.onecheck.model;


public class AddAddressResponseDO {
    private String savedAddressId;

    public String getSavedAddressId() {
        return savedAddressId;
    }

    public void setSavedAddressId(String savedAddressId) {
        this.savedAddressId = savedAddressId;
    }
}
