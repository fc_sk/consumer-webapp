package com.freecharge.rest.onecheck.model;

import java.util.List;

public class WithdrawFee extends Fee {
	
	private WithdrawFeeBreakup withdrawFeeBreakup;
	
	static public class WithdrawFeeBreakup {
		
		private FixedFee fixedFee;
		private RelativeFee relativeFee;
		
		static public class FixedFee extends Fee {

			@Override
			public String toString() {
				return "FixedFee [toString()=" + super.toString() + "]";
			}
			
		}
		
		static public class RelativeFee extends Fee{
			private List<Fee> details;

			@Override
			public String toString() {
				return "RelativeFee [details=" + details + ", toString()=" + super.toString() + "]";
			}

			public List<Fee> getDetails() {
				return details;
			}

			public void setDetails(List<Fee> details) {
				this.details = details;
			}
		}

		public FixedFee getFixedFee() {
			return fixedFee;
		}

		public void setFixedFee(FixedFee fixedFee) {
			this.fixedFee = fixedFee;
		}

		public RelativeFee getRelativeFee() {
			return relativeFee;
		}

		public void setRelativeFee(RelativeFee relativeFee) {
			this.relativeFee = relativeFee;
		}

		@Override
		public String toString() {
			return "WithdrawFeeBreakup [fixedFee=" + fixedFee + ", relativeFee=" + relativeFee + "]";
		}
		
	}

	public WithdrawFeeBreakup getWithdrawFeeBreakup() {
		return withdrawFeeBreakup;
	}

	public void setWithdrawFeeBreakup(WithdrawFeeBreakup withdrawFeeBreakup) {
		this.withdrawFeeBreakup = withdrawFeeBreakup;
	}

	@Override
	public String toString() {
		return "WithdrawFee [withdrawFeeBreakup=" + withdrawFeeBreakup + ", toString()=" + super.toString() + "]";
	}
}
