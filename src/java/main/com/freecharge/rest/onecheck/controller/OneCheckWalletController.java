package com.freecharge.rest.onecheck.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.service.AutoPayAppService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.IdentityAuthorizedException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.onecheck.VerifyAndWithdrawRequestDO;
import com.freecharge.rest.onecheck.model.AccountDetailsDO;
import com.freecharge.rest.onecheck.model.AddBankAccountRequestDO;
import com.freecharge.rest.onecheck.model.DeleteCardResponse;
import com.freecharge.rest.onecheck.model.DeviceInformation;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.GetSeedRequestDO;
import com.freecharge.rest.onecheck.model.ListCardsResponse;
import com.freecharge.rest.onecheck.model.OneCheckTransactionDetails;
import com.freecharge.rest.onecheck.model.PaymentModesResponse;
import com.freecharge.rest.onecheck.model.UpdateBankAccountRequestDO;
import com.freecharge.rest.onecheck.model.WithdrawAmount;
import com.freecharge.rest.onecheck.model.WithdrawFee;
import com.freecharge.rest.onecheck.model.WithdrawFee.WithdrawFeeBreakup;
import com.freecharge.rest.onecheck.model.WithdrawFee.WithdrawFeeBreakup.FixedFee;
import com.freecharge.rest.onecheck.model.WithdrawFee.WithdrawFeeBreakup.RelativeFee;
import com.freecharge.rest.onecheck.model.WithdrawFeeDetails;
import com.freecharge.rest.onecheck.service.FCWalletService;
import com.freecharge.rest.onecheck.service.OneCheckFCWalletService;
import com.freecharge.rest.wallet.InvalidIMSTokenException;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.snapdeal.payments.sdmoney.exceptions.InsufficientFundsException;
import com.snapdeal.payments.sdmoney.service.model.Balance;
import com.snapdeal.payments.sdmoney.service.model.Fee;
import com.snapdeal.payments.sdmoney.service.model.GetAccountBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetWithdrawalFeeDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.type.BalanceType;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionRecommendation;

@Controller
@RequestMapping("/rest/fcwallet/v1/*")
public class OneCheckWalletController extends AbstractRestController{
    private Logger logger = LoggingFactory.getLogger(OneCheckWalletController.class);

    @Autowired
    private FCWalletService         oneCheckService;
    
    @Autowired
    private OneCheckWalletService ocWalletService;

    @Autowired
    private OneCheckFCWalletService walletService;
    
    @Autowired
    private AutoPayAppService autoPayAppService;
    
    @Autowired
    private AppConfigService appConfigService;
    
    private final BigDecimal GLOBAL_LOWER_WITHDRAW_LIMIT = new BigDecimal(100);
    private final BigDecimal GLOBAL_UPPER_WITHDRAW_LIMIT = new BigDecimal(5000);
    
    @NoLogin
    @Csrf(exclude = true)
    @NoSession
    @RequestMapping(value = "cards/list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ListCardsResponse listCards(@RequestBody String customerIdJson, HttpServletRequest httpRequest) {
    	return walletService.listCards(customerIdJson, httpRequest);
    }

    @NoLogin
    @Csrf(exclude = true)
    @NoSession
    @RequestMapping(value = "cards/delete", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String deleteCard(@RequestBody String cardTokenJson, HttpServletRequest httpRequest) {
        
        if (StringUtils.isBlank(cardTokenJson)) {
            return FCWalletErrorCode.E15.toJson();
        }
        
        Map<String, Object> jsonMap = new Gson().fromJson(cardTokenJson, HashMap.class);
        
        if (jsonMap == null || jsonMap.isEmpty()) {
            return FCWalletErrorCode.E15.toJson();      
        }
        
        if (!jsonMap.containsKey("cardToken") || jsonMap.get("cardToken") == null) {
            return FCWalletErrorCode.E15.toJson();
        }
        
        Object cardToken = jsonMap.get("cardToken");
        
        if (!(cardToken instanceof String)) {
            return FCWalletErrorCode.E15.toJson();
        }
        
        String tokenString = (String) cardToken;
        
        if (StringUtils.isBlank(tokenString)) {
            return FCWalletErrorCode.E15.toJson();
        }
        
        if (StringUtils.equals("null", tokenString)) {
            return FCWalletErrorCode.E15.toJson();
        }   
        
        DeleteCardResponse deleteCardResponse = walletService.deleteCard(cardTokenJson, oneCheckService.getHeaders(httpRequest));
        return new Gson().toJson(deleteCardResponse);
    }

    @NoLogin
    @Csrf(exclude = true)
    @NoSession
    @RequestMapping(value = "payment/modes", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody PaymentModesResponse getPaymentModes(HttpServletRequest httpRequest) {
        return walletService.getPaymentModes(oneCheckService.getHeaders(httpRequest));
    }


    @RequestMapping(value = "onecheck/gettxndetails", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    OneCheckTransactionDetails getTransactionDetails(HttpServletRequest httpRequest) {
        String transactionId = httpRequest.getParameter("transactionId");
        return walletService.getTransactionDetails(transactionId, oneCheckService.getHeaders(httpRequest));
    }

    @NoLogin
    @Csrf(exclude = true)
    @NoSession
    @RequestMapping(value = "verifyandwithdraw", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String verifyAndWithdraw(@RequestBody Map<String, String> paramMap, HttpServletRequest request) {
    	logger.info("Enter into verifyandwithdraw for paramMap: "+paramMap);
    	VerifyAndWithdrawRequestDO verifyAndWithdrawRequestDO = new VerifyAndWithdrawRequestDO();
    	verifyAndWithdrawRequestDO.setAccountToken(paramMap.get("accountToken"));
    	verifyAndWithdrawRequestDO.setAmount(paramMap.get("amount"));
    	verifyAndWithdrawRequestDO.setOtp(paramMap.get("otp"));
    	verifyAndWithdrawRequestDO.setOtpId(paramMap.get("otpId"));
    	verifyAndWithdrawRequestDO.setVisitId(paramMap.get("visitId"));
    	verifyAndWithdrawRequestDO.setIdempotencyId(UUID.randomUUID().toString());
        verifyAndWithdrawRequestDO.setOtpPurpose("MONEY_OUT");
        
    	DeviceInformation deviceInfo = new DeviceInformation();
    	deviceInfo.setClientId(paramMap.get("clientId"));
    	
    	verifyAndWithdrawRequestDO.setDeviceInformation(deviceInfo);
    	
    	if (isValidWithdrawAmount(verifyAndWithdrawRequestDO.getAmount())) {
    		return oneCheckService.verifyAndWithdraw(verifyAndWithdrawRequestDO, request);
    	} else {
    		return FCWalletErrorCode.E20.toJson();
    	}
    }

    @NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "bank/addaccount", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String addBankAccount(@RequestBody AddBankAccountRequestDO addBankAccountRequestDO,
            HttpServletRequest request) {
        /*
         * Do not remove any validation checks for the field
         * security concern
         */
        if (StringUtils.isBlank(addBankAccountRequestDO.getAccountNumber())) {
            return FCWalletErrorCode.E23.toJson();
        }
        
        if (StringUtils.isBlank(addBankAccountRequestDO.getBankName())) {
            return FCWalletErrorCode.E24.toJson();
        }
        
        if (StringUtils.isBlank(addBankAccountRequestDO.getIfsc())) {
            return FCWalletErrorCode.E25.toJson();
        }
        
        if (StringUtils.isBlank(addBankAccountRequestDO.getShortName())) {
            return FCWalletErrorCode.E26.toJson();
        }
        
        if(addBankAccountRequestDO.getShortName().length() > 20) {
        	return FCWalletErrorCode.E04.toJson();
        }
        
        if (!FCUtil.isWelFormedName(addBankAccountRequestDO.getShortName())) {
    		return FCWalletErrorCode.E39.toJson();
    	}
    	
		if (!FCUtil.isWelFormedName(addBankAccountRequestDO.getBankName())) {
			return FCWalletErrorCode.E40.toJson();
		}
        
        if (!FCUtil.isValidBankAccountNumber(addBankAccountRequestDO.getAccountNumber())) {
    	    return FCWalletErrorCode.E30.toJson();
        }
       
        if (!FCUtil.isValidIfscCode(addBankAccountRequestDO.getIfsc())) {
    	   return FCWalletErrorCode.E31.toJson();
        }
        return oneCheckService.addBankAccount(addBankAccountRequestDO, request);
    }

    @NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "bank/updateaccount", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String updateBankAccount(@RequestBody UpdateBankAccountRequestDO updateBankAccountRequestDO,
            HttpServletRequest request) {
    	/*
         * Do not remove any validation checks for the field
         * security concern
         */
    	
    	if (StringUtils.isBlank(updateBankAccountRequestDO.getBankAccountDetails().getIfsc())) {
    		return FCWalletErrorCode.E25.toJson();
    	}
    	if (StringUtils.isBlank(updateBankAccountRequestDO.getBankAccountDetails().getBankName())) {
            return FCWalletErrorCode.E24.toJson();
        }
        
        if (StringUtils.isBlank(updateBankAccountRequestDO.getBankAccountDetails().getShortName())) {
            return FCWalletErrorCode.E26.toJson();
        }
        
        if(updateBankAccountRequestDO.getBankAccountDetails().getShortName().length() > 20) {
        	return FCWalletErrorCode.E04.toJson();
        }
        
    	if (!FCUtil.isValidBankAccountNumber(updateBankAccountRequestDO.
    			getBankAccountDetails().getAccountNumber())) {
    		return FCWalletErrorCode.E30.toJson();
    	}
    	
    	if (!FCUtil.isValidIfscCode(updateBankAccountRequestDO.getBankAccountDetails().getIfsc())) {
    		return FCWalletErrorCode.E31.toJson();
    	}
    	
    	if (!FCUtil.isWelFormedName(updateBankAccountRequestDO.getBankAccountDetails().getShortName())) {
    		return FCWalletErrorCode.E39.toJson();
    	}
    	
		if (!FCUtil.isWelFormedName(updateBankAccountRequestDO.getBankAccountDetails().getBankName())) {
			return FCWalletErrorCode.E40.toJson();
		}
    	
    	if (isBankAccountOwnedByUser(updateBankAccountRequestDO, request)) {
    		return oneCheckService.updateBankAccount(updateBankAccountRequestDO, request);
    	} else {
    		return FCWalletErrorCode.E08.toJson();
    	} 
    }

    @NoLogin
    @NoSession
    @Csrf(exclude = true)
    @RequestMapping(value = "getbalance", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object> getWalletBalance(HttpServletRequest request) {
        Map<String,Object> responseMap = new HashMap<>();
        
        String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        
        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = "NA";
        }
        
        if (StringUtils.isBlank(userAgent)) {
            userAgent = "NA";
        }
        String imsToken = FCSessionUtil.getAppCookie(request);
        logger.info("WalletToken from Session for getBalance: "+imsToken);
        if (imsToken==null) {
        	imsToken = request.getHeader("token");
        	logger.info("WalletToken from header for getBalance: "+imsToken);
        } else {
        	/*if(!imsToken.equals(FCSessionUtil.getCurrentSession().getUuid())) {
        		IdentityAuthorizedException identityApiException = new IdentityAuthorizedException("Invalid user session");
        		throw identityApiException;
        	}*/
        }
        
        boolean isWithdrawable = Boolean.parseBoolean(request.getParameter("isWithdrawable"));
        String fcversionStr = request.getParameter("fcversion");
        String fcChannelStr = request.getParameter("fcChannel");
        
        isWithdrawable=isWithdrawable && isWithdrawbleFeeEnable(fcversionStr, fcChannelStr);
        boolean isGVDetailsNeeded = false;
        isGVDetailsNeeded = isGVDetailsNeeded || checkGVLoadMoneyAllowed(fcversionStr, fcChannelStr);
        
        logger.info("Enter into getBalance for walletToken: "+imsToken);
        try {
            GetAccountBalanceDetailsResponse balanceDetailsByToken = ocWalletService.getBalanceDetailsByToken(imsToken, userAgent, ipAddress,isWithdrawable, isGVDetailsNeeded);
            
            Map<BalanceType, BigDecimal> balanceDetails = balanceDetailsByToken.getBalanceDetails();
            
            logger.info("blockedin balance:"+balanceDetailsByToken.getTotalBlockedInBalance());
            responseMap.put("totalBalance", balanceDetailsByToken.getTotalBalance().toString());
            responseMap.put("generalBalance", balanceDetails.get(BalanceType.GENERAL_BALANCE).toString());
            responseMap.put("voucherBalance", balanceDetails.get(BalanceType.VOUCHER_BALANCE).toString());
            responseMap.put("depositLimit", balanceDetails.get(BalanceType.CREDITABLE_BALANCE).toString());
            
            if(isGVDetailsNeeded){
               responseMap.put("voucherDepositLimit", balanceDetails.get(BalanceType.CREDITABLE_VOUCHER_BALANCE).toString());
               responseMap.put("voucherDepositCountLimit", balanceDetails.get(BalanceType.CREDITABLE_VOUCHER_AVAILABLE_COUNT).toString());
               responseMap.put("debitableBalance", balanceDetails.get(BalanceType.DEBITABLE_BALANCE).toString());               
               responseMap.put("balanceLimitReasons", balanceDetailsByToken.getBalanceLimitReasons());
            }
            
            responseMap.put("totalblockedInBalance", String.valueOf(balanceDetailsByToken.getTotalBlockedInBalance()));
            responseMap.put("maxGeneralBalance", balanceDetailsByToken.getMaxGeneralBalance().toString());
            responseMap.put("minWithdrawableBalance", balanceDetailsByToken.getMinWithdrawableBalance().toString());
            BigDecimal withdrawLimit = balanceDetails.get(BalanceType.WITHDRAWABLE_BALANCE);
            if(!isWithdrawable) {
            	responseMap.put("withdrawLimit", "0.0");
            } else {
	            if (isBelowGlobalWithdrawLowerLimit(withdrawLimit)) {
	                responseMap.put("withdrawLimit", "0.0");
	            } else if (isAboveGlobalWithdrawUpperLimit(withdrawLimit)) {
	                responseMap.put("withdrawLimit", GLOBAL_UPPER_WITHDRAW_LIMIT.toString());
	            } else {
	                responseMap.put("withdrawLimit", withdrawLimit.toString());
	            }
	            Map<BalanceType,Map<TransactionRecommendation,String>> reasonMap=balanceDetailsByToken.getBalanceLimitReasons();
	            if(reasonMap!=null && reasonMap.containsKey(BalanceType.WITHDRAWABLE_BALANCE)) {
	            	Map<TransactionRecommendation,String> withdrawBalanceMap = reasonMap.get(BalanceType.WITHDRAWABLE_BALANCE);
	            	if(withdrawBalanceMap!=null && withdrawBalanceMap.containsKey(TransactionRecommendation.WITHDRAW_MESSAGE)) {
	            		responseMap.put("withdrawMessage",withdrawBalanceMap.get(TransactionRecommendation.WITHDRAW_MESSAGE));
	            	}
	            	
	            }
            }
            
        } catch (InvalidIMSTokenException e) {
            logger.error("Error in getBalance method",e);
            Map<String, String> errorMap = FCWalletErrorCode.E18.toErrorMap();
            responseMap = new HashMap<>();
            for(Entry<String, String> error:errorMap.entrySet()){
               responseMap.put(error.getKey(), error.getValue());
            }
            
        } catch (Exception e) {
            logger.error("Exception while fetching Wallet balance", e);
            Map<String, String> errorMap = FCWalletErrorCode.INTERNAL_ERROR.toErrorMap();
            responseMap = new HashMap<>();
            for(Entry<String, String> error:errorMap.entrySet()){
               responseMap.put(error.getKey(), error.getValue());
            }
        }
        
        return responseMap;
    }
    
    private boolean checkGVLoadMoneyAllowed(String fcversionStr,String fcChannelStr)  {
       boolean isGVLoadMoneyAllowed = false;
       if(fcChannelStr==null) {
          logger.info("isGVLoadMoneyAllowed is "+isGVLoadMoneyAllowed +" for fcChannnel: "+fcChannelStr);
          return isGVLoadMoneyAllowed;
       }
       Integer fcChannel = Integer.parseInt(fcChannelStr);
       String channelName = getChannelName(fcChannel); 
       
       if(fcChannel.equals(FCConstants.CHANNEL_ID_WEB) || fcChannel.equals(FCConstants.CHANNEL_ID_MOBILE)) {
          return true;
       }

       String data=appConfigService.getChildProperty(AppConfigService.GV_LOAD_MONEY_ALLOWED_CONFIG,channelName);
       
       if(data==null || data.isEmpty()) {
          logger.error("GVLoadMoneyAllowed data is empty/null for key: "+channelName+" fcversion: "+fcversionStr);
       } else {
             Integer minAppVersion = null;
             JSONParser parser = new JSONParser();
             try {
                JSONObject jsonObj= (JSONObject) parser.parse(data);
                if(jsonObj.containsKey("minAppVersion")) {
                    minAppVersion =Integer.parseInt((String)jsonObj.get("minAppVersion"));
                 }
                if(fcversionStr!=null && minAppVersion!=null) {
                   Integer fcversion = Integer.parseInt(fcversionStr);
                   isGVLoadMoneyAllowed =  fcversion.intValue() >= minAppVersion.intValue();
                }
             } catch (ParseException e) {
                logger.error("minAppversionMap parse error in isGVLoadMoneyAllowed method for fcChannel: "+fcChannelStr);
             }
       } 
       logger.info("isGVLoadMoneyAllowed is "+isGVLoadMoneyAllowed +" for fcChannnel: "+fcChannelStr +" and fcversion: "+fcversionStr);
       return isGVLoadMoneyAllowed;
     }
    
   private boolean isWithdrawbleFeeEnable(String fcversionStr,String fcChannelStr)  {
    	boolean isWithdrawEnable = false;
    	if(fcChannelStr==null) {
    		logger.info("isWithdrawEnable is "+isWithdrawEnable +" for fcChannnel: "+fcChannelStr);
    		return isWithdrawEnable;
    	}
    	Integer fcChannel = Integer.parseInt(fcChannelStr);
    	String channelName = getChannelName(fcChannel); 
    	String data=appConfigService.getChildProperty(AppConfigService.WITHDRAW_BALANCE_CONFIG,channelName);
		if(data==null || data.isEmpty()) {
			logger.error("withdrawBalance data is empty/null for key: "+channelName+" fcversion: "+fcversionStr);
		} else {
			if(fcChannel.equals(FCConstants.CHANNEL_ID_WEB) || fcChannel.equals(FCConstants.CHANNEL_ID_MOBILE)) {
				isWithdrawEnable =true;
			} else {
				Integer minAppVersion = null;
	            JSONParser parser = new JSONParser();
	            try {
					JSONObject jsonObj= (JSONObject) parser.parse(data);
					if(jsonObj.containsKey("minAppVersion")) {
						 minAppVersion =Integer.parseInt((String)jsonObj.get("minAppVersion"));
				    }
					if(fcversionStr!=null && minAppVersion!=null) {
						Integer fcversion = Integer.parseInt(fcversionStr);
						isWithdrawEnable =  fcversion.intValue() >= minAppVersion.intValue();
					}
				} catch (ParseException e) {
					logger.error("minAppversionMap parse error in isWithdrawbleFeeEnable method for fcChannel: "+fcChannelStr);
				}
			}
		} 
		logger.info("isWithdrawEnable is "+isWithdrawEnable +" for fcChannnel: "+fcChannelStr +" and fcversion: "+fcversionStr);
    	return isWithdrawEnable;
    }
    
    private String getChannelName(Integer channelId){
	    return FCConstants.channelNameMasterMap.get(FCConstants.channelMasterMap.get(channelId)).toLowerCase();
	}
    
    @Csrf(exclude = true)
    @RequestMapping(value = "getWithdrawFeeDetails", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody WithdrawFeeDetails getWithdrawFeeDetails(HttpServletRequest request) throws InvalidParameterException {
    	String amountStr = request.getParameter("amount");
    	if(StringUtils.isBlank(amountStr)) {
    		throw new InvalidParameterException("amount", amountStr,
    				"amount cannot be null", ValidationErrorCode.INVALID_AMOUNT);
    	}
        logger.info("Enter into getWithdrawFeeDetails for amount:- "+amountStr);
        String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        
        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = "NA";
        }
        
        if (StringUtils.isBlank(userAgent)) {
            userAgent = "NA";
        }
        String imsToken = FCSessionUtil.getAppCookie(request);
        WithdrawFeeDetails withdrawFeeDetails = null;
        try {
        	BigDecimal amount =new BigDecimal(amountStr);
            GetWithdrawalFeeDetailsResponse withdrawalFeeDetailsResponse = ocWalletService.getWithdrawFeeDetails(imsToken, userAgent, ipAddress, amount);
            if(withdrawalFeeDetailsResponse!=null) {
            	withdrawFeeDetails = createWithdrawFeeDetails(withdrawalFeeDetailsResponse);
            }
            
        } catch (InvalidIMSTokenException e) {
        	logger.error("Error in getBalance method",e);
        } catch (InsufficientFundsException ife) { 
        	throw new InvalidParameterException("amount", amountStr, ife.getMessage(),ValidationErrorCode.INVALID_AMOUNT);
        } catch (Exception e) {
            logger.error("Exception while fetching Wallet balance", e);
        }
        
        return withdrawFeeDetails;
    }

	private WithdrawFeeDetails createWithdrawFeeDetails(GetWithdrawalFeeDetailsResponse withdrawalFeeDetailsResponse) {
		WithdrawFeeDetails withdrawFeeDetails;
		withdrawFeeDetails = new WithdrawFeeDetails();
		
		WithdrawAmount withdrawAmount = new WithdrawAmount();
		withdrawFeeDetails.setWithdrawAmount(withdrawAmount);
		withdrawAmount.setAmount(withdrawalFeeDetailsResponse.getWithdrawableAmount());
		withdrawAmount.setName("Amount Credited to Bank");
		
		WithdrawFee withdrawFee = new WithdrawFee();
		withdrawFeeDetails.setWithdrawFee(withdrawFee);
		
		BigDecimal withdrawFeeAmount=withdrawalFeeDetailsResponse.getEffectiveFee();
		withdrawFee.setAmount(withdrawFeeAmount);
		withdrawFee.setName("Withdraw Fee Applicable");
		
		WithdrawFeeBreakup withdrawFeeBreakup = new WithdrawFeeBreakup();
		withdrawFee.setWithdrawFeeBreakup(withdrawFeeBreakup);
		
		FixedFee fixedFee = new FixedFee();
		fixedFee.setAmount(withdrawalFeeDetailsResponse.getFeeBreakup().getFixedFeeAmount());
		fixedFee.setName("Basic Fee");

		RelativeFee relativeFee = new RelativeFee();
		relativeFee.setAmount(withdrawalFeeDetailsResponse.getFeeBreakup().getRelativeFeeAmount());
		relativeFee.setName("Variable Fee");
		List<com.freecharge.rest.onecheck.model.Fee> relativeFeeDetails = new ArrayList<>();
		List<Fee>fees=withdrawalFeeDetailsResponse.getFeeBreakup().getRelativeFeeDetails();
		if(fees!=null && !fees.isEmpty()) {
			for(Fee fee:fees) {
				com.freecharge.rest.onecheck.model.Fee relativeFeeDetail = new com.freecharge.rest.onecheck.model.Fee();
				relativeFeeDetail.setAmount(fee.getFeeAmount());
				String details = fee.getFeeInPercentage() + "% of Rs."+fee.getFeeOnAmount();
				relativeFeeDetail.setName(details);
				relativeFeeDetails.add(relativeFeeDetail);
				
			}
		}
		relativeFee.setDetails(relativeFeeDetails);
		
		withdrawFeeBreakup.setFixedFee(fixedFee);
		withdrawFeeBreakup.setRelativeFee(relativeFee);
		return withdrawFeeDetails;
	}

	@NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "bank/getaccounts", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getBankAccounts(HttpServletRequest request) {
        return oneCheckService.getBankAccounts(request);
    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "getbalancelimits", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getWalletBalanceLimits(HttpServletRequest request) {
    	return oneCheckService.getWalletBalanceLimits(request);
        
    }
    
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "{referrer}/authtoken/validate", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String validateAuthToken(@PathVariable String referrer, HttpServletRequest request) {
        return oneCheckService.validateAuthTokenForReferrer(referrer, request);
    }

    @NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "/authtoken/generate", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String generateTransferToken(@RequestBody Map<String, String> authToken, HttpServletRequest request) {
    	return oneCheckService.generateTransferToken(authToken.get("authToken"));
    }

    @NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "/seed/get", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String getSeed(GetSeedRequestDO requestDo, HttpServletRequest request) {
        return oneCheckService.getSeed(requestDo, request);
    }
    
    @NoSession
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "/counter/get", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getCounter(HttpServletRequest request) {
        return oneCheckService.getCounter(request);
    }
    
    private boolean isValidWithdrawAmount(String withdrawAmount) {
    	if(!FCUtil.isValidTxnAmount(withdrawAmount)) {
    		return false;
    	}
        BigDecimal amountToWithdraw = new BigDecimal(withdrawAmount);
        int lowerLimtResult = amountToWithdraw.compareTo(GLOBAL_LOWER_WITHDRAW_LIMIT);  
        int upperLimitResult = amountToWithdraw.compareTo(GLOBAL_UPPER_WITHDRAW_LIMIT);
        	if (lowerLimtResult >= 0 && upperLimitResult <= 0) {
        		return true;
        	}
    	return false;
    }
            
    private boolean isBelowGlobalWithdrawLowerLimit(BigDecimal withdrawLimit) {
        return withdrawLimit.compareTo(GLOBAL_LOWER_WITHDRAW_LIMIT) < 0;
    }
    
    private boolean isAboveGlobalWithdrawUpperLimit(BigDecimal withdrawLimit) {
        return withdrawLimit.compareTo(GLOBAL_UPPER_WITHDRAW_LIMIT) > 0;
    }
    
    private boolean isBankAccountOwnedByUser(UpdateBankAccountRequestDO updateBankAccountRequestDO, 
    		HttpServletRequest request) {
    	String accountDetails = oneCheckService.getBankAccounts(request);
    	JsonParser parser = new JsonParser();
    	Gson gson = new Gson();
    	JsonArray jsonArray;
    	jsonArray = (JsonArray) parser.parse(accountDetails);
		List<AccountDetailsDO> accInfoList = new ArrayList<AccountDetailsDO>();
		for(JsonElement obj : jsonArray) {
			AccountDetailsDO ado = gson.fromJson( obj , AccountDetailsDO.class);
			accInfoList.add(ado);
		}
		for (AccountDetailsDO accountInfo : accInfoList) {
		     if (accountInfo.getAccountToken().equals(updateBankAccountRequestDO.getBankAccountDetails().getAccountToken())) {
		    	 return true;
		     }
		}
    	return false;
    }
    
	@Csrf(exclude = true)
	@NoLogin
	@NoSessionWrite
	@Authenticate(api = "autoPay.recharge")
	@RequestMapping(value = "getWalletBalance/{userId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getWalletBalance(@PathVariable Integer userId) {
		Balance balance = autoPayAppService.getBalance(userId);
		if (balance != null) {
			return balance.getTotalBalance().toPlainString();
		}
		return null;
	}

}
