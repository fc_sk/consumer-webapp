package com.freecharge.rest.onecheck.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.cardstorage.CardDetailsDo;
import com.freecharge.cardstorage.CardMetaData;
import com.freecharge.cardstorage.CardStorageDAO;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCCardUtil;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashUtil;
import com.freecharge.payment.services.DebitATMStatusHandler;
import com.freecharge.payment.services.KlickPayGatewayHandler;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.rest.onecheck.model.CustomerCard;
import com.freecharge.rest.onecheck.model.DeleteCardResponse;
import com.freecharge.rest.onecheck.model.InitPaymentRequestDO;
import com.freecharge.rest.onecheck.model.ListCardsResponse;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse.CallStatus;
import com.freecharge.rest.onecheck.model.OneCheckTransactionDetails;
import com.freecharge.rest.onecheck.model.PaymentModesResponse;
import com.freecharge.rest.onecheck.model.TransactionDetail;
import com.freecharge.rest.onecheck.model.WalletCreditInitResponseDo;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.rest.onecheck.util.WalletErrorCodeUtil;
import com.google.api.client.util.Strings;

@Service
public class OneCheckFCWalletService {

    private Logger                    logger                 = LoggingFactory.getLogger(OneCheckFCWalletService.class);

    @Autowired
    private FCProperties              fcProperties;

    private static final ObjectMapper jsonHandler            = new ObjectMapper();

    @Autowired
    private AppConfigService          appConfig;
    
    @Autowired
    private CardStorageDAO cardStorageDAO;
    
    @Autowired
    private FCWalletService         oneCheckService;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private DebitATMStatusHandler  klickpayGatewayHandler;
    
    @Autowired
    private FCCardUtil fcCardUtil;

    
    public boolean isAPIMockConfigEnabled() {
        try {
            /*JSONObject configJson = appConfig.getCustomProp(AppConfigService.ONECHECK_API_MOCK_ENABLED);

            if (configJson == null) {
                return false;
            }

            String enabledString = (String) configJson.get("mock.enabled");
            if (StringUtils.isNotBlank(enabledString)) {
                return Boolean.parseBoolean(enabledString);
            }*/
            return false;
        } catch (Exception e) {
            logger.error("Some error while the getting the app config value "
                    + "  for mocking the one check API. Returning false", e);
        }
        return false;
    }

    private OneCheckRawResponse sendPost(String postData, String apiURLProperty, Map<String, String> headersFromFC) {

        if (isAPIMockConfigEnabled()) {
            return OneCheckRawResponse.mockResponse(apiURLProperty);
        }

        String url = fcProperties.getOnecheckHostUrl() + fcProperties.getProperty(apiURLProperty);
        HttpClient client = new HttpClient();
        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();

        connectionParams.setConnectionTimeout(fcProperties.getOneCheckConnectTimeout());
        connectionParams.setSoTimeout(fcProperties.getOnecheckReadTimeout());
        client.getHttpConnectionManager().setParams(connectionParams);

        PostMethod postMethod = new PostMethod(url);

        Map<String, String> headersMap = new HashMap<String, String>(1);
        headersMap.putAll(headersFromFC);

        // Setting the headers.
        for (String key : headersMap.keySet()) {
            postMethod.addRequestHeader(key, headersMap.get(key));
        }
        logger.info("Post Data:"+postData+"\nurl:"+url);
        try {
            StringRequestEntity requestEntity = new StringRequestEntity(postData, "application/json", "UTF-8");
            int responseCode;
            String response = "";
            postMethod.setRequestEntity(requestEntity);

            responseCode = client.executeMethod(postMethod);
            if (responseCode != HttpStatus.SC_OK) {
                return OneCheckRawResponse.httpErrorResponse(responseCode);
            }

            response = postMethod.getResponseBodyAsString();
            logger.info("Response for the url " + url + "  is " + response);
            return OneCheckRawResponse.successfulResponse(response);
        } catch (IOException e) {
            logger.error("IO Exception while calling OneCheck", e);
            return OneCheckRawResponse.internalErrorResponse();
        } finally {
            postMethod.releaseConnection();
        }
    }

    private OneCheckRawResponse doHttpGet(String apiURLProperty, Map<String, String> headersFromFC) {
        return doHttpGet(apiURLProperty, headersFromFC, null);
    }

    private OneCheckRawResponse doHttpGet(String apiURLProperty, Map<String, String> headersFromFC, Map<String, String> queryParameters) {

        if (isAPIMockConfigEnabled()) {
            return OneCheckRawResponse.mockResponse(apiURLProperty);
        }

        String url = fcProperties.getOnecheckHostUrl() + fcProperties.getProperty(apiURLProperty);
        HttpClient client = new HttpClient();
        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();

        connectionParams.setConnectionTimeout(fcProperties.getOneCheckConnectTimeout());
        connectionParams.setSoTimeout(fcProperties.getOnecheckReadTimeout());
        client.getHttpConnectionManager().setParams(connectionParams);

        GetMethod getMethod = new GetMethod(url);

        if(queryParameters!=null && queryParameters.size()!=0) {
            NameValuePair[] nvpairs =  new NameValuePair[queryParameters.size()];
            int i=0;
            for(String key: queryParameters.keySet()) {
                nvpairs[i++] = new NameValuePair(key, queryParameters.get(key));
            }
            getMethod.setQueryString(nvpairs);
        }

        Map<String, String> headersMap = new HashMap<String, String>(1);
        headersMap.putAll(headersFromFC);

        // Setting the headers.
        for (String key : headersMap.keySet()) {
            getMethod.addRequestHeader(key, headersMap.get(key));
        }

        try {
            int responseCode;
            String response = "";
            responseCode = client.executeMethod(getMethod);
            if (responseCode != HttpStatus.SC_OK) {
                return OneCheckRawResponse.httpErrorResponse(responseCode);
            }

            response = getMethod.getResponseBodyAsString();
            return OneCheckRawResponse.successfulResponse(response);
        } catch (IOException e) {
            logger.error("IO Exception while calling OneCheck", e);
            return OneCheckRawResponse.internalErrorResponse();
        } finally {
            getMethod.releaseConnection();
        }
    }

    public WalletCreditInitResponseDo initWalletCreditTransaction(InitPaymentRequestDO initPaymentReqDo, Map<String, String> headersMap) {
        try {
        	String requestBody = jsonHandler.writeValueAsString(initPaymentReqDo);
        	logger.info("Request Body for initWalletCreditTransaction : " +requestBody);
            OneCheckRawResponse oneCheckRawResponse = this.sendPost(requestBody, OneCheckConstants.INIT_CREDIT_IN_URL, headersMap);
            WalletCreditInitResponseDo responseDo = new WalletCreditInitResponseDo();
            WalletErrorCodeUtil.setWalletStatusCode(oneCheckRawResponse, responseDo);
            if (oneCheckRawResponse.getCallStatus() != CallStatus.Success) {
                logger.error("Invalid call status while posting the data to the onecheck " + OneCheckConstants.INIT_CREDIT_IN_URL + " API. CallStatus : "
                        + oneCheckRawResponse.getCallStatus() + " HttpStatus = " + oneCheckRawResponse.getHttpStatus());
                return responseDo;
            }

            Map<String, Object> responseMap = oneCheckRawResponse.getResponseMap();
            responseDo.setPgUrl((String) responseMap.get("pg_url"));
            responseDo.setChecksum((String) responseMap.get("checksum"));
            responseDo.setCurrency((String) responseMap.get("currency"));
            responseDo.setTxnId((String) responseMap.get("txn_id"));
            responseDo.setRedirectUrl((String) responseMap.get("redirect_url"));
            responseDo.setTxnAmount(((Number) responseMap.get("txn_amount")).doubleValue());
            responseDo.setReqChannel((String) responseMap.get("req_channel"));
            responseDo.setMerchantId((String) responseMap.get("merchant_id"));
            responseDo.setUserId((String) responseMap.get("user_id"));

            return responseDo;
        } catch (IOException e) {
            logger.error("Some error while posting the data to the onecheck " + OneCheckConstants.INIT_CREDIT_IN_URL + " API ", e);
            throw new IllegalStateException(e);
        }
    }

    public ListCardsResponse listCards(String customerIdJson, HttpServletRequest request) {
        try {
            Map<String, String> headersMap = oneCheckService.getHeaders(request);
            
            OneCheckRawResponse oneCheckRawResponse = this.doHttpGet(OneCheckConstants.LIST_CARDS_URL, headersMap);
            ObjectMapper mapper = new ObjectMapper();
            String responseJson = mapper.writeValueAsString(oneCheckRawResponse);
            logger.info("Onecheck list cards response : "+ responseJson);
            ListCardsResponse responseDo = new ListCardsResponse();
            WalletErrorCodeUtil.setWalletStatusCode(oneCheckRawResponse, responseDo);
            if (oneCheckRawResponse.getCallStatus() != CallStatus.Success) {
                logger.error("Invalid call status while posting the data to the onecheck "
                        + OneCheckConstants.LIST_CARDS_URL + " API. CallStatus : "
                        + oneCheckRawResponse.getCallStatus() + " HttpStatus = " + oneCheckRawResponse.getHttpStatus());
                return responseDo;
            }

            Map<String, Object> responseMap = oneCheckRawResponse.getResponseMap();
            @SuppressWarnings("unchecked")
            ArrayList<LinkedHashMap<String, Object>> array = (ArrayList<LinkedHashMap<String, Object>>) responseMap
                    .get("cards");
            List<CustomerCard> customerCards = new ArrayList<CustomerCard>();
            List<CardDetailsDo> cardDetailsDos = new ArrayList<CardDetailsDo>();
            for (int i = 0; i < array.size(); ++i) {
                LinkedHashMap<String, Object> map = array.get(i);
                CustomerCard customerCard = new CustomerCard();
                customerCard.setCardBrand((String) map.get("card_brand"));
                customerCard.setCardIssuer((String) map.get("card_issuer"));
                customerCard.setCardMask((String) map.get("card_mask"));
                customerCard.setCardReference((String) map.get("card_reference"));
                customerCard.setCardToken((String) map.get("card_token"));
                customerCard.setNickName((String) map.get("nickname"));
                customerCard.setCardNumber((String) map.get("card_isin"));
                customerCard.setCardType((String) map.get("card_type"));
                customerCard.setCardFingerprint((String)map.get("card_fingerprint"));
                
                CardDetailsDo cardDo = new CardDetailsDo();
                cardDo.setCardBinHead(((String) map.get("card_isin")).substring(0,6));
                cardDo.setCardFingerprint((String)map.get("card_fingerprint"));
                cardDetailsDos.add(cardDo);
                customerCard.setDebitAtmEnabled(false);
//                Map<String, String> debitATMStatusMap = klickpayGatewayHandler.isDebitATMEnabled(((String) map.get("card_isin")).substring(0,6), customerIdJson, (String)map.get("card_fingerprint"));
//                if (debitATMStatusMap.containsKey("status")) {
//                    customerCard.setDebitAtmEnabled(Boolean.parseBoolean(debitATMStatusMap.get("status")));
//                }
                if (Strings.isNullOrEmpty(customerCard.getCardBrand())) {
                	CardMetaData cardMeta = cardStorageDAO.findCardByReference(customerCard.getCardReference());
                	if(cardMeta!=null)
                	customerCard.setCardBrand(cardMeta.getCardBrand());
                }
                customerCard.setIsAllowedForPurpose(true);
                customerCards.add(customerCard);
            }
            
            List<CardDetailsDo> debitATMStatusList = klickpayGatewayHandler.isDebitATMEnabled(
                    customerIdJson, cardDetailsDos);
            
            for(CustomerCard each: customerCards){
                CardDetailsDo cardDetail = FCUtil.searchCardInResponse(each.getCardFingerprint(), debitATMStatusList);
                if(cardDetail==null)
                    continue;
                each.setDebitAtmEnabled(Boolean.parseBoolean(cardDetail.getStatus()));
                each.setCardFingerprint(null);
            }
            
            // Check credit cards for GV in load money flow
            String paymentPurpose = request.getParameter(PaymentConstants.PAYMENT_REQUEST_PARAM_PAYMENT_PURPOSE);
            fcCardUtil.graylistCardsByPaymentPurpose(customerCards, paymentPurpose);
            
            responseDo.setCards(customerCards);
            return responseDo;
        } catch (Exception e) {
            logger.error("Some error while posting the data to the onecheck " + OneCheckConstants.LIST_CARDS_URL
                    + " API ", e);
            throw new IllegalStateException(e);
        }
    }

    public DeleteCardResponse deleteCard(String cardTokenJson, Map<String, String> headersMap) {
        try {
            OneCheckRawResponse oneCheckRawResponse = this.sendPost(cardTokenJson, OneCheckConstants.DEL_CARD_URL,
                    headersMap);

            DeleteCardResponse responseDo = new DeleteCardResponse();
            WalletErrorCodeUtil.setWalletStatusCode(oneCheckRawResponse, responseDo);
            if (oneCheckRawResponse.getCallStatus() != CallStatus.Success) {
                logger.error("Invalid call status while posting the data to the onecheck "
                        + OneCheckConstants.DEL_CARD_URL + " API. CallStatus : " + oneCheckRawResponse.getCallStatus()
                        + " HttpStatus = " + oneCheckRawResponse.getHttpStatus());
                return responseDo;
            }

            Map<String, Object> responseMap = oneCheckRawResponse.getResponseMap();
            responseDo.setCardReference((String) responseMap.get("card_reference"));
            responseDo.setCardToken((String) responseMap.get("card_token"));
            responseDo.setDeleted((Boolean) responseMap.get("deleted"));

            return responseDo;
        } catch (Exception e) {
            logger.error("Some error while posting the data to the onecheck " + OneCheckConstants.DEL_CARD_URL
                    + " API ", e);
            throw new IllegalStateException(e);
        }
    }

    public OneCheckTransactionDetails getTransactionDetails(String transactionId, Map<String, String> headersMap) {
        try {

            String clientKey = fcProperties.getOnecheckClientKey();
            String clientId= fcProperties.getOnecheckClientId();
            String data = (clientId+clientKey+transactionId);
            headersMap.put("clientid", clientId);
            headersMap.put("checksum", HashUtil.getSHA256Hash(data));
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("transactionid", transactionId);
            OneCheckRawResponse oneCheckRawResponse = this
                    .doHttpGet(OneCheckConstants.ONE_CHECK_TXN_DETAILS_URL, headersMap, queryParams);
            OneCheckTransactionDetails responseDo = new OneCheckTransactionDetails();
            if (oneCheckRawResponse.getCallStatus() != CallStatus.Success) {
                logger.error("Invalid call status while posting the data to the onecheck "
                        + OneCheckConstants.ONE_CHECK_TXN_DETAILS_URL + " API for transaction Id : " + transactionId
                        + " CallStatus : " + oneCheckRawResponse.getCallStatus() + " HttpStatus = "
                        + oneCheckRawResponse.getHttpStatus());
                return responseDo;
            }

            Map<String, Object> responseMap = oneCheckRawResponse.getResponseMap();
            logger.info("OneCheckREsponseMap for transaction: " + transactionId + "| " + responseMap);
            responseDo.setTransactionId((String) responseMap.get("transactionid"));
            responseDo.setUserId((String) responseMap.get("userId"));
            responseDo.setAmount((Double) responseMap.get("txnAmount"));
            @SuppressWarnings("unchecked") ArrayList<LinkedHashMap<String, Object>> array = (ArrayList<LinkedHashMap<String, Object>>) responseMap
                    .get("details");

            List<TransactionDetail> detailsList= new ArrayList<TransactionDetail>();
            for (int i = 0; i < array.size(); ++i) {
                LinkedHashMap<String, Object> map = array.get(i);
                TransactionDetail detail = new TransactionDetail();
                detail.setContext((String) map.get("context"));
                detail.setStatus((String) map.get("status"));
                detail.setCreateTime((String) map.get("createTime"));
                detailsList.add(detail);
            }
            responseDo.setDetails(detailsList);
            return responseDo;

        } catch (Exception e) {
            logger.error(
                    "Some error while posting the data to the onecheck " + OneCheckConstants.ONE_CHECK_TXN_DETAILS_URL
                            + " API for transaction Id : " + transactionId, e);
            throw new IllegalStateException(e);
        }
    }

    public PaymentModesResponse getPaymentModes(Map<String, String> headersMap) {
        try {
            OneCheckRawResponse oneCheckRawResponse = this
                    .doHttpGet(OneCheckConstants.GET_PAYMENT_MODES_URL, headersMap);

            PaymentModesResponse responseDo = new PaymentModesResponse();
            WalletErrorCodeUtil.setWalletStatusCode(oneCheckRawResponse, responseDo);
            if (oneCheckRawResponse.getCallStatus() != CallStatus.Success) {
                logger.error("Invalid call status while posting the data to the onecheck "
                        + OneCheckConstants.GET_PAYMENT_MODES_URL + " API. CallStatus : "
                        + oneCheckRawResponse.getCallStatus() + " HttpStatus = " + oneCheckRawResponse.getHttpStatus());
                return responseDo;
            }

            Map<String, Object> responseMap = oneCheckRawResponse.getResponseMap();

            responseDo.setCustomerId((String) responseMap.get("customer_id"));
            @SuppressWarnings("unchecked")
            ArrayList<LinkedHashMap<String, Object>> array = (ArrayList<LinkedHashMap<String, Object>>) responseMap
                    .get("cards");

            List<CustomerCard> customerCards = new ArrayList<CustomerCard>();
            for (int i = 0; i < array.size(); ++i) {
                LinkedHashMap<String, Object> map = array.get(i);
                CustomerCard customerCard = new CustomerCard();
                customerCard.setCardBrand((String) map.get("card_brand"));
                customerCard.setCardIssuer((String) map.get("card_issuer"));
                customerCard.setCardMask((String) map.get("card_mask"));
                customerCard.setCardReference((String) map.get("card_reference"));
                customerCard.setCardToken((String) map.get("card_token"));
                customerCard.setNickName((String) map.get("nickname"));

                customerCards.add(customerCard);
            }
            responseDo.setCards(customerCards);

            @SuppressWarnings("unchecked")
            ArrayList<String> netBankingModes = (ArrayList<String>) responseMap.get("netbanking_modes");
            responseDo.setNetBankingModes(netBankingModes);

            return responseDo;
        } catch (Exception e) {
            logger.error("Some error while posting the data to the onecheck " + OneCheckConstants.GET_PAYMENT_MODES_URL
                    + " API ", e);
            throw new IllegalStateException(e);
        }
    }
}
