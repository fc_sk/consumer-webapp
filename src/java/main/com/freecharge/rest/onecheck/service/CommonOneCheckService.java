package com.freecharge.rest.onecheck.service;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.platform.metrics.CloudWatchMetricsClient;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.model.SDDefaultResponseDO;
import com.freecharge.web.service.CommonHttpService;
import com.freecharge.web.service.CommonHttpService.HttpResponse;
import com.google.gson.Gson;

@Service
public class CommonOneCheckService {

    private Logger logger = LoggingFactory.getLogger(FCWalletService.class);

    @Autowired
    private CommonHttpService httpService;
    @Autowired
    private FCProperties      fcProperties;
    @Autowired
    MetricsClient             metricsClient;
    @Autowired
    CloudWatchMetricsClient   cloudWatchMetricsClient;

    public CommonHttpService.HttpResponse getSDResponse(String metricName,
                                String requestBody,
                                Map<String, String> headers,
                                String method,
                                Map<String, String> params,
                                int timeout) {
        final long startTime=System.currentTimeMillis();;
        String url=fcProperties.getProperty(FCProperties.ONECHECK_HOST_URL)+metricName;
        if ("GET".equalsIgnoreCase(method)) {

            try {

                CommonHttpService.HttpResponse response = httpService.fireGetRequestWithTimeOutAndCustomHeader(url, params,
                        timeout, headers);
                logger.info("url:"+url+"\nRequest body:"+requestBody+"\nmethod:"+method);
                recordWalletApiMetrics(response, metricName, startTime);
                if (response != null) {
                    logger.info("Got response from SD : " + response.getResponseBody());
                }
                return response;
            } catch (IOException e) {
                logger.error("Error while making GET request for " + url + " with params " + (params!=null ? params.toString() : ""), e);
                recordException(metricName, startTime);
                return null;
            }
        } else {
            try {
                
                CommonHttpService.HttpResponse response = httpService.firePostRequestWithCustomHeader(url, requestBody,
                        false, timeout, headers);
                logger.info("url:"+url+"\nRequest body:"+requestBody+"\nmethod:"+method);
                recordWalletApiMetrics(response, metricName, startTime);
                logger.info("url:"+url+"\nRequest body:"+requestBody+"\nmethod:"+method);
                if (response != null) {
                    logger.info("Got response from SD : " + response.getResponseBody());
                }
                return response;
            } catch (IOException e) {
                logger.error("Error while making POST request for " + url + " with params " + (params!=null ? params.toString() : ""), e);
                recordException(metricName, startTime);
                return null;
            }
        }
    }

    private void recordException(String metricName, long startTime) {
        metricsClient.recordCount("OneCheckAPI", metricName + ".IOException", 1);
        /*cloudWatchMetricsClient.recordCount("OneCheckAPI", metricName + ".IOException", 1);*/
        metricsClient.recordLatency("OneCheckAPI", metricName+".IOException", System.currentTimeMillis() - startTime);
        /*cloudWatchMetricsClient.recordLatency("OneCheckAPI", metricName+".IOException",
                System.currentTimeMillis() - startTime);*/
        
    }

    private void recordWalletApiMetrics(HttpResponse response, String metricName, long startTime) {
        try {
            metricsClient.recordLatency("OneCheckAPI", metricName, System.currentTimeMillis() - startTime);
            /*cloudWatchMetricsClient.recordLatency("OneCheckAPI", metricName, System.currentTimeMillis() - startTime);*/
    
            if (response == null) {
                metricsClient.recordCount("OneCheckAPI", metricName + ".null", 1);
                /*cloudWatchMetricsClient.recordCount("OneCheckAPI", metricName + ".null", 1);*/
                return;
            }
    
            SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(response.getResponseBody(),
                    SDDefaultResponseDO.class);
            if (sdDefaultResponseDO != null) {
                if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                    metricsClient.recordCount("OneCheckAPI",
                            metricName + ".errorCode." + sdDefaultResponseDO.getError().get("errorCode"), 1);
                    /*cloudWatchMetricsClient.recordCount("OneCheckAPI",
                            metricName + ".errorCode." + sdDefaultResponseDO.getError().get("errorCode"), 1);*/
                } else {
                    metricsClient.recordCount("OneCheckAPI", metricName + ".errorCode.UNKNOWN", 1);
                   /* cloudWatchMetricsClient.recordCount("OneCheckAPI", metricName + ".errorCode.UNKNOWN", 1);*/
                }
            } else {
                metricsClient.recordCount("OneCheckAPI", metricName + ".errorCode.UNKNOWN_RESPONSE", 1);
                /*cloudWatchMetricsClient.recordCount("OneCheckAPI", metricName + ".errorCode.UNKNOWN_RESPONSE", 1);*/
            }
        } catch (Exception e) {
            logger.error("Caught exception on recording latency for metric " + metricName, e);
        }

    }
}