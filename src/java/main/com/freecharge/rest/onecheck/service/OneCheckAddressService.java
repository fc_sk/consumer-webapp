package com.freecharge.rest.onecheck.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.codedeploy.model.ErrorCode;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.onecheck.model.AddAddressResponseDO;
import com.freecharge.rest.onecheck.model.Address;
import com.freecharge.rest.onecheck.model.DeleteAddressRequestDO;
import com.freecharge.rest.onecheck.model.DeleteAddressResponseDO;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse;
import com.freecharge.rest.onecheck.model.SDDefaultResponseDO;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.web.service.CommonHttpService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class OneCheckAddressService {

    private Logger                  logger = LoggingFactory.getLogger(OneCheckAddressService.class);

    @Autowired
    private CommonOneCheckService   commonOneCheckService;

    @Autowired
    private FCProperties            fcProperties;

    @Autowired
    private OneCheckFCWalletService oneCheckWalletService;
   
    private final String ERROR_CODE_KEY = "errorCode";

    public String getUserAddresses(HttpServletRequest request) {
       /* if (oneCheckWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.GET_USER_ADDRESSES_URL).getResponseMap());
        }

        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.GET_USER_ADDRESSES_URL), null, getHeaders(request), "GET",
                null, OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForGetAddresses(response,
                    fcProperties.getProperty(OneCheckConstants.GET_USER_ADDRESSES_URL));
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "Error gettinng wallet balance limits.");
            return new Gson().toJson(errorMap);
        }*/
    	Object addressList = new ArrayList<>();
        TypeToken<List<Address>> tt = new TypeToken<List<Address>>() {};
        List<Address> responseDO = new Gson().fromJson(new Gson().toJson(addressList), tt.getType());
        return new Gson().toJson(responseDO);
    }

    private String extractResponseFromResponseBodyForAddAddress(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                AddAddressResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        AddAddressResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "No response from underlying SD api");
            return new Gson().toJson(errorMap);
        }
    }

    private String extractResponseFromResponseBodyForDeleteAddress(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                DeleteAddressResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        DeleteAddressResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "No response from underlying SD api");
            return new Gson().toJson(errorMap);
        }
    }

    private String extractResponseFromResponseBodyForGetAddresses(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                Object addressList = sdDefaultResponseDO.getData().get("addresses");
                if (addressList == null) {
                    addressList = new ArrayList<>();
                }
                TypeToken<List<Address>> tt = new TypeToken<List<Address>>() {};
                
                List<Address> responseDO = new Gson().fromJson(new Gson().toJson(addressList), tt.getType());
                
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "No response from underlying SD api");
            return new Gson().toJson(errorMap);
        }
    }

    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> headers = new HashMap<>();
        headers.put(OneCheckConstants.TOKEN, request.getHeader(OneCheckConstants.TOKEN));
        headers.put(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER,
                request.getHeader(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER));
        headers.put(OneCheckConstants.USER_AGENT, request.getHeader(OneCheckConstants.USER_AGENT));
        headers.put(OneCheckConstants.CONTENT_TYPE, request.getHeader(OneCheckConstants.CONTENT_TYPE));
        headers.put(OneCheckConstants.ACCEPT, request.getHeader(OneCheckConstants.ACCEPT));
        return headers;
    }

    public String addAddress(Address address, HttpServletRequest request) {
        /*if (oneCheckWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.ADD_ADDRESS_URL).getResponseMap());
        }

        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.ADD_ADDRESS_URL), new Gson().toJson(address),
                getHeaders(request), "POST", null, OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForAddAddress(response,
                    fcProperties.getProperty(OneCheckConstants.ADD_ADDRESS_URL));
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "Error generating otp.");
            return new Gson().toJson(errorMap);
        }*/
    	Map<String, String> errorMap = new HashMap<>();
        errorMap.put("errorCode", ErrorCode.IAM_ROLE_PERMISSIONS.toString());
        errorMap.put("errorMessage", "Communication addresses are no longer supported on Freecharge.");
        return new Gson().toJson(errorMap);
    }

    public String deleteAddress(DeleteAddressRequestDO deleteAddressRequestDO, HttpServletRequest request) {
       /* if (oneCheckWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.DELETE_ADDRESS_URL).getResponseMap());
        }
        if (FCUtil.isEmpty(deleteAddressRequestDO.getUserAddressId())) {
            return FCWalletErrorCode.E04.toJson();
        }
        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.DELETE_ADDRESS_URL),
                new Gson().toJson(deleteAddressRequestDO), getHeaders(request), "POST", null,
                OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForDeleteAddress(response,
                    fcProperties.getProperty(OneCheckConstants.DELETE_ADDRESS_URL));
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "Error generating otp.");
            return new Gson().toJson(errorMap);
        }*/
    	 Map<String, String> errorMap = new HashMap<>();
         errorMap.put("errorCode", ErrorCode.IAM_ROLE_PERMISSIONS.toString());
         errorMap.put("errorMessage", "Communication addresses are no longer supported on Freecharge.");
         return new Gson().toJson(errorMap);
    }
}