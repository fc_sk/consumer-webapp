package com.freecharge.rest.onecheck.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.codedeploy.model.ErrorCode;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.timeout.HttpResponse;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.OTPGenerateRequestDO;
import com.freecharge.rest.onecheck.model.OTPGenerateResponseDO;
import com.freecharge.rest.onecheck.model.OTPValidateRequestDO;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse;
import com.freecharge.rest.onecheck.model.SDDefaultResponseDO;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.wallet.OnecheckCommandBuilder;
import com.freecharge.web.service.CommonHttpService;
import com.google.gson.Gson;

@Service
public class OneCheckOTPService {

    private Logger logger = LoggingFactory.getLogger(OneCheckOTPService.class);

    @Autowired
    private CommonOneCheckService commonOneCheckService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private OneCheckFCWalletService oneCheckWalletService;
    
    @Autowired
    private ExternalClientCallService externalClientCallService;
    
    private final String ERROR_CODE_KEY = "errorCode";

    public String generateOtp(OTPGenerateRequestDO otpGenerateRequestDO, HttpServletRequest request) {
    	if (oneCheckWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.OTP_GENERATE_URL).getResponseMap());
        }
    	String url=fcProperties.getProperty(FCProperties.ONECHECK_HOST_URL)+fcProperties.getProperty(OneCheckConstants.OTP_GENERATE_URL);
        String responseBody = getSDResponse(url, new Gson().toJson(otpGenerateRequestDO), request, "oneCheckOTPService", "generateOtp");
        if (responseBody != null) {
            return extractResponseFromResponseBodyForGenerate(responseBody, fcProperties.getProperty(OneCheckConstants.OTP_GENERATE_URL));
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "Error generating otp.");
            return new Gson().toJson(errorMap);
        }
    }

    private String extractResponseFromResponseBodyForGenerate(String responseBody, String url) {
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey(ERROR_CODE_KEY)) {
            	logger.info("SDDefaultResponseDO Error:"+sdDefaultResponseDO.getError());
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                OTPGenerateResponseDO responseDO = new Gson().fromJson((new Gson().toJson(sdDefaultResponseDO.getData())),
                        OTPGenerateResponseDO.class);
                logger.info("ResponseDo otpid:"+responseDO.getOtpId());
                return new Gson().toJson(responseDO);
            }
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "No response from underlying SD api");
            return new Gson().toJson(errorMap);
        }
    }


    private String extractResponseFromResponseBodyForValidate(String responseBody, String url) {
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                OTPGenerateResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        OTPGenerateResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "No response from underlying SD api");
            return new Gson().toJson(errorMap);
        }
    }

    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> headers = new HashMap<>();
        headers.put(OneCheckConstants.TOKEN, request.getHeader(OneCheckConstants.TOKEN));
        headers.put(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER,
                request.getHeader(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER));
        headers.put(OneCheckConstants.USER_AGENT, request.getHeader(OneCheckConstants.USER_AGENT));
        headers.put(OneCheckConstants.CONTENT_TYPE, request.getHeader(OneCheckConstants.CONTENT_TYPE));
        headers.put(OneCheckConstants.ACCEPT, request.getHeader(OneCheckConstants.ACCEPT));
        return headers;
    }

    public String validateOtp(OTPValidateRequestDO otpValidateRequestDO, HttpServletRequest request) {

        if (oneCheckWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.OTP_VALIDATE_URL).getResponseMap());
        }
    	String url=fcProperties.getProperty(FCProperties.ONECHECK_HOST_URL)+fcProperties.getProperty(OneCheckConstants.OTP_VALIDATE_URL);
        String responseBody = getSDResponse(url, new Gson().toJson(otpValidateRequestDO), request, "oneCheckOTPService", "validateOtp");

        if (responseBody != null) {
            return extractResponseFromResponseBodyForValidate(responseBody, fcProperties.getProperty(OneCheckConstants.OTP_VALIDATE_URL));
        } else {
            Map<String, String> errorMap = new HashMap<>();
            errorMap.put("errorCode", ErrorCode.INTERNAL_ERROR.toString());
            errorMap.put("errorMessage", "Error generating otp.");
            return new Gson().toJson(errorMap);
        }
    }
    
	public String getSDResponse(String url, String requestBody, HttpServletRequest request, String serviceName, String metricName) {
		HttpClient httpClient = new HttpClient();
		PostMethod postMethod = new PostMethod(url);
		try {
			logger.info("requestBody:"+requestBody);
			Map<String, String> headers = getHeaders(request);
			for (Map.Entry<String, String> entry : headers.entrySet()) {
				postMethod.setRequestHeader(entry.getKey(), entry.getValue());
				logger.info("Header:" + postMethod.getRequestHeader(entry.getKey()));
			}
			postMethod.setRequestBody(requestBody);
			postMethod.setDoAuthentication(false);
			postMethod.getParams().setSoTimeout(3000);
		    
			Integer httpStatusCode = externalClientCallService
					.executeWithTimeOut(OnecheckCommandBuilder.getSDResponseCommand(httpClient, postMethod, serviceName, metricName));
			logger.info("Response for url:" + url + "\nhttpstatusCode:" + httpStatusCode + "\nStatus text:"
					+ postMethod.getStatusText());
			logger.info("ResponseBodyAsString:"+postMethod.getResponseBodyAsString());
			return postMethod.getResponseBodyAsString();
		} catch (Exception e) {
			logger.error("API error for Url:" + url + "Exception:", e);
		} finally {
			postMethod.releaseConnection();
		}
		return null;
	}
}

