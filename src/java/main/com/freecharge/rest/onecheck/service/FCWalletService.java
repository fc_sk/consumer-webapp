package com.freecharge.rest.onecheck.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.woden.wsdl20.extensions.http.HTTPHeader;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.services.PaymentPortalService;
import com.freecharge.rest.onecheck.VerifyAndWithdrawRequestDO;
import com.freecharge.rest.onecheck.model.AccountDetailsDO;
import com.freecharge.rest.onecheck.model.AddBankAccountRequestDO;
import com.freecharge.rest.onecheck.model.AddBankAccountResponseDO;
import com.freecharge.rest.onecheck.model.DeviceInformation;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.GetCounterResponseDO;
import com.freecharge.rest.onecheck.model.GetSeedRequestDO;
import com.freecharge.rest.onecheck.model.GetSeedResponseDO;
import com.freecharge.rest.onecheck.model.OneCheckRawResponse;
import com.freecharge.rest.onecheck.model.SDDefaultResponseDO;
import com.freecharge.rest.onecheck.model.UpdateBankAccountRequestDO;
import com.freecharge.rest.onecheck.model.UpdateBankAccountResponseDO;
import com.freecharge.rest.onecheck.model.VerifyAndWithdrawResponseDO;
import com.freecharge.rest.onecheck.model.WalletBalanceLimitsResponseDO;
import com.freecharge.rest.onecheck.model.WalletBalanceResponseDO;
import com.freecharge.rest.onecheck.model.WalletCreditInitReqDo;
import com.freecharge.rest.onecheck.model.WalletCreditInitResponseDo;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.exception.IMSClientException;
import com.freecharge.web.service.CommonHttpService;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.snapdeal.ims.dto.UserDetailsDTO;

@Service
public class FCWalletService {

    private Logger                  logger = LoggingFactory.getLogger(FCWalletService.class);

    @Autowired
    private FCProperties            fcProperties;

    @Autowired
    private OneCheckFCWalletService oneCheckFCWalletService;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private CommonOneCheckService   commonOneCheckService;
    
    @Autowired
    private PaymentPortalService paymentPortalService;
    
    private final String ERROR_CODE_KEY = "errorCode";
    
    private final BigDecimal GLOBAL_WITHDRAW_LIMIT = new BigDecimal(100);
    
    private static final ObjectMapper jsonHandler  = new ObjectMapper();

    public WalletCreditInitResponseDo handleWalletCreditInitTransacion(WalletCreditInitReqDo creditInitReqDo) {
        WalletCreditInitResponseDo responseDo = new WalletCreditInitResponseDo();
        responseDo.setTxnAmount(creditInitReqDo.getTxnAmount());
        return responseDo;
    }

    public String getWalletBalance(HttpServletRequest request) {
        if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.WALLET_BALANCE_URL).getResponseMap());
        }

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("showlimit", "true");
        
        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.WALLET_BALANCE_URL), null, getHeaders(request), "GET",
                requestMap, OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForWalletBalance(response,
                    fcProperties.getProperty(OneCheckConstants.WALLET_BALANCE_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForWalletBalance(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                WalletBalanceResponseDO balanceDo = new WalletBalanceResponseDO();
                
                Map<String, Object> responseMap = sdDefaultResponseDO.getData();
                balanceDo.setGeneralBalance(responseMap.get("generalBalance").toString());
                balanceDo.setVoucherBalance(responseMap.get("voucherBalance").toString());
                balanceDo.setTotalBalance(responseMap.get("totalBalance").toString());
                
                Map<String, Object> limitMap = (Map<String, Object>) responseMap.get("balanceLimits");
                Object withdrawLimitObject = limitMap.get("WITHDRAWABLE__BALANCE");
                
                if (withdrawLimitObject != null && StringUtils.isNotBlank(withdrawLimitObject.toString())) {
                    BigDecimal withdrawLimit = new BigDecimal(withdrawLimitObject.toString());
                    
                    if (isLessThanGlobalWithdrawLimit(withdrawLimit)) {
                        balanceDo.setWithdrawLimit("0.0");
                    } else {
                        balanceDo.setWithdrawLimit(withdrawLimit.toString());
                    }
                } else {
                    balanceDo.setWithdrawLimit("0.0");
                }
                
                return new Gson().toJson(balanceDo);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private boolean isLessThanGlobalWithdrawLimit(BigDecimal withdrawLimit) {
        return withdrawLimit.compareTo(GLOBAL_WITHDRAW_LIMIT) < 0;
    }

    private String extractResponseFromResponseBodyForWalletBalanceLimits(CommonHttpService.HttpResponse response,
            String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                WalletBalanceLimitsResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        WalletBalanceLimitsResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForVerifyAndWithdraw(CommonHttpService.HttpResponse response,
            String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCAPIErrorCode((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY), "VerifyAndWithdraw").toJson();
            } else {
                VerifyAndWithdrawResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        VerifyAndWithdrawResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }
    
    private String extractResponseFromResponseBodyForGetSeed(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                GetSeedResponseDO gsrd = new GetSeedResponseDO();
                
                Map<String, Object> responseMap = sdDefaultResponseDO.getData();
                Double expiryTsString = (Double) responseMap.get("validUpto");
                String seed = (String) responseMap.get("seed");
                Boolean rsaEnabled = (Boolean) responseMap.get("rsaEnabled");
                
                gsrd.setSeed(seed);
                gsrd.setValidUpto(expiryTsString.longValue());
                gsrd.setRsaEnabled(rsaEnabled);

                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();

                return gson.toJson(gsrd);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForGetCounter(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                
                Map<String, Object> responseMap = sdDefaultResponseDO.getData();
                double timeInMillis = (double) responseMap.get("time");
                
                GetCounterResponseDO gcrd = new GetCounterResponseDO();
                gcrd.setTime(new Double(timeInMillis).longValue());

                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.S").create();
                
                return gson.toJson(gcrd);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForAddBankAccount(CommonHttpService.HttpResponse response, String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                
                return FCWalletErrorCode.fromOCAPIErrorCode((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY), "addBankAccount").toJson();
            } else {
                AddBankAccountResponseDO responseDO = new Gson().fromJson(new Gson().toJson(sdDefaultResponseDO.getData()),
                        AddBankAccountResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForUpdateBankAccount(CommonHttpService.HttpResponse response,
            String url) {
        String responseBody = response.getResponseBody();
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                UpdateBankAccountResponseDO responseDO = new Gson().fromJson(new Gson().toJson(
                        sdDefaultResponseDO.getData().get("bankAccountDetails")),
                        UpdateBankAccountResponseDO.class);
                return new Gson().toJson(responseDO);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    private String extractResponseFromResponseBodyForGettingBankAccounts(CommonHttpService.HttpResponse response,
            String url) {
        String responseBody = response.getResponseBody();
        logger.info(responseBody);
        SDDefaultResponseDO sdDefaultResponseDO = new Gson().fromJson(responseBody, SDDefaultResponseDO.class);
        if (sdDefaultResponseDO != null) {
            logger.info("SD Error code : " + sdDefaultResponseDO.getError() + " and data : " + sdDefaultResponseDO.getData());
            if (sdDefaultResponseDO.getError() != null && sdDefaultResponseDO.getError().containsKey("errorCode")) {
                return FCWalletErrorCode.fromOCErrorCodeToJson((String) sdDefaultResponseDO.getError().get(ERROR_CODE_KEY));
            } else {
                @SuppressWarnings("unchecked")
				List<AccountDetailsDO> responseDOList = new Gson().fromJson(new Gson().toJson(
                        sdDefaultResponseDO.getData().get("userAccounts")), List.class);
                return new Gson().toJson(responseDOList);
            }
        } else {
            logger.error("No response from SD api " + response.getStatusCode() + " for url " + url);
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    public Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> headers = new HashMap<>();
        headers.put(OneCheckConstants.TOKEN, request.getHeader(OneCheckConstants.TOKEN));
        headers.put(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER,
                request.getHeader(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER));
        headers.put(OneCheckConstants.USER_AGENT, request.getHeader(OneCheckConstants.USER_AGENT));
        headers.put(OneCheckConstants.CONTENT_TYPE, request.getHeader(OneCheckConstants.CONTENT_TYPE));
        headers.put(OneCheckConstants.ACCEPT, request.getHeader(OneCheckConstants.ACCEPT));
        headers.put("ipAddress", request.getRemoteAddr());
        headers.put("referer", request.getHeader("Referer"));
        headers.put("host", request.getHeader("x-forwarded-for"));
        logger.info("Header : " + headers);
        return headers;
    }

    public String getWalletBalanceLimits(HttpServletRequest request) {
        if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.WALLET_BALANCE_LIMITS_URL).getResponseMap());
        }

        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.WALLET_BALANCE_LIMITS_URL), null, getHeaders(request), "GET",
                null, OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForWalletBalanceLimits(response,
                    fcProperties.getProperty(OneCheckConstants.WALLET_BALANCE_LIMITS_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    public String verifyAndWithdraw(VerifyAndWithdrawRequestDO verifyAndWithdrawRequestDO, HttpServletRequest request) {
    	try {
    		if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
                return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.VERIFY_AND_WITHDRAW_URL).getResponseMap());
            }
        	String requestBody = jsonHandler.writeValueAsString(verifyAndWithdrawRequestDO);
        	logger.info("Request Body for verifyAndWithdraw : " +requestBody);
            CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(fcProperties.getProperty(OneCheckConstants.VERIFY_AND_WITHDRAW_URL), requestBody, getHeaders(request), "POST", null, OneCheckConstants.API_TIMEOUT);
            if (response != null) {
                return extractResponseFromResponseBodyForVerifyAndWithdraw(response,
                        fcProperties.getProperty(OneCheckConstants.VERIFY_AND_WITHDRAW_URL));
            } else {
                return FCWalletErrorCode.INTERNAL_ERROR.toJson();
            }
		} catch (IOException e) {
			logger.error("Some error while posting the data to the onecheck " + OneCheckConstants.VERIFY_AND_WITHDRAW_URL + " API ", e);
            throw new IllegalStateException(e);
		}
    }
    
    public String getSeed(GetSeedRequestDO getSeedRequestDO, HttpServletRequest request) {
        Gson gs = new Gson();
        JsonElement jsonTree = gs.toJsonTree(getSeedRequestDO);
        
        JsonObject jo = new JsonObject();
        jo.add("deviceInfo", jsonTree);
        jo.addProperty("source", "FC");
        
        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.GET_SEED_URL),
                jo.toString(), createRequestHeader(request), "POST", null,
                OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForGetSeed(response,
                    fcProperties.getProperty(OneCheckConstants.VERIFY_AND_WITHDRAW_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    public String getCounter(HttpServletRequest request) {
        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.GET_COUNTER_URL),
                null, createRequestHeader(request), "GET", null,
                OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForGetCounter(response,
                    fcProperties.getProperty(OneCheckConstants.VERIFY_AND_WITHDRAW_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

	public String addBankAccount(AddBankAccountRequestDO addBankAccountRequestDO, HttpServletRequest request) {
		if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
			return new Gson()
					.toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.ADD_BANK_ACCOUNT_URL).getResponseMap());
		}
		CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
				fcProperties.getProperty(OneCheckConstants.ADD_BANK_ACCOUNT_URL),
				new Gson().toJson(addBankAccountRequestDO), getHeaders(request), "POST", null,
				OneCheckConstants.API_TIMEOUT);
		logger.info("Account Url:" + fcProperties.getProperty(OneCheckConstants.ADD_BANK_ACCOUNT_URL));
		logger.info("AddbankAccountRequestDo:" + addBankAccountRequestDO.getAccountNumber() + "\nShort name:"
				+ addBankAccountRequestDO.getShortName());
		logger.info("user token:" + request.getHeader("token"));
		if (response != null) {
			return extractResponseFromResponseBodyForAddBankAccount(response,
					fcProperties.getProperty(OneCheckConstants.ADD_BANK_ACCOUNT_URL));
		} else {
			return FCWalletErrorCode.INTERNAL_ERROR.toJson();
		}
	}

    public String getBankAccounts(HttpServletRequest request) {
        if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.ADD_BANK_ACCOUNT_URL).getResponseMap());
        }

        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.GET_BANK_ACCOUNTS_URL), null, getHeaders(request), "GET",
                null, OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForGettingBankAccounts(response,
                    fcProperties.getProperty(OneCheckConstants.GET_BANK_ACCOUNTS_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    public String updateBankAccount(UpdateBankAccountRequestDO updateBankAccountRequestDO, HttpServletRequest request) {

        if (oneCheckFCWalletService.isAPIMockConfigEnabled()) {
            return new Gson().toJson(OneCheckRawResponse.mockResponse(OneCheckConstants.UPDATE_BANK_ACCOUNT_URL));
        }
        CommonHttpService.HttpResponse response = commonOneCheckService.getSDResponse(
                fcProperties.getProperty(OneCheckConstants.UPDATE_BANK_ACCOUNT_URL),
                new Gson().toJson(updateBankAccountRequestDO), getHeaders(request), "POST", null,
                OneCheckConstants.API_TIMEOUT);
        if (response != null) {
            return extractResponseFromResponseBodyForUpdateBankAccount(response,
                    fcProperties.getProperty(OneCheckConstants.UPDATE_BANK_ACCOUNT_URL));
        } else {
            return FCWalletErrorCode.INTERNAL_ERROR.toJson();
        }
    }

    public String validateAuthTokenForReferrer(String referrer, HttpServletRequest request) {
        /*
         * emailId
         * mobileNumber
         * firstName
         * middleName
         * lastName
         * displayName
         * gender
         * dob
         * languagePref
         * 
         */
        
        try {
            Map<String, String> userDetails  = oneCheckWalletService.getTransferToken(getHeaders(request));
            
            if (userDetails != null) {
                return new Gson().toJson(userDetails);
            }
        } catch (Exception e) {
            logger.error("Caught exception while doing validateAuthTokenForReferrer : " + referrer, e);
        }

        return FCWalletErrorCode.INTERNAL_ERROR.toJson();
    }
    
    public String generateTransferToken(String authToken) {
        
        try {
            Map<String, String> responseMap = oneCheckWalletService.generateTransferTOken(authToken);
            return new Gson().toJson(responseMap);
        } catch (IMSClientException e) {
            logger.error("Caught Exception while getting transfer token for token " + authToken, e);
        }
        
        return FCWalletErrorCode.INTERNAL_ERROR.toJson();
   }

   private Map<String, String> createRequestHeader(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getHeader("X-FORWARDED-FOR");

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = "NA";
        }
        
        if (StringUtils.isBlank(userAgent)) {
            userAgent = "NA";
        }
        
        
        Map<String, String> requestHeader = new HashMap<>();
        requestHeader.put(OneCheckConstants.USER_AGENT, userAgent);
        requestHeader.put(OneCheckConstants.CONTENT_TYPE, "application/json");
        requestHeader.put(OneCheckConstants.TOKEN, request.getHeader(OneCheckConstants.TOKEN));
        requestHeader.put(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER, ipAddress);
        return requestHeader;
   }
}