package com.freecharge.rest.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.gson.JsonObject;

public class CcsClientWrapper {
	private final Logger logger = LoggingFactory.getLogger(getClass());
	private ICCS ccsClient;
	
	@Autowired
	public void setCcsClient(ICCS ccsClient) {
		this.ccsClient = ccsClient;
	}

	public String getValue(String key) {
		String value = null;
		try {
			value = ccsClient.getConf(key);
		} catch (CCSException e) {
			logger.error("Caught CCSException is ", e);
		} catch (Exception e) {
			logger.error("Caught Exception is ", e);
		}
		return value;
	}
	
	public JsonObject getJsonValue(String key) {
		try {
			return ccsClient.getConf(key, JsonObject.class);
		} catch (CCSException e) {
			logger.error("Caught CCSException is ", e);
		} catch (Exception e) {
			logger.error("Caught Exception is ", e);
		}
		return null;
	}
}
