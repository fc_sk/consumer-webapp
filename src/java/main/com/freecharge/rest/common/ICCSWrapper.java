package com.freecharge.rest.common;

import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;

public class ICCSWrapper {
	public static ICCS get(String ccsHost, String basePath) {
		ICCSConfig ccsConfig = new AbstractCCSConfig();
		ccsConfig.setCcsHost(ccsHost);
		ccsConfig.setSocketTimeout(1000);
		ccsConfig.setConnectTimeout(500);
		ccsConfig.setDefaultAttachPath(basePath);
		ICCS ccs = new CCS();
		ccs.init(ccsConfig);
		return ccs;
	}
}
