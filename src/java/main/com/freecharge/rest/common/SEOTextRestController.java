package com.freecharge.rest.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.service.NewSEOService;
import com.freecharge.web.util.WebConstants;
import com.google.common.base.CharMatcher;

@Controller
@RequestMapping("/rest/*")
public class SEOTextRestController {
    private final Logger          logger         = LoggingFactory.getLogger(getClass());

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    NewSEOService                 newSEOService;

    public static final Integer   NO_OPERATOR_ID = 0;

    @RequestMapping(value = "operator/{operatorId}/{productId}/seotext", method = RequestMethod.GET)
    @NoLogin
    public @ResponseBody
    ResponseStatus getSEOText(@PathVariable int operatorId, @PathVariable int productId) {
        SEOAttributesBusinessDO seoBusinessDO = new SEOAttributesBusinessDO();
        ResponseStatus responseStatus = new ResponseStatus();
        Integer varOperatorId = operatorId;
        Integer varProductId = productId;

        /*
         * The operatorId is 0 in case of
         * www.freecharge.in/online-mobile-recharge,online-dth-recharge and
         * online-datacard-recharge
         */
        if (varOperatorId == NO_OPERATOR_ID && varProductId != null) {
            try {
                String productType = FCConstants.productTypeToNameMap.get(FCConstants.productMasterMap
                        .get(varProductId));
                String channelId = Integer.toString(FCConstants.CHANNEL_ID_WEB);
                seoBusinessDO = newSEOService.populateSeoAttributes(productType, null, channelId);
                responseStatus.setStatus("Success");
                responseStatus.setResult(seoBusinessDO);
                return responseStatus;
            } catch (RuntimeException e) {
                responseStatus.setStatus("Fail");
                logger.error("Exception while getting SEOText for productId=" + varProductId, e);
                return responseStatus;
            }
        } else {
            try {
                OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorId);
                String productType = FCConstants.productTypeToNameMap.get(FCConstants.productMasterMap
                        .get(operatorMaster.getFkProductMasterId()));
                /*
                 * Database returns the operator name [for eg: Tata Photon Plus
                 * but the xml file format is tata_photon_plus] to convert the
                 * received operator name to required format the below code is
                 * used which replaces the space and other special character with underscore
                 */
                String operatorName = CharMatcher.anyOf("()").or(CharMatcher.WHITESPACE).replaceFrom(operatorMaster.getName(), "_"); 
                String channelId = Integer.toString(FCConstants.CHANNEL_ID_WEB);
                if (productType != null && operatorName != null && channelId != null) {
                    seoBusinessDO = newSEOService.populateSeoAttributes(productType, operatorName, channelId);
                } else {
                    seoBusinessDO = newSEOService
                            .populateSeoAttributes(WebConstants.PRODUCT_TYPE_HOME_PAGE, null, null);
                }
                responseStatus.setStatus("Success");
                responseStatus.setResult(seoBusinessDO);
            } catch (RuntimeException e) {
                responseStatus.setStatus("Fail");
                logger.error("Exception while getting SEOText for operatorId=" + operatorId, e);
            }
            return responseStatus;
        }
    }
}
