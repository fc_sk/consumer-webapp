package com.freecharge.rest.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.service.CommonService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.ResponseStatus;
/**
 *  User: Rajani 
 *  Date: 20/01/14 
 */
@Controller
@RequestMapping("/*")
public class RefreshFCConstantsController extends AbstractRestController {
    
    @Autowired
    private CommonService commonService;
    
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @RequestMapping(value = "/protected/rest/operatormaster/refresh", method = RequestMethod.GET)
    public @ResponseBody ResponseStatus refreshOperatorMaster() {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            commonService.refreshOperatorMaster();
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("OperatorMaster Updated");
        } catch(Exception e) {
            logger.error("Error while Updating OperatorMaster=",e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("OperatorMaster not Updated");
        }
        return responseStatus;
    }
    
    @RequestMapping(value = "/protected/rest/statemaster/refresh", method = RequestMethod.GET)
    public @ResponseBody ResponseStatus refreshStateMasterList() {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            commonService.refreshStateMasterListAndMap();
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult("StateMaster Updated");
        } catch(Exception e) {
            logger.error("Error while Updating StateMaster=",e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("StateMaster not Updated");
        }
        return responseStatus;
    }
    

}
