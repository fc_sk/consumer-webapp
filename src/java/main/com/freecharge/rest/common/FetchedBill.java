package com.freecharge.rest.common;

import java.io.Serializable;

public class FetchedBill implements Serializable{
	
	private Integer id;
	private String sessionId;
	private Integer billerId;
	private Float billAmount;
	private String accountId;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getBillerId() {
		return billerId;
	}
	public void setBillerId(Integer billerId) {
		this.billerId = billerId;
	}
	public Float getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(Float billAmount) {
		this.billAmount = billAmount;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	@Override
	public String toString() {
		return "FetchedBill [id=" + id + ", sessionId=" + sessionId + ",  billerId="
				+ billerId + ", billAmount=" + billAmount + ", accountId=" + accountId + "]";
	}
	
}
