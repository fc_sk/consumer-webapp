package com.freecharge.rest.common;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.app.domain.entity.MetroReportDataEntity;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.rest.model.MetroReportDataRequest;

import com.freecharge.admin.service.AccessControlService;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.recharge.services.MetroReportService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.MetroReportData;
import com.freecharge.rest.model.MetroReportDataRequest;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/*")
public class MetroReportController extends AbstractRestController{

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private MetroReportService metroReportService;

	@Autowired
	protected AccessControlService accessControlService;

	@Autowired
	private AppConfigService appConfigService;

    private List<MetroReportData> reportData;

    private MetroReportDataRequest metroDataRequest;

    @RequestMapping(value = "/rest/metro/data", method = RequestMethod.GET)
	public @ResponseBody
	final ModelAndView getMetroReportDataForDay(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
		String source = httpRequest.getParameter("source");
		String fromDateStr = httpRequest.getParameter("start_date");
		String toDateStr = httpRequest.getParameter("end_date");
		String afcCardNoStr = httpRequest.getParameter("afcCardNo");
		String paymentStatusStr = httpRequest.getParameter("paymentStatus");
		String orderId = httpRequest.getParameter("orderId");
		Boolean paginationRequired = false;
        Date paginateDate = null;
        Integer pageNumber = null;
        try {
            pageNumber = Integer.parseInt(httpRequest.getParameter("pageNumber"));
        }
        catch (Exception e) {
            pageNumber = 1;
        }

		if(metroDataRequest!=null && source.equalsIgnoreCase("dataPage")) {
			fromDateStr = metroDataRequest.getStartDate();
			toDateStr = metroDataRequest.getEndDate();
			afcCardNoStr = metroDataRequest.getAfcCardNumber();
			paymentStatusStr = metroDataRequest.getPaymentStatus();
			orderId = metroDataRequest.getOrderId();
		}

		Long noOfPages = null;
        reportData = new ArrayList<>();
		if (orderId != null && orderId.isEmpty()){
			orderId = null;
		}
		if (afcCardNoStr != null && afcCardNoStr.isEmpty()){
			afcCardNoStr = null;
		}
		if (paymentStatusStr != null && paymentStatusStr.isEmpty()){
			paymentStatusStr = null;
		}

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/dd/MM");
        Date fromDate = null;
        Date toDate = null;
        if((toDateStr == null || toDateStr.isEmpty()) && (fromDateStr == null || fromDateStr.isEmpty())) {
            if(null == orderId || orderId.isEmpty()) {
                toDate = new Date();
                Calendar calendarHelper = Calendar.getInstance();
                calendarHelper.setTime(toDate);
                if (afcCardNoStr == null || afcCardNoStr.isEmpty()){
                    calendarHelper.add(Calendar.DAY_OF_MONTH, -1);
                } else{
                    calendarHelper.add(Calendar.MONTH, -2);
                }
                fromDate = calendarHelper.getTime();
                toDateStr = formatter.format(toDate);
                fromDateStr = formatter.format(fromDate);
            }
        }
        else {
            try {
                fromDate = formatter.parse(fromDateStr);
                toDate = formatter.parse(toDateStr);
            } catch (ParseException e) {
                logger.error("Error caught in parsing date", e);
                e.printStackTrace();
                return null;
            }
			if((afcCardNoStr == null || afcCardNoStr.isEmpty()) && (orderId == null || orderId.isEmpty())) {
				paginationRequired = true;
                Calendar calendarHelper = Calendar.getInstance();
                calendarHelper.setTime(fromDate);
                calendarHelper.add(Calendar.DAY_OF_MONTH, pageNumber-1);
                paginateDate = calendarHelper.getTime();
			}
        }
		Boolean paymentStatus = null;
		if ("0".equalsIgnoreCase(paymentStatusStr)){
			paymentStatus = false;
		} else if ("1".equalsIgnoreCase(paymentStatusStr)) {
			paymentStatus = true;
		}
		metroDataRequest = new MetroReportDataRequest();
		metroDataRequest.setAfcCardNumber(afcCardNoStr);
		metroDataRequest.setEndDate(toDateStr);
		metroDataRequest.setOrderId(orderId);
		metroDataRequest.setPaymentStatus(paymentStatusStr);
		metroDataRequest.setStartDate(fromDateStr);
		if(paginationRequired) {
			reportData = metroReportService.searchMetroTransactionHistory(paginateDate, paginateDate, afcCardNoStr, paymentStatus, orderId);
			noOfPages = metroReportService.getNumberOfPagesOfMetroData(toDate, fromDate);
			model.addAttribute("noOfPages", noOfPages);
			model.addAttribute("currentPage", pageNumber);
		}
		else {
			reportData = metroReportService.searchMetroTransactionHistory(toDate, fromDate, afcCardNoStr, paymentStatus, orderId);
		}
		model.addAttribute("SampleDataMapList", reportData);
		model.addAttribute("InputDataMap",metroDataRequest);
		return new ModelAndView(ViewConstants.METRO_REPORT);
	}
	
	@RequestMapping(value = "/rest/metro/home", method = RequestMethod.GET)
	public final ModelAndView doReveresal(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws IOException {
		String userEmailId = FCSessionUtil.getLoggedInEmailId();
		if (!   hasAccess(userEmailId)) {
			return null;
		}
        metroDataRequest = null;
        reportData = new ArrayList<>();
		return new ModelAndView(ViewConstants.METRO_REPORT_HOME);
	}

	@RequestMapping(value = "/rest/metro/download", method = RequestMethod.GET)
	public void downloadMetroReport(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws IOException {
		response.setContentType("text/csv");
		String reportName = "Metro_Report.csv";
		response.setHeader("Content-disposition", "attachment;filename="+reportName);
 
		ArrayList<String> rows = new ArrayList<String>();
		rows.add("Freecharge Tranx ID,Date & Time of Payment Txn,Date & Time of Recharge Txn,Amount,Payment status,Payment Type,Recharge status,Name,Email,Phone No,AFC Card no,Refund Status, Refund Date");
		rows.add("\n");
		
		for(MetroReportData metroData : reportData) {
			rows.add(metroData.getOrderId() + "," + metroData.getPaymentTime() + "," + metroData.getRechargeTime() + "," + metroData.getAmount() +","
					+ metroData.getPaymentStatus() + "," + metroData.getPaymentType() + "," + metroData.getRechargeStatus()+ "," + metroData.getFirstName() +","
							+ metroData.getEmail() +"," + metroData.getPhoneNo() + "," + metroData.getAfcCardNumber()+ "," + metroData.getRefundStatus()+ "," + metroData.getRefundDate());
			rows.add("\n");
			
		}
 
		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}
 
		response.getOutputStream().flush();
		
	}

	private boolean hasAccess(String emailId) {
		JSONObject emailIdListJson = appConfigService.getMetroReportEmailIds();
		String emailIdList = (String) emailIdListJson.get("emailIds");
		String comma = ",";
		if(emailIdList.contains(emailId)) {
			return true;
		}
		return false;
	}


}
