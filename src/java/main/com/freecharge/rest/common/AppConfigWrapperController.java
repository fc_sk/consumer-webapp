package com.freecharge.rest.common;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/rest/appconfig")
@Controller
public class AppConfigWrapperController extends AbstractRestController {

    @Autowired
    private AppConfigService appConfigService;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @NoLogin
    @NoSession
    @Csrf(exclude = true)
    @RequestMapping(value="/custom/details", method= RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getCustomConfDetails(HttpServletRequest httpRequest) throws Exception {
        String key = httpRequest.getParameter("key");
        String fcChannel = httpRequest.getParameter("fcChannel");
        logger.info("Fetching details for key: "+key);

        String response = appConfigService.getChildProperty(key,fcChannel.toLowerCase());

        if (response == null) {
            throw new Exception("appconfig key not found for key = " + key);
        }

        return response;
    }
}
