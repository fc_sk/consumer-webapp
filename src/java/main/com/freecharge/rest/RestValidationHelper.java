package com.freecharge.rest;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.recharge.exception.InvalidOrderIDException;
import com.freecharge.web.util.WebConstants;

@Component
public class RestValidationHelper {
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired 
	private OrderService orderService;
	
	public void validateOrderIdBelongsToUser(String orderId) throws InvalidOrderIDException {
		if (orderId == null || orderId.isEmpty()){
			logger.error("Invalid order id received: " + orderId);
			throw new InvalidOrderIDException("Invalid order id, " + orderId);
		}
		FreechargeContext fc = FreechargeContextDirectory.get();
		if (fc != null){
			FreechargeSession fs = fc.getFreechargeSession();
			if (fs != null){
				Map<String, Object> map = fs.getSessionData();
		        Integer userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);
				Integer userIdFromOrder = orderService.getUserIdForOrder(orderId);
				if (userId != null && userId.equals(userIdFromOrder)){
					return;
				}
			}
		}
		throw new InvalidOrderIDException("Invalid order id, " + orderId);
	}
}
