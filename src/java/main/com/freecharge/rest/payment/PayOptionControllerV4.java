package com.freecharge.rest.payment;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.cardstorage.ICardStorageServiceV2;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.PaymentOptionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.model.FPSParams;
import com.freecharge.rest.model.ListCardsV4Request;
import com.freecharge.util.RequestUtil;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.google.gson.Gson;

@Controller
@RequestMapping("/rest/v4/*")
public class PayOptionControllerV4 {
    @Autowired
    @Qualifier("card-and-meta")
    ICardStorageServiceV2 cardStorageService;

    @Autowired
    PaymentOptionService paymentOptionService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    FCProperties fcProperties;

    @Autowired
    private MetricsClient metricsClient;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Csrf
    @NoSessionWrite
    @RequestMapping(value = "payoptions", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, Object> listV4(@RequestBody ListCardsV4Request listCardsV4Request,
                               HttpServletRequest httpRequest) {
        String appVersion = listCardsV4Request.getAppVersion();
        //HACK code for verifying if appversion is not integer then get the appversion from other tag.
        try {
            Integer.parseInt(listCardsV4Request.getAppVersion());
        } catch (Exception exception) {
            try {
                Integer.parseInt(listCardsV4Request.getFpsParams().getAppVersion());
                appVersion = listCardsV4Request.getFpsParams().getAppVersion();
            } catch (Exception ex) {
                logger.info(" Exception in appversion: invalid appversion :" + ex.getLocalizedMessage(), ex);
            }
        }

        String appType = listCardsV4Request.getAppType();
        String productType=listCardsV4Request.getProductType();
        logger.info("Into Payoptions V4 : appVersion :" + appVersion + " appType : " + appType);
        logger.info("Into Payoptions V4 : json : " + new Gson().toJson(listCardsV4Request));
        logger.info("Into Payoptions V4 : Product_type" + listCardsV4Request.getProductType());
        long start = System.currentTimeMillis();
        Integer userID = getUserID();
        long fkstart = System.currentTimeMillis();
        FPSParams fpsParams = listCardsV4Request.getFpsParams();
        logger.info("Into Payoptions : fpsParams" + fpsParams);
        Users user = userServiceProxy.getLoggedInUser();
        String emailId = user.getEmail().toLowerCase().trim();
        String oneCheckUserId = null;
        boolean isOneCheckUser = false;
        if ((oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId)) != null) {
            isOneCheckUser = true;
        }
        String userId = user.getUserId().toString();
        if (isOneCheckUser) {
            userId = oneCheckUserId;
        }
        fpsParams.setUserId(userId);
        fpsParams.setUserAgentStr(RequestUtil.getUserAgent(httpRequest));
        fpsParams.setClientIp(RequestUtil.getRealClientIpAddress(httpRequest));
        Map<String, Object> result = cardStorageService.listV4(userID, fpsParams,productType);
        metricsClient.recordLatency("API", "payoption.fortknox", System.currentTimeMillis() - fkstart);
        long dynamostart = System.currentTimeMillis();
        Map<String, String> paymentOptionUsed = paymentOptionService.fetchFromDynamoLastPayOption(emailId);
        metricsClient.recordLatency("API", "payoption.dynamo", System.currentTimeMillis() - dynamostart);
        if (!FCUtil.isEmpty(paymentOptionUsed))
            result.put("lastUsedPayment", paymentOptionUsed);
        else
            result.put("lastUsedPayment", new HashMap<String, String>());
        String[] iciciQuickCheckout = paymentOptionService.getProductListFromCCS(fcProperties.getCcsIciciQcList());
        Map<String, Object> iciciQCMap = new HashMap<String, Object>();
        if (iciciQuickCheckout != null) {
            iciciQCMap.put("products", iciciQuickCheckout);
        }
        result.put("icici_qc", iciciQCMap);
        String[] debitAtmList = paymentOptionService.getProductListFromCCS(fcProperties.getCcsDebitAtmList());
        if (debitAtmList != null) {
            Map<String, Object> debitAtmMap = new HashMap<String, Object>();
            debitAtmMap.put("products", debitAtmList);
            result.put("debitAtm", debitAtmMap);
        }

        String[] ittpProductsList = paymentOptionService.getProductListFromCCS(fcProperties.getCcsITTPProductsList());
        if (ittpProductsList != null) {
            Map<String, Object> ittpProductsMap = new HashMap<String, Object>();
            ittpProductsMap.put("products", ittpProductsList);
            result.put("ittpProducts", ittpProductsMap);
        }

        boolean upiEnabledForVersion = paymentOptionService.isUpiEnabledForAppVersion(appVersion, appType);
        logger.info("Into Payoptions : upiEnabledForVersion : " + upiEnabledForVersion);
        List<String> featureList = new ArrayList<String>();
        featureList.add("UPI");
        Map<String, Boolean> featureResponseMap = paymentOptionService.areFeaturesEligible(featureList, emailId,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));

        if (upiEnabledForVersion && featureResponseMap != null && featureResponseMap.get("UPI") != null) {
            logger.info("Into Payoptions : featureResponseMap UPI : " + featureResponseMap.get("UPI"));
            result.put("upi_enabled", featureResponseMap.get("UPI"));
        } else result.put("upi_enabled", false);


        metricsClient.recordLatency("API", "payoption.total", System.currentTimeMillis() - start);
        return result;
    }

    private Integer getUserID() {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Object userIdObject = fs.getSessionData().get(WebConstants.SESSION_USER_USERID);
        return (Integer) userIdObject;
    }

}
