package com.freecharge.rest.payment;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.cardstorage.ICardStorageService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/rest/card/*")
public class CardResourceController extends AbstractRestController {
	
	private final Logger              logger                         = LoggingFactory.getLogger(getClass());
	
    @Autowired
    @Qualifier("card-and-meta")
    ICardStorageService cardStorageService;
    
    @Autowired
    private AppConfigService appConfigService;

    @NoSessionWrite
    @RequestMapping(value = "list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, Object>  list() {
        Integer userID = getUserID();
        logger.info("userId:"+userID);
        return cardStorageService.list(userID);
    }
    
    @NoSessionWrite
    @RequestMapping(value = "{cardToken}/delete", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> delete(@PathVariable String cardToken) {
        Integer userID = getUserID();
        return cardStorageService.delete(userID, cardToken);
    }
    
    private Integer getUserID() {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Object userIdObject = fs.getSessionData().get(WebConstants.SESSION_USER_USERID);
        return (Integer) userIdObject;
    }
}
