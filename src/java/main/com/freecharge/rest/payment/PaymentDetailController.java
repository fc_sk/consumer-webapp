package com.freecharge.rest.payment;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.web.GetAllPaymentTxnForOrderIdResponse;
import com.freecharge.payment.dos.web.GetFailedPaymentTxnForOrderIdResponse;
import com.freecharge.payment.dos.web.GetPaymentTxnByMtxnIdResponse;
import com.freecharge.payment.dos.web.GetPendingPaymentTxnForOrderIdResponse;
import com.freecharge.payment.dos.web.GetSuccessfulPaymentTxnForOrderIdResponse;
import com.freecharge.payment.dos.web.PaymentTxn;
import com.freecharge.payment.services.PaymentDetailService;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/paymentDetail/*")
public class PaymentDetailController {

    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private PaymentDetailService paymentDetailService;
    
    @NoLogin
    @NoSessionWrite
    @Authenticate(api="paymentDetail.getPaymentTxnByMtxnId")
    @RequestMapping(value = "getPaymentTxnByMtxnId", method = RequestMethod.GET, produces = "application/json")
    private @ResponseBody GetPaymentTxnByMtxnIdResponse getPaymentTxnByMtxnId(@QueryParam("mtxnId") String mtxnId) {
        try{
            GetPaymentTxnByMtxnIdResponse response = new GetPaymentTxnByMtxnIdResponse();
            if (FCUtil.isEmpty(mtxnId)) {
                logger.info("Mtxn id is empty.");
                response.setErrorCode("E002");
                response.setErrorDesc("Invalid Mtxn Id.");
            } else {
                List<PaymentTxn> paymentTxn = paymentDetailService.getPaymentTxnByMtxnId(mtxnId);
                response.setPaymentTxns(paymentTxn);
            }
            
            return response;
        }catch(Exception e){
            logger.error("Error while getting payment txn for "+mtxnId, e);
            GetPaymentTxnByMtxnIdResponse response = new GetPaymentTxnByMtxnIdResponse();
            response.setErrorCode("E999");
            response.setErrorDesc("Internal Error.");
            return response;
        }
    }
    
}