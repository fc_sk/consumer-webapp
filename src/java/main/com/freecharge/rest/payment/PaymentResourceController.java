package com.freecharge.rest.payment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.web.NetBankingOption;
import com.freecharge.payment.dos.web.NetBankingOptionResponse;
import com.freecharge.payment.exception.PaymentTransactionNotFoundException;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.services.ITTPHandler;
import com.freecharge.payment.services.PaymentOptionService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.QuickCheckoutHandler;
import com.freecharge.payment.services.binrange.CardBin;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.PaymentMethod;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CheckoutWebDO;
import com.freecharge.web.webdo.PaymentTypeMasterWebDO;
import com.freecharge.web.webdo.PaymentTypeOptionWebDO;

@Controller
@RequestMapping("/rest/payment/*")
public class PaymentResourceController extends AbstractRestController {
    @Autowired
    private CartService               cartService;

    @Autowired
    private FreefundService           freefundService;

    private Logger                    logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppConfigService          appConfig;

    @Autowired
    private PaymentSuccessAction      paymentSuccessAction;

    @Autowired
    @Qualifier("payUGatewayHandler")
    private IGatewayHandler           payUGatewayHandler;

    @Autowired
    private MetricsClient             metricsClient;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private KestrelWrapper            kestrelWrapper;

    @Autowired
    private PaymentService            paymentService;

    @Autowired
    private UserService               userService;

    @Autowired
    private OneCheckWalletService     oneCheckWalletService;

    @Autowired
    FCProperties                      fcProperties;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    QuickCheckoutHandler              quickCheckoutHandler;
    
    @Autowired
    UserServiceProxy                  userServiceProxy;
    
    @Autowired
    PaymentsCache paymentsCache;
    
    @Autowired
    private WalletService walletService;
    
    @Autowired
    private PricingService pricingService;
    
    @Autowired
    private WalletWrapper walletWrapper;
    
    private static final String FAILURE_REASON = "failureReason";

    @Autowired
    private PaymentOptionService paymentOptionService;
    
    @NoSessionWrite
    @RequestMapping(value = "/{lookupId}/options", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getPaymentOptionsForLid(@PathVariable String lookupId) {
        CartBusinessDo cartBusinessDO = cartService.getCartBusinessDoByLid(lookupId);
        boolean isOneCheckUser = false;
        String oneCheckUserId = null;
        String email = userService.getEmail(cartBusinessDO.getUserId());
        if ((oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email)) != null) {
            isOneCheckUser = true;
        }
        ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
        String productTypeCode = "";
        if (ProductName.WalletCash == primaryProduct) {
            productTypeCode = "T_1";
        } else {
            productTypeCode = "V_1";
        }
        logger.info("Product Type : " + productTypeCode);
        List<PaymentTypeMasterWebDO> paymentTypeOptionsList = getPaymentTypeOptionsList(productTypeCode);

        List<PaymentMethod> paymentMethodList = convert(paymentTypeOptionsList, isOneCheckUser);
        CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
        List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
        addQuickCheckoutOptions(allowablePGList, oneCheckUserId, cartBusinessDo.getUserId(), primaryProduct.getProductType());
        HashMap<String, Object> options = new HashMap<>();
        options.put("allowablePGList", allowablePGList);
        options.put("options", paymentMethodList);
        return options;
    }

    private void addQuickCheckoutOptions(List<String> allowablePGList, String oneCheckUserId, Integer fcUserId, String productType) {
        if (allowablePGList != null && !allowablePGList.isEmpty() && allowablePGList.contains(PaymentConstants.ALL)) {
            if (!Arrays.asList(FCConstants.PRODUCT_TYPE_MOBILE, FCConstants.PRODUCT_TYPE_DATACARD,
                    FCConstants.PRODUCT_TYPE_DTH, FCConstants.PRODUCT_TYPE_METRO).contains(
                            productType)) {
                String userId = fcProperties + "-" + fcUserId;
                if (!StringUtils.isEmpty(oneCheckUserId)) {
                    userId = oneCheckUserId;
                }
                logger.info("Getting qc status for ICICI NB option");
                String qcStatus = quickCheckoutHandler.getQCStatusForUser(userId);
                logger.info("Response from KPay for QC status : " + qcStatus);
                if (!StringUtils.isEmpty(qcStatus)) {
                    if (qcStatus.equals("qc_enabled")) {
                        allowablePGList.add("ICICI_USER_QC");
                    } else if (qcStatus.equals("merch_qc_enabled")) {
                        allowablePGList.add("ICICI_MERC_QC");
                    }
                }
            }
        }
        
    }

    @NoSessionWrite
    @RequestMapping(value = "/iciciqc", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, String> getICICIQCOption() {
        HashMap<String, String> options = new HashMap<String, String>();
        boolean isOneCheckUser = false;
        String oneCheckUserId = null;
        Users loggedInUser = userServiceProxy.getLoggedInUser();
        if (loggedInUser == null) {
            return options;
        }
        if ((oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(loggedInUser.getEmail())) != null) {
            isOneCheckUser = true;
        }

        String userId = fcProperties + "-" + loggedInUser.getUserId();
        if (isOneCheckUser) {
            userId = oneCheckUserId;
        }
        String qcStatus = quickCheckoutHandler.getQCStatusForUser(userId);
        if (!StringUtils.isEmpty(qcStatus)) {
            if (qcStatus.equals("qc_enabled")) {
                options.put("ICICI_USER_QC", "true");
            } else if (qcStatus.equals("merch_qc_enabled")) {
                options.put("ICICI_MERC_QC", "true");
            }
        }
        logger.debug("WS response being returned : " + options);
        return options;
    }
    
    @NoSessionWrite
    @RequestMapping(value = "{lookupId}/plan", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> getPaymentPlan(@PathVariable String lookupId, HttpServletRequest request,
            HttpServletResponse response, Model model) throws Exception {
        logger.info("Lookup ID : " + lookupId);
        CheckoutWebDO checkoutWebDO = new CheckoutWebDO();
        checkoutWebDO.setLookupID(lookupId);

        String checkoutDelegate = WebConstants.DELEGATE_BEAN_CHECKOUT;
        CheckoutWebDO delegateResponse = (CheckoutWebDO) DelegateHelper.processRequest(request, response,
                checkoutDelegate, model, checkoutWebDO).getBaseWebDO();

        WalletEligibility walletEligibility = delegateResponse.getWalletEligibility();
        PaymentBreakup paymentBreakup = delegateResponse.getPaymentBreakup();

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("walletEligibility", walletEligibility);
        responseMap.put("paymentBreakup", paymentBreakup);

        return responseMap;
    }

    private List<PaymentTypeMasterWebDO> getPaymentTypeOptionsList(String productTypeCode) {
        List<PaymentTypeMasterWebDO> ptmws = new ArrayList<PaymentTypeMasterWebDO>();
        if (FCConstants.paymentTypesForProductAffiliate.isEmpty()
                || FCConstants.paymentTypesForProductAffiliate.get(productTypeCode) == null) {
            logger.info("FCConstants.paymentTypesForProductAffiliate is empty");
            logger.info("Getting payment options for " + productTypeCode.split("_")[0]);
            paymentService.getPaymentTypeOptionsForWeb(productTypeCode.split("_")[0], 1);
        }
        List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate.get(productTypeCode);
        if (ptmbs != null) {
            for (PaymentTypeMasterBusinessDO ptmb : ptmbs) {
                PaymentTypeMasterWebDO ptmw = new PaymentTypeMasterWebDO();
                ptmw.setCode(ptmb.getCode());
                ptmw.setId(ptmb.getId());
                ptmw.setName(ptmb.getName());
                List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation.get(ptmb.getRelationId());
                List<PaymentTypeOptionWebDO> ptows = new ArrayList<PaymentTypeOptionWebDO>();
                for (PaymentTypeOptionBusinessDO ptob : ptobs) {
                    PaymentTypeOptionWebDO ptow = new PaymentTypeOptionWebDO();
                    ptow.setCode(ptob.getCode());
                    ptow.setDisplayImg(ptob.getDisplayImg());
                    ptow.setDisplayName(ptob.getDisplayName());
                    ptow.setId(ptob.getId());
                    ptow.setImgUrl(ptob.getImgUrl());
                    ptows.add(ptow);
                }
                ptmw.setOptions(ptows);
                ptmws.add(ptmw);
            }
        }
        return ptmws;
    }

    private List<PaymentMethod> convert(List<PaymentTypeMasterWebDO> fromList, boolean isOneCheckUser) {
        List<PaymentMethod> toList = new ArrayList<>();

        for (PaymentTypeMasterWebDO from : fromList) {
            if (isOneCheckUser) {
                // Do not show freecharge wallet payment option
                if (from.getCode() != null
                        && from.getCode().equalsIgnoreCase(PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION)) {
                    continue;
                }
            } else {
                // Do not show one check / SD wallet to the user.
                if (from.getCode() != null
                        && from.getCode().equalsIgnoreCase(PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION)) {
                    continue;
                }
            }

            List<PaymentTypeOptionWebDO> options = from.getOptions();

            for (PaymentTypeOptionWebDO option : options) {
                PaymentMethod paymentMethod = new PaymentMethod();

                paymentMethod.setPaymentMethodCode(from.getCode());
                paymentMethod.setPaymentMethodName(from.getName());

                paymentMethod.setDisplayImage(option.getDisplayImg());
                paymentMethod.setDisplayName(option.getDisplayName());
                paymentMethod.setPaymentOptionCode(option.getCode());
                paymentMethod.setImgUrl(option.getImgUrl());

                toList.add(paymentMethod);
            }
        }

        return toList;
    }

    @NoSessionWrite
    @Deprecated
    @RequestMapping(value = "app/total_amount_to_pay", method = RequestMethod.GET, produces = "application/json")
    public String cartPrice(HttpServletRequest request, HttpServletResponse response, Model model) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String lid = request.getParameter(FCConstants.LOOKUPID);
        logger.info("Recieving request in cartSummary for LID : " + lid);
        if (lid == null || StringUtils.isBlank(lid)) {
            logger.error("No lid present in the request.... Session Id : " + fs.getUuid());
            model.addAttribute("error", "Technical error occured. Please try again later.");
        }

        try {
            CheckoutWebDO checkoutWebDO = new CheckoutWebDO();
            checkoutWebDO.setLookupID(lid);

            String checkoutDelegate = WebConstants.DELEGATE_BEAN_CHECKOUT;
            WebContext webContext = DelegateHelper.processRequest(request, response, checkoutDelegate, model,
                    checkoutWebDO);

            model.addAttribute("walletEligibility", ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletEligibility());
            model.addAttribute("payablePGAmount", ((CheckoutWebDO) webContext.getBaseWebDO()).getPaymentBreakup()
                    .getPayablePGAmount().getAmount());
            model.addAttribute("payableTotalAmount", ((CheckoutWebDO) webContext.getBaseWebDO()).getPaymentBreakup()
                    .getPayableTotalAmount().getAmount());
            model.addAttribute("paymentBreakup", ((CheckoutWebDO) webContext.getBaseWebDO()).getPaymentBreakup());
            if (model.containsAttribute("stateMaster")) {
                model.addAttribute("stateMaster", null);
            }
            model.addAttribute("status", ((CheckoutWebDO) webContext.getBaseWebDO()).getSatus());

            String status = ((CheckoutWebDO) webContext.getBaseWebDO()).getSatus();
            if (status == null || status.equals("fail")) {
                model.addAttribute("status", "fail");
                model.addAttribute("error", "Technical error occured. Please try again later.");
            }

        } catch (FCRuntimeException fcexp) {
            logger.error("Exception occured in checkout controller", fcexp);
            model.addAttribute("status", "fail");
            model.addAttribute("error", "Technical error occured. Please try again later.");
        } catch (Exception exp) {
            logger.error("Exception occured in checkout controller", exp);
            model.addAttribute("status", "fail");
            model.addAttribute("error", "Technical error occured. Please try again later.");
        }

        return "jsonView";
    }
    
    @RequestMapping(value = "{lookupId}/allowable_pg_list", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getAllowablePGList(@PathVariable String lookupId,
            HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

        logger.info("Request received for lookupId : " + lookupId + ", fcChannel : "
                + request.getParameter(FCConstants.FC_CHANNEL) + ", appVersion : "
                + request.getParameter(FCConstants.FC_ANDROID_APP_VERSION));

        CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
        String txnChannel = request.getParameter(FCConstants.FC_CHANNEL);
        String appVersion = request.getParameter(FCConstants.FC_ANDROID_APP_VERSION);
        int intTxnChannel = 0;

        if (!FCUtil.isEmpty(txnChannel)) {
            intTxnChannel = Integer.parseInt(txnChannel);
        }

        List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, intTxnChannel, appVersion);

        HashMap<String, Object> options = new HashMap<>();
        options.put("allowablePGList", allowablePGList);
        return options;
    }

    @RequestMapping(value = "{lookupId}/allowable_pg_list/add_cash", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getAllowablePGListAddCash(@PathVariable String lookupId,
            HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

        logger.info("Request received for lookupId : " + lookupId + ", fcChannel : "
                + request.getParameter(FCConstants.FC_CHANNEL) + ", appVersion : "
                + request.getParameter(FCConstants.FC_ANDROID_APP_VERSION));

        
        String txnChannel = request.getParameter(FCConstants.FC_CHANNEL);
        String appVersion = request.getParameter(FCConstants.FC_ANDROID_APP_VERSION);
        int intTxnChannel = 0;

        if (!FCUtil.isEmpty(txnChannel)) {
            intTxnChannel = Integer.parseInt(txnChannel);
        }
        logger.info("txnChannel : "+txnChannel+", appVersion : "+appVersion+", intTxnChannel : "+intTxnChannel);
        List<String> allowablePGList = freefundService.getAllowablePGList(lookupId, intTxnChannel, appVersion);

        HashMap<String, Object> options = new HashMap<>();
        options.put("allowablePGList", allowablePGList);
        return options;
    }
    
    /*
     * This API is to be used by APPS to get the allowablePGList along with the walletEligibility, paymentBreakup.
     * This API is *NOT* to be used by WEB as it does not return the payment "options"
     */
    @RequestMapping(value = "{lookupId}/v2/pglist", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getAllowablePGListV2(@PathVariable String lookupId,
            HttpServletRequest request, HttpServletResponse response) {
        HashMap<String, Object> responseMap = new HashMap<>();
        CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
        Double totalPayableAmount = pricingService.getPayableAmount(cartBusinessDo);
        
        String txnChannel = request.getParameter(FCConstants.FC_CHANNEL);
        String appVersion = request.getParameter(FCConstants.FC_ANDROID_APP_VERSION);
        int intTxnChannel = 0;

        if (!FCUtil.isEmpty(txnChannel)) {
            intTxnChannel = Integer.parseInt(txnChannel);
        }
        List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, intTxnChannel, appVersion);
        // ICICI Quickcheckout payment option will be passed in a different API call
        responseMap.put("allowablePGList", allowablePGList);

        Users user = userServiceProxy.getLoggedInUser();
        int userId = user.getUserId();
        Amount walletBalance = walletWrapper.getTotalBalance(userId);
        WalletEligibility walletEligibility = new WalletEligibility();
        if (!FCUtil.isEmpty(allowablePGList) && !allowablePGList.contains(PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {
            walletEligibility = walletService.calculateWalletEligibility(userId, new Amount(totalPayableAmount), cartBusinessDo, walletBalance);
        }
        PaymentBreakup paymentBreakup = walletService.calculatePaymentBreakup(userId, new Amount(totalPayableAmount), cartBusinessDo, walletEligibility, walletBalance);

        responseMap.put("walletEligibility", walletEligibility);
        responseMap.put("paymentBreakup", paymentBreakup);
        return responseMap;
    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "callback/payu")
    public void handlePayuS2SResponse(HttpServletRequest request, HttpServletResponse response) {
        try {
            long startTime = System.currentTimeMillis();
            Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
            logger.info("S2S Message body from Payu : " + PaymentUtil.getResponseDataWithoutCardData(requestMap));
            if (null == requestMap) {
                logger.info("Payu S2S message is empty, so NOT updating payments.");
                return;
            }

            if (!appConfig.isPayUS2SEnabled()) {
                logger.info("Payu S2S is not enabled, so NOT updating payments.");
                return;
            }

            logger.info("Payu S2S is running in payment process mode so going ahead with payments update");
            PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
            paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
            paymentResponseDO.setRequestMap(requestMap);
            Map<String, Object> paymentGatewayDataMap = null;
            paymentGatewayDataMap = (Map<String, Object>) payUGatewayHandler
                    .handleS2SPaymentResponse(paymentResponseDO);

            @SuppressWarnings({ "unchecked" })
            Map<String, String> resultMap = (Map<String, String>) paymentGatewayDataMap
                    .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);

            Boolean didPaymentSucceed = Boolean.valueOf((String) resultMap
                    .get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
            Boolean isPaymentAlreadyAcknolodged = Boolean.valueOf((String) resultMap
                    .get(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED));
            String orderId = (String) resultMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
            if (didPaymentSucceed && !isPaymentAlreadyAcknolodged) {
                metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE, "S2SEnqueue");
                String mtxnID = (String) resultMap.get(PaymentConstants.MERCHANT_TXN_ID);
                paymentSuccessAction.onPaymentSuccess(orderId, mtxnID);
            }

            if (!isPaymentAlreadyAcknolodged) {
                CardBin cardBin = PaymentUtil.getCardBinDataFromS2SResponse(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE,
                        requestMap);
                // Korath : Enqueue to consumers for card bin range table
                // updation
                kestrelWrapper.enqueueForCardBinRangeInsert(cardBin, didPaymentSucceed);
            }
            metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE
                    + "_s2s", System.currentTimeMillis() - startTime);
        } catch (PaymentTransactionNotFoundException e) {
            logger.info("No payment found; aborting S2S processing");
            metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE,
                    "S2S_PaymentTransactionNotFound");
        } catch (Exception e) {
            metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE, "S2S_Exception");
            logger.error("Exception while handling callbck from PayU", e);
        }
    }
    
    @NoLogin
    @RequestMapping(value = "{merchantName}/netBankingOptions")
    public @ResponseBody NetBankingOptionResponse getNetBankingOptions(@PathVariable String merchantName) {
        try{
            Merchant merchant = Merchant.valueOf(merchantName);
            return paymentOptionService.getNetBankingOptions(merchant);
        }catch(IllegalArgumentException e){
            logger.error("Invalid merchant Name "+merchantName);
            NetBankingOptionResponse response = new NetBankingOptionResponse("E001", "Invalid merchant.");
            response.setBanks(Collections.<NetBankingOption>emptyList());
            response.setPriorityBanks(Collections.<NetBankingOption>emptyList());
            return response;
        }catch (Exception e) {
            logger.error("Error getting netbanking options for merchant "+merchantName, e);
            List<NetBankingOption> netBankingOptions = paymentOptionService.getAllNetBankingOptions();
            NetBankingOptionResponse response = new NetBankingOptionResponse("E999", "Internal Server Error.");
            response.setBanks(netBankingOptions);
            response.setPriorityBanks(Collections.<NetBankingOption>emptyList());
            return response;
        }
    }

    @RequestMapping(value = "failure/reason/{orderId}", method = RequestMethod.GET)
    public @ResponseBody Map<String, String> getFailureReason(@PathVariable String orderId){
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put(FAILURE_REASON, paymentsCache.get(orderId+PaymentConstants.FAILURE_REASON_CACHE_KEY));
        logger.info("Failure reason for Order Id "+orderId+": "+responseMap );
        return responseMap;
    }
    
}
