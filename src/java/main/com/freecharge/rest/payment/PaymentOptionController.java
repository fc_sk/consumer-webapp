package com.freecharge.rest.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.cardstorage.ICardStorageService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.PaymentOptionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/rest/v2/*")
public class PaymentOptionController extends AbstractRestController {
	@Autowired
	@Qualifier("card-and-meta")
	ICardStorageService cardStorageService;

	@Autowired
	PaymentOptionService paymentOptionService;

	@Autowired
	UserServiceProxy userServiceProxy;

	@Autowired
	FCProperties fcProperties;
	
	@Autowired
    private MetricsClient metricsClient;
	
	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Csrf
	@NoSessionWrite
	@RequestMapping(value = "payoptions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Map<String, Object> list(HttpServletRequest httpRequest) {
		
		String appVersion = httpRequest.getParameter("fcversion");
		String appType = httpRequest.getParameter("fcAppType");
		
		long start = System.currentTimeMillis();
		Integer userID = getUserID();
		long fkstart = System.currentTimeMillis();
		Map<String, Object> result = cardStorageService.list(userID);
		metricsClient.recordLatency("API", "payoption.fortknox", System.currentTimeMillis() - fkstart);
		Users user = userServiceProxy.getLoggedInUser();
		String emailId = user.getEmail().toLowerCase().trim();
		long dynamostart = System.currentTimeMillis();
		Map<String, String> paymentOptionUsed = paymentOptionService.fetchFromDynamoLastPayOption(emailId);
		metricsClient.recordLatency("API", "payoption.dynamo", System.currentTimeMillis() - dynamostart);
		if (!FCUtil.isEmpty(paymentOptionUsed))
			result.put("lastUsedPayment", paymentOptionUsed);
		else
			result.put("lastUsedPayment", new HashMap<String, String>());
		String[] iciciQuickCheckout = paymentOptionService.getProductListFromCCS(fcProperties.getCcsIciciQcList());
		Map<String, Object> iciciQCMap = new HashMap<String, Object>();
		if (iciciQuickCheckout != null) {
			iciciQCMap.put("products", iciciQuickCheckout);
		}
		result.put("icici_qc", iciciQCMap);
		String[] debitAtmList = paymentOptionService.getProductListFromCCS(fcProperties.getCcsDebitAtmList());
		if (debitAtmList != null) {
			Map<String, Object> debitAtmMap = new HashMap<String, Object>();
			debitAtmMap.put("products", debitAtmList);
			result.put("debitAtm", debitAtmMap);
		}
		
		boolean upiEnabledForVersion = paymentOptionService.isUpiEnabledForAppVersion(appVersion, appType);
		List<String> featureList = new ArrayList<String>();
		featureList.add("UPI");
		Map<String, Boolean> featureResponseMap = paymentOptionService.areFeaturesEligible(featureList, emailId, fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
		
		if(upiEnabledForVersion && featureResponseMap != null && featureResponseMap.get("UPI") != null) {
			result.put("upi_enabled", featureResponseMap.get("UPI"));
		} else result.put("upi_enabled", false);
		
		
		metricsClient.recordLatency("API", "payoption.total", System.currentTimeMillis() - start);
		return result;
	}
	
	private Integer getUserID() {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Object userIdObject = fs.getSessionData().get(WebConstants.SESSION_USER_USERID);
		return (Integer) userIdObject;
	}
	
}


