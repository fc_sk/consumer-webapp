package com.freecharge.rest.merchandising;

public class MerchandisingData {
	private String imageUrl;
	private String ctaActionUrl;
	private String text;
	private String shortText;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCtaActionUrl() {
		return ctaActionUrl;
	}
	public void setCtaActionUrl(String ctaActionUrl) {
		this.ctaActionUrl = ctaActionUrl;
	}
	
	@Override
	public String toString() {
		return "MerchandisingData [imageUrl=" + imageUrl + ", ctaActionUrl=" + ctaActionUrl + ", text=" + text
				+ ", shortText=" + shortText + "]";
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getShortText() {
		return shortText;
	}
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
}
