package com.freecharge.rest.merchandising;

public class MerchandisingOffers extends MerchandisingData{
	private String offerId;

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	@Override
	public String toString() {
		return "MerchandisingOffers [offerId=" + offerId + ", toString()=" + super.toString() + "]";
	}
}
