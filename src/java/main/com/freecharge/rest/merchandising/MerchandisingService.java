package com.freecharge.rest.merchandising;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.common.framework.exception.MerchandisingException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Service
public class MerchandisingService {

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private AppConfigService appConfigService;
	private final String CATEGORY_GRID = ".category.grid";
	private final String  CATEGORY_OFFERS = ".category.offers";
	private final String CATEGORY_NEWADDITION = ".category.newAdditions";
	private final String ALL_CATEGORY_OFFERS = "all.category.offers";
	private final String ALL_CATEGORY_NEWADDITION = "all.category.newAdditions";
	
	public List<MerchandisingBanner> getMerchandisingAppBanner(Integer fcversion,Integer fcChannel,String propertyType) throws InvalidParameterException {
		logger.info("Enter into getMerchandisingAppBanner for fcversion: "+fcversion+" fcChannel: "+fcChannel+" propertyType: "+propertyType);
		validationParameter(fcversion,fcChannel,propertyType);
		String key= getChannelName(fcChannel)+"."+propertyType+".image";
		List<MerchandisingBanner> merchandisingBanners = getMerchandisingBanner(key,fcversion);
		logger.info("getMerchandisingAppBanner for fcversion: "+fcversion+" fcChannel: "+fcChannel+" propertyType: "+propertyType+" = "+merchandisingBanners);
		return merchandisingBanners;
	}
	
	private void validationParameter(Integer fcversion,Integer fcChannel,String propertyType) throws InvalidParameterException {
		if(fcversion==null) {
			throw new InvalidParameterException("appversion", null, "fcversion is required", ValidationErrorCode.INVALID_PARAMETER);
		}
		if(fcChannel==null) {
			throw new InvalidParameterException("fcChannel", null, "fcChannel is required", ValidationErrorCode.INVALID_PARAMETER);
		}
		if(FCUtil.isEmpty(propertyType)) {
			throw new InvalidParameterException("propertyType", null, "propertyType is required", ValidationErrorCode.INVALID_PARAMETER);
		}
	}
	
	private String getChannelName(Integer channelId){
	    return FCConstants.channelNameMasterMap.get(FCConstants.channelMasterMap.get(channelId)).toLowerCase();
	}
	
	private List<MerchandisingBanner> getMerchandisingBanner(String key,Integer fcversion) {
		Type listType = new TypeToken<ArrayList<MerchandisingBanner>>() {}.getType();
		
		String data=appConfigService.getChildProperty(AppConfigService.APP_BANNER_CONFIG,key);
		if(data==null || data.isEmpty()) {
			logger.error("getMerchandisingBanner data is empty/null for key: "+key+" fcversion: "+fcversion);
			throw new MerchandisingException();
		}
		List<MerchandisingBanner> bannerList = new ArrayList<>() ;
		try {
			bannerList=new Gson().fromJson(data,listType);
			Iterator<MerchandisingBanner> it = bannerList.iterator();
			while(it.hasNext()) {
				MerchandisingBanner banner = it.next();
				if(banner.getMinAppVersion()>fcversion || banner.getMaxAppVersion()<fcversion) {
					it.remove();
				}
			}
		} catch (Exception e){
			logger.error("getting banner data error",e);
		}
		return bannerList;
	}
	
	public MerchandisingBanner getMerchandisingEmailBanner(String propertType) {
		String key= "email."+propertType+".image";
		String data=appConfigService.getChildProperty(AppConfigService.EMAIL_BANNER_CONFIG,key);
		MerchandisingBanner banner = null ;
		try {
			banner=new Gson().fromJson(data,MerchandisingBanner.class);
		} catch (Exception e){
			logger.error("getting banner data error",e);
		}
		return banner;
	}
	
	public List<CategoryGrid> getCategoryGrid(Integer fcChannel) {
		String channelName = getChannelName(fcChannel);
		String key= channelName+CATEGORY_GRID;
		List<CategoryGrid> categories= new ArrayList<>();
        JSONObject config = appConfigService.getCustomProp(key);
        if(config==null) {
        	return categories;
        }
        Type listOfferType = new TypeToken<ArrayList<MerchandisingOffers>>() {}.getType();
        Type listNewAdditionType = new TypeToken<ArrayList<MerchandisingNewAdditions>>() {}.getType();
        JSONObject channelOffers = appConfigService.getCustomProp(channelName+CATEGORY_OFFERS);
        JSONObject channelAdditions = appConfigService.getCustomProp(channelName+CATEGORY_NEWADDITION);
        JSONObject allOffers = appConfigService.getCustomProp(ALL_CATEGORY_OFFERS);
        JSONObject allAdditions = appConfigService.getCustomProp(ALL_CATEGORY_NEWADDITION);
        for(Object categoryName:config.keySet()) {
        	String data = String.valueOf(config.get(categoryName));
        	CategoryGrid category=new Gson().fromJson(data,CategoryGrid.class);
        	category.setName(categoryName.toString());
        	
        	String offers=appConfigService.getChildProperty(channelOffers, categoryName.toString());
        	if(FCUtil.isEmpty(offers)) {
        		offers=appConfigService.getChildProperty(allOffers, categoryName.toString());
        	}
        	List<MerchandisingOffers> merchandisingOffers = new ArrayList<>();
        	if(!FCUtil.isEmpty(offers)) {
        		merchandisingOffers=new Gson().fromJson(offers,listOfferType);
        	}
        	category.setOffers(merchandisingOffers);
        	String additions=appConfigService.getChildProperty(channelAdditions, categoryName.toString());
        	if(FCUtil.isEmpty(additions)) {
        		additions=appConfigService.getChildProperty(allAdditions, categoryName.toString());
        	}
        	List<MerchandisingNewAdditions> merchandisingAdditions = new ArrayList<>();
        	if(!FCUtil.isEmpty(additions)) {
        		merchandisingAdditions=new Gson().fromJson(additions,listNewAdditionType);
        	}
        	category.setNewAdditions(merchandisingAdditions);
        	category.setTotalNewAdditions(merchandisingAdditions.size());
        	categories.add(category);
        }
        Collections.sort(categories);
		return categories;
	}
	
	
	
	
}
