package com.freecharge.rest.merchandising;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.LoadMerchandisingRequest;

@Controller
@RequestMapping("/rest/api/*")
public class MerchandisingController extends AbstractRestController {
	
	@Autowired
	private MerchandisingService merchandisingService;
	
	@NoLogin
	@RequestMapping(value = "merchandising/banner", method = RequestMethod.GET)
	public @ResponseBody List<MerchandisingBanner> getMenuOptions(LoadMerchandisingRequest request, HttpServletRequest httpRequest, HttpServletResponse response,
			ModelMap model) throws InvalidPathValueException, InvalidParameterException {
		Integer fcChannel = request.getFcChannel();
		Integer fcversion = request.getFcversion();
		String propertyType = request.getPropertyType();
		return merchandisingService.getMerchandisingAppBanner(fcversion, fcChannel, propertyType);
	}
	
	@NoLogin
	@RequestMapping(value = "category/grid", method = RequestMethod.GET)
	public @ResponseBody List<CategoryGrid> getCategorygrid(@RequestParam Integer fcChannel , HttpServletRequest httpRequest, HttpServletResponse response,
			ModelMap model) throws InvalidPathValueException, InvalidParameterException {
		return merchandisingService.getCategoryGrid(fcChannel);
	}
}
