package com.freecharge.rest.merchandising;

import java.util.List;

public class CategoryGrid implements Comparable<CategoryGrid>{
	private String name;
	private String imageUrl;
	private String ctaActionUrl;
	private Integer position;
	private Integer showBubbleRibben=0;
	private Integer totalNewAdditions;
	private List<MerchandisingOffers> offers;
	private List<MerchandisingNewAdditions> newAdditions;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCtaActionUrl() {
		return ctaActionUrl;
	}

	public void setCtaActionUrl(String ctaActionUrl) {
		this.ctaActionUrl = ctaActionUrl;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<MerchandisingOffers> getOffers() {
		return offers;
	}

	public void setOffers(List<MerchandisingOffers> offers) {
		this.offers = offers;
	}

	public List<MerchandisingNewAdditions> getNewAdditions() {
		return newAdditions;
	}

	public void setNewAdditions(List<MerchandisingNewAdditions> newAdditions) {
		this.newAdditions = newAdditions;
	}

	@Override
	public String toString() {
		return "CategoryGrid [name=" + name + ", imageUrl=" + imageUrl + ", ctaActionUrl=" + ctaActionUrl
				+ ", position=" + position + ", showBubbleRibben=" + showBubbleRibben + ", totalNewAdditions="
				+ totalNewAdditions + ", offers=" + offers + ", newAdditions=" + newAdditions + "]";
	}

	@Override
	public int compareTo(CategoryGrid o) {
		if(o.getPosition()==null && this.getPosition()==null){
			return 0;
		} 
		if(this.getPosition()==null)  {
			return -1;
		}
		if(o.getPosition()==null)  {
			return 1;
		}
		return this.getPosition().compareTo(o.getPosition());
	}

	public Integer getTotalNewAdditions() {
		return totalNewAdditions;
	}

	public void setTotalNewAdditions(Integer totalNewAdditions) {
		this.totalNewAdditions = totalNewAdditions;
	}

	public Integer getShowBubbleRibben() {
		return showBubbleRibben;
	}

	public void setShowBubbleRibben(Integer showBubbleRibben) {
		this.showBubbleRibben = showBubbleRibben;
	}

}
