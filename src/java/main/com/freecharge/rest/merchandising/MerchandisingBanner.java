package com.freecharge.rest.merchandising;

import org.codehaus.jackson.annotate.JsonIgnore;

public class MerchandisingBanner {
	private String imageUrl;
	private String  backgroundColor;
	private String ctaActionUrl;
	private Integer sequenceNo;
	@JsonIgnore
	private Integer minAppVersion;
	@JsonIgnore
	private Integer maxAppVersion;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCtaActionUrl() {
		return ctaActionUrl;
	}
	public void setCtaActionUrl(String ctaActionUrl) {
		this.ctaActionUrl = ctaActionUrl;
	}
	public Integer getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public Integer getMinAppVersion() {
		return minAppVersion;
	}
	public void setMinAppVersion(Integer minAppVersion) {
		this.minAppVersion = minAppVersion;
	}
	public Integer getMaxAppVersion() {
		return maxAppVersion;
	}
	public void setMaxAppVersion(Integer maxAppVersion) {
		this.maxAppVersion = maxAppVersion;
	}
	@Override
	public String toString() {
		return "MerchandisingBanner [imageUrl=" + imageUrl + ", backgroundColor=" + backgroundColor + ", ctaActionUrl="
				+ ctaActionUrl + ", sequenceNo=" + sequenceNo + ", minAppVersion=" + minAppVersion + ", maxAppVersion="
				+ maxAppVersion + "]";
	}
	public String getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
}
