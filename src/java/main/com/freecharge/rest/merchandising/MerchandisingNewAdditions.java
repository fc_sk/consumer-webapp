package com.freecharge.rest.merchandising;

public class MerchandisingNewAdditions extends MerchandisingData{
	private String billerId;

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	@Override
	public String toString() {
		return "MerchandisingNewAdditions [billerId=" + billerId + ", toString()=" + super.toString() + "]";
	}
}
