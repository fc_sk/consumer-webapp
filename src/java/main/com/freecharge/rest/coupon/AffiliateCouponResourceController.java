package com.freecharge.rest.coupon;

import com.freecharge.affiliate.order.AffiliateOrder;
import com.freecharge.api.coupon.common.CouponConstants;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.coupon.service.web.model.CouponData;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.service.AffiliateCouponService;
import com.freecharge.common.comm.fulfillment.AffiliateCouponFulfilmentService;
import com.freecharge.common.comm.fulfillment.FulfilmentFailedException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.coupon.model.AffiliateCouponDataLite;
import com.freecharge.rest.model.AffiliateCouponsRequest;
import com.freecharge.rest.model.LoadCouponsRequest;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * This class provides rest service end point for coupon dispatch in ATM
 * transactions. For affiliates controller.
 * 
 * @author shirish
 * 
 */
@Controller
@RequestMapping("/*")
public class AffiliateCouponResourceController extends AbstractRestController{
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	private AffiliateCouponService atmCouponService;
	
	@Autowired
	private AffiliateCouponFulfilmentService atmFulfilmentService;
	
	
	@Autowired
    private MetricsClient metricsClient;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@NoLogin
    @RequestMapping(value = "/coupons/affiliate/free", method = RequestMethod.GET) 
    public @ResponseBody Map<String, Object> getFreeCoupons(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	long startTime = System.currentTimeMillis(); 
    	MobileUtil.setIPAddressInSession(httpRequest);
    	String startStr = httpRequest.getParameter("start");
    	String countStr = httpRequest.getParameter("count");
     	String categoryIdsStr = httpRequest.getParameter("cid");
    	String minValueStr = httpRequest.getParameter("minValue");
    	String maxValueStr = httpRequest.getParameter("maxValue");
/*    	String minPriceStr = httpRequest.getParameter("minPrice");
    	String maxPriceStr = httpRequest.getParameter("maxPrice");*/
    	String affiliateId = httpRequest.getParameter("affiliateId");
    	String accessToken = httpRequest.getParameter("accessToken");
    	CouponControllerHelper.validateAffiliateId(affiliateId);
    	CouponControllerHelper.validateAffiliateAccessToken(accessToken);
     	CouponControllerHelper.validateStart(startStr);
     	CouponControllerHelper.validateCount(countStr);
     	CouponControllerHelper.validateCategoryIds(categoryIdsStr);
     	CouponControllerHelper.validateMinValue(minValueStr);
    	CouponControllerHelper.validateMaxValue(maxValueStr);
    	CouponData couponData = couponServiceProxy.getFilteredCoupons(startStr, countStr, categoryIdsStr, minValueStr, maxValueStr, "0", "0", 
    			FCConstants.CHANNEL_ID_SERVICE, null, true, false, null,1, Integer.MIN_VALUE	);
    	List<Map<String,String>> freeCouponsData = new ArrayList<>();
    	Map<String, Object> data = new HashMap<>();
    	if (couponData == null){
    		logger.error("Got empty coupon data from coupon service.");
    		return data;
    	}
    	List<Coupon> coupons = couponData.getCoupons();
/*    	CouponFraudCheckRequest requestData = new CouponFraudCheckRequest();
     	CouponData filtereCouponData = couponService.filterOutCoupons(couponData, startStr, countStr, categoryIdsStr, minValueStr, maxValueStr, minPriceStr, maxPriceStr, FCConstants.CHANNEL_ID_SERVICE, null);
     	coupons = filtereCouponData.getCoupons();*/
     	
     	
     	for (Coupon coupon : coupons){
     		if (coupon.getPrice() == 0){
     			Map<String, String> couponDataMap = new LinkedHashMap<>();
     			couponDataMap.put("couponId", String.valueOf(coupon.getCouponId()));
     			couponDataMap.put("couponName", coupon.getCouponName());
     			couponDataMap.put("couponImage200x200", coupon.getImage_200_200());
     			couponDataMap.put("couponImage400x200", coupon.getImage_400_200());
     			couponDataMap.put("couponImage600x200", coupon.getImage_600_400());
     			couponDataMap.put("couponValue", String.valueOf(coupon.getCouponValue()));
     			couponDataMap.put("couponMerchantName", coupon.getMerchantTitle());
     			couponDataMap.put("tnc", coupon.getTermsConditions());
     			couponDataMap.put("validityDate", coupon.getValidityDate());
     			couponDataMap.put("cities", coupon.getCities());
     			couponDataMap.put("minValidAmount", String.valueOf(coupon.getMinRedemptionAmount()));
     			couponDataMap.put("shortDescription", coupon.getCouponShortDesc());
     			freeCouponsData.add(couponDataMap);
     		}
     	}
  		long endTime = System.currentTimeMillis(); 
		long duration = endTime - startTime;
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("AffiliateCouponResourceController", "getFreeCoupons", duration);
		}
		data.put("coupons", freeCouponsData);
		data.put("categories", couponData.getCategories());
    	return data;
    }

	private String getCouponImageUrlForAffiliate(String affiliateId, Coupon coupon) {
		return coupon.getCouponImagePath();
	}
	
	@Csrf(exclude=true)
	@NoLogin
	@RequestMapping(value = "/TNC", method = RequestMethod.GET)
	public @ResponseBody String getCouponHistoryTnCCaps(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		return handleAtmRequest(httpRequest);
	}
	
	@Csrf(exclude=true)
	@NoLogin
	@RequestMapping(value = "/tnc", method = RequestMethod.GET)
	public @ResponseBody String getCouponHistoryTnC(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		return handleAtmRequest(httpRequest);
	}

	private String handleAtmRequest(HttpServletRequest httpRequest)
			throws InvalidParameterException {
		String id = httpRequest.getParameter("id");
		if (id == null){
			id = httpRequest.getParameter("ID");
		}
		
		if (id == null || !FCUtil.isInteger(id)){
			throw new InvalidParameterException("id", id, "id value should be an integer.", ValidationErrorCode.INVALID_COUPON_IDENTIFIER);
		}
		CouponHistory couponHistory = couponServiceProxy.getCouponTncByHistoryId(Integer.parseInt(id));
		String tnc = "";
		if (couponHistory != null){
			tnc = couponHistory.getTermsAndConditions();
		}
		return tnc;
	}
	
	@Csrf(exclude=true)
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/coupons/affiliate/block", method = RequestMethod.POST)
	public @ResponseBody List<Map<String, String>> blockCouponForAffiliate(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
//		if (fcProperties.isProdMode()){
//			return null;
//		}
		String affiliateOrderId = httpRequest.getParameter("orderId");
		String couponIds = httpRequest.getParameter("couponIds");
		String affiliateIdStr = httpRequest.getParameter("affiliateId");
		String accessToken = httpRequest.getParameter("accessToken");
		CouponControllerHelper.validateOrderId(affiliateOrderId);
		CouponControllerHelper.validateCouponIds(couponIds);
		CouponControllerHelper.validateAffiliateId(affiliateIdStr);
		CouponControllerHelper.validateAffiliateAccessToken(accessToken);
		Integer affiliateId = Integer.parseInt(affiliateIdStr);
		Map<String, String> affiliateRequestData = new HashMap<String, String>();
		affiliateRequestData.put(CouponConstants.KEY_COUPON_IDS, couponIds);
		List<Map<String, String>> listOfCouponData = new ArrayList<>();
		
		long startTime = System.currentTimeMillis();
		AffiliateCouponsRequest request = new AffiliateCouponsRequest();
		//TODO: Shirish move the max count of coupon per transaction to DB.
		CouponControllerHelper.validateAffiliateCouponIds(couponIds, affiliateId);
		
		request.setCouponAffiliateId(affiliateId);
		request.setAffiliateOrderId(affiliateOrderId);
		request.setAffiliateRequestData(affiliateRequestData);
		String orderId = null;
		try {
			AffiliateOrder order = atmCouponService.saveAffiliateCouponsOrder(request);
			if (order != null){
				orderId = order.getOrderId();
				logger.debug("OrderId: " + orderId);
			}
			List<BlockedCoupon> blockedCoupons = atmFulfilmentService.doCouponsFulfilment(order);
			if (blockedCoupons == null){
				logger.error("Could not send coupon text for orderId: " + orderId + " as blockedCoupons is null.");
				return null;
			}
			for (BlockedCoupon blockedCoupon : blockedCoupons){
				if (blockedCoupon == null){
					logger.error("Got blockedCoupon as null for orderId: " + order.getOrderId());
				} else{
					Integer couponStoreId = blockedCoupon.getCouponStoreId();
					Coupon coupon = couponServiceProxy.getCouponWithTnc(couponStoreId, FCConstants.CHANNEL_ID_SERVICE);
					if(coupon!=null){
						Map<String, String> couponData = new LinkedHashMap<>();
						String commaSeparateCouponCodes = getCommaSeparatedCouponCodes(blockedCoupon);
						couponData.put("couponCode", commaSeparateCouponCodes);
						couponData.put("couponId", String.valueOf(coupon.getCouponId()	));
						couponData.put("couponName", coupon.getCouponName());
						couponData.put("couponMerchantName", coupon.getMerchantTitle());
						couponData.put("couponShortDesc", coupon.getCouponShortDesc());
						couponData.put("tnc", coupon.getTermsConditions());
						couponData.put("validityDate", coupon.getValidityDate());
						couponData.put("freeChargeOrderId", blockedCoupon.getOrderId());
						couponData.put("isPrintRequired", String.valueOf(coupon.isPrintable()));
						listOfCouponData.add(couponData);
					}
				}
				
			}
			
		} catch (FulfilmentFailedException e) {
			logger.error("Could not send coupon text for orderId: " + orderId + " as AffiliateFulfilment failed.", e);
		} catch (Exception e) {
			logger.error("Could not send coupon for orderId: " + orderId + " as AffiliateFulfilment failed.", e);
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		logger.debug("Took milliseconds: " + duration);
		metricsClient.recordLatency("AffiliateCouponResourceController", "blockCouponForAffiliate", duration);
		return listOfCouponData;
	}
	
	private String getCommaSeparatedCouponCodes(BlockedCoupon blockedCoupon) {
		if (blockedCoupon == null || blockedCoupon.getBlockedCodes() == null){
			return "";
		}
		List<BlockedCouponCode> codes = blockedCoupon.getBlockedCodes();
		StringBuilder codesStringBuilder = new StringBuilder();
		boolean isFirst = true;
		for (BlockedCouponCode code : codes){
			if (!isFirst){
				codesStringBuilder.append(",");
			}
			codesStringBuilder.append(code.getCode());
		}
		return codesStringBuilder.toString();
	}

	
	@Csrf(exclude=true)
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/prizm/coupon", method = RequestMethod.POST)
	public @ResponseBody AffiliateCouponDataLite blockCouponForATM(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		String prnNo = httpRequest.getParameter("prnNo");
		String merchantId = httpRequest.getParameter("merchantId");
		String atmId = httpRequest.getParameter("atmId");
		validateRequest(prnNo, merchantId, atmId);
		AffiliateCouponDataLite atmCouponData = null;
		
		long startTime = System.currentTimeMillis();
		Map<String, String> affiliateRequestData = new HashMap<String, String>();
		affiliateRequestData.put(FCConstants.KEY_COUPON_MERCHANT_ID, merchantId);
		affiliateRequestData.put(FCConstants.ATM_ID, atmId);
		
		
		AffiliateCouponsRequest request = new AffiliateCouponsRequest();
		request.setAffiliateOrderId(prnNo);
		request.setCouponAffiliateId(FCConstants.AFFILIATE_ID_ATM);
		request.setAffiliateRequestData(affiliateRequestData);
		
		AffiliateOrder order = atmCouponService.saveAffiliateCouponsOrder(request);
		try {
			atmCouponData = atmFulfilmentService.doMerchantCouponFulfilment(order);
		} catch (FulfilmentFailedException e) {
			logger.error("Could not send coupon text for orderId: " + order.getOrderId() + " as AffiliateFulfilment failed.", e);
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		logger.debug("Took milliseconds: " + duration);
		metricsClient.recordLatency("AffiliateCouponResourceController", "blockCouponForATM", duration);
		return atmCouponData;
	}
	
	@Csrf(exclude=true)
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/prizm/dummy/atm/coupon", method = RequestMethod.POST)
	public @ResponseBody AffiliateCouponDataLite blockCouponForATMDummy(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		String prnNo = httpRequest.getParameter("prnNo");
		String merchantId = httpRequest.getParameter("merchantId");
		String atmId = httpRequest.getParameter("atmId");
		validateRequest(prnNo, merchantId, atmId);
		AffiliateCouponDataLite atmCouponData = null;
		
		long startTime = System.currentTimeMillis();
		Map<String, String> affiliateRequestData = new HashMap<String, String>();
		affiliateRequestData.put(FCConstants.KEY_COUPON_MERCHANT_ID, merchantId);
		affiliateRequestData.put(FCConstants.ATM_ID, atmId);
		
		
		AffiliateCouponsRequest request = new AffiliateCouponsRequest();
		request.setAffiliateOrderId(prnNo);
		request.setCouponAffiliateId(FCConstants.AFFILIATE_ID_ATM);
		request.setAffiliateRequestData(affiliateRequestData);
		
		AffiliateOrder order = atmCouponService.saveAffiliateCouponsOrder(request);
		try {
			atmCouponData = atmFulfilmentService.doMerchantCouponFulfilment(order);
		} catch (FulfilmentFailedException e) {
			logger.error("Could not send coupon text for orderId: " + order.getOrderId() + " as AffiliateFulfilment failed.", e);
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		logger.debug("Took milliseconds: " + duration);
		metricsClient.recordLatency("AffiliateCouponResourceController", "blockCouponForATMDummy", duration);
		return atmCouponData;
	}


	private void validateRequest(String prnNo, String merchantId, String atmId) throws InvalidParameterException {
		CouponControllerHelper.validateAtmId(atmId);
		CouponControllerHelper.validateMerchantId(merchantId);
		CouponControllerHelper.validatePrnNo(prnNo);
	}
}
