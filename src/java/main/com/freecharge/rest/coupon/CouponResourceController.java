package com.freecharge.rest.coupon;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.common.entity.CouponStoreLocation;
import com.freecharge.api.coupon.service.model.CouponOrderStatus;
import com.freecharge.api.coupon.service.model.Request.CouponFraudCheckRequest;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponResponse;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.coupon.service.web.model.CouponCategory;
import com.freecharge.api.coupon.service.web.model.CouponCity;
import com.freecharge.api.coupon.service.web.model.CouponData;
import com.freecharge.api.coupon.service.web.model.CouponTnC;
import com.freecharge.api.coupon.service.web.model.CouponTncData;
import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.api.coupon.service.web.model.CouponsDataVO;
import com.freecharge.api.coupon.service.web.model.PageCouponCategory;
import com.freecharge.api.coupon.service.web.model.SingleCouponTnC;
import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.dao.jdbc.LocationReadDao;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.StateMaster;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.domain.entity.jdbc.ZoneMaster;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.TxnCrossSellService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.UserIdHelper;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.fulfillment.AffiliateCouponFulfilmentService;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.coupon.service.CouponDummyDataManager;
import com.freecharge.coupon.service.CouponExperimentService;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.location.AppCityService;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.exception.PersonalDetailException;
import com.freecharge.mobile.service.helper.SessionHelper;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.RestValidationHelper;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;
import com.freecharge.rest.coupon.model.MarkAsUsedpojo;
import com.freecharge.rest.model.BlockCouponForScaleTest;
import com.freecharge.rest.model.LoadCouponsRequest;
import com.freecharge.rest.model.SaveCouponsRequest;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.CookieUtil;
import com.freecharge.util.TrackerEvent;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

/**
 * Handles coupon related calls. 
 * The rest call controller for all coupons related calls.. Please consult coupons team before changing.
 * 
 * Entry point to coupons API.
 * @author shirish
 *
 */
@Controller
@RequestMapping("/*")
public class CouponResourceController extends AbstractRestController{
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AffiliateCouponFulfilmentService atmFulfilmentService;
    
    @Autowired
    private VoucherService voucherService;
    
    @Autowired
    private LocationReadDao locationReadDao;
    
   
    @Autowired
    private TxnCrossSellService txnCrossSellService;

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    private MobileCheckoutService mCheckoutService;

    static final String FOOD_AND_BEVERGES = "Food-n-Beverages";

    @Autowired
    private Tracker tracker;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private AppConfigService appConfigService;
    
	@Autowired
    private MetricsClient metricsClient;
	
	@Autowired
    private UserIdHelper userIdHelper;
	

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	/*@Autowired
	private ICouponService couponService;*/
	
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
    @Autowired
    private AppCityService appCityService;

    @Autowired
    private CouponExperimentService couponExperimentService;

    @Autowired
	private CartService cartService;
	
    private SessionHelper sessionHelper = new SessionHelper();
    
    @Autowired 
	private RestValidationHelper validationHelper;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
	private CouponService couponService;
    
    @RequestMapping(value = "/rest/coupon/h/order/status/{orderId}", method = RequestMethod.GET)
    public @ResponseBody
    CouponOrderStatus getHCouponOrderStatus(@PathVariable String orderId,  
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	logger.info("Getting order status for order Id: " + orderId);
    	MobileUtil.setIPAddressInSession(httpRequest);
    	validationHelper.validateOrderIdBelongsToUser(orderId);
    	return couponServiceProxy.getOrderStatus(orderId);
    }
    
    @NoLogin
    @RequestMapping(value = "/rest/coupon/all", method = RequestMethod.GET)
    public @ResponseBody
    CouponsDataVO getAllCoupons(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyCouponDataVO();
    	}
    	MobileUtil.setIPAddressInSession(httpRequest);
    	int appVersion = getAppVersion(httpRequest);
        return getAllCouponsWithOrdering(request,appVersion);
    }

	private int getAppVersion(HttpServletRequest httpRequest) {
		String appVersionStr = httpRequest.getParameter("fcversion");
    	int appVersion =1;
    	if(appVersionStr !=null && FCUtil.isInteger(appVersionStr)) {
    		appVersion= Integer.parseInt(appVersionStr);
    	}
		return appVersion;
	}
	
	private int setDefaultVersion(HttpServletRequest httpRequest) {
		String appVersionStr = httpRequest.getParameter("fcversion");
		int appVersion =1000;
		if(appVersionStr !=null && FCUtil.isInteger(appVersionStr)) {
    		appVersion= Integer.parseInt(appVersionStr);
    	}
		return appVersion;
	}
	

	private CouponsDataVO getAllCouponsWithOrdering(LoadCouponsRequest request,int appVersion) {
        Map<String, List<CouponVO>> couponsCategoryMap = voucherService.getAllActiveCouponsWithOrdering(FCConstants.CHANNEL_ID_ANDROID_APP, appVersion);
        List<VoucherCat> categories = voucherService.getAllActiveVoucherCat();
        Collections.sort(categories, new VoucherCat.CategoryDetailComparator());
        // Putting FOOD_AND_BEVERGES category on the top of Coupon List because of more opting rate then other category
        categories = setCouponCategoryOnFirst(categories,FOOD_AND_BEVERGES);
        removeEmptyCategories(couponsCategoryMap, categories);
        CouponsDataVO coupons = new CouponsDataVO();
        List<com.freecharge.api.coupon.service.web.model.VoucherCat> webcats = MapperUtil.map(categories, com.freecharge.api.coupon.service.web.model.VoucherCat.class);
        coupons.setCategories(webcats);
        coupons.setCategoryMap(couponsCategoryMap);

        return coupons;
	}

	@Csrf(exclude = true)
    @RequestMapping(value = "/rest/coupon/c/hcoupon/cart/save", method = RequestMethod.POST)
    public @ResponseBody
    HashMap<String, String> saveHCoupon(SaveCouponsRequest saveCouponsRequest, Model model, HttpServletRequest request) throws InvalidPathValueException,InvalidParameterException {
		logger.info("Entering in saveHCoupon, SaveCouponsRequest is  " + saveCouponsRequest );
		String userEmailId = FCSessionUtil.getLoggedInEmailId();
    	if (userEmailId == null || userEmailId.isEmpty()){
    		throw new InvalidParameterException("EmailId",userEmailId,"User emailId not present",ValidationErrorCode.INVALID_EMAIL);
    	}
		
    	Integer userId = userIdHelper.getUserIdFromEmail(userEmailId);
		String lookUpId = null;
		lookUpId = saveCouponsRequest.getLookupID();
		if(lookUpId!=null && !(isLookupIdValidForUser(lookUpId,userId))) {
			lookUpId =null;
		}
    	String lookupId = cartService.createCartForHCoupons(saveCouponsRequest.getItemIdArray(), userId, lookUpId);
    	sessionHelper.saveLookupIdInSession(lookupId);
    	HashMap<String, String> lookupIdMap = new HashMap();
    	lookupIdMap.put("lookupId", lookupId);
        return lookupIdMap;
	}

	private boolean isLookupIdValidForUser(String lookUpId,Integer userId) {
		CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookUpId);
		Integer userIdFromLookupId=cartBusinessDo.getUserId();
		if(!(userIdFromLookupId.equals(userId))){
			return false;
		}
		return true;
	}
    @Csrf(exclude = true)
    @RequestMapping(value = "/rest/coupon/optin/save", method = RequestMethod.POST)
    public @ResponseBody
    HashMap<String, String> saveCoupon(SaveCouponsRequest saveCouponsRequest, Model model, HttpServletRequest request) throws InvalidPathValueException {
    	logger.info("Entering in saveCoupon, SaveCouponsRequest is  " + saveCouponsRequest );
     	if(saveCouponsRequest == null || StringUtils.isBlank(saveCouponsRequest.getItemIdArray()) 
     			|| StringUtils.isBlank(saveCouponsRequest.getType()) || StringUtils.isBlank(saveCouponsRequest.getLookupID()) )
    	{
    		Cookie clientTrackingCookie = CookieUtil.getCookie(request, fcProperties.getClientTrackingCookieName());
        	Cookie cookie = CookieUtil.getCookie(request, fcProperties.getVisitTrackingCookieName());
        	if(clientTrackingCookie != null){
            	logger.info("cookies in saveCoupon. clientTrackingCookie : " + clientTrackingCookie.getValue() );
        	}
        	if(cookie != null){
            	logger.info("cookies in saveCoupon. VisitTrackingCookie : "  + cookie.getValue());
        	}
    	}
     	TxnCrossSellBusinessDo bdo = MapperUtil.map(saveCouponsRequest, TxnCrossSellBusinessDo.class);
     	try {
     		
     		TxnHomePageBusinessDO txnHomePageBusinessDO = txnHomePageService.getTransactionDetails(saveCouponsRequest
     				.getLookupID());
     		bdo.setHomePageId(txnHomePageBusinessDO.getTxnHomePageId());
     		bdo.setServiceNo(txnHomePageBusinessDO.getServiceNumber());
     		bdo.setProType(txnHomePageBusinessDO.getProductType());
     		bdo.setOperatorName(txnHomePageBusinessDO.getOperatorName());
     		txnCrossSellService.saveAllTxnCrossSellService(bdo);

     		mCheckoutService.initiateCheckout(bdo.getLookupID(), saveCouponsRequest.getItemIdArray());
     	} catch (PersonalDetailException e) {
     		logger.error("Got PersonalDetailException", e);
     	} catch (CartExpiredException e) {
     		logger.error("Got CartExpiredException", e);
     	} catch (ServiceUnavailableException e) {
     		logger.error("Exception caught while initiateCheckout",e);
     	}

        HashMap<String, String> lookupId = new HashMap();
        lookupId.put("lookupId", bdo.getLookupID());
        return lookupId;
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/whatismyip", method = RequestMethod.GET) 
    public @ResponseBody String getMyIP(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) 
    		throws InvalidPathValueException {
    	return MobileUtil.getRealClientIpAddress(httpRequest);
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/whatismycity", method = RequestMethod.GET) 
    public @ResponseBody String getMyIPBasedCity(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) 
    		throws InvalidPathValueException {
    	return getCityName(httpRequest);
    }

    @NoLogin
    @RequestMapping(value = "/rest/coupon/lite", method = RequestMethod.GET) 
    public @ResponseBody CouponData getCategoryCouponsLite(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyCouponData();
    	}
    	long startTime = System.currentTimeMillis(); 
    	MobileUtil.setIPAddressInSession(httpRequest);
    	Integer channelId = request.getFcChannel();
    	int appVersion=getAppVersion(httpRequest);
    	CouponFraudCheckRequest couponFraudCheckRequest =getCouponFraudCheckRequest();
    	CouponData couponData = couponServiceProxy.getCouponDataLite(couponFraudCheckRequest,channelId,appVersion);
    	long endTime = System.currentTimeMillis(); 
		long duration = endTime - startTime;
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("CouponService", "getCategoryCouponsLite", duration);
		}
    	return couponData;
    }
    
    @NoLogin
    @RequestMapping(value = "/rest/coupon/paid/carousel", method = RequestMethod.GET) 
    public @ResponseBody String getPaidCouponCarousel(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response) throws InvalidPathValueException {
    	JSONObject carousel = appConfigService.getCouponCarousel();
    	return ( carousel == null ? "[]" : carousel.get("carousel").toString());
    }
    
    @Csrf(exclude = true)
    @NoSessionWrite
    @NoLogin
    @RequestMapping(value = "/protected/rest/coupon/blockcoupon", method = RequestMethod.POST)
    public @ResponseBody BlockedCoupon blockCouponForScaleTest(@RequestBody BlockCouponForScaleTest request,HttpServletRequest httpRequest, 
    		HttpServletResponse response, ModelMap model){
		BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
		blockCouponRequest.setCouponStoreId(request.getCouponStoreId());
		blockCouponRequest.setEmail(request.getEmail());
		blockCouponRequest.setOrderId(request.getOrderId());
		blockCouponRequest.setQuantity(request.getQuantity());
		blockCouponRequest.setCouponOptinType(request.getCouponOptinType());
		BlockedCoupon blockedCoupon = new BlockedCoupon();
		Integer userId = (int) userServiceProxy.getUserIdByEmailId(request.getEmail());
    	try {
    		BlockedCouponResponse blockResponse  = couponServiceProxy.blockCouponForOrder(blockCouponRequest,userId);
    		if (blockResponse == null || blockResponse.getBlockedCoupon(request.getCouponStoreId()) == null || 
    				blockResponse.getBlockedCoupon(request.getCouponStoreId()).getBlockedCodes().size() < request.getQuantity()){
    			logger.error("Error blocking codes for orderId: " + request.getOrderId() +  " couponStoreId: " + request.getCouponStoreId());
    		} else {
    			blockedCoupon = blockResponse.getBlockedCoupon(request.getCouponStoreId());
    		}
		} catch (Exception e) {
			logger.error("Error blockCouponForScaleTest: ", e);
		}
    	return blockedCoupon;
    }
    
    @NoSessionWrite
    @NoLogin
    @RequestMapping(value = "/protected/rest/coupon/recostore", method = RequestMethod.GET) 
    public @ResponseBody Map<String, String> getRecoStore(HttpServletRequest httpRequest, 
    		HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	String userEmail = httpRequest.getParameter("email");
    	Integer userId = userIdHelper.getUserIdFromEmail(userEmail);
    	if (userId == null){
    		throw new InvalidParameterException("email", userEmail, "User not found", ValidationErrorCode.INVALID_USER_IDENTIFIER);
    	}
    	return couponServiceProxy.getRecoStoreData(userId, FCConstants.CHANNEL_ID_WEB);
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/protected/rest/coupon/setdummy", method = RequestMethod.GET)
    public @ResponseBody String setDummy(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) {
    	CouponDummyDataManager.setDummyCouponMode(true);
    	return "Set Dummy Done";
    }
    
    @NoLogin
    @RequestMapping(value = "/protected/rest/coupon/unsetdummy", method = RequestMethod.GET)
    public @ResponseBody String unSetDummy(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) {
    	CouponDummyDataManager.setDummyCouponMode(false);
    	return "Set Dummy UnDone";
    }
    
    private static final List<Coupon> EMPTY_COUPONS = new ArrayList<Coupon>();
    @NoLogin
    @RequestMapping(value = "/rest/coupon/page", method = RequestMethod.GET) 
    public @ResponseBody List<Coupon> getCategoryCouponsLitePaginated(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		String startStr = httpRequest.getParameter("start");
    		int start = 1;
    		if (FCUtil.isInteger(startStr)){
    			start = Integer.parseInt(startStr);
    			if (start > 1){
    				return EMPTY_COUPONS;
    			}
    		}
    		return CouponDummyDataManager.getDummyCoupons();
    	}
    	long startTime = System.currentTimeMillis(); 
    	boolean wasReco = false;
    	MobileUtil.setIPAddressInSession(httpRequest);
    	String startStr = httpRequest.getParameter("start");
    	String countStr = httpRequest.getParameter("count");
     	String categoryIdsStr = httpRequest.getParameter("cid");
     	String isPromotedStr = httpRequest.getParameter("isPromoted");
    	String minValueStr = httpRequest.getParameter("minValue");
    	String maxValueStr = httpRequest.getParameter("maxValue");
    	String minPriceStr = httpRequest.getParameter("minPrice");
    	String maxPriceStr = httpRequest.getParameter("maxPrice");
    	Integer channelId = request.getFcChannel();
    	String imeiNo = httpRequest.getParameter("imei");
    	FreechargeSession session=FCSessionUtil.getCurrentSession();
    	if(imeiNo!=null && session!=null) {
    		session.setIMEINumber(imeiNo);
    	}
    	checkInputFields(startStr, countStr, categoryIdsStr, isPromotedStr, 
    			minValueStr, maxValueStr, minPriceStr, maxPriceStr);
    	boolean isPromoted = false;
    	if (isPromotedStr != null){
    		if (Integer.parseInt(isPromotedStr) == 1){
    			isPromoted = true;
    		}
    	}
    	int appVersion = getAppVersion(httpRequest);
        String city = cityFilterExperiment(request, httpRequest);

    	List<Coupon> coupons = new ArrayList<>();
    	try{
    		boolean includeTnC = false;
    		CouponData couponData = couponServiceProxy.getFilteredCouponsForDisplay(startStr, countStr, categoryIdsStr,
                    minValueStr, maxValueStr, minPriceStr, maxPriceStr, request.getFcChannel(),
                    getCouponFraudCheckRequest(), includeTnC, isPromoted, city, appVersion, Integer.MIN_VALUE);
    		if (couponData == null){
    			return coupons;
    		}
    		coupons = couponData.getCoupons();
        		
    	} catch(Exception e){
    		logger.error("Error fetching filteredCoupons.", e);
    	}
    	
    	if (channelId == FCConstants.CHANNEL_ID_WEB && FCSessionUtil.isUserLoggedIn()){
    		tracker.trackEvent(TrackerEvent.COUPON_DESKTOP_USER_IS_LOGGED_IN.createTrackingEvent());
    	}
  		long endTime = System.currentTimeMillis(); 
		long duration = endTime - startTime;
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated", duration);
			if (wasReco){
				metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated_RECO", duration);
			}
		}
    	return coupons;
    }
    
    @NoLogin
    @RequestMapping(value = "/rest/hcoupon/page", method = RequestMethod.GET) 
    public @ResponseBody Map<String,Object> getCategoryCouponsLitePaginatedMap(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyHCouponMap();
    	}
    	long startTime = System.currentTimeMillis(); 
    	boolean wasReco = false;
    	MobileUtil.setIPAddressInSession(httpRequest);
    	String startStr = httpRequest.getParameter("start");
    	String countStr = httpRequest.getParameter("count");
     	String categoryIdsStr = httpRequest.getParameter("cid");
     	String isPromotedStr = httpRequest.getParameter("isPromoted");
    	String minValueStr = httpRequest.getParameter("minValue");
    	String maxValueStr = httpRequest.getParameter("maxValue");
    	String minPriceStr = httpRequest.getParameter("minPrice");
    	String maxPriceStr = httpRequest.getParameter("maxPrice");
    	String withCategoryStr = httpRequest.getParameter("withCat");
    	String isInTxnStr = httpRequest.getParameter("isInTxn");
    	String latStr = httpRequest.getParameter("lat");
    	String longStr = httpRequest.getParameter("long");
    	Integer channelId = request.getFcChannel();
    	String imeiNo = httpRequest.getParameter("imei");
    	FreechargeSession session=FCSessionUtil.getCurrentSession();
    	if(imeiNo!=null && session!=null) {
    		session.setIMEINumber(imeiNo);
    	}
    	checkInputFields(startStr, countStr, categoryIdsStr, isPromotedStr, 
    			minValueStr, maxValueStr, minPriceStr, maxPriceStr);
    	checkLatLongFields(latStr,longStr);
    	boolean isPromoted = false;
    	if (isPromotedStr != null){
    		if (Integer.parseInt(isPromotedStr) == 1){
    			isPromoted = true;
    		}
    	}
    	boolean withCategory = false;
    	if (withCategoryStr !=null && FCUtil.isBoolean(withCategoryStr)) {
    		withCategory = Boolean.parseBoolean(withCategoryStr);
    	}
    	boolean isInTxn = false;
    	if (isInTxnStr !=null && FCUtil.isBoolean(isInTxnStr)) {
    		isInTxn = Boolean.parseBoolean(isInTxnStr);
    	}
    	
    	int appVersion = getAppVersion(httpRequest);
        String city = cityFilterExperiment(request, httpRequest);

        CouponData couponData = null;
    	Map<String,Object> couponCategoryMap = new HashMap<String, Object>();
    	populateIsHCouponVisibleFlag(withCategory, couponData,
				couponCategoryMap);
    	try{
    		boolean includeTnC = false;
    		if(latStr == null || longStr == null) {
    			return couponCategoryMap;
    		}
    		
    		
    		// Handle first category coupon counts if withCategory is true
    		if (withCategory  && categoryIdsStr == null){
    			// Get coupon data without pagination to return correct category coupons list,
    			couponData = couponServiceProxy.getFilteredHCouponsForDisplay(null, null, null,
                        minValueStr, maxValueStr, minPriceStr, maxPriceStr, request.getFcChannel(),
                        getCouponFraudCheckRequest(), includeTnC, isPromoted, city, appVersion, latStr, longStr, isInTxn,
						Integer.MIN_VALUE);
    			if (couponData==null || couponData.getCategories() == null || couponData.getCategories().isEmpty()){
    				return couponCategoryMap;
    			} else{
    				couponCategoryMap.put("categories", couponData.getCategories());
    			}
    			
    			
    			// Since with category is true. Filter with pagination for first category and return results.
    			CouponCategory firstCategory = couponData.getCategories().get(0);
    			if (firstCategory != null && firstCategory.getCouponIds() != null
    					&& !firstCategory.getCouponIds().isEmpty()){
    				categoryIdsStr = String.valueOf(firstCategory.getCategoryId());
					couponData = couponServiceProxy.getFilteredHCouponsForDisplay(startStr, countStr, categoryIdsStr,
		                    minValueStr, maxValueStr, minPriceStr, maxPriceStr, request.getFcChannel(),
		                    getCouponFraudCheckRequest(), includeTnC, isPromoted, city, appVersion, latStr, longStr,
							isInTxn, Integer.MIN_VALUE);
    			}
    			
    		} else{
    			// If with category is false
    			couponData = couponServiceProxy.getFilteredHCouponsForDisplay(startStr, countStr, categoryIdsStr,
                        minValueStr, maxValueStr, minPriceStr, maxPriceStr, request.getFcChannel(),
                        getCouponFraudCheckRequest(), includeTnC, isPromoted, city, appVersion, latStr, longStr, isInTxn,
						Integer.MIN_VALUE);
        		if (couponData == null){
        			return couponCategoryMap;
        		}
    		}
    		couponCategoryMap.put("coupons", couponData.getCoupons());
    	} catch(Exception e){
    		logger.error("Error fetching filteredCoupons.", e);
    	}
    	
    	if (channelId == FCConstants.CHANNEL_ID_WEB && FCSessionUtil.isUserLoggedIn()){
    		tracker.trackEvent(TrackerEvent.COUPON_DESKTOP_USER_IS_LOGGED_IN.createTrackingEvent());
    	}
  		long endTime = System.currentTimeMillis(); 
		long duration = endTime - startTime;
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated", duration);
			if (wasReco){
				metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated_RECO", duration);
			}
		}
		
		populateIsHCouponVisibleFlag(withCategory, couponData,
				couponCategoryMap);
		
    	return couponCategoryMap;
    }

    /*New API for getting combined filtered and hcoupons*/
    @RequestMapping(value = "/rest/filtered/coupons", method = RequestMethod.GET)
    public @ResponseBody Map<String,Object> getFilteredCouponsAndHCoupons(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	Map<String,Object> result = getFilteredCouponsAndHCouponsseService(httpRequest, false);
    	logErrorResults(result, httpRequest);
    	return result;
    }

	private void logErrorResults(Map<String, Object> couponCategoryMap, HttpServletRequest httpRequest) {
		try {
			String orderId = null;
			if (httpRequest != null) {
				orderId = httpRequest.getParameter("orderId");
			}
			
			boolean isCouponsEmpty = false;
			if (couponCategoryMap != null && !(couponCategoryMap.isEmpty())) {
				List<Coupon> coupons = null;
				if (couponCategoryMap.containsKey("coupons")) {
					coupons = (List<Coupon>) couponCategoryMap.get("coupons");
				}
				if (coupons == null || coupons.size() < 1) {
					isCouponsEmpty = true;
				}
			} else {
				isCouponsEmpty = true;
			}

			if (isCouponsEmpty) {
				logger.error("final couponCategoryMap does not contain coupons list for orderId = " + orderId);
				metricsClient.recordEvent("CouponServiceCall", "fetchCoupons", "finalFetchCouponsEmpty");
			} else {
				logger.info("result couponCategoryMap has coupons " + orderId);
				metricsClient.recordEvent("CouponServiceCall", "fetchCoupons", "finalFetchHasCoupons");
			}
		} catch (Exception ex) {
			logger.error("Exception while logging coupon fetch results in signalfx, please ignore", ex);
		}
	}

	/*New API for getting combined filtered and hcoupons*/
	@RequestMapping(value = "/rest/v2/filtered/coupons/", method = RequestMethod.GET)
	public @ResponseBody Map<String,Object> getFilteredCouponsAndHCouponsWithHighValueCoupons(
			HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		Map<String,Object> result = getFilteredCouponsAndHCouponsseService(httpRequest, true);
		logErrorResults(result, httpRequest);
		return result;
	}

	private Map<String, Object> getFilteredCouponsAndHCouponsseService(HttpServletRequest httpRequest,
																	   boolean showHighValueCoupons) throws InvalidParameterException {
		long startTime = System.currentTimeMillis();
		String orderId = httpRequest.getParameter("orderId");
		Map<String,Object> couponCategoryMap = new HashMap<String, Object>();
		if (orderId == null || orderId.isEmpty()) {
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullOrderIdInRequest");
			couponCategoryMap.put("errorCode", "ORDERID_IS_NULL");
			couponCategoryMap.put("errorMessage", "Transaction OrderId is required");
			logger.error("OrderId is null in request");
			return couponCategoryMap;
		}

		logger.info("Started getting Alltype coupons for orderId " + orderId);

    	/*Getting transaction amount*/
		UserTransactionHistory userTransactionHistory = couponService.findUserTransactionHistoryByOrderId(orderId);
		if (userTransactionHistory == null || userTransactionHistory.getTransactionAmount() == null) {
			couponCategoryMap.put("errorCode", "NO_TXN_HISTORY_FOR_ORDERID");
			couponCategoryMap.put("errorMessage", "Transaction history not found for given orderId");
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullTxnHistoryForOrderId");
			logger.error("Transaction history not found for given orderId " + orderId);
			return couponCategoryMap;
		}

		if (userTransactionHistory.getTransactionStatus() == null) {
			couponCategoryMap.put("errorCode", "NO_TXN_STATUS_FOR_ORDERID");
			couponCategoryMap.put("errorMessage", "Transaction status not found for given orderId");
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullTxnStatusForOrderId");
			logger.error("Transaction status not found for given orderId " + orderId);
			return couponCategoryMap;
		}

		String txnStatus = userTransactionHistory.getTransactionStatus();
		boolean txnSuccess = false;
		if (txnStatus.equalsIgnoreCase("00") || txnStatus.equalsIgnoreCase("0")) {
			txnSuccess = true;
		}

		if (!txnSuccess) {
			couponCategoryMap.put("errorCode", "ORDERID_TXN_NOT_SUCCESS");
			couponCategoryMap.put("errorMessage", "Transaction status not success for given orderId");
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "FalseTxnStatusForOrderId");
			logger.error("Transaction status not success for given orderId " + orderId);
			return couponCategoryMap;
		}

		if (userTransactionHistory.getFkUserId() == null) {
			couponCategoryMap.put("errorCode", "NO_USERID_FOR_ORDERID");
			couponCategoryMap.put("errorMessage", "UserId not found for given orderId");
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullUserIdInTxnHistory");
			logger.error("UserId not found in transaction history for given orderId " + orderId);
			return couponCategoryMap;
		}

		Double txnAmount = userTransactionHistory.getTransactionAmount();

		logger.info("Transaction amount for orderId " + orderId + ", is = " + String.valueOf(txnAmount));

		Map<String, Object> couponAmountQuantityRange = couponService.getAllowedAmountRangeAndQuantityFromCouponTracker(orderId, txnAmount.intValue());
		Integer couponAmountRange = (Integer) couponAmountQuantityRange.get("allowedAmount");
		Integer allowedCouponQuantity = (Integer) couponAmountQuantityRange.get("allowedQuantity");

		if (couponAmountRange < 1 || allowedCouponQuantity < 1) {
			logger.info("Coupons already used for orderId " + orderId);
			couponCategoryMap.put("errorCode", "BUCKETLIMIT_QUANTITY_CROSSED");
			couponCategoryMap.put("errorMessage", "OrderId already crossed bucket limit");
			couponCategoryMap.put("bucketLimit", couponAmountRange);
			couponCategoryMap.put("allowedQuantity", allowedCouponQuantity);
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "BucketLimitCrossed");
			logger.error("OrderId already crossed bucket limit " + orderId);
			return couponCategoryMap;
		}

		logger.info("Starting fetching coupons for orderId = " + orderId);
		boolean wasReco = false;

    	/*Request params from UI*/
		String latStr = httpRequest.getParameter("lat");
		String longStr = httpRequest.getParameter("long");

		String channelId = httpRequest.getParameter("fcChannel");
		int appVersion = setDefaultVersion(httpRequest);

		Integer channel = null;
		if (channelId != null && FCUtil.isInteger(channelId)) {
			channel = Integer.parseInt(channelId);
		}

		String startStr = "0";
		String countStr = "15";
		boolean isInTxn = false;

		String imeiNo = httpRequest.getParameter("imei");
		FreechargeSession session=FCSessionUtil.getCurrentSession();
		if(imeiNo!=null && session!=null) {
			session.setIMEINumber(imeiNo);
		}
		String appVersionStr = httpRequest.getParameter("fcversion");
		logger.info("Request params for  api are lat =" + latStr + ", long = " + longStr + ", orderId = "+ orderId + ", fcChannel = "+ channelId + ", imei = " + imeiNo + "appVersionStr = " + appVersionStr);
		String city = null;

    	/*Setting default values*/
		boolean isPromoted = false;

		logger.info("UserId in session = " + FCSessionUtil.getLoggedInUserId().intValue() + ", for orderId = " + orderId);
		logger.info("UserId in UTH = " + userTransactionHistory.getFkUserId() + ", for orderId = " + orderId);

		if(userTransactionHistory.getFkUserId().intValue() != FCSessionUtil.getLoggedInUserId().intValue()){
			couponCategoryMap.put("errorCode", "USER_ID_NOT_MATCHED");
			couponCategoryMap.put("errorMessage", "user not matched");
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "SessionUserIdNotMatched");
			logger.error("Session userId not matched for orderId " + orderId);
			return couponCategoryMap;
		}

		//Get max or min from values from orderId
		String minValueStr = "0";
		String maxValueStr = String.valueOf(couponAmountRange);
		String minPriceStr = "0";
		String maxPriceStr = String.valueOf(couponAmountRange);

		Timestamp txnDate = userTransactionHistory.getCreatedOn();

    	/*Validation*/
		checkInputFields(startStr, countStr, null, null,
				minValueStr, maxValueStr, minPriceStr, maxPriceStr);
		CouponData couponData = null;
		Map<String, Object> couponDataApiResultMap = new HashMap<>();

		try{
			String categoryIdsStr = null;
			boolean includeTnC = false;
			//	CouponFraudCheckRequest fraudCheckObj = getCouponFraudCheckRequest();

			logger.info("getFilteredCouponsAndHCouponsForDisplay parameters"+" startStr="+startStr+" countStr="+countStr+
					" categoryIdsStr="+categoryIdsStr+" minValueStr="+ minValueStr+" maxValueStr="+
					maxValueStr+" minPriceStr="+ minPriceStr+" maxPriceStr="+ maxPriceStr+" channel="+ channel+
					" includeTnC="+ includeTnC+" isPromoted="+ isPromoted+" city="+ city+" appVersion="+
					appVersion+" latStr="+ latStr+" longStr="+ longStr+" isInTxn="+ isInTxn+" orderId="+
					orderId+" txnDate="+ txnDate);

			long startCouponServiceCallTime = System.currentTimeMillis();
			couponDataApiResultMap = couponServiceProxy.getFilteredCouponsAndHCouponsForDisplay(startStr, countStr,
					categoryIdsStr, minValueStr, maxValueStr, minPriceStr, maxPriceStr, channel,
					null, includeTnC, isPromoted, city, appVersion, latStr, longStr, isInTxn,
					orderId, txnDate, showHighValueCoupons?txnAmount.intValue():Integer.MIN_VALUE);
			metricsClient.recordLatency("CouponServiceCall", "getFilteredCouponsAndHCouponsForDisplay", System.currentTimeMillis() - startCouponServiceCallTime);
			if (couponDataApiResultMap != null && !couponDataApiResultMap.isEmpty()) {
				if (couponDataApiResultMap.containsKey("couponData")) {
					couponData = (CouponData) couponDataApiResultMap.get("couponData");	
				}
				
				if (couponDataApiResultMap.containsKey("errorCode")) {
					String errorCode = (String) couponDataApiResultMap.get("errorCode");	
					String errorMessage = (String) couponDataApiResultMap.get("errorMessage");
					couponCategoryMap.put("errorCode", errorCode);
					couponCategoryMap.put("errorMessage", errorMessage);
					metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "OrderIdExpired");
					logger.error("OrderId date expired for coupon given orderId " + orderId);
					return couponCategoryMap;
				}	
			} else {
				metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullCouponDataApiResultMap");
				logger.error("couponDataApiResultMap is null for " + orderId);
			}
			if (couponData == null) {
				metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullCouponDataObjectInResponse");
				logger.error("couponData object in result map is null for " + orderId);
				return couponCategoryMap;
			}

			if (couponData.getCoupons() == null) {
				metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "NullCouponsListInResponse");
				logger.error("couponList in couponData is null for " + orderId);
			} else {
				if (couponData.getCoupons().size() < 1) {
					metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "EmptyCouponsListInResponse");
					logger.error("couponList in couponData is empty for " + orderId);
				}
			}

			couponCategoryMap.put("coupons", couponData.getCoupons());
			couponCategoryMap.put("categories", couponData.getCategories());
			couponCategoryMap.put("multiCategories", couponData.getMultiCategories());
			couponCategoryMap.put("txnAmount", String.valueOf(txnAmount));
			couponCategoryMap.put("bucketLimit", String.valueOf(couponAmountRange));
			couponCategoryMap.put("allowedQuantity", allowedCouponQuantity);
		} catch(Exception e){
			metricsClient.recordEvent("CouponServiceCall", "fetchCouponsAPIError", "Exception");
			logger.error("Error fetching filteredCoupons.", e);
		}

		logger.info("Filtered-API Finished calling coupon service :" + couponCategoryMap);
		if (channel == FCConstants.CHANNEL_ID_WEB && FCSessionUtil.isUserLoggedIn()){
			tracker.trackEvent(TrackerEvent.COUPON_DESKTOP_USER_IS_LOGGED_IN.createTrackingEvent());
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;

		metricsClient.recordLatency("CouponService", "getFilteredCouponsAndHCoupons", duration);
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated", duration);
			if (wasReco){
				metricsClient.recordLatency("CouponService", "getCategoryCouponsLitePaginated_RECO", duration);
			}
		}

		return couponCategoryMap;
	}


    /*End of getting combined coupons*/

	private void populateIsHCouponVisibleFlag(boolean withCategory,
			CouponData couponData, Map<String, Object> couponCategoryMap) {
		boolean isHCouponVisible = false;
		if(withCategory){
			if (couponData != null && couponData.getCoupons() != null && couponData.getCoupons().size() > 0){
				isHCouponVisible = true;
			}
		}
		couponCategoryMap.put("isHCouponVisible", isHCouponVisible);
	}

	private void checkInputFields(String startStr, String countStr,
			String categoryIdsStr, String isPromotedStr, String minValueStr,
			String maxValueStr, String minPriceStr, String maxPriceStr)
			throws InvalidParameterException {
		if (startStr != null && !FCUtil.isInteger(startStr)){
    		throw new InvalidParameterException("start", startStr, "Expected integer.", ValidationErrorCode.INVALID_START_PARAMETER);
    	}
    	if (countStr != null && !FCUtil.isInteger(countStr)){
    		throw new InvalidParameterException("count", countStr, "Expected integer.", ValidationErrorCode.INVALID_COUNT_PARAMETER);
    	}
    	if (categoryIdsStr != null && !FCUtil.isCommaSeparatedIntegers(categoryIdsStr)){
    		throw new InvalidParameterException("cid", categoryIdsStr, "Expected integer.", ValidationErrorCode.INVALID_CATEGORY_ID);
    	}
    	if (minValueStr != null && !FCUtil.isInteger(minValueStr)){
    		throw new InvalidParameterException("minValue", minValueStr, "Expected integer.", ValidationErrorCode.INVALID_MIN_VALUE_PARAMETER);
    	}
    	if (maxValueStr != null && !FCUtil.isInteger(maxValueStr)){
    		throw new InvalidParameterException("maxValue", maxValueStr, "Expected integer.", ValidationErrorCode.INVALID_MAX_VALUE_PARAMETER);
    	}
    	if (minPriceStr != null && !FCUtil.isInteger(minPriceStr)){
    		throw new InvalidParameterException("minPrice", minPriceStr, "Expected integer.", ValidationErrorCode.INVALID_MIN_PRICE_PARAMETER);
    	}
    	if (maxPriceStr != null && !FCUtil.isInteger(maxPriceStr)){
    		throw new InvalidParameterException("maxPrice", maxPriceStr, "Expected integer.", ValidationErrorCode.INVALID_MAX_PRICE_PARAMETER);
    	}
    	if (isPromotedStr != null && !FCUtil.isInteger(isPromotedStr)){
    		throw new InvalidParameterException("isPromoted", isPromotedStr, "Expected integer.", ValidationErrorCode.INVALID_MAX_VALUE_PARAMETER);
    	}
	}
	
	private void checkLatLongFields(String latStr, String longStr)
			throws InvalidParameterException {
		if (latStr != null && !FCUtil.isFloat(latStr)){
    		throw new InvalidParameterException("latitude", latStr, "Expected Float.", ValidationErrorCode.INVALID_START_PARAMETER);
    	}
    	if (longStr != null && !FCUtil.isFloat(longStr)){
    		throw new InvalidParameterException("longitude", longStr, "Expected Float.", ValidationErrorCode.INVALID_COUNT_PARAMETER);
    	}
	}

	private String cityFilterExperiment(LoadCouponsRequest request,	HttpServletRequest httpRequest) {
        if (!appConfigService.isAppCouponCityFilterEnabled() || FCStringUtils.isBlank(request.getImei())){
            logger.info(String.format("app_coupon_city_filter: %s for imei: %s",
                    appConfigService.isAppCouponCityFilterEnabled(), request.getImei()));
            return null;
        }

        String city = null;
        if (request.getFcChannel().equals(FCConstants.CHANNEL_ID_ANDROID_APP)){
            try {
                city = this.appCityService.getCityForImei(request.getImei());

            }catch (Exception e){
                logger.warn(String.format("no city found for imei: %s", request.getImei()), e);
            }
            if (city!=null){
                //City found. Track measure 1 success in experiment table
                logger.info(String.format("Experiment success for imei: %s", request.getImei()));
                this.couponExperimentService.addCouponExperimentMeasure(httpRequest.getParameter("app_uid"), request.getImei(), 1);
            }else {
                //City not found. Track measure 2 success in experiment table
                this.couponExperimentService.addCouponExperimentMeasure(httpRequest.getParameter("app_uid"), request.getImei(), 2);
            }
        }

        return city;
	}
	
    private CouponFraudCheckRequest getCouponFraudCheckRequest() {
    	String cookie = FCSessionUtil.getPermanentCookie();
		String ipAddress = FCSessionUtil.getIpAddress();
		String email = FCSessionUtil.getLoggedInEmailId();
		String imeiNo = FCSessionUtil.getIMEINumber();
		String profileNo = null;
		if (email != null) {
			Users user = userServiceProxy.getUserByEmailId(email);
			if(user!=null) {
				profileNo = user.getMobileNo();
				logger.info("profileNo: "+ profileNo);
			}
		}
		CouponFraudCheckRequest fraudCheck = new CouponFraudCheckRequest();
		fraudCheck.setCookie(cookie);
		fraudCheck.setEmail(email);
		fraudCheck.setIpAddress(ipAddress);
		fraudCheck.setProfileNo(profileNo);
		fraudCheck.setImeiNo(imeiNo);
		return fraudCheck;
	}

	private String getCityName(HttpServletRequest httpRequest) {
		String ipAddress = MobileUtil.getRealClientIpAddress(httpRequest);
		String cityName = couponServiceProxy.getCityName(ipAddress);
		logger.debug("City for IP " + ipAddress + " =  " + cityName);
		return cityName;
	}

	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/catwcoupons", method = RequestMethod.GET) 
    public @ResponseBody List<CouponCategory> getCategoryListWithCouponIds(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	/*CouponData couponData = couponCacheManager.getCouponDataLite(request.getFcChannel(), getCouponFraudCheckRequest());
    	List<CouponCategory> couponCategories = couponData.getCategories();
    	return couponCategories;*/
		if (CouponDummyDataManager.isDummyCouponMode()){
			return CouponDummyDataManager.getDummyCategories();
		}
    	Integer channel = request.getFcChannel();
    	int appVersion = getAppVersion(httpRequest);
    	CouponData couponData = null;
    	CouponFraudCheckRequest couponFraudCheckRequest = getCouponFraudCheckRequest();
    	if (channel != null && FCConstants.CHANNEL_ID_WEB == channel){
    		couponData = couponServiceProxy.getDesktopCouponDataNonPromotedLite(couponFraudCheckRequest,channel,appVersion);
    	} else{
    		couponData = couponServiceProxy.getCouponDataLite(couponFraudCheckRequest,channel,appVersion);	
    	}
    	List<CouponCategory> couponCategories = new ArrayList<>();
    	if(couponData==null) {
    		return couponCategories;
    	}
    	List<Coupon> coupons= couponData.getCoupons();
    	Map<Integer,Integer> categoryVsFreeCoupons = new HashMap<Integer, Integer>();
    	for(Coupon coupon:coupons) {
    		if(coupon.getPrice()==0) {
	    		if(categoryVsFreeCoupons.get(coupon.getCategoryId()) == null){
	    			categoryVsFreeCoupons.put(coupon.getCategoryId(),0);
	    		}
	    		categoryVsFreeCoupons.put(coupon.getCategoryId(),categoryVsFreeCoupons.get(coupon.getCategoryId())+1);
    		}
    	}
    	couponCategories = couponData.getCategories();
    	for(CouponCategory category:couponCategories) {
    		Integer freeCouponCount=categoryVsFreeCoupons.get(category.getCategoryId());
    		if(freeCouponCount==null){
    			category.setFreeCouponCount(0);
    			category.setPaidCouponCount(category.getCouponIds().size());
    		} else {
    			category.setFreeCouponCount(freeCouponCount);
    			category.setPaidCouponCount(category.getCouponIds().size()-freeCouponCount);
    		}
    	}
    	return couponCategories;
    }
	

	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/getAllCities", method = RequestMethod.GET,produces="application/json") 
    public @ResponseBody List<CityMaster> getAllCities(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		List<CityMaster> allCities = null;
		allCities = locationReadDao.getCities();
		return allCities;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/getAllStates", method = RequestMethod.GET,produces="application/json") 
    public @ResponseBody List<StateMaster> getAllStates(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		List<StateMaster> allStates = null;
		allStates = locationReadDao.getStates();
		return allStates;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/getAllZones", method = RequestMethod.GET,produces="application/json") 
    public @ResponseBody List<ZoneMaster> getAllZones(HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		List<ZoneMaster> allZones = null;
		allZones = locationReadDao.getAllZones();
		return allZones;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/search", method = RequestMethod.GET) 
    public @ResponseBody List<Coupon> getCouponsFor(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
		boolean wasReco = false;
    	MobileUtil.setIPAddressInSession(httpRequest);
    	String startStr = httpRequest.getParameter("start");
    	String countStr = httpRequest.getParameter("count");
     	String categoryIdsStr = httpRequest.getParameter("cid");
     	String isPromotedStr = httpRequest.getParameter("isPromoted");
    	String minValueStr = httpRequest.getParameter("minValue");
    	String maxValueStr = httpRequest.getParameter("maxValue");
    	String minPriceStr = httpRequest.getParameter("minPrice");
    	String maxPriceStr = httpRequest.getParameter("maxPrice");
    	Integer channelId = request.getFcChannel();
    	String searchKey = httpRequest.getParameter("searchKey");
    	String imeiNo = httpRequest.getParameter("imei");
    	FreechargeSession session=FCSessionUtil.getCurrentSession();
    	if(imeiNo!=null && session!=null) {
    		session.setIMEINumber(imeiNo);
    	}
     	checkInputFields(startStr, countStr, categoryIdsStr, isPromotedStr,
				minValueStr, maxValueStr, minPriceStr, maxPriceStr);
    	boolean isPromoted = false;
    	if (isPromotedStr != null){
    		if (Integer.parseInt(isPromotedStr) == 1){
    			isPromoted = true;
    		}
    	}
    	int appVersion = getAppVersion(httpRequest);
    	List<Coupon> coupons=new ArrayList<Coupon>();
    	String city = cityFilterExperiment(request, httpRequest);
    	try{
    		boolean includeTnC = false;
    		coupons=couponServiceProxy.getCouponsForKeySearch(searchKey, startStr, countStr, categoryIdsStr, minValueStr, 
    				maxValueStr, minPriceStr, maxPriceStr, channelId, getCouponFraudCheckRequest(), includeTnC, isPromoted, city, appVersion);
    	} catch(Exception e){
    		logger.error("Error fetching search coupons.", e);
    	}
    	return coupons;
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/promocategories", method = RequestMethod.GET) 
    public @ResponseBody List<PageCouponCategory> getDesktopPromotedCategoryList(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyPageCategories();
		}
    	List<PageCouponCategory> categories = new ArrayList<>();
    	int appVersion = getAppVersion(httpRequest);
    	CouponData couponData = couponServiceProxy.getDesktopCouponDataPromotedLite(getCouponFraudCheckRequest(),request.getFcChannel(),appVersion);
    	if(couponData==null){
    		return categories;
    	}
    	List<CouponCategory> couponCategories = couponData.getCategories();
    	if (couponCategories == null){
    		return categories;
    	}
    	for (CouponCategory couponCategory : couponCategories){
    		if (couponCategory != null && couponCategory.getCouponIds() != null
    				&& couponCategory.getCouponIds().size() > 0){
    			PageCouponCategory category = new PageCouponCategory();
        		category.setCategoryId(couponCategory.getCategoryId());
        		category.setCategoryName(couponCategory.getCategoryName());
        		category.setCouponCount(couponCategory.getCouponIds().size());
        		category.setCategoryImagePath(couponCategory.getCategoryImagePath());
        		categories.add(category);
    		}
    	}
    	return categories;
    }
    

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/location", method = RequestMethod.GET) 
    public @ResponseBody CouponStoreLocation getCouponStoreLocationById(@RequestParam String couponId, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	CouponStoreLocation couponStoreLocation = null;
    	if(couponId!=null && FCUtil.isInteger(couponId)) {
    		try{
    			couponStoreLocation = couponServiceProxy.getCouponLocationById(Integer.parseInt(couponId));
    		} catch (Exception e) {
    			logger.error("Error fetching location for couponId: "+couponId, e);
    		}
    	}
    	return couponStoreLocation;
    }

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/categories", method = RequestMethod.GET) 
    public @ResponseBody List<PageCouponCategory> getCategoryList(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidParameterException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyPageCategories();
		}
    	List<PageCouponCategory> categories = new ArrayList<>();
    	int appVersion = getAppVersion(httpRequest);
     	CouponData couponData = couponServiceProxy.getCouponDataLite(getCouponFraudCheckRequest(),request.getFcChannel(),appVersion);
    	if(couponData==null){
    		return categories;
    	}
     	List<CouponCategory> couponCategories = couponData.getCategories();
    	if (couponCategories == null){
    		return categories;
    	}
    	for (CouponCategory couponCategory : couponCategories){
    		if (couponCategory != null && couponCategory.getCouponIds() != null
    				&& couponCategory.getCouponIds().size() > 0){
    			PageCouponCategory category = new PageCouponCategory();
        		category.setCategoryId(couponCategory.getCategoryId());
        		category.setCategoryName(couponCategory.getCategoryName());
        		category.setCouponCount(couponCategory.getCouponIds().size());
        		category.setCategoryImagePath(couponCategory.getCategoryImagePath());
        		categories.add(category);
    		}
    	}
    	return categories;
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/categoryCoupons", method = RequestMethod.GET) 
    public @ResponseBody CouponData getCategoryCoupons(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyCouponData();
		}
    	MobileUtil.setIPAddressInSession(httpRequest);
    	int appVersion = getAppVersion(httpRequest);
    	CouponData couponData = couponServiceProxy.getCouponData(getCouponFraudCheckRequest(),request.getFcChannel(),appVersion);
    	return couponData;
    }
    
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/tnc", method = RequestMethod.GET) 
    public @ResponseBody CouponTncData getCouponTnC(@RequestParam String couponIds, @RequestParam String ts,
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (couponIds == null || couponIds.isEmpty()){
    		return null;
    	}
    	StringTokenizer tokenizer = new StringTokenizer(couponIds, ",");
    	List<CouponTnC> couponTnCs = new ArrayList<>();
    	Set<CouponCity> cities = new HashSet<>();
    	while (tokenizer.hasMoreTokens()){
    		String couponIdStr = tokenizer.nextToken();
    		if (FCUtil.isInteger(couponIdStr)){
    			int couponId = Integer.parseInt(couponIdStr);
    			CouponTnC couponTnc = couponServiceProxy.getCouponTnC(couponId);
    			if(couponTnc!=null){
    				couponTnCs.add(couponTnc);
    			}
    			List<CouponCity> couponCities = couponServiceProxy.getCouponCities(couponId);
    			if(couponCities!=null) {
    				cities.addAll(couponServiceProxy.getCouponCities(couponId));
    			}
    		}
    	}
    	CouponTncData couponConditions = new CouponTncData();
    	couponConditions.setTncs(couponTnCs);
    	couponConditions.setCities(cities);
    	return couponConditions;
    }
    
    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/tnc1", method = RequestMethod.GET) 
    public @ResponseBody SingleCouponTnC getSingleCouponTnC(@RequestParam String couponId, @RequestParam String ts,
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (couponId == null || couponId.isEmpty()){
    		return null;
    	}
    	SingleCouponTnC singleTnc = new SingleCouponTnC();
    		if (FCUtil.isInteger(couponId)){
    			int couponIdInt = Integer.parseInt(couponId);
    			CouponTnC tnc = couponServiceProxy.getCouponTnC(couponIdInt);   
    			singleTnc.setCouponId(tnc.getCouponId());
    			singleTnc.setTnc(tnc.getTnc());
    			String cityNames = couponServiceProxy.getCommaSeparatedCityNamesWithCouponStoreId(couponIdInt);
    			singleTnc.setCityNames(cityNames);
    		}
    	return singleTnc;
    }
    
    
    
    
	@RequestMapping(value = "/protected/rest/coupon/refresh", method = RequestMethod.GET)
    public @ResponseBody Map<String, Boolean> refreshCoupons(){
		Map<String, Boolean> cacheRefreshData = new HashMap<>();
    	try {
    		cacheRefreshData = couponServiceProxy.doCacheRefresh();
		} catch (Exception e) {
			logger.error("Error refreshing coupon cache: ", e);
		}
    	return cacheRefreshData;
    }
	
	
	
	@RequestMapping(value = "/protected/rest/coupon/merchant/refresh", method = RequestMethod.GET)
    public @ResponseBody String refreshCouponMerchants(){
    	String message = "Coupon refresh succesful";
    	try {
//			voucherService.refreshAllActiveCoupons();
    		couponServiceProxy.refreshCouponMerchantTrieMap();
		} catch (Exception e) {
			message = "Coupon Merchant refresh failed, " + e.getCause();
		}
    	return message;
    }
	
	@RequestMapping(value = "/protected/rest/coupon/atm/refresh", method = RequestMethod.GET)
    public @ResponseBody String refreshATMCoupons(){
    	String message = "ATM Coupon refresh succesful";
    	try {
    		atmFulfilmentService.updateMerchantMap();
		} catch (Exception e) {
			message = "ATM Coupon refresh failed, " + e.getCause();
		}
    	return message;
    }

    private List<VoucherCat> setCouponCategoryOnFirst(final List<VoucherCat> categories, String couponCategoryOnFirst) {
        List<VoucherCat> categoriesNew = new ArrayList<VoucherCat>(categories);
        if (categories != null) {

            for (VoucherCat voucherCat : categories) {
                 if (voucherCat != null && voucherCat.getCatDetail().equals(couponCategoryOnFirst)) {
                    categoriesNew.remove(voucherCat);
                    categoriesNew.add(0, voucherCat);
                 }
            }
        }
        return categoriesNew;

    }

    private void removeEmptyCategories(Map<String, List<CouponVO>> couponsCategoryMap, List<VoucherCat> categories) {
        List<VoucherCat> emptyCategories = new ArrayList<VoucherCat>();
        for (VoucherCat category : categories) {
            if (StringUtils.isEmpty(category.getCatName())) {
                emptyCategories.add(category);
            }
        }
        categories.removeAll(emptyCategories);
    }

    /*
     * To create a new voucher category of name Others.This call will combine all categories into 6 main categories.
     */

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/rest/coupon/allLessCat", method = RequestMethod.GET)
    public @ResponseBody
    CouponsDataVO getAllLessCatCoupons(LoadCouponsRequest request, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyCouponDataVO();
    	}
        long startTime = System.currentTimeMillis(); 
    	MobileUtil.setIPAddressInSession(httpRequest);
    	int appVersion = getAppVersion(httpRequest);
        CouponsDataVO coupons = getAllCouponsWithOrdering(request,appVersion);
        if (coupons == null || coupons.getCategoryMap() == null){
        	return null;
        }
        // For Category Map details :
        Map<String, List<CouponVO>> tempCouponsCategoryMap = coupons.getCategoryMap();
        Map<String, List<CouponVO>> couponsCategoryMap = new HashMap<String, List<CouponVO>>();
        List<CouponVO> sumOfOthersVoucherList = new ArrayList<CouponVO>();
        
        for (Map.Entry<String, List<CouponVO>>  couponCatMap :  tempCouponsCategoryMap.entrySet()) {           
            if (!(FCConstants.VoucherCategoryList.contains(couponCatMap.getKey()))) {
                List<CouponVO> voucherList = couponCatMap.getValue();
                for (CouponVO voucherVO : voucherList) {
                    voucherVO.setVoucherCat("Others");
                    sumOfOthersVoucherList.add(voucherVO);
                }
            } else {
                if ("ALL".equals(couponCatMap.getKey())) {
                    for (CouponVO voucherVOALL : couponCatMap.getValue()) {
                        if (!(FCConstants.VoucherCategoryList.contains(voucherVOALL.getVoucherCat()))) {
                            voucherVOALL.setVoucherCat("Others");
                        }           
                    }
                }
                couponsCategoryMap.put(couponCatMap.getKey(), couponCatMap.getValue());
            }
        }
        couponsCategoryMap.put("Others", sumOfOthersVoucherList);
        
        //For Category lists: 
        List<com.freecharge.api.coupon.service.web.model.VoucherCat> webcats = coupons.getCategories();
        webcats = getVouchersNotCatOthers(webcats);
        com.freecharge.api.coupon.service.web.model.VoucherCat voucherCatForOthers = getVoucherCatDetailsForOthers();
        webcats.add(voucherCatForOthers);
        
        CouponsDataVO couponsDetails = new CouponsDataVO();
        couponsDetails.setCategoryMap(couponsCategoryMap);
        couponsDetails.setCategories(webcats);
        long endTime = System.currentTimeMillis(); 
		long duration = endTime - startTime;
		if(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED)){
			metricsClient.recordLatency("CouponService", "getAllLessCatCoupons", duration);
		}
        return couponsDetails;
    }

    @NoSessionWrite
	@NoLogin
	@RequestMapping(value = "/rest/coupons/save", method = RequestMethod.POST)
    public @ResponseBody
    String saveCoupon(SaveCouponsRequest saveCouponsRequest, Model model) throws InvalidPathValueException{
		String result = "true";
        TxnCrossSellBusinessDo bdo;
		try {
			bdo = MapperUtil.map(saveCouponsRequest, TxnCrossSellBusinessDo.class);
			TxnHomePageBusinessDO txnHomePageBusinessDO = txnHomePageService.getTransactionDetails(saveCouponsRequest
			        .getLookupID());
			bdo.setHomePageId(txnHomePageBusinessDO.getTxnHomePageId());
			bdo.setServiceNo(txnHomePageBusinessDO.getServiceNumber());
			bdo.setProType(txnHomePageBusinessDO.getProductType());
			bdo.setOperatorName(txnHomePageBusinessDO.getOperatorName());
			txnCrossSellService.saveAllTxnCrossSellService(bdo);
		} catch (Exception e) {
			result = "false";
		}
        return result;
    }		
    
    
    @NoSessionWrite
	@NoLogin
    @RequestMapping(value = "/rest/coupons/recommended", method = RequestMethod.GET)
	public @ResponseBody List<Integer> getRecoCoupons(@RequestParam Integer fcChannel){
    	
    	List<Integer> couponIds = null;
    	try{
    		String userEmailId = FCSessionUtil.getLoggedInEmailId();
    		Integer userId = userIdHelper.getUserIdFromEmail(userEmailId);
         	couponIds = couponServiceProxy.getRecommendedCouponsForUser(userId, fcChannel);
    	}catch(Exception e){
    		logger.error("An exception occured in getRecoCoupons : " + e);
    	}
    	return couponIds;
	}
    
    
	private com.freecharge.api.coupon.service.web.model.VoucherCat getVoucherCatDetailsForOthers() {
		com.freecharge.api.coupon.service.web.model.VoucherCat voucherCatForOthers = new com.freecharge.api.coupon.service.web.model.VoucherCat();
        voucherCatForOthers.setCatDetail("Others");
        voucherCatForOthers.setCatFlag(true);
        voucherCatForOthers.setCatName("Others");
        voucherCatForOthers.setVoucherCatId(1000);
        return voucherCatForOthers;
    }

    private List<com.freecharge.api.coupon.service.web.model.VoucherCat> getVouchersNotCatOthers(List<com.freecharge.api.coupon.service.web.model.VoucherCat> tempCategories) {
        List<com.freecharge.api.coupon.service.web.model.VoucherCat> categoryList = new ArrayList<com.freecharge.api.coupon.service.web.model.VoucherCat>();
        for (com.freecharge.api.coupon.service.web.model.VoucherCat voucherCat : tempCategories) {
            if (FCConstants.VoucherCategoryList.contains(voucherCat.getCatName())) {
                categoryList.add(voucherCat);
            }
        }
    return categoryList;
   }
    
    private boolean validateUserSessionEmail(String userEmailId) throws InvalidPathValueException {
		boolean isValid = false;
		if (userEmailId != null && userEmailId.equals(FCSessionUtil.getLoggedInEmailId())){
			isValid = true;
		}
		if (!isValid){
			throw new InvalidPathValueException("/users/{userEmailId}/cards", userEmailId, FCSessionUtil.getLoggedInEmailId());
		}
		return isValid;
	}
    private static Random random = new Random(System.currentTimeMillis()); 
    
    /*New API to mark coupon as used*/
    @RequestMapping(value = "/rest/coupons/markAsUsed", method = RequestMethod.POST)
    public @ResponseBody String markCouponAsUsed(@RequestBody MarkAsUsedpojo jsonRequest){
 
    	logger.info("Entered into API /rest/coupons/markAsUsed");
 
    	Integer userId = jsonRequest.getUser_id();
    	String couponCode = jsonRequest.getCoupon_code();
    	String orderId = jsonRequest.getOrder_id();
    	boolean is_used=	jsonRequest.isIs_used();
    	String status;
    	
    	
    	long startTime = System.currentTimeMillis();
    	status= couponServiceProxy.markCouponAsUsedInApiService(couponCode,orderId,is_used);

    	metricsClient.recordLatency("CouponServiceCall", "markCouponAsUsed", System.currentTimeMillis() - startTime);
    	
    	return status;
    }
}