package com.freecharge.rest.coupon;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.exception.ResourceNotFoundException;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.csrf.annotations.Csrf;

@Controller
@RequestMapping("/*")
public class FixCouponRewardController {
	
	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private OrderIdSlaveDAO orderIdSlaveDAO;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@Autowired
    private FixRewardCouponService fixRewardCouponService;
	
	@NoLogin
    @RequestMapping(value = "/coupon/reward", method = RequestMethod.GET) 
    public String getFixedRewardCoupon(@RequestParam(value="l") String lookupId,HttpServletRequest httpRequest, HttpServletResponse response,  Model model) {
    	String orderId=orderIdSlaveDAO.getOrderIdForLookUpId(lookupId);
    	if(orderId==null) {
			logger.error("Error, order id is null for the lookupId : " + lookupId);
			throw new ResourceNotFoundException();
    	}
    	List<Coupon> coupons=couponServiceProxy.getFixedRewardCoupon(orderId);
    	Boolean eligible = couponServiceProxy.getFixedRewardCouponsEligible(orderId);
    	if(coupons==null || coupons.size()==0) {
    		throw new ResourceNotFoundException();
    	}
    	int totalCouponWorth = 0;
    	for(Coupon coupon:coupons) {
    		totalCouponWorth+=coupon.getCouponValue();
    	}
    	model.addAttribute("totalCouponWorth",totalCouponWorth);
    	model.addAttribute("coupons", coupons);
    	model.addAttribute("l", lookupId);
    	model.addAttribute("eligible", eligible);
    	return "coupons/couponReward";
    }
	
	@NoLogin
	@Csrf(exclude=true)
    @RequestMapping(value = "/coupon/reward/claim", method = RequestMethod.POST) 
    public @ResponseBody boolean blockFixedRewardCoupon(@RequestParam(value="l") String lookupId,HttpServletRequest httpRequest, HttpServletResponse response,  Model model) {
    	String orderId=orderIdSlaveDAO.getOrderIdForLookUpId(lookupId);
    	if(orderId==null) {
			logger.error("Error, order id is null for the lookupId : " + lookupId);
    		return false;
    	}
    	Boolean eligible = couponServiceProxy.getFixedRewardCouponsEligible(orderId);
    	if (!eligible) {
    		return false;
    	}
    	return fixRewardCouponService.doFixedCouponFulfilment(orderId);
    }

	
}
