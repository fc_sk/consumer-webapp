package com.freecharge.rest.coupon;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;

public class CouponControllerHelper {
	
	private static final String AFFILIATE_ACCESS_TOKEN = "512c95d5-61f8-4146-be44-307ec2e900fb";
	
	public static void validateAffiliateCouponIds(String couponIds,
			Integer affiliateId) throws InvalidParameterException {
		if (affiliateId == FCConstants.AFFILIATE_ID_BDK){
			List<Integer> couponIdList = FCUtil.getIntegerList(couponIds);
			if (couponIdList != null && couponIdList.size() > 1){
				throw new InvalidParameterException("couponIds", couponIds, "couponIds should have only 1 couponId.", ValidationErrorCode.INVALID_COUPON_IDENTIFIER);
			}
		}
	}
	
	public static void validateMaxValue(String maxValueStr)
			throws InvalidParameterException {
		if (maxValueStr != null && !FCUtil.isInteger(maxValueStr)){
    		throw new InvalidParameterException("maxValue", maxValueStr, "Expected integer.", ValidationErrorCode.INVALID_MAX_VALUE_PARAMETER);
    	}
	}

	public static  void validateMinValue(String minValueStr)
			throws InvalidParameterException {
		if (minValueStr != null && !FCUtil.isInteger(minValueStr)){
    		throw new InvalidParameterException("minValue", minValueStr, "Expected integer.", ValidationErrorCode.INVALID_MIN_VALUE_PARAMETER);
    	}
	}

	public static  void validateCategoryIds(String categoryIdsStr)
			throws InvalidParameterException {
		if (categoryIdsStr != null && !FCUtil.isCommaSeparatedIntegers(categoryIdsStr)){
    		throw new InvalidParameterException("cid", categoryIdsStr, "Expected comma separated integers.", ValidationErrorCode.INVALID_CATEGORY_ID);
    	}
	}
	
	public static  void validateCouponIds(String couponIds)
			throws InvalidParameterException {
		if (couponIds != null && !FCUtil.isCommaSeparatedIntegers(couponIds)){
    		throw new InvalidParameterException("couponIds", couponIds, "Expected comma separated integers.", ValidationErrorCode.INVALID_CATEGORY_ID);
    	}
	}

	public static  void validateCount(String countStr)
			throws InvalidParameterException {
		if (countStr != null && !FCUtil.isInteger(countStr)){
    		throw new InvalidParameterException("count", countStr, "Expected integer.", ValidationErrorCode.INVALID_COUNT_PARAMETER);
    	}
	}

	public static  void validateStart(String startStr)
			throws InvalidParameterException {
		if (startStr != null && !FCUtil.isInteger(startStr)){
    		throw new InvalidParameterException("start", startStr, "Expected integer.", ValidationErrorCode.INVALID_START_PARAMETER);
    	}
	}
	
	public static void validateAffiliateId(String affiliateId)
			throws InvalidParameterException {
		if (!FCUtil.isInteger(affiliateId)){
			throw new InvalidParameterException("affiliateId",affiliateId, "affiliateId value should not be empty.", ValidationErrorCode.INVALID_AFFILIATE_IDENTIFIER);
		}else if(affiliateId.length() > 4){
			throw new InvalidParameterException("affiliateId", affiliateId, "affiliateId value should have max 4 length.", ValidationErrorCode.INVALID_AFFILIATE_IDENTIFIER);
		} else if (!affiliateId.equals("70")){
			throw new InvalidParameterException("affiliateId", affiliateId, "affiliateId value invalid.", ValidationErrorCode.INVALID_AFFILIATE_IDENTIFIER);
		}
	}
	
	public static void validateAffiliateAccessToken(String accessToken)
			throws InvalidParameterException {
		if (accessToken==null || accessToken.isEmpty()){
			throw new InvalidParameterException("accessToken",accessToken, "accessToken value should not be empty.", ValidationErrorCode.INVALID_AFFILIATE_IDENTIFIER);
		}else if(!accessToken.equalsIgnoreCase(AFFILIATE_ACCESS_TOKEN)){
			throw new InvalidParameterException("accessToken", accessToken, "accessToken value invalid.", ValidationErrorCode.INVALID_AFFILIATE_IDENTIFIER);
		} 
	}
	
	public static void validateMerchantId(String merchantId)
			throws InvalidParameterException {
		if (!FCUtil.isInteger(merchantId)){
			throw new InvalidParameterException("merchantId",merchantId, "merchantId value should not be empty.", ValidationErrorCode.INVALID_MERCHANT_IDENTIFIER);
		}else if(merchantId.length() > 5){
			throw new InvalidParameterException("merchantId", merchantId, "merchantId value should have max 6 length.", ValidationErrorCode.INVALID_MERCHANT_IDENTIFIER);
		}
	}

	public static void validateOrderId(String orderId)
			throws InvalidParameterException {
		if (StringUtils.isBlank(orderId)){
			throw new InvalidParameterException("orderId", orderId, "orderId value should not be empty.", ValidationErrorCode.INVALID_ORDER_IDENTIFIER);
		}else if(orderId.length() > 19){
			throw new InvalidParameterException("orderId", orderId, "orderId value should have max 19 length.", ValidationErrorCode.INVALID_ORDER_IDENTIFIER);
		}
	}
	
	public static void validatePrnNo(String prnNo)
			throws InvalidParameterException {
		if (StringUtils.isBlank(prnNo)){
			throw new InvalidParameterException("prnNo", prnNo, "prnNo value should not be empty.", ValidationErrorCode.INVALID_PRN_NO);
		} else if(prnNo.length() > 19){
			throw new InvalidParameterException("prnNo", prnNo, "prnNo value should have max 19 length.", ValidationErrorCode.INVALID_PRN_NO);
		}
	}

	public static void validateAtmId(String atmId)
			throws InvalidParameterException {
		if (StringUtils.isBlank(atmId)){
			throw new InvalidParameterException("atmIdentifier", atmId, "atmIdentifier value should not be empty.", ValidationErrorCode.INVALID_ATM_IDENTIFIER);
		}else if(atmId.length() > 8){
			throw new InvalidParameterException("atmIdentifier", atmId, "atmIdentifier value should have max 8 length.", ValidationErrorCode.INVALID_ATM_IDENTIFIER);
		}
	}
}
