package com.freecharge.rest.coupon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.useragent.UserAgentController;

@Service
public class FixRewardCouponService {
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	public FulfillmentService fulfillmentService;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@Autowired
	private OrderIdSlaveDAO orderIdSlaveDAO;
	
	@Autowired
	private FCProperties	fcProperties;
	
	@Autowired
    private UserAgentController           userAgentController;
	
	@Autowired
    private HomeBusinessDao                       homeBusinessDao;
	
	@Autowired
    private UserTransactionHistoryService transactionHistoryService;
	
	public boolean doFixedCouponFulfilment(String orderId) {
		List<Coupon> coupons=couponServiceProxy.getFixedRewardCoupon(orderId);
    	if(coupons!=null) {
    		List<BlockCouponRequest> blockedCouponList = new ArrayList<>();
    		Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
    		for (Coupon coupon: coupons) {
                BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
                blockCouponRequest.setCouponOptinType(CouponOptinType.OPTIN);
                blockCouponRequest.setCouponStoreId(coupon.getCouponId());
                blockCouponRequest.setOrderId(orderId);
                blockCouponRequest.setQuantity(1);
                blockedCouponList.add(blockCouponRequest);
            }
    		try {
    			Map<String, Object> userDetail = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
    	        UserTransactionHistory userTransactionHistory = transactionHistoryService
    	                .findUserTransactionHistoryByOrderId(orderId);
    	        if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
    	            userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
    	            userDetail.put("circleName", userTransactionHistory.getServiceRegion());
    	            userDetail.put("productType", userTransactionHistory.getProductType());
    	        }
    	        UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
    	        Map<String, String> variableValues = createVariableMapForCouponReward(orderId, userDetailsObj);
    			couponServiceProxy.doCouponFulfillment(blockedCouponList, userId,variableValues);
    			return true;
    		} catch (Exception e) {
    			logger.error("Error in fixed coupon fullfilment",e);
    		}
    	}
    	return false;
	}
	
	private Map<String, String> createVariableMapForCouponReward(String orderId, UserDetails userDetailsObj) {
        String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();

        String voucherPrefix = fcProperties.getProperty("voucherprefix");
        voucherPrefix = voucherPrefix.trim();

        Map<String, String> variableValues = new HashMap<String, String>();
        String userAgentRecordUrl = userAgentController.getRecordUrl(userDetailsObj.getUserEmail());
        String firstName = userDetailsObj.getUserFirstName();
        String customerName = null;
        if (FCUtil.isEmpty(firstName) || firstName.contains("@")) {
            customerName = "FreeCharger";
        } else {
            customerName = firstName;
        }
        variableValues.put("firstname", customerName);
        variableValues.put("imgPrefix", imgPrefix);
        variableValues.put("voucherPrefix", voucherPrefix);
        variableValues.put("userAgentRecordUrl", userAgentRecordUrl);
        variableValues.put("timestamp", String.valueOf(System.currentTimeMillis()));
        variableValues.put("productType", userDetailsObj.getProductType());
        variableValues.put("mobileno", userDetailsObj.getUserMobileNo());
        variableValues.put("orderno", userDetailsObj.getOrderId());
        variableValues.put("userEmail", userDetailsObj.getUserEmail());
		return variableValues;
	}
	
}