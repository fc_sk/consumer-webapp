package com.freecharge.rest.coupon;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.coupon.service.exclusive.ExclusiveCouponService;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.user.helper.RestSessionHelper;

/**
 * This controller class handles the web requests for exclusive coupons
 * @author shirish
 *
 */
@Controller
@RequestMapping("/*")
public class ExclusiveCouponResourceController extends AbstractRestController {
	
	@Autowired
	private ExclusiveCouponService exclusiveCouponService;
	
    @NoLogin
    @RequestMapping(value = "/rest/coupon/exclusive", method = RequestMethod.GET) 
    public @ResponseBody List<Coupon> getExclusiveCoupons(@RequestParam String email, @RequestParam Integer fcChannel, 
    		HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) throws InvalidPathValueException {
    	MobileUtil.setIPAddressInSession(httpRequest);
    	RestSessionHelper.validateUserSessionEmail(email);
    	List<Coupon> coupons = exclusiveCouponService.getExclusiveCouponsForUser(email, fcChannel);
    	return coupons;
    }
    
}
