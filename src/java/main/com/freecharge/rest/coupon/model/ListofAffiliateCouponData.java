package com.freecharge.rest.coupon.model;

import java.util.List;
import java.util.Map;

public class ListofAffiliateCouponData {
	private List<Map<String, String>> listOfCouponDataMap;

	public List<Map<String, String>> getListOfCouponDataMap() {
		return listOfCouponDataMap;
	}

	public void setListOfCouponDataMap(List<Map<String, String>> listOfCouponDataMap) {
		this.listOfCouponDataMap = listOfCouponDataMap;
	}
}