package com.freecharge.rest.coupon.model;

import java.io.Serializable;

/**
 * Created by shwetanka on 1/5/17.
 */
public class SaveCouponRes implements Serializable {
    private Integer couponId;
    private Boolean status;
    private String message;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SaveCouponRes{" +
                "couponId=" + couponId +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
