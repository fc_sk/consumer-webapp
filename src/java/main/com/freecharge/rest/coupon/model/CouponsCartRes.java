package com.freecharge.rest.coupon.model;

import com.freecharge.api.coupon.service.model.CouponCartItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shwetanka on 1/5/17.
 */
public class CouponsCartRes implements Serializable {
    private String orderId;
    private Boolean status;
    private List<CouponCartItem> couponCartItems;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<CouponCartItem> getCouponCartItems() {
        return couponCartItems;
    }

    public void setCouponCartItems(List<CouponCartItem> couponCartItems) {
        this.couponCartItems = couponCartItems;
    }
}
