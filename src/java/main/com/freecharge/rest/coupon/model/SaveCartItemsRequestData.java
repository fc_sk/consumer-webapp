package com.freecharge.rest.coupon.model;
import java.io.Serializable;
import java.util.List;

public class SaveCartItemsRequestData implements Serializable {

	private String orderId;
	private List<SaveCouponReq> saveCouponReqs;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public List<SaveCouponReq> getSaveCouponReqs() {
		return saveCouponReqs;
	}

	public void setSaveCouponReqs(List<SaveCouponReq> saveCouponReqs) {
		this.saveCouponReqs = saveCouponReqs;
	}

	@Override
	public String toString() {
		return "SaveCartItemsRequestData{" +
				"orderId='" + orderId + '\'' +
				", saveCouponReqs=" + saveCouponReqs +
				'}';
	}
}
