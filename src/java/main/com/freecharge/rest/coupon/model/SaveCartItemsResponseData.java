package com.freecharge.rest.coupon.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SaveCartItemsResponseData implements Serializable {
	private String orderId;
	private Boolean status;
	private List<SaveCouponRes> saveCouponRes;

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<SaveCouponRes> getSaveCouponRes() {
		return saveCouponRes;
	}

	public void setSaveCouponRes(List<SaveCouponRes> saveCouponRes) {
		this.saveCouponRes = saveCouponRes;
	}

	@Override
	public String toString() {
		return "SaveCartItemsResponseData{" +
				"orderId='" + orderId + '\'' +
				", status=" + status +
				", saveCouponRes=" + saveCouponRes +
				'}';
	}
}
