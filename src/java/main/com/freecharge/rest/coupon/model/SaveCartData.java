package com.freecharge.rest.coupon.model;

public class SaveCartData {
	private Integer couponId;
	private String couponType;
	private Integer cartItemId;
	private Boolean saveResult;

	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public Boolean getSaveResult() {
		return saveResult;
	}
	public void setSaveResult(Boolean saveResult) {
		this.saveResult = saveResult;
	}
	public Integer getCartItemId() {
		return cartItemId;
	}
	public void setCartItemId(Integer cartItemId) {
		this.cartItemId = cartItemId;
	}
}
