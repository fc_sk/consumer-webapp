package com.freecharge.rest.coupon.model;

public class SaveCartItemsResponse {
    private String orderId;
    private Integer couponId;
    private Boolean result;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "SaveCartItemsResponse [orderId=" + orderId + ", couponId=" + couponId + ", result=" + result + "]";
    }

}
