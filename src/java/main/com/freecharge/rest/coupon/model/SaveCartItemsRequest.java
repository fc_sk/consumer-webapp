package com.freecharge.rest.coupon.model;

public class SaveCartItemsRequest {
    private String orderId;
    private Integer couponId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    @Override
    public String toString() {
        return "SaveCartItemsRequest [orderId=" + orderId + ", couponId=" + couponId + "]";
    }

}
