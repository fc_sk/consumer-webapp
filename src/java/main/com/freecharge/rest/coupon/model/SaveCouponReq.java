package com.freecharge.rest.coupon.model;

import java.io.Serializable;

/**
 * Created by shwetanka on 1/5/17.
 */
public class SaveCouponReq implements Serializable {
    private Integer couponId;
    private String couponType;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    @Override
    public String toString() {
        return "SaveCouponReq{" +
                "couponId=" + couponId +
                ", couponType='" + couponType + '\'' +
                '}';
    }
}
