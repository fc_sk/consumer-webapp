package com.freecharge.rest.coupon.model;

public class AffiliateCouponDataLite {
	private String couponCode;
	private String tnCUrl;
	private String shortDesc;
	private String validity;
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getTnCUrl() {
		return tnCUrl;
	}
	public void setTnCUrl(String tnCUrl) {
		this.tnCUrl = tnCUrl;
	}	
	@Override
	public String toString() {
		return new StringBuilder().append(
		"couponCode: " + couponCode + ", tnCUrl: " + tnCUrl
		+ ", shortDesc: " + shortDesc + ", validity:"
		+ validity).toString();
	}

}
