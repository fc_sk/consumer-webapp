package com.freecharge.rest.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.analytics.CartItemDetails;
import com.freecharge.analytics.OrderDetails;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.service.CouponCrossSellService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.RestValidationHelper;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.webdo.RechargeInitiateWebDo;

@Controller
@RequestMapping("/rest/orders/*")
public class OrderResourceController extends AbstractRestController{
	@Autowired 
	private RestValidationHelper validationHelper;
	
	@Autowired
	private CouponCrossSellService couponCrossSellService;
	
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private RechargeInitiateService rechargeInitiateService;
    
    @NoSessionWrite
	@RequestMapping(value="/{orderId}/coupons", method=RequestMethod.GET)
	public @ResponseBody List<CouponHistory> getCouponsForOrder(@PathVariable String orderId, ModelMap model) throws InvalidPathValueException {
		validationHelper.validateOrderIdBelongsToUser(orderId);
		List<String> orderIds = new ArrayList<String>();
		orderIds.add(orderId);
		List<CouponHistory> coupons = couponCrossSellService.getCouponAndCrossSellHistory(orderIds);
		return coupons;
	}
	
    @NoSessionWrite
    @RequestMapping(value="/{orderId}/details", method=RequestMethod.GET)
    public @ResponseBody Map<String, Object> getOrderDetails(@PathVariable String orderId) throws InvalidPathValueException {
        Map<String, Object> responseMap = new HashMap<>();
        
        RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        
        OrderDetails od = new OrderDetails();
        List<CartItemDetails> cartItemList = new ArrayList<>();
        od.setCartItemList(cartItemList);
        
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        dataMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, true);
        dataMap.put(PaymentConstants.AMOUNT, rechargeInitiateWebDo.getAmount());

        responseMap.put("orderDetails", od);
        responseMap.put("riwd", rechargeInitiateWebDo);
        responseMap.put("resultMap", dataMap);
        responseMap.put("product", rechargeDetails.getProductName());
        responseMap.put("operator", rechargeDetails.getOperatorName());
        responseMap.put("circle", rechargeDetails.getCircleName());

        return responseMap;
    }
}
