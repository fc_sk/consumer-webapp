package com.freecharge.rest.menuoptions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.google.gson.Gson;

@Controller
@RequestMapping("/*")
public class MenuOptionsController {

	@Autowired
	private MenuOptionsService menuOptionsService;

	@Autowired
	private AppConfigService appConfigservice;

	private Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "/app/appmenu", method = RequestMethod.GET)
	public @ResponseBody MenuModel getMenuOptions(HttpServletRequest httpRequest, HttpServletResponse response,
			ModelMap model) throws InvalidPathValueException {
		Integer fcChannel = Integer.parseInt(httpRequest.getParameter("fcChannel"));
		Integer fcversion = Integer.parseInt(httpRequest.getParameter("fcversion"));
		return menuOptionsService.getMenuOptions(fcChannel, fcversion);
	}

	@NoLogin
	@NoSession
	@RequestMapping(value = "protected/get/appmenu", method = RequestMethod.GET)
	public @ResponseBody MenuOptionsDetail getMenuModel(HttpServletRequest httpRequest, HttpServletResponse response,
			ModelMap model) {
		String data = appConfigservice.getChildProperty(appConfigservice.MENU_OPTIONS_CONFIG,
				appConfigservice.MENU_OPTIONS_MODEL);
		return new Gson().fromJson(data, MenuOptionsDetail.class);
	}

	@NoLogin
	@Csrf(exclude = true)
    @NoSessionWrite
	@RequestMapping(value = "protected/update/appmenu", method = RequestMethod.POST,headers = "Accept=application/json")
	public @ResponseBody Boolean updateMenuOptions(@RequestBody MenuOptionsDetail menuOptionsDetail, HttpServletResponse response,
			ModelMap model) throws InvalidPathValueException {
		boolean status= false;
		try {
			Map<String, String> menuModelMap = new HashMap<>();
			menuModelMap.put(appConfigservice.MENU_OPTIONS_MODEL, new Gson().toJson(menuOptionsDetail));
			String jsonMap=JSONValue.toJSONString(menuModelMap);
			status=appConfigservice.setMenuOptionsValue(jsonMap);
		} catch (Exception e) {
			logger.error("Error in Saving MenuModel");
		}
		return status;
	}

}
