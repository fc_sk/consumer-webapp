package com.freecharge.rest.menuoptions;

import java.io.Serializable;

public class SubMenuOptionsDetail implements Serializable{

	private String displayName;
	private String url;
	private Boolean isNew;
	private int minAppVersion;
	private int maxAppVersion;
	private int fcChannel;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public int getMinAppVersion() {
		return minAppVersion;
	}
	public void setMinAppVersion(int minAppVersion) {
		this.minAppVersion = minAppVersion;
	}
	public int getMaxAppVersion() {
		return maxAppVersion;
	}
	public void setMaxAppVersion(int maxAppVersion) {
		this.maxAppVersion = maxAppVersion;
	}
	public int getFcChannel() {
		return fcChannel;
	}
	public void setFcChannel(int fcChannel) {
		this.fcChannel = fcChannel;
	}
	@Override
	public String toString() {
		return "SubMenuOptionsDetail [displayName=" + displayName + ", url=" + url + ", isNew=" + isNew + ", minAppVersion=" + minAppVersion + ", maxAppVersion=" + maxAppVersion + ", fcChannel="
				+ fcChannel + "]";
	}
	
	

}
