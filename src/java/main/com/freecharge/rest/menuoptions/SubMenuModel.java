package com.freecharge.rest.menuoptions;

import org.codehaus.jackson.annotate.JsonIgnore;

public class SubMenuModel {
	private String displayName;
	private String url;
	private Boolean isNew=true;
	@JsonIgnore
	private int minAppVersion;
	@JsonIgnore
	private int maxAppVersion;
	@JsonIgnore
	private int fcChannel;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getMinAppVersion() {
		return minAppVersion;
	}
	public void setMinAppVersion(int minAppVersion) {
		this.minAppVersion = minAppVersion;
	}
	public int getMaxAppVersion() {
        return maxAppVersion;
    }
    public void setMaxAppVersion(int maxAppVersion) {
        this.maxAppVersion = maxAppVersion;
    }
    public int getFcChannel() {
		return fcChannel;
	}
	public void setFcChannel(int fcChannel) {
		this.fcChannel = fcChannel;
	}
    @Override
    public String toString() {
        return "SubMenuModel [displayName=" + displayName + ", url=" + url + ", isNew=" + isNew + ", minAppVersion="
                + minAppVersion + ", maxAppVersion=" + maxAppVersion + ", fcChannel=" + fcChannel + "]";
    }
}
