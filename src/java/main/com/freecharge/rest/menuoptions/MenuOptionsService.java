package com.freecharge.rest.menuoptions;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.google.gson.Gson;

@Service
public class MenuOptionsService {

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private AppConfigService appConfigService;

	public MenuModel getMenuOptions(Integer fcChannel,Integer fcversion) {
		MenuModel menuModel = new MenuModel();
		try {
			String data=appConfigService.getChildProperty(appConfigService.MENU_OPTIONS_CONFIG, appConfigService.MENU_OPTIONS_MODEL);
			menuModel=new Gson().fromJson(data, MenuModel.class);
			List<SubMenuModel> subMenus=new ArrayList<>();
			for(SubMenuModel subMenu:menuModel.getSubMenu()) {
				if(subMenu.getFcChannel()==fcChannel && fcversion>=subMenu.getMinAppVersion() && fcversion<=subMenu.getMaxAppVersion()){
					subMenus.add(subMenu);
				}
			}
			if(subMenus.size()==0) {
				menuModel.setIsVisible(false);
				menuModel.setSubMenu(null);
			} else {
				menuModel.setSubMenu(subMenus);
			}
		} catch (Exception e) {
			logger.info("Something Error in getMenuOptions", e);
		}
		return menuModel;
	}
}
