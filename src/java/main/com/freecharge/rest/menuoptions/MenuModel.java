package com.freecharge.rest.menuoptions;

import java.util.List;

public class MenuModel {
	String displayName ;
	String iconUrl;
	Boolean isNew=true;
	Boolean isVisible=true;
	List<SubMenuModel> subMenu;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public List<SubMenuModel> getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(List<SubMenuModel> subMenu) {
		this.subMenu = subMenu;
	}
	public Boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public Boolean getIsVisible() {
		return isVisible;
	}
	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
