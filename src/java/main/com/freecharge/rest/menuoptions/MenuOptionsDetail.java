package com.freecharge.rest.menuoptions;

import java.io.Serializable;
import java.util.List;

public class MenuOptionsDetail implements Serializable{
	private String displayName ;
	private String iconUrl;
	private boolean isNew=true;
	private boolean isVisible=true;
	private List<SubMenuOptionsDetail> subMenu;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public boolean getIsNew() {
		return isNew;
	}
	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}
	public boolean getIsVisible() {
		return isVisible;
	}
	public void setIsVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	public List<SubMenuOptionsDetail> getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(List<SubMenuOptionsDetail> subMenu) {
		this.subMenu = subMenu;
	}
	@Override
	public String toString() {
		return "MenuOptionsDetail [displayName=" + displayName + ", iconUrl=" + iconUrl + ", isNew=" + isNew + ", isVisible=" + isVisible + ", subMenu=" + subMenu + "]";
	}
	

}
