package com.freecharge.rest.wallet;

public class InvalidIMSTokenException extends Exception {
    
    public InvalidIMSTokenException(Throwable cause) {
        super(cause);
    }
}
