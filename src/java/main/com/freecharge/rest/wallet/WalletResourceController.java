package com.freecharge.rest.wallet;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.addcash.service.AddCashService;
import com.freecharge.addcash.service.exception.AddCashProductToCartException;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletLimitRequest;
import com.freecharge.wallet.service.WalletLimitResponse;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.AddCashWebDO;

@Controller
@RequestMapping("/rest/*")
public class WalletResourceController {
    
    @Autowired
    private AddCashService addCashService;
    
    @Autowired
    private WalletService walletService;

    
    
    private static interface JSPVariables {
        public final String WALLET_LIMIT_STATUS = "limitStatus";
        public final String WALLET_LIMIT_MESSAGE = "limitFailReason";
    }

    
    @RequestMapping(value="/wallet/add", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> addCash(@ModelAttribute(value = "addCashWebDo") AddCashWebDO addCashWebDO) {
        String lookupId = null;
        try {
            lookupId = addCashService.addProductToCartForAddCash(addCashWebDO.getAddCashAmount());
        } catch (AddCashProductToCartException e) {
            lookupId = null;
        }
        
        Map<String, String> lookupIdMap = new HashMap<String, String>(); 
        lookupIdMap.put("lookupId", lookupId);
        return lookupIdMap;
        
    }
    
    @NoSessionWrite
    @RequestMapping(value = "wallet/limit/{amountToValidate}", method = RequestMethod.GET)
    public @ResponseBody ResponseStatus checkAddCashLimit(@PathVariable String amountToValidate, Model model) {
        ResponseStatus responseStatus = new ResponseStatus();
        boolean isValid = validateForAmount(amountToValidate, responseStatus);
        if (!isValid) {
            return responseStatus; 
            
        }

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Integer userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);

        Amount amountToEvaluate = new Amount(amountToValidate);

        WalletLimitRequest limitRequest = new WalletLimitRequest();
        limitRequest.setAmountToTest(amountToEvaluate);
        limitRequest.setFundSource(Wallet.FundSource.CASH_FUND);
        limitRequest.setTransactionType(Wallet.TransactionType.DEPOSIT);
        limitRequest.setUserId(userId.longValue());

        WalletLimitResponse limitResponse = walletService.evaluateWalletLimit(limitRequest);
        if (Wallet.LimitStatus.Failure == limitResponse.getLimitStatus()) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(limitResponse.getDescriptionString());
        } else {
            responseStatus.setStatus(FCConstants.SUCCESS);
            responseStatus.setResult(FCConstants.SUCCESS);
        }

        return responseStatus;
    }
    
    
    private boolean validateForAmount(String amountString, ResponseStatus responseStatus) {
        boolean isValid = false;

        if (StringUtils.isEmpty(amountString)) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Empty Amount value");
        } else if (!StringUtils.isNumeric(amountString)) {
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Numbers only, please.");
        } else {
            isValid = true;
        }

        return isValid;
    }


}
