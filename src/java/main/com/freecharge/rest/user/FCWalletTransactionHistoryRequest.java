package com.freecharge.rest.user;

import java.io.Serializable;

public class FCWalletTransactionHistoryRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private String            startDate;
    private String            endDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "FCWalletTransactionHistoryRequest { " +
                "startDate = " + startDate +
                " , endDate = " + endDate +
                " }";
    }
}
