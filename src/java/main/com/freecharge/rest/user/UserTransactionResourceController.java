package com.freecharge.rest.user;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.wallet.service.OneCheckUserService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;

@Controller
@RequestMapping("/rest/userTransaction/*")
public class UserTransactionResourceController {
	private static final Logger logger = LoggerFactory.getLogger(UserTransactionResourceController.class);

    @Autowired
    private OneCheckUserService   oneCheckUserService;

    @NoSessionWrite
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "v2/transactiondetails", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public TransactionDetails getTxnDetails(FCWalletTransactionHistoryRequest historyRequest, HttpServletRequest request) throws Exception {
		logger.debug(String.format("Get transaction details is started: request - %s", historyRequest));
        List<OneCheckTransactionSummary> transactionSummaries = oneCheckUserService.findTransactions(historyRequest, request);
		logger.debug(String.format("Get transaction details is completed: request - %s", historyRequest));
        return new TransactionDetails(true, transactionSummaries);
    }
}
