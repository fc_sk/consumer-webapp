package com.freecharge.rest.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.blacklistdomain.service.BlacklistDomainService;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.mobile.web.view.City;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.model.SignupRequest;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.TrackerEvent;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;

/**
 * Controller for Sign-up flow
 */
@Controller
@RequestMapping("/rest/*")
public class SignupResourceController extends AbstractRestController {

    @Autowired
    private RegistrationService             registrationService;

    @Autowired
    private CommonService                   commonService;

    @Autowired
    private MasterData                      masterData;

    private Logger                          logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private Tracker                         tracker;

    @Autowired
    private FCProperties    fcProperties;
    
    @Autowired
    private BlacklistDomainService             blacklistDomainService;
    
    @Autowired
    private MetricsClient              metricsClient;
    
   
    @RequestMapping(value = "signup", method = RequestMethod.POST)
    @NoLogin
    @ResponseBody
    public ResponseStatus doRegistration(final SignupRequest signupRequest, final BindingResult bindingResult,
            final HttpServletRequest request) throws InvalidPathValueException {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "signupResourceController");
        tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_ATTEMPT.createTrackingEvent());
        ResponseStatus responseStatus = new ResponseStatus();
        EmailValidator emailValidator = EmailValidator.getInstance();
        try {
            if (signupRequest == null) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EMPTY_REQUEST));
                return responseStatus;
            }

            if (StringUtils.isBlank(signupRequest.getPassword())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EMPTY_PASSWORD));
                return responseStatus;
            }

            if (signupRequest.getPassword().length() < 6 || signupRequest.getPassword().length() > 40) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_WRONG_PASSWORD_LENGTH));
                return responseStatus;
            }

            if (StringUtils.isBlank(signupRequest.getConfirmPassword())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EMPTY_CONF_PASSWORD));
                return responseStatus;
            }

            if (StringUtils.isBlank(signupRequest.getEmail())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EMPTY_EMAIL));
                return responseStatus;
            }
            if (!emailValidator.isValid(signupRequest.getEmail())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_INVALID_EMAIL));
                return responseStatus;
            }
            if(!FCUtil.isValidEmailFormat(signupRequest.getEmail())) {
            	responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_INVALID_EMAIL_FORMAT));
                return responseStatus;
            }

            if (!blacklistDomainService.isIncorrectDomain(signupRequest.getEmail())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_INCORRECT_DOMAIN));
                return responseStatus;
            }

            if (!blacklistDomainService.isDomainSpelledWrong(signupRequest.getEmail())) {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_DOMAIN_SPELLED_WRONG));
                return responseStatus;
            }
            
            if(!FCUtil.isWelFormedName(signupRequest.getFirstName())) {
            	responseStatus.setStatus(FCConstants.FAIL);
            	responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_INVALID_NAME));
            	return responseStatus;
            }

            if (!signupRequest.getPassword().equals(signupRequest.getConfirmPassword())) {
                bindingResult.rejectValue("password", fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_PASSWORD_MISMATCH));
            }

            if (bindingResult.hasErrors()) {
                metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "fail");
                StringBuffer error = new StringBuffer("");
                for (ObjectError errorObject : bindingResult.getAllErrors()) {
                    if (errorObject.getDefaultMessage() != null) {
                        error.append(errorObject.getDefaultMessage()).append("\n");
                    } else {
                        error.append(errorObject.getCode()).append("\n");
                    }
                }
                responseStatus.setStatus("Validation Fail");
                responseStatus.setResult(error.toString());
                return responseStatus;
            }
            /* Check if another user is already logged in */

            if (FCSessionUtil.isAnotherUserLoggedIn(signupRequest.getEmail())) {
                responseStatus.setStatus("ALREADY_LOGGEDIN");
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_ALREADY_LOGGEDIN));
                return responseStatus;

            }

            RegisterBusinessDO registrationbdo = MapperUtil.map(signupRequest, RegisterBusinessDO.class);

            if (signupRequest.getAffiliate_unique_id() != null
                    && Integer.parseInt(signupRequest.getAffiliate_unique_id()) > 0) {
                registrationbdo.setFkAffiliateProfileId(Integer.parseInt(signupRequest.getAffiliate_unique_id()));
            }

            if (registrationbdo.getCountryId() == null) {
                registrationbdo.setCountryId(FCConstants.DEFAULT_COUNTRY_ID);
            }
            registrationbdo.setLoginSource(SessionConstants.LoginSource.EMAIL);

            // Setting IMEI and Windows Unique device Id for UserDataPointPublishService to publish, 
            // empty check is not required since handled in publish method
            registrationbdo.setImei(request.getParameter(FCConstants.IMEI));
            registrationbdo.setUniqueDeviceId(request.getParameter(FCConstants.DEVICE_UNIQUE_ID));

            String status = registrationService.registerUser(responseStatus, registrationbdo);

            if (FCConstants.SUCCESS.equals(status)) {
                metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "success");
                tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_SUCCESS.createTrackingEvent());
                return responseStatus;
            }
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "fail");
            if (status != null && status.compareToIgnoreCase(WebConstants.EMAIL_ID_EXISTS) == 0) {
                logger.error("Your requested " + registrationbdo.getEmail()
                        + " already exist with us.so cannot registered ");
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult("Your requested email id already exist with us");
            } else {
                logger.error("Your requested " + registrationbdo.getEmail()
                        + " already exist with us.so cannot registered ");
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EXCEPTION));
            }

        } catch (FCRuntimeException fcexp) {
            logger.error("Error during signup", fcexp);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EXCEPTION));
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
        } catch (Exception exp) {
            logger.error("Unable to Register User", exp);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_EXCEPTION));
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "signup", endTime - startTime);
        return responseStatus;
    }

    @RequestMapping(value = "state/get", method = RequestMethod.GET)
    @NoLogin
    @ResponseBody
    public Map<Integer, StateMaster> getStateList() throws InvalidPathValueException {
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "srcGetStateList");
        return commonService.getStateMasterMap();

    }

    @NoSessionWrite
    // TODO Need to exclude from session altogether.
    @RequestMapping(value = "city/get/{stateId}", method = RequestMethod.GET)
    @NoLogin
    @ResponseBody
    public Map<String, List<City>> getStateList(@PathVariable final Integer stateId) throws InvalidPathValueException {
        Map<String, List<City>> cityMap = new HashMap<String, List<City>>();
        cityMap.put("cityList", masterData.getCitiesForState(stateId));
        return cityMap;

    }

}
