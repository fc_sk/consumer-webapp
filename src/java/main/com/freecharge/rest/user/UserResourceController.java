package com.freecharge.rest.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.*;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.Null;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.model.CouponHistoryMaster;
import com.freecharge.api.coupon.service.model.CouponImage;
import com.freecharge.api.coupon.service.web.model.CouponHistoryTnC;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.domain.view.CouponHistoryView;
import com.freecharge.app.domain.view.CouponHistoryViewLite;
import com.freecharge.app.domain.view.CouponHistoryViewMapper;
import com.freecharge.cardstorage.ICardStorageService;
import com.freecharge.common.businessdo.MyContactsBussinessDO;
import com.freecharge.common.businessdo.MyContactsListBusinessDo;
import com.freecharge.common.businessdo.MyRechargesBusinessDO;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.encryption.mdfive.MD5;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.HttpExecutorService;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.DynamoConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.RechargeProductType;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeDoType;
import com.freecharge.recharge.businessDo.SuccessfulBillRechargeDo;
import com.freecharge.recharge.businessDo.SuccessfulBillRechargeWebDo;
import com.freecharge.recharge.businessDo.SuccessfulRechargeDo;
import com.freecharge.recharge.businessDo.SuccessfulRechargeWebDoNew;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.RestConstants;
import com.freecharge.rest.user.helper.RestSessionHelper;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.mapper.MyProfileDOMapper;
import com.freecharge.web.webdo.ProfileWebDO;

@Controller
@Service
@RequestMapping("/")
public class UserResourceController extends AbstractRestController {

	@Autowired
	@Qualifier("card-and-meta")
	private ICardStorageService cardStorageService;

	@Autowired
	private MyAccountService myAccountService;

	@Autowired
	private WalletService walletService;

	@Autowired
	private UserIdHelper userIdHelper;

	@Autowired
	private UserService userService;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private MetricsClient metricsClient;

	private final Logger logger = LoggingFactory.getLogger(getClass());


	private final org.slf4j.Logger log = LoggerFactory.getLogger(UserResourceController.class);
	@Autowired
	private RechargeDetailsService successfulRechargeService;

	@Autowired
	private SuccessfulRechargeService sucRechargeService;

	@Autowired
	private CouponHistoryViewMapper couponHistoryViewMapper;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	@Autowired
	WalletWrapper walletWrapper;

	@Autowired
	private OneCheckWalletService ocService;

	@Autowired
	OperatorCircleService prefixService;

	@Autowired
	private CommonService commonService;

	SimpleDateFormat writeDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public static final Integer MINIMUM_PARTIAL_EMAILID_LENGTH = 8;

	private static final int CIRCLE_ID_ALL = 33;

	@Autowired
	private HttpExecutorService httpExecuterService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private HttpClient rechargePlanHttpClient;

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/cards", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getCards(@PathVariable String userEmailId, ModelMap model)
			throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		int userId = getUserId(userEmailId);
		return cardStorageService.list(userId);
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/contacts", method = RequestMethod.GET)
	public @ResponseBody List<MyContactsBussinessDO> getContacts(@PathVariable String userEmailId, ModelMap model)
			throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		int userId = getUserId(userEmailId);
		MyContactsListBusinessDo mycontactslistbusinessdo = new MyContactsListBusinessDo();
		mycontactslistbusinessdo.setUserid(userId);
		myAccountService.getMycontacts(mycontactslistbusinessdo);
		return mycontactslistbusinessdo.getContactslist();
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/recharges", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, String>> getUserRecharges(@PathVariable String userEmailId, ModelMap model,
			HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		int userId = getUserId(userEmailId);
		MyRechargesBusinessDO bdo = new MyRechargesBusinessDO();
		bdo.setUserid(userId);
		if (request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY) != null
				&& !request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY).isEmpty()) {
			bdo.setChannel(request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY));
		}
		bdo.setAppVersion(MobileUtil.getMobileAppVersionFromRequest(request));
		if (request.getParameter("allTxns") != null && !request.getParameter("allTxns").isEmpty()
				&& Boolean.parseBoolean(request.getParameter("allTxns"))) {
			bdo.setReturnPaymentTxns(true);
		}
		myAccountService.getUserTransactionHistory(bdo);
		return bdo.getRechargeHistoryList();
	}


	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/rechargedetails", method = RequestMethod.GET)
	public @ResponseBody List<RechargeDetails> getUserRechargeDetails(@PathVariable String userEmailId,
			HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		List<RechargeDetails> allRechargeDetails = successfulRechargeService.getAllUserRecharges(userEmailId, request);
		return allRechargeDetails;
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/recharges/success", method = RequestMethod.GET)
	public @ResponseBody List<RechargeDetails> getUserSuccesfulRecharges(@PathVariable String userEmailId,
			HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		List<RechargeDetails> successfullRechargeDetails = successfulRechargeService
				.getSuccessfulUserRecharges(userEmailId, request);

		return successfullRechargeDetails;
	}

	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/rest/v2/users/recharges/success", method = RequestMethod.GET)
	public @ResponseBody List<RechargeDetails> getUserSuccesfulRechargesTransferToken(HttpServletRequest request)
			throws InvalidPathValueException {
		String transferToken = request.getHeader("txtoken");

		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");

		if (StringUtils.isBlank(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}

		if (StringUtils.isBlank(ipAddress)) {
			ipAddress = "NA";
		}

		try {
			String userEmailByTransferToken = ocService.getUserEmailByTransferToken(transferToken, userAgent,
					ipAddress);

			List<RechargeDetails> successfullRechargeDetails = successfulRechargeService
					.getSuccessfulUserRecharges(userEmailByTransferToken, request);
			return successfullRechargeDetails;
		} catch (Exception e) {
			logger.error("Got exception while fetching user from transfer token: ", e);
			throw new InvalidPathValueException("/rest/v2/users/{userEmailId}/recharges/success", "", "");
		}
	}
	
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/rest/v2/users/transactions/success/new", method = RequestMethod.GET)
	public @ResponseBody Map<String, List<?>> getSuccesfulRechargesTransferToken(HttpServletRequest request)
			throws InvalidPathValueException {
		String transferToken = request.getHeader("txtoken");

		String userAgent = request.getHeader("User-Agent");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");

		if (StringUtils.isBlank(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}

		if (StringUtils.isBlank(ipAddress)) {
			ipAddress = "NA";
		}

		try {
			String userEmailByTransferToken = ocService.getUserEmailByTransferToken(transferToken, userAgent,
					ipAddress);
			return getRecentsAPIResponse(userEmailByTransferToken);

		} catch (Exception e) {
			logger.error("Got exception while fetching user from transfer token: ", e);
			throw new InvalidPathValueException("/rest/v2/users/transactions/success/new", "", "");
		}
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/transactions/success/new", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, ?>> getSuccesfulRecharges(@PathVariable String userEmailId,
			HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		List<Map<String, ?>> response = new ArrayList<Map<String, ?>>();
		Map<String, AttributeValue> dynamoResponse = sucRechargeService.getSuccessfulRechargesForEmailId(userEmailId,
				null);
		if (dynamoResponse != null) {
			if (dynamoResponse.get(DynamoConstants.MOBILE_AMOUNT_FIELD) != null) {
				String prepaidDynamo = dynamoResponse.get(DynamoConstants.MOBILE_AMOUNT_FIELD).getS();
				Map<String, ?> prepaidResponse = formatDynamoData(prepaidDynamo, RechargeConstants.MOBILE_PRODUCT_TYPE,
						userEmailId);
				response.add(prepaidResponse);
			}
			if (dynamoResponse.get(DynamoConstants.POSTPAID_AMOUNT_FIELD) != null) {
				String postpaidDynamo = dynamoResponse.get(DynamoConstants.POSTPAID_AMOUNT_FIELD).getS();
				Map<String, ?> postpaidResponse = formatDynamoData(postpaidDynamo,
						RechargeConstants.POST_PAID_PRODUCT_TYPE, userEmailId);
				response.add(postpaidResponse);
			}
			if (dynamoResponse.get(DynamoConstants.DATACARD_AMOUNT_FIELD) != null) {
				String datacardDynamo = dynamoResponse.get(DynamoConstants.DATACARD_AMOUNT_FIELD).getS();
				Map<String, ?> datacardResponse = formatDynamoData(datacardDynamo,
						RechargeConstants.DATA_CARD_PRODUCT_TYPE, userEmailId);
				response.add(datacardResponse);
			}
			if (dynamoResponse.get(DynamoConstants.POSTAPID_DATACARD_AMOUNT_FIELD) != null) {
				String datacardDynamo = dynamoResponse.get(DynamoConstants.POSTAPID_DATACARD_AMOUNT_FIELD).getS();
				Map<String, ?> datacardResponse = formatDynamoData(datacardDynamo,
						RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE, userEmailId);
				response.add(datacardResponse);
			}
			if (dynamoResponse.get(DynamoConstants.DTH_AMOUNT_FIELD) != null) {
				String dthDynamo = dynamoResponse.get(DynamoConstants.DTH_AMOUNT_FIELD).getS();
				Map<String, ?> dthResponse = formatDynamoData(dthDynamo, RechargeConstants.DTH_PRODUCT_TYPE,
						userEmailId);
				response.add(dthResponse);
			}
			if (dynamoResponse.get(DynamoConstants.ELECTRICITY_AMOUNT_FIELD) != null) {
				String electricityDynamo = dynamoResponse.get(DynamoConstants.ELECTRICITY_AMOUNT_FIELD).getS();
				Map<String, ?> electricityResponse = formatBillTypeDynamoData(electricityDynamo,
						RechargeConstants.ELECTRICITY_PRODUCT_TYPE, userEmailId);
				response.add(electricityResponse);
			}
			if (dynamoResponse.get(DynamoConstants.GAS_AMOUNT_FIELD) != null) {
				String gasDynamo = dynamoResponse.get(DynamoConstants.GAS_AMOUNT_FIELD).getS();
				Map<String, ?> gasResponse = formatBillTypeDynamoData(gasDynamo, RechargeConstants.GAS_PRODUCT_TYPE,
						userEmailId);
				response.add(gasResponse);
			}
			if (dynamoResponse.get(DynamoConstants.LANDLINE_AMOUNT_FIELD) != null) {
				String landlineDynamo = dynamoResponse.get(DynamoConstants.LANDLINE_AMOUNT_FIELD).getS();
				Map<String, ?> landlineResponse = formatBillTypeDynamoData(landlineDynamo,
						RechargeConstants.LANDLINE_PRODUCT_TYPE, userEmailId);
				response.add(landlineResponse);
			}
			if (dynamoResponse.get(DynamoConstants.BROADBAND_AMOUNT_FIELD) != null) {
				String bbDynamo = dynamoResponse.get(DynamoConstants.BROADBAND_AMOUNT_FIELD).getS();
				Map<String, ?> broadbandRes = formatBillTypeDynamoData(bbDynamo,
						RechargeConstants.BROADBAND_PRODUCT_TYPE, userEmailId);
				response.add(broadbandRes);
			}
			if (dynamoResponse.get(DynamoConstants.METRO_AMOUNT_FIELD) != null) {
				String metroDynamo = dynamoResponse.get(DynamoConstants.METRO_AMOUNT_FIELD).getS();
				Map<String, ?> metroResponse = formatBillTypeDynamoData(metroDynamo,
						RechargeConstants.METRO_PRODUCT_TYPE, userEmailId);
				response.add(metroResponse);
			}
			if (dynamoResponse.get(DynamoConstants.WATER_AMOUNT_FIELD) != null) {
				String waterDynamo = dynamoResponse.get(DynamoConstants.WATER_AMOUNT_FIELD).getS();
				Map<String, ?> waterResponse = formatBillTypeDynamoData(waterDynamo,
						RechargeConstants.WATER_PRODUCT_TYPE, userEmailId);
				response.add(waterResponse);
			}
			if (dynamoResponse.get(DynamoConstants.GOOGLE_CREDITS_AMOUNT_FIELD) != null) {
				String googleCreditsDynamo = dynamoResponse.get(DynamoConstants.GOOGLE_CREDITS_AMOUNT_FIELD).getS();
				Map<String, ?> googleCreditsResponse = formatDynamoData(googleCreditsDynamo,
						RechargeConstants.GOOGLE_CREDITS_PRODUCT_TYPE, userEmailId);
				response.add(googleCreditsResponse);
			}
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/transactions/success/new/v2", method = RequestMethod.GET)
	public @ResponseBody Map<String, List<?>> getSuccesfulRechargesNew(@PathVariable String userEmailId,
			HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		return getRecentsAPIResponse(userEmailId);
		
	}

	private Map<String, List<?>> getRecentsAPIResponse(String userEmailId) {
		Map<String, List<?>> response = new HashMap<String, List<?>>();
		long dynamoStartTime = System.currentTimeMillis();
		Map<String, AttributeValue> dynamoResponse = sucRechargeService.getSuccessfulRechargesForEmailId(userEmailId,
				null);
		logger.info("Dynamo response time -  " + (System.currentTimeMillis() - dynamoStartTime)/1000.0);
		if (dynamoResponse != null) {
			/*
			  1. get all recharge objects
			  2. sort and set ranking
			  3. copy rank values when creating WebDos
			 */
			long sortStartTime = System.currentTimeMillis();
			List<RechargeDoType> globalRechargeList = new ArrayList<>();
			for(RechargeProductType rechargeType : RechargeProductType.values()) {
				if (dynamoResponse.get(rechargeType.getName()) != null) {
					if (rechargeType.isBillType()) {
						List<SuccessfulBillRechargeDo> billRechargeDoList = sucRechargeService.getBillRechargeObjFromJSON(dynamoResponse.get(rechargeType.getName()).getS());
						for (SuccessfulBillRechargeDo billRechargeDo : billRechargeDoList) {
							billRechargeDo.setRechargeProductType(rechargeType);
							globalRechargeList.add(billRechargeDo);
						}
					} else {
						List<SuccessfulRechargeDo> rechargeList = sucRechargeService.getRechargeObjFromJSON(dynamoResponse.get(rechargeType.getName()).getS());
						for (SuccessfulRechargeDo rechargeDo : rechargeList) {
							rechargeDo.setRechargeProductType(rechargeType);
							globalRechargeList.add(rechargeDo);
						}
					}
				}
			}
			//Adding filter of only active operators
			HashSet<Integer> activeOperatorsMasters = new HashSet<Integer>();
			Map<Integer, OperatorMaster> rechargeOperators = commonService.getOperatorMasters();
			List<BillerDetails> billersDetails = commonService.getBillPayOperatorMasterList(null);
			for (Map.Entry<Integer,OperatorMaster> entry : rechargeOperators.entrySet()){
				activeOperatorsMasters.add(entry.getValue().getOperatorMasterId());
			}

			for(BillerDetails billerDetail: billersDetails){
				activeOperatorsMasters.add(billerDetail.getOperatorMasterId());
			}

				// ranking logic goes here
			sortRecharges(globalRechargeList);
			logger.info("Sorting time -  " + (System.currentTimeMillis() - sortStartTime)/1000.0);
			// once sorted, bifurcate by Recharge Product Type add them to  response
			long categorizeTime = System.currentTimeMillis();
			Map<RechargeProductType, List<RechargeDoType>> rechargeMap = new HashMap<>();
			int rankIdx = 1;
			for(RechargeDoType rechargeDo : globalRechargeList){
				if(rankIdx >= fcProperties.getIntProperty(fcProperties.getMaxEntriesRechargeHistory())){
					// Only include top 20
					break;
				}
				RechargeProductType rechargeProductType = rechargeDo.getRechargeProductType();
				if(rechargeMap.get(rechargeProductType) == null){
					logger.debug("Recharge Product Type Found - " + rechargeProductType.getName());
					rechargeMap.put(rechargeProductType, new ArrayList<RechargeDoType>());
				}
				rechargeDo.setRank(rankIdx);
				logger.debug("Assiging rank " + rankIdx + "to " + rechargeDo.getRechargeProductType().getName() + " - " + rechargeDo.getFrequency() + " | " + rechargeDo.getTimestamp());
				rankIdx++;
				rechargeMap.get(rechargeProductType).add(rechargeDo);
			}
			logger.debug("Product based categorization time -  " + (System.currentTimeMillis() - categorizeTime)/1000.0);
			long structureTime = System.currentTimeMillis();
			for(RechargeProductType rechargeProductType : rechargeMap.keySet()){
				List<?> prepaidResponse = null;
				String productTypeCode = RechargeProductType.getProductTypeCode(rechargeProductType);
				if(rechargeProductType.isBillType()){
					List<SuccessfulBillRechargeDo> billRechargeDos = (List<SuccessfulBillRechargeDo>)(List<?>)rechargeMap.get(rechargeProductType);
					filterActiveBillOperators(billRechargeDos,activeOperatorsMasters);
					prepaidResponse = formatBillTypeDynamoDataNew(billRechargeDos, productTypeCode, userEmailId);
				}
				else{
					List<SuccessfulRechargeDo> rechargeDoList = (List<SuccessfulRechargeDo>)(List<?>)rechargeMap.get(rechargeProductType);
					filterActiveRechargeOperators(rechargeDoList,activeOperatorsMasters);
					prepaidResponse = formatDynamoDataNew(rechargeDoList, productTypeCode, userEmailId);
				}
				response.put(productTypeCode, prepaidResponse);

			}
			logger.debug("Structuring time -  " + (System.currentTimeMillis() - structureTime)/1000.0);

		}
		return response;
	}

	/**
	 * Ranking of recharges in history based on custom logic
	 * Sorting in decreasing order
	 * @param globalRechargeList
	 */
	private void sortRecharges(List<RechargeDoType> globalRechargeList) {
		Collections.sort(globalRechargeList, new Comparator<RechargeDoType>() {
			@Override
			public int compare(RechargeDoType obj1, RechargeDoType obj2) {
				if(obj1.getFrequency() == obj2.getFrequency()){
					if (obj1.getTimestamp() > obj2.getTimestamp())
						return -1;
					else if (obj1.getTimestamp() < obj2.getTimestamp())
						return 1;
					else
						return 0;
				}
				else
					return -(obj1.getFrequency() - obj2.getFrequency());
			}
		});
	}

	private List<SuccessfulBillRechargeWebDo> formatBillTypeDynamoDataNew(List<SuccessfulBillRechargeDo> rechargeDoList,
																		  String productType, String email) {
		return getSuccesfulBillRechargeWebDoList(rechargeDoList, productType, email);
	}

	/**
	 * Retrieves list of successfull recharge web Data objects
	 * @param rechargeList list of persistent successful bill recharge objects
	 * @param productType
	 * @param email
	 * @return
	 */
	private List<SuccessfulBillRechargeWebDo> getSuccesfulBillRechargeWebDoList(List<SuccessfulBillRechargeDo> rechargeList, String productType,
																				String email) {

		// Prepare the webDo to send. Also find the operator status for the
		// given operatorId and circleId;
		List<SuccessfulBillRechargeWebDo> suWebDos = new LinkedList<SuccessfulBillRechargeWebDo>();

		for (SuccessfulBillRechargeDo suDo : rechargeList) {
			SuccessfulBillRechargeWebDo newWebDo = new SuccessfulBillRechargeWebDo();
			if (suDo.getOperatorId() != 0) {
				//hack for IGL - temp fix - remove after one two months
				if(suDo.getOperatorId() == 403){
					suDo.setBillerType(3);
				}
				newWebDo.setBillRechargeDetails(suDo);
				suWebDos.add(newWebDo);
			} else {
				logger.info("operator/circle in recharge_history for " + email + " set as ,operatorId:"
						+ suDo.getOperatorId() + ",circleId" + suDo.getCircleId() + " ,serviceNumber:"
						+ suDo.getServiceNumber() + ",productType:"+productType);
				metricsClient.recordCount("RecentsAPI", "CorruptData." + productType, 1);
			}

		}
		return suWebDos;
	}

	private List<SuccessfulBillRechargeWebDo> formatBillTypeDynamoDataNew(String utilityProductDynamo,
			String productType, String email) {
		return succesfulBillRechargeList(utilityProductDynamo, productType, email);
	}

	private List<SuccessfulBillRechargeWebDo> succesfulBillRechargeList(String utilityProductDynamo, String productType,
			String email) {
		List<SuccessfulBillRechargeDo> rechargeList = null;
		rechargeList = sucRechargeService.getBillRechargeObjFromJSON(utilityProductDynamo);

		// Prepare the webDo to send. Also find the operator status for the
		// given operatorId and circleId;
		List<SuccessfulBillRechargeWebDo> suWebDos = new LinkedList<SuccessfulBillRechargeWebDo>();

		for (SuccessfulBillRechargeDo suDo : rechargeList) {
			SuccessfulBillRechargeWebDo newWebDo = new SuccessfulBillRechargeWebDo();
			if (suDo.getOperatorId() != 0) {
				newWebDo.setBillRechargeDetails(suDo);
				suWebDos.add(newWebDo);
			} else {
				logger.info("operator/circle in recharge_history for " + email + " set as ,operatorId:"
						+ suDo.getOperatorId() + ",circleId" + suDo.getCircleId() + " ,serviceNumber:"
						+ suDo.getServiceNumber() + ",productType:"+productType);
				metricsClient.recordCount("RecentsAPI", "CorruptData." + productType, 1);
			}

		}
		return suWebDos;
	}

	private List<SuccessfulRechargeWebDoNew> formatDynamoDataNew(List<SuccessfulRechargeDo> rechargeDoList, String productType,
																 String email) {
		return getSuccessfulRechargeWebDoList(rechargeDoList, productType, email);
	}

	/**
	 * Retrieves list of successfull recharge web Data objects
	 * @param rechargeList list of persistent successful recharge objects
	 * @param productType
	 * @param email
	 * @return
	 */
	private List<SuccessfulRechargeWebDoNew> getSuccessfulRechargeWebDoList(List<SuccessfulRechargeDo> rechargeList, String productType,
																			String email) {

		// Prepare the webDo to send. Also find the operator status for the
		// given operatorId and circleId;
		List<SuccessfulRechargeWebDoNew> suWebDos = new LinkedList<SuccessfulRechargeWebDoNew>();

		for (SuccessfulRechargeDo suDo : rechargeList) {
			SuccessfulRechargeWebDoNew newWebDo = new SuccessfulRechargeWebDoNew();
			if (suDo.getOperatorId() != 0 && suDo.getCircleId() != 0) {
				setPlanInfoIfExists(suDo);
				
				newWebDo.setRechargeDetails(suDo);
				suWebDos.add(newWebDo);
			} else {
				if (FCUtil.isMobileRecharge(productType)|| FCUtil.isDataCard(productType)) {
					if (FCUtil.isMobileRecharge(productType)) {
						if (productType.equals(RechargeConstants.POST_PAID_PRODUCT_TYPE) && suDo.getOperatorId() != 0) {
							suDo.setCircleId(CIRCLE_ID_ALL);
							newWebDo.setRechargeDetails(suDo);
							suWebDos.add(newWebDo);
						} else {
							Map<String, Object> prefixData = prefixService.getProductTypeData(suDo.getServiceNumber(),
									productType);
							if (!FCUtil.isEmpty(prefixData)) {
								addSanitizedData(suDo, newWebDo, suWebDos,
										Integer.valueOf(prefixData.get("operatorId").toString()),
										Integer.valueOf(prefixData.get("circleId").toString()), productType);
							}
						}

					}
					else if (FCUtil.isDataCard(productType)) {
						Map<String, Object> prefixData = prefixService.getProductTypeData(suDo.getServiceNumber(),
								productType);
						if (!FCUtil.isEmpty(prefixData)) {
							addSanitizedData(suDo, newWebDo, suWebDos,
									Integer.valueOf(prefixData.get("operatorId").toString()),
									Integer.valueOf(prefixData.get("circleId").toString()),productType);
						} else {
							String productId = Integer
									.toString(ProductName.fromPrimaryProductType(productType).getProductId());
							List<Map<String, Object>> mnpData = prefixService.getMNPOperator(suDo.getServiceNumber(),
									productId);
							if (!FCUtil.isEmpty(mnpData) && mnpData.get(0).get("operatorMasterId") != null
									&& mnpData.get(0).get("circleMasterId") != null) {
								addSanitizedData(suDo, newWebDo, suWebDos,
										Integer.valueOf(mnpData.get(0).get("operatorMasterId").toString()),
										Integer.valueOf(mnpData.get(0).get("circleMasterId").toString()),productType);
							}
						}
					}
				}
				logger.info("operator/circle in recharge_history for " + email + " set as ,operatorId:"
						+ suDo.getOperatorId() + ",circleId" + suDo.getCircleId() + " ,serviceNumber:"
						+ suDo.getServiceNumber() + ",productType:"+productType);
				if(suDo.getOperatorId()==0 && suDo.getCircleId() ==0){
					metricsClient.recordCount("RecentsAPI", "CorruptData." + productType, 1);
				}
				metricsClient.recordCount("RecentsAPI", "CorruptDataInDynamo." + productType, 1);
			}
		}
		return suWebDos;
	}


	private List<SuccessfulRechargeWebDoNew> formatDynamoDataNew(String productDynamo, String productType,
			String email) {
		return successfulRechargeList(productDynamo, productType, email);
	}

	private List<SuccessfulRechargeWebDoNew> successfulRechargeList(String productDynamo, String productType,
			String email) {
		List<SuccessfulRechargeDo> rechargeList = null;
		rechargeList = sucRechargeService.getRechargeObjFromJSON(productDynamo);

		// Prepare the webDo to send. Also find the operator status for the
		// given operatorId and circleId;
		List<SuccessfulRechargeWebDoNew> suWebDos = new LinkedList<SuccessfulRechargeWebDoNew>();

		for (SuccessfulRechargeDo suDo : rechargeList) {
			SuccessfulRechargeWebDoNew newWebDo = new SuccessfulRechargeWebDoNew();
			if (suDo.getOperatorId() != 0 && suDo.getCircleId() != 0) {
				setPlanInfoIfExists(suDo);
				newWebDo.setRechargeDetails(suDo);
				suWebDos.add(newWebDo);
			} else {
				if (FCUtil.isMobileRecharge(productType)|| FCUtil.isDataCard(productType)) {
					if (FCUtil.isMobileRecharge(productType)) {
						if (productType.equals(RechargeConstants.POST_PAID_PRODUCT_TYPE) && suDo.getOperatorId() != 0) {
							suDo.setCircleId(CIRCLE_ID_ALL);
							newWebDo.setRechargeDetails(suDo);
							suWebDos.add(newWebDo);
						} else {
							Map<String, Object> prefixData = prefixService.getProductTypeData(suDo.getServiceNumber(),
									productType);
							if (!FCUtil.isEmpty(prefixData)) {
								addSanitizedData(suDo, newWebDo, suWebDos,
										Integer.valueOf(prefixData.get("operatorId").toString()),
										Integer.valueOf(prefixData.get("circleId").toString()), productType);
							}
						}

					}
					else if (FCUtil.isDataCard(productType)) {
						Map<String, Object> prefixData = prefixService.getProductTypeData(suDo.getServiceNumber(),
								productType);
						if (!FCUtil.isEmpty(prefixData)) {
							addSanitizedData(suDo, newWebDo, suWebDos,
									Integer.valueOf(prefixData.get("operatorId").toString()),
									Integer.valueOf(prefixData.get("circleId").toString()),productType);
						} else {
							String productId = Integer
									.toString(ProductName.fromPrimaryProductType(productType).getProductId());
							List<Map<String, Object>> mnpData = prefixService.getMNPOperator(suDo.getServiceNumber(),
									productId);
							if (!FCUtil.isEmpty(mnpData) && mnpData.get(0).get("operatorMasterId") != null
									&& mnpData.get(0).get("circleMasterId") != null) {
								addSanitizedData(suDo, newWebDo, suWebDos,
										Integer.valueOf(mnpData.get(0).get("operatorMasterId").toString()),
										Integer.valueOf(mnpData.get(0).get("circleMasterId").toString()),productType);
							}
						}
					}
				}
				logger.info("operator/circle in recharge_history for " + email + " set as ,operatorId:"
						+ suDo.getOperatorId() + ",circleId" + suDo.getCircleId() + " ,serviceNumber:"
						+ suDo.getServiceNumber() + ",productType:"+productType);
				if(suDo.getOperatorId()==0 && suDo.getCircleId() ==0){
					metricsClient.recordCount("RecentsAPI", "CorruptData." + productType, 1);
				}
				metricsClient.recordCount("RecentsAPI", "CorruptDataInDynamo." + productType, 1);
			}
		}
		return suWebDos;
	}

	private void setPlanInfoIfExists(SuccessfulRechargeDo suDo) {
		try {
			List<Double> amountList = new ArrayList<>();
			Map<Double, List<String>> amountVsDenomListMap = new HashMap<>();
			if (suDo.getDenomList() != null && suDo.getDenomList().size() > 0) {
				for (Object denomList : suDo.getDenomList()) {
					if (denomList != null) {
						List<String> amountPlanTypeList = (List) denomList;
						if (!FCUtil.isEmpty(amountPlanTypeList)) {
							String amount = amountPlanTypeList.get(0);
							if (!FCUtil.isEmpty(amount) && FCUtil.isDouble(amount)) {
								Double damount = Double.parseDouble(amount);
								amountList.add(damount);
								amountVsDenomListMap.put(damount, amountPlanTypeList);
							} else if (!FCUtil.isEmpty(amountPlanTypeList) && amountPlanTypeList.size() > 1) {
								amount = amountPlanTypeList.get(1);
								if (!FCUtil.isEmpty(amount) && FCUtil.isDouble(amount)) {
									Double damount = Double.parseDouble(amount);
									amountList.add(damount);
									amountVsDenomListMap.put(damount, amountPlanTypeList);
								}
							}
						}
					}
				}
				HashMap<Double, LinkedHashMap> rechargePlanInfoDoMap = getRechargePlanInfo(suDo.getOperatorId(), suDo.getCircleId(),
						suDo.getServiceNumber(), amountList);
				if (rechargePlanInfoDoMap != null && rechargePlanInfoDoMap.size() > 0) {
					logger.info("Result from recharge plan info api " + rechargePlanInfoDoMap.toString());
					for (Map.Entry<Double, LinkedHashMap> entry : rechargePlanInfoDoMap.entrySet()) {
						List<String> amountPlanTypeList = amountVsDenomListMap.get(Double.parseDouble(String.valueOf(entry.getKey())));
						logger.info("Amount type plan list " + amountPlanTypeList + " " + entry.getKey());
						if (amountPlanTypeList != null && entry != null) {
							LinkedHashMap<String, Object> rechargePlanCachedInfo = entry.getValue();
							logger.info("Recharge plan info sub cat " + rechargePlanCachedInfo.get("subCategory"));
							amountPlanTypeList.removeAll(amountPlanTypeList);
							amountPlanTypeList.add(entry.getKey() + "");
							amountPlanTypeList.add(String.valueOf(rechargePlanCachedInfo.get("subCategory")));
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error setting subcategory for recharge data ", e);
		}
	}

	private HashMap<Double, LinkedHashMap> getRechargePlanInfo(int operatorId, int circleId, String serviceNumber, List<Double> damount) {
		String url = fcProperties.getProperty(FCConstants.NEW_PLANS_GET_CATEGORY_API)
				+ operatorId + "/" + circleId + "/" + StringUtils.join(damount, ",");
		GetMethod getMethod = new GetMethod(url);
		try {
			rechargePlanHttpClient.executeMethod(getMethod);
			String responseBody = "";
			try(BufferedReader reader = new BufferedReader(new InputStreamReader(getMethod.getResponseBodyAsStream()))) {
				String line;
				while ((line = reader.readLine()) != null){
					responseBody+=line;
				}
			}
			if (responseBody != null && responseBody.length() > 0) {
				HashMap<Double, LinkedHashMap> rechargePlanInfoDo;
				ObjectMapper mapper = new ObjectMapper();
				rechargePlanInfoDo = mapper.readValue(responseBody, HashMap.class);
				return rechargePlanInfoDo;
			}
			return null;
		} catch (Exception e) {
			logger.error("Exception getting new plans category for "+ operatorId + " " + circleId + " " + serviceNumber
					+ " Url " + url, e);
		}
		return null;
	}

	private void addSanitizedData(SuccessfulRechargeDo suDo, SuccessfulRechargeWebDoNew newWebDo,
			List<SuccessfulRechargeWebDoNew> suWebDos, Integer operatorId, Integer circleId,String productType) {
		suDo.setOperatorId(operatorId);
		suDo.setCircleId(circleId);
		setPlanInfoIfExists(suDo);
		newWebDo.setRechargeDetails(suDo);
		suWebDos.add(newWebDo);
		metricsClient.recordCount("RecentsAPI", "SanitizedData." + productType, 1);
	}

	private Map<String, ?> formatBillTypeDynamoData(String utilityProductDynamo, String productType, String email) {
		Map<String, Object> returnData = new HashMap<String, Object>();
		List<SuccessfulBillRechargeWebDo> suWebDos = succesfulBillRechargeList(utilityProductDynamo, productType,
				email);
		returnData.put(productType, suWebDos);
		return returnData;
	}

	private Map<String, ?> formatDynamoData(String productDynamo, String productType, String email) {
		Map<String, Object> returnData = new HashMap<String, Object>();
		List<SuccessfulRechargeWebDoNew> suWebDos = successfulRechargeList(productDynamo, productType, email);
		returnData.put(productType, suWebDos);
		return returnData;

	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/{productType}/recharges/success", method = RequestMethod.GET)
	public @ResponseBody List<RechargeDetails> getUserSuccesfulRechargesByProductType(@PathVariable String userEmailId,
			@PathVariable String productType, HttpServletRequest request) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);

		List<RechargeDetails> successfullRechargeDetails = successfulRechargeService
				.getSuccessfulUserRechargesByUserEmail(userEmailId, productType, request);
		return successfullRechargeDetails;
	}

	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/transactions", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getTransactions(@PathVariable String userEmailId, ModelMap model)
			throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		int userId = getUserId(userEmailId);
		SimpleDateFormat readDateFormat = new SimpleDateFormat("d MMM yy hh:mm a");
		Map<String, Object> lastEntry = walletService.findLastEntry(userId);
		List<Map<String, Object>> rows = walletService.getBalanceRows(userId, lastEntry, true);

		if (rows != null) {
			for (Map<String, Object> listmap : rows) {
				for (Map.Entry<String, Object> entry : listmap.entrySet()) {
					if (entry.getKey().equalsIgnoreCase("created_ts")) {
						try {
							String formattedDateString = writeDateFormat
									.format(readDateFormat.parse((String) entry.getValue()));
							entry.setValue(formattedDateString);
						} catch (Exception e) {
							logger.error("Error parsing date " + entry.getValue(), e);
						}
					}
				}
			}
		}

		return rows;
	}

	private int getUserId(String userEmailId) throws InvalidPathValueException {
		int userId = userIdHelper.getUserIdFromEmail(userEmailId);
		return userId;
	}

	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{partialEmailId}/checkpartial", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> checkUserByPartialEmailId(@PathVariable String partialEmailId,
			ModelMap model) {
		List<Map<String, Object>> userInfoList = new ArrayList<Map<String, Object>>();
		if (partialEmailId.length() >= MINIMUM_PARTIAL_EMAILID_LENGTH) {
			List<Users> users = userService.getUserByPartialEmailId(partialEmailId);
			if (users != null) {
				for (Users userObject : users) {
					Map<String, Object> userMap = new HashMap<String, Object>();
					userMap.put("email", userObject.getEmail());
					userMap.put("isExists", true);
					userMap.put("isActive", userObject.getIsActive());
					userInfoList.add(userMap);
				}
			} else {
				Map<String, Object> userMap = new HashMap<String, Object>();
				userMap.put("email", partialEmailId);
				userMap.put("isExists", false);
				userMap.put("isActive", false);
				userInfoList.add(userMap);
			}
		} else {
			logger.info("Given ParitalEmailId=" + partialEmailId + "is less than" + MINIMUM_PARTIAL_EMAILID_LENGTH
					+ "returning empty list");
		}
		return userInfoList;
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/balance", method = RequestMethod.GET)
	public @ResponseBody Amount getBalance(@PathVariable String userEmailId) throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		// This is to ensure that loggedin user email is fetched from session,
		// hence
		// handling case sensitive issues
		userEmailId = FCSessionUtil.getLoggedInEmailId();
		int userId = getUserId(userEmailId);
		Amount balance = walletService.findBalance(userId);
		return balance;
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/profile", method = RequestMethod.GET)
	public @ResponseBody ProfileWebDO getProfile(@PathVariable String userEmailId, ModelMap model)
			throws InvalidPathValueException {
		validateUserSessionEmail(userEmailId);
		return getUserProfile(userEmailId);
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/user/cityName", method = RequestMethod.GET)
	public @ResponseBody String getCity(ModelMap model) throws InvalidPathValueException {
		ProfileWebDO profile = getUserProfile(FCSessionUtil.getLoggedInEmailId());
		String cityName = getCityName(profile);
		return cityName;
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/user/{email}/ocstatus")
	public @ResponseBody Map<String, String> getMigrationStatus(@PathVariable String email, HttpServletRequest request) {
		Integer userId = userIdHelper.getUserIdFromEmail(email);
		Users loggedInUser = userServiceProxy.getLoggedInUser();

		if (!StringUtils.equalsIgnoreCase(loggedInUser.getEmail(), email)) {
			logger.error("Unauthorised user. Logged in user = " + loggedInUser.getEmail() + ". Input email = " + email);
			return ErrorCode.EMAIL_NOT_PRESENT.restResponse();
		}

		String walletToken = FCSessionUtil.getAppCookie(request);
		MigrationStatus migrationStatus = null;
		try {
			migrationStatus = userServiceProxy.getUserMigrationStatusByEmail(email, walletToken);
		} catch (Exception e) {
			logger.error("Caught exception ", e);
			return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
		}
		Amount balance = Amount.ZERO;

		Map<String, String> responseMap = new HashMap<>();
		responseMap.put("migrationStatus", migrationStatus.getMigrationStatus());
		responseMap.put("balance", balance.getAmount().toString());
		responseMap.put("walletToken", walletToken);

		return responseMap;
	}

	private String getCityName(ProfileWebDO profile) {
		if (profile == null || profile.getCitylist() == null) {
			logger.error("Could not get city for profile.");
			return null;
		}
		String cityId = profile.getCity();
		if (cityId == null) {
			logger.error("City Id is null. Could not get city for profile.");
			return null;
		}
		List<CityMaster> cities = profile.getCitylist();
		for (CityMaster city : cities) {
			if (cityId.equals(String.valueOf(city.getCityMasterId()))) {
				return city.getCityName();
			}
		}
		return null;
	}

	@NoSessionWrite
	@RequestMapping(value = "/protected/rest/users/{userEmailId}/profile", method = RequestMethod.GET)
	public @ResponseBody ProfileWebDO getProtectedProfile(@PathVariable String userEmailId, ModelMap model)
			throws InvalidPathValueException {
		return getUserProfile(userEmailId);
	}

	private ProfileWebDO getUserProfile(String userEmailId) throws InvalidPathValueException {
		int userId = getUserId(userEmailId);
		ProfileBusinessDO profilebusinessdo = new ProfileBusinessDO();
		profilebusinessdo.setUserid(userId);
		myAccountService.getProfileByUserId(profilebusinessdo);
		MyProfileDOMapper doMapper = new MyProfileDOMapper();
		ProfileWebDO profileWebDO = doMapper.convertBusinessDOtoWebDO(profilebusinessdo, null);
		profileWebDO.sethUserid(MD5.hash(Integer.toString(userId)));

		String userName = profileWebDO.getName();
		if (!PaymentUtil.isWelFormedName(userName))
			profileWebDO.setName("");
		return profileWebDO;
	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/coupons", method = RequestMethod.GET)
	public @ResponseBody List<CouponHistoryView> getCoupons(@PathVariable String userEmailId,
			@RequestParam(required = false) String afterDate, ModelMap model)
					throws InvalidPathValueException, InvalidParameterException {
		
		long startCouponServiceTime = System.currentTimeMillis();
		RestSessionHelper.validateUserSessionEmail(userEmailId);
		List<CouponHistoryView> couponsView = new ArrayList<>();
		List<CouponHistoryMaster> couponHistoryMasterList = null;

		Users user = userServiceProxy.getUserByEmailId(userEmailId);
		if (user != null && user.getUserId() != null) {
			long startCouponServiceCallTime = System.currentTimeMillis();
			
			couponHistoryMasterList=couponServiceProxy.getCouponHistoryMasterListByUserId(user.getUserId());
			
			metricsClient.recordLatency("CouponServiceCall", "getCouponHistoryMasterListByUserId", System.currentTimeMillis() - startCouponServiceCallTime);
			if (couponHistoryMasterList != null) {
				for (CouponHistoryMaster couponHistoryMasterObject : couponHistoryMasterList) {
					try {
						logger.info("Entering into mapCouponHistoryMaster..");
						CouponHistoryView couponHistoryView = couponHistoryViewMapper
								.mapCouponHistoryMaster(couponHistoryMasterObject);
						String validityDate = couponHistoryView.getValidityDate();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String formattedValidityDate = writeDateFormat.format(sdf.parse(validityDate));
						couponHistoryView.setValidityDate(formattedValidityDate);
						couponsView.add(couponHistoryView);
					} catch (ParseException p) {
						logger.error("Exception while parsing " + couponHistoryMasterObject.getCouponHistory().
								getValidityDate(), p);
					} catch (Exception e) {
						logger.error("Exception while mapping for UserId="+couponHistoryMasterObject.getCouponHistory().getUserId(), e);
					}
				}

			}
		}
		metricsClient.recordLatency("CouponServiceCall", "getCouponHistoryMasterListByUserId", 
				System.currentTimeMillis() - startCouponServiceTime);
		
		return couponsView;
	}

	/**
	 * Used by, ANDROIDAPP
	 * 
	 */
	@NoSessionWrite
	@RequestMapping(value = "/rest/users/{userEmailId}/coupons/lite", method = RequestMethod.GET)
	public @ResponseBody List<CouponHistoryViewLite> getCouponsLite(@PathVariable String userEmailId,
			@RequestParam(required = false) String afterDate, ModelMap model)
					throws InvalidPathValueException, InvalidParameterException {
		RestSessionHelper.validateUserSessionEmail(userEmailId);
		List<CouponHistory> coupons = null;
		List<CouponHistoryViewLite> couponsView = new ArrayList<>();
		Users user = userServiceProxy.getUserByEmailId(userEmailId);
		if (user != null && user.getUserId() != null) {
			coupons = couponServiceProxy.getCouponHistoryListByUserId(user.getUserId());

			if (coupons != null) {
				SimpleDateFormat readDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
				Map<Integer, List<CouponImage>> couponImages = couponServiceProxy
						.getCouponImagesInMemAppConfigService();
				for (CouponHistory couponHistoryObject : coupons) {
					try {
						CouponHistoryViewLite couponHistoryView = couponHistoryViewMapper
								.mapCouponHistoryLite(couponHistoryObject, couponImages);
						String validityDate = couponHistoryView.getValidityDate();
						if (validityDate != null) {
							String formattedValidityDate = dateFormatter.format(readDateFormatter.parse(validityDate));
							couponHistoryView.setValidityDate(formattedValidityDate);
						}
						couponsView.add(couponHistoryView);
					} catch (Exception e) {
						logger.error("Error parsing date " + couponHistoryObject.getValidityDate(), e);
					}
				}

			}
		}
		return couponsView;
	}

	/**
	 * Used by, ANDROIDAPP
	 * 
	 * @throws InvalidParameterException
	 */
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "/rest/user/coupon-tnc", method = RequestMethod.GET)
	public @ResponseBody CouponHistoryTnC getUserCouponTnC(@RequestParam String id, HttpServletRequest httpRequest,
			HttpServletResponse response, ModelMap model) throws InvalidPathValueException, InvalidParameterException {
		if (id == null || id.isEmpty()) {
			return null;
		}
		CouponHistoryTnC couponTnC = null;
		if (FCUtil.isInteger(id)) {
			couponTnC = new CouponHistoryTnC();
			int couponHistoryId = Integer.parseInt(id);
			CouponHistory couponHistory = couponServiceProxy.getCouponHistoryById(couponHistoryId);
			if (couponHistory != null) {
				String tnc = couponHistory.getTermsAndConditions();
				if (tnc != null) {
					tnc = tnc.replaceAll("<br>", "\r\n");
					tnc = tnc.replaceAll("<br/>", "\r\n");
					tnc = tnc.replaceAll("<b>", "");
					tnc = tnc.replaceAll("</b>", "");
					tnc = tnc.replaceAll("<BR>", "\r\n");
					tnc = tnc.replaceAll("<BR/>", "\r\n");
					tnc = tnc.replaceAll("<B>", "");
					tnc = tnc.replaceAll("</B>", "");
				}
				couponTnC.setTnc(tnc);
				couponTnC.setCityNames(couponHistory.getCityNameList());
			}
		}
		return couponTnC;
	}

	private String validateUserEmailId(String userEmailId) throws InvalidPathValueException {
		if (!MobileUtil.isEmailId(userEmailId)) {
			throw new InvalidPathValueException("/users/{userEmailId}/cards", userEmailId, RestConstants.TYPE_EMAIL);
		}
		return userEmailId;
	}

	private boolean validateUserSessionEmail(String userEmailId) throws InvalidPathValueException {
		boolean isValid = false;
		if (userEmailId != null && userEmailId.equalsIgnoreCase(FCSessionUtil.getLoggedInEmailId())) {
			isValid = true;
		}
		if (!isValid) {
			throw new InvalidPathValueException("/users/{userEmailId}/cards", userEmailId,
					FCSessionUtil.getLoggedInEmailId());
		}
		return isValid;
	}

	private List<SuccessfulRechargeDo> filterActiveRechargeOperators(List<SuccessfulRechargeDo> recentRecharges, HashSet<Integer> acitveOperators){
		ListIterator<SuccessfulRechargeDo> iter = recentRecharges.listIterator();
		while(iter.hasNext()){
			if(!acitveOperators.contains(iter.next().getOperatorId())){
				iter.remove();
			}
		}
		return recentRecharges;
	}

	private List<SuccessfulBillRechargeDo> filterActiveBillOperators(List<SuccessfulBillRechargeDo> recentBillRecharges, HashSet<Integer> acitveOperators){
		ListIterator<SuccessfulBillRechargeDo> iter = recentBillRecharges.listIterator();
		while(iter.hasNext()){
			if(!acitveOperators.contains(iter.next().getOperatorId())){
				iter.remove();
			}
		}
		return recentBillRecharges;
	}

	/**
	 *  To overcome the problem related to security Post version of the methods has been released.
	 *  In  get  version of methods the user's emaild comes as path variable which is a security breach.
	 *  Added by Aman (changes to remove the user email id from the url) start
	 * **/

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/v2/recharges", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody List<Map<String, String>> getUserRechargesUsingPostParam(@RequestBody String userEmailId, ModelMap model,
																				  HttpServletRequest request) throws InvalidPathValueException {
		log.info("Inside the getUserRechargesUsingPostParam:"+userEmailId);
		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);
			throw new InvalidPathValueException("/rest/users/recharges",emailId,FCSessionUtil.getLoggedInEmailId());
		}

		validateUserSessionEmail(emailId);
		int userId = getUserId(emailId);
		MyRechargesBusinessDO bdo = new MyRechargesBusinessDO();
		bdo.setUserid(userId);
		if (request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY) != null
				&& !request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY).isEmpty()) {
			bdo.setChannel(request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY));
		}
		bdo.setAppVersion(MobileUtil.getMobileAppVersionFromRequest(request));
		if (request.getParameter("allTxns") != null && !request.getParameter("allTxns").isEmpty()
				&& Boolean.parseBoolean(request.getParameter("allTxns"))) {
			bdo.setReturnPaymentTxns(true);
		}
		myAccountService.getUserTransactionHistory(bdo);
		return bdo.getRechargeHistoryList();
	}


	@NoSessionWrite
	@RequestMapping(value = "/rest/users/rechargedetails", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody List<RechargeDetails> getUserRechargeDetailsUsingPost(@RequestBody String userEmailId,
																	  HttpServletRequest request) throws InvalidPathValueException {
		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);
			throw new InvalidPathValueException("/rest/users/rechargedetails",emailId,FCSessionUtil.getLoggedInEmailId());
		}
		validateUserSessionEmail(emailId);
		List<RechargeDetails> allRechargeDetails = successfulRechargeService.getAllUserRecharges(emailId, request);
		return allRechargeDetails;
	}



	@NoSessionWrite
	@RequestMapping(value = "/rest/users/recharges/success", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody List<RechargeDetails> getUserSuccesfulRechargesUsingPost(@RequestBody String userEmailId,
																		 HttpServletRequest request) throws InvalidPathValueException {
		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);
			throw new InvalidPathValueException("/rest/users/transactions/success/",emailId,FCSessionUtil.getLoggedInEmailId());
		}

		validateUserSessionEmail(emailId);
		List<RechargeDetails> successfullRechargeDetails = successfulRechargeService
				.getSuccessfulUserRecharges(emailId, request);

		return successfullRechargeDetails;
	}

	@SuppressWarnings("unchecked")
	@NoSessionWrite
	@RequestMapping(value = "/rest/users/transactions/success/new/v2", method = RequestMethod.POST,produces="application/json")
	public @ResponseBody Map<String, List<?>> getSuccesfulRechargesNewUsingPost(@RequestBody String userEmailId,
																	   HttpServletRequest request) throws InvalidPathValueException {
		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);
			throw new InvalidPathValueException("/rest/users/transactions/success/new/v2",emailId,FCSessionUtil.getLoggedInEmailId());
		}
		validateUserSessionEmail(emailId);
		return getRecentsAPIResponse(emailId);

	}

	@NoSessionWrite
	@RequestMapping(value = "/rest/users/balance", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody Amount getBalanceUsingPost(@RequestBody String userEmailId) throws InvalidPathValueException {

		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);
			throw new InvalidPathValueException("/rest/users/balance",emailId,FCSessionUtil.getLoggedInEmailId());
		}
		validateUserSessionEmail(emailId);
		// This is to ensure that loggedin user email is fetched from session,
		// hence
		// handling case sensitive issues
		userEmailId = FCSessionUtil.getLoggedInEmailId();
		int userId = getUserId(userEmailId);
		Amount balance = walletService.findBalance(userId);
		return balance;
	}


	@NoSessionWrite
	@RequestMapping(value = "/rest/user/ocstatus",method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody Map<String, String> getMigrationStatusUsingPost(@RequestBody String email, HttpServletRequest request) {

		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(email, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);

		}

		Integer userId = userIdHelper.getUserIdFromEmail(emailId);
		Users loggedInUser = userServiceProxy.getLoggedInUser();

		if (!StringUtils.equalsIgnoreCase(loggedInUser.getEmail(), emailId)) {
			logger.error("Unauthorised user. Logged in user = " + loggedInUser.getEmail() + ". Input email = " + emailId);
			return ErrorCode.EMAIL_NOT_PRESENT.restResponse();
		}

		String walletToken = FCSessionUtil.getAppCookie(request);
		MigrationStatus migrationStatus = null;
		try {
			migrationStatus = userServiceProxy.getUserMigrationStatusByEmail(emailId, walletToken);
		} catch (Exception e) {
			logger.error("Caught exception ", e);
			return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
		}
		Amount balance = walletService.findBalance(userId);

		Map<String, String> responseMap = new HashMap<>();
		responseMap.put("migrationStatus", migrationStatus.getMigrationStatus());
		responseMap.put("balance", balance.getAmount().toString());
		responseMap.put("walletToken", walletToken);

		return responseMap;
	}


	@NoSessionWrite
	@RequestMapping(value = "/rest/users/coupons", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody List<CouponHistoryView> getCouponsUsingPost(@RequestBody String userEmailId,
															@RequestParam(required = false) String afterDate, ModelMap model)
			throws InvalidPathValueException, InvalidParameterException {

		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);

		}

		long startCouponServiceTime = System.currentTimeMillis();
		RestSessionHelper.validateUserSessionEmail(emailId);
		List<CouponHistoryView> couponsView = new ArrayList<>();
		List<CouponHistoryMaster> couponHistoryMasterList = null;

		Users user = userServiceProxy.getUserByEmailId(emailId);
		if (user != null && user.getUserId() != null) {
			long startCouponServiceCallTime = System.currentTimeMillis();

			couponHistoryMasterList=couponServiceProxy.getCouponHistoryMasterListByUserId(user.getUserId());

			metricsClient.recordLatency("CouponServiceCall", "getCouponHistoryMasterListByUserId", System.currentTimeMillis() - startCouponServiceCallTime);
			if (couponHistoryMasterList != null) {
				for (CouponHistoryMaster couponHistoryMasterObject : couponHistoryMasterList) {
					try {
						logger.info("Entering into mapCouponHistoryMaster..");
						CouponHistoryView couponHistoryView = couponHistoryViewMapper
								.mapCouponHistoryMaster(couponHistoryMasterObject);
						String validityDate = couponHistoryView.getValidityDate();
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String formattedValidityDate = writeDateFormat.format(sdf.parse(validityDate));
						couponHistoryView.setValidityDate(formattedValidityDate);
						couponsView.add(couponHistoryView);
					} catch (ParseException p) {
						logger.error("Exception while parsing " + couponHistoryMasterObject.getCouponHistory().
								getValidityDate(), p);
					} catch (Exception e) {
						logger.error("Exception while mapping for UserId="+couponHistoryMasterObject.getCouponHistory().getUserId(), e);
					}
				}

			}
		}
		metricsClient.recordLatency("CouponServiceCall", "getCouponHistoryMasterListByUserId",
				System.currentTimeMillis() - startCouponServiceTime);

		return couponsView;
	}



	@NoSessionWrite
	@RequestMapping(value = "/rest/users/coupons/lite", method = RequestMethod.POST,produces = "application/json")
	public @ResponseBody List<CouponHistoryViewLite> getCouponsLiteUsingPost(@RequestBody String userEmailId,
																	@RequestParam(required = false) String afterDate, ModelMap model)
			throws InvalidPathValueException, InvalidParameterException {

		String emailId = null;
		ObjectMapper mapper =null;
		try{
			mapper = new ObjectMapper();
			HashMap requestData= mapper.readValue(userEmailId, HashMap.class);
			emailId = (String)requestData.get("email");
		}catch (IOException| NullPointerException e) {
			logger.error("Got exception parsing the email in the request:", e);

		}

		RestSessionHelper.validateUserSessionEmail(emailId);
		List<CouponHistory> coupons = null;
		List<CouponHistoryViewLite> couponsView = new ArrayList<>();
		Users user = userServiceProxy.getUserByEmailId(emailId);
		if (user != null && user.getUserId() != null) {
			coupons = couponServiceProxy.getCouponHistoryListByUserId(user.getUserId());

			if (coupons != null) {
				SimpleDateFormat readDateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
				Map<Integer, List<CouponImage>> couponImages = couponServiceProxy
						.getCouponImagesInMemAppConfigService();
				for (CouponHistory couponHistoryObject : coupons) {
					try {
						CouponHistoryViewLite couponHistoryView = couponHistoryViewMapper
								.mapCouponHistoryLite(couponHistoryObject, couponImages);
						String validityDate = couponHistoryView.getValidityDate();
						if (validityDate != null) {
							String formattedValidityDate = dateFormatter.format(readDateFormatter.parse(validityDate));
							couponHistoryView.setValidityDate(formattedValidityDate);
						}
						couponsView.add(couponHistoryView);
					} catch (Exception e) {
						logger.error("Error parsing date " + couponHistoryObject.getValidityDate(), e);
					}
				}

			}
		}
		return couponsView;
	}

	/**
	 * Added by Aman (changes to remove the user email id from the url) end
	 * **/
}
