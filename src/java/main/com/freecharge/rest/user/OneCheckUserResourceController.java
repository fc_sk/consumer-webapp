package com.freecharge.rest.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.service.OneCheckUserService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/fcwallet/v1/*")
public class OneCheckUserResourceController {
    @Autowired
    OneCheckWalletService oneCheckWalletService;

    @Autowired
    OneCheckUserService   oneCheckUserService;

    @NoSessionWrite
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "user/transactiondetails", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<OneCheckTransactionSummary> getTxnDetails(
            FCWalletTransactionHistoryRequest historyRequest, HttpServletRequest request) throws Exception {
        List<OneCheckTransactionSummary> transactionSummaries = oneCheckUserService.findTransactions(historyRequest,
                request);
        return transactionSummaries;
    }

    @NoSessionWrite
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "user/vouchers", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<VoucherDetails> getVouchersForUser(HttpServletRequest request) throws Exception {
        return oneCheckUserService.getUsersVouchers(request);
    }
    
    @NoSessionWrite
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "user/expiredvouchers", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<VoucherDetails> getExpiredVouchersForUser(HttpServletRequest request) throws Exception {
        return oneCheckUserService.getUsersExpiredVouchers(request);
    }
    
    @NoSessionWrite
    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "user/vouchertransactions", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<OneCheckTransactionSummary> getVoucherTransactionsForUser(HttpServletRequest request) throws Exception {
        return oneCheckUserService.getVoucherTransactionsForUser(request);
    }
}
