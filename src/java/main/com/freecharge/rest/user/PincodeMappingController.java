package com.freecharge.rest.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.PincodeMapping;
import com.freecharge.app.service.PincodeMappingService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller("/")
public class PincodeMappingController {

	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private PincodeMappingService pincodeMappingService;

	@NoSessionWrite
	@NoLogin
	@Csrf(exclude = true)
	@RequestMapping(value = "/pincode/mapping/{pin}", method = RequestMethod.GET)
	@ResponseBody
	public String getPincodeMapping(@PathVariable("pin") String pin, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("Request received for pincode " + pin);
		List<PincodeMapping> pincodeMapping = pincodeMappingService.fetchMappingForPincode(pin);
		try {
			if (pincodeMapping == null) {
				logger.info("Returing empty response for pincode " + pin);
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "Pincode mapping details not present");
				return "";
			}
			if (pincodeMapping != null) {
				JSONArray listOfPincodeMapping = new JSONArray();
				for (PincodeMapping mapping : pincodeMapping) {
					JSONObject json = (JSONObject) new JSONParser().parse(FCUtil.serialize(mapping));
					listOfPincodeMapping.add(json);
				}
				JSONObject obj = new JSONObject();
				obj.put("pincode", pin);
				obj.put("pincodeMapping", listOfPincodeMapping);
				logger.info("Returned response for pincode " + pin + listOfPincodeMapping.toString());
				return obj.toString();
			}
		} catch (Exception e) {
			logger.error("While fetching pinocde mapping", e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Not able to fetch pincode mapping details. Please try later");
		}
		return "";
	}

	public static void main(String[] ars) throws Exception {
		List<PincodeMapping> pincodeMapping = new ArrayList<PincodeMapping>();
		PincodeMapping pincodeMappin = new PincodeMapping();
		Integer pincode = 12345;
		pincodeMappin.setCity("Bangalore");
		pincodeMappin.setPincode(12345);
		pincodeMappin.setState("Karnataka");
		pincodeMapping.add(pincodeMappin);
		JSONArray listOfPincodeMapping = new JSONArray();
		for (PincodeMapping mapping : pincodeMapping) {
			JSONObject json = (JSONObject) new JSONParser().parse(FCUtil.serialize(mapping));
			listOfPincodeMapping.add(json);
		}
		JSONObject obj = new JSONObject();
		obj.put("pincode", pincode);
		obj.put("pincodeMapping", listOfPincodeMapping);
		System.out.println(obj.toString());
	}

}
