package com.freecharge.rest.user.bean;

import java.io.Serializable;
import java.sql.Timestamp;

import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.util.Amount;

/**
 * This bean is used to capture details such as mobile number, amount and so on
 * for a failure recharge. Note that this bean does not contain back end
 * details such as aggregator, retries, etc.,
 * 
 * @author arun
 * 
 */
public class FailureRechargeInfo implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String operatorCode;
    private String circleCode;
    private String mobileNo;
    private Amount amount;
    private Timestamp rechargeTime;
    private String orderID;
    private String planType;

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public Timestamp getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(Timestamp rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }
    
    public String getPlanType() {
           return planType;
    }

    public void setPlanType(String planType) {
           this.planType = planType;
    }

    public static FailureRechargeInfo createFailureRechargeInfo(TransactionStatus transactionStatus,
            UserDetails UserDetailsObj) {
           FailureRechargeInfo failureRechargeInfo = new FailureRechargeInfo();
           failureRechargeInfo.setMobileNo(UserDetailsObj.getUserMobileNo());
           failureRechargeInfo.setAmount(new Amount(UserDetailsObj.getAmount()));
           failureRechargeInfo.setOperatorCode(transactionStatus.getOperator());
           failureRechargeInfo.setOrderID(UserDetailsObj.getOrderId());
           failureRechargeInfo.setRechargeTime(new Timestamp(System.currentTimeMillis()));
           failureRechargeInfo.setPlanType(transactionStatus.getPlanType());
        return failureRechargeInfo;
    }
}
