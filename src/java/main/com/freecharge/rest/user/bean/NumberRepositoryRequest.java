package com.freecharge.rest.user.bean;


public class NumberRepositoryRequest {
    private String numberList;
    private String emailId;

    public String getNumberList() {
        return numberList;
    }

    public void setNumberList(String numberList) {
        this.numberList = numberList;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
