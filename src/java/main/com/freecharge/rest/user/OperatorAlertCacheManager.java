package com.freecharge.rest.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.cache.CacheManager;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.common.framework.logging.LoggingFactory;

@Repository("operatorAlertCacheManager")
public class OperatorAlertCacheManager extends CacheManager {
	private static Logger             logger = LoggingFactory.getLogger(OperatorAlertCacheManager.class);
    private static final String                 OPERATOR_ALERT_PREFIX = "operatorAlert_";
    private final int MAX_RETENTION_IN_DAYS = 7;

    @Autowired
    private OperatorCircleService operatorCircleService;

    public List<OperatorAlert> getOperatorAlert(final Integer operatorId) {
        return getOperatorAlertByKey(operatorId+"");
    }
    
    public List<OperatorAlert> getOperatorAlert(final Integer operatorId, final Integer circleId) {
        return getOperatorAlertByKey(operatorId+"_"+circleId);
    }
    
    public List<OperatorAlert> getOperatorAlertForScoreRange(final Integer operatorId, final long minScore,
            final long maxScore) {
        return getOperatorAlertForScoreRange(operatorId+"", minScore, maxScore);
    }
    
    public List<OperatorAlert> getOperatorAlertForScoreRange(final Integer operatorId, final Integer circleId, final long minScore,
            final long maxScore) {
        return getOperatorAlertForScoreRange(operatorId+"_"+circleId, minScore, maxScore);
    }

    public void setOperatorAlertId(final Integer operatorId, final Integer circleId, final OperatorAlert operatorAlertObject, final long score) {
        setOperatorAlert(operatorId+"_"+circleId, operatorAlertObject, score);
    }
    
    public void setOperatorAlertId(final Integer operatorId, final OperatorAlert operatorAlertObject, final long score) {
        setOperatorAlert(operatorId+"", operatorAlertObject, score);
    }
    
    public void clearOperatorAlertId(final Integer operatorId, final Integer circleId, final long score1, final long score2) {
        clearOperatorAlert(operatorId+"_"+circleId, score1, score2);
    }

    public void clearOperatorAlertId(final Integer operatorId, final long score1, final long score2) {
        clearOperatorAlert(operatorId+"", score1, score2);
    }
    
    public List<OperatorAlert> getOperatorAlertForScoreRange(final String key, final long minScore,
            final long maxScore) {
        List<OperatorAlert> op = new ArrayList<>();
        // Gets all the operator Alerts for an operator id in the score range
        Set<?> opAlerts = getForScoreRange(OPERATOR_ALERT_PREFIX + key, minScore, maxScore);
        for (Object operatorAlert: opAlerts) {
            OperatorAlert opAl = (OperatorAlert) operatorAlert;
            op.add(opAl);
        }
        return op;
    }
    
    public List<OperatorAlert> getOperatorAlertByKey(final String key) {
        List<OperatorAlert> op = new ArrayList<>();
        // Gets all the operator Alerts for an operator id.
        logger.info("Gets all the operator Alerts for an operator id.");
        Set<?> opAlerts = getForIndexRange(OPERATOR_ALERT_PREFIX + key, 0, -1);
        for (Object operatorAlert: opAlerts) {
            OperatorAlert opAl = (OperatorAlert) operatorAlert;
            op.add(opAl);
        }
        logger.info("Alerts for operator id :"+op.toString());
        return op;
    }
    
    public void setOperatorAlert(final String key, final OperatorAlert operatorAlertObject, final long score) {
        final DateTime today = new DateTime();
        setWithScore(OPERATOR_ALERT_PREFIX + key, operatorAlertObject, score);
        if (operatorAlertObject.getExpiryTime() != null) {
            expireAt(OPERATOR_ALERT_PREFIX + key, operatorAlertObject.getExpiryTime());
        } else {
            expireAt(OPERATOR_ALERT_PREFIX + key, today.plusDays(MAX_RETENTION_IN_DAYS).toDate());
        }
    }
    
    public void clearOperatorAlert(final String key, final long score1, final long score2) {
        deleteInScoreRange(OPERATOR_ALERT_PREFIX + key, score1, score2);
    }

    public List<OperatorAlert> getAllOperatorAlerts() {
        List<Map<String, Object>> operators = operatorCircleService.getAllOperators();
        
        List<OperatorAlert> operatorAlertObjectList = new ArrayList<OperatorAlert>();
        if (operators != null && operators.size() != 0) {
        	logger.info("Reading operators list received from operator Circle Service");
            //Read entries for operators
            for (Map<String, Object> operator : operators) {
                Integer id = Integer.parseInt(String.valueOf(operator.get("operatorMasterId")));
                logger.info("Operator ID : "+id);
                List<OperatorAlert> operatorAlert = getOperatorAlert(id);
                logger.info("operatorAlert : "+operatorAlert);
                if (operatorAlert != null) {
                    operatorAlertObjectList.addAll(operatorAlert);
                }
            }
            logger.info("added all the operatorAlert object to operatorAlertObject "+ operatorAlertObjectList);
            //Read entries for operator, circle
            logger.info("Getting Entries for Operator Circle");
            List<CircleMaster> circles = operatorCircleService.getCircles();
            logger.info("circles : "+circles);
            for (Map<String, Object> operator : operators) {
                for (CircleMaster circle : circles) {
                	logger.info("Circle master Details : "+circle.toString());
                    Integer operatorId = Integer.parseInt(String.valueOf(operator.get("operatorMasterId")));
                    Integer circleId = Integer.parseInt(String.valueOf(circle.getCircleMasterId()));
                    logger.info("Operator Id : "+ operatorId + " Circle ID : "+circleId);
                    List<OperatorAlert> operatorAlert = getOperatorAlert(operatorId, circleId);
                    if (operatorAlert != null) {
                    	logger.info("Operator Alert List : " + operatorAlert.toString());
                        operatorAlertObjectList.addAll(operatorAlert);
                    }
                }
            }
        }
        logger.info("Returning operatorAlertObjectList "+ operatorAlertObjectList);
        return operatorAlertObjectList;
    }
}
