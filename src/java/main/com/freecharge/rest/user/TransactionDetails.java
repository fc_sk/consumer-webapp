package com.freecharge.rest.user;

import java.util.List;

public class TransactionDetails {

	private boolean enableP2P;
	private List<OneCheckTransactionSummary> transactionSummaries;

	public TransactionDetails(boolean enableP2P,
			List<OneCheckTransactionSummary> transactionSummaries) {
		this.enableP2P = enableP2P;
		this.transactionSummaries = transactionSummaries;
	}

	public boolean getEnableP2P() {
		return enableP2P;
	}

	public void setEnableP2P(boolean enableP2P) {
		this.enableP2P = enableP2P;
	}

	public List<OneCheckTransactionSummary> getTransactionSummaries() {
		return transactionSummaries;
	}

	public void setTransactionSummaries(
			List<OneCheckTransactionSummary> transactionSummaries) {
		this.transactionSummaries = transactionSummaries;
	}
	
}
