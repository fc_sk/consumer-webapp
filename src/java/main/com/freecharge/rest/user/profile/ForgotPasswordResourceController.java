package com.freecharge.rest.user.profile;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.ForgotPasswordBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.web.controller.HomeController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/rest/*")
public class ForgotPasswordResourceController extends AbstractRestController {
	@Autowired
	private ForgotPasswordService forgotPasswordService;
	
	private UserAuthService userAuthService;
   
	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private MetricsClient metricsClient;
	
	@Autowired
	public void setUserAuthService(UserAuthService userAuthService) {
       this.userAuthService = userAuthService;
	}
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@NoLogin
	@RequestMapping(value="/password/reset/{email}/link", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> sendPassworResetLink(@PathVariable String email, ModelMap model) throws InvalidParameterException {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "fprcSendPasswordResetLink");
		logger.debug("Entered Forgot business DO : "+email);
		if (email == null || email.isEmpty()) {
			throw new InvalidParameterException("email", email,"Enter email Id.", ValidationErrorCode.INVALID_EMAIL);
		}
		
		ForgotPasswordBusinessDO forgotPasswordBusinessDO = new ForgotPasswordBusinessDO();
		forgotPasswordBusinessDO.setEmail(email);
		
		Map<String, Object> resultMap = new HashMap<String,Object>();
		try {
			forgotPasswordService.getPassword(forgotPasswordBusinessDO);		
			resultMap.put("forgotPasswordResult", forgotPasswordBusinessDO.getStatus());
	
			if (forgotPasswordBusinessDO != null && (forgotPasswordBusinessDO.getUserValid())) {
				resultMap.put("status", "success");
			} else {
				resultMap.put("status", "fail");
			}
		} catch (FCRuntimeException fcexp) {
			logger.error("Failed to send mail in case of forget password feature to : " + email , fcexp);
			resultMap.put("status", "fail");
			resultMap.put("forgotPasswordResult",fcProperties.getProperty("error.message"));
		} catch (NullPointerException exp) {
			logger.error("Failed to send mail in case of forget password feature to : " + email , exp);
			resultMap.put("status", "fail");
			resultMap.put("forgotPasswordResult",fcProperties.getProperty("error.message"));
		} catch (Exception exp) {
			logger.error("Failed to send mail in case of forget password feature to : " + email , exp);
			resultMap.put("status", "fail");
			resultMap.put("forgotPasswordResult",fcProperties.getProperty("error.message"));
		}
		
		resultMap.put("email", forgotPasswordBusinessDO.getEmail());
		return resultMap;
	}
}
