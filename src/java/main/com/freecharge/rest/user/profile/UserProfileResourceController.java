package com.freecharge.rest.user.profile;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.CheckOldPasswordRequest;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.model.UpdateUserProfileRequest;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/rest/user/profile/*")
public class UserProfileResourceController extends AbstractRestController{

    @Autowired
    private MyAccountService myAccountService;
    
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    static final String FAILED = "Failed";
    static final String SUCCESS = "Success";

    @Autowired
    private MetricsClient              metricsClient;
   
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public @ResponseBody ResponseStatus updateProfile(UpdateUserProfileRequest updateUserProfileRequest, BindingResult bindingResult)
           throws InvalidPathValueException {
        long startTime = System.currentTimeMillis();
        logger.debug("Entered updateProfile method of userprofilecontroller");
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userProfileResourceController");
            Integer userid = checkValidation(updateUserProfileRequest, bindingResult, responseStatus);

            if(userid ==null || (responseStatus.getStatus()!=null && responseStatus.getStatus().equals(FAILED)))
            {
                return responseStatus;
            }

            updateUserProfileRequest.setUserid(userid);
            ProfileBusinessDO profilebusinessdo = MapperUtil.map(updateUserProfileRequest, ProfileBusinessDO.class);
            myAccountService.updateProfile(profilebusinessdo);
            profilebusinessdo = MapperUtil.map(updateUserProfileRequest, ProfileBusinessDO.class);
            myAccountService.updateAddress(profilebusinessdo);
            responseStatus.setStatus("Success");
            responseStatus.setResult("User Profile Updated Succesfully");
            logger.debug("Leaving the updateProfile method of userprofilecontroller");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
        }  catch (Exception exp) {
            logger.error("Exception raised while updating the profile ", exp);
            responseStatus.setStatus(FAILED);
            responseStatus.setResult("User Profile not Updated");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
        return responseStatus;
    }
    
    
    @RequestMapping(value = "oldpassword/check", method = RequestMethod.POST)
    public @ResponseBody ResponseStatus chedkUseOlderPassword(CheckOldPasswordRequest checkOldPasswordRequest, HttpServletRequest request, HttpServletResponse response, Model model) {
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "uprcChedkUseOlderPassword");
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            response.setContentType("application/json");
            String email = checkOldPasswordRequest.getEmail();
            String password = checkOldPasswordRequest.getOldpassword();
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            Map<String, Object> map = fs.getSessionData();
            boolean islogin = false;
            String logedinemail = null;
            if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
                islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                logedinemail = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
            }
            if (!islogin || email == null || !logedinemail.equals(email)) {
                model.addAttribute("loginstatus", false);
                responseStatus.setStatus("Failed");
                responseStatus.setResult("Please login with your credentials");
                return responseStatus;
            }
          
            ProfileBusinessDO profilebusinessdo = MapperUtil.map(checkOldPasswordRequest, ProfileBusinessDO.class);
            profilebusinessdo.setOldpassword(password);
            myAccountService.checkUserOldPassword(profilebusinessdo);
            boolean ispswdcorrect = profilebusinessdo.isIsoldpswdcorrect();
            
            if(ispswdcorrect)
            {
                responseStatus.setStatus(FCConstants.SUCCESS);
                responseStatus.setResult("Correct Password");
            }
            else
            {
                responseStatus.setStatus(FCConstants.FAIL);
                responseStatus.setResult("Incorrect Password");
            }
            
        } catch (Exception e) {
            logger.error("Error in UserProfileController while checking the user oldpassword" + e);
            responseStatus.setStatus(FCConstants.FAIL);
            responseStatus.setResult("Technical error occured. Please try again later.");
        }
        
        return responseStatus;
    }
    
    private Integer checkValidation(UpdateUserProfileRequest updateUserProfileRequest, BindingResult bindingResult,
            ResponseStatus responseStatus) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        boolean islogin = false;
        String email = null;
        Integer userid = null;
        if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
            islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
            email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
            userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
        }
        if (!islogin || email == null || !email.equals(updateUserProfileRequest.getEmail())) {
          //  model.addAttribute("errormsg", "Please login with your credentials");
            responseStatus.setStatus(FAILED);
            responseStatus.setResult("Please login with your credentials");
        }
        String oldpswd = updateUserProfileRequest.getOldpassword();
        String newpswd = updateUserProfileRequest.getNewpassword();
        String confirmpswd = updateUserProfileRequest.getConfirmpassword();
         
        if (((newpswd != null && !newpswd.isEmpty() && confirmpswd != null && !confirmpswd.isEmpty()) && newpswd.equals(confirmpswd)) && (oldpswd != null && !oldpswd.isEmpty())) {
            updateUserProfileRequest.setIspasswordupdate(true);
            if (confirmpswd.length() < 6 || newpswd.length() < 6 || oldpswd.length() < 6) {
                responseStatus.setStatus(FAILED);
                responseStatus.setResult("password length should be 6 charecters");
            }

        }
        if (updateUserProfileRequest.getName() == null || updateUserProfileRequest.getName().isEmpty() || updateUserProfileRequest.getName().length() < 4) {
            responseStatus.setStatus(FAILED);
            responseStatus.setResult("Name should be minimum 4 charecters");
        }
        ValidationUtils.rejectIfEmpty(bindingResult, "mobileNo", "mobileNo not empty.");
        /*
         * ValidationUtils.rejectIfEmpty(bindingResult, "dbindingResultobString", "data of birt not empty."); if(profileWebDO.getDobString() != null){ String datestring=profileWebDO.getDobString(); SimpleDateFormat dateformat=new SimpleDateFormat("dd/MM/yyyy"); try{ Date date=dateformat.parse(datestring); if(date.after(Calendar.getInstance().getTime())) bindingResult.reject("date of birth "); }catch (Exception e) { bindingResult.reject("date error "); } }
         */
        
        if (bindingResult.hasErrors()) {
            responseStatus.setStatus(FAILED);
            responseStatus.setResult("password length should be 6 charecters");
        }
        return userid;
    }

   
}
