package com.freecharge.rest.user;

import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Repository;

import com.freecharge.common.cache.RedisCacheManager;

@Repository("operatorDownCacheManager")
public class OperatorDownCacheManager extends RedisCacheManager {
    private static final String OPERATOR_DOWN_PREFIX = "operatorDownQueue_";

    private Long updateCount(String key, Long delta) {
        RedisAtomicLong counter = new RedisAtomicLong(key, this.getCacheTemplate().getConnectionFactory());
        Long count = counter.getAndAdd(delta);
        return count;
    }

    private Long getCount(String key) {
        RedisAtomicLong counter = new RedisAtomicLong(key, this.getCacheTemplate().getConnectionFactory());
        return counter.get();
    }

    public Long incOperatorDownQueueCount(int operatorId) {
        return updateCount(OPERATOR_DOWN_PREFIX + operatorId, 1l);
    }

    public Long decOperatorDownQueueCount(int operatorId) {
        return updateCount(OPERATOR_DOWN_PREFIX + operatorId, -1l);
    }

    public long getOperatorQueueCount(int operatorId) {
        Long count = getCount(OPERATOR_DOWN_PREFIX + operatorId);
        if (count == null) {
            return 0;
        }
        return count;
    }
}