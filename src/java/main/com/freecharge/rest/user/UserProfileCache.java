package com.freecharge.rest.user;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.recharge.services.CommonRechargeService;
import com.freecharge.rest.user.bean.FailureRechargeInfo;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;

/**
 * A cache to hold various user related objects.
 * To start with; it'll be used to hold the successful
 * recharge done by the user in the last 10 minutes.
 * @author arun
 *
 */
public class UserProfileCache extends RedisCacheManager {
    private final String SUCCESS_RECHARGE_KEY = "_lastsuccess";
    private final String FAILURE_RECHARGE_KEY = "_lastfailure";
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private RedisTemplate<String, Object> userProfileCacheTemplate;
    
    @Autowired
    private CommonRechargeService rechargeService;
    
	private final Logger logger = LoggingFactory.getLogger(getClass());

    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.userProfileCacheTemplate;
    }
    
    public SuccessfulRechargeInfo getLastSuccessfulRecharge(String emailId) {
        return (SuccessfulRechargeInfo) get(emailId + SUCCESS_RECHARGE_KEY); 
    }
    
    public SuccessfulRechargeInfo getLastFailurefulRecharge(String emailId) {
           return (SuccessfulRechargeInfo) get(emailId + FAILURE_RECHARGE_KEY); 
    }
    
    public void saveLastSuccessfulRecharge(String emailId, SuccessfulRechargeInfo rechargeInfo) {
        if (!StringUtils.isBlank(rechargeInfo.getOperatorCode())) {
            if (rechargeService.getSuccessBlockSeconds(rechargeInfo.getOperatorCode()) > 0) {
                set(emailId + SUCCESS_RECHARGE_KEY, rechargeInfo, rechargeService.getSuccessBlockSeconds(rechargeInfo.getOperatorCode()), TimeUnit.SECONDS);
                set(rechargeInfo.getMobileNo() + SUCCESS_RECHARGE_KEY, rechargeInfo, rechargeService.getSuccessBlockSeconds(rechargeInfo.getOperatorCode()), TimeUnit.SECONDS);
            }
        }
		logger.info("Cache set successfuly for orderID:" + rechargeInfo.getOrderID() + " and serviceNumber:"
				+ rechargeInfo.getMobileNo());
        
    }
    
    public void saveLastFailureRecharge(String emailId, FailureRechargeInfo rechargeInfo) {
           if (!StringUtils.isBlank(rechargeInfo.getOperatorCode())) {
               if (rechargeService.isFailureRetryAttemptBlocked(rechargeInfo.getOperatorCode())) {
                   set(emailId + FAILURE_RECHARGE_KEY, rechargeInfo, rechargeService.getFailureBlockSeconds(rechargeInfo.getOperatorCode()), TimeUnit.SECONDS);
                   set(rechargeInfo.getMobileNo() + FAILURE_RECHARGE_KEY, rechargeInfo, rechargeService.getFailureBlockSeconds(rechargeInfo.getOperatorCode()), TimeUnit.SECONDS);
               }
           }
    }
    
    public SuccessfulRechargeInfo getLastSuccessfulRechargeByNumber(String serviceNumber) {
        return (SuccessfulRechargeInfo) get(serviceNumber + SUCCESS_RECHARGE_KEY); 
    }
    
    public FailureRechargeInfo getLastFailureRechargeByNumber(String serviceNumber) {
           return (FailureRechargeInfo) get(serviceNumber + FAILURE_RECHARGE_KEY); 
    }
    
}
