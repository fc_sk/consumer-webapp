package com.freecharge.rest.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.UserLoginDetailsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.CookieHandler;
import com.freecharge.common.framework.session.service.UserSessionService;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.rest.model.LoginRequest;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.sns.UserDataPointSNSService;
import com.freecharge.uuip.UserDataPointType;

@Component
public class LoginSessionResourceHelper {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    private ForgotPasswordService forgotPasswordService;

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private UserDataPointSNSService userDataPointSNSService;

    @Autowired
    protected FCProperties fcProperties;

    @Autowired
    CookieHandler cookieHandler;
    
    @Autowired
    UserSessionService userSessionService;

    public interface RememberMeHandler {
        RememberMeHandler NO_REMEMBER = new RememberMeHandler() {
            @Override
            public void handle(UserLoginDetailsBusinessDO bean) { /* do nothing */
            }
        };

        void handle(UserLoginDetailsBusinessDO bean);
    }

    public ResponseStatus loginWithPassword(final HttpServletRequest request, final HttpServletResponse response,
            final LoginRequest loginRequest, final RememberMeHandler rem) {
        return this.internalDelegateLogin(request, response, loginRequest, false, rem, SessionConstants.LoginSource.EMAIL);
    }

    /**
     * Allow Login without Password. Useful for social login.
     * 
     * @param request
     *            parameter - May need additional information sent in request such as IMEI, Device Id etc.
     * @param loginRequest
     *            parameter - Provide Login details of currently logged in user
     * @param rem
     *            parameter - RememberMeHandler Interface
     * @param loginSource
     *            parameter - LoginSource is of type enum which say whether user has logged in using FACEBOOK, GOOGLE,
     *            EMAIL
     * @return internalDelegatgeLogin() - returns ResponseStatus
     */
    public ResponseStatus passwordlessLogin(final HttpServletRequest request, final LoginRequest loginRequest,
            final RememberMeHandler rem, final SessionConstants.LoginSource loginSource) {
        return this.internalDelegateLogin(request, null, loginRequest, true, rem, loginSource);
    }

    /**
     * Handle the login functionality for bot social login and email login.
     * 
     * @param loginRequest
     *            - Provide Login details of currently logged in user
     * @param loginWithoutPassword
     *            -Indicate login happened with or without password
     * @param rem
     *            - RememberMeHandle Interface
     * @param loginSource
     *            - LoginSource is of type enum which say whether user has logged in using FACEBOOK, GOOGLE, EMAIL
     * @return responseStatus - returns responseStatus
     */
    private ResponseStatus internalDelegateLogin(final HttpServletRequest request, final HttpServletResponse response,
            final LoginRequest loginRequest, final boolean loginWithoutPassword, final RememberMeHandler rem,
            final SessionConstants.LoginSource loginSource) {
        ResponseStatus responseStatus = new ResponseStatus();
        try {
            if (loginRequest.getEmail() == null) {
                throw new IllegalArgumentException("Requested login email-address is NULL!");
            }
            /* Check if another user is already logged in */
            if (FCSessionUtil.isAnotherUserLoggedIn(loginRequest.getEmail())) {
                responseStatus.setStatus("ALREADY_LOGGEDIN");
                responseStatus.setResult(""
                        + "Another user is already logged in from the same browser. To continue using this account,"
                        + " you will have to sign out the other user and login in again.This is done to protect your"
                        + " account and to ensure the privacy of your information.");
                return responseStatus;
            }
            UserLoginDetailsBusinessDO bdo = MapperUtil.map(loginRequest, UserLoginDetailsBusinessDO.class);
            if (loginWithoutPassword) {
                userAuthService.getUserLoginDetailsPasswordless(bdo);
            } else {
                userAuthService.getUserLoginDetails(bdo);
                String cookieValue = null;
                FreechargeSession freechargeSession = FCSessionUtil.getCurrentSession();
                if (bdo.isLoggedIn()) {
                    Users users = this.userServiceProxy.getUserByEmailId(loginRequest.getEmail());
                    boolean isPasswordLinkExpired = forgotPasswordService.expirePasswordLinkOnSuccessfulLogin(users
                            .getEmail());
                    if (freechargeSession != null) {
                        final String channelString = request.getParameter("fcChannel");
                        final int channelId = channelString == null ? -1 : Integer.parseInt(channelString);
                        if (channelId != FCConstants.CHANNEL_ID_ANDROID_APP
                                && channelId != FCConstants.CHANNEL_ID_IOS_APP
                                && channelId != FCConstants.CHANNEL_ID_WINDOWS_APP
                                && channelId != FCConstants.CHANNEL_ID_MOOLAH_APP) {
                            cookieValue = this.cookieHandler.checkAndSetCookies(request, response, true);
                            List<TxnHomePage> list = txnHomePageService.findBySessionId(FCSessionUtil
                                    .getCurrentSession().getUuid());
                            if (FCUtil.isEmpty(list)) {
                                freechargeSession.setUuid(cookieValue);
                            }
                            for (TxnHomePage txnHomePageList : list) {
                                if (txnHomePageList != null && cookieValue != null) {
                                    int status = txnHomePageService.updateTxnHomePage(txnHomePageList.getLookupId(),
                                            cookieValue);
                                    if (status <= 0) {
                                        logger.info("Failed to update table txn_home_page with new cookie value: "
                                                + cookieValue);
                                    } else {
                                        freechargeSession.setUuid(cookieValue);
                                    }

                                }
                            }
                        }
                    }
                }
            }
            final boolean isLogin = bdo.isLoggedIn();
            if (isLogin) {
                String eMail = loginRequest.getEmail();
                String firstName = (bdo.getFirstName());
                Integer userId = new Integer(bdo.getUserId());

                FreechargeSession.setUserRelatedSessionData(eMail, firstName, isLogin, userId, loginSource, 
                        request.getParameter(FCConstants.IMEI), request.getParameter(FCConstants.DEVICE_UNIQUE_ID));
                FreechargeSession freechargeSession = FCSessionUtil.getCurrentSession();
                userSessionService.storeSession(freechargeSession);
                logger.info("session stored successfully for userId:" + userId);
                
                if (loginRequest.getRememberme().equalsIgnoreCase("1")) {
                    rem.handle(bdo);
                }
                if (firstName != null && !firstName.isEmpty()) {
                    firstName = WordUtils.capitalize(firstName);
                }
                responseStatus.setStatus(FCConstants.SUCCESS);
                responseStatus.setResult(FCConstants.SUCCESS);
                
                //Publishing IMEI and uniqueDeviceId to amazon SNS for mystique (Unique User Identification Service)
                Date date = new Date();
                userDataPointSNSService.publishUserDataPoint(userId,
                        request.getParameter(FCConstants.IMEI), UserDataPointType.IMEI, date);
                userDataPointSNSService.publishUserDataPoint(userId,
                        request.getParameter(FCConstants.DEVICE_UNIQUE_ID),
                        UserDataPointType.WINDOWS_UNIQUE_DEVICE_ID, date);
            } else {
                responseStatus.setStatus(FCConstants.ERROR);
                responseStatus.setResult("Please check your username and password.");
            }
        } catch (Exception exp) {
            logger.error(
                    String.format("In login service for userId=%s while calling login service: ",
                            loginRequest.getEmail()), exp);
            responseStatus.setStatus(FCConstants.ERROR);
            responseStatus.setResult(FCConstants.ERROR);
            return responseStatus;
        }
        return responseStatus;
    }
}
