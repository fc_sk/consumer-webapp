package com.freecharge.rest.user;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.service.FailedLoginService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/*")
public class ReCaptchaController {
    private static final Logger LOGGER = LoggingFactory.getLogger(ReCaptchaController.class);
    @Autowired
    private FailedLoginService failedLoginService;
    @Autowired
    private FCProperties fcProperties;
    @Autowired
    private AppConfigService appConfigService;

    private static final long  ONE_MIN_MILLISCONDS = 60 * 1000;
    private static final String IS_CAPTCHA = "isCaptcha";
    

    @NoLogin
    @NoSessionWrite
    @RequestMapping(value = "/users/{userEmailId}/checklogincaptcha", method = RequestMethod.GET)
    public @ResponseBody
    ModelMap failedLoginCaptcha(@PathVariable String userEmailId, ModelMap model) throws InvalidPathValueException {
        Map<String, Object> userMap = new HashMap<String, Object>();
        try {
            if(appConfigService.isFailedLoginRecaptchaEnabled()) {
                int reCaptchaFraudCheckCount = Integer.parseInt(fcProperties.getCaptchaFraudCheckCount());
                int reCaptchaLoginThresholdMins = Integer.parseInt(fcProperties.getReCaptchaLoginThresholdMins());
                long beforeThresholdMins = new Date().getTime() - (reCaptchaLoginThresholdMins * ONE_MIN_MILLISCONDS);
                Timestamp start = new Timestamp(beforeThresholdMins);
                if (failedLoginService.isFailedLoginThreshold(userEmailId, reCaptchaFraudCheckCount, start)) {
                    userMap.put(IS_CAPTCHA, true);
                } else {
                    userMap.put(IS_CAPTCHA, false);
                }
            } else {
                userMap.put(IS_CAPTCHA, false);
            }
        } catch (RuntimeException e) {
            LOGGER.error("Failed to check if captcha is required or not.", e);
        }
        return model.addAllAttributes(userMap);
    }
}
