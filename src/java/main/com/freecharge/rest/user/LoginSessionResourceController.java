package com.freecharge.rest.user;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.framework.util.CSRFTokenManager;
import org.apache.bcel.generic.FCONST;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.FailedLogin;
import com.freecharge.app.service.FailedLoginService;
import com.freecharge.common.businessdo.UserLoginDetailsBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.encryption.mdfive.MD5;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.ReCaptchaService;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.LoginRequest;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.user.LoginSessionResourceHelper.RememberMeHandler;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.TrackerEvent;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/rest/*")
public class LoginSessionResourceController extends AbstractRestController {

    @Autowired
    private EPinEncrypt                ePinEncrypt;

    @Autowired
    private FCProperties               fcproperties;

    @Autowired
    private LoginSessionResourceHelper loginHelper;

    @Autowired
    private Tracker                    tracker;

    @Autowired
    private MetricsClient              metricsClient;

    @Autowired
    private UserResourceController     userResourceController;

    @Autowired
    private FailedLoginService         failedLoginService;

    @Autowired
    private ReCaptchaService           reCaptchaService;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private AppConfigService appConfigService;

    @Value("${mongo.db.username}")
    private String userName;

    @Value("${mongo.db.password}")
    private String password;

    @Value("${mongo.db.hosts}")
    private String hosts;

    @Value("${mongo.db.name}")
    private String dbName;

    private final Logger               logger    = LoggingFactory.getLogger(getClass());
    public static final String         DO_LOGOUT = "doLogout";

    public static class WhoAmI {
        private final String email;
        private final String name;
        private final long   credits;
        private String hUserid;

        public WhoAmI(String email, String name, long credits, String hUserid) {
            this.email = email;
            this.name = name;
            this.credits = credits;
            this.hUserid = hUserid;
        }

        @SuppressWarnings("unused")
        public String getEmail() {
            return email;
        }

        @SuppressWarnings("unused")
        public String getName() {
            return name;
        }

        @SuppressWarnings("unused")
        public long getCredits() {
            return credits;
        }

        public String gethUserid() {
            return hUserid;
        }

        public void sethUserid(String hUserid) {
            this.hUserid = hUserid;
        }
    }

    @RequestMapping(value = "/whoami", method = RequestMethod.GET)
    public @ResponseBody
    WhoAmI whoami() {
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "lsrcWhoAmI");
        String loggedInEmail = (String) FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_EMAIL_ID);
        String firstName = (String) FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_FIRSTNAME);
        Integer loggedInUserId = FCSessionUtil.getLoggedInUserId();
        String hUserId = MD5.hash(Integer.toString(loggedInUserId));
        Amount credits = Amount.ZERO;
        try {
            credits = userResourceController.getBalance(loggedInEmail);
        } catch (InvalidPathValueException ipve) {
            logger.error("Error (should never happen) while fetching user credits:", ipve);
        }
        return new WhoAmI(loggedInEmail, firstName, credits.getAmount().longValue(), hUserId);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @NoLogin
    public @ResponseBody
    Map<String, Object> doLogin(final LoginRequest loginRequest, final BindingResult bindingResult,
            final HttpServletRequest request, final HttpServletResponse response) throws InvalidPathValueException {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "loginSessionResource1");
        tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_ATTEMPT.createTrackingEvent());
        // Setting freecharge home in model
        Map<String, Object> responseStatus = new HashMap<>();
        boolean isValidCaptcha = true;
        String ip = MobileUtil.getRealClientIpAddress(request);
        if (!FCUtil.isEmpty(loginRequest.getChallenge())) {
            isValidCaptcha = reCaptchaService.isValid(loginRequest.getChallenge(), loginRequest.getResponse(), ip);
        }
        try {
            ValidationUtils.rejectIfEmpty(bindingResult, "email", "User can not be empty.");
            ValidationUtils.rejectIfEmpty(bindingResult, "password", "Password can not be empty");
            if(loginRequest.getRememberme() != null && !loginRequest.getRememberme().equals("true") && !loginRequest.getRememberme().equals("false") && 
            		!loginRequest.getRememberme().equals("0") && !loginRequest.getRememberme().equals("1"))
        		bindingResult.addError(new ObjectError("rememberme", "rememberme has invalid value"));
            if (bindingResult.hasErrors()) {
                metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
                responseStatus.put("status", FCConstants.FAIL);
                responseStatus.put("result", bindingResult.getAllErrors());
            } else {
                if (isValidCaptcha) {
                    ResponseStatus respStatus = loginHelper.loginWithPassword(request, response, loginRequest, 
                    		new RememberMeHandler() {
                        @Override
                        public void handle(final UserLoginDetailsBusinessDO bdo) {
                            setRememberMECookie(response, bdo.getUserId().toString());
                        }
                    });
                    responseStatus.put("status", respStatus.getStatus());
                    responseStatus.put("result", respStatus.getResult());
                    if (FCConstants.SUCCESS.equals(responseStatus.get("status"))) {
                        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "success");
                        tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_SUCCESS.createTrackingEvent());
                    } else {
                        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
                    }
                } else {
                    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
                    responseStatus.put("status", FCConstants.ERROR);
                    responseStatus.put("result", "Please enter the captcha text properly.");
                }
            }
        } catch (RuntimeException exp) {
            logger.error(
                    String.format("In login service for userId=%s while calling login service: ",
                            loginRequest.getEmail()), exp);
            responseStatus.put("status", FCConstants.ERROR);
            responseStatus.put("result", FCConstants.ERROR);
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
        }
        if (!FCConstants.SUCCESS.equals(responseStatus.get("status"))) {
            FailedLogin failedLogin = failedLoginService.createFailedLogin(loginRequest.getEmail(), ip, new Timestamp(
                    new Date().getTime()));
            logger.info("Failed login attempt - email - " + loginRequest.getEmail() + " IP address - " + ip);
            failedLoginService.insertFailedLogin(failedLogin);
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "login", endTime - startTime);
        return responseStatus;
    }

    @Csrf
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public @ResponseBody
    ResponseStatus logout(final HttpServletRequest request) throws InvalidPathValueException {
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "lsrcLogout");
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setResult(FCConstants.SUCCESS);
        responseStatus.setStatus(FCConstants.SUCCESS);
        // All the logout related activities are done in HTTPRequestInterceptor
        // class. This interceptor searches for the below
        // attribute in request and if found, does the logout related
        // activities.
        request.setAttribute(DO_LOGOUT, true);

        return responseStatus;
    }

    public void setRememberMECookie(final HttpServletResponse response, final String userId) {
        String encryptedUserId = null;
        try {
            if (userId != null) {
                encryptedUserId = ePinEncrypt.getEncryptedPin(userId);
            }
        } catch (Exception e) {
            logger.error("Error occured while encrypting userid", e);
        }
        if (encryptedUserId != null) {
            Cookie userCookie = new Cookie(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_USERID),
                    encryptedUserId);
            userCookie.setMaxAge(Integer.parseInt(fcproperties
                    .getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_AGE)));
            userCookie.setPath(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_PATH));

            if (fcproperties.isCookieHttpsOnly()) {
                userCookie.setSecure(true);
            }

            userCookie.setHttpOnly(true);
            response.addCookie(userCookie);
        }
    }

    @NoLogin
    @RequestMapping(value = "/session/create", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> createSession(final HttpServletRequest request) throws InvalidPathValueException {
        try {
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "lsrcCreateSession");
            // Request to create session for user and return back the CSRFToken
            Map<String, String> csrfMap = new HashMap<String, String>();
            csrfMap.put("csrfRequestIdentifier", CSRFTokenManager.getTokenFromSession());
            return csrfMap;
        } catch (Exception e) {
            metricsClient.recordEvent("sessionCreateCall", "sessionCreate", "error");
            logger.error("Exception during rest session create call (rscc)", e);
            throw new RuntimeException(e);
        }
    }
    
}
