package com.freecharge.rest.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.PgAlert;
import com.freecharge.common.cache.CacheManager;
import com.freecharge.payment.services.PaymentGatewayManagementService;

@Repository("pgAlertCacheManager")
public class PgAlertCacheManager extends CacheManager {
    
    static String PG_ALERT_PREFIX = "pgAlert_";

    @Autowired
    private PaymentGatewayManagementService     paymentGatewayManagementService;
    
    public PgAlert getPgAlertById(Long pgId){
        return (PgAlert)get(PG_ALERT_PREFIX+pgId);
    }

    public void setPgAlertId(Long pgId, PgAlert pgAlertObject){
        set(PG_ALERT_PREFIX+pgId, pgAlertObject);
    }

    public void clearPgrAlertId(Long pgId){
        delete(PG_ALERT_PREFIX+pgId);
    }
    
    public List<PgAlert> getAllPgAlerts(){
        Map<Integer, String>  paymentGateways = paymentGatewayManagementService.getBanksGrpMaster();
        List<PgAlert> pgAlertObjectList = new ArrayList<PgAlert>();
        
        if(paymentGateways != null && paymentGateways.size() != 0){
            for(Map.Entry<Integer, String> pgEntry : paymentGateways.entrySet()){
               Long id = Long.parseLong(String.valueOf(pgEntry.getKey()));
                PgAlert pgAlert = (PgAlert)getPgAlertById(id);
                if(pgAlert!= null)
                    pgAlertObjectList.add(pgAlert);
            }
        }
        return pgAlertObjectList;
    }
    

}

