package com.freecharge.rest.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.service.UserIdHelper;
import com.freecharge.app.service.UserPreferenceService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.model.SaveUserPreferenceRequest;
import com.mongodb.DBObject;

@Controller
@RequestMapping("/rest/user/preference/*")
public class UserPreferenceController extends AbstractRestController {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private UserIdHelper userIdHelper;

    @Autowired
    private UserPreferenceService userPreferenceService;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public @ResponseBody ResponseStatus savePreference(@RequestBody final SaveUserPreferenceRequest saveUserPreferenceRequest)
            throws InvalidPathValueException {
    	ResponseStatus responseStatus = new ResponseStatus();
        if (saveUserPreferenceRequest == null) {
        	 responseStatus.setStatus("Failed");
             responseStatus.setResult(saveUserPreferenceRequest);
            throw new InvalidPathValueException("save", null, "SaveUserPreferenceRequest");    
        }
        if (!this.validateUserSessionEmail(saveUserPreferenceRequest.getUserEmailId())) {
        	 responseStatus.setStatus("Failed");
             responseStatus.setResult(saveUserPreferenceRequest);
            throw new InvalidPathValueException("save", saveUserPreferenceRequest.getUserEmailId(),
                    FCSessionUtil.getLoggedInEmailId());
        }
        if (saveUserPreferenceRequest.getType() == null || saveUserPreferenceRequest.getType().trim().length() == 0) {
        	 responseStatus.setStatus("Failed");
             responseStatus.setResult(saveUserPreferenceRequest);
             throw new InvalidPathValueException("save", saveUserPreferenceRequest.getType(), "type");
        } else {
        	 if(saveUserPreferenceRequest.getUserPrefMap() != null &&
        			 saveUserPreferenceRequest.getUserPrefMap().get("nickName") != null && 
        			  !FCUtil.isWelFormedName(saveUserPreferenceRequest.getUserPrefMap().get("nickName").toString())) {
        		 responseStatus.setStatus("Failed");
                 responseStatus.setResult("Invalid nick name:"+
        		 saveUserPreferenceRequest.getUserPrefMap().get("nickName"));
            	throw new IllegalArgumentException("Invalid nick name:" + saveUserPreferenceRequest.getUserPrefMap().
            			get("nickName"));
            } else {
            	   String preferenceId = this.userPreferenceService.insertOrUpdatePreference(saveUserPreferenceRequest.getPrefId(), 
            			   saveUserPreferenceRequest.getUserEmailId(),
                           saveUserPreferenceRequest.getUserPrefMap(), saveUserPreferenceRequest.getType(),
                           saveUserPreferenceRequest.getUserRating());
                           responseStatus.setStatus("Success");
                           responseStatus.setResult(saveUserPreferenceRequest);
                   saveUserPreferenceRequest.setPrefId(preferenceId);
            }
             
       }
        return responseStatus;
    }

    @RequestMapping(value = "list/{userEmailId}/{type}", method = RequestMethod.GET)
    public @ResponseBody List<DBObject> getUserPreferences(@PathVariable("userEmailId") final String userEmailId,
            @PathVariable("type") final String type) throws Exception {
        if (!this.validateUserSessionEmail(userEmailId)) {
            throw new InvalidPathValueException("listAll/{userEmailId}", userEmailId,
                    FCSessionUtil.getLoggedInEmailId());
        }
        List<DBObject> pref = userPreferenceService.getUserPreferencesByUserIdandType(userEmailId, type);
        return pref;
    }

    @RequestMapping(value = "listAll/{userEmailId}", method = RequestMethod.GET)
    public @ResponseBody List<DBObject> getAllUserPreferences(@PathVariable("userEmailId") final String userEmailId)
            throws InvalidPathValueException {
        if (!this.validateUserSessionEmail(userEmailId)) {
            throw new InvalidPathValueException("listAll/{userEmailId}", userEmailId,
                    FCSessionUtil.getLoggedInEmailId());
        }
        List<DBObject> pref = userPreferenceService.getAllUserPreferences(userEmailId);
        return pref;
    }

    @RequestMapping(value = "delete/{userEmailId}/{preferenceId}", method = RequestMethod.GET)
    public @ResponseBody String deletePreference(@PathVariable final String userEmailId,
            @PathVariable final String preferenceId) throws InvalidPathValueException {
        if (!this.validateUserSessionEmail(userEmailId)) {
            throw new InvalidPathValueException("delete/{userEmailId}/{preferenceId}", userEmailId,
                    FCSessionUtil.getLoggedInEmailId());
        }
        if (preferenceId == null || preferenceId.trim().length() == 0) {
            throw new InvalidPathValueException("/delete/{userEmailId}/{preferenceId}", preferenceId,
                    "Valid preferenceId");
        }
        userPreferenceService.deletePreference(userEmailId, preferenceId);
        return "success";
    }

    @RequestMapping(value = "deleteAll/{userEmailId}", method = RequestMethod.GET)
    public @ResponseBody String deleleAllUserPreferences(@PathVariable final String userEmailId)
            throws InvalidPathValueException {
        if (!this.validateUserSessionEmail(userEmailId)) {
            throw new InvalidPathValueException("deleteAll/{userEmailId}", userEmailId,
                    FCSessionUtil.getLoggedInEmailId());
        }
        userPreferenceService.deleteAllUserPreferences(userEmailId);
        return "success";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseStatus editPreference(@RequestBody final SaveUserPreferenceRequest saveUserPreferenceRequest)
            throws InvalidPathValueException {
    	ResponseStatus responseStatus = new ResponseStatus();
        if (saveUserPreferenceRequest == null) {
        	responseStatus.setStatus("Failed");
            responseStatus.setResult(saveUserPreferenceRequest);
            throw new InvalidPathValueException("edit", null, "SaveUserPreferenceRequest");
        }
        if (!this.validateUserSessionEmail(saveUserPreferenceRequest.getUserEmailId())) {
        	responseStatus.setStatus("Failed");
            responseStatus.setResult(saveUserPreferenceRequest);
            throw new InvalidPathValueException("edit", saveUserPreferenceRequest.getUserEmailId(),
                    FCSessionUtil.getLoggedInEmailId());
        }
        if (saveUserPreferenceRequest.getPrefId() == null) {
        	responseStatus.setStatus("Failed");
            responseStatus.setResult(saveUserPreferenceRequest);
            throw new InvalidPathValueException("edit", null, "preference id");
        } else {
        	String prefId = this.userPreferenceService.insertOrUpdatePreference(saveUserPreferenceRequest.getPrefId(),
                    saveUserPreferenceRequest.getUserEmailId(),
                    saveUserPreferenceRequest.getUserPrefMap(), saveUserPreferenceRequest.getType(), saveUserPreferenceRequest.getUserRating());
        	responseStatus.setStatus("Success");
        	saveUserPreferenceRequest.setPrefId(prefId);
            responseStatus.setResult(saveUserPreferenceRequest);
        }
        return responseStatus;
    }

    @RequestMapping(value = "insertOrUpdate", method = RequestMethod.POST)
    public @ResponseBody ResponseStatus insertOrUpdatePreference(
            @RequestBody final SaveUserPreferenceRequest saveUserPreferenceRequest) throws InvalidPathValueException {
        ResponseStatus responseStatus = new ResponseStatus();
        if (saveUserPreferenceRequest == null) {
            throw new InvalidPathValueException("save", null, "SaveUserPreferenceRequest");
        }
        if (!validateUserSessionEmail(saveUserPreferenceRequest.getUserEmailId())) {
            throw new InvalidPathValueException("save", saveUserPreferenceRequest.getUserEmailId(),
                    FCSessionUtil.getLoggedInEmailId());
        }
        if (saveUserPreferenceRequest.getType() == null || saveUserPreferenceRequest.getType().trim().length() == 0) {
            throw new InvalidPathValueException("save", saveUserPreferenceRequest.getType(), "type");
        }
        try {
            String preferenceId = this.userPreferenceService.insertOrUpdatePreference(saveUserPreferenceRequest.getPrefId(), saveUserPreferenceRequest.getUserEmailId(),
                    saveUserPreferenceRequest.getUserPrefMap(), saveUserPreferenceRequest.getType(),
                    saveUserPreferenceRequest.getUserRating());
            saveUserPreferenceRequest.setPrefId(preferenceId);
            responseStatus.setStatus("Success");
            responseStatus.setResult(saveUserPreferenceRequest);
        } catch (RuntimeException re) {
            logger.error("Failed to do insert or update" + re);
            responseStatus.setStatus("Failed");
            responseStatus.setResult(saveUserPreferenceRequest);
        }
        return responseStatus;
    }

    
    @RequestMapping(value = "insertUpdateBatch", method = RequestMethod.POST)
    public @ResponseBody List<ResponseStatus> insertUpdateBatchPreference(
            @RequestBody final List<SaveUserPreferenceRequest> saveListOfUserPreferenceRequest) throws InvalidPathValueException {
        
        List<ResponseStatus> responseStatus = new ArrayList<ResponseStatus>();
        
        if (saveListOfUserPreferenceRequest == null || saveListOfUserPreferenceRequest.size()<0) {
            throw new InvalidPathValueException("save", null, "SaveUserPreferenceRequest");
        }
        
        for (SaveUserPreferenceRequest saveUserPreferenceRequestObj :  saveListOfUserPreferenceRequest) {
            ResponseStatus responseStatusObj = updateUserpreference(saveUserPreferenceRequestObj, saveListOfUserPreferenceRequest);
            responseStatus.add(responseStatusObj);
        }
        
        return responseStatus;
    }

    
    private ResponseStatus updateUserpreference(SaveUserPreferenceRequest saveUserPreferenceRequestObj, List<SaveUserPreferenceRequest> saveListOfUserPreferenceRequest) {
        
        
        ResponseStatus responseStatusObj = new ResponseStatus();
        
        try {
            
        if (!validateUserSessionEmail(saveUserPreferenceRequestObj.getUserEmailId())) {
            throw new InvalidPathValueException("save", saveUserPreferenceRequestObj.getUserEmailId(),
                    FCSessionUtil.getLoggedInEmailId());
        }
        
        if (saveUserPreferenceRequestObj.getType() == null || saveUserPreferenceRequestObj.getType().trim().length() == 0) {
            throw new InvalidPathValueException("save", saveUserPreferenceRequestObj.getType(), "type");
        }

        	String preferenceId = this.userPreferenceService.insertOrUpdatePreference(saveUserPreferenceRequestObj.getPrefId(), saveUserPreferenceRequestObj.getUserEmailId(),
                    saveUserPreferenceRequestObj.getUserPrefMap(), saveUserPreferenceRequestObj.getType(),
                    saveUserPreferenceRequestObj.getUserRating());
            saveUserPreferenceRequestObj.setPrefId(preferenceId);
            responseStatusObj.setStatus("Success");

            responseStatusObj.setResult(saveListOfUserPreferenceRequest);
            
        } catch (InvalidPathValueException re) {
            logger.error("throws InvalidPathValueException " + re);
            responseStatusObj.setStatus("Failed");
            responseStatusObj.setResult(saveListOfUserPreferenceRequest);
        } catch (RuntimeException exception) {
            logger.error("Failed to do insert or update" + exception);
            responseStatusObj.setStatus("Failed");
            responseStatusObj.setResult(saveListOfUserPreferenceRequest);
        }
        
        return responseStatusObj;
        
    }
    
    private boolean validateUserSessionEmail(String userEmailId) throws InvalidPathValueException {
        if (userEmailId != null && userEmailId.equals(FCSessionUtil.getLoggedInEmailId()))
            return true;
        else
            return false;
    }
}
