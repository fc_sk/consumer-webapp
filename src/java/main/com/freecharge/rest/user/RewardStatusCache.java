package com.freecharge.rest.user;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.reward.web.RewardEligibleResult;

@Component
public class RewardStatusCache extends RedisCacheManager  {
	
	private final long  TIME_TO_CACHE = 1;
	private final Logger logger = LoggingFactory.getLogger(getClass());
	public final String ORDER_ID = "orderId";
	public final String EVENT_ID = "eventId";
	public final String USER_ID = "userId";
	public final String REWARD_VALUE = "rewardValue";
	public final String RECHARGE_MOBILE_NUMBER = "rechargeMobileNumber";
	
	private final String REWARD_ELIGIBLE_RESULT = "RER_";
	private final String REWARD_HISTORY = "RH_";
	
	public RewardEligibleResult getRewardStatus(
			String orderId) {
		try {
			return (RewardEligibleResult) get(REWARD_ELIGIBLE_RESULT+orderId);
		} catch (Exception e) {
			logger.error("Error in getting cache data for reward status for orderId: "+orderId, e);
			return null;
		}
	}

	public void setRewardStatus(String orderId,
			RewardEligibleResult result) {
		try {
			set(REWARD_ELIGIBLE_RESULT+orderId, result, TIME_TO_CACHE, TimeUnit.HOURS);
		} catch (Exception e) {
			logger.error("Error in setting cache data for reward status for orderId: "+orderId, e);
		}
	}
	
	public void deleteRewardStatus(String orderId) {
		try {
			delete(REWARD_ELIGIBLE_RESULT+orderId);
		} catch (Exception e) {
			logger.error("Error in delete cache data for reward status for orderId: "+orderId, e);
		}
	}
	
	public Map<String,Object> getGrowthRewardHistory(String orderId) {
		try {
			return (Map<String, Object>) get(REWARD_HISTORY+orderId);
		} catch (Exception e) {
			logger.error("Error in getting cache data for reward history for orderId: "+orderId, e);
			return null;
		}
	}
	
	public void setGrowthRewardHistory(String orderId,
			Map<String, Object> result) {
		try {
			set(REWARD_HISTORY+orderId, result, TIME_TO_CACHE, TimeUnit.HOURS);
		} catch (Exception e) {
			logger.error("Error in setting cache data for reward history for orderId: "+orderId, e);
		}
	}
	
	public void deleteGrowthRewardHistory(String orderId) {
		try {
			delete(REWARD_HISTORY+orderId);
		} catch (Exception e) {
			logger.error("Error in delete cache data for reward history for orderId: "+orderId, e);
		}
	}
}