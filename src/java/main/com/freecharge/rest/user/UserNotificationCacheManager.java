package com.freecharge.rest.user;

import com.freecharge.app.domain.entity.InactiveOperatorMetaData;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.cache.CacheManager;
import com.freecharge.recharge.businessDo.OperatorUpNotificationDo;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by yashveer on 30/5/16.
 */
@Repository("userNotificationCacheManager")
public class UserNotificationCacheManager extends CacheManager {
    private static final String OPERATOR_UPTIME_NOTIFY_SET_PREFIX = "operatorUptimeNotify_";
    private final int MAX_RETENTION_HOURS = 12;

    @Autowired
    private OperatorAlertCacheManager operatorAlertCacheManager;

/**
    public List<String> getImpactedUsers(final Integer operatorId, final Integer circleId) {
        InactiveOperatorMetaData metaData = getInactiveOperatorMetaData(operatorId + "_" + circleId);
        if(metaData!=null){
            return metaData.getUserNotificationList();
        }
        return Collections.emptyList();
    }*/

    /*public void addImpactedUser(final Integer operatorId, final Integer circleId, OperatorUpNotificationDo notificationDo){
        String accessKey = getKey(operatorId, circleId);
        = getInactiveOperatorMetaData(accessKey);
        if(metaData == null){
            // instantiate a new obj
            metaData = new InactiveOperatorMetaData();
            operatorAlertCacheManager.
            metaData.setOperatorAlert();
            //set fields
        }
        metaData.getUserNotificationList().add(notificationDo);
        // Save the object
        setInactiveOperatorMetaData(accessKey, metaData);
    }*/

    public InactiveOperatorMetaData getInactiveOperatorMetaData(final Integer operatorId, final Integer circleId) {
        InactiveOperatorMetaData metaData = null;
        Object operatorObj = get(getKey(operatorId, circleId));
        if(operatorObj!=null){
            metaData = (InactiveOperatorMetaData)operatorObj;
        }
        return metaData;
    }

    public void addImpactedUser(final Integer operatorId, final Integer circleId, OperatorUpNotificationDo notificationDo){
        String key = getKey(operatorId, circleId);
        addToSet(key, notificationDo);
    }

    private InactiveOperatorMetaData getInactiveOperatorMetaData(final String key) {
        InactiveOperatorMetaData metaData = null;
        Object operatorObj = get(key);
        if(operatorObj!=null){
            metaData = (InactiveOperatorMetaData)operatorObj;
        }
        return metaData;
    }

    public String getKey(final Integer operatorId, final Integer circleid){
        String key = operatorId.toString();
        if(circleid!=null){
            key += "_" + circleid;
        }
        return OPERATOR_UPTIME_NOTIFY_SET_PREFIX + key;
    }

    @Override
    public InactiveOperatorMetaData get(String key) {
        InactiveOperatorMetaData metaData = null;
        Object obj = super.get(key);
        if(obj!=null){
            metaData = (InactiveOperatorMetaData)obj;
        }
        return metaData;
    }

    public void setInactiveOperatorMetaData(final String key, final InactiveOperatorMetaData operatorMetaData) {
        final DateTime today = new DateTime();
        //set(OPERATOR_METADATA_PREFIX + key, operatorMetaData, operatorMetaData.getOperatorAlert().getExpiryTime().getTime(), TimeUnit.MILLISECONDS);
        /*if (operatorAlertObject.getExpiryTime() != null) {
            expireAt(OPERATOR_ALERT_PREFIX + key, operatorAlertObject.getExpiryTime());
        } else {
            expireAt(OPERATOR_ALERT_PREFIX + key, today.plusDays(MAX_RETENTION_IN_DAYS).toDate());
        }*/
    }



}
