package com.freecharge.rest.user;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mongo.repo.MobileNumberRepository;
import com.freecharge.mongo.repos.UserPreferenceRepository;
import com.freecharge.rest.user.bean.NumberRepositoryRequest;
import com.google.gson.Gson;
import com.mongodb.DBObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/number/")
public class MobileNumberController {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private MobileNumberRepository mobileNumberRepository;

    @Autowired
    private UserPreferenceRepository userPreferenceRepository;

    @RequestMapping(value = "/name/preference", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> getName(@RequestBody NumberRepositoryRequest numberRepositoryRequest) {
        Map<String, String> result = new HashMap<>();
        String numbers = numberRepositoryRequest.getNumberList();
        String name = null;
        if (FCUtil.isNotEmpty(numbers) && numbers.length() > 0) {
            for (String number : numbers.split(",")) {
                logger.info("Searching nickname for number " + number);

                List<DBObject> dbObjectList = userPreferenceRepository.getUserPreferencesByUserandType(
                        numberRepositoryRequest.getEmailId(), FCConstants.NICKNAME);
                if (!FCUtil.isEmpty(dbObjectList) && dbObjectList.size() > 0) {
                    for (DBObject dbObject1 : dbObjectList) {
                        String prefObject  = String.valueOf(dbObject1.get("userPrefMap"));
                        if (FCUtil.isNotEmpty(prefObject) && !"NULL".equalsIgnoreCase(prefObject)) {
                            Map<String, Object> prefMap = new Gson().fromJson(prefObject, Map.class);
                            String serviceNumber = String.valueOf(prefMap.get(FCConstants.SERVICE_NUMBER));
                            if (FCUtil.isNotEmpty(serviceNumber) && !"NULL".equalsIgnoreCase(serviceNumber) &&
                                    number.equalsIgnoreCase(serviceNumber)) {
                                String nameFromUserPref = String.valueOf(prefMap.get(FCConstants.NICKNAME));
                                if (FCUtil.isNotEmpty(nameFromUserPref) && !"NULL".equalsIgnoreCase(nameFromUserPref)) {
                                    name = nameFromUserPref;
                                }
                            }
                        }
                    }
                }
                if (FCUtil.isEmpty(name) || "NULL".equalsIgnoreCase(name)) {
                    DBObject dbObject = mobileNumberRepository.getDataByIdForSuccessCases(number);
                    if (dbObject != null) {
                        name  = String.valueOf(dbObject.get("name"));
                    }
                }
                result.put(number, name);
                name = null;
            }
            return result;
        }
        result.put("result","failed");
        return result;
    }

}
