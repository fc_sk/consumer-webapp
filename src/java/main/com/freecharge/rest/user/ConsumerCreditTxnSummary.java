package com.freecharge.rest.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsumerCreditTxnSummary implements Comparable<ConsumerCreditTxnSummary> {
	
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private String merchantName;
	private String merchantId;
	private String fcOrderId;
	private String txnAmount;
	private String txnStatus;
	private String refundId;
	private String lenderTxnId;
	private String lenderName;
	private String lenderId;
	private String emiTenure;
	private String emiAmount;
	private String interestRate;
	private String creditAvailabilityStatus;
	private String totalApprovedCredit;
	private String currentAvailableCredit;
	private String timestamp;
	private String loanDestination;
	private String processingFee;
	private String bankDetails;
	private String accountNumber;
	private String loanAmount;


	public String getMerchantName() {
		return merchantName;
	}


	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}


	public String getMerchantId() {
		return merchantId;
	}


	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}


	public String getFcOrderId() {
		return fcOrderId;
	}


	public void setFcOrderId(String fcOrderId) {
		this.fcOrderId = fcOrderId;
	}


	public String getTxnAmount() {
		return txnAmount;
	}


	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}


	public String getTxnStatus() {
		return txnStatus;
	}


	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}


	public String getRefundId() {
		return refundId;
	}


	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}


	public String getLenderTxnId() {
		return lenderTxnId;
	}


	public void setLenderTxnId(String lenderTxnId) {
		this.lenderTxnId = lenderTxnId;
	}


	public String getLenderName() {
		return lenderName;
	}


	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}


	public String getLenderId() {
		return lenderId;
	}


	public void setLenderId(String lenderId) {
		this.lenderId = lenderId;
	}


	public String getCreditAvailabilityStatus() {
		return creditAvailabilityStatus;
	}


	public void setCreditAvailabilityStatus(String creditAvailabilityStatus) {
		this.creditAvailabilityStatus = creditAvailabilityStatus;
	}

	public String getTotalApprovedCredit() {
		return totalApprovedCredit;
	}

	public void setTotalApprovedCredit(String totalApprovedCredit) {
		this.totalApprovedCredit = totalApprovedCredit;
	}

	public String getCurrentAvailableCredit() {
		return currentAvailableCredit;
	}

	public void setCurrentAvailableCredit(String currentAvailableCredit) {
		this.currentAvailableCredit = currentAvailableCredit;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getEmiTenure() {
		return emiTenure;
	}

	public void setEmiTenure(String emiTenure) {
		this.emiTenure = emiTenure;
	}

	public String getEmiAmount() {
		return emiAmount;
	}
	
	public void setEmiAmount(String emiAmount) {
		this.emiAmount = emiAmount;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	
	public String getLoanDestination() {
		return loanDestination;
	}


	public void setLoanDestination(String loanDestination) {
		this.loanDestination = loanDestination;
	}


	public String getProcessingFee() {
		return processingFee;
	}


	public void setProcessingFee(String processingFee) {
		this.processingFee = processingFee;
	}


	public String getBankDetails() {
		return bankDetails;
	}


	public void setBankDetails(String bankDetails) {
		this.bankDetails = bankDetails;
	}


	public String getAccountNumber() {
		return accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getLoanAmount() {
		return loanAmount;
	}


	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}



	@Override
	public int compareTo(ConsumerCreditTxnSummary o) {
		 Date txnDate1 = null, txnDate2;
	        try {
	            txnDate1 = formatter.parse(timestamp);
	        } catch (ParseException e) {
	            txnDate1 = new Date();
	        }
	        try {
	            txnDate2 = formatter.parse(o.getTimestamp());
	        } catch (ParseException e) {
	            txnDate2 = new Date();
	        }
	        return txnDate2.compareTo(txnDate1);
	}

}
