package com.freecharge.rest.user;

public class VoucherDetails {

    private String voucherId;

    private String businessEntity;

    private Double voucherAmount;

    private String voucherStatus;

    private String voucherCreatedOn;

    private Double voucherBalance;

    private String voucherExpiryDate;

    private String voucherExpiredOn;

    public String getBusinessEntity() {
        return businessEntity;
    }

    public void setBusinessEntity(String businessEntity) {
        this.businessEntity = businessEntity;
    }

    public Double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(Double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherCreatedOn() {
        return voucherCreatedOn;
    }

    public void setVoucherCreatedOn(String voucherCreatedOn) {
        this.voucherCreatedOn = voucherCreatedOn;
    }

    public Double getVoucherBalance() {
        return voucherBalance;
    }

    public void setVoucherBalance(Double voucherBalance) {
        this.voucherBalance = voucherBalance;
    }

    public String getVoucherExpiredOn() {
        return voucherExpiredOn;
    }

    public void setVoucherExpiredOn(String voucherExpiredOn) {
        this.voucherExpiredOn = voucherExpiredOn;
    }

    public String getVoucherExpiryDate() {
        return voucherExpiryDate;
    }

    public void setVoucherExpiryDate(String voucherExpiryDate) {
        this.voucherExpiryDate = voucherExpiryDate;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

}
