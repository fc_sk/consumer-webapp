package com.freecharge.rest.user.helper;

import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.common.util.FCSessionUtil;

public class RestSessionHelper {
	public static boolean validateUserSessionEmail(String userEmailId) throws InvalidPathValueException {
		boolean isValid = false;
		if (userEmailId != null && userEmailId.equals(FCSessionUtil.getLoggedInEmailId())){
			isValid = true;
		}
		if (!isValid){
			throw new InvalidPathValueException("freecharge.in", userEmailId, FCSessionUtil.getLoggedInEmailId());
		}
		return isValid;
	}
}
