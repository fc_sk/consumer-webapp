package com.freecharge.rest.user;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class OneCheckTransactionSummary implements Comparable<OneCheckTransactionSummary> {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String   txnId;
    private String   txnType;
    private String   businssTxnType;
    private Double   amount;
    private Double   withdrawalFee;
    private String   metaData;
    private String   merchantName;
    private String   transactionDate;
    private BigDecimal runningBalance; 
    private String idempotencyId;
    private String pgTransactionId;
    private String pgUsed;
    private String pgUnderlier;
	private Map<String, Object> contextMap;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
    
    public Double getWithdrawalFee() {
        return withdrawalFee;
    }

    public void setWithdrawalFee(Double withdrawalFee) {
        this.withdrawalFee = withdrawalFee;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getRunningBalance() {
		return runningBalance;
	}

	public void setRunningBalance(BigDecimal runningBalance) {
		this.runningBalance = runningBalance;
	}

	public String getIdempotencyId() {
		return idempotencyId;
	}

	public void setIdempotencyId(String idempotencyId) {
		this.idempotencyId = idempotencyId;
	}

	public String getPgTransactionId() {
		return pgTransactionId;
	}

	public void setPgTransactionId(String pgTransactionId) {
		this.pgTransactionId = pgTransactionId;
	}

	public String getPgUsed() {
		return pgUsed;
	}

	public void setPgUsed(String pgUsed) {
		this.pgUsed = pgUsed;
	}

	public String getPgUnderlier() {
		return pgUnderlier;
	}

	public void setPgUnderlier(String pgUnderlier) {
		this.pgUnderlier = pgUnderlier;
	}

	@Override
    public int compareTo(OneCheckTransactionSummary o) {
        Date txnDate1 = null, txnDate2;
        try {
            txnDate1 = formatter.parse(transactionDate);
        } catch (ParseException e) {
            txnDate1 = new Date();
        }
        try {
            txnDate2 = formatter.parse(o.getTransactionDate());
        } catch (ParseException e) {
            txnDate2 = new Date();
        }

        return txnDate2.compareTo(txnDate1);
    }

    public String getBusinssTxnType() {
        return businssTxnType;
    }

    public void setBusinssTxnType(String businssTxnType) {
        this.businssTxnType = businssTxnType;
    }

	public void setContextMap(Map<String, Object> contextMap) {
		this.contextMap = contextMap;
	}
	
	public Map<String, Object> getContextMap() {
		return contextMap;
	}
}
