package com.freecharge.rest.billpay;

import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.BilldeskTransactionBackFill.BBPSTransactionBackFillResponseDTO;
import com.freecharge.BilldeskTransactionBackFill.BillPayTxn;
import com.freecharge.BilldeskTransactionBackFill.BillPayTxnList;
import com.freecharge.BilldeskTransactionBackFill.BilldeskBackfillTypes;
import com.freecharge.BilldeskTransactionBackFill.BilldeskTransactionBackFillService;
import com.freecharge.BilldeskTransactionBackFill.ProcessFFDTO;
import com.freecharge.BilldeskTransactionBackFill.UpdateReconMetaDTO;
import com.freecharge.app.domain.dao.ProductMasterDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.PreAGHitNotificationService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.fulfilment.service.SkippedFFTaskSchedulerService;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.webdo.BBPSTransactionDetails;

@Controller
@RequestMapping("/internal/*")
public class BillpayInternalResourceController {
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
    private BillPaymentService billPaymentService;

    @Autowired
	private BilldeskTransactionBackFillService billdeskTransactionBackFillService;
    
    @Autowired
    private FulfillmentService   fulfillmentService;  
    
    @Autowired
    private RechargeInitiateService rechargeInitiateService;
    
    @Autowired
    private PreAGHitNotificationService   preHitService;  
    
    @Autowired
    private SkippedFFTaskSchedulerService skippedFFTaskSchedulerService;
    
	
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "updateunknowntxnsuccess", method = RequestMethod.GET)
	public @ResponseBody Boolean updateUnknowTxnSuccess(HttpServletRequest httpRequest) {
		Boolean result = false;
		logger.info("Reched into updateunknowntxnsuccess");
		try {
			String authenticator = httpRequest.getParameter("authenticator");
			String date = httpRequest.getParameter("date");
			return billPaymentService.updateUnknownToSuccessTxn(authenticator,date );
		} catch (Exception e) {
			logger.error("Exception caught while updating success", e);
			return result;
		}
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "getOrderDetails/{reconId}", method = RequestMethod.GET)
    public @ResponseBody String fetchOrderDetailByReconId(@PathVariable String reconId, HttpServletResponse response, Model model, HttpServletRequest httpRequest) {
    	Map<String, String> result = rechargeInitiateService.getOrderDetailByReconId(reconId);
    	logger.info("Recevied request for get order details for recon Id : " + reconId);
    	if(result != null) {
    		return result.get("OrderId");
    	}
    	
    	return "Unable to find corresponding orderId for reconId:"+reconId;
    }
	
	@NoLogin
	@NoSessionWrite
	@RequestMapping(value = "updateunknowntxnfailure", method = RequestMethod.GET)
	public @ResponseBody Boolean updateUnknowTxnFailure(HttpServletRequest httpRequest) {
		Boolean result = false;
		logger.info("Reched into updateunknowntxnfailure");
		try {
			String date = httpRequest.getParameter("date");
			return billPaymentService.markUnknowTxnToFailure(date);
		} catch (Exception e) {
			logger.error("Exception caught while updating success", e);
			return result;
		}
	}

	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "processUnknownBdkTxn", method = RequestMethod.GET)
    public @ResponseBody
    BBPSTransactionBackFillResponseDTO processUnknownBdkTxn(HttpServletRequest httpRequest) {
        BBPSTransactionBackFillResponseDTO bbpsTransactionBackFillResponseDTO  = new BBPSTransactionBackFillResponseDTO();
        logger.info("Reched into processUnknownBdkTxn");
        try {
            bbpsTransactionBackFillResponseDTO = billdeskTransactionBackFillService.processUnknownBdkTxn();
            logger.info("BBPSTransactionBackFillResponseDTO "+bbpsTransactionBackFillResponseDTO.toString());
        } catch (Exception e) {
            logger.error("Exception caught while updating success", e);
            return bbpsTransactionBackFillResponseDTO;
        }
        return  bbpsTransactionBackFillResponseDTO;
    }
	
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "processUnknownBdkUtilitiesTxn/{processingDate}", method = RequestMethod.GET)
    public @ResponseBody
    BBPSTransactionBackFillResponseDTO processUnknownBdkUtilitiesTxn(HttpServletRequest httpRequest,@PathVariable String processingDate) {
        BBPSTransactionBackFillResponseDTO bbpsTransactionBackFillResponseDTO  = new BBPSTransactionBackFillResponseDTO();
        logger.info("Reched into processUnknownBdkTxn");
        try {
            bbpsTransactionBackFillResponseDTO = billdeskTransactionBackFillService.processBdkTxnV2(BilldeskBackfillTypes.UTILITIES, processingDate);
            logger.info("BBPSTransactionBackFillResponseDTO "+bbpsTransactionBackFillResponseDTO.toString());
        } catch (Exception e) {
            logger.error("Exception caught while updating success", e);
            return bbpsTransactionBackFillResponseDTO;
        }
        return  bbpsTransactionBackFillResponseDTO;
    }
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "processUnknownBdkBbpsUtilitiesTxn/{processingDate}", method = RequestMethod.GET)
    public @ResponseBody
    BBPSTransactionBackFillResponseDTO processUnknownBdkBbpsUtilitiesTxn(HttpServletRequest httpRequest,@PathVariable String processingDate) {
        BBPSTransactionBackFillResponseDTO bbpsTransactionBackFillResponseDTO  = new BBPSTransactionBackFillResponseDTO();
        logger.info("Reched into processUnknownBdkTxn");
        try {
            bbpsTransactionBackFillResponseDTO = billdeskTransactionBackFillService.processBdkTxnV2(BilldeskBackfillTypes.BBPS_UTILITIES, processingDate);
            logger.info("BBPSTransactionBackFillResponseDTO "+bbpsTransactionBackFillResponseDTO.toString());
        } catch (Exception e) {
            logger.error("Exception caught while updating success", e);
            return bbpsTransactionBackFillResponseDTO;
        }
        return  bbpsTransactionBackFillResponseDTO;
    }
	
	
	@NoLogin
	@NoSessionWrite
	@Csrf(exclude = true)
	@RequestMapping(value = "markPendingUtilitiesTxn", method = RequestMethod.POST)
	public @ResponseBody Map<String,String> markPendingUtilitiesTxn(@RequestBody BillPayTxnList billpayTxnList) {
		Map<String,String> result = null;
		logger.info("Reched into markUnknownTxn");		
	    if(billpayTxnList!=null && !CollectionUtils.isEmpty(billpayTxnList.getBillpayTxnList())){
	    	try {
				result = billdeskTransactionBackFillService.updateUtilitiesStatus(billpayTxnList);
			} catch (Exception e) {
				logger.error("Error Occurred in Updating value",e);
			}
	    }
	    return result;
	}
	
	@NoLogin
    @NoSessionWrite
    @RequestMapping(value = "processUnknownBdkPostpaidUtilitiesTxn/{processingDate}", method = RequestMethod.GET)
    public @ResponseBody
    BBPSTransactionBackFillResponseDTO processUnknownBdkPostpaidUtilitiesTxn(HttpServletRequest httpRequest,@PathVariable String processingDate) {
        BBPSTransactionBackFillResponseDTO bbpsTransactionBackFillResponseDTO  = new BBPSTransactionBackFillResponseDTO();
        logger.info("Reched into processUnknownBdkTxn");
        try {
            bbpsTransactionBackFillResponseDTO = billdeskTransactionBackFillService.processBdkTxnV2(BilldeskBackfillTypes.POSTPAID, processingDate);
            logger.info("BBPSTransactionBackFillResponseDTO "+bbpsTransactionBackFillResponseDTO.toString());
        } catch (Exception e) {
            logger.error("Exception caught while updating success", e);
            return bbpsTransactionBackFillResponseDTO;
        }
        return  bbpsTransactionBackFillResponseDTO;
    }
	


	@NoLogin
	@NoSessionWrite
    @Csrf(exclude = true)
	@RequestMapping(value = "backfill/ff", method = RequestMethod.POST)
	public @ResponseBody Map<String, Boolean> processFF(@RequestBody ProcessFFDTO params) {

		Map<String, Boolean> result = new HashMap<>();
		if (params == null || params.getOrderIds() == null || params.getCred() == null
				|| !params.getCred().equals(RechargeConstants.FF_RETRY_CRED)) {
			result.put("Access", false);
			return result;
		}
		logger.info("Reched into processFF for " + params);
		try {
			for (String order : params.getOrderIds()) {
				try {
					fulfillmentService.doFulfillment(order, true, true, false);
					result.put(order, true);

				} catch (Exception e) {
					result.put(order, false);
					logger.error("Exception caught while processFF", e);
				}
			}

		} catch (Exception e) {
			logger.error("Exception caught while processFF", e);
		}
		return result;
	}
	
	@NoLogin
	@NoSessionWrite
    @Csrf(exclude = true)
	@RequestMapping(value = "process/postFulfillmentTask/{durationInMinutes}", method = RequestMethod.POST)
	public @ResponseBody void processPostFFTasks(	@PathVariable int durationInMinutes) {
		logger.info("going to process tasks for past : "+durationInMinutes+" minutes");
		skippedFFTaskSchedulerService.pushSkippedFFRecords(0, durationInMinutes);
		logger.info("processed tasks for past : "+durationInMinutes+" minutes");
	}

	
	@NoLogin
	@NoSessionWrite
    @Csrf(exclude = true)
	@RequestMapping(value = "process/postFulfillmentTaskList", method = RequestMethod.POST)
	public @ResponseBody Map<String, Boolean> postFulfillmentTaskList(@RequestBody ProcessFFDTO params) {

		Map<String, Boolean> result = new HashMap<>();
		if (params == null || params.getOrderIds() == null || params.getCred() == null
				|| !params.getCred().equals(RechargeConstants.FF_RETRY_CRED)) {
			result.put("Access", false);
			return result;
		}
		logger.info("Reched into postFulfillmentTaskList for " + params);
		try {
			for (String order : params.getOrderIds()) {
				try {
					skippedFFTaskSchedulerService.enqueue(order);
					result.put(order, true);

				} catch (Exception e) {
					result.put(order, false);
					logger.error("Exception caught while processFF", e);
				}
			}

		} catch (Exception e) {
			logger.error("Exception caught while processFF", e);
		}
		return result;
	}
	
	@NoLogin
	@NoSessionWrite
	@Csrf(exclude = true)
	@RequestMapping(value = "process/agHitRcn", method = RequestMethod.POST)
	public @ResponseBody Map<String, Boolean> UpdateReconMetaForUtilities(@RequestBody UpdateReconMetaDTO params) {

		Map<String, Boolean> result = new HashMap<>();
		if (params == null || params.getOrderIdToReconIdMap() == null || params.getCred() == null
				|| !params.getCred().equals(RechargeConstants.FF_RETRY_CRED)) {
			result.put("Access", false);
			return result;
		}
		logger.info("Reched into UpdateReconMetaForUtilities for " + params);
		try {
			for (Map.Entry<String, String> payload : params.getOrderIdToReconIdMap().entrySet()) {
				try {
					fulfillmentService.UpdateReconMetaForUtilities(payload.getKey(), payload.getValue(), params.isForceUpdate());
					result.put(payload.getKey(), true);
				} catch (Exception e) {
					result.put(payload.getKey(), false);
					logger.error("Exception caught while UpdateReconMetaForUtilities", e);
				}
			}

		} catch (Exception e) {
			logger.error("Exception caught while UpdateReconMetaForUtilities", e);
		}
		return result;
	}
	
	@NoLogin
	@NoSessionWrite
    @Csrf(exclude = true)
	@RequestMapping(value = "process/preAgHit", method = RequestMethod.POST)
	public @ResponseBody Map<String, Boolean> processPreAgHitTxns(@RequestBody ProcessFFDTO params) {

		Map<String, Boolean> result = new HashMap<>();
		if (params == null || params.getOrderIds() == null || params.getCred() == null
				|| !params.getCred().equals(RechargeConstants.FF_RETRY_CRED)) {
			result.put("Access", false);
			return result;
		}
		logger.info("Reched into processPreAgHitTxns for " + params);
		try {
			for (String order : params.getOrderIds()) {
				try {
					preHitService.notifyPreAGHITTransactions(order);
					result.put(order, true);

				} catch (Exception e) {
					result.put(order, false);
					logger.error("Exception caught while processPreAgHitTxns", e);
				}
			}

		} catch (Exception e) {
			logger.error("Exception caught while processPreAgHitTxns", e);
		}
		return result;
	}
}
