package com.freecharge.rest.billpay.proxy;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCategoryMaster;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCircleMaster;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.beans.AddBillerResponse;
import com.freecharge.infrastructure.billpay.beans.BBPSBillTransactionDetails;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.infrastructure.billpay.beans.BillTransactionLedger;
import com.freecharge.infrastructure.billpay.beans.CreateBillPayRequest;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResultNew;
import com.freecharge.infrastructure.billpay.beans.GenericBillPayDetails;
import com.freecharge.infrastructure.billpay.beans.MetroValidationResponse;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.infrastructure.billpay.exception.BillPayValidationException;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;

@Service
public class BillPayServiceProxy implements IBillPayService {
    private IBillPayService  localBillPayService;
    private IBillPayService  remoteBillPayService;

    @Autowired
    private AppConfigService appConfigService;

    @PostConstruct
    public void init() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                "classpath:billpay-service-webapp-application-context.xml");
        localBillPayService = (IBillPayService) applicationContext.getBean("billPayApiService");
        ApplicationContext applicationContextClient = new ClassPathXmlApplicationContext(
                "classpath:billpay-service-client-beans.xml");
        remoteBillPayService = (IBillPayService) applicationContextClient.getBean("billPayApiServiceClient");
    }

    private IBillPayService getActiveBillPayService() {
        boolean isBillPayRemoting = appConfigService.isBillPayRemotingEnabled();

        IBillPayService activeBillPayService = localBillPayService;
        if (isBillPayRemoting) {
            activeBillPayService = remoteBillPayService;
        }
        return activeBillPayService;
    }

    @Override
    public BillWithSubscriberNumber createBillPay(CreateBillPayRequest createBillPayRequest, Integer userId,
            String email, String mobileNumber) throws BillPayValidationException, Exception {
        BillWithSubscriberNumber billWithSubscriberNumber = getActiveBillPayService().createBillPay(
                createBillPayRequest, userId, email, mobileNumber);
        return billWithSubscriberNumber;
    }

    @Override
    public FulfillBillResponse fulfillBill(String billId, String email, String mobileNumber) throws IOException,
            Exception {
        return getActiveBillPayService().fulfillBill(billId, email, mobileNumber);
    }

    @Override
    public FulfillBillResponse executeBillPayStatusCheck(String billId) throws ParseException {
        return getActiveBillPayService().executeBillPayStatusCheck(billId);
    }

    @Override
    public BillWithSubscriberNumber getBillDetails(String billId) {
        return getActiveBillPayService().getBillDetails(billId);
    }

    @Override
    public BillTransaction getBillTransaction(String billId) {
        return getActiveBillPayService().getBillTransaction(billId);
    }
    
    @Override
    public BBPSBillTransactionDetails getBillTransactionDetails(String billId) {
        return getActiveBillPayService().getBillTransactionDetails(billId);
    }

    @Override
    public List<BillTransactionLedger> getBillTransactionLedgers(String billId) {
        return getActiveBillPayService().getBillTransactionLedgers(billId);
    }

    @Override
    public List<Map<String, Object>> getPendingBillDataInRange(Date from, Date to) throws ParseException {
        return getActiveBillPayService().getPendingBillDataInRange(from, to);
    }

    @Override
    public List<BillerDetails> getBillPayOperatorMaster(String locationId) {
        return getActiveBillPayService().getBillPayOperatorMaster(locationId);
    }

    @Override
    public List<BillPayCategoryMaster> getBillPayCategoryMaster() {
        return getActiveBillPayService().getBillPayCategoryMaster();
    }

    @Override
    public AddBillerResponse addBiller(CreateBillPayRequest createBillPayRequest, Integer userId, String email,
            String mobileNo) throws BillPayValidationException, Exception {
        return getActiveBillPayService().addBiller(createBillPayRequest, userId, email, mobileNo);
    }

    @Override
    public List<ViewUserBillsResponse> getBillAndBillerForUser(Integer userId) throws ParseException {
        return getActiveBillPayService().getBillAndBillerForUser(userId);
    }

    @Override
    public void deleteBiller(Integer userId, String fcBillerId) throws Exception {
        getActiveBillPayService().deleteBiller(userId, fcBillerId);
    }

    @Override
    public void updateBillerNotification(Integer userId, String fcBillerId, Boolean notificationStatus)
            throws Exception {
        getActiveBillPayService().updateBillerNotification(userId, fcBillerId, notificationStatus);
    }

    @Override
    public Map<String, BillPayValidator> getBillPayValidators() {
        return getActiveBillPayService().getBillPayValidators();
    }

    @Override
    public BillPayValidator getBillPayValidatorForOperator(Integer operatorId, Integer circleId) {
        return getActiveBillPayService().getBillPayValidatorForOperator(operatorId, circleId);
    }

    @Override
    public List<BillPayCircleMaster> getBillPayCircleMasterList() {
        return getActiveBillPayService().getBillPayCircleMasterList();
    }

    @Override
    public FetchAndPayResult fetchOnlineBill(String sessionId, GenericBillPayDetails genericBillPayRequest) {
        return getActiveBillPayService().fetchOnlineBill(sessionId, genericBillPayRequest);
    }

    @Override
    public BillPayOperatorMaster getBillPayOperatorFromName(String operatorCode) {
        return getActiveBillPayService().getBillPayOperatorFromName(operatorCode);
    }

    @Override
    public MetroValidationResponse validateCard(String operatorId, String cardNum) {
        return getActiveBillPayService().validateCard(operatorId, cardNum);
    }

    @Override
    public Boolean reverseStatusToFailure(String billId) {
        return getActiveBillPayService().reverseStatusToFailure(billId);
    }

    @Override
    public List<BillPayOperatorMaster> getOperatorMastersFromProductId(Integer productId) {
        return getActiveBillPayService().getOperatorMastersFromProductId(productId);
    }

    @Override
    public void triggerBillPayDailyBatchJob(Calendar processingDate) {
        getActiveBillPayService().triggerBillPayDailyBatchJob(processingDate);
    }

	@Override
	public String getUniqueId() {
		return getActiveBillPayService().getUniqueId();
	}

	@Override
	public BillWithSubscriberNumber createBillPayNew(CreateBillPayRequest createBillPayRequest, String sessionId, Integer userId, String email,
			String mobileNo) throws BillPayValidationException, Exception {
		return getActiveBillPayService().createBillPayNew(createBillPayRequest, sessionId, userId, email, mobileNo);
	}

    @Override
    public FetchAndPayResult fetchOnlineBillWithEmailAndUserId(String session, Integer userId, String email,
            GenericBillPayDetails bbpd) {
        return getActiveBillPayService().fetchOnlineBillWithEmailAndUserId(session, userId, email, bbpd);
    }

    @Override
    public FetchAndPayResult fetchOnlineBillWithUserId(String session, Integer userId, GenericBillPayDetails gbpd) {
        return getActiveBillPayService().fetchOnlineBillWithUserId(session, userId, gbpd);
    }

    @Override
    public void triggerBillPayJob(Integer jobType) {
        getActiveBillPayService().triggerBillPayJob(jobType);
    }

	@Override
	public FulfillBillResponse fulfillBillNew(String billId, String email, String mobileNumber,Integer paymentType,String paymentOption, Map<String, Object> metaDataMap)
			throws IOException, Exception {
		return getActiveBillPayService().fulfillBillNew(billId, email,mobileNumber,paymentType,paymentOption, metaDataMap);
	}

	@Override
	public FetchAndPayResultNew fetchOnlineBillWithLastBill(FetchAndPayResult fetchAndPayResult, String email,
            GenericBillPayDetails bbpd) {
		return getActiveBillPayService().fetchOnlineBillWithLastBill(fetchAndPayResult, email, bbpd);
	}

	@Override
	public List<BillerDetails> getBillPayOperatorMasterWithChannelBlocking(String arg0, Integer arg1, Integer arg2) {
		return getActiveBillPayService().getBillPayOperatorMasterWithChannelBlocking(arg0, arg1, arg2);
	}

	@Override
	public Map<String, String> getBillPayGetInappDeepLinksForFCBillerIdList(List<String> fcBillerIdList) {
		return getActiveBillPayService().getBillPayGetInappDeepLinksForFCBillerIdList(fcBillerIdList);
	}

	@Override
	public ViewUserBillsResponse getUserBiller(String billId, Integer userId, String serviceNumber, String aggregator) throws Exception {
		return getActiveBillPayService().getUserBiller(billId, userId, serviceNumber, aggregator);
	}

	@Override
	public FulfillBillResponse updateUnknownBilldeskTxnSuccess(String billId) throws ParseException {
		return getActiveBillPayService().updateUnknownBilldeskTxnSuccess(billId);
	}

	@Override
	public FulfillBillResponse updateUnknownBilldeskTxnFailure(String billId) throws ParseException {
		return getActiveBillPayService().updateUnknownBilldeskTxnFailure(billId);
	}

}