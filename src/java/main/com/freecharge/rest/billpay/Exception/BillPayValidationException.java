package com.freecharge.rest.billpay.Exception;

import com.freecharge.api.error.ErrorCode;

public class BillPayValidationException extends Exception {
    private static final long serialVersionUID = 1L;
    
    private ErrorCode errorCode;
    
    public BillPayValidationException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
