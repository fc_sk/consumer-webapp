package com.freecharge.rest.billpay;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.cartcheckout.CreateCartTaskRequest;
import com.freecharge.cartcheckout.constants;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.client.CreateCartRequest;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.payments.ts.TaskScheduler;
import com.snapdeal.payments.ts.dto.TaskDTO;
import com.snapdeal.payments.ts.exception.DuplicateTaskException;
import com.snapdeal.payments.ts.exception.InvalidTaskTypeException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.AutoPayAppService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.FeeEngineService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.autopay.clientdo.AutoPaySchedule;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.fulfilment.FetchedBillService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.AddBillerResponse;
import com.freecharge.infrastructure.billpay.beans.CreateBillPayRequest;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResultNew;
import com.freecharge.infrastructure.billpay.beans.GenericBillPayDetails;
import com.freecharge.infrastructure.billpay.beans.MetroValidationResponse;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.infrastructure.billpay.exception.BillPayValidationException;
import com.freecharge.infrastructure.billpay.exception.BillPayValidationExceptionV2;
import com.freecharge.infrastructure.billpay.exception.DuplicateBillerException;
import com.freecharge.infrastructure.billpay.exception.FetchBillValidationException;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.types.GenericBillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.util.BillFactory;
import com.freecharge.infrastructure.billpay.util.BillPayConstants;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.BillPayDetails;
import com.freecharge.recharge.businessDo.BillPayRequestWebDo;
import com.freecharge.recharge.businessDo.RechargeResponseWebDo;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;
import com.freecharge.rest.common.FetchedBill;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.service.CommonHttpService;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.CalculateChargeRequest;
import com.freecharge.web.webdo.CalculateChargeResponse;
import com.freecharge.web.webdo.ProductPaymentWebDO;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

@Controller
@RequestMapping("/rest/bill/*")
public class BillPayResourceController extends AbstractRestController {

    private static final String USER_ID_TAG = "{userId}";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;

    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Autowired
    private CartService cartService;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private FetchedBillService fetchedBillService;
    
    @Autowired
    private CommonHttpService commonHttpService;
    
    @Autowired
    private BBPSDetailsDao bbpsDetailsDao;
    
    @Autowired
    private BillPaymentService billPaymentService;
    
    @Autowired
    private AutoPayAppService autoPayAppService;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    CartCheckoutServiceClientImpl cartClient;
    
    @Value("${fee.engine.endpoint.url}")
	private String feeEngineUrl;
    
    @Value("${fee.engine.client.name}")
	private String feeEngineClientName;
    @Value("${fee.engine.client.key}")
	private String feeEngineClientKey;

    @SuppressWarnings("serial")
    public static final ArrayList<String> addBillerCodes = new ArrayList<String>() {
        {
            add(BillPayConstants.BILLPAY_FC_SUCCESS_CODE);
            add(BillPayConstants.BILLPAY_FC_NO_DUE_BILL);
        }
    };

    private final String CALL_STATUS = "callStatus";
    private final String ERROR_CODE  = "errorCode";
    private final String ERROR_MSG   = "errorMessage";
    private final String SUCCESS     = "success";
    private final String FAILURE     = "failure";
    private final String LOOKUP_ID   = "lookupId";

    @RequestMapping(value = "/createBill", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> createBillPay(CreateBillPayRequest createBillPayRequest,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
                    throws com.freecharge.infrastructure.billpay.exception.BillPayValidationException,BillPayValidationExceptionV2, Exception {

        return createBillPayNew(createBillPayRequest, bindingResult, request, response);

    }

    private Map<String, String> createBillPayNew(CreateBillPayRequest createBillPayRequest, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response) {

        long start = System.currentTimeMillis();
        BillWithSubscriberNumber bsesBill;
        FetchAndPayResult fetchAndPayResult = null;
        try {
            Users user = userServiceProxy.getLoggedInUser();
            GenericBillPayDetails genericBillPayRequest =new GenericBillPayDetails();
            genericBillPayRequest.setAdditionalInfo1(createBillPayRequest.getAdditionalInfo1());
            genericBillPayRequest.setAdditionalInfo2(createBillPayRequest.getAdditionalInfo2());
            genericBillPayRequest.setAdditionalInfo3(createBillPayRequest.getAdditionalInfo3());
            genericBillPayRequest.setAdditionalInfo4(createBillPayRequest.getAdditionalInfo4());
            genericBillPayRequest.setAdditionalInfo5(createBillPayRequest.getAdditionalInfo5());
            genericBillPayRequest.setServiceProvider(createBillPayRequest.getServiceProvider());
            genericBillPayRequest.setServiceRegion(createBillPayRequest.getServiceRegion());
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            logger.info("CreateBillpayRequest: " + createBillPayRequest.toString());
            fetchAndPayResult = billPayServiceRemote.fetchOnlineBillWithEmailAndUserId(fs.getUuid(),
    				user.getUserId(), user.getEmail(), genericBillPayRequest);
            if(fetchAndPayResult == null ) {
            	return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
	                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
            }
            if (appConfigService.isCreateBillV2Enabled()) {
                logger.info("Using Create Bill Pay V2 method");
                if(appConfigService.isConvenienceFeeEnabled()) {
                	FetchedBill fetchedBill;
					try {
						fetchedBill = fetchedBillService.getFetchedBill(fs.getUuid(), createBillPayRequest.getServiceProvider(), createBillPayRequest.getAdditionalInfo1());
					} catch (Exception e) {
						return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
			                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
					}
                	if(fetchedBill != null && fetchedBill.getBillAmount() != null) {
                		createBillPayRequest.setBillAmount(fetchedBill.getBillAmount());
                	}else {
                		logger.info("Couldn't find this user in fetched bill table for sessionId: " + fs.getUuid());
                	}
                	
                }
                bsesBill = billPayServiceRemote.createBillPayNew(createBillPayRequest, fs.getUuid(), user.getUserId(),
                        user.getEmail(), user.getMobileNo());
            } else {
                bsesBill = billPayServiceRemote.createBillPay(createBillPayRequest, user.getUserId(), user.getEmail(),
                        user.getMobileNo());
            }
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        } catch (BillPayValidationExceptionV2 e){
        	logger.error("The error code is : [" + e.getErrorCode() + "] The error message is : [" + e.getErrorMessage() + "]");
            return failureResponse(e.getErrorCode(), e.getErrorMessage());
        } catch (BillPayValidationException e) {
        	return e.getErrorCode().restResponse();
        } catch (FetchBillValidationException e) {
            return failureResponse(e.getErrorCode(), e.getErrorMessage());
        } catch (Exception e) {
            logger.error("Caught exception on create bill ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        }
        
        try {
        	createNewBillTxnHomePageEntry(bsesBill);
        	 logger.info("Inserted In createNewBillTxnHomePageEntry");
        }catch(Exception e) {
        	logger.error("Exception In createNewBillTxnHomePageEntry{}",e);
        }
       
        metricsClient.recordLatency("Billpay", "CreateBill", System.currentTimeMillis() - start);
        String messageDetails = null;
        String customerName = null;
        String billPeriod = null;        
        if(fetchAndPayResult.getInfo() == null) {
        	logger.info("FetchAndPayInfo Result is null!!!");
        }else {
        	for(HashMap<String, String> dataMap : fetchAndPayResult.getInfo()) {
        		for (Map.Entry<String, String> entry : dataMap.entrySet()) {
        			if(entry.getKey().equals("messageDetails")) {
        				messageDetails = entry.getValue();
        				logger.info("MessageDetails: " +messageDetails);
        			}
        			if(entry.getKey().equals("customerName")) {
        				customerName = entry.getValue();
        			}
        			if(entry.getKey().equals("billPeriod")) {
                        billPeriod = entry.getValue();
                    }
        		}
        	}
        }
        logger.info("Final messageDetails: " +messageDetails);
        Boolean isBBPSActive = false;
        if(appConfigService.isBBPSEnabled() && isBbpsEnabledBiller(String.valueOf(createBillPayRequest.getFcBillerId()))) {
        	isBBPSActive = true;
        }
        saveBBPSDetails(fetchAndPayResult.getBillDueDate(), createBillPayRequest.getAdditionalInfo1(), messageDetails, customerName, bsesBill.getLookupId(), isBBPSActive, billPeriod);
        String productType = ProductName.fromProductId(bsesBill.getProductId()).getProductType();
        CreateCartRequest createCartRequest = new CreateCartTaskRequest();
        logger.info("creating cart request for createBill for lookupid: "+ bsesBill.getLookupId());
        createCartRequest.setCategoryName(FCConstants.productTypeToNameMap.get(productType));
        createCartRequest.setLookupId(bsesBill.getLookupId());
        createCartRequest.setProductType(ProductName.fromProductId(bsesBill.getProductId()).getProductType());
        createCartRequest.setAmount(createBillPayRequest.getBillAmount());
        createCartRequest.setAdditionalInfo1(createBillPayRequest.getAdditionalInfo1());
        createCartRequest.setAdditionalInfo2(createBillPayRequest.getAdditionalInfo2());
        createCartRequest.setAdditionalInfo3(createBillPayRequest.getAdditionalInfo3());
        createCartRequest.setAdditionalInfo4(createBillPayRequest.getAdditionalInfo4());
        createCartRequest.setAdditionalInfo5(createBillPayRequest.getAdditionalInfo5());
        logger.info("Setting userid for cart request "+ FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
        createCartRequest.setUserId((String) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
        logger.info("setting category name: "+ createCartRequest.getCategoryName());
        try{
                logger.info("Created cart request: " + createCartRequest.toString());
                logger.info("calling cart checkout service to create cart");
                CheckoutResponse checkoutResponse = cartClient.createCart(createCartRequest);
                logger.info("cart id returned from checkout" + checkoutResponse.getCheckoutId());

        }
        catch(Exception exp){

            logger.info("Unable to create cart in cartcheckout. Scheduling task scheduler for lookupid: "+createCartRequest.getLookupId());
            CreateCartTaskRequest createCartTaskRequest = (CreateCartTaskRequest) createCartRequest;
            String taskId = generateTaskId();
            createCartTaskRequest.setTaskId(taskId);
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setRequest(createCartTaskRequest);
            taskDTO.setTaskType(constants.CREATE_CART_TASK_TYPE);

            taskDTO.setCurrentScheduleTime(new Date());
            try {
                taskScheduler.submitTask(taskDTO);
            } catch (DuplicateTaskException e) {

                final String exceptionMsg =
                        String.format("DuplicateTaskException while creating cart, "
                                + "taskId: %s", taskId);
                logger.error(exceptionMsg, e);
            } catch (InvalidTaskTypeException e) {

                String exceptionMsg =
                        String.format(
                                "InvalidTaskTypeException while creating IMSWalletTask, "
                                        + " taskId: %s", taskId);
                logger.error(exceptionMsg, e);
                throw e;
            }
        }

        return successResponse(bsesBill.getLookupId());
    }
    
    private void saveBBPSDetails(String billDueDate, String customerAccountNumber, String messageDetails, String customerName, String lookupId, Boolean isBBPSActive, String billPeriod) {
    	BBPSTransactionDetails bbpsDetails = new BBPSTransactionDetails();
    	
    	bbpsDetails.setCustomerName(customerName);
    	bbpsDetails.setCustomerAccountNumber(customerAccountNumber);
    	bbpsDetails.setLookupId(lookupId);
    	bbpsDetails.setBillDueDate(billDueDate);
    	bbpsDetails.setMessageDetails(messageDetails);
    	bbpsDetails.setBbpsReferenceNumber(null);
    	bbpsDetails.setBbpsMetaData(null);
    	bbpsDetails.setAggregatorTxnId(null);
    	bbpsDetails.setIsBbpsActive(isBBPSActive);
    	bbpsDetails.setBillPeriod(billPeriod);
    	bbpsDetailsDao.insertBBPSDetails(bbpsDetails);
    	
    }
    
    private Boolean isBbpsEnabledBiller(String billerIdRequest) {
    	String bbpsEnabledBillersStr = appConfigService.getBBPSEnabledBillers();
    	List<String> bbpsEnabledBillersList = Arrays.asList(bbpsEnabledBillersStr.split("[,]"));
    	for(String billerId : bbpsEnabledBillersList) {
    		if(billerIdRequest.equals(billerId)) {
    			return true;
    		}
    	}
    	return false;
    }

    @RequestMapping(value = "/v2/createBill", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> createBillPayV2(CreateBillPayRequest createBillPayRequest,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
                    throws com.freecharge.infrastructure.billpay.exception.BillPayValidationException,BillPayValidationExceptionV2, Exception {
        logger.info("Has hit the api /v2/createBill");
        logger.info("Create bill request : " + createBillPayRequest.toString());
        Map<String, String> returnMap = createBillPayNew(createBillPayRequest, bindingResult, request, response);
        if (SUCCESS.equals(returnMap.get(CALL_STATUS))) {
            String lookupId = returnMap.get(LOOKUP_ID);
            logger.info("About to create cart for lookupId:" + lookupId);
            try {
				cartService.createCartByLookupId(lookupId);
			} catch (ServiceUnavailableException e) {
				return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
	                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
			}
        }
        return returnMap;
    }

    @RequestMapping(value = "/addBiller", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> addBiller(CreateBillPayRequest createBillPayRequest,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response)
                    throws com.freecharge.infrastructure.billpay.exception.BillPayValidationException, Exception {
        long start = System.currentTimeMillis();
        AddBillerResponse addBillerResponse;
        try {
            Users user = userServiceProxy.getLoggedInUser();
            addBillerResponse = billPayServiceRemote.addBiller(createBillPayRequest, user.getUserId(), user.getEmail(),
                    user.getMobileNo());
            if (!addBillerResponse.getIsSuccess()) {
                return failureResponse(addBillerResponse.getErrorCode(), addBillerResponse.getErrorMessage());
            }
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        } catch (BillPayValidationException e) {
            return e.getErrorCode().restResponse();
        } catch (DuplicateBillerException e) {
            Map<String, String> resp = failureResponse(e.getErrorCode(), null);
            resp.put("fcBillerId", e.getfcBillerId());
            return resp;
        } catch (Exception e) {
            logger.error("Caught exception on add biller ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        }
        Map<String, String> resp = successResponse();
        resp.put("billerAccountId", addBillerResponse.getBillerAccountId());
        metricsClient.recordLatency("Billpay", "AddBiller", System.currentTimeMillis() - start);
        return resp;
    }

    @RequestMapping(value = "/fetchBill", method = RequestMethod.POST, produces = "application/json")
    @Csrf(exclude = true)
    @NoLogin
    public @ResponseBody String fetchBill(GenericBillPayDetails genericBillPayRequest, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response) {
        logger.info("Fetch bill requested for info1 " + genericBillPayRequest.getAdditionalInfo1()
                + " and service provider " + genericBillPayRequest.getServiceProvider());
        long start = System.currentTimeMillis();
        FetchAndPayResult fetchAndPayResult = null;
        Gson gosGson = new Gson();
        Users user = new Users();
        String sessionId = "-1";
        try {
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            user = userServiceProxy.getLoggedInUser();
            sessionId = fs.getUuid();
            if (user == null) {
//            	if(appConfigService.isConvenienceFeeEnabled()) {
            		fetchAndPayResult = billPayServiceRemote.fetchOnlineBill(fs.getUuid(), genericBillPayRequest);
            		logger.info("fetchAndPayResult : " + fetchAndPayResult.toString());
//            	}
            } else {
            		logger.info("Calling this function");
            		fetchAndPayResult = billPayServiceRemote.fetchOnlineBillWithEmailAndUserId(fs.getUuid(),
            				user.getUserId(), user.getEmail(), genericBillPayRequest);
            		logger.info("fetchAndPayResult : " + fetchAndPayResult.toString());
            }
            if (fetchAndPayResult == null) {
                fetchAndPayResult = new FetchAndPayResult();
                fetchAndPayResult.setErrorCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
                fetchAndPayResult.setErrorMessage(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
                logger.error("fetchAndPayResult is null");
            }
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException on fetch bill ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        } catch (Exception e) {
            logger.error("Caught exception on fetch bill ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        }
        List<HashMap<String, String>> info = fetchAndPayResult.getInfo();
        if(appConfigService.isConvenienceFeeEnabled()) {
        	logger.info(" it is true");
        }else {
        	logger.info("it is false");
        }
        Float convenienceFee = 0F;
        Float totalAmount = 0F;
        HashMap<String, String> convienceFee = new HashMap();
        HashMap<String, String> totalAmountMap = new HashMap();
        if(appConfigService.isConvenienceFeeEnabled()) {
        	logger.info("C Fee is enabled");
        }
        if(appConfigService.isConvenienceFeeEnabled() && fetchAndPayResult != null && 
        		!fetchAndPayResult.getErrorCode().equals(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE) && fetchAndPayResult.getBillAmount() != null) {
        	
            
            try {
            	FeeEngineService feeService = new FeeEngineService(feeEngineUrl,feeEngineClientName, feeEngineClientKey);
            	logger.info("feeEngineUrl: " + feeEngineUrl);
            	logger.info("feeEngineClientName" + feeEngineClientName);
            	logger.info("feeEngineClientKey" + feeEngineClientKey);
            	CalculateChargeRequest feeRequest = new CalculateChargeRequest();
//    			feeRequest.setCategoryId("CatagoryId"); //Optional
            	
            	
            	logger.info("billerId: " + genericBillPayRequest.getServiceProvider());
            	JSONObject json = appConfigService.getCustomProp(AppConfigService.MERCHANT_ID_MAPPING);
            	logger.info("json : " + json.toJSONString());
            	if(json.containsKey(genericBillPayRequest.getServiceProvider())) {
                    String temp = String.valueOf(json.get(genericBillPayRequest.getServiceProvider()));
                }else {
                	logger.info("json doesn't contain biller details");
                }
            	
            	
            	String merchantId=appConfigService.getChildProperty(AppConfigService.MERCHANT_ID_MAPPING,String.valueOf(genericBillPayRequest.getServiceProvider()));
            	feeRequest.setMerchantId(merchantId);
//    			feeRequest.setPartnerId("partnerId"); //Optional
//    			feeRequest.setPlatformId("platformId"); //Optional
            	feeRequest.setTransactionTime(new Date());
            	feeRequest.setTxnAmount(BigDecimal.valueOf(fetchAndPayResult.getBillAmount()));
            	logger.info("Fee Request: " + feeRequest.toString());

            	CalculateChargeResponse feeResponse = feeService.calculateFeeCharge(feeRequest);
    			if(feeResponse != null) {
    				totalAmount = fetchAndPayResult.getBillAmount();
    				convenienceFee = feeResponse.getFeeCharge().getFeeAmount().floatValue();
    				totalAmountMap.put("name", "Bill");
    		        totalAmountMap.put("value", String.valueOf(totalAmount));
    		        info.add(totalAmountMap);
    		        JSONObject json1 = appConfigService.getCustomProp(AppConfigService.CONVENIENCE_FEE_TITLE);
    		        
    		        logger.info("json1 : " + json1.toJSONString());
                	if(json.containsKey("title")) {
                        String temp = String.valueOf(json.get("title"));
                    }else {
                    	logger.info("json doesn't contain title details");
                    }
    		        
    		        String title=appConfigService.getChildProperty(AppConfigService.CONVENIENCE_FEE_TITLE,"title");
    		        convienceFee.put("name", title);
    		        convienceFee.put("value" , String.valueOf(convenienceFee));
    		        info.add(convienceFee);
    		        fetchAndPayResult.setBillAmount(fetchAndPayResult.getBillAmount() + convenienceFee);
    			}else {
    				logger.error("Got null from fee engine for user: " + user.getEmail());
    				fetchAndPayResult = new FetchAndPayResult();
                	fetchAndPayResult.setErrorCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
                	fetchAndPayResult.setErrorMessage(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
    			}
            } catch (Exception e) {
            	logger.error("Exception caught while calling feeService for userId : " + user.getUserId(),e );
            	fetchAndPayResult = new FetchAndPayResult();
            	fetchAndPayResult.setErrorCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
            	fetchAndPayResult.setErrorMessage(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
            }
           
            

        }
        
        if(fetchAndPayResult != null && fetchAndPayResult.getInfo() != null && !fetchAndPayResult.getErrorCode().equals(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE)) {
        	List<HashMap<String, String>> finalInfo = fetchAndPayResult.getInfo();
        	for(int i = 0; i< finalInfo.size() ; i++) {
        		HashMap<String, String> hMap = finalInfo.get(i);
        		if(hMap.containsKey("messageDetails")) {
        			finalInfo.remove(hMap);
        		}
        		if(hMap.containsKey("customerName")) {
        			finalInfo.remove(hMap);
        		}
        	}
        }
        
        String resp = gosGson.toJson(fetchAndPayResult);
        if(appConfigService.isFetchBillNewEnabled()&& user!=null){
        	FetchAndPayResultNew fetchAndPayResultNew = billPayServiceRemote.fetchOnlineBillWithLastBill(fetchAndPayResult,
    				user.getEmail(), genericBillPayRequest);
        	logger.info("fetchAndPayResultNew : " + fetchAndPayResultNew.toString());
        	resp = gosGson.toJson(fetchAndPayResultNew);
        }		
        logger.info("Fetch bill response for info1 " + genericBillPayRequest.getAdditionalInfo1() + " is " + resp);
        addBillerIfLoggedIn(fetchAndPayResult, genericBillPayRequest, request);
//        FetchedBill fetchedBill = populateFetchedBill(fetchAndPayResult, sessionId, genericBillPayRequest);
//        Boolean saveFetchedBillResult = fetchedBillService.insertFetchedBill(fetchedBill);
//        if(!saveFetchedBillResult) {
//        	fetchAndPayResult = new FetchAndPayResult();
//        	fetchAndPayResult.setErrorCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
//        	fetchAndPayResult.setErrorMessage(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
//        	resp = gosGson.toJson(fetchAndPayResult);
//        	return resp;
//        }
        metricsClient.recordLatency("Billpay", "FetchBill", System.currentTimeMillis() - start);
        return resp;
        
        
    }
    
    private FetchedBill populateFetchedBill(FetchAndPayResult fetchAndPayResult, String sessionId, GenericBillPayDetails genericBillPayRequest) {
    	FetchedBill fetchedBill = new  FetchedBill();
    	if(genericBillPayRequest.getServiceProvider() != null) {
    		fetchedBill.setBillerId(genericBillPayRequest.getServiceProvider());
    	}
    	if(genericBillPayRequest.getAdditionalInfo1() != null) {
    		fetchedBill.setAccountId(genericBillPayRequest.getAdditionalInfo1());
    	}
    	if(fetchAndPayResult.getBillAmount() != null) {
    		fetchedBill.setBillAmount(fetchAndPayResult.getBillAmount());
    	}
    	
    	fetchedBill.setSessionId(sessionId);
    	return fetchedBill;
    	
    }
      

    private void addBillerIfLoggedIn(FetchAndPayResult fetchAndPayResult, GenericBillPayDetails genericBillPayRequest,
            HttpServletRequest request) {
        try {
            String channelRequestId = request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY);
            int channelId = FCConstants.CHANNEL_ID_WEB;
            if (channelRequestId != null) {
                channelId = Integer.parseInt(channelRequestId);
            }
            if (!MobileUtil.isAppChannel(channelId)) {
                return;
            }
            if (fetchAndPayResult != null && addBillerCodes.contains(fetchAndPayResult.getErrorCode())) {
                Users user = userServiceProxy.getLoggedInUser();
                if (user != null) {
                    CreateBillPayRequest createBillPayRequest = new CreateBillPayRequest();
                    createBillPayRequest.setAdditionalInfo1(genericBillPayRequest.getAdditionalInfo1());
                    createBillPayRequest.setAdditionalInfo2(genericBillPayRequest.getAdditionalInfo2());
                    createBillPayRequest.setAdditionalInfo3(genericBillPayRequest.getAdditionalInfo3());
                    createBillPayRequest.setAdditionalInfo4(genericBillPayRequest.getAdditionalInfo4());
                    createBillPayRequest.setAdditionalInfo5(genericBillPayRequest.getAdditionalInfo5());
                    createBillPayRequest.setServiceProvider(genericBillPayRequest.getServiceProvider());
                    createBillPayRequest.setServiceRegion(genericBillPayRequest.getServiceRegion());
                    billPayServiceRemote.addBiller(createBillPayRequest, user.getUserId(), user.getEmail(),
                            user.getMobileNo());
                }
            }
        } catch (Exception e) {
            logger.error("Failed to add biller.", e);
        }
    }

    @RequestMapping(value = "{fcBillerId}/deleteBiller", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> deleteBiller(@PathVariable String fcBillerId) throws Exception {
        long start = System.currentTimeMillis();
        try {
            Integer userId = userServiceProxy.getLoggedInUser().getUserId();
            billPayServiceRemote.deleteBiller(userId, fcBillerId);
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        } catch (Exception e) {
            logger.error("Failed to delete biller for billerAccountId : " + fcBillerId, e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        }
        metricsClient.recordLatency("Billpay", "DeleteBiller", System.currentTimeMillis() - start);
        return successResponse();
    }

    @RequestMapping(value = "{fcBillerId}/notifications/{notificationStatus}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> updateBillerNotification(@PathVariable String fcBillerId,
            @PathVariable Boolean notificationStatus) throws Exception {
        long start = System.currentTimeMillis();
        try {
            Integer userId = userServiceProxy.getLoggedInUser().getUserId();
            billPayServiceRemote.updateBillerNotification(userId, fcBillerId, notificationStatus);
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        } catch (Exception e) {
            logger.error("Failed to delete biller for billerAccountId : " + fcBillerId, e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE);
        }
        metricsClient.recordLatency("Billpay", "updateBillerNotification", System.currentTimeMillis() - start);
        return successResponse();
    }

    @RequestMapping(value = "/billDetails", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String getBillDetails() throws Exception {
        long start = System.currentTimeMillis();
        List<ViewUserBillsResponse> billDetails;
        List<String> jsonStrings = new ArrayList<String>();
        Gson gosGson = new Gson();
        try {
            Integer userId = userServiceProxy.getLoggedInUser().getUserId();
            billDetails = billPayServiceRemote.getBillAndBillerForUser(userId);
            if (billDetails == null) {
                billDetails = new ArrayList<>();
            }
            serilaiseAndAddAutoPayMetadata(billDetails, jsonStrings,userId);
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        } catch (Exception e) {
            logger.error("Caught exception on fetching bill details ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        }
        metricsClient.recordLatency("Billpay", "getBillDetails", System.currentTimeMillis() - start);
        return !jsonStrings.isEmpty() ? jsonStrings.toString() : gosGson.toJson(billDetails);
    }

    private void serilaiseAndAddAutoPayMetadata(List<ViewUserBillsResponse> billDetails, List<String> jsonStrings,int userId) {
        try {
            List<String> serviceNumbers = new ArrayList<>();
            for (ViewUserBillsResponse response : billDetails) {
                serviceNumbers.add(response.getAdditionalInfo1());
            }
            HashMap<String, String> headers = new HashMap<String,String>();
            headers.put("Content-Type","application/json");
            CommonHttpService.HttpResponse response = commonHttpService.fireGetRequestWithTimeOutAndCustomHeader(
                    fcProperties.getAutoPayListUrl().replace(USER_ID_TAG, String.valueOf(userId)), null, 2000, headers);
            Type type = new TypeToken<List<AutoPaySchedule>>(){}.getType();
            List<AutoPaySchedule> autopaySchedules = new Gson().fromJson(response.getResponseBody(), type);
            for (ViewUserBillsResponse bill : billDetails) {
                Gson gson = new Gson();
                JsonElement jsonElement = gson.toJsonTree(bill);
                for(AutoPaySchedule schedule:autopaySchedules){
                    if (schedule.getBillPayDetails() != null && schedule.getBillPayDetails().getServiceNumber()
                            .equalsIgnoreCase(bill.getAdditionalInfo1())) {
                        jsonElement.getAsJsonObject().add("autopay", gson.toJsonTree(schedule));
                    }
                }
                jsonStrings.add(gson.toJson(jsonElement));
            }
        } catch (Exception ex) {
            logger.error("Error fetching autopay details.", ex);
            jsonStrings.clear();
        }
    }

    @RequestMapping(value = "metro/{operatorId}/{cardNum}/validate", produces = "application/json")
    @NoLogin
    @Csrf(exclude = true)
    public @ResponseBody String metroCardValidate(@PathVariable String operatorId, @PathVariable String cardNum)
            throws Exception {
        long start = System.currentTimeMillis();
        MetroValidationResponse validationResponse;
        Gson gosGson = new Gson();
        try {
            validationResponse = billPayServiceRemote.validateCard(operatorId, cardNum);
        } catch (RemoteConnectFailureException e) {
            logger.error("Caught RemoteConnectFailureException ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_REQUEST_TIMED_OUT,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        } catch (Exception e) {
            logger.error("Caught exception on validate card ", e);
            return failureResponse(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE,
                    BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_MESSAGE).toString();
        }
        metricsClient.recordLatency("Billpay", "ValidateMetroCard", System.currentTimeMillis() - start);
        return gosGson.toJson(validationResponse);
    }

    private Map<String, String> successResponse(String lookupID) {
        Map<String, String> map = new HashMap<>();
        map.put(CALL_STATUS, SUCCESS);
        map.put(LOOKUP_ID, lookupID);
        return map;
    }

    private Map<String, String> successResponse() {
        Map<String, String> map = new HashMap<>();
        map.put(CALL_STATUS, SUCCESS);
        return map;
    }

    private Map<String, String> failureResponse(String errorCode, String errorMessage) {
        Map<String, String> map = new HashMap<>();
        map.put(CALL_STATUS, FAILURE);
        if (errorCode != null && !errorCode.isEmpty()) {
            map.put(ERROR_CODE, errorCode);
        }
        if (errorMessage != null && !errorMessage.isEmpty()) {
            map.put(ERROR_MSG, errorMessage);
        }
        return map;
    }

    private TxnHomePage createNewBillTxnHomePageEntry(BillWithSubscriberNumber eb) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        fs.setRechargePlanType(RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE);
        TxnHomePage txnHomePage = new TxnHomePage();
        txnHomePage.setSessionId(fs.getUuid());
        txnHomePage.setLookupId(eb.getLookupId());

        txnHomePage.setOperatorName(eb.getOperatorName());
        txnHomePage.setCircleName(eb.getCircleName());
        txnHomePage.setServiceNumber(eb.getBillId());
        txnHomePage.setAmount(eb.getBillAmount());
        ProductName productName = ProductName.fromProductId(eb.getProductId());
        txnHomePage.setProductType(productName.getProductType());
        txnHomePage.setFkProductId(productName.getProductId());
        txnHomePage.setCreatredOn(new Timestamp(System.currentTimeMillis()));
        txnHomePage.setFkCircleId(eb.getCircleId());
        txnHomePage.setFkOperatorId(eb.getOperatorId());

        txnHomePageService.saveTransactionHomePage(txnHomePage);

        // save in-txn-data
        HashMap<String, Object> transactionInfo = new HashMap<String, Object>();
        transactionInfo.put(InTxnDataConstants.RECHARGE_AMOUNT, eb.getBillAmount());

        transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, eb.getCircleName());
        transactionInfo.put(InTxnDataConstants.OPERATOR_NAME,
                billPayServiceRemote.getBillPayOperatorFromName(eb.getOperatorName()).getName());
        transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, eb.getCircleId());
        transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, eb.getOperatorId());
        transactionInfo.put(InTxnDataConstants.PRODUCT_TYPE,
                FCConstants.productTypeToNameMap.get(productName.getProductType()));
        transactionInfo.put(InTxnDataConstants.SERVICE_NUMBER, eb.getAdditionalInfo1());
        transactionInfo.put(InTxnDataConstants.SESSION_ID, fs.getUuid());
        try {
            campaignServiceClient.saveInTxnData1(eb.getLookupId(), FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID,
                    transactionInfo);
        } catch (Exception e) {
            logger.error("Error saving intxndata in BillpayResourceController: ", e);
        }

        return txnHomePage;
    }

	@Csrf(exclude = true)
	@NoLogin
	@NoSessionWrite
	@Authenticate(api = "autoPay.recharge")
	@RequestMapping(value = "autoPayBill/getBillDetails/{orderId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody BillPayDetails getBillerMetadata(@PathVariable String orderId) {
		UserTransactionHistory userTransactionHistory = userTransactionHistoryService
				.findUserTransactionHistoryByOrderId(orderId);
		String fcBillerId = orderService.getBillIdForOrderId(orderId, userTransactionHistory.getProductType());
		BillWithSubscriberNumber bill = billPayServiceRemote.getBillDetails(fcBillerId);
		GenericBillWithSubscriberNumber billWithSubscriberNumber = BillFactory.covertToGenericBill(
				bill.getSerializedBillObject(), bill.getBillId());
		BillPayDetails billPayDetails = new BillPayDetails();
		billPayDetails.setBillWithSubscriberNumber(billWithSubscriberNumber);
		billPayDetails.setOperatorName(userTransactionHistory.getServiceProvider());
		billPayDetails.setCircleName(userTransactionHistory.getServiceRegion());
		billPayDetails.setOperatorId(billWithSubscriberNumber.getOperatorId());
		billPayDetails.setCircleId(billWithSubscriberNumber.getCircleId());
		billPayDetails.setAmount(billWithSubscriberNumber.getBillAmount());
		Users user = userServiceProxy.getUserFromOrderId(orderId);
		billPayDetails.setUserMobileNumber(user.getMobileNo());
		billPayDetails.setUserEmailId(user.getEmail());
		billPayDetails.setUserName(userServiceProxy.getUserProfile(user.getUserId()).getFirstName());
		return billPayDetails;
	}
	
//	@Csrf(exclude = true)
	
	
    @Csrf(exclude = true)
    @NoLogin
    @NoSessionWrite
    @Authenticate(api = "autoPay.recharge")
    @RequestMapping(value = "autoPayBill", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody RechargeResponseWebDo autoPayBill(@RequestBody BillPayRequestWebDo billPayRequestWebDo,
            HttpServletRequest request, HttpServletResponse response){
        sessionManager.generateMockSessionForAutopay(billPayRequestWebDo.getUserId(), request, response);
        request.setAttribute(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY, String.valueOf(FCConstants.CHANNEL_ID_AUTO_PAY));
        RechargeResponseWebDo rechargeResponseWebDo = new RechargeResponseWebDo();
        try {          
            
            String errorCode = autoPayAppService.validateBalance(billPayRequestWebDo.getCreateBillPayRequest().getBillAmount(), billPayRequestWebDo.getUserId());
            if (errorCode != null) {
            	rechargeResponseWebDo.setStatusCode(errorCode);
                return rechargeResponseWebDo;
            }
            
            Map<String, String> returnMap = createBillPayNew(billPayRequestWebDo.getCreateBillPayRequest(),
                    new BeanPropertyBindingResult(new Object(), ""), request, response);
            logger.info(returnMap);
            if (!SUCCESS.equals(returnMap.get(CALL_STATUS))) {
                return rechargeResponseWebDo;
            }
            String lookupId = returnMap.get(LOOKUP_ID);
            try {
				cartService.createCartByLookupId(lookupId);
			} catch (Exception e) {
				logger.error("Exception caught while createCartByLookupId for lookupId: " + lookupId,e);
			}
            
            autoPayAppService.insertRequestLookupMap(billPayRequestWebDo.getRequestId(), lookupId);

            // Now start the payment through wallet.
            String productId = Integer
                    .toString(ProductName.fromPrimaryProductType(billPayRequestWebDo.getProductType()).getProductId());

            ProductPaymentWebDO productPaymentWebDO = new ProductPaymentWebDO();
            // Only Wallet Payment is accepted.
            productPaymentWebDO.setLookupID(lookupId);
            productPaymentWebDO.setPaymenttype(PaymentConstants.PAYMENT_TYPE_WALLET);
            productPaymentWebDO.setProductType(billPayRequestWebDo.getProductType());
            productPaymentWebDO.setProductId(productId);

            // Complete the payment.
            Map<String, Object> paymentResponse = paymentService.doCompletePayment(productPaymentWebDO,
                    new BeanPropertyBindingResult(new Object(), ""), request, response, "fcBalance");
            String orderId = null;
            if (!FCUtil.isEmpty(paymentResponse)) {
                orderId = (String) paymentResponse.get(PaymentConstants.ORDER_ID);
            }
            rechargeResponseWebDo = new RechargeResponseWebDo();
            if (FCUtil.isEmpty(orderId)) {
                rechargeResponseWebDo.setStatusCode(RechargeConstants.PAYMENT_ERROR);
                return rechargeResponseWebDo;
            }
            RechargeDetails details = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
            rechargeResponseWebDo.setOrderId(orderId);
            rechargeResponseWebDo.setStatusCode(details.getTransactionStatusCode());
        } catch (Exception ex) {
            logger.error("Error doing autopay  for bill with service number:"
                    + billPayRequestWebDo.getCreateBillPayRequest().getAdditionalInfo1(), ex);
            rechargeResponseWebDo.setStatusCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
        }
        return rechargeResponseWebDo;
    }

    private String generateTaskId(){
        return UUID.randomUUID().toString() + new Timestamp(System.currentTimeMillis());
    }

}
