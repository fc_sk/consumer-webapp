package com.freecharge.rest.versionUpgrade;

public class ResultAppDetails {
	private boolean forceUpdate;
	private int newAppVersion;
	private String webMessageUrl;
	
	public boolean isForceUpdate() {
		return forceUpdate;
	}
	public void setForceUpdate(boolean forceUpdate) {
		this.forceUpdate = forceUpdate;
	}
	public int getNewAppVersion() {
		return newAppVersion;
	}
	public void setNewAppVersion(int newAppVersion) {
		this.newAppVersion = newAppVersion;
	}
	public String getWebMessageUrl() {
		return webMessageUrl;
	}
	public void setWebMessageUrl(String webMessageUrl) {
		this.webMessageUrl = webMessageUrl;
	}
	public String toString(){
		return "ResultAppDetails[forceUpdate = "+forceUpdate+", newAppVersion = "+newAppVersion+
				",webMessageUrl = "+webMessageUrl+"]";
	}

}
