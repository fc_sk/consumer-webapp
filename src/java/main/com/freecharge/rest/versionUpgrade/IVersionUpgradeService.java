package com.freecharge.rest.versionUpgrade;

public interface IVersionUpgradeService {
	public ResultAppDetails getAppUpgradeDetails(AppDetails appDetails);
}
