package com.freecharge.rest.versionUpgrade;

import java.io.Serializable;

public class AppVersion implements Serializable{
	private int minAppVersion;
	private int newAppVersion;
	private int channelId;
	private String webMessageUrl;
	private boolean isActive;
	
	public int getMinAppVersion() {
		return minAppVersion;
	}
	public void setMinAppVersion(int minAppVersion) {
		this.minAppVersion = minAppVersion;
	}
	
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public String getWebMessageUrl() {
		return webMessageUrl;
	}
	public void setWebMessageUrl(String webMessageUrl) {
		this.webMessageUrl = webMessageUrl;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public int getNewAppVersion() {
		return newAppVersion;
	}
	public void setNewAppVersion(int newAppVersion) {
		this.newAppVersion = newAppVersion;
	}
	
	

}
