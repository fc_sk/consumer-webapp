package com.freecharge.rest.versionUpgrade;

import java.util.List;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.AppConfigService;
import com.google.gson.Gson;

@Service
public class VersionUpgradeService implements IVersionUpgradeService {
	private static final Logger logger = LoggerFactory.getLogger(VersionUpgradeService.class);

	@Autowired
	 private AppConfigService appConfigService;

	@Override
	public ResultAppDetails getAppUpgradeDetails(AppDetails appDetails) {
		ResultAppDetails resultAppDetails = new ResultAppDetails();
		JSONObject obj = appConfigService.getForceUpgradeDetails();
		if(obj== null){
			logger.debug("Failure in appConfiguration");
			return resultAppDetails;
		}
		Gson gson =new Gson();
		if(obj.keySet()!=null){
			for(Object channelId : obj.keySet())
			{
				if(Integer.parseInt((String)channelId) == appDetails.getChannelId())
				{	
					String innerJsonList = String.valueOf(obj.get(channelId));
					for(String innerObjJson : innerJsonList.split(";"))
					{
						AppVersion appVersion = gson.fromJson(innerObjJson,AppVersion.class);
						 
						int minAppVersion = appVersion.getMinAppVersion();
						boolean isActive =appVersion.isActive();
						if(isActive == true && minAppVersion > appDetails.getFcversion()){
							resultAppDetails.setNewAppVersion(appVersion.getNewAppVersion());
							resultAppDetails.setForceUpdate(true);
							resultAppDetails.setWebMessageUrl(appVersion.getWebMessageUrl());
						}else{
							logger.debug("version cannot be upgraded");
							resultAppDetails.setForceUpdate(false);
						}
					}
					break;
				}
			}
			
		}else{
			logger.debug("there are no entries in appconfig Force Upgrade Details");
			resultAppDetails.setForceUpdate(false);
		}
		return resultAppDetails;
	}

}
