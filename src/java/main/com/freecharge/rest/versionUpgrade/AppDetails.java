package com.freecharge.rest.versionUpgrade;

public class AppDetails {
	private int fcversion;
	private String imei;
	private String androidId;
	private String advId;
	private String imsi;
	private int channelId;
	
	public int getFcversion() {
		return fcversion;
	}
	public void setFcversion(int fcversion) {
		this.fcversion = fcversion;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getAndroidId() {
		return androidId;
	}
	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}
	public String getAdvId() {
		return advId;
	}
	public void setAdvId(String advId) {
		this.advId = advId;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public String toString(){
		return "AppDetails [fcversion =" +fcversion+ ",imei = "+imei+ 
				", androidId ="+androidId+ ", advId =" +advId +",imsi = "+imsi+ ", channelId ="+channelId+"]";
	}
	
}
