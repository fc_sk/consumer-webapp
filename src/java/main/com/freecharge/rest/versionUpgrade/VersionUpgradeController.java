package com.freecharge.rest.versionUpgrade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.properties.AppConfigService;
import com.google.gson.JsonObject;

@Controller
@RequestMapping("/rest/app/*")
public class VersionUpgradeController {
	 private final Logger logger = Logger.getLogger(getClass());
	 
	 @Autowired
	 private VersionUpgradeService versionUpgradeService;
	 
	 @NoLogin
	 @RequestMapping(value = "version", method = RequestMethod.GET, produces = "application/json",
	            headers = "Accept=application/json")
	 @ResponseBody public ResultAppDetails getAppUpgrade(AppDetails appDetails,HttpServletRequest httpRequest, HttpServletResponse response)
	 		throws Exception{
		 logger.info("App Details Received:" + appDetails.toString());
		 ResultAppDetails resultAppDetails = versionUpgradeService.getAppUpgradeDetails(appDetails);
		 return resultAppDetails;	 
	 }
	
}