package com.freecharge.rest.model;


public class SaveCouponsRequest{
	
    private String itemIdArray;
    private String type;
    private String lookupID;
    

    public String getItemIdArray() {
        return itemIdArray;
    }

    public void setItemIdArray(String itemIdArray) {
        this.itemIdArray = itemIdArray;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public String getLookupID() {
		return lookupID;
	}

	public void setLookupID(String lookupID) {
		this.lookupID = lookupID;
	}

	@Override
	public String toString() {
		return "SaveCouponsRequest [itemIdArray=" + itemIdArray + ", type="
				+ type + ", lookupID=" + lookupID + "]";
	}
}