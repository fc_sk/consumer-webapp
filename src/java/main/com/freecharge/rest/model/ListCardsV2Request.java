package com.freecharge.rest.model;

public class ListCardsV2Request {
    private String appVersion;
    private String appType;
    private FPSParams fpsParams;
    
    public String getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    public String getAppType() {
        return appType;
    }
    public void setAppType(String appType) {
        this.appType = appType;
    }
    public FPSParams getFpsParams() {
        return fpsParams;
    }
    public void setFpsParams(FPSParams fpsParams) {
        this.fpsParams = fpsParams;
    }
    
}
