package com.freecharge.rest.model;

import com.freecharge.common.framework.basedo.AbstractDO;

public class ReCaptchaObject extends AbstractDO{
    private String response;
    private String challenge;

    public String getResponse() {
        return response;
    }
    public void setResponse(String response) {
        this.response = response;
    }
    public String getChallenge() {
        return challenge;
    }
    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }
}
