package com.freecharge.rest.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class MetroReportDataRequest implements Serializable{
	
	private String startDate;
	private String endDate;
	private String afcCardNumber;
	private String paymentStatus;
	private String orderId;
	

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getAfcCardNumber() {
		return afcCardNumber;
	}
	public void setAfcCardNumber(String afcCardNumber) {
		this.afcCardNumber = afcCardNumber;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
