package com.freecharge.rest.model.coupon;


import com.freecharge.common.framework.basedo.AbstractDO;

public class BlockedCouponAndCouponCategory extends AbstractDO {
    private String couponCategory;
    private Integer faceValue;
    private String readableValidityDate;
    private String termsConditions;
    private String voucherImgUrl;
    private String voucherName;
    private String couponCode;

    public String getCouponCategory() {
        return couponCategory;
    }

    public void setCouponCategory(String couponCategory) {
        this.couponCategory = couponCategory;
    }

    public Integer getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(Integer faceValue) {
        this.faceValue = faceValue;
    }

    public String getReadableValidityDate() {
        return readableValidityDate;
    }

    public void setReadableValidityDate(String readableValidityDate) {
        this.readableValidityDate = readableValidityDate;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getVoucherImgUrl() {
        return voucherImgUrl;
    }

    public void setVoucherImgUrl(String voucherImgUrl) {
        this.voucherImgUrl = voucherImgUrl;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
