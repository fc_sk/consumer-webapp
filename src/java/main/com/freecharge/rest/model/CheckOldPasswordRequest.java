package com.freecharge.rest.model;

public class CheckOldPasswordRequest {
    
    private String email;
    private String oldpassword;
    
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getOldpassword() {
        return oldpassword;
    }
    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }
    
}
