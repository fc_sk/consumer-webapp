package com.freecharge.rest.model;

public class LoadMerchandisingRequest {
	private Integer fcChannel;
	private Integer fcversion;
	private String propertyType;
	
	public Integer getFcChannel() {
		return fcChannel;
	}
	public void setFcChannel(Integer fcChannel) {
		this.fcChannel = fcChannel;
	}
	public Integer getFcversion() {
		return fcversion;
	}
	public void setFcversion(Integer fcversion) {
		this.fcversion = fcversion;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	
}
