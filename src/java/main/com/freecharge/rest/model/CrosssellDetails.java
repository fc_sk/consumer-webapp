package com.freecharge.rest.model;


public class CrosssellDetails {
	private String title;
	 private Integer storeId;
	 private Integer id;
	 private Integer price;
	 private String tnc;
	 private String headDescription;
	 private String description;
	 private String primaryproduct;
	 private String mailTnc;
	 private String mailSubject;
	 private String mailImgUrl;
	 private String label;
	 private String validityDate;
	
	 public String getValidityDate() {
		return validityDate;
	}
	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}
	private String imgurl;
	
	 public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getStoreId() {
		return storeId;
	}
	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getTnc() {
		return tnc;
	}
	public void setTnc(String tnc) {
		this.tnc = tnc;
	}
	public String getHeadDescription() {
		return headDescription;
	}
	public void setHeadDescription(String headDescription) {
		this.headDescription = headDescription;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrimaryproduct() {
		return primaryproduct;
	}
	public void setPrimaryproduct(String primaryproduct) {
		this.primaryproduct = primaryproduct;
	}
	public String getMailTnc() {
		return mailTnc;
	}
	public void setMailTnc(String mailTnc) {
		this.mailTnc = mailTnc;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailImgUrl() {
		return mailImgUrl;
	}
	public void setMailImgUrl(String mailImgUrl) {
		this.mailImgUrl = mailImgUrl;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getImgurl() {
		return imgurl;
	}
	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}
	 
	 
	
}
