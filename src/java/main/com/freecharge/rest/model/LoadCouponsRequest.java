package com.freecharge.rest.model;


public class LoadCouponsRequest{
    private String imei;
    private Integer fcChannel;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getFcChannel() {
		return fcChannel;
	}

	public void setFcChannel(Integer fcChannel) {
		this.fcChannel = fcChannel;
	}
    
}