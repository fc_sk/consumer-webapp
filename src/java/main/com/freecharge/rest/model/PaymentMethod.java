package com.freecharge.rest.model;

/**
 * This is a uber bean that contains, in de-normalized form,
 * everything about one *specific* payment method. A few
 * examples of payment methods are as below.
 * <ul>
 * <li> HDFC Bank Credit Card</li>
 * <li> ICICI Net Banking </li>
 * <li> ICICI Bank Credit Card </li>
 * <li> AirTel Cash Money. </li>
 * </ul>
 * 
 * As can be seen from above examples, there could be multiple
 * types of credit cards and so on.
 * 
 * For now the primary consumer of this bean is front end,
 * in json form. But that could change.
 * 
 * @author arun
 *
 */
public class PaymentMethod {
    private String paymentMethodName;
    private String paymentMethodCode;
    private String displayName;
    private String displayImage;
    private String imgUrl;
    private String paymentOptionCode;
    
    public String getPaymentMethodName() {
        return paymentMethodName;
    }
    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }
    
    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }
    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    public String getDisplayImage() {
        return displayImage;
    }
    public void setDisplayImage(String displayImage) {
        this.displayImage = displayImage;
    }
    
    public String getImgUrl() {
        return imgUrl;
    }
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    
    public String getPaymentOptionCode() {
        return paymentOptionCode;
    }
    public void setPaymentOptionCode(String paymentOptionCode) {
        this.paymentOptionCode = paymentOptionCode;
    }
}
