package com.freecharge.rest.model;

import java.util.List;

import org.springmodules.validation.bean.conf.loader.annotation.handler.Length;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.api.coupon.service.model.CityMaster;
import com.freecharge.app.domain.entity.StateMaster;

public class UpdateUserProfileRequest {
   
    private String title;
    private String name;
    private String email;
    private String oldpassword;
    private String newpassword;
    private String confirmpassword;
    private String mobileNo;
    private boolean ispasswordupdate=false;
    private Integer userid;
    private String area;
    @NotBlank(message = "Not blank")
    private String address;
    private String landmar;
    @NotBlank(message = "Not blank")
    @Length(max = 6,min=6)
    private String pincode;
    @NotBlank(message = "Not blank")
    private String state;
    @NotBlank(message = "Not blank")
    private String city;
    private List<StateMaster> statelist;
    private List<CityMaster> citylist;
    private boolean isoldpswdcorrect;
    private Integer userProfileId;
    
    
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getLandmar() {
        return landmar;
    }
    public void setLandmar(String landmar) {
        this.landmar = landmar;
    }
    public String getPincode() {
        return pincode;
    }
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public List<StateMaster> getStatelist() {
        return statelist;
    }
    public void setStatelist(List<StateMaster> statelist) {
        this.statelist = statelist;
    }
    public List<CityMaster> getCitylist() {
        return citylist;
    }
    public void setCitylist(List<CityMaster> citylist) {
        this.citylist = citylist;
    }
    public boolean isIsoldpswdcorrect() {
        return isoldpswdcorrect;
    }
    public void setIsoldpswdcorrect(boolean isoldpswdcorrect) {
        this.isoldpswdcorrect = isoldpswdcorrect;
    }
    public Integer getUserProfileId() {
        return userProfileId;
    }
    public void setUserProfileId(Integer userProfileId) {
        this.userProfileId = userProfileId;
    }
    public Integer getUserid() {
        return userid;
    }
    public void setUserid(Integer userid) {
        this.userid = userid;
    }
    public boolean isIspasswordupdate() {
        return ispasswordupdate;
    }
    public void setIspasswordupdate(boolean ispasswordupdate) {
        this.ispasswordupdate = ispasswordupdate;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getOldpassword() {
        return oldpassword;
    }
    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }
    public String getNewpassword() {
        return newpassword;
    }
    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
    public String getConfirmpassword() {
        return confirmpassword;
    }
    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }
    public String getMobileNo() {
        return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
    
    
}
