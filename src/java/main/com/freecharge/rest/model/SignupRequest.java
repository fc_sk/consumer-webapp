package com.freecharge.rest.model;

import org.springmodules.validation.bean.conf.loader.annotation.handler.Length;
import org.springmodules.validation.bean.conf.loader.annotation.handler.MaxLength;
import org.springmodules.validation.bean.conf.loader.annotation.handler.MinLength;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

public class SignupRequest {
    @NotBlank(message = "Email can not be blank")
    private String email;
    @NotBlank(message = "password can not be blank")
    @MinLength(value=6,message="Password field should contain Minimum 6 characters")
    @MaxLength(value=20,message="Password field should contain Maximum 20 characters")
    private String password;
    @NotBlank(message = "Mobile Number can not be blank")
    @MaxLength(value=10,message="Mobile Number field should contain Min and Max 10 numbers")
    @MinLength(value=10,message="Mobile Number field should contain Min and Max 10 numbers")
    private String mobileNo;
    @NotBlank(message = "Address can not be blank")
    private String address1;
    private String landmark;
    @NotBlank(message ="City can not be blank")
    private Integer cityId;
    @NotBlank(message ="Postal can not be blank")
    @Length(min = 6, max = 6, message = "Postal code should be 6 digits")
    private String postalCode;
    private Integer stateId;
    @NotBlank(message = "First Name can not be blank")
    private String firstName;
    @NotBlank(message = "Confirm Password can not be blank")
    private String confirmPassword;
    @NotBlank(message = "Title can not be blank")
    private String title;
    private String area;
    private boolean registrationStatusFlag;
    @NotBlank(message = "affiliate_unique_id can not be blank.")
    private String affiliate_unique_id;
    
    public String getAffiliate_unique_id() {
        return affiliate_unique_id;
    }
    public void setAffiliate_unique_id(String affiliate_unique_id) {
        this.affiliate_unique_id = affiliate_unique_id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getMobileNo() {
        return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    public String getLandmark() {
        return landmark;
    }
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }
    public Integer getCityId() {
        return cityId;
    }
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public Integer getStateId() {
        return stateId;
    }
    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getConfirmPassword() {
        return confirmPassword;
    }
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public boolean isRegistrationStatusFlag() {
        return registrationStatusFlag;
    }
    public void setRegistrationStatusFlag(boolean registrationStatusFlag) {
        this.registrationStatusFlag = registrationStatusFlag;
    }
}