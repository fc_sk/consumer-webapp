package com.freecharge.rest.model;

import java.util.List;

import com.freecharge.api.coupon.service.web.model.CouponOptinType;

public class BlockCouponForScaleTest {
	private String orderId;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Integer getCouponStoreId() {
		return couponStoreId;
	}
	public void setCouponStoreId(Integer couponStoreId) {
		this.couponStoreId = couponStoreId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public CouponOptinType getCouponOptinType() {
		return couponOptinType;
	}
	public void setCouponOptinType(CouponOptinType couponOptinType) {
		this.couponOptinType = couponOptinType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	private Integer couponStoreId;
	private Integer quantity;
	 private CouponOptinType couponOptinType;
	private String email;

}
