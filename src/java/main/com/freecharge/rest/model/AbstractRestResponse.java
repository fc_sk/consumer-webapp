package com.freecharge.rest.model;

/**
 * Base class for responses that are returned in
 * REST calls.
 * @author arun
 *
 */
public class AbstractRestResponse {
    private String status;
    private String errorMessage;

    public String getStatus() {
        return status;
    }
    public void setSuccessResponse() {
        this.status = "success";
    }
    public void setFailureResponse() {
        this.status = "failure";
    }
    

    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
