package com.freecharge.rest.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListCardsV4Request {
    private String appVersion;
    private String appType;
    private FPSParams fpsParams;
    private String productType;

    public String getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    public String getAppType() {
        return appType;
    }
    public void setAppType(String appType) {
        this.appType = appType;
    }
    public FPSParams getFpsParams() {
        return fpsParams;
    }
    public void setFpsParams(FPSParams fpsParams) {
        this.fpsParams = fpsParams;
    }
    public String getProductType() { return productType; }
    public void setProductType(String productType) { this.productType = productType; }
}
