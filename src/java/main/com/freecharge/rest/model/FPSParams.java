package com.freecharge.rest.model;

public class FPSParams {
    private String userId;
    private String platformType;
    private String assignedClientId;
    private String osVersion;
    private String oemDeviceId;
    private String visitId;
    private String userAgentStr;
    private UserAgentEntity userAgentEntity;
    private String clientIp;
    private String appVersion;
    private String deviceName;
    private String lanIp;
    private double txnAmount;
    private String cardBinHead;
    private String cardFingerprint;
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlatformType() {
        return platformType;
    }
    
    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }
    
    public String getAssignedClientId() {
        return assignedClientId;
    }
    
    public void setAssignedClientId(String assignedClientId) {
        this.assignedClientId = assignedClientId;
    }
    
    public String getOsVersion() {
        return osVersion;
    }
    
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }
    
    public String getOemDeviceId() {
        return oemDeviceId;
    }
    
    public void setOemDeviceId(String oemDeviceId) {
        this.oemDeviceId = oemDeviceId;
    }
    
    public String getUserAgentStr() {
        return userAgentStr;
    }

    public void setUserAgentStr(String userAgentStr) {
        this.userAgentStr = userAgentStr;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getLanIp() {
        return lanIp;
    }

    public void setLanIp(String lanIp) {
        this.lanIp = lanIp;
    }

    public double getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(double txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getCardBinHead() {
        return cardBinHead;
    }

    public void setCardBinHead(String cardBinHead) {
        this.cardBinHead = cardBinHead;
    }

    public String getCardFingerprint() {
        return cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public UserAgentEntity getUserAgentEntity() {
        return userAgentEntity;
    }

    public void setUserAgentEntity(UserAgentEntity userAgentEntity) {
        this.userAgentEntity = userAgentEntity;
    }

    @Override
    public String toString() {
        return "FPSParams [userId=" + userId + ", platformType=" + platformType + ", assignedClientId="
                + assignedClientId + ", osVersion=" + osVersion + ", oemDeviceId=" + oemDeviceId + ", visitId="
                + visitId + ", userAgentStr=" + userAgentStr + ", userAgentEntity=" + userAgentEntity + ", clientIp="
                + clientIp + ", appVersion=" + appVersion + ", deviceName=" + deviceName + ", lanIp=" + lanIp
                + ", txnAmount=" + txnAmount + ", cardBinHead=" + cardBinHead + ", cardFingerprint=" + cardFingerprint
                + "]";
    }

}
