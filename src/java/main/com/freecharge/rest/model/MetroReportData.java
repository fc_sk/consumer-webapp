package com.freecharge.rest.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class MetroReportData implements Serializable{
	
	private String afcCardNumber;
	private String orderId;
	private Timestamp paymentTime;
	private Timestamp rechargeTime;
	private String rechargeStatus;
	private String paymentStatus;
	private String paymentType;
	private String firstName;
	private String email;
	private String phoneNo;
	private Double amount;
	private String refundStatus;
	private Date refundDate;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAfcCardNumber() {
		return afcCardNumber;
	}
	public void setAfcCardNumber(String afcCardNumber) {
		this.afcCardNumber = afcCardNumber;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Timestamp getPaymentTime() {
		return paymentTime;
	}
	public void setPaymentTime(Timestamp paymentTime) {
		this.paymentTime = paymentTime;
	}
	public Timestamp getRechargeTime() {
		return rechargeTime;
	}
	public void setRechargeTime(Timestamp rechargeTime) {
		this.rechargeTime = rechargeTime;
	}
	public String getRechargeStatus() {
		return rechargeStatus;
	}
	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public String toString() {
		return "afcCardNumber = " + afcCardNumber + "  orderID = " + orderId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setRefundStatus(String refundStatus) { this.refundStatus = refundStatus; }
	public String getRefundStatus() { return refundStatus;}
	public void setRefundDate(Date refundDate) { this.refundDate = refundDate; }
	public Date getRefundDate() { return refundDate; }
}
