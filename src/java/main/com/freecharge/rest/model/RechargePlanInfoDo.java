package com.freecharge.rest.model;


import java.util.Map;

public class RechargePlanInfoDo {

    private Long rechargePlanMasterId;
    private String planNameInDB;
    private String description;
    private double amount;
    private double talktime;
    private String validity;
    private long productId;
    private String productName;
    private long operatorId;
    private String operatorName;
    private long circleId;
    private String circleName;
    private Map<String, String> metadata;
    private String category;
    private String subCategory;
    private boolean isPopular;

    public Long getRechargePlanMasterId() {
        return rechargePlanMasterId;
    }

    public void setRechargePlanMasterId(Long rechargePlanMasterId) {
        this.rechargePlanMasterId = rechargePlanMasterId;
    }

    public String getPlanNameInDB() {
        return planNameInDB;
    }

    public void setPlanNameInDB(String planNameInDB) {
        this.planNameInDB = planNameInDB;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTalktime() {
        return talktime;
    }

    public void setTalktime(double talktime) {
        this.talktime = talktime;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(long operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public long getCircleId() {
        return circleId;
    }

    public void setCircleId(long circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPopular() {
        return isPopular;
    }

    public void setPopular(boolean isPopular) {
        this.isPopular = isPopular;
    }
}
