package com.freecharge.rest.model;

import java.util.Map;

public class AffiliateCouponsRequest {
	private int couponAffiliateId;
	private String affiliateOrderId;
	private Map<String, String> affiliateRequestData;
	
	public int getCouponAffiliateId() {
		return couponAffiliateId;
	}
	public void setCouponAffiliateId(int couponAffiliateId) {
		this.couponAffiliateId = couponAffiliateId;
	}
	public String getAffiliateOrderId() {
		return affiliateOrderId;
	}
	public void setAffiliateOrderId(String affiliateOrderId) {
		this.affiliateOrderId = affiliateOrderId;
	}
	public Map<String, String> getAffiliateRequestData() {
		return affiliateRequestData;
	}
	public void setAffiliateRequestData(Map<String, String> affiliateRequestData) {
		this.affiliateRequestData = affiliateRequestData;
	}

}
