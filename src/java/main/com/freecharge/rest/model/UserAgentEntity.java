package com.freecharge.rest.model;

public class UserAgentEntity {
    private String userAgentName;
    private String userAgentVersion;
    private String osVersion;
    
    public String getUserAgentName() {
        return userAgentName;
    }
    public void setUserAgentName(String userAgentName) {
        this.userAgentName = userAgentName;
    }
    public String getUserAgentVersion() {
        return userAgentVersion;
    }
    public void setUserAgentVersion(String userAgentVersion) {
        this.userAgentVersion = userAgentVersion;
    }
    public String getOsVersion() {
        return osVersion;
    }
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }
    @Override
    public String toString() {
        return "UserAgentEntity [userAgentName=" + userAgentName + ", userAgentVersion=" + userAgentVersion
                + ", osVersion=" + osVersion + "]";
    }
}
