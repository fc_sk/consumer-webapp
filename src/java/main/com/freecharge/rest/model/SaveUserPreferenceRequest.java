package com.freecharge.rest.model;

import java.util.Map;

public class SaveUserPreferenceRequest {

    @Override
    public String toString() {
        return "SaveUserPreferenceRequest [userEmailId=" + userEmailId + ", type=" + type + ", userPrefMap="
                + userPrefMap + ", prefId=" + prefId + "userRating=" + userRating + "]";
    }

    private String userEmailId;
    private String type;
    private Map<String, Object> userPrefMap;
    private String prefId;
    private String userRating;

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(final String userRating) {
        this.userRating = userRating;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(final String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public Map<String, Object> getUserPrefMap() {
        return userPrefMap;
    }

    public void setUserPrefMap(final Map<String, Object> userPrefMap) {
        this.userPrefMap = userPrefMap;
    }

    public String getPrefId() {
        return prefId;
    }

    public void setPrefId(final String prefId) {
        this.prefId = prefId;
    }
}
