package com.freecharge.rest.model;


public class RechargeRequest {

    
    private String serviceNumber;
    private String operatorName;
    private String circleName;
    private Float amount;
    private String productType;
    private String sessionId;
    private String lookupId;
    private String rechargePlanType;
    private String operator;
    private String fulfillmentIdf;
    private Integer planId;
    private String planCategory;

    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    public String getRechargePlanType() {
        return rechargePlanType;
    }
    public void setRechargePlanType(String rechargePlanType) {
        this.rechargePlanType = rechargePlanType;
    }
    public String getServiceNumber() {
        return serviceNumber;
    }
    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }
    public String getOperatorName() {
        return operatorName;
    }
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
    public String getCircleName() {
        return circleName;
    }
    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }
    public Float getAmount() {
        return amount;
    }
    public void setAmount(Float amount) {
        this.amount = amount;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getSessionId() {
        return sessionId;
    }
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    public String getLookupId() {
        return lookupId;
    }
    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }
	public String getFulfillmentIdf() {
		return fulfillmentIdf;
	}
	public void setFulfillmentIdf(String fulfillmentIdf) {
		this.fulfillmentIdf = fulfillmentIdf;
	}

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanCategory() {
        return planCategory;
    }

    public void setPlanCategory(String planCategory) {
        this.planCategory = planCategory;
    }

    @Override
    public String toString() {
        return "RechargeRequest{" +
                "serviceNumber='" + serviceNumber + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", circleName='" + circleName + '\'' +
                ", amount=" + amount +
                ", productType='" + productType + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", lookupId='" + lookupId + '\'' +
                ", rechargePlanType='" + rechargePlanType + '\'' +
                ", operator='" + operator + '\'' +
                ", fulfillmentIdf='" + fulfillmentIdf + '\'' +
                ", planId=" + planId +
                ", planCategory='" + planCategory + '\'' +
                '}';
    }
}
