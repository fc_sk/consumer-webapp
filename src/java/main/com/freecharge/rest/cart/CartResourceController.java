package com.freecharge.rest.cart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.service.web.model.CouponBasic;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.TransactionHomePage;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.RestControllerHelper;
import com.freecharge.rest.model.RechargeData;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@Controller
@RequestMapping("/rest/*")
public class CartResourceController extends AbstractRestController {
	@Autowired
	private CartService cartService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private EasyDBWriteDAO easyDBWriteDAO;

	@Autowired
	private RestControllerHelper restControllerHelper;

	@Autowired
	private PricingService pricingService;

	@Autowired
	private FCProperties fcProperties;

	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;

	@NoSessionWrite
	@NoLogin
	@RequestMapping(value = "/cart/{lookupId}", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getCart(@PathVariable String lookupId, ModelMap model)throws InvalidParameterException {
		if (!(restControllerHelper.isLookupIdValid(lookupId))) {
			throw new InvalidParameterException("lookupId", lookupId,"Invalid LookupId", ValidationErrorCode.INVALID_LOOKUPID);
		}
		CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
		Integer cartId = null;
		if (cartBusinessDo != null) {
			cartId = cartBusinessDo.getId().intValue();
		}
		Map<String, Object> jsonObjects = new HashMap<String, Object>();
		List<CouponBasic> couponDetailsList = getCouponData(cartBusinessDo,cartId);
		jsonObjects.put("couponDetails", couponDetailsList);
		RechargeData rechargeData = getRechargeData(lookupId, cartBusinessDo,cartId);
		jsonObjects.put("rechargeData", rechargeData);
		return jsonObjects;
	}

	@NoSessionWrite
	@NoLogin
	@RequestMapping(value = "/coupon/c/hcoupon/cart/{lookupId}", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getHCouponCart(
			@PathVariable String lookupId, ModelMap model)
			throws InvalidParameterException {
		String userEmailId = FCSessionUtil.getLoggedInEmailId();
		Map<String, Object> jsonObjects = new HashMap<String, Object>();
		CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
		if (userEmailId != null && cartBusinessDo != null) {
			if (!(restControllerHelper.isLookupIdValid(cartBusinessDo.getUserId(), userEmailId))) {
				throw new InvalidParameterException("lookupId", lookupId,"Invalid LookupId",ValidationErrorCode.INVALID_LOOKUPID);
			}
			Integer cartId = null;
			if (cartBusinessDo != null) {
				cartId = cartBusinessDo.getId().intValue();
			}
			List<CouponBasic> couponDetailsList = getCouponData(cartBusinessDo,cartId);
			jsonObjects.put("couponDetails", couponDetailsList);
			jsonObjects.put("couponDetails", couponDetailsList);
			return jsonObjects;
		} else {
			logger.error("error getting userEmailId and/or cartBusinessDO. Check lookUpId");
		}
		return jsonObjects;
	}

	private List<CouponBasic> getCouponData(CartBusinessDo cartBusinessDo,
			Integer cartId) {
		Integer couponQuantity = null;
		List<CouponBasic> couponDetailsList = new ArrayList<CouponBasic>();
		if (cartBusinessDo != null) {
			List<CouponStore> couponStores = couponService
					.getAllCouponStores(cartBusinessDo);
			logger.info("couponStores size: " + couponStores.size());
			for (CouponStore couponStore : couponStores) {
				if (couponStore != null) {
					logger.info("inside for loop for coupon id : "+ couponStore.getCouponStoreId());
					CouponBasic couponDetails = new CouponBasic();
					couponDetails.setCouponId(couponStore.getCouponStoreId());
					couponDetails.setCampaignName(couponStore.getCampaignName());
					couponDetails.setCouponValue(couponStore.getCouponValue());
					couponDetails.setCouponType(couponStore.getCouponType());
					couponDetails.setCouponImagePath(couponStore.getCouponImagePath());
					couponDetails.setPrice(couponStore.getPrice());
					couponDetails.setValidityDate(couponStore.getValidityDate());
					if (couponStore.getCouponType().equalsIgnoreCase(FCConstants.COUPON_TYPE_C)) {
						couponQuantity = cartService.getCouponQuantity(
						couponStore.getFkCrosssellId(), cartId);
						for (int i = 1; i <= couponQuantity; i++) {
							couponDetailsList.add(couponDetails);
						}
					} else {
						couponQuantity = cartService.getCouponQuantity(
							couponStore.getCouponStoreId(), cartId);
						for (int i = 1; i <= couponQuantity; i++) {
							couponDetailsList.add(couponDetails);
						}
					}

					/**
					 * cuponCode-blockTime-are not added
					 */
				}
			}
		} else {
			logger.info("shitting here");
		}
		logger.info("couponDetailsList size: " + couponDetailsList.size());
		return couponDetailsList;
	}

	private RechargeData getRechargeData(String lookupId,
			CartBusinessDo cartBusinessDo, Integer cartId) {
		RechargeData rechargeData = new RechargeData();
		TransactionHomePage transactionHomePageOld = new TransactionHomePage();
		transactionHomePageOld.setLookupId(lookupId);
		List<TransactionHomePage> txnHomePages = easyDBWriteDAO
				.get(transactionHomePageOld);

		if (txnHomePages.size() > 1) {
			logger.info("More than one transaction home page details found for lookupId "
					+ lookupId + ", contact the dev team immediately");
		}

		TransactionHomePage transactionHomePage = new TransactionHomePage();
		transactionHomePage = FCUtil.getFirstElement(txnHomePages);

		rechargeData.setCircleName(transactionHomePage.getCircleName());
		rechargeData.setOperatorName(transactionHomePage.getOperatorName());
		rechargeData.setServiceNumber(transactionHomePage.getServiceNumber());
		if (transactionHomePage.getProductType() != null&& FCUtil.isUtilityPaymentType(transactionHomePage.getProductType())) {
			String billId = transactionHomePage.getServiceNumber();
			BillWithSubscriberNumber billWithSubscriberNumber = billPayServiceRemote.getBillDetails(billId);
			rechargeData.setServiceNumber(billWithSubscriberNumber.getAdditionalInfo1());
		}
		rechargeData.setProductName(transactionHomePage.getProductType());
		rechargeData.setCartId(cartId);
		rechargeData.setOperatorId(transactionHomePage.getFkOperatorId());
		rechargeData.setCircleId(transactionHomePage.getFkCircleId());

		if (cartBusinessDo != null) {
			rechargeData.setTotalAmount(pricingService.getPayableAmount(cartBusinessDo));
			rechargeData.setRechargeAmount(cartService.getRechargeAmount(cartBusinessDo));
		} else {
			// done by Pavan :p
			rechargeData.setTotalAmount(transactionHomePage.getAmount().doubleValue());
			rechargeData.setRechargeAmount((Double) transactionHomePage.getAmount().doubleValue());
		}

		List<CartItems> cartItemsList = cartService.getCartItemsByCartId(cartId);

		if (cartItemsList != null && !fcProperties.isCouponPricingEnabled()) {
			for (CartItems cartItems : cartItemsList) {
				if ((ProductMaster.ProductName.Charges.getProductId()) == cartItems.getFlProductMasterId()) {
					rechargeData.setHandlingCharges(cartItems.getPrice());
				}
			}
		}
		return rechargeData;
	}
}