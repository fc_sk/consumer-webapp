package com.freecharge.rest.cart;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidParametersException;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.util.FCConstants;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.rest.RestControllerHelper;
import com.freecharge.rest.model.CrosssellDetails;

@Controller
@RequestMapping("/rest/*")
public class CrosssellResourceController extends AbstractRestController {
	@Autowired
    VoucherService voucherServce;
	
	@Autowired
    private CrosssellService crosssellService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private RestControllerHelper restControllerHelper;
	
	
	@RequestMapping(value="/cart/crosssell/all", method=RequestMethod.GET)
	public @ResponseBody List<CrosssellDetails> getAllCrosssell(ModelMap model) throws InvalidParametersException {
		List<CrosssellDetails> CrosssellDetailsList = new ArrayList<CrosssellDetails>();
		List<CrosssellBusinessDo> crosssellBusinessDoList = crosssellService.getAllCrosssell();
		
		for (CrosssellBusinessDo crosssellBusinessDo : crosssellBusinessDoList) {
			CrosssellDetails crosssellDetails = new CrosssellDetails();
			crosssellDetails.setTitle(crosssellBusinessDo.getTitle());
			crosssellDetails.setStoreId(crosssellBusinessDo.getStoreId());
			if (crosssellBusinessDo.getCrosssell() != null) {
				crosssellDetails.setId(crosssellBusinessDo.getCrosssell().getId());
				crosssellDetails.setPrice(crosssellBusinessDo.getCrosssell().getPrice());
				crosssellDetails.setLabel(crosssellBusinessDo.getCrosssell().getLabel());
				crosssellDetails.setValidityDate(crosssellBusinessDo.getCrosssell().getValidUpto().toString());
			}
			crosssellDetails.setTnc(crosssellBusinessDo.getTnc());
			crosssellDetails.setHeadDescription(crosssellBusinessDo.getHeadDescription());
			crosssellDetails.setDescription(crosssellBusinessDo.getDescription());
			crosssellDetails.setPrimaryproduct(crosssellBusinessDo.getPrimaryproduct());
			crosssellDetails.setMailTnc(crosssellBusinessDo.getMailTnc());
			crosssellDetails.setMailSubject(crosssellBusinessDo.getMailSubject());
			crosssellDetails.setMailImgUrl(crosssellBusinessDo.getMailImgUrl());
			crosssellDetails.setImgurl(crosssellBusinessDo.getImgurl());
			CrosssellDetailsList.add(crosssellDetails);
		}	
		return CrosssellDetailsList;

	}
	
	@RequestMapping(value="/cart/crosssell/{amount}/{productName}", method=RequestMethod.GET)
	public @ResponseBody List<CrosssellDetails> getAllCrosssellCoupon(@PathVariable String amount, @PathVariable String productName, ModelMap model) throws InvalidParametersException {
		validateParameters(amount, productName);
		
		Float amountVlaue = Float.parseFloat(amount);
		String productType = FCConstants.productNameToTypeMap.get(productName);
		List<CrosssellDetails> crosssellDetailsList = new ArrayList<CrosssellDetails>();
		
		List<CrosssellBusinessDo> crosssellBusinessDoList = crosssellService.getAllCrosssellCoupon(amountVlaue, productType);
		for (CrosssellBusinessDo crosssellBusinessDo : crosssellBusinessDoList) {
			CrosssellDetails crosssellDetails = new CrosssellDetails();
			crosssellDetails.setTitle(crosssellBusinessDo.getTitle());
			crosssellDetails.setStoreId(crosssellBusinessDo.getStoreId());
			crosssellDetails.setId(crosssellBusinessDo.getId().intValue());
			crosssellDetails.setPrice(crosssellBusinessDo.getPrice());
			crosssellDetails.setTnc(crosssellBusinessDo.getTnc());
			crosssellDetails.setHeadDescription(crosssellBusinessDo.getHeadDescription());
			crosssellDetails.setDescription(crosssellBusinessDo.getDescription());
			crosssellDetails.setPrimaryproduct(crosssellBusinessDo.getPrimaryproduct());
			crosssellDetails.setMailTnc(crosssellBusinessDo.getMailTnc());
			crosssellDetails.setMailSubject(crosssellBusinessDo.getMailSubject());
			crosssellDetails.setMailImgUrl(crosssellBusinessDo.getMailImgUrl());
			crosssellDetails.setImgurl(crosssellBusinessDo.getImgurl());
			CrosssellBusinessDo crosssellBusinessDoNew = crosssellService.getCrosssell(crosssellBusinessDo.getId().intValue());
			crosssellDetails.setLabel(crosssellBusinessDoNew.getCrosssell().getLabel());
			crosssellDetails.setValidityDate(crosssellBusinessDoNew.getCrosssell().getValidUpto().toString());
			
			crosssellDetailsList.add(crosssellDetails);
		}	
		return crosssellDetailsList;
	}	
	
	private void validateParameters(String amount, String productName) throws  InvalidParametersException {
		List<InvalidParameterException> InvalidParameterExceptionList = new ArrayList<InvalidParameterException>();
		if (amount != null) {
			if (!(StringUtils.isNumeric(amount))) {
				InvalidParameterExceptionList.add(new InvalidParameterException("amount", amount, "Amount field should be numeric", ValidationErrorCode.INVALID_NUMBER_FORMAT));				
			}
		}
		
		if (productName != null) {
			if  (!(productName.equals(ProductMaster.ProductName.Mobile.toString()) || productName.equals(ProductMaster.ProductName.DTH.toString()) || productName.equals(ProductMaster.ProductName.DataCard.toString()) || productName.equals(ProductMaster.ProductName.PostPaidDataCard.toString()) || productName.equals(ProductMaster.ProductName.MobilePostpaid.toString()))) {
				InvalidParameterExceptionList.add(new InvalidParameterException("productName", productName,"Valid product name is required", ValidationErrorCode.INVALID_PRODUCT_NAME));
			}
		}
		
		if (InvalidParameterExceptionList.size() > 0) {
			throw new InvalidParametersException(InvalidParameterExceptionList);
		}
	}

	@RequestMapping(value="/cart/crosssell/{lookupId}", method=RequestMethod.GET)
	public @ResponseBody List<CrosssellDetails> getCrosssellDetails(@PathVariable String lookupId, ModelMap model) throws InvalidParameterException {
		
		if (!(restControllerHelper.isLookupIdValid(lookupId))) {
	    	throw new InvalidParameterException("lookupId", lookupId,"Invalid LookupId", ValidationErrorCode.INVALID_LOOKUPID);
	    }
		
		List<CrosssellDetails> crosssellDetailsList = new ArrayList<CrosssellDetails>();
		List<Map<String, Object>> objectMap = crosssellService.getCrossSellDetailsBylookupId(lookupId);
		for (Map<String, Object> crosssell : objectMap) {
			CrosssellDetails crosssellDetails = new CrosssellDetails();
			crosssellDetails.setTitle(crosssell.get("crosssellname").toString());
			crosssellDetails.setPrice((Integer)crosssell.get("crosssellamount"));
			crosssellDetails.setTnc((String)crosssell.get("termsConditions"));
			crosssellDetails.setHeadDescription((String)crosssell.get("crosssellheadDescription"));
			crosssellDetails.setDescription((String)crosssell.get("crossselldescription"));
			crosssellDetails.setPrimaryproduct((String)crosssell.get("crosssellprimaryproduct"));
			crosssellDetails.setMailTnc((String)crosssell.get("crosssellMailTnc"));
			crosssellDetails.setMailSubject((String)crosssell.get("crosssellMailSubject"));
			crosssellDetails.setMailImgUrl((String)crosssell.get("crosssellMailImgUrl"));
			crosssellDetails.setImgurl((String)crosssell.get("couponImagePath"));
			crosssellDetails.setStoreId((Integer)crosssell.get("couponStoreId"));
			
			CouponStore couoponStore = couponService.getCouponStore((Integer)crosssell.get("couponStoreId"));
			if(couoponStore!=null) {
				crosssellDetails.setValidityDate(couoponStore.getValidityDate().toString());
			
				crosssellDetails.setId(couoponStore.getFkCrosssellId());
			
				CrosssellBusinessDo crosssellBusinessDo = crosssellService.getCrosssell(couoponStore.getFkCrosssellId());
				crosssellDetails.setLabel(crosssellBusinessDo.getCrosssell().getLabel());
			}
			crosssellDetailsList.add(crosssellDetails);
		}
		return crosssellDetailsList;
	}
}
