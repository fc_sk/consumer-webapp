package com.freecharge.seo.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class SEOAttributesWebDO extends BaseWebDO {

	private static final long serialVersionUID = 1L;

	private String level;
	private String product;
	private Integer productId;
	private Integer channelId;
	private String operatorName;
	private String url;
	private String imgUrl;
	private String pageTitle;
	private String pageHeading;
	private String metaDescription;
	private String metaKeywords;
	private String freeCouponText;
	private String doubleCouponText;
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public String getPageHeading() {
		return pageHeading;
	}
	public void setPageHeading(String pageHeading) {
		this.pageHeading = pageHeading;
	}
	public String getMetaDescription() {
		return metaDescription;
	}
	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}
	public String getMetaKeywords() {
		return metaKeywords;
	}
	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}
	public String getFreeCouponText() {
		return freeCouponText;
	}
	public void setFreeCouponText(String freeCouponText) {
		this.freeCouponText = freeCouponText;
	}
	public String getDoubleCouponText() {
		return doubleCouponText;
	}
	public void setDoubleCouponText(String doubleCouponText) {
		this.doubleCouponText = doubleCouponText;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	
}
