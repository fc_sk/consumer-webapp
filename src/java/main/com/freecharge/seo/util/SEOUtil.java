package com.freecharge.seo.util;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.seo.webdo.SEOAttributesWebDO;
import com.freecharge.web.util.WebConstants;

public class SEOUtil {

	 private static Logger logger = LoggingFactory.getLogger(SEOUtil.class);
	 
	 public static boolean handleSEORedirection(String productType, String operator, Integer channelId, Model model) throws IOException {
	    	
	    	String delegateName = WebConstants.DELEGATE_SEO_ATTRIBUTE;
	    	    	
	    	try {
	    		SEOAttributesWebDO seoAttributesWebDO = new SEOAttributesWebDO();
	    		if (!StringUtils.isEmpty(productType)) {
	    			seoAttributesWebDO.setProduct(productType);
	    		}
	    		if (!StringUtils.isEmpty(operator)) {
	    			seoAttributesWebDO.setOperatorName(operator);
	    		}
	    		if (channelId != null) {
	    			seoAttributesWebDO.setChannelId(channelId);
	    		}
	    		
	    		WebContext webContext = DelegateHelper.processRequest(null, null, delegateName, model, seoAttributesWebDO);
	    		
	    		seoAttributesWebDO = (SEOAttributesWebDO)webContext.getBaseWebDO();
	    		
	    		model.addAttribute("seoAttributes", seoAttributesWebDO);
	    		
	    		return true;
	    
	    	} catch (Exception exp) {
	    		logger.error(" Exception in handleSEORedirection : ", exp);
	    		return false;
	    	}

		}
}
