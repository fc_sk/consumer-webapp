package com.freecharge.seo.mapper;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.webdo.SEOAttributesWebDO;

public class SEOAttributesDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		SEOAttributesWebDO seoAttributesWebDO = new SEOAttributesWebDO();
		SEOAttributesBusinessDO seoAttributesBusinessDO = (SEOAttributesBusinessDO)baseBusinessDO;
		
		seoAttributesWebDO.setLevel(seoAttributesBusinessDO.getLevel());
		seoAttributesWebDO.setProduct(seoAttributesBusinessDO.getProduct());
		seoAttributesWebDO.setProductId(seoAttributesBusinessDO.getProductId());
		seoAttributesWebDO.setOperatorName(seoAttributesBusinessDO.getOperatorName());
		seoAttributesWebDO.setUrl(seoAttributesBusinessDO.getUrl());
		seoAttributesWebDO.setImgUrl(seoAttributesBusinessDO.getImgUrl());
		seoAttributesWebDO.setPageTitle(seoAttributesBusinessDO.getPageTitle());
		seoAttributesWebDO.setPageHeading(seoAttributesBusinessDO.getPageHeading());
		seoAttributesWebDO.setMetaDescription(seoAttributesBusinessDO.getMetaDescription());
		seoAttributesWebDO.setMetaKeywords(seoAttributesBusinessDO.getMetaKeywords());
		seoAttributesWebDO.setFreeCouponText(seoAttributesBusinessDO.getFreeCouponText());
		seoAttributesWebDO.setDoubleCouponText(seoAttributesBusinessDO.getDoubleCouponText());
		seoAttributesWebDO.setChannelId(seoAttributesBusinessDO.getChannelId());	
		return seoAttributesWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {

		SEOAttributesBusinessDO seoAttributesBusinessDO = new SEOAttributesBusinessDO();
		SEOAttributesWebDO seoAttriWebDO = (SEOAttributesWebDO)baseWebDO;
		
		seoAttributesBusinessDO.setLevel(seoAttriWebDO.getLevel());
		seoAttributesBusinessDO.setProduct(seoAttriWebDO.getProduct());
		seoAttributesBusinessDO.setProductId(seoAttriWebDO.getProductId());
		seoAttributesBusinessDO.setOperatorName(seoAttriWebDO.getOperatorName());
		seoAttributesBusinessDO.setUrl(seoAttriWebDO.getUrl());
		seoAttributesBusinessDO.setImgUrl(seoAttriWebDO.getImgUrl());
		seoAttributesBusinessDO.setPageTitle(seoAttriWebDO.getPageTitle());
		seoAttributesBusinessDO.setPageHeading(seoAttriWebDO.getPageHeading());
		seoAttributesBusinessDO.setMetaDescription(seoAttriWebDO.getMetaDescription());
		seoAttributesBusinessDO.setMetaKeywords(seoAttriWebDO.getMetaKeywords());
		seoAttributesBusinessDO.setFreeCouponText(seoAttriWebDO.getFreeCouponText());
		seoAttributesBusinessDO.setDoubleCouponText(seoAttriWebDO.getDoubleCouponText());
		seoAttributesBusinessDO.setChannelId(seoAttriWebDO.getChannelId());
		return seoAttributesBusinessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
