package com.freecharge.seo.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.seo.service.SEOService;

public class SEOAttributesDelegate implements WebDelegate {

	@Autowired
	SEOService seoService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
		
		baseBusinessDO = seoService.populateSeoAttributes(baseBusinessDO);
		if( baseBusinessDO != null) {
			BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
	        webContext.setBaseWebDO(baseWebDO);
	        
		} else {
			webContext.setBaseWebDO(null);
		}
	}

}
