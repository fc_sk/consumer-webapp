package com.freecharge.seo.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class SEOAttributesBusinessDO extends BaseBusinessDO {

    private static final long serialVersionUID = 3L;

    private String level;
    private String product;
    private Integer productId;
    private Integer channelId;
    private String operatorName;
    private String url;
    private String imgUrl;
    private String imgAlt;
    private String pageTitle;
    private String pageHeading;
    private String metaDescription;
    private String metaKeywords;
    private String freeCouponText;
    private String doubleCouponText;
    private String operatorId;
    private String footerTextParaOne;
    private String footerTextParaTwo;
    private String paraOneTextOne;
    private String paraOneTextTwo;
    private String paraOneTextThree;
    private String paraOneTextFour;
    private String paraOneTextFive;
    private String paraOneTextSix;
    private String paraOneTextSeven;
    private String paraTwoTextOne;
    private String paraTwoTextTwo;
    private String paraTwoTextThree;
    private String paraTwoTextFour;
    private String paraTwoTextFive;
    private String paraTwoTextSix;
    private String paraTwoTextSeven;
    private String paraTwoTextEight;
    private String paraTwoTextNine;
    private String paraTwoTextTen;
    private String paraThreeTextOne;
    private String paraThreeTextTwo;
    private String paraThreeTextThree;
    private String paraThreeTextFour;
    private String paraThreeTextFive;
    private String paraThreeTextSix;
    private String paraThreeTextSeven;
    private String paraFourTextOne;
    private String paraFourTextTwo;
    private String paraFourTextThree;
    private String paraFourTextFour;
    private String paraFourTextFive;
    private String paraFourTextSix;
    private String paraFourTextSeven;
    private String paraFourTextEight;
    private String paraFourTextNine;
    private String planMetaTitle;
    private String planMetaDescription;
    private String planSubHeading;
    private String planTextParaOne;
    private String planTextParaTwo;
    private String planPageUrl;
    private String planOperatorLogo;

    public String getLevel() {
        return level;
    }
    public void setLevel(String level) {
        this.level = level;
    }
    public String getProduct() {
        return product;
    }
    public void setProduct(String product) {
        this.product = product;
    }
    public Integer getProductId() {
        return productId;
    }
    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    public String getOperatorName() {
        return operatorName;
    }
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getImgUrl() {
        return imgUrl;
    }
    public String getImgAlt() {
		return imgAlt;
	}
	public void setImgAlt(String imgAlt) {
		this.imgAlt = imgAlt;
	}
	public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    public String getPageTitle() {
        return pageTitle;
    }
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    public String getPageHeading() {
        return pageHeading;
    }
    public void setPageHeading(String pageHeading) {
        this.pageHeading = pageHeading;
    }
    public String getMetaDescription() {
        return metaDescription;
    }
    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }
    public String getMetaKeywords() {
        return metaKeywords;
    }
    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }
    public String getFreeCouponText() {
        return freeCouponText;
    }
    public void setFreeCouponText(String freeCouponText) {
        this.freeCouponText = freeCouponText;
    }
    public String getDoubleCouponText() {
        return doubleCouponText;
    }
    public void setDoubleCouponText(String doubleCouponText) {
        this.doubleCouponText = doubleCouponText;
    }
    public Integer getChannelId() {
        return channelId;
    }
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }
    public String getOperatorId() {
        return operatorId;
    }
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }
    public String getFooterTextParaOne() {
        return footerTextParaOne;
    }
    public void setFooterTextParaOne(String footerTextParaOne) {
        this.footerTextParaOne = footerTextParaOne;
    }
    public String getFooterTextParaTwo() {
        return footerTextParaTwo;
    }
    public void setFooterTextParaTwo(String footerTextParaTwo) {
        this.footerTextParaTwo = footerTextParaTwo;
    }
    public String getParaOneTextOne() {
        return paraOneTextOne;
    }
    public void setParaOneTextOne(String paraOneTextOne) {
        this.paraOneTextOne = paraOneTextOne;
    }

    public String getParaOneTextTwo() {
        return paraOneTextTwo;
    }
    public void setParaOneTextTwo(String paraOneTextTwo) {
        this.paraOneTextTwo = paraOneTextTwo;
    }
    public String getParaOneTextThree() {
        return paraOneTextThree;
    }
    public void setParaOneTextThree(String paraOneTextThree) {
        this.paraOneTextThree = paraOneTextThree;
    }
    public String getParaOneTextFour() {
        return paraOneTextFour;
    }
    public void setParaOneTextFour(String paraOneTextFour) {
        this.paraOneTextFour = paraOneTextFour;
    }
    public String getParaOneTextFive() {
        return paraOneTextFive;
    }
    public void setParaOneTextFive(String paraOneTextFive) {
        this.paraOneTextFive = paraOneTextFive;
    }
    public String getParaOneTextSix() {
		return paraOneTextSix;
	}
	public void setParaOneTextSix(String paraOneTextSix) {
		this.paraOneTextSix = paraOneTextSix;
	}
	public String getParaOneTextSeven() {
		return paraOneTextSeven;
	}
	public void setParaOneTextSeven(String paraOneTextSeven) {
		this.paraOneTextSeven = paraOneTextSeven;
	}
	public String getParaTwoTextOne() {
        return paraTwoTextOne;
    }
    public void setParaTwoTextOne(String paraTwoTextOne) {
        this.paraTwoTextOne = paraTwoTextOne;
    }
    public String getParaTwoTextTwo() {
        return paraTwoTextTwo;
    }
    public void setParaTwoTextTwo(String paraTwoTextTwo) {
        this.paraTwoTextTwo = paraTwoTextTwo;
    }
    public String getParaTwoTextThree() {
        return paraTwoTextThree;
    }
    public void setParaTwoTextThree(String paraTwoTextThree) {
        this.paraTwoTextThree = paraTwoTextThree;
    }
    public String getParaTwoTextFour() {
        return paraTwoTextFour;
    }
    public void setParaTwoTextFour(String paraTwoTextFour) {
        this.paraTwoTextFour = paraTwoTextFour;
    }
    public String getParaTwoTextFive() {
        return paraTwoTextFive;
    }
    public void setParaTwoTextFive(String paraTwoTextFive) {
        this.paraTwoTextFive = paraTwoTextFive;
    }
    public String getParaTwoTextSix() {
        return paraTwoTextSix;
    }
    public void setParaTwoTextSix(String paraTwoTextSix) {
        this.paraTwoTextSix = paraTwoTextSix;
    }
    public String getParaTwoTextSeven() {
        return paraTwoTextSeven;
    }
    public void setParaTwoTextSeven(String paraTwoTextSeven) {
        this.paraTwoTextSeven = paraTwoTextSeven;
    }
    public String getParaTwoTextEight() {
        return paraTwoTextEight;
    }
    public void setParaTwoTextEight(String paraTwoTextEight) {
        this.paraTwoTextEight = paraTwoTextEight;
    }
    public String getParaTwoTextNine() {
        return paraTwoTextNine;
    }
    public void setParaTwoTextNine(String paraTwoTextNine) {
        this.paraTwoTextNine = paraTwoTextNine;
    }
    public String getParaTwoTextTen() {
        return paraTwoTextTen;
    }
    public void setParaTwoTextTen(String paraTwoTextTen) {
        this.paraTwoTextTen = paraTwoTextTen;
    }
    public String getParaThreeTextOne() {
        return paraThreeTextOne;
    }
    public void setParaThreeTextOne(String paraThreeTextOne) {
        this.paraThreeTextOne = paraThreeTextOne;
    }
    public String getParaThreeTextTwo() {
        return paraThreeTextTwo;
    }
    public void setParaThreeTextTwo(String paraThreeTextTwo) {
        this.paraThreeTextTwo = paraThreeTextTwo;
    }
    public String getParaThreeTextThree() {
        return paraThreeTextThree;
    }
    public void setParaThreeTextThree(String paraThreeTextThree) {
        this.paraThreeTextThree= paraThreeTextThree;
    }
    public String getParaThreeTextFour() {
        return paraThreeTextFour;
    }
    public void setParaThreeTextFour(String paraThreeTextFour) {
        this.paraThreeTextFour = paraThreeTextFour;
    }
    public String getParaThreeTextFive() {
        return paraThreeTextFive;
    }
    public void setParaThreeTextFive(String paraThreeTextFive) {
        this.paraThreeTextFive = paraThreeTextFive;
    }
    public void setParaThreeTextSix(String paraThreeTextSix) {
        this.paraThreeTextSix = paraThreeTextSix;
    }
    public String getParaThreeTextSix() {
        return paraThreeTextSix;
    }
    public String getParaThreeTextSeven() {
        return paraThreeTextSeven;
    }
    public void setParaThreeTextSeven(String paraThreeTextSeven) {
        this.paraThreeTextSeven = paraThreeTextSeven;
    }
    public String getParaFourTextOne() {
		return paraFourTextOne;
	}
	public void setParaFourTextOne(String paraFourTextOne) {
		this.paraFourTextOne = paraFourTextOne;
	}
	public String getParaFourTextTwo() {
		return paraFourTextTwo;
	}
	public void setParaFourTextTwo(String paraFourTextTwo) {
		this.paraFourTextTwo = paraFourTextTwo;
	}
	public String getParaFourTextThree() {
		return paraFourTextThree;
	}
	public void setParaFourTextThree(String paraFourTextThree) {
		this.paraFourTextThree = paraFourTextThree;
	}
	public String getParaFourTextFour() {
		return paraFourTextFour;
	}
	public void setParaFourTextFour(String paraFourTextFour) {
		this.paraFourTextFour = paraFourTextFour;
	}
	public String getParaFourTextFive() {
		return paraFourTextFive;
	}
	public void setParaFourTextFive(String paraFourTextFive) {
		this.paraFourTextFive = paraFourTextFive;
	}
	public String getParaFourTextSix() {
		return paraFourTextSix;
	}
	public void setParaFourTextSix(String paraFourTextSix) {
		this.paraFourTextSix = paraFourTextSix;
	}
	public String getParaFourTextSeven() {
		return paraFourTextSeven;
	}
	public void setParaFourTextSeven(String paraFourTextSeven) {
		this.paraFourTextSeven = paraFourTextSeven;
	}
	public String getParaFourTextEight() {
		return paraFourTextEight;
	}
	public void setParaFourTextEight(String paraFourTextEight) {
		this.paraFourTextEight = paraFourTextEight;
	}
	public String getParaFourTextNine() {
		return paraFourTextNine;
	}
	public void setParaFourTextNine(String paraFourTextNine) {
		this.paraFourTextNine = paraFourTextNine;
	}
	public String getPlanMetaTitle() {
        return planMetaTitle;
    }
    public void setPlanMetaTitle(String planMetaTitle) {
        this.planMetaTitle = planMetaTitle;
    }
    public String getPlanMetaDescription() {
        return planMetaDescription;
    }
    public void setPlanMetaDescription(String planMetaDescription) {
        this.planMetaDescription = planMetaDescription;
    }
    public String getPlanSubHeading() {
        return planSubHeading;
    }
    public void setPlanSubHeading(String planSubHeading) {
        this.planSubHeading = planSubHeading;
    }
    public String getPlanTextParaOne() {
        return planTextParaOne;
    }
    public void setPlanTextParaOne(String planTextParaOne) {
        this.planTextParaOne = planTextParaOne;
    }
    public String getPlanTextParaTwo() {
        return planTextParaTwo;
    }
    public void setPlanTextParaTwo(String planTextParaTwo) {
        this.planTextParaTwo = planTextParaTwo;
    }
    public String getPlanPageUrl() {
        return planPageUrl;
    }
    public void setPlanPageUrl(String planPageUrl) {
        this.planPageUrl = planPageUrl;
    }
    public String getPlanOperatorLogo() {
        return planOperatorLogo;
    }
    public void setPlanOperatorLogo(String planOperatorLogo) {
        this.planOperatorLogo = planOperatorLogo;
    }
}
