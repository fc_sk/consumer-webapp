package com.freecharge.seo.service;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.castor.CastorMarshaller;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.XMLConverter;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;

public class NewSEOService {

    @Resource(name="SEOXMLConverter")
    XMLConverter xmlConverter;

    @Autowired
    CastorMarshaller seoUnMarshaller;

    public static final String FILE_EXT = ".xml";

    private static final Logger logger = LoggingFactory.getLogger(NewSEOService.class);

    public void setXmlConverter(XMLConverter xmlConverter) {
        this.xmlConverter = xmlConverter;
    }
    public SEOAttributesBusinessDO populateSeoAttributes(String product, String operatorName, String channelId) {
        try {
            SEOAttributesBusinessDO seoBusinessDO = (SEOAttributesBusinessDO) xmlConverter
                    .convertFromXMLToObject(getSeoFileName(product, operatorName, channelId));
            return seoBusinessDO;
        } catch (IOException e) {
            logger.error("Error while populating SeoAttributes=",e);
            }
        return null;
    }

    /* This method helps to load plan based seo attributes used for jsp pages */
    public void populatePlanBasedSeoAttributes(SEOAttributesBusinessDO seoBusinessDO){
        if(!FCUtil.isEmpty(seoBusinessDO.getPlanMetaTitle())) {
            seoBusinessDO.setPageTitle(seoBusinessDO.getPlanMetaTitle());
        }
        if(!FCUtil.isEmpty(seoBusinessDO.getPlanMetaDescription())) {
            seoBusinessDO.setMetaDescription(seoBusinessDO.getPlanMetaDescription());
        }
    }

    @SuppressWarnings("null")
    private static String getSeoFileName(String product, String operatorName, String channelId) {
        StringBuilder seoAttributeFileName = new StringBuilder();

        if (!StringUtils.isEmpty(product)) {
            seoAttributeFileName.append(StringUtils.lowerCase(product));
        }
        if (!StringUtils.isEmpty(operatorName)) {
            seoAttributeFileName.append("-");
            seoAttributeFileName.append(StringUtils.lowerCase(operatorName));
        }
        if (!StringUtils.isEmpty(channelId) && !"null".equalsIgnoreCase(channelId)) {
            seoAttributeFileName.append("-");
            seoAttributeFileName.append(StringUtils.lowerCase(channelId));
        }
        seoAttributeFileName.append(FILE_EXT);
        return seoAttributeFileName.toString();

    }
}
