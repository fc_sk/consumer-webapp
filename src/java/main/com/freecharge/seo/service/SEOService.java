package com.freecharge.seo.service;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.castor.CastorMarshaller;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.util.XMLConverter;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;

public class SEOService {

	@Resource(name="SEOXMLConverter")
	XMLConverter xmlConverter;
	
	@Autowired
	CastorMarshaller seoUnMarshaller;
	
	public void setXmlConverter(XMLConverter xmlConverter) {
		this.xmlConverter = xmlConverter;
	}
		
	public static final String FILE_EXT = ".xml";
	public BaseBusinessDO populateSeoAttributes(BaseBusinessDO baseBusinessDO) {
		
		SEOAttributesBusinessDO seoAttributesBusinessDO = (SEOAttributesBusinessDO)baseBusinessDO;
		
		Object mapRes = null;

    	try {
			seoAttributesBusinessDO = (SEOAttributesBusinessDO) xmlConverter.convertFromXMLToObject(getSeoFileName(seoAttributesBusinessDO.getProduct(), seoAttributesBusinessDO.getOperatorName()
					, String.valueOf(seoAttributesBusinessDO.getChannelId())));
			return seoAttributesBusinessDO;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	@SuppressWarnings("null")
	private String getSeoFileName(String product, String operatorName, String channelId) {
		StringBuilder seoAttributeFileName = new StringBuilder();
		
		if (!StringUtils.isEmpty(product)) {
			seoAttributeFileName.append(StringUtils.lowerCase(product));
		}
		if (!StringUtils.isEmpty(operatorName)) {
			seoAttributeFileName.append("-");
			seoAttributeFileName.append(StringUtils.lowerCase(operatorName));
		}
		if (!StringUtils.isEmpty(channelId) && !"null".equalsIgnoreCase(channelId)) {
			seoAttributeFileName.append("-");
			seoAttributeFileName.append(StringUtils.lowerCase(channelId));
		}
		seoAttributeFileName.append(FILE_EXT);
		return seoAttributeFileName.toString();

	}
}
