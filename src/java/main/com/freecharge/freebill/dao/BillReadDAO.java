package com.freecharge.freebill.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.common.BillPaymentErrorCodeMap;
import com.freecharge.freebill.common.BillPaymentGatewayPriority;
import com.freecharge.freebill.dao.BillReadDAO.BillPaymentStatusMapper;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;

@Component
public class BillReadDAO {
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(BillReadDAO.class);
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	private static final String SQL_FIND_MERCHANT_OBJECT = "" +
            "SELECT * FROM bill_postpaid_merchants " +
            "WHERE fk_operator_master_id = :operatorID";

	private static final String SQL_FIND_ALL_MERCHANTS = "" +
	            "SELECT * FROM bill_postpaid_merchants";

	private static final String SQL_FIND_POSTPAID_OPERATOR_ID = "" +
            "SELECT postpaid_operator_master_id FROM prepaid_postpaid_operator_mapping " +
            "WHERE prepaid_operator_master_id = :prepaidOperatorID";
	
	private static final String SQL_BILL_PAYMENT_GATWEWAY_BY_OPERATOR_ID = "" +
            "SELECT * FROM bill_payment_gateway_priority " +
            "WHERE fk_operator_id =:fkOperatorId ";
	
	private static final String SQL_BILL_PAYMENT_GATWEWAY = "" +
            "SELECT * FROM bill_payment_gateway_priority ";
            
	
	private static final String SQL_BILL_PAYMENT_ERROR_CODE = "" +
            "SELECT * FROM bill_payment_errorcode_map " +
            "WHERE bill_payment_gateway_response_code =:billPaymentGatewayResponseCode and bill_payment_gateway = :billPaymentGateway";

	private static final String SQL_BILL_PAYMENT_ERROR_CODE_ALL = "" + "SELECT * FROM bill_payment_errorcode_map";
	
    private static final String SQL_GET_BILLPAYMENTS_EMAIL = ""
    		+ "SELECT bps.* FROM txn_fulfilment tf "
    		+ "INNER JOIN bill_payment_status bps on tf.order_id = bps.affiliate_trans_id "
    		+ "WHERE tf.email=:email "
    		+ "ORDER BY tf.created_at DESC";

    private static final String SQL_GET_BILLPAYMENTS_EMAIL_DATE = ""
    		+ "SELECT bps.* FROM txn_fulfilment tf "
    		+ "INNER JOIN bill_payment_status bps on tf.order_id = bps.affiliate_trans_id "
    		+ "WHERE tf.email=:email AND tf.created_at>=:fromDate and tf.created_at<=:toDate "
    		+ "ORDER BY tf.created_at DESC";

	/**
	 * Lookup operatorID in database to find Billdesk merchantID. Return NULL if
	 * not found.
	 * @param operatorID
	 * @return
	 */
	
	public String findPostpaidOperatorIdByPrepaidOperatorID(int operatorID) {
        Map<String, Integer> params = Collections.singletonMap("prepaidOperatorID", operatorID);
        List<String> rows = jdbcTemplate.queryForList(SQL_FIND_POSTPAID_OPERATOR_ID, params, String.class);
        return rows.isEmpty()? null: rows.get(0);
    }
	
	public BillPostpaidMerchant findMerchantObjectByOperatorID(int operatorID) {
        Map<String, Integer> params = Collections.singletonMap("operatorID", operatorID);
        List<BillPostpaidMerchant> rows = jdbcTemplate.query(SQL_FIND_MERCHANT_OBJECT, params, new BillPostpaidMerchantMapper());
        return rows.isEmpty()? null: rows.get(0);
    }
	
	public BillPaymentErrorCodeMap findBillPaymentInternalResponseCode(String BillPaymentGatewayResponseCode, String billpaymentGateway) {        
		List<BillPaymentErrorCodeMap> rows =  jdbcTemplate.query(SQL_BILL_PAYMENT_ERROR_CODE,
                FCUtil.createMap("billPaymentGatewayResponseCode", BillPaymentGatewayResponseCode, "billPaymentGateway", billpaymentGateway), new BillPaymentErrorCodeMapper());
        return rows.isEmpty()? null: rows.get(0);
    }

	/*
	 * Used only to cache all response codes.
	 */
	public List<BillPaymentErrorCodeMap> findAllBillPaymentInternalResponseCodeMap() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<BillPaymentErrorCodeMap> rows =  jdbcTemplate.query(SQL_BILL_PAYMENT_ERROR_CODE_ALL,paramMap, new BillPaymentErrorCodeMapper());
		return rows.isEmpty()? null: rows;
	}

	public List<BillPaymentGatewayPriority> findBillPaymentGatewayByOperatorId(String fkOperatorId) {
        List<BillPaymentGatewayPriority> rows =  jdbcTemplate.query(SQL_BILL_PAYMENT_GATWEWAY_BY_OPERATOR_ID,
                FCUtil.createMap("fkOperatorId", fkOperatorId), new BillPaymentGatewayPriorityMapper());
        return rows.isEmpty()? null: rows;
    }
	
	public List<BillPaymentGatewayPriority> findBILLPaymentGateway() {
	    Map<String, Object> paramMap = new HashMap<String, Object>();
        List<BillPaymentGatewayPriority> rows =  jdbcTemplate.query(SQL_BILL_PAYMENT_GATWEWAY,paramMap, new BillPaymentGatewayPriorityMapper());
        return rows.isEmpty()? null: rows;
    }
	
	private static final String SQL_FIND_OXIGEN_OPERATOR_MERCHANT_ID = ""
	        + "SELECT * from bill_postpaid_merchants"
	        + " where fk_operator_master_id= :operatorMasterId";
	
	
	private static final String SQL_FIND_FAILED_STATUES_BY_DATE_RANGE = ""
            + "SELECT pt.mtxn_id"
            + " FROM payment_txn pt, bill_payment_status bps, user_transaction_history uth"
            + " WHERE pt.order_id = bps.affiliate_trans_id AND uth.order_id=bps.affiliate_trans_id AND pt.is_successful=1 AND bps.updated_at >= :date_from AND bps.updated_at <= :date_to"
            + "   AND NOT bps.is_success AND uth.transaction_status not in ('00', '0','08', '-1')"
            + "   AND pt.payment_txn_id NOT IN (SELECT fk_payment_txn_id FROM payment_refund_transactions where sent_to_pg >= :date_from)";

    public List<String> findFailedStatusMtxnsByDateRange(Date from, Date to) {
        Date fromDate = FCUtil.floor(from);
        Date toDate = FCUtil.ceiling(to);
        return jdbcTemplate.queryForList(SQL_FIND_FAILED_STATUES_BY_DATE_RANGE,
                FCUtil.createMap("date_from", fromDate, "date_to", toDate), String.class);
    }

    public BillPostpaidMerchant findOperatorMerchantId(Integer operatorMasterId) {
        Map<String, Object> mapValue = new HashMap<String, Object>();
        mapValue.put("operatorMasterId", operatorMasterId);
        List<BillPostpaidMerchant> listObj = jdbcTemplate.query(SQL_FIND_OXIGEN_OPERATOR_MERCHANT_ID,mapValue, new BillPostpaidMerchantMapper());
        return listObj.isEmpty()? null : listObj.get(0) ;
    }
    
    public static class BillPaymentStatusMapper implements RowMapper<BillPaymentStatus> {
		private final List<String> fieldNames;
		public BillPaymentStatusMapper() {
			this.fieldNames = Arrays.asList(new String[] {
					"status_id", "affiliate_id", "affiliate_trans_id", "product_type", "postpaid_number",
					"postpaid_ac_number", "amount", "is_success", "auth_status",
					"auth_detail", "retry_number", "created_at", "updated_at", "bill_payment_gateway", "gateway_reference_number",
					"operator_name","circle_name","fk_product_master_id","fk_operator_master_id"
			});
		}
		public BillPaymentStatusMapper(List<String> fieldNames) {
			this.fieldNames = fieldNames;
		}
		@Override
		public BillPaymentStatus mapRow(ResultSet rs, int row) throws SQLException {
			BillPaymentStatus billPaymentStatus = new BillPaymentStatus();
			if (fieldNames.contains("affiliate_id")) billPaymentStatus.setAffiliateID(rs.getInt("affiliate_id"));
			if (fieldNames.contains("affiliate_trans_id")) billPaymentStatus.setAffiliateTransID(rs.getString("affiliate_trans_id"));
			if (fieldNames.contains("amount")) billPaymentStatus.setAmount(rs.getBigDecimal("amount"));
			if (fieldNames.contains("auth_detail")) billPaymentStatus.setAuthDetail(rs.getString("auth_detail"));
			if (fieldNames.contains("auth_status")) billPaymentStatus.setAuthStatus(rs.getString("auth_status"));
			if (fieldNames.contains("created_at")) billPaymentStatus.setCreatedAt(rs.getTimestamp("created_at"));
			if (fieldNames.contains("postpaid_ac_number")) billPaymentStatus.setPostpaidAccountNumber(rs.getString("postpaid_ac_number"));
			if (fieldNames.contains("postpaid_number")) billPaymentStatus.setPostpaidNumber(rs.getString("postpaid_number"));
			if (fieldNames.contains("product_type")) billPaymentStatus.setProductType(rs.getString("product_type"));
			if (fieldNames.contains("retry_number")) billPaymentStatus.setRetryNumber(rs.getInt("retry_number"));
			if (fieldNames.contains("is_success")) billPaymentStatus.setSuccess((Boolean)(rs.getObject("is_success")==null ?
					null : rs.getBoolean("is_success") ));
			if (fieldNames.contains("updated_at")) billPaymentStatus.setUpdatedAt(rs.getTimestamp("updated_at"));
			if (fieldNames.contains("status_id")) billPaymentStatus.setId(rs.getLong("status_id"));
			if (fieldNames.contains("bill_payment_gateway")) billPaymentStatus.setBillPaymentGateway(rs.getString("bill_payment_gateway"));
			if (fieldNames.contains("gateway_reference_number")) billPaymentStatus.setGatewayReferenceNumber(rs.getString("gateway_reference_number"));
			if (fieldNames.contains("operator_name")) billPaymentStatus.setOperatorName(rs.getString("operator_name"));
			if (fieldNames.contains("circle_name")) billPaymentStatus.setCircleName(rs.getString("circle_name"));
			if (fieldNames.contains("fk_product_master_id")) billPaymentStatus.setFk_product_master_id(rs.getInt("fk_product_master_id"));
			if (fieldNames.contains("fk_operator_master_id")) billPaymentStatus.setFk_operator_master_id(rs.getInt("fk_operator_master_id"));
			return billPaymentStatus;
		}
	}
	
	public static class BillTxnHomePageMapper implements RowMapper<BillTxnHomePage> {
		@Override
		public BillTxnHomePage mapRow(ResultSet rs, int row) throws SQLException {
			BillTxnHomePage billTxnHomePage = new BillTxnHomePage();
			billTxnHomePage.setId(rs.getLong("id"));
			billTxnHomePage.setPostpaidNumber(rs.getString("postpaid_phone_number"));
			billTxnHomePage.setPostpaidAccountNumber(rs.getString("postpaid_account_number"));
			billTxnHomePage.setPostpaidMerchantName(rs.getString("postpaid_merchant_name"));
			billTxnHomePage.setBillAmount(rs.getBigDecimal("bill_amount"));
			billTxnHomePage.setBillDueDate(rs.getTime("bill_due_date"));
			billTxnHomePage.setCreatedAt(rs.getTimestamp("created_at"));
			billTxnHomePage.setPostpaidMerchantName(rs.getString("bill_provider"));
			return billTxnHomePage;
		}
	}
	
	public static class BillPostpaidMerchantMapper implements RowMapper<BillPostpaidMerchant> {
        @Override                     
        public BillPostpaidMerchant mapRow(ResultSet rs, int row) throws SQLException {
            BillPostpaidMerchant billPostPaidMerchant = new BillPostpaidMerchant();
            billPostPaidMerchant.setPostpaidMerchantId(rs.getInt("postpaid_merchant_id"));
            billPostPaidMerchant.setFkOperatorMasterId(rs.getInt("fk_operator_master_id"));
            billPostPaidMerchant.setBilldeskMerchantId(rs.getString("billdesk_merchant_id"));
            billPostPaidMerchant.setOxigenMerchantId(rs.getString("oxigen_merchant_id"));
            billPostPaidMerchant.setEuronetMerchantId(rs.getString("euronet_merchant_id"));
            return billPostPaidMerchant;
        }
    }
	
    private static final String SQL_FIND_BILLPAYMENT_ATTEMPT = "SELECT * FROM bill_payment_attempts "
                                                                     + "WHERE fk_status_id = :statusID ORDER BY attempt_id DESC LIMIT 1";

    public BillPaymentAttempt findBillPaymentAttemptByStatusId(long statusId) {
        /*
         * To avoid unnecessary memory allocation we used fixed size immutable
         * singletonMap
         */
    	try {
    		Map<String, Long> params = Collections.singletonMap("statusID", statusId);
            BillPaymentAttempt billPaymentAttempt = jdbcTemplate.queryForObject(SQL_FIND_BILLPAYMENT_ATTEMPT, params,
                    new BillPaymentAttemptMapper());
            return billPaymentAttempt;
    	} catch (EmptyResultDataAccessException er) {
    		logger.error("No bill_payment_attempts records for status id " + statusId, er);
    		return null;
    	}
    }
    
	private static final String SQL_FIND_BILLPAYMENT_ATTEMPTS = "SELECT * FROM bill_payment_attempts "
			+ "WHERE fk_status_id = :statusID ORDER BY attempt_id DESC";
	private static final String SQL_FIND_BILLPAYMENT_ATTEMPT_ORDERID = "SELECT * FROM bill_payment_status bps,bill_payment_attempts bpa "
			+ "WHERE bps.status_id=bpa.fk_status_id and bps.affiliate_trans_id = :affiliate_trans_id";
	private static final String SQL_FIND_BILLPAYMENT_STATUS_ORDERID = "SELECT bps.* FROM bill_payment_status bps where bps.affiliate_trans_id = :affiliate_trans_id";

	 public List<BillPaymentAttempt> findBillPaymentAttemptsByStatusId(long statusId) {
        /*
        * To avoid unnecessary memory allocation we used fixed size immutable
        * singletonMap
        */
        Map<String, Long> params = Collections.singletonMap("statusID", statusId);
        List<BillPaymentAttempt> billPaymentAttempts = jdbcTemplate.query(SQL_FIND_BILLPAYMENT_ATTEMPTS, params,
                new BillPaymentAttemptMapper());
        return billPaymentAttempts;
    }

    public static class BillPaymentAttemptMapper implements RowMapper<BillPaymentAttempt> {
        @Override
        public BillPaymentAttempt mapRow(ResultSet rs, int row) throws SQLException {
            BillPaymentAttempt billPaymentAttempt = new BillPaymentAttempt();
            billPaymentAttempt.setId(rs.getLong("attempt_id"));
            billPaymentAttempt.setStatusID(rs.getLong("fk_status_id"));
            billPaymentAttempt.setRawRequest(rs.getString("raw_request"));
            billPaymentAttempt.setRawResponse(rs.getString("raw_response"));
            billPaymentAttempt.setSuccess(rs.getBoolean("is_success"));
            billPaymentAttempt.setAuthStatus(rs.getString("auth_status"));
            billPaymentAttempt.setAuthDetail(rs.getString("auth_detail"));
            billPaymentAttempt.setCreatedAt(rs.getTimestamp("created_at"));
            return billPaymentAttempt;
        }
    }
    
    public static class BillPaymentErrorCodeMapper implements RowMapper<BillPaymentErrorCodeMap> {
		@Override
		public BillPaymentErrorCodeMap mapRow(ResultSet rs, int row) throws SQLException {
			BillPaymentErrorCodeMap billPaymentErrorCodeMapObj = new BillPaymentErrorCodeMap();
			billPaymentErrorCodeMapObj.setId(rs.getInt("id"));
			billPaymentErrorCodeMapObj.setBillPaymentGateway(rs.getString("bill_payment_gateway"));
			billPaymentErrorCodeMapObj.setBillPaymentGatewayResponseCode(rs.getString("bill_payment_gateway_response_code"));
			billPaymentErrorCodeMapObj.setBillPaymentInternalResponseCode(rs.getString("bill_payment_internal_response_code"));
			billPaymentErrorCodeMapObj.setBillPaymentInyternalMessage(rs.getString("bill_payment_internal_message"));
			billPaymentErrorCodeMapObj.setIsRetryable(rs.getBoolean("is_retryable"));
			billPaymentErrorCodeMapObj.setIsDisplayEnabled(rs.getBoolean("is_display_enabled"));
			return billPaymentErrorCodeMapObj;
		}
	}
    
    public static class BillPaymentGatewayPriorityMapper implements RowMapper<BillPaymentGatewayPriority> {
        @Override
        public BillPaymentGatewayPriority mapRow(ResultSet rs, int row) throws SQLException {
            BillPaymentGatewayPriority billPaymentGatewayPriority = new BillPaymentGatewayPriority();
            billPaymentGatewayPriority.setId(rs.getInt("id"));
            billPaymentGatewayPriority.setBillType(rs.getString("bill_type"));
            billPaymentGatewayPriority.setBillProvider(rs.getString("bill_provider"));
            billPaymentGatewayPriority.setIsActive(rs.getBoolean("is_active"));
            billPaymentGatewayPriority.setFkOperatorId(rs.getInt("fk_operator_id"));
            return billPaymentGatewayPriority;
        }
    }

    public List<BillPostpaidMerchant> findAllPostpaidMerchants() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        List<BillPostpaidMerchant> rows =  jdbcTemplate.query(SQL_FIND_ALL_MERCHANTS,paramMap, new BillPostpaidMerchantMapper());
        return rows.isEmpty()? null: rows;
    }
    
    public List<BillPaymentStatus> getAllBillPaymentsForEmail(String email) {
    	
		List<BillPaymentStatus> billPayments = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
        try {
			paramMap.put("email", email);
			billPayments =  jdbcTemplate.query(SQL_GET_BILLPAYMENTS_EMAIL, paramMap, new BillPaymentStatusMapper());
		} catch (DataAccessException e) {
			logger.error("Exception Occured In Bill Payments Retrieval", e);
		} 
        
        return billPayments;
	}
	
	public List<BillPaymentStatus> getAllBillPaymentsForEmailAndDate(String email, Timestamp fromDate, Timestamp toDate) {
		
		List<BillPaymentStatus> billPayments = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
        try {
			paramMap.put("email", email);
			paramMap.put("fromDate", fromDate);
			paramMap.put("toDate", toDate);
			
			billPayments =  jdbcTemplate.query(SQL_GET_BILLPAYMENTS_EMAIL_DATE, paramMap, new BillPaymentStatusMapper());
		} catch (DataAccessException e) {
			logger.error("Exception Occured In Bill Payments Retrieval", e);
		}
        
        return billPayments;
	}
	
	public List<BillPaymentAttempt> findBillPaymentAttemptByOrderId(String orderId) {
		try {
			Map<String, String> params = Collections.singletonMap("affiliate_trans_id", orderId);
			List<BillPaymentAttempt> billPaymentAttempts = null;
			billPaymentAttempts = jdbcTemplate.query(SQL_FIND_BILLPAYMENT_ATTEMPT_ORDERID, params,
					new BillPaymentAttemptMapper());
			return billPaymentAttempts;
		} catch (EmptyResultDataAccessException er) {
			logger.error("No bill_payment_attempts records for status id " + orderId, er);
			return null;
		}

	}
	
	public BillPaymentStatus findBillPaymentStatusByOrderId(String orderId) {
        try {
            Map<String, String> params = Collections.singletonMap("affiliate_trans_id", orderId);
            BillPaymentStatus billpaymentStatus = jdbcTemplate.queryForObject(SQL_FIND_BILLPAYMENT_STATUS_ORDERID, params,
                    new BillPaymentStatusMapper());
            return billpaymentStatus;
        } catch (EmptyResultDataAccessException er) {
            logger.error("No bill_payment_attempts records for status id " + orderId, er);
            return null;
        }
    }
}
