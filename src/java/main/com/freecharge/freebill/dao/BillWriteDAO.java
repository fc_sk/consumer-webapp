package com.freecharge.freebill.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.dao.BillReadDAO.BillPaymentStatusMapper;
import com.freecharge.freebill.dao.BillReadDAO.BillTxnHomePageMapper;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.freebill.statuscheckservice.BillPaymentStatusCheckResponse;

@Component
public class BillWriteDAO {
    private static Logger logger = LoggingFactory.getLogger(BillReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertIntoBillPaymentAttempt;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        insertIntoBillPaymentAttempt = new SimpleJdbcInsert(dataSource).withTableName("bill_payment_attempts").usingGeneratedKeyColumns("attempt_id", "n_last_updated", "n_created");
	}

	private static final String SQL_INSERT_BILL_PAYMENT_STATUS = ""
            + "INSERT INTO bill_payment_status ("
            + "  affiliate_id, affiliate_trans_id, product_type, postpaid_number ,postpaid_ac_number,"
            + "  amount, bill_payment_gateway,"
            + "  gateway_reference_number, is_success, auth_status, auth_detail, created_at, "
            + "  updated_at,operator_name, circle_name, fk_operator_master_id, fk_product_master_id"
            + ") VALUES ("
            + "  :affiliate_id, :affiliate_trans_id, :product_type, :postpaid_number, :postpaid_ac_number,"
            + "  :amount,:bill_payment_gateway,:gateway_reference_number, :is_success, :auth_status, "
            + "  :auth_detail, :created_at, :updated_at, :operator_name, :circle_name, :fk_operator_id, :fk_product_id "
            + ")";
    
    private static final String SQL_UPDATE_BILL_PAYMENT_STATUS = ""
            + "UPDATE bill_payment_status SET"
            + "  product_type=:product_type, postpaid_number=:postpaid_number, postpaid_ac_number=:postpaid_ac_number,"
            + "  amount=:amount, bill_payment_gateway=:bill_payment_gateway, gateway_reference_number=:gateway_reference_number, is_success=:is_success,"
            + "  auth_status=:auth_status, auth_detail=:auth_detail, retry_number=:retry_number,"
            + "  updated_at=:updated_at, fk_operator_master_id=:fk_operator_id, fk_product_master_id=:fk_product_id, operator_name=:operator_name,"
            + "  circle_name=:circle_name"
            + "  WHERE "
            + "  affiliate_id=:affiliate_id and affiliate_trans_id=:affiliate_trans_id";
    
    private static final String SQL_FIND_BILLPAYMENTSTATUS = "" +
            "SELECT * FROM bill_payment_status " +
            "WHERE affiliate_trans_id = :affiliateTransId";

    private static final String SQL_FIND_BILLTXNHOMEPAGE = "" +
            "SELECT * FROM bill_txn_home_page " +
            "WHERE id = :id";

	private static final String SQL_UPDATE_BILL_PAYMENT_ATTEMPT = ""
			+"UPDATE bill_payment_attempts SET"
			+" raw_response=:raw_response, is_success=:is_success, auth_status=:auth_status, auth_detail=:auth_detail "
			+" WHERE "
			+" attempt_id=:attempt_id ";

    public void updateBillPaymentStatus(BillPaymentStatus req, int trialNumber) {
        Timestamp now = new Timestamp(new Date().getTime());
        Map<String, Object> paramMap = FCUtil.createMap(
                "affiliate_id",       (Object) req.getAffiliateID(),
                "affiliate_trans_id", req.getAffiliateTransID(),
                "product_type",       req.getProductType(),
                "postpaid_number",    req.getPostpaidNumber(),
                "postpaid_ac_number", req.getPostpaidAccountNumber(),
                "amount",             req.getAmount(),
                "bill_payment_gateway",             req.getBillPaymentGateway(),
                "gateway_reference_number",             req.getGatewayReferenceNumber(),
                "is_success",   req.getSuccess(),
                "auth_status",  req.getAuthStatus(),
                "auth_detail",  req.getAuthDetail(),
                "retry_number", trialNumber,
                "updated_at", now,
                "fk_operator_id", req.getFk_operator_master_id(),
                "fk_product_id", req.getFk_product_master_id(),
                "operator_name", req.getOperatorName(),
                "circle_name", req.getCircleName());
        jdbcTemplate.update(SQL_UPDATE_BILL_PAYMENT_STATUS, new MapSqlParameterSource(paramMap));
    }
    
    public long createBillPaymentStatus(BillPaymentStatus req) {
        Timestamp now = new Timestamp(new Date().getTime());
        Map<String, Object> paramMap = FCUtil.createMap(
                "affiliate_id",       (Object) req.getAffiliateID(),
                "affiliate_trans_id", req.getAffiliateTransID(),
                "product_type",       req.getProductType(),
                "postpaid_number",    req.getPostpaidNumber(),
                "postpaid_ac_number", req.getPostpaidAccountNumber(),
                "amount",             req.getAmount(),
                "bill_payment_gateway",             req.getBillPaymentGateway(),
                "gateway_reference_number",             req.getGatewayReferenceNumber(),
                //
                "is_success",   req.getSuccess(),
                "auth_status",  req.getAuthStatus(),
                "auth_detail",  req.getAuthDetail(),
                //
                "created_at", now,
                "updated_at", now,
                "operator_name",      req.getOperatorName(),
                "circle_name",        req.getCircleName(),
                "fk_operator_id",     req.getFk_operator_master_id(),
                "fk_product_id",      req.getFk_product_master_id());
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_INSERT_BILL_PAYMENT_STATUS, new MapSqlParameterSource(paramMap), holder);
        List<Map<String, Object>> keys = holder.getKeyList();
        if (keys.size() > 1) {
            Map<String, Object> p1 = keys.get(0);
            Map<String, Object> p2 = keys.get(1);
            Long k1 = (Long) p1.get(p1.keySet().iterator().next());
            Long k2 = (Long) p1.get(p2.keySet().iterator().next());
            return Math.min(k1, k2);
        }
        return (Long) holder.getKey();
    }
    

	public long insertBillPaymentAttempt(BillPaymentAttempt attempt) {
		Map<String, Object> args = FCUtil.createMap(
				"fk_status_id", (Object) attempt.getStatusID(),
				"raw_request",  attempt.getRawRequest(),
				"raw_response", attempt.getRawResponse(),
				"is_success",   attempt.getSuccess(),
				"auth_status",  attempt.getAuthStatus(),
				"auth_detail",  attempt.getAuthDetail(),
				"created_at",   new Timestamp(new Date().getTime()));
		return (Long) insertIntoBillPaymentAttempt.executeAndReturnKey(args);
	}

	public BillPaymentStatus findBillPaymentStatusByOrderId(String orderId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
        paramMap.put("affiliateTransId", orderId);
        List<BillPaymentStatus> listOfObj =  jdbcTemplate.query(SQL_FIND_BILLPAYMENTSTATUS, paramMap, new BillPaymentStatusMapper());
        if (listOfObj!=null && !listOfObj.isEmpty()) {
            return listOfObj.get(0);
        } 
    } catch(Exception exception) {
            logger.error("Exception occured during findBillPaymentStatusByOrderId(" +
                    "String orderId) method of BillReadDAO class ", exception);
        }
        return null;
    }

    public BillTxnHomePage findBillTxnHomePageById(String id) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
        paramMap.put("id", id);
        List<BillTxnHomePage> listOfObj =  jdbcTemplate.query(SQL_FIND_BILLTXNHOMEPAGE, paramMap, new BillTxnHomePageMapper());
        if (listOfObj!=null && !listOfObj.isEmpty()) {
            return listOfObj.get(0);
        } 
    } catch(Exception exception) {
            logger.error("Exception occured during findBillTxnHomePageById(" +
                    "String id) method of BillReadDAO class ", exception);
        }
        return null;
    }
    
    public void updateBillPaymentGatewayPriority(String operatorId,String priority) {
        String updateString = "update bill_payment_gateway_priority set bill_provider = :priority WHERE fk_operator_id = :operatorId";
        Map<String, Object> namedParameters = new HashMap<String, Object>();
        namedParameters.put("operatorId", operatorId);
        namedParameters.put("priority", priority);
        jdbcTemplate.update(updateString, namedParameters);
    }

	public void updateBillPaymentAttempt(BillPaymentStatus billPaymentStatus,
			BillPaymentStatusCheckResponse billPaymentStatusCheckResponse,BillPaymentAttempt billPaymentAttempt) {
		try {			
			Map<String, Object> paramMap = FCUtil.createMap("raw_response",
					(Object) billPaymentStatusCheckResponse.getRowResponse(), "is_success",
					billPaymentStatus.getSuccess(), "auth_status", billPaymentStatus.getAuthStatus(), "auth_detail",
					billPaymentStatus.getAuthDetail(), "attempt_id",
					billPaymentAttempt.getId());
			jdbcTemplate.update(SQL_UPDATE_BILL_PAYMENT_ATTEMPT, new MapSqlParameterSource(paramMap));
		} catch (Exception e) {
			logger.error("Exception caught while updating bill_payment_attempts ", e);
		}

	}

}
