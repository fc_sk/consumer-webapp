package com.freecharge.freebill.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.dao.BillReadDAO.BillPaymentStatusMapper;
import com.freecharge.freebill.domain.BillPaymentStatus;

public class BillPaymentStatusReadDao {

    private static Logger logger = LoggingFactory.getLogger(BillReadDAO.class);
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final String SQL_FIND_UNKNOWN_BILL_PAYMENT_TXN = ""
            + "SELECT * FROM bill_payment_status "
            + "WHERE is_success is null and created_at >= :from and created_at<= :to";

    public List<BillPaymentStatus> findUnknownStatusBillPaymentTxn(Date from, Date to) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
            paramMap.put("from", from);
            paramMap.put("to", to);

            List<BillPaymentStatus> listOfObj = jdbcTemplate.query(SQL_FIND_UNKNOWN_BILL_PAYMENT_TXN, paramMap,
                    new BillPaymentStatusMapper());
            if (listOfObj != null && !listOfObj.isEmpty()) {
                return listOfObj;
            }
        } catch (Exception exception) {
            logger.error("exception occured during findUnknownStatusBillPaymentTxn ", exception);
        }
        return null;
    }
}
