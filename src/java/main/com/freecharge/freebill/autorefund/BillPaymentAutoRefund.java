package com.freecharge.freebill.autorefund;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.util.PaymentConstants;

public class BillPaymentAutoRefund {
    
    @Autowired
    private BillReadDAO billReadDAO;
    
    @Autowired
    private PaymentRefund paymentRefund;

    private static final Logger logger = LoggingFactory
            .getLogger(BillPaymentAutoRefund.class);

   
    public void doBillPaymentRefundForBillPaymentFailedTransactions(Date startDate, Date endDate) {
        logger.info("In BillPaymentAutoRefund for doBillPaymentRefundForBillPaymentFailedTransactions ");
        List<String> listOfFailedBillPaymentMtxnId = billReadDAO.findFailedStatusMtxnsByDateRange(startDate, endDate);
        if(listOfFailedBillPaymentMtxnId!=null)
        logger.info("size of listOfFailedBillPaymentMtxnId " + listOfFailedBillPaymentMtxnId.size());
        if (listOfFailedBillPaymentMtxnId!=null && !listOfFailedBillPaymentMtxnId.isEmpty()) {
            
            for(String failedMtxnId : listOfFailedBillPaymentMtxnId) {
                try {
                    Set<String> merchantOrderIds = new HashSet<String>();
                    merchantOrderIds.add(failedMtxnId);
                    logger.info("Started bill Pyment refund for mtxn id " + failedMtxnId);
                    paymentRefund.doBillPaymentRefund(merchantOrderIds,PaymentConstants.RECHARGE_FAILURE);
               } catch (Exception exception) {
                   logger.error("error occured while refunding to bank for bill payment failure transaction " + failedMtxnId , exception);
               }
            }
        } else {
             logger.info("No order ids found for refunding failure bill payment transactions");   
        }
        
    }
}
