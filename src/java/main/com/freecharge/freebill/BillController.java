package com.freecharge.freebill;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

import com.freecharge.web.util.WebConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.snapdeal.payments.ts.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.UserService;
import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.cartcheckout.CreateCartTaskRequest;
import com.freecharge.cartcheckout.constants;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.client.CreateCartRequest;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freebill.service.BillPostpaidValidation;
import com.freecharge.freebill.service.BillerMobilePostpaid;
import com.freecharge.freebill.service.MobilePostpaidService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.services.PostpaidFetchBillService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.recharge.InTransactionHelper;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.util.BaseValidationService;
import com.freecharge.web.webdo.CommonSessionWebDo;
import com.google.common.collect.ImmutableMap;
import com.snapdeal.payments.ts.dto.TaskDTO;
import com.snapdeal.payments.ts.exception.DuplicateTaskException;
import com.snapdeal.payments.ts.exception.InvalidTaskTypeException;

@Controller
@RequestMapping("/rest/bill/*")
public class BillController {



	protected final Logger logger = LoggingFactory.getLogger(getClass().getName());

	@Autowired
	MobilePostpaidService mobilePostpaidService;

	@Autowired
	BillPostpaidValidation billPostpaidValidation;

	@Autowired
	BillPaymentService billTxnHomePageService;

	@Autowired
	OperatorCircleService prefixService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private OperatorAlertService alertService;
	
	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
    private CartService cartService;
	

	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	BaseValidationService baseValidationService;

	@Autowired
	RectifyTranslator rectifyTranslator;

	@Autowired
	private InTransactionHelper inTransactionHelper;
	
	@Autowired
	private PostpaidFetchBillService postpaidFetchBillService;

	@Autowired
	private TaskScheduler taskScheduler;

	@Autowired
	CartCheckoutServiceClientImpl cartClient;
	

	private final String ERROR_MSG   = "errorMessage";
    private final String ERROR_CODE = "errorCode";
    private final String CALL_STATUS = "callStatus";
    private final String FAILURE = "failure";
    private final String SUCCESS = "success";
    private final String LOOKUP_ID = "lookupId";
    private final String HOME_PAGE_ID = "txnHomePgID";
    
    private final String VODAFONE = "Vodafone Postpaid";
    private final String AIRTEL = "Airtel Postpaid";   
   

    private final Map<String, String> INTERNAL_ERROR = ImmutableMap.of(
            CALL_STATUS, FAILURE, 
            ERROR_CODE, ErrorCode.INTERNAL_SERVER_ERROR.getErrorNumberString()
    );

	@NoLogin
	@RequestMapping(value = "postpaid/mobile/{mobileNumber}/operator/{operatorID}/amount/{amount}", method = RequestMethod.POST)
	public @ResponseBody Map<String, String>  onMobileProceed(@PathVariable String mobileNumber, @PathVariable String operatorID, @PathVariable String amount) {
		return onMobileProceedNew(mobileNumber,operatorID,amount);
	   
	}
	
	@NoLogin
	@RequestMapping(value = "rectifyTranslator", method = RequestMethod.GET)
	public @ResponseBody String rectifyTranslator() {
		rectifyTranslator.rectifyTranslatorForOrdersAsync("/tmp/OrderData.xls");
		return "Processed";
	}
	
	private Map<String, String> onMobileProceedNew(String mobileNumber, String operatorID, String amount) {
	    try {
            ErrorCode errorCode = billPostpaidValidation.postpaidRequestParamvalidation(mobileNumber, operatorID, amount);
            if (errorCode != ErrorCode.NO_ERROR) {
                   return errorResponse(errorCode);
            }
                       
            if (appConfig.isOperatorAlertBlockEnabled()) {
            	if(alertService.isOperatorInAlert(Integer.valueOf(operatorID),Integer.valueOf(RechargeConstants.RECHARGE_CIRCLE_FOR_ALL))){
            	
            		return ErrorCode.OPERATOR_DOWN.restResponse();
            	}
                
            }

			if (appConfigService.isInTransactionValidationEnabled()) {
				// build recharge request object
				RechargeRequest postPaidRequest = new RechargeRequest();
				postPaidRequest.setOperator(operatorID);
				postPaidRequest.setCircleName("33");
				postPaidRequest.setServiceNumber(mobileNumber);
				//postPaidRequest.setAmount(Float.parseFloat("50"));
				postPaidRequest.setAmount(Float.parseFloat(amount));
				postPaidRequest.setProductType(ProductName.MobilePostpaid.getProductType());

				ErrorCode planValidation = inTransactionHelper.validate(postPaidRequest, null, mobileNumber);
				if (planValidation != ErrorCode.NO_ERROR) {
					//return planValidation.restResponse();
					return failureResponse(planValidation.getErrorNumberString(), planValidation.getErrorMessage());
				}
			}
            
			SuccessfulRechargeInfo previousRecharge = userService.getLastSuccessfulRechargeByNumber(mobileNumber);
			if (appConfig.isRepeatRechargeBlockingEnabled()) {
				if (previousRecharge != null) {
					final String previousRechargedOperator = previousRecharge.getOperatorCode();
					if (!StringUtils.isBlank(previousRechargedOperator)) {
						if (VODAFONE.equals(previousRechargedOperator)) {
							return ErrorCode.REPEAT_TRANSACTION_EIGHT_MINUTES.restResponse();
						} else if (AIRTEL.equals(previousRechargedOperator)) {
							return ErrorCode.REPEAT_TRANSACTION_EIGHT_MINUTES.restResponse();
						}
					}
				}
			}
                BillPaymentCommonValidationApiResponse response = billPostpaidValidation.validate(operatorID, mobileNumber, null, new BigDecimal(amount));
                TxnHomePage thp = billTxnHomePageService.saveMobileTransactionInfo(mobileNumber, Double.parseDouble(amount), response, operatorID);
                
                updateSession(mobileNumber, amount, thp);
                
                return successResponse(thp.getLookupId(), thp.getId());

        } catch (Exception e) {
            logger.error(String.format("Error validating postpaid mobile=%s, merchantID=%s, amount=%s", mobileNumber, operatorID, amount), e);
            return INTERNAL_ERROR;
        }           
    }

	private Map<String, String> failureResponse(String errorCode, String errorMessage) {
		Map<String, String> map = new HashMap<>();
		map.put(CALL_STATUS, FAILURE);
		if (errorCode != null && !errorCode.isEmpty()) {
			map.put(ERROR_CODE, errorCode);
		}
		if (errorMessage != null && !errorMessage.isEmpty()) {
			map.put(ERROR_MSG, errorMessage);
		}
		return map;
	}


	private Map<String, String> onMobileProceedNewV3(String mobileNumber, String operatorID, String amount) {
		try {
			ErrorCode errorCode = billPostpaidValidation.postpaidRequestParamvalidationV3(mobileNumber, operatorID,
					amount);			
			
			if (errorCode != ErrorCode.NO_ERROR) {
				return baseValidationService.processError(errorCode);
			}
			if(isPostpaidBillerValidationEnabledForOperator(operatorID)) {
				logger.info(String.format("Trying postpaid biller validation for operator : %s, amount : %s, mobileNumber : %s", operatorID, amount, mobileNumber));
				RechargeRequest rechargeRequest = new RechargeRequest();
				try {
					Float amountFlt = Float.parseFloat(amount);
					rechargeRequest.setAmount(amountFlt);
				}catch (NumberFormatException e) {
					logger.error("Unable to parse amount : " + amount + " ,because of error : ", e);
				}
				rechargeRequest.setServiceNumber(mobileNumber);
				rechargeRequest.setOperator(operatorID);
				rechargeRequest.setProductType(ProductMaster.ProductName.MobilePostpaid.getProductType());
				rechargeRequest.setCircleName("-1");
				String rechargePlanType = RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
				ErrorCode planValidation = inTransactionHelper.validate(rechargeRequest, rechargePlanType, mobileNumber);
				if (planValidation != ErrorCode.NO_ERROR) {
					return failureResponse(planValidation.getErrorNumberString(), planValidation.getErrorMessage());
				}
			}
			Map<String, String> validation = baseValidationService.validatePostpaidProduct(mobileNumber, operatorID, amount);
			if (validation != null && !validation.isEmpty()) {
				return validation;
			}
			
			 if (appConfig.isOperatorAlertBlockEnabled()) {
	            	if(alertService.isOperatorInAlert(Integer.valueOf(operatorID),Integer.valueOf(RechargeConstants.RECHARGE_CIRCLE_FOR_ALL))){
	            	
	            		return baseValidationService.processError(ErrorCode.OPERATOR_DOWN);
	            	}
	                
	            }

		
			BillPaymentCommonValidationApiResponse response = billPostpaidValidation.validate(operatorID, mobileNumber,
					null, new BigDecimal(amount));
			TxnHomePage thp = billTxnHomePageService.saveMobileTransactionInfo(mobileNumber, Double.parseDouble(amount),
					response, operatorID);

			updateSession(mobileNumber, amount, thp);

			return successResponse(thp.getLookupId(), thp.getId());

		} catch (Exception e) {
			logger.error(String.format("Error validating postpaid mobile=%s, merchantID=%s, amount=%s", mobileNumber,
					operatorID, amount), e);
			return baseValidationService.processError(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}
	
	private boolean isPostpaidBillerValidationEnabledForOperator(String operatorId) {
        JSONObject configJson = appConfigService.getCustomProp(appConfigService.RECHARGE_CUSTOM_CONFIG);

        if (configJson != null) {
            final String postpaidBillerValidationOperators = (String) configJson.get(appConfigService.POSTPAID_BILLER_VALIDATION_OPERATORS);
            logger.info("Postpaid biller validation operators : " + postpaidBillerValidationOperators);
            if (!StringUtils.isBlank(postpaidBillerValidationOperators)) {
                List<String> eventList = Arrays.asList(postpaidBillerValidationOperators.split(","));

                if (eventList.contains(operatorId)) {
                    return true;
                }

            }
        }

        return false;
    }

    @NoLogin
    @RequestMapping(value = "v2/postpaid/mobile/{mobileNumber}/operator/{operatorID}/amount/{amount}", method = RequestMethod.POST)
    public @ResponseBody Map<String, String> onMobileProceedV2(@PathVariable String mobileNumber,
            @PathVariable String operatorID, @PathVariable String amount) {
        Map<String, String> response = onMobileProceedNew(mobileNumber, operatorID, amount);
        if (SUCCESS.equals(response.get(CALL_STATUS))) {
            String lookupId = response.get(LOOKUP_ID);
			cartService.createCartByLookupId(lookupId);
        }
        return response;
    }
    
	@NoLogin
	@RequestMapping(value = "v3/postpaid/mobile/{mobileNumber}/operator/{operatorID}/amount/{amount}", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> onMobileProceedV3(RechargeRequest rechargeRequest, BindingResult bindingResult, @PathVariable String mobileNumber,
															   @PathVariable String operatorID, @PathVariable String amount) {
		Map<String, String> response = onMobileProceedNewV3(mobileNumber, operatorID, amount);
		if (SUCCESS.equals(response.get(CALL_STATUS))) {
			String lookupId = null;
			try {
				lookupId = response.get(LOOKUP_ID);
				cartService.createCartByLookupId(lookupId, rechargeRequest.getPlanId(), rechargeRequest.getPlanCategory());

				CreateCartRequest createCartRequest = new CreateCartTaskRequest();
				logger.info("creating cart request for lookupid :"+lookupId);
				createCartRequest.setCategoryName(FCConstants.productTypeToNameMap.get("M"));
				createCartRequest.setLookupId(lookupId);
				createCartRequest.setProductType(ProductMaster.ProductName.MobilePostpaid.getProductType());
				createCartRequest.setAmount(new Float(amount));
				createCartRequest.setCircleName("-1");
				createCartRequest.setOperatorId(operatorID);
				logger.info("Setting userid for cart request "+ FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
				createCartRequest.setUserId((String) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
				try{
						logger.debug("Created cart request" + createCartRequest.toString());
						logger.info("calling cart checkout service to create cart");
						CheckoutResponse checkoutResponse = cartClient.createCart(createCartRequest);
						logger.info("cart id returned from checkout" + checkoutResponse.getCheckoutId());
				}
				catch(Exception exp){

					logger.info("Unable to create cart in cartcheckout. Scheduling task scheduler for lookupid: "+createCartRequest.getLookupId());
					CreateCartTaskRequest createCartTaskRequest = (CreateCartTaskRequest) createCartRequest;
					String taskId = generateTaskId();
					createCartTaskRequest.setTaskId(taskId);
					//check why taskdto should be made as final
					TaskDTO taskDTO = new TaskDTO();
					taskDTO.setRequest(createCartTaskRequest);
					taskDTO.setTaskType(constants.CREATE_CART_TASK_TYPE);

					taskDTO.setCurrentScheduleTime(new Date());
					try {
						taskScheduler.submitTask(taskDTO);
					} catch (DuplicateTaskException e) {

						final String exceptionMsg =
								String.format("DuplicateTaskException while creating cart, "
										+ "taskId: %s", taskId);
						logger.error(exceptionMsg, e);
					} catch (InvalidTaskTypeException e) {

						String exceptionMsg =
								String.format(
										"InvalidTaskTypeException while creating IMSWalletTask, "
												+ " taskId: %s", taskId);
						logger.error(exceptionMsg, e);
						throw e;
					}
				}


			} catch (Exception e) {
				logger.error("Failed to create cart for lookupid " + lookupId);
				return baseValidationService.processError(ErrorCode.INTERNAL_SERVER_ERROR);
			}
		}
		return response;
	}
    
	
	private List<OperatorAlert> getActiveAlertsForNumber(String serviceNumber, String operatorID) {
	    List<Map<String,Object>> mnpData = prefixService.getMNPOperator(serviceNumber, String.valueOf(FCConstants.PRODUCT_ID_RECHARGE_MOBILE));
        String circle = "ALL";
        List<OperatorAlert> operatorAlerts = null;
        if (!FCUtil.isEmpty(mnpData)) {
            circle = mnpData.get(0).get("circleMasterId") == null ? null : mnpData.get(0)
                    .get("circleMasterId").toString();
            operatorAlerts = alertService
                    .processAndGetOperatorAlert(Integer.parseInt(operatorID), circle);
        }
        return operatorAlerts;
	}

    private void updateSession(String mobileNumber, String amount, TxnHomePage thp) {
        CommonSessionWebDo commonSessionWebDo = new CommonSessionWebDo();
        commonSessionWebDo.setType(ProductName.MobilePostpaid.getProductType());
        commonSessionWebDo.setPostpaidMobileNumber(mobileNumber);
        commonSessionWebDo.setPostpaidAmount(amount);
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_type", commonSessionWebDo.getType());
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_rechargeAmount", commonSessionWebDo.getRechargeAmount());
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_postpaidAmount", commonSessionWebDo.getPostpaidAmount());
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_mobNum", commonSessionWebDo.getRechargeMobileNumber());
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_operatorName", commonSessionWebDo.getOperatorName());
        fs.getSessionData().put(thp.getLookupId() + "_CommonSessionPojo_primaryAmount", commonSessionWebDo.getPrimaryAmount());
        fs.addLookupID(thp.getLookupId());
    }
    
    private Map<String, String> successResponse(String lookupID, Integer txnHomePgID) {
        return ImmutableMap.of(
                CALL_STATUS, SUCCESS, 
                LOOKUP_ID, lookupID, 
                HOME_PAGE_ID, txnHomePgID.toString()
                );
    }
    
    private Map<String, String> errorResponse(ErrorCode errorCode) {
        return ImmutableMap.of(
                CALL_STATUS, FAILURE, 
                ERROR_CODE, errorCode.getErrorNumberString() 
                );
    }
    
       @NoLogin
       @RequestMapping(value = "postpaid/billers", method = RequestMethod.GET, produces = "application/json")
       public String getMobilePostpaidBillersInfo(Model model) {
              Map<String, Map<String, Object>> data = new LinkedHashMap<String, Map<String, Object>>();
              for (String each : BillerMobilePostpaid.ALL_BILLERS.keySet()) {
                     BillerMobilePostpaid obj = BillerMobilePostpaid.ALL_BILLERS.get(each);
                     data.put(each, obj.toJsonMap());
              }
              model.addAttribute("data", data);
              return "jsonView";
       }

       @NoLogin
       @RequestMapping(value = "postpaid/mobile/{mobileNumber}/provider", method = RequestMethod.GET, produces = "application/json")
       public String getMobileProvider(@PathVariable String mobileNumber, Model model) {
              Integer operatorID = mobilePostpaidService.findMNPOperatorByMobileNumber(mobileNumber);
              if (operatorID == null) {
                     model.addAttribute("result", "error");
              } else {

                     if (prefixService.isExcludedForAutoDetection(operatorID.toString())) {
                            model.addAttribute("result", "error");
                            return "jsonView";
                     }

                     model.addAttribute("result", "success");
                     model.addAttribute("operatorID", operatorID);
                     
                     List<OperatorAlert> operatorAlerts = getActiveAlertsForNumber(mobileNumber, String.valueOf(operatorID));
                     
                     if (!FCUtil.isEmpty(operatorAlerts)) {
                         model.addAttribute("operatorAlert", operatorAlerts.get(0));
                     }
                     
                     try {
                            BillPaymentCommonValidationApiResponse res = billPostpaidValidation.validate(
                                          String.valueOf(operatorID), mobileNumber, null, BigDecimal.ZERO);
                            if (res.getBillAmount().compareTo(BigDecimal.ZERO) > 0) {
                                   model.addAttribute("amount", res.getBillAmount().toPlainString());
                            }
                     } catch (RuntimeException e) {
                            logger.error(e);
                     }
              }
              return "jsonView";
       }

       @RequestMapping(value = "postpaid/validate", method = RequestMethod.GET, produces = "application/json")
       public String getMobileBillInfo(String merchantID, String mobileNumber, String accountNumber, String amount,
                     Model model) {
              try {
                     BillPaymentCommonValidationApiResponse response = billPostpaidValidation.validate(merchantID,
                                   mobileNumber, accountNumber, new BigDecimal(amount));
                     model.addAttribute("status", "success");
                     model.addAttribute("data", response);
              } catch (Exception ex) {
                     logger.error(ex.getMessage(), ex);
                     model.addAttribute("status", "error");
                     model.addAttribute("message", "Unable to validate request");
              }
              return "jsonView";
       }

	@NoLogin
	@RequestMapping(value = "postpaid/billerType/operator/{operatorId}/mobile/{mobileNumber}", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getBillerTypeDetails(@PathVariable String operatorId,
			@PathVariable String mobileNumber) {
		Map<String, Object> result = postpaidFetchBillService.fetchPostpaidBill(operatorId,mobileNumber);		
		return result;

	}

	private String generateTaskId(){
		return UUID.randomUUID().toString() + new Timestamp(System.currentTimeMillis());
	}
	
}
