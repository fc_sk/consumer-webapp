package com.freecharge.freebill.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;

public class BillPaymentStatus extends AbstractDO {

    private long id;
    private int affiliateID;
    private String affiliateTransID;
    private String productType;
    private String postpaidNumber;
    private String postpaidAccountNumber;
    private BigDecimal amount;
    private Boolean success;
    private String authStatus;
    private String authDetail;
    private int retryNumber; // no need to set this because the DAO layer takes
                             // care of this while saving
    //
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String billPaymentGateway;
    private String gatewayReferenceNumber;
    
/* below new fields are added to insert new field in bill_payment_status */
    
    private String circleName;
    private String operatorName;
    private int fk_operator_master_id;
    private int fk_product_master_id;

    public static BillPaymentStatus createForFreecharge(TxnHomePage thp, String requestString,
            BillPaymentCommonApiResponse response, String orderID) {
        BillPaymentStatus status = new BillPaymentStatus();
        status.setAffiliateID(1);
        status.setAffiliateTransID(orderID);
        status.setProductType(thp.getProductType()); //BillTxnHomePage bthp
        status.setPostpaidNumber(thp.getBillTxnHomePage().getPostpaidNumber());
        status.setPostpaidAccountNumber(thp.getBillTxnHomePage().getPostpaidAccountNumber());
        status.setAmount(thp.getBillTxnHomePage().getBillAmount());
        if (response.getSuccess() != null && response.getSuccess() == true) {
            status.setSuccess(true);
        }
        if (response.getSuccess() != null && response.getSuccess() == false) {
            status.setSuccess(false);
        }
        if(response.getSuccess() == null) {
            status.setSuccess(null);
        }
        status.setAuthStatus(response.getBillPaymentStatusCode());
        status.setAuthDetail(response.getAuthDetail());
        status.setBillPaymentGateway(response.getBillPaymentGateway());
        status.setGatewayReferenceNumber(response.getGatewayReferenceNumber());
        status.setCreatedAt(new Timestamp(response.getCreatedTime().getTime()));
        status.setCircleName(thp.getCircleName());
        status.setOperatorName(thp.getOperatorName());
        status.setFk_operator_master_id(thp.getFkOperatorId());
        status.setFk_product_master_id(thp.getFkProductId());
        return status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAffiliateID() {
        return affiliateID;
    }

    public void setAffiliateID(int affiliateID) {
        this.affiliateID = affiliateID;
    }

    public String getAffiliateTransID() {
        return affiliateTransID;
    }

    public void setAffiliateTransID(String affiliateTransID) {
        this.affiliateTransID = affiliateTransID;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPostpaidNumber() {
        return postpaidNumber;
    }

    public void setPostpaidNumber(String postpaidNumber) {
        this.postpaidNumber = postpaidNumber;
    }

    public String getPostpaidAccountNumber() {
        return postpaidAccountNumber;
    }

    public void setPostpaidAccountNumber(String postpaidAccountNumber) {
        this.postpaidAccountNumber = postpaidAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getAuthDetail() {
        return authDetail;
    }

    public void setAuthDetail(String authDetail) {
        this.authDetail = authDetail;
    }

    public int getRetryNumber() {
        return retryNumber;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }

    public String getBillPaymentGateway() {
        return billPaymentGateway;
    }

    public void setBillPaymentGateway(String billPaymentGateway) {
        this.billPaymentGateway = billPaymentGateway;
    }

    public String getGatewayReferenceNumber() {
        return gatewayReferenceNumber;
    }

    public void setGatewayReferenceNumber(String gatewayReferenceNumber) {
        this.gatewayReferenceNumber = gatewayReferenceNumber;
    }
	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public int getFk_operator_master_id() {
		return fk_operator_master_id;
	}

	public void setFk_operator_master_id(int fk_operator_master_id) {
		this.fk_operator_master_id = fk_operator_master_id;
	}

	public int getFk_product_master_id() {
		return fk_product_master_id;
	}

	public void setFk_product_master_id(int fk_product_master_id) {
		this.fk_product_master_id = fk_product_master_id;
	}
}
