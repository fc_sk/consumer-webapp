package com.freecharge.freebill.domain;

import java.io.Serializable;

public class BillPostpaidMerchant  implements Serializable{

    private static final long serialVersionUID = 1L;
    private Integer postpaidMerchantId;
    private Integer fkOperatorMasterId;
    private String billdeskMerchantId;
    private String oxigenMerchantId;
    private String euronetMerchantId;


    public String getEuronetMerchantId() {
        return euronetMerchantId;
    }

    public void setEuronetMerchantId(String euronetMerchantId) {
        this.euronetMerchantId = euronetMerchantId;
    }

    public Integer getPostpaidMerchantId() {
        return postpaidMerchantId;
    }

    public void setPostpaidMerchantId(Integer postpaidMerchantId) {
        this.postpaidMerchantId = postpaidMerchantId;
    }

    public Integer getFkOperatorMasterId() {
        return fkOperatorMasterId;
    }

    public void setFkOperatorMasterId(Integer fkOperatorMasterId) {
        this.fkOperatorMasterId = fkOperatorMasterId;
    }

    public String getBilldeskMerchantId() {
        return billdeskMerchantId;
    }

    public void setBilldeskMerchantId(String billdeskMerchantId) {
        this.billdeskMerchantId = billdeskMerchantId;
    }

    public String getOxigenMerchantId() {
        return oxigenMerchantId;
    }

    public void setOxigenMerchantId(String oxigenMerchantId) {
        this.oxigenMerchantId = oxigenMerchantId;
    }
}
