package com.freecharge.freebill.domain;

import java.sql.Timestamp;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;

public class BillPaymentAttempt extends AbstractDO {

	private long id; // attempt_id   BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	private long statusID; // fk_status_id BIGINT NOT NULL,
	private String  rawRequest; // raw_request  VARCHAR(2000) NOT NULL,
	private String  rawResponse; // raw_response VARCHAR(2000) NOT NULL,
	private Boolean success; // is_success   BOOLEAN       NOT NULL DEFAULT FALSE,
	private String  authStatus; // auth_status  VARCHAR(10)   NOT NULL,
	private String  authDetail; // auth_detail  VARCHAR(200),
	private Timestamp createdAt; // created_at   DATETIME      NOT NULL

	public static BillPaymentAttempt create(BillPaymentStatus status, BillPaymentCommonApiResponse response) {
		BillPaymentAttempt attempt = new BillPaymentAttempt();
		attempt.setStatusID(status.getId());
		attempt.setSuccess(status.getSuccess());
		attempt.setAuthStatus(status.getAuthStatus());
		attempt.setAuthDetail(status.getAuthDetail());
		attempt.setCreatedAt(status.getCreatedAt());
		attempt.setRawRequest(response.getRequestparam());
		attempt.setRawResponse(response.getRowResponse());
		attempt.setCreatedAt(status.getCreatedAt());
		return attempt;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getStatusID() {
		return statusID;
	}
	public void setStatusID(long statusID) {
		this.statusID = statusID;
	}
	public String getRawRequest() {
		return rawRequest;
	}
	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}
	public String getRawResponse() {
		return rawResponse;
	}
	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getAuthStatus() {
		return authStatus;
	}
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}
	public String getAuthDetail() {
		return authDetail;
	}
	public void setAuthDetail(String authDetail) {
		this.authDetail = authDetail;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

}
