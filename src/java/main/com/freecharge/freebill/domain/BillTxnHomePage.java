package com.freecharge.freebill.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;

@Entity
@Table(name="bill_txn_home_page")
public class BillTxnHomePage extends AbstractDO {

	private long id;
	private BigDecimal billAmount;
	private Date billDueDate;

	private String postpaidNumber;
	private String postpaidAccountNumber;
	private String postpaidMerchantName;
	private String postpaidMerchantId;
    private Timestamp createdAt;
	private String billProvider; 

    public static BillTxnHomePage createForMobilePostpaid(String mobileNumber, BillPaymentCommonValidationApiResponse vr, String operatorName) {
		BillTxnHomePage bean = new BillTxnHomePage();
		bean.setPostpaidNumber(mobileNumber);
		bean.setBillAmount(vr.getBillAmount());
		bean.setBillDueDate(vr.getBillDueDate());
		bean.setCreatedAt(new Timestamp(new Date().getTime()));
		bean.setBillProvider(vr.getBillProvider());
		bean.setPostpaidMerchantId(vr.getOperatorId());
		bean.setPostpaidMerchantName(operatorName);
		return bean;
	}

	public BillTxnHomePage createWithID(long id) {
		BillTxnHomePage ret = new BillTxnHomePage();
		ret.setId(id);
		ret.setCreatedAt(getCreatedAt());
		ret.setPostpaidNumber(getPostpaidNumber());
		return ret;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "bill_amount")
	public BigDecimal getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(BigDecimal billAmount) {
		this.billAmount = billAmount;
	}

	@Column(name = "bill_due_date")
	public Date getBillDueDate() {
		return billDueDate;
	}

	public void setBillDueDate(Date billDueDate) {
		this.billDueDate = billDueDate;
	}

	@Column(name = "postpaid_phone_number")
	public String getPostpaidNumber() {
		return postpaidNumber;
	}
	public void setPostpaidNumber(String postpaidNumber) {
		this.postpaidNumber = postpaidNumber;
	}

	@Column(name = "postpaid_account_number")
	public String getPostpaidAccountNumber() {
		return postpaidAccountNumber;
	}

	public void setPostpaidAccountNumber(String postpaidAccountNumber) {
		this.postpaidAccountNumber = postpaidAccountNumber;
	}

	@Column(name = "postpaid_merchant_name")
	public String getPostpaidMerchantName() {
		return postpaidMerchantName;
	}
	public void setPostpaidMerchantName(String postpaidMerchantName) {
		this.postpaidMerchantName = postpaidMerchantName;
	}

	@Column(name = "created_at")
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	@Column(name = "bill_provider")
	public String getBillProvider() {
        return billProvider;
    }
	
	@Column(name = "postpaid_merchant_id")
	public String getPostpaidMerchantId() {
        return postpaidMerchantId;
    }

    public void setPostpaidMerchantId(String postpaidMerchantId) {
        this.postpaidMerchantId = postpaidMerchantId;
    }


    public void setBillProvider(String billProvider) {
        this.billProvider = billProvider;
    }

}
