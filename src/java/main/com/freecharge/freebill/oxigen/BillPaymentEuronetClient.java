package com.freecharge.freebill.oxigen;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpHost;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BillValidationAPIResponse;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.billdesk.RawBillValidationAPIResponse;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.BillPaymentVerificationAPIResponse;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.services.EuronetHttpsHostConfig;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

public class BillPaymentEuronetClient extends AggregatorService implements IBillPaymentInterface {

    @Autowired
    private BillReadDAO billReadDAO;

    @Autowired
    TxnHomePageDAO txnHomePageDAO;

    @Autowired
    BillWriteDAO billWriteDAO;
    
    @Autowired
    private EuronetService euronetService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
	private EuronetHttpsHostConfig euronetHttpsHostConfig;
    
    @Autowired
    private AppConfigService appConfigService;

    private static Logger logger = LoggingFactory.getLogger(BillPaymentEuronetClient.class);

    public static final String MERCHANT_REFERENCE_NO_PARAM = "merchantrefno";
    public static final String RESPONSE_CODE_PARAM = "responsecode";
    public static final String EURONET_REFERENCE_NO_PARAM = "enrefno";
    public static final String GATEWAY_REF_NUMBER = "operatorrefno";
    List<String> idempotencyExclusion = ImmutableList.of("60");
    private final List<String> ideaPostpaidValidationCodes = ImmutableList.of("CI", "EI");

    private String euromerchantcode;
    private String eurousername;
    private String eurouserpass;
    private String europaymentprovider;
    private String europaymentmode;
    private String eurochannelcode;
    private String euroservicecode;
    private String eurodthservicecode;
    private String euroOptional1;
    private String eurostorecode;
    private String euroterminalcode;
    private String eurocustomerid;
    private String eurotcflag;
    private String euroverificationrequrl;
    private String euroValidationUrl;
    private String eurotopupurl;
    private String eurotrnsstatusreq;
    private String euroBalenceReqUrl;
    private String euronetValidationUrl;
    private String rechargeAPI;
    private HttpConnectionManagerParams connectionManagerConfig;
    private HostConfiguration euronetHostConfig;
	private HttpClient httpClient;

	

    public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public String getEuromerchantcode() {
        return euromerchantcode;
    }

    public void setEuromerchantcode(String euromerchantcode) {
        this.euromerchantcode = euromerchantcode;
    }

    public String getEurousername() {
        return eurousername;
    }

    public void setEurousername(String eurousername) {
        this.eurousername = eurousername;
    }

    public String getEurouserpass() {
        return eurouserpass;
    }

    public void setEurouserpass(String eurouserpass) {
        this.eurouserpass = eurouserpass;
    }

    public String getEuropaymentprovider() {
        return europaymentprovider;
    }

    public void setEuropaymentprovider(String europaymentprovider) {
        this.europaymentprovider = europaymentprovider;
    }

    public String getEuropaymentmode() {
        return europaymentmode;
    }

    public void setEuropaymentmode(String europaymentmode) {
        this.europaymentmode = europaymentmode;
    }

    public String getEurochannelcode() {
        return eurochannelcode;
    }

    public void setEurochannelcode(String eurochannelcode) {
        this.eurochannelcode = eurochannelcode;
    }

    public String getEuroservicecode() {
        return euroservicecode;
    }

    public void setEuroservicecode(String euroservicecode) {
        this.euroservicecode = euroservicecode;
    }

    public String getEurodthservicecode() {
        return eurodthservicecode;
    }

    public void setEurodthservicecode(String eurodthservicecode) {
        this.eurodthservicecode = eurodthservicecode;
    }

    public String getEuroOptional1() {
        return euroOptional1;
    }

    public void setEuroOptional1(String euroOptional1) {
        this.euroOptional1 = euroOptional1;
    }

    public String getEurostorecode() {
        return eurostorecode;
    }

    public void setEurostorecode(String eurostorecode) {
        this.eurostorecode = eurostorecode;
    }

    public String getEuroterminalcode() {
        return euroterminalcode;
    }

    public void setEuroterminalcode(String euroterminalcode) {
        this.euroterminalcode = euroterminalcode;
    }

    public String getEurocustomerid() {
        return eurocustomerid;
    }

    public void setEurocustomerid(String eurocustomerid) {
        this.eurocustomerid = eurocustomerid;
    }

    public String getEurotcflag() {
        return eurotcflag;
    }

    public void setEurotcflag(String eurotcflag) {
        this.eurotcflag = eurotcflag;
    }

    public String getEuroverificationrequrl() {
        return euroverificationrequrl;
    }

    public void setEuroverificationrequrl(String euroverificationrequrl) {
        this.euroverificationrequrl = euroverificationrequrl;
    }

    public String getEurotopupurl() {
        return eurotopupurl;
    }

    public void setEurotopupurl(String eurotopupurl) {
        this.eurotopupurl = eurotopupurl;
    }

    public String getEurotrnsstatusreq() {
        return eurotrnsstatusreq;
    }

    public void setEurotrnsstatusreq(String eurotrnsstatusreq) {
        this.eurotrnsstatusreq = eurotrnsstatusreq;
    }

    public String getEuroBalenceReqUrl() {
        return euroBalenceReqUrl;
    }

    public void setEuroBalenceReqUrl(String euroBalenceReqUrl) {
        this.euroBalenceReqUrl = euroBalenceReqUrl;
    }

    public String getEuronetValidationUrl() {
        return euronetValidationUrl;
    }

    public void setEuronetValidationUrl(String euronetValidationUrl) {
        this.euronetValidationUrl = euronetValidationUrl;
    }

    public String getRechargeAPI() {
        return rechargeAPI;
    }

    public void setRechargeAPI(String rechargeAPI) {
        this.rechargeAPI = rechargeAPI;
    }

    public String getEuroValidationUrl() {
        return euroValidationUrl;
    }

    public void setEuroValidationUrl(String euroValidationUrl) {
        this.euroValidationUrl = euroValidationUrl;
    }

    public HttpConnectionManagerParams getConnectionManagerConfig() {
		return connectionManagerConfig;
	}

	public void setConnectionManagerConfig(
			HttpConnectionManagerParams connectionManagerConfig) {
		this.connectionManagerConfig = connectionManagerConfig;
	}

	public HostConfiguration getEuronetHostConfig() {
		return euronetHostConfig;
	}

	public void setEuronetHostConfig(HostConfiguration euronetHostConfig) {
		this.euronetHostConfig = euronetHostConfig;
	}


	public void init() {
		logger.info("Calling init method BillPaymentEuronet service");
		euronetHostConfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HOST), 443, "https");
		httpClient.setHostConfiguration(euronetHostConfig);

	}
	
	@Override
    public BillPaymentCommonApiResponse submitPaymentRequest(String aggregatorTxnId, TxnHomePage thp,
            BillPostpaidMerchant billPostpaidMerchant, Integer paymentType, String paymentOption) throws Exception {
        
        String response = null;
        Boolean shouldTimeOut = true;
        Integer timeOutValue = 1000 * 1500;
        BillPaymentCommonApiResponse billPaymentCommonApiResponse = null;
        BillPaymentVerificationAPIResponse billPaymentVerificationAPIResponse = null;
        NameValuePair[] euronetParam = null;
        String accountNumber = "";
        String invoiceNumber = "";
  
        String mobileNumber = thp.getBillTxnHomePage().getPostpaidNumber();
		if (thp.getFkOperatorId() == fcProperties.getIntProperty(FCProperties.DOCOMO_POSTPAID_GSM)
				|| thp.getFkOperatorId() == fcProperties.getIntProperty(FCProperties.DOCOMO_POSTPAID_CDMA)) {
			billPaymentVerificationAPIResponse = euronetService.verificationForOperator(aggregatorTxnId, mobileNumber, thp.getFkOperatorId());
			accountNumber = billPaymentVerificationAPIResponse.getAccountNumber();
			invoiceNumber = billPaymentVerificationAPIResponse.getInvoiceNumber();
			euronetParam = createEuronetPostpaidRequest(aggregatorTxnId, thp, billPostpaidMerchant, accountNumber,
					invoiceNumber);
			logger.info("Account Number:"+billPaymentVerificationAPIResponse.getAccountNumber());
			logger.info("Invoice Number:"+billPaymentVerificationAPIResponse.getInvoiceNumber());
		} else {
			euronetParam = createEuronetPostpaidRequest(aggregatorTxnId, thp, billPostpaidMerchant, accountNumber,
					invoiceNumber);
		}
        String requestParam = createRowRequestToSaveInDB(euronetParam);
        
        

        GetMethod method = new GetMethod(this.getRechargeAPI());
        method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
        method.setQueryString(euronetParam);

        logger.info(" Executing postpaid request from euronet for mobile no :" + euronetParam[14]);
        logger.info("Euronet postpaid request URI-[raw request in the form of querystring]:" + method.getQueryString());       
        try {
            
            int statusCode = executeWithTimeout(aggregatorTxnId, httpClient, method, shouldTimeOut, timeOutValue);

        if (statusCode == HttpStatus.SC_OK) {
            response = method.getResponseBodyAsString();
        } else {
            logger.info("[IN MODERATOR] Received HTTP status code" + statusCode);
        }
			billPaymentCommonApiResponse = handleEuronetPostpaidResponse(response, euronetParam, requestParam);
		} catch (Exception e) {
			logger.error("exception occured for euronet postpaid payment ", e);
			metricsClient.recordEvent("PostPaid", "Euronet", "Exception");
			billPaymentCommonApiResponse = handleEuronetPostpaidResponse(response, euronetParam, requestParam);
		} finally {
			logger.info("billPaymentCommonApiResponse ### "+billPaymentCommonApiResponse);
			logger.info(" Releasing connection from IN for Euronet postpaid for order no " + aggregatorTxnId);
			logger.info("Euronet postpaid recharge request Url " + this.getRechargeAPI() + " with param "
					+ method.getQueryString() + " with Recharge Response from Oxigen " + response);
			method.releaseConnection();
		}
		logger.info("Output of postpaid request from Euronet : " + response);
		return billPaymentCommonApiResponse;
    }

	
    @Override
    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
            List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Double getBalanceCriticalThreshold() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Double getBalanceWarnThreshold() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
            throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
            IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Double getWalletBalance() throws IOException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public BillPaymentCommonValidationApiResponse submitValidationRequest(IBillValidationRequest req,
            BillPostpaidMerchant billPostpaidMerchant) throws ClientProtocolException, IOException {
            String responseCode = null;
            RawBillValidationAPIResponse vr = new RawBillValidationAPIResponse();
            vr.setStatusValid("Y");
            BillValidationAPIResponse billValidationAPIResponse = vr.toBillValidationResponse(req,responseCode);
            return (convertValidationResponseToCommonApiResponse(billValidationAPIResponse, req));
    }

    private BillPaymentCommonApiResponse handleEuronetPostpaidResponse(String response, NameValuePair[] euronetParam,String requestParam) {
        
        Map<String, String> topupResponseMap = null;
        
        if(!StringUtils.isEmpty(response)) {
             topupResponseMap = FCUtil.stringToHashMap(response);
             if(CollectionUtils.isEmpty(topupResponseMap)) {
            	 logger.info("Creating UnknownResponse from : "+response);
            	response="enrefno=unknown&merchantrefno="+euronetParam[0].toString() + 
                         "&operatorrefno=unknown&responseaction=Transaction status not known&" +
                         "responsecode=UP&responsemessage=Transaction status not known";
                 topupResponseMap = FCUtil.stringToHashMap(response);
             }
            
        } else {
            response="enrefno=unknown&merchantrefno="+euronetParam[0].toString() + 
                    "&operatorrefno=unknown&responseaction=Transaction status not known&" +
                    "responsecode=UP&responsemessage=Transaction status not known";
            topupResponseMap = FCUtil.stringToHashMap(response);
            
        }
        return convertBillPaymentApiResponseToCommonResponse
                (response,requestParam,euronetParam[0].toString(), topupResponseMap);
    }

    private BillPaymentCommonApiResponse convertBillPaymentApiResponseToCommonResponse(String response, String requestParams, String orderId, Map<String, String> topupResponseMap) {
        BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
        billPaymentCommonApiResponse.setRequestparam(requestParams);
        billPaymentCommonApiResponse.setTxnId(orderId);
        billPaymentCommonApiResponse.setRowResponse(response);
        
        billPaymentCommonApiResponse.setBillPaymentStatusCode(topupResponseMap.get(RESPONSE_CODE_PARAM).trim());
        logger.info("Euronet response value " + topupResponseMap.get(RESPONSE_CODE_PARAM));
		String StatusCode = "";
		try {
			StatusCode = topupResponseMap.get(RESPONSE_CODE_PARAM).trim();
		} catch (Exception e) {
			logger.error("Exception deducing res-code : " + e);
			metricsClient.recordEvent("PostPaid", "Euronet", "INV-RES-CODE");
			StatusCode = topupResponseMap.get(RESPONSE_CODE_PARAM);
		}
       
		if(topupResponseMap!=null && StatusCode.equalsIgnoreCase(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE)) {
            billPaymentCommonApiResponse.isSuccess(true);
            metricsClient.recordEvent("PostPaid", "Euronet", "success");
        }else if( topupResponseMap!=null && RechargeConstants.EURO_ERROR_CODES.contains(StatusCode)) {
            billPaymentCommonApiResponse.isSuccess(false);
            metricsClient.recordEvent("PostPaid", "Euronet", "failure");
        }else {
        	 metricsClient.recordEvent("PostPaid", "Euronet", "underProcess");
             billPaymentCommonApiResponse.isSuccess(null);
        }
		
        billPaymentCommonApiResponse.setAuthDetail("");
        billPaymentCommonApiResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_EURONET_GATEWAY);
        
        String agRefNumber = topupResponseMap.get(EURONET_REFERENCE_NO_PARAM);
        if (agRefNumber != null) {
            agRefNumber = agRefNumber.trim();
        }else {
            metricsClient.recordEvent("PostPaid", "Euronet", "NULL-REF-NO");

        }
        billPaymentCommonApiResponse.setGatewayReferenceNumber(agRefNumber == null ? "unknown" : agRefNumber);
        billPaymentCommonApiResponse.setCreatedTime(new Date());
        return billPaymentCommonApiResponse;
    }
    
    private BillPaymentCommonApiResponse createDummyBillPaymentResponse(String aggregatorTxnId, String requestParams) {
        
    BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
    billPaymentCommonApiResponse.setRequestparam(requestParams);
    billPaymentCommonApiResponse.setTxnId(aggregatorTxnId);
    billPaymentCommonApiResponse.setRowResponse("Exception Raised" + aggregatorTxnId);
    billPaymentCommonApiResponse.setBillPaymentStatusCode(null);
    billPaymentCommonApiResponse.setAuthDetail("");
    billPaymentCommonApiResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_EURONET_GATEWAY);
    billPaymentCommonApiResponse.setGatewayReferenceNumber("");
    billPaymentCommonApiResponse.isSuccess(null);
    metricsClient.recordEvent("PostPaid", "Euronet", "underProcess");
    billPaymentCommonApiResponse.setCreatedTime(new Date());
    return billPaymentCommonApiResponse;
    }
    
    private BillPaymentCommonValidationApiResponse convertValidationResponseToCommonApiResponse(
            BillValidationAPIResponse billValidationAPIResponse, IBillValidationRequest req) {
        BillPaymentCommonValidationApiResponse apiResponse = new BillPaymentCommonValidationApiResponse();
        apiResponse.setOperatorId(req.getOperatorID());
        apiResponse.setBillAmount(req.getTransactionAmount());
        apiResponse.setBillDueDate(FCUtil.currentDate());
        apiResponse.setBillProvider("euronet");
        apiResponse.setStatus("Y");
        if (ideaPostpaidValidationCodes.contains(billValidationAPIResponse.getResponseCode())) {
            apiResponse.setErrorCode(ErrorCode.NO_ERROR);
        } else {
            apiResponse.setErrorCode(ErrorCode.NO_ERROR);
        }
        return apiResponse;
    }

    
    private NameValuePair[] createEuronetPostpaidRequest(String aggregatorTxnId, TxnHomePage thp,
            BillPostpaidMerchant billPostpaidMerchant, String accountnumber, String invoicenumber) {

        BigDecimal amount = null;
        String mobileNumber = "";
        String euroOperatorCode = "";
        String euronetOption1ParamValue = "Default";
        
		if (billPostpaidMerchant != null) {
			if (!Strings.isNullOrEmpty(billPostpaidMerchant.getEuronetMerchantId())) {
				euroOperatorCode = billPostpaidMerchant.getEuronetMerchantId();
			} else {
				euroOperatorCode = "";
			}
		} else {
			if (thp.getFkOperatorId().equals(fcProperties.getProperty(FCProperties.DOCOMO_POSTPAID_GSM))) {
				euroOperatorCode = "TAD";
			} else if (thp.getFkOperatorId().equals(fcProperties.getProperty(FCProperties.DOCOMO_POSTPAID_CDMA))) {
				euroOperatorCode = "TAT";
			}
		}
        logger.info("The euronet operator code is : " + euroOperatorCode+"\n operator id:"+thp.getFkOperatorId());
        logger.info("euronetmerchant id in payment request:"+billPostpaidMerchant.getEuronetMerchantId());
        BillTxnHomePage billTxnHomePage = thp.getBillTxnHomePage();

        amount = billTxnHomePage.getBillAmount();
        mobileNumber = billTxnHomePage.getPostpaidNumber();

        NameValuePair[] euronetParam = new NameValuePair[19];

        euronetParam[0] = new NameValuePair(MERCHANT_REFERENCE_NO_PARAM, aggregatorTxnId);
        euronetParam[1] = new NameValuePair("merchantcode", euromerchantcode);
        euronetParam[2] = new NameValuePair("username", eurousername);
        euronetParam[3] = new NameValuePair("userpass", eurouserpass);
        euronetParam[4] = new NameValuePair("paymentprovider", europaymentprovider);
        euronetParam[5] = new NameValuePair("paymentmode", europaymentmode);
        euronetParam[6] = new NameValuePair("channelcode", eurochannelcode);
        euronetParam[7] = new NameValuePair("servicecode", euroservicecode);
        euronetParam[8] = new NameValuePair("amount", Integer.toString(amount.intValue()));
        euronetParam[9] = new NameValuePair("storecode", eurostorecode);
        euronetParam[10] = new NameValuePair("terminalcode", euroterminalcode);
        euronetParam[11] = new NameValuePair("customerid", eurocustomerid);
        euronetParam[12] = new NameValuePair("tcflag", eurotcflag);
        euronetParam[13] = new NameValuePair("spcode", euroOperatorCode);
        euronetParam[14] = new NameValuePair("consumerno", mobileNumber);
        euronetParam[15] = new NameValuePair("optional1", euronetOption1ParamValue);
        euronetParam[16] = new NameValuePair("optional2", "");
        //account number and invoice number required in case of fetch and pay postapaid
        euronetParam[17] = new NameValuePair("accountnumber", accountnumber);
        euronetParam[18] = new NameValuePair("invoicenumber", invoicenumber);

        return euronetParam;
    }
    
	
	private String createRowRequestToSaveInDB(NameValuePair[] euronetParam) {
		StringBuilder parametersPassed = new StringBuilder();
		parametersPassed.append("merchantrefno = " + euronetParam[0].getValue().toString() + ", ");
		parametersPassed.append("merchantcode = " + euromerchantcode + ", ");
		parametersPassed.append("username = " + eurousername + ", ");
		parametersPassed.append("userpass = " + eurouserpass + ", ");
		parametersPassed.append("paymentprovider = " + europaymentprovider + ", ");
		parametersPassed.append("paymentmode = " + europaymentmode + ", ");
		parametersPassed.append("channelcode = " + eurochannelcode + ", ");
		parametersPassed.append("servicecode = " + euronetParam[7].getValue().toString() + ", ");
		parametersPassed.append("consumerno = " + euronetParam[14].getValue().toString() + ", ");
		parametersPassed.append("amount = " + euronetParam[8].getValue().toString() + ", ");
		parametersPassed.append("storecode = " + eurostorecode + ", ");
		parametersPassed.append("terminalcode = " + euroterminalcode + ", ");
		parametersPassed.append("customerid = " + eurocustomerid + ", ");
		parametersPassed.append("tcflag = " + eurotcflag + ", ");		
		parametersPassed.append("optional1 = " + euronetParam[15].getValue().toString() + ", ");
		parametersPassed.append("spcode = " + euronetParam[13].getValue().toString() + ", ");
		parametersPassed.append("optional2 = " + euronetParam[16].getValue().toString() + ", ");
		return parametersPassed.toString();
	}

    @Override
    public String getBalanceCachekey() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getBalanceUpdateTimeCachekey() {
        throw new RuntimeException("Not implemented");
    }
    
}