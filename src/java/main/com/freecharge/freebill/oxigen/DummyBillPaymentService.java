package com.freecharge.freebill.oxigen;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.freebill.billdesk.IBillPaymentRequest;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.util.RechargeConstants;

/**
 * This is a dummy Oxigen bill payment aggregator service class - used only for
 * testing
 */

public class DummyBillPaymentService extends AggregatorService implements IBillPaymentInterface {

    private static Logger logger = LoggingFactory.getLogger(DummyBillPaymentService.class);

    @Autowired
    MetricsClient metricsClient;

    @Autowired
    private BillReadDAO billReadDAO;

    private String oxibankrefno;
    private String oxistatus;

    @Override
    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
            List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Double getBalanceCriticalThreshold() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Double getBalanceWarnThreshold() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
            throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
            IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Double getWalletBalance() throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BillPaymentCommonApiResponse submitPaymentRequest(String aggregatorTxnId, TxnHomePage thp,
            BillPostpaidMerchant billPostpaidMerchant, Integer paymentType, String paymentOption) throws Exception {
        String oxiOperatorCode = "";
        String merchantRefNo = "";
        String amount = "";
        String mobileNumber = "";
        metricsClient.timeStampEvent(aggregatorTxnId, MetricsClient.EventName.BillPaymentAttempt);

        metricsClient.recordLatency("UserSession", "BillPaymentAttempt", aggregatorTxnId, MetricsClient.EventName.PaySuccess);

        if (billPostpaidMerchant != null) {
            oxiOperatorCode = billPostpaidMerchant.getOxigenMerchantId();
        }

        String oxigenPostpaidMobileRechargeType = RechargeConstants.OXIGEN_BILL_PAYMENT_TYPE;

        merchantRefNo = oxigenPostpaidMobileRechargeType + "*" + oxiOperatorCode + "," + mobileNumber;

        StringBuffer parametersPassed = new StringBuffer();

        BillTxnHomePage billTxnHomePage = thp.getBillTxnHomePage();

        amount = billTxnHomePage.getBillAmount().toString();
        mobileNumber = billTxnHomePage.getPostpaidNumber();

        parametersPassed.append("Transid = " + aggregatorTxnId + ", ");
        parametersPassed.append("merchantrefno = " + merchantRefNo + ", ");
        parametersPassed.append("requestdate = " + new Date() + ", ");
        parametersPassed.append("status = " + oxistatus + ", ");
        parametersPassed.append("bankrefno = " + oxibankrefno + ", ");
        parametersPassed.append("amount = " + amount);

        BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
        billPaymentCommonApiResponse.setRequestparam(parametersPassed.toString());
        billPaymentCommonApiResponse.setTxnId(aggregatorTxnId);
        billPaymentCommonApiResponse.setRowResponse("dummy postpaid request" + aggregatorTxnId);
        billPaymentCommonApiResponse.setBillPaymentStatusCode(null);
        billPaymentCommonApiResponse.setAuthDetail("Dummy bill payment");
        billPaymentCommonApiResponse.setBillPaymentGateway("dummy_" + PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY);
        billPaymentCommonApiResponse.setGatewayReferenceNumber("12345");
        billPaymentCommonApiResponse.setCreatedTime(new Date());

        String billPaymentResponseCode = mobileNumber.substring(mobileNumber.length() - 2);
        /*
         * Dummy test code
         */
        //billPaymentResponseCode="29";
        
        if (billPaymentResponseCode.equals("10")) {
            throw new IOException("Mock IO Exception for order ID: " + aggregatorTxnId);
        }
        if (billPaymentResponseCode.equals("00")) {
            billPaymentCommonApiResponse.isSuccess(true);
        } else if (billPaymentResponseCode.equals(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
            billPaymentCommonApiResponse.isSuccess(null);
        } else {
            billPaymentCommonApiResponse.isSuccess(false);
            billPaymentCommonApiResponse.setBillPaymentStatusCode("ECI");
        }

        logger.info("[MOCK] returning responseCode=" + billPaymentResponseCode);
        return billPaymentCommonApiResponse;
    }

    @Override
    public BillPaymentCommonValidationApiResponse submitValidationRequest(IBillValidationRequest req,
            BillPostpaidMerchant billPostpaidMerchant) throws ClientProtocolException, IOException {
        // TODO Auto-generated method stub
        return null;
    }

    public String getOxibankrefno() {
        return oxibankrefno;
    }

    public void setOxibankrefno(String oxibankrefno) {
        this.oxibankrefno = oxibankrefno;
    }

    public String getOxistatus() {
        return oxistatus;
    }

    public void setOxistatus(String oxistatus) {
        this.oxistatus = oxistatus;
    }

    @Override
    public String getBalanceCachekey() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getBalanceUpdateTimeCachekey() {
        throw new RuntimeException("Not implemented");
    }
}
