package com.freecharge.freebill.oxigen;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BillValidationAPIResponse;
import com.freecharge.freebill.billdesk.IBillPaymentRequest;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.billdesk.RawBillValidationAPIResponse;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;

public class BillPaymentOxigenClient extends AggregatorService implements IBillPaymentInterface {
    
    @Autowired
    private BillReadDAO billReadDAO;
    
    @Autowired
    TxnHomePageDAO txnHomePageDAO;

    @Autowired
    BillWriteDAO billWriteDAO;

    
    private static Logger logger = LoggingFactory.getLogger(BillPaymentOxigenClient.class);

    public BillPaymentCommonApiResponse submitPaymentRequest(String aggregatorTxnId, TxnHomePage thp, BillPostpaidMerchant billPostpaidMerchant, Integer paymentType, String paymentOption) {
        String oxiOperatorCode = "";
        String amount = "";
        String mobileNumber = "";
        String merchantRefNo = "";
        BillPaymentCommonApiResponse billPaymentCommonApiResponse = null;

        String oxigenPostpaidMobileRechargeType = RechargeConstants.OXIGEN_BILL_PAYMENT_TYPE;

        if (billPostpaidMerchant != null) {
            oxiOperatorCode = billPostpaidMerchant.getOxigenMerchantId();
        }
        
        BillTxnHomePage billTxnHomePage = thp.getBillTxnHomePage();
        
        amount = billTxnHomePage.getBillAmount().toString();
        mobileNumber = billTxnHomePage.getPostpaidNumber();
        
        logger.info("processing Bill Payment for oxigen " + aggregatorTxnId);
        PostMethod method = new PostMethod(oxirequesturl);
        StringBuffer parametersPassed = new StringBuffer();
        try {
            String merchantKeyword = null;
            Boolean shouldTimeOut = true;
            Integer timeOutValue = -1;
            timeOutValue = 1000*1500;
 
            logger.info(" processing Bill Payment for oxigen  with merchantKeyword : " + merchantKeyword);
            Calendar cal = Calendar.getInstance();
            Date dt = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
            String requestdate = sdf.format(dt);
            merchantRefNo = oxigenPostpaidMobileRechargeType + "*" + oxiOperatorCode + "," + mobileNumber;
            logger.info("postpaid bill keyword merchantRefNo " + merchantRefNo);

            method.setParameter("Transid", aggregatorTxnId);
            method.setParameter("merchantrefno", merchantRefNo);
            method.setParameter("requestdate", requestdate);
            method.setParameter("status", oxistatus);
            method.setParameter("bankrefno", oxibankrefno);
            method.setParameter("amount", amount);
            method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
            parametersPassed.append("Transid = " + aggregatorTxnId + ", ");
            parametersPassed.append("merchantrefno = " + merchantRefNo + ", ");
            parametersPassed.append("requestdate = " + requestdate + ", ");
            parametersPassed.append("status = " + oxistatus + ", ");
            parametersPassed.append("bankrefno = " + oxibankrefno + ", ");
            parametersPassed.append("amount = " + amount);

            logger.info("Executing recharge request from oxigen for operatorCode : " + oxiOperatorCode
                    + " && mobile no :" + mobileNumber, null);
            // Execute and print response

            String response = null;

            int statusCode = executeWithTimeout(aggregatorTxnId, this.getRechargeHttpClient(), method, shouldTimeOut, timeOutValue);

            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
            } else {
                logger.info("[IN MODERATOR] Received HTTP status code for oxigen : " + statusCode, null);
            }
            
            String[] outputResult = new String[0];
            outputResult = response.split("\\|");
                
            
            if (outputResult.length > 0) {
                 billPaymentCommonApiResponse = convertBillPaymentApiResponseToCommonResponse
                        (response,parametersPassed.toString(),aggregatorTxnId, outputResult);
                              
            } else {
                billPaymentCommonApiResponse = createDummyBillPaymentResponse(aggregatorTxnId, parametersPassed.toString());
            }
            logger.info("Oxigen Recharge request Url "+oxirequesturl+" with param "+parametersPassed.toString()
                    +" with Recharge Response from Oxigen " + response);
        } catch(Exception exception) {
            logger.error("exception occured for oxigen postpaid payment " , exception);
            billPaymentCommonApiResponse = createDummyBillPaymentResponse(aggregatorTxnId, parametersPassed.toString());
        }
        finally {
            // release connection
            logger.info("[IN MODERATOR] Releasing connection from IN in doRecharge(finally) for oxigen ", null);
            method.releaseConnection();
        }
        return billPaymentCommonApiResponse;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse 
            inResponseFromRequest) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        String response = "";

        String merchantRefNo = "Checkstatus," + rechargeDo.getMobileNo();

        PostMethod method;
        method = new PostMethod(statusCheckAPI);

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

        try {
            String aggregatorOrderId = fcProperties.getInTransIdPrefix() + "0_" + 
            rechargeDo.getInRequestId().toString();
            String aggrReferenceNo="";

            method.setParameter("Transid", aggregatorOrderId);
            method.setParameter("merchantrefno", merchantRefNo);
            method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
            logger.info("About to call Oxi status check for Order ID: " + rechargeDo.getAffiliateTransId()); 
            int statusCode = this.getStatusCheckHttpClient().executeMethod(method);

            if (statusCode == HttpStatus.SC_OK) {
                //Should be of the format <status code>|<status message>
                //0 - successful
                //1 - transaction not found
                response = method.getResponseBodyAsString();

                String[] outputResult = new String[0];
                if (response != null && !response.equalsIgnoreCase("")) {
                    outputResult = response.split("\\|");
                }

                String status = outputResult[0].trim();
                logger.info("Oxigen status check api response string for order id " + 
                rechargeDo.getAffiliateTransId() + "  " + response) ;
                if(outputResult.length==3 && outputResult[2].trim().split("-").length==2)
                aggrReferenceNo = outputResult[2].trim().split("-")[1].trim();

                if (RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE.equals(status)) {
                    aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
                    aggregatorResponseDo.setAggrResponseCode(status);
                    aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
                    aggregatorResponseDo.setRawResponse(response);
                    aggregatorResponseDo.setAggrReferenceNo(aggrReferenceNo);
                }  else {
                    aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                    aggregatorResponseDo.setAggrResponseCode(status);
                    aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                    aggregatorResponseDo.setRawResponse(response);
                }
            } else {
                logger.info("[IN MODERATOR] Received HTTP status code in getTransactionStatus" +
                        " for oxigen : " + statusCode, null);
                aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
            }
        } catch (Exception exception) {
            logger.error("exception occured for oxigen postpaid payment " ,exception);
        } finally{
            method.releaseConnection();
        }
        logger.info("[IN MODERATOR] Get Transaction Status from oxigen : " + response, null);
        return aggregatorResponseDo;
    }
    // Currently No Validation api present for oxigen postpaid bil payment
    public BillPaymentCommonValidationApiResponse submitValidationRequest(IBillValidationRequest req, BillPostpaidMerchant billPostpaidMerchant) throws ClientProtocolException, IOException {
        try {
            RawBillValidationAPIResponse vr = new RawBillValidationAPIResponse();
            vr.setStatusValid("Y");
            BillValidationAPIResponse billValidationAPIResponse = vr.toBillValidationResponse(req, null);
            return (convertValidationResponseToCommonApiResponse(billValidationAPIResponse, req));
        } catch (Exception exception) {
            logger.error("vaildation api response is failed for oxigen" , exception);
            throw new IOException();
        }
    }

        private BillPaymentCommonApiResponse convertBillPaymentApiResponseToCommonResponse(String response, String requestParams, String aggregatorTxnId, String[] outputResult) {
        String aggrReferenceNo = null;
        BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
        billPaymentCommonApiResponse.setRequestparam(requestParams);
        billPaymentCommonApiResponse.setTxnId(aggregatorTxnId);
        billPaymentCommonApiResponse.setRowResponse(response);
        billPaymentCommonApiResponse.setBillPaymentStatusCode(outputResult[0].trim());
        logger.info("oxigen response value " + outputResult[0]);
        if(outputResult.length>0 && !outputResult[0].trim().isEmpty() && outputResult[0].trim().equalsIgnoreCase(RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE)) {
            billPaymentCommonApiResponse.isSuccess(true);
            metricsClient.recordEvent("PostPaid", "Oxigen", "success");
        } 
        if(outputResult.length>0 && !outputResult[0].trim().isEmpty() && !outputResult[0].trim().equalsIgnoreCase(RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE)) {
            billPaymentCommonApiResponse.isSuccess(false);
            metricsClient.recordEvent("PostPaid", "Oxigen", "false");
        }
        billPaymentCommonApiResponse.setAuthDetail(outputResult[1].trim());
        billPaymentCommonApiResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY);
        
        if(outputResult[1].trim().split("-").length==2) {
        aggrReferenceNo = outputResult[1].trim().split("-")[1].trim();
        }
        billPaymentCommonApiResponse.setGatewayReferenceNumber(aggrReferenceNo);
        billPaymentCommonApiResponse.setCreatedTime(new Date());
        return billPaymentCommonApiResponse;
    }
    
    private BillPaymentCommonValidationApiResponse convertValidationResponseToCommonApiResponse(
            BillValidationAPIResponse billValidationAPIResponse, IBillValidationRequest req) {
        BillPaymentCommonValidationApiResponse apiResponse = new BillPaymentCommonValidationApiResponse();
        apiResponse.setOperatorId(req.getOperatorID());
        apiResponse.setBillAmount(req.getTransactionAmount());
        apiResponse.setBillDueDate(FCUtil.currentDate());
        apiResponse.setBillProvider("oxigen");
        if(billValidationAPIResponse.getStatus()!=null && billValidationAPIResponse.getStatus().equalsIgnoreCase("Y")) {
            apiResponse.setStatus("Y");
        } else {
            apiResponse.setStatus("F");
        }
        return apiResponse;
    }
    
    
    @Autowired
    private FCProperties fcProperties;

    private String oxibankrefno;
    private String oxirequesturl;
    private String oxistatus;
    private String statusCheckAPI;
    private String oxiusername;
    private String oxipassword;
    private String balanceCheckUrl;
    private HttpClient rechargeHttpClient;
    private HttpClient statusCheckHttpClient;
    
    public String getStatusCheckAPI() {
        return statusCheckAPI;
    }
    
    public void setStatusCheckAPI(String statusCheckAPI) {
        this.statusCheckAPI = statusCheckAPI;
    }

    public HttpClient getStatusCheckHttpClient() {
        return statusCheckHttpClient;
    }
    
    public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
        this.statusCheckHttpClient = statusCheckHttpClient;
    }
    
    public void setRechargeHttpClient(HttpClient httpClient) {
        this.rechargeHttpClient = httpClient;
    }
    
    public HttpClient getRechargeHttpClient() {
        return rechargeHttpClient;
    }

    public void setBalanceCheckUrl(String balanceCheckUrl) {
        this.balanceCheckUrl = balanceCheckUrl;
    }

    public void setOxibankrefno(String oxibankrefno) {
        this.oxibankrefno = oxibankrefno;
    }

    public void setOxirequesturl(String oxirequesturl) {
        this.oxirequesturl = oxirequesturl;
    }

    public void setOxistatus(String oxistatus) {
        this.oxistatus = oxistatus;
    }

    public void setOxiusername(String oxiusername) {
        this.oxiusername = oxiusername;
    }

    public void setOxipassword(String oxipassword) {
        this.oxipassword = oxipassword;
    }
    
    private BillPaymentCommonApiResponse createDummyBillPaymentResponse(String orderId, String requestParams) {
        BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
        billPaymentCommonApiResponse.setRequestparam(requestParams);
        billPaymentCommonApiResponse.setTxnId(orderId);
        billPaymentCommonApiResponse.setRowResponse("Exception occured" + orderId);
        billPaymentCommonApiResponse.setBillPaymentStatusCode(null);
        billPaymentCommonApiResponse.setAuthDetail("");
        billPaymentCommonApiResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY);
        billPaymentCommonApiResponse.setGatewayReferenceNumber("");
        billPaymentCommonApiResponse.isSuccess(null);
        metricsClient.recordEvent("PostPaid", "Oxigen", "underProcess");
        billPaymentCommonApiResponse.setCreatedTime(new Date());
        return billPaymentCommonApiResponse;
    }

    @Override
    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
            List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        throw new RuntimeException("Not implemented");
    }


    @Override
    public Double getWalletBalance() throws IOException {
        throw new RuntimeException("Not implemented");
    }
    
    @Override
    protected Double getBalanceWarnThreshold() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    protected Double getBalanceCriticalThreshold() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getBalanceCachekey() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getBalanceUpdateTimeCachekey() {
        throw new RuntimeException("Not implemented");
    }
}

