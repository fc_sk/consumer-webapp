package com.freecharge.freebill;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillTransactionLedger;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.recharge.MNPListener;
import com.freecharge.recharge.businessdao.InExcelReader;
import com.freecharge.recharge.businessdao.ReportData;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.RechargeSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.wallet.OneCheckWalletService;

@Service
public class RectifyTranslator {
	
	private Logger                        logger = LoggingFactory.getLogger(getClass());	

	@Autowired
    private InService                     inService;
	
	@Autowired
    private FCProperties                  fcProperties;
	
	@Autowired
   	@Qualifier("notificationApiServiceClient")
   	private INotificationToolApiService   notificationService;
	
	@Autowired
    private OneCheckWalletService 		  oneCheckWalletService;
	
	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService     		  billPayServiceRemote;
	
	@Autowired
    private OrderService 				  orderService;
	
	@Autowired
    private MNPListener 				  mnpListener;
	
	@Autowired
    private BillPaymentService            billPaymentService;
	
	@Autowired
    private RechargeSNSService 			  rechargeSNSService;
	
	@Autowired
    private UserTransactionHistoryService transactionHistoryService;
	
	@Autowired
    private HomeService                   homeService;

	@Autowired
	private RechargeRetryConfigReadThroughCache rechargeCache;
	
	private InExcelReader 				  inExcelReader = new InExcelReader();
	
	private void notifyForSuccessfulRecharge(Map<String, Object> payloadData) throws Exception {
		String orderId = (String) payloadData.get("orderId");
		Long userId = (Long) payloadData.get("userId");
		logger.info("rechargeSNSService publish triggering for orderId : " + orderId);
		String statusCode = "00";
		try {
			UserTransactionHistory userTransactionHistory = transactionHistoryService
	                .findUserTransactionHistoryByOrderId(orderId);
			Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
	        if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
	            userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
	            userDetail.put("circleName", userTransactionHistory.getServiceRegion());
	            userDetail.put("productType", userTransactionHistory.getProductType());
	        }
	        UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
	        TxnHomePageBusinessDO homePage = orderService.getHomePageForOrder(orderId);
	        String emailId = userDetailsObj.getUserEmail();
			String oneCheckId = null;
			try {
				oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			} catch (Exception e) {
				logger.error("Caught exception in fetching oneCheckId", e);
			}
			RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
			ReconMetaData reconMetaData = new ReconMetaData();
			rechargeAlertBean.setOrderId(orderId);
			rechargeAlertBean.setOneCheckId(oneCheckId);
			rechargeAlertBean.setChannel(orderService.getChannelName(userDetailsObj.getChannelId()));
			rechargeAlertBean.setServiceNumber(userDetailsObj.getUserMobileNo());
			rechargeAlertBean.setServiceProvider(userDetailsObj.getOperatorName());
			rechargeAlertBean.setServiceRegion(userDetailsObj.getCircleName());
			rechargeAlertBean.setAmount(Double.valueOf(userDetailsObj.getAmount()));
			
			reconMetaData.setCategoryType(userDetailsObj.getOperatorName());

			boolean isMnp = mnpListener.isMNPNumber(userDetailsObj.getUserMobileNo(), homePage.getOperatorName(),
					homePage.getProductId());
			rechargeAlertBean.setIsMNP(isMnp ? "yes" : "no");
			//get cache value for lob3
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				boolean lob3 = rechargeCache.getLOBKey(orderId);
				if (lob3) {
					rechargeAlertBean.setLob("ondeck_utility");
					// get the merchant id for aggregator, i.e billdesk
					String merchantId = fcProperties.getProperty(FCConstants.BILLDESK_MERCHANT_ID);
					rechargeAlertBean.setMobMerchantId(merchantId);
				}
			}

			if (FCConstants.PRODUCT_IDS_FOR_IN_REQUEST.contains(userDetailsObj.getProductType())) {
				InTransactionData inTransactionData = inService.getInTransactionData(orderId);
				rechargeAlertBean
						.setAg(RechargeUtil.getAggregatorName(inTransactionData.getInResponse().getAggrName()));
				rechargeAlertBean.setAgRequestId(
						fcProperties.getInTransIdPrefix() + inTransactionData.getInResponse().getRetryNumber() + "_"
								+ inTransactionData.getInResponse().getRequestId());
				rechargeAlertBean.setPlanType(inService.getInRequest(orderId).getRechargePlan());
				rechargeAlertBean.setOperatorTxnId(inTransactionData.getInResponse().getOprTransId());
				//used to set parameters for the new reporting flow
				reconMetaData.setCategorySubtype(RechargeUtil.getAggregatorName(inTransactionData.getInResponse().getAggrName()));
				reconMetaData.setCategoryId(inService.getInRequest(orderId).getRechargePlan());
				reconMetaData.setReconId(fcProperties.getInTransIdPrefix() + inTransactionData.getInResponse().getRetryNumber() + "_"
						+ inTransactionData.getInResponse().getRequestId());
				reconMetaData.setReferenceId(inTransactionData.getInResponse().getAggrTransId());
				
			} else if (userDetailsObj.getProductType().equals(FCConstants.PRODUCT_TYPE_POSTPAID)) {
				BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
				if(billPaymentStatus == null) {
					throw new RuntimeException("billPaymentStatus is null.");
				}else {
					rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(billPaymentStatus.getBillPaymentGateway()));
					rechargeAlertBean.setAgRequestId(
							fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderId);
					rechargeAlertBean.setPlanType(FCConstants.RECHARGE_TYPE_TOPUP);
					rechargeAlertBean.setOperatorTxnId(billPaymentStatus.getGatewayReferenceNumber());
					
					//used to set parameters for the new reporting flow
					reconMetaData.setCategorySubtype(RechargeUtil.getAggregatorName(billPaymentStatus.getBillPaymentGateway()));
					reconMetaData.setCategoryId(FCConstants.RECHARGE_TYPE_TOPUP);
					reconMetaData.setReconId(fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderId);
					reconMetaData.setReferenceId(billPaymentStatus.getGatewayReferenceNumber());
				}
				
			} else if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				String aggregatorName = (String) payloadData.get("aggregator");
				String billTransactionId = (String) payloadData.get("billTransactionId");
				String aggrBillTransactionId = (String) payloadData.get("aggrBillTransactionId");
				String fcBillerId = null;
				List<BillTransactionLedger> billDetails = billPayServiceRemote.getBillTransactionLedgers(homePage.getServiceNumber());
				if (billDetails != null) {
					BillTransactionLedger latestTxn = billDetails.get(0);
					ViewUserBillsResponse viewUserBillsResponse = billPayServiceRemote.getUserBiller(homePage.getServiceNumber(), userId.intValue(), userDetailsObj.getUserMobileNo(), RechargeUtil.getAggregatorName(latestTxn.getAggregator()));
					if(viewUserBillsResponse != null) {
						fcBillerId = viewUserBillsResponse.getFcBillerId();
					}else {
						logger.info("Got null ViewUserBillsResponse for orderId : " + orderId);
					}
				}
				rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(aggregatorName));
				rechargeAlertBean.setAgRequestId(billTransactionId);
				rechargeAlertBean.setFcBillerId(fcBillerId);
				rechargeAlertBean.setOperatorTxnId(aggrBillTransactionId);
				rechargeAlertBean.setPlanType(FCConstants.RECHARGE_TYPE_TOPUP);

				reconMetaData.setCategorySubtype(RechargeUtil.getAggregatorName(aggregatorName));
				reconMetaData.setReconId(billTransactionId);
				reconMetaData.setReferenceId(aggrBillTransactionId);
				reconMetaData.setCategoryId(FCConstants.RECHARGE_TYPE_TOPUP);
			}

			String rechargedProductType = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType())
					.getLabel();
			rechargeAlertBean.setProduct(rechargedProductType);
			rechargeAlertBean.setStatusCode(statusCode);
			rechargeAlertBean.setUserId(userId);
			rechargeAlertBean.setEmailId(emailId);
			rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus());
			
			//setting the parameter for recon meta data as it is required by new Reporting flow
			rechargeAlertBean.setReconMetaData(reconMetaData);

			rechargeSNSService.publish("{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
		} catch (Exception e) {
			logger.error("rechargeSNSService publish failure for orderId : " + orderId, e);
			throw e;
		}
	}
	
	public void rectifyTranslatorForOrders(String filePath, boolean sleepThread) throws InterruptedException {
		logger.info("Reading data from file : " + filePath);
		ReportData reportData = null;
		try {
			reportData = inExcelReader.read(filePath);
		}catch (Exception e) {
			logger.error("Unable to read from " + filePath + " because of exception : ", e);
			return;
		}
		logger.info("Data has been read from file : " + filePath);
		List<List<String>> allOrderDetails = reportData.getData();
		Integer counter = 1;
		allOrderDetails.remove(0);
		for(List<String> orderData : allOrderDetails) {
			if(counter % 100 == 0) {
				Thread.sleep(5000);
			}
			Map<String, Object> orderDataMap = new HashMap<>();
			String orderId = orderData.get(0).trim();
			Long userId = 0L;
			try {
				Double userIdDbl = Double.parseDouble(orderData.get(1).trim());
				userId = userIdDbl.longValue();
			}catch (NumberFormatException e) {
				logger.error("Unable to parse userId : " + orderData.get(1).trim() +" for recordNumber : " + counter);
			}
			String aggregatorName = orderData.get(25).trim();
			String billTransactionId = null;
			Long billTransactionIdLong = null;
			try {
				Double billTransactionIdDbl = Double.parseDouble(orderData.get(26).trim());
				billTransactionIdLong = billTransactionIdDbl.longValue();
				billTransactionId = String.valueOf(billTransactionIdLong);
			}catch (NumberFormatException e) {
				logger.error("Unable to parse billTransactionId : " + orderData.get(26).trim() +" for recordNumber : " + counter);
			}
			String aggrBillTransactionId = orderData.get(27).trim();
			orderDataMap.put("orderId", orderId);
			orderDataMap.put("userId", userId);
			orderDataMap.put("aggregator",aggregatorName);
			orderDataMap.put("billTransactionId",billTransactionId);
			orderDataMap.put("aggrBillTransactionId",aggrBillTransactionId);
			logger.info(String.format("Record Num : %s, orderId : %s, userId : %s, aggregator : %s, billTransactionId : %s, aggrBillTransactionId : %s", 
					String.valueOf(counter), 
					String.valueOf(orderDataMap.get("orderId")),
					String.valueOf(orderDataMap.get("userId")),
					String.valueOf(orderDataMap.get("aggregator")),
					String.valueOf(orderDataMap.get("billTransactionId")),
					String.valueOf(orderDataMap.get("aggrBillTransactionId"))));
			try {
				notifyForSuccessfulRecharge(orderDataMap);
			} catch (Exception e) {
				logger.error("Unable to process recordNumber : " + counter);
			}
			counter++;	
		}
		
	}
	
	public void rectifyTranslatorForOrdersAsync(final String filePath) {
		final Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					logger.info("Starting thread : RectifyTranslatorThread");
					rectifyTranslatorForOrders(filePath, true);
					logger.info("Finished thread : RectifyTranslatorThread");
				} catch (InterruptedException e) {
					logger.error("InterruptedException exception : ", e);
				} catch (Exception e) {
					logger.error("Unknown exception inside thread : ", e);
				}
			}
		});
		thread.setName("RectifyTranslatorThread");
		try {
			thread.start();
		}catch (IllegalThreadStateException e) {
			logger.error("Unable to start thread : " + thread.getName() + " because of exception : ", e);
		}catch (Exception e) {
			logger.error("Unknown exception before start thread : "+ thread.getName() + " : ", e);
		}
	}
}
