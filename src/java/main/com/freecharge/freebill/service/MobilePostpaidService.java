package com.freecharge.freebill.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BilldeskException;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.BillPaymentFactory;
import com.freecharge.freebill.common.BillPaymentGatewayPriority;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.collect.ImmutableMap;

@Service
public class MobilePostpaidService {

    private static Logger logger = LoggingFactory.getLogger(MobilePostpaidService.class);
    
	@Autowired
	private OperatorCircleService operatorCircleService;

	@Autowired
	private BillReadDAO billReadDAO;

	@Autowired BillPaymentFactory billPaymentFactory;
	
	@Autowired 
	private MetricsClient metricsClient;

    @Autowired
    AmazonDynamoDBAsync dynamoClientMum;
 
	/**
	 * Find and return the merchant-ID; return NULL if unable to find.
	 * @param mobileNumber
	 * @return
	 */
	public Integer findOperatorIdByMobileNumber(String mobileNumber) {
	    Integer prepaidMobileOperatorId = null;
	    String postpaidMobileOperatorId = null;
		Map<String, Object> operator = operatorCircleService.findOperatorByMobilePrefix(mobileNumber);
		
		if (operator!=null) {
		    prepaidMobileOperatorId = (Integer)operator.get("operatorMasterId");
		}
		
		if(prepaidMobileOperatorId!=null) {
		    postpaidMobileOperatorId = billReadDAO.findPostpaidOperatorIdByPrepaidOperatorID(prepaidMobileOperatorId);
		}
		
		return (postpaidMobileOperatorId==null)? null:Integer.parseInt(postpaidMobileOperatorId);
	}
	
	public Integer findMNPOperatorByMobileNumber(String mobileNumber) {
        Integer mnpOpreator = getMnpOpreator(mobileNumber);
        
        if (mnpOpreator != null) {
            return mnpOpreator;
        }
        
        return findOperatorIdByMobileNumber(mobileNumber);
	}
	
	private Integer getMnpOpreator(String mobileNumber) {
	    GetItemRequest getItemRequest = new GetItemRequest("mnpMap", ImmutableMap.of("serviceNumber", new AttributeValue(mobileNumber)));
	    getItemRequest.setConsistentRead(false);
	    
        final long startTime = System.currentTimeMillis();
        try {
            GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);

            metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
            metricsClient.recordEvent("DynamoDB", "GetItem", "Success");
            
            if (getItemResult !=null && getItemResult.getItem() != null) {
                Map<String, AttributeValue> mnpItem = getItemResult.getItem();

                final String mnpProduct = mnpItem.get("pr").getS();
                final String mnpOperator = mnpItem.get("oi").getS();
                
                if (StringUtils.equals(Integer.toString(ProductName.MobilePostpaid.getProductId()), mnpProduct)) {
                    return Integer.parseInt(mnpOperator);
                }
            }
            
        } catch (AmazonServiceException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonServiceException while fetching MNP", e);
        } catch (AmazonClientException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonClientException while fetching MNP", e);
        }
        
        return null;
	}

	public BillPaymentCommonValidationApiResponse validate(
			String operatorID, String mobileNumber, String accountNumber, BigDecimal amount) {
		IBillValidationRequest req = createValidationRequest(operatorID, amount, mobileNumber, accountNumber);
		BillPostpaidMerchant billPostpaidMerchant=null;
		List<BillPaymentGatewayPriority> billPaymentGatewayPrioritiesList = billReadDAO.findBillPaymentGatewayByOperatorId(operatorID);
		
		String billPymentGateway = billPaymentGatewayPrioritiesList==null ? null : billPaymentGatewayPrioritiesList.get(0).getBillProvider();
		
		if(billPymentGateway==null) {
		    logger.info(" Bill payment gateway response is null ");
		    billPymentGateway = PaymentConstants.BILL_PAPYMENT_DEFAULT_GATEWAY;
		} else {
		    logger.info("Bill payment gateway type for merchantID " + operatorID + " is " +  billPymentGateway);
		}
		
		StringTokenizer stringTokenizer = new StringTokenizer(billPymentGateway, ",");
		
		while(stringTokenizer.hasMoreElements()) {
		    billPymentGateway = stringTokenizer.nextElement().toString();
		    break;
		}
		
        IBillPaymentInterface billPaymentInterface = billPaymentFactory.getBillPaymentGateway(billPymentGateway);
        
        if(req.getOperatorID()!=null && !req.getOperatorID().isEmpty()) {
            billPostpaidMerchant = billReadDAO.findMerchantObjectByOperatorID(Integer.parseInt(operatorID)); 
        }
        
        if(billPostpaidMerchant==null) {
            throw new IllegalStateException("billPostpaidMerchant is found null for operator ID" + operatorID);
        }
		
		try {
		    BillPaymentCommonValidationApiResponse response = billPaymentInterface.submitValidationRequest(req, billPostpaidMerchant);
			return response;
		} catch(IOException e) {
			throw new BilldeskException(String.format(
					"IO error when validating bill-payment merchantID=%s, mobileNumber=%s, accountNumber=%s, amount=%s",
					FCUtil.stringify(operatorID), FCUtil.stringify(mobileNumber), FCUtil.stringify(accountNumber),
					FCUtil.stringify(amount)),
					e);
		}
	}

	private static MobilePostpaidValidationRequest createValidationRequest(String operatorID, BigDecimal transactionAmount,
			final String mobileNumber, final String accountNumber) {
		return createValidationRequest(
		        operatorID, transactionAmount, mobileNumber, accountNumber, FCUtil.getAsiaKolkataTimeNow());
	}

	private static MobilePostpaidValidationRequest createValidationRequest(String operatorID, BigDecimal transactionAmount,
			final String mobileNumber, final String accountNumber, Calendar timestamp) {
		switch (operatorID) {
		case BillerMobilePostpaid.OPERATOR_ID_AIRTEL:            // Airtel postpaid mobile
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_LOOP:              // Loop mobile
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_BSNL_CELLONE:      // BSNL CellOne
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_DOCOMO:       // Tata Docomo (GSM) - ac no (1) or mobile no (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount, timestamp, accountNumber, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_IDEA_POSTPAID:     // IDEA Postpaid
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_TELESERVICES: // Tata TeleServices  - ac no (1) or mobile no (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount, timestamp, accountNumber, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_VODAFONE_POSTPAID: // Vodafone postpaid
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_CDMA:     // Reliance Comm Infrastructure Ltd - mob# (1) or ac# (2)
			return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber, accountNumber);
			
		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_GSM:     // Reliance Comm Infrastructure Ltd - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber, accountNumber);
		
		case BillerMobilePostpaid.OPERATOR_ID_AIRCEL:     // Aircel - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber, accountNumber);
		
		case BillerMobilePostpaid.OPERATOR_ID_MTS:     // MTS - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber, accountNumber);
		
		default:
			throw new IllegalArgumentException("Invalid merchantID: " + FCUtil.stringify(operatorID));
		}
	}

	private abstract static class MobilePostpaidValidationRequest extends AbstractValidationRequest {
		private final String operatorID;
		private final BigDecimal transactionAmount;
		private final String consumerNumber;

        public MobilePostpaidValidationRequest(String operatorID, BigDecimal transactionAmount, Calendar timestamp,
                String consumerNumber) {
            super(timestamp);
            this.operatorID = operatorID;
            this.transactionAmount = transactionAmount;
            this.consumerNumber = consumerNumber;
        }
		@Override
		public String getOperatorID() {
			return operatorID;
		}
		@Override
		public BigDecimal getTransactionAmount() {
			return transactionAmount;
		}
		@Override
		public String getConsumerNumber() {
            return consumerNumber;
        }
	}

	private static MobilePostpaidValidationRequest vrFromMobileNumber(
			String operatorID, BigDecimal transactionAmount, Calendar timestamp, final String mobileNumber) {
		FCUtil.assertNonEmptyArgument(mobileNumber, "mobileNumber");
		return new MobilePostpaidValidationRequest(operatorID, transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidValidationRequest fromAccountAndMobileNumber(
			String operatorID, BigDecimal transactionAmount, Calendar timestamp,
			final String accountNumber, final String mobileNumber) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(
				mobileNumber, "mobileNumber",
				accountNumber, "accountNumber"));
		return new MobilePostpaidValidationRequest(operatorID, transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return accountNumber;
			}
			@Override
			public String getAdditionalInfo2() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidValidationRequest vrFromMobileAndAccountNumber(
			String operatorID, BigDecimal transactionAmount, Calendar timestamp,
			final String mobileNumber, final String accountNumber) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(
				mobileNumber, "mobileNumber",
				accountNumber, "accountNumber"));
		return new MobilePostpaidValidationRequest(operatorID, transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}
			@Override
			public String getAdditionalInfo2() {
				return accountNumber;
			}
		};
	}

	// ------------ Payment --------------

	private abstract static class MobilePostpaidPaymentRequest extends AbstractPaymentRequest {
		private final String operatorID;
		private final BigDecimal transactionAmount;
		private final String orderID;
		public MobilePostpaidPaymentRequest(String operatorID, BigDecimal transactionAmount, String orderID) {
			FCUtil.assertAllNonEmptyArgument(FCUtil.createMap(
			        operatorID, "operatorID",
					orderID, "orderID"));
			FCUtil.assertNonNullArgument(transactionAmount, "transactionAmount");
			this.operatorID = operatorID;
			this.transactionAmount = transactionAmount;
			this.orderID = orderID;
		}
		@Override
		public String getOperatorID() { return operatorID; }
		@Override
		public BigDecimal getTransactionAmount() { return transactionAmount; }
		@Override
		public String getCcno() { return orderID; }
		@Override
		public String getCustomerID() {
			return getAdditionalInfo1();
		}
	}

	public MobilePostpaidPaymentRequest createPaymentRequest(BillTxnHomePage bthp, String orderID, TxnHomePage thp) {
		final String operatorID = Integer.toString(thp.getFkOperatorId());
		final BigDecimal transactionAmount = bthp.getBillAmount();
		final String mobileNumber = bthp.getPostpaidNumber();
		final String accountNumber = bthp.getPostpaidAccountNumber();
		switch (operatorID) {
		case BillerMobilePostpaid.OPERATOR_ID_AIRTEL:            // Airtel postpaid mobile
			return prFromMobileNumber(operatorID, transactionAmount, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_LOOP:              // Loop mobile
			return prFromMobileNumber(operatorID, transactionAmount, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_BSNL_CELLONE:      // BSNL CellOne
			return prFromMobileNumber(operatorID, transactionAmount, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_DOCOMO:       // Tata Docomo (GSM) - ac no (1) or mobile no (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount, accountNumber, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_IDEA_POSTPAID:     // IDEA Postpaid
			return prFromMobileNumber(operatorID, transactionAmount, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_TELESERVICES: // Tata TeleServices  - ac no (1) or mobile no (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount, accountNumber, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_VODAFONE_POSTPAID: // Vodafone postpaid
			return prFromMobileNumber(operatorID, transactionAmount, mobileNumber, orderID);

		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_CDMA:     // Reliance Comm Infrastructure Ltd - mob# (1) or ac# (2)
			return vrFromMobileAndAccountNumber(operatorID, transactionAmount, mobileNumber, accountNumber, orderID);
			
		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_GSM:     // Reliance Comm Infrastructure Ltd - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, mobileNumber, accountNumber, orderID);

        case BillerMobilePostpaid.OPERATOR_ID_AIRCEL:     // Aircel - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, mobileNumber, accountNumber, orderID);

        case BillerMobilePostpaid.OPERATOR_ID_MTS:     // MTS - mob# (1) or ac# (2)
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, mobileNumber, accountNumber, orderID);

		default:
			throw new IllegalArgumentException("Invalid operatorID: " + FCUtil.stringify(operatorID));
		}
	}

	private static MobilePostpaidPaymentRequest prFromMobileNumber(
			String operatorID, BigDecimal transactionAmount, final String mobileNumber, final String orderID) {
		FCUtil.assertNonEmptyArgument(mobileNumber, "mobileNumber");
		return new MobilePostpaidPaymentRequest(operatorID, transactionAmount, orderID) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidPaymentRequest fromAccountAndMobileNumber(
			String merchantID, BigDecimal transactionAmount, final String accountNumber, final String mobileNumber,
			final String orderID) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(
				mobileNumber, "mobileNumber",
				accountNumber, "accountNumber"));
		return new MobilePostpaidPaymentRequest(merchantID, transactionAmount, orderID) {
			@Override
			public String getAdditionalInfo1() {
				return accountNumber;
			}
			@Override
			public String getAdditionalInfo2() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidPaymentRequest vrFromMobileAndAccountNumber(
			String merchantID, BigDecimal transactionAmount, final String mobileNumber, final String accountNumber,
			final String orderID) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(
				mobileNumber, "mobileNumber",
				accountNumber, "accountNumber"));
		return new MobilePostpaidPaymentRequest(merchantID, transactionAmount, orderID) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}
			@Override
			public String getAdditionalInfo2() {
				return accountNumber;
			}
		};
	}

}
