package com.freecharge.freebill.service;

import java.util.Calendar;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.freebill.billdesk.IBillValidationRequest;


public abstract class AbstractValidationRequest extends AbstractDO implements IBillValidationRequest {

	private final Calendar timestamp;

	public AbstractValidationRequest(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public final String getTimestamp() {
		return String.format("%04d%02d%02d%02d%02d%02d",
				timestamp.get(Calendar.YEAR),
				timestamp.get(Calendar.MONTH) + 1,
				timestamp.get(Calendar.DAY_OF_MONTH),
				timestamp.get(Calendar.HOUR_OF_DAY),
				timestamp.get(Calendar.MINUTE),
				timestamp.get(Calendar.SECOND));
	}

	// ---- additional-info 2-7 are not always used; provide default impl ----

	@Override
	public String getAdditionalInfo2() { return null; }
	@Override
	public String getAdditionalInfo3() { return null; }
	@Override
	public String getAdditionalInfo4() { return null; }
	@Override
	public String getAdditionalInfo5() { return null; }
	@Override
	public String getAdditionalInfo6() { return null; }
	@Override
	public String getAdditionalInfo7() { return null; }

}
