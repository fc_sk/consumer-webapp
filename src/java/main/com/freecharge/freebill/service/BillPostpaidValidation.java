package com.freecharge.freebill.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BilldeskException;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.BillPaymentFactory;
import com.freecharge.freebill.common.BillPaymentGatewayPriority;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.rest.validators.ServiceNumberFormatValidator;

@Service
public class BillPostpaidValidation {

	private static Logger logger = LoggingFactory
			.getLogger(BillPostpaidValidation.class);

	@Autowired
	private BillReadDAO billReadDAO;

	@Autowired
	BillPaymentFactory billPaymentFactory;
	
	@Autowired
    private RechargeMetricService     rechargeMetricService;

	@Autowired
	private OperatorCircleService operatorCircleService;
	
	@Autowired
    private MetricsClient             metricsClient;
	
	private final int POSTPAID_NUMBER_LENGTH = 10;

	public BillPaymentCommonValidationApiResponse validate(String operatorID,
			String mobileNumber, String accountNumber, BigDecimal amount) {
		IBillValidationRequest req = createValidationRequest(operatorID,
				amount, mobileNumber, accountNumber);
		BillPostpaidMerchant billPostpaidMerchant = null;
		List<BillPaymentGatewayPriority> billPaymentGatewayPriority = billReadDAO
				.findBillPaymentGatewayByOperatorId(operatorID);
		String billPymentGateway = billPaymentGatewayPriority.get(0)
				.getBillProvider();

		if (billPymentGateway == null) {
			logger.info(" Bill payment gateway response is null ");
			billPymentGateway = PaymentConstants.BILL_PAPYMENT_DEFAULT_GATEWAY;
		} else {
			logger.info("Bill payment gateway type for merchantID "
					+ operatorID + " is " + billPymentGateway);
		}

		StringTokenizer aggrprioritytokens = new StringTokenizer(
				billPymentGateway, ",");

		while (aggrprioritytokens.hasMoreElements()) {

			billPymentGateway = aggrprioritytokens.nextElement().toString();
			break;
		}

		logger.info("billPayment Gateway for mobileno:"+mobileNumber+" is "+billPymentGateway);
		
		IBillPaymentInterface billPaymentInterface = billPaymentFactory
				.getBillPaymentGateway(billPymentGateway);
		if (req.getOperatorID() != null && !req.getOperatorID().isEmpty()) {
			billPostpaidMerchant = billReadDAO
					.findMerchantObjectByOperatorID(Integer
							.parseInt(operatorID));
		}

		if (billPostpaidMerchant == null) {
			throw new IllegalStateException(
					"billPostpaidMerchant is found null for operator ID"
							+ operatorID);
		}

		try {
			BillPaymentCommonValidationApiResponse response = billPaymentInterface
					.submitValidationRequest(req, billPostpaidMerchant);
			return response;
		} catch (IOException e) {
			throw new BilldeskException(
					String.format(
							"IO error when validating bill-payment merchantID=%s, mobileNumber=%s, accountNumber=%s, amount=%s",
							FCUtil.stringify(operatorID),
							FCUtil.stringify(mobileNumber),
							FCUtil.stringify(accountNumber),
							FCUtil.stringify(amount)), e);
		}
	}

	private static MobilePostpaidValidationRequest createValidationRequest(
			String operatorID, BigDecimal transactionAmount,
			final String mobileNumber, final String accountNumber) {
		return createValidationRequest(operatorID, transactionAmount,
				mobileNumber, accountNumber, FCUtil.getAsiaKolkataTimeNow());
	}

	private abstract static class MobilePostpaidValidationRequest extends
			AbstractValidationRequest {
		private final String operatorID;
		private final BigDecimal transactionAmount;
		private final String consumerNumber;
		
        public MobilePostpaidValidationRequest(String operatorID,
				BigDecimal transactionAmount, Calendar timestamp, String consumerNumber) {
			super(timestamp);
			this.operatorID = operatorID;
			this.transactionAmount = transactionAmount;
			this.consumerNumber = consumerNumber;
		}

		@Override
		public String getOperatorID() {
			return operatorID;
		}

		@Override
		public BigDecimal getTransactionAmount() {
			return transactionAmount;
		}
		
		public String getConsumerNumber() {
            return consumerNumber;
        }
	}

	private static MobilePostpaidValidationRequest createValidationRequest(
			String operatorID, BigDecimal transactionAmount,
			final String mobileNumber, final String accountNumber,
			Calendar timestamp) {
		switch (operatorID) {
		case BillerMobilePostpaid.OPERATOR_ID_AIRTEL: // Airtel postpaid mobile
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp,
					mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_LOOP: // Loop mobile
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp,
					mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_BSNL_CELLONE: // BSNL CellOne
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp,
					mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_DOCOMO: // Tata Docomo (GSM)
															// - ac no (1) or
															// mobile no (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount,
					timestamp, accountNumber, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_IDEA_POSTPAID: // IDEA Postpaid
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp,
					mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_TATA_TELESERVICES: // Tata
																	// TeleServices
																	// - ac no
																	// (1)
																	// or mobile
																	// no
																	// (2)
			return fromAccountAndMobileNumber(operatorID, transactionAmount,
					timestamp, accountNumber, mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_VODAFONE_POSTPAID: // Vodafone
																	// postpaid
			return vrFromMobileNumber(operatorID, transactionAmount, timestamp,
					mobileNumber);

		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_CDMA: // Reliance Comm
																// Infrastructure
																// Ltd - mob#
																// (1)
																// or ac# (2)
			return vrFromMobileAndAccountNumber(operatorID, transactionAmount,
					timestamp, mobileNumber, accountNumber);

		case BillerMobilePostpaid.OPERATOR_ID_RELIANCE_GSM: // Reliance Comm
															// Infrastructure
															// Ltd - mob# (1) or
															// ac# (2)
			return vrFromMobileAndAccountNumber(operatorID, transactionAmount,
					timestamp, mobileNumber, accountNumber);
		
        case BillerMobilePostpaid.OPERATOR_ID_AIRCEL: // Aircel
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber,
                    accountNumber);

        case BillerMobilePostpaid.OPERATOR_ID_MTS: // MTS
            return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber,
                    accountNumber);
            
        case BillerMobilePostpaid.OPERATOR_ID_JIO:
        	return vrFromMobileAndAccountNumber(operatorID, transactionAmount, timestamp, mobileNumber,
                    accountNumber);

		default:
			throw new IllegalArgumentException("Invalid merchantID: "
					+ FCUtil.stringify(operatorID));
		}
	}

	private static MobilePostpaidValidationRequest vrFromMobileNumber(
			String operatorID, BigDecimal transactionAmount,
			Calendar timestamp, final String mobileNumber) {
		FCUtil.assertNonEmptyArgument(mobileNumber, "mobileNumber");
		return new MobilePostpaidValidationRequest(operatorID,
				transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidValidationRequest fromAccountAndMobileNumber(
			String operatorID, BigDecimal transactionAmount,
			Calendar timestamp, final String accountNumber,
			final String mobileNumber) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(mobileNumber,
				"mobileNumber", accountNumber, "accountNumber"));
		return new MobilePostpaidValidationRequest(operatorID,
				transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return accountNumber;
			}

			@Override
			public String getAdditionalInfo2() {
				return mobileNumber;
			}
		};
	}

	private static MobilePostpaidValidationRequest vrFromMobileAndAccountNumber(
			String operatorID, BigDecimal transactionAmount,
			Calendar timestamp, final String mobileNumber,
			final String accountNumber) {
		FCUtil.assertAnyNonEmptyArgument(FCUtil.createMap(mobileNumber,
				"mobileNumber", accountNumber, "accountNumber"));
		return new MobilePostpaidValidationRequest(operatorID,
				transactionAmount, timestamp, mobileNumber) {
			@Override
			public String getAdditionalInfo1() {
				return mobileNumber;
			}

			@Override
			public String getAdditionalInfo2() {
				return accountNumber;
			}
		};
	}

	public ErrorCode postpaidRequestParamvalidation(String mobileNumber,
			String operatorID, String amount) {
		if (StringUtils.isBlank(operatorID)) {
			return ErrorCode.OPERATOR_NOT_PRESENT;
		}

		if (!BillerMobilePostpaid.ALL_BILLERS.keySet().contains(operatorID)) {
			return ErrorCode.INVALID_OPERATOR;
		}

		ErrorCode amountValidationResult = AmountFormatValidator
				.validate(amount);

		if (amountValidationResult != ErrorCode.NO_ERROR) {
			return amountValidationResult;
		}

		if (!BillPaymentPostpaidAmountValidation(operatorID, amount)) {
			return ErrorCode.INVALID_AMOUNT_FOR_OPERATOR;
		}

		if (ServiceNumberFormatValidator.validate(mobileNumber,
				POSTPAID_NUMBER_LENGTH) != ErrorCode.NO_ERROR) {
			return ServiceNumberFormatValidator.validate(mobileNumber,
					POSTPAID_NUMBER_LENGTH);
		}
        if (rechargeMetricService.isPostpaidPermanentFailure(RechargeConstants.POST_PAID_PRODUCT_TYPE,
                operatorID, mobileNumber)) {
            metricsClient.recordEvent("Postpaid", "Save.ServerBlock", "PermanentFailure");
            logger.info("blocking transaction for PostpaidPermanentFailure " + mobileNumber);
            return ErrorCode.POSTPAID_PERMANENT_FAILURE;
        }

		return ErrorCode.NO_ERROR;
	}
	
	public ErrorCode postpaidRequestParamvalidationV3(String mobileNumber,
			String operatorID, String amount) {
		if (StringUtils.isBlank(operatorID)) {
			return ErrorCode.OPERATOR_NOT_PRESENT;
		}
		
		if (!AmountFormatValidator.isValid(Float.parseFloat(amount))) {
			return ErrorCode.INVALID_AMOUNT_FORMAT;
		}

		if (!operatorCircleService.isValidOperator(ProductName.MobilePostpaid.getProductId(), operatorID)) {
			return ErrorCode.INVALID_OPERATOR;
		}

        if (rechargeMetricService.isPostpaidPermanentFailure(RechargeConstants.POST_PAID_PRODUCT_TYPE,
                operatorID, mobileNumber)) {
            metricsClient.recordEvent("Postpaid", "Save.ServerBlock", "PermanentFailure");
            logger.info("blocking transaction for PostpaidPermanentFailure " + mobileNumber);
            return ErrorCode.POSTPAID_PERMANENT_FAILURE;
        }

		return ErrorCode.NO_ERROR;
	}

	public Boolean BillPaymentPostpaidAmountValidation(String operatorId,
			String amount) {

		Integer amountValue = Integer.parseInt(amount);

		switch (Integer.parseInt(operatorId)) {
		case 56:
			if (amountValue < 10)
				return false;
			return true;
		case 57:
			if (amountValue < 10)
				return false;
			return true;
		case 58:
			if (amountValue < 10)
				return false;
			return true;
		case 59:
			if (amountValue < 100)
				return false;
			return true;
		case 60:
			if (amountValue < 100)
				return false;
			return true;
		case 61:
			if (amountValue < 100)
				return false;
			return true;
		case 62:
			if (amountValue < 10)
				return false;
			return true;
		case 63:
			if (amountValue < 50)
				return false;
			return true;
		case 64:
			if (amountValue < 50)
				return false;
			return true;
		default:
			return true;
		}
	}
}