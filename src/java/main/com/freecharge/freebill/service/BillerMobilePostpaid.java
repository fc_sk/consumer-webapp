package com.freecharge.freebill.service;

import java.util.Map;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;

public class BillerMobilePostpaid extends AbstractDO {

	public static final String OPERATOR_ID_AIRTEL            = "56";
	public static final String OPERATOR_ID_LOOP              = "57";
	public static final String OPERATOR_ID_BSNL_CELLONE      = "58";
	public static final String OPERATOR_ID_TATA_DOCOMO       = "59";
	public static final String OPERATOR_ID_IDEA_POSTPAID     = "60";
	public static final String OPERATOR_ID_TATA_TELESERVICES = "61";
	public static final String OPERATOR_ID_VODAFONE_POSTPAID = "62";
	public static final String OPERATOR_ID_RELIANCE_CDMA     = "63";
	public static final String OPERATOR_ID_RELIANCE_GSM     =  "64";
    public static final String OPERATOR_ID_AIRCEL            = "65";
    public static final String OPERATOR_ID_MTS               = "66";
    public static final String OPERATOR_ID_JIO 				 = "91";

	public final String merchantID;
	public final String displayName;
	public final boolean partialPaymentAllowed;
	public final boolean paymentAmountMustEqualValidationAmount;

	public BillerMobilePostpaid(String merchantID, String displayName,
			boolean partialPaymentAllowed, boolean paymentAmountMustEqualValidationAmount) {
		this.merchantID = merchantID;
		this.displayName = displayName;
		this.partialPaymentAllowed = partialPaymentAllowed;
		this.paymentAmountMustEqualValidationAmount = paymentAmountMustEqualValidationAmount;
	}

	public static final Map<String, BillerMobilePostpaid> ALL_BILLERS = FCUtil.createMap(
	        OPERATOR_ID_AIRTEL, new BillerMobilePostpaid(OPERATOR_ID_AIRTEL, "Airtel", true, true),
	        OPERATOR_ID_LOOP, new BillerMobilePostpaid(OPERATOR_ID_LOOP, "Loop Mobile", true, false),
	        OPERATOR_ID_BSNL_CELLONE, new BillerMobilePostpaid(OPERATOR_ID_BSNL_CELLONE, "BSNL CellOne", false, false),
	        OPERATOR_ID_TATA_DOCOMO, new BillerMobilePostpaid(OPERATOR_ID_TATA_DOCOMO, "Tata Docomo", true, false),
	        OPERATOR_ID_IDEA_POSTPAID, new BillerMobilePostpaid(OPERATOR_ID_IDEA_POSTPAID, "Idea Postpaid", true, false),
	        OPERATOR_ID_TATA_TELESERVICES, new BillerMobilePostpaid(OPERATOR_ID_TATA_TELESERVICES, "Tata Teleservices", true, false),
	        OPERATOR_ID_VODAFONE_POSTPAID, new BillerMobilePostpaid(OPERATOR_ID_VODAFONE_POSTPAID, "Vodafone Postpaid", true, false),
	        OPERATOR_ID_RELIANCE_CDMA, new BillerMobilePostpaid(OPERATOR_ID_RELIANCE_CDMA, "Reliance Communications Infrastructure Ltd", true, false),
	        OPERATOR_ID_AIRCEL, new BillerMobilePostpaid(OPERATOR_ID_AIRCEL, "Aircel", true, false),
	        OPERATOR_ID_MTS, new BillerMobilePostpaid(OPERATOR_ID_MTS, "MTS", true, false),
	        OPERATOR_ID_RELIANCE_GSM, new BillerMobilePostpaid(OPERATOR_ID_RELIANCE_GSM, "Reliance Communications Infrastructure Ltd", true, false));
	

	public Map<String, Object> toJsonMap() {
		return FCUtil.createMap(
				"display-name", (Object) displayName,
				"partial-payment-allowed", partialPaymentAllowed,
				"payment-amt-must-equal-valid-amt", paymentAmountMustEqualValidationAmount);
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public boolean isPartialPaymentAllowed() {
		return partialPaymentAllowed;
	}

	public boolean isPaymentAmountMustEqualValidationAmount() {
		return paymentAmountMustEqualValidationAmount;
	}

}
