package com.freecharge.freebill.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.caucho.hessian.client.HessianConnectionException;
import com.freecharge.BilldeskTransactionBackFill.UpdateTransactionStatusResponse;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BilldeskException;
import com.freecharge.freebill.billdesk.IBillPaymentRequest;
import com.freecharge.freebill.common.BillPaymentCacheManager;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.BillPaymentErrorCodeMap;
import com.freecharge.freebill.common.BillPaymentFactory;
import com.freecharge.freebill.common.BillPaymentGatewayPriority;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.infrastructure.billpay.beans.BillTransactionLedger;
import com.freecharge.infrastructure.billpay.beans.CreateBillPayRequest;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse.Status;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.order.service.dao.BillpayUnknownStatusDao;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeCustomConfigUtil;
import com.freecharge.sns.RechargeAttemptSNSService;
import com.freecharge.sns.RechargeSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.sns.bean.RechargeAttemptBean;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.BillpayUnknownStatus;

@Service
public class BillPaymentService {

	public static final int ADHOC_BILLER_TYPE = 0;

	private static Logger logger = LoggingFactory.getLogger(BillPaymentService.class);

	@Autowired
	private KestrelWrapper kestrelWrapper;

	@Autowired
	TxnHomePageDAO txnHomePageDAO;

	@Autowired
	BillReadDAO billReadDAO;

	@Autowired
	private BillPaymentCacheManager billPaymentCacheManager;

	@Autowired
	BillWriteDAO billWriteDAO;

	@Autowired
	private AppConfigService appConfig;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private MobilePostpaidService mobilePostpaidService;

	@Autowired
	private BillPaymentFactory billPaymentFactory;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private OrderIdSlaveDAO orderIdDao;

	@Autowired
	private FulfillmentService fulfillmentService;

	@Autowired
	private TxnFulFilmentService txnFulFilmentService;

	@Autowired
	private BillPostpaidValidation billPostpaidValidation;

	@Autowired
	private InService inService;

	@Autowired
	private UserTransactionHistoryService transactionHistoryService;

	@Autowired
	private MetricsClient metricsClient;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	private OperatorCircleService operatorCircleService;

	@Autowired
	private HomeBusinessDao homeBusinessDao;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private RechargeAttemptSNSService rechargeAttemptSNSService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private RechargeSNSService rechargeSNSService;

	@Autowired
	private CampaignServiceClient campaignServiceClient;

	@Autowired
	private BBPSDetailsDao bbpsDetailsDao;

	@Autowired
	private BillpayUnknownStatusDao billpayUnknownStatusDao;

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private RechargeCustomConfigUtil rechargeCustomConfigUtil;

	private static final String FETCH_BILL_OPERATORS = "FetchBillOperator";

	private static final String EURONET_FETCH_BILL_OPERATOR = "EuronetFetchBillOperator";

	private static final String BDK_FETCH_BILL_OPERATOR = "BilldeskFetchBillOperator";

	private static final String MESSAGE_PARAM_TILDE_SEPARATOR = "~";

	@Transactional
	public TxnHomePage saveMobileTransactionInfo(String mobileNumber, double amount,
			BillPaymentCommonValidationApiResponse response, String fkOperatorId) throws Exception {

		String operatorName = getOperatorName(fkOperatorId);
		if (operatorName == null) {
			throw new Exception("Operator not found for operator id : " + fkOperatorId);
		}
		TxnHomePage txnHomePage = new TxnHomePage();
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		txnHomePage.setSessionId(fs.getUuid());
		txnHomePage.setLookupId(FCUtil.getLookupID());
		txnHomePage.setCircleName("ALL");
		txnHomePage.setOperatorName(operatorName);
		txnHomePage.setServiceNumber(mobileNumber);
		txnHomePage.setAmount((float) amount);
		txnHomePage.setProductType(RechargeConstants.POST_PAID_PRODUCT_TYPE);
		txnHomePage.setFkProductId(201);
		txnHomePage.setFkOperatorId(Integer.parseInt(fkOperatorId));
		txnHomePage.setCreatredOn(new Timestamp(System.currentTimeMillis()));
		BillTxnHomePage bthp = BillTxnHomePage.createForMobilePostpaid(mobileNumber, response, operatorName);
		txnHomePage.setBillTxnHomePage(bthp);
		Integer id = txnHomePageDAO.saveTransactionInfo(txnHomePage);
		txnHomePage.setId(id);

		// Saving in-txn-data
		HashMap<String, Object> transactionInfo = new HashMap<String, Object>();
		transactionInfo.put(InTxnDataConstants.RECHARGE_AMOUNT, amount);
		transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, txnHomePage.getCircleName());
		transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, operatorName);
		transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, Integer.parseInt(fkOperatorId));
		transactionInfo.put(InTxnDataConstants.PRODUCT_TYPE,
				FCConstants.productTypeToNameMap.get(RechargeConstants.POST_PAID_PRODUCT_TYPE));
		transactionInfo.put(InTxnDataConstants.SERVICE_NUMBER, mobileNumber);
		transactionInfo.put(InTxnDataConstants.SESSION_ID, fs.getUuid());
		try {
			campaignServiceClient.saveInTxnData1(txnHomePage.getLookupId(), FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID,
					transactionInfo);
		} catch (Exception e) {
			logger.error("Error saving inTxndata: " + fs.getUuid(), e);
		}
		return txnHomePage;
	}

	public void initiatePostpaidFulfillment(String orderID) {
		if (appConfig.isAsyncBillPaymentEnabled()) {
			kestrelWrapper.enqueuePostpaidFulfillment(orderID);
		} else {
			fulfillPostpaidSynchronously(orderID);
		}
	}

	public void initiateBillpayFulfillment(String orderID, boolean nodalFlag) {
		if (appConfig.isAsyncBillPaymentEnabled()) {
			kestrelWrapper.enqueueBillayFulfillment(orderID, nodalFlag);
		} else {
			fulfillBillpaymentSynchronously(orderID);
		}
	}

	public void fulfillPostpaidSynchronously(String orderID) {
		try {
			logger.info("fulfillPostpaidSynchronously ##### : "+orderID);

			txnFulFilmentService.updateTxnFulfilmentAndSaveRechargeHistory(orderID);
			paymentTransactionService.assertPaymentSuccessAndDebitWallet(orderID, FundDestination.RECHARGE,
					RechargeConstants.RECHARGE_TYPE);
			makeBillPayment(orderID, null);
			initiateBillPaymentFulfillmentProcess(orderID);
		} catch (DuplicateRequestException e) {
			logger.error(
					"Got duplicate entry exception while debiting from wallet; not going ahead with bill payment for order ID ["
							+ orderID + "], Exception " + ExceptionUtils.getFullStackTrace(e));
			metricsClient.recordEvent("Recharge", "Missed.postpaid", "DuplicateRequestException");
		} catch (Exception e) {
			logger.error("Got exception with bill payment for order ID [" + orderID + "], Exception "
					+ ExceptionUtils.getFullStackTrace(e));
			BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
			if (billPaymentStatus == null) {
				logger.info("No recharge attempt caught for orderId: " + orderID);
				transactionHistoryService.updateStatus(orderID, RechargeConstants.DUMMY_RECHARGE_FAILURE_CODE);
				kestrelWrapper.enqueueUPUpdate(orderID);
				metricsClient.recordEvent("Recharge", "Missed.postpaid", "Exception");
			}

		}
	}

	public Boolean fulfillPostpaidRetry(String orderID, InRechargeRetryMap inRechargeRetryMap) {
		Boolean billPaymentStatus = false;
		try {
			logger.info("inRechargeRetryMap ##### : "+inRechargeRetryMap);
			billPaymentStatus = makeBillPayment(orderID, inRechargeRetryMap);
			logger.info("billPaymentStatus ##### : "+billPaymentStatus);

			initiateBillPaymentFulfillmentProcess(orderID);
		} catch (Exception e) {
			logger.error("Got exception while bill payment for order ID [" + orderID + "], Exception "
					+ ExceptionUtils.getFullStackTrace(e));
		}
		return billPaymentStatus;
	}

	public void fulfillBillpaymentSynchronously(String orderID) {
		try {
			txnFulFilmentService.updateTxnFulfilmentAndSaveRechargeHistory(orderID);
			paymentTransactionService.assertPaymentSuccessAndDebitWallet(orderID, FundDestination.RECHARGE,
					RechargeConstants.RECHARGE_TYPE);
			String productType = orderService.getProductTypeForAllRechargeCase(orderID);
			String billId = orderIdDao.getBillIdForOrderId(orderID, productType);
			Users user = userServiceProxy.getUserFromOrderId(orderID);
			initBillPayment(orderID);

			PaymentTransaction paymentTransaction = paymentTransactionService
					.getSuccessfulPaymentTransactionByOrderId(orderID);
			Integer paymentType = paymentTransaction.getPaymentType();
			String paymentOption = paymentTransaction.getPaymentOption();

			FulfillBillResponse billResponse = null;
			// We are directly calling the DDB for fetching message details against
			// lookupId, idea is if no message detail is present this is not bbps biller
			// hence we can pass
			// metadata as null
			if (appConfig.isFulfillNewEnabled()) {
				logger.info("Calling FulfillBill New for " + orderID);
				Map<String, Object> metaDataMap = new HashMap();
				String lookupId = orderIdDao.getLookupIdForOrderId(orderID);
				BBPSTransactionDetails bbpsDetails = bbpsDetailsDao.getBBPSDetailsForLookupId(lookupId);
				if (bbpsDetails != null) {
					logger.info("Dynamo object" + bbpsDetails.toString());
				} else {
					logger.info("Received null from dynamo for lookupId " + lookupId);
				}
				String messageDetails = null;

				String billPeriod = null;
				if(bbpsDetails != null){
                    messageDetails = bbpsDetails.getMessageDetails() == null ? messageDetails : bbpsDetails.getMessageDetails();
                    billPeriod = bbpsDetails.getBillPeriod() == null ? billPeriod : bbpsDetails.getBillPeriod();
				}
				logger.info("LookupId: " + lookupId + " messageDetails: " + messageDetails);
				logger.info("Bill Period : "+billPeriod);
				metaDataMap.put("messageDetails", messageDetails);
				metaDataMap.put("orderId", orderID);
				metaDataMap.put("billPeriod", billPeriod);
				try {
					billResponse = billPayServiceRemote.fulfillBillNew(billId, user.getEmail(), user.getMobileNo(),
							paymentType, paymentOption, metaDataMap);
				} catch (Exception ex) {
					logger.error("Exception : " + ex + " For ORDER-ID " + orderID + " , Enquing for retry", ex);
					kestrelWrapper.enqueueForFFNotificationRetry(orderID);
					throw ex;
				}

				if (billResponse.getMetaData() != null) {
					Map<String, String> metaData = billResponse.getMetaData();

					if (bbpsDetails != null && bbpsDetails.getIsBbpsActive()) {
						logger.info("BBPSReferenceNumber: " + metaData.get("BBPSReferenceNumber"));
						bbpsDetails.setBbpsReferenceNumber(metaData.get("BBPSReferenceNumber"));
						logger.info("Ag txn ID: " + metaData.get("aggregatorTxnId"));
						bbpsDetails.setAggregatorTxnId(metaData.get("aggregatorTxnId"));
						bbpsDetails.setLookupId(lookupId);
						bbpsDetails.setIsBbpsActive(true);
						bbpsDetailsDao.updateBBPSDetails(bbpsDetails);

					} else {
						logger.info("Since there was no data in ddb for lookupId: " + lookupId
								+ " it is not bbps call since messageDeails field, mandatory for BBPS txn, was not present ");
					}

				} else {
					logger.info("BillResponse meta data is null");
				}
				logger.info("Called FulfillBill New for " + orderID);
			} else {
				// Map<String, Object> metaDataMap = new HashMap();
				billResponse = billPayServiceRemote.fulfillBill(billId, user.getEmail(), user.getMobileNo());
			}
			BillWithSubscriberNumber bill = billPayServiceRemote.getBillDetails(billId);
			BillPayValidator validator = billPayServiceRemote.getBillPayValidatorForOperator(bill.getOperatorId(),
					bill.getCircleId());
			if (billResponse != null && validator != null && billResponse.getResponseStatus() == Status.Success
					&& validator.getBillerType() == ADHOC_BILLER_TYPE) {
				CreateBillPayRequest createBillPayRequest = createBillPayRequest(bill);
				billPayServiceRemote.addBiller(createBillPayRequest, user.getUserId(), user.getEmail(),
						user.getMobileNo());
			}
			doFullfilment(orderID);
		} catch (DuplicateRequestException e) {
			logger.error(
					"Got duplicate entry exception while debiting from wallet; not going ahead with bill payment for order ID ["
							+ orderID + "], Exception " + ExceptionUtils.getFullStackTrace(e));
			metricsClient.recordEvent("Recharge", "Missed.billpay", "DuplicateRequestException");
		} catch (IOException e) {
			logger.error("Got IOException while doing fulfillBill [" + orderID + "], Exception "
					+ ExceptionUtils.getFullStackTrace(e));
			metricsClient.recordEvent("Recharge", "Missed.billpay", "IOException");
		} catch (Exception e) {
			logger.error("Got Exception while doing fulfillBill [" + orderID + "], Exception "
					+ ExceptionUtils.getFullStackTrace(e));
			String productType = orderService.getProductTypeForAllRechargeCase(orderID);
			String billId = orderIdDao.getBillIdForOrderId(orderID, productType);
			BillTransaction billTransaction = billPayServiceRemote.getBillTransaction(billId);
			if (billTransaction == null) {
				logger.info("No Billpay attempt caught for orderId:" + orderID);
				transactionHistoryService.updateStatus(orderID, RechargeConstants.DUMMY_RECHARGE_FAILURE_CODE);
				kestrelWrapper.enqueueUPUpdate(orderID);
				metricsClient.recordEvent("Recharge", "Missed.billpay", "Exception");
			}

		}
	}

	public void excuteUPTxnclearing() {

		Calendar newDate = Calendar.getInstance();
		newDate.add(Calendar.DAY_OF_MONTH, -7);

		Date processingDate = newDate.getTime();
		String processingDateStr = new SimpleDateFormat("MM-dd-yyyy").format(processingDate);
		markUnknowTxnToFailure(processingDateStr);
	}

	public Boolean updateUnknownToSuccessTxn(String authenticator, String date) {
		BillpayUnknownStatus result = billpayUnknownStatusDao.getUnknownStatusTxn(authenticator, date);
		logger.info("billpayUnknown status data for auth: " + authenticator + " and date: " + date + " : "
				+ result.toString());
		try {
			FulfillBillResponse billResponse = billPayServiceRemote.updateUnknownBilldeskTxnSuccess(result.getBillId());
			if (billResponse.getResponseStatus().equals(Status.Success)) {
				billpayUnknownStatusDao.updateStatus(result.getBillId(), "success", false);
				doFullfilment(result.getOrderId());
				return true;
			} else {
				logger.error(
						"Something went wrong, received failure while updating unknow transaction for authenticator: "
								+ authenticator + " for date: " + date);
			}
		} catch (Exception e) {
			logger.error("Exception caught while updating unknown transaction for auth: " + authenticator
					+ " for date: " + date, e);
			return false;

		}
		return false;
	}

	public Boolean markUnknowTxnToFailure(String date) {
		List<BillpayUnknownStatus> result = billpayUnknownStatusDao.getAllUnknownStatusTxn(date);
		if (result != null && result.size() > 0) {
			for (BillpayUnknownStatus entry : result) {
				try {
					FulfillBillResponse billResponse = billPayServiceRemote
							.updateUnknownBilldeskTxnFailure(entry.getBillId());
					if (billResponse.getResponseStatus().equals(Status.Failure)) {
						billpayUnknownStatusDao.updateStatus(entry.getBillId(), "failure", false);
						doFullfilment(entry.getOrderId());
					} else {
						logger.error(
								"Something went wrong, received failure while updating unknow transaction for authenticator: "
										+ entry.getAuthenticator() + " for date: " + date);
					}
				} catch (Exception e) {
					logger.error("Exception caught while updating txn as failure for billId: " + entry.getBillId(), e);
				}
			}
			return true;
		} else {
			logger.info("Couldn't find any entries for date: " + date);
		}
		return true;
	}

	private CreateBillPayRequest createBillPayRequest(BillWithSubscriberNumber bill) {
		CreateBillPayRequest createBillPayRequest = new CreateBillPayRequest();
		createBillPayRequest.setServiceProvider(bill.getOperatorId());
		createBillPayRequest.setServiceRegion(bill.getCircleId());
		createBillPayRequest.setAdditionalInfo1(bill.getAdditionalInfo1());
		createBillPayRequest.setAdditionalInfo2(bill.getAdditionalInfo2());
		createBillPayRequest.setAdditionalInfo3(bill.getAdditionalInfo3());
		createBillPayRequest.setAdditionalInfo4(bill.getAdditionalInfo4());
		createBillPayRequest.setAdditionalInfo5(bill.getAdditionalInfo5());
		createBillPayRequest.setDueDate(bill.getDueDate());
		createBillPayRequest.setBillDate(bill.getBillDate());
		createBillPayRequest.setBillNumber(bill.getBillNumber());
		createBillPayRequest.setAgBillId(bill.getAgBillId());
		createBillPayRequest.setBillAmount(bill.getBillAmount());
		createBillPayRequest.setFcBillerId(bill.getFcBillerId());

		return createBillPayRequest;
	}

	private void initBillPayment(String orderID) {
		UserTransactionHistory userTransactionHistory = transactionHistoryService
				.findUserTransactionHistoryByOrderId(orderID);
		if (userTransactionHistory != null) {
			Map<String, Object> updatedUserDetail = new HashMap<String, Object>();
			updatedUserDetail.put("circleName", "");
			updatedUserDetail.put("operatorName", userTransactionHistory.getServiceProvider());
			updatedUserDetail.put("productType", userTransactionHistory.getProductType());
			transactionHistoryService.createOrUpdateRechargeHistory(orderID, updatedUserDetail,
					RechargeConstants.IN_UNDER_PROCESS_CODE);
		}
	}

	public Boolean reverseBillPayTransactionToFailure(String orderId, String productType) {
		RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
		rechargeAlertBean.setRechargeReversal(true);
		rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus());
		String billId = orderIdDao.getBillIdForOrderId(orderId, productType);
		if (billPayServiceRemote.reverseStatusToFailure(billId)) {
			doFullfilment(orderId);
			List<BillTransactionLedger> billDetails = billPayServiceRemote.getBillTransactionLedgers(billId);
			if (billDetails != null) {
				rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(billDetails.get(0).getAggregator()));
				try {
					ViewUserBillsResponse viewUserBillsResponse = billPayServiceRemote.getUserBiller(billId, null, null,
							null);
					logger.info(String.format("Fetched fcBillerId for billId %s : %s", billId,
							viewUserBillsResponse.getFcBillerId()));
					rechargeAlertBean.setFcBillerId(viewUserBillsResponse.getFcBillerId());
				} catch (Exception e) {
					logger.error(
							String.format("Unable to fetch fcBillerId for billId %s becasue of exception : ", billId),
							e);
				}
				rechargeAlertBean.setOperatorTxnId(billDetails.get(0).getAggrBillTransactionId());
			}
			rechargeAlertBean.setAgRequestId(billId);
			rechargeAlertBean.setOrderId(orderId);
			try {
				rechargeSNSService.publish(orderId,
						"{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
			} catch (IOException e) {
				logger.error("Failed to push reversal notification for orderId : " + rechargeAlertBean.getOrderId());
			}
			return true;
		}
		return false;
	}

	public void doFullfilment(String orderID) {
		fulfillmentService.doFulfillment(orderID, true, false, false);
	}

	@Transactional
	public Boolean makeBillPayment(String orderID, InRechargeRetryMap inRechargeRetryMap) {
		BillPostpaidMerchant billPostpaidMerchant = null;
		/*
		 * -------------------------------------- All the stuff below, in a transaction:
		 * -------------------------------------- 1. read TxnHomePage based on orderID
		 * 2. insert a row into bill_payment_request 3. make request to BillDesk to make
		 * the payment
		 */
		BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
		PaymentTransaction paymentTransaction = paymentTransactionService
				.getSuccessfulPaymentTransactionByOrderId(orderID);
		Integer paymentType = paymentTransaction.getPaymentType();
		String paymentOption = paymentTransaction.getPaymentOption();
		logger.info("Inside bill payment service method make Bill Payment. The paymentType is : [" + paymentType
				+ "] and the payment option is : [" + paymentOption + "] for Order Id : [" + orderID + "]");

		// fetch TxnHomePage by order-ID
		TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderID);

		/* below code added to change the thp in case of postpaid retry */
		if (inRechargeRetryMap != null && inRechargeRetryMap.getIsRetryable()) {
			logger.info("Attmepting PostPaid Retry");
			logger.info("Thp being set by inRechargeRetryMap for orderId:" + orderID + " is " + "RetryOperator:"
					+ inRechargeRetryMap.getRetryOperatorName() + " RetryOperatorMasterId:"
					+ inRechargeRetryMap.getFkOperatorMasterId() + " RetryProductId:"
					+ inRechargeRetryMap.getRetryProductId());

			thp.setCircleName("ALL");
			thp.setProductType(RechargeConstants.POST_PAID_PRODUCT_TYPE);
			thp.setOperatorName(inRechargeRetryMap.getRetryOperatorName());
			thp.setFkOperatorId(Integer.parseInt(inRechargeRetryMap.getFkRetryOperatorMasterId()));
			thp.setFkProductId(Integer.parseInt(inRechargeRetryMap.getRetryProductId()));

			BillPaymentCommonValidationApiResponse response = billPostpaidValidation.validate(
					String.valueOf(thp.getFkOperatorId()), thp.getServiceNumber(), null,
					new BigDecimal(thp.getAmount()));
			logger.info("validation response" + response);
			BillTxnHomePage bthp = BillTxnHomePage.createForMobilePostpaid(thp.getServiceNumber(), response,
					thp.getOperatorName());
			thp.setBillTxnHomePage(bthp);

		} else {
			/*
			 * Insert/Update user_transaction_history with "08"
			 */
			Map<String, Object> userDetail = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderID);
			transactionHistoryService.createOrUpdateRechargeHistory(orderID, userDetail,
					RechargeConstants.IN_UNDER_PROCESS_CODE);
		}
		// Boolean isBBPSEnabled = true;
		// if(isBBPSEnabled) {
		// BillPaymentCommonValidationApiResponse response =
		// billPostpaidValidation.validate(
		// String.valueOf(thp.getFkOperatorId()), thp.getServiceNumber(), null,
		// new BigDecimal(thp.getAmount()));
		// logger.info("validation response" + response.toString());
		// }
		StringTokenizer aggrprioritytokens = getBillPaymentAggregators(Integer.toString(thp.getFkOperatorId()));
		logger.info("Bill payment gateway type for order id " + orderID + " is " + aggrprioritytokens.toString());

		billPostpaidMerchant = this.getPostpaidMerchantByOperatorId(thp.getFkOperatorId());
		// try to make payment

		BillPaymentCommonApiResponse response = null;

		int trialNumber = 0;
		if (billPaymentStatus != null) {
			trialNumber = billPaymentStatus.getRetryNumber() + 1;
		}

		while (aggrprioritytokens.hasMoreElements()) {

			String billPaymentGateway = aggrprioritytokens.nextElement().toString();

			if (fcProperties.shouldMockBillPaymentAG()) {
				logger.info("[MOCK] For orderid: " + orderID + ", operator priority was: " + billPaymentGateway
						+ " - changing to: dummy_" + billPaymentGateway);
				billPaymentGateway = "dummy_" + billPaymentGateway;
			}
			logger.info("BillPayment Gateway used is " + billPaymentGateway);
			IBillPaymentInterface billPaymentInterface = billPaymentFactory.getBillPaymentGateway(billPaymentGateway);
			String aggregatorTransactionId = fcProperties.getInTransIdPrefix() + trialNumber + "_" + orderID;
			notifyRechargeAttempt(RechargeUtil.getAggregatorName(billPaymentGateway), aggregatorTransactionId, orderID,
					thp);
			try {
				response = billPaymentInterface.submitPaymentRequest(aggregatorTransactionId, thp, billPostpaidMerchant,
						paymentType, paymentOption);
				logger.info("response ##### : "+response);

				// if(isBBPSEnabled) {
				// logger.info("Now going to insert bbps referneceNumber for orderId: " +
				// orderID);
				// insertBBPSReferenceNumber(response.getRowResponse(),orderID);
				// }
				Boolean retryStatus = handlePostpaidResponse(response, orderID, trialNumber, thp);
				logger.info("retryStatus ###### : "+retryStatus);
				if (!retryStatus) {
					break;
				}
			} catch (Exception exception) {
				logger.info("exception ###### : "+exception);

				throw new BilldeskException(String.format("Unable to submit payment request (orderID=%s) to Billdesk",
						aggregatorTransactionId), exception);
			}
			trialNumber++;
			logger.info("trialNumber ###### : "+trialNumber);
		}

		insertRechargeRetryData(response, inRechargeRetryMap, orderID);
		updateUserTxnHistoryForSuccessfulRetry(response, inRechargeRetryMap, orderID);
		logger.info("response ##### : "+response);
		return response.getSuccess();
	}

	// @Transactional
	// private void insertBBPSReferenceNumber(String agResponse, String orderId) {
	// try {
	// String[] splittedParams = StringUtils.splitPreserveAllTokens(agResponse,
	// MESSAGE_PARAM_TILDE_SEPARATOR);
	// String bbpsReferenceNumber = splittedParams[14];
	// logger.info("[AnantQA]BBPS ref number for orderId: " + orderId + " is " +
	// bbpsReferenceNumber);
	// UserTransactionHistory uthistory =
	// userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
	// if(uthistory != null) {
	// logger.info("[AnantQA]uth is not null printing metaData: " +
	// uthistory.getMetadata() + " ads: " + uthistory.getServiceProvider());
	// }
	// long result =
	// userTransactionHistoryService.insertMetadata(bbpsReferenceNumber, orderId);
	// logger.info("[AnantQA]Result from dao call: " + result);
	// } catch (Exception e) {
	// logger.error("Exception caught while inserting metaData for orderId: " +
	// orderId, e);
	// }
	// }

	
	public UpdateTransactionStatusResponse backfillSuccessBilldeskPostpaid(String orderID) {
		UpdateTransactionStatusResponse updateResponse = new UpdateTransactionStatusResponse();
		BillPaymentCommonApiResponse response = null; //Created Dummy success response
		TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderID);
		BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
		Integer trialNumber = billPaymentStatus.getRetryNumber() +1;
		handlePostpaidResponse(response, orderID, trialNumber, thp);
		insertRechargeRetryData(response, /*inRechargeRetryMap */ null, orderID);
		updateResponse.setOrderId(orderID);
		updateResponse.setSuccessfulUpdate(true);
		return updateResponse;
		//Then start fulfillment
	}
	
	
	public UpdateTransactionStatusResponse backfillFailureBilldeskPostpaid(String orderID) {
		UpdateTransactionStatusResponse updateResponse = new UpdateTransactionStatusResponse();
		BillPaymentCommonApiResponse response = null; //Created Dummy failure response
		TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderID);
		BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
		Integer trialNumber = billPaymentStatus.getRetryNumber() +1;
		handlePostpaidResponse(response, orderID, trialNumber, thp);
		insertRechargeRetryData(response, /*inRechargeRetryMap */ null, orderID);
		updateResponse.setOrderId(orderID);
		updateResponse.setSuccessfulUpdate(true);
		return updateResponse;
		//Then start fulfillment
	}
	
	
	private void notifyRechargeAttempt(String billPaymentGateway, String aggregatorTransactionId, String orderID,
			TxnHomePage thp) {
		try {
			RechargeAttemptBean rechargeAttemptBean = getRechargeAttemptBean(billPaymentGateway,
					aggregatorTransactionId, orderID, thp);
			rechargeAttemptSNSService.publish(orderID,
					"{\"message\" : " + RechargeAttemptBean.getJsonString(rechargeAttemptBean) + "}");
		} catch (Exception e) {
			logger.error("Caught some exception pushing recharge attempt notification for orderId " + orderID, e);
		}
	}

	private RechargeAttemptBean getRechargeAttemptBean(String billPaymentGateway, String aggregatorTransactionId,
			String orderID, TxnHomePage thp) {
		RechargeAttemptBean rechargeAttemptBean = new RechargeAttemptBean();
		rechargeAttemptBean.setAg(billPaymentGateway);
		rechargeAttemptBean.setAgRequestId(aggregatorTransactionId);
		rechargeAttemptBean.setAmount(thp.getAmount());
		rechargeAttemptBean.setOrderId(orderID);
		rechargeAttemptBean.setProduct(ProductName.MobilePostpaid.getLabel());
		rechargeAttemptBean.setServiceNumber(thp.getServiceNumber());
		return rechargeAttemptBean;
	}

	public StringTokenizer getBillPaymentAggregators(String operatorId) {
		List<BillPaymentGatewayPriority> billPaymentGatewayPriorityList = billReadDAO
				.findBillPaymentGatewayByOperatorId(operatorId);

		String billPymentGateway = billPaymentGatewayPriorityList == null ? null
				: billPaymentGatewayPriorityList.get(0).getBillProvider();

		if (billPymentGateway == null) {
			logger.info(" Bill payment gateway response is null ");
			billPymentGateway = PaymentConstants.BILL_PAPYMENT_DEFAULT_GATEWAY;
		}
		logger.info("Billpayment Gateway:" + billPymentGateway);
		return new StringTokenizer(billPymentGateway, ",");
	}

	private void insertRechargeRetryData(BillPaymentCommonApiResponse response, InRechargeRetryMap inRechargeRetryMap,
			String orderId) {
		RechargeRetryData rechargeRetryData = new RechargeRetryData();
		if (response != null && inRechargeRetryMap != null) {
			String fcResponseCode;
			if (response.getSuccess() == null) {
				fcResponseCode = null;
			} else if (response.getSuccess()) {
				fcResponseCode = RechargeConstants.SUCCESS_RESPONSE_CODE;
			} else {
				fcResponseCode = RechargeConstants.GENERIC_BILL_PAYMENT_FAILURE_CODE;
			}
			rechargeRetryData.setOrderId(orderId);
			rechargeRetryData.setProductType(FCConstants.PRODUCT_TYPE_POSTPAID);
			rechargeRetryData.setRetryOperatorName(inRechargeRetryMap.getRetryOperatorName());
			rechargeRetryData.setFinalRetry(response.getSuccess() == null ? false : response.getSuccess());
			rechargeRetryData.setAggrName(response.getBillPaymentGateway());
			rechargeRetryData.setAggrRespCode(response.getBillPaymentStatusCode());
			rechargeRetryData.setAggrRespMsg(response.getRowResponse());
			rechargeRetryData.setAggrTransId(response.getGatewayReferenceNumber());
			rechargeRetryData.setOprTransId("");
			rechargeRetryData.setInRespCode(fcResponseCode);
			rechargeRetryData.setInRespMsg("");
			inService.insertRechargeRetryData(rechargeRetryData);
		}
	}

	/*
	 * Update operator and circle in user_transaction_history only if the retry is
	 * success.
	 */
	private void updateUserTxnHistoryForSuccessfulRetry(BillPaymentCommonApiResponse response,
			InRechargeRetryMap inRechargeRetryMap, String orderID) {
		if (response != null && inRechargeRetryMap != null) {
			// logger.info("raw response : " + response.getRowResponse());
			// String[] splittedParams =
			// StringUtils.splitPreserveAllTokens(response.getRowResponse(), "~");
			// String bbpsReferenceNumber = null;
			// if(splittedParams.length >20 && splittedParams[21] != null) {
			// bbpsReferenceNumber = splittedParams[20];
			// }

			Map<String, Object> updatedUserDetail = new HashMap<String, Object>();
			updatedUserDetail.put("circleName", "ALL");
			updatedUserDetail.put("operatorName", inRechargeRetryMap.getRetryOperatorName());
			updatedUserDetail.put("productType", FCConstants.PRODUCT_TYPE_POSTPAID);
			// updatedUserDetail.put("BBPSReferenceNumber", bbpsReferenceNumber);
			if (response.getSuccess() != null && response.getSuccess()) {
				transactionHistoryService.createOrUpdateRechargeHistory(orderID, updatedUserDetail,
						RechargeConstants.IN_UNDER_PROCESS_CODE);
				metricsClient.recordEvent("Recharge", "OperatorRetry.Success",
						inRechargeRetryMap.getFkOperatorMasterId() + "-"
								+ inRechargeRetryMap.getFkRetryOperatorMasterId());
			} else {
				metricsClient.recordEvent("Recharge", "OperatorRetry.Failure",
						inRechargeRetryMap.getFkOperatorMasterId() + "-"
								+ inRechargeRetryMap.getFkRetryOperatorMasterId());
			}
		}
	}

	public IBillPaymentRequest createBillPaymentRequest(String orderID, String productType, BillTxnHomePage bthp,
			TxnHomePage thp) {
		if (FCUtil.isEmpty(productType)) {
			throw new IllegalArgumentException(
					"Expected a valid bill-payment product type, but found " + FCUtil.stringify(productType));
		}
		switch (productType) {
		case "M":
			return mobilePostpaidService.createPaymentRequest(bthp, orderID, thp);

		default:
			throw new IllegalArgumentException("Unsupported productType " + FCUtil.stringify(productType));
		}
	}

	private void initiateBillPaymentFulfillmentProcess(String OrderId) {
		fulfillmentService.doFulfillment(OrderId, true, false, false);

	}

	public BillPaymentStatus findBillPaymentStatusByOrderId(String orderId) {
		BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderId);
		return billPaymentStatus;
	}

	public List<BillPaymentStatus> getAllBillPaymentsForEmail(String email) {
		List<BillPaymentStatus> billPayments = billReadDAO.getAllBillPaymentsForEmail(email);
		return billPayments;
	}

	public List<BillPaymentStatus> getAllBillPaymentsForEmailAndDate(String email, Timestamp fromDate,
			Timestamp toDate) {
		List<BillPaymentStatus> billPayments = billReadDAO.getAllBillPaymentsForEmailAndDate(email, fromDate, toDate);
		return billPayments;
	}

	/*
	 * private BillPaymentErrorCodeMap saveBillPaymentResponse(String orderID,
	 * BillPaymentCommonApiResponse response, int trialNumber,TxnHomePage thp) {
	 * 
	 * BillPaymentErrorCodeMap billPaymentErrorCodeMap = null;
	 * 
	 * if (response != null && response.getBillPaymentStatusCode() != null &&
	 * !response.getBillPaymentStatusCode().isEmpty()) { billPaymentErrorCodeMap =
	 * getBillPaymentInternalResponseCode(response.getBillPaymentStatusCode(),
	 * response.getBillPaymentGateway()); }
	 * logger.info("billPaymentErrorCodeMap ### : "+billPaymentErrorCodeMap); if
	 * (billPaymentErrorCodeMap == null ||
	 * StringUtils.isEmpty(billPaymentErrorCodeMap.
	 * getBillPaymentInternalResponseCode())) { response.isSuccess(null); } else if
	 * (billPaymentErrorCodeMap != null &&
	 * billPaymentErrorCodeMap.getBillPaymentInternalResponseCode()
	 * .equals(RechargeConstants.IN_UNDER_PROCESS_CODE)) { response.isSuccess(null);
	 * } else if (billPaymentErrorCodeMap != null &&
	 * billPaymentErrorCodeMap.getBillPaymentInternalResponseCode().equals("1")) {
	 * response.isSuccess(true); } else if (billPaymentErrorCodeMap != null &&
	 * billPaymentErrorCodeMap.getBillPaymentInternalResponseCode().equals("0")) {
	 * response.isSuccess(false); }
	 * 
	 * BillPaymentStatus status = BillPaymentStatus.createForFreecharge(thp,
	 * response.getRequestparam(), response, orderID);
	 * 
	 * long statusID;
	 * 
	 * if (trialNumber == 0) { statusID =
	 * billWriteDAO.createBillPaymentStatus(status); } else {
	 * billWriteDAO.updateBillPaymentStatus(status, trialNumber); BillPaymentStatus
	 * billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
	 * statusID = billPaymentStatus.getId(); }
	 * 
	 * status.setId(statusID); BillPaymentAttempt attempt =
	 * BillPaymentAttempt.create(status, response);
	 * billWriteDAO.insertBillPaymentAttempt(attempt); return
	 * billPaymentErrorCodeMap; }
	 */
	private BillPaymentErrorCodeMap saveBillPaymentResponse(String orderID, BillPaymentCommonApiResponse response,
			int trialNumber, TxnHomePage thp) {
		BillPaymentErrorCodeMap billPaymentErrorCodeMap = null;

		// TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderID);

		if (response != null && response.getBillPaymentStatusCode() != null
				&& !response.getBillPaymentStatusCode().isEmpty()) {
			billPaymentErrorCodeMap = getBillPaymentInternalResponseCode(response.getBillPaymentStatusCode(),
					response.getBillPaymentGateway());
		}
		logger.info("billPaymentErrorCodeMap ### : " + billPaymentErrorCodeMap);

		/*
		 * If internal error code is '08' mark the transaction as pending
		 */
		if (billPaymentErrorCodeMap != null && billPaymentErrorCodeMap.getBillPaymentInternalResponseCode()
				.equals(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
			response.isSuccess(null);
		}

		if (billPaymentErrorCodeMap != null
				&& billPaymentErrorCodeMap.getBillPaymentInternalResponseCode().equals("1")) {
			response.isSuccess(true);
		}
		if (billPaymentErrorCodeMap != null
				&& billPaymentErrorCodeMap.getBillPaymentInternalResponseCode().equals("0")) {
			response.isSuccess(false);
		}
		BillPaymentStatus status = BillPaymentStatus.createForFreecharge(thp, response.getRequestparam(), response,
				orderID);

		long statusID;

		if (trialNumber == 0) {
			statusID = billWriteDAO.createBillPaymentStatus(status);
		} else {
			billWriteDAO.updateBillPaymentStatus(status, trialNumber);
			BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
			statusID = billPaymentStatus.getId();
		}

		status.setId(statusID);
		BillPaymentAttempt attempt = BillPaymentAttempt.create(status, response);
		billWriteDAO.insertBillPaymentAttempt(attempt);
		logger.info("billPaymentErrorCodeMap ### : " + billPaymentErrorCodeMap);

		return billPaymentErrorCodeMap;
	}

	private String getOperatorName(String operatorId) {
		try {

			OperatorMaster operatorMaster = operatorCircleService.getOperator(Integer.parseInt(operatorId));
			if (operatorMaster != null) {
				return operatorMaster.getName();
			}
		} catch (Exception exception) {
			logger.error(
					"exception raised while fetching operator name using operator master id in bill payment service ",
					exception);

		}
		return null;
	}

	public BillPaymentAttempt findBillPaymentAttemptByStatusId(long statusId) {
		return billReadDAO.findBillPaymentAttemptByStatusId(statusId);
	}

	public List<BillPaymentAttempt> findBillPaymentAttemptsByStatusId(long statusId) {
		return billReadDAO.findBillPaymentAttemptsByStatusId(statusId);
	}

	public TransactionStatus getBillPaymentStatus(UserDetails userDetailsObj, String orderId) {
		logger.info("create transaction status for bill payment for order id " + orderId);
		TransactionStatus transactionStatus = new TransactionStatus();
		BillPaymentStatus billPaymentStatus = findBillPaymentStatusByOrderId(orderId);
		if (billPaymentStatus == null) {
			logger.info("No billPaymnet status found for orderId:" + orderId);
			return null;
		}
		Boolean billPaymentstatus = billPaymentStatus.getSuccess();
		String responseCode = billPaymentstatus == null ? RechargeConstants.IN_UNDER_PROCESS_CODE
				: (billPaymentstatus == true ? "00" : "55");
		transactionStatus.setTransactionStatus(responseCode);
		transactionStatus.setSubscriberIdentificationNumber(userDetailsObj.getUserMobileNo());
		transactionStatus.setOperator(userDetailsObj.getOperatorName());

		/*
		 * commenting below code below we can get data from userDetailsObj instead of
		 * hitting 2 queries
		 * 
		 * String lookupId = orderIdSlaveDAO.getLookupIdForOrderId(orderId);
		 * List<TxnHomePage> txnHomePage =
		 * txnHomePageService.findTxnHomePageByLookupId(lookupId);
		 * 
		 * String subscriberIdentificationNumber =
		 * txnHomePage.get(0).getBillTxnHomePage().getPostpaidNumber();
		 * transactionStatus.setSubscriberIdentificationNumber(
		 * subscriberIdentificationNumber);
		 * transactionStatus.setOperator(txnHomePage.get(0).getBillTxnHomePage().
		 * getPostpaidMerchantName());
		 */

		return transactionStatus;
	}

	private Boolean handlePostpaidResponse(BillPaymentCommonApiResponse response, String orderID, int trialNumber,
			TxnHomePage thp) {
		BillPaymentErrorCodeMap billPaymentErrorCodeMap = saveBillPaymentResponse(orderID, response, trialNumber, thp);
		logger.info("billPaymentErrorCodeMap ###### : "+billPaymentErrorCodeMap);

		if (billPaymentErrorCodeMap != null && billPaymentErrorCodeMap.getIsRetryable()) {
			return true;
		} else {
			if ((response.getBillPaymentGateway() != null)
					&& response.getBillPaymentGateway().equals(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY)
					&& response.getAuthDetail() != null && response.getAuthDetail().equals("BDK_EXCEPTION")) {
				return true;
			}
			return false;
		}
	}

	public BillPostpaidMerchant getPostpaidMerchantByOperatorId(Integer operatorId) {
		try {
			BillPostpaidMerchant billPostpaidMerchant = billPaymentCacheManager
					.getPostpaidMerchantByOperatorId(operatorId);
			if (billPostpaidMerchant != null) {
				return billPostpaidMerchant;
			} else {
				billPaymentCacheManager.setPostpaidMerchants(billReadDAO.findAllPostpaidMerchants());
			}
		} catch (Exception e) {
			logger.error("error in getPostpaidAggrPriority of billpaymentsevice: " + operatorId);
		}
		return billReadDAO.findMerchantObjectByOperatorID(operatorId);
	}

	public BillPaymentGatewayPriority getPostpaidAggrPriority(Integer circleOperatorMappingId) {
		try {
			BillPaymentGatewayPriority billPaymentPriority = billPaymentCacheManager
					.getPostpaidAggPriority(circleOperatorMappingId);
			if (billPaymentPriority != null) {
				return billPaymentPriority;
			} else {
				billPaymentCacheManager.setPostpaidAggPriority(billReadDAO.findBILLPaymentGateway());
			}
		} catch (Exception e) {
			logger.error("error in getPostpaidAggrPriority of billpaymentsevice: " + circleOperatorMappingId);
		}
		return billPaymentCacheManager.getPostpaidAggPriority(circleOperatorMappingId);
	}

	public Boolean updatePostpaidPaymentStatus(final String orderId, final String finalStatus,
			final String givenAggregatorName, String aggrRef) {
		RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
		try {
			BillPaymentStatus billPaymentStatus = billWriteDAO
					.findBillPaymentStatusByOrderId(orderId.substring(orderId.indexOf("_") + 1));

			String emailId = userServiceProxy.getEmailIdFromOrderId(orderId);

			if (billPaymentStatus == null) {
				return false;
			}

			Boolean initialBPS = billPaymentStatus.getSuccess();

			/*
			 * This will handle oxigen-to-euronet retry transactions. We will check whether
			 * given aggregatorName(by cs guy) matches aggregator name of final inResponse
			 * object of transaction.
			 */
			if (!FCUtil.isEmpty(givenAggregatorName)) {
				if ("euronet".equalsIgnoreCase(givenAggregatorName)) {
					if (!("BillPaymentEuronetClient".equalsIgnoreCase(billPaymentStatus.getBillPaymentGateway()))) {
						return false;
					}
				} else if ("oxigen".equalsIgnoreCase(givenAggregatorName)) {
					if (!("billPaymentOxigenClient".equalsIgnoreCase(billPaymentStatus.getBillPaymentGateway()))) {
						return false;
					}
				}
			}

			if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)
					&& StringUtils.isEmpty(aggrRef)) {
				logger.error("AGtrans id is needed to move a status to success ");
				return false;
			}

			if (billPaymentStatus.getSuccess() != null) {
				// old status - failed , new status - success
				// We do not allow failure to success as failed orders are already refunded to
				// customer.
				// This amount should be recovered from AG during recon hence we do not update.
				if (!billPaymentStatus.getSuccess()
						&& finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
					logger.error("Cannot reverse status from fail to success for order id :"
							+ billPaymentStatus.getAffiliateTransID());
					return false;
				}
				// old status - success , new status - success , no need to update
				if (billPaymentStatus.getSuccess()
						&& finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
					if (!StringUtils.isEmpty(aggrRef)
							&& (StringUtils.isEmpty(billPaymentStatus.getGatewayReferenceNumber())
									|| "unknown".equalsIgnoreCase(billPaymentStatus.getGatewayReferenceNumber()))) {
						billPaymentStatus.setGatewayReferenceNumber(aggrRef);
						billWriteDAO.updateBillPaymentStatus(billPaymentStatus, billPaymentStatus.getRetryNumber());
						return true;
					}

					return false;
				}
				// old status - failure , new status - failure , no need to update
				if (!billPaymentStatus.getSuccess()
						&& (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_FAILED) || finalStatus
								.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_UP_FORCE_FAILED))) {
					return false;
				}
			}

			if (!StringUtils.isEmpty(aggrRef) && (StringUtils.isEmpty(billPaymentStatus.getGatewayReferenceNumber())
					|| "unknown".equalsIgnoreCase(billPaymentStatus.getGatewayReferenceNumber()))) {
				billPaymentStatus.setGatewayReferenceNumber(aggrRef);
			}

			if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
				logger.info("Postpaid bill payment is already " + billPaymentStatus.getSuccess()
						+ " and the reversal request is to make it " + finalStatus + "orderID:" + orderId);
				rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus());
				billPaymentStatus
						.setSuccess(finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS));
			}

			if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_FAILED)
					|| finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_UP_FORCE_FAILED)) {
				logger.info("Postpaid bill payment is already " + billPaymentStatus.getSuccess()
						+ " and the reversal request is to make it " + finalStatus + "orderID:" + orderId);
				billPaymentStatus.setSuccess(false);
				rechargeAlertBean.setRechargeReversal(true);
				rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus());
			}

			if (initialBPS != null && initialBPS) {
				billPaymentStatus.setAuthStatus(RechargeConstants.POSTPAID_SUCCESS_REVERSAL_AUTH_STATUS);
			} else {
				billPaymentStatus.setAuthStatus(
						finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_UP_FORCE_FAILED)
								? RechargeConstants.POSTPAID_UP_FORCE_REVERSAL_AUTH_STATUS
								: RechargeConstants.POSTPAID_REVERSAL_AUTH_STATUS);
			}
			billPaymentStatus.setAuthDetail(RechargeConstants.POSTPAID_ADMIN_STATUS_UPDATE_AUTH_DETAIL);
			billPaymentStatus.setUpdatedAt(new Timestamp(new Date().getTime()));
			if (billPaymentStatus.getSuccess() != null) {
				if (billPaymentStatus.getSuccess()) {
					Map<String, Object> updatedUserDetail = new HashMap<String, Object>();
					updatedUserDetail.put("circleName", "");
					updatedUserDetail.put("operatorName", billPaymentStatus.getOperatorName());
					updatedUserDetail.put("productType", ProductMaster.ProductName.MobilePostpaid.getProductType());
					userTransactionHistoryService.createOrUpdateRechargeHistory(billPaymentStatus.getAffiliateTransID(),
							updatedUserDetail, RechargeConstants.IN_UNDER_PROCESS_CODE);
				}
			}
			billWriteDAO.updateBillPaymentStatus(billPaymentStatus, billPaymentStatus.getRetryNumber());
			this.initiateBillPaymentFulfillmentProcess(orderId);

			rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(billPaymentStatus.getBillPaymentGateway()));
			rechargeAlertBean.setAgRequestId(
					fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderId);
			rechargeAlertBean.setEmailId(emailId);
			rechargeAlertBean.setOrderId(orderId);
			rechargeAlertBean.setOperatorTxnId(billPaymentStatus.getGatewayReferenceNumber());

			try {
				rechargeSNSService.publish(orderId,
						"{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
			} catch (IOException e) {
				logger.error("Failed to push reversal notification for orderId : " + rechargeAlertBean.getOrderId());
			}

			logger.info("Postpaid admin reversal: updated bill payment status and enqueued for fulfillment for orderID "
					+ orderId);
			return true;
		} catch (Exception e) {
			logger.error(" Error while doing the reversal for postpaid for orderID: " + orderId, e);
		}
		return false;
	}

	@Transactional
	public BillPaymentErrorCodeMap getBillPaymentInternalResponseCode(String billPaymentGatewayResponseCode,
			String billpaymentGateway) {
		try {
			List<BillPaymentErrorCodeMap> billPaymentErrorCodeMaps = billPaymentCacheManager
					.getBillPaymentErrorCodeMap(billPaymentGatewayResponseCode, billpaymentGateway);
			if (billPaymentErrorCodeMaps != null && billPaymentErrorCodeMaps.size() > 0) {
				return billPaymentErrorCodeMaps.get(0);
			} else {
				billPaymentCacheManager
						.setBillPaymentErrorCodeMap(billReadDAO.findAllBillPaymentInternalResponseCodeMap());
			}
		} catch (Exception e) {
			logger.error("error while getting the data from billPaymentCacheManager: ", e);
		}
		return billReadDAO.findBillPaymentInternalResponseCode(billPaymentGatewayResponseCode, billpaymentGateway);
	}

	public boolean validatePostpaidRetry(String orderId) {
		BillPaymentStatus billPaymentStatus = findBillPaymentStatusByOrderId(orderId);

		if (billPaymentStatus == null) {
			logger.info("First postpaid attempt. Going ahead with postpaid recharge for " + orderId);
			return true;
		}

		List<BillPaymentAttempt> billPaymentAttempts = findBillPaymentAttemptsByStatusId(billPaymentStatus.getId());

		for (BillPaymentAttempt billPaymentAttempt : billPaymentAttempts) {
			Boolean status = billPaymentAttempt.getSuccess();
			if (status == null || status == true) {
				logger.error("Already a postpaid attempt is in progress for " + orderId + ", Skipping this one.");
				return false;
			}
		}
		return true;
	}

	public boolean validateBillPayRetry(String orderId) {
		String productType = orderService.getProductTypeForAllRechargeCase(orderId);
		String billId = orderIdDao.getBillIdForOrderId(orderId, productType);
		List<BillTransactionLedger> billDetails = billPayServiceRemote.getBillTransactionLedgers(billId);

		if (billDetails == null || billDetails.isEmpty()) {
			logger.info("First attempt of utility payment for orderID " + orderId);
			return true;
		}
		for (BillTransactionLedger billTransactionLedger : billDetails) {
			if (billTransactionLedger.getTransactionStatus() == null
					|| !inService.isFailure(billTransactionLedger.getTransactionStatus())) {
				logger.info("Already this order is in progress with status "
						+ billTransactionLedger.getTransactionStatus());
				return false;
			}
		}
		return true;
	}

	public boolean isFetchBillOperator(String operatorId) {
		List<String> eventList = rechargeCustomConfigUtil.getEventListForKey(FETCH_BILL_OPERATORS);
		if (eventList != null && eventList.contains(operatorId)) {
			return true;
		}
		return false;
	}

	public boolean toFetchBillFromEuronet(String operatorId) {
		List<String> eventList = rechargeCustomConfigUtil.getEventListForKey(EURONET_FETCH_BILL_OPERATOR);
		if (eventList != null && eventList.contains(operatorId)) {
			return true;
		}
		return false;
	}

	public boolean toFetchBillFromBilldesk(String operatorId) {
		List<String> eventList = rechargeCustomConfigUtil.getEventListForKey(BDK_FETCH_BILL_OPERATOR);
		if (eventList != null && eventList.contains(operatorId)) {
			return true;
		}
		return false;
	}

}
