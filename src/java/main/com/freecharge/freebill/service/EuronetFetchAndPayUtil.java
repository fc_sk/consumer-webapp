package com.freecharge.freebill.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.common.BillPaymentVerificationAPIResponse;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;
import com.freecharge.recharge.services.EuronetService;
import com.google.api.client.repackaged.com.google.common.base.Strings;

@Service
public class EuronetFetchAndPayUtil {
	
	@Autowired
	private EuronetService euronetService;
	
	protected final Logger logger = LoggingFactory.getLogger(getClass().getName());
	
	public FetchAndPayResult getEuroFetchbillResult(String mobileNumber, Integer operatorId) {
		FetchAndPayResult fpr = new FetchAndPayResult();
		List<HashMap<String,String>> infoList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> data = new HashMap<>();
		BillPaymentVerificationAPIResponse billpayVerificationResponse = euronetService
				.verificationForOperator("FETCH_AND_PAY_CALL", mobileNumber, operatorId);
		logger.info("billpayVerificationResponse:"+billpayVerificationResponse.getCustomerName());
		if (!Strings.isNullOrEmpty(billpayVerificationResponse.getBillAmount())) {
			fpr.setBillAmount(Float.parseFloat(billpayVerificationResponse.getBillAmount()));
		} else {
			fpr.setBillAmount(0F);
		}
		fpr.setBillDueDate(billpayVerificationResponse.getDueDate());
		fpr.setBillNumber(billpayVerificationResponse.getInvoiceNumber());
		fpr.setErrorCode(billpayVerificationResponse.getResponseCode());
		data.put("Customer Name", billpayVerificationResponse.getCustomerName());
		data.put("Account Number", billpayVerificationResponse.getAccountNumber());
		data.put("Mobile Number", mobileNumber);
		infoList.add(data);
		fpr.setInfo(infoList);
		logger.info("fetchand pay result:"+fpr);
		return fpr;
	}
}



