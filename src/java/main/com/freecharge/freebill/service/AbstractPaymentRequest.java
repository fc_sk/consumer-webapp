package com.freecharge.freebill.service;

import com.freecharge.freebill.billdesk.IBillPaymentRequest;

public abstract class AbstractPaymentRequest implements IBillPaymentRequest {

	@Override
	public String getBankID() { return "FCH"; }

	@Override
	public String getItemCode() { return "INSTAPAY"; }

	@Override
	public String getAdditionalInfo2() { return null; }
	@Override
	public String getAdditionalInfo3() { return null; }
	@Override
	public String getAdditionalInfo4() { return null; }
	@Override
	public String getAdditionalInfo5() { return null; }
	@Override
	public String getAdditionalInfo6() { return null; }
	@Override
	public String getAdditionalInfo7() { return null; }

}
