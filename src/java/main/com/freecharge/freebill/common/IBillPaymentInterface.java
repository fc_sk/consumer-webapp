package com.freecharge.freebill.common;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.domain.BillPostpaidMerchant;

public interface IBillPaymentInterface {

    public BillPaymentCommonApiResponse submitPaymentRequest(String orderID, TxnHomePage thp, BillPostpaidMerchant billPostpaidMerchant, Integer paymentType, String paymentOption) throws Exception;
    public BillPaymentCommonValidationApiResponse submitValidationRequest(IBillValidationRequest req, BillPostpaidMerchant billPostpaidMerchant) throws ClientProtocolException, IOException;
    //public AggregatorResponseDo submitPaymentValidationRequest(RechargeDo rechargeDo, InResponse inResponseFromRequest) throws HttpException, ConnectException,ConnectTimeoutException, SocketException, SocketTimeoutException,IOException;
}
