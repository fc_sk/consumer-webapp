package com.freecharge.freebill.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.freecharge.freebill.statuscheckservice.BillDeskStatusCheckService;
import com.freecharge.freebill.statuscheckservice.EuronetStatusCheckService;
import com.freecharge.freebill.statuscheckservice.IBIllPaymentStatusCheckInterface;
import com.freecharge.freebill.statuscheckservice.OxigenStatusCheckService;
import com.freecharge.payment.util.PaymentConstants;

public class BillPaymentFactory implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Autowired
	private OxigenStatusCheckService oxigenStatusCheckService;

	@Autowired
	private BillDeskStatusCheckService billDeskStatusCheckService;
	
	@Autowired
	private EuronetStatusCheckService EuronetStatusCheckService;

	public IBillPaymentInterface getBillPaymentGateway(String paymentGatewayName) {

		if (this.getApplicationContext().containsBean(paymentGatewayName))
			return (IBillPaymentInterface) this.getApplicationContext()
					.getBean(paymentGatewayName);
		else
			return null;
	}

	public IBIllPaymentStatusCheckInterface getBillPaymentStatusCheckGateway(
			String paymentGatewayName) {

		if (paymentGatewayName
				.equalsIgnoreCase(PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY)) {
			return oxigenStatusCheckService;
		} else {
			if (paymentGatewayName
					.equalsIgnoreCase(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY)) {
				return billDeskStatusCheckService;
			} else {
				if (paymentGatewayName
						.equalsIgnoreCase(PaymentConstants.BILL_PAYMENT_EURONET_GATEWAY)) {
					return EuronetStatusCheckService;
				}
			}
		}
		return null;
	}

	public void setApplicationContext(ApplicationContext ctx)
			throws BeansException {
		this.applicationContext = ctx;
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
