package com.freecharge.freebill.common;

public class BillPaymentVerificationAPIResponse {

	String accountNumber;
	String billAmount;
	String consumerno;
	String invoiceNumber;
	String customerEmail;
	String customerName;
	String dueDate;
	String lastBilledAmount;
	String responseAction;
	String responseCode;
	String responseMessage;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}
	public String getConsumerno() {
		return consumerno;
	}
	public void setConsumerno(String consumerno) {
		this.consumerno = consumerno;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getLastBilledAmount() {
		return lastBilledAmount;
	}
	public void setLastBilledAmount(String lastBilledAmount) {
		this.lastBilledAmount = lastBilledAmount;
	}
	public String getResponseAction() {
		return responseAction;
	}
	public void setResponseAction(String responseAction) {
		this.responseAction = responseAction;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
