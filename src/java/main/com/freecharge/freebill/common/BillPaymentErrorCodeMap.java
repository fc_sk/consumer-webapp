package com.freecharge.freebill.common;

public class BillPaymentErrorCodeMap implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private String billPaymentGateway;
    private String billPaymentGatewayResponseCode;
    private String billPaymentInternalResponseCode;
    private String billPaymentInyternalMessage;
    private Boolean isRetryable;
    private Boolean isDisplayEnabled;

    public Boolean getIsDisplayEnabled() {
		return isDisplayEnabled;
	}

	public void setIsDisplayEnabled(Boolean isDisplayEnabled) {
		this.isDisplayEnabled = isDisplayEnabled;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBillPaymentGateway() {
        return billPaymentGateway;
    }

    public void setBillPaymentGateway(String billPaymentGateway) {
        this.billPaymentGateway = billPaymentGateway;
    }

    public String getBillPaymentGatewayResponseCode() {
        return billPaymentGatewayResponseCode;
    }

    public void setBillPaymentGatewayResponseCode(String billPaymentGatewayResponseCode) {
        this.billPaymentGatewayResponseCode = billPaymentGatewayResponseCode;
    }

    public String getBillPaymentInternalResponseCode() {
        return billPaymentInternalResponseCode;
    }

    public void setBillPaymentInternalResponseCode(String billPaymentInternalResponseCode) {
        this.billPaymentInternalResponseCode = billPaymentInternalResponseCode;
    }

    public String getBillPaymentInyternalMessage() {
        return billPaymentInyternalMessage;
    }

    public void setBillPaymentInyternalMessage(String billPaymentInyternalMessage) {
        this.billPaymentInyternalMessage = billPaymentInyternalMessage;
    }

    public Boolean getIsRetryable() {
        return isRetryable;
    }

    public void setIsRetryable(Boolean isRetryable) {
        this.isRetryable = isRetryable;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillPaymentErrorCodeMap [id=").append(id).append(", billPaymentGateway=")
				.append(billPaymentGateway).append(", billPaymentGatewayResponseCode=")
				.append(billPaymentGatewayResponseCode).append(", billPaymentInternalResponseCode=")
				.append(billPaymentInternalResponseCode).append(", billPaymentInyternalMessage=")
				.append(billPaymentInyternalMessage).append(", isRetryable=").append(isRetryable)
				.append(", isDisplayEnabled=").append(isDisplayEnabled).append("]");
		return builder.toString();
	}
    
}
