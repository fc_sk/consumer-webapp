package com.freecharge.freebill.common;

import java.math.BigDecimal;
import java.util.Date;

import com.freecharge.api.error.ErrorCode;

public class BillPaymentCommonValidationApiResponse {

    private String status;
    @Override
	public String toString() {
		return "BillPaymentCommonValidationApiResponse [status=" + status + ", operatorId=" + operatorId
				+ ", billAmount=" + billAmount + ", billDueDate=" + billDueDate + ", rowResponse=" + rowResponse
				+ ", billProvider=" + billProvider + "]";
	}
	private String operatorId;
    private BigDecimal billAmount;
    private Date billDueDate;
    private String rowResponse;
    private String billProvider;
    ErrorCode errorCode;
    
    public ErrorCode getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
    public String getBillProvider() {
        return billProvider;
    }
    public void setBillProvider(String billProvider) {
        this.billProvider = billProvider;
    }
    public String getRowResponse() {
        return rowResponse;
    }
    public void setRowResponse(String rowResponse) {
        this.rowResponse = rowResponse;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getOperatorId() {
        return operatorId;
    }
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }
    public BigDecimal getBillAmount() {
        return billAmount;
    }
    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }
    public Date getBillDueDate() {
        return billDueDate;
    }
    public void setBillDueDate(Date billDueDate) {
        this.billDueDate = billDueDate;
    }
}