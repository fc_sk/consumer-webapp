package com.freecharge.freebill.common;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.dao.BillPaymentStatusReadDao;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freebill.statuscheckservice.BillPaymentStatusCheckResponse;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;

@Service
public class BillPaymentStatusCheck {

    private static final Logger logger = LoggingFactory.getLogger(BillPaymentStatusCheck.class);

    @Autowired
    private BillPaymentStatusReadDao billPaymentStatusReadDao;

    @Autowired
    private BillPaymentFactory billPaymentFactory;

    @Autowired
    private BillReadDAO billReadDAO;

    @Autowired
    private BillWriteDAO billWriteDAO;

    @Autowired
    private FulfillmentService fulfillmentService;

    @Autowired
    private BillPaymentService billPaymentService;

    @Autowired
    KestrelWrapper kestrelWrapper;
    
    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    public void doBillPaymentStatusCheck(Date startDate, Date endDate) {
        logger.info("In BillPaymentStatusCheck for status check request");
        String orderId = null;
        String billPaymentGateway = null;
        List<BillPaymentStatus> listOfPaymentstatusUnknownTxn = billPaymentStatusReadDao
                .findUnknownStatusBillPaymentTxn(startDate, endDate);
        if (listOfPaymentstatusUnknownTxn != null)
            logger.info("bill payment status check list size : " + listOfPaymentstatusUnknownTxn.size());
        try {
            if (listOfPaymentstatusUnknownTxn != null && !listOfPaymentstatusUnknownTxn.isEmpty()) {
                for (BillPaymentStatus billPaymentStatus : listOfPaymentstatusUnknownTxn) {
                    try {
                        orderId = billPaymentStatus.getAffiliateTransID();
                        billPaymentGateway = billPaymentStatus.getBillPaymentGateway();
                        logger.info("order id for status check request " + orderId + " with gateway "
                                + billPaymentGateway);
                        if (orderId != null && !orderId.isEmpty() && billPaymentGateway != null
                                && !billPaymentGateway.isEmpty()) {
                            processRechargeStatusAsync(orderId);
                        } else {
                            logger.info("order id or bill payment gateway is null billPaymentStatus id"
                                    + billPaymentStatus.getAffiliateTransID());
                        }
                    } catch (Exception exception) {
                        logger.error("exception occured while checking bill payment status check for orderId "
                                + orderId + " ", exception);
                    }
                }
            } else {
                logger.info("No records found in bill payment status for the time duration from " + startDate + " to "
                        + endDate);
            }
        } catch (Exception exception) {
            logger.error("Exception occured during Bill PaymentstatusUnknownTxn " + orderId + " ", exception);
        }

    }

    public void updateStatusAndIntitiateFulfilment(BillPaymentStatusCheckResponse billPaymentStatusCheckResponse,
            BillPaymentStatus billPaymentStatus) {
        logger.info("row response of status check api " + billPaymentStatusCheckResponse.getRowResponse());
        String txnStatusResCode = billPaymentStatusCheckResponse.getGatewayTxnStatusCode();
        BillPaymentErrorCodeMap gatewayInternalResponseCode = billPaymentService.getBillPaymentInternalResponseCode(
                txnStatusResCode, billPaymentStatusCheckResponse.getGatewayName());
        logger.info("row response of status " + txnStatusResCode);
        if (gatewayInternalResponseCode != null) {
            logger.info("internal status code " + gatewayInternalResponseCode.getBillPaymentInternalResponseCode());
            if (gatewayInternalResponseCode.getBillPaymentInternalResponseCode() != null
                    && gatewayInternalResponseCode.getBillPaymentInternalResponseCode().equals(
                            RechargeConstants.IN_UNDER_PROCESS_CODE)) {
                billPaymentStatus.setSuccess(null);
            } else {
                if (gatewayInternalResponseCode.getBillPaymentInternalResponseCode().equalsIgnoreCase(
                        PaymentConstants.BILL_PAPYMENT_INTERNAL_SUCCESS_RESPONSE_CODE)) {
                    billPaymentStatus.setGatewayReferenceNumber(billPaymentStatusCheckResponse
                            .getGatewayReferenceNumber());
					billPaymentStatus.setAuthStatus(billPaymentStatusCheckResponse.getGatewayTxnStatusCode());
					billPaymentStatus.setAuthDetail(billPaymentStatusCheckResponse.getTransactionStatusMessage());
                    billPaymentStatus.setSuccess(true);
                } else {
                    billPaymentStatus.setGatewayReferenceNumber(billPaymentStatusCheckResponse
                            .getGatewayReferenceNumber());
                    billPaymentStatus.setSuccess(false);
                }
            }
        } else {
            logger.info("gatewayInternalResponseCode is empty for orderId "
                    + billPaymentStatusCheckResponse.getOrderId());
            if (!StringUtils.isEmpty(txnStatusResCode)
                    && !AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(txnStatusResCode)) {
                billPaymentStatus.setGatewayReferenceNumber(billPaymentStatusCheckResponse.getGatewayReferenceNumber());
                billPaymentStatus.setSuccess(false);
            }
        }
        billPaymentStatus.setUpdatedAt(new Timestamp(new Date().getTime()));
        if (billPaymentStatus.getSuccess() != null) {
            if (billPaymentStatus.getSuccess()) {
                Map<String, Object> updatedUserDetail = new HashMap<String, Object>();
                updatedUserDetail.put("circleName", "");
                updatedUserDetail.put("operatorName", billPaymentStatus.getOperatorName());
                updatedUserDetail.put("productType", ProductMaster.ProductName.MobilePostpaid.getProductType());
                userTransactionHistoryService.createOrUpdateRechargeHistory(billPaymentStatus.getAffiliateTransID(),
                        updatedUserDetail, RechargeConstants.IN_UNDER_PROCESS_CODE);
            }
            billWriteDAO.updateBillPaymentStatus(billPaymentStatus, billPaymentStatus.getRetryNumber());
            BillPaymentAttempt billPaymentAttempt = billReadDAO.findBillPaymentAttemptByStatusId(billPaymentStatus.getId());
            billWriteDAO.updateBillPaymentAttempt(billPaymentStatus,billPaymentStatusCheckResponse,billPaymentAttempt);
            fulfillmentService.doFulfillment(billPaymentStatus.getAffiliateTransID(), true, false, false);
        }
    }

    private void processRechargeStatusAsync(String orderId) {

        kestrelWrapper.enqueueForPostpaidStatusCheck(orderId);
    }
}
