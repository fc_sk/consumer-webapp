package com.freecharge.freebill.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.common.cache.CacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;

public class BillPaymentCacheManager  extends CacheManager {
    private static final String BILL_PAYMENT_ERRORCODE_MAP_CACHE = "billPaymentErrorCodeMap";
    private static String POSTPAID_AGGREGATOR_PRIORITY_CACHED_KEY = "postpaidAggregatorCachedKey";
    private static String POSTPAID_MERCHANT_CACHED_KEY = "postpaidMerchantCachedKey";
    private static String BILL_PAY_NAME_OPR_MASTER_KEY = "billPayNameOperatorMasterCodeMap";
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    private final Logger logger = LoggingFactory.getLogger(BillPaymentCacheManager.class);

    // Error Code Mapping

    public List<BillPaymentErrorCodeMap> getBillPaymentErrorCodeMap(String billPaymentGatewayResponseCode, String billpaymentGateway) {
        Map<String, List<BillPaymentErrorCodeMap>> listOfMap = (Map<String, List<BillPaymentErrorCodeMap>>) get(BILL_PAYMENT_ERRORCODE_MAP_CACHE);
        if (listOfMap != null && listOfMap.size() > 0 && billPaymentGatewayResponseCode != null && billpaymentGateway != null) {
            return listOfMap.get(billPaymentGatewayResponseCode +"-"+ billpaymentGateway);
        }
        return null;
    }

    public void setBillPaymentErrorCodeMap(List<BillPaymentErrorCodeMap> inErrorCodeList) {
        Map<String, List<BillPaymentErrorCodeMap>> listOfMap = new HashMap<String, List<BillPaymentErrorCodeMap>>();

        if (inErrorCodeList != null && inErrorCodeList.size() > 0) {

            for (BillPaymentErrorCodeMap billPaymentErrorCodeMap : inErrorCodeList) {
                String billPaymentGatewayResponseCode = billPaymentErrorCodeMap.getBillPaymentGatewayResponseCode();
                String billpaymentGateway = billPaymentErrorCodeMap.getBillPaymentGateway();

                if (listOfMap.get( billPaymentGatewayResponseCode +"-"+ billpaymentGateway ) != null) {
                    List<BillPaymentErrorCodeMap> inIterateErrorCodeList = listOfMap.get(billPaymentGatewayResponseCode +"-"+ billpaymentGateway);
                    inIterateErrorCodeList.add(billPaymentErrorCodeMap);
                    listOfMap.put(billPaymentGatewayResponseCode +"-"+ billpaymentGateway, inIterateErrorCodeList);
                } else {
                    List<BillPaymentErrorCodeMap> inIterateErrorCodeList = new ArrayList<BillPaymentErrorCodeMap>();
                    inIterateErrorCodeList.add(billPaymentErrorCodeMap);
                    listOfMap.put(billPaymentGatewayResponseCode +"-"+ billpaymentGateway, inIterateErrorCodeList);
                }
            }
        }
        set(BILL_PAYMENT_ERRORCODE_MAP_CACHE, listOfMap);
    }

    public void setPostpaidAggPriority(List<BillPaymentGatewayPriority> billPaymentGatewayPrioritiesListObj) {
        Map<Integer, BillPaymentGatewayPriority> mapByPaymentgateway = new HashMap<Integer, BillPaymentGatewayPriority>();
        if (billPaymentGatewayPrioritiesListObj != null && billPaymentGatewayPrioritiesListObj.size() > 0) {
            for (BillPaymentGatewayPriority billPaymentPriority : billPaymentGatewayPrioritiesListObj) {
                mapByPaymentgateway.put(billPaymentPriority.getFkOperatorId(), billPaymentPriority);
            }
            set(POSTPAID_AGGREGATOR_PRIORITY_CACHED_KEY, mapByPaymentgateway);
        }
    }

    public BillPaymentGatewayPriority getPostpaidAggPriority(Integer operatorId) {
        if (operatorId != null) {
            Map<Integer, BillPaymentGatewayPriority> map = (HashMap<Integer, BillPaymentGatewayPriority>) get(POSTPAID_AGGREGATOR_PRIORITY_CACHED_KEY);
            if (map != null && map.size() > 0) {
                return map.get(operatorId);
            }
        }
        return null;
    }

    public void deletePaymentGatewayPriorityCachedKey() {
        delete(POSTPAID_AGGREGATOR_PRIORITY_CACHED_KEY);
    }

    public BillPostpaidMerchant getPostpaidMerchantByOperatorId(Integer operatorId) {
        if (operatorId != null) {
            Map<Integer, BillPostpaidMerchant> map = (HashMap<Integer, BillPostpaidMerchant>) get(POSTPAID_MERCHANT_CACHED_KEY);
            if (map != null && map.size() > 0) {
                return map.get(operatorId);
            }
        }
        return null;
    }

    public void setPostpaidMerchants(List<BillPostpaidMerchant> billPostpaidMerchants) {
        Map<Integer, BillPostpaidMerchant> mapBypostPaidMerchant = new HashMap<Integer, BillPostpaidMerchant>();
        if (billPostpaidMerchants != null && billPostpaidMerchants.size() > 0) {
            for (BillPostpaidMerchant billPostpaidMerchant : billPostpaidMerchants) {
                mapBypostPaidMerchant.put(billPostpaidMerchant.getFkOperatorMasterId(), billPostpaidMerchant);
            }
            set(POSTPAID_MERCHANT_CACHED_KEY, mapBypostPaidMerchant);
        }
    }
    
    public BillPayOperatorMaster getBillPayOperatorFromName(String operatorName) {
        if (StringUtils.isEmpty(operatorName)) {
            return null;
        }
        BillPayOperatorMaster operatorMaster = null;
        Map<String, BillPayOperatorMaster> map = getBillPayOperatorMasterMap();
        operatorMaster = map.get(operatorName);
        if (operatorMaster == null) {
            operatorMaster = billPayServiceRemote.getBillPayOperatorFromName(operatorName);
            map.put(operatorName, operatorMaster);
            setBillPayOperatorMasterMap(map);
        }
        return operatorMaster;
    }

    @SuppressWarnings("unchecked")
    private Map<String, BillPayOperatorMaster> getBillPayOperatorMasterMap() {
        Map<String, BillPayOperatorMaster> map = (HashMap<String, BillPayOperatorMaster>) get(
                BILL_PAY_NAME_OPR_MASTER_KEY);
        if (map == null) {
            map = new HashMap<String, BillPayOperatorMaster>();
            set(BILL_PAY_NAME_OPR_MASTER_KEY, map);
        }
        return map;
    }

    private void setBillPayOperatorMasterMap(Map<String, BillPayOperatorMaster> map) {
        set(BILL_PAY_NAME_OPR_MASTER_KEY, map);
    }

}
