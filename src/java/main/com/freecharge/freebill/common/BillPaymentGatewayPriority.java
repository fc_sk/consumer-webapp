package com.freecharge.freebill.common;

public class BillPaymentGatewayPriority implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String billType;
    private String billProvider;
    private Boolean isActive;
    private Integer fkOperatorId;

    public Integer getFkOperatorId() {
		return fkOperatorId;
	}

	public void setFkOperatorId(Integer fkOperatorId) {
		this.fkOperatorId = fkOperatorId;
	}

	public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBillProvider() {
        return billProvider;
    }

    public void setBillProvider(String billProvider) {
        this.billProvider = billProvider;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getId().equals(((BillPaymentGatewayPriority)obj).getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }
}
