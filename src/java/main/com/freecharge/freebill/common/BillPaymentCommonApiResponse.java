package com.freecharge.freebill.common;

import java.util.Date;

public class BillPaymentCommonApiResponse {

    private String requestparam;
    private String txnId;
    private String rowResponse;
    private Boolean success;
    private String billPaymentStatusCode;
    private String billPaymentAuthDetail;
    private String authDetail;
    private String billPaymentGateway;
    private String gatewayReferenceNumber;
    private Date createdTime;

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getBillPaymentGateway() {
        return billPaymentGateway;
    }

    public void setBillPaymentGateway(String billPaymentGateway) {
        this.billPaymentGateway = billPaymentGateway;
    }

    public String getAuthDetail() {
        return authDetail;
    }

    public void setAuthDetail(String authDetail) {
        this.authDetail = authDetail;
    }

    public String getRowResponse() {
        return rowResponse;
    }

    public void setRowResponse(String rowResponse) {
        this.rowResponse = rowResponse;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void isSuccess(Boolean success) {
        this.success = success;
    }

    public String getBillPaymentStatusCode() {
        return billPaymentStatusCode;
    }

    public void setBillPaymentStatusCode(String billPaymentStatusCode) {
        this.billPaymentStatusCode = billPaymentStatusCode;
    }

    public String getBillPaymentAuthDetail() {
        return billPaymentAuthDetail;
    }

    public void setBillPaymentAuthDetail(String billPaymentAuthDetail) {
        this.billPaymentAuthDetail = billPaymentAuthDetail;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getRequestparam() {
        return requestparam;
    }

    public void setRequestparam(String requestparam) {
        this.requestparam = requestparam;
    }
    
    public String getGatewayReferenceNumber() {
        return gatewayReferenceNumber;
    }

    public void setGatewayReferenceNumber(String gatewayReferenceNumber) {
        this.gatewayReferenceNumber = gatewayReferenceNumber;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillPaymentCommonApiResponse [requestparam=").append(requestparam).append(", txnId=")
				.append(txnId).append(", rowResponse=").append(rowResponse).append(", success=").append(success)
				.append(", billPaymentStatusCode=").append(billPaymentStatusCode).append(", billPaymentAuthDetail=")
				.append(billPaymentAuthDetail).append(", authDetail=").append(authDetail)
				.append(", billPaymentGateway=").append(billPaymentGateway).append(", gatewayReferenceNumber=")
				.append(gatewayReferenceNumber).append(", createdTime=").append(createdTime).append("]");
		return builder.toString();
	}
    
}
