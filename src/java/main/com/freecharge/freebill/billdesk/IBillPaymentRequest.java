package com.freecharge.freebill.billdesk;

import java.math.BigDecimal;

public interface IBillPaymentRequest {

	public String getOperatorID();             // | mandatory
	public String getCustomerID();             // | mandatory
	public BigDecimal getTransactionAmount();  // | mandatory
	public String getBankID();                 // | mandatory -- "FRC"
	public String getItemCode();               // | mandatory - "INSTAPAY" (mapped as per biller)
	public String getAdditionalInfo1();    // optional
	public String getAdditionalInfo2();    // optional
	public String getAdditionalInfo3();    // optional
	public String getAdditionalInfo4();    // optional
	public String getAdditionalInfo5();    // optional
	public String getAdditionalInfo6();    // optional
	public String getAdditionalInfo7();    // optional
	public String getCcno();                   // | mandatory - Unique FreeCharge Debit Reference Number

}
