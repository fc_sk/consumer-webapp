package com.freecharge.freebill.billdesk;

import java.math.BigDecimal;
import java.util.Date;

import com.freecharge.common.framework.basedo.AbstractDO;

public class BillValidationAPIResponse extends AbstractDO {

	private String SourceId;
	private String MerchantId;
	private String CustomerId;
	private String AdditionalInfo1;
	private String AdditionalInfo2;
	private String AdditionalInfo3;
	private String AdditionalInfo4;
	private String AdditionalInfo5;
	private String AdditionalInfo6;
	private String AdditionalInfo7;
	private BigDecimal BillAmount;
	private Date BillDueDate;
	private String Filler1;
	private String Filler2;
	private String Filler3;
	private String status;
	private String responseCode;

	public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getSourceId() {
		return SourceId;
	}
	public void setSourceId(String sourceId) {
		SourceId = sourceId;
	}
	public String getMerchantId() {
		return MerchantId;
	}
	public void setMerchantId(String merchantId) {
		MerchantId = merchantId;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getAdditionalInfo1() {
		return AdditionalInfo1;
	}
	public void setAdditionalInfo1(String additionalInfo1) {
		AdditionalInfo1 = additionalInfo1;
	}
	public String getAdditionalInfo2() {
		return AdditionalInfo2;
	}
	public void setAdditionalInfo2(String additionalInfo2) {
		AdditionalInfo2 = additionalInfo2;
	}
	public String getAdditionalInfo3() {
		return AdditionalInfo3;
	}
	public void setAdditionalInfo3(String additionalInfo3) {
		AdditionalInfo3 = additionalInfo3;
	}
	public String getAdditionalInfo4() {
		return AdditionalInfo4;
	}
	public void setAdditionalInfo4(String additionalInfo4) {
		AdditionalInfo4 = additionalInfo4;
	}
	public String getAdditionalInfo5() {
		return AdditionalInfo5;
	}
	public void setAdditionalInfo5(String additionalInfo5) {
		AdditionalInfo5 = additionalInfo5;
	}
	public String getAdditionalInfo6() {
		return AdditionalInfo6;
	}
	public void setAdditionalInfo6(String additionalInfo6) {
		AdditionalInfo6 = additionalInfo6;
	}
	public String getAdditionalInfo7() {
		return AdditionalInfo7;
	}
	public void setAdditionalInfo7(String additionalInfo7) {
		AdditionalInfo7 = additionalInfo7;
	}
	public BigDecimal getBillAmount() {
		return BillAmount;
	}
	public void setBillAmount(BigDecimal billAmount) {
		BillAmount = billAmount;
	}
	public void setBillAmount(String billAmount) {
		BillAmount = new BigDecimal(billAmount);
	}
	public Date getBillDueDate() {
		return BillDueDate;
	}
	public void setBillDueDate(Date billDueDate) {
		BillDueDate = billDueDate;
	}
	public String getFiller1() {
		return Filler1;
	}
	public void setFiller1(String filler1) {
		Filler1 = filler1;
	}
	public String getFiller2() {
		return Filler2;
	}
	public void setFiller2(String filler2) {
		Filler2 = filler2;
	}
	public String getFiller3() {
		return Filler3;
	}
	public void setFiller3(String filler3) {
		Filler3 = filler3;
	}
       public String getResponseCode() {
              return responseCode;
       }
       public void setResponseCode(String responseCode) {
              this.responseCode = responseCode;
       }
	
}