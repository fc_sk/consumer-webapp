package com.freecharge.freebill.billdesk;

public class BilldeskException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BilldeskException(String message) {
		super(message);
	}

	public BilldeskException(String message, Throwable cause) {
		super(message, cause);
	}

}
