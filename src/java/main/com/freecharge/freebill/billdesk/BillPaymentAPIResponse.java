package com.freecharge.freebill.billdesk;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.regex.Pattern;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;

public class BillPaymentAPIResponse extends AbstractDO {

	private String rawResponse;
	private boolean success;

	private String MerchantID;
	private String CustomerId;
	private String TxnReferenceNo;
	private String BankRefNo;
	private String TxnAmount;
	private String BankId;
	private String BankMerchantID;
	private String TxnType;

	private String CurrencyType;
	private String ItemCode;
	private String SecurityType;
	private String SecurityId;
	private String SecurityPassword;
	private String TxnDate;
	private String AuthStatus;
	private String AuthDetail;
	private String SettlementType;
	private String AdditionalInfo1;
	private String AdditionalInfo2;
	private String AdditionalInfo3;
	private String AdditionalInfo4;
	private String AdditionalInfo5;
	private String AdditionalInfo6;
	private String AdditionalInfo7;
	private String ErrorStatus;
	private String ErrorDescription;
	private String Checksum;

    /*
     *           0     |    1     |      2       |    3    |     4     |   5  |       6      |   7    |       8     |    9    |      10     |     11    |       12       |          13       |     14    |       15      |       16      |       17      |       18      |       19      |       20      |       21      |       22      |     23    |        24      |    25    |
     * Spec: MerchantID|CustomerId|TxnReferenceNo|BankRefNo|Txn Amount |BankId|BankMerchantID|Txn Type|Currency Type|Item Code|Security Type|Security Id|SecurityPassword|Txn Date           |Auth Status|Settlement Type|AdditionalInfo1|AdditionalInfo2|AdditionalInfo3|AdditionalInfo4|AdditionalInfo5|AdditionalInfo6|AdditionalInfo7|ErrorStatus|ErrorDescription|Checksum  |
     * E.g.: MERCHANTID|9820123456|MFRC0412001668|234324   |00002400.30|FRC   |22270726      |NA      |INR          |NA       |NA           |NA         |NA              |12-12-2004 16:08:56|0300       |NA             |DA01017224     |AXPIY          |NA             |NA             |NA             |NA             |NA             |NA         |NA              |3734835005|
     * Sample: IDEA    |9911137574|MFCH3087968871|0        |00000002.00|FCH   |IDEA          |NA      |INR          |INSTAPAY |NA           |NA         |NA              |27-08-2013 12:58:06|0399       |NA             |NA             |NA             |NA             |NA             |NA             |NA             |NA             |NA         |Invalid Source  |2878846342|
     */

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public static BillPaymentAPIResponse parse(String response) {
        FCUtil.assertNonEmptyArgument(response, "response");
        String[] tokens = response.split(Pattern.quote("|"));
        if (tokens.length != 26) {
            throw new IllegalArgumentException(String.format(
                    "Expected 26 tokens separated by | but found %d tokens. Before parsing: %s", tokens.length, response));
        }
        BillPaymentAPIResponse bpr = new BillPaymentAPIResponse();
        bpr.setMerchantID(      tokens[0]);
        bpr.setCustomerId(      tokens[1]);
        bpr.setTxnReferenceNo(  tokens[2]);
        bpr.setBankRefNo(       tokens[3]);
        bpr.setTxnAmount(       tokens[4]);
        bpr.setBankId(          tokens[5]);
        bpr.setBankMerchantID(  tokens[6]);
        bpr.setTxnType(         tokens[7]);
        bpr.setCurrencyType(    tokens[8]);
        bpr.setItemCode(        tokens[9]);
        bpr.setSecurityType(    tokens[10]);
        bpr.setSecurityId(      tokens[11]);
        bpr.setSecurityPassword(tokens[12]);
        bpr.setTxnDate(         tokens[13]);
        bpr.setAuthStatus(      tokens[14]);
        bpr.setSettlementType(  tokens[15]);
        bpr.setAdditionalInfo1( tokens[16]);
        bpr.setAdditionalInfo2( tokens[17]);
        bpr.setAdditionalInfo3( tokens[18]);
        bpr.setAdditionalInfo4( tokens[19]);
        bpr.setAdditionalInfo5( tokens[20]);
        bpr.setAdditionalInfo6( tokens[21]);
        bpr.setAdditionalInfo7( tokens[22]);
        bpr.setErrorStatus(     tokens[23]);
        bpr.setErrorDescription(tokens[24]);
        bpr.setChecksum(        tokens[25]);
        // set extra data
        bpr.setSuccess(AUTH_SUCCESS.equals(bpr.getAuthStatus()));
        bpr.setAuthDetail(inferAuthDetail(bpr.getAuthStatus()));
        bpr.setRawResponse(response);

        return bpr;
    }

    public static final String AUTH_SUCCESS = "0300";

    public static final Map<String, String> AUTH_STATUSES = FCUtil.createMap(
            "0300", "Success",
            "0399", "Invalid Authentication at Bank",
            "NA",   "Invalid Input in the Request Message",
            "0002", "BillDesk is waiting for Response from Bank",
            "0001", "Error at BillDesk");

    public static String inferAuthDetail(String authStatus) {
        if (FCUtil.isEmpty(authStatus)) {
            return "Expected valid AuthStatus but found empty: " + FCUtil.stringify(authStatus);
        }
        String detail = AUTH_STATUSES.get(authStatus);
        return detail!=null? detail: ("Unsupported AuthStatus=" + FCUtil.stringify(authStatus));
    }

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getTxnReferenceNo() {
		return TxnReferenceNo;
	}
	public void setTxnReferenceNo(String txnReferenceNo) {
		TxnReferenceNo = txnReferenceNo;
	}
	public String getBankRefNo() {
		return BankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		BankRefNo = bankRefNo;
	}
	public String getTxnAmount() {
		return TxnAmount;
	}
	public void setTxnAmount(String txnAmount) {
		TxnAmount = txnAmount;
	}
	public String getBankId() {
		return BankId;
	}
	public void setBankId(String bankId) {
		BankId = bankId;
	}
	public String getBankMerchantID() {
		return BankMerchantID;
	}
	public void setBankMerchantID(String bankMerchantID) {
		BankMerchantID = bankMerchantID;
	}
	public String getTxnType() {
		return TxnType;
	}
	public void setTxnType(String txnType) {
		TxnType = txnType;
	}
	public String getCurrencyType() {
		return CurrencyType;
	}
	public void setCurrencyType(String currencyType) {
		CurrencyType = currencyType;
	}
	public String getItemCode() {
		return ItemCode;
	}
	public void setItemCode(String itemCode) {
		ItemCode = itemCode;
	}
	public String getSecurityType() {
		return SecurityType;
	}
	public void setSecurityType(String securityType) {
		SecurityType = securityType;
	}
	public String getSecurityId() {
		return SecurityId;
	}
	public void setSecurityId(String securityId) {
		SecurityId = securityId;
	}
	public String getSecurityPassword() {
		return SecurityPassword;
	}
	public void setSecurityPassword(String securityPassword) {
		SecurityPassword = securityPassword;
	}
	public String getTxnDate() {
		return TxnDate;
	}
	public void setTxnDate(String txnDate) {
		TxnDate = txnDate;
	}
	public String getAuthStatus() {
		return AuthStatus;
	}
	public void setAuthStatus(String authStatus) {
		AuthStatus = authStatus;
	}
	public String getAuthDetail() {
		return AuthDetail;
	}
	public void setAuthDetail(String authDetail) {
		AuthDetail = authDetail;
	}
	public String getSettlementType() {
		return SettlementType;
	}
	public void setSettlementType(String settlementType) {
		SettlementType = settlementType;
	}
	public String getAdditionalInfo1() {
		return AdditionalInfo1;
	}
	public void setAdditionalInfo1(String additionalInfo1) {
		AdditionalInfo1 = additionalInfo1;
	}
	public String getAdditionalInfo2() {
		return AdditionalInfo2;
	}
	public void setAdditionalInfo2(String additionalInfo2) {
		AdditionalInfo2 = additionalInfo2;
	}
	public String getAdditionalInfo3() {
		return AdditionalInfo3;
	}
	public void setAdditionalInfo3(String additionalInfo3) {
		AdditionalInfo3 = additionalInfo3;
	}
	public String getAdditionalInfo4() {
		return AdditionalInfo4;
	}
	public void setAdditionalInfo4(String additionalInfo4) {
		AdditionalInfo4 = additionalInfo4;
	}
	public String getAdditionalInfo5() {
		return AdditionalInfo5;
	}
	public void setAdditionalInfo5(String additionalInfo5) {
		AdditionalInfo5 = additionalInfo5;
	}
	public String getAdditionalInfo6() {
		return AdditionalInfo6;
	}
	public void setAdditionalInfo6(String additionalInfo6) {
		AdditionalInfo6 = additionalInfo6;
	}
	public String getAdditionalInfo7() {
		return AdditionalInfo7;
	}
	public void setAdditionalInfo7(String additionalInfo7) {
		AdditionalInfo7 = additionalInfo7;
	}
	public String getErrorStatus() {
		return ErrorStatus;
	}
	public void setErrorStatus(String errorStatus) {
		ErrorStatus = errorStatus;
	}
	public String getErrorDescription() {
		return ErrorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}
	public String getChecksum() {
		return Checksum;
	}
	public void setChecksum(String checksum) {
		Checksum = checksum;
	}

}
