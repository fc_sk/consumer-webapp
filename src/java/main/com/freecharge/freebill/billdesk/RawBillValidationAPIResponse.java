package com.freecharge.freebill.billdesk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;

/**
 * This is an XStream POJO - doesn't follow JavaBean property naming convention.
 */
public class RawBillValidationAPIResponse extends AbstractDO {

	private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyyMMdd");

	private String SourceId;
	private String MerchantId;
	private String CustomerId;
	private String AdditionalInfo1;
	private String AdditionalInfo2;
	private String AdditionalInfo3;
	private String AdditionalInfo4;
	private String AdditionalInfo5;
	private String AdditionalInfo6;
	private String AdditionalInfo7;
	private String StatusValid;
	private String StatusDesc;
	private String BillAmount;
	private String BillDueDate;
	private String Filler1;
	private String Filler2;
	private String Filler3;

	public static final long MILLIS_24_HOURS = 24 * 3600 * 1000;

	public BillValidationAPIResponse toBillValidationResponse(IBillValidationRequest req, String responseCode) {
		/*if (!("Y".equalsIgnoreCase(StatusValid))) {
			throw new BilldeskException("Validation status: " + StatusDesc==null? "NULL": ("'" + StatusDesc + "'"));
		}*/
		BillValidationAPIResponse resp = new BillValidationAPIResponse();
		resp.setStatus(StatusValid);
		resp.setSourceId(SourceId);
		resp.setMerchantId(/* "AIRTUPMOB" */ MerchantId);
		resp.setCustomerId(CustomerId);
		resp.setAdditionalInfo1(AdditionalInfo1);
		resp.setAdditionalInfo2(AdditionalInfo2);
		resp.setAdditionalInfo3(AdditionalInfo3);
		resp.setAdditionalInfo4(AdditionalInfo4);
		resp.setAdditionalInfo5(AdditionalInfo5);
		resp.setAdditionalInfo6(AdditionalInfo6);
		resp.setAdditionalInfo7(AdditionalInfo7);
		resp.setResponseCode(responseCode);
		if ("NA".equals(BillAmount) || BillAmount==null) {
		    resp.setBillAmount(req.getTransactionAmount());
		} else {
		    resp.setBillAmount(/* "30" */ BillAmount);
		}
		String strDate = /* "20130720" */ BillDueDate;
		try {
		    if ("NA".equals(strDate) || strDate==null) {
		        resp.setBillDueDate(new Date(System.currentTimeMillis() + MILLIS_24_HOURS));
		    } else {
	            resp.setBillDueDate(DATE_PARSER.parse(strDate));
		    }
		} catch (ParseException e) {
			throw new IllegalStateException(
					"Expected a valid date in yyyyMMdd format, but found " + FCUtil.stringify(strDate), e);
		}
		resp.setFiller1(Filler1);
		resp.setFiller2(Filler2);
		resp.setFiller3(Filler3);
		return resp;
	}

	public String getSourceId() {
		return SourceId;
	}
	public void setSourceId(String sourceId) {
		SourceId = sourceId;
	}
	public String getMerchantId() {
		return MerchantId;
	}
	public void setMerchantId(String merchantId) {
		MerchantId = merchantId;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getAdditionalInfo1() {
		return AdditionalInfo1;
	}
	public void setAdditionalInfo1(String additionalInfo1) {
		AdditionalInfo1 = additionalInfo1;
	}
	public String getAdditionalInfo2() {
		return AdditionalInfo2;
	}
	public void setAdditionalInfo2(String additionalInfo2) {
		AdditionalInfo2 = additionalInfo2;
	}
	public String getAdditionalInfo3() {
		return AdditionalInfo3;
	}
	public void setAdditionalInfo3(String additionalInfo3) {
		AdditionalInfo3 = additionalInfo3;
	}
	public String getAdditionalInfo4() {
		return AdditionalInfo4;
	}
	public void setAdditionalInfo4(String additionalInfo4) {
		AdditionalInfo4 = additionalInfo4;
	}
	public String getAdditionalInfo5() {
		return AdditionalInfo5;
	}
	public void setAdditionalInfo5(String additionalInfo5) {
		AdditionalInfo5 = additionalInfo5;
	}
	public String getAdditionalInfo6() {
		return AdditionalInfo6;
	}
	public void setAdditionalInfo6(String additionalInfo6) {
		AdditionalInfo6 = additionalInfo6;
	}
	public String getAdditionalInfo7() {
		return AdditionalInfo7;
	}
	public void setAdditionalInfo7(String additionalInfo7) {
		AdditionalInfo7 = additionalInfo7;
	}
	public String getStatusValid() {
		return StatusValid;
	}
	public void setStatusValid(String statusValid) {
		StatusValid = statusValid;
	}
	public String getStatusDesc() {
		return StatusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		StatusDesc = statusDesc;
	}
	public String getBillAmount() {
		return BillAmount;
	}
	public void setBillAmount(String billAmount) {
		BillAmount = billAmount;
	}
	public String getBillDueDate() {
		return BillDueDate;
	}
	public void setBillDueDate(String billDueDate) {
		BillDueDate = billDueDate;
	}
	public String getFiller1() {
		return Filler1;
	}
	public void setFiller1(String filler1) {
		Filler1 = filler1;
	}
	public String getFiller2() {
		return Filler2;
	}
	public void setFiller2(String filler2) {
		Filler2 = filler2;
	}
	public String getFiller3() {
		return Filler3;
	}
	public void setFiller3(String filler3) {
		Filler3 = filler3;
	}

}
