package com.freecharge.freebill.billdesk;

import java.math.BigDecimal;

public interface IBillValidationRequest {

	public String getOperatorID();
	public String getTimestamp();
	public BigDecimal getTransactionAmount();
	public String getAdditionalInfo1();
	public String getAdditionalInfo2();
	public String getAdditionalInfo3();
	public String getAdditionalInfo4();
	public String getAdditionalInfo5();
	public String getAdditionalInfo6();
	public String getAdditionalInfo7();
	public String getConsumerNumber();

}
