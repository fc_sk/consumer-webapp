package com.freecharge.freebill.billdeskpostpaid;

import com.freecharge.util.CustomHttpClientBuilder;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.LoggingConstants;
import com.freecharge.freebill.common.BillPaymentErrorCodeMap;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.fatals.InvalidChecksumException;
import com.freecharge.infrastructure.billpay.service.aggregator.BillPayBilldeskUtil;
import com.freecharge.infrastructure.billpay.util.BillPayConstants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.recharge.LogRequestService;
import com.google.common.collect.ImmutableMap;



@Service
public class BillPaymentBillDeskUtil {

	private static String SOURCE_ID = "FRC";
	private static final String MESSAGE_PARAM_TILDE_SEPARATOR = "~";
	private static final String AUTHENTICATOR_SEPARATOR = "!";
	public final static String DEFAULT_DETAILS = "NA";
	private static String PAYMENT_TYPE_PNY = "PNY";
	public static final String MESSAGE_PARAM_NAME = "msg";
	private static final String VALID_RESPONSE = "Y";

	private static final Object BDK_SUCCESS = "0";

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

	static SimpleDateFormat sdfBill = new SimpleDateFormat("yyyyMMdd");

	private static Logger logger = Logger.getLogger(BillPaymentBillDeskUtil.class);
	
	private static final SimpleDateFormat sdfLog = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	@Autowired
	private LogRequestService logRequestService;

	@Autowired
	MetricsClient metricsClient;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	private BillReadDAO billReadDao;
	
	@Autowired
	private AppConfigService appConfigService;
	
    private static final String PAYMENT_TYPE_WALLET = "WP";
	
	private static Map<String, String> FCBDKShortFormMap;

	static {
		FCBDKShortFormMap = ImmutableMap.<String, String> builder().put("ICIC", "ICI").put("AXIS", "UTI")
				.put("HDFC", "HDF").put("IDBI", "IDB").put("KOBK", "162").put("SBIN", "SBI").put("PJRB", "PNB")
				.put("YEBK", "YBK").put("BOI", "BOI").put("IOB", "IOB").put("UBI", "UBI").put("ANDB", "ADB")
				.put("ALLB", "ALB").put("BAHK", "BBK").put("BOBC", "BBC").put("BOBR", "BBR").put("MAHB", "BOM")
				.put("CANB", "CNB").put("CENB", "CBI").put("CITU", "CUB").put("CORB", "CRP").put("CSYB", "CSB")
				.put("DENB", "DEN").put("DEUB", "DBK").put("DHLB", "DLB").put("FEDB", "FBK").put("INDB", "INB")
				.put("INIB", "IDS").put("INVB", "ING").put("JNKB", "JKB").put("KRTB", "KBL").put("KARB", "KVB")
				.put("LXCB", "LVC").put("LXRB", "LVR").put("ORTB", "OBC").put("PJSB", "PSB").put("RATB", "RTN")
				.put("SHVB", "SVC").put("SINB", "SIB").put("SBJB", "SBJ").put("SHYB", "SBH").put("SMYB", "SBM")
				.put("SPTB", "SBP").put("STVB", "SBT").put("SYNB", "SYD").put("TMEB", "TMB").put("UNIB", "UNI")
				.put("VIJB", "VJB").build();
	}

	public FetchAndPayResultBDK getFetchAndPayResultBDK(HttpConnectionManagerParams connectionManagerConfig,
			String mobileNumber, Integer operatorId, String merchantId) throws ParseException {
		String traceId = billPayServiceRemote.getUniqueId();
//		Boolean isBBPAEnabled = true;
		String msg = null;
		if(appConfigService.isBBPSBillPaymentEnabled() && isBBPSEnabledMerchant(merchantId)) {
			logger.info("merchant is BBPS enabled");
			msg = createBillFetchMsgNew(traceId, mobileNumber, merchantId);
		}else {
			msg = createBillFetchMsg(traceId, mobileNumber, merchantId);;
		}
//		msg = createBillFetchMsg(traceId, mobileNumber, merchantId);
		logger.info("Request msg for mobileNumber:" + mobileNumber + " is " + msg) ;
		String executeFetchAndPayResponse = null;
		String aggrErrorCode = null;
		BillPaymentErrorCodeMap errorCodeMap =null;
		long start = System.currentTimeMillis();
		Map<String, Object> fetchBillLogDataMap = logRequestService.initValidationLogDataMap(mobileNumber, operatorId,
				33, "billdesk", LoggingConstants.ValidationCallType.POSTPAID_FETCH_BILL.getName());
		fetchBillLogDataMap.put(LoggingConstants.RAW_REQUEST, msg);
		try {
			executeFetchAndPayResponse = postBilldeskFetchPayRequestWithTimeout(connectionManagerConfig, msg, true);

			logger.info("Fetch Bill msg from BillDesk for mobileNumber:" + mobileNumber + " is "
					+ executeFetchAndPayResponse);
			aggrErrorCode = getErrorCodeFromRawResponse(executeFetchAndPayResponse, 23);
			 errorCodeMap = billReadDao.findBillPaymentInternalResponseCode(aggrErrorCode, PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
			logger.info("aggrErrorCode is " + aggrErrorCode);
		} catch (Exception e) {
			logger.error("Exception caught at fetchAndPay call is :", e);
			logger.info("Time Taken is" + (System.currentTimeMillis() - start));

		} finally {
			metricsClient.recordLatency("PostPaid", "bdk.FetchAndPay", System.currentTimeMillis() - start);
		}
		fetchBillLogDataMap.put(LoggingConstants.RAW_RESPONSE, executeFetchAndPayResponse);
		fetchBillLogDataMap.put(LoggingConstants.RESPONSE_TIMESTAMP, sdfLog.format(new Date()));
		fetchBillLogDataMap.put(LoggingConstants.RESPONSE_CODE, aggrErrorCode);
		
		logRequestService.logInTransactionValidation(fetchBillLogDataMap);
		return getFetchAndPayResponseBDK(msg, executeFetchAndPayResponse, 23, aggrErrorCode,errorCodeMap);
	}

	public FetchAndPayResultBDK getFetchAndPayResultBDK(HttpClient bdkHttpClient,
			String mobileNumber, Integer operatorId, String merchantId) throws ParseException {
		String traceId = billPayServiceRemote.getUniqueId();
//		Boolean isBBPAEnabled = true;
		String msg = null;
		if(appConfigService.isBBPSBillPaymentEnabled() && isBBPSEnabledMerchant(merchantId)) {
			logger.info("merchant is BBPS enabled");
			msg = createBillFetchMsgNew(traceId, mobileNumber, merchantId);
		}else {
			msg = createBillFetchMsg(traceId, mobileNumber, merchantId);;
		}
//		msg = createBillFetchMsg(traceId, mobileNumber, merchantId);
		logger.info("Request msg for mobileNumber:" + mobileNumber + " is " + msg) ;
		String executeFetchAndPayResponse = null;
		String aggrErrorCode = null;
		BillPaymentErrorCodeMap errorCodeMap =null;
		long start = System.currentTimeMillis();
		Map<String, Object> fetchBillLogDataMap = logRequestService.initValidationLogDataMap(mobileNumber, operatorId,
				33, "billdesk", LoggingConstants.ValidationCallType.POSTPAID_FETCH_BILL.getName());
		fetchBillLogDataMap.put(LoggingConstants.RAW_REQUEST, msg);
		try {
			executeFetchAndPayResponse = postBilldeskFetchPayRequestWithTimeout(bdkHttpClient, msg, true);

			logger.info("Fetch Bill msg from BillDesk for mobileNumber:" + mobileNumber + " is "
					+ executeFetchAndPayResponse);
			aggrErrorCode = getErrorCodeFromRawResponse(executeFetchAndPayResponse, 23);
			 errorCodeMap = billReadDao.findBillPaymentInternalResponseCode(aggrErrorCode, PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
			logger.info("aggrErrorCode is " + aggrErrorCode);
		} catch (Exception e) {
			logger.error("Exception caught at fetchAndPay call is :", e);
			logger.info("Time Taken is" + (System.currentTimeMillis() - start));

		} finally {
			metricsClient.recordLatency("PostPaid", "bdk.FetchAndPay", System.currentTimeMillis() - start);
		}
		fetchBillLogDataMap.put(LoggingConstants.RAW_RESPONSE, executeFetchAndPayResponse);
		fetchBillLogDataMap.put(LoggingConstants.RESPONSE_TIMESTAMP, sdfLog.format(new Date()));
		fetchBillLogDataMap.put(LoggingConstants.RESPONSE_CODE, aggrErrorCode);
		
		logRequestService.logInTransactionValidation(fetchBillLogDataMap);
		return getFetchAndPayResponseBDK(msg, executeFetchAndPayResponse, 23, aggrErrorCode,errorCodeMap);
	}
	
	private Boolean isBBPSEnabledMerchant(String merchantId) {
		JSONObject json = appConfigService.getCustomProp(AppConfigService.BBPS_ENABLED_BILLERS);
    	logger.info("json : " + json.toJSONString());
    	if(json.containsKey("merchantList")) {
            String operatorList = String.valueOf(json.get("merchantList"));
            String[] splittedParams = StringUtils.splitPreserveAllTokens(operatorList, ",");
            Set<String> billersSet = new HashSet<String>(Arrays.asList(splittedParams));
            if(billersSet.contains(merchantId)) {
            	return true;
            }
    	}
    	return false;
	}

	private String postBilldeskFetchPayRequestWithTimeout(HttpClient bdkHttpClient,
			String rawRequest, boolean shouldTimeout) {
		logger.info("Posting " + rawRequest + " to billdesk. " + fcProperties.getBdkUrl());
		CloseableHttpResponse httpResponse = null;
		try {
			CloseableHttpClient httpClient = CustomHttpClientBuilder.getCustomHttpClient(fcProperties.getBdkUrl(), 6000, 10000);
            HttpPost httpPost = new HttpPost(fcProperties.getBdkUrl());
            
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair(BillPayBilldeskUtil.MESSAGE_PARAM_NAME, rawRequest));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            httpResponse=httpClient.execute(httpPost);
            Integer statusCode = httpResponse.getStatusLine().getStatusCode();
			String rawResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			
			logger.info("Raw Response from billdesk : " + rawResponse);

			if (statusCode.equals(HttpStatus.SC_OK)) {
				return rawResponse.trim();
			} else {
				logger.info("Got HTTP Response Code : " + statusCode + " for bdk post request " + rawRequest);
				return null;
			}
		} catch (Exception e) {
			logger.error("Got Exception while recharging for bdk request : " + rawRequest, e);
			return null;
		} finally {
			try {
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException e) {	
			}
		}
	}
	
	private String postBilldeskFetchPayRequestWithTimeout(HttpConnectionManagerParams connectionManagerConfig,
			String rawRequest, boolean shouldTimeout) {
		logger.info("Posting " + rawRequest + " to billdesk. " + fcProperties.getBdkUrl());
		
		CloseableHttpResponse httpResponse = null;
		
		try {
			
			CloseableHttpClient httpClient = CustomHttpClientBuilder.getCustomHttpClient(fcProperties.getBdkUrl(), 6000, 10000);
            HttpPost httpPost = new HttpPost(fcProperties.getBdkUrl());
            
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair(BillPayBilldeskUtil.MESSAGE_PARAM_NAME, rawRequest));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            httpResponse=httpClient.execute(httpPost);
            Integer statusCode = httpResponse.getStatusLine().getStatusCode();
			String rawResponse = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			
			logger.info("Raw Response from billdesk : " + rawResponse);

			if (statusCode.equals(HttpStatus.SC_OK)) {
				return rawResponse.trim();
			} else {
				logger.info("Got HTTP Response Code : " + statusCode + " for bdk post request " + rawRequest);
				return null;
			}
		} catch (Exception e) {
			logger.error("Got Exception while recharging for bdk request : " + rawRequest, e);

			return null;
		} finally {
			try {
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException e) {	
			}
		}
	}

	private FetchAndPayResultBDK getFetchAndPayResponseBDK(String rawRequest, String rawResponse, int expectedLength,
			String aggrErrorCode,BillPaymentErrorCodeMap errorCodeMap) throws ParseException {
		FetchAndPayResultBDK fetchAndPayResult = new FetchAndPayResultBDK();
		fetchAndPayResult.setRawRequest(rawRequest);
		if (FCUtil.isEmpty(aggrErrorCode)) {
			fetchAndPayResult.setErrorCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
			fetchAndPayResult.setErrorMessage(RechargeConstants.FETCH_BILL_FAILURE_GENERIC_ERROR_MSG);
		} else {
			if (!aggrErrorCode.equals(BDK_SUCCESS) && errorCodeMap != null) {
				fetchAndPayResult.setErrorCode(errorCodeMap.getBillPaymentInternalResponseCode());
				fetchAndPayResult.setErrorMessage(errorCodeMap.getBillPaymentInyternalMessage());
			} else {
				fetchAndPayResult.setErrorCode(aggrErrorCode);
			}
		}
		if (rawResponse == null) {
			logger.error("Did not get a response from aggregator.");
			return fetchAndPayResult;
		}
		if (validateChecksum(rawResponse)) {
			fetchAndPayResult.setResponse(rawResponse);
			String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
			if (splittedParams.length != expectedLength) {
				logger.error("Expected fields : " + expectedLength + ", Actual fields : " + splittedParams.length
						+ " Raw response : " + rawResponse);
				return fetchAndPayResult;
			}
			if (!splittedParams[6].equals(VALID_RESPONSE) || !splittedParams[7].equals("0")
					|| !splittedParams[8].equals("0")) {
				logger.info("Invalid Response : " + rawResponse);
				fetchAndPayResult.setBillerId(splittedParams[9]);
			} else {
				fetchAndPayResult.setBillerId(splittedParams[9]);
				if (splittedParams[13] != null && !splittedParams[13].equals(DEFAULT_DETAILS)) {
					fetchAndPayResult.setBillNumber(splittedParams[13]);
				}
				if (splittedParams[14] != null && !splittedParams[14].equals(DEFAULT_DETAILS)) {
					fetchAndPayResult.setBillDate(BillPayConstants.dateViewFormat
							.format(BillPayConstants.billdeskDateFormat.parse(splittedParams[14])));
				}
				if (splittedParams[15] != null && !splittedParams[15].equals(DEFAULT_DETAILS)) {
					fetchAndPayResult.setBillDueDate(BillPayConstants.dateViewFormat
							.format(BillPayConstants.billdeskDateFormat.parse(splittedParams[15])));
				}
				if (splittedParams[16] != null && !splittedParams[16].equals(DEFAULT_DETAILS)) {
					Float billAmount = Float.parseFloat(splittedParams[16]);
					Float zero = Float.parseFloat("0");
					fetchAndPayResult.setBillAmount(zero.compareTo(billAmount) < 0 ? billAmount : zero);
				}
				fetchAndPayResult.setPayWithoutBill(splittedParams[17].equals(VALID_RESPONSE) ? true : false);
				fetchAndPayResult.setPartialPayment(splittedParams[18].equals(VALID_RESPONSE) ? true : false);
				fetchAndPayResult.setInfo(getInfo(splittedParams));
			}
			return fetchAndPayResult;
		} else {
			logger.error("Checksum failed for " + rawResponse, new InvalidChecksumException(rawResponse));
			return fetchAndPayResult;
		}
	}

	private List<HashMap<String, String>> getInfo(String[] splittedParams) {
		List<HashMap<String, String>> info = new ArrayList<>();
		int i = 0;
		for (i = 0; i < 3; i++) {
			boolean isInfoValid = isInfoValidForIndex(splittedParams, 10 + i);
			if (!isInfoValid) {
				break;
			}
			HashMap<String, String> data = new HashMap<>();
			data.put("value", splittedParams[10 + i]);
			info.add(i, data);
		}
		String extraAuth = splittedParams[21];
		String[] splitAuth = StringUtils.splitPreserveAllTokens(extraAuth, AUTHENTICATOR_SEPARATOR);
		if (splitAuth.length == 2) {
			for (int j = 0; i < 5 && j < 2; i++, j++) {

				boolean isInfoValid = isInfoValidForIndex(splitAuth, j);
				if (!isInfoValid) {
					break;
				}
				HashMap<String, String> data = new HashMap<>();
				data.put("value", splitAuth[j]);
				info.add(i, data);
			}
		}
		return info;
	}

	private boolean isInfoValidForIndex(String[] splittedParams, int index) {
		try {
			return splittedParams[index] != null ? true : false;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean validateChecksum(String rawResponse) {
		String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
		if (splittedParams != null && splittedParams.length > 0) {
			String checkSumField = splittedParams[splittedParams.length - 1];

			String[] array = Arrays.copyOf(splittedParams, splittedParams.length - 1);

			String receivedMsg = StringUtils.join(array, MESSAGE_PARAM_TILDE_SEPARATOR);
			String calculatedChecksum = String.valueOf(createChecksum(receivedMsg, fcProperties.getBdkWorkingKey()));
			if (calculatedChecksum.equals(checkSumField)) {
				return true;
			}
		}
		return false;
	}

	private static long createChecksum(String msg, String bdkWorkingKey) {
		String msgToBeHashed = msg + MESSAGE_PARAM_TILDE_SEPARATOR + bdkWorkingKey;
		byte[] msgByte = msgToBeHashed.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(msgByte, 0, msgByte.length);
		return checksum.getValue();
	}

	private String getErrorCodeFromRawResponse(String rawResponse, int expectedLength) {
		if (rawResponse == null) {
			return null;
		}
		if (validateChecksum(rawResponse)) {
			String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
			if (splittedParams.length == expectedLength) {
				return splittedParams[7];
			}
		}
		return null;
	}

	private String createBillFetchMsg(String traceId, String mobileNumber, String merchantId) {
		StringBuilder msg = new StringBuilder();
		String userId = "1000";
		msg.append(generateMsgHeader(BillPayConstants.BDK_ONLINE_BILL_FETCH_REQUEST_CODE, traceId, userId));

		String info2 = DEFAULT_DETAILS;
		String info3 = DEFAULT_DETAILS;
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(merchantId).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(mobileNumber).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(info2)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(info3).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS);
		long checksum = createChecksum(msg.toString(), fcProperties.getBdkWorkingKey());
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(checksum);

		return msg.toString();
	}

	private String createBillFetchMsgNew(String traceId, String mobileNumber, String merchantId) {
		StringBuilder msg = new StringBuilder();
		String userId = "1000";
		msg.append(generateMsgHeaderNew(BillPayConstants.BDK_ONLINE_BILL_FETCH_REQUEST_CODE, traceId, userId));

		String info2 = DEFAULT_DETAILS;
		String info3 = DEFAULT_DETAILS;
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(merchantId).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(mobileNumber).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(info2)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(info3).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append("NA!NA!" + mobileNumber).append(MESSAGE_PARAM_TILDE_SEPARATOR).append("10.10.10.10!01-23-45-67-89-ab")
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS);
		long checksum = createChecksum(msg.toString(), fcProperties.getBdkWorkingKey());
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(checksum);

		return msg.toString();
	}

	private String generateMsgHeader(String messageCode, String traceId, String userId) {
		StringBuilder headerMsg = new StringBuilder();
		String timestamp = sdf.format(new Date());
		headerMsg.append(messageCode).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(traceId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(SOURCE_ID).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(timestamp).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId);
		return headerMsg.toString();
	}
	
	private String generateMsgHeaderNew(String messageCode, String traceId, String userId) {
		StringBuilder headerMsg = new StringBuilder();
		String timestamp = sdf.format(new Date());
		headerMsg.append(messageCode).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(traceId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(SOURCE_ID).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(timestamp).append(MESSAGE_PARAM_TILDE_SEPARATOR).append("BD01BD05MOB000000001!INT!NA!NA!NA")
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId);
		return headerMsg.toString();
	}
	
	
	public static String getCardOptionForBDK(Integer paymentTypeFromWebapp, String paymentOptionFromWebapp) {

        String cardNumber = DEFAULT_DETAILS;
        if (paymentTypeFromWebapp != null && paymentOptionFromWebapp != null) {
            if (paymentTypeFromWebapp == 3) {
                logger.info("paymentOption as per Webapp is:"+paymentOptionFromWebapp);
                if (FCBDKShortFormMap.containsKey(paymentOptionFromWebapp)) {
                    cardNumber = FCBDKShortFormMap.get(paymentOptionFromWebapp);
                    logger.info("cardNumber fro Map is:"+FCBDKShortFormMap.get(paymentOptionFromWebapp));
                }
            }
        }
        return cardNumber;
    }
	
	  public static String getCardTypeForBDK(Integer paymentTypeFromWebapp, String paymentOptionFromWebapp) {
	        String cardType = PAYMENT_TYPE_WALLET;
	        if (paymentTypeFromWebapp != null && paymentOptionFromWebapp != null) {
	            switch (paymentTypeFromWebapp) {
	                case 1:
	                    cardType = "CC";
	                    break;
	                case 2:
	                    cardType ="DC";
	                    break;
	                case 3:
	                    cardType = "NB";
	                    break;
	                case 6:
	                    cardType = "WP";
	                    break;
	                default:
	                    cardType = PAYMENT_TYPE_WALLET;
	            }
	        }
	        logger.info("Card Type for bdk is : [" + cardType + "]");
	        return cardType;
	    }
	  
	  public static String getPaymentTypeBDK(String cardType, String cardOption) {
		  logger.info("Inside get payment type method for BDK. CardType is : [" + cardType + "] and CardOption is : [" + cardOption + "]");
			StringBuilder paymentType = new StringBuilder();
			paymentType.append(cardType).append(AUTHENTICATOR_SEPARATOR).append(cardOption);
			logger.info("Payment type for bdk is : [" + paymentType + "]");
			return paymentType.toString();
		}
}
