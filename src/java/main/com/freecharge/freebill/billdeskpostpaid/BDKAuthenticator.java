package com.freecharge.freebill.billdeskpostpaid;

import java.util.HashMap;
import java.util.List;

import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;

public class BDKAuthenticator {

	private String additionalInfo1;
	private String additionalInfo2;
	private String additionalInfo3;
	private String additionalInfo4;
	private String additionalInfo5;

	public BDKAuthenticator(FetchAndPayResultBDK fpr) {

		List<HashMap<String, String>> infoList = fpr.getInfo();
		if (infoList != null && infoList.size() > 0) {
			this.setAdditionalInfo1(infoList.get(0).get("value"));
			if (infoList.size() > 1) {
				this.setAdditionalInfo2(infoList.get(1).get("value"));
			}
			if (infoList.size() > 2) {
				this.setAdditionalInfo3(infoList.get(2).get("value"));
			}
			if (infoList.size() > 3) {
				this.setAdditionalInfo4(infoList.get(3).get("value"));
			}
			if (infoList.size() > 4) {
				this.setAdditionalInfo5(infoList.get(4).get("value"));
			}
		}
	}

	public String getAdditionalInfo1() {
		return additionalInfo1;
	}

	public void setAdditionalInfo1(String additionalInfo1) {
		this.additionalInfo1 = additionalInfo1;
	}

	public String getAdditionalInfo2() {
		return additionalInfo2;
	}

	public void setAdditionalInfo2(String additionalInfo2) {
		this.additionalInfo2 = additionalInfo2;
	}

	public String getAdditionalInfo3() {
		return additionalInfo3;
	}

	public void setAdditionalInfo3(String additionalInfo3) {
		this.additionalInfo3 = additionalInfo3;
	}

	public String getAdditionalInfo4() {
		return additionalInfo4;
	}

	public void setAdditionalInfo4(String additionalInfo4) {
		this.additionalInfo4 = additionalInfo4;
	}

	public String getAdditionalInfo5() {
		return additionalInfo5;
	}

	public void setAdditionalInfo5(String additionalInfo5) {
		this.additionalInfo5 = additionalInfo5;
	}

}
