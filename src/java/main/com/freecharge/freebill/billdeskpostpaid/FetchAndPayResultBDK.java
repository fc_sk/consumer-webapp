package com.freecharge.freebill.billdeskpostpaid;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;

public class FetchAndPayResultBDK extends FetchAndPayResult implements Serializable {

	private static final long serialVersionUID = 1L;
	private String rawRequest;
	private String response;

	public String getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "FetchAndPayResultBDK [rawRequest=" + rawRequest + ", response=" + response + ", errorCode="
				+ super.getErrorCode() + ", errorMessage=" + super.getErrorMessage() + ", billerId="
				+ super.getBillerId() + ", billNumber=" + super.getBillNumber() + ", billDate=" + super.getBillDate()
				+ ", billDueDate=" + super.getBillDueDate() + ", billAmount=" + super.getBillAmount() + ", info="
				+ super.getInfo() + "]";
	}

}
