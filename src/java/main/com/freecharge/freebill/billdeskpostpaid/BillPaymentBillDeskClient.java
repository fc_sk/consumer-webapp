package com.freecharge.freebill.billdeskpostpaid;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.BillValidationAPIResponse;
import com.freecharge.freebill.billdesk.IBillValidationRequest;
import com.freecharge.freebill.billdesk.RawBillValidationAPIResponse;
import com.freecharge.freebill.common.BillPaymentCommonApiResponse;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.common.IBillPaymentInterface;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillDeskBillPaymentStatus;
import com.freecharge.infrastructure.billpay.fatals.InvalidChecksumException;
import com.freecharge.infrastructure.billpay.service.aggregator.AggregatorResponse;
import com.freecharge.infrastructure.billpay.service.aggregator.BillPayBilldeskUtil;
import com.freecharge.infrastructure.billpay.util.BillPayConstants;
import com.freecharge.infrastructure.billpay.util.BillPayUtil;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.web.webdo.BBPSTransactionDetails;

public class BillPaymentBillDeskClient extends AggregatorService implements IBillPaymentInterface {

	private static String SOURCE_ID = "FRC";
	private static final String MESSAGE_PARAM_TILDE_SEPARATOR = "~";
	private static final String AUTHENTICATOR_SEPARATOR = "!";
	public final static String DEFAULT_DETAILS = "NA";
	private static String PAYMENT_TYPE_PNY = "PNY";
	public static final String MESSAGE_PARAM_NAME = "msg";
	private static final String VALID_RESPONSE = "Y";

	private static final Object BDK_SUCCESS = "0";

	private static Logger logger = Logger.getLogger(BillPaymentBillDeskClient.class);

	private HttpConnectionManagerParams connectionManagerConfig;

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

	static SimpleDateFormat sdfBill = new SimpleDateFormat("yyyyMMdd");
	
	private HttpClient bdkHttpClient;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	MetricsClient metricsClient;
	
	@Autowired
	BillPaymentBillDeskUtil billdeskUtil;
	
	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	private BillPaymentService billPayService;
	
	@Autowired
	private BBPSDetailsDao bbpsDetailsDao;
	
	private static final String MESSAGE_PARAM_COMMA_SEPARATOR = ",";

	@Override
	public BillPaymentCommonApiResponse submitPaymentRequest( String aggregatorTxnId,
			TxnHomePage thp, BillPostpaidMerchant billPostpaidMerchant, Integer paymentType, String paymentOption) throws Exception {

		BillPaymentCommonApiResponse response = null;
		String requestParam = null;
		FetchAndPayResultBDK fpr = null;
		try {
			BillTxnHomePage billTxnHomePage = thp.getBillTxnHomePage();
			BigDecimal amount = billTxnHomePage.getBillAmount();
			String mobileNumber = billTxnHomePage.getPostpaidNumber();
			Integer operatorId = billPostpaidMerchant.getFkOperatorMasterId();
			String merchantId = billPostpaidMerchant.getBilldeskMerchantId();
			
			
            
			fpr = billdeskUtil.getFetchAndPayResultBDK(bdkHttpClient,mobileNumber, operatorId, merchantId);
			logger.info("Fetch And Pay Request is " + requestParam);
			logger.info("FetchAndPayResultBDK is " + fpr.toString());
			if (BDK_SUCCESS.equals(fpr.getErrorCode()) && isAmountValidated(fpr,amount,operatorId)) {
				BDKResponseDTO responseDTO =  null;
				try {
					metricsClient.recordEvent("PostPaid", "bdk.FetchAndPay", "Success");
				    responseDTO = executeBillPay(fpr, amount, aggregatorTxnId, paymentType, paymentOption, merchantId);
					
					AggregatorResponse agResponse = responseDTO.getResponse();
					String[] splittedParamsAg = StringUtils.splitPreserveAllTokens(agResponse.getRawResponse(), MESSAGE_PARAM_TILDE_SEPARATOR);
					String bbpsReferenceNumber = "";
					if(splittedParamsAg!=null && splittedParamsAg.length>14) {
						 bbpsReferenceNumber = splittedParamsAg[14];

					}
					logger.info("[AnantQa] merchantId: " + merchantId);
					if(appConfigService.isBBPSBillPaymentEnabled() && isBBPSEnabledMerchant(merchantId)) {
						String[] splittedParams = StringUtils.splitPreserveAllTokens(fpr.getResponse(), MESSAGE_PARAM_TILDE_SEPARATOR);
						String messageDetails = null;
						BBPSTransactionDetails bbpsDetails = new BBPSTransactionDetails();
						if(splittedParams.length>20 && splittedParams[20] != null) {
							messageDetails = splittedParams[20];
							bbpsDetails.setBillDueDate(splittedParams[15]);
							
				        }
						
						
						//TODO Need to check this flow for post paid as  there in no message detials for post paid. Need to insert bbps reference number along with lookup id and isBBPSActive flag;
						bbpsDetails.setCustomerAccountNumber(mobileNumber);
						bbpsDetails.setCustomerName(null);
						bbpsDetails.setMessageDetails(messageDetails);
						bbpsDetails.setBbpsReferenceNumber(bbpsReferenceNumber);
						bbpsDetails.setLookupId(thp.getLookupId());
						bbpsDetails.setIsBbpsActive(true);

						bbpsDetailsDao.insertBBPSDetails(bbpsDetails);
					}
					logger.info("AgResponse for aggregatorTxnId:" + aggregatorTxnId + " is " + agResponse.toString());
					response = convertToCommonApiResponse(agResponse, aggregatorTxnId, responseDTO.getTraceId());
					logger.info(
							"Common Api Response for aggregatorTxnId:" + aggregatorTxnId + " is " + response);
					return response;
				
				}catch(Exception ex) {
					logger.error("Exception caught while billpayment :"+aggregatorTxnId+" is ",ex);
					return createDummyBillPaymentResponse(aggregatorTxnId, responseDTO);

				}
			}
		} catch (Exception e) {
			logger.error("Exception caught for aggregartorTxnId:"+aggregatorTxnId+" is ",e);
			return createDummyCommonApiResponse(aggregatorTxnId, requestParam);
		}
		response = createDummyCommonApiResponse(aggregatorTxnId, requestParam);
		response.setRequestparam(fpr.getRawRequest());
		if(fpr.getResponse()!=null){
			response.setRowResponse(fpr.getResponse());		
		}else{
			response.setRowResponse("Fetch Bill Response Not Received / Fetch Bill Validation Failure");
		}		
		response.setSuccess(false);
		return response;
	}
	
	private Boolean isBBPSEnabledMerchant(String merchantId) {
		JSONObject json = appConfigService.getCustomProp(AppConfigService.BBPS_ENABLED_BILLERS);
    	logger.info("json : " + json.toJSONString());
    	if(json.containsKey("merchantList")) {
            String operatorList = String.valueOf(json.get("merchantList"));
            String[] splittedParams = StringUtils.splitPreserveAllTokens(operatorList, MESSAGE_PARAM_COMMA_SEPARATOR);
            Set<String> billersSet = new HashSet<String>(Arrays.asList(splittedParams));
            if(billersSet.contains(merchantId)) {
            	return true;
            }
    	}
    	return false;
	}

	private boolean isAmountValidated(FetchAndPayResultBDK fpr, BigDecimal amount, Integer operatorId) {
		if (fpr == null) {
			logger.info("Fetch Bill Object is null");
			return false;
		}

		if (appConfigService.isPostPaidFetchBillSupportEnabled()) {
			if (billPayService.isFetchBillOperator(String.valueOf(operatorId))) {
				if (fpr.getBillAmount() != amount.floatValue()) {
					logger.info(
							"Amount mismatch in FetchBill Amount " + fpr.getBillAmount() + " and UI amount " + amount);
					return false;
				}
			}
		}
		return true;
	}

	private BillPaymentCommonApiResponse createDummyCommonApiResponse(String aggregatorTxnId, String requestParam) {
		BillPaymentCommonApiResponse dummyResponse = new BillPaymentCommonApiResponse();
		if (!FCUtil.isEmpty(requestParam)) {
			dummyResponse.setRequestparam(requestParam);
		} else {
			dummyResponse.setRequestparam("Not known");
		}
		dummyResponse.setTxnId(aggregatorTxnId);
		dummyResponse.setRowResponse("Exception raised " + aggregatorTxnId);
		dummyResponse.setBillPaymentStatusCode(null);
		dummyResponse.setAuthDetail("BDK_EXCEPTION");
		dummyResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
		dummyResponse.isSuccess(null);
		dummyResponse.setCreatedTime(new Date());
		metricsClient.recordEvent("PostPaid", "bdk.ExecuteBillPay", "Exception");
		return dummyResponse;
	}
	
	private BillPaymentCommonApiResponse createDummyBillPaymentResponse(String aggregatorTxnId,
			BDKResponseDTO responseDTO) {
		logger.info("BDKResponseDTO : "+ responseDTO);

		BillPaymentCommonApiResponse dummyResponse = new BillPaymentCommonApiResponse();
		if (responseDTO != null && responseDTO.getResponse() != null) {
			dummyResponse.setRequestparam(responseDTO.getResponse().getRawRequest());
			if (StringUtils.isEmpty(responseDTO.getResponse().getRawResponse())) {
				dummyResponse.setRowResponse(responseDTO.getResponse().getRawResponse());
			} else {
				dummyResponse.setRowResponse("Exception raised " + aggregatorTxnId);
			}
			dummyResponse.setAuthDetail(responseDTO.getTraceId());
		} else {
			dummyResponse.setRequestparam("Not known");
			dummyResponse.setRowResponse("Exception raised " + aggregatorTxnId);
			dummyResponse.setAuthDetail("BDK_EXCEPTION");
		}
		dummyResponse.setTxnId(aggregatorTxnId);
		dummyResponse.setBillPaymentStatusCode(null);
		dummyResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
		dummyResponse.isSuccess(null);
		dummyResponse.setCreatedTime(new Date());
		metricsClient.recordEvent("PostPaid", "bdk.ExecuteBillPay", "Exception");
		logger.info("createDummyBillPaymentResponse : "+ dummyResponse);
		return dummyResponse;
	}

	private BillPaymentCommonApiResponse convertToCommonApiResponse(AggregatorResponse agResponse,
			String aggregatorTxnId, String traceId) {
		BillPaymentCommonApiResponse billPaymentCommonApiResponse = new BillPaymentCommonApiResponse();
		billPaymentCommonApiResponse.setRequestparam(agResponse.getRawRequest());
		billPaymentCommonApiResponse.setRowResponse(agResponse.getRawResponse());
		billPaymentCommonApiResponse.setTxnId(aggregatorTxnId);
		billPaymentCommonApiResponse.setBillPaymentGateway(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
		billPaymentCommonApiResponse.setAuthDetail(traceId);
		billPaymentCommonApiResponse.setGatewayReferenceNumber(agResponse.getAggregatorTransId());
		billPaymentCommonApiResponse.setCreatedTime(new Date());
		if (agResponse.getFcResponseCode() != null
				&& agResponse.getFcResponseCode().equals(BillPayConstants.BILLPAY_FC_SUCCESS_CODE)) {
			metricsClient.recordEvent("PostPaid", "bdk.ExecuteBillPay", "Success");
			billPaymentCommonApiResponse.setSuccess(true);
		} else if (agResponse.getFcResponseCode() != null
				&& agResponse.getFcResponseCode().equals(BillPayConstants.BILLPAY_FC_UNDER_PROCESS_CODE)) {
			metricsClient.recordEvent("PostPaid", "bdk.ExecuteBillPay", "UnderProcess");
			billPaymentCommonApiResponse.setSuccess(null);
		} else {
			metricsClient.recordEvent("PostPaid", "bdk.ExecuteBillPay", "Failure");
			billPaymentCommonApiResponse.setSuccess(false);
		}
		billPaymentCommonApiResponse.setBillPaymentStatusCode(agResponse.getResponseCode());

		return billPaymentCommonApiResponse;
	}

	private BDKResponseDTO executeBillPay(FetchAndPayResultBDK fpr, BigDecimal amount, String aggregatorTxnId, Integer paymentType, String paymentOption, String merchantId) {
		String msg = null;
		BDKResponseDTO responseDTO=new BDKResponseDTO();
		String traceId=aggregatorTxnId;
        try {
        	if(appConfigService.isBBPSBillPaymentEnabled() && isBBPSEnabledMerchant(merchantId)) {
    			msg = getBillPaymentRequestMsgNew(fpr, amount, paymentType, paymentOption);
    		}else {
    			msg = getBillPaymentRequestMsg(fpr, amount, paymentType, paymentOption);
    		}
    		logger.info("Execute BillPay Request for aggregatorTxnId:" + aggregatorTxnId + " is " + msg);
        	String[] splittedParams = StringUtils.splitPreserveAllTokens(msg,
					MESSAGE_PARAM_TILDE_SEPARATOR);	
        	if(splittedParams!=null && splittedParams.length>3)  {
        		traceId = splittedParams[2]+splittedParams[1];
        	}
        }catch(Exception ex) {
        	logger.error("Exception while finding traceId");
	
        }
        logger.info("traceId ######## : "+traceId);
		responseDTO.setTraceId(traceId);
		String executeBillPayRawResponse = null;
		long start = System.currentTimeMillis();
		String aggrErrorCode = null;
		try {
			executeBillPayRawResponse = postBilldeskRequestWithTimeout(msg, aggregatorTxnId, true);
			logger.info("Execute BillPay Response for aggregatorTxnId:" + aggregatorTxnId + " is "
					+ executeBillPayRawResponse.toString());
			aggrErrorCode = getErrorCodeFromRawResponse(executeBillPayRawResponse, 16);
			logger.info("aggrErrorCode is" + aggrErrorCode);
		} catch (ConnectTimeoutException | SocketException e) {
			logger.error("ConnectTimeoutException #####  "+ e);
		} catch (Exception e) {
			logger.error("Exception on billpayment ", e);
		} finally {
			metricsClient.recordLatency("PostPaid", "bdk.ExecuteBillPay", System.currentTimeMillis() - start);
		}
		AggregatorResponse agRes = getBillPayAggregatorResponse(msg, executeBillPayRawResponse, 16, 10, aggrErrorCode);
		
		if (agRes != null && StringUtils.isEmpty(agRes.getAggregatorTransId())) {
			agRes.setAggregatorTransId(responseDTO.getTraceId());
		}
		responseDTO.setResponse(agRes);
		return responseDTO;
	}

	private AggregatorResponse getBillPayAggregatorResponse(String rawRequest, String executeBillPayRawResponse,
			Integer expectedColumn, Integer billPaymentStatusIndex, String aggrErrorCode) {
		AggregatorResponse aggregatorResponse = new AggregatorResponse(BillPayConstants.BILLPAY_FC_UNDER_PROCESS_CODE);
		aggregatorResponse.setRawRequest(rawRequest);
		aggregatorResponse.setRawResponse(executeBillPayRawResponse);
		aggregatorResponse.setAggregator(BillPayConstants.AGGREGATOR_NAME_BILLDESK);

		if (executeBillPayRawResponse == null) {
			logger.error("Did not get a response from aggregator.");
			return aggregatorResponse;
		}
		try {
			if (validateChecksum(executeBillPayRawResponse)) {
				String[] splittedParams = StringUtils.splitPreserveAllTokens(executeBillPayRawResponse,
						MESSAGE_PARAM_TILDE_SEPARATOR);
				if (splittedParams.length != expectedColumn) {
					logger.error("Expected fields : " + expectedColumn + " , Actual fields : " + splittedParams.length
							+ " Raw response : " + executeBillPayRawResponse);
					return aggregatorResponse;
				}
				if (!splittedParams[6].equals(VALID_RESPONSE) || !splittedParams[7].equals("0")
						|| !splittedParams[8].equals("0")) {
					logger.info("Failure Response : " + executeBillPayRawResponse);
				}

				String paymentStatus = splittedParams[billPaymentStatusIndex];

				if (paymentStatus == null) {
					aggregatorResponse.setFcResponseCode(BillPayConstants.BILLPAY_FC_UNDER_PROCESS_CODE);
					return aggregatorResponse;
				}

				aggregatorResponse.setResponseCode(splittedParams[7]);
				aggregatorResponse.setResponseMessage(splittedParams[8]);
				aggregatorResponse.setAggregatorTransId(getAggrTxnId(rawRequest, executeBillPayRawResponse));

				if (paymentStatus.equals(BillDeskBillPaymentStatus.PAID.name())) {
					aggregatorResponse.setFcResponseCode(BillPayConstants.BILLPAY_FC_SUCCESS_CODE);
				} else {
					if (FCUtil.isEmpty(aggrErrorCode) || (!aggrErrorCode.equals(BDK_SUCCESS))) {
						aggregatorResponse.setFcResponseCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);
					}
				}
				return aggregatorResponse;
			} else {
				logger.error("Checksum failed for " + executeBillPayRawResponse,
						new InvalidChecksumException(executeBillPayRawResponse));
				return aggregatorResponse;
			}
		} catch (Exception ex) {
			logger.error("Exception Parsing BDK response : " + ex);
			return aggregatorResponse;
		}
	}

	private String getAggrTxnId(String rawRequest, String executeBillPayRawResponse) {
		String[] requestSplit = StringUtils.splitPreserveAllTokens(rawRequest, MESSAGE_PARAM_TILDE_SEPARATOR);
		String[] responseSplit = StringUtils.splitPreserveAllTokens(executeBillPayRawResponse,
				MESSAGE_PARAM_TILDE_SEPARATOR);
		if (requestSplit.length >= 1 && requestSplit[0].equals(BillPayConstants.BDK_PAYMENT_REQUEST_CODE)) {
			if (responseSplit.length >= 10) {
				return responseSplit[9];
			}
		} else if (requestSplit.length >= 1 && requestSplit[0].equals(BillPayConstants.BDK_TXN_STATUS_REQUEST_CODE)) {
			if (requestSplit.length >= 7) {
				return requestSplit[6];
			}
		}
		logger.error("Aggregator Txn Id is going to be set as null for " + rawRequest);
		return null;
	}

	private String getBillPaymentRequestMsg(FetchAndPayResultBDK fpr, BigDecimal amount, Integer paymentTypeFromKP, String paymentOption) {
		StringBuilder msg = new StringBuilder();
		BDKAuthenticator auth = new BDKAuthenticator(fpr);
		String authenticator = generateAuthenticatorField(auth.getAdditionalInfo1(), auth.getAdditionalInfo2(),
				auth.getAdditionalInfo3(), auth.getAdditionalInfo4(), auth.getAdditionalInfo5());
		String timestamp = sdfBill.format(new Date());
		String billerAccountId = DEFAULT_DETAILS;
		String billDueDate = convertToBdkDateFormat(fpr.getBillDueDate(), timestamp);
		String billDate = convertToBdkDateFormat(fpr.getBillDate(), timestamp);
		String billNumber = BillPayUtil.isEmpty(fpr.getBillNumber()) ? DEFAULT_DETAILS : fpr.getBillNumber();
		String billId = DEFAULT_DETAILS;
		
		logger.info("Payment type from KP is : [" + paymentTypeFromKP + "] and payment option from KP is : ["+ paymentOption + "]");
		String cardType = BillPaymentBillDeskUtil.getCardTypeForBDK(paymentTypeFromKP, paymentOption);
	    String cardOption = BillPaymentBillDeskUtil.getCardOptionForBDK(paymentTypeFromKP,paymentOption);
	    String bdkPaymentType = BillPaymentBillDeskUtil.getPaymentTypeBDK(cardType,cardOption);
		String paymentType = PAYMENT_TYPE_PNY;
		String userId = "1000";
		String traceId = "5" + billPayServiceRemote.getUniqueId();
		msg.append(generateMsgHeader(BillPayConstants.BDK_PAYMENT_REQUEST_CODE, traceId, userId));
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(fpr.getBillerId()).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(billerAccountId).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(authenticator).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(billId).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billDueDate)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billDate).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(billNumber).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(String.format("%.2f", amount))
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(paymentType).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(traceId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(bdkPaymentType).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS);

		long checksum = createChecksum(msg.toString(), fcProperties.getBdkWorkingKey());
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(checksum);
		return msg.toString();
	}
	
	
	private String getBillPaymentRequestMsgNew(FetchAndPayResultBDK fpr, BigDecimal amount, Integer paymentTypeFromKP, String paymentOption) {
		StringBuilder msg = new StringBuilder();
		BDKAuthenticator auth = new BDKAuthenticator(fpr);
		String authenticator = generateAuthenticatorField(auth.getAdditionalInfo1(), auth.getAdditionalInfo2(),
				auth.getAdditionalInfo3(), auth.getAdditionalInfo4(), auth.getAdditionalInfo5());
		String timestamp = sdfBill.format(new Date());
		String billerAccountId = DEFAULT_DETAILS;
		String billDueDate = convertToBdkDateFormat(fpr.getBillDueDate(), timestamp);
		String billDate = convertToBdkDateFormat(fpr.getBillDate(), timestamp);
		String billNumber = BillPayUtil.isEmpty(fpr.getBillNumber()) ? DEFAULT_DETAILS : fpr.getBillNumber();
		String billId = DEFAULT_DETAILS;
		
		logger.info("Payment type from KP is : [" + paymentTypeFromKP + "] and payment option from KP is : ["+ paymentOption + "]");
		String cardType = BillPaymentBillDeskUtil.getCardTypeForBDK(paymentTypeFromKP, paymentOption);
	    String cardOption = BillPaymentBillDeskUtil.getCardOptionForBDK(paymentTypeFromKP,paymentOption);
	    String bdkPaymentType = BillPaymentBillDeskUtil.getPaymentTypeBDK(cardType,cardOption);
		String paymentType = PAYMENT_TYPE_PNY;
		String userId = "1000"; //Anant TODO get the correct userId for bbps postpaid recharges
		String traceId = "5" + billPayServiceRemote.getUniqueId();
		String[] splittedParams = StringUtils.splitPreserveAllTokens(fpr.getResponse(), MESSAGE_PARAM_TILDE_SEPARATOR);
		String messageDetails = null;
		if(splittedParams.length>20 && splittedParams[20] != null) {
			messageDetails = splittedParams[20];
        }
		Date now = new Date();
		Long timeStamp = now.getTime();
		String randomBankReferenceNumber = "FCAB" + String.valueOf(timeStamp);
		logger.info("[AnantQa] bank reference number: " + randomBankReferenceNumber.toString());
		String captureInfo = "NA!NA";
		if(cardType.equals("WP")) {
        	cardType = "WP!NA";
        	captureInfo = "Freecharge!" + auth.getAdditionalInfo1();
        }else if(cardType.equals("CC")) {
        	cardType = "CC!xxxxxx";
        	captureInfo = "xxxx xxxx!Card";
        }else if(cardType.equals("DC")) {
        	cardType = "DC!xxxxxx";
        	captureInfo = "xxxx xxxx!Card";
        }else if(cardType.equals("NB")) {
        	cardType = "NB!xxx";
        	captureInfo = "IFSC0000001!PG";
        }else if(cardType.equals("UP")) {
//        	cardType = "NB!xxx";
        	captureInfo = "xxx@xxxx";
        }
		
		msg.append(generateMsgHeader(BillPayConstants.BDK_PAYMENT_REQUEST_CODE, traceId,userId));
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(fpr.getBillerId())
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billerAccountId).append(MESSAGE_PARAM_TILDE_SEPARATOR)
		.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(authenticator)
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billId).append(MESSAGE_PARAM_TILDE_SEPARATOR)
		.append(billDueDate).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billDate)
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billNumber).append(MESSAGE_PARAM_TILDE_SEPARATOR)
		.append(String.format("%.2f", amount)).append(MESSAGE_PARAM_TILDE_SEPARATOR)
		.append(paymentType).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(randomBankReferenceNumber.toString())
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(messageDetails) 
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(cardType)
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append("NA!NA!NA")
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append("10.10.10.10!01-23-45-67-89-ab^^" + captureInfo) 
		.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS);



		long checksum = createChecksum(msg.toString(), fcProperties.getBdkWorkingKey());
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(checksum);
		return msg.toString();
	}

	private String convertToBdkDateFormat(String normalDateFormat, String defaultReturn) {
		try {
			if (BillPayUtil.isEmpty(normalDateFormat)) {
				return defaultReturn;
			}

			return BillPayConstants.billdeskDateFormat.format(BillPayConstants.dateViewFormat.parse(normalDateFormat));
		} catch (Exception e) {
			return defaultReturn;
		}
	}

	private String generateAuthenticatorField(String info1, String info2, String info3, String info4, String info5) {
		info2 = !isValidInfo(info2) ? "" : AUTHENTICATOR_SEPARATOR + info2;
		info3 = !isValidInfo(info3) ? "" : AUTHENTICATOR_SEPARATOR + info3;
		info4 = !isValidInfo(info4) ? "" : AUTHENTICATOR_SEPARATOR + info4;
		info5 = !isValidInfo(info5) ? "" : AUTHENTICATOR_SEPARATOR + info5;
		return (info1 + info2 + info3 + info4 + info5);
	}

	private boolean isValidInfo(String info) {
		return !FCUtil.isEmpty(info) ;
	}
	

	private String getErrorCodeFromRawResponse(String rawResponse, int expectedLength) {
		if (rawResponse == null) {
			return null;
		}
		if (validateChecksum(rawResponse)) {
			String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
			if (splittedParams.length == expectedLength) {
				return splittedParams[7];
			}
		}
		return null;
	}
	

	private boolean validateChecksum(String rawResponse) {
		try {
			String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
			if (splittedParams != null && splittedParams.length > 0) {
				String checkSumField = splittedParams[splittedParams.length - 1];

				String[] array = Arrays.copyOf(splittedParams, splittedParams.length - 1);

				String receivedMsg = StringUtils.join(array, MESSAGE_PARAM_TILDE_SEPARATOR);
				String calculatedChecksum = String.valueOf(createChecksum(receivedMsg, fcProperties.getBdkWorkingKey()));
				if (calculatedChecksum.equals(checkSumField)) {
					return true;
				}
			}
			return false;
		}catch(Exception ex) {
			logger.error("Exception validating checksum");
			return false;
		}
	}

	private String postBilldeskRequestWithTimeout(String rawRequest, String aggregatorTxnId, boolean shouldTimeout) throws IOException {

		logger.info("Posting " + rawRequest + " to billdesk. " + fcProperties.getBdkUrl());

		
		PostMethod method = new PostMethod(fcProperties.getBdkUrl());
		method.setParameter(BillPayBilldeskUtil.MESSAGE_PARAM_NAME, rawRequest);
		//int retryCount = 0;

		try {
			Integer statusCode = executeWithoutTimeout(aggregatorTxnId, bdkHttpClient, method, "billdesk");
			String rawResponse = method.getResponseBodyAsString();
			logger.info("Raw Response from billdesk : " + rawResponse);

			if (statusCode.equals(HttpStatus.SC_OK)) {
				return rawResponse.trim();
			} else {
				logger.info("Got HTTP Response Code : " + statusCode + " for bdk post request " + rawRequest);
				return null;
			}
		}  catch (ConnectTimeoutException | SocketException e) {
        	logger.error("Something bad happened; got ConnectTimeoutException while recharging for orderID: ["
                    + aggregatorTxnId + "]", e);
        	
            throw e;
        }
		catch (IOException ioe) {
			logger.error("Got Exception while recharging for bdk request : " + rawRequest, ioe);
			logger.info("Respushing same request");
			
		} catch (Exception e) {
			logger.error("Got Exception while recharging for bdk request : " + rawRequest, e);
			return null;
		} finally {
			method.releaseConnection();

		}
			return null;
	}

	private static long createChecksum(String msg, String bdkWorkingKey) {
		String msgToBeHashed = msg + MESSAGE_PARAM_TILDE_SEPARATOR + bdkWorkingKey;
		byte[] msgByte = msgToBeHashed.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(msgByte, 0, msgByte.length);
		return checksum.getValue();
	}

	private String generateMsgHeader(String messageCode, String traceId, String userId) {
		StringBuilder headerMsg = new StringBuilder();
		String timestamp = sdf.format(new Date());
		headerMsg.append(messageCode).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(traceId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(SOURCE_ID).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(timestamp).append(MESSAGE_PARAM_TILDE_SEPARATOR).append("BD01BD05MOB000000001!INT!NA!NA!NA")
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId);
		return headerMsg.toString();
	}

	@Override
	public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
			List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
					ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
			throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
			IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getWalletBalance() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Double getBalanceWarnThreshold() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Double getBalanceCriticalThreshold() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBalanceCachekey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBalanceUpdateTimeCachekey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BillPaymentCommonValidationApiResponse submitValidationRequest(IBillValidationRequest req,
			BillPostpaidMerchant billPostpaidMerchant) throws ClientProtocolException, IOException {
		String responseCode = null;
		try {
			RawBillValidationAPIResponse vr = new RawBillValidationAPIResponse();
			vr.setStatusValid("Y");
			BillValidationAPIResponse billValidationAPIResponse = vr.toBillValidationResponse(req, responseCode);
			return (convertValidationResponseToCommonApiResponse(billValidationAPIResponse, req));
		} catch (Exception e) {
			logger.error("vaildation api response is failed for billdesk", e);
			throw new IOException();
		}
	}

	private BillPaymentCommonValidationApiResponse convertValidationResponseToCommonApiResponse(
			BillValidationAPIResponse billValidationAPIResponse, IBillValidationRequest req) {
		BillPaymentCommonValidationApiResponse apiResponse = new BillPaymentCommonValidationApiResponse();
		apiResponse.setOperatorId(req.getOperatorID());
		apiResponse.setBillAmount(req.getTransactionAmount());
		apiResponse.setBillDueDate(FCUtil.currentDate());
		apiResponse.setBillProvider("billdesk");
		if (billValidationAPIResponse.getStatus() != null
				&& billValidationAPIResponse.getStatus().equalsIgnoreCase("Y")) {
			apiResponse.setStatus("Y");
		} else {
			apiResponse.setStatus("F");
		}
		return apiResponse;
	}

	public HttpConnectionManagerParams getConnectionManagerConfig() {
		return connectionManagerConfig;
	}

	public void setConnectionManagerConfig(HttpConnectionManagerParams connectionManagerConfig) {
		this.connectionManagerConfig = connectionManagerConfig;
	}

	public HttpClient getBdkHttpClient() {
		return bdkHttpClient;
	}

	public void setBdkHttpClient(HttpClient bdkHttpClient) {
		this.bdkHttpClient = bdkHttpClient;
	}

}
