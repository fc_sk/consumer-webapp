package com.freecharge.freebill.billdeskpostpaid;

import com.freecharge.infrastructure.billpay.service.aggregator.AggregatorResponse;

public class BDKResponseDTO {
	
	private AggregatorResponse response;
	private String traceId;
	public AggregatorResponse getResponse() {
		return response;
	}
	public void setResponse(AggregatorResponse response) {
		this.response = response;
	}
	public String getTraceId() {
		return traceId;
	}
	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BDKResponseDTO [response=").append(response).append(", traceId=").append(traceId).append("]");
		return builder.toString();
	}

}
