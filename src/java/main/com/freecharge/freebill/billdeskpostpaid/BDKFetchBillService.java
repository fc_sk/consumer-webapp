package com.freecharge.freebill.billdeskpostpaid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.FetchBillValidatorDAO;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;

@Service
public class BDKFetchBillService {

	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private BDKCacheManager bdkCacheManager;

	@Autowired
	private FetchBillValidatorDAO fetchBillValidatorDAO;

	public BillPayValidator getBillPayValidator(String operatorId) {
		BillPayValidator validator = bdkCacheManager.getValidator(operatorId);
		try {
			if (validator != null) {
				return validator;
			} else {
				bdkCacheManager.setValidator(fetchBillValidatorDAO.getValidator(operatorId), operatorId);
			}
		} catch (Exception e) {
			logger.error("Exception caught in fetching Validator ", e);
		}
		return fetchBillValidatorDAO.getValidator(operatorId);
	}

}
