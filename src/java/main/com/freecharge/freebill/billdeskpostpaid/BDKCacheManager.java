package com.freecharge.freebill.billdeskpostpaid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;

@Component
public class BDKCacheManager extends RedisCacheManager {

	private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RedisTemplate<String, Object> bdkCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        this.bdkCacheTemplate.setValueSerializer(new GenericToStringSerializer<>(Long.class));
        return this.bdkCacheTemplate;
    }
    
    
	public BillPayValidator getValidator(String operatorId) {
		try {
			final String cacheKey = "fetchBillValidator" + "_" + operatorId;
			return (BillPayValidator) get(cacheKey);
		} catch (Exception e) {
			logger.error("Exception caught in fetching validators", e);
		}
		return null;
	}


	public void setValidator(BillPayValidator validator,String operatorId) {
		final String cacheKey = "fetchBillValidator" + "_" + operatorId;
		set(cacheKey, validator);
		
	}
	

}
