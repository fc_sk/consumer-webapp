package com.freecharge.freebill.statuscheckservice;

import com.freecharge.freebill.domain.BillPaymentStatus;

public interface IBIllPaymentStatusCheckInterface {
    
    public BillPaymentStatusCheckResponse submitBillPaymentStatusCheckRequest(String orderID, BillPaymentStatus billPaymentStatus) throws Exception;


}
