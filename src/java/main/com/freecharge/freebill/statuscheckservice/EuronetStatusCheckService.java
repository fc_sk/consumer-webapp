package com.freecharge.freebill.statuscheckservice;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;

public class EuronetStatusCheckService implements IBIllPaymentStatusCheckInterface {

    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory
            .getRefundLoggerName(EuronetStatusCheckService.class.getName()));
    
    @Autowired
    private FCProperties fcProperties;
    
    public static final String RESPONSE_CODE_PARAM = "responsecode";
    public static final String GATEWAY_REF_NUMBER = "operatorrefno";
    public static final String EURONET_REFERENCE_NO_PARAM = "enrefno";

    private HttpClient statusCheckHttpClient;
    
    private String euromerchantcode;
    
	private String eurotrnsstatusreq;

	public HttpClient getStatusCheckHttpClient() {
        return statusCheckHttpClient;
    }

    public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
        this.statusCheckHttpClient = statusCheckHttpClient;
    }

    public void setEurotrnsstatusreq(String eurotrnsstatusreq) {
		this.eurotrnsstatusreq = eurotrnsstatusreq;
	}
    
    public String getEurotrnsstatusreq() {
		return eurotrnsstatusreq;
	}
    
    public String getEuromerchantcode() {
		return euromerchantcode;
	}

	public void setEuromerchantcode(String euromerchantcode) {
		this.euromerchantcode = euromerchantcode;
	}

    public BillPaymentStatusCheckResponse submitBillPaymentStatusCheckRequest(String orderID,
            BillPaymentStatus billPaymentStatus) throws Exception {

    	logger.info("Euronet status check process start for order ID " + orderID);
        String merchantReferenceCode =   fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderID;;
        HostConfiguration hostconfig = this.getStatusCheckHttpClient().getHostConfiguration();
        hostconfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HOST), 443, "https");
        this.getStatusCheckHttpClient().setHostConfiguration(hostconfig);
        String euronetTransactionId = billPaymentStatus.getGatewayReferenceNumber();
        NameValuePair[] nameValuePairs = new NameValuePair[3];

        if (StringUtils.isEmpty(billPaymentStatus.getGatewayReferenceNumber()) || billPaymentStatus.getGatewayReferenceNumber().equals("unknown")) {
            nameValuePairs[0] = new NameValuePair("merchantrefno", merchantReferenceCode);
            nameValuePairs[1] = new NameValuePair("merchantcode", euromerchantcode);
            nameValuePairs[2] = new NameValuePair("enrefno", "");
        } else {
            nameValuePairs[0] = new NameValuePair("merchantrefno","");
            nameValuePairs[1] = new NameValuePair("merchantcode",euromerchantcode);
            nameValuePairs[2] = new NameValuePair("enrefno",euronetTransactionId);
        }
        
		GetMethod method;
		method = new GetMethod(eurotrnsstatusreq);
		method.setQueryString(nameValuePairs);

        Map<String, String> responseMap = new HashMap<String, String>();

        String response = "";

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

		try {

			int statusCode = this.getStatusCheckHttpClient().executeMethod(method);

			if (statusCode == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
			} else {
				logger.info("Received HTTP status code for Transaction Status Request:" + statusCode + " For order ID " + orderID);
                aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
			}

            logger.info("euronet Status check request param is: " +
            method.getQueryString()
            + " Output for Transaction Status Request from Euronet : " + response + " For order id " + orderID);

            if(StringUtils.isNotEmpty(response)) {
                responseMap = FCUtil.stringToHashMap(response);
            } else {
                response="enrefno=unknown&merchantrefno=0_"+orderID + "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=Transaction status not known";
                responseMap = FCUtil.stringToHashMap(response);
            }
			
		} catch(Exception e){
			logger.error("Error In Euronet status Check",e);
		}finally {
			logger.info("[IN MODERATOR] Releasing connection from IN in getTransactionStatus for euronet", null);
			method.releaseConnection();
		}
		
		String agRefNumber = responseMap.get(EURONET_REFERENCE_NO_PARAM);
        if (agRefNumber != null) {
            agRefNumber = agRefNumber.trim();
        }
		String status = responseMap.get(RESPONSE_CODE_PARAM);
		
		
		BillPaymentStatusCheckResponse billPaymentStatusCheckResponse = new BillPaymentStatusCheckResponse();
        billPaymentStatusCheckResponse.setGatewayTxnStatusCode(status);
        billPaymentStatusCheckResponse.setRowResponse(response);
        billPaymentStatusCheckResponse.setGatewayReferenceNumber(agRefNumber == null ? "unknown" : agRefNumber);
        billPaymentStatusCheckResponse.setGatewayName(PaymentConstants.BILL_PAYMENT_EURONET_GATEWAY);
        billPaymentStatusCheckResponse.setOrderId(orderID);
        return billPaymentStatusCheckResponse;
	}

}
