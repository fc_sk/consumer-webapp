package com.freecharge.freebill.statuscheckservice;

public class BillPaymentStatusCheckResponse {

    private String gatewayTxnStatusCode;
    private String transactionStatusMessage;
    private String gatewayReferenceNumber;
    private String rowRequest;
    private String rowResponse;
    private String gatewayName;
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getGatewayTxnStatusCode() {
        return gatewayTxnStatusCode;
    }

    public void setGatewayTxnStatusCode(String gatewayTxnStatusCode) {
        this.gatewayTxnStatusCode = gatewayTxnStatusCode;
    }

    public String getTransactionStatusMessage() {
        return transactionStatusMessage;
    }

    public void setTransactionStatusMessage(String transactionStatusMessage) {
        this.transactionStatusMessage = transactionStatusMessage;
    }

    public String getGatewayReferenceNumber() {
        return gatewayReferenceNumber;
    }

    public void setGatewayReferenceNumber(String gatewayReferenceNumber) {
        this.gatewayReferenceNumber = gatewayReferenceNumber;
    }

    public String getRowRequest() {
        return rowRequest;
    }

    public void setRowRequest(String rowRequest) {
        this.rowRequest = rowRequest;
    }

    public String getRowResponse() {
        return rowResponse;
    }

    public void setRowResponse(String rowResponse) {
        this.rowResponse = rowResponse;
    }

}
