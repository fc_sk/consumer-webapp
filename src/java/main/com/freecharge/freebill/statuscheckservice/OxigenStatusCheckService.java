package com.freecharge.freebill.statuscheckservice;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.OxigenService;

public class OxigenStatusCheckService implements IBIllPaymentStatusCheckInterface {
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    OxigenService oxigenService;

    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory
            .getRefundLoggerName(OxigenStatusCheckService.class.getName()));

    private String statusCheckAPI;

    private HttpClient statusCheckHttpClient;

    public HttpClient getStatusCheckHttpClient() {
        return statusCheckHttpClient;
    }

    public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
        this.statusCheckHttpClient = statusCheckHttpClient;
    }

    public String getStatusCheckAPI() {
        return statusCheckAPI;
    }

    public void setStatusCheckAPI(String statusCheckAPI) {
        this.statusCheckAPI = statusCheckAPI;
    }

    public BillPaymentStatusCheckResponse submitBillPaymentStatusCheckRequest(String orderID,
            BillPaymentStatus billPaymentStatus) throws Exception {
        String gatewayReferenceNo = "";
        String response = "";
        PostMethod method = null;
        logger.info("About to call Oxi status check for Order ID: " + billPaymentStatus.getAffiliateTransID());
        try {
            String aggregatorOrderId = fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderID;
            String merchantRefNo = "Checkstatus," + billPaymentStatus.getPostpaidNumber();
            method = new PostMethod(statusCheckAPI);
            method.setParameter("Transid", aggregatorOrderId);
            method.setParameter("merchantrefno", merchantRefNo);
            method.setRequestHeader("Authorization", "BASIC "+oxigenService.getAuthKey());
            int statusCode = this.getStatusCheckHttpClient().executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
                String[] outputResult = new String[0];
                if (response != null && !response.equalsIgnoreCase("")) {
                    outputResult = response.split("\\|");
                }
                String status = outputResult[0].trim();
                logger.info("Oxigen status check api response string for order id "
                        + billPaymentStatus.getAffiliateTransID() + "  " + response);
                if (outputResult.length >= 3 && outputResult[2].trim().split("-").length == 2)
                    gatewayReferenceNo = outputResult[2].trim().split("-")[1].trim();
                BillPaymentStatusCheckResponse billPaymentStatusCheckResponse = new BillPaymentStatusCheckResponse();
                billPaymentStatusCheckResponse.setGatewayTxnStatusCode(status);
                billPaymentStatusCheckResponse.setRowResponse(response);
                billPaymentStatusCheckResponse.setGatewayReferenceNumber(gatewayReferenceNo);
                billPaymentStatusCheckResponse.setGatewayName(PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY);
                billPaymentStatusCheckResponse.setOrderId(orderID);
                return billPaymentStatusCheckResponse;
            } else {
                logger.info("Received HTTP status code in getTransactionStatus" + " for oxigen : "
                        + statusCode + " " + orderID);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            method.releaseConnection();
        }
        return null;
    }

}
