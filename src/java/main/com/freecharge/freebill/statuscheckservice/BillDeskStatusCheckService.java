package com.freecharge.freebill.statuscheckservice;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.fatals.InvalidChecksumException;
import com.freecharge.infrastructure.billpay.service.aggregator.BillPayBilldeskService;
import com.freecharge.infrastructure.billpay.service.aggregator.BillPayBilldeskUtil;
import com.freecharge.infrastructure.billpay.util.BillPayConstants;
import com.freecharge.payment.util.PaymentConstants;
public class BillDeskStatusCheckService implements IBIllPaymentStatusCheckInterface {

	private static String SOURCE_ID = "FRC";
	private static final String MESSAGE_PARAM_TILDE_SEPARATOR = "~";
	private static final String AUTHENTICATOR_SEPARATOR = "!";
	public final static String DEFAULT_DETAILS = "NA";
	private static final String VALID_RESPONSE = "Y";
	private static final String BDK_CODE = "U05003";

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

	private HttpClient statusCheckHttpClient;

	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	BillReadDAO billReadDAO;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	private static final Logger logger = Logger.getLogger(BillDeskStatusCheckService.class);
	
	public enum BillDeskBillPaymentStatus {
	    PAID, FAILED, TOBECONFIRMED
	}


	public BillPaymentStatusCheckResponse submitBillPaymentStatusCheckRequest(String orderID,
			BillPaymentStatus billPaymentStatus) throws Exception {
		logger.info("Doing Status check with BillDesk");
		String gatewayReferenceNumber = getGatewayReferenceNumber(orderID,billPaymentStatus);
		billPaymentStatus.setGatewayReferenceNumber(gatewayReferenceNumber);
		String msg = getStatusCheckMsg(billPaymentStatus);
		String statusCheckResponse = null;
		logger.info("Status check request for orderId:" + orderID + " is " + msg);
		try {
			statusCheckResponse = postBilldeskRequestWithTimeout(msg, billPaymentStatus.getBillPaymentGateway(), true);
			logger.info("Status check response for orderId:" + orderID + " is " + statusCheckResponse);
			return convertToBillPayStatusCheckResponse(msg,statusCheckResponse, 29, 17,billPaymentStatus.getGatewayReferenceNumber(),orderID);

		} catch (Exception e) {
			logger.error("Exception caught during BillDesk Status Check", e);

		}
		return dummyBillPayStatusCheckResponse(orderID);
	}

	private String getGatewayReferenceNumber(String orderID, BillPaymentStatus billPaymentStatus) {
		if (FCUtil.isEmpty(billPaymentStatus.getGatewayReferenceNumber())) {
			List<BillPaymentAttempt> billPaymentAttempts = billReadDAO.findBillPaymentAttemptByOrderId(orderID);
			for (BillPaymentAttempt billPaymentAttempt : billPaymentAttempts) {
				if (!FCUtil.isEmpty(billPaymentAttempt.getRawRequest())) {
					if (!billPaymentAttempt.getRawRequest().startsWith(BDK_CODE))
						continue;
					String rawRequest = billPaymentAttempt.getRawRequest();
					String[] requestSplitter = rawRequest.split(MESSAGE_PARAM_TILDE_SEPARATOR);
					return SOURCE_ID + requestSplitter[1];
				}
			}
		}
		return billPaymentStatus.getGatewayReferenceNumber();
	}

	private BillPaymentStatusCheckResponse dummyBillPayStatusCheckResponse(String orderId) {
		BillPaymentStatusCheckResponse billPaymentStatusCheckResponse = new BillPaymentStatusCheckResponse();
		billPaymentStatusCheckResponse.setGatewayTxnStatusCode("");
		billPaymentStatusCheckResponse.setRowResponse("");
		billPaymentStatusCheckResponse.setGatewayReferenceNumber("");
		billPaymentStatusCheckResponse.setGatewayName(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
		billPaymentStatusCheckResponse.setOrderId(orderId);
		return billPaymentStatusCheckResponse;
	}

	private BillPaymentStatusCheckResponse convertToBillPayStatusCheckResponse(String rawRequest,String rawResponse, int expectedLength,
			int billPaymentStatusIndex,String gatewayReferenceNumber, String orderId) {
		BillPaymentStatusCheckResponse billPaymentStatusCheckResponse = new BillPaymentStatusCheckResponse();
		billPaymentStatusCheckResponse.setOrderId(orderId);
		billPaymentStatusCheckResponse.setRowRequest(rawRequest);
		billPaymentStatusCheckResponse.setRowResponse(rawResponse);
		billPaymentStatusCheckResponse.setGatewayName(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY);
		if (rawResponse == null) {
			logger.error("Did not get a response from aggregator.");
			return billPaymentStatusCheckResponse;

		}
		if (validateChecksum(rawResponse, fcProperties.getBdkWorkingKey())) {
			String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
			if (splittedParams.length != expectedLength) {
				logger.error("Expected fields : " + expectedLength + " , Actual fields : " + splittedParams.length
						+ " Raw response : " + rawResponse);
				return billPaymentStatusCheckResponse;
			}
			if (!splittedParams[6].equals(VALID_RESPONSE) || !splittedParams[7].equals("0")
					|| !splittedParams[8].equals("0")) {
				logger.info("Failure Response : " + rawResponse);
			}

			String paymentStatus = splittedParams[billPaymentStatusIndex];

			if (paymentStatus == null) {
				billPaymentStatusCheckResponse.setGatewayTxnStatusCode(BillPayConstants.BILLPAY_FC_UNDER_PROCESS_CODE);
				return billPaymentStatusCheckResponse;
			}
			if (!FCUtil.isEmpty(splittedParams[13])) {
				billPaymentStatusCheckResponse.setGatewayReferenceNumber(splittedParams[13]);
			} else {
				billPaymentStatusCheckResponse.setGatewayReferenceNumber(gatewayReferenceNumber);
			}

			if (paymentStatus.equals(BillDeskBillPaymentStatus.PAID.name())) {
				billPaymentStatusCheckResponse.setGatewayTxnStatusCode(BillPayConstants.BILLPAY_FC_SUCCESS_CODE);
			}else{
				billPaymentStatusCheckResponse.setGatewayTxnStatusCode(BillPayConstants.BILLPAY_FC_GENERIC_FAILURE_CODE);				
			}
			billPaymentStatusCheckResponse.setTransactionStatusMessage(splittedParams[19]);
			return billPaymentStatusCheckResponse;
		} else {
			logger.error("Checksum failed for " + rawResponse, new InvalidChecksumException(rawResponse));
			return billPaymentStatusCheckResponse;
		}
	}
		 

	private boolean validateChecksum(String rawResponse, String bdkWorkingKey) {
		String[] splittedParams = StringUtils.splitPreserveAllTokens(rawResponse, MESSAGE_PARAM_TILDE_SEPARATOR);
		if (splittedParams != null && splittedParams.length > 0) {
			String checkSumField = splittedParams[splittedParams.length - 1];

			String[] array = Arrays.copyOf(splittedParams, splittedParams.length - 1);

			String receivedMsg = StringUtils.join(array, MESSAGE_PARAM_TILDE_SEPARATOR);
			String calculatedChecksum = String.valueOf(createChecksum(receivedMsg, bdkWorkingKey));
			if (calculatedChecksum.equals(checkSumField)) {
				return true;
			}
		}
		return false;
	}

	private static long createChecksum(String msg, String bdkWorkingKey) {
		String msgToBeHashed = msg + MESSAGE_PARAM_TILDE_SEPARATOR + bdkWorkingKey;
		byte[] msgByte = msgToBeHashed.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(msgByte, 0, msgByte.length);
		return checksum.getValue();
	}

	private String postBilldeskRequestWithTimeout(String rawRequest, String billPaymentGateway, boolean b) {
		logger.info("Posting " + rawRequest + " to billdesk. " + fcProperties.getBdkUrl());

		PostMethod method = new PostMethod(fcProperties.getBdkUrl());
		method.setParameter(BillPayBilldeskUtil.MESSAGE_PARAM_NAME, rawRequest);
		try {
			Integer statusCode = this.getStatusCheckHttpClient().executeMethod(method);
			String rawResponse = method.getResponseBodyAsString();
			logger.info("Raw Response from billdesk : " + rawResponse);

			if (statusCode.equals(HttpStatus.SC_OK)) {
				return rawResponse.trim();
			} else {
				logger.info("Got HTTP Response Code : " + statusCode + " for bdk post request " + rawRequest);
				return null;
			}
		} catch (Exception e) {
			logger.error("Got Exception while recharging for bdk request : " + rawRequest, e);

			return null;
		} finally {
			method.releaseConnection();
		}
	}

	private String getStatusCheckMsg(BillPaymentStatus billPaymentStatus) {
		StringBuilder msg = new StringBuilder();
		String traceId = "5" + billPayServiceRemote.getUniqueId();
		String userId = "1000";
		
		msg.append(generateMsgHeader(BillPayConstants.BDK_TXN_STATUS_REQUEST_CODE, traceId, userId));

		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(billPaymentStatus.getGatewayReferenceNumber())
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(DEFAULT_DETAILS).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(DEFAULT_DETAILS);
		long checksum = createChecksum(msg.toString(), fcProperties.getBdkWorkingKey());
		msg.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(checksum);

		return msg.toString();
	}

	private String generateMsgHeader(String messageCode, String traceId, String userId) {
		StringBuilder headerMsg = new StringBuilder();
		String timestamp = sdf.format(new Date());
		headerMsg.append(messageCode).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(traceId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(SOURCE_ID).append(MESSAGE_PARAM_TILDE_SEPARATOR)
				.append(timestamp).append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId)
				.append(MESSAGE_PARAM_TILDE_SEPARATOR).append(userId);
		return headerMsg.toString();
	}

	public HttpClient getStatusCheckHttpClient() {
		return statusCheckHttpClient;
	}

	public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
		this.statusCheckHttpClient = statusCheckHttpClient;
	}

}
