package com.freecharge.cartcheckout;

import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.client.CreateCartRequest;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.payments.sdmoney.exceptions.InternalErrorException;
import com.snapdeal.payments.ts.registration.TaskExecutor;
import com.snapdeal.payments.ts.registration.TaskRequest;
import com.snapdeal.payments.ts.response.ExecutorResponse;
import com.snapdeal.payments.ts.response.RetryInfo;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartCheckoutTaskExecutor  implements TaskExecutor<TaskRequest> {

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    CartCheckoutServiceClientImpl cartClient;
    private final Logger logger   = LoggingFactory.getLogger(getClass());


    @Override
    public ExecutorResponse execute(TaskRequest taskRequest) {
        logger.info("Executing create cart task");
        ExecutorResponse execResponse = new ExecutorResponse();
        final CreateCartTaskRequest createCartTaskRequest = (CreateCartTaskRequest) taskRequest;
        CreateCartRequest createCartRequest;
        try{
                createCartRequest = convertCreateCartTaskToCreateCartRequest(createCartTaskRequest);
                CheckoutResponse checkoutResponse = cartClient.createCart(createCartRequest);
                execResponse.setCompletionLog(checkoutResponse.getCheckoutId());
                execResponse.setStatus(ExecutorResponse.Status.SUCCESS);

        }
        catch (InternalErrorException exp) {

            logger.error("InternalServerException while creating cart, metadata: " +
                    createCartTaskRequest + "...retrying", exp);

            RetryInfo retryInfo = new RetryInfo();
            retryInfo.setType(RetryInfo.RetryType.EXPONENTIAL);
            retryInfo.setExpBase(constants.TASK_EXPONENTIAL_BASE);
            retryInfo.setWaitTime(
                    constants.DEFAULT_ON_FAILURE_TASK_REEXECUTION_WAIT_TIME);
            execResponse.setAction(retryInfo);
            execResponse.setStatus(ExecutorResponse.Status.RETRY);
        }
        catch(Exception exp){
            logger.error("Exception occured while creating cart, metadata: "
                    + createCartTaskRequest, exp);
            execResponse.setCompletionLog(exp.getMessage());
            execResponse.setStatus(ExecutorResponse.Status.FAILURE);
        }
        return execResponse;
    }

    private CreateCartRequest convertCreateCartTaskToCreateCartRequest(CreateCartTaskRequest createCartTaskRequest){
        //CreateCartRequest createCartRequest = new CreateCartRequest();
        CreateCartRequest createCartRequest = (CreateCartRequest) createCartTaskRequest;
        logger.info("Setting userid for cart request "+ FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
        createCartRequest.setUserId((String) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.USER_ID));
        /*createCartRequest.setServiceRegion(createCartTaskRequest.getServiceRegion());
        createCartRequest.setChannelId(createCartTaskRequest.getChannelId());
        createCartRequest.setAmount(createCartTaskRequest.getAmount());
        createCartRequest.setFcUserId(createCartTaskRequest.getFcUserId());
        createCartRequest.setMobileNumber(createCartTaskRequest.getMobileNumber());
        createCartRequest.setCircleName(createCartTaskRequest.getCircleName());
        createCartRequest.setOperatorId(createCartTaskRequest.getOperatorId());
        createCartRequest.setOperator(createCartTaskRequest.getOperator());
        createCartRequest.setAdditionalInfo1(createCartTaskRequest.getAdditionalInfo1());
        createCartRequest.setAdditionalInfo2(createCartTaskRequest.getAdditionalInfo2());
        createCartRequest.setAdditionalInfo3(createCartTaskRequest.getAdditionalInfo3());
        createCartRequest.setAdditionalInfo4(createCartTaskRequest.getAdditionalInfo4());
        createCartRequest.setAdditionalInfo5(createCartTaskRequest.getAdditionalInfo5());
        createCartRequest.setPlanType(createCartTaskRequest.getPlanType());
        createCartRequest.setProductType(createCartTaskRequest.getProductType());*/

        return createCartRequest;
    }
}
