package com.freecharge.cartcheckout;

import com.snapdeal.payments.ts.registration.TaskExecutor;
import com.snapdeal.payments.ts.registration.TaskRegistrationInfo;
import com.snapdeal.payments.ts.registration.TaskSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartCheckoutTaskInfo implements TaskRegistrationInfo {
    @Autowired
    private CartCheckoutTaskExecutor executor;

    @Autowired
    private CreateCartTaskSerializer serializer;

    @Override
    public TaskExecutor getTaskExecutor() {
        return executor;
    }

    @Override
    public TaskSerializer<CreateCartTaskRequest> getTaskSerializer() {
        return serializer;
    }

    @Override
    public String getTaskType() {
        return constants.CREATE_CART_TASK_TYPE;
    }

    @Override
    public long getRetryLimit() {
        return constants.DEFAULT_CREATE_CART_RETRY_LIMIT;
    }

    @Override
    public long getExecutionTime() {
        return 30 * 1000;
    }

    @Override
    public boolean isRecurring() {
        return false;
    }
}
