package com.freecharge.cartcheckout;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.gson.Gson;
import com.snapdeal.payments.ts.registration.TaskSerializer;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class CreateCartTaskSerializer implements TaskSerializer<CreateCartTaskRequest> {

    private final Logger logger   = LoggingFactory.getLogger(getClass());

    @Override
    public CreateCartTaskRequest fromString(String request) {
        CreateCartTaskRequest target = null;
        try {
            Gson gson = new Gson();

            if (request != null) {
                target = gson.fromJson(request, CreateCartTaskRequest.class);
            }

        }catch (Exception exp){
            logger.info("task serilalizer failed",exp);
        }
        return target;
    }

    @Override
    public String toString(CreateCartTaskRequest request) {
        Gson gson = new Gson();
        String json = null;
        if (request != null) {
            json = gson.toJson(request);
        }

        return json;
    }

}
