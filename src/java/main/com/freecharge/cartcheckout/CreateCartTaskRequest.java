package com.freecharge.cartcheckout;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.freecharge.common.client.CreateCartRequest;
import com.snapdeal.payments.ts.registration.TaskRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCartTaskRequest extends CreateCartRequest implements TaskRequest {

    @NotNull
    @Size(min=1)
    @JsonIgnore
    private String taskId;

    @Override
    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /*String fcUserId;
    String serviceNumber;
    String productType;
    Float rechargeAmount;
    String operator;
    String circleName;
    String planType;
    String mobileNumber;
    String operatorId;
    Float amount;
    Integer channelId;
    Integer serviceProvider;
    Integer serviceRegion;
    String additionalInfo1;
    String additionalInfo2;
    String additionalInfo3;
    String additionalInfo4;
    String additionalInfo5;



    public String getFcUserId() {
        return fcUserId;
    }

    public void setFcUserId(String fcUserId) {
        this.fcUserId = fcUserId;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Float getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(Float rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(Integer serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Integer getServiceRegion() {
        return serviceRegion;
    }

    public void setServiceRegion(Integer serviceRegion) {
        this.serviceRegion = serviceRegion;
    }

    public String getAdditionalInfo1() {
        return additionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        this.additionalInfo1 = additionalInfo1;
    }

    public String getAdditionalInfo2() {
        return additionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        this.additionalInfo2 = additionalInfo2;
    }

    public String getAdditionalInfo3() {
        return additionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        this.additionalInfo3 = additionalInfo3;
    }

    public String getAdditionalInfo4() {
        return additionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        this.additionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo5() {
        return additionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        this.additionalInfo5 = additionalInfo5;
    }*/
}


