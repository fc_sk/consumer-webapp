package com.freecharge.cartcheckout;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.cart.client.ICartCheckoutServiceClinet;
import com.freecharge.common.client.CheckoutDetailsRequestParams;
import com.freecharge.common.client.CheckoutResponse;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class CartCheckoutClientService {

	private static Logger logger = LoggingFactory.getLogger(CartCheckoutClientService.class);
	
	@Autowired
	private ICartCheckoutServiceClinet cartCheckoutClinet;
	
	public String getCheckoutIdFromLookupId(String lookupId) throws Exception {
		logger.info("going to fetch checkoutId from lookup id " + lookupId);
		CheckoutDetailsRequestParams requestparams = new CheckoutDetailsRequestParams();
		requestparams.setLookupId(lookupId);
		CheckoutResponse response = cartCheckoutClinet.getCheckoutId(requestparams);
		return response.getCheckoutId();
	}
	
}
