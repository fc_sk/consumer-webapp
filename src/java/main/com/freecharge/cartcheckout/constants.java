package com.freecharge.cartcheckout;

public class constants {

    public static String CREATE_CART_TASK_TYPE = "WEBAPP_CREATE_CART";
    public static final long DEFAULT_CREATE_CART_RETRY_LIMIT = 14;
    public static final int TASK_EXPONENTIAL_BASE = 3;
    public static final long DEFAULT_ON_FAILURE_TASK_REEXECUTION_WAIT_TIME = 5*60*1000; //150ms

}
