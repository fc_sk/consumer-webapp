package com.freecharge.cartcheckout;

import com.freecharge.cart.client.ICartCheckoutServiceClinet;
import com.freecharge.cart.client.impl.CartCheckoutServiceClientImpl;
import com.freecharge.cart.client.utils.ClientDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.EnvironmentEnum;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class CartCheckoutClient {

    @Value("${cartCheckout.client.port}")
    private String port;

    @Value("${cartCheckout.client.clientKey}")
    private String clientKey;

    @Value("${cartCheckout.client.IP}")
    private String ip;

    @Value("${cartCheckout.client.clientId}")
    private String clientId;

    @Value("${cartCheckout.client.apiTimeout}")
    private String apiTimeout;

    @Value("${cartCheckout.client.env}")
    private String clientenv;

    private ICartCheckoutServiceClinet cartCheckoutClient;

    private static Logger LOGGER = LoggingFactory.getLogger(CartCheckoutClient.class);
    @PostConstruct
    public void init() {
        try {
            EnvironmentEnum env = EnvironmentEnum.TESTING;
            if(clientenv.equalsIgnoreCase("prod")) {
                env = EnvironmentEnum.PRODUCTION;
            }
            ClientDetails.init(ip,port,clientKey,clientId, env);
            cartCheckoutClient = new CartCheckoutServiceClientImpl();
        } catch (Exception e) {
            LOGGER.error("Exception while initializing CartCheckout Client", e);
        }
    }
}
