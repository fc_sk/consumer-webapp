package com.freecharge.consumer;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class PaymentTsmLockReminderService {
	
	Logger logger = LoggingFactory.getLogger(getClass());
	
	public void handlePaymentLockReminderNotification(String globalTxnId, String globalTxnType) {
		logger.info("processing payment lock reminder notification for global txn id " + globalTxnId);
		
	}
}
