package com.freecharge.mailer.campaign;

import java.util.ArrayList;
import java.util.List;

public enum MailerTypeEnum {

	PREPAIDSUCCESS(1, "V", "SUCCESS"),
	PREPAIDCOUPON(2, "V", "COUPON"),
	POSTPAIDSUCCESS(3, "M", "SUCCESS"),
	POSTPAIDCOUPON(4, "M", "COUPON");
	
	String productType;
	Integer id;
	String mailerType;
	
	private MailerTypeEnum(Integer id, String productType, String mailerType) {
		this.productType = productType;
		this.id = id;
		this.mailerType = mailerType;
	}
	
	public String getMailerType() {
		return mailerType;
	}
	
	public void setMailerType(String mailerType) {
		this.mailerType = mailerType;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getProductType() {
		return productType;
	}
	
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public static MailerTypeEnum findById(Integer id){
		for(MailerTypeEnum mailerType : MailerTypeEnum.values()){
			if(mailerType.getId().equals(id)){
				return mailerType;
			}
		}
		return null;
	}
	
	public static List<Integer> getIdsForProductTypes(String type){
		List<Integer> mailerTypeList = new ArrayList<>();
		for(MailerTypeEnum mailerTypeValue : MailerTypeEnum.values()){
			if(mailerTypeValue.getProductType().equalsIgnoreCase(type)){
				mailerTypeList.add(mailerTypeValue.getId());
			}
		}
		return mailerTypeList;
	}
}
