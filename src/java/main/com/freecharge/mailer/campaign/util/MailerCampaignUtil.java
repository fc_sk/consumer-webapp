package com.freecharge.mailer.campaign.util;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mailer.dao.IMailerCampaignDao;
import com.freecharge.mailer.dao.MailerCampaignEntity;

@Service("mailerCampaignUtil")
public class MailerCampaignUtil {
	
	@Autowired
	IMailerCampaignDao mailerDao;

	private Logger logger = LoggingFactory.getLogger(getClass());

	public void updateMailerPriorities(Map<Long, Integer> priorityMap) {
		if (priorityMap == null) {
			logger.debug("Null priority map received.");
			return;
		}
		for (Map.Entry<Long, Integer> entry : priorityMap.entrySet()) {
			this.mailerDao.updateMailerCampaignPriority(entry.getKey(), entry.getValue());
		}
		logger.debug("Campaign priorities updated. Now updating enable Campaigns.");
		this.updateEnabledMailerCampaigns();
	}

	private void updateEnabledMailerCampaigns() {
		this.mailerDao.updateEnabledMailerCampaigns();
	}

	public void updateCampaignMailerStatus(Long id, Boolean status) {
		this.mailerDao.updateMailerCampaignStatus(id, status);
	}

	public List<MailerCampaignEntity> getAllCampaignsById(String campaignIds) {
		return this.mailerDao.getMailerCampaignsByIds(campaignIds);
	}

	public List<MailerCampaignEntity> getEnabledMailerCampaigns() {
		return this.mailerDao.getEnabledMailerCampaigns();
	}

}
