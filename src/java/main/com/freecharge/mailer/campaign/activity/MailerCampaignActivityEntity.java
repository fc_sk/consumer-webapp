package com.freecharge.mailer.campaign.activity;

import java.util.Date;

public class MailerCampaignActivityEntity implements java.io.Serializable{
	private static final long serialVersionUID = -370643122614134654L;
	
	private Long id;
	private Long userId;
	private Long mailerCampaignId;
	private String lookupId;
	private Date createdOn;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getMailerCampaignId() {
		return mailerCampaignId;
	}
	public void setMailerCampaignId(Long mailerCampaignId) {
		this.mailerCampaignId = mailerCampaignId;
	}
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
