package com.freecharge.mailer.campaign.activity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

@Repository("mailerCampaignActivity")
public class MailerCampaignActivityDao {

	private String table = "mailer_campaign_activity";
	
	private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert addActivity;

    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jt = new NamedParameterJdbcTemplate(dataSource);
        this.addActivity = new SimpleJdbcInsert(dataSource).withTableName(table).usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }
    
    public int getNumTimesActivityDone(Long campaignId, Long userId){
        String sql = "select count(*) from " + table + " where user_id = :userId and fk_mailer_campaign_id = :campaignId ";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("campaignId", campaignId);
        return this.jt.queryForInt(sql, params);
    }
	
    public void recordActivity(Long userId, Long campaignId, String lookupId){
		Map<String, Object> queryData = new HashMap<>();
		queryData.put("user_id", userId);
		queryData.put("lookup_id", lookupId);
		queryData.put("fk_mailer_campaign_id", campaignId);
		this.addActivity.execute(queryData);
	}
    
    public List<MailerCampaignActivityEntity> getMailerCampaignsForUser(Long userId){
    	String sql = "select * from " + table + " where user_id = :userId ";
    	Map<String, Object> queryData = new HashMap<>();
		queryData.put("userId", userId);
		return jt.query(sql, queryData, new MailerCampaignEntityRowMapper());
    }
    
    private static final class MailerCampaignEntityRowMapper implements RowMapper<MailerCampaignActivityEntity> {
        public MailerCampaignActivityEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        	MailerCampaignActivityEntity mailerCampaignActivity = new MailerCampaignActivityEntity();
            mailerCampaignActivity.setId(rs.getLong("id"));
            mailerCampaignActivity.setLookupId(rs.getString("lookup_id"));
            mailerCampaignActivity.setMailerCampaignId(rs.getLong("fk_mailer_campaign_id"));
            mailerCampaignActivity.setUserId(rs.getLong("user_id"));
            mailerCampaignActivity.setCreatedOn(rs.getDate("created_on"));
            return mailerCampaignActivity;
        }
    }
}
