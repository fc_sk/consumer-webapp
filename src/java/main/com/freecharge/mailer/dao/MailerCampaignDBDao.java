package com.freecharge.mailer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;

@Repository("mailerCampaignDBDao")
public class MailerCampaignDBDao implements IMailerCampaignDao{
    private static Logger logger = LoggingFactory.getLogger(MailerCampaignDBDao.class);
    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertMailerCampaign;
    private String tableName = "mailer_campaign"; 
        
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertMailerCampaign = new SimpleJdbcInsert(dataSource).withTableName("mailer_campaign").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    @Override
    public long addMailerCampaign(String name, Long conditionId, Date startDate, Date endDate, Integer mailerType, 
    		Integer priority, Boolean enabled, String params ){
    	Map<String, Object> insertParams = new HashMap<String, Object>();
    	insertParams.put("camp_name", name);
    	insertParams.put("fk_ar_condition_id", conditionId);
    	insertParams.put("camp_start_date", startDate);
    	insertParams.put("camp_end_date", endDate);
    	insertParams.put("mailer_type", mailerType);
    	insertParams.put("camp_priority", priority);
    	insertParams.put("is_enabled", enabled);
    	insertParams.put("camp_params", params);
    	
    	Number primaryKey = insertMailerCampaign.executeAndReturnKey(insertParams);
    	logger.info("Inserted into Mailer Campaign with name : " + name );
        return primaryKey.longValue();
    }
    
    @Override
    public void updateMailerCampaign(MailerCampaignEntity mailerEntity) {
        String sql = "update " + tableName + " set camp_name = :campaignName, fk_ar_condition_id = :conditionId, camp_start_date = :startDate, " +
                "camp_end_date = :endDate, mailer_type = :mailerType, is_enabled = :enabled, " +
                "camp_params = :params where id = :id;";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(mailerEntity);
        this.jt.update(sql, parameters);
        logger.info(String.format("Mailer Campaign: %s is updated.", mailerEntity.getCampaignName()));
    }

    @Override
    public MailerCampaignEntity getMailerCampaignById(long id) {
        String sql = "SELECT * FROM " + tableName + " WHERE id = :id";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        List<MailerCampaignEntity> rows = jt.query(sql, params, new MailerCampaignRowMapper());
        if (rows.size() == 0) {
            return null;
        }
        return rows.get(0);
    }

    @Override
    public void deleteMailerCampaignById(long id) {
        String sql = "DELETE FROM " + tableName + " WHERE id = :id";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        jt.update(sql, params);
    }
    
    @Override
    public void deleteAllMailerCampaigns() {
        String sql = "DELETE FROM " + tableName;
        Map<String, Object> params = new HashMap<String, Object>();
        jt.update(sql, params);        
    }
    
    @Override
    public List<MailerCampaignEntity> getAllMailerCamaigns() {
        String sql = "SELECT * from " + tableName;
        Map<String, Object> params = new HashMap<String, Object>();
        return jt.query(sql, params, new MailerCampaignRowMapper());
    }
    
    @Override
    public List<MailerCampaignEntity> getEnabledMailerCampaigns() {
    	logger.info("getting all enabled mailer campaigns");
        String sql = "SELECT * from " + tableName + " WHERE camp_end_date > now() and camp_start_date < now() order by camp_priority";
        Map<String, Object> params = new HashMap<String, Object>();
        logger.info("returning all the mailer campaigns");
        return jt.query(sql, params, new MailerCampaignRowMapper());
        
    }
       
    private static final class MailerCampaignRowMapper implements RowMapper<MailerCampaignEntity> {
        public MailerCampaignEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        	MailerCampaignEntity mailerEntity = new MailerCampaignEntity();
            mailerEntity.setId(rs.getLong("id"));
            mailerEntity.setCampaignName(rs.getString("camp_name"));
            mailerEntity.setConditionId(rs.getLong("fk_ar_condition_id"));
            mailerEntity.setStartDate(rs.getDate("camp_start_date"));
            mailerEntity.setEndDate(rs.getDate("camp_end_date"));
            mailerEntity.setMailerType(rs.getInt("mailer_type"));
            mailerEntity.setPriority(rs.getInt("camp_priority") == 0 ? null : rs.getInt("camp_priority"));
            Date campEndDate = rs.getDate("camp_end_date");
            if(campEndDate.compareTo(new Date(System.currentTimeMillis())) > 0){
            	mailerEntity.setEnabled(true);
            }else {
            	mailerEntity.setEnabled(false);
            }
            	
            mailerEntity.setParams(rs.getString("camp_params"));
            mailerEntity.setCreatedOn(rs.getDate("created_on"));
            mailerEntity.setUpdatedOn(rs.getDate("updated_on"));
            return mailerEntity;
        }
    }

	@Override
	public int updateMailerCampaignStatus(long campaignId, boolean status) {
		String sql = "update " + tableName + " set is_enabled = :status WHERE id = :campaignId";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("campaignId", campaignId);
        params.put("status", (status) ? 1 : 0);
        return jt.update(sql, params);
	}

	@Override
	public void updateMailerCampaignPriority(Long id, Integer priority){
		 Map<String, Object> params = new HashMap<String, Object>();
	     params.put("id", id);
	     params.put("camp_priority", priority);
		 String sql = "update " + tableName + " set camp_priority = :camp_priority WHERE id = :id";
		 jt.update(sql, params);
	}

	@Override
	public void updateEnabledMailerCampaigns() {
		//Nothing to be done in db. only caching is handled in caching dao
	}

	@Override
	public List<MailerCampaignEntity> getMailerCampaignsByIds(String campaignIds) {
		List<MailerCampaignEntity> entityList = new ArrayList<>();
		String sql = "select * from " + tableName + " where id in ( :campaignIds );";
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("campaignIds", campaignIds);
	    return jt.query(sql, params, new MailerCampaignRowMapper());
	}
}