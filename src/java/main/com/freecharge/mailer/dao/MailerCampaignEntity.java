package com.freecharge.mailer.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.json.simple.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;

import com.freecharge.common.util.FCStringUtils;



public class MailerCampaignEntity implements java.io.Serializable{
	private static final long serialVersionUID = -3706431226141654654L;
	
	private Long id;
	@NotBlank(message = "Campaign name is required.")
	private String campaignName;
	@NotNull
	private Long conditionId;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;
	@NotNull
	private Integer mailerType;
	private Integer priority;
	private Boolean enabled;
	private String params;
	private Date createdOn;
    private Date updatedOn;
    private String startDateStr;
    private String endDateStr;
    private Map<String, Object> temp = new HashMap<>();
	
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public Long getConditionId() {
		return conditionId;
	}
	public void setConditionId(Long conditionId) {
		this.conditionId = conditionId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getMailerType() {
		return mailerType;
	}
	public void setMailerType(Integer mailerType) {
		this.mailerType = mailerType;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public JSONObject getParamsAsJson(){
		return FCStringUtils.jsonToMap(this.params);
	}
	
	public Map<String, Object> getTemp() {
		return temp;
	}
}
