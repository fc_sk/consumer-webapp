package com.freecharge.mailer.dao;

import java.util.Date;
import java.util.List;

/**
 * Interface for ARDeal DAO
 * This interface is implemented by the ARDealDBDao and ARDealCachingDao
 * 
 * One should always use the ARDealCachingDao for the application.
 * This should happen by default because ARDealCachingDao has been annotated as @Primary
 */
public interface IMailerCampaignDao {

    public long addMailerCampaign(String name, Long conditionId, Date startDate, Date endDate, Integer mailerType, 
    		Integer priority, Boolean enabled, String params);

    public void updateMailerCampaign(MailerCampaignEntity mailerEntity);
    
    public MailerCampaignEntity getMailerCampaignById(long id);

    public void deleteMailerCampaignById(long id);

    public void deleteAllMailerCampaigns();
   
    public int updateMailerCampaignStatus(long campaignId, boolean status);
    
    public List<MailerCampaignEntity> getEnabledMailerCampaigns();

    public List<MailerCampaignEntity> getAllMailerCamaigns();
    
    public void updateMailerCampaignPriority(Long id, Integer priority);

	public void updateEnabledMailerCampaigns();

	public List<MailerCampaignEntity> getMailerCampaignsByIds(String campaignIds);
    
}