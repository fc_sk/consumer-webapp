package com.freecharge.campaignhistory.entity;

/**
 * Created by Vivek on 30/03/16.
 */
public class ConditionResult {
    Boolean status;
    RuleResult failedRule;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public RuleResult getFailedRule() {
        return failedRule;
    }

    public void setFailedRule(RuleResult failedRule) {
        this.failedRule = failedRule;
    }

    @Override
    public String toString() {
        return "ConditionResult{" +
                "status=" + status +
                ", failedRule=" + failedRule +
                '}';
    }
}
