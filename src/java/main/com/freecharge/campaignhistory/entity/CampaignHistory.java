package com.freecharge.campaignhistory.entity;


import java.util.List;
import java.util.Map;

/**
 * Created by Vivek on 30/03/16.
 */
public class CampaignHistory {
    String id;
    Long userId;
    String userEmail;
    String orderId;
    String uniqueId;
    String imeiNumber;
    String advertisementId;
    String productType;
    String deviceUniqueId;
    String profileNumber;
    String serviceNumber;
    Integer merchantId;
    String merchantName;
    Integer campaignId;
    String campaignName;
    String trigger;
    Integer rechargeAmount;
    String cardHash;
    ConditionResult conditionResult;
    Long conditionResultUpdatedAt;
    Map<Integer, RewardResult> rewardProcessResults;
    Object nonRewardNotifications;
    Long createdAt;
    String oneCheckWalletId;
    String customerId;
    String customerName;
    String promocode;
    List<CampaignError> campaignErrors;
    Long updatedOn;
    String ffClassId;
    String channelId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(String advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    public String getProfileNumber() {
        return profileNumber;
    }

    public void setProfileNumber(String profileNumber) {
        this.profileNumber = profileNumber;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public Integer getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(Integer rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getCardHash() {
        return cardHash;
    }

    public void setCardHash(String cardHash) {
        this.cardHash = cardHash;
    }

    public ConditionResult getConditionResult() {
        return conditionResult;
    }

    public void setConditionResult(ConditionResult conditionResult) {
        this.conditionResult = conditionResult;
    }

    public Long getConditionResultUpdatedAt() {
        return conditionResultUpdatedAt;
    }

    public void setConditionResultUpdatedAt(Long conditionResultUpdatedAt) {
        this.conditionResultUpdatedAt = conditionResultUpdatedAt;
    }

    public Map<Integer, RewardResult> getRewardProcessResults() {
        return rewardProcessResults;
    }

    public void setRewardProcessResults(Map<Integer, RewardResult> rewardProcessResults) {
        this.rewardProcessResults = rewardProcessResults;
    }

    public Object getNonRewardNotifications() {
        return nonRewardNotifications;
    }

    public void setNonRewardNotifications(Object nonRewardNotifications) {
        this.nonRewardNotifications = nonRewardNotifications;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getOneCheckWalletId() {
        return oneCheckWalletId;
    }

    public void setOneCheckWalletId(String oneCheckWalletId) {
        this.oneCheckWalletId = oneCheckWalletId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public List<CampaignError> getCampaignErrors() {
        return campaignErrors;
    }

    public void setCampaignErrors(List<CampaignError> campaignErrors) {
        this.campaignErrors = campaignErrors;
    }

    public Long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getFfClassId() {
        return ffClassId;
    }

    public void setFfClassId(String ffClassId) {
        this.ffClassId = ffClassId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @Override
    public String toString() {
        return "CampaignHistory{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", userEmail='" + userEmail + '\'' +
                ", orderId='" + orderId + '\'' +
                ", uniqueId='" + uniqueId + '\'' +
                ", imeiNumber='" + imeiNumber + '\'' +
                ", advertisementId='" + advertisementId + '\'' +
                ", productType='" + productType + '\'' +
                ", deviceUniqueId='" + deviceUniqueId + '\'' +
                ", profileNumber='" + profileNumber + '\'' +
                ", serviceNumber='" + serviceNumber + '\'' +
                ", merchantId=" + merchantId +
                ", merchantName='" + merchantName + '\'' +
                ", campaignId=" + campaignId +
                ", campaignName='" + campaignName + '\'' +
                ", trigger='" + trigger + '\'' +
                ", rechargeAmount=" + rechargeAmount +
                ", cardHash='" + cardHash + '\'' +
                ", conditionResult=" + conditionResult +
                ", conditionResultUpdatedAt=" + conditionResultUpdatedAt +
                ", rewardProcessResults=" + rewardProcessResults +
                ", nonRewardNotifications=" + nonRewardNotifications +
                ", createdAt=" + createdAt +
                ", oneCheckWalletId='" + oneCheckWalletId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", promocode='" + promocode + '\'' +
                ", campaignErrors=" + campaignErrors +
                ", updatedOn=" + updatedOn +
                ", ffClassId='" + ffClassId + '\'' +
                ", channelId='" + channelId + '\'' +
                '}';
    }
}
