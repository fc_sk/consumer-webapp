package com.freecharge.campaignhistory.entity;

import java.util.Map;

/**
 * Created by Vivek on 28/03/16.
 */
public class Campaign {
    Integer id;
    String name;
    Integer merchantId;
    String corporateAccountId;
    String offerConstruct;
    String termsAndConditions;
    String howToRedeem;
    String pageLink;
    String startTime;
    String endTime;
    Integer freefundClassId;
    String createdAt;
    String createdBy;
    String updatedAt;
    String updatedBy;
    String campaignState;
    String campaignType;
    Boolean closedGroup;
    String lookupDataMap;
    String lookupDataValues;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getCorporateAccountId() {
        return corporateAccountId;
    }

    public void setCorporateAccountId(String corporateAccountId) {
        this.corporateAccountId = corporateAccountId;
    }

    public String getOfferConstruct() {
        return offerConstruct;
    }

    public void setOfferConstruct(String offerConstruct) {
        this.offerConstruct = offerConstruct;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getHowToRedeem() {
        return howToRedeem;
    }

    public void setHowToRedeem(String howToRedeem) {
        this.howToRedeem = howToRedeem;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getFreefundClassId() {
        return freefundClassId;
    }

    public void setFreefundClassId(Integer freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCampaignState() {
        return campaignState;
    }

    public void setCampaignState(String campaignState) {
        this.campaignState = campaignState;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Boolean getClosedGroup() {
        return closedGroup;
    }

    public void setClosedGroup(Boolean closedGroup) {
        this.closedGroup = closedGroup;
    }

    public String getLookupDataMap() {
        return lookupDataMap;
    }

    public void setLookupDataMap(String lookupDataMap) {
        this.lookupDataMap = lookupDataMap;
    }

    public String getLookupDataValues() {
        return lookupDataValues;
    }

    public void setLookupDataValues(String lookupDataValues) {
        this.lookupDataValues = lookupDataValues;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", merchantId=" + merchantId +
                ", corporateAccountId='" + corporateAccountId + '\'' +
                ", offerConstruct='" + offerConstruct + '\'' +
                ", termsAndConditions='" + termsAndConditions + '\'' +
                ", howToRedeem='" + howToRedeem + '\'' +
                ", pageLink='" + pageLink + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", freefundClassId=" + freefundClassId +
                ", createdAt='" + createdAt + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", campaignState='" + campaignState + '\'' +
                ", campaignType='" + campaignType + '\'' +
                ", closedGroup=" + closedGroup +
                ", lookupDataMap='" + lookupDataMap + '\'' +
                ", lookupDataValues='" + lookupDataValues + '\'' +
                '}';
    }
}
