package com.freecharge.campaignhistory.entity;

/**
 * Created by Vivek on 30/03/16.
 */
public class RewardReattemptResponse {
    Boolean status;
    String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RewardReattemptResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}

