package com.freecharge.campaignhistory.entity;

/**
 * Created by Vivek on 31/03/16.
 */
public class RewardInfo {
    Integer percent;
    Integer failedCreditedAmount;
    Integer successfulCreditedAmount;
    Integer creditedAmount;
    Integer maxAmount;
    Integer expiryInDays;

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public Integer getFailedCreditedAmount() {
        return failedCreditedAmount;
    }

    public void setFailedCreditedAmount(Integer failedCreditedAmount) {
        this.failedCreditedAmount = failedCreditedAmount;
    }

    public Integer getSuccessfulCreditedAmount() {
        return successfulCreditedAmount;
    }

    public void setSuccessfulCreditedAmount(Integer successfulCreditedAmount) {
        this.successfulCreditedAmount = successfulCreditedAmount;
    }

    public Integer getCreditedAmount() {
        return creditedAmount;
    }

    public void setCreditedAmount(Integer creditedAmount) {
        this.creditedAmount = creditedAmount;
    }

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getExpiryInDays() {
        return expiryInDays;
    }

    public void setExpiryInDays(Integer expiryInDays) {
        this.expiryInDays = expiryInDays;
    }

    @Override
    public String toString() {
        return "RewardInfo{" +
                "percent=" + percent +
                ", failedCreditedAmount=" + failedCreditedAmount +
                ", successfulCreditedAmount=" + successfulCreditedAmount +
                ", creditedAmount=" + creditedAmount +
                ", maxAmount=" + maxAmount +
                ", expiryInDays=" + expiryInDays +
                '}';
    }
}
