package com.freecharge.campaignhistory.entity;

import java.util.List;

/**
 * Created by Vivek on 30/03/16.
 */
public class RuleResult {
    String ruleId;
    String key;
    List<Object> allowedValues;
    String name;
    String actualValue;
    Boolean status;
    String failureMessage;
    String detailedFailureMessage;
    Boolean notificationEnabled;
    Integer failureEmailTemplateId;
    Integer failureSMSTemplateId;
    Integer failurePushTemplateId;

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Object> getAllowedValues() {
        return allowedValues;
    }

    public void setAllowedValues(List<Object> allowedValues) {
        this.allowedValues = allowedValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActualValue() {
        return actualValue;
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getDetailedFailureMessage() {
        return detailedFailureMessage;
    }

    public void setDetailedFailureMessage(String detailedFailureMessage) {
        this.detailedFailureMessage = detailedFailureMessage;
    }

    public Boolean getNotificationEnabled() {
        return notificationEnabled;
    }

    public void setNotificationEnabled(Boolean notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
    }

    public Integer getFailureEmailTemplateId() {
        return failureEmailTemplateId;
    }

    public void setFailureEmailTemplateId(Integer failureEmailTemplateId) {
        this.failureEmailTemplateId = failureEmailTemplateId;
    }

    public Integer getFailureSMSTemplateId() {
        return failureSMSTemplateId;
    }

    public void setFailureSMSTemplateId(Integer failureSMSTemplateId) {
        this.failureSMSTemplateId = failureSMSTemplateId;
    }

    public Integer getFailurePushTemplateId() {
        return failurePushTemplateId;
    }

    public void setFailurePushTemplateId(Integer failurePushTemplateId) {
        this.failurePushTemplateId = failurePushTemplateId;
    }

    @Override
    public String toString() {
        return "RuleResult{" +
                "ruleId='" + ruleId + '\'' +
                ", key='" + key + '\'' +
                ", allowedValues=" + allowedValues +
                ", name='" + name + '\'' +
                ", actualValue='" + actualValue + '\'' +
                ", status=" + status +
                ", failureMessage='" + failureMessage + '\'' +
                ", detailedFailureMessage='" + detailedFailureMessage + '\'' +
                ", notificationEnabled=" + notificationEnabled +
                ", failureEmailTemplateId=" + failureEmailTemplateId +
                ", failureSMSTemplateId=" + failureSMSTemplateId +
                ", failurePushTemplateId=" + failurePushTemplateId +
                '}';
    }
}
