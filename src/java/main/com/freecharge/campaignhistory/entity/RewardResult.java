package com.freecharge.campaignhistory.entity;

import java.util.List;

/**
 * Created by Vivek on 30/03/16.
 */
public class RewardResult {
    String status;
    String message;
    RewardInfo info;
    Integer childRewardId;
    Integer childRewardMasterId;
    String type;
    List<RewardTransaction> txnResult;
    Long userId;
    String refId;
    Integer rewardId;
    String orderId;
    Integer campaignId;
    String uniqueId;
    String trigger;
    Object processResults;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RewardInfo getInfo() {
        return info;
    }

    public void setInfo(RewardInfo info) {
        this.info = info;
    }

    public Integer getChildRewardId() {
        return childRewardId;
    }

    public void setChildRewardId(Integer childRewardId) {
        this.childRewardId = childRewardId;
    }

    public Integer getChildRewardMasterId() {
        return childRewardMasterId;
    }

    public void setChildRewardMasterId(Integer childRewardMasterId) {
        this.childRewardMasterId = childRewardMasterId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<RewardTransaction> getTxnResult() {
        return txnResult;
    }

    public void setTxnResult(List<RewardTransaction> txnResult) {
        this.txnResult = txnResult;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Integer getRewardId() {
        return rewardId;
    }

    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public Object getProcessResults() {
        return processResults;
    }

    public void setProcessResults(Object processResults) {
        this.processResults = processResults;
    }
    @Override
    public String toString() {
        return "RewardResult{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", info=" + info +
                ", childRewardId=" + childRewardId +
                ", childRewardMasterId=" + childRewardMasterId +
                ", type='" + type + '\'' +
                ", txnResult=" + txnResult +
                ", userId=" + userId +
                ", refId='" + refId + '\'' +
                ", rewardId=" + rewardId +
                ", orderId='" + orderId + '\'' +
                ", campaignId=" + campaignId +
                ", uniqueId='" + uniqueId + '\'' +
                ", trigger='" + trigger + '\'' +
                ", processResults=" + processResults +
                '}';
    }
}
