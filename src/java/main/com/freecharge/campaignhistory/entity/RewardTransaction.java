package com.freecharge.campaignhistory.entity;

/**
 * Created by Vivek on 31/03/16.
 */
public class RewardTransaction {
    String idempotencyId;
    Integer amount;
    String status;
    String txnId;

    public String getIdempotencyId() {
        return idempotencyId;
    }

    public void setIdempotencyId(String idempotencyId) {
        this.idempotencyId = idempotencyId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    @Override
    public String toString() {
        return "RewardTransaction{" +
                "idempotencyId='" + idempotencyId + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                ", txnId='" + txnId + '\'' +
                '}';
    }
}
