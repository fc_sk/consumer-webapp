package com.freecharge.campaignhistory.entity;

/**
 * Created by Vivek on 31/03/16.
 */
public class CampaignError {
    String failedService;
    String message;

    public String getFailedService() {
        return failedService;
    }

    public void setFailedService(String failedService) {
        this.failedService = failedService;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CampaignError{" +
                "failedService='" + failedService + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
