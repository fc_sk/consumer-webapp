package com.freecharge.campaignhistory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.freecharge.campaignhistory.entity.Campaign;
import com.freecharge.campaignhistory.entity.CampaignHistory;
import com.freecharge.campaignhistory.entity.RewardReattemptResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.virtualcard.exception.FundPostException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.xml.ws.http.HTTPException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vivek on 28/03/16.
 */
@Service("campaignHistoryService")
public class CampaignHistoryService {

    @Autowired
    FCProperties fcProperties;

    private static Logger logger                               = LoggingFactory.getLogger(CampaignHistoryService.class);
    private static final String    allCampaignsURI             = "/api/getrecentcampaigns";
    private static final String    getCampaignsURI             = "/api/user-campaign-history";
    private static final String    retriggerRewardURI          = "/api/manualreward";
    public final static String     STATUS_SUCCESS              = "Success";
    public final static String     STATUS_FAILED               = "Failed";
    public final static String     STATUS_STRING               = "Status";
    public final static String     RESPONSE_STRING             = "Response";
    public final static String     ERROR_MESSAGE_STRING        = "ErrorMessage";
    private final static String    CAMPAIGN_ID_STRING          = "campaignId";
    private final static String    EMAIL_STRING                = "email";
    private final static String    UNIQUE_ID_STRING            = "uniqueId";
    private final static String    TRIGGER_STRING              = "trigger";

    public List<Campaign> getAllCampaigns() {
        List<Campaign> campaignList = null;
        logger.debug("Fetching all campaigns from Campaign History");
        StringBuilder restUri = new StringBuilder();
        ObjectMapper mapper = new ObjectMapper();
        String campaignServiceURI = fcProperties.getProperty(FCProperties.CAMPAIGN_SERVICE_URI);
        restUri.append(campaignServiceURI).append(allCampaignsURI);

        logger.debug("Campaign History Fetch URI: " + restUri);
        StringBuilder status = new StringBuilder();
        try {
            RestTemplate restTemplate = new RestTemplate();
            String responseReceived = restTemplate.getForObject(new URI(restUri.toString()), String.class);
            campaignList = mapper.readValue(responseReceived, TypeFactory.defaultInstance().
                    constructCollectionType(List.class, Campaign.class));
            logger.info("campaignListReceived: " + campaignList + " received from campaign service");
        } catch (RestClientException e) {
            status.append("Service threw an exception while fetching all campaigns via rest call");
            logger.error(status, e);
        } catch (URISyntaxException e) {
            status.append("Service threw an exception while forming URI for fetching all campaigns via rest call");
            logger.error(status, e);
        } catch (Exception e) {
            status.append("Unexpected exception while fetching all campaigns");
            logger.error(status, e);
        }
        return campaignList;
    }

    public Map<String, Object> getCampaignByUser(String emailId, Integer campaignId) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        List<CampaignHistory> campaignHistory = new ArrayList<CampaignHistory>();
        StringBuilder restUri = new StringBuilder();
        ObjectMapper objectMapper = new ObjectMapper();
        logger.info("Getting campaign history by email: "+emailId+" and campaignId: "+campaignId);
        String campaignServiceURI = fcProperties.getProperty(FCProperties.CAMPAIGN_SERVICE_URI);
        restUri.append(campaignServiceURI).append(getCampaignsURI).append("/").append(emailId).append("/").
                append(campaignId);
        try {
            RestTemplate restTemplate = new RestTemplate();
            String responseReceived = restTemplate.getForObject(new URI(restUri.toString()), String.class);
            campaignHistory = objectMapper.readValue(responseReceived, TypeFactory.defaultInstance().
                    constructCollectionType(List.class, CampaignHistory.class));
            logger.info("Response received from Campaign History: "+campaignHistory);
            responseMap.put(STATUS_STRING, STATUS_SUCCESS);
            responseMap.put(RESPONSE_STRING, campaignHistory);
        }
        catch (UnsupportedEncodingException e) {
            String error = "Error in encoding JSON request to String for email: "+emailId+", campaignId: "+campaignId;
            logger.error(error, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_MESSAGE_STRING, error);
        }
        catch (JsonParseException e) {
            String error = "Error in parsing JSON response for email: "+emailId+", campaignId: "+campaignId;
            logger.error(error, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_MESSAGE_STRING, error);
        }
        catch (Exception e) {
            String error = "Error in getting campaign history for email: "+emailId+", campaignId: "+campaignId;
            logger.error(error, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_MESSAGE_STRING, error);
        }
        return responseMap;
    }

    public RewardReattemptResponse reAttemptReward(Integer campaignId, String trigger, String uniqueId) {
        logger.info("Starting re-attempt of reward for campaignId: "+campaignId+", trigger: "+trigger+", uniqueId: "
        +uniqueId);
        JSONObject jsonRequestObject = new JSONObject();
        RewardReattemptResponse rewardReattemptResponse = new RewardReattemptResponse();
        try {
            jsonRequestObject.put(TRIGGER_STRING, trigger);
            jsonRequestObject.put(CAMPAIGN_ID_STRING, campaignId);
            jsonRequestObject.put(UNIQUE_ID_STRING, uniqueId);
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonRequest = jsonRequestObject.toString();
            HttpClient client = new HttpClient();
            StringBuilder restUri = new StringBuilder();
            //restUri.append(FCProperties.CAMPAIGN_SERVICE_URI).append(retriggerRewardURI);
            PostMethod postMethod = new PostMethod(restUri.toString());                 //TODO: To be added
            StringRequestEntity requestEntity = new StringRequestEntity(jsonRequest, "application/json", "UTF-8");
            postMethod.setRequestEntity(requestEntity);
            int statusCode = client.executeMethod(postMethod);
            if (statusCode == 200) {
                String reattemptRewardResponse = postMethod.getResponseBodyAsString();
                logger.info("Response received from Campaign History: "+reattemptRewardResponse);
                rewardReattemptResponse = objectMapper.readValue(reattemptRewardResponse,
                        RewardReattemptResponse.class);
            } else {
                logger.error("HTTP Response status code returned: "+statusCode);
                rewardReattemptResponse.setStatus(false);
                rewardReattemptResponse.setMessage("HTTP Response code wrong");
            }
        }
        catch (JSONException jsonException) {
            String error = "Error in parsing JSON request for uniqueId: "+uniqueId+", campaignId: "+campaignId+
                    ", trigger: "+trigger;
            logger.error(error, jsonException);
            rewardReattemptResponse.setStatus(false);
            rewardReattemptResponse.setMessage(error);
        }
        catch (UnsupportedEncodingException e) {
            String error = "Error in encoding JSON request to String for uniqueId: "+uniqueId+", campaignId: "+campaignId+
                    ", trigger: "+trigger;
            logger.error(error, e);
            rewardReattemptResponse.setStatus(false);
            rewardReattemptResponse.setMessage(error);
        }
        catch (JsonParseException e) {
            String error = "Error in parsing JSON response for uniqueId: "+uniqueId+", campaignId: "+campaignId+
                    ", trigger: "+trigger;
            logger.error(error, e);
            rewardReattemptResponse.setStatus(false);
            rewardReattemptResponse.setMessage(error);
        }
        catch (Exception e) {
            String error = "Error in reattempting reward for uniqueId: "+uniqueId+", campaignId: "+campaignId+
                    ", trigger: "+trigger;
            logger.error(error, e);
            rewardReattemptResponse.setStatus(false);
            rewardReattemptResponse.setMessage(error);
        }
        return rewardReattemptResponse;
    }
}
