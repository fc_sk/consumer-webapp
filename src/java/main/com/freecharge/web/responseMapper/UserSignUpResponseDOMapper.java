package com.freecharge.web.responseMapper;

import com.freecharge.common.framework.basedo.BaseResponseDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.ResponseDOMapper;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.web.responsedo.UserSignUpResponseDO;
import com.freecharge.web.webdo.UserDO;

public class UserSignUpResponseDOMapper implements ResponseDOMapper<BaseWebDO, BaseResponseDO> {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseResponseDO baseResponseDO, WebContext webContext) {
		
		return null;
	}

	@Override
	public BaseResponseDO convertWebDOToResponseDO(BaseWebDO baseWebDO, WebContext webContext) {
		
		UserSignUpResponseDO userSignUpResponseDO = new UserSignUpResponseDO();
		UserDO userDO = (UserDO) baseWebDO;
		userSignUpResponseDO.setRegistrationStatus(userDO.isRegistrationStatusFlag());
		userSignUpResponseDO.setUserId(userDO.getUserId());
		userSignUpResponseDO.setUserProfileId(userDO.getUserProfileId());
		
		return userSignUpResponseDO;
	}

}
