package com.freecharge.web.responseMapper;

import com.freecharge.common.framework.basedo.BaseResponseDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.ResponseDOMapper;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.web.responsedo.UserAuthLoginResponseDO;
import com.freecharge.web.webdo.UserLoginDetailsWebDO;

public class UserAuthLoginResponseDOMapper implements ResponseDOMapper<BaseWebDO, BaseResponseDO> {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseResponseDO baseResponseDO, WebContext webContext) {
		
		return null;
	}

	@Override
	public BaseResponseDO convertWebDOToResponseDO(BaseWebDO baseWebDO, WebContext webContext) {
		
		UserAuthLoginResponseDO userAuthLoginResponseDO = new UserAuthLoginResponseDO();
		UserLoginDetailsWebDO userLoginDetailsWebDO = (UserLoginDetailsWebDO) baseWebDO;
		userAuthLoginResponseDO.setLoggedIn(userLoginDetailsWebDO.isLoggedIn());
		userAuthLoginResponseDO.setDisplayName(userLoginDetailsWebDO.getFirstName());
		userAuthLoginResponseDO.setUserId(userLoginDetailsWebDO.getUserId());
		userAuthLoginResponseDO.setUserProfileId(userLoginDetailsWebDO.getUserProfileId());
		return userAuthLoginResponseDO;
	}

}