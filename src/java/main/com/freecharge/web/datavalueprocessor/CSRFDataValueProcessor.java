package com.freecharge.web.datavalueprocessor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.freecharge.common.framework.util.CSRFTokenManager;
import org.springframework.web.servlet.support.RequestDataValueProcessor;

/**
 * This class injects CSRF token as a hidden field in all spring forms.
 */
public class CSRFDataValueProcessor implements RequestDataValueProcessor {
    @Override
    public String processAction(HttpServletRequest httpServletRequest, String action) {
        return action; //default
    }

    @Override
    public String processFormFieldValue(HttpServletRequest httpServletRequest, String name, String value, String type) {
        return value; //default
    }

    @Override
    public Map<String, String> getExtraHiddenFields(HttpServletRequest httpServletRequest) {
        Map<String, String> hiddenFields = new HashMap<String, String>(1);
        String token = CSRFTokenManager.getTokenFromSession();
        hiddenFields.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, token);
        return hiddenFields;
    }

    @Override
    public String processUrl(HttpServletRequest httpServletRequest, String url) {
        return url; //default
    }
}
