package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.businessdo.CouponInventoryBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.CouponInventoryWebDO;

public class CouponInventoryDOMapper implements WebDOMapper {

	@Autowired
	VouchersQuantityDOMapper vouchersQtyDOMapper;
	
	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		CouponInventoryWebDO couponInventoryWebDO = new CouponInventoryWebDO();
		
		CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;
		couponInventoryWebDO.setOrderId(couponInventoryBusinessDO.getOrderId());
		couponInventoryWebDO.setCouponInventoryList(couponInventoryBusinessDO.getCouponInventoryList());
		couponInventoryWebDO.setCouponCodes(couponInventoryBusinessDO.getCouponCodes());
		return couponInventoryWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		CouponInventoryBusinessDO couponInventoryBusinessDO = new CouponInventoryBusinessDO();
		
		CouponInventoryWebDO couponInventoryWebDO = (CouponInventoryWebDO) baseWebDO;
		couponInventoryBusinessDO.setOrderId(couponInventoryWebDO.getOrderId());
		couponInventoryBusinessDO.setCouponInventoryList(couponInventoryWebDO.getCouponInventoryList());
		couponInventoryBusinessDO.setCouponCodes(couponInventoryWebDO.getCouponCodes());
		return couponInventoryBusinessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}
}
