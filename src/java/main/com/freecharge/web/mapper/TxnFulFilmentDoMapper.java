package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.TxnFulFilmentBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.TxnFulFilmentWebDo;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: May 16, 2012
 * Time: 5:08:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class TxnFulFilmentDoMapper implements WebDOMapper {

    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        TxnFulFilmentWebDo txnFulFilmentWebDo = new TxnFulFilmentWebDo();
        txnFulFilmentWebDo.setAddressMobile(((TxnFulFilmentBusinessDo)baseBusinessDO).getAddressMobile());
        txnFulFilmentWebDo.setBillingAdd(((TxnFulFilmentBusinessDo)baseBusinessDO).getBillingAdd());
        txnFulFilmentWebDo.setBillingCity(((TxnFulFilmentBusinessDo)baseBusinessDO).getBillingCity());
        txnFulFilmentWebDo.setBillingCountry(((TxnFulFilmentBusinessDo)baseBusinessDO).getBillingCountry());
        txnFulFilmentWebDo.setBillingPincode(((TxnFulFilmentBusinessDo)baseBusinessDO).getBillingPincode());
        txnFulFilmentWebDo.setDeliveryAdd(((TxnFulFilmentBusinessDo)baseBusinessDO).getDeliveryAdd());
        txnFulFilmentWebDo.setDeliveryCity(((TxnFulFilmentBusinessDo)baseBusinessDO).getDeliveryCity());
        txnFulFilmentWebDo.setDeliveryCountry(((TxnFulFilmentBusinessDo)baseBusinessDO).getDeliveryCountry());
        txnFulFilmentWebDo.setDeliveryName(((TxnFulFilmentBusinessDo)baseBusinessDO).getDeliveryName());
        txnFulFilmentWebDo.setDeliveryPincode(((TxnFulFilmentBusinessDo)baseBusinessDO).getDeliveryPincode());
        txnFulFilmentWebDo.setEmail(((TxnFulFilmentBusinessDo)baseBusinessDO).getEmail());
        txnFulFilmentWebDo.setGuest(((TxnFulFilmentBusinessDo)baseBusinessDO).getGuest());
        txnFulFilmentWebDo.setOrderId(((TxnFulFilmentBusinessDo)baseBusinessDO).getOrderId());
        txnFulFilmentWebDo.setTransactionStatus(((TxnFulFilmentBusinessDo)baseBusinessDO).getTransactionStatus());
        txnFulFilmentWebDo.setTxnHomePageId(((TxnFulFilmentBusinessDo)baseBusinessDO).getTxnHomePageId());

        return txnFulFilmentWebDo;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        TxnFulFilmentBusinessDo txnFulFilmentBusinessDo = new TxnFulFilmentBusinessDo();
        txnFulFilmentBusinessDo.setAddressMobile(((TxnFulFilmentWebDo)baseWebDO).getAddressMobile());
                txnFulFilmentBusinessDo.setBillingAdd(((TxnFulFilmentWebDo)baseWebDO).getBillingAdd());
                txnFulFilmentBusinessDo.setBillingCity(((TxnFulFilmentWebDo)baseWebDO).getBillingCity());
                txnFulFilmentBusinessDo.setBillingCountry(((TxnFulFilmentWebDo)baseWebDO).getBillingCountry());
                txnFulFilmentBusinessDo.setBillingPincode(((TxnFulFilmentWebDo)baseWebDO).getBillingPincode());
                txnFulFilmentBusinessDo.setDeliveryAdd(((TxnFulFilmentWebDo)baseWebDO).getDeliveryAdd());
                txnFulFilmentBusinessDo.setDeliveryCity(((TxnFulFilmentWebDo)baseWebDO).getDeliveryCity());
                txnFulFilmentBusinessDo.setDeliveryCountry(((TxnFulFilmentWebDo)baseWebDO).getDeliveryCountry());
                txnFulFilmentBusinessDo.setDeliveryName(((TxnFulFilmentWebDo)baseWebDO).getDeliveryName());
                txnFulFilmentBusinessDo.setDeliveryPincode(((TxnFulFilmentWebDo)baseWebDO).getDeliveryPincode());
                txnFulFilmentBusinessDo.setEmail(((TxnFulFilmentWebDo)baseWebDO).getEmail());
                txnFulFilmentBusinessDo.setGuest(((TxnFulFilmentWebDo)baseWebDO).getGuest());
                txnFulFilmentBusinessDo.setOrderId(((TxnFulFilmentWebDo)baseWebDO).getOrderId());
                txnFulFilmentBusinessDo.setTransactionStatus(((TxnFulFilmentWebDo)baseWebDO).getTransactionStatus());
                txnFulFilmentBusinessDo.setTxnHomePageId(((TxnFulFilmentWebDo)baseWebDO).getTxnHomePageId());
                return txnFulFilmentBusinessDo;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }

}