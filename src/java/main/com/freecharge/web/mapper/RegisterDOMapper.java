package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.webdo.UserDO;

/**
 * Created with IntelliJ IDEA.
 * User: abc
 * Date: 5/3/12
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegisterDOMapper implements WebDOMapper {
	@Autowired
	private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;
	
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        UserDO baseWebDO = new UserDO();
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO) baseBusinessDO;
        baseWebDO.setEmail(registerBusinessDO.getEmail());
        baseWebDO.setPassword(registerBusinessDO.getPassword());
        baseWebDO.setDob(registerBusinessDO.getDob());
        baseWebDO.setMobileNo(registerBusinessDO.getMobileNo());
        baseWebDO.setMorf(registerBusinessDO.getMorf());
        baseWebDO.setRegistrationStatus(registerBusinessDO.getRegistrationStatus());
        baseWebDO.setIsLoggedin(registerBusinessDO.getIsLoggedin());
        //baseWebDO.setSessionData(((RegisterBusinessDO)baseBusinessDO).getSessionData());
        baseWebDO.setFirstName(registerBusinessDO.getFirstName());
        baseWebDO.setAddress1(registerBusinessDO.getAddress1());
        baseWebDO.setCity(registerBusinessDO.getCity());
        baseWebDO.setCityId(registerBusinessDO.getCityId());
        baseWebDO.setPostalCode(registerBusinessDO.getPostalCode());
        baseWebDO.setStateId(registerBusinessDO.getStateId());
        baseWebDO.setCountryId(registerBusinessDO.getCountryId());
        baseWebDO.setStateMaster(registerBusinessDO.getStateMaster());
        baseWebDO.setCountryMaster(registerBusinessDO.getCountryMaster());
        baseWebDO.setArea(registerBusinessDO.getArea());
        baseWebDO.setStreet(registerBusinessDO.getStreet());
        baseWebDO.setLandmark(registerBusinessDO.getLandmark());
        baseWebDO.setUserProfileId(registerBusinessDO.getUserProfileId());
        baseWebDO.setUserId(registerBusinessDO.getUserId());
        String addr= registerBusinessDO.getAddress1()+", "+ registerBusinessDO.getCity()+", "+ registerBusinessDO.getState()+", "+ registerBusinessDO.getCountry()+", "+ registerBusinessDO.getPostalCode();
        baseWebDO.setComboAddr(addr);
        if (registerBusinessDO.getFkAffiliateProfileId() != null && registerBusinessDO.getFkAffiliateProfileId() >0)
        	baseWebDO.setAffiliate_unique_id(registerBusinessDO.getFkAffiliateProfileId().toString());
        else 
        	baseWebDO.setAffiliate_unique_id(fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.affiliate_unique_id")); //Explicitly setting Freecharge.in uniqueId
        baseWebDO.setType(registerBusinessDO.getType());
        return baseWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        RegisterBusinessDO baseBusinessDO = new RegisterBusinessDO();

        String stateId = webContext.getRequest().getParameter("stateId");
        if (stateId != null && stateId != "") {
            baseBusinessDO.setStateId(Integer.parseInt(stateId));
        } else {
            Integer stId = ((UserDO) baseWebDO).getStateId();
            if (stId != null) {
                baseBusinessDO.setStateId(stId);
            } else {
                baseBusinessDO.setStateId(FCConstants.DEFAULT_STATEID);
            }
        }

        //todo abhi - should we do the null check here
        String cityId = webContext.getRequest().getParameter("cityId");
        baseBusinessDO.setCityId(Integer.parseInt(cityId));

        baseBusinessDO.setEmail(((UserDO) baseWebDO).getEmail());
        baseBusinessDO.setPassword(((UserDO) baseWebDO).getPassword());
        baseBusinessDO.setDob(((UserDO) baseWebDO).getDob());
        baseBusinessDO.setMobileNo(((UserDO) baseWebDO).getMobileNo());
        baseBusinessDO.setMorf(((UserDO) baseWebDO).getMorf());
        baseBusinessDO.setNickAlias(((UserDO) baseWebDO).getNickAlias());
        baseBusinessDO.setAddress1(((UserDO) baseWebDO).getAddress1());
        baseBusinessDO.setCity(((UserDO) baseWebDO).getCity());
        baseBusinessDO.setLandmark(((UserDO) baseWebDO).getLandmark());
        baseBusinessDO.setPostalCode(((UserDO) baseWebDO).getPostalCode());
        baseBusinessDO.setFirstName(((UserDO) baseWebDO).getFirstName());
        baseBusinessDO.setServiceNumber(((UserDO) baseWebDO).getServiceNumber());
        baseBusinessDO.setState(((UserDO) baseWebDO).getState());
        Integer countryId = ((UserDO) baseWebDO).getCountryId();
        baseBusinessDO.setArea(((UserDO) baseWebDO).getArea());
        baseBusinessDO.setStreet(((UserDO) baseWebDO).getStreet());
        baseBusinessDO.setLandmark(((UserDO) baseWebDO).getLandmark());
        if (countryId != null) {
            baseBusinessDO.setCountryId(countryId);
        } else {
            baseBusinessDO.setCountryId(99);
        }


        baseBusinessDO.setTxnHomePageId(((UserDO) baseWebDO).getTxnHomePageId());
        //baseBusinessDO.setCountryId(((UserDO)baseWebDO).getCountryId());
        baseBusinessDO.setTitle(((UserDO) baseWebDO).getTitle());
        baseBusinessDO.setUserProfileId(((UserDO) baseWebDO).getUserProfileId());
        baseBusinessDO.setUserId(((UserDO) baseWebDO).getUserId());
        if (((UserDO) baseWebDO).getAffiliate_unique_id() != null && Integer.parseInt(((UserDO) baseWebDO).getAffiliate_unique_id()) >0)
        	baseBusinessDO.setFkAffiliateProfileId(Integer.parseInt(((UserDO) baseWebDO).getAffiliate_unique_id()));
        else 
        	 baseBusinessDO.setFkAffiliateProfileId(Integer.parseInt(fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.fk_affiliate_profile_id")));  //Explicitly setting Freecharge.in Id
        baseBusinessDO.setType(((UserDO) baseWebDO).getType());
        return baseBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
