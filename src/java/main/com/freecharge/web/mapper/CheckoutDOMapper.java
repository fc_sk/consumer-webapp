package com.freecharge.web.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.CheckoutBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CartItemsWebDO;
import com.freecharge.web.webdo.CartWebDo;
import com.freecharge.web.webdo.CheckoutWebDO;
import com.freecharge.web.webdo.PaymentTypeMasterWebDO;
import com.freecharge.web.webdo.PaymentTypeOptionWebDO;

@SuppressWarnings("rawtypes")
public class CheckoutDOMapper implements WebDOMapper {
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        CheckoutBusinessDO checkoutBusinessDO = (CheckoutBusinessDO) baseBusinessDO;
        CheckoutWebDO checkoutWebDO = (CheckoutWebDO) webContext.getBaseWebDO();

        checkoutWebDO.setLookupID(checkoutBusinessDO.getLookupID());
        checkoutWebDO.setTxnHomePageId(checkoutBusinessDO.getTxnHomePageId());
        checkoutWebDO.setDefaultProfile(checkoutBusinessDO.getDefaultProfile());
        checkoutWebDO.setSatus(checkoutBusinessDO.getSatus());
        checkoutWebDO.setOperatorImgUrl(checkoutBusinessDO.getOperatorImgUrl());
        checkoutWebDO.setProductType(checkoutBusinessDO.getProductType());
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();


        Boolean isloggedin = false;
        if (fs != null) {
            Map map = fs.getSessionData();
            if (map != null) {
                if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                    isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                }
            }
        }
        if (isloggedin != null && isloggedin) {
            List<PaymentTypeMasterWebDO> ptmws = new ArrayList<PaymentTypeMasterWebDO>();
            String key = checkoutBusinessDO.getProductType() + "_" + FCConstants.DEFAULT_AFFILIATE_ID;
            List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate.get(key);
            if (ptmbs != null) {
                for (PaymentTypeMasterBusinessDO ptmb : ptmbs) {
                    PaymentTypeMasterWebDO ptmw = new PaymentTypeMasterWebDO();
                    ptmw.setCode(ptmb.getCode());
                    ptmw.setId(ptmb.getId());
                    ptmw.setName(ptmb.getName());
                    List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation.get(ptmb.getRelationId());
                    List<PaymentTypeOptionWebDO> ptows = new ArrayList<PaymentTypeOptionWebDO>();
                    for (PaymentTypeOptionBusinessDO ptob : ptobs) {
                        PaymentTypeOptionWebDO ptow = new PaymentTypeOptionWebDO();
                        ptow.setCode(ptob.getCode());
                        ptow.setDisplayImg(ptob.getDisplayImg());
                        ptow.setDisplayName(ptob.getDisplayName());
                        ptow.setId(ptob.getId());
                        ptow.setImgUrl(ptob.getImgUrl());
                        ptows.add(ptow);
                    }
                    ptmw.setOptions(ptows);
                    ptmws.add(ptmw);
                }
            }

            checkoutWebDO.setPaymentTypes(ptmws);

            checkoutWebDO.setPayableTotalAmount(checkoutBusinessDO.getPaymentBreakup().getPayableTotalAmount().getAmount().toString());
            checkoutWebDO.setPayablePGAmount(checkoutBusinessDO.getPaymentBreakup().getPayablePGAmount().getAmount().toString());
            checkoutWebDO.setPayableWalletAmount(checkoutBusinessDO.getPaymentBreakup().getPayableWalletAmount().getAmount().toString());
            checkoutWebDO.setWalletBalance(checkoutBusinessDO.getPaymentBreakup().getWalletBalance().getAmount().toString());
            checkoutWebDO.setWalletEnabled(checkoutBusinessDO.getWalletEnabled());
            checkoutWebDO.setWalletDisplayEnabled(checkoutBusinessDO.getWalletDisplayEnabled());
            checkoutWebDO.setWalletEligibility(checkoutBusinessDO.getWalletEligibility());
            checkoutWebDO.setPaymentBreakup(checkoutBusinessDO.getPaymentBreakup());

            checkoutWebDO.setCardStorageOptIn(checkoutBusinessDO.getCardStorageOptIn());
            checkoutWebDO.setIsOffersEnabled(checkoutBusinessDO.getIsOffersEnabled());
            CartBusinessDo cartBusinessDo = checkoutBusinessDO.getCartBusinessDo();
            if (cartBusinessDo != null) {
                CartWebDo cartWebDo = new CartWebDo();
                cartWebDo.setId(cartBusinessDo.getId());
                cartWebDo.setLookupID(cartBusinessDo.getLookupID());
                cartWebDo.setOpen(cartBusinessDo.getOpen());
                cartWebDo.setOrderId(cartBusinessDo.getOrderId());
                cartWebDo.setUserId(cartBusinessDo.getUserId());
                Double totalPrice = 0d;

                List<CartItemsWebDO> list = new ArrayList<CartItemsWebDO>();
                for (CartItemsBusinessDO cibo : cartBusinessDo.getCartItems()) {
                    if (cibo.getDeleted())
                        continue;
                    CartItemsWebDO ciw = new CartItemsWebDO();
                    ciw.setDisplayLabel(cibo.getDisplayLabel());
                    ciw.setId(cibo.getId());
                    ciw.setImgUrl("");
                    ciw.setItemId(cibo.getItemId());
                    ciw.setItemTableName(cibo.getEntityId());
                    ciw.setLookupID(cibo.getLookupID());
                    ciw.setPrice(cibo.getPrice());
                    ciw.setProductId(cibo.getProductMasterId());
                    ciw.setMerchantName("Testing");
                    ciw.setQuantity(cibo.getQuantity());

                    Integer productType = FCConstants.productMasterType.get(cibo.getProductMasterId());
                    String productTypeStr = FCConstants.productTypeMap.get(productType);
                    ciw.setProductType(productTypeStr);

                    if (!(ciw.getItemTableName() != null && ciw.getItemTableName().equalsIgnoreCase("site_merchant"))) {
                        totalPrice += cibo.getPrice();
                    }

                    if (ciw.getItemTableName() != null && ciw.getItemTableName().equalsIgnoreCase("FreeFund")) {
                        totalPrice -= 30;
                    }

                    list.add(ciw);
                }

                cartWebDo.setItemsList(list);
                cartWebDo.setTotalCartPrice(totalPrice);

                checkoutWebDO.setCartWebDo(cartWebDo);
            }
        }
        return checkoutWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        CheckoutBusinessDO checkoutBusinessDO = new CheckoutBusinessDO();

        checkoutBusinessDO.setLookupID(((CheckoutWebDO) baseWebDO).getLookupID());

        return checkoutBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }
}
