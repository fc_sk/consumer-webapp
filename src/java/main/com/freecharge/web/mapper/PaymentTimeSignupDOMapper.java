package com.freecharge.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.webdo.CrosssellWebDo;
import com.freecharge.web.webdo.PaymentTypeMasterWebDO;
import com.freecharge.web.webdo.PaymentTypeOptionWebDO;
import com.freecharge.web.webdo.UserDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 26, 2012
 * Time: 7:14:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentTimeSignupDOMapper implements WebDOMapper{
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        UserDO baseWebDO = new UserDO();
        baseWebDO.setRegistrationStatus(((RegisterBusinessDO)baseBusinessDO).getRegistrationStatus());
        baseWebDO.setStateMaster(((RegisterBusinessDO)baseBusinessDO).getStateMaster());
        baseWebDO.setDefaultProfile(((RegisterBusinessDO)baseBusinessDO).getDefaultProfile());
        baseWebDO.setUserContactDetails(((RegisterBusinessDO)baseBusinessDO).getUserContactDetails());
        baseWebDO.setCityMasterList(((RegisterBusinessDO)baseBusinessDO).getCityMasterList());
        List<CrosssellWebDo> cwds = new ArrayList<CrosssellWebDo>();

        List<CrosssellBusinessDo> crosssellBusinessDoList = ((RegisterBusinessDO)baseBusinessDO).getCrosssellBusinessDos();
        for(CrosssellBusinessDo crosssellBusinessDo: crosssellBusinessDoList) {
        	CrosssellWebDo crosssellWebDo = new CrosssellWebDo();
        	crosssellWebDo.setCategory(crosssellBusinessDo.getCategory());
        	crosssellWebDo.setDescription(crosssellBusinessDo.getDescription());
        	crosssellWebDo.setHeadDescription(crosssellBusinessDo.getHeadDescription());
        	crosssellWebDo.setId(crosssellBusinessDo.getId());
        	crosssellWebDo.setImgurl(crosssellBusinessDo.getImgurl());
        	crosssellWebDo.setLookupID(crosssellBusinessDo.getLookupID());
        	crosssellWebDo.setOrder(crosssellBusinessDo.getOrder());
        	crosssellWebDo.setPrice(crosssellBusinessDo.getPrice());
        	crosssellWebDo.setRefundable(crosssellBusinessDo.isRefundable());
        	crosssellWebDo.setMaxuses(crosssellBusinessDo.getMaxuses());
        	crosssellWebDo.setOthercondition(crosssellBusinessDo.getOthercondition());
        	crosssellWebDo.setPrimaryproduct(crosssellBusinessDo.getPrimaryproduct());
        	crosssellWebDo.setPrimaryproductamount(crosssellBusinessDo.getPrimaryproductamount());
        	crosssellWebDo.setTitle(crosssellBusinessDo.getTitle());
        	crosssellWebDo.setClassName(crosssellBusinessDo.getClassName());
        	crosssellWebDo.setTemplate(crosssellBusinessDo.getTemplate());
        	crosssellWebDo.setTnc(crosssellBusinessDo.getTnc());
        	crosssellWebDo.setMailTnc(crosssellBusinessDo.getMailTnc());
        	crosssellWebDo.setMailSubject(crosssellBusinessDo.getMailSubject());
        	crosssellWebDo.setMailImgUrl(crosssellBusinessDo.getMailImgUrl());
        	
            cwds.add(crosssellWebDo);
        }
        baseWebDO.setCrosssellWebDo(cwds);

        List<PaymentTypeMasterWebDO> ptmws = new ArrayList<PaymentTypeMasterWebDO>();
        String key = ((RegisterBusinessDO)baseBusinessDO).getProductType() + "_" + FCConstants.DEFAULT_AFFILIATE_ID;
        List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate.get(key);
        if ( ptmbs != null){
            for(PaymentTypeMasterBusinessDO ptmb : ptmbs){
                PaymentTypeMasterWebDO ptmw = new PaymentTypeMasterWebDO();
                ptmw.setCode(ptmb.getCode());
                ptmw.setId(ptmb.getId());
                ptmw.setName(ptmb.getName());
                List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation.get(ptmb.getRelationId());
                List<PaymentTypeOptionWebDO> ptows = new ArrayList<PaymentTypeOptionWebDO>();
                for(PaymentTypeOptionBusinessDO ptob : ptobs){
                    PaymentTypeOptionWebDO ptow = new PaymentTypeOptionWebDO();
                    ptow.setCode(ptob.getCode());
                    ptow.setDisplayImg(ptob.getDisplayImg());
                    ptow.setDisplayName(ptob.getDisplayName());
                    ptow.setId(ptob.getId());
                    ptow.setImgUrl(ptob.getImgUrl());
                    ptows.add(ptow);
                }
                ptmw.setOptions(ptows);
                ptmws.add(ptmw);
            }
        }

        baseWebDO.setPaymentTypes(ptmws);

        return baseWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        RegisterBusinessDO baseBusinessDO = new RegisterBusinessDO();
        Integer  stateId=((UserDO)baseWebDO).getStateId();
        baseBusinessDO.setStateId(stateId == null ? FCConstants.DEFAULT_STATEID:stateId);

        Integer cityId = ((UserDO)(baseWebDO)).getCityId();
        baseBusinessDO.setCityId(cityId);

        baseBusinessDO.setEmail(((UserDO)baseWebDO).getEmail().trim());
        baseBusinessDO.setMobileNo(((UserDO)baseWebDO).getMobileNo());
        //baseBusinessDO.setStreet(((UserDO)baseWebDO).getStreet());
        baseBusinessDO.setAddress1(((UserDO)baseWebDO).getAddress1());
        baseBusinessDO.setCity(((UserDO)baseWebDO).getCity());
        baseBusinessDO.setPostalCode(((UserDO)baseWebDO).getPostalCode());
        baseBusinessDO.setFirstName(((UserDO)baseWebDO).getFirstName());
        baseBusinessDO.setServiceNumber(((UserDO)baseWebDO).getServiceNumber());
        baseBusinessDO.setState(((UserDO)baseWebDO).getState());
        Integer countryId=((UserDO)baseWebDO).getCountryId();
        baseBusinessDO.setCountryId(countryId == null ? FCConstants.DEFAULT_COUNTRY_ID:countryId);
        baseBusinessDO.setTxnHomePageId(((UserDO)baseWebDO).getTxnHomePageId());
        baseBusinessDO.setTitle(((UserDO)baseWebDO).getTitle());
        baseBusinessDO.setLandmark(((UserDO)baseWebDO).getLandmark());
        baseBusinessDO.setArea(((UserDO)baseWebDO).getArea());
        baseBusinessDO.setPassword(((UserDO)baseWebDO).getPassword());
        baseBusinessDO.setConfirmPassword(((UserDO)baseWebDO).getConfirmPassword());
        baseBusinessDO.setLookupID(((UserDO)baseWebDO).getLookupID());
        return baseBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
