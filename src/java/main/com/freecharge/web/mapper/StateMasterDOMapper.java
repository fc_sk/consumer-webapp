package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.StateMasterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.StateMasterWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 20, 2012
 * Time: 2:10:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class StateMasterDOMapper implements WebDOMapper{
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
       
        StateMasterWebDO stateMasterWebDO=new StateMasterWebDO();
        stateMasterWebDO.setStateMaster(((StateMasterBusinessDO)(baseBusinessDO)).getStateList());
        return stateMasterWebDO;

    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        return new StateMasterBusinessDO();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
