package com.freecharge.web.mapper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.businessdo.PasswordRecoveryBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.PasswordRecoveryWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2012
 * Time: 12:40:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class PasswordRecoveryDOMapper implements WebDOMapper{
	@Autowired
    EPinEncrypt ePinEncrypt;
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
      PasswordRecoveryBusinessDO passwordRecoveryBusinessDO=  (PasswordRecoveryBusinessDO)baseBusinessDO;
        PasswordRecoveryWebDO passwordRecoveryWebDO=new PasswordRecoveryWebDO();
         passwordRecoveryWebDO.setUserExist(passwordRecoveryBusinessDO.getUserExist());
         passwordRecoveryWebDO.setEncryptValue(passwordRecoveryBusinessDO.getEncryptValue());
         passwordRecoveryWebDO.setEmail(passwordRecoveryBusinessDO.getEmail());
         if(passwordRecoveryBusinessDO.getStatus().equals("ValidLink")){
	         try {
				passwordRecoveryWebDO.setEncryptEmail(ePinEncrypt.getEncryptedPin(passwordRecoveryBusinessDO.getEmail()));
				passwordRecoveryWebDO.setUserId(ePinEncrypt.getEncryptedPin(passwordRecoveryBusinessDO.getUserId()));
			} catch (Exception e) {
				logger.error("exception occurred while encrypting the email id:"+e);
			}
         }
        passwordRecoveryWebDO.setStatus(passwordRecoveryBusinessDO.getStatus());
        return passwordRecoveryWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        PasswordRecoveryBusinessDO passwordRecoveryBusinessDO=new PasswordRecoveryBusinessDO();
        PasswordRecoveryWebDO passwordRecoveryWebDO=(PasswordRecoveryWebDO)baseWebDO;
        String encValue=webContext.getRequest().getParameter("esource");
        //String encUserId=webContext.getRequest().getParameter("eid");
        if(encValue == null && encValue == "" )
        return null;
        passwordRecoveryBusinessDO.setEncryptValue(encValue);
        
        return passwordRecoveryBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
