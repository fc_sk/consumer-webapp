package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.SuccessfulRechargeBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.SuccessfulRechargeWebDO;

public class SuccessfulRechargeDOMapper implements WebDOMapper{

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO,
			WebContext webContext) {
		SuccessfulRechargeBusinessDO businessDO =(SuccessfulRechargeBusinessDO)baseBusinessDO;
		SuccessfulRechargeWebDO webDO = new SuccessfulRechargeWebDO();
		webDO.setSuccessfulRecharges(businessDO.getSuccessfulRecharges());
		webDO.setSuccessOrderIds(businessDO.getSuccessOrderIds());
		webDO.setUserEmail(businessDO.getUserEmail());
		return webDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO,
			WebContext webContext) {
		SuccessfulRechargeWebDO webDO =(SuccessfulRechargeWebDO)baseWebDO;
		SuccessfulRechargeBusinessDO businessDO = new SuccessfulRechargeBusinessDO();
		businessDO.setSuccessfulRecharges(webDO.getSuccessfulRecharges());
		businessDO.setSuccessOrderIds(webDO.getSuccessOrderIds());
		businessDO.setUserEmail(webDO.getUserEmail());
		return businessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
