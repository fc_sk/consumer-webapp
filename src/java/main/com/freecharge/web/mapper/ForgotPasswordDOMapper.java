package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.ForgotPasswordBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.ForgotPasswordWebDo;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/2/12
 * Time: 8:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordDOMapper implements WebDOMapper {


    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
       ForgotPasswordBusinessDO forgotPasswordBusinessDO=(ForgotPasswordBusinessDO)baseBusinessDO ;
       ForgotPasswordWebDo forgotPasswordWebDo=new ForgotPasswordWebDo();
       forgotPasswordWebDo.setMessage(forgotPasswordBusinessDO.getStatus());
       forgotPasswordWebDo.setUserValid(forgotPasswordBusinessDO.getUserValid());
        return forgotPasswordWebDo;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        ForgotPasswordWebDo forgotPasswordWebDo=(ForgotPasswordWebDo) baseWebDO ;
        ForgotPasswordBusinessDO forgotPasswordBusinessDO=new ForgotPasswordBusinessDO()    ;
        //forgotPasswordBusinessDO.setEmail(forgotPasswordWebDo.getEmail());
        String email=webContext.getRequest().getParameter("email").trim();
        if(email != null && email != "")
        forgotPasswordBusinessDO.setEmail(email);
        
        return forgotPasswordBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }
}
