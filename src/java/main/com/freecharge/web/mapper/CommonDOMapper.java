package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.CommonBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.UserDO;

public class CommonDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO,WebContext webContext) {
		UserDO userDO = (UserDO)webContext.getBaseWebDO();
		CommonBusinessDO businessDO = (CommonBusinessDO)baseBusinessDO;
		userDO.setStateMaster(businessDO.getStateMaster());
		userDO.setCityMasterList(businessDO.getCityMasterList());
		return userDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO,WebContext webContext) {
		UserDO userDO = (UserDO)webContext.getBaseWebDO();
		CommonBusinessDO businessDO = new CommonBusinessDO();
		return businessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
