package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.tracker.Tracker;
import com.freecharge.web.webdo.HomeWebDo;
import com.freecharge.web.webdo.TxnHomePageWebDO;

/**
 * Created by IntelliJ IDEA. User: abc Date: May 17, 2012 Time: 6:08:19 PM To
 * change this template use File | Settings | File Templates.
 */
public class TxnHomePageDOMapper implements WebDOMapper {
    
    @Autowired
    private Tracker tracker;
    
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		TxnHomePageBusinessDO txnHomePageBusinessDO = (TxnHomePageBusinessDO) baseBusinessDO;
		TxnHomePageWebDO txnHomePageWebDO = new TxnHomePageWebDO();
		txnHomePageWebDO.setTxnHomePageId(txnHomePageBusinessDO.getTxnHomePageId());
		txnHomePageWebDO.setServiceNumber(txnHomePageBusinessDO.getServiceNumber());
		txnHomePageWebDO.setOperatorName(txnHomePageBusinessDO.getOperatorName());
		txnHomePageWebDO.setCircleName(txnHomePageBusinessDO.getCircleName());
		txnHomePageWebDO.setAmount(txnHomePageBusinessDO.getAmount());
		txnHomePageWebDO.setProductType(txnHomePageBusinessDO.getProductType());
		txnHomePageWebDO.setSessionId(txnHomePageBusinessDO.getSessionId());
		txnHomePageWebDO.setLookupId(txnHomePageBusinessDO.getLookupId());
		txnHomePageWebDO.setServiceStatus(txnHomePageBusinessDO.getServiceStatus());
		txnHomePageWebDO.setContactDetails(txnHomePageBusinessDO.getContactDetails());
		return txnHomePageWebDO;
	}

	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
		String amount = "";
		if (baseWebDO instanceof HomeWebDo) {
			HomeWebDo homeWebDo = (HomeWebDo) baseWebDO;

			if (homeWebDo.getType().equals("V")) {
				txnHomePageBusinessDO.setServiceNumber(homeWebDo.getMobileNumber());
				amount = homeWebDo.getAmount();
			} else if (homeWebDo.getType().equals("D")) {
				txnHomePageBusinessDO.setServiceNumber(homeWebDo.getDthNumber());
				amount = homeWebDo.getDthAmount();
			} else if (homeWebDo.getType().equals("C") || homeWebDo.getType().equals("F")) {
				txnHomePageBusinessDO.setServiceNumber(homeWebDo.getDataCardNumber());
				amount = homeWebDo.getDataCardAmount();
			}
			
			if (homeWebDo.getRechargeType()==null) {
				txnHomePageBusinessDO.setRechargePlanType(RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE);
			} else {
				txnHomePageBusinessDO.setRechargePlanType(homeWebDo.getRechargeType());
			}
			
			if (amount != null && amount != "") {
				txnHomePageBusinessDO.setAmount(new Float(amount.trim()));
			}
			txnHomePageBusinessDO.setOperatorName(homeWebDo.getOperator());
			txnHomePageBusinessDO.setCircleName(homeWebDo.getCircle());
			txnHomePageBusinessDO.setProductType(homeWebDo.getType());
			String lookupID = FCUtil.getLookupID();
			tracker.trackLookupId(lookupID);
			webContext.getRequest().setAttribute("lookupID", lookupID);
			txnHomePageBusinessDO.setLookupId(lookupID);
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			fs.setRechargePlanType(txnHomePageBusinessDO.getRechargePlanType());
			String sessionId = fs.getUuid();
			txnHomePageBusinessDO.setSessionId(sessionId);
                        
                        if ("1".equals(homeWebDo.getFestiveMessageFlag())) {
                            txnHomePageBusinessDO.setFestiveMessage(homeWebDo.getFestiveMessage());
                        }
		} else if (baseWebDO instanceof TxnHomePageWebDO) {
			TxnHomePageWebDO txnHomePageWebDO = (TxnHomePageWebDO) baseWebDO;
			txnHomePageBusinessDO.setServiceNumber(txnHomePageWebDO.getServiceNumber());

			txnHomePageBusinessDO.setOperatorName(txnHomePageWebDO.getOperatorName());
			txnHomePageBusinessDO.setCircleName(txnHomePageWebDO.getCircleName());
			txnHomePageBusinessDO.setProductType(txnHomePageWebDO.getProductType());
			txnHomePageBusinessDO.setLookupId(txnHomePageWebDO.getLookupId());
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			String sessionId = fs.getUuid();
			// txnHomePageBusinessDO.setSessionId(sessionId);
                        txnHomePageBusinessDO.setFestiveMessage(txnHomePageWebDO.getFestiveMessage());
		}

		return txnHomePageBusinessDO;
	}

	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// To change body of implemented methods use File | Settings | File
		// Templates.
	}

	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		return null; // To change body of implemented methods use File |
		// Settings | File Templates.
	}
}
