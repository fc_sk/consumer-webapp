package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.MyContactsListBusinessDo;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.MyContactsListWebDo;

public class MyContactsListDOMapper implements WebDOMapper<MyContactsListWebDo, MyContactsListBusinessDo>{

	@Override
	public MyContactsListWebDo convertBusinessDOtoWebDO(
			MyContactsListBusinessDo baseBusinessDO, WebContext webContext) {
		MyContactsListWebDo contactsweblist=new MyContactsListWebDo();
		contactsweblist.setUserid(baseBusinessDO.getUserid());
		contactsweblist.setEmail(baseBusinessDO.getEmail());
		contactsweblist.setContactslist(baseBusinessDO.getContactslist());
		contactsweblist.setOperatorlist(baseBusinessDO.getOperatorlist());
		return contactsweblist;
	}

	@Override
	public MyContactsListBusinessDo convertWebDOToBusinessDO(
			MyContactsListWebDo baseWebDO, WebContext webContext) {
		MyContactsListBusinessDo contatsbusinessdo=new MyContactsListBusinessDo();
		contatsbusinessdo.setUserid(baseWebDO.getUserid());
		contatsbusinessdo.setEmail(baseWebDO.getEmail());
		contatsbusinessdo.setContactslist(contatsbusinessdo.getContactslist());
		contatsbusinessdo.setOperatorlist(baseWebDO.getOperatorlist());
		return contatsbusinessdo;
	}

	@Override
	public void mergeWebDos(MyContactsListWebDo oldBaseWebDO,
			MyContactsListWebDo newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MyContactsListWebDo createWebDO(MyContactsListWebDo baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
