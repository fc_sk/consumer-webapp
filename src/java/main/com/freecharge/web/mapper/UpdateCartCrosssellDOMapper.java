package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartCrosssellBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.util.Amount;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.webdo.CartCrosssellWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 9, 2012
 * Time: 3:02:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdateCartCrosssellDOMapper implements WebDOMapper{
    @Autowired
	private CartService cartService;
    
    @Autowired
    private WalletService walletService;
 
    @Autowired
    private PricingService pricingService;
    
    @Autowired
    private WalletWrapper walletWrapper;
    
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        CartCrosssellWebDO cartCrosssellWebDO = (CartCrosssellWebDO)webContext.getBaseWebDO();
        CartCrosssellBusinessDO cartCrosssellBusinessDO = (CartCrosssellBusinessDO)baseBusinessDO;
        cartCrosssellWebDO.setCartId(cartCrosssellBusinessDO.getCartId());
        cartCrosssellWebDO.setCartItemsId(cartCrosssellBusinessDO.getCartItemsId());
        cartCrosssellWebDO.setCrosssellAmount(cartCrosssellBusinessDO.getCrosssellAmount());
        cartCrosssellWebDO.setProductTypeId(cartCrosssellBusinessDO.getProductTypeId());
        cartCrosssellWebDO.setTxncrossellId(cartCrosssellBusinessDO.getTxncrossellId());
        cartCrosssellWebDO.setCrosssellId(cartCrosssellBusinessDO.getCrosssellId());
        cartCrosssellWebDO.setTitle(cartCrosssellBusinessDO.getTitle());
        CartBusinessDo cartToInspect = cartService.getCart(cartCrosssellBusinessDO.getLookupID());
        Double totalPrice = pricingService.getPayableAmount(cartToInspect);
        cartCrosssellWebDO.setTotalPrice(totalPrice);
        
        Amount walletBalance = walletWrapper.getTotalBalance(cartToInspect.getUserId());

        WalletEligibility walletEligibility = walletService.calculateEligibility(cartToInspect.getUserId(), new Amount(totalPrice), cartToInspect, walletBalance);
        cartCrosssellWebDO.setWalletEligibility(walletEligibility);
        
        PaymentBreakup paymentBreakup = walletService.calculatePaymentBreakup(cartToInspect.getUserId(), new Amount(totalPrice), cartToInspect, walletEligibility, walletBalance);
        cartCrosssellWebDO.setPaymentBreakup(paymentBreakup);
        
        return cartCrosssellWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        return null;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
