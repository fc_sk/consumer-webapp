package com.freecharge.web.mapper;

import org.apache.commons.lang.StringUtils;

import com.freecharge.recharge.util.RechargeConstants;

public class UIResponseMapper {

    public static String getRechargeReponseForUser(String dbInresponseCode) {
        if (dbInresponseCode == null) {
            return RechargeConstants.IN_UNDER_PROCESS_CODE;
        } else if (StringUtils.equals(dbInresponseCode, RechargeConstants.RECHARGE_RETRY_INTERNAL_CODE)) {
            return RechargeConstants.IN_UNDER_PROCESS_CODE;
        } else if (StringUtils.equals(dbInresponseCode.trim(), RechargeConstants.INITIAL_RESPONSE_STATUS)) {
            return RechargeConstants.IN_UNDER_PROCESS_CODE;
        } else {
            return dbInresponseCode.trim();
        }
    }

}
