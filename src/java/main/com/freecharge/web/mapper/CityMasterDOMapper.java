package com.freecharge.web.mapper;

import org.apache.log4j.Logger;

import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.CityMasterWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 1, 2012
 * Time: 4:39:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class CityMasterDOMapper implements WebDOMapper{
    private Logger logger = LoggingFactory.getLogger(getClass());
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        CityMasterWebDO baseWebDO = new CityMasterWebDO();
        baseWebDO.setStateId(((CityMasterBusinessDO)baseBusinessDO).getStateId());
        baseWebDO.setCityMasterList(((CityMasterBusinessDO)baseBusinessDO).getCityMasterList());
        return baseWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        CityMasterBusinessDO baseBusinessDO = new CityMasterBusinessDO();
        String stateIdStr =   webContext.getRequest().getParameter("stateId");
        Integer stateId=1;
        if(stateIdStr != null && stateIdStr != ""){
            try{
               stateId = Integer.parseInt(stateIdStr);
            }catch (NumberFormatException nfe){
                logger.error("Exception occured while converting stateId String into Integer.",nfe);
            }
        }
        baseBusinessDO.setStateId(stateId);
        return baseBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
