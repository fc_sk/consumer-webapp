package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.LoginWebDO;

public class UserAuthDOMapper implements WebDOMapper {


    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        LoginWebDO loginWebDO = new LoginWebDO();
           loginWebDO.setEmail(((LoginBusinessDO)baseBusinessDO).getEmail());
        loginWebDO.setPassword(((LoginBusinessDO)baseBusinessDO).getPassword());
        loginWebDO.setLogin(((LoginBusinessDO)baseBusinessDO).isLogin());
        loginWebDO.setRememberme(((LoginBusinessDO)baseBusinessDO).getRememberMe());
        loginWebDO.setUserId(((LoginBusinessDO)baseBusinessDO).getUserId());
        loginWebDO.setFirstName(((LoginBusinessDO)baseBusinessDO).getFirstName());
        return loginWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        LoginBusinessDO loginBusinessDO = new LoginBusinessDO();
          loginBusinessDO.setEmail(((LoginWebDO)baseWebDO).getEmail());
        loginBusinessDO.setPassword(((LoginWebDO)baseWebDO).getPassword());
        loginBusinessDO.setLogin(((LoginWebDO)baseWebDO).isLogin());
        loginBusinessDO.setRememberMe(((LoginWebDO)baseWebDO).getRememberme());
        loginBusinessDO.setUserId(((LoginWebDO)baseWebDO).getUserId());
        loginBusinessDO.setFirstName(((LoginWebDO)baseWebDO).getFirstName());
        return loginBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }
}