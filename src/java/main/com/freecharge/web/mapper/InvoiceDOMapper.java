package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.common.businessdo.InvoiceBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.web.webdo.InvoiceWebDO;

public class InvoiceDOMapper implements WebDOMapper {
	@Autowired
	FCProperties fcProperties;
	@Autowired
	private PricingService pricingService;
	
	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		InvoiceBusinessDO invoiceBusinessDO = ((InvoiceBusinessDO) baseBusinessDO);
		InvoiceWebDO invoiceWebDO = new InvoiceWebDO();
		
		if (invoiceBusinessDO.getRechargeInfoVO() != null) {
		invoiceWebDO.setRechargeInfoVO(invoiceBusinessDO.getRechargeInfoVO());
		invoiceBusinessDO.getRechargeInfoVO().setServiceCharge(0.0f);
		invoiceBusinessDO.getRechargeInfoVO().setCouponCharges(0.0);
		}
		if (invoiceBusinessDO.getCartAndCartItemsList() != null && invoiceBusinessDO.getCartAndCartItemsList().size() > 0) {
			Float recAmount = invoiceBusinessDO.getRechargeInfoVO().getTotalAmount();
			int handlingCharge = 0;
			if (invoiceBusinessDO.getPrimaryProduct() != null){
				if (fcProperties.isCouponPricingEnabled()){
					Double couponCharges = pricingService.getCouponCharges(invoiceBusinessDO.getOrderId());
					invoiceBusinessDO.getRechargeInfoVO().setCouponCharges(couponCharges);
				}
				handlingCharge = pricingService.getHandlingCharge(invoiceBusinessDO.getPrimaryProduct().getProductId());	
			}
			invoiceBusinessDO.getRechargeInfoVO().setTotalAmount(handlingCharge + recAmount);
			invoiceBusinessDO.getRechargeInfoVO().setServiceCharge(Float.valueOf(handlingCharge));
		}
		invoiceWebDO.getRechargeInfoVO().setHasPaidCoupons(pricingService.hasPaidCoupons(invoiceBusinessDO.getOrderId()));
		invoiceWebDO.setCartAndCartItemsList(invoiceBusinessDO.getCartAndCartItemsList());
		invoiceWebDO.setTotalAmount(invoiceBusinessDO.getTotalAmount());
		
		invoiceWebDO.setPrimaryProductName(invoiceBusinessDO.getPrimaryProduct());
		
		invoiceWebDO.setPaymentTime(invoiceBusinessDO.getPaymentTime());
		invoiceWebDO.setOrderId(invoiceBusinessDO.getOrderId());
		
		return invoiceWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		InvoiceWebDO invoiceWebDO = ((InvoiceWebDO) (baseWebDO));
		String orderId = webContext.getRequest().getParameter("orderId");
		InvoiceBusinessDO invoiceBusinessDO = new InvoiceBusinessDO();
		invoiceBusinessDO.setOrderId(orderId);
		invoiceWebDO.setOrderId(orderId);
		return invoiceBusinessDO;
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {

		return null;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

	}

}
