package com.freecharge.web.mapper;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.businessdo.ChangePasswordBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.ChangePasswordWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 9, 2012
 * Time: 8:37:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChangePasswordDOMapper  implements WebDOMapper {
	@Autowired
    EPinEncrypt ePinEncrypt;
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
         ChangePasswordBusinessDO changePasswordBusinessDO=(ChangePasswordBusinessDO)baseBusinessDO     ;
         ChangePasswordWebDO changePasswordWebDO=new ChangePasswordWebDO();
        changePasswordWebDO.setStatus(changePasswordBusinessDO.getStatus());
        return changePasswordWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        ChangePasswordBusinessDO changePasswordBusinessDO=new ChangePasswordBusinessDO();
        changePasswordBusinessDO.setEmail( ((ChangePasswordWebDO)(baseWebDO)).getEmail() );
        //EPinEncrypt ePinEncrypt=new EPinEncrypt();
        try {
           changePasswordBusinessDO.setNewPassword( ePinEncrypt.getEncryptedPin( ((ChangePasswordWebDO)(baseWebDO)).getNewPassword()) );
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
      
        return changePasswordBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
