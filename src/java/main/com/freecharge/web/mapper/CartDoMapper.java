package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.CartWebDo;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 28, 2012
 * Time: 11:31:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class CartDoMapper implements WebDOMapper {

    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        CartWebDo cartWebDo = new CartWebDo();
        cartWebDo.setLookupID(((CartBusinessDo)baseBusinessDO).getLookupID());

        return cartWebDo;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        CartBusinessDo cartBusinessDo = new CartBusinessDo();

        cartBusinessDo.setLookupID(((CartWebDo)baseWebDO).getLookupID());
        return cartBusinessDo;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }

}