package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.ReissueCouponInventoryBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.ReissueCouponInventoryWebDO;

public class ReissueCouponInventoryDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		ReissueCouponInventoryWebDO reissueCouponInventoryWebDO = new ReissueCouponInventoryWebDO();
		
		ReissueCouponInventoryBusinessDO reissueCouponInventoryBusinessDO = (ReissueCouponInventoryBusinessDO) baseBusinessDO;
		reissueCouponInventoryWebDO.setOrderId(reissueCouponInventoryBusinessDO.getOrderId());
		reissueCouponInventoryWebDO.setDeletedCouponInventoryDetails(reissueCouponInventoryBusinessDO.getDeletedCouponInventoryDetails());
		reissueCouponInventoryWebDO.setReissueCouponInventoryDetails(reissueCouponInventoryBusinessDO.getReissueCouponInventoryDetails());
		reissueCouponInventoryWebDO.setDeletedCouponInventoryList(reissueCouponInventoryBusinessDO.getDeletedCouponInventoryList());
		reissueCouponInventoryWebDO.setReissueCouponInventoryList(reissueCouponInventoryBusinessDO.getReissueCouponInventoryList());
		reissueCouponInventoryWebDO.setFcTicketId(reissueCouponInventoryBusinessDO.getFcTicketId());
		reissueCouponInventoryWebDO.setIs_deleted(reissueCouponInventoryBusinessDO.isIs_deleted());
		reissueCouponInventoryWebDO.setRemarks(reissueCouponInventoryBusinessDO.getRemarks());
		reissueCouponInventoryWebDO.setCreatedBy(reissueCouponInventoryBusinessDO.getCreatedBy());
		reissueCouponInventoryWebDO.setDeletedBy(reissueCouponInventoryBusinessDO.getDeletedBy());
		
		return reissueCouponInventoryWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		ReissueCouponInventoryBusinessDO reissueCouponInventoryBusinessDO = new ReissueCouponInventoryBusinessDO();
		
		ReissueCouponInventoryWebDO reissueCouponInventoryWebDO =  (ReissueCouponInventoryWebDO) baseWebDO;
		reissueCouponInventoryBusinessDO.setOrderId(reissueCouponInventoryWebDO.getOrderId());
		reissueCouponInventoryBusinessDO.setDeletedCouponInventoryDetails(reissueCouponInventoryWebDO.getDeletedCouponInventoryDetails());
		reissueCouponInventoryBusinessDO.setReissueCouponInventoryDetails(reissueCouponInventoryWebDO.getReissueCouponInventoryDetails());
		reissueCouponInventoryBusinessDO.setDeletedCouponInventoryList(reissueCouponInventoryWebDO.getDeletedCouponInventoryList());
		reissueCouponInventoryBusinessDO.setReissueCouponInventoryList(reissueCouponInventoryWebDO.getReissueCouponInventoryList());
		reissueCouponInventoryBusinessDO.setFcTicketId(reissueCouponInventoryWebDO.getFcTicketId());
		reissueCouponInventoryBusinessDO.setIs_deleted(reissueCouponInventoryWebDO.isIs_deleted());
		reissueCouponInventoryBusinessDO.setRemarks(reissueCouponInventoryWebDO.getRemarks());
		reissueCouponInventoryBusinessDO.setCreatedBy(reissueCouponInventoryWebDO.getCreatedBy());
		reissueCouponInventoryBusinessDO.setDeletedBy(reissueCouponInventoryWebDO.getDeletedBy());
		
		return reissueCouponInventoryBusinessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
