package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.TestBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.TestWebDO;

public class TestDOMapper implements WebDOMapper {


    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        TestWebDO testWebDO = new TestWebDO();
                testWebDO.setOne(((TestBusinessDO)baseBusinessDO).getOne()+1001);
                testWebDO.setDbName(((TestBusinessDO)baseBusinessDO).getDbName());
        return testWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        TestBusinessDO baseBusinessDO = new TestBusinessDO();
        baseBusinessDO.setOne(((TestWebDO)baseWebDO).getOne()*1000);
        baseBusinessDO.setTwo(((TestWebDO)baseWebDO).getOne()*2000);

        return baseBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {

    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;
    }
}
