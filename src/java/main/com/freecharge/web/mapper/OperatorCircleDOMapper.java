package com.freecharge.web.mapper;

import java.util.Iterator;
import java.util.Map;

import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.webdo.OperatorCircleWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 23, 2012
 * Time: 12:13:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class OperatorCircleDOMapper implements WebDOMapper {
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        OperatorCircleBusinessDO operatorCircleBusinessDO=(OperatorCircleBusinessDO)baseBusinessDO;
        OperatorCircleWebDO operatorCircleWebDO=new OperatorCircleWebDO();

        operatorCircleWebDO.setPrefixData(operatorCircleBusinessDO.getPrefixData());
        //operatorCircleWebDO.setAllOperatorsList(operatorCircleBusinessDO.getAllOperatorsList());
        return operatorCircleWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
    String prefix=      webContext.getRequest().getParameter("prefix");
    String productType=      webContext.getRequest().getParameter("productType");    
       OperatorCircleBusinessDO operatorCircleBusinessDO=    new OperatorCircleBusinessDO();
        Map<Integer , String> mp= FCConstants.productMasterMap;
        Iterator it = mp.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                Integer key= (Integer)pairs.getKey();
              String value=(String)  pairs.getValue();
                if(value.equalsIgnoreCase(productType)){
                 operatorCircleBusinessDO.setProductType(key.toString());   
               break;
                }
 }
        if(prefix!=null && prefix.length()>4)
        operatorCircleBusinessDO.setPrefix(prefix.substring(0,FCConstants.PREFIX_LENGTH_FIVE_DIGIT));
        return operatorCircleBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
