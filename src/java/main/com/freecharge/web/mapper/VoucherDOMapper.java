package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.VouchersBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.VouchersWebDO;

public class VoucherDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		VouchersBusinessDO voucherBusinessDo =(VouchersBusinessDO)baseBusinessDO;
		VouchersWebDO vouchersWebDO=new VouchersWebDO();
		vouchersWebDO.setVoucherData(voucherBusinessDo.getVoucherData());
		vouchersWebDO.setVoucherIdList(voucherBusinessDo.getVoucherIdList());
		return vouchersWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO,	WebContext webContext) {
		VouchersBusinessDO voucherBusinessDo=new VouchersBusinessDO();
		return voucherBusinessDo ;
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

}
