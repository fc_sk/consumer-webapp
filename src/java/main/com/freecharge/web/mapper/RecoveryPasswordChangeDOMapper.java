package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.PasswordRecoveryBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.PasswordRecoveryWebDO;

public class RecoveryPasswordChangeDOMapper implements WebDOMapper{

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO,WebContext webContext) {
		PasswordRecoveryBusinessDO passwordRecoveryBusinessDO=  (PasswordRecoveryBusinessDO)baseBusinessDO;
        PasswordRecoveryWebDO passwordRecoveryWebDO=new PasswordRecoveryWebDO();
        passwordRecoveryWebDO.setStatus(passwordRecoveryBusinessDO.getStatus());
        passwordRecoveryWebDO.setEncryptValue(passwordRecoveryBusinessDO.getEncryptValue());
        return passwordRecoveryWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO,	WebContext webContext) {
		PasswordRecoveryBusinessDO passwordRecoveryBusinessDO=new PasswordRecoveryBusinessDO();
	    PasswordRecoveryWebDO passwordRecoveryWebDO=(PasswordRecoveryWebDO)baseWebDO;
        passwordRecoveryBusinessDO.setEmail(passwordRecoveryWebDO.getEmail());
        passwordRecoveryBusinessDO.setUserId(passwordRecoveryWebDO.getUserId());
        passwordRecoveryBusinessDO.setEncryptValue(passwordRecoveryWebDO.getEncryptValue());
        passwordRecoveryBusinessDO.setNewPassword(passwordRecoveryWebDO.getNewPassword());
        passwordRecoveryBusinessDO.setConfirmPassword(passwordRecoveryWebDO.getConfirmPassword());
        return passwordRecoveryBusinessDO;

	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
