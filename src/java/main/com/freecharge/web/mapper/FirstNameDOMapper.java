package com.freecharge.web.mapper;

import org.apache.log4j.Logger;

import com.freecharge.common.businessdo.FirstNameBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.FirstNameWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 22, 2012
 * Time: 5:44:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class FirstNameDOMapper implements WebDOMapper{
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        FirstNameWebDO firstNameWebDO = new FirstNameWebDO();
        FirstNameBusinessDO firstNameBusinessDO =(FirstNameBusinessDO)baseBusinessDO;
        firstNameWebDO.setFirstName(firstNameBusinessDO.getFirstName());
        firstNameWebDO.setTxnHomePageId(firstNameBusinessDO.getTxnHomePageId());
        firstNameWebDO.setStatus(firstNameBusinessDO.getStatus());
        return firstNameWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        FirstNameBusinessDO firstNameBusinessDO =new FirstNameBusinessDO();
        FirstNameWebDO firstNameWebDO =(FirstNameWebDO)baseWebDO;
        String firstname=webContext.getRequest().getParameter("firstName");
        firstNameBusinessDO.setFirstName(firstname);
        String txnHomePageId=webContext.getRequest().getParameter("txnHomePageId");
        if(txnHomePageId != null && txnHomePageId != ""){
            Integer txnid=null;
            try{
                txnid=new Integer(txnHomePageId);
            }catch (NumberFormatException e){
                 txnid=0;
                logger.error("Exception raised while converting txnHomePageId into integer",e);
            }
            firstNameBusinessDO.setTxnHomePageId(txnid);            
        }
        return firstNameBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
