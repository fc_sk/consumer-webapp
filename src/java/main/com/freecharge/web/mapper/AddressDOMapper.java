package com.freecharge.web.mapper;

import org.apache.log4j.Logger;

import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.webdo.AddressWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 23, 2012
 * Time: 4:03:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddressDOMapper implements WebDOMapper{
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        AddressBusinessDO addressBusinessDO=(AddressBusinessDO)baseBusinessDO;
        AddressWebDO addressWebDO= new AddressWebDO();
        addressWebDO.setAddress(addressBusinessDO.getAddress());
        addressWebDO.setTitle(addressBusinessDO.getTitle());
        addressWebDO.setFirstName(addressBusinessDO.getFirstName());
        //addressWebDO.setStreet(addressBusinessDO.getStreet());
        addressWebDO.setArea(addressBusinessDO.getArea());
        addressWebDO.setLandmark(addressBusinessDO.getLandmark());
        addressWebDO.setCity(addressBusinessDO.getCity());
        addressWebDO.setCityId(addressBusinessDO.getCityId());
        addressWebDO.setCountry(addressBusinessDO.getCountry());
        addressWebDO.setCountryId(addressBusinessDO.getCountryId());
        addressWebDO.setPostalCode(addressBusinessDO.getPostalCode());
        addressWebDO.setState(addressBusinessDO.getState());
        addressWebDO.setStateId(addressBusinessDO.getStateId());
        addressWebDO.setStateMaster(addressBusinessDO.getStateMaster());
        addressWebDO.setCountryMaster(addressBusinessDO.getCountryMaster());
        addressWebDO.setComboAddr(addressBusinessDO.getAddress()+", "+addressBusinessDO.getArea()+", "+addressBusinessDO.getLandmark()+", "+addressBusinessDO.getCity()+", "+addressBusinessDO.getState()+", "+addressBusinessDO.getCountry()+", "+addressBusinessDO.getPostalCode());
        addressWebDO.setStatus(addressBusinessDO.getStatus());
        addressWebDO.setTxnHomePageId(addressBusinessDO.getTxnHomePageId());
        addressWebDO.setTxnfulfillmentId(addressBusinessDO.getTxnfulfilmentId());
        addressWebDO.setUserProfileId(addressBusinessDO.getUserProfileId());
        addressWebDO.setDefaultProfile(addressBusinessDO.getDefaultProfile());
        addressWebDO.setIsEdit(addressBusinessDO.getIsEdit());
        return addressWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
        AddressBusinessDO addressBusinessDO = new AddressBusinessDO();
        AddressWebDO addressWebDO = (AddressWebDO)baseWebDO;
        String title =  addressWebDO.getTitle();
        String firstName = addressWebDO.getFirstName();
        String addr = addressWebDO.getAddress();
        //String street = webContext.getRequest().getParameter("street");
        String area = addressWebDO.getArea();
        String landmark =addressWebDO.getLandmark();
        String city =addressWebDO.getCity();
        String pincode = addressWebDO.getPostalCode();
        String defaultProfile = addressWebDO.getDefaultProfile();
        Integer cityId = addressWebDO.getCityId();
        String actionParam = webContext.getRequest().getParameter("action");
        String isEdit = webContext.getRequest().getParameter("isEdit");
        if(isEdit != null && isEdit != ""){
        	addressBusinessDO.setIsEdit(isEdit);
        }else{
        	addressBusinessDO.setIsEdit("yes");
        }
        Integer stateId = null;
        Integer stateIdInit = addressWebDO.getStateId();
        if(stateIdInit != null && stateIdInit !=0 ){            
            stateId=stateIdInit;            
        }else{
            stateId = FCConstants.DEFAULT_STATEID;
        }
        Integer countryId = null;
        Integer countryIdInit = addressWebDO.getCountryId();
        if(countryIdInit != null && countryIdInit != 0){           
            countryId=countryIdInit;            
        }else{
            countryId=FCConstants.DEFAULT_COUNTRY_ID;
        }
        Integer txnHomePageId = null;
        String txnHomePageIdStr = webContext.getRequest().getParameter("transactionHomePageId");
        if(txnHomePageIdStr != null && txnHomePageIdStr != ""){
            try{
                 txnHomePageId = Integer.parseInt(txnHomePageIdStr);
            }catch (NumberFormatException e){
                txnHomePageId = 0;
                logger.error("Exception occured while converting txnHomepageIdStr String into Integer..",e);
            }
        }else{
            txnHomePageId = 0;
        }
        Integer txnfulfilmentId = null;
        String txnfulfilmentIdStr =  webContext.getRequest().getParameter("txnfulfilmentId");
        if(txnfulfilmentIdStr != null && txnfulfilmentIdStr != ""){
            try{
                txnfulfilmentId = Integer.parseInt(txnfulfilmentIdStr);
            }catch (NumberFormatException nfe){
                txnfulfilmentId = 0;
                logger.error("Exception occured while converting txnfulfilmentId String tp Integer in AddressDOMapper..",nfe);
            }
        }else{
            txnfulfilmentId = 0; 
        }
        Integer userProfileId = 0;
        String userProfileIdStr = webContext.getRequest().getParameter("userProfileId");
        if(userProfileIdStr != null && userProfileIdStr != ""){
            try{
                userProfileId = Integer.parseInt(userProfileIdStr);
            }catch (NumberFormatException nfe){
                userProfileId = 0;
                logger.error("Exception occured while parsing userprofileId string to integer..",nfe);
            }
        }
        Integer userRechargeContactId = 0;
        String userRechargeContactIdStr = webContext.getRequest().getParameter("userRechargeContactId");
        if(userRechargeContactIdStr != null && userRechargeContactIdStr != ""){
            try{
                userRechargeContactId = Integer.parseInt(userRechargeContactIdStr);
            }catch (NumberFormatException nfe){
                userRechargeContactId = 0;
                logger.error("Exception occured while parsing userRechargeContactId String to Intager");
            }
        }
        Integer userId = 0;
        String userIdStr = webContext.getRequest().getParameter("userId");
        if(userIdStr != null && userIdStr != ""){
            try{
                userId = Integer.parseInt(userIdStr);
            }catch (NumberFormatException nfe){
                userId = 0;
                logger.error("Exception occured while parsing userId String to Integer..",nfe);
            }
        }
        addressBusinessDO.setTitle(title);
        addressBusinessDO.setFirstName(firstName);
        addressBusinessDO.setAddress(addr);
        //addressBusinessDO.setStreet(street);
        addressBusinessDO.setArea(area);
        addressBusinessDO.setLandmark(landmark);
        addressBusinessDO.setCity(city);
        addressBusinessDO.setCityId(cityId);
        addressBusinessDO.setPostalCode(pincode);
        addressBusinessDO.setStateId(stateId);
        addressBusinessDO.setCountryId(countryId);
        addressBusinessDO.setTxnHomePageId(txnHomePageId);
        addressBusinessDO.setTxnfulfilmentId(txnfulfilmentId);
        addressBusinessDO.setUserProfileId(userProfileId);
        addressBusinessDO.setUserRechargeContactId(userRechargeContactId);
        addressBusinessDO.setUserId(userId);
        addressBusinessDO.setDefaultProfile(defaultProfile);
        addressBusinessDO.setActionParam(actionParam);

        return addressBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
