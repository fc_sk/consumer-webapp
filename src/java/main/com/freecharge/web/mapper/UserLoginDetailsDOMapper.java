package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.UserLoginDetailsBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.UserLoginDetailsWebDO;

public class UserLoginDetailsDOMapper implements WebDOMapper {

	
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		UserLoginDetailsWebDO userLoginDetailsWebDO = new UserLoginDetailsWebDO();
		UserLoginDetailsBusinessDO userLoginDetailsBusinessDO = (UserLoginDetailsBusinessDO) baseBusinessDO;
		userLoginDetailsWebDO.setTitle(userLoginDetailsBusinessDO.getTitle());
		userLoginDetailsWebDO.setFirstName(userLoginDetailsBusinessDO.getFirstName());
		userLoginDetailsWebDO.setLastName(userLoginDetailsBusinessDO.getLastName());
		userLoginDetailsWebDO.setUserId(userLoginDetailsBusinessDO.getUserId());
		userLoginDetailsWebDO.setEmail(userLoginDetailsBusinessDO.getEmail());
		userLoginDetailsWebDO.setPassword(userLoginDetailsBusinessDO.getPassword());
		userLoginDetailsWebDO.setLoggedIn(userLoginDetailsBusinessDO.isLoggedIn());
		userLoginDetailsWebDO.setUserProfileId(userLoginDetailsBusinessDO.getUserProfileId());
		return userLoginDetailsWebDO;
		
	}


	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		UserLoginDetailsBusinessDO userLoginDetailsBusinessDO = new UserLoginDetailsBusinessDO();
		UserLoginDetailsWebDO userLoginDetailsWebDO = (UserLoginDetailsWebDO) baseWebDO;
		userLoginDetailsBusinessDO.setUserId(userLoginDetailsWebDO.getUserId());
		userLoginDetailsBusinessDO.setTitle(userLoginDetailsWebDO.getTitle());
		userLoginDetailsBusinessDO.setFirstName(userLoginDetailsWebDO.getFirstName());
		userLoginDetailsBusinessDO.setLastName(userLoginDetailsWebDO.getLastName());
		userLoginDetailsBusinessDO.setEmail(userLoginDetailsWebDO.getEmail());
		userLoginDetailsBusinessDO.setPassword(userLoginDetailsWebDO.getPassword());
		userLoginDetailsBusinessDO.setLoggedIn(userLoginDetailsWebDO.isLoggedIn());
		userLoginDetailsBusinessDO.setUserProfileId(userLoginDetailsWebDO.getUserProfileId());
		return userLoginDetailsBusinessDO;
	}

	
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		
	}

	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		return null;
	}

}