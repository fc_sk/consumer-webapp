package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.MyContactsBussinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.MyContactsWebDO;

public class MyContactsDOMapper implements WebDOMapper<MyContactsWebDO, MyContactsBussinessDO> {

	@Override
	public MyContactsWebDO convertBusinessDOtoWebDO(
			MyContactsBussinessDO baseBusinessDO, WebContext webContext) {
		
		MyContactsWebDO mycontactswebdo=new MyContactsWebDO();
		mycontactswebdo.setUserid(baseBusinessDO.getUserid());
		mycontactswebdo.setEmail(baseBusinessDO.getEmail());
		mycontactswebdo.setName(baseBusinessDO.getName());
		mycontactswebdo.setServiceno(baseBusinessDO.getServiceno());
		mycontactswebdo.setOperator(baseBusinessDO.getOperator());
		mycontactswebdo.setOperatorlist(baseBusinessDO.getOperatorlist());
		mycontactswebdo.setProduct(baseBusinessDO.getProduct());
		mycontactswebdo.setProductlist(baseBusinessDO.getProductlist());
		mycontactswebdo.setContactid(baseBusinessDO.getContactid());
		mycontactswebdo.setCircle(baseBusinessDO.getCircle());
		mycontactswebdo.setUpdatestatus(baseBusinessDO.getUpdatestatus());
		mycontactswebdo.setOperatorname(baseBusinessDO.getOperatorname());
		return mycontactswebdo;
	}

	@Override
	public MyContactsBussinessDO convertWebDOToBusinessDO(
			MyContactsWebDO baseWebDO, WebContext webContext) {
		// TODO Auto-generated method stub
		MyContactsBussinessDO mycontactsbusinessdo=new MyContactsBussinessDO();
		mycontactsbusinessdo.setUserid(baseWebDO.getUserid());
		mycontactsbusinessdo.setEmail(baseWebDO.getEmail());
		mycontactsbusinessdo.setName(baseWebDO.getName());
		mycontactsbusinessdo.setServiceno(baseWebDO.getServiceno());
		mycontactsbusinessdo.setOperator(baseWebDO.getOperator());
		mycontactsbusinessdo.setOperatorlist(baseWebDO.getOperatorlist());
		mycontactsbusinessdo.setProduct(baseWebDO.getProduct());
		mycontactsbusinessdo.setProductlist(baseWebDO.getProductlist());
		mycontactsbusinessdo.setContactid(baseWebDO.getContactid());
		mycontactsbusinessdo.setCircle(baseWebDO.getCircle());
		mycontactsbusinessdo.setUpdatestatus(baseWebDO.getUpdatestatus());
		mycontactsbusinessdo.setOperatorname(baseWebDO.getOperatorname());
		return mycontactsbusinessdo;
	}

	@Override
	public void mergeWebDos(MyContactsWebDO oldBaseWebDO,
			MyContactsWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MyContactsWebDO createWebDO(MyContactsWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
