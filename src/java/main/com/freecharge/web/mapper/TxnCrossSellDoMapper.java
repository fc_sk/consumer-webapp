package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.webdo.TxnCrossSellWebDo;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: May 16, 2012
 * Time: 11:31:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class TxnCrossSellDoMapper implements WebDOMapper<TxnCrossSellWebDo, TxnCrossSellBusinessDo> {

    public TxnCrossSellWebDo convertBusinessDOtoWebDO(TxnCrossSellBusinessDo baseBusinessDO, WebContext webContext) {
        TxnCrossSellWebDo txnCrossSellWebDo = new TxnCrossSellWebDo();
        txnCrossSellWebDo.setCouponId(((TxnCrossSellBusinessDo)baseBusinessDO).getCouponId());
        txnCrossSellWebDo.setCrossSellId(((TxnCrossSellBusinessDo)baseBusinessDO).getCrossSellId());
        txnCrossSellWebDo.setHomePageId(((TxnCrossSellBusinessDo)baseBusinessDO).getHomePageId());
        txnCrossSellWebDo.setItemIdArray(((TxnCrossSellBusinessDo)baseBusinessDO).getItemIdArray());
        txnCrossSellWebDo.setType(((TxnCrossSellBusinessDo)baseBusinessDO).getType());
        return txnCrossSellWebDo;
    }

    public TxnCrossSellBusinessDo convertWebDOToBusinessDO(TxnCrossSellWebDo webDO, WebContext webContext) {
        TxnCrossSellBusinessDo txnCrossSellBusinessDo = new TxnCrossSellBusinessDo();
        txnCrossSellBusinessDo.setCouponId(webDO.getCouponId());
        txnCrossSellBusinessDo.setCrossSellId(webDO.getCrossSellId());
        txnCrossSellBusinessDo.setHomePageId(webDO.getHomePageId());
        txnCrossSellBusinessDo.setItemIdArray(webDO.getItemIdArray());
        txnCrossSellBusinessDo.setType(webDO.getType());
        txnCrossSellBusinessDo.setProType(webDO.getProType());
        txnCrossSellBusinessDo.setLookupID(webDO.getLookupID());
        
        // set request param properties
        String amount = webContext.getRequest().getParameter("amount");
        if(!FCUtil.isEmpty(amount)){
            txnCrossSellBusinessDo.setAmount(Float.parseFloat(amount));
        }
        // recharges
        txnCrossSellBusinessDo.setServiceNo(webContext.getRequest().getParameter("serviceNo"));
        txnCrossSellBusinessDo.setOperatorName(webContext.getRequest().getParameter("operatorName"));
        
        return txnCrossSellBusinessDo;
    }

    public void mergeWebDos(TxnCrossSellWebDo oldBaseWebDO, TxnCrossSellWebDo newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public TxnCrossSellWebDo createWebDO(TxnCrossSellWebDo baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
