package com.freecharge.web.mapper;

import java.util.ArrayList;
import java.util.List;

import com.freecharge.common.businessdo.VouchersQuantityBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.VouchersQuantityWebDO;

public class VouchersQuantityDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		VouchersQuantityWebDO vouchersQtyWebDO = new VouchersQuantityWebDO();
		
		VouchersQuantityBusinessDO vouchersQtyBusinessDO = (VouchersQuantityBusinessDO) baseBusinessDO;
		vouchersQtyWebDO.setCouponStoreId(vouchersQtyBusinessDO.getCouponStoreId());
		vouchersQtyWebDO.setQuantity(vouchersQtyBusinessDO.getQuantity());
		vouchersQtyWebDO.setComplimentary(vouchersQtyBusinessDO.isComplimentary());
		
		return vouchersQtyWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		VouchersQuantityBusinessDO vouchersQtyBusinessDO = new VouchersQuantityBusinessDO();
		
		VouchersQuantityWebDO vouchersQtyWebDO = (VouchersQuantityWebDO) baseWebDO;
		vouchersQtyBusinessDO.setCouponStoreId(vouchersQtyWebDO.getCouponStoreId());
		vouchersQtyBusinessDO.setQuantity(vouchersQtyWebDO.getQuantity());
		vouchersQtyBusinessDO.setComplimentary(vouchersQtyWebDO.isComplimentary());
		
		return vouchersQtyBusinessDO;
		
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<VouchersQuantityWebDO> convertBusinessDOListtoWebDOList(List<VouchersQuantityBusinessDO> list, WebContext webContext) {
		List<VouchersQuantityWebDO> vouchersQtyWebDOList = new ArrayList<VouchersQuantityWebDO>();
		
		for ( BaseBusinessDO baseBusinessDO : list) {
			vouchersQtyWebDOList.add((VouchersQuantityWebDO) convertBusinessDOtoWebDO(baseBusinessDO, webContext));
		}
		
		return vouchersQtyWebDOList;
	}
	
	public List<VouchersQuantityBusinessDO> convertWebDOListtoBusinessDOList(List<VouchersQuantityWebDO> list, WebContext webContext) {
		List<VouchersQuantityBusinessDO> vouchersQtyBusinessDOList = new ArrayList<VouchersQuantityBusinessDO>();
		
		for ( BaseWebDO baseWebDO : list) {
			vouchersQtyBusinessDOList.add((VouchersQuantityBusinessDO) convertWebDOToBusinessDO(baseWebDO, webContext));
		}
		
		return vouchersQtyBusinessDOList;
	}
	
}
