package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.FeedbackBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.FeedbackWebDO;

public class FeedbackDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO,WebContext webContext) {
		FeedbackBusinessDO feedbackBusinessDO = (FeedbackBusinessDO)webContext.getBaseBusinessDO();
		FeedbackWebDO feedbackWebDO = (FeedbackWebDO)webContext.getBaseWebDO();
		feedbackWebDO.setStatus(feedbackBusinessDO.getStatus());
		return feedbackWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO,WebContext webContext) {
		FeedbackWebDO feedbackWebDO = (FeedbackWebDO)webContext.getBaseWebDO();
		FeedbackBusinessDO feedbackBusinessDO = new FeedbackBusinessDO();
		feedbackBusinessDO.setEmail(feedbackWebDO.getEmail());
		feedbackBusinessDO.setTopicArea(feedbackWebDO.getTopicArea());
		feedbackBusinessDO.setSubject(feedbackWebDO.getSubject());
		feedbackBusinessDO.setMessage(feedbackWebDO.getMessage());
		return feedbackBusinessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub

	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

}
