package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.MyRechargeContactProfileBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.MyRechargeContactProfileWebDO;

public class MyRechargeContactProfileDOMapper implements WebDOMapper {

	@Override
	public MyRechargeContactProfileWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		MyRechargeContactProfileWebDO myRechargeContactProfileWebDO = new MyRechargeContactProfileWebDO();
		
		MyRechargeContactProfileBusinessDO myRechargeContactProfileBusinessDO = (MyRechargeContactProfileBusinessDO)baseBusinessDO;
		myRechargeContactProfileWebDO.setAddress1(myRechargeContactProfileBusinessDO.getAddress1());
		myRechargeContactProfileWebDO.setArea(myRechargeContactProfileBusinessDO.getArea());
		myRechargeContactProfileWebDO.setCity(myRechargeContactProfileBusinessDO.getCity());
		myRechargeContactProfileWebDO.setEmail(myRechargeContactProfileBusinessDO.getEmail());
		myRechargeContactProfileWebDO.setLandmark(myRechargeContactProfileBusinessDO.getLandmark());
		myRechargeContactProfileWebDO.setName(myRechargeContactProfileBusinessDO.getName());
		myRechargeContactProfileWebDO.setPincode(myRechargeContactProfileBusinessDO.getPincode());
		myRechargeContactProfileWebDO.setRechargeContactNo(myRechargeContactProfileBusinessDO.getRechargeContactNo());
		myRechargeContactProfileWebDO.setState(myRechargeContactProfileBusinessDO.getState());
		myRechargeContactProfileWebDO.setTitle(myRechargeContactProfileBusinessDO.getTitle());
		myRechargeContactProfileWebDO.setUserId(myRechargeContactProfileBusinessDO.getUserId());
		myRechargeContactProfileWebDO.setUserProfileId(myRechargeContactProfileBusinessDO.getUserProfileId());
		myRechargeContactProfileWebDO.setCircle(myRechargeContactProfileBusinessDO.getCircle());
		myRechargeContactProfileWebDO.setLastRechargeAmount(myRechargeContactProfileBusinessDO.getLastRechargeAmount());
		myRechargeContactProfileWebDO.setNickAlias(myRechargeContactProfileBusinessDO.getNickAlias());
		myRechargeContactProfileWebDO.setOperatorCode(myRechargeContactProfileBusinessDO.getOperatorCode());
		myRechargeContactProfileWebDO.setProduct(myRechargeContactProfileBusinessDO.getProduct());
		myRechargeContactProfileWebDO.setUserRechargeContactId(myRechargeContactProfileBusinessDO.getUserRechargeContactId());
		
		return myRechargeContactProfileWebDO;
	}

	@Override
	public MyRechargeContactProfileBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		MyRechargeContactProfileBusinessDO myRechargeContactProfileBusinessDO = new MyRechargeContactProfileBusinessDO();
		
		MyRechargeContactProfileWebDO myRechargeContactProfileWebDO = (MyRechargeContactProfileWebDO)baseWebDO;
		myRechargeContactProfileBusinessDO.setAddress1(myRechargeContactProfileWebDO.getAddress1());
		myRechargeContactProfileBusinessDO.setArea(myRechargeContactProfileWebDO.getArea());
		myRechargeContactProfileBusinessDO.setCity(myRechargeContactProfileWebDO.getCity());
		myRechargeContactProfileBusinessDO.setEmail(myRechargeContactProfileWebDO.getEmail());
		myRechargeContactProfileBusinessDO.setLandmark(myRechargeContactProfileWebDO.getLandmark());
		myRechargeContactProfileBusinessDO.setName(myRechargeContactProfileWebDO.getName());
		myRechargeContactProfileBusinessDO.setPincode(myRechargeContactProfileWebDO.getPincode());
		myRechargeContactProfileBusinessDO.setRechargeContactNo(myRechargeContactProfileWebDO.getRechargeContactNo());
		myRechargeContactProfileBusinessDO.setState(myRechargeContactProfileWebDO.getState());
		myRechargeContactProfileBusinessDO.setTitle(myRechargeContactProfileWebDO.getTitle());
		myRechargeContactProfileBusinessDO.setUserId(myRechargeContactProfileWebDO.getUserId());
		myRechargeContactProfileBusinessDO.setUserProfileId(myRechargeContactProfileWebDO.getUserProfileId());
		myRechargeContactProfileBusinessDO.setCircle(myRechargeContactProfileWebDO.getCircle());
		myRechargeContactProfileBusinessDO.setLastRechargeAmount(myRechargeContactProfileWebDO.getLastRechargeAmount());
		myRechargeContactProfileBusinessDO.setNickAlias(myRechargeContactProfileWebDO.getNickAlias());
		myRechargeContactProfileBusinessDO.setOperatorCode(myRechargeContactProfileWebDO.getOperatorCode());
		myRechargeContactProfileBusinessDO.setProduct(myRechargeContactProfileWebDO.getProduct());
		myRechargeContactProfileBusinessDO.setUserRechargeContactId(myRechargeContactProfileWebDO.getUserRechargeContactId());
		
		return myRechargeContactProfileBusinessDO;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}


}
