package com.freecharge.web.mapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.ProfileWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 7, 2012
 * Time: 4:55:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyProfileDOMapper implements WebDOMapper<ProfileWebDO, ProfileBusinessDO> {
	
	SimpleDateFormat dateformat=new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public ProfileWebDO convertBusinessDOtoWebDO(
			ProfileBusinessDO baseBusinessDO, WebContext webContext) {
		// TODO Auto-generated method stub
		ProfileWebDO profilewebdo=new ProfileWebDO();
		profilewebdo.setTitle(baseBusinessDO.getTitle());
		profilewebdo.setArea(baseBusinessDO.getArea());
		profilewebdo.setCity(baseBusinessDO.getCity());
		profilewebdo.setCitylist(baseBusinessDO.getCitylist());
		profilewebdo.setConfirmpassword(baseBusinessDO.getConfirmpassword());
		profilewebdo.setEmail(baseBusinessDO.getEmail());
		profilewebdo.setLandmar(baseBusinessDO.getLandmar());
		profilewebdo.setName(baseBusinessDO.getName());
		profilewebdo.setNewpassword(baseBusinessDO.getNewpassword());
		profilewebdo.setOldpassword(baseBusinessDO.getOldpassword());
		profilewebdo.setPincode(baseBusinessDO.getPincode());
		profilewebdo.setState(baseBusinessDO.getState());
		profilewebdo.setStatelist(baseBusinessDO.getStatelist());
		profilewebdo.setAddress(baseBusinessDO.getAddress());
		profilewebdo.setUserid(baseBusinessDO.getUserid());
		profilewebdo.setIspasswordupdate(baseBusinessDO.isIspasswordupdate());
		profilewebdo.setIsoldpswdcorrect(baseBusinessDO.isIsoldpswdcorrect());
		profilewebdo.setUserProfileId(baseBusinessDO.getUserProfileId());
		profilewebdo.setMorf(baseBusinessDO.getMorf());
		profilewebdo.setMobileNo(baseBusinessDO.getMobileNo());
		profilewebdo.setIsDefaultProfile(baseBusinessDO.isIsDefaultProfile());
		profilewebdo.setDob(baseBusinessDO.getDob());
		if(baseBusinessDO.getDob() != null){
			String dobstring=dateformat.format(baseBusinessDO.getDob());
		profilewebdo.setDobString(dobstring);
		}
		return profilewebdo;
	}

	@Override
	public ProfileBusinessDO convertWebDOToBusinessDO(ProfileWebDO baseWebDO,
			WebContext webContext) {
		// TODO Auto-generated method stub
		ProfileBusinessDO profilebusinessdo=new ProfileBusinessDO();
		profilebusinessdo.setTitel(baseWebDO.getTitle());
		profilebusinessdo.setArea(baseWebDO.getArea());
		profilebusinessdo.setCity(baseWebDO.getCity());
		profilebusinessdo.setCitylist(baseWebDO.getCitylist());
		profilebusinessdo.setConfirmpassword(baseWebDO.getConfirmpassword());
		profilebusinessdo.setEmail(baseWebDO.getEmail());
		profilebusinessdo.setLandmar(baseWebDO.getLandmar());
		profilebusinessdo.setName(baseWebDO.getName());
		profilebusinessdo.setNewpassword(baseWebDO.getNewpassword());
		profilebusinessdo.setOldpassword(baseWebDO.getOldpassword());
		profilebusinessdo.setPincode(baseWebDO.getPincode());
		profilebusinessdo.setState(baseWebDO.getState());
		profilebusinessdo.setStatelist(baseWebDO.getStatelist());
		profilebusinessdo.setAddress(baseWebDO.getAddress());
		profilebusinessdo.setUserid(baseWebDO.getUserid());
		profilebusinessdo.setIspasswordupdate(baseWebDO.isIspasswordupdate());
		profilebusinessdo.setIsoldpswdcorrect(baseWebDO.isIsoldpswdcorrect());
		profilebusinessdo.setUserProfileId(baseWebDO.getUserProfileId());
		profilebusinessdo.setMorf(baseWebDO.getMorf());
		profilebusinessdo.setMobileNo(baseWebDO.getMobileNo());
		profilebusinessdo.setIsDefaultProfile(baseWebDO.isIsDefaultProfile());
		profilebusinessdo.setDob(baseWebDO.getDob());
		if(baseWebDO.getDobString()!= null){
			try{
			Date date=dateformat.parse(baseWebDO.getDobString());
			profilebusinessdo.setDob(date);
			}catch (Exception e) {
				
			}
		}
		return profilebusinessdo;
	}

	@Override
	public void mergeWebDos(ProfileWebDO oldBaseWebDO, ProfileWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProfileWebDO createWebDO(ProfileWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<ProfileWebDO> convertBusinessDOtoWebDOList(List<ProfileBusinessDO> baseBusinessDOList, WebContext webContext) {
		List<ProfileWebDO> profileWebDOList = new ArrayList<ProfileWebDO>();
		
		for ( ProfileBusinessDO profileBusinessDO : baseBusinessDOList) {
			ProfileWebDO profileWebDO = convertBusinessDOtoWebDO(profileBusinessDO, webContext);
			profileWebDOList.add(profileWebDO);
		}
		return profileWebDOList;
		
	}

}
