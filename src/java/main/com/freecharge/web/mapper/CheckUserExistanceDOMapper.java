package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.CheckUserBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.CheckUserExistanceWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 12, 2012
 * Time: 12:32:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckUserExistanceDOMapper implements WebDOMapper{
    public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
        CheckUserExistanceWebDO checkUserExistanceWebDO=new CheckUserExistanceWebDO();
        CheckUserBusinessDO checkUserBusinessDO=(CheckUserBusinessDO)baseBusinessDO;
        checkUserExistanceWebDO.setStatusMsg(checkUserBusinessDO.getStatusMsg());
        checkUserExistanceWebDO.setEmail(checkUserBusinessDO.getEmail());
        return checkUserExistanceWebDO;
    }

    public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
          String email=webContext.getRequest().getParameter("email") ;
        CheckUserBusinessDO checkUserBusinessDO=new CheckUserBusinessDO();
       checkUserBusinessDO. setEmail(email);
        return checkUserBusinessDO;
    }

    public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
