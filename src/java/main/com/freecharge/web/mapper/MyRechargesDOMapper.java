package com.freecharge.web.mapper;

import com.freecharge.common.businessdo.MyRechargesBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.web.webdo.MyRechargesWebDO;

public class MyRechargesDOMapper implements WebDOMapper {

	@Override
	public BaseWebDO convertBusinessDOtoWebDO(BaseBusinessDO baseBusinessDO, WebContext webContext) {
		MyRechargesBusinessDO myRechargesBusinessDO=((MyRechargesBusinessDO)baseBusinessDO);
		MyRechargesWebDO myRechargesWebDO=new MyRechargesWebDO();
		myRechargesWebDO.setRechargeHistoryList(myRechargesBusinessDO.getRechargeHistoryList());
		webContext.setBaseWebDO(myRechargesWebDO);
		return myRechargesWebDO;
	}

	@Override
	public BaseBusinessDO convertWebDOToBusinessDO(BaseWebDO baseWebDO, WebContext webContext) {
		MyRechargesWebDO rechargewebdo=((MyRechargesWebDO)baseWebDO);
	MyRechargesBusinessDO myRechargesBusinessDO=new MyRechargesBusinessDO();
	String email = (String)webContext.getRequest().getAttribute(("email"));
    String pageNo = webContext.getRequest().getParameter("pageNo");
    myRechargesBusinessDO.setEmail(email);
    myRechargesBusinessDO.setUserid(rechargewebdo.getUserid());
		return myRechargesBusinessDO;
	}


	@Override
	public BaseWebDO createWebDO(BaseWebDO baseWebDO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void mergeWebDos(BaseWebDO oldBaseWebDO, BaseWebDO newBaseWebDO) {
		// TODO Auto-generated method stub
		
	}

}
