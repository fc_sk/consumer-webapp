package com.freecharge.web.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Contains all the methods needed to carry out CSRF related activities.
 */
@Component
public class SysinfoManager {
    
    private static Logger logger = LoggingFactory.getLogger(SysinfoManager.class.getName());
    
    public static String getHostInitial() {
        String hostname = "";
        
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            logger.error("UnknownHostException while fetching the hostname.");
            return "unkown-host";
        }
        
        return hostname;
    }
   
}
