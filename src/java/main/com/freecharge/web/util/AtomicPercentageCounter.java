package com.freecharge.web.util;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicPercentageCounter {

	private final AtomicInteger counter = new AtomicInteger(0);

	/**
	 * Returns next percentage value from 1 to 100.
     * NOTE: This function is not idempotent - each call increments percentage.
     * @return
     */
    public int incrementAndGet() {
    	boolean updated = false;
    	int newval = 0;
    	while (!updated) {
        	final int curr = counter.get();
        	newval = (curr >= 100)? /* rollover */  1: /* increment */ curr + 1;
        	updated = counter.compareAndSet(curr, newval);
    	}
    	return newval;
    }

}
