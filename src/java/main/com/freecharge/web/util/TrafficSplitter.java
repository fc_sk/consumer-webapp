package com.freecharge.web.util;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RollingIntSequence;

public class TrafficSplitter {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    private volatile String weightString = null;
    private volatile RollingIntSequence rollingIntSequence = null;

    final Callable<String> configReader;
    final int defaultIndex;

    public TrafficSplitter(final Callable<String> configReader, final int defaultNext) {
        this.configReader = configReader;
        this.defaultIndex = defaultNext;
    }

    public void update(String configWeightString) {
        this.weightString = configWeightString;
        if (configWeightString == null) {
            this.rollingIntSequence = null;
            try {
                throw new Exception("Traffic-splitter disabled due to NULL weight-string");
            } catch (Exception e) {
                logger.warn(e.getMessage(), e);
            }
        } else {
            final String[] weightSplitString = configWeightString.split(",");
            try {
                this.rollingIntSequence =
                        RollingIntSequence.createInterleaved(Integer.parseInt(weightSplitString[0]),
                                Integer.parseInt(weightSplitString[1]), Integer.parseInt(weightSplitString[2]));
            } catch (NumberFormatException e) {
                this.rollingIntSequence = null;
                logger.error("Error finding split weights from: " + this.weightString, e);
            } catch (IllegalArgumentException e) {
                this.rollingIntSequence = null;
                logger.error(String.format(
                        "Traffic-splitter disabled. Found insufficient weights to split traffic into: %s",
                        this.weightString), e);
            }
        }
    }

    public int next() {
        try {
            String configWeightString = null;
            try {
                configWeightString = configReader.call();
            } catch (Exception e) {
                logger.error("Error fetching configuration 'weight string'", e);
                return defaultIndex;
            }
            if (!FCUtil.equal(configWeightString, this.weightString)) {
                update(configWeightString);
            }
            if (this.rollingIntSequence != null) {
                return rollingIntSequence.next();
            }
        } catch (RuntimeException e) {
            logger.error("Error finding next weight index", e);
        }
        return defaultIndex;
    }

}
