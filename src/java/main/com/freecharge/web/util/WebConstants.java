package com.freecharge.web.util;

public class WebConstants {
    
    // Merchant constants.
    public static final class Merchant {
        public static final String IS_LOGGED_IN = "merchant.loggedIn";
        public static final String EMAIL_ID = "merchant.emailId";
        public static final String USER_ID = "merchant.userId";
        public static final String MERCHANT_REFERENCE = "merchant.reference";
    }

    //Constants for delegate bean mappings

    public static final String DELEGATE_BEAN_TEST = "testingDelegateInt";//methoda name

    public static final String DELEGATE_BEAN_LOGIN = "loginDelegate";
    
    public static final String DELEGATE_BEAN_USER_LOGIN ="userLoginDelegate";
    
    public static final String DELEGATE_BEAN_UPD_ADD_USER_LOGIN = "updateAddressUserLoginDelegate";

    public static final String DELEGATE_BEAN_HOME = "home";

    public static final String DELEGATE_BEAN_EDIT = "edit";

    public static final String DELEGATE_BEAN_REGISTER = "registrationDelegate";
    
    public static final String DELEGATE_BEAN_REGISTER_NOSESSION = "registrationWithoutSessionDelegate";
    
    public static final String DELEGATE_BEAN_FORGOTPASSWORD = "forgotPasswordDelegateFC";

    public static final String DELEGATE_BEAN_MYPROFILE = "myprofileDelegateFC";
    
    public static final String DELEGATE_BEAN_USER_MY_PROFILE ="userMyProfileDelegateFC";
    
    public static final String DELEGATE_BEAN_MY_RCHG_CONTACT_PROFILE ="myRechargeContactProfileDelegateFC";

    public static final String DELEGATE_BEAN_UPDATEPROFILE = "updateProfileDelegateFC";
    
    public static final String DELEGATE_BEAN_UPDATE_DEFAULT_PROFILE = "updateDefaultProfileDelegateFC";
    
    public static final String DELEGATE_BEAN_UPDATE_RCHG_CONTACT_PROFILE = "updateRchgContactProfileDelegateFC";

    public static final String DELEGATE_BEAN_UPDATEADDRESS ="updateAddressDelegateFC";
    
    public static final String DELEGATE_BEAN_USERCONTACTS="mycontactsDelegateFC";
    
    public static final String DELEGATE_BEAN_CHECKOLDPASSWORD ="passwordCheckDelegateFC";
    
    public static final String DELEGATE_BEAN_OLDPASSWORD_CHECK = "oldPasswordCheckDelegateFC";

    public static final String DELEGATE_BEAN_UPDATECONTACT="updatecontactDelegateFC";
    
    public static final String DELEGATE_BEAN_SELECT_COUPONS = "selectCoupon";

    public static final String DELEGATE_BEAN_CHANGE_PASSWORD = "changePasswordDelegateFC";

    public static final String DELEGATE_BEAN_PASSWORD_RECOVERY = "passwordRecoveryDelegateFC";

    public static final String DELEGATE_BEAN_RECOVERED_PASSWORD_CHANGE = "recoveredPasswordChangeDelegateFC";

    public static final String DELEGATE_BEAN_DOESUSEREXIST = "doesUserExistDelegateFC";

    public static final String DELEGATE_BEAN_CART = "cartDelegate";

    public static final String DELEGATE_BEAN_TXN_CROSS_SELL = "txnCrossSellDelegate";

    public static final String DELEGATE_BEAN_TXN_FULFILMENT = "txnFulFilmentDelegate";

    public static final String PAYMENT_RESPONCE_CODE = "sucess";

    public static final String DELEGATE_BEAN_RECHARGE_INITIATE = "rechargeInitiaterDelegate";

    public static final String DELEGATE_BEAN_PAYMENT_TIME_LOGIN = "paymentTimrLoginDelegateFC";

    public static final String DELEGATE_BEAN_LOGIN_AS_GUEST = "loginAsGuestDeligateFC";

    public static final String DELEGATE_BEAN_TXNHOMEPAGE = "txnHomePageDelegateFC";

    public static final String DELEGETE_BEAN_SAVE_TXNHOMEPAGE = "saveTxnHomePageDelegateFC";

    public static final String DELEGATE_STATE_MASTER = "stateMaster";

    public static final String DELEGATE_OPERATOR_CIRCLE = "operatorCircle";

    public static final String DELEGATE_BEAN_SAVEFIRSTNAME = "saveFirstNameDelegateFC";

    public static final String DELEGATE_BEAN_SAVEADDRESS ="saveAddressDelegateFC";

    public static String DELEGATE_BEAN_PAYMENTOPTION_LIST_DETAIL = "paymentOptionListDetailsFC";
   
    public static String DELEGATE_BEAN_CITYMASTER = "cityMasterFC";

    public static final String DELEGATE_PRODUCT_PAYMENT = "productPayment";

    public static final String DELEGATE_MY_RECHARGES = "MYRECHARGES";

    public static final String DELEGATE_MY_RECHARGE_CONTACTS = "myRechargeContactsDelegateFC";
    
    public static final String DELEGATE_SEO_ATTRIBUTE = "seoAttributesDelegateFC";
    
	public static final String DELEGATE_REISSUE_INVENTORY = "reissueInventoryDelegate";
	
	public static final String DELEGATE_SUCCESSFUL_RECHARGE = "successfulRechargeDelegate";
    /*    Session map keys*/

    public static final String SESSION_USER_EMAIL_ID ="userName";
    public static final String SESSION_USER_IS_LOGIN ="isLogin";
    public static final String NEED_LOGIN = "needLogin";
    public static final String SESSION_USER_FIRSTNAME = "firstName";
    public static final String SESSION_USER_USERID="loggedinuserid";
    public static final String SESSION_FINGER_PRINT = "fingerPrint";
    public static final String SESSION_COMMON_POJO = "_CommonSessionPojo";
	public static final String SESSION_RECHARGE_TYPE = "_CommonSessionPojo_type";
    public static final String SESSION_OPERATOR_NAME = "_CommonSessionPojo_operatorName";
    public static final String SESSION_RECHARGE_AMOUNT = "_CommonSessionPojo_rechargeAmount";
    public static final String SESSION_LOGIN_SOURCE = "loginSource";
    public static final String CLIENT_REAL_IP = "clientRealIp";
    
    public static final String POST_LOGIN_PAGE = "postLoginPage";

    public static final String SESSION_CHANNELID="channelId";
    public static final String MOBILESITE_CHANNELID="2";
    public static final String MOBILEAPP_CHANNELID="3";
   
    public static final String DELEGATE_VIEW_INVOICE = "VIEWINVOICE";
    public static final String DELEGATE_BEAN_PERSONALDETAILS = "personalDetailsDelegateFC";
    public static final String DELEGATE_BEAN_CHECKOUT = "checkoutDelegateFC";
	public static final String DELEGATE_BEAN_FEEDBACK = "feedbackDelegateFC";	
	public static String EMAIL_ID_EXISTS = "emaildIdExists";
	public static final String DELEGATE_VOUCHER_DATA = "VOUCHERDELEGATE";
	public static final String DELEGATE_BEAN_RECHARGE_AGAIN="RECHARGEAGAIN";
	public static final String DELEGATE_BEAN_EDIT_DETAILS="EDITDETAILS";

	public static final String DELEGATE_BEAN_COMMON = "commonDelegateFC";
	

	//Constants for Recharge Mis panel
	
	public static final String MIS_USER_SESSION_PARAM = "misLogin";
	
	//Constants for Payment Gateway Mis panel
	
	public static final String PG_MIS_USER_SESSION_PARAM = "pgMisLogin";
	

	public static final String DELEGATE_ACTIVE_COUPONS = "activeCouponIdsDelegate";
	public static final String DELEGATE_UPDATE_INVENTORY = "updateInventoryDelegate";
	public static final String DELEGATE_RESET_INVENTORY = "resetInventoryDelegate";

    public static final String LOOK_UP_ID_HTTP_PARAM = "lookUpID";
    
    public static final String ATTR_BINOFFER_BUSSINESS_DO_MAP = "ATTR_BINOFFER_BUSSINESS_DO_MAP";
    public static final String ATTR_BINOFFER_BUSSINESS_DO_LIST = "ATTR_BINOFFER_BUSSINESS_DO_LIST";
    public static final String ATTR_BINOFFER_BUSSINESS_DO = "ATTR_BINOFFER_BUSSINESS_DO";
    
    public static final String ATTR_CROSSSELL_BUSSINESS_DO_LIST = "ATTR_CROSSSELL_BUSSINESS_DO_LIST";
    public static final String ATTR_CROSSSELL_BUSSINESS_DO = "ATTR_CROSSSELL_BUSSINESS_DO";

	public static final String ATTR_CITY_LIST = "ATTR_CITY_LIST";

	public static final String ALL_DENOMINATION_TYPES = "ALL_DENOMINATION_TYPES";

	public static final String ALL_PRODUCT_TYPES = "ALL_PRODUCT_TYPES";

	public static final String ALL_OPERATORS = "ALL_OPERATORS";
	
	public static final String ALL_OPERATOR_RETRY_MAP = "ALL_OPERATOR_RETRY_MAP";
	public static final String OPERATOR_RETRY = "OPERATOR_RETRY";

	public static final String ALL_PRODUCTS = "ALL_PRODUCTS";

	public static final String ALL_CIRCLES = "ALL_CIRCLES";
	
	public static final String ALL_NAMES = "ALL_NAMES";

	public static final String ALL_RECHARGE_PLANS = "ALL_RECHARGE_PLANS";
	
	public static final String ALL_RECHARGE_PLANS_IN_MODERATION = "ALL_RECHARGE_PLANS_IN_MODERATION";
	
	public static final String RECHARGE_PLAN = "RECHARGE_PLAN";

	public static final String RECHARGE_PLANS_AVAILABLE = "RECHARGE_PLANS_AVAILABLE";

	public static final String ERROR_MESSAGE = "ERROR_MESSAGE";

	public static final String STATUS = "STATUS";

	public static final String ATTR_STORED_CITY_LIST = "ATTR_STORED_CITY_LIST";
	public static final String ATTR_CITY_MAP = "ATTR_CITY_MAP";
	public static final String ATTR_STATE_MAP = "ATTR_STATE_MAP";
	public static final String BIN_OFFERS_LIST = "BIN_OFFERS_LIST";
	public static final String PRODUCT_TYPE ="type";
	
	public static final String OPERATOR ="operator";
	public static final int RECHARGE_HISTORY_LIMIT =30;
	public static final int BILL_PAYMENT_HISTORY_LIMIT =30;
	public static final int WALLET_HISTORY_LIMIT =30;

    public static final String ATTR_ARDEAL_ENTITIES = "ATTR_ARDEAL_ENTITIES"; 
    public static final String ATTR_ARACTION_ENTITIES = "ATTR_ARACTION_ENTITIES";

	public static final String RECHARGE_PLANS = "ALL_RECHARGE_PLANS";
	public static final String SHOW_RECHARGE_PLANS = "showRechargePlans";

	public static final String FORCE_CAPTCHA = "forceCaptcha";
	
	public static final String ALL_MERCHANTS = "ALL_MERCHANTS";
	public static final String ALL_CAMPAIGNS = "ALL_CAMPAIGNS";

    public static final String PRODUCT_TYPE_HOME_PAGE = "default";
    
    public static final String LAST_ORDER_AMOUNT = "lastOrderAmount";

	public static final String USER_ID = "userId";

    public static final String CVVLESS_TXN_TYPE = "cvvless";

}