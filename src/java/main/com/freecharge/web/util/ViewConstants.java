package com.freecharge.web.util;


/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 11:15 AM
 * This class is a constant holder for all view names.
 */
public class ViewConstants {
    public static final String FREECHARGE_HOME = "home/view";
    public static final String FREECHARGE_SINGLEPAGE_HOME = "home/spview";
    public static final String FREECHARGE_SINGLEPAGE_HOME_NEW = "home/spview-new";
    
    public static final String FREECHARGE_SINGLEPAGE_WALLET = "home/spwallet";
    public static final String FREECHARGE_WALLET_SUCCESS = "home/walletsuccess";
    public static final String FREECHARGE_WALLET_FAILURE = "home/walletfailure";
    
    public static final String FREECHARGE_WHY_UPGRADE = "home/whyupgrade";
    
    public static final String AGGREGATOR_PRIORITY = "admin/aggregatorPriority";
    public static final String POSTPAID_AGGREGATOR_PRIORITY = "admin/postpaidAggregatorPriority";
    public static final String APP_CONFIG = "admin/appconfig";
    public static final String APP_CONFIG_USERIDS = "admin/appconfig/userids";
    public static final String REFUND_LOGIN_ADMIN_VIEW = "admin/refund/login";
    public static final String REFUND_LIST_ADMIN_VIEW = "admin/refund/list";
    public static final String CUSTOMER_TRAIL_HOME_VIEW = "admin/customertrail/home";
    public static final String CUSTOMER_TRAIL_LISTING = "admin/customertrail/listing";
    public static final String SD_CUSTOMER_TRAIL_LISTING = "admin/customertrail/SDlisting";
    public static final String P2P_P2M_CUSTOMER_TRAIL_LISTING = "admin/customertrail/P2PAndP2MHistory";
    public static final String P2P_P2M_ORDER_ID_VIEW = "admin/customertrail/P2PAndP2MOrderIdView";
    public static final String P2P_SPLIT_CUSTOMER_TRAIL_LISTING = "admin/customertrail/P2PSplitHistory";
    public static final String SPLIT_BY_ORDER_ID_VIEW = "admin/customertrail/P2PSplitOderIdView";
    
    public static final String USER_HISTORY_BY_MERCHANT_VIEW = "admin/customertrail/userHistoryByMerchant";
    public static final String USER_MERCHANT_DETAILS_INPUT_PAGE = "admin/customertrail/userMerchantInputDetails";
    
    public static final String CUSTOMER_TRAIL_BY_PROFILENO = "admin/customertrail/search/profileNo";
    public static final String MANUAL_REFUND_HOME = "admin/refund/manual/home";
    public static final String MANUAL_REFUND_RESULT = "admin/refund/manual/result";
    public static final String PREFIX_LIST = "admin/prefixList";


    public static final String MANUAL_REFUND_NEW = "admin/refund/manual/new";
    public static final String MANUAL_REFUND_NEW_RESULT = "admin/refund/manual/new-result";

    public static final String PAY_STATUS_CHECK_NEW = "admin/refund/paystatuscheck/new";
    public static final String PAY_STATUS_CHECK_NEW_RESULT = "admin/refund/paystatuscheck/new-result";

    public static final String ADMIN_PREDICTION_SUCCESS_LINK = "admin/growthclient/prediction/success";
    public static final String ADMIN_PREDICTION_FAILURE_LINK = "admin/growthclient/prediction/failure";
    public static final String ADMIN_BOLT_HISTORY_SUCCESS_LINK = "admin/growthclient/bolthistory/success";
    public static final String ADMIN_BOLT_HISTORY_FAILURE_LINK = "admin/growthclient/bolthistory/failure";

    public static final String BILL_DESK_UPDATOR_HOME = "admin/refund/billDeskUpdator/home";
    public static final String BILL_DESK_UPDATOR_RESULT = "admin/refund/billDeskUpdator/result";
    public static final String REFUND_HOME = "admin/refund/home";
    public static final String REFUND_HISTORY_LIST = "admin/refund/history/list";
    public static final String FAILED_OXIGEN_TRANSACTIONS_LIST = "admin/refund/failedOxigenTransactions/list";
    public static final String CROSSSELL_ADMIN_HOME_VIEW = "admin/crosssell/home";
    public static final String BINOFFER_ADMIN_HOME_VIEW = "admin/binoffer/home";
    public static final String BINOFFER_HOME_VIEW_LIST = "app/binoffer/home/list";
    public static final String BINOFFER_HOME_VIEW_TNC = "app/binoffer/tnc";
    
    public static final String RECHARGE_PLANS_HOME_VIEW = "admin/rechargeplans/home";
    public static final String RECHARGE_PLANS_MODERATION_VIEW = "admin/rechargeplans/moderation";
    public static final String RECHARGE_PLANS_LIST_VIEW = "admin/rechargeplans/list";
    public static final String ADMIN_PAGE_HOME_VIEW = "admin/adminpage/adminlanding";
    public static final String CUSTOMER_WHITELIST_USER_VIEW = "admin/whitelist/home";
    public static final String CUSTOMER_WHITELIST_USER_RESULT = "admin/whitelist/result";
	public static final String CUSTOMER_BAN_USER_VIEW = "admin/banuser/home";
	public static final String CUSTOMER_BAN_USER_RESULT = "admin/banuser/result";
	public static final String RECHARGE_RESPONSE_HANDLE_VIEW = "admin/response/handler";
	public static final String RECHARGE_RESPONSE_UPDATE_SUCCESS = "admin/response/update/success";
	public static final String RECHARGE_RESPONSE_UPDATE_FAIL = "admin/response/update/fail";
	public static final String EMAIL_INPUT_TRANSACTION_VIEW = "email/input/transaction/home";
	public static final String CUSTOMER_ORDERID_RESULT = "customer/orderid/result";
	public static final String MOBILE_INPUT_TRANSACTION_VIEW = "customer/inputmobile/transaction/home";
	public static final String FREEFUND_ADMIN_HOME_VIEW = "admin/freefund/home";
    public static final String WALLET_HOME = "home/mybalance";
	public static final String PROMOCODE_ADMIN_HOME_VIEW = "admin/promocode/home";
	public static final String ATTR_ALL_PROMOCODES = "ALL_PROMOCODES";
	public static final String ATTR_PROMOCODE = "PROMOCODE";
	public static final String MOBILE_INPUT_RESULT = "mobile/input/result";
	public static final String EMAIL_INPUT_RESULT = "email/input/result";	
	public static final String WALLET_DEBIT_STATUS_VIEW  = "home/walletdebitstatus";
	public static final String MIS_VIEW  = "recharge/mis";
	public static final String ADMIN_FULFILMENT_VIEW  = "admin/fulfilment/home";
	public static final String ADMIN_COUPON_REDEMPTION_VIEW  = "admin/couponredemption/home";
	public static final String WALLET_HISTORY_VIEW       = "home/wallethistory";
	public static final String WALLET_LOGIN_VIEW         = "wallet/debit";
	public static final String WALLET_DEBIT_VIEW         = "home/walletdebit";
	public static final String IMPERSONATE               = "admin/impersonate";
	public static final String REDIRECT_URL               =  "/app/handlerecharge.htm";
	public static final String ERROR_URL               =  "/app/payment.htm";
    public static final String CITIBANK_OFFER = "offers/citibank";
    public static final String NEW_PAYMENT_WINDOW = "productPayment/newWindow";

	public static final String WALLET_EMAIL_INPUT_RESULT = "admin/customertrail/wallettransaction/result";

	public static final String CUSTOMER_TRAIL_PROMOCODE_RESULT = "admin/promocode/result";
	public static final String REFERRER_EMAIL_RESULT = "admin/referralemail/result";
	public static final String LIFELINE_EMAIL_RESULT = "admin/lifelineemail/result";
	
    public static final String ARDEAL_ADMIN_HOME_VIEW = "admin/ardeal/home";
    public static final String ARDEAL_ADMIN_REWARDS_VIEW = "admin/ardeal/rewards";
    public static final String ARDEAL_ACTION_TYPES_VIEW = "admin/ardeal/action/actionTypes";
    public static final String ARDEAL_CONFIGURE_VOUCHER_OPT_IN_ACTION_VIEW = "admin/ardeal/action/VoucherOptIn/configure";
    public static final String ARDEAL_ADMIN_EDIT_ARDEAL_VIEW = "admin/ardeal/edit";
    public static final String ARDEAL_ADMIN_EDIT_ACTION_VIEW = "admin/ardeal/action/edit";
    public static final String ARDEAL_ADMIN_EDIT_REWARD_VIEW = "admin/ardeal/reward/edit";
    public static final String ARDEAL_ADMIN_EDIT_CONDITION_VIEW = "admin/ardeal/condition/edit";

    public static final String ADMIN_WALLET_REVOKE_VIEW = "admin/wallet/revokebalance";
    public static final String CASH_BACK_REFUND = "admin/refund/manual/cashback";
    public static final String CASH_BACK = "admin/refund/manual/diffcashbacks";

	public static final String FREEFUND_CLASS_EDIT = "admin/freefund/viewAndedit";
    public static final String MERCHANT_LOGIN = "merchant/loginform";
    public static final String COUPON_REDEMPTION_FORM = "merchant/redemptionform";
    public static final String COUPON_REDEMPTION_RESULT = "merchant/redemptionresult";
    public static final String COUPON_REDEMPTION_CHECK_RESULT = "merchant/redemptioncheckresult";
    public static final String OPERATOR_ALERT = "admin/operatoralert/create";
    public static final String OPERATOR_ALERT_LIST = "admin/operatoralert/list";
    public static final String PG_ADMIN_ALERT = "admin/pgalert/create";
    public static final String PG_ALERT_LIST = "admin/pgalert/list";
    public static final String EDIT_RECHARGE_PLANS_MODERATION = "admin/rechargeplans/edit";
    
    public static final String ARDEAL_CASHBACK_HOME_VIEW = "admin/ardeal/cashback/home";
	public static final String ADMIN_COUPON_REISSUE_VIEW  = "admin/couponreissue/home";
    
    public static final String MAILER_CAMPAIGN = "/admin/mailer/campaign/home";
    public static final String EDIT_MAILER_CAMPAIGN = "/admin/mailer/campaign/edit";

    public static final String TRACKER_JS = "trackerJs";
    public static final String AG_PREFERENCE = "agpreference";
    public static final String FREEFUND_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW = "/admin/freefund/generateandupload/home";
    public static final String FREEFUND_LARGE_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW = "/admin/freefund/largegenerateandupload/home";
    public static final String GENERATE_CODES_ADMIN_HOME_VIEW = "/admin/generateCodes/home";

    public static final String UPDATE_EMAIL_ID = "admin/updateemail";
    public static final String UPDATE_EAIL_ID_RESULT = "admin/updateemails/result";
    public static final String REVERSAL_HANDLER_VIEW = "admin/reversal/handler/view";
    public static final String UTM_TRACKER_HOME_VIEW = "admin/utmtracker/home";
    public static final String UTM_TRACKER_CREATE_KEYWORD_VIEW = "/admin/utmtracker/home/createShortcode";
    public static final String FRAUD_HANDLER_ADMIN_VIEW = "admin/fraud/handler/tool/view";
    public static final String FRAUD_HANDLER_ADMIN_RESULT = "admin/fraud/handler/tool/processresult";
    public static final String OPERATOR_RETRY_CONFIG_VIEW = "admin/operatorretry/home";
    public static final String GROWTH_EVENT_ADMIN_HOME_VIEW = "admin/growthevent/home";
    public static final String GROWTH_EVENT_ADMIN_EDIT_VIEW = "admin/growthevent/edit";

    public static final String EVENT_REWARDS_ADMIN_HOME_VIEW = "admin/eventrewards/home";
    public static final String FREEFUND_ADDITIONAL_REWARDS_HOME_VIEW = "/admin/freefund/additionalreward/home";
    public static final String FREEFUND_ADDITIONAL_REWARDS_CREATE_VIEW = "/admin/freefund/additionalreward/home/create";
    public static final String GENERIC_PROMOCODE_USAGE_DETAILS = "/admin/customertrail/genericCodeUsageDetails";
    public static final String BLACKLIST_DOMAIN_ADMIN_HOME_VIEW = "admin/blacklistdomain/home";
    public static final String REWARD_COUPON_SEARCH_VIEW = "/admin/rewardcoupon/search/view"; 
    
    public static final String METRO_REPORT_HOME = "/admin/growthevent/admin/metroreporthome";
    public static final String METRO_REPORT = "/admin/growthevent/admin/metroreportdetails";

    public static final String PROMOCODE_UTILITY_HOME_VIEW = "/admin/promocode/utility/home";
    public static final String SHOPO_ORDER_ID_VIEW = "admin/customertrail/ShopoOrderIdView";
	public static final String FPS_USER_TRANSACTION_DETAILS = "/admin/customertrail/batcave/getusertransactiondetails";
	public static final String PAID_COUPNS_LISTING = "admin/customertrail/paidCouponsView";
	
	public static final String CUSTOMER_TRAIL_UPITXN_DETAILS = "admin/customertrail/upiTxnDetails";
	public static final String CUSTOMER_TRAIL_UPITXN_LIST = "admin/customertrail/upiTxnList";

}
