package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Base class for free charge interceptors. It exposes a few utility methods.
 * @author arun
 *
 */
public class FCBaseInterceptor extends HandlerInterceptorAdapter {
    protected String getQueryString(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String queryString = request.getQueryString();
        
        String requestString = requestURI;
        
        if (queryString != null && !queryString.isEmpty()) {
            requestString = requestString + "?" + queryString;
        }

        return requestString;
    }
}
