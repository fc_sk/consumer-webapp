package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import com.freecharge.admin.controller.AppConfigController;
import com.freecharge.common.comm.email.EmailAppService;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.IEmailappTemplateNameResolver;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.emailapp.client.TemplateName;

import flexjson.transformer.HtmlEncoderTransformer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.HtmlUtils;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;


/**
 * This interceptor sets app config attributes in request.
 */
public class AppConfigInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private EmailAppService  emailAppService;

    @Autowired
    protected FCProperties   fcProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("In AppConfigInterceptor.");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
        if (fcProperties.isProdMode()) {
        	if(modelAndView.getModel() != null && modelAndView.getModel().get(AppConfigController.OLD_VALUE_KEY) != null && 
        			modelAndView.getModel().get(AppConfigController.NEW_VALUE_KEY) != null) {
        			String oldStatus = modelAndView.getModel().get(AppConfigController.OLD_VALUE_KEY).toString();
        			String newStatus = modelAndView.getModel().get(AppConfigController.NEW_VALUE_KEY).toString();
        			String path = request.getRequestURI();
        			/*Getting current admin user*/
        			String curUserEmail = null;
        			FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
        			Map<String, Object> map = freechargeSession.getSessionData();
        			if (map != null) {
        				curUserEmail = (String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL);
        			}
        			Map<String, String> variableValues = new HashMap<>();
        			variableValues.put("path", path);
        			variableValues.put("emailId", curUserEmail);
        			variableValues.put("oldStatus", HtmlUtils.htmlEscape(oldStatus));
        			variableValues.put("newStatus", HtmlUtils.htmlEscape(newStatus));
        			EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        			emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.APPCONFIG_PROPERTY_TOGGLED_TO));
        			emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.APPCONFIG_PROPERTY_TOGGLED_FROM));
        			emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.APCONFIG_PROPERTY_TOGGLED_SUBJECT));
        			emailBusinessDO.setVariableValues(variableValues);
        			variableValues.put("subject", emailBusinessDO.getSubject() + "\t" + '[' + path + ']' + "\tChanged");
        			if (newStatus != null || (oldStatus != null && newStatus == null)) {
        				emailAppService.submitEmailRequest(emailBusinessDO, new IEmailappTemplateNameResolver() {
        					@Override
        					public String resolveEmailappTemplateName() {
        						return TemplateName.APPCONFIG_CHANGE;
        					}
        				});
        			}
        	}
        }
    }

    private static final Logger logger = LoggingFactory.getLogger(AppConfigInterceptor.class);
}
