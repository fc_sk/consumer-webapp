package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.service.ISessionService;
import com.freecharge.platform.metrics.MetricsClient;

/**
 * Looks at the UserAgent and if it's whitelisted then sets a session 
 * variable to true. For now this is used to enable phased release of
 * SmoothWhiskey.
 * @author arun
 *
 */
public class UserAgentInterceptor extends FCBaseInterceptor {
    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private ISessionService sessionService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }
    
    

}
