package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;

/**
 * Makes sure that the contents served out have the same version no as the one configured in our AppConfigReader.getWebAssetsVersion(). This is done
 * because we cache our contents using cloudfront. So, if someone requests a file from cloudfront with a random cache version, cloud front hits
 * our server for that file, our server does not do a validation of the cache version and serves out the file that it has. Cluoudfront now caches this
 * file with that version no. In future, if we really have a file with the same version as the rogue one, cloudfront serves it from it's
 * cache which is dangerous.
 */
public class ContentInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppConfigService appConfigService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //We do not version images
        if (isImageRequest(request)) {
            logger.debug("Image request - " + request.getRequestURI());
            return true;
        }

        String version = request.getParameter("v");

        if (doVersionsMatch(version, request)) {
            return true;
        }

        logger.info(String.format("Request %s does not have the current version %s. Referrer is %s",
                request.getRequestURI(), appConfigService.getWebAssetsVersion(), request.getHeader("referer")));

        //todo - Remove once we fix the hiccup
        //response.sendError(HttpServletResponse.SC_NOT_FOUND);
        return true;
    }

    private boolean doVersionsMatch(String version, HttpServletRequest request) {
        if (version == null || version.isEmpty()) {
            logger.error("Found empty or null version for css or js request with URI:[" + request.getRequestURI() + "]");
        }
        
        String configuredVersion = "" + appConfigService.getWebAssetsVersion();
        return configuredVersion.equals(version);
    }

    private boolean isImageRequest(HttpServletRequest request) {
        //Some of our image paths have a double slash. Should be fixed.
        return request.getRequestURI().startsWith("/content/images") || request.getRequestURI().startsWith("//content/images");
    }

}
