package com.freecharge.web.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.ISessionService;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.web.util.WebConstants;

/**
 * Intercept to determine if user is logged and redirect him to
 * appropriate page if not.
 * @author arun
 *
 */
public class LoginInterceptor extends AbstractLoginInterceptor {
    private static final Logger logger = LoggingFactory.getLogger(LoginInterceptor.class);

    private final String MOBILE_URI_PREFIX = "/m/";
    
    private final String REST_URI_PREFIX = "/rest/";
    
    private final String PROTECTED_URI_PREFIX = "/protected/";

    @Autowired
    private ISessionService sessionService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private TxnHomePageDAO homePageDao;
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        if (loginCheckRequired(handler, request)) {
            if (!isLoggedIn()) {
                logger.info("User is not logged in while accessing page: " + request.getRequestURI());
                FreechargeContext fcContext = FreechargeContextDirectory.get();
                
                if (fcContext == null) {
                    logger.info("Context data is empty something is wrong redirecting to home page");
                    RequestDispatcher rd = request.getRequestDispatcher("/");
                    rd.forward(request, response);
                }
                
                FreechargeSession fcSession = fcContext.getFreechargeSession();
                
                if (fcSession == null) {
                    logger.info("Session data is empty something is wrong redirecting to home page");
                    RequestDispatcher rd = request.getRequestDispatcher("/");
                    rd.forward(request, response);
                }
                
                Map<String, Object> sessionMap = fcSession.getSessionData();
                
                if (sessionMap == null) {
                    logger.info("Session Map is empty something is wrong redirecting to home page");
                    RequestDispatcher rd = request.getRequestDispatcher("/");
                    rd.forward(request, response);
                }
                
                String freeChargeRootURL = fcProperties.getAppRootUrl();
                String queryString = getQueryString(request);
                
                String postLoginUrl = freeChargeRootURL + queryString;

                /*
                 * Once a user logs out lid becomes meaningless as
                 * it's associated with session and session ID changes
                 * on login. In such cases we just redirect the user to
                 * home page.
                 */
                if (!containsLookupId(request)) {
                    sessionMap.put(WebConstants.NEED_LOGIN, Boolean.TRUE);
                    sessionMap.put(WebConstants.POST_LOGIN_PAGE, postLoginUrl);
                }
                
                sessionService.storeSession(fcSession);
                
                response.sendRedirect("/");
                
                return false;
            } else {
                /* 
                 * even if the user is logged in; check if any LID
                 *  being accessed belongs to the current session.
                 * 
                 */
                if (containsLookupId(request)) {
                    FreechargeContext fcContext = FreechargeContextDirectory.get();
                    String lookUpId = getLookupId(request);
                    
                    boolean isValidLookupId = isLookupIdValid(fcContext, lookUpId);

                    if (!isValidLookupId) {
                        logger.info("lookup Id is invalid for session ID: ["
                                + fcContext.getSessionId()
                                + "] Lookup id in request: [" + lookUpId
                                + "]. Redirecting to home page");
                        
                        response.sendRedirect("/");
                        
                        return false;
                    }
                }
            }
        }
        
        return true;
    }

    private boolean loginCheckRequired(Object handler, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        
        if (requestURI.startsWith(MOBILE_URI_PREFIX)) {
            return false;
        }
        
        if (requestURI.startsWith(REST_URI_PREFIX)) {
            return false;
        }
    
        /*
         * URIs beginning with /protected/ will be accessible only for machines within the VPN.
         * This is enforced thru rules in the nginx load balancer.
         * /protected/* can be used for REST URIs which we want to use internally.
         * This can also be used for some features which we do not want to release yet but test internally. 
         */
        if (requestURI.startsWith(PROTECTED_URI_PREFIX)) {
            return false;
        }

        /*
         * Pass through in case of mobile request.
         * It'll be handled in a mobile specific login
         * interceptor.
         */
        if (MobileUtil.isMobile(request)) {
            return false;
        }
        
        if (!fcProperties.isLoginCheckEnabled()) {
            return false;
        }
        
        /*
         * If admin link, login check is not enabled.
        */
        if (request.getRequestURI().startsWith("/admin")) {
            return false;
        }
        
        /*
         * No login check for merchant links.
         * There's a different interceptor for that.
         */
        if (request.getRequestURI().startsWith("/merchant")) {
            return false;
        }
        
        /*
         * If annotation is present *and* the method is not excluded *then* check is enabled.
         */
        if (!(handler instanceof HandlerMethod)) {
            return false;
        }
        
        HandlerMethod requestHandler = (HandlerMethod) handler;
        
        NoLogin noLogin = requestHandler.getMethodAnnotation(NoLogin.class);
        
        if (noLogin != null) {
            return false;
        }
        
        return true;
    }

   /* private boolean isLookupIdValid(FreechargeContext fcContext, String lookUpId) {
        return fcContext.getFreechargeSession().getLookupIDs().contains(lookUpId);
    }*/
    
    private boolean isLookupIdValid(FreechargeContext fcContext, String lookUpId) {
        String sessionId = fcContext.getSessionId();
        boolean isValidLookupId = false;
        List<TxnHomePage> homePageList = homePageDao.findBySessionId(sessionId);
        if (homePageList != null) {
            for (TxnHomePage txnHomePage : homePageList) {
                if (areLookupIdsValidAndEqual(txnHomePage.getLookupId(), lookUpId)) {
                    isValidLookupId = true;
                    break;
                }
            }
        }
        return isValidLookupId;
    }

    private boolean containsLookupId(HttpServletRequest request) {
        String lid = getLookupId(request);
        
        if (lid != null && !lid.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    private String getLookupId(HttpServletRequest request) {
    	String[] lookupIdParamNames = new String[]{"lid", "lookupID", "lookupId", "lookup_id"};
    	String lookupId = null;
    	for (String lookupIdParamName : lookupIdParamNames){
    		lookupId = request.getParameter(lookupIdParamName);
    		if (lookupId != null){
    			break;
    		}
    	}
        return lookupId;
    }
    
    private boolean areLookupIdsValidAndEqual(String lookupIdFromSession, String lookUpId) {
        if (lookupIdFromSession == null || lookUpId == null) {
            return false;
        }
        
        if (lookupIdFromSession.isEmpty() || lookUpId.isEmpty()) {
            return false;
            
        }
        
        return lookupIdFromSession.equals(lookUpId);
    }

}
