package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.ABTestingConstants;

/**
 * Use this class for setting custom values in session based on the request URI.
 * You can configure the custom AB testing URL in urlrewrite.xml.
 * 
 * @author shirish
 * 
 */
public class ABTestingInterceptor extends HandlerInterceptorAdapter {
	private Logger logger = LoggingFactory
			.getLogger(ABTestingInterceptor.class);

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		handlerMcafeeHeader(request);
		return true;
	}

	/**
	 * Set the mcafee header var in session data based on the homepage url which user hits.
	 * 
	 * If user hits freecharge.in unset do not show him mcafee header.
	 * If user hits freecharge.in/v2 show him mcafee header. /v2 redirects to /app/home.htm?isMcAffeeHeaderEnabled=true (check urlrewrite.xml).
	 */
	private void handlerMcafeeHeader(HttpServletRequest request) {
		if (FreechargeContextDirectory.get() != null) {
			FreechargeSession session = FreechargeContextDirectory.get()
					.getFreechargeSession();
			// check if session is initialized.
			String startUrl = String.valueOf(request.getAttribute("javax.servlet.forward.request_uri"));
			if (session != null) {
				if (session.getSessionData() != null) {
					String showHeader = request
							.getParameter(ABTestingConstants.EXPERIMENT_HEADER_REQUEST_PARAM_NAME);
					if (showHeader != null
							&& showHeader.equalsIgnoreCase("true")) {
						
						//logger.info("Setting Google Experiment Header for url=[" + startUrl+"], sessionId=[" +session.getUuid() + "]");
						session.getSessionData()
								.put(ABTestingConstants.EXPERIMENT_HEADER_REQUEST_PARAM_NAME,
										true);
					}else {
						if (!request.getRequestURI().equals("/app/login.htm")){
							//logger.info("Not Adding Google Experiment Header for url=[" + startUrl+"]");
							session.getSessionData().put(ABTestingConstants.EXPERIMENT_HEADER_REQUEST_PARAM_NAME,
											false);
						}
					}
				} // end sessionData null check. 
			} // end if null session check.
		} // end if freecharge context null check.
	} // end method.

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
}
