package com.freecharge.web.interceptor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation to indicate that
 * a controller does not update the
 * session and hence the session object
 * does not need to be written back to
 * persistence session store.
 * @author arun
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoSessionWrite {

}
