package com.freecharge.web.interceptor.bean;

import org.apache.commons.lang.StringUtils;

/**
 * Bean to capture UserAgent details that are of interest to us. For now 
 * it contains OS and Browser names.
 * @author arun
 *
 */
public class FCUserAgent {
    private final String browser;
    private final String os;
    
    private final String ALL = "ALL";
    
    public FCUserAgent(String os, String browser) {
        this.os = os;
        this.browser = browser;
    }
    
    public String getBrowser() {
        return browser;
    }
    
    public String getOS() {
        return os;
    }
    
    public Boolean isSameUserAgent(String os, String browser) {
        // If enabled for all browsers and OSes
        if (ALL.equals(this.os) && ALL.equals(this.browser)) {
        	return true;
        }
        
        if (StringUtils.isEmpty(this.os) || StringUtils.isEmpty(this.browser)) {
            return false;
        }
        
        // If enabled for a specific browser irrespective of OS
        if (ALL.equals(this.os) && this.browser.equals(browser)) {
        	return true;
        }
        
        // IF enabled for a specific OS irrespective of Browser
        if (ALL.equals(this.browser) && this.os.equals(os)) {
        	return true;
        }
        
        if (StringUtils.isEmpty(os) || StringUtils.isEmpty(browser)) {
            return false;
        }
        
        if (this.os.equals(os) && this.browser.equals(browser)) {
            return true;
        }
        
        return false;
    }
}
