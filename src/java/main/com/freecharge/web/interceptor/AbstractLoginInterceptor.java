package com.freecharge.web.interceptor;

import java.util.Map;

import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.web.util.WebConstants;

public abstract class AbstractLoginInterceptor extends FCBaseInterceptor {
  
    protected boolean isLoggedIn() {
        Boolean login = false;
        
        if (FreechargeContextDirectory.get() != null) {
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            
            if (fs == null) {
                return false;
            }
            
            final Map<String, Object> sessionData = fs.getSessionData();
            
            if (sessionData == null) {
                return false;
            }
            
            if (!sessionData.containsKey(WebConstants.SESSION_USER_IS_LOGIN)) {
                return false;
            }
            
            login = (Boolean) sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        }
        
        if (login == null || !login) {
            return false;
        }
        
        return true;
    }

}
