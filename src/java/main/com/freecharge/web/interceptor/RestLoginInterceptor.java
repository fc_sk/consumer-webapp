package com.freecharge.web.interceptor;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

/**
 * Intercept rest calls which hit URL /rest/** and check if the user is logged in. Throw JSON error if user is not logged in.
 * @author Shirish
 *
 */
public class RestLoginInterceptor extends AbstractLoginInterceptor {
    private static final Logger logger = LoggingFactory.getLogger(RestLoginInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
    	  HandlerMethod requestHandler = (HandlerMethod) handler;
          NoLogin noLogin = requestHandler.getMethodAnnotation(NoLogin.class);
          
          if (noLogin != null) {
              return true;
          }
          
            if (!isLoggedIn()) {
                logger.info("User is not logged in while accessing page: " + request.getRequestURI());
                FreechargeContext fcContext = FreechargeContextDirectory.get();
                
                if (fcContext == null) {
                    logger.info("Context data is empty something is wrong sending unauthorized error code.");
                }
                
                FreechargeSession fcSession = fcContext.getFreechargeSession();
                
                if (fcSession == null) {
                    logger.info("Session data is empty something is wrong sending unauthorized error code.");
                } else{
                	Map<String, Object> sessionMap = fcSession.getSessionData();
                    if (sessionMap == null) {
                        logger.info("Session Map is empty something is wrong  sending unauthorized error code.");
                    }                	
                }
                handleNotLoggedInResponse(response);
                return false;
            } 
        return true;
    }
    
    private void handleNotLoggedInResponse(HttpServletResponse response){
        try {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication required");
		} catch (IOException e) {
			logger.error("Error sending unauthorized error response to client", e);
		}
    }
}
