package com.freecharge.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.framework.exception.IdentityAuthorizedException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.service.CookieHandler;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.social.web.SocialAuthController;

/**
 * Interceptor to validate CSRF tokens.
 */
public class CSRFInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggingFactory.getLogger(CSRFInterceptor.class);

    @Autowired
    private CSRFTokenManager csrfTokenManager;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private CookieHandler cookieHandler;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //To make sure this is a spring controller request
        if (handler instanceof HandlerMethod) {
            if (shouldCheck(request, (HandlerMethod)handler)) {
                String requestToken = csrfTokenManager.getCSRFToken(request);
                //OAuth2 spec says the CSRF token should be passed in a state param
                if (SocialAuthController.FB_CALLBACK_CONTROLLER_METHOD_NAME.equals(((HandlerMethod) handler).getMethod().getName())) {
                    requestToken = request.getParameter("state");
                }
                String sessionToken = CSRFTokenManager.getTokenFromSession();
                logger.info("requestedToken: "+requestToken+" sessionToken: "+sessionToken);
                if (sessionToken.equals(requestToken)) {
                    return true;
                } else {
                    logger.error(String.format("CSRF token mismatch. Session token - %s, Request token - %s. Token mismatch URL is %s", sessionToken, requestToken, request.getRequestURI()));
                    if (!isAppRequest(request)) {
                        cookieHandler.expireApplicationCookie(request, response);
                    } else {
                    	IdentityAuthorizedException identityApiException = new IdentityAuthorizedException("Invalid user session");
                    	throw identityApiException;
                    }
                    if (FCUtil.isAjaxCall(request)){
                    	response.setStatus(HttpStatus.SC_FORBIDDEN);
                    } else{
                        response.sendRedirect("/");
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean shouldCheck(HttpServletRequest httpServletRequest, HandlerMethod handlerMethod) {
        if (fcProperties.isCSRFCheckEnabled()) {
            /*
             * 1. If admin link, CSRF check is not enabled.
             * 2. If CSRF annotation is present *and* the method is not excluded *then* check is enabled.
             * 3. If CSRF annotation is absent *then* all POST methods are enabled for check and all GET methods are disabled for check
             */

            if (httpServletRequest.getRequestURI().startsWith("/admin")) {
                return false;
            }

            Csrf csrf = handlerMethod.getMethodAnnotation(Csrf.class);
            
            if (csrf != null) {
                return !csrf.exclude();
            } else {
                if ("POST".equals(httpServletRequest.getMethod())) {
                    return true;
                }
                
                if ("GET".equals(httpServletRequest.getMethod())) {
                    return false;
                }
            }
            
        }

        return false;
    }
    
    private boolean isAppRequest(HttpServletRequest request) {
        String appType = request.getParameter("fcAppType");
        return (appType != null);
    }
}
