package com.freecharge.web.interceptor;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.api.authenticaton.ApiAuthenticationService;
import com.freecharge.common.api.authenticaton.ApiAuthenticatorCacheDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.commons.ApiAuthenticator;
import com.freecharge.web.interceptor.annotation.Authenticate;

public class ApiAuthenticationInterceptor extends HandlerInterceptorAdapter{

    private static final Logger logger = LoggingFactory.getLogger(ApiAuthenticationInterceptor.class);
    
    private static final String ACCESS_KEY_HEADER = "accessKey";
    
    @Autowired
    private ApiAuthenticationService apiAuthenticationService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod requestHandler = (HandlerMethod) handler;
        Authenticate authenticate = requestHandler.getMethod().getAnnotation(Authenticate.class);
        
        if (authenticate == null) {
            return true;
        }
        
        if(!authenticateApi(request, authenticate.api())){
            logger.info("Unable to authenticate for api "+authenticate.api());
            handleNotAuthenticatedResponse(response);
            return false;
        }
        
        return true;
    }

    private boolean authenticateApi(HttpServletRequest request, String api) throws IOException {
        String accessKey = request.getHeader(ACCESS_KEY_HEADER);
        if(FCUtil.isEmpty(accessKey)){
            logger.info("Accesss key is empty!");
            return false;
        }
        ApiAuthenticatorCacheDo apiAuthenticatorCacheDo = apiAuthenticationService.getApiAithenticatorDetails(accessKey);
        if(apiAuthenticatorCacheDo == null){
            logger.info("Could not find access details for access key "+accessKey);
            return false;
        }
        ApiAuthenticator apiAuthenticator = ApiAuthenticator.builder(apiAuthenticatorCacheDo.getSecretKey()).build();
        if(!apiAuthenticator.authenticate(request)){
            logger.info("Could not authenticate request for accessKey "+accessKey);
            return false;
        }
        Set<String> accessableApis = apiAuthenticatorCacheDo.getAccessibleApis();
        if(!accessableApis.contains(api) && !accessableApis.contains("*")){
            logger.info("Could not authorise request for accessKey "+accessKey);
            return false;
        }
        return true;
    }
    
    private void handleNotAuthenticatedResponse(HttpServletResponse response){
        try {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication required");
        } catch (IOException e) {
            logger.error("Error sending unauthorized error response to client", e);
        }
    }
}
