package com.freecharge.web.interceptor;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.CookieHandler;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.controller.LoginController;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class HTTPRequestInterceptor extends FCBaseInterceptor {
    private Logger logger = LoggingFactory.getLogger(HTTPRequestInterceptor.class);

    @Autowired
    private CookieHandler cookieHandler;

    @Autowired
    private FCProperties fcproperties;
    
    @Autowired

    private MetricsClient metricsClient;

    public static final String METRICS_SERVICE = "Session";
    public static final String CREATE_SESSION_METRIC_NAME = "createSession";
    public static final String STORE_SESSION_METRIC_NAME = "storeSession";

    private static final List<String> TO_BE_EXCLUDED_EXTENSIONS = ImmutableList.of(
        ".css", ".js", ".gif", ".jpg", ".jpeg", ".png", ".api", ".txt", ".woff", ".tff", ".ico", ".tpl"
    );

    private final List<String> EXCLUDED_PATHS = ImmutableList.of(
            "/rest/payment/callback/payu",
            "/rest/payment/notification",
            "/rest/coupon/all",
            "/rest/operators/Mobile",
            "/rest/operators/DTH",
            "/rest/operators/DataCard",
            "/payment/s2s/bd",
            "/rest/state/get",
            "/rest/circles",
            "/content/templates/templates.json",
            "/app/getoperatorcircle.htm",
            "/admin/timeline/events",
            "/app/card/store/vcc"
    );
    
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /*
         * Whether session is needed at all. 
         * TODO: Move this to a method.
         */
        if (handler instanceof HandlerMethod) {
            HandlerMethod requestHandler = (HandlerMethod) handler;
            NoSession noSession = requestHandler.getMethodAnnotation(NoSession.class);
            if (noSession != null) {
                return true;
            }
        }

        // Create Session Object
        String uri = request.getRequestURI();
        if (uri != null) {
            if (shouldCreateSession(uri)) {
                try {
                	long startTime = System.currentTimeMillis();

                	SessionManager sessionManager = (SessionManager) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_SESSIONMANAGER);
                    sessionManager.retrieveSession(request, response);
                    sessionManager.checkAndSetRememberMeDetails(request, response);
                	
                    long endTime = System.currentTimeMillis();
                	long latency = endTime - startTime;
                	try {
                        metricsClient.recordLatency(METRICS_SERVICE, CREATE_SESSION_METRIC_NAME, latency);
                	} catch(Exception e) {
                		logger.error("Error occurred while recording latency", e);
                	}
                } catch (Exception e) { //We are catching and just throwing exception here for logging purpose. If we do not do this, this exception
                    //gets logged in tomcat localhost log and not in our app log.
                    logger.error("Error occurred in pre handle", e);
                    throw e;
                }
            } else {
                FreechargeContext freechargeContext = new FreechargeContext();
                FreechargeContextDirectory.set(freechargeContext);
            }
        }
        return true;
    }

    public void postHandle( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (isSessionWriteNeeded(request, handler)) {
            long startTime = System.currentTimeMillis();
        	
            SessionManager sessionManager = (SessionManager) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_SESSIONMANAGER);
        	sessionManager.storeSession(request, response);
    		
        	long endTime = System.currentTimeMillis();
        	long latency = endTime - startTime;
        	try {
                metricsClient.recordLatency(METRICS_SERVICE, STORE_SESSION_METRIC_NAME, latency);        		
        	} catch(Exception e) {
        		logger.error("Error occurred while recording latency", e);
        	}

            //If logout action, do all the logout related activities.
            if (request.getAttribute(LoginController.DO_LOGOUT) != null) {
                if ((Boolean)request.getAttribute(LoginController.DO_LOGOUT)) {
                    doLogout(request, response);
                }
            }
        }else {
        	logger.info("No session write needed in HTTPRequestInterceptor");
        }
    }

    /**
     * Checks whether session store should be
     * updated as part of this request. For now
     * this method is a combination of exclusion list
     * as well as annotation driven, bit ugly, but soon
     * we shall move to annotation based.
     * @param handler
     */
    private boolean isSessionWriteNeeded(HttpServletRequest request, Object handler) {
        if (!(handler instanceof HandlerMethod)) {
            return false;
        }

        HandlerMethod requestHandler = (HandlerMethod) handler;
        NoSessionWrite noWrite = requestHandler.getMethodAnnotation(NoSessionWrite.class);
        
        if (noWrite != null) {
            return false;
        }
        
        String uri = request.getRequestURI();
        if (uri == null) {
            return false;
        }
        if (shouldCreateSession(uri)) {
            return true;
        }

        return false;
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (ex != null) {
            logger.error("----------<Exception interceptor start>----------");
            logger.error("Request URI is:" + request.getRequestURI());
            logger.error("Request param map:" + request.getParameterMap());
            logger.error("Caught exception in request interceptor", ex);
            logger.error("----------<Exception interceptor end>----------");
        }

        String uri = request.getRequestURI();
        if (uri != null) {
            FreechargeContextDirectory.invalidateData();
        }
    }

    private void doLogout(HttpServletRequest request, HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "logout", "click");
        try {
            FreechargeSession.setUserRelatedSessionData(null, null, false, null, null, 
                    request.getParameter(FCConstants.IMEI), request.getParameter(FCConstants.DEVICE_UNIQUE_ID));
            cookieHandler.expireApplicationCookie(request, response);

            SessionManager sessionManager = (SessionManager) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_SESSIONMANAGER);
            sessionManager.deleteSession();

            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                String passremeber = fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_USERID);

                for (int i = 0; i < cookies.length; i++) {
                    if (cookies[i].getName().equals(passremeber)) {
                        Cookie cookie = cookies[i];
                        cookie.setPath(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_PATH));
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "logout", "exception");
            logger.info("Exception caught",e);
        }
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "logout", "success");
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "logout", endTime - startTime);
    }

    private boolean shouldCreateSession(String uri) {
        if (StringUtils.isNotEmpty(uri)) {
            if (EXCLUDED_PATHS.contains(uri)) {
                return false;
            }
        }

        if (FCUtil.containsPartialMatch(uri, TO_BE_EXCLUDED_EXTENSIONS)) {
            return false;
        }

        return true;
    }
}
