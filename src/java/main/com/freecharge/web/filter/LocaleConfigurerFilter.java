package com.freecharge.web.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.LocaleResolver;

/**
 * Initialises locale in
 * {@link org.springframework.context.i18n.LocaleContextHolder} so that other
 * classes do not require access to HttpServletRequest object to get the current
 * locale. User <code>LocaleContextHolder.getLocale()<code> to get reference of current request locale.
 * 
 * @author shirish
 * 
 */
public class LocaleConfigurerFilter extends OncePerRequestFilter{
	@Autowired
	private LocaleResolver localeResolver;
	
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		if (localeResolver != null) {
			Locale locale = localeResolver.resolveLocale(request);
			LocaleContextHolder.setLocale(locale);
		}

		chain.doFilter(request, response);

		if (localeResolver != null) {
			LocaleContextHolder.resetLocaleContext();
		}
	}
}
