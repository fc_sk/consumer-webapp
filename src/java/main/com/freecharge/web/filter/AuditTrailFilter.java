package com.freecharge.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AuditTrailFilterManager;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.SpringBeanLocator;

/**
 * Created by IntelliJ IDEA. User: Toshiba Date: Apr 18, 2012 Time: 3:30:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class AuditTrailFilter extends OncePerRequestFilter {

	public FilterConfig filterConfig;
	private static Logger logger = LoggingFactory.getLogger(AuditTrailFilter.class);

	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

		String uri = httpServletRequest.getRequestURI();
		logger.info("Uri for the request is " + uri);
		if (uri != null) {
			if (uri.indexOf(".css") == -1 && uri.indexOf(".js") == -1 && uri.indexOf(".gif") == -1 && uri.indexOf(".jpg") == -1 && uri.indexOf(".jpeg") == -1 && uri.indexOf(".png") == -1 && uri.indexOf(".ico") == -1  && uri.indexOf(".api") == -1) {

				AuditTrailFilterManager auditTrailFilterManager = (AuditTrailFilterManager) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_AUDITTRAILMANAGER);
				auditTrailFilterManager.auditTrailFilterManager(httpServletRequest, uri);

				try {
					filterChain.doFilter(httpServletRequest, httpServletResponse);
				} catch (Exception exp) {

					logger.error("exception raised in the AuditTrailFilter. ", exp);
					throw new ServletException(exp);
				}
			} else {
				filterChain.doFilter(httpServletRequest, httpServletResponse);

			}
		}

	}

}
