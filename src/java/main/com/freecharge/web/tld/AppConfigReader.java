package com.freecharge.web.tld;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.util.BeanLocator;

public class AppConfigReader {

    public static String webAssetsVersion() {
        final AppConfigService acs = BeanLocator.getAppConfigServiceBean();
        return "" + acs.getWebAssetsVersion();
    }

}
