package com.freecharge.web.tld;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.util.BeanLocator;

public class FCPropertiesReader {

    public static String value(String key) {
        if ("version.no".equals(key)) {
            return AppConfigReader.webAssetsVersion();
        }
        FCProperties fCProperties = BeanLocator.getFCPropertiesBean();
        return fCProperties.getProperty(key);
    }

}
