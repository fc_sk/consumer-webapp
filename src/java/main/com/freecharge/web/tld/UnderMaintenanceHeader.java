package com.freecharge.web.tld;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.util.BeanLocator;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 5/4/13
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class UnderMaintenanceHeader {
    public static boolean isUnderMaintenanceHeaderSet() {
        AppConfigService appConfigService = BeanLocator.getAppConfigServiceBean();
        return appConfigService.isUnderMaintenanceHeaderBannerEnabled();
    }
}
