package com.freecharge.web.responsedo;

import com.freecharge.common.framework.basedo.BaseResponseDO;

public class UserSignUpResponseDO extends BaseResponseDO {
 

	private static final long serialVersionUID = 1L;

	private boolean registrationStatus;
    private Integer userId;
    private Integer userProfileId;
	public boolean isRegistrationStatus() {
		return registrationStatus;
	}
	public void setRegistrationStatus(boolean registrationStatus) {
		this.registrationStatus = registrationStatus;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}
    
}
