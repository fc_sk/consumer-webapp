package com.freecharge.web.responsedo;

import com.freecharge.common.framework.basedo.BaseResponseDO;

public class UserAuthLoginResponseDO extends BaseResponseDO{

    private String displayName;
    private boolean isLoggedIn;
    private Integer userId;
    private Integer userProfileId;
    
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}

}