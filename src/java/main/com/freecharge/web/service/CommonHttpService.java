package com.freecharge.web.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This class uses commonHttpClient defined in commonApplicationContext.
 * It has methods to make http post/get call for a given url.
 * Authentication also can be set per host.
 */
@Service("commonHttpService")
public class CommonHttpService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private HttpClient commonHttpClient;

    public CommonHttpService setNoTcpDelay(Boolean noTcpDelay){
        this.commonHttpClient.getHttpConnectionManager().getParams().setTcpNoDelay(noTcpDelay);
        return this;
    }

    public CommonHttpService setConnectionTimeOut(Integer timeout){
        this.commonHttpClient.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
        return this;
    }

    public HttpResponse firePostRequest(String url, Map<String, String> paramMap, Boolean auth) throws
            IOException, HTTPException {
        return this.firePostRequestWithTimeOut(url, paramMap, auth, null);
    }

    public HttpResponse firePostRequestWithTimeOut(String url, Map<String, String> paramMap, Boolean auth, Integer timeout)
            throws IOException, HTTPException {
        PostMethod post = new PostMethod(url);
        if (timeout!=null){
            post.getParams().setSoTimeout(timeout);
        }
        if (auth){
            post.setDoAuthentication(true);
        }
        List<NameValuePair> params = new ArrayList<>();
        for (Map.Entry<String, String> pm : paramMap.entrySet()){
            params.add(new NameValuePair(pm.getKey(), pm.getValue()));
        }
        post.setRequestBody(params.toArray(new NameValuePair[params.size()]));
        logger.info("Making POST request for url: "+url);
        try{
            int status = this.commonHttpClient.executeMethod(post);
            logger.info(String.format("Successful request to: %s with auth: %s", url, auth));
            return new HttpResponse(status, post.getStatusText(), post.getResponseBodyAsString());
        }finally {
            logger.debug("Connection release in post for url: "+url);
            post.releaseConnection();
        }
    }

    public HttpResponse firePostRequestWithCustomHeader(String url, String json, Boolean auth,
                                                        Integer timeout, Map<String, String> headers)
            throws IOException, HTTPException {
        PostMethod post = new PostMethod(url);
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            post.setRequestHeader(entry.getKey(), entry.getValue());
        }

        if (timeout!=null){
            post.getParams().setSoTimeout(timeout);
        }

        if (auth){
            post.setDoAuthentication(true);
        }

        post.setRequestBody(json);
        logger.info("Making POST request for url: "+url + " and body " + json);
        try{
            int status = this.commonHttpClient.executeMethod(post);
            logger.info(String.format("Successful request to: %s with auth: %s", url, auth));
            return new HttpResponse(status, post.getStatusText(), post.getResponseBodyAsString());
        }finally {
            logger.debug("Connection release in post for url: "+url);
            post.releaseConnection();
        }
    }

    public HttpResponse fireGetRequest(String url, Map<String, String> paramMap) throws IOException,
            HTTPException {
        return this.fireGetRequestWithTimeOut(url, paramMap, null);
    }

    public HttpResponse fireGetRequestWithTimeOut(String url, Map<String, String> paramMap, Integer timeout) throws IOException,
            HTTPException {
        GetMethod get = new GetMethod(url);
        if (timeout!=null){
            get.getParams().setSoTimeout(timeout);
        }
        List<NameValuePair> params = new ArrayList<>();
        for (Map.Entry<String, String> pm : paramMap.entrySet()){
            params.add(new NameValuePair(pm.getKey(), pm.getValue()));
        }
        get.setQueryString(params.toArray(new NameValuePair[params.size()]));
        logger.info("Making GET request: "+url+" query_string: "+get.getQueryString());
        try {
            int statusCode = this.commonHttpClient.executeMethod(get);
            logger.info("Got status: "+statusCode+" for url: "+url+"?"+get.getQueryString());
            String responseBody = "";
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()))) {
                String line;
                while ((line = reader.readLine()) != null){
                    responseBody+=line;
                }
            }
            return new HttpResponse(statusCode, get.getStatusText(), responseBody);
        }finally {
            logger.debug("Connection release in get for url: "+url);
            get.releaseConnection();
        }
    }

    public HttpResponse fireGetRequestWithTimeOutAndCustomHeader(String url, Map<String, String> paramMap,
                                                                 Integer timeout, Map<String, String> headers)
            throws IOException,
            HTTPException {
        GetMethod get = new GetMethod(url);
        if (timeout!=null){
            get.getParams().setSoTimeout(timeout);
        }

        for (Map.Entry<String, String> entry : headers.entrySet()) {
            get.setRequestHeader(entry.getKey(), entry.getValue());
        }

        List<NameValuePair> params = new ArrayList<>();

        if (paramMap != null) {
            for (Map.Entry<String, String> pm : paramMap.entrySet())    {
                params.add(new NameValuePair(pm.getKey(), pm.getValue()));
            }
        }

        get.setQueryString(params.toArray(new NameValuePair[params.size()]));
        logger.info("Making GET request: "+url+" query_string: "+get.getQueryString());
        try {
            int statusCode = this.commonHttpClient.executeMethod(get);
            logger.info("Got status: "+statusCode+" for url: "+url+"?"+get.getQueryString());
            String responseBody = "";
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()))) {
                String line;
                while ((line = reader.readLine()) != null){
                    responseBody+=line;
                }
            }
            return new HttpResponse(statusCode, get.getStatusText(), responseBody);
        }finally {
            logger.debug("Connection release in get for url: "+url);
            get.releaseConnection();
        }
    }

    public CommonHttpService setAuthentication(String host, Integer port, String username, String password){
        this.commonHttpClient.getParams().setAuthenticationPreemptive(true);
        this.commonHttpClient.getState().setCredentials(new AuthScope(host, port, AuthScope.ANY_REALM, AuthPolicy.BASIC),
                new UsernamePasswordCredentials(username, password));
        return this;
    }

    public class HttpResponse {
        Integer statusCode;
        String status;
        String responseBody;

        private HttpResponse(Integer statusCode, String status, String responseBody) {
            this.statusCode = statusCode;
            this.status = status;
            this.responseBody = responseBody;
        }

        public Integer getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getResponseBody() {
            return responseBody;
        }

        public void setResponseBody(String responseBody) {
            this.responseBody = responseBody;
        }
    }
}
