package com.freecharge.web.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.FreefundReadDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeCampaignReadDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeCampaignWriteDAO;
import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.google.common.base.CaseFormat;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 8/11/12 Time: 8:23 PM To change this template use File |
 * Settings | File Templates.
 */
@Service
public class PromocodeCampaignService {
    
	@Autowired
	private PromocodeCampaignWriteDAO promocodeCampaignWriteDAO;
	
	@Autowired
    private PromocodeCampaignReadDAO promocodeCampaignReadDAO;
	
	@Autowired
	private FreefundReadDAO freefundReadDAO;

    @Autowired
    private UserServiceProxy userServiceProxy;

    public Integer insertPromocodeCampaign(PromocodeCampaign promocodeCampaign) {
	    return this.promocodeCampaignWriteDAO.insertPromocodeCampaign(promocodeCampaign);
	}
 
    public List<PromocodeCampaign> getAllPromocodeCampaignByUserId(String couponCode, Long userId) {
	    Long freefundCouponId = Long.valueOf(freefundReadDAO.getPromocodeIdByCode(couponCode));
        return this.promocodeCampaignReadDAO.getPromocodeCampaignByUserId(freefundCouponId, userId);
    }
    
    public PromocodeCampaign getPromocodeCampaign(Long couponId, Long userId) { 
        List<PromocodeCampaign> list = promocodeCampaignReadDAO.getPromocodeCampaignByUserId(couponId, userId);
        
        if (list != null && list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

	public List<String> getRedeemedPromocodeOrderIdForUser(Long userId, Long freefundClassId)  {
	    return this.promocodeCampaignReadDAO.getRedeemedPromocodeOrderIdForUser(userId, freefundClassId);   
	}

	public List<String> getRedeemedPromocodeOrderIdForServiceNumber(String serviceNumber, Long freefundClassId) {
	    return this.promocodeCampaignReadDAO.getRedeemedPromocodeOrderIdForServiceNumber(serviceNumber, freefundClassId);
	}
		
	public static String getBeanName() {
		return CaseFormat.UPPER_CAMEL.to(
				CaseFormat.LOWER_CAMEL,
					PromocodeCampaignService.class.getSimpleName());
	}

    public List<Map<String, Object>> getPromocodeUsageDetails(long promocodeId, String key, String value) {
        List<Map<String, Object>> promocodeList = promocodeCampaignReadDAO.getPromocodeUsageDetails(promocodeId,
                key, value);
        for (Map<String, Object> promocode : promocodeList) {
            String userId = String.valueOf(promocode.get("fk_user_id"));
            if (FCUtil.isNotEmpty(userId)) {
                Users user = userServiceProxy.getUserByUserId(Integer.parseInt(userId));
                promocode.put("email", user.getEmail());
            }
        }
        return promocodeList;
    }
}