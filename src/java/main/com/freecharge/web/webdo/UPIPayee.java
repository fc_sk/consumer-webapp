package com.freecharge.web.webdo;

public class UPIPayee {
    
    private String bamount;
    private String beneVpa;
    private String beneName;
    public String getBamount() {
        return this.bamount;
    }
    public void setBamount(String bamount) {
        this.bamount = bamount;
    }
    public String getBeneVpa() {
        return this.beneVpa;
    }
    public void setBeneVpa(String beneVpa) {
        this.beneVpa = beneVpa;
    }
    public String getBeneName() {
        return this.beneName;
    }
    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }   
    
    

}
