package com.freecharge.web.webdo;

import com.freecharge.web.webdo.FeeEngineException;
import com.freecharge.web.webdo.ExceptionErrorCode;

public class ValidationException extends FeeEngineException{

	public void setErrorCode(ExceptionErrorCode code)
	{
		super.setErrorCode(code);
	}

	public ValidationException(Exception e)
	{
		super(e);
		setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION);
	}

	public ValidationException(Throwable e)
	{
		super(e);
		setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION);
	}

	public ValidationException(String message)
	{
		super(message);
		setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION); 
	} 
//	public ValidationException() { 
//		setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION);
//	}

}
