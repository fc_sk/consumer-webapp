package com.freecharge.web.webdo;

public class UPIDeviceInfo {
    private String app;
    private String mobile;
    private String os;
    private String type;
    
    public String getApp() {
        return this.app;
    }
    public void setApp(String app) {
        this.app = app;
    }
    public String getMobile() {
        return this.mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getOs() {
        return this.os;
    }
    public void setOs(String os) {
        this.os = os;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    

}
