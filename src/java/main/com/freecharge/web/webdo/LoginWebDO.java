package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;


/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: Apr 30, 2012
 * Time: 12:19:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginWebDO extends BaseWebDO{
    private String email;
    private String password;
    private boolean login;
    private String action;
    private String rememberme;
    private String userId;
    private String firstName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

	public String getRememberme() {
		return rememberme;
	}

	public void setRememberme(String rememberme) {
		this.rememberme = rememberme;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
    
}
