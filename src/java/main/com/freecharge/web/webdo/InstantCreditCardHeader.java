package com.freecharge.web.webdo;

public class InstantCreditCardHeader {
    private String      requestUUID;

    private String      serviceRequestId;

    private String      serviceRequestVersion;

    private String      channelId;

    public String getRequestUUID() {
        return requestUUID;
    }

    public void setRequestUUID(String requestUUID) {
        this.requestUUID = requestUUID;
    }

    public String getServiceRequestId() {
        return serviceRequestId;
    }

    public void setServiceRequestId(String serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }

    public String getServiceRequestVersion() {
        return serviceRequestVersion;
    }

    public void setServiceRequestVersion(String serviceRequestVersion) {
        this.serviceRequestVersion = serviceRequestVersion;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
