package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class TestWebDO extends BaseWebDO {

    private Integer one;
    private String dbName;

    public Integer getOne() {
        return one;
    }

    public void setOne(Integer one) {
        this.one = one;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
