package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 4:03:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserRechageDetailsWebDo extends BaseWebDO {
    private String rechargeMobileNo;
    private Integer rechargeAmount;
    private Integer refundAmount;
    private Integer handlingCharges;
    private Integer walletAmount;
    private Integer walletDeductedAmount;
    private Integer totalPGAmount;
    private Integer totalCouponAmount;
    private String sessionId;

    public String getRechargeMobileNo() {
        return rechargeMobileNo;
    }

    public void setRechargeMobileNo(String rechargeMobileNo) {
        this.rechargeMobileNo = rechargeMobileNo;
    }

    public Integer getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(Integer rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public Integer getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }

    public Integer getHandlingCharges() {
        return handlingCharges;
    }

    public void setHandlingCharges(Integer handlingCharges) {
        this.handlingCharges = handlingCharges;
    }

    public Integer getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Integer walletAmount) {
        this.walletAmount = walletAmount;
    }

    public Integer getWalletDeductedAmount() {
        return walletDeductedAmount;
    }

    public void setWalletDeductedAmount(Integer walletDeductedAmount) {
        this.walletDeductedAmount = walletDeductedAmount;
    }

    public Integer getTotalPGAmount() {
        return totalPGAmount;
    }

    public void setTotalPGAmount(Integer totalPGAmount) {
        this.totalPGAmount = totalPGAmount;
    }

    public Integer getTotalCouponAmount() {
        return totalCouponAmount;
    }

    public void setTotalCouponAmount(Integer totalCouponAmount) {
        this.totalCouponAmount = totalCouponAmount;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
