package com.freecharge.web.webdo;


import com.freecharge.common.framework.basedo.BaseWebDO;

public class VouchersQuantityWebDO extends BaseWebDO {

	private static final long serialVersionUID = 1L;

	private Integer couponStoreId;
	private Integer quantity;
	private boolean isComplimentary;
	
	
	public Integer getCouponStoreId() {
		return couponStoreId;
	}
	public void setCouponStoreId(Integer couponStoreId) {
		this.couponStoreId = couponStoreId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public boolean isComplimentary() {
		return isComplimentary;
	}
	public void setComplimentary(boolean isComplimentary) {
		this.isComplimentary = isComplimentary;
	}
	
	
}
