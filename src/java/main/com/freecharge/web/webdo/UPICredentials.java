package com.freecharge.web.webdo;

public class UPICredentials {
    
    private UPIHashData data;
    private String subType;
    private String type;
    
    public UPIHashData getData() {
        return this.data;
    }
    public void setData(UPIHashData data) {
        this.data = data;
    }
    public String getSubType() {
        return this.subType;
    }
    public void setSubType(String subType) {
        this.subType = subType;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }

}
