package com.freecharge.web.webdo;

import java.io.Serializable;

public enum ServiceChargeEntity implements Serializable{

	ServiceTax("ServiceTax"), SwachhBharatCess("SwachhBharatCess"), KrishiKalyanCess("KrishiKalyanCess");
	
	private String entityName;
	
	private ServiceChargeEntity(String name) {
		this.entityName = name;
	}
	
	public String getName() {
		return entityName;
	}
}
