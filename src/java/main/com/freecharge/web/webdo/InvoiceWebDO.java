package com.freecharge.web.webdo;

import java.sql.Timestamp;
import java.util.List;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.basedo.BaseWebDO;

public class InvoiceWebDO extends BaseWebDO{
	private RechargeInfoVO rechargeInfoVO;
	private Double totalAmount;
    private Timestamp paymentTime;
	
	private ProductName primaryProductName;
	
    public void setPaymentTime(Timestamp paymentTime) {
       this.paymentTime = paymentTime;
    }

    public Timestamp getPaymentTime() {
       return paymentTime;
    }

	public ProductName getPrimaryProductName() {
        return primaryProductName;
    }
	
	public void setPrimaryProductName(ProductName primaryProductName) {
        this.primaryProductName = primaryProductName;
    }

	public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setRechargeInfoVO(RechargeInfoVO rechargeInfoVO) {
		this.rechargeInfoVO = rechargeInfoVO;
	}

	private List<CartAndCartItemsVO> cartAndCartItemsList;
	
	public List<CartAndCartItemsVO> getCartAndCartItemsList() {
		return cartAndCartItemsList;
	}

	public void setCartAndCartItemsList(List<CartAndCartItemsVO> cartAndCartItemsList) {
		this.cartAndCartItemsList = cartAndCartItemsList;
	}

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public RechargeInfoVO getRechargeInfoVO() {
		return rechargeInfoVO;
	}
}
