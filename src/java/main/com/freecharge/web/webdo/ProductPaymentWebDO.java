package com.freecharge.web.webdo;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ProductPaymentWebDO extends PaymentRequestCardDetails {

    private final org.apache.log4j.Logger logger = LoggingFactory.getLogger(getClass());

    private static final long serialVersionUID = 1L;
    private String upiInfo;
    private String couponCode;
	private int offerType;
	private int binOfferId;
	private String productType;
    private String paymenttype;
    private String productId;
    private String affiliateid;
    //private String paymentoption;
    private String cartId;
    //final payment form paramater
    private String at;
    private String pycur;
    private String orderid;
    private String st = "false";
    private String pgover = "";
    private String pyop;
    private String pytype;
    private String frnm;
    private String mbph;
    private String email;
    private String blad1;
    private String blctr;
    private String blst;
    private String blpin;
    private String blct;

    private String scurl;
    private String furl;
    private String errurl;
    private String pdid;
    private String affid;
    private String opid;

    private String breaker;

    private String actionUrl;
    
    private String ccHolderName;
    private String ccNum;
    private String ccExpMon;
    private String ccExpYear;
    private String ccCvv;
    private String ccCardToken;
    private String ccCardSaveBool;
    private String ccCardSaveName;

    private String dbHolderName;
    private String dbNum;
    private String dbExpMon;
    private String dbExpYear;
    private String dbCvv;
    
    private String usePartialWalletPayment;
    private String dbCardToken;
    private String dbCardSaveBool;
    private String dbCardSaveName;

    private String unCheckedStoreCard;

    private Map<String,String> params;

    private String transactionTypeStatus;
    
    private String alertMobileNo;
    private boolean setAsProfileNumberFlag;
    
    private boolean isQCPayment;

    private boolean isDebitPinTxn;
    
    private boolean isIttpTxn;
    
    private String isSavedCardTxn;
    
    private String retryOrderId;
    
    private String utmDetails;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductPaymentWebDO.class);


    public String getTransactionTypeStatus() {
        return transactionTypeStatus;
    }

    public void setTransactionTypeStatus(String transactionTypeStatus) {
        this.transactionTypeStatus = transactionTypeStatus;
    }

    public boolean isSetAsProfileNumberFlag() {
		return setAsProfileNumberFlag;
	}

	public void setSetAsProfileNumberFlag(boolean setAsProfileNumberFlag) {
		this.setAsProfileNumberFlag = setAsProfileNumberFlag;
	}



	public String getAlertMobileNo() {
		return alertMobileNo;
	}



	public void setAlertMobileNo(String alertMobileNo) {
		this.alertMobileNo = alertMobileNo;
	}

    public Map<String,String> getParams(){
        if (params != null) {
            params.put("is_saved_card_txn", isSavedCardTxn);
            return params;
        }
        params = new HashMap<String, String>();
        params.put("at", at);
        params.put("upiInfo", upiInfo);
        params.put("pycur", pycur);
        params.put("orderid", orderid);
        params.put("st", st);
        params.put("pgover", pgover);
        params.put("pyop", pyop);
        params.put("pytype", pytype);
        params.put("frnm", frnm);
        params.put("mbph", mbph);
        params.put("email", email);
        params.put("blad1", blad1);
        params.put("blctr", blctr);
        params.put("blst", blst);
        params.put("blpin", blpin);
        params.put("blct", blct);
        params.put("couponCode", couponCode);
        params.put("productType", productType);

        params.put("scurl", scurl);
        params.put("errurl", errurl);
        params.put("pdid", pdid);
        params.put("affid", affid);
        params.put("opid", opid);
        
        params.put("ccHolderName", ccHolderName);
        params.put("ccNum", ccNum);
        params.put("ccExpMon", ccExpMon);
        params.put("ccExpYear", ccExpYear);
        params.put("ccCvv", ccCvv);

        params.put("dbHolderName", dbHolderName);
        params.put("dbNum", dbNum);
        params.put("dbExpMon", dbExpMon);
        params.put("dbExpYear", dbExpYear);
        params.put("dbCvv", dbCvv);

        params.put("usePartialWalletPayment", usePartialWalletPayment);

        // extra fields for card storage, removed in PaymentPortalController
        params.put("ccCardToken", ccCardToken);
        params.put("ccCardSaveBool", ccCardSaveBool);
        params.put("ccCardSaveName", ccCardSaveName);
        params.put("dbCardToken", dbCardToken);
        params.put("dbCardSaveBool", dbCardSaveBool);
        params.put("dbCardSaveName", dbCardSaveName);
        
        params.put("cardHolderName", cardHolderName);
        params.put("cardNumber", cardNumber);
        params.put("cardExpMonth", cardExpMonth);
        params.put("cardExpYear", cardExpYear);
        params.put("cardCvv", cardCvv);

        // extra fields for card storage, removed in PaymentPortalController
        params.put("cardToken", cardToken);
        params.put("saveCard", String.valueOf(saveCard));
        params.put("saveCardName", saveCardName);
        params.put("isDebitCard", String.valueOf(isDebitCard));

        params.put("breaker", breaker);
        params.put("binOfferId", binOfferId+"");
        
        params.put("is_qc", Boolean.toString(isQCPayment));
        params.put("is_debit_atm_pin", Boolean.toString(isDebitPinTxn));
        params.put("is_ittp", Boolean.toString(isIttpTxn));
        params.put("is_saved_card_txn", isSavedCardTxn);
        LOGGER.info("retryOrderId : " + retryOrderId);
        params.put("retry_order_id", retryOrderId);
        return params;

    }
    
    
    
    public String getUsePartialWalletPayment() {
        return usePartialWalletPayment;
    }
    
    public void setUsePartialWalletPayment(String usePartialWalletPayment) {
        this.usePartialWalletPayment = usePartialWalletPayment;
    }
    
    public Boolean isPartialWalletPaymentsSet() {
        if ("true".equals(usePartialWalletPayment)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
    

    public String getCcCardToken() {
		return ccCardToken;
	}

	public void setCcCardToken(String ccCardToken) {
		this.ccCardToken = ccCardToken;
	}

	public String getUpiInfo() {
        return this.upiInfo;
    }

    public void setUpiInfo(String upiInfo) {
        this.upiInfo = upiInfo;
    }

    public String getCcCardSaveBool() {
		return ccCardSaveBool;
	}

	public void setCcCardSaveBool(String ccCardSaveBool) {
		this.ccCardSaveBool = ccCardSaveBool;
	}

	public String getCcCardSaveName() {
		return ccCardSaveName;
	}

	public void setCcCardSaveName(String ccCardSaveName) {
		this.ccCardSaveName = ccCardSaveName;
	}

	public String getDbCardSaveBool() {
		return dbCardSaveBool;
	}

	public void setDbCardSaveBool(String dbCardSaveBool) {
		this.dbCardSaveBool = dbCardSaveBool;
	}

	public String getDbCardToken() {
		return dbCardToken;
	}

	public void setDbCardToken(String dbCardToken) {
		this.dbCardToken = dbCardToken;
	}

	public String getDbCardSaveName() {
		return dbCardSaveName;
	}

	public void setDbCardSaveName(String dbCardSaveName) {
		this.dbCardSaveName = dbCardSaveName;
	}

	public String getDbCvv() {
        return dbCvv;
    }
    
    public void setDbCvv(String dbCvv) {
        this.dbCvv = dbCvv;
    }
    
    public void setDbExpMon(String dbExpMon) {
        this.dbExpMon = dbExpMon;
    }
    
    public String getDbExpMon() {
        return dbExpMon;
    }
    
    public void setDbExpYear(String dbExpYear) {
        this.dbExpYear = dbExpYear;
    }
    
    public String getDbExpYear() {
        return dbExpYear;
    }
    
    public void setDbHolderName(String dbHolderName) {
        this.dbHolderName = dbHolderName;
    }
    
    public String getDbHolderName() {
        return dbHolderName;
    }
    
    public void setDbNum(String dbNum) {
        this.dbNum = dbNum;
    }
    
    public String getDbNum() {
        return dbNum;
    }
    
    public String getCcCvv() {
        return ccCvv;
    }
    
    public void setCcCvv(String ccCvv) {
        this.ccCvv = ccCvv;
    }
    
    public String getCcNum() {
        return ccNum;
    }
    
    public void setCcNum(String ccNum) {
        this.ccNum = ccNum;
    }
    
    public String getCcExpMon() {
        return ccExpMon;
    }
    
    public void setCcExpMon(String ccExpMon) {
        this.ccExpMon = ccExpMon;
    }
    
    public String getCcExpYear() {
        return ccExpYear;
    }
    
    public void setCcExpYear(String ccExpYear) {
        this.ccExpYear = ccExpYear;
    }
    
    public String getCcHolderName() {
        return ccHolderName;
    }
    
    public void setCcHolderName(String ccHolderName) {
        this.ccHolderName = ccHolderName;
    }
    
    public String getProductType() {
		return productType;
	}
    
    public void setProductType(String productType) {
		this.productType = productType;
	}
    
    public String getCouponCode() {
		return couponCode;
	}
    
    public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

    public int getOfferType() {
		return offerType;
	}

	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}

	public int getBinOfferId() {
		return binOfferId;
	}

	public void setBinOfferId(int binOfferId) {
		this.binOfferId = binOfferId;
	}

	public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAffiliateid() {
        return affiliateid;
    }

    public void setAffiliateid(String affiliateid) {
        this.affiliateid = affiliateid;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getPaymentoption() {
        return pyop;
    }

    public void setPaymentoption(String pyop) {
        this.pyop = pyop;
    }

    //payment portal redirect form paramters
    private String paymentPortalRedirectAction;


    //security and validation errors
    private Boolean error;
    private String errorDesc;


    public String getPaymentPortalRedirectAction() {
        return paymentPortalRedirectAction;
    }

    public void setPaymentPortalRedirectAction(String paymentPortalRedirectAction) {
        this.paymentPortalRedirectAction = paymentPortalRedirectAction;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }

    public String getPycur() {
        return pycur;
    }

    public void setPycur(String pycur) {
        this.pycur = pycur;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getPgover() {
        return pgover;
    }

    public void setPgover(String pgover) {
        this.pgover = pgover;
    }

    public String getPyop() {
        return pyop;
    }

    public void setPyop(String pyop) {
        this.pyop = pyop;
    }

    public String getPytype() {
        return pytype;
    }

    public void setPytype(String pytype) {
        this.pytype = pytype;
    }

    public String getFrnm() {
        return frnm;
    }

    public void setFrnm(String frnm) {
        this.frnm = frnm;
    }

    public String getMbph() {
        return mbph;
    }

    public void setMbph(String mbph) {
        this.mbph = mbph;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBlad1() {
        return blad1;
    }

    public void setBlad1(String blad1) {
        this.blad1 = blad1;
    }

    public String getBlctr() {
        return blctr;
    }

    public void setBlctr(String blctr) {
        this.blctr = blctr;
    }

    public String getBlst() {
        return blst;
    }

    public void setBlst(String blst) {
        this.blst = blst;
    }

    public String getBlpin() {
        return blpin;
    }

    public void setBlpin(String blpin) {
        this.blpin = blpin;
    }

    public String getBlct() {
        return blct;
    }

    public void setBlct(String blct) {
        this.blct = blct;
    }

    public String getScurl() {
        return scurl;
    }

    public void setScurl(String scurl) {
        this.scurl = scurl;
    }

    public String getErrurl() {
        return errurl;
    }

    public void setErrurl(String errurl) {
        this.errurl = errurl;
    }

    public String getPdid() {
        return pdid;
    }

    public void setPdid(String pdid) {
        this.pdid = pdid;
    }

    public String getAffid() {
        return affid;
    }

    public void setAffid(String affid) {
        this.affid = affid;
    }

    public String getOpid() {
        return opid;
    }

    public void setOpid(String opid) {
        this.opid = opid;
    }

    public String getBreaker() {
        return breaker;
    }

    public void setBreaker(String breaker) {
        this.breaker = breaker;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public String getFurl() {
        return furl;
    }

    public void setFurl(String furl) {
        this.furl = furl;
    }

    public String getUnCheckedStoreCard() {
        return unCheckedStoreCard;
    }

    public void setUnCheckedStoreCard(String unCheckedStoreCard) {
        this.unCheckedStoreCard = unCheckedStoreCard;
    }

    public boolean isQCPayment() {
        return isQCPayment;
    }

    public void setQCPayment(boolean isQCPayment) {
        this.isQCPayment = isQCPayment;
    }
    
    public boolean isDebitPinTxn() {
        return isDebitPinTxn;
    }

    public void setDebitPinTxn(boolean isDebitPinTxn) {
        this.isDebitPinTxn = isDebitPinTxn;
    }

    public boolean isIttpTxn() {
        return isIttpTxn;
    }

    public void setIttpTxn(boolean isIttpTxn) {
        this.isIttpTxn = isIttpTxn;
    }

    public String isSavedCardTxn() {
        return this.isSavedCardTxn;
    }

    public void setSavedCardTxn(String isSavedCardTxn) {
        this.isSavedCardTxn = isSavedCardTxn;
    }

    public String getRetryOrderId() {
        return retryOrderId;
    }

    public void setRetryOrderId(String retryOrderId) {
        this.retryOrderId = retryOrderId;
    }

    public String getUtmDetails() {
        return utmDetails;
    }

    public void setUtmDetails(String utmDetails) {
        this.utmDetails = utmDetails;
    }

    public boolean isSaveCardChecked() {
        if ((!FCUtil.isEmpty(this.ccCardSaveBool) && Boolean.parseBoolean(this.ccCardSaveBool))
                || (!FCUtil.isEmpty(this.dbCardSaveBool) && Boolean.parseBoolean(this.dbCardSaveBool)) || isSaveCard()) {
            return true;
        }
        return false;
    }
    
    public void setCardDetails() {
        if (paymenttype.equals(PaymentConstants.PAYMENT_TYPE_CREDITCARD)
                && (!FCUtil.isEmpty(ccNum) || !FCUtil.isEmpty(ccCardToken))) {
            setCardRelatedData(ccNum, ccHolderName, ccCardToken, ccCvv, ccExpMon, ccExpYear, ccCardSaveName,
                    ccCardSaveBool, false);
        } else if (paymenttype.equals(PaymentConstants.PAYMENT_TYPE_DEBITCARD)
                && (!FCUtil.isEmpty(dbNum) || !FCUtil.isEmpty(dbCardToken))) {
            setCardRelatedData(dbNum, dbHolderName, dbCardToken, dbCvv, dbExpMon, dbExpYear, dbCardSaveName,
                    dbCardSaveBool, true);
        }
    }

    private void setCardRelatedData(String cardNo, String cardHolderName, String cardToken, String cvv, String expMon,
            String expYear, String savedCardName, String saveCard, Boolean isDebitCard) {
        setDebitCard(isDebitCard);
        setCardNumber(cardNo);
        setCardHolderName(cardHolderName);
        setCardToken(cardToken);
        setCardCvv(cvv);
        setCardExpMonth(expMon);
        setCardExpYear(expYear);
        setSaveCard(Boolean.valueOf(saveCard));
        setSaveCardName(savedCardName);
        logger.info("Card type pre calc " + cardNo+ " " + isDebitCard + " " + getPaymentoption());
        setCardType(PaymentUtil.getCardTypeFromCard(cardNo, isDebitCard, getPaymentoption()));
        logger.info("Card type " + cardType);
    }
    
    @Override
    public String toString() {
        return "ProductPaymentWebDO [upiInfo= "+upiInfo + "couponCode=" + couponCode + ", offerType=" + offerType + ", binOfferId="
                + binOfferId + ", productType=" + productType + ", paymenttype=" + paymenttype + ", productId="
                + productId + ", affiliateid=" + affiliateid + ", cartId=" + cartId + ", at=" + at
                + ", pycur=" + pycur + ", orderid=" + orderid + ", st=" + st + ", pgover=" + pgover + ", pyop=" + pyop
                + ", pytype=" + pytype + ", frnm=" + frnm + ", mbph=" + mbph + ", email=" + email + ", blad1=" + blad1
                + ", blctr=" + blctr + ", blst=" + blst + ", blpin=" + blpin + ", blct=" + blct + ", scurl=" + scurl
                + ", furl=" + furl + ", errurl=" + errurl + ", pdid=" + pdid + ", affid=" + affid + ", opid=" + opid
                + ", breaker=" + breaker + ", actionUrl=" + actionUrl + ", usePartialWalletPayment=" + usePartialWalletPayment
                + ", unCheckedStoreCard=" + unCheckedStoreCard + ", params=" + params
                + ", alertMobileNo=" + alertMobileNo + ", setAsProfileNumberFlag=" + setAsProfileNumberFlag
                + ", paymentPortalRedirectAction=" + paymentPortalRedirectAction + ", error=" + error + ", errorDesc="
                + errorDesc + ", utmDetails="+utmDetails+"]";
    }
}
