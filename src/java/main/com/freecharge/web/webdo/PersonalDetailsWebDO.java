package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.api.coupon.service.model.CityMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.util.TxnCrosssellItemsVO;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 16, 2012
 * Time: 11:10:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class PersonalDetailsWebDO extends BaseWebDO{
    private String serviceNumber;
    private Integer txnHomePageId;
    private UserContactDetail userContactDetail;
    private String defaultProfile;
    private String satus;
    private CartWebDo cart;
    private List<StateMaster> stateMaster;
    private List<CrosssellWebDo> crosssellWebDos;
    private List<PaymentTypeMasterWebDO> paymentTypes;
    private List<TxnCrosssellItemsVO> txnCrossSellIdList;
    private List<CityMaster> cityMasterList;
    private String operatorImgUrl;
    private String operatorName;
    
    private String productType;
    private String circleName;
    
    private WalletEligibility walletEligibility;
    private PaymentBreakup paymentBreakup;
    
    private String walletBalance;
    private String payableTotalAmount;
    private String payablePGAmount;
    private String payableWalletAmount;
    
    private Boolean walletEnabled;
    private Boolean walletDisplayEnabled;

    private Boolean cardStorageOptIn;

    private Boolean contactDetailsReq;
    
    public PaymentBreakup getPaymentBreakup() {
        return paymentBreakup;
    }
    
    public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
        this.paymentBreakup = paymentBreakup;
    }
    
    public WalletEligibility getWalletEligibility() {
        return walletEligibility;
    }
    
    public void setWalletEligibility(WalletEligibility walletEligibility) {
        this.walletEligibility = walletEligibility;
    }

    public Boolean getWalletDisplayEnabled() {
        return walletDisplayEnabled;
    }
    
    public void setWalletDisplayEnabled(Boolean walletDisplayEnabled) {
        this.walletDisplayEnabled = walletDisplayEnabled;
    }
    
    public Boolean getWalletEnabled() {
        return walletEnabled;
    }
    
    public void setWalletEnabled(Boolean walletEnabled) {
        this.walletEnabled = walletEnabled;
    }

    public String getWalletBalance() {
        return walletBalance;
    }
    
    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }
    
    public String getPayableTotalAmount() {
        return payableTotalAmount;
    }
    
    public void setPayableTotalAmount(String payableTotalAmount) {
        this.payableTotalAmount = payableTotalAmount;
    }
    
    public String getPayablePGAmount() {
        return payablePGAmount;
    }
    
    public void setPayablePGAmount(String payablePGAmount) {
        this.payablePGAmount = payablePGAmount;
    }
    
    public String getPayableWalletAmount() {
        return payableWalletAmount;
    }
    
    public void setPayableWalletAmount(String payableWalletAmount) {
        this.payableWalletAmount = payableWalletAmount;
    }
    
    public List<CrosssellWebDo> getCrosssellWebDos() {
		return crosssellWebDos;
	}

	public void setCrosssellWebDos(List<CrosssellWebDo> crosssellWebDos) {
		this.crosssellWebDos = crosssellWebDos;
	}

	public CartWebDo getCart() {
		return cart;
	}

	public List<StateMaster> getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(List<StateMaster> stateMaster) {
		this.stateMaster = stateMaster;
	}

	public void setCart(CartWebDo cart) {
		this.cart = cart;
	}

	public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public UserContactDetail getUserContactDetail() {
        return userContactDetail;
    }

    public void setUserContactDetail(UserContactDetail userContactDetail) {
        this.userContactDetail = userContactDetail;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getSatus() {
        return satus;
    }

    public void setSatus(String satus) {
        this.satus = satus;
    }

    public List<PaymentTypeMasterWebDO> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentTypeMasterWebDO> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

	public void setTxnCrossSellIdList(List<TxnCrosssellItemsVO> txnCrossSellIdList) {
		this.txnCrossSellIdList = txnCrossSellIdList;
	}

	public List<TxnCrosssellItemsVO> getTxnCrossSellIdList() {
		return txnCrossSellIdList;
	}

	public List<CityMaster> getCityMasterList() {
		return cityMasterList;
	}

	public void setCityMasterList(List<CityMaster> cityMasterList) {
		this.cityMasterList = cityMasterList;
	}

	public String getOperatorImgUrl() {
		return operatorImgUrl;
	}

	public void setOperatorImgUrl(String operatorImgUrl) {
		this.operatorImgUrl = operatorImgUrl;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

    public Boolean getCardStorageOptIn() {
        return cardStorageOptIn;
    }

    public void setCardStorageOptIn(Boolean cardStorageOptIn) {
        this.cardStorageOptIn = cardStorageOptIn;
    }

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public Boolean getContactDetailsReq() {
		return contactDetailsReq;
	}

	public void setContactDetailsReq(Boolean contactDetailsReq) {
		this.contactDetailsReq = contactDetailsReq;
	}
}
