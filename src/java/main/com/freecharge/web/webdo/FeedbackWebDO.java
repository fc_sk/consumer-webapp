package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class FeedbackWebDO extends BaseWebDO{
	private String email;
	private String topicArea;
	private String subject;
	private String message;
	private String status;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTopicArea() {
		return topicArea;
	}
	public void setTopicArea(String topicArea) {
		this.topicArea = topicArea;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
