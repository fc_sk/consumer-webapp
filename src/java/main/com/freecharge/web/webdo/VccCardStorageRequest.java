package com.freecharge.web.webdo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VccCardStorageRequest {
    private String applicationSerNo;

    private String userId;

    private String checksum;

    public String getApplicationSerNo() {
        return applicationSerNo;
    }

    public void setApplicationSerNo(String applicationSerNo) {
        this.applicationSerNo = applicationSerNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
}
