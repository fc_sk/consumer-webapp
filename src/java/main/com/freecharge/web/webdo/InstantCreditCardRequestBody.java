package com.freecharge.web.webdo;

public class InstantCreditCardRequestBody {
    private String      applicationSerNo;

    public String getApplicationSerNo() {
        return applicationSerNo;
    }

    public void setApplicationSerNo(String applicationSerNo) {
        this.applicationSerNo = applicationSerNo;
    }
}
