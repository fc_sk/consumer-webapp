package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class MyRechargesWebDO extends BaseWebDO{
	private List<Map<String,String>> rechargeHistoryList;
	private String email;
	private Integer userid;
	public List<Map<String, String>> getRechargeHistoryList() {
		return rechargeHistoryList;
	}
	public void setRechargeHistoryList(List<Map<String, String>> rechargeHistoryList) {
		this.rechargeHistoryList = rechargeHistoryList;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
}
