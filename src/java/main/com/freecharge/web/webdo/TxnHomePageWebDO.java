package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 17, 2012
 * Time: 5:57:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class TxnHomePageWebDO extends BaseWebDO{
    private Integer txnHomePageId;
    private Integer billTxnHomePageId;
    private String serviceNumber;
    private String operatorName;
    private String circleName;
    private Float amount;
    private String productType;
    private String sessionId;
    private String lookupId;
    private String serviceStatus;
    private List<Map<String , Object>> txnCrossSellList;
    private UserContactDetail contactDetails;
    private String defaultProfile;
    private String festiveMessage;

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public Integer getBillTxnHomePageId() {
		return billTxnHomePageId;
	}

	public void setBillTxnHomePageId(Integer billTxnHomePageId) {
		this.billTxnHomePageId = billTxnHomePageId;
	}

	public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public List<Map<String , Object>> getTxnCrossSellList() {
        return txnCrossSellList;
    }

    public void setTxnCrossSellList(List<Map<String , Object>> txnCrossSellList) {
        this.txnCrossSellList = txnCrossSellList;
    }

    public UserContactDetail getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(UserContactDetail contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getFestiveMessage() {
        return festiveMessage;
    }

    public void setFestiveMessage(String f) {
        this.festiveMessage = f;
    }
}
