package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class PaymentTypeOptionWebDO extends BaseWebDO {

    private String displayName;
    private String displayImg;
    private String imgUrl;
    private String code;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayImg() {
        return displayImg;
    }

    public void setDisplayImg(String displayImg) {
        this.displayImg = displayImg;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
