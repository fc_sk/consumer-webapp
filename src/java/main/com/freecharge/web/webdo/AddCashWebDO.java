package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class AddCashWebDO extends BaseWebDO {
    private String addCashAmount;
    
    public String getAddCashAmount() {
        return addCashAmount;
    }
    
    public void setAddCashAmount(String addCashAmount) {
        this.addCashAmount = addCashAmount;
    }
}
