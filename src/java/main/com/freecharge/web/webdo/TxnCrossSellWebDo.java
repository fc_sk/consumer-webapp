package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: May 16, 2012
 * Time: 11:18:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class TxnCrossSellWebDo extends BaseWebDO{
    private Integer homePageId;
    private Integer couponId;
    private Integer crossSellId;
    private String itemIdArray;
    private String type;
    private String proType;

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Integer getHomePageId() {
        return homePageId;
    }

    public void setHomePageId(Integer homePageId) {
        this.homePageId = homePageId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getCrossSellId() {
        return crossSellId;
    }

    public void setCrossSellId(Integer crossSellId) {
        this.crossSellId = crossSellId;
    }

    public String getItemIdArray() {
        return itemIdArray;
    }

    public void setItemIdArray(String itemIdArray) {
        this.itemIdArray = itemIdArray;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
