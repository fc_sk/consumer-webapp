package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 11, 2012
 * Time: 3:03:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordWebDo extends BaseWebDO{
    private boolean userValid;
    private String email;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getUserValid() {
        return userValid;
    }

    public void setUserValid(boolean userValid) {
        this.userValid = userValid;
    }
}
