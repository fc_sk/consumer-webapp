package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class CartItemsWebDO extends BaseWebDO {

    private Integer itemId;
    private String itemTableName;
    private Integer productId;
    private String displayLabel;
    private Double price;
    private String imgUrl;
    private String productType;
    private String merchantName;
    private Integer quantity;
    
    
    
    public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemTableName() {
        return itemTableName;
    }

    public void setItemTableName(String itemTableName) {
        this.itemTableName = itemTableName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
}
