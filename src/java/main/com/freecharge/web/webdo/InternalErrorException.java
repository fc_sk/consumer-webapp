package com.freecharge.web.webdo;

import com.freecharge.web.webdo.ExceptionErrorCode;
import com.freecharge.web.webdo.FeeEngineException;

public class InternalErrorException extends FeeEngineException{
	

	public InternalErrorException(String message) {
		super(message);
	}

	public InternalErrorException(Throwable e) {
		super(e);
	}

	public InternalErrorException(String message, Throwable e) {
		super(message, e);
	}

	{
		this.setErrorCode(ExceptionErrorCode.INTERNAL_ERROR);
	}

}
