package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class CouponInventoryWebDO extends BaseWebDO {

	private static final long serialVersionUID = 1L;

	private String orderId;
	private String couponInventoryDetails;
	private List couponInventoryList;
	private Map<String, List<String>> couponCodes;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public List getCouponInventoryList() {
		return couponInventoryList;
	}
	public void setCouponInventoryList(List couponInventoryList) {
		this.couponInventoryList = couponInventoryList;
	}
	public Map<String, List<String>> getCouponCodes() {
		return couponCodes;
	}
	public void setCouponCodes(Map<String, List<String>> couponCodes) {
		this.couponCodes = couponCodes;
	}
	public String getCouponInventoryDetails() {
		return couponInventoryDetails;
	}
	public void setCouponInventoryDetails(String couponInventoryDetails) {
		this.couponInventoryDetails = couponInventoryDetails;
	}
	
}
