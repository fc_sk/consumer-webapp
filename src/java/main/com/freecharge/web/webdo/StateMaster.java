package com.freecharge.web.webdo;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 20, 2012
 * Time: 3:02:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class StateMaster {

    private Integer stateMasterId;
	private String stateName;
	private Boolean isActive;
	private Integer fkZoneMasterId;
	private Integer fkCountryMasterId;

    public Integer getStateMasterId() {
        return stateMasterId;
    }

    public void setStateMasterId(Integer stateMasterId) {
        this.stateMasterId = stateMasterId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getFkZoneMasterId() {
        return fkZoneMasterId;
    }

    public void setFkZoneMasterId(Integer fkZoneMasterId) {
        this.fkZoneMasterId = fkZoneMasterId;
    }

    public Integer getFkCountryMasterId() {
        return fkCountryMasterId;
    }

    public void setFkCountryMasterId(Integer fkCountryMasterId) {
        this.fkCountryMasterId = fkCountryMasterId;
    }
}
