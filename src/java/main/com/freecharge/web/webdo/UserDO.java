package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import org.springmodules.validation.bean.conf.loader.annotation.handler.Length;
import org.springmodules.validation.bean.conf.loader.annotation.handler.MaxLength;
import org.springmodules.validation.bean.conf.loader.annotation.handler.MinLength;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created with IntelliJ IDEA.
 * User: abc
 * Date: 5/3/12
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserDO extends BaseWebDO {

    private Integer userId;
    private Integer type;
    @NotBlank(message = "Email can not be blank")
    private String email;
    @NotBlank(message = "password can not be blank")
    @MinLength(value=6,message="Password field should contain Minimum 6 characters")
    @MaxLength(value=20,message="Password field should contain Maximum 20 characters")
    private String password;
    @NotBlank(message = "Mobile Number can not be blank")
    @MaxLength(value=10,message="Mobile Number field should contain Min and Max 10 numbers")
    @MinLength(value=10,message="Mobile Number field should contain Min and Max 10 numbers")
    private String mobileNo;
    private String dob;
    private Boolean isActive;
    private Boolean isLoggedin;
    private String morf;
    private String registrationStatus;
    private String nickAlias;
    @NotBlank(message = "Address can not be blank")
    private String address1;
    private String landmark;
    private String city;
    @NotBlank(message ="City can not be blank")
    private Integer cityId;
    @NotBlank(message ="Postal can not be blank")
    @Length(min = 6, max = 6, message = "Postal code should be 6 digits")
    private String postalCode;
    private String serviceNumber;
    private String state;
    private Integer stateId;
    private String pincode;
    private UserContactDetail userContactDetails;
    private Integer txnHomePageId;
    private Integer countryId;
    private String country;
    @NotBlank(message = "First Name can not be blank")
    private String firstName;
    private String lastName;
    @NotBlank(message = "Confirm Password can not be blank")
    private String confirmPassword;
    @NotBlank(message = "Title can not be blank")
    private String title;
    private String comboAddr;
    private Integer txnfulfilmentId;
    private String terms;
    private String street;
    private String area;
    private List<CrosssellWebDo> crosssellWebDo;
    private List<PaymentTypeMasterWebDO> paymentTypes;
    private Integer userRechargeContactId;
    private String defaultProfile;
    private String rememberme;
    private boolean registrationStatusFlag;
    @NotBlank(message = "affiliate_unique_id can not be blank.")
    private String affiliate_unique_id;
    private Integer userProfileId;
    private Map<String,Object> sessionData;
    private List<CityMaster> cityMasterList;
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean getLoggedin() {
        return isLoggedin;
    }

    public void setLoggedin(Boolean loggedin) {
        isLoggedin = loggedin;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public Boolean getIsLoggedin() {
        return this.isLoggedin;
    }

    public void setIsLoggedin(Boolean isLoggedin) {
        this.isLoggedin = isLoggedin;
    }

    public String getMorf() {
        return this.morf;
    }

    public void setMorf(String morf) {
        this.morf = morf;
    }

    public String getRegistrationStatus() {
        return this.registrationStatus;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public String getNickAlias() {
		return this.nickAlias;
	}

	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


    public Map<String, Object> getSessionData() {
        return sessionData;
    }

    public void setSessionData(Map<String, Object> sessionData) {
        this.sessionData = sessionData;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    private List<StateMaster> stateMaster;
    private List<CountryMaster> countryMaster;

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }

    public List<CountryMaster> getCountryMaster() {
        return countryMaster;
    }

    public void setCountryMaster(List<CountryMaster> countryMaster) {
        this.countryMaster = countryMaster;
    }

    public String getComboAddr() {
        return comboAddr;
    }

    public void setComboAddr(String comboAddr) {
        this.comboAddr = comboAddr;
    }

    public Integer getTxnfulfilmentId() {
        return txnfulfilmentId;
    }

    public void setTxnfulfilmentId(Integer txnfulfilmentId) {
        this.txnfulfilmentId = txnfulfilmentId;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<CrosssellWebDo> getCrosssellWebDo() {
        return crosssellWebDo;
    }

    public void setCrosssellWebDo(List<CrosssellWebDo> crosssellWebDo) {
        this.crosssellWebDo = crosssellWebDo;
    }

    public List<PaymentTypeMasterWebDO> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentTypeMasterWebDO> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public Integer getUserRechargeContactId() {
        return userRechargeContactId;
    }

    public void setUserRechargeContactId(Integer userRechargeContactId) {
        this.userRechargeContactId = userRechargeContactId;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public UserContactDetail getUserContactDetails() {
        return userContactDetails;
    }

    public void setUserContactDetails(UserContactDetail userContactDetails) {
        this.userContactDetails = userContactDetails;
    }

	public String getRememberme() {
		return rememberme;
	}

	public void setRememberme(String rememberme) {
		this.rememberme = rememberme;
	}

	public boolean isRegistrationStatusFlag() {
		return registrationStatusFlag;
	}

	public void setRegistrationStatusFlag(boolean registrationStatusFlag) {
		this.registrationStatusFlag = registrationStatusFlag;
	}

	public String getAffiliate_unique_id() {
		return affiliate_unique_id;
	}

	public void setAffiliate_unique_id(String affiliate_unique_id) {
		this.affiliate_unique_id = affiliate_unique_id;
	}

	public Integer getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}

	public List<CityMaster> getCityMasterList() {
		return cityMasterList;
	}

	public void setCityMasterList(List<CityMaster> cityMasterList) {
		this.cityMasterList = cityMasterList;
	}
}
