package com.freecharge.web.webdo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonPropertyOrder;


@JsonPropertyOrder({ "merchantId", "txnAmount", "transactionTime" })
public class CalculateChargeRequest implements Serializable{
	

	private String merchantId;

	private BigDecimal txnAmount;



	public String getMerchantId() {
		return merchantId;
	}


	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}


	public Date getTransactionTime() {
		return transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public BigDecimal getTxnAmount() {
		return txnAmount;
	}


	

	public void setTxnAmount(BigDecimal amount) {
		if (amount != null) {
			this.txnAmount = amount.setScale(2, RoundingMode.HALF_DOWN);
		} else {
			this.txnAmount = amount;
		}
	}
	
	@Override
	public String toString() {
		return "CalculateChargeRequest [  merchantId=" + merchantId + ", txnAmount=" + txnAmount + ", transactionTime="
				+ transactionTime + "]";
	}
	
	private Date transactionTime = new Date();

}
