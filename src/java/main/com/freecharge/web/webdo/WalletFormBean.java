package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: jitender
 * Date: Jan 25, 2012
 * Time: 4:15:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class WalletFormBean extends BaseWebDO {
    private List<Object> myWalletList;
    private List<Object> myOrderList;
    private Object[] profileDetails;

    public WalletFormBean() {
    }

    public WalletFormBean(List<Object> myWalletList, List<Object> myOrderList, Object[] profileDetails) {
        this.myWalletList = myWalletList;
        this.myOrderList = myOrderList;
        this.profileDetails = profileDetails;
    }

    public List<Object> getMyWalletList() {
        return myWalletList;
    }

    public void setMyWalletList(List<Object> myWalletList) {
        this.myWalletList = myWalletList;
    }

    public List<Object> getMyOrderList() {
        return myOrderList;
    }

    public void setMyOrderList(List<Object> myOrderList) {
        this.myOrderList = myOrderList;
    }

    public Object[] getProfileDetails() {
        return profileDetails;
    }

    public void setProfileDetails(Object[] profileDetails) {
        this.profileDetails = profileDetails;
    }
}
