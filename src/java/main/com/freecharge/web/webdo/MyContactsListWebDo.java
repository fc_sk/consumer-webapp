package com.freecharge.web.webdo;

import java.util.List;

import org.springmodules.validation.bean.conf.loader.annotation.handler.NotNull;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class MyContactsListWebDo extends BaseWebDO{

	private String email;
	@NotNull(message ="UserId cannot be blank")	
	private Integer userid;
	private List contactslist;
	private List operatorlist;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List getContactslist() {
		return contactslist;
	}
	public void setContactslist(List contactslist) {
		this.contactslist = contactslist;
	}
	public List getOperatorlist() {
		return operatorlist;
	}
	public void setOperatorlist(List operatorlist) {
		this.operatorlist = operatorlist;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	
}
