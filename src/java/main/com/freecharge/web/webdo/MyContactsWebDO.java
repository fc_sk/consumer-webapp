package com.freecharge.web.webdo;

import java.util.Date;
import java.util.List;

import org.springmodules.validation.bean.conf.loader.annotation.handler.Length;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.framework.basedo.BaseWebDO;

public class MyContactsWebDO extends BaseWebDO{
	
	private String contactid;
	private Integer userid;
	private String email;
	@NotBlank(message = "Not blank")
	private String name;
	@NotBlank(message = "Not blank")
	@Length(min=10,max=10)
	private String serviceno;
	private Integer product;
	@NotBlank(message = "Not blank")
	private String operator;
	private String operatorname;
	//@NotBlank(message = "Not blank")
	private String circle;
	private boolean updatestatus=false;
	private List<OperatorMaster> operatorlist;
	private List<ProductMaster> productlist;
	private Date lastRechargeDate;
	
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) { 
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getServiceno() {
		return serviceno;
	}
	public void setServiceno(String serviceno) {
		this.serviceno = serviceno;
	}
	public Integer getProduct() {
		return product;
	}
	public void setProduct(Integer product) {
		this.product = product;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public List<OperatorMaster> getOperatorlist() {
		return operatorlist;
	}
	public void setOperatorlist(List<OperatorMaster> operatorlist) {
		this.operatorlist = operatorlist;
	}
	public List<ProductMaster> getProductlist() {
		return productlist;
	}
	public void setProductlist(List<ProductMaster> productlist) {
		this.productlist = productlist;
	}
	public String getOperatorname() {
		return operatorname;
	}
	public void setOperatorname(String operatorname) {
		this.operatorname = operatorname;
	}
	public String getContactid() {
		return contactid;
	}
	public void setContactid(String contactid) {
		this.contactid = contactid;
	}
	
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public boolean getUpdatestatus() {
		return updatestatus;
	}
	public void setUpdatestatus(boolean updatestatus) {
		this.updatestatus = updatestatus;
	}
	public Date getLastRechargeDate() {
		return lastRechargeDate;
	}
	public void setLastRechargeDate(Date lastRechargeDate) {
		this.lastRechargeDate = lastRechargeDate;
	}
	
	
}
