package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 9, 2012
 * Time: 5:26:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class CartCrosssellWebDO extends BaseWebDO{
    private String status;
    private Integer txnHomePageId;
    private Integer cartId;
    private Integer crosssellId;
    private Float crosssellAmount;
    private Integer productTypeId;
    private Integer txncrossellId;
    private Integer cartItemsId;
    private String title;
    private Double totalPrice;
    private WalletEligibility walletEligibility;
    private PaymentBreakup paymentBreakup;
    
    public PaymentBreakup getPaymentBreakup() {
        return paymentBreakup;
    }
    
    public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
        this.paymentBreakup = paymentBreakup;
    }
    
    public WalletEligibility getWalletEligibility() {
        return walletEligibility;
    }
    
    public void setWalletEligibility(WalletEligibility walletEligibility) {
        this.walletEligibility = walletEligibility;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public Integer getCrosssellId() {
        return crosssellId;
    }

    public void setCrosssellId(Integer crosssellId) {
        this.crosssellId = crosssellId;
    }

    public Float getCrosssellAmount() {
        return crosssellAmount;
    }

    public void setCrosssellAmount(Float crosssellAmount) {
        this.crosssellAmount = crosssellAmount;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getTxncrossellId() {
        return txncrossellId;
    }

    public void setTxncrossellId(Integer txncrossellId) {
        this.txncrossellId = txncrossellId;
    }

    public Integer getCartItemsId() {
        return cartItemsId;
    }

    public void setCartItemsId(Integer cartItemsId) {
        this.cartItemsId = cartItemsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
