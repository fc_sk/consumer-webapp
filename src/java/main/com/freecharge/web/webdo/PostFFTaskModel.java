package com.freecharge.web.webdo;

import java.io.Serializable;
import java.util.Date;

import com.freecharge.post.fulfillment.state.PostFulfillmentTaskState;

public class PostFFTaskModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String orderId;
	private PostFulfillmentTaskState state;
	private int retryCount;
	private String rawRequest;
	private String reason;
	private Date created;
	private Date updated;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public PostFulfillmentTaskState getState() {
		return state;
	}

	public void setState(PostFulfillmentTaskState state) {
		this.state = state;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public String getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostFFTaskModel [id=").append(id).append(", orderId=").append(orderId).append(", state=")
				.append(state).append(", retryCount=").append(retryCount).append(", rawRequest=").append(rawRequest)
				.append(", reason=").append(reason).append(", created=").append(created).append(", updated=")
				.append(updated).append("]");
		return builder.toString();
	}

	
	
	
}
