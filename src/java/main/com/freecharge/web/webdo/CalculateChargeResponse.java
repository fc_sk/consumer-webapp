package com.freecharge.web.webdo;


public class CalculateChargeResponse {
	
	private FeeCharge feeCharge;

	public FeeCharge getFeeCharge() {
		return feeCharge;
	}

	public void setFeeCharge(FeeCharge feeCharge) {
		this.feeCharge = feeCharge;
	}

}
