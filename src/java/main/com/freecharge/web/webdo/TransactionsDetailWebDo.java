package com.freecharge.web.webdo;

import java.io.Serializable;
import java.util.List;

import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.common.businessdo.AggregatorBusinessDo;
import com.freecharge.common.businessdo.BankServiceBusinessDo;
import com.freecharge.common.businessdo.CircleBusinessDo;
import com.freecharge.common.businessdo.ModeOfPaymentBusinessDo;
import com.freecharge.common.businessdo.OperatorBusinessDo;
import com.freecharge.common.businessdo.PGTransactionRequestBusinessDo;
import com.freecharge.common.businessdo.PaymentGatewayBusinessDo;
import com.freecharge.common.businessdo.TransactionDataBusinessDo;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Feb 8, 2012
 * Time: 10:01:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionsDetailWebDo extends BaseWebDO implements Serializable{

    //personal details
    
    private OperatorBusinessDo operator;
    private CircleBusinessDo circle;
    private List<CouponVO> couponSet;

    private TransactionDataBusinessDo transactionDataBusinessDo;
	private ModeOfPaymentBusinessDo modeOfPayment;
	private BankServiceBusinessDo bankService;
	private PGTransactionRequestBusinessDo pgTransRqst;
	private String checkSum;

    
   // private String operatorServiceCode;
    private AggregatorBusinessDo aggregator;
    private String orderId;
    private PaymentGatewayBusinessDo paymentGateway;
   // private FcRechargeTransaction fcRechargeTransaction;
  //  private String rtRspnAgrCode;
  //  private String rtRspnAgrTransId;
 //   private String rtRspnMessage;
 //   private String rtRspnOprTransId;
 //   private Integer couponTimes;

    public OperatorBusinessDo getOperator() {
        return operator;
    }

    public void setOperator(OperatorBusinessDo operator) {
        this.operator = operator;
    }

    public CircleBusinessDo getCircle() {
        return circle;
    }

    public void setCircle(CircleBusinessDo circle) {
        this.circle = circle;
    }

    public List<CouponVO> getCouponSet() {
        return couponSet;
    }

    public void setCouponSet(List<CouponVO> couponSet) {
        this.couponSet = couponSet;
    }

    public TransactionDataBusinessDo getTransactionDataBusinessDo() {
        return transactionDataBusinessDo;
    }

    public void setTransactionDataBusinessDo(TransactionDataBusinessDo transactionDataBusinessDo) {
        this.transactionDataBusinessDo = transactionDataBusinessDo;
    }

    public ModeOfPaymentBusinessDo getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(ModeOfPaymentBusinessDo modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public BankServiceBusinessDo getBankService() {
        return bankService;
    }

    public void setBankService(BankServiceBusinessDo bankService) {
        this.bankService = bankService;
    }

    public PGTransactionRequestBusinessDo getPgTransRqst() {
        return pgTransRqst;
    }

    public void setPgTransRqst(PGTransactionRequestBusinessDo pgTransRqst) {
        this.pgTransRqst = pgTransRqst;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public AggregatorBusinessDo getAggregator() {
        return aggregator;
    }

    public void setAggregator(AggregatorBusinessDo aggregator) {
        this.aggregator = aggregator;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PaymentGatewayBusinessDo getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGatewayBusinessDo paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}