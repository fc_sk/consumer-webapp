package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA. User: Toshiba Date: May 11, 2012 Time: 11:27:25 AM To change this template use File | Settings | File Templates.
 */
public class RechargeInitiateWebDo extends BaseWebDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6776974057281238863L;

	private String circleCode;
    private Integer affiliateid;
    private String orderId;
    private String productType;
    private String operator;
    private String mobileNo;
    private double amount;
    private String rechargeResponse;
    private String paymentResponse;
    private String lookupID;
    private String email;
    private String deliveryName;
    private String deliveryAdd;
    private String deliveryCity;
    private String deliveryState;
    private String deliveryCountry;
    private String deliveryPincode;
    private List<Map<String, Object>> operatorList;
    private String paymentPortalResultDesc;
    private String paymentPortalResultResponseCode;
    private String paymentPortalAmount;
    private String paymentPortalTransId;
    private String paymentPortalPaymentGateway;
    private String paymentPortalMode;
    private String paymentPortalMerchantId;
    private Integer transactionStatus;
    private String refundTo;
    private Boolean invoice = false;
    private String txnType;
	
	public String getRechargeResponse() {
		return rechargeResponse;
	}

	public void setRechargeResponse(String rechargeResponse) {
		this.rechargeResponse = rechargeResponse;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public String getPaymentResponse() {
		return paymentResponse;
	}

	public void setPaymentResponse(String paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCircleCode() {
		return circleCode;
	}

	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}

	public Integer getAffiliateid() {
		return affiliateid;
	}

	public void setAffiliateid(Integer affiliateid) {
		this.affiliateid = affiliateid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getLookupID() {
		return lookupID;
	}

	public void setLookupID(String lookupID) {
		this.lookupID = lookupID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getDeliveryAdd() {
		return deliveryAdd;
	}

	public void setDeliveryAdd(String deliveryAdd) {
		this.deliveryAdd = deliveryAdd;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryState() {
		return deliveryState;
	}

	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}

	public String getDeliveryPincode() {
		return deliveryPincode;
	}

	public void setDeliveryPincode(String deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}

	public List<Map<String, Object>> getOperatorList() {
		return operatorList;
	}

	public void setOperatorList(List<Map<String, Object>> operatorList) {
		this.operatorList = operatorList;
	}

	public String getPaymentPortalResultDesc() {
		return paymentPortalResultDesc;
	}

	public void setPaymentPortalResultDesc(String paymentPortalResultDesc) {
		this.paymentPortalResultDesc = paymentPortalResultDesc;
	}

	public String getPaymentPortalResultResponseCode() {
		return paymentPortalResultResponseCode;
	}

	public void setPaymentPortalResultResponseCode(String paymentPortalResultResponseCode) {
		this.paymentPortalResultResponseCode = paymentPortalResultResponseCode;
	}

	public String getPaymentPortalAmount() {
		return paymentPortalAmount;
	}

	public void setPaymentPortalAmount(String paymentPortalAmount) {
		this.paymentPortalAmount = paymentPortalAmount;
	}

	public String getPaymentPortalTransId() {
		return paymentPortalTransId;
	}

	public void setPaymentPortalTransId(String paymentPortalTransId) {
		this.paymentPortalTransId = paymentPortalTransId;
	}

	public String getPaymentPortalPaymentGateway() {
		return paymentPortalPaymentGateway;
	}

	public void setPaymentPortalPaymentGateway(String paymentPortalPaymentGateway) {
		this.paymentPortalPaymentGateway = paymentPortalPaymentGateway;
	}

	public String getPaymentPortalMode() {
		return paymentPortalMode;
	}

	public void setPaymentPortalMode(String paymentPortalMode) {
		this.paymentPortalMode = paymentPortalMode;
	}

	public String getPaymentPortalMerchantId() {
		return paymentPortalMerchantId;
	}

	public void setPaymentPortalMerchantId(String paymentPortalMerchantId) {
		this.paymentPortalMerchantId = paymentPortalMerchantId;
	}

    public Integer getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getRefundTo() {
        return refundTo;
    }

    public void setRefundTo(String refundTo) {
        this.refundTo = refundTo;
    }

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public Boolean getInvoice() {
		return invoice;
	}

	public void setInvoice(Boolean invoice) {
		this.invoice = invoice;
	}

}
