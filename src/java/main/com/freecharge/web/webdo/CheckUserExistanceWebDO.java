package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 12, 2012
 * Time: 12:27:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckUserExistanceWebDO extends BaseWebDO{
    private String email;
    private String statusMsg;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
}
