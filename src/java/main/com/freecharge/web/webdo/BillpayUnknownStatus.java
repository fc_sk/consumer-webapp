package com.freecharge.web.webdo;

import java.io.Serializable;

public class BillpayUnknownStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String billId;
	private String authenticator;
	private Double amount;
	private String txnDate;
	private Boolean isActive;
	private String status;
	private String orderId;
	private String agTxnId;

	
	
	public String getAgTxnId() {
		return agTxnId;
	}
	public void setAgTxnId(String agTxnId) {
		this.agTxnId = agTxnId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBillId() {
		return billId;
	}
	public void setBillId(String billId) {
		this.billId = billId;
	}
	public String getAuthenticator() {
		return authenticator;
	}
	public void setAuthenticator(String authenticator) {
		this.authenticator = authenticator;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillpayUnknownStatus [id=").append(id).append(", billId=").append(billId)
				.append(", authenticator=").append(authenticator).append(", amount=").append(amount)
				.append(", txnDate=").append(txnDate).append(", isActive=").append(isActive).append(", status=")
				.append(status).append(", orderId=").append(orderId).append(", agTxnId=").append(agTxnId).append("]");
		return builder.toString();
	}
	
	
	
}
