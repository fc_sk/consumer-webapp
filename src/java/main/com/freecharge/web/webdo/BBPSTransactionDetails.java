package com.freecharge.web.webdo;

import java.io.Serializable;
import java.util.Date;

public class BBPSTransactionDetails implements Serializable{

	private Long id;
	private String customerName;
	private String customerAccountNumber;
	private String lookupId;
	private String billDueDate;
	private String messageDetails;
	private String bbpsReferenceNumber;
	private String bbpsMetaData;
	private String aggregatorTxnId;
	private boolean isBbpsActive;
	private String billPeriod;

	
	

	public String getBillPeriod() {
		return billPeriod;
	}




	public void setBillPeriod(String billPeriod) {
		this.billPeriod = billPeriod;
	}




	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}




	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}




	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}




	public String getLookupId() {
		return lookupId;
	}




	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}




	public String getBillDueDate() {
		return billDueDate;
	}




	public void setBillDueDate(String billDueDate) {
		this.billDueDate = billDueDate;
	}




	public String getMessageDetails() {
		return messageDetails;
	}




	public void setMessageDetails(String messageDetails) {
		this.messageDetails = messageDetails;
	}




	public String getBbpsReferenceNumber() {
		return bbpsReferenceNumber;
	}




	public void setBbpsReferenceNumber(String bbpsReferenceNumber) {
		this.bbpsReferenceNumber = bbpsReferenceNumber;
	}




	public String getBbpsMetaData() {
		return bbpsMetaData;
	}




	public void setBbpsMetaData(String bbpsMetaData) {
		this.bbpsMetaData = bbpsMetaData;
	}




	public String getAggregatorTxnId() {
		return aggregatorTxnId;
	}




	public void setAggregatorTxnId(String aggregatorTxnId) {
		this.aggregatorTxnId = aggregatorTxnId;
	}




	public boolean getIsBbpsActive() {
		return isBbpsActive;
	}




	public void setIsBbpsActive(boolean isBbpsActive) {
		this.isBbpsActive = isBbpsActive;
	}




	@Override
	public String toString() {
		return "BBPSTransactionDetails [id=" + id + ", customerName=" + customerName + ", customerAccountNumber="
				+ customerAccountNumber + ", lookupId=" + lookupId + ", billDueDate=" + billDueDate
				+ ", messageDetails=" + messageDetails + ", bbpsReferenceNumber=" + bbpsReferenceNumber
				+ ", bbpsMetaData=" + bbpsMetaData + ", aggregatorTxnId=" + aggregatorTxnId + ", isBbpsActive="
				+ isBbpsActive + ", billPeriod=" + billPeriod + "]";
	}
	
}
