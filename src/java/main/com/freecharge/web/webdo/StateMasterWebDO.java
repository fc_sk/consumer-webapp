package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 19, 2012
 * Time: 5:00:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class StateMasterWebDO extends BaseWebDO{
    private List<StateMaster> stateMaster;

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }
}
