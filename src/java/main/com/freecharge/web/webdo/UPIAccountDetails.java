package com.freecharge.web.webdo;

public class UPIAccountDetails {
    
    private String accRefNumber;
    private String aeba;
    private String dLength;
    private String dType;
    private String ifsc;
    private String maskedAccnumber;
    private String name;
    private String status;
    private String type;
    private String vpa;
    public String getAccRefNumber() {
        return this.accRefNumber;
    }
    public void setAccRefNumber(String accRefNumber) {
        this.accRefNumber = accRefNumber;
    }
    public String getAeba() {
        return this.aeba;
    }
    public void setAeba(String aeba) {
        this.aeba = aeba;
    }
    public String getdLength() {
        return this.dLength;
    }
    public void setdLength(String dLength) {
        this.dLength = dLength;
    }
    public String getdType() {
        return this.dType;
    }
    public void setdType(String dType) {
        this.dType = dType;
    }
    public String getIfsc() {
        return this.ifsc;
    }
    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }
    public String getMaskedAccnumber() {
        return this.maskedAccnumber;
    }
    public void setMaskedAccnumber(String maskedAccnumber) {
        this.maskedAccnumber = maskedAccnumber;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getVpa() {
        return this.vpa;
    }
    public void setVpa(String vpa) {
        this.vpa = vpa;
    }

}
