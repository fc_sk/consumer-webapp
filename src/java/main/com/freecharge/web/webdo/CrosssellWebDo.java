package com.freecharge.web.webdo;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: May 11, 2012
 * Time: 11:37:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class CrosssellWebDo extends BaseWebDO{

	private String category;
    private Integer price;
	private String order;
	private String imgurl;
	private String tnc;
	private String description;
	private String headDescription;
    private String title;
    private boolean refundable;
    private String primaryproduct;
    private Integer primaryproductamount;
    private String othercondition;
    private Integer maxuses;
    private Integer txncrossellId;
    private Integer cartItemsId;
    private String className;
    private String template;
    private String mailTnc;
    private String mailSubject;
    private String mailImgUrl;

    
    
    public String getMailTnc() {
		return mailTnc;
	}

	public void setMailTnc(String mailTnc) {
		this.mailTnc = mailTnc;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailImgUrl() {
		return mailImgUrl;
	}

	public void setMailImgUrl(String mailImgUrl) {
		this.mailImgUrl = mailImgUrl;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getHeadDescription() {
		return headDescription;
	}

	public void setHeadDescription(String headDescription) {
		this.headDescription = headDescription;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPrimaryproduct() {
		return primaryproduct;
	}

	public void setPrimaryproduct(String primaryproduct) {
		this.primaryproduct = primaryproduct;
	}

	public Integer getPrimaryproductamount() {
		return primaryproductamount;
	}

	public void setPrimaryproductamount(Integer primaryproductamount) {
		this.primaryproductamount = primaryproductamount;
	}

	public String getOthercondition() {
		return othercondition;
	}

	public void setOthercondition(String othercondition) {
		this.othercondition = othercondition;
	}

	public Integer getMaxuses() {
		return maxuses;
	}

	public void setMaxuses(Integer maxuses) {
		this.maxuses = maxuses;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(boolean refundable) {
        this.refundable = refundable;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getTnc() {
        return tnc;
    }

    public void setTnc(String tnc) {
        this.tnc = tnc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public Integer getTxncrossellId() {
		return txncrossellId;
	}

	public void setTxncrossellId(Integer txncrossellId) {
		this.txncrossellId = txncrossellId;
	}

	public Integer getCartItemsId() {
		return cartItemsId;
	}

	public void setCartItemsId(Integer cartItemsId) {
		this.cartItemsId = cartItemsId;
	}
}