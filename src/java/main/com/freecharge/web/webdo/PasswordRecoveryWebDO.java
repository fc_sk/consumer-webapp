package com.freecharge.web.webdo;

import org.springmodules.validation.bean.conf.loader.annotation.handler.MinLength;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.mobile.validator.annotation.PasswordResetInput;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2012
 * Time: 11:43:09 AM
 * To change this template use File | Settings | File Templates.
 */
@PasswordResetInput
public class PasswordRecoveryWebDO extends BaseWebDO{
	@NotBlank(message = "password can not be blank")
	@MinLength(value=6,message="Password field should contain Minimum 6 characters")
	private String newPassword;
	@NotBlank(message = "Confirm password can not be blank")
	@MinLength(value=6,message="Confirm password field should contain 6 characters")
	private String confirmPassword;
    private String email;
    private String userId;
    private String status;
    private boolean userExist;
    private String encryptValue; 
    private String encryptEmail;

    public boolean getUserExist() {
        return userExist;
    }

    public void setUserExist(boolean userExist) {
        this.userExist = userExist;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getStatus() {
           return status;
    }

    public void setStatus(String status) {
           this.status = status;
       }

       public String getEmail() {
           return email;
       }

       public void setEmail(String email) {
           this.email = email;
       }

       public String getNewPassword() {
           return newPassword;
       }

       public void setNewPassword(String newPassword) {
           this.newPassword = newPassword;
       }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

	public String getEncryptValue() {
		return encryptValue;
	}

	public void setEncryptValue(String encryptValue) {
		this.encryptValue = encryptValue;
	}

	public String getEncryptEmail() {
		return encryptEmail;
	}

	public void setEncryptEmail(String encryptEmail) {
		this.encryptEmail = encryptEmail;
	}
}
