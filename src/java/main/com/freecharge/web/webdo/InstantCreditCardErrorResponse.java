package com.freecharge.web.webdo;

public class InstantCreditCardErrorResponse {
    private String      httpCode;

    private String      httpMessage;

    private String      moreInformation;

    private String      backendErrorCode;
}
