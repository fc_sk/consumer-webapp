package com.freecharge.web.webdo;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstantCreditCardRequest {

    @JsonProperty("FetchCardDetailsRequest")
    private FetchCardDetailsRequest fetchCardDetailsRequest;

    public FetchCardDetailsRequest getFetchCardDetailsRequest() {
        return fetchCardDetailsRequest;
    }

    public void setFetchCardDetailsRequest(FetchCardDetailsRequest fetchCardDetailsRequest) {
        this.fetchCardDetailsRequest = fetchCardDetailsRequest;
    }
}
