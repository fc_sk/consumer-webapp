package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA. User: user Date: May 5, 2012 Time: 2:17:29 PM To change this template use File | Settings | File Templates.
 */
public class PaymentOptionListWebDO extends BaseWebDO {
	private List<Map<String, Object>> paymentOptionsList;
	private String txnFulfilmentStatus;

	public List<PaymentTypeOptions> getPaymentTypeOptionsList() {
		return paymentTypeOptionsList;
	}

	public void setPaymentTypeOptionsList(List<PaymentTypeOptions> paymentTypeOptionsList) {
		this.paymentTypeOptionsList = paymentTypeOptionsList;
	}

	List<PaymentTypeOptions> paymentTypeOptionsList;

	public List<Map<String, Object>> getPaymentOptionsList() {
		return paymentOptionsList;
	}

	public void setPaymentOptionsList(List<Map<String, Object>> paymentOptionsList) {
		this.paymentOptionsList = paymentOptionsList;
	}

	public String getTxnFulfilmentStatus() {
		return txnFulfilmentStatus;
	}

	public void setTxnFulfilmentStatus(String txnFulfilmentStatus) {
		this.txnFulfilmentStatus = txnFulfilmentStatus;
	}
}
