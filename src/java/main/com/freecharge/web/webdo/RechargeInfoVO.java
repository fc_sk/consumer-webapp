package com.freecharge.web.webdo;

public class RechargeInfoVO {
	private String orderId;
	private String rechargedNo;
	private String rechargedDate;
	private Float rechargeAmount;
	private String operatorName;
	private String circleName;
	private Float serviceCharge;
	private Float totalAmount;
	private String city;
	private String street;
	private String pinCode;
	private String stateName;
	private String address;
	private String landMark;
	private boolean hasFreeFund = false;
	private Float freeFundAmount = new Float(0);
	private Integer crossSellCount;
	private Float crossSellAmount;
	private Double couponCharges;
	private boolean hasPaidCoupons;
	private boolean hasFeeDetails;
	private FeeDetails feeDetails;
	
	

	public Integer getCrossSellCount() {
		return crossSellCount;
	}

	public void setCrossSellCount(Integer crossSellCount) {
		this.crossSellCount = crossSellCount;
	}

	public Float getCrossSellAmount() {
		return crossSellAmount;
	}

	public void setCrossSellAmount(Float crossSellAmount) {
		this.crossSellAmount = crossSellAmount;
	}

	public Float getFreeFundAmount() {
		return freeFundAmount;
	}
	
	public void setFreeFundAmount(Float freeFundAmount) {
		this.freeFundAmount = freeFundAmount;
	}
	
	public boolean getHasFreeFund() {
		return hasFreeFund;
	}
	
	public void setHasFreeFund(boolean hasFreeFund) {
		this.hasFreeFund = hasFreeFund;
	}
	
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	private String area;
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getRechargedNo() {
		return rechargedNo;
	}
	public void setRechargedNo(String rechargedNo) {
		this.rechargedNo = rechargedNo;
	}
	public String getRechargedDate() {
		return rechargedDate;
	}
	public void setRechargedDate(String rechargedDate) {
		this.rechargedDate = rechargedDate;
	}
	public Float getRechargeAmount() {
		return rechargeAmount;
	}
	public void setRechargeAmount(Float rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}
	public Float getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(Float serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public Float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public Double getCouponCharges() {
		return couponCharges;
	}

	public void setCouponCharges(Double couponCharges) {
		this.couponCharges = couponCharges;
	}

	public boolean isHasPaidCoupons() {
		return hasPaidCoupons;
	}

	public void setHasPaidCoupons(boolean hasPaidCoupons) {
		this.hasPaidCoupons = hasPaidCoupons;
	}

	public boolean isHasFeeDetails() {
		return hasFeeDetails;
	}

	public void setHasFeeDetails(boolean hasFeeDetails) {
		this.hasFeeDetails = hasFeeDetails;
	}
	
	public FeeDetails getFeeDetails() {
		return feeDetails;
	}

	public void setFeeDetails(FeeDetails feeDetails) {
		this.feeDetails = feeDetails;
	}
}
