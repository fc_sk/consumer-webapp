package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.common.framework.basedo.BaseWebDO;

public class VouchersWebDO extends BaseWebDO {
	private Map<String, List<CouponVO>> voucherData;
	private List<Integer> voucherIdList;
	
	public void setVoucherData(Map<String, List<CouponVO>> voucherData) {
		this.voucherData = voucherData;
	}

	public Map<String, List<CouponVO>> getVoucherData() {
		return voucherData;
	}

	public List<Integer> getVoucherIdList() {
		return voucherIdList;
	}

	public void setVoucherIdList(List<Integer> voucherIdList) {
		this.voucherIdList = voucherIdList;
	}

}
