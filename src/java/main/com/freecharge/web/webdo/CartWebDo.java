package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: May 11, 2012
 * Time: 10:33:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class CartWebDo extends BaseWebDO {

    private Integer orderId;
    private Integer userId;
    private Boolean isOpen;
    private Double totalCartPrice;

    private List<CartItemsWebDO> itemsList;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public List<CartItemsWebDO> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<CartItemsWebDO> itemsList) {
        this.itemsList = itemsList;
    }

    @Override
    public String toString() {
        return "CartWebDo{" +
                "orderId=" + orderId +
                '}';
    }

    public Double getTotalCartPrice() {
        return totalCartPrice;
    }

    public void setTotalCartPrice(Double totalCartPrice) {
        this.totalCartPrice = totalCartPrice;
    }
}