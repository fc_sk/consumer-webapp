package com.freecharge.web.webdo;

import java.util.List;

import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 23, 2012
 * Time: 4:09:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddressWebDO extends BaseWebDO{
	@NotBlank(message = "Address can not be blank")
    private String address;
	/*@NotBlank(message = "City can not be blank")*/
    private String city;
	@NotBlank(message = "Postalcode can not be blank")
    private String postalCode;
	@NotBlank(message = "State can not be blank")
    private String state;
    private String country;
    private Integer stateId;
    private Integer countryId;
    private String status;
    private Integer txnHomePageId;
    private String comboAddr;
    private Integer userProfileId;
    private Integer userRechargeContactId;
    private Integer userId;
    private Integer txnfulfillmentId;
    private String defaultProfile;
    private String street;
    @NotBlank(message = "Area can not be blank")
    private String area;
    private String landmark;
    @NotBlank(message = "title can not be blank")
    private String title;
    @NotBlank(message = "Firstname can not be blank")
    private String firstName;
    private String isEdit;
    private Integer cityId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getComboAddr() {
        return comboAddr;
    }

    public void setComboAddr(String comboAddr) {
        this.comboAddr = comboAddr;
    }
    private List<StateMaster> stateMaster;
    private List<CountryMaster> countryMaster;

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }

    public List<CountryMaster> getCountryMaster() {
        return countryMaster;
    }

    public void setCountryMaster(List<CountryMaster> countryMaster) {
        this.countryMaster = countryMaster;
    }

    public Integer getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(Integer userProfileId) {
        this.userProfileId = userProfileId;
    }

    public Integer getUserRechargeContactId() {
        return userRechargeContactId;
    }

    public void setUserRechargeContactId(Integer userRechargeContactId) {
        this.userRechargeContactId = userRechargeContactId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTxnfulfillmentId() {
        return txnfulfillmentId;
    }

    public void setTxnfulfillmentId(Integer txnfulfillmentId) {
        this.txnfulfillmentId = txnfulfillmentId;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getCityId() {
		return cityId;
	}
}
