package com.freecharge.web.webdo;

import java.math.BigDecimal;

public class FeeDetails {
	
	private BigDecimal paymentHandlingCharge;
	private BigDecimal serviceTax;
	private BigDecimal swachhBharatCess;
	private BigDecimal krishiKalyanCess;
	
	public BigDecimal getPaymentHandlingCharge() {
		return paymentHandlingCharge;
	}
	public void setPaymentHandlingCharge(BigDecimal paymentHandlingCharge) {
		this.paymentHandlingCharge = paymentHandlingCharge;
	}
	public BigDecimal getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}
	public BigDecimal getSwachhBharatCess() {
		return swachhBharatCess;
	}
	public void setSwachhBharatCess(BigDecimal swachhBharatCess) {
		this.swachhBharatCess = swachhBharatCess;
	}
	public BigDecimal getKrishiKalyanCess() {
		return krishiKalyanCess;
	}
	public void setKrishiKalyanCess(BigDecimal krishiKalyanCess) {
		this.krishiKalyanCess = krishiKalyanCess;
	}
	@Override
	public String toString() {
		return "FeeDetails [paymentHandlingCharge=" + paymentHandlingCharge + ", serviceTax=" + serviceTax
				+ ", swachhBharatCess=" + swachhBharatCess + ", krishiKalyanCess=" + krishiKalyanCess + "]";
	}
}
