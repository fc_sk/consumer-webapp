package com.freecharge.web.webdo;

import org.codehaus.jackson.annotate.JsonProperty;

public class FetchCardDetailsResponse {

    @JsonProperty("SubHeader")
    private InstantCreditCardHeader subHeader;

    @JsonProperty("FetchCardDetailsResponseBodyEncrypted")
    private String fetchCardDetailsResponseBodyEncrypted;

    public InstantCreditCardHeader getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(InstantCreditCardHeader subHeader) {
        this.subHeader = subHeader;
    }

    public String getFetchCardDetailsResponseBodyEncrypted() {
        return fetchCardDetailsResponseBodyEncrypted;
    }

    public void setFetchCardDetailsResponseBodyEncrypted(String fetchCardDetailsResponseBodyEncrypted) {
        this.fetchCardDetailsResponseBodyEncrypted = fetchCardDetailsResponseBodyEncrypted;
    }
}
