package com.freecharge.web.webdo;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstantCreditCardResponse {

    @JsonProperty("FetchCardDetailsResponse")
    private FetchCardDetailsResponse fetchCardDetailsResponse;

    public FetchCardDetailsResponse getFetchCardDetailsResponse() {
        return fetchCardDetailsResponse;
    }

    public void setFetchCardDetailsResponse(FetchCardDetailsResponse fetchCardDetailsResponse) {
        this.fetchCardDetailsResponse = fetchCardDetailsResponse;
    }
}
