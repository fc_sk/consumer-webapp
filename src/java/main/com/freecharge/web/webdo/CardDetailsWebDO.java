/**
 * 
 */
package com.freecharge.web.webdo;

import java.util.HashMap;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * @author mandar
 * 
 */
public class CardDetailsWebDO extends BaseWebDO {

    private String              ccHolderName;
    private String              ccNum;
    private String              ccExpMon;
    private String              ccExpYear;
    private String              ccCvv;
    private String              ccCardToken;
    private String              ccCardSaveBool;
    private String              ccCardSaveName;

    private String              dbHolderName;
    private String              dbNum;
    private String              dbExpMon;
    private String              dbExpYear;
    private String              dbCvv;
    private String              dbCardToken;
    private String              dbCardSaveBool;
    private String              dbCardSaveName;
    
    private String channel;

    // Adding an extra param for card meta storage
    private String              pyop;

    private Map<String, String> params;

    public Map<String, String> getParams() {
        if (params != null)
            return params;
        params = new HashMap<String, String>();

        params.put("ccHolderName", ccHolderName);
        params.put("ccNum", ccNum);
        params.put("ccExpMon", ccExpMon);
        params.put("ccExpYear", ccExpYear);
        params.put("ccCvv", ccCvv);
        params.put("ccCardToken", ccCardToken);
        params.put("ccCardSaveBool", ccCardSaveBool);
        params.put("ccCardSaveName", ccCardSaveName);

        params.put("dbHolderName", dbHolderName);
        params.put("dbNum", dbNum);
        params.put("dbExpMon", dbExpMon);
        params.put("dbExpYear", dbExpYear);
        params.put("dbCvv", dbCvv);
        params.put("dbCardToken", dbCardToken);
        params.put("dbCardSaveBool", dbCardSaveBool);
        params.put("dbCardSaveName", dbCardSaveName);
        params.put("pyop", pyop);

        params.put("channel", channel);
        return params;

    }

    public String getCcHolderName() {
        return ccHolderName;
    }

    public void setCcHolderName(String ccHolderName) {
        this.ccHolderName = ccHolderName;
    }

    public String getCcNum() {
        return ccNum;
    }

    public void setCcNum(String ccNum) {
        this.ccNum = ccNum;
    }

    public String getCcExpMon() {
        return ccExpMon;
    }

    public void setCcExpMon(String ccExpMon) {
        this.ccExpMon = ccExpMon;
    }

    public String getCcExpYear() {
        return ccExpYear;
    }

    public void setCcExpYear(String ccExpYear) {
        this.ccExpYear = ccExpYear;
    }

    public String getCcCvv() {
        return ccCvv;
    }

    public void setCcCvv(String ccCvv) {
        this.ccCvv = ccCvv;
    }

    public String getCcCardToken() {
        return ccCardToken;
    }

    public void setCcCardToken(String ccCardToken) {
        this.ccCardToken = ccCardToken;
    }

    public String getCcCardSaveBool() {
        return ccCardSaveBool;
    }

    public void setCcCardSaveBool(String ccCardSaveBool) {
        this.ccCardSaveBool = ccCardSaveBool;
    }

    public String getCcCardSaveName() {
        return ccCardSaveName;
    }

    public void setCcCardSaveName(String ccCardSaveName) {
        this.ccCardSaveName = ccCardSaveName;
    }

    public String getDbHolderName() {
        return dbHolderName;
    }

    public void setDbHolderName(String dbHolderName) {
        this.dbHolderName = dbHolderName;
    }

    public String getDbNum() {
        return dbNum;
    }

    public void setDbNum(String dbNum) {
        this.dbNum = dbNum;
    }

    public String getDbExpMon() {
        return dbExpMon;
    }

    public void setDbExpMon(String dbExpMon) {
        this.dbExpMon = dbExpMon;
    }

    public String getDbExpYear() {
        return dbExpYear;
    }

    public void setDbExpYear(String dbExpYear) {
        this.dbExpYear = dbExpYear;
    }

    public String getDbCvv() {
        return dbCvv;
    }

    public void setDbCvv(String dbCvv) {
        this.dbCvv = dbCvv;
    }

    public String getDbCardToken() {
        return dbCardToken;
    }

    public void setDbCardToken(String dbCardToken) {
        this.dbCardToken = dbCardToken;
    }

    public String getDbCardSaveBool() {
        return dbCardSaveBool;
    }

    public void setDbCardSaveBool(String dbCardSaveBool) {
        this.dbCardSaveBool = dbCardSaveBool;
    }

    public String getDbCardSaveName() {
        return dbCardSaveName;
    }

    public void setDbCardSaveName(String dbCardSaveName) {
        this.dbCardSaveName = dbCardSaveName;
    }

    public String getPyop() {
        return pyop;
    }

    public void setPyop(String pyop) {
        this.pyop = pyop;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    
}
