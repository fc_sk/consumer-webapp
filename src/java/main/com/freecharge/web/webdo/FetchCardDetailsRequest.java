package com.freecharge.web.webdo;

import org.codehaus.jackson.annotate.JsonProperty;

public class FetchCardDetailsRequest {

    @JsonProperty("SubHeader")
    private InstantCreditCardHeader subHeader;

    @JsonProperty("FetchCardDetailsRequestBodyEncrypted")
    private String fetchCardDetailsRequestBodyEncrypted;

    public InstantCreditCardHeader getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(InstantCreditCardHeader subHeader) {
        this.subHeader = subHeader;
    }

    public String getFetchCardDetailsRequestBodyEncrypted() {
        return fetchCardDetailsRequestBodyEncrypted;
    }

    public void setFetchCardDetailsRequestBodyEncrypted(String fetchCardDetailsRequestBodyEncrypted) {
        this.fetchCardDetailsRequestBodyEncrypted = fetchCardDetailsRequestBodyEncrypted;
    }
}
