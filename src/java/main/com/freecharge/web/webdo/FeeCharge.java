package com.freecharge.web.webdo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;


public class FeeCharge implements Serializable{
	
	private String feeId;
	
	private int feeVersion;

	private String serviceChargeId;

	private FeeType feeType;

	private BigDecimal feeAmount;

	private BigDecimal txnAmount;
	
	private Map<SettlementEntity, FeeShare> feeShare;

	private Map<ServiceChargeEntity, FeeShare> serviceChargeShare;

	public String getFeeId() {
		return feeId;
	}

	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

	public int getFeeVersion() {
		return feeVersion;
	}

	public void setFeeVersion(int feeVersion) {
		this.feeVersion = feeVersion;
	}

	public String getServiceChargeId() {
		return serviceChargeId;
	}

	public void setServiceChargeId(String serviceChargeId) {
		this.serviceChargeId = serviceChargeId;
	}

	public FeeType getFeeType() {
		return feeType;
	}

	public void setFeeType(FeeType feeType) {
		this.feeType = feeType;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public Map<SettlementEntity, FeeShare> getFeeShare() {
		return feeShare;
	}

	public void setFeeShare(Map<SettlementEntity, FeeShare> feeShare) {
		this.feeShare = feeShare;
	}

	public Map<ServiceChargeEntity, FeeShare> getServiceChargeShare() {
		return serviceChargeShare;
	}

	public void setServiceChargeShare(Map<ServiceChargeEntity, FeeShare> serviceChargeShare) {
		this.serviceChargeShare = serviceChargeShare;
	}

	public BigDecimal getFeeAmount() {
		return feeAmount;
	}

	
	
	public void setFeeAmount(BigDecimal amount) {
		if (amount != null) {
			this.feeAmount = amount.setScale(2, RoundingMode.HALF_EVEN);
		} else {
			this.feeAmount = amount;
		}
	}

}
