package com.freecharge.web.webdo;

public class PostRechargeDetailsWebDo {

	private String errorBucket;
	private String title;
	private String reasonMsg;
	private String nextMsg;
	private String careMsg;
	private Boolean isActionRequired;
	private String actionBucket;
	private String actionBucketDeepLink;
	private String actionBucketCTA;
	private String howToUseWeb;
	private String howToUseAndroid;

	public String getHowToUseWeb() {
		return howToUseWeb;
	}

	public void setHowToUseWeb(String howToUseWeb) {
		this.howToUseWeb = howToUseWeb;
	}

	public String getHowToUseAndroid() {
		return howToUseAndroid;
	}

	public void setHowToUseAndroid(String howToUseAndroid) {
		this.howToUseAndroid = howToUseAndroid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReasonMsg() {
		return reasonMsg;
	}

	public void setReasonMsg(String reasonMsg) {
		this.reasonMsg = reasonMsg;
	}

	public String getNextMsg() {
		return nextMsg;
	}

	public void setNextMsg(String nextMsg) {
		this.nextMsg = nextMsg;
	}

	public String getCareMsg() {
		return careMsg;
	}

	public void setCareMsg(String careMsg) {
		this.careMsg = careMsg;
	}

	public Boolean getIsActionRequired() {
		return isActionRequired;
	}

	public void setIsActionRequired(Boolean isActionRequired) {
		this.isActionRequired = isActionRequired;
	}

	public String getActionBucket() {
		return actionBucket;
	}

	public void setActionBucket(String actionBucket) {
		this.actionBucket = actionBucket;
	}

	public String getActionBucketDeepLink() {
		return actionBucketDeepLink;
	}

	public void setActionBucketDeepLink(String actionBucketDeepLink) {
		this.actionBucketDeepLink = actionBucketDeepLink;
	}

	public String getActionBucketCTA() {
		return actionBucketCTA;
	}

	public void setActionBucketCTA(String actionBucketCTA) {
		this.actionBucketCTA = actionBucketCTA;
	}
	
	
	public String getErrorBucket() {
		return errorBucket;
	}

	public void setErrorBucket(String errorBucket) {
		this.errorBucket = errorBucket;
	}

	@Override
	public String toString() {
		return "PostRechargeDetailsWebDo [errorBucket=" + errorBucket + ", title=" + title + ", reasonMsg=" + reasonMsg
				+ ", nextMsg=" + nextMsg + ", careMsg=" + careMsg + ", isActionRequired=" + isActionRequired
				+ ", actionBucket=" + actionBucket + ", actionBucketDeepLink=" + actionBucketDeepLink
				+ ", actionBucketCTA=" + actionBucketCTA + "]";
	}

	

}
