package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class PaymentTypeMasterWebDO extends BaseWebDO {

    private String name;
    private String code;
    private List<PaymentTypeOptionWebDO> options;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<PaymentTypeOptionWebDO> getOptions() {
        return options;
    }

    public void setOptions(List<PaymentTypeOptionWebDO> options) {
        this.options = options;
    }
}
