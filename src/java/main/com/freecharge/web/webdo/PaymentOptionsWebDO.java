package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 5, 2012
 * Time: 11:41:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentOptionsWebDO extends BaseWebDO {

    public List<PaymentTypeMaster> getPaymentTypeMasterList() {
        return paymentTypeMasterList;
    }

    public void setPaymentTypeMasterList(List<PaymentTypeMaster> paymentTypeMasterList) {
        this.paymentTypeMasterList = paymentTypeMasterList;
    }

    List<PaymentTypeMaster> paymentTypeMasterList ;
    
}
