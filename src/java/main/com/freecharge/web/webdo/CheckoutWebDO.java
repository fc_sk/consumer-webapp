package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

public class CheckoutWebDO extends BaseWebDO {
    private Integer txnHomePageId;
    private String defaultProfile;
    private String satus;
    private List<PaymentTypeMasterWebDO> paymentTypes;
    private String operatorImgUrl;
    
    private String productType;
    
    private WalletEligibility walletEligibility;
    private PaymentBreakup paymentBreakup;
    
    private String walletBalance;
    private String payableTotalAmount;
    private String payablePGAmount;
    private String payableWalletAmount;
    
    private Boolean walletEnabled;
    private Boolean walletDisplayEnabled;

    private Boolean cardStorageOptIn;

    private CartWebDo cartWebDo;
    
    /*
     * This is a catch all boolean used to
     * indicate if *any* kind of discount 
     * (freefund/cash-back/offer-code/etc.,) is
     * enabled for a given cart. Introduced for
     * add-cash-to-wallet product. Keep that in mind
     * if you want to re-use this field.
     */
    private Boolean isOffersEnabled = true;

    public Boolean getIsOffersEnabled() {
        return isOffersEnabled;
    }
    
    public void setIsOffersEnabled(Boolean isOffersEnabled) {
        this.isOffersEnabled = isOffersEnabled;
    }

    public PaymentBreakup getPaymentBreakup() {
        return paymentBreakup;
    }
    
    public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
        this.paymentBreakup = paymentBreakup;
    }
    
    public WalletEligibility getWalletEligibility() {
        return walletEligibility;
    }
    
    public void setWalletEligibility(WalletEligibility walletEligibility) {
        this.walletEligibility = walletEligibility;
    }

    public Boolean getWalletDisplayEnabled() {
        return walletDisplayEnabled;
    }
    
    public void setWalletDisplayEnabled(Boolean walletDisplayEnabled) {
        this.walletDisplayEnabled = walletDisplayEnabled;
    }
    
    public Boolean getWalletEnabled() {
        return walletEnabled;
    }
    
    public void setWalletEnabled(Boolean walletEnabled) {
        this.walletEnabled = walletEnabled;
    }

    public String getWalletBalance() {
        return walletBalance;
    }
    
    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }
    
    public String getPayableTotalAmount() {
        return payableTotalAmount;
    }
    
    public void setPayableTotalAmount(String payableTotalAmount) {
        this.payableTotalAmount = payableTotalAmount;
    }
    
    public String getPayablePGAmount() {
        return payablePGAmount;
    }
    
    public void setPayablePGAmount(String payablePGAmount) {
        this.payablePGAmount = payablePGAmount;
    }
    
    public String getPayableWalletAmount() {
        return payableWalletAmount;
    }
    
    public void setPayableWalletAmount(String payableWalletAmount) {
        this.payableWalletAmount = payableWalletAmount;
    }
    
    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getSatus() {
        return satus;
    }

    public void setSatus(String satus) {
        this.satus = satus;
    }

    public List<PaymentTypeMasterWebDO> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentTypeMasterWebDO> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public String getOperatorImgUrl() {
        return operatorImgUrl;
    }

    public void setOperatorImgUrl(String operatorImgUrl) {
        this.operatorImgUrl = operatorImgUrl;
    }

    public Boolean getCardStorageOptIn() {
        return cardStorageOptIn;
    }

    public void setCardStorageOptIn(Boolean cardStorageOptIn) {
        this.cardStorageOptIn = cardStorageOptIn;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public CartWebDo getCartWebDo() {
        return cartWebDo;
    }

    public void setCartWebDo(CartWebDo cartWebDo) {
        this.cartWebDo = cartWebDo;
    }
}
