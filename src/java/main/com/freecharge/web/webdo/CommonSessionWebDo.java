package com.freecharge.web.webdo;

import java.io.Serializable;
import java.util.Map;

import com.freecharge.common.util.FCUtil;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: May 21, 2012
 * Time: 9:40:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommonSessionWebDo implements Serializable{
    private String rechargeMobileNumber;
    private String rechargeAmount;
    private String postpaidMobileNumber;
    private String postpaidAmount;
    private String type;
    private String paymentAmount;
    private String operatorName;

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getRechargeMobileNumber() {
        return rechargeMobileNumber;
    }

    public void setRechargeMobileNumber(String rechargeMobileNumber) {
        this.rechargeMobileNumber = rechargeMobileNumber;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getPostpaidMobileNumber() {
		return postpaidMobileNumber;
	}

	public void setPostpaidMobileNumber(String postpaidMobileNumber) {
		this.postpaidMobileNumber = postpaidMobileNumber;
	}

	public String getPostpaidAmount() {
		return postpaidAmount;
	}

	public void setPostpaidAmount(String postpaidAmount) {
		this.postpaidAmount = postpaidAmount;
	}

	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    private static Map<String, Integer> PRODUCT_TYPE_ID_MAP = FCUtil.createMap(
    		"V", 1,
    		"D", 2,
    		"C", 3,
    		"F", 12,
    		"M", 201,
    		"X", 21);

    public Integer getProductTypeInt(){
    	Integer id = PRODUCT_TYPE_ID_MAP.get(this.type);
    	return id==null? 0: id;
    }


    public String getPrimaryAmount() {
    	if (this.type == null) {
    		throw new IllegalStateException("productType is found NULL");
    	}
        switch (this.type) {
		case "V":
		case "D":
		case "C":
		case "F":
		case "T":
		case "B":
		case "L":
		case "E":
		case "I":
		case "G":
		case "Z":
		case "Y":
		case "W":
		case "X":
			return rechargeAmount;

		case "M":
			return postpaidAmount;

		default:
			throw new IllegalStateException("Cannot handle product type: " + (type==null? "NULL": ("'" + type + "'")));
		}
    }
}
