package com.freecharge.web.webdo;

import java.util.List;

public class FCUPIWebDO {
    
    private UPIAccountDetails ac;
    
    private String amount;
    
    private UPICredentials cred;
    
    private String customerId;
    
    private UPIDeviceInfo device;
    
    private List<UPIPayee> payees;
    
    private String remarks;
    
    private String txnId;
    
    private boolean saveAsBeneficiary;

    public UPIAccountDetails getAc() {
        return this.ac;
    }

    public void setAc(UPIAccountDetails ac) {
        this.ac = ac;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public UPICredentials getCred() {
        return this.cred;
    }

    public void setCred(UPICredentials cred) {
        this.cred = cred;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public UPIDeviceInfo getDevice() {
        return this.device;
    }

    public void setDevice(UPIDeviceInfo device) {
        this.device = device;
    }

    public List<UPIPayee> getPayees() {
        return this.payees;
    }

    public void setPayees(List<UPIPayee> payees) {
        this.payees = payees;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTxnId() {
        return this.txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public boolean isSaveAsBeneficiary() {
        return this.saveAsBeneficiary;
    }

    public void setSaveAsBeneficiary(boolean saveAsBeneficiary) {
        this.saveAsBeneficiary = saveAsBeneficiary;
    }

    
    

}
