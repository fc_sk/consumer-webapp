package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA. User: user Date: May 23, 2012 Time: 12:03:02 PM To change this template use File | Settings | File Templates.
 */
public class OperatorCircleWebDO extends BaseWebDO {
	private String prefix;
	private List<Map<String, Object>> AllOperatorsList;
	private List<Map<String, Object>> prefixData;

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public List<Map<String, Object>> getPrefixData() {
		return prefixData;
	}

	public void setPrefixData(List<Map<String, Object>> prefixData) {
		this.prefixData = prefixData;
	}

	public List<Map<String, Object>> getAllOperatorsList() {
		return AllOperatorsList;
	}

	public void setAllOperatorsList(List<Map<String, Object>> allOperatorsList) {
		AllOperatorsList = allOperatorsList;
	}

}
