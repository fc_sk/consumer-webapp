package com.freecharge.web.webdo;

import java.io.Serializable;

public class BBPSDetails implements Serializable{

	private String bbpsReferenceNumber;
	private String dueDate;
	private String customerAccountNumber;
	private String customerName;
	
	@Override
	public String toString() {
		return "BBPSDetails [bbpsReferenceNumber=" + bbpsReferenceNumber + ", dueDate=" + dueDate
				+ ", customerAccountNumber=" + customerAccountNumber + ", customerName=" + customerName + "]";
	}
	
	
	public String getBbpsReferenceNumber() {
		return bbpsReferenceNumber;
	}
	public void setBbpsReferenceNumber(String bbpsReferenceNumber) {
		this.bbpsReferenceNumber = bbpsReferenceNumber;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getCustomerAccountNumber() {
		return customerAccountNumber;
	}
	public void setCustomerAccountNumber(String customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
}
