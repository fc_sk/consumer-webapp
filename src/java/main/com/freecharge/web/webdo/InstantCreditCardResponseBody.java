package com.freecharge.web.webdo;

import org.codehaus.jackson.annotate.JsonProperty;

public class InstantCreditCardResponseBody {
    private Boolean      isCardDetailsFetched;

    private String       failureReason;

    @JsonProperty("CardDetails")
    private InstantCreditCardDetail cardDetails;

    public Boolean getIsCardDetailsFetched() {
        return isCardDetailsFetched;
    }

    public void setIsCardDetailsFetched(Boolean cardDetailsFetched) {
        isCardDetailsFetched = cardDetailsFetched;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public InstantCreditCardDetail getCardDetails() {
        return cardDetails;
    }

    public void setCardDetails(InstantCreditCardDetail cardDetails) {
        this.cardDetails = cardDetails;
    }
}
