package com.freecharge.web.webdo;

import com.freecharge.web.webdo.ExceptionErrorCode;

public class InternalClientException extends InternalErrorException{
	

	public InternalClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public InternalClientException(String message) {
		super(message);
	}

	public InternalClientException(Throwable e) {
		super(e);
	}

	{
		this.setErrorCode(ExceptionErrorCode.CLIENT_INTERNAL);
	}

}
