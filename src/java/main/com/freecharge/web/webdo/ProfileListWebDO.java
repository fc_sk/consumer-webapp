package com.freecharge.web.webdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseListWebDO;

public class ProfileListWebDO extends BaseListWebDO {
	
    private List<ProfileWebDO> profileWebDOList;

	public List<ProfileWebDO> getProfileWebDOList() {
		return profileWebDOList;
	}

	public void setProfileWebDOList(List<ProfileWebDO> profileWebDOList) {
		this.profileWebDOList = profileWebDOList;
	}
    
	

}
