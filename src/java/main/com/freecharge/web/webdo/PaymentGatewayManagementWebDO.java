package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class PaymentGatewayManagementWebDO  extends BaseWebDO {

    private String product;
    private String bankGrp;
    private String paymentGateway;

    private String submit;

    private List<Map<String, Object>> data;


    

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBankGrp() {
        return bankGrp;
    }

    public void setBankGrp(String bankGrp) {
        this.bankGrp = bankGrp;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public List<Map<String, Object>> getData() {
        return data;
    }

    public void setData(List<Map<String, Object>> data) {
        this.data = data;
    }
}
