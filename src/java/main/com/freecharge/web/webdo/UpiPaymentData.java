package com.freecharge.web.webdo;

import java.util.List;

public class UpiPaymentData {

    private Account ac;
    private String amount;
    private Credential cred;
    private String customerId;
    private Device device;
    private List<Payee> payees;
    private String remarks;
    private String txnId;
    private boolean saveAsBeneficiary = true;
    private String emailId;
    private String imsId;
    private String orderId;
    private String realIp;

    public String getRealIp() {
        return realIp;
    }

    public void setRealIp(String realIp) {
        this.realIp = realIp;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getImsId() {
        return imsId;
    }

    public void setImsId(String imsId) {
        this.imsId = imsId;
    }

    @Override
    public String toString() {
        return "UpiPaymentData [ac=" + ac + ", amount=" + amount + ", cred="
                + cred + ", customerId=" + customerId + ", device=" + device
                + ", payees=" + payees + ", remarks=" + remarks + ", txnId="
                + txnId + ", saveAsBeneficiary=" + saveAsBeneficiary
                + ", emailId=" + emailId + ", imsId=" + imsId + ", orderId="
                + orderId + ", realIp=" + realIp + "]";
    }

    public Account getAc() {
        return ac;
    }

    public void setAc(Account ac) {
        this.ac = ac;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Credential getCred() {
        return cred;
    }

    public void setCred(Credential cred) {
        this.cred = cred;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public List<Payee> getPayees() {
        return payees;
    }

    public void setPayees(List<Payee> payees) {
        this.payees = payees;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public boolean isSaveAsBeneficiary() {
        return saveAsBeneficiary;
    }

    public void setSaveAsBeneficiary(boolean saveAsBeneficiary) {
        this.saveAsBeneficiary = saveAsBeneficiary;
    }

    public static class Account {
        private String accRefNumber;
        private String iin;
        private String aeba;
        private String dlength;
        private String dtype;
        private String ifsc;
        private String maskedAccnumber;
        private String name;
        private String status;
        private String type;
        private String vpa;

        @Override
        public String toString() {
            return "Account [accRefNumber=" + accRefNumber + ", iin=" + iin
                    + ", aeba=" + aeba + ", dlength=" + dlength + ", dtype="
                    + dtype + ", ifsc=" + ifsc + ", maskedAccnumber="
                    + maskedAccnumber + ", name=" + name + ", status=" + status
                    + ", type=" + type + ", vpa=" + vpa + "]";
        }

        public String getAccRefNumber() {
            return accRefNumber;
        }
        public void setAccRefNumber(String accRefNumber) {
            this.accRefNumber = accRefNumber;
        }
        public String getIin() {
            return iin;
        }
        public void setIin(String iin) {
            this.iin = iin;
        }
        public String getAeba() {
            return aeba;
        }
        public void setAeba(String aeba) {
            this.aeba = aeba;
        }
        public String getDlength() {
            return dlength;
        }
        public void setDlength(String dlength) {
            this.dlength = dlength;
        }
        public String getDtype() {
            return dtype;
        }
        public void setDtype(String dtype) {
            this.dtype = dtype;
        }
        public String getIfsc() {
            return ifsc;
        }
        public void setIfsc(String ifsc) {
            this.ifsc = ifsc;
        }
        public String getMaskedAccnumber() {
            return maskedAccnumber;
        }
        public void setMaskedAccnumber(String maskedAccnumber) {
            this.maskedAccnumber = maskedAccnumber;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getStatus() {
            return status;
        }
        public void setStatus(String status) {
            this.status = status;
        }
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public String getVpa() {
            return vpa;
        }
        public void setVpa(String vpa) {
            this.vpa = vpa;
        }

    }

    public static class Credential {
        private String subType;
        private String type;
        private Data data;

        @Override
        public String toString() {
            return "Credential [subType=" + subType + ", type=" + type + ", data=" + data + "]";
        }
        public String getSubType() {
            return subType;
        }
        public void setSubType(String subType) {
            this.subType = subType;
        }
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public Data getData() {
            return data;
        }
        public void setData(Data data) {
            this.data = data;
        }
        public static class Data {
            private String code;
            private String encryptedBase64String;
            private String ki;

            public String getKi() {
                return this.ki;
            }
            public void setKi(String ki) {
                this.ki = ki;
            }
            @Override
            public String toString() {
                return "Data [code=" + code + ", encryptedBase64String=" + encryptedBase64String + "]";
            }
            public String getCode() {
                return code;
            }
            public void setCode(String code) {
                this.code = code;
            }
            public String getEncryptedBase64String() {
                return encryptedBase64String;
            }
            public void setEncryptedBase64String(String encryptedBase64String) {
                this.encryptedBase64String = encryptedBase64String;
            }
        }
    }

    public static class Device {
        private String app;
        private String mobile;
        private String os;
        private String type;
        private String capability;
        private String gcmid;
        private String geocode;
        private String id;
        private String ip;
        private String location;

        @Override
        public String toString() {
            return "Device [app=" + app + ", mobile=" + mobile + ", os=" + os + ", type=" + type + ", capability="
                    + capability + ", gcmid=" + gcmid + ", geocode=" + geocode + ", id=" + id + ", ip=" + ip
                    + ", location=" + location + "]";
        }
        public String getCapability() {
            return capability;
        }
        public void setCapability(String capability) {
            this.capability = capability;
        }
        public String getGcmid() {
            return gcmid;
        }
        public void setGcmid(String gcmid) {
            this.gcmid = gcmid;
        }
        public String getGeocode() {
            return geocode;
        }
        public void setGeocode(String geocode) {
            this.geocode = geocode;
        }
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getIp() {
            return ip;
        }
        public void setIp(String ip) {
            this.ip = ip;
        }
        public String getLocation() {
            return location;
        }
        public void setLocation(String location) {
            this.location = location;
        }
        public String getApp() {
            return app;
        }
        public void setApp(String app) {
            this.app = app;
        }
        public String getMobile() {
            return mobile;
        }
        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
        public String getOs() {
            return os;
        }
        public void setOs(String os) {
            this.os = os;
        }
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }

    }

    public static class Payee {
        private String bamount;
        private String beneVpa;
        private String beneName;

        @Override
        public String toString() {
            return "Payee [bamount=" + bamount + ", beneVpa=" + beneVpa + ", beneName=" + beneName + "]";
        }
        public String getBamount() {
            return bamount;
        }
        public void setBamount(String bamount) {
            this.bamount = bamount;
        }
        public String getBeneVpa() {
            return beneVpa;
        }
        public void setBeneVpa(String beneVpa) {
            this.beneVpa = beneVpa;
        }
        public String getBeneName() {
            return beneName;
        }
        public void setBeneName(String beneName) {
            this.beneName = beneName;
        }
    }
}
