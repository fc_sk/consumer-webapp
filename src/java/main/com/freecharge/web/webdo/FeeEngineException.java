package com.freecharge.web.webdo;

import com.freecharge.web.webdo.ExceptionErrorCode;
import com.freecharge.web.webdo.FeeEngineException;

public class FeeEngineException extends RuntimeException {

	private static final long serialVersionUID = 8345160879391444809L;

	private ExceptionErrorCode errorCode;
	private Class<? extends FeeEngineException> exceptionCause;

	public FeeEngineException withErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
		return this;

	}

	public FeeEngineException(Throwable e) {
		super(e);
	}

	public FeeEngineException(String message) {
		super(message);
	}

	public FeeEngineException(String message, Throwable e) {
		super(message, e);
	}

	public ExceptionErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ExceptionErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Class<?> getExceptionCause() {
		return exceptionCause;
	}

	public void setExceptionCause(Class<? extends FeeEngineException> exceptionCause) {
		this.exceptionCause = exceptionCause;
	}

	{
		this.setExceptionCause(this.getClass());
	}
}
