package com.freecharge.web.webdo;

public class InstantCreditCardDetail {
    private String          cardNumber;

    private String          cardEmbossingDetails;

    private String          cardExpiryDate;

    private String          cardCVV2;

    private String          cardSerNo;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardEmbossingDetails() {
        return cardEmbossingDetails;
    }

    public void setCardEmbossingDetails(String cardEmbossingDetails) {
        this.cardEmbossingDetails = cardEmbossingDetails;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public String getCardCVV2() {
        return cardCVV2;
    }

    public void setCardCVV2(String cardCVV2) {
        this.cardCVV2 = cardCVV2;
    }

    public String getCardSerNo() {
        return cardSerNo;
    }

    public void setCardSerNo(String cardSerNo) {
        this.cardSerNo = cardSerNo;
    }
}
