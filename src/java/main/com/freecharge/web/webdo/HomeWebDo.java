package com.freecharge.web.webdo;

import java.util.List;
import java.util.Map;

import org.springmodules.validation.bean.conf.loader.annotation.handler.Length;
import org.springmodules.validation.bean.conf.loader.annotation.handler.NotBlank;

import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA. User: Toshiba Date: May 8, 2012 Time: 1:13:35 PM To
 * change this template use File | Settings | File Templates.
 */
public class HomeWebDo extends BaseWebDO {
	@NotBlank(message = "Mobile number not blank")
	@Length(max = 10, message = "mobile not exceed 10 digits")
	private String mobileNumber = "";
	@NotBlank
	private String amount = "";
	@NotBlank(message = "Operator not blank")
	private String operator;
	private String circle;
	private List<Map<String, Object>> operatorList;
	private String operatorName;
	private String operatorCode;
	private String type;
	private String dthAmount = "";
	private String dataCardAmount = "";
	private Integer couponWorthAmt;
	private Integer tabId;
	private String displayServiceNo;
	private String displayOperatorName;
	private Integer displayAmount;
	private Integer txnHomePageId;
	private List<String> itemIdArr;
	private Integer remainingBalance;
    private Integer noOfCoupons=0;
    private Integer payableAmount=0;
    private Integer roundOffAmount=0;
    private List<StateMaster> stateMaster;
    private List<VoucherCat> categories ;
    private List circleList;
    private List<String> rechargePlanNameList;
    private String rechargeType;
    private String festiveMessageFlag;
    private String festiveMessage;

	public List<VoucherCat> getCategories() {
		return categories;
	}

	public void setCategories(List<VoucherCat> categories) {
		this.categories = categories;
	}

	public Integer getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(Integer payableAmount) {
		this.payableAmount = payableAmount;
	}


	public Integer getNoOfCoupons() {
		return noOfCoupons;
	}

	public void setNoOfCoupons(Integer noOfCoupons) {
		this.noOfCoupons = noOfCoupons;
	}

	public Integer getDisplayAmount() {
		return displayAmount;
	}

	public void setDisplayAmount(Integer displayAmount) {
		this.displayAmount = displayAmount;
	}

	public String getDisplayServiceNo() {
		return displayServiceNo;
	}

	public void setDisplayServiceNo(String displayServiceNo) {
		this.displayServiceNo = displayServiceNo;
	}

	public String getDisplayOperatorName() {
		return displayOperatorName;
	}

	public void setDisplayOperatorName(String displayOperatorName) {
		this.displayOperatorName = displayOperatorName;
	}

	public Integer getTabId() {
		return tabId;
	}

	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}

	public Integer getCouponWorthAmt() {
		return couponWorthAmt;
	}

	public void setCouponWorthAmt(Integer couponWorthAmt) {
		this.couponWorthAmt = couponWorthAmt;
	}

	public Integer getRemainingBalance() {
		return remainingBalance;
	}

	public void setRemainingBalance(Integer remainingBalance) {
		this.remainingBalance = remainingBalance;
	}

	private String dataCardNumber = "";

	

	public List<String> getItemIdArr() {
		return itemIdArr;
	}

	public void setItemIdArr(List<String> itemIdArr) {
		this.itemIdArr = itemIdArr;
	}

	public Integer getTxnHomePageId() {
		return txnHomePageId;
	}

	public void setTxnHomePageId(Integer txnHomePageId) {
		this.txnHomePageId = txnHomePageId;
	}

	public String getDthAmount() {
		return dthAmount;
	}

	public void setDthAmount(String dthAmount) {
		this.dthAmount = dthAmount;
	}

	public String getDthNumber() {
		return dthNumber;
	}

	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}

	private String dthNumber = "";

	private int productMasterId;

	public String getDataCardNumber() {
		return dataCardNumber;
	}

	public List getCircleList() {
		return circleList;
	}

	public void setCircleList(List circleList) {
		this.circleList = circleList;
	}
	public void setDataCardNumber(String dataCardNumber) {
		this.dataCardNumber = dataCardNumber;
	}

	public String getDataCardAmount() {
		return dataCardAmount;
	}

	public void setDataCardAmount(String dataCardAmount) {
		this.dataCardAmount = dataCardAmount;
	}

	public HomeWebDo() {

	}

	public int getProductMasterId() {
		return productMasterId;
	}

	public void setProductMasterId(int productMasterId) {
		this.productMasterId = productMasterId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public List<Map<String, Object>> getOperatorList() {
		return operatorList;
	}

	public void setOperatorList(List<Map<String, Object>> operatorList) {
		this.operatorList = operatorList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<StateMaster> getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(List<StateMaster> stateMaster) {
		this.stateMaster = stateMaster;
	}

	public void setRoundOffAmount(Integer roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}

	public Integer getRoundOffAmount() {
		return roundOffAmount;
	}

	public List<String> getRechargePlanNameList() {
		return rechargePlanNameList;
	}

	public void setRechargePlanNameList(List<String> rechargePlanNameList) {
		this.rechargePlanNameList = rechargePlanNameList;
	}
	
	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

    public String getFestiveMessageFlag() {
        return festiveMessageFlag;
    }

    public void setFestiveMessageFlag(String f) {
        this.festiveMessageFlag = f;
    }

    public String getFestiveMessage() {
        return festiveMessage;
    }

    public void setFestiveMessage(String f) {
        this.festiveMessage = f;
    }
}
