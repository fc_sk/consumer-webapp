package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 9, 2012
 * Time: 8:45:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChangePasswordWebDO extends BaseWebDO{
    private String email="vijay.sudigali@gmail.com";
       private String newPassword;
      private String oldPassword;
    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    private String status;

       public String getStatus() {
           return status;
       }

       public void setStatus(String status) {
           this.status = status;
       }

       public String getEmail() {
           return email;
       }

       public void setEmail(String email) {
           this.email = email;
       }

       public String getNewPassword() {
           return newPassword;
       }

       public void setNewPassword(String newPassword) {
           this.newPassword = newPassword;
       }
    
}
