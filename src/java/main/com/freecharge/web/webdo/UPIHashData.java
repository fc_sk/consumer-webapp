package com.freecharge.web.webdo;

public class UPIHashData {
    
    private String code;
    private String encryptedBase64String;
    private String ki;
    
    public String getCode() {
        return this.code;
    }
    public String getKi() {
        return this.ki;
    }
    public void setKi(String ki) {
        this.ki = ki;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getEncryptedBase64String() {
        return this.encryptedBase64String;
    }
    public void setEncryptedBase64String(String encryptedBase64String) {
        this.encryptedBase64String = encryptedBase64String;
    }

}
