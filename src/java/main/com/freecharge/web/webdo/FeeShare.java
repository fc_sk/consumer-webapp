package com.freecharge.web.webdo;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class FeeShare{

	private BigDecimal amount;
	
	private BigDecimal feeValue;
	
	private FeeValueType feeValueType;
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public FeeValueType getFeeValueType() {
		return feeValueType;
	}

	public void setFeeValueType(FeeValueType feeValueType) {
		this.feeValueType = feeValueType;
	}

	public BigDecimal getFeeValue() {
		return feeValue;
	}

	
	
	public void setFeeValue(BigDecimal feeValue) {
		if (feeValue != null) {
			this.feeValue = feeValue.setScale(2, RoundingMode.HALF_DOWN);
		} else {
			this.feeValue = feeValue;
		}
	}

}
