package com.freecharge.web.webdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 22, 2012
 * Time: 5:47:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class FirstNameWebDO extends BaseWebDO{
    private String firstName;
    private Integer txnHomePageId;
    private String status;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
