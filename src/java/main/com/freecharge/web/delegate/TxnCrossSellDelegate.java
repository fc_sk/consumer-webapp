package com.freecharge.web.delegate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.TxnCrossSellService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.TxnCrossSellWebDo;

public class TxnCrossSellDelegate implements WebDelegate {
	private Logger logger = LoggingFactory.getLogger(getClass());

    private TxnCrossSellService txnCrossSellService;

    @Autowired
    public void setTxnCrossSellService(TxnCrossSellService txnCrossSellService) {
        this.txnCrossSellService = txnCrossSellService;
    }

    public void delegate(WebContext webContext) {
    	
        TxnCrossSellWebDo txnCrossSellWebDo = (TxnCrossSellWebDo)webContext.getBaseWebDO();
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(txnCrossSellWebDo,webContext);

		txnCrossSellService.saveAllTxnCrossSellService(baseBusinessDO);
    }
}