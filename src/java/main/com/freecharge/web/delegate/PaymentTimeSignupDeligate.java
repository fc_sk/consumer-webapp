package com.freecharge.web.delegate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 14, 2012
 * Time: 11:02:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentTimeSignupDeligate implements WebDelegate {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private CrosssellService crosssellService;

    @Autowired
    private PaymentService paymentService;
    @Autowired
    private CommonService commonService;

    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO)baseBusinessDO;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(registerBusinessDO.getLookupID(), fs);
        String rechargeAmount = FCSessionUtil.getRechargeAmount(registerBusinessDO.getLookupID(), fs);
        String registrationStatus=registrationService.paymentTimeSignup(baseBusinessDO);;
        commonService.getStateMasterList(baseBusinessDO);
        //registrationService.getCountryMasterList(baseBusinessDO);
        webContext.getModel().addAttribute("registrationStatus",registrationStatus);
        ((RegisterBusinessDO)baseBusinessDO).setRegistrationStatus(registrationStatus);
        List<CrosssellBusinessDo> crosssellBusinessDos = null;
            if (rechargeAmount != null) {
                Float amt = Float.parseFloat(rechargeAmount);
                String productType = type == null ? "V" : type;
                registerBusinessDO.setProductType(productType);
                crosssellBusinessDos = crosssellService.getAllCrosssellCoupon(amt, productType);
                registerBusinessDO.setCrosssellBusinessDos(crosssellBusinessDos);
                paymentService.getPaymentTypeOptionsForWeb(productType, FCConstants.DEFAULT_AFFILIATE_ID);
            }

        fs.getSessionData().put(
                registerBusinessDO.getLookupID() + "_" + FCConstants.SESSION_DATA_USERCONTACTDETAIL + "_userId",
                registerBusinessDO.getUserContactDetails().getUserId());
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
