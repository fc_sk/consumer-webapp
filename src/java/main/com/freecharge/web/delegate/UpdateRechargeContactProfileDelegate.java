package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

public class UpdateRechargeContactProfileDelegate implements WebDelegate {

	@Autowired
	private MyAccountService myAccountService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        boolean updateStatus = myAccountService.updateRechargeContactProfile(baseBusinessDO);
        if (updateStatus) {
        	BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        	webContext.setBaseWebDO(baseWebDO);
        } else 
        	webContext.setBaseWebDO(null);
	}

}
