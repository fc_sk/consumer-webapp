package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.FeedbackService;
import com.freecharge.common.businessdo.FeedbackBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

public class FeedbackDelegate implements WebDelegate{
	@Autowired
	private FeedbackService feedbackService;
	@Override
	public void delegate(WebContext webContext) throws Exception {
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
		String status = feedbackService.sendFeedBack(baseBusinessDO);
		((FeedbackBusinessDO)baseBusinessDO).setStatus(status);
		webContext.setBaseBusinessDO(baseBusinessDO);
		BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
	}

}
