package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.PasswordRecoveryWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2012
 * Time: 12:38:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class PasswordRecoveryWebDelegate implements WebDelegate {
     @Autowired
    private ForgotPasswordService forgotPasswordService;
    public void delegate(WebContext webContext) throws Exception {
         BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
         if(baseBusinessDO != null){
        	 forgotPasswordService.doesUserExist(baseBusinessDO);
        	 BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        	 webContext.setBaseWebDO(baseWebDO);
         }else{
        	 ((PasswordRecoveryWebDO)webContext.getBaseWebDO()).setStatus("fail");
         }
    }
}
