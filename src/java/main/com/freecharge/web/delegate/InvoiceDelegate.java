package com.freecharge.web.delegate;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.InvoiceBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.recharge.services.UserTransactionHistoryService;

public class InvoiceDelegate implements WebDelegate{
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private PricingService pricingService;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
private MyAccountService myAccountService;
@Autowired
public void setMyAccountService(MyAccountService myAccountService) {
	this.myAccountService = myAccountService;
}
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
		
		InvoiceBusinessDO invoiceBusinessDO = ((InvoiceBusinessDO) (baseBusinessDO));
		
		String orderId = invoiceBusinessDO.getOrderId();
		
		if (orderId != null && !orderId.isEmpty()) {
		    CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId);
		    
		    if (cartToInspect != null) {
		        UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
		        String finalProductType = userTransactionHistory.getProductType();
		        ProductName finalProductName = ProductName.fromPrimaryProductType(finalProductType);
		        invoiceBusinessDO.setTotalAmount(pricingService.getPayableAmount(cartToInspect));
		        invoiceBusinessDO.setPrimaryProduct(finalProductName);

		        if (ProductName.WalletCash == finalProductName) {
		            List<PaymentTransaction> paymentTransactionList = paymentTransactionService.getPaymentTransactionsByOrderId(orderId);
		            
		            for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                        if (paymentTransaction.getIsSuccessful()) {
                            invoiceBusinessDO.setPrimaryProduct(ProductName.WalletCash);
                            invoiceBusinessDO.setTotalAmount(paymentTransaction.getAmount());
                            invoiceBusinessDO.setPaymentTime(new Timestamp(paymentTransaction.getSetToPg().getTime()));
                            invoiceBusinessDO.setOrderId(paymentTransaction.getOrderId());

                            BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
                            webContext.setBaseWebDO(baseWebDO);
                            
                            return;
                        }
                    }
		            
		        }
		    }
		}
		
		myAccountService.getInvoiceInfo(baseBusinessDO);
		
		myAccountService.getCouponInfo(baseBusinessDO);
        

        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
		
	}

}
