package com.freecharge.web.delegate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.mapper.MyProfileDOMapper;
import com.freecharge.web.webdo.ProfileListWebDO;
import com.freecharge.web.webdo.ProfileWebDO;

public class UserMyProfileDelegate implements WebDelegate {

	@Autowired
	private MyAccountService myAccountService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
	
		ProfileBusinessDO profilebusinessdo=(ProfileBusinessDO)webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
	
		List<ProfileBusinessDO> profileBusinessDOList = myAccountService.getUserProfileList(profilebusinessdo);
		List<ProfileWebDO> profileWebDOList = ((MyProfileDOMapper) webContext.getWebDOMapper()).convertBusinessDOtoWebDOList(profileBusinessDOList,webContext);
		
		ProfileListWebDO profileListWebDO = new ProfileListWebDO();
		profileListWebDO.setProfileWebDOList(profileWebDOList);
		
		webContext.setBaseWebDO(profileListWebDO);
	}

}
