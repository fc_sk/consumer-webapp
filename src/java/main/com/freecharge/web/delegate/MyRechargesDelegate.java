package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.MyRechargesBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.MyRechargesWebDO;

public class MyRechargesDelegate implements WebDelegate{
	private MyAccountService myAccountService;
	@Autowired
	public void setMyAccountService(MyAccountService myAccountService) {
		this.myAccountService = myAccountService;
	}
	@Override
	public void delegate(WebContext webContext) throws Exception {
		MyRechargesBusinessDO myRechargesBusinessDO = (MyRechargesBusinessDO) webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
		myAccountService.getUserTransactionHistory(myRechargesBusinessDO);
		MyRechargesWebDO myRechargesWebDO = (MyRechargesWebDO) webContext.getWebDOMapper().convertBusinessDOtoWebDO(myRechargesBusinessDO, webContext);
	}

}
