package com.freecharge.web.delegate;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.cardstorage.UserCardStorageChoicePreference;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CheckoutBusinessDO;
import com.freecharge.common.easydb.EasyDBReadDAO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.util.WebConstants;

public class CheckoutDelegate implements WebDelegate {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private UserAuthService userAuthService;
    @Autowired
    private CrosssellService crosssellService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private CartService cartService;
    @Autowired
    private PricingService pricingService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private WalletService walletService;
    
    @Autowired
    private WalletWrapper walletWrapper;
    
    @Autowired
    private EasyDBReadDAO easyDBReadDAO;

    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
        CheckoutBusinessDO checkoutBusinessDO = (CheckoutBusinessDO) baseBusinessDO;
        webContext.setBaseBusinessDO(baseBusinessDO);

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(checkoutBusinessDO.getLookupID(), fs);
        checkoutBusinessDO.setSatus("success");

        CartBusinessDo cartBusinessDo = cartService.getCart(checkoutBusinessDO.getLookupID());
        if (cartBusinessDo!=null){
            checkoutBusinessDO.setCartBusinessDo(cartBusinessDo);
        }
        commonService.getStateMasterList(checkoutBusinessDO);

        Boolean isloggedin = false;
        
        if (fs != null) {
            Map map = fs.getSessionData();
            if (map != null) {
                if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                    isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                }
            }
        }

        if (isloggedin != null && isloggedin) {
            if (type != null) {
                String productType = type == null ? "V": type;
                checkoutBusinessDO.setProductType(productType);
                paymentService.getPaymentTypeOptionsForWeb(productType, FCConstants.DEFAULT_AFFILIATE_ID);
            }
            
            Object userIdObject = fs.getSessionData().get(WebConstants.SESSION_USER_USERID);
            int userId = userIdObject==null? -1: (Integer) userIdObject;
            
            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDo);
            Double totalPayableAmount = pricingService.getPayableAmount(cartBusinessDo);
            
            /*
             * Wallet is not elabled for wallet cash product
             */
            if (ProductName.WalletCash != primaryProduct) {
                checkoutBusinessDO.setWalletEnabled(walletService.isWalletPayEnabled());
                checkoutBusinessDO.setWalletDisplayEnabled(walletService.isWalletDataDisplayEnabled());

                
                if (walletService.isWalletDataDisplayEnabled()) {
                    Amount walletBalance = walletWrapper.getDebitableBalance(userId);
                    logger.info("WalletBalance "+walletBalance);
                    WalletEligibility walletEligibility = new WalletEligibility();
                    walletEligibility = walletService.calculateEligibility(userId, new Amount(totalPayableAmount), cartBusinessDo, walletBalance);
                    checkoutBusinessDO.setWalletEligibility(walletEligibility);

                    PaymentBreakup paymentBreakup = walletService.calculatePaymentBreakup(userId, new Amount(totalPayableAmount), cartBusinessDo, walletEligibility, walletBalance);
                    checkoutBusinessDO.setPaymentBreakup(paymentBreakup);
                    
                }
            } else {
                checkoutBusinessDO.setIsOffersEnabled(false);
                checkoutBusinessDO.setWalletEnabled(false);
                checkoutBusinessDO.setWalletDisplayEnabled(false);
                
                WalletEligibility walletEligibility = new WalletEligibility();
                walletEligibility.setEligibleForFullWalletPayment(false);
                walletEligibility.setEligibleForPartialPayment(false);
                checkoutBusinessDO.setWalletEligibility(walletEligibility);
                
                PaymentBreakup paymentBreakup = new PaymentBreakup();
                paymentBreakup.setPayablePGAmount(new Amount(totalPayableAmount));
                paymentBreakup.setPayableTotalAmount(new Amount(totalPayableAmount));
                paymentBreakup.setPayableWalletAmount(Amount.ZERO);
                paymentBreakup.setWalletBalance(Amount.ZERO);
                checkoutBusinessDO.setPaymentBreakup(paymentBreakup);
            }
        }

        populateUserCardStorageChoicePreference(checkoutBusinessDO);
        
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO, webContext);
        webContext.setBaseWebDO(baseWebDO);
    }

    private void populateUserCardStorageChoicePreference(CheckoutBusinessDO checkoutBusinessDO) {
        UserCardStorageChoicePreference userCardStorageChoicePreference = new UserCardStorageChoicePreference();

        Integer userId = ((Integer) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.SESSION_USER_USERID));
        userCardStorageChoicePreference.setUserId(userId);

        //if there is no entry in the db, it means the user has never deselected the card storage option and hence opt him/her in
        try {
            List cardStoragePreference = easyDBReadDAO.get(userCardStorageChoicePreference);
            checkoutBusinessDO.setCardStorageOptIn(cardStoragePreference.isEmpty());
        } catch (Exception e) {
            logger.error("Something bad happened while fetching card storage preference. Enabling it for now and going ahead with payments");
            checkoutBusinessDO.setCardStorageOptIn(true);
        }
    }
}
