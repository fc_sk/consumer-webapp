package com.freecharge.web.delegate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 23, 2012
 * Time: 12:04:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class OperatorCircleDelegate implements WebDelegate {
	
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
	private OperatorCircleService operatorCircleService;
    @Autowired
    public void setOperatorCircleService(
			OperatorCircleService operatorCircleService) {
		this.operatorCircleService = operatorCircleService;
	}

    public void delegate(WebContext webContext) throws Exception {

        try {
            BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(
                    webContext.getBaseWebDO(), webContext);
            
            operatorCircleService.getOperatorCircleList(baseBusinessDO);
            
            BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO, webContext);
            webContext.setBaseWebDO(baseWebDO);
        } catch (Exception e) {
            //Adding this to investigate the NPE occurring in prod
            logger.error("Error getting OperatorCircleList ", e);
            throw e;
        }

    }
}
