package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseResponseDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.UserDO;

public class RegisterWithoutSessionDelegate implements WebDelegate {

		private RegistrationService registrationService;
		
	    @Autowired
	    public void setRegistrationService(RegistrationService registrationService){
	        this.registrationService=registrationService;
	    }
	    
	    public void delegate(WebContext webContext) {
	        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
	        //boolean registrationStatus = registrationService.registerWithoutSession(baseBusinessDO);
	        boolean registrationStatus = registrationService.registerOnBothDB(baseBusinessDO);
	        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
	        UserDO userDO = (UserDO) baseWebDO;
	        userDO.setRegistrationStatusFlag(registrationStatus);
	        BaseResponseDO baseResponseDO = webContext.getResponseDOMapper().convertWebDOToResponseDO(baseWebDO, webContext);
	        webContext.setBaseWebDO(baseWebDO);
	        webContext.setBaseResponseDO(baseResponseDO);
	    }
}

