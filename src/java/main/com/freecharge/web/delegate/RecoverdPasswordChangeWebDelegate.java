package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2012
 * Time: 3:14:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class RecoverdPasswordChangeWebDelegate implements WebDelegate {
    @Autowired
    private ForgotPasswordService forgotPasswordService;
    public void delegate(WebContext webContext) throws Exception {

        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);

        forgotPasswordService.changePassword(baseBusinessDO);

        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
