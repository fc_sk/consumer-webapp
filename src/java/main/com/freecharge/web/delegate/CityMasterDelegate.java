package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.CommonService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 1, 2012
 * Time: 4:40:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class CityMasterDelegate implements WebDelegate{
    @Autowired
    private CommonService commonService;
    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        commonService.getCityMasterList(baseBusinessDO);
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
