package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.ProfileWebDO;

/**
 * Created by IntelliJ IDEA. User: user Date: May 7, 2012 Time: 4:58:08 PM To
 * change this template use File | Settings | File Templates.
 */
public class MyProfileDelegate implements WebDelegate {
	
	@Autowired
	private MyAccountService myAccountService;
	

	public void delegate(WebContext webContext) throws Exception {
		
		ProfileBusinessDO profilebusinessdo=(ProfileBusinessDO)webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
		
		
		myAccountService.getProfile(profilebusinessdo);
		
		ProfileWebDO profilewebdo=(ProfileWebDO)webContext.getWebDOMapper().convertBusinessDOtoWebDO(profilebusinessdo, webContext);
		
		webContext.setBaseWebDO(profilewebdo);
		

	}
}
