package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.MyContactsBussinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.MyContactsWebDO;

public class MyContactsDelegate implements WebDelegate {

	@Autowired
	private MyAccountService myAccountService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		// TODO Auto-generated method stub
		MyContactsBussinessDO mycontactsbusinessdo=(MyContactsBussinessDO)webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
		myAccountService.updateContact(mycontactsbusinessdo);
		MyContactsWebDO mycontactswebdo=(MyContactsWebDO)webContext.getWebDOMapper().convertBusinessDOtoWebDO(mycontactsbusinessdo, webContext);
		webContext.setBaseWebDO(mycontactswebdo);
	}

}
