package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

public class PasswordCheckDelegate implements WebDelegate {

	@Autowired
	private MyAccountService myAccountService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		// TODO Auto-generated method stub
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
		myAccountService.checkUserOldPassword(baseBusinessDO);
		BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
		webContext.setBaseWebDO(baseWebDO);
	}

}
