package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.TxnFulFilmentWebDo;

public class TxnFulFilmentDelegate implements WebDelegate {

    private TxnFulFilmentService txnFulFilmentService;
    @Autowired
    public void setTxnFulFilmentService(TxnFulFilmentService txnFulFilmentService) {
        this.txnFulFilmentService = txnFulFilmentService;
    }

    public void delegate(WebContext webContext) {
        TxnFulFilmentWebDo txnFulFilmentWebDo = (TxnFulFilmentWebDo)webContext.getBaseWebDO();
        //for remove all items
        //System.out.println("cartWebDo.getIsRemove() = "+cartWebDo.getIsRemove());

    }
}