package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 9, 2012
 * Time: 8:03:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChangePasswordDelegate implements WebDelegate{

          @Autowired
    private UserAuthService userAuthService;

    public void delegate(WebContext webContext) throws Exception {

        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);

        userAuthService.changePassword(baseBusinessDO);

        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);


    }
    }

