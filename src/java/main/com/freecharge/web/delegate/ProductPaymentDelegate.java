package com.freecharge.web.delegate;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.app.service.CartCrosssellService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.cardstorage.CardStorageDAO;
import com.freecharge.cardstorage.CardStorageService;
import com.freecharge.cardstorage.UserCardStorageChoicePreference;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.ProductPaymentWebDO;

public class ProductPaymentDelegate implements WebDelegate {
	private static Logger logger = LoggingFactory.getLogger(ProductPaymentDelegate.class);
	@Autowired
	private UserAuthService userAuthService;
	@Autowired
	private CartService cartService;
	@Autowired
    private PricingService pricingService;
	@Autowired
	private FCProperties properties;
	
	@Autowired
	private CartCrosssellService cartCrosssellService;
	
	@Autowired
	private FreefundService freefundService;
	
	@Autowired
	private BinOfferService binOfferService;
	
	@Autowired
	private UsersDAO usersDao;
	
	@Autowired
	private PaymentPlanService paymentPlanService;
	
	@Autowired
	private WalletService walletService;

    @Autowired
    private CardStorageDAO cardStorageDAO;

    @Autowired
    private CardStorageService cardStorageService;
    
    @Autowired
    WalletWrapper walletWrapper;

	public void delegate(WebContext webContext) throws Exception {
		ProductPaymentBusinessDO productPaymentBusinessDO = (ProductPaymentBusinessDO) webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
		logger.info("get payment option " +  productPaymentBusinessDO.getPaymentoption());
		String lookupID = productPaymentBusinessDO.getLookupID();
		Cart cartObj = cartService.getCartBylookUp(lookupID);
		
		/*
		 * We auto-detect payment option
		 * for credit card cards. 
		 */
        if (isPaymentTypeCreditCard(productPaymentBusinessDO)
                && !productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase(PaymentConstants.AMEX_PAYMENT_OPTION)) {
            String cardType = PaymentUtil.getCreditCardType(((ProductPaymentWebDO) webContext.getBaseWebDO()).getCcNum());

            productPaymentBusinessDO.setPaymentoption(cardType);
            ((ProductPaymentWebDO) webContext.getBaseWebDO()).setPaymentoption(cardType);

        }

		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(productPaymentBusinessDO.getLookupID(), fs);
        Integer productId = FCConstants.productMasterMapReverse.get(type);
		CartBusinessDo cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());

		String orderId = userAuthService.generateOrderId(fs.getUuid(), productPaymentBusinessDO, FCConstants.DEFAULT_AFFILIATE_ID, productId, 1, null);
		
		/* Modify the cart for MasterCard Item*/
		if(productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("MC") || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DMC") || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DCME") || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DSME")) {
			CartItems cartItems = new CartItems();
	        cartItems.setCart(cartObj);
	        cartItems.setFkItemId(null);
	        cartItems.setEntityId("");
	        cartItems.setFlProductMasterId(7);
	        cartItems.setPrice(0.0);
	        cartItems.setDisplayLabel("master card");
	        cartItems.setQuantity(1);
	        Date currentDate = new Date();
	        cartItems.setAddedOn(new Timestamp(currentDate.getTime()));
	        cartItems.setIsDeleted(false);
	        cartService.saveCartItems(cartItems);
		}else {
			List<CartItems> cartItems = cartService.getCartItemsByCartId(cartObj.getCartId());
			if(cartItems != null && !cartItems.isEmpty()) {
				for(CartItems cartItem: cartItems) {
					if(cartItem.getFlProductMasterId().intValue() == 7 ){
						cartService.removeCartItem(cartItem.getCartItemsId());
					}
				}
			}
		}
		
		cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());
		
		productPaymentBusinessDO.setOrderId(orderId);
		validateAndSetPaymentPlan(cartBusinessDo, productPaymentBusinessDO);

        // hashkey generation format is amount+orderid+"freecharge"+key
	    String f_string = productPaymentBusinessDO.getAmount() + orderId + "freecharge" + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);
	    String h_key = PaymentUtil.getHashKey(f_string);

	    productPaymentBusinessDO.setBreaker(h_key);
	        
        //If a user has opted out of card storage, then the user should not have the card storage auto selected the next time he/she lands up on the
        //payment page.
        updateUserCardStoragePreference(productPaymentBusinessDO);

		webContext.getWebDOMapper().convertBusinessDOtoWebDO(productPaymentBusinessDO, webContext);
	}

    private boolean isPaymentTypeCreditCard(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return PaymentConstants.PAYMENT_TYPE_CREDITCARD.equals(productPaymentBusinessDO.getPaymenttype());
    }

	/**
	 * This method does data validation and creates payment plan
	 * for the order.
	 * Validation is carried out to ensure that payable amount
	 * is equal to sum of all amounts in the payment plan. Also
	 * validated is amount in wallet < payable amount.
	 * @param cartBusinessDo
	 * @param paymentBusinessDO
	 */
	private void validateAndSetPaymentPlan(CartBusinessDo cartBusinessDo, ProductPaymentBusinessDO paymentBusinessDO) {
	    /*
	     * If cart contains freefund then get rid of partial payment
	     * 
	     */
	    if (freefundService.containsFreeFund(cartBusinessDo) && !freefundService.allPaymentOptionsAllowed(cartBusinessDo)) {
	        logger.info("Cart for Order ID: [" + paymentBusinessDO.getOrderId() + "] contains freefund so settting partial payment to false");
	        paymentBusinessDO.setUsePartialWalletPayment(false);
	    }
	    
	    Double expectedPayAmount = pricingService.getPayableAmount(cartBusinessDo);
	    PaymentPlan paymentPlan = new PaymentPlan();
        paymentPlan.setPlanReferenceId(paymentBusinessDO.getOrderId());
	    
	    if (paymentBusinessDO.getUsePartialWalletPayment()) {
	        logger.info("Partial payment is enabled for Order ID : [" + paymentBusinessDO.getOrderId() + "]");
	        Integer userId = cartBusinessDo.getUserId();
	        Amount walletBalance = walletWrapper.getTotalBalance(userId);
	        
	        int walletBalancePayAmountCompare = walletBalance.getAmount().compareTo(new BigDecimal(expectedPayAmount));
	        
            if (walletBalancePayAmountCompare > 0 || walletBalancePayAmountCompare == 0) {
                String errorString = "Wallet balance is greater than or equal to pay amount for partial payment. WalletBalance : ["
                        + walletBalance.getAmount()
                        + "]. PayAmount : ["
                        + expectedPayAmount + "]";
                logger.error(errorString);
                throw new RuntimeException(errorString);
            }
            
            Amount walletAmount = walletBalance;
            Amount pgAmount = new Amount(expectedPayAmount - walletAmount.getAmount().doubleValue());
            
            paymentPlan.setPgAmount(pgAmount);
            paymentPlan.setWalletAmount(walletAmount);
	    } else {
            logger.info("Partial payment is disabled for Order ID : [" + paymentBusinessDO.getOrderId() + "]");

            paymentPlan.setPgAmount(new Amount(expectedPayAmount));
            paymentPlan.setWalletAmount(Amount.ZERO);
	    }
	    
	    logger.info("About to create payment plan : " + paymentPlan);

        paymentPlanService.createPaymentPlan(paymentPlan);
        

        if (paymentPlan.getPgAmount().getAmount().equals(BigDecimal.ZERO)) {
            logger.info("PG Amount is zero. Setting amount as wallet amount for order ID [" + paymentBusinessDO.getOrderId() + "]");
            paymentBusinessDO.setAmount(paymentPlan.getWalletAmount().getAmount().doubleValue());
        } else {
            logger.info("PG Amount is non-zero. Setting amount as pg amount for order ID [" + paymentBusinessDO.getOrderId() + "]");
            paymentBusinessDO.setAmount(paymentPlan.getPgAmount().getAmount().doubleValue());
        }
	}

    private boolean isPaymentTypeDebitCard(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return PaymentConstants.PAYMENT_TYPE_DEBITCARD.equals(productPaymentBusinessDO.getPaymenttype());
    }

    private boolean checkWhetherUserOptedOutOfCardStorage(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return (isPaymentTypeCreditCard(productPaymentBusinessDO))
                || (isPaymentTypeDebitCard(productPaymentBusinessDO));
    }

    private void updateUserCardStoragePreference(ProductPaymentBusinessDO productPaymentBusinessDO) {
        Integer userId = (Integer) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.SESSION_USER_USERID);
        if (checkWhetherUserOptedOutOfCardStorage(productPaymentBusinessDO) && !cardStorageService.isUserPreferencePresent(userId)) {
            if (productPaymentBusinessDO.getUnCheckedStoreCard()) {
                UserCardStorageChoicePreference userCardStorageChoicePreference = new UserCardStorageChoicePreference();
                userCardStorageChoicePreference.setUserId(userId);
                userCardStorageChoicePreference.setPreference(UserCardStorageChoicePreference.Preference.DO_NOT_CHOOSE);
                cardStorageDAO.insertUserCardStorageChoicePreference(userCardStorageChoicePreference);
            }
        }
    }
}

