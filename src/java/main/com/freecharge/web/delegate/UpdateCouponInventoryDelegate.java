package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.VoucherService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

public class UpdateCouponInventoryDelegate implements WebDelegate {

	@Autowired
    VoucherService voucherService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
		boolean updateStatus = voucherService.updateCouponInventoryNew(baseBusinessDO);
		if (updateStatus) {
			BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO, webContext);
			webContext.setBaseWebDO(baseWebDO);
		} else
			webContext.setBaseWebDO(null);
	}

}
