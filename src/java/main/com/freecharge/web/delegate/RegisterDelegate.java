package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.util.WebConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abc
 * Date: 5/3/12
 * Time: 11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegisterDelegate implements WebDelegate{
    private RegistrationService registrationService;
    @Autowired
    public void setRegistrationService(RegistrationService registrationService){
        this.registrationService=registrationService;
    }
    public void delegate(WebContext webContext) {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        //String registrationStatus=registrationService.doRegistrationService(baseBusinessDO);
        boolean registrationStatus = registrationService.registerOnBothDB(baseBusinessDO);
        if(registrationStatus){
           registrationService.sendSuccessfulRegistrationMail(baseBusinessDO);
           webContext.getModel().addAttribute("registrationStatus","success");
           
        }else {
        	if(((RegisterBusinessDO)baseBusinessDO).getRegistrationStatus().compareToIgnoreCase(WebConstants.EMAIL_ID_EXISTS) == 0)
        	{
        		webContext.getModel().addAttribute("registrationStatus","emailExisted");
        	} else 
        		webContext.getModel().addAttribute("registrationStatus","failed");
        }
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
