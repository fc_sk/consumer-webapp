package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.PaymentService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 28, 2012
 * Time: 12:39:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentOptionListDetailsDelegate implements WebDelegate{
    @Autowired
    private PaymentService paymentService;

    public void delegate(WebContext webContext) throws Exception {
         BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
         paymentService.getPaymentOptionTypeList(baseBusinessDO);
         BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
         webContext.setBaseWebDO(baseWebDO);
    }
}
