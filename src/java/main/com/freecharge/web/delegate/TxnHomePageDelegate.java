package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 17, 2012
 * Time: 6:09:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class TxnHomePageDelegate implements WebDelegate{
    
    @Autowired
    private TxnHomePageService txnHomePageService;
    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        TxnHomePageBusinessDO txnHomePageBusinessDO=txnHomePageService.getTransactionDetails(webContext);
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(txnHomePageBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
