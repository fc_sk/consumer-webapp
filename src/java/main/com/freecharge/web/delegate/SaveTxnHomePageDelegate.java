package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.TxnHomePageWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 18, 2012
 * Time: 3:01:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class SaveTxnHomePageDelegate implements WebDelegate {
    @Autowired
    private TxnHomePageService txnHomePageService;
  
    @Transactional
    public void delegate(WebContext webContext) throws Exception {
        		BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        		txnHomePageService.saveTransactionInfo(baseBusinessDO);
        		BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        		TxnHomePageWebDO txnHomePageWebDO = (TxnHomePageWebDO)baseWebDO;
               //  selectCouponsService.saveTransactionInfo(baseBusinessDO);
               // List<Map<String , Object>> txnCrossSellList = txnHomePageService.getCartItemsList(baseBusinessDO.getLookupID());
               // txnHomePageWebDO.setTxnCrossSellList(txnCrossSellList);
                webContext.setBaseWebDO(txnHomePageWebDO);
    }
}
