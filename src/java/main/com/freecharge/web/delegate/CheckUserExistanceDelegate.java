package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 12, 2012
 * Time: 12:30:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckUserExistanceDelegate  implements WebDelegate{
    private UserAuthService userAuthService;
     @Autowired
    public void setUserAuthService(UserAuthService userAuthService) {
        this.userAuthService = userAuthService;
    }

    public void delegate(WebContext webContext) throws Exception {


BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);

        userAuthService.validateUser(baseBusinessDO);

        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
        
        
    }
}
