package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/2/12
 * Time: 8:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordDelegate implements WebDelegate {

    @Autowired
    private ForgotPasswordService forgotPasswordService;

    public void delegate(WebContext webContext) throws Exception {

        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);

        forgotPasswordService.getPassword(baseBusinessDO);

        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);


    }
}
