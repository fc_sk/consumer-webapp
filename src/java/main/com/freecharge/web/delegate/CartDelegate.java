package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.CartService;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.CartWebDo;

public class CartDelegate implements WebDelegate {

    private CartService cartService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    public void delegate(WebContext webContext) {
        CartWebDo cartWebDo = (CartWebDo)webContext.getBaseWebDO();
        //for remove all items
        //System.out.println("cartWebDo.getIsRemove() = "+cartWebDo.getIsRemove());
//        if(cartWebDo.getIsRemove()!=null && cartWebDo.getIsRemove().equals("yes")){
//            System.out.println("removing====================");
//           cartService.cleanCartItem(cartWebDo.getLookupID());
//        }
//        else if(cartWebDo.getCartItemId()==null){
//            BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(cartWebDo,webContext);
//            Integer itemId = cartService.saveCart(baseBusinessDO);
//            // for set item id in added items on page
//            cartWebDo.setCartItemId(itemId);
//        }else if(cartWebDo.getCartItemId()!=null){
//            System.out.println("]]]]]]]]]]]]="+cartWebDo.getCartItemId());
//               cartService.removeCartItem(cartWebDo.getCartItemId());
//        }
    }
}