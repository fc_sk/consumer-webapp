package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.MyContactsListBusinessDo;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.web.webdo.MyContactsListWebDo;

public class MyContactsListDelegate implements WebDelegate{

	@Autowired
	private MyAccountService myAccountService;
	
	@Override
	public void delegate(WebContext webContext) throws Exception {
		// TODO Auto-generated method stub
		MyContactsListBusinessDo mycontactslistbusinessdo=(MyContactsListBusinessDo)webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(), webContext);
		myAccountService.getMycontacts(mycontactslistbusinessdo);
		MyContactsListWebDo mycontactswebdo=(MyContactsListWebDo)webContext.getWebDOMapper().convertBusinessDOtoWebDO(mycontactslistbusinessdo, webContext);
		webContext.setBaseWebDO(mycontactswebdo);
	}

}
