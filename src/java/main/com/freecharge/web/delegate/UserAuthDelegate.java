package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

public class UserAuthDelegate implements WebDelegate {


   @Autowired
    private UserAuthService userAuthService;

    public void setUserAuthService(UserAuthService userAuthService) {
        this.userAuthService = userAuthService;
    }

    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        userAuthService.doLogin(baseBusinessDO);
        LoginBusinessDO loginBusinessDO=(LoginBusinessDO) baseBusinessDO;
        if(loginBusinessDO.isLogin()){
        	userAuthService.doUserProfile(baseBusinessDO);
        }
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);


    }
}