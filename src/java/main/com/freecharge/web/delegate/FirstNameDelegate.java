package com.freecharge.web.delegate;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.businessdo.FirstNameBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 22, 2012
 * Time: 5:39:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class FirstNameDelegate implements WebDelegate{
    @Autowired
    private RegistrationService registrationService;
    public void delegate(WebContext webContext) throws Exception {
        BaseBusinessDO baseBusinessDO = webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);
        Integer value=registrationService.saveFirstName(baseBusinessDO);
        if(value != 0){
            ((FirstNameBusinessDO)baseBusinessDO).setStatus("success");
            //webContext.getModel().addAttribute("status","success");
        }else{
            ((FirstNameBusinessDO)baseBusinessDO).setStatus("fail");
            //webContext.getModel().addAttribute("status","fail");
        }
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(baseBusinessDO,webContext);
        webContext.setBaseWebDO(baseWebDO);
    }
}
