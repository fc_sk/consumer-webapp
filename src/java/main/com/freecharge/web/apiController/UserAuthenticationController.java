package com.freecharge.web.apiController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.responsedo.UserAuthLoginResponseDO;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.UserLoginDetailsWebDO;

@Controller
@RequestMapping("/userAuthenticate/*")
public class UserAuthenticationController {
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
    private MetricsClient              metricsClient;
	
/*	@RequestMapping(value = "login", method = RequestMethod.POST)
	public 
	String userLoginDelegate(@ModelAttribute(value = "userLoginDetailsWebDO") UserLoginDetailsWebDO userLoginDetailsWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
	    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "click");
	    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "loginUserAuth");
		// Remove the userLoginDetailsWebDO Object attached to the model,
		// so that its not returned as part of response.
		model.addAttribute("userLoginDetailsWebDO", null);
		try {

			response.setContentType("application/json");
			
			ValidationUtils.rejectIfEmpty(bindingResult, "email", "User can not be empty.");
			ValidationUtils.rejectIfEmpty(bindingResult, "password", "Password can not be empty");

			if (bindingResult.hasErrors()) {
			    
				if (bindingResult.getFieldError("email") != null) {
					model.addAttribute("STATUS", "ERROR_EMAIL");
					model.addAttribute("RESULT", "Email is missing");
				}
				else if (bindingResult.getFieldError("password") != null){
					model.addAttribute("STATUS", "ERROR_PASSWORD");
					model.addAttribute("RESULT", "Password is missing");				
				}
				else {
					model.addAttribute("STATUS", "ERROR");
					model.addAttribute("RESULT", "Please provide valid email and password.");
				}
			}
			else {
				String delegateName = WebConstants.DELEGATE_BEAN_USER_LOGIN;
				
				WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, userLoginDetailsWebDO);
				
				userLoginDetailsWebDO = (UserLoginDetailsWebDO) webContext.getBaseWebDO();
				boolean isLogin = userLoginDetailsWebDO.isLoggedIn();
					
				UserAuthLoginResponseDO userAuthLoginResponseDO = (UserAuthLoginResponseDO) webContext.getBaseResponseDO();
		
				if (isLogin) {
					
					model.addAttribute("STATUS", "SUCCESS");
					model.addAttribute("RESULT", userAuthLoginResponseDO);
					metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "success");
					long endTime = System.currentTimeMillis();
			        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "login", endTime - startTime);
					return "jsonView";
					 
				} else {

					model.addAttribute("STATUS", "ERROR");
					model.addAttribute("RESULT", "Please provide valid email and password.");
				}
			}
			
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in userLoginDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
			return "jsonView";
		} catch (Exception exp) {
			logger.error(" Exception in userLoginDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
			return "jsonView";
		}
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
		return "jsonView";
	}*/

}