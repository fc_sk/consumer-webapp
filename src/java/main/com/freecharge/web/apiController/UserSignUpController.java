package com.freecharge.web.apiController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.UserDO;

@Controller
@RequestMapping("/userAuthenticate/*")
public class UserSignUpController {

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private Validator validator;
	
	@Autowired
    private MetricsClient              metricsClient;

/*	@Csrf
	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public 
	String userSignUpDelegate(@ModelAttribute(value = "userDO") UserDO userDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "userSignupController");
		// Remove the userDO Object attached to the model,
		// so that its not returned as part of response.
		model.addAttribute("userDO", null);
		response.setContentType("application/json");
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_REGISTER_NOSESSION;
			
			//Check if valid userId is present. Register user with the same userId.
			*//* TODO: Not the desired way.
			 * Ideally userId should be returned from the new system and same should 
			 * be inserted in old DB.
			 * This behavior should change when we go live with new website. 
			*//*
*//*			if (userDO.getUserId() == null || userDO.getUserId() <=0) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "UserId is missing"); 
				return "jsonView";
			}*//*
			validator.validate(userDO,bindingResult);
			
			if (bindingResult.hasErrors()) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "Required fields missing"); 
				return "jsonView";
	        }
		
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, userDO);
			userDO = (UserDO) webContext.getBaseWebDO();
			
			
			model.addAttribute("RESULT", userDO.getRegistrationStatus());
			
*//*			UserSignUpResponseDO userSignUpResponseDO = (UserSignUpResponseDO) webContext.getBaseResponseDO();
			
			boolean status = userSignUpResponseDO.isRegistrationStatus();
			
			if(status == false){
				logger.error("User Registration failed for : "+userDO.getEmail()+" Reason: " + userDO.getRegistrationStatus().toString());
				model.addAttribute("STATUS", "FAIL");
				model.addAttribute("RESULT", userDO.getRegistrationStatus()); 
				
			}else{
				
				model.addAttribute("STATUS", "SUCCESS");
				model.addAttribute("RESULT", userSignUpResponseDO); 			
				
				logger.info("Successfully Registered user : " + userDO.getEmail());
			}*//*
			
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in userSignUpDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Exception in userSignUpDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		}
		return "jsonView";
	}*/
}
