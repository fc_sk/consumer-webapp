package com.freecharge.web.apiController;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.MyContactsListWebDo;
import com.freecharge.web.webdo.MyRechargeContactProfileWebDO;
import com.freecharge.web.webdo.ProfileListWebDO;
import com.freecharge.web.webdo.ProfileWebDO;

@Controller
@RequestMapping("/userProfile/*")
public class UserMyProfileController {

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private Validator validator;
	
	@Autowired
    private MetricsClient              metricsClient;

    @RequestMapping(value = "myProfile", method = RequestMethod.POST)
	
	public String getMyProfileDelegate(@ModelAttribute(value = "profileWebDO") ProfileWebDO profileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "umpcGetMyProfileDelegate");

		model.addAttribute("profileWebDO", null);
		try {
		    
			response.setContentType("application/json");


			if (profileWebDO.getUserid() == null || profileWebDO.getUserid() <= 0) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "UserId is required."); 
				return "jsonView";
	        }
			
			String delegateName = WebConstants.DELEGATE_BEAN_USER_MY_PROFILE;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			ProfileListWebDO profileListWebDO = (ProfileListWebDO) webContext.getBaseWebDO();
	
			if (profileListWebDO.getProfileWebDOList() == null || profileListWebDO.getProfileWebDOList().isEmpty()) {
				model.addAttribute("STATUS","ERROR");
				model.addAttribute("RESULT","No default profile exists for user.");
				return "jsonView";
			}
				
			model.addAttribute("STATUS","SUCCESS");
			model.addAttribute("RESULT",profileListWebDO);
			
			
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in getMyProfileDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Exception in getMyProfileDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		}
		
		
		return "jsonView";
	}

	@RequestMapping(value = "updateDefaultprofile", method = RequestMethod.POST)
	public String updateDefaultProfileDelegate(@ModelAttribute(value = "profileWebDO") ProfileWebDO profileWebDO,BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
		model.addAttribute("profileWebDO", null);
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userMyProfileController2");
			boolean errorStatus = false;
			String errorMessage = null;
			
			response.setContentType("application/json");
			
			if (profileWebDO.isIspasswordupdate() && !profileWebDO.isIsoldpswdcorrect()) {
				errorStatus = true;
				errorMessage = "Old Password is incorrect.";
			}

			Integer userId = profileWebDO.getUserid();
			String email = profileWebDO.getEmail();
			String mobileNo = profileWebDO.getMobileNo();
			
			if ( userId == null || userId <=0 || email == null || email.isEmpty() || mobileNo == null || mobileNo.isEmpty()) {
				errorStatus = true;
				errorMessage = "Required fields are missing."; 	
			}

			if (errorStatus) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", errorMessage);
				return "jsonView";
			}
			
			// Set DOB value from the Provided String
			if (profileWebDO.getDobString() != null && !profileWebDO.getDobString().isEmpty()) {
				Date dob = (new SimpleDateFormat("dd-MM-yyyy")).parse(profileWebDO.getDobString());
				profileWebDO.setDob(dob);
			}
						
			// Update default User Profile Information
			String delegateName = WebConstants.DELEGATE_BEAN_UPDATE_DEFAULT_PROFILE;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			
			profileWebDO = (ProfileWebDO) webContext.getBaseWebDO();
			if (profileWebDO == null) {
				model.addAttribute("STATUS","ERROR");
				model.addAttribute("RESULT",null);
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
				return "jsonView";
			}
			
			profileWebDO.setCitylist(null);
			profileWebDO.setStatelist(null);
			model.addAttribute("STATUS","SUCCESS");
			model.addAttribute("RESULT",profileWebDO);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in updateDefaultProfileDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Error in updateDefaultProfileDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return "jsonView";
	}
	
	@RequestMapping(value = "myRechargeContactProfile", method = RequestMethod.POST)
	public String myRechargeContactProfileDelegate(@ModelAttribute(value = "myRechargeContactProfileWebDO") MyRechargeContactProfileWebDO myRechargeContactProfileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
		model.addAttribute("myRechargeContactProfileWebDO", null);
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userMyProfileController3");
			response.setContentType("application/json");
			
			validator.validate(myRechargeContactProfileWebDO, bindingResult);

			if (bindingResult.hasErrors()) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "Required fields are missing."); 
				return "jsonView";
	        }
			
			String delegateName = WebConstants.DELEGATE_BEAN_MY_RCHG_CONTACT_PROFILE;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, myRechargeContactProfileWebDO);
			myRechargeContactProfileWebDO = (MyRechargeContactProfileWebDO) webContext.getBaseWebDO();
			if (myRechargeContactProfileWebDO == null) {
				model.addAttribute("STATUS","ERROR");
				model.addAttribute("RESULT","Either the user does not exist or no default profile exists for user.");
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
				return "jsonView";
			}
			model.addAttribute("STATUS","SUCCESS");
			model.addAttribute("RESULT",myRechargeContactProfileWebDO);
			
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in myRechargeContactProfileDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Exception in myRechargeContactProfileDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return "jsonView";
	}
	
	@RequestMapping(value = "updateprofile", method = RequestMethod.POST)
	public String updateProfileDelegate(@ModelAttribute(value = "myRechargeContactProfileWebDO") MyRechargeContactProfileWebDO myRechargeContactProfileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
		model.addAttribute("myRechargeContactProfileWebDO", null);
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userMyProfileController1");
			String delegateName = WebConstants.DELEGATE_BEAN_UPDATE_RCHG_CONTACT_PROFILE;
			
			response.setContentType("application/json");
			
			validator.validate(myRechargeContactProfileWebDO,bindingResult);

			if (bindingResult.hasErrors()) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "Required fields are missing."); 
				return "jsonView";
	        }
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, myRechargeContactProfileWebDO);
			myRechargeContactProfileWebDO = (MyRechargeContactProfileWebDO) webContext.getBaseWebDO();
			
			if (myRechargeContactProfileWebDO == null) {
				model.addAttribute("STATUS","ERROR");
				model.addAttribute("RESULT",null);
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
				return "jsonView";
			}
			
			model.addAttribute("STATUS","SUCCESS");
			model.addAttribute("RESULT",myRechargeContactProfileWebDO);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
            
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in updateProfileDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Error in updateProfileDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
			return "jsonView";
		}
		
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return "jsonView";
	}
	
	@RequestMapping(value = "checkoldpswd", method = RequestMethod.POST)
	public String  checkUseOlderPasswordDelegate(@ModelAttribute(value = "profileWebDO") ProfileWebDO profileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED,
                "umpcCheckUseOlderPasswordDelegate");
		model.addAttribute("profileWebDO", null);
		System.out.println(" UserId : " + profileWebDO.getUserid() + " PWD : " + profileWebDO.getOldpassword());
		try{
			response.setContentType("application/json");
			
			ValidationUtils.rejectIfEmpty(bindingResult, "userid", "UserId can not be empty.");
			ValidationUtils.rejectIfEmpty(bindingResult, "email", "UserId can not be empty.");
			ValidationUtils.rejectIfEmpty(bindingResult, "oldpassword", "UserId can not be empty.");
			
			if (bindingResult.hasErrors()) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "Required fields are missing."); 
				return "jsonView";
	        }
			
			String delegateName = WebConstants.DELEGATE_BEAN_OLDPASSWORD_CHECK;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			profileWebDO=((ProfileWebDO)(webContext.getBaseWebDO()));

			boolean ispswdcorrect=profileWebDO.isIsoldpswdcorrect();
			String status = "ERROR";
			if (ispswdcorrect)
				status = "SUCCESS";
				
			model.addAttribute("STATUS", status);
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in checkUseOlderPasswordDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Error in checkUseOlderPasswordDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		}		
		return "jsonView";
	}
	
	@RequestMapping(value = "myRechargeContacts", method = RequestMethod.POST)
	public String getMyRechargeContactsDelegate(@ModelAttribute(value = "myContactsListWebDO") MyContactsListWebDo myContactsListWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		
		model.addAttribute("myContactsListWebDO", null);
		try {

			response.setContentType("application/json");
			
			validator.validate(myContactsListWebDO,bindingResult);

			if (bindingResult.hasErrors()) {
				model.addAttribute("STATUS", "ERROR");
				model.addAttribute("RESULT", "Required fields are missing."); 
				return "jsonView";
	        }

			String delegateName = WebConstants.DELEGATE_MY_RECHARGE_CONTACTS;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, myContactsListWebDO);
			myContactsListWebDO = (MyContactsListWebDo) (webContext.getBaseWebDO());
			
			if (myContactsListWebDO.getContactslist() == null || myContactsListWebDO.getContactslist().isEmpty()) {
				model.addAttribute("STATUS","ERROR");
				model.addAttribute("RESULT","No Recharge Contacts exists for user.");
				return "jsonView";
			}
			
			model.addAttribute("STATUS","SUCCESS");
			model.addAttribute("RESULT",myContactsListWebDO);
			
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in getMyRechargeContactsDelegate : ", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Exception in getMyRechargeContactsDelegate : ", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			
			return "jsonView";
		}
		
		return "jsonView";
	}
}
