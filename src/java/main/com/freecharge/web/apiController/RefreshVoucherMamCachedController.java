package com.freecharge.web.apiController;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.service.VoucherService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.RechargeSchedulerService;

@Controller
@RequestMapping("/admin/refresh/*")
public class RefreshVoucherMamCachedController {
	
	private static Logger logger = LoggingFactory.getLogger(RefreshVoucherMamCachedController.class);
	
	@Autowired
	private VoucherService voucherService;
	
	@Autowired
    RechargeSchedulerService rechargeSchedulerService;
	
	@RequestMapping(value = "coupon", method = RequestMethod.GET)
    @NoLogin
	public String adminLogin() {
		logger.debug("Initiate refresh coupon memcached ");
		voucherService.refreshAllActiveCoupons();
		return "jsonView";
	}


	/*@RequestMapping(value = "schedular", method = RequestMethod.GET)
	public String adminSchedular(HttpServletRequest request, HttpServletResponse response,  Model model) {
		if(request.getParameter("request_id")!=null)
		{	String requestID = request.getParameter("request_id");
			Long requestId = Long.parseLong(requestID);
			String statusCode = rechargeSchedulerService.getRetryRechargeStatus(requestId);
			model.addAttribute("statusCode", statusCode);
}
		return null;
	}*/


}
