package com.freecharge.web.apiController;

import com.freecharge.campaign.client.CampaignServiceClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by surya on 12/01/16.
 */
@Service
public class CampaignFrameworkService {

    private Logger logger = Logger.getLogger(getClass());

    @Autowired
    CampaignServiceClient campaignServiceClient;

    public List<String> getAllowedPGListFromNewCF(Integer freefundClassId) {
        try {
            return campaignServiceClient.getPaymentTypesForFreeFundClassId(freefundClassId);
        } catch (Exception e) {
            logger.info("Exception in fetching PG List from campaign service. ", e);
            return null;
        }
    }
}
