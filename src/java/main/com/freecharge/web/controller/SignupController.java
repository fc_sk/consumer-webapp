package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.json.JsonResponse;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.tracker.Tracker;
import com.freecharge.useragent.UserLoginEventPublisherHelper;
import com.freecharge.util.TrackerEvent;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.HomeWebDo;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.StateMasterWebDO;
import com.freecharge.web.webdo.UserDO;

@Controller
@RequestMapping("/app/*")
public class SignupController {
    public static final String METRIC_SERVICE = "signUp";
    public static final String METRIC_ACCESS = "signUpAccess";

	@Autowired
	private Validator validator;

	@Autowired
	private FCProperties fcproperties;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private UserLoginEventPublisherHelper userLoginEventPublisherHelper;

    @Autowired
    private Tracker tracker;

	private Logger logger = LoggingFactory.getLogger(getClass());

    public static final String USER_ADD_LINK = "/app/signup.htm";

/*    @Csrf
	@RequestMapping(value = "signup", method = RequestMethod.GET)
	@NoLogin
	public ModelAndView signUp( HttpServletRequest request, HttpServletResponse response, Model model) {
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "signupController");
        String view = "home/signup";
		String pageTitle = "FreeCharge Sign up";
		tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_ATTEMPT.createTrackingEvent());
		// Setting freecharge home in model
		     try {
             String delegateName = WebConstants.DELEGATE_STATE_MASTER;
        	WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new StateMasterWebDO());
			model.addAttribute("loginWebDO", new LoginWebDO());
			model.addAttribute("stateMasterList", ((StateMasterWebDO) webContext.getBaseWebDO()).getStateMaster());

		    model.addAttribute("userDO", new UserDO());
			model.addAttribute("title", pageTitle);
			model.addAttribute("loginWebDO", new LoginWebDO());
			tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_SUCCESS.createTrackingEvent());
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
		} catch (Exception exp) {
			logger.error("Any Exception that could come", exp);
			return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
		}

		return new ModelAndView(view);
	}

	@RequestMapping(value = "adduser", method = RequestMethod.POST)*//*	@RequestMapping(value = "is-logged-in")
	public @ResponseBody Boolean isLoggedIn(MobileLogin login, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mlcIsLoggedIn");
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		Boolean isLoggedIn = false;
		if (fs != null) {
			Map<String, Object> sessionData = fs.getSessionData();
			if (sessionData != null){
				Object isLoggedInObj = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
				if (isLoggedInObj != null){
					isLoggedIn = (Boolean) isLoggedInObj;
				}
			}
		}
		return isLoggedIn;
	}*//*
    @NoLogin
	public @ResponseBody
	String registration(@ModelAttribute(value = "userDO") UserDO userDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        metricsClient.recordEvent(METRIC_SERVICE, METRIC_ACCESS, "dAttempt");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "click");
        boolean success = false;
        long startTime = System.currentTimeMillis();
		JsonResponse jsonResponse = new JsonResponse();
		JSONArray jsonArray = new JSONArray();
		String pageTitle = "FreeCharge Sign up";
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_REGISTER;
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

            //Copying confirmPassword field to password field as new signup form does not have confirmPassword field
            //So to avoid validation failure
            userDO.setConfirmPassword(userDO.getPassword());

             validator.validate(userDO,bindingResult);
             if(!userDO.getPassword().equals(userDO.getConfirmPassword())){
            	 bindingResult.rejectValue("password", "Password and confirm password fields should match");
             }
             if (bindingResult.hasErrors()) {
				StringBuffer error = new StringBuffer("");
				for(ObjectError errorObject:bindingResult.getAllErrors()){
					if(errorObject.getDefaultMessage()!=null){
						error.append(errorObject.getDefaultMessage()).append("\n");
					}else{
						error.append(errorObject.getCode()).append("\n");
					}
				}
				jsonResponse.setStatus("validationfail");
				jsonResponse.setResult(error.toString());
				jsonArray.add(jsonResponse);
				return  jsonArray + "";
            }
             *//* Check if another user is already logged in *//*

             if (FCSessionUtil.isAnotherUserLoggedIn(userDO.getEmail())) {
            	 jsonResponse.setStatus("ALREADY_LOGGEDIN");
            	 jsonResponse.setResult("Another user is already logged in from the same browser. To continue using this account, you will have to sign out the other user and login in again.This is done to protect your account and to ensure the privacy of your information.");
            	 jsonArray.add(jsonResponse);
            	 return jsonArray + "";

             }
			userDO.setAffiliate_unique_id(String.valueOf(FCConstants.AFFILIATE_ID_WEB));
            WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, userDO);
			String status = null;
			if(webContext.getModel().asMap().get("registrationStatus")!=null){
				status = webContext.getModel().asMap().get("registrationStatus").toString();
			}
			if(status!=null && status.equalsIgnoreCase("emailExisted")){
				logger.error("Your requested "+userDO.getEmail()+" already exist with us.so cannot registered ");
				jsonResponse.setStatus("fail");
				jsonResponse.setResult("Your requested email id already exist with us");
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "fail");
			}else if (status !=null && status.equalsIgnoreCase("failed")) {
				logger.error("Your requested "+userDO.getEmail()+" already exist with us.so cannot registered ");
				jsonResponse.setStatus("fail");
				jsonResponse.setResult("Technical Error: Please try again.");
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "fail");
			} else {
				model.addAttribute("registrationStatus", ((UserDO) (webContext.getBaseWebDO())).getRegistrationStatus());
				//fs.setLogin(((UserDO) (webContext.getBaseWebDO())).getIsLoggedin());
				//fs.setSessionData(((UserDO) (webContext.getBaseWebDO())).getSessionData());
				model.addAttribute("homeWebDo", new HomeWebDo());
				model.addAttribute("loginWebDO", new LoginWebDO());
				model.addAttribute("title", pageTitle);
				logger.info("user registered Successfully with Email Id" + userDO.getEmail());
				jsonResponse.setStatus("success");
				jsonResponse.setResult("user registered Successfully with Email Id " + userDO.getEmail());

                userLoginEventPublisherHelper.publish(request, this);
                metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "success");
                success = true;
			}
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			jsonResponse.setStatus("fail");
			jsonResponse.setResult("Technical Error: Please try again.");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
		} catch (Exception exp) {
			logger.error("Unable to Register User ");
			logger.error("Any Exception that could come", exp);
			jsonResponse.setStatus("fail");
			jsonResponse.setResult("Technical Error: Please try again.");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
		}
		jsonArray.add(jsonResponse);

        if (success) {
            metricsClient.recordEvent(METRIC_SERVICE, METRIC_ACCESS, "dSuccess");
        } else {
            metricsClient.recordEvent(METRIC_SERVICE, METRIC_ACCESS, "dFailure");
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "signup", endTime - startTime);
		return  jsonArray + "";
	}*/
}
