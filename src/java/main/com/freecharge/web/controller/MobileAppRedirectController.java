package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/app/FcAppStatus/*")
public class MobileAppRedirectController {

    @RequestMapping(value = "{paymentResult}/{orderId}/{isAddCash}/result")
    public @ResponseBody
    String saveCart(HttpServletRequest request, HttpServletResponse response, Model model,
            @PathVariable String paymentResult, @PathVariable String orderId, @PathVariable String isAddCash) {
        return null;

    }
}
