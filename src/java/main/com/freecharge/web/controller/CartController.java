package com.freecharge.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.coupon.service.CouponServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.coupon.service.web.model.BlockedCouponResponse;
import com.freecharge.api.coupon.service.web.model.CouponBlockError;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.rest.coupon.model.CouponsCartRes;
import com.freecharge.rest.coupon.model.SaveCartItemsRequestData;
import com.freecharge.rest.coupon.model.SaveCouponReq;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CartWebDo;
import java.util.ArrayList;

@Controller
@RequestMapping("/app/*")
public class CartController {
	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private CouponServiceProxy couponServiceProxy;

	@Autowired
	private CartService cartService;

	@Autowired
	private CouponService couponService;

	@Value("${coupon.fulfilment.pool.size}")
	private Integer poolSize;
	@Autowired
	private OrderIdWriteDAO orderIdDAO;
	
	@Autowired
    private MetricsClient metricsClient;

	@RequestMapping(value = "savecart", method = RequestMethod.POST)
	public @ResponseBody String saveCart(@ModelAttribute(value = "cartWebDo") CartWebDo cartWebDo,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		String cartItemId = "0";
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_CART;
			cartWebDo.setOrderId(8);
			// cartWebDo.setProductId(1);
			cartWebDo.setUserId(4);

			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, cartWebDo);
			// cartItemId = cartWebDo.getCartItemId() + "";
			model.addAttribute("lookupID", cartWebDo.getLookupID());
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.error("Any Exception that could come", exp);
			// currently to the same view but have to be redirected to error
			// views.
		}
		return cartItemId;
	}

	@RequestMapping(value = "save-coupons-in-cart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> saveCartItems(@RequestBody SaveCartItemsRequestData saveCartItemsRequestData) {
		final Map<String, Object> result = new HashMap<>();
		long startTime = System.currentTimeMillis();
		if (saveCartItemsRequestData == null || saveCartItemsRequestData.getOrderId() == null || saveCartItemsRequestData.getSaveCouponReqs() == null) {
			logger.info("Please pass all params ");
			result.put("status", false);
			result.put("errorMessage", "REQUEST_EMPTY");
			result.put("errorCode", "EMPTY_REQUEST");
			return result;
		}

		if (saveCartItemsRequestData.getSaveCouponReqs().size() < 0) {
			logger.info("Coupons list are empty");
			result.put("status", false);
			result.put("errorMessage", "Request does not have coupons");
			result.put("errorCode", "NO_COUPONS_IN_REQUEST");
			return result;
		}
		logger.info("Started savecartItems for = " + saveCartItemsRequestData.getOrderId());
		UserTransactionHistory userTransactionHistory = couponService.findUserTransactionHistoryByOrderId(saveCartItemsRequestData.getOrderId());

		if (userTransactionHistory == null) {
			logger.info("No user transaction history found for = " + saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("errorMessage", "User transaction history is not available");
			result.put("errorCode", "NO_TXN_HISTORY_FOR_ORDERID");
			return result;
		}

		logger.info("UserId in session = " + FCSessionUtil.getLoggedInUserId().intValue() + ", for orderId = " + saveCartItemsRequestData.getOrderId());
		logger.info("UserId in UTH = " + userTransactionHistory.getFkUserId() + ", for orderId = " + saveCartItemsRequestData.getOrderId());

		if(userTransactionHistory.getFkUserId().intValue() != FCSessionUtil.getLoggedInUserId().intValue()){
			logger.error("Session userId not matched for orderId " + saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("errorCode", "USER_ID_NOT_MATCHED");
			result.put("errorMessage", "user not matched");
			return result;
		}

		if (!couponService.isSuccessTransaction(userTransactionHistory)) {
			logger.error("Transaction status not success for given orderId " + saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("errorMessage", "Transaction status not success for given orderId");
			result.put("errorCode", "ORDERID_TXN_NOT_SUCCESS");
			return result;
		}

		if (!couponService.isValidOrderId(userTransactionHistory)) {
			logger.error("OrderId is not valid " + saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("errorMessage", "OrderId is expired");
			result.put("errorCode", "ORDER_ID_EXPIRED");
			return result;
		}

		Integer txnBucketLimit =  couponService.getBucketLimitOfTransaction(saveCartItemsRequestData.getOrderId(), userTransactionHistory);
		Map<String, String> couponDataMap = new HashMap<>();
		couponDataMap.put("bucketLimit", String.valueOf(txnBucketLimit));

		logger.info("Transaction bucket limit = " + txnBucketLimit + " , orderId = " + saveCartItemsRequestData.getOrderId());

		Map<String, Object> allowedCouponAmountAndQuantityMap = couponService.getAllowedAmountAndQuantityByCouponTracker(saveCartItemsRequestData.getOrderId(), txnBucketLimit);
		Integer allowedCouponAmount = (Integer) allowedCouponAmountAndQuantityMap.get("allowedAmountRange");
		Integer allowedQuantity = (Integer) allowedCouponAmountAndQuantityMap.get("allowedCouponQuantity");

		logger.info("Allowed amount for orderId "+ saveCartItemsRequestData.getOrderId() + " is " + allowedCouponAmount);
		logger.info("Allowed quantity for orderId " + saveCartItemsRequestData.getOrderId() + " is " + allowedQuantity);

		if (allowedCouponAmount < 1 || allowedQuantity < 1) {
			logger.info("Coupon bucket range or allowed quantity already used for order id "+saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("errorMessage", "Coupons already used bucket amount or quantity this order id.");
			result.put("errorCode", "BUCKET_AMOUNT_OR_QUANTITY_CROSSED");
			return result;
		}

		boolean isRequestCrossedBucketRange  = isRequestCouponsCrossedBucketOrQuantity(saveCartItemsRequestData.getSaveCouponReqs(), allowedCouponAmount, allowedQuantity);
		if (isRequestCrossedBucketRange) {
			logger.info("Requested coupons amount and quantity crossed allowed amount = "+ allowedCouponAmount +" or allowed quantity = "+ allowedQuantity + ", for orderId = "+ saveCartItemsRequestData.getOrderId());
			result.put("status", false);
			result.put("allowed_amount", allowedCouponAmount);
			result.put("allowed_quantity", allowedQuantity);
			result.put("errorMessage", "Requested coupon amount crossed remaining bucket-amount");
			result.put("errorCode", "REQUESTED_COUPONS_CROSSED_BUCKET");
			return result;
		}
	
		CouponsCartRes couponsCartRes = null;
		final CouponsCartRes couponsCartRes1 = couponsCartRes;
		try {
			couponsCartRes = cartService.saveCouponsInCart(saveCartItemsRequestData);
		} catch (Exception exp1) {
			logger.error("Exception occurred inside catch while saving cart item: ", exp1);
			result.put("status", false);
			result.put("message", "A problem occurred while saving coupons. Please try after some time.");
			return result;
		}

		if (couponsCartRes == null || couponsCartRes.getCouponCartItems() == null || couponsCartRes.getCouponCartItems().size() < 1) {
			result.put("status", false);
			result.put("message", "Cart is expired or all CouponIds already used");
			result.put("errorCode", "CART_ITEM_CREATION_FAILED");
			return result;
		}
		
		logger.info("cartService saveCart response");
		for (CouponCartItem cci : couponsCartRes.getCouponCartItems()) {
			logger.info("saveCart passing couponId" + cci.getCouponId() + "getCouponOptInType=" + cci.getType());
		}

		result.put("status", false);
		if (couponsCartRes.getStatus()) {
			result.put("status", true);
			final List<CouponCartItem> couponCartItems = couponsCartRes.getCouponCartItems();
			final String orderId = couponsCartRes.getOrderId();
			try {
				BlockedCouponResponse blockedCouponResponse	= couponService.doAllTypeCouponFulfilment(couponCartItems, orderId, couponDataMap);
				if (blockedCouponResponse == null) {
					logger.error("BlockedCouponResponse is null " + orderId);
					deleteAllCartItems(orderId);
					result.put("status", false);
					result.put("errorMessage", "BlockedCouponResponse is null");
					result.put("errorCode", "FULFILMENT_RESPONSE_IS_NULL");
					return result;
				}
				
				if (blockedCouponResponse.getBlockedCoupons() == null || blockedCouponResponse.getBlockedCoupons().size() == 0) {
					if (blockedCouponResponse.getErrors() == null || blockedCouponResponse.getErrors().size() == 0) {
						logger.error("BlockedCouponResponse fileds are null " + orderId);
						deleteAllCartItems(orderId);
						result.put("errorMessage", "BlockedCouponResponse fileds are null");
						result.put("errorCode", "FULFILMENT_RESPONSE_IS_NULL");
						result.put("status", false);
						return result;
					}
				}
				
				List<Integer> blockedCouponCodeIdList = new ArrayList<Integer>(blockedCouponResponse.getBlockedCoupons().keySet());
				List<Integer> errorCouponCodeList = new ArrayList<>();
				result.put("blockedCouponIds", blockedCouponCodeIdList);
				
				if (blockedCouponResponse != null && blockedCouponResponse.getErrors() != null) {
					for (CouponBlockError coupon : blockedCouponResponse.getErrors()) {
						errorCouponCodeList.add(coupon.getCouponId());
					}
				}
				result.put("errorCouponIds", errorCouponCodeList);
				if (errorCouponCodeList != null && (errorCouponCodeList.size() > 0)) {
					deleteCouponFromCartItems(orderId, errorCouponCodeList);
				}
			} catch (Exception e){				
				logger.error("Error in coupon fulfillment for orderId: " + orderId, e);
				result.put("status", false);
				result.put("errorMessage", "Exception while calling fullfilment");
				result.put("errorCode", "EXCEPTION_CALLING_FULFILMENT");
				deleteAllCartItems(orderId);
			}
		}
		
		metricsClient.recordLatency("CouponService", "saveCartItems", System.currentTimeMillis() - startTime);
		return result;
	}

	@Transactional
	private void deleteAllCartItems(String orderId) {
		String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
		CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
		if (cartBusinessDo == null) {
			logger.error("Cart is null for orderId ="+ orderId + ", lookupId = "+ lookupId);
			return;
		}
		logger.info("cartBusinessDo cart Id = " + cartBusinessDo.getId());
		for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo.getCartItems()) {
			if (cartItemsBusinessDO.getEntityId() != null) {
				if (cartItemsBusinessDO.getEntityId().equalsIgnoreCase("post_txn_coupon")) {
					logger.info("CouponId to delete = " + cartItemsBusinessDO.getItemId());
					if (cartItemsBusinessDO.getId() != null) {
						cartService.removeCartItem(cartItemsBusinessDO.getId().intValue());
					}
				}
			}
		}
	}
	
	@Transactional
	private void deleteCouponFromCartItems(String orderId, List<Integer> errorCouponCodeList) {
		String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
		CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
		if (cartBusinessDo == null) {
			logger.error("Cart is null for orderId ="+ orderId + ", lookupId = "+ lookupId);
			return;
		}
		logger.info("cartBusinessDo cart Id = " + cartBusinessDo.getId());
		for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo.getCartItems()) {
			if (cartItemsBusinessDO.getEntityId() != null) {
				if (cartItemsBusinessDO.getEntityId().equalsIgnoreCase("post_txn_coupon")
						&& errorCouponCodeList.contains(cartItemsBusinessDO.getItemId())) {
					logger.info("CouponId to delete = " + cartItemsBusinessDO.getItemId());
					cartService.removeCartItem(cartItemsBusinessDO.getId().intValue());
				}
			}
		}
	}

	private boolean isRequestCouponsCrossedBucketOrQuantity(List<SaveCouponReq> saveCouponReqList, Integer allowedCouponAmount, Integer allowedCouponQuantity) {		
		Integer reqCouponAmounts = 0;
		Integer reqCouponCounts = 0;

		for (SaveCouponReq re : saveCouponReqList) {
			CouponStore couponStore =  couponServiceProxy.getCouponStore(re.getCouponId());
			reqCouponAmounts = reqCouponAmounts + couponStore.getCouponValue();
			reqCouponCounts = reqCouponCounts + 1;
		}
		if ((reqCouponAmounts > allowedCouponAmount) || (reqCouponCounts > allowedCouponQuantity)) {
			return true;
		}
		return false;
	}

	@RequestMapping(value = "cleancart", method = RequestMethod.POST)
	public void cleanCart(@ModelAttribute(value = "cartWebDo") CartWebDo cartWebDo, BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			// cartWebDo.setOrderId(3);
			String delegateName = WebConstants.DELEGATE_BEAN_CART;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, cartWebDo);
			model.addAttribute("lookupID", cartWebDo.getLookupID());
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.error("Any Exception that could come", exp);
			// currently to the same view but have to be redirected to error
			// views.
		}
	}
}
