package com.freecharge.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 7/9/12
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/app/*")
public class FreeChargeDummyPaymentController {

    @RequestMapping(value = "freechargedummypayment", method = RequestMethod.POST)
    public String foo(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam Map<String, String> requestMapping) {
        String data = requestMapping.get("msg");

        String orderId;
		String amount;
        
        if(null == data || data.isEmpty()) {
        	amount = requestMapping.get("Amount");
        	orderId = requestMapping.get("Order_Id");
        } else {
            String[] datum = data.split("\\|");
            orderId = datum[1];
    		amount = datum[3];
        }

		return "redirect:/payment/handlefreechargedummyresponse.htm?Order_Id=" + orderId+"&Amount="+amount+"&AuthDesc=Y&nb_order_no=123456";
    }
}
