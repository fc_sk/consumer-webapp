package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.exception.CouponTamperedException;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.TxnCrossSellWebDo;
import com.freecharge.web.webdo.TxnFulFilmentWebDo;

@Controller
@RequestMapping("/app/*")
public class CrossSellController {
	
	public static final String SAVECROSSSELL_LINK = "/app/saveCrossSell.htm";
	public static final String SAVETXNFULFILMENT_LINK = "/app/saveTxnFulFilment.htm";
	
	@Autowired
	private Validator validator;

	@Autowired
	private CouponService couponService;
	
	@Autowired
	private CrosssellService crossSellService;

	private Logger logger = LoggingFactory.getLogger(getClass());

	@RequestMapping(value = "saveCrossSell", method = RequestMethod.POST)
	public String saveTxnCrossSell(@ModelAttribute(value = "txnCrossSellWebDo") TxnCrossSellWebDo txnCrossSellWebDo, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "";
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_TXN_CROSS_SELL;
			DelegateHelper.processRequest(request, response, delegateName, model, txnCrossSellWebDo);
			view = "redirect:/app/personaldetails.htm?lid=" + txnCrossSellWebDo.getLookupID();
		} catch (CouponTamperedException cte) {
		    logger.info("Coupons were tampered!! Returning to home page");
            view = "redirect:/app/home.htm";
		} catch (FCRuntimeException exp) {
			logger.error("exception raised", exp);
			view = "redirect:/app/home.htm";
		} catch (Exception exp) {
			logger.error("some exception raised while saving the crosssell", exp);
			view = "home/error";
		}
		return view;
	}

	@RequestMapping(value = "saveTxnFulFilment", method = RequestMethod.POST)
	public void saveTxnFulFilment(@ModelAttribute(value = "txnFulFilmentWebDo") TxnFulFilmentWebDo txnFulFilmentWebDo, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_TXN_FULFILMENT;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, txnFulFilmentWebDo);

		} catch (Exception exp) {
			exp.printStackTrace();
			logger.error("Any Exception that could come", exp);
			// currently to the same view but have to be redirected to error
			// views.
		}
	}
}

