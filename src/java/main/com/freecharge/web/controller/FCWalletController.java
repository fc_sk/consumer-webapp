package com.freecharge.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.web.interceptor.annotation.NoSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.notification.email.SendEmailRequest;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.promo.util.RequestIdGenerator;
import com.freecharge.wallet.CreditsEncashDetailsService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.service.CreditsEncashDetails;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.util.FCWalletUtil;
import com.freecharge.web.webdo.CreditsEncashDetailsWebDO;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.payments.sdmoney.service.model.Balance;

@Controller
@RequestMapping("/rest/fcwallet/v1/*")
public class FCWalletController {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private WalletService walletService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private CreditsEncashDetailsService creditsEncashDetailsService;

    @Autowired
    private FCWalletUtil fcWalletUtil;
    
    @Autowired
    private RequestIdGenerator requestIdGenerator;
    
    @Autowired
	@Qualifier("notificationApiServiceClient")
	private INotificationToolApiService notificationService;
    
    private static Integer EMAIL_ID_FOR_BANK_ACCOUNT_DETAILS = 8; //This is configued in campaign tool
    //Keeping it static for now, TODO add working link functionality for each user email
    private static String EDIT_BANK_DETAILS_LINK = "https://www.freecharge.in/app/onecheck/credits/encash/data?emailid=";
        
    /**
     * For migrating fc credits to fc wallet
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @NoSession
    @RequestMapping(value = "migrateFcCreditsToFcWallet", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody Map<String, Object> migrateFcCreditsToFcWallet(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> responseMap = new HashMap<>();
        String fcWalletToken = null;
        try {
            fcWalletToken = request.getHeader("token");
            UserDetailsDTO oneCheckUser = oneCheckWalletService.getOneCheckUserByToken(fcWalletToken);
            String emailId = oneCheckUser.getEmailId();
            MigrationStatus fcWalletMigrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, fcWalletToken);
            logger.info("Wallet migration status : " + fcWalletMigrationStatus);
            boolean appendDate = true;//Change it to app config.
            if(null != fcWalletMigrationStatus && "UPGRADE_COMPLETED".equals(fcWalletMigrationStatus.getMigrationStatus())) {
                    responseMap = walletService.fcCreditsToFcWallet(oneCheckUser.getUserId(), oneCheckUser.getFcUserId(), appendDate);
            } else {
                responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE, "Wallet not fully migrated");
            }
        } catch (Exception e) {
            logger.error("Exception while migrating fc credits to fc wallet", e);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        return responseMap;
    }

    /**
     * For checking status of fc wallet and fc credits migration
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "getWalletMigrationStatus", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody Map<String, Object> getWalletMigrationStatus(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> responseMap = new HashMap<>();
        String fcWalletToken = null;
        try {
            fcWalletToken = request.getHeader("token");
            UserDetailsDTO oneCheckUser = oneCheckWalletService.getOneCheckUserByToken(fcWalletToken);
            String email = oneCheckUser.getEmailId();
            logger.info("Getting wallet migration status for email : " + email);
            MigrationStatus walletMigrated = oneCheckWalletService.getFcWalletMigrationStatus(email, fcWalletToken);
            logger.info("Wallet migration status : " + walletMigrated);
            boolean creditsMigrated = false;
            if(null != walletMigrated && "UPGRADE_COMPLETED".equals(walletMigrated.getMigrationStatus())) {
                responseMap.put("walletMigrated", true);
                creditsMigrated = fcWalletUtil.ifFcCreditsToFcWalletMigrationStatusSuccess(email);
            } else {
                responseMap.put("walletMigrated", false);
            }
            responseMap.put("creditsMigrated", creditsMigrated);
        } catch (Exception e) {
            logger.error("Exception while getting wallet status for wallet token: " + fcWalletToken, e);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        return responseMap;
    }

    /**
     * Debits fc credits and stores bank details to encash those credits
     * @param creditsEncashDetailsWebDO
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "storeBankDetailsToEncashCredit", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody Map<String, Object> storeBankDetailToEncashFcCredits(
            @RequestBody CreditsEncashDetailsWebDO creditsEncashDetailsWebDO, HttpServletRequest request,
            HttpServletResponse response) throws DuplicateRequestException {
        Map<String, Object> responseMap = new HashMap<>();
        String fcWalletToken = null;
        try {
            fcWalletToken = request.getHeader("token");
            UserDetailsDTO userDetails = oneCheckWalletService.getOneCheckUserByToken(fcWalletToken);
            int fcUserId = userDetails.getFcUserId();
            String emailId = userDetails.getEmailId();
            MigrationStatus fcWalletMigrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, fcWalletToken);
            logger.info("Wallet migration status : " + fcWalletMigrationStatus);
            if(null != fcWalletMigrationStatus && "UPGRADE_COMPLETED".equals(fcWalletMigrationStatus.getMigrationStatus())) {
                Wallet walletBalance = walletService.findAndLockBalanceByUserID(fcUserId);
                logger.info("Wallet balance : " + walletBalance.toString());
                if(null != walletBalance && walletBalance.getCurrentBalance() > 0d) {
                    double currentBalance = walletBalance.getCurrentBalance();
                    WalletDebitRequest walletDebitRequest = walletService.getWalletDebitRequestForWalletMigration(
                            walletBalance,
                            Wallet.FundDestination.REFUND_TO_BANK, fcUserId, requestIdGenerator.nextRequestId());
                    walletService.debit(walletDebitRequest);
                    CreditsEncashDetails creditsEncashDetails = new CreditsEncashDetails();
                    creditsEncashDetails.setEmailId(creditsEncashDetailsWebDO.getEmailId());
                    creditsEncashDetails.setBankAccountNo(creditsEncashDetailsWebDO.getBankAccountNo());
                    creditsEncashDetails.setBeneficiaryName(creditsEncashDetailsWebDO.getBeneficiaryName());
                    creditsEncashDetails.setIfscCode(creditsEncashDetailsWebDO.getIfscCode());
                    creditsEncashDetails.setUserId(fcUserId);
                    String mtxnId = walletDebitRequest.getMtxnId();
                    creditsEncashDetails.setReferenceId(mtxnId);
                    creditsEncashDetails.setAmount(currentBalance);
                    creditsEncashDetailsService.insertBankDetails(creditsEncashDetails);
                    fcWalletUtil.storeWalletMigrationStatusInDynamo(emailId, userDetails.getUserId(),
                            PaymentConstants.ENCASH_CREDIT);
                    // Notification changes starts here
                    
                    String editLink = EDIT_BANK_DETAILS_LINK + emailId;
                    List<String> tags = new ArrayList<String>();
					tags.add(creditsEncashDetailsWebDO.getEmailId());
					tags.add(creditsEncashDetailsWebDO.getBankAccountNo());
                    SendEmailRequest emailRequest = new SendEmailRequest();
					Map<String,Object> emailContentDataMap = new HashMap<String,Object>();
					emailContentDataMap.put("beneficiaryName",creditsEncashDetailsWebDO.getBeneficiaryName());
					emailContentDataMap.put("ifscCode",creditsEncashDetailsWebDO.getIfscCode());
					emailContentDataMap.put("accountNumber",creditsEncashDetailsWebDO.getBankAccountNo());
					emailContentDataMap.put("editLink",editLink);
					emailRequest.setContentTemplateDataMap(emailContentDataMap);
					emailRequest.setEmailId(EMAIL_ID_FOR_BANK_ACCOUNT_DETAILS);
					Map<String,Object> emailSubjectDataMap = new HashMap<String,Object>();
					emailRequest.setSubjectTemplateDataMap(emailSubjectDataMap);
					//Using registered emailId for sending email
					emailRequest.setToEmail(emailId);
					emailRequest.setTags(tags);
					notificationService.sendEmail(emailRequest);
					
					//Notification changes end here
                    responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                    responseMap.put("RESULT", "Details added");
                    responseMap.put(FCConstants.REFERENCE_ID, mtxnId);
                    responseMap.put(FCConstants.AMOUNT, currentBalance);
                } else {
                    responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                    responseMap.put("RESULT", "Details cant be added, no credits to encash");
                }
            } else {
                responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put("RESULT", "Details cant be added, wallet does not exists");
            }
        } catch (Exception e) {
            logger.error("Exception while storing bank details for fc credits encashment for fc wallet token: " + fcWalletToken, e);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        
        return responseMap;
    }

    /**
     * Debits fc credits and stores bank details to encash those credits
     * @param creditsEncashDetailsWebDO
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "updateBankDetailsToEncashCredit", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody Map<String, Object> updateBankDetailToEncashFcCredits(
            @RequestBody CreditsEncashDetailsWebDO creditsEncashDetailsWebDO, HttpServletRequest request,
            HttpServletResponse response) throws DuplicateRequestException {
        Map<String, Object> responseMap = new HashMap<>();
        String fcWalletToken = null;
        try {
            fcWalletToken = request.getHeader("token");
            if (fcWalletToken == null || fcWalletToken.isEmpty()){
            	responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put("RESULT", "Details could not be updated. Please login to http://www.freecharge.in and refresh this page");
                return responseMap;
            }
            UserDetailsDTO userDetails = oneCheckWalletService.getOneCheckUserByToken(fcWalletToken);
            if (userDetails == null){
            	responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put("RESULT", "Details could not be updated. Please login to http://www.freecharge.in and refresh this page");
                return responseMap;
            }
            int fcUserId = userDetails.getFcUserId();
            String emailId = userDetails.getEmailId();
            MigrationStatus fcWalletMigrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, fcWalletToken);
            logger.info("Wallet migration status : " + fcWalletMigrationStatus);
            if(null != fcWalletMigrationStatus && "UPGRADE_COMPLETED".equals(fcWalletMigrationStatus.getMigrationStatus())) {
                    CreditsEncashDetails creditsEncashDetails = new CreditsEncashDetails();
                    creditsEncashDetails.setEmailId(creditsEncashDetailsWebDO.getEmailId());
                    creditsEncashDetails.setBankAccountNo(creditsEncashDetailsWebDO.getBankAccountNo());
                    creditsEncashDetails.setBeneficiaryName(creditsEncashDetailsWebDO.getBeneficiaryName());
                    creditsEncashDetails.setIfscCode(creditsEncashDetailsWebDO.getIfscCode());
                    creditsEncashDetails.setUserId(fcUserId);
                    creditsEncashDetailsService.updateBankDetails(creditsEncashDetails);
                    responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                    responseMap.put("RESULT", "Details updated");
                } else {
                    responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                    responseMap.put("RESULT", "Details can not be updated, wallet does not exist");
                }
        } catch (Exception e) {
            logger.error("Exception while updating bank details for fc credits encashment for fc wallet token: " + fcWalletToken, e);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        return responseMap;
    }

    /**
     * Debits fc credits and stores bank details to encash those credits
     * @param creditsEncashDetailsWebDO
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "updateBankDetailsForm", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude=true)
    public ModelAndView updateBankDetailsForm(HttpServletRequest request,
            HttpServletResponse response) throws DuplicateRequestException {
        Map<String, Object> responseMap = new HashMap<>();
        ModelAndView modelAndView = new ModelAndView("fcBankTransferForm", responseMap);
        String fcWalletToken = null;
        try {
            fcWalletToken = request.getHeader("token");
           /* if (fcWalletToken == null || fcWalletToken.isEmpty()){
            	responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put("RESULT", "Please login to http://www.freecharge.in and refresh this page");
                return modelAndView;
            }*/
            UserDetailsDTO userDetails = oneCheckWalletService.getOneCheckUserByToken(fcWalletToken);
            if (userDetails == null){
            	responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                responseMap.put("RESULT", "Details could not be updated. Please login to http://www.freecharge.in and refresh this page");
                return modelAndView;
            }
            String emailId = userDetails.getEmailId();
            MigrationStatus fcWalletMigrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, fcWalletToken);
            logger.info("Wallet migration status : " + fcWalletMigrationStatus);
            if(null != fcWalletMigrationStatus && "UPGRADE_COMPLETED".equals(fcWalletMigrationStatus.getMigrationStatus())) {
             
            
            	} else {
                    responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                    responseMap.put("RESULT", "Details can not be updated, wallet does not exist");
                }
        } catch (Exception e) {
            logger.error("Exception while updating bank details for fc credits encashment for fc wallet token: " + fcWalletToken, e);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        return modelAndView;
    }
    
    /**
     * To get fc wallet balance which includes:
     * voucherBalance- Voucher Balance amount that can’t be withdrawn
     * generalBalance- Balance amount that can be withdrawn to user bank account
     * totalBalance- Total One Check Wallet balance (General Balance + Voucher Balance)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "getFcWalletBalance", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody Map<String, Object> getFcWalletBalance(HttpServletRequest request,
                                     HttpServletResponse response) {
        Map<String, Object> responseMap = new HashMap<>();
        UserDetailsDTO oneCheckUser = oneCheckWalletService.getOneCheckUserByToken(request.getHeader("token"));
        Balance fcWalletAccountBalance = oneCheckWalletService.getFCWalletAccountBalance(oneCheckUser.getUserId());
        if(null != fcWalletAccountBalance) {
            responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
        } else {
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        }
        responseMap.put("fcWalletBalance", fcWalletAccountBalance);
        return responseMap;
    }

    @RequestMapping(value = "checkEmail", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude=true)
    public @ResponseBody void checkEmail(HttpServletRequest request,
                    HttpServletResponse response) {
        SendEmailRequest emailRequest = new SendEmailRequest();
        Map<String, Object> emailContentDataMap = new HashMap<String, Object>();
        emailContentDataMap.put("TestEmailMsg", "HODOR:: HELLO WORLD");
        emailRequest.setContentTemplateDataMap(emailContentDataMap);
        emailRequest.setEmailId(EMAIL_ID_FOR_BANK_ACCOUNT_DETAILS);
        Map<String, Object> emailSubjectDataMap = new HashMap<String, Object>();
        emailRequest.setSubjectTemplateDataMap(emailSubjectDataMap);
        //Using registered emailId for sending email
        emailRequest.setToEmail("lakshya.bansal@freecharge.com");
        Boolean isMailSent = notificationService.sendEmail(emailRequest);
        logger.info("is MailSent:" + isMailSent);
    }

}

