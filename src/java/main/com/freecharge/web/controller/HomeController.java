package com.freecharge.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.freecharge.antifraud.FraudDetectionSchedular;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.centaur.CentaurServiceProxy;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.SessionService;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HomeValidator;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.session.FCWalletSession;
import com.freecharge.rest.onecheck.session.FCWalletSessionService;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.service.NewSEOService;
import com.freecharge.tracker.TrackerUtil;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.TrafficSplitter;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.HomeWebDo;
import com.freecharge.web.webdo.OperatorCircleWebDO;
import com.google.gson.Gson;

@Controller
@RequestMapping("/app/*")
public class HomeController {
    public static final String METRICS_PAGE_ACCESS = "pageAccess";

    @Autowired
    private Validator validator;

    @Autowired
    private FCProperties fcproperties;

    @Autowired
    private HomeValidator homeValidator;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    FraudDetectionSchedular fraudDetectionSchedular;

    @Autowired
    NewSEOService newSEOService;

    @Autowired
    private TrackerUtil trackerUtil;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private FCWalletSessionService fcwSessionService;
    
    @Autowired
    private SessionService fcSessionService;
    
    @Autowired
    private CentaurServiceProxy centaurServiceProxy;
    
    private static final  int OLD_WEBSITE_INDEX = 2;
    private static final int CONTROL_WEBSITE_INDEX = 0;
    private final String TRANSFER_TOKEN = "txtoken";
    private final String REFERRER = "ref";
    private final String WALLET_COOKIE = "fcw";
    private final String FC_COOKIE = "app_fc";
    private final String SESSION_USER_IS_LOGIN ="isLogin";
    private final String SESSION_USER_USERID="loggedinuserid";
    private final String USER_DETAILS_MAP = "userDetailsMap";

    private TrafficSplitter trafficSplitter;

    Gson gson = new Gson();

    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private void initializeTrafficSplitter(final AppConfigService appConfig) {
        this.trafficSplitter = new TrafficSplitter(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return appConfig.getSplitTrafficWeight();
            }
        }, OLD_WEBSITE_INDEX);
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderHomePage(HomeWebDo homeWebDo,
            HttpServletRequest request, HttpServletResponse response,
            Model model) {
       return renderCommonHomePage(homeWebDo, request, response, model);
    }

    @RequestMapping(value = "status")
    @ResponseStatus(value = HttpStatus.OK)
    @NoSessionWrite
    @NoLogin
    @ResponseBody
    public Void elbStatus() {
        return null;
    }

    @RequestMapping(value = {"/wallet", "/wallet/cash", "/wallet/storedCards", "/wallet/addressbook", "/wallet/banks", "/wallet/profile", "/wallet/transactions", "/wallet/mobile", "/wallet/whyupgrade", "/wallet/virtualCard", "/wallet/statement"}, method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderWalletHomePage(Map<String, String> requestMap, Model model, HttpServletRequest request, HttpServletResponse response) {
       return renderWalletPage(requestMap, model, request, response);
    }
    
    @RequestMapping(value = { "/app/whyupgrade" }, method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderWhyUpgradePage(Map<String, String> requestMap, Model model, HttpServletRequest request,
            HttpServletResponse response) {
        return new ModelAndView(ViewConstants.FREECHARGE_WHY_UPGRADE);
    }

    public ModelAndView renderWalletPage(Map<String, String> requestMap, Model model, HttpServletRequest request, HttpServletResponse response) {
        String transferToken = request.getParameter(TRANSFER_TOKEN);
        String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        
        // TODO: Fix this ugly variable.
        boolean notLoggedIn = true;
        
        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }
        
        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = "NA";
        }
        
        if (StringUtils.isNotBlank(transferToken)) {
            Map<String, String> headers = new HashMap<>();
            headers.put(OneCheckConstants.TOKEN, transferToken);
            headers.put(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER, ipAddress);
            headers.put(OneCheckConstants.USER_AGENT, userAgent);
            headers.put(OneCheckConstants.CONTENT_TYPE, "application/json");
            logger.info("OneCheckConstants.TOKEN:"+transferToken);
            Map<String, String> userDetails  = oneCheckWalletService.getTransferToken(headers);
            logger.info("userDetails:"+userDetails.get("emailId"));
            logger.info("token");
            if (MapUtils.isNotEmpty(userDetails)) {
                String userToken = userDetails.get("token");
                
                createFCWalletSession(response, userToken);
                
                model.addAttribute(USER_DETAILS_MAP, new Gson().toJson(userDetails));
                notLoggedIn = false;
            }
        } else {
            String fcSessionId = getFCSessionId(request);
            logger.info("else block-fcsessionid:"+fcSessionId);
            if (StringUtils.isNotBlank(fcSessionId)) {
                FreechargeSession freechargeSession = fcSessionService.retrieveSession(fcSessionId);
                logger.info("FreechargeSession:"+freechargeSession);
                if (isLoggedIn(freechargeSession)) {
                    Integer fcUserId = retrieveUserId(freechargeSession);
                    logger.info("fcUserid:"+fcUserId);
                    if (fcUserId != null) {
                        try {

                            String walletToken = fcSessionId;
                            logger.info("wallettoken:"+walletToken);
                            setWalletUserDetails(model, userAgent, ipAddress, walletToken);

                            // get rid of any stale wallet session if it exists.
                            deleteFCWalletSession(request);

                            notLoggedIn = false;
                        } catch (Exception e) {
                            logger.error("Exception while fetching user details", e);
                        }
                    }
                }
            }

            if (notLoggedIn) {
                Cookie walletCookie = getWalletCookie(request);
                
                if (walletCookie != null && StringUtils.isNotBlank(walletCookie.getValue())) {
                    String walletSessionId = walletCookie.getValue();
                    
                    FCWalletSession fcwSession = fcwSessionService.retrieveSession(walletSessionId);
                    
                    if (fcwSession != null) {
                        String userToken = fcwSession.getUserToken();
                        
                        if (StringUtils.isNotBlank(userToken)) {
                            try {
                                setWalletUserDetails(model, userAgent, ipAddress, userToken);
                                notLoggedIn = false;
                            } catch (Exception e) {
                                logger.error("Exception while fetching user details from userToken", e);;
                            }
                        }
                    }
                } 
            }
        }
        model.addAttribute("walletAPIEndPoint", fcproperties.getFCWalletAPIEndPoint());
        model.addAttribute("vCardAPIEndPoint", fcproperties.getVCardAPIEndPoint());
        model.addAttribute("notLoggedIn", notLoggedIn);
        model.addAttribute("googleClientID", fcproperties.getProperty(FCConstants.GOOGLE_CLIENT_ID));
        model.addAttribute("googleRedirectURI", fcproperties.getProperty(FCConstants.GOOGLE_REDIRECT_URI));
        model.addAttribute("trackerVisitApi", trackerUtil.getVisitApi());
        
        logger.info("walletAPIendpoint:"+fcproperties.getFCWalletAPIEndPoint());
        
        return new ModelAndView(ViewConstants.FREECHARGE_SINGLEPAGE_WALLET);
    }
    
    private void deleteFCWalletSession(HttpServletRequest request) {
        Cookie walletCookie = getWalletCookie(request);
        
        if (walletCookie != null && StringUtils.isNotBlank(walletCookie.getValue())) {
            String walletSessionId = walletCookie.getValue();
            
            FCWalletSession fcwSession = fcwSessionService.retrieveSession(walletSessionId);
            
            if (fcwSession != null) {
                fcwSessionService.deleteSession(walletSessionId);
            }
        }

    }
    
    private void setWalletUserDetails(Model model, String userAgent, String ipAddress, String userToken) {
        Map<String, String> userDetails = oneCheckWalletService.getUserByToken(userToken, userAgent, ipAddress);
        logger.info("passed token:"+userToken+"userDetails:"+userDetails.get("token"));
        model.addAttribute(USER_DETAILS_MAP, new Gson().toJson(userDetails));
    }

    private void createFCWalletSession(HttpServletResponse response, String walletToken) {
        String sessionId = UUID.randomUUID().toString();
        FCWalletSession fcwSession = new FCWalletSession(sessionId, walletToken);
        fcwSessionService.saveSession(fcwSession);
        storeSessionId(response, sessionId);
    }

    @RequestMapping(value = "walletsuccess", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderWalletSuccessPage(Map<String, String> requestMap, Model model, HttpServletRequest request) {
       return new ModelAndView(ViewConstants.FREECHARGE_WALLET_SUCCESS);
    }

    @RequestMapping(value = "walletfailure", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderWalletFailurePage(Map<String, String> requestMap, Model model, HttpServletRequest request) {
       return new ModelAndView(ViewConstants.FREECHARGE_WALLET_FAILURE);
    }

    @RequestMapping(value = "univercell", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderUnivercellHomePage(HomeWebDo homeWebDo,
            HttpServletRequest request, HttpServletResponse response,
            Model model) {
        model.addAttribute("isUnivercellHomePage", true);
        return renderCommonHomePage(homeWebDo, request, response, model);
    }

    public ModelAndView renderCommonHomePage(HomeWebDo homeWebDo,
            HttpServletRequest request, HttpServletResponse response,
            Model model) {

        logger.debug("Website home user agent - " + request.getHeader("User-Agent"));
        
        // This will help us post deployment, if we see an abnormal drop in this
        // no, we know something has gone wrong with the deployment.
        metricsClient.recordEvent(METRICS_PAGE_ACCESS, "homePage", "click");
        SEOAttributesBusinessDO seoBusinessDO = new SEOAttributesBusinessDO();
        try {
            if (!StringUtils.isEmpty(request
                    .getParameter(WebConstants.PRODUCT_TYPE))) {
                 seoBusinessDO = newSEOService.populateSeoAttributes(
                        request.getParameter(WebConstants.PRODUCT_TYPE),
                        request.getParameter(WebConstants.OPERATOR),
                        Integer.toString(FCConstants.CHANNEL_ID_WEB));
            } else {
                seoBusinessDO = newSEOService.populateSeoAttributes(WebConstants.PRODUCT_TYPE_HOME_PAGE, null, null);
            }

            if(!FCUtil.isEmpty(request.getParameter(WebConstants.SHOW_RECHARGE_PLANS))){
                boolean showRechargePlans = Boolean.valueOf(String.valueOf(request.getParameter(WebConstants.SHOW_RECHARGE_PLANS)));
                if(showRechargePlans){
                    model.addAttribute(WebConstants.SHOW_RECHARGE_PLANS, showRechargePlans);
                    newSEOService.populatePlanBasedSeoAttributes(seoBusinessDO);
                }
            }

            model.addAttribute("seoAttributes", seoBusinessDO);

        } catch (FCRuntimeException fcexp) {
            logger.error("Exception while SEO redirection of new ui .", fcexp);
        } catch (Exception exp) {
            logger.error("Exception while SEO redirection of new ui", exp);
        }

        /*
         * To indicate whether this is friends and family page. In this case
         * this is *not* friends and family hence value is set to "general"
         */

        model.addAttribute("releaseType", "general");

        // Pass the affiliateIds of various channels
        // and let the consumer of this REST call decide which one to use
        model.addAttribute("affiliateIdWeb", FCConstants.AFFILIATE_ID_WEB);
        model.addAttribute("affiliateIdMobile", FCConstants.AFFILIATE_ID_MOBILE);
        model.addAttribute("affiliateIdApp", FCConstants.AFFILIATE_ID_ANDROID_APP);
        model.addAttribute("walletRedirectUrl", fcproperties.getWalletRedirectUrl());
        model.addAttribute("facebookLoginEnabled", appConfig.isFacebookLoginEnabled());
        model.addAttribute("googleLoginEnabled", appConfig.isGoogleLoginEnabled());

        JSONObject obj = this.appConfig.getCustomProp(FCConstants.CUSTOM_LOGIN_PROPS);
        if (obj!=null){
            model.addAttribute("loginCustomMessage", obj.get("message"));
        }
        JSONObject homePageJsonObject = this.appConfig.getCustomProp(FCConstants.CUSTOM_LOGIN_PROPS);
        if (homePageJsonObject!=null){
            model.addAttribute("homePageCustomMessage", homePageJsonObject.get("homepagemessage"));
        }
        model.addAttribute("googleClientID",
                fcproperties.getProperty(FCConstants.GOOGLE_CLIENT_ID));
        model.addAttribute("googleRedirectURI",
                fcproperties.getProperty(FCConstants.GOOGLE_REDIRECT_URI));

        model.addAttribute("fingerPrintingEnabled", appConfig.isFingerPrintingEnabled());
        model.addAttribute("isSokratiPixelEnabled", appConfig.isSokratiPixelEnabled());

        model.addAttribute("forcePaidCouponsEnabled", appConfig.isForcePaidCouponsEnabled());
        model.addAttribute("isRupayEnabled", appConfig.isRupayEnabled());
        model.addAttribute("testimonialVersionNo", fcproperties.getTestimonialVersion());
        model.addAttribute("isMissionsTabEnabled", getMissionsTabEnabled());
        model.addAttribute("exclusiveCouponsEnabled", appConfig.isExclusiveCouponsEnabled());
        // Disabling custom reco UI.
        model.addAttribute("recoCouponsEnabled", false);
        // setting tracker visit api
        model.addAttribute("trackerVisitApi", trackerUtil.getVisitApi());

        // find split and proceed to chosen target
       /* if (fcproperties.isTrafficSplitEnabled()) { // do not redirect if this is beta site
            int index = splitTraffic(request);
            if (index == CONTROL_WEBSITE_INDEX) {
                final String url = fcproperties.getProperty(FCProperties.SITE_VERSION_NEW);
                final RedirectView rv = new RedirectView(url, false, true, false);
                return new ModelAndView(rv "redirect:" + url);
            }
        }*/
        
        if (appConfig.isCentaurEnabledForABTesting()) {
            String redirectUrl = fcproperties.getNewSiteUrl();
            if (!FCUtil.isEmpty(request.getQueryString())) {
                redirectUrl = redirectUrl + "?" + request.getQueryString();
            }
            final RedirectView rv = new RedirectView(redirectUrl, false, true, false);
            return new ModelAndView(rv);
        }

        // find whether new UI is requested
        return new ModelAndView(ViewConstants.FREECHARGE_SINGLEPAGE_HOME_NEW);
    }

    @RequestMapping(value = "getoperatorcircle", method = RequestMethod.GET)
    @NoLogin
    public String getOperraotCircle(HttpServletRequest request, HttpServletResponse response, Model model) {
        String delegateName = WebConstants.DELEGATE_OPERATOR_CIRCLE;
        response.setContentType("application/json");

        try {
            WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new OperatorCircleWebDO());

            final List<Map<String, Object>> prefixData = ((OperatorCircleWebDO) webContext.getBaseWebDO()).getPrefixData();

            model.addAttribute("prefixData", prefixData);
        } catch (FCRuntimeException fcexp) {
            logger.error("exception raised exception details are ", fcexp);
            return "jsonView";
        } catch (Exception exp) {
            logger.error("exception raised exception details are ", exp);
            return "jsonView";
        }

        return "jsonView";
    }

    public boolean getMissionsTabEnabled() {
        return false;
    }
    
    private void storeSessionId(HttpServletResponse response, String sessionId) {
        Cookie ck = new Cookie("fcw", sessionId);
        ck.setMaxAge((int) TimeUnit.SECONDS.convert(1, TimeUnit.DAYS));
        ck.setPath("/");
        
        response.addCookie(ck);
    }
    
    private Cookie getWalletCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (WALLET_COOKIE.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }

        return null;
    }
    
    private Cookie getFCCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (FC_COOKIE.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }

        return null;
    }
    
    private String getFCSessionId(HttpServletRequest request) {
        Cookie fcCookie = getFCCookie(request);
        
        if (fcCookie != null) {
            return fcCookie.getValue();
        }
        logger.info("cookie Value:"+fcCookie);
        return null;
    }
    
    private Integer retrieveUserId(FreechargeSession freechargeSession) {
        Map<String, Object> sessionData = freechargeSession.getSessionData();
        
        if (sessionData.containsKey(SESSION_USER_USERID)) {
            Object userIdObj = sessionData.get(SESSION_USER_USERID);
            
            if (userIdObj != null && userIdObj instanceof Integer) {
                return (Integer) userIdObj;
            }
        }
        
        return null;
    }

    private boolean isLoggedIn(FreechargeSession freechargeSession) {
        if (freechargeSession == null) {
            return false;
        }
        Map<String, Object> sessionData = freechargeSession.getSessionData();
        
        if (sessionData == null) {
            return false;
        }
        
        if (sessionData.containsKey(SESSION_USER_IS_LOGIN)) {
            Object loggedInObj = sessionData.get(SESSION_USER_IS_LOGIN);
            
            if (loggedInObj != null && loggedInObj instanceof Boolean) {
                return true;
            }
        }
        
        return false;
    }

}
