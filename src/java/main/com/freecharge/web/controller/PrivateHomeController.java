package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.service.NewSEOService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.HomeWebDo;
import com.google.gson.Gson;

@Controller
@RequestMapping("/protected/*")
public class PrivateHomeController {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    private static final String METRICS_PAGE_ACCESS = "private";
    private static final String RELEASE_TYPE = "releaseType";
    private static final String FRIENDS_AND_FAMILY = "fnf";

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private AppConfigService appConfig;
    
    @Autowired
    NewSEOService newSEOService;
    
    Gson gson = new Gson();

    @RequestMapping(value = "home", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView renderPrivateHomePage(HomeWebDo homeWebDo, HttpServletRequest request, HttpServletResponse response, Model model) {
        //This will help us post deployment, if we see an abnormal drop in this no, we know something has gone wrong with the deployment.
        metricsClient.recordEvent(METRICS_PAGE_ACCESS, "homePage", "click");

        try {
            if (!StringUtils.isEmpty(request.getParameter(WebConstants.PRODUCT_TYPE))) {
                SEOAttributesBusinessDO seoBusinessDO = newSEOService.populateSeoAttributes(
                        request.getParameter(WebConstants.PRODUCT_TYPE), request.getParameter(WebConstants.OPERATOR),
                        Integer.toString(FCConstants.CHANNEL_ID_WEB));
                model.addAttribute("seoAttributes", seoBusinessDO);
            }
        } catch (FCRuntimeException fcexp) {
            logger.error("Exception while SEO redirection of new ui .", fcexp);
        } catch (Exception exp) {
            logger.error("Exception while SEO redirection of new ui", exp);
        }

        /*
         * To indicate whether this is friends and family 
         * page. In this case this is friends and family
         * hence value is set to ""
         */
        model.addAttribute(RELEASE_TYPE, FRIENDS_AND_FAMILY);
        model.addAttribute("isRupayEnabled", appConfig.isRupayEnabled());

        String experimentName = request.getParameter("e"); 
        model.addAttribute("experimentName", experimentName);
        return new ModelAndView(ViewConstants.FREECHARGE_SINGLEPAGE_HOME);
    }
    
    @RequestMapping(value = "mhome")
    @NoLogin
    public ModelAndView newHomePage(HttpServletRequest request, HttpServletResponse response, Model model) {
        metricsClient.recordEvent(METRICS_PAGE_ACCESS, "mobileHomePage", "click");

        String view = "mobile/new";

        logger.debug("Private New mobile home user agent - " + request.getHeader("User-Agent"));
        metricsClient.recordEvent("pageAccess", "newMobileHomePage", "click");

        model.addAttribute(RELEASE_TYPE, FRIENDS_AND_FAMILY);

        return new ModelAndView(view);
    }
}
