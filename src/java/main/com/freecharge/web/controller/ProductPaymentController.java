package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.web.util.ViewConstants;

/*

  Make Payment btn -> doPayment() JS                                                                                                                           
                            |                                                                                                                                  
                            v                                                                                                                                  
                      /checkout/pay/dopayment.htm -->  PaymentCheckoutRequestController    ProductPaymentBusinessDO                                                    
                      (Open in new window)                 .doPayment()               <---------------->      ProductPaymentDelegate                                                
                                                                                                                actionUrl is set in DOMapper                   
                                                        populates resultMap                                                                                    
                                                        and actionUrl is populated in map with key 'waitpageKey'                                                                                     
                      									PaymentRequestDelegate                                                          
                                                          IPaymentType                                   
                                                          .doPayment()                                  
                                                          IGatewayHandler                                 
                                                          .paymentRequestSales()                         
                                                        - actionUrl is set in specific GatewayHandler
                                                        - return URL is set where the PG will send response
                                                        if successful render payment/request tile (/WEB-INF/views/payment/payment.jsp)
                                                                     |                                          
                       									 <-----------+                                                                                         
                        Creates a form with all                                                                                                                
                        resultMap params and                                                                                                                   
                        auto-submits to                                                                                                                        
                        the specific PaymentGateway                                                                                                            
                                  |                                                                                                                            
  Payment Gateway           <-----+                                                                                                                           
   processes and                                                                                                                                                 
   makes a HTTP request                                                                                                                                             
   to /payment/handle*response                                                                                                                                                               
           |                                                                                                                                                   
           |                                                                                                                                                   
           +------------------------------------>      PaymentPortalController                                                                                                        
                                                          .handlePaymentResponse()  PaymentResponseBusinessDO                                                                                                     
                                                                                     <--------------------> PaymentResponseDelegate                                                   
                                                                                                              IPaymentType                                     
                                                                                                                .handleResponse()
                                                                                                              IGatewayhandler
                                                        renders tiledef                                         .paymentResponse()
                                                         "payment/response"                                   CouponInventory and CrossSell stuff              
                                                                |                                                                                              
                      paymentresponse.jsp       <---------------+                                                                                              
                      autosubmits to                                                                                                                           
                      "/app/handlerecharge.htm"                                                                                                              
                                |                                                                                                                            
                                |                                                                                                                            
                                +---------------->     RechargeInitiateContaroller.handleRecharge()  -------------> renders tile (was recharge/initiated for some time) handle/recharge
                                 																					 |
                                 																					 | 	
                          																				redirects to RechargeInitiateContaroller.doRecharge()
                                																					 |
                                																					 |	
                                																		renders tile "recharge/view"                                                                                                                                                                                                                                                                                                                                                                                       
                                                           															.handleRecharge()
   
   This is the Payment Flow. Pretty hairy.
   
   * A pattern is used again and again. Create a HTML form and fill it with hidden params - then submit it automatically using Javascript. 
     As far as I can see, this is just to handover control to another Java Controller class. Why? Why? Why?  
   * Why use the HTML form mechanism to submit to the Payment Gateways?
     Why not use HttpClient from within Java code? (as is done while communicating with the Recharge Aggregators)
     (Answer) Reason being, it's not just a fire and forget call. It is HTTP POST to payment gateways upon which 
     PGs open up 3-d secure window OR internet banking site OR cash-card site and what not. Customers have to interact
     with the resultant HTML page. In theory, we could do all this
     from within Java code on the server side, however it'd be hairier than what it already is. Recharge process, on the other
     hand is fairly straight forward and doesn't require further user interaction. 

        - ChetanV  
        - The unwanted payment request redirects are removed 
        	-ShirishS                     
 */

@Controller
@RequestMapping("/checkout/pay/*")
public class ProductPaymentController {

    public static final String NEW_PAYMENT_WINDOW = "/checkout/pay/newpaymentwindow.htm";
    public static final String DO_PAYMENT_ACTION = "/checkout/pay/dopayment.htm";

    @RequestMapping(value = "newpaymentwindow", method = RequestMethod.GET)
    public String newPaymentWindow() {
        return ViewConstants.NEW_PAYMENT_WINDOW;
    }

    @RequestMapping(value = "recievepayment")
    public void recievePayment(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    }

    @RequestMapping(value = "paymenterror")
    public void recievePaymentError(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    }
}
