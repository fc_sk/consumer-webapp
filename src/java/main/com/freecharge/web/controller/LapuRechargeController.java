package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.logging.LoggingFactory;

@Controller
@RequestMapping("/response/*")
public class LapuRechargeController {

    Logger logger = LoggingFactory.getLogger(RechargeAdminController.class);

    @RequestMapping(value = "/rechargeResponseHandler", method = RequestMethod.GET)
    public void lapuResponseHandler(HttpServletRequest request, HttpServletResponse response) {
        String xmldata = request.getParameter("xmldata");
        logger.info(xmldata);
    }
}
