package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.condition.Result;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.social.config.FacebookManager;
import com.freecharge.social.web.FBFraudCheckService;

@Controller
@RequestMapping("/fraud/check/*")
public class FraudCheckController {

    @Autowired
    private FacebookManager facebookManager;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private FBFraudCheckService fbFraudCheckService;
    @RequestMapping(value = "fbcheck", method = RequestMethod.GET, produces = "application/json")
    public String fbCheck(HttpServletRequest request, Model model){
        String failureMessage = "";

        try {
            Result result = new Result();
            result.setSuccess(true);
            Users curUser = this.userServiceProxy.getLoggedInUser();
            String detailedFailureMessage = fcProperties.getProperty(FCProperties.MSG_OFFER_MAX_LIMIT_REACHED);
            Connection<Facebook> fbConnection = null;

            if (curUser==null){
                failureMessage = "User is not logged in.";
                return returnFailedMessage(model, failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            if(request.getParameter(FBFraudCheckService.FB_ACCESS_TOKEN) == null ||
                    FCStringUtils.isBlank(String.valueOf(request.getParameter(FBFraudCheckService.FB_ACCESS_TOKEN)))) {

                failureMessage = "Invalid accessToken : " + request.getParameter(FBFraudCheckService.FB_ACCESS_TOKEN)
                        + " for USER_ID : " + curUser.getUserId();
                return returnFailedMessage(model, failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            String accessToken = String.valueOf(request.getParameter(FBFraudCheckService.FB_ACCESS_TOKEN));
            logger.debug("User : " + curUser.getUserId() + " with accessToken : " + accessToken);
            fbConnection = facebookManager.createConnectionFromAccessToken(accessToken);

            Integer minPostCount = this.appConfig.getConfigValue(FBFraudCheckService.FB_MIN_POSTS_COUNT,
                    Integer.valueOf(fcProperties.getProperty(FCConstants.FB_DEFAULT_MIN_POSTS_COUNT)));
            Integer lastPostDayDiff =  this.appConfig.getConfigValue(FBFraudCheckService.FB_LAST_POST_DAY_DIFFERENCE,
                    Integer.valueOf(fcProperties.getProperty(FCConstants.FB_DEFAULT_LAST_POST_DAY_DIFFERENCE)));

            logger.debug("Minimum post count: " + minPostCount + ", Last Post Day Diff: " + lastPostDayDiff);

            if(minPostCount > 0 || lastPostDayDiff > 0) {
                result = fbFraudCheckService.fbFraudCheck(curUser, fbConnection, minPostCount, lastPostDayDiff);
            }

            if(!result.isSuccess()) {
                return returnFailedMessage(model, result.getFailureMessage(), result.getDetailedFailureMessage(), ErrorCode.FB_FRAUD_ERROR);
            }

            logger.info("User: " + curUser.getUserId() + " have passed Facebook Fraud Check Controller with Minimum post count: "
                    + minPostCount + ", Last Post Day Diff: " + lastPostDayDiff);
            model.addAttribute("result", result);
            model.addAttribute("message", "Facebook account validated.");
            this.metricsClient.recordEvent(FCConstants.FRAUD_CHECK_METRIC_SERVICE, FBFraudCheckService.METRIC_FB_CHECK, "success");

        }catch (Exception e){
            logger.error(ErrorCode.FB_FRAUD_ERROR + ": Something went wrong", e);
            failureMessage = ErrorCode.FB_FRAUD_ERROR + ": Something went wrong. Exception : " + e.getMessage();
            return returnFailedMessage(model, failureMessage, fcProperties.getProperty(FCProperties.MSG_TECHNICAL_ISSUE),
                    ErrorCode.UNKNOWN_ERROR);
        }

        return "jsonView";
    }

    private String returnFailedMessage(Model model, String failureMsg, String detailedFailureMsg, ErrorCode errorCode){
        logger.info(failureMsg);
        Result result = new Result();
        result.setSuccess(false);
        result.setErrorCode(errorCode.getErrorNumberString());
        result.setFailureMessage(failureMsg);
        result.setDetailedFailureMessage(detailedFailureMsg);
        this.metricsClient.recordEvent(FCConstants.FRAUD_CHECK_METRIC_SERVICE, FBFraudCheckService.METRIC_FB_CHECK_FAILED, errorCode.toString());
        model.addAttribute("result", result);
        return "jsonView";
    }

    private static final Logger logger = LoggingFactory.getLogger(FraudCheckController.class);
}
