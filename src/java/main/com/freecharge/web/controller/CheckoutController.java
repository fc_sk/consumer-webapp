package com.freecharge.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.Promocode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.PromocodeService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.CaptchaService;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCHttpUtil;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.FraudCheckService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.tracker.Tracker;
import com.freecharge.tracker.TrackerEventVo;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CheckoutWebDO;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.PaymentTypeMasterWebDO;
import com.freecharge.web.webdo.UserDO;

@Controller
public class CheckoutController {
    public static final class JSPVariables {
        public static final String TOTAL_PAYABLE_AMOUNT = "payableTotalAmount";
        public static final String PG_PAYABLE_AMOUNT = "payablePGAmount";
        public static final String WALLET_PAYABLE_AMOUNT = "payableWalletAmount";
        public static final String WALLET_BALANCE = "walletBalance";
        public static final String WALLET_ENABLED = "isWalletPayEnabled";
        public static final String WALLET_DISPLAY_ENABLED = "isWalletDataDisplayEnabled";
        public static final String PARTIAL_PAYMENT_ELIGIBLE = "eligibleForPartialPayment";
        public static final String FULL_WALLET_PAYMENT_ELIGIBLE = "eligibleForFullWalletPayment";
        public static final String OFFERS_ENABLED = "offersEnabled";
    }
    
    public static final String PAYMENT_LINK = "app/checkout.htm";
    public static final String NEW_PAYMENT_LINK = "/quickpay.htm";
    public static final String TOTALAMOUNTTOPAY_LINK = "app/total_amount_to_pay.htm";
    
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private FCHttpUtil fcHttpUtil;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private FreefundService freefundService;
    
    @Autowired
    private PromocodeService promocodeService;
    
    @Autowired
    private BinOfferService binOfferService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private FraudCheckService fraudCheckService;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private Tracker tracker;

    @Autowired
    private CaptchaService captchaService;
    
    @Autowired
    private AppConfigService appConfigService;

    @RequestMapping(value = {"app/checkout", "quickpay"}, method = RequestMethod.GET)
    public ModelAndView payment(HttpServletRequest request, HttpServletResponse response, Model model) {
        TrackerEventVo trackerEventVo = new TrackerEventVo();
        trackerEventVo.setEventName("Dealscontinue");
        tracker.trackEvent(trackerEventVo);
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "continue1", "click");
        
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String lid = request.getParameter(FCConstants.LOOKUPID);
        String pg = request.getParameter(FCConstants.PG);
        boolean isPaymentFail = false;
        if("fail".equals(request.getParameter("payment"))) {
            isPaymentFail = true;
        }
        
        model.addAttribute("payment", request.getParameter("payment"));
        
        if (StringUtils.isBlank(lid)) {
            logger.error("No lid present in the request.... Session Id : " + fs.getUuid());
            model.addAttribute("loginWebDO", new LoginWebDO());
            model.addAttribute("userDO", new UserDO());
            return new ModelAndView("home/error");
        }
        
        String view = "app/quickpay";
        
        Integer userId = userServiceProxy.getLoggedInUser().getUserId();

        model.addAttribute(WebConstants.FORCE_CAPTCHA, forceCaptcha(fs));

        try {
            populatePGOpenNewWindow(model);
            
            CheckoutWebDO checkoutWebDO = new CheckoutWebDO();
            checkoutWebDO.setLookupID(lid);
            
            CartBusinessDo cartBusinessDo = cartService.getCart(lid);
            
            if(cartBusinessDo == null) {
                logger.error("Something wrong with the lid: "+lid+", cartBusinessDo is null. ");
                model.addAttribute("error", "Technical error occured. Please try again later.");
                return new ModelAndView("home/error");
            }
            
            List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, pg);
            model.addAttribute("allowablePGList", allowablePGList);
            FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
            freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
            freeFundRequestBusinessDO.setLookupID(cartBusinessDo.getLookupID());
            boolean containsFreefund = freefundService.containsFreeFund(cartBusinessDo);
            model.addAttribute("containsFreefund", containsFreefund);
            CartItemsBusinessDO cartItemsBusinessDO = freefundService.getFreeFundItem(cartBusinessDo);
            model.addAttribute("freefundCartItem", cartItemsBusinessDO);
            if(containsFreefund) {
                FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(Long.parseLong(cartItemsBusinessDO.getItemId()+""));
                model.addAttribute("codeType", freefundCoupon.getPromoEntity());
                model.addAttribute("code", freefundCoupon.getFreefundCode());
                model.addAttribute("freefundItemPrice", freefundService.getFreeFundItemPrice(lid));
            }
                        
            String checkoutDelegate = WebConstants.DELEGATE_BEAN_CHECKOUT;

            WebContext webContext = DelegateHelper.processRequest(request, response, checkoutDelegate, model, checkoutWebDO);
            model.addAttribute("txnHomePgID", ((CheckoutWebDO) webContext.getBaseWebDO()).getTxnHomePageId());
            String status = ((CheckoutWebDO) webContext.getBaseWebDO()).getSatus();
            if (status == null || status.equals("fail")) {
                model.addAttribute("error", "Technical error occured. Please try again later.");
                view = "home/error";
                return new ModelAndView(view);
            }
            
            Boolean isOffersEnabled = ((CheckoutWebDO) webContext.getBaseWebDO()).getIsOffersEnabled();
            
            model.addAttribute("defaultProfile", ((CheckoutWebDO) webContext.getBaseWebDO()).getDefaultProfile());
            List<PaymentTypeMasterWebDO> paymentTypes = ((CheckoutWebDO) webContext.getBaseWebDO()).getPaymentTypes();
            model.addAttribute("paymentTypes", paymentTypes);

            model.addAttribute(JSPVariables.TOTAL_PAYABLE_AMOUNT, ((CheckoutWebDO) webContext.getBaseWebDO()).getPayableTotalAmount());
            model.addAttribute(JSPVariables.PG_PAYABLE_AMOUNT, ((CheckoutWebDO) webContext.getBaseWebDO()).getPayablePGAmount());
            model.addAttribute(JSPVariables.WALLET_PAYABLE_AMOUNT, ((CheckoutWebDO) webContext.getBaseWebDO()).getPayableWalletAmount());
            model.addAttribute(JSPVariables.WALLET_BALANCE, ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletBalance());
            model.addAttribute(JSPVariables.WALLET_ENABLED, ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletEnabled());
            model.addAttribute(JSPVariables.WALLET_DISPLAY_ENABLED, ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletDisplayEnabled());
            model.addAttribute(JSPVariables.PARTIAL_PAYMENT_ELIGIBLE, ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletEligibility().getEligibleForPartialPayment());
            model.addAttribute(JSPVariables.FULL_WALLET_PAYMENT_ELIGIBLE, ((CheckoutWebDO) webContext.getBaseWebDO()).getWalletEligibility().getEligibleForFullWalletPayment());
            
            model.addAttribute(JSPVariables.OFFERS_ENABLED, isOffersEnabled);

            Promocode promocode = promocodeService.getFirstActivePromocode();
            
            if (promocode != null && isOffersEnabled) {
                model.addAttribute("promocode", promocode);
            }
            
            if (isOffersEnabled) {
                Map<String, BinOffer> binOffers =  binOfferService.getActiveOffers(userId);
                model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO_MAP, binOffers);
            }
            

            model.addAttribute("cardStorageOptIn", checkoutWebDO.getCardStorageOptIn());

        } catch (FCRuntimeException fcexp) {
            logger.error("FCException in the request called for url " + request.getRequestURL()+"?"+request.getQueryString());
            logger.error("Exception occured in checkout controller", fcexp);
            model.addAttribute("error", "Technical error occured. Please try again later.");
            view = "home/error";
        } catch (Exception exp) {
            logger.error("Exception in the request called for url " + request.getRequestURL()+"?"+request.getQueryString());
            logger.error("Exception occured in checkout controller", exp);
            model.addAttribute("error", "Technical error occured. Please try again later.");
            view = "home/error";
        }
        /* To handle back button issue, do not have default cache control of browser */
        fcHttpUtil.doNotCache(response);
        return new ModelAndView(view);
    }
    
	@RequestMapping(value = "app/total_amount_to_pay", method = RequestMethod.GET, produces = "application/json")
    public String cartPrice(HttpServletRequest request, HttpServletResponse response, Model model) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String lid = request.getParameter(FCConstants.LOOKUPID);
        logger.info("Receiving request in cartSummary for LID : " + lid);
        if (lid == null || StringUtils.isBlank(lid)) {
            logger.error("No lid present in the request.... Session Id : " + fs.getUuid());
            model.addAttribute("error", "Technical error occurred. Please try again later.");
        }
        
        try {
            logger.info("in cartSummary method");
            Users curUser = this.userServiceProxy.getLoggedInUser();
            if (curUser==null){
                logger.error("User is not logged in.");
                model.addAttribute("error", "User is not logged in.");
                return "jsonView";
            }
            CheckoutWebDO checkoutWebDO = new CheckoutWebDO();
            checkoutWebDO.setLookupID(lid);
            
            String checkoutDelegate = WebConstants.DELEGATE_BEAN_CHECKOUT;
            WebContext webContext = DelegateHelper.processRequest(request, response, checkoutDelegate, model, checkoutWebDO);

            //model.addAttribute("data", (CheckoutWebDO) webContext.getBaseWebDO());
            CheckoutWebDO finalCheckoutWebDo = (CheckoutWebDO) webContext.getBaseWebDO();
            model.addAttribute("walletEligibility", finalCheckoutWebDo.getWalletEligibility());
            model.addAttribute("payablePGAmount", finalCheckoutWebDo.getPaymentBreakup().getPayablePGAmount().getAmount());
            model.addAttribute("payableTotalAmount", finalCheckoutWebDo.getPaymentBreakup().getPayableTotalAmount().getAmount());
            model.addAttribute("paymentBreakup", finalCheckoutWebDo.getPaymentBreakup());
            if(model.containsAttribute("stateMaster")) {
            	model.addAttribute("stateMaster", null);
            }
            model.addAttribute("status", ((CheckoutWebDO) webContext.getBaseWebDO()).getSatus());
            
            String status = ((CheckoutWebDO) webContext.getBaseWebDO()).getSatus();
            if (status == null || status.equals("fail")) {
                model.addAttribute("status", "fail");
                model.addAttribute("error", "Technical error occurred. Please try again later.");
            }
            if (finalCheckoutWebDo.getPaymentBreakup().getPayableTotalAmount().getAmount().intValue() < 1){
                logger.info("Zero payment for user: "+curUser.getUserId()+" lid: "+lid);
                //Get freefund coupon
                FreefundCoupon freefundCoupon = this.cartService.getFreefundCoupon(checkoutWebDO.getCartWebDo());
                if (freefundCoupon!=null){
                    model.addAttribute("showFBConnect", this.fraudCheckService.shouldShowFbConnect(curUser,
                            freefundCoupon.getFreefundClassId(), request));
                }
            }
        } catch (FCRuntimeException fcexp) {
            logger.error("Exception occurred in checkout controller", fcexp);
            model.addAttribute("status", "fail");
            model.addAttribute("error", "Technical error occurred. Please try again later.");
        } catch (Exception exp) {
            logger.error("Exception occured in checkout controller", exp);
            model.addAttribute("status", "fail");
            model.addAttribute("error", "Technical error occured. Please try again later.");
        }

        return "jsonView";
    }

    
    private void populatePGOpenNewWindow(Model model) {
        final boolean pgOpenNewWindow = fcProperties.getBooleanProperty(FCConstants.PG_OPEN_NEW_WINDOW);
        model.addAttribute(FCConstants.MODEL_PG_OPEN_NEW_WINDOW, pgOpenNewWindow);
    }
    
    
    private List<PaymentTypeMasterWebDO> getPaymentTypesForCampaining(List<PaymentTypeMasterWebDO> paymentTypes, CartBusinessDo cartBusinessDo) {
    	List<PaymentTypeMasterWebDO> allowedTypes = null;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Integer userId = ((Integer) fs.getSessionData().get(WebConstants.SESSION_USER_USERID));
        
        if(!(ProductName.WalletCash == cartService.getPrimaryProduct(cartBusinessDo))){
        	allowedTypes = new ArrayList<PaymentTypeMasterWebDO>();
        	for(PaymentTypeMasterWebDO paymentTypeMasterWebDO : paymentTypes) {
        		if(PaymentConstants.DEBIT_CARD.equals(paymentTypeMasterWebDO.getName()) || PaymentConstants.CREDIT_CARD.equals(paymentTypeMasterWebDO.getName())) {
        			allowedTypes.add(paymentTypeMasterWebDO);
        		}
        	}
        	return allowedTypes;
        }
		return paymentTypes;
	}

	/*
	 * Ajax call to get user profile number to display it in Alert number text box
	 */
	@RequestMapping(value = "app/sms_alert_data", method = RequestMethod.GET, produces = "application/json")
	public String getSmsAlertData(HttpServletRequest request, HttpServletResponse response, Model model) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Integer userId = ((Integer) fs.getSessionData().get(WebConstants.SESSION_USER_USERID));
		Users users = userServiceProxy.getUserByUserId(userId);
		String profileMobileNo = users.getMobileNo();
		model.addAttribute("profileMobileNo", profileMobileNo);
		model.addAttribute("status","success");
		return "jsonView"; 
	}
	

    private boolean forceCaptcha(FreechargeSession fs) {
        if(appConfigService.isRecaptchaEnabled()) {
            return captchaService.shouldCheckCaptcha(fs);
        } 
        return false;
    }
}
