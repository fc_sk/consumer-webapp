package com.freecharge.web.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.service.InCachedService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/admin/operatorretry/*")
public class OperatorRetryConfigController extends BaseAdminController {
    private Logger logger=LoggingFactory.getLogger(getClass());
    
    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private InService             inService;

    @Autowired
    private InCachedService       inCachedService;

    public static final String    OPERATOR_RETRY_CONFIG_URL = "/admin/operatorretry/home";

    public static final int       INVALID                   = -1;
    public static final String    OPERATOR_RETRY_ID         = "id";

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String adminhome(@RequestParam Map<String, String> mapping,
            @ModelAttribute("AdminUsers") AdminUsers adminUsers, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.OPERATOR_RETRY_CONFIG)) {
            return get403HtmlResponse(response);
        }

        List<Map<String, Object>> operatorMap = operatorCircleService.getAllOperators();

        List<InRechargeRetryMap> allOperatorRetryMap = inService.getAllOperatorRetryConfig();

        model.addAttribute(WebConstants.ALL_OPERATORS, operatorMap);
        model.addAttribute(WebConstants.ALL_OPERATOR_RETRY_MAP, allOperatorRetryMap);

        int operatorRetryId = INVALID;
        try {
            operatorRetryId = Integer.parseInt(request.getParameter(OPERATOR_RETRY_ID));
        } catch (Exception e) {

        }
        if (operatorRetryId != INVALID) {
            InRechargeRetryMap inRechargeRetryMap = inService.getOperatorRetryConfigFromId(operatorRetryId);
            if (inRechargeRetryMap != null) {
                model.addAttribute(WebConstants.OPERATOR_RETRY, inRechargeRetryMap);
            }
        }

        return ViewConstants.OPERATOR_RETRY_CONFIG_VIEW;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude = true)
    public String doSave(InRechargeRetryMap inRechargeRetryMap, Model model) {
        try {
            if (inRechargeRetryMap.getId() == null || inRechargeRetryMap.getId() == -1) {
                inService.insertInRechargeRetry(inRechargeRetryMap);
            } else {
                inService.updateInRechargeRetry(inRechargeRetryMap);
            }
            model.addAttribute(WebConstants.STATUS, "Updated");
            inCachedService.setOperatorRetryConfig(inService.getInRechargeRetryDetails());
            return "jsonView";
        } catch (Exception e) {
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Exception occured while updating/creating operator retry map.");
            logger.error("Exception occured while updating/creating operator retry map ",e);
            return "jsonView";
        }
    }
    
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude = true)
    public String doDelete(InRechargeRetryMap inRechargeRetryMap, Model model) {
        try {
            if (inRechargeRetryMap.getId() == null) {
                model.addAttribute(WebConstants.STATUS, "Error");
                model.addAttribute(WebConstants.ERROR_MESSAGE, "Config Id is null. Unable to delete.");
            } else {
                inService.deleteOperatorRetryConfig(inRechargeRetryMap.getId());
            }
            model.addAttribute(WebConstants.STATUS, "Deleted");
            inCachedService.setOperatorRetryConfig(inService.getInRechargeRetryDetails());
        } catch (Exception e) {
            model.addAttribute(WebConstants.STATUS, "Error");
            model.addAttribute(WebConstants.ERROR_MESSAGE, "Exception occured while updating/creating operator retry map.");
            logger.error("Exception occured while updating/creating operator retry map ",e);
        }
        return "jsonView";
    }
}
