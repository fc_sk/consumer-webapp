package com.freecharge.web.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.CommonService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.service.NewSEOService;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.FeedbackWebDO;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.UserDO;

@Controller
@RequestMapping("/app/*")
public class StaticPagesController {
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    NewSEOService newSEOService;
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "contactus", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView contactUs(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/contactus";
        model.addAttribute("page", "contactus");
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }
    
    @RequestMapping(value = "pepsicampaign", method = RequestMethod.GET)
    @NoLogin
    public String pepsicampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if(appConfigService.isPepsiCampaignEnabled()) {
            return "pepsicampaign";
        } else {
            return this.get403HtmlResponse(response);
        }
    }

    @RequestMapping(value = "newpepsicampaign", method = RequestMethod.GET)
    @NoLogin
    public String newpepsicampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if(appConfigService.isPepsiCampaignEnabled()) {
            return "newpepsicampaign";
        } else {
            return this.get403HtmlResponse(response);
        }
    }

    @RequestMapping(value = "DanceIndiaDance4-details", method = RequestMethod.GET)
    @NoLogin
    public String did4Details(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "did4Details";
    }

    @RequestMapping(value = "mcdGiftFestival-details", method = RequestMethod.GET)
    @NoLogin
    public String mcdGiftFestivalDetails(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mcdGiftFestivalDetails";
    }

    @RequestMapping(value = "past-campaigns", method = RequestMethod.GET)
    @NoLogin
    public String pastCampaigns(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "home/pastcampaigns";
    }
    
    @RequestMapping(value = "aboutus", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView aboutUs(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/aboutus";
        model.addAttribute("page", "aboutus");
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "termsandconditions", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView termsAndConditions(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/termsandconditions";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("page", "termsandconditions");
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "upiterms", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView upiterms(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/upiterms";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("page", "upiterms");
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "privacypolicy", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView privacyPolicy(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/privacypolicy";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("page", "privacypolicy");
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "feedback", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView feedbackPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/feedback";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("page", "feedback");
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "feedback", method = RequestMethod.POST)
    @NoLogin
    public String feedback(@ModelAttribute(value = "feedbackWebDO") FeedbackWebDO feedbackWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        //String view = "home/feedback";
        try {
            String delegateName = WebConstants.DELEGATE_BEAN_FEEDBACK;
            WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, feedbackWebDO);
            model.addAttribute("status", ((FeedbackWebDO) webContext.getBaseWebDO()).getStatus());
            model.addAttribute("page", "feedback");
            model.addAttribute("stateMaster", commonService.getStateMasterList());

        } catch (Exception e) {
            logger.error("Exception occured in staticPages controller fro feed back", e);
            model.addAttribute("status", "fail");
        }
        return "jsonView";
    }

    @RequestMapping(value = "faq.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView faq(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/faq";
        model.addAttribute("page", "faq");
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "customersupport.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView customerSupport(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/customersupport";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "sitemap.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView siteMap(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/sitemap";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "rss.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView rss(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/rss";
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "surveyfeedback.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView surveyfeedback(HttpServletRequest request, HttpServletResponse response) {
        String view = "home/surveyfeedback";
        ModelAndView modelAndView = new ModelAndView(view);
        modelAndView.addObject("stateMaster", commonService.getStateMasterList());
        return modelAndView;
    }
    
    @RequestMapping(value = "freefundtnc.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView freefundtnc(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/freefundtnc";
        model.addAttribute("page", "freefundtnc");
        model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
        model.addAttribute("stateMaster", commonService.getStateMasterList());
        return new ModelAndView(view);
    }

    @RequestMapping(value = "dynamiccampaigns.htm", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView dynamicCampaigns(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/dynamiccampaigns";
        
        String campaignName = request.getParameter("campaign");
        String product = request.getParameter("product");
        
        try {
            // Handle SEO Redirection Url Params
            if (!StringUtils.isEmpty(campaignName) && !StringUtils.isEmpty(product)) {
                SEOAttributesBusinessDO seoBusinessDO = newSEOService
                        .populateSeoAttributes(product, campaignName, null);
                model.addAttribute("seoAttributes", seoBusinessDO);

            }
        	if (!StringUtils.isEmpty(campaignName)) {
        		StringBuilder htmlFile = new StringBuilder();

        		htmlFile.append(campaignName);
        		htmlFile.append(".jsp");
        		model.addAttribute("htmlFile",htmlFile.toString());
        	}

        	model.addAttribute("userDO", new UserDO());
        	model.addAttribute("loginWebDO", new LoginWebDO());
        	model.addAttribute("page", "campaign");
            model.addAttribute("stateMaster", commonService.getStateMasterList());  

        } catch (Exception e) {
            logger.error("Exception in dynamiccampaigns controller", e);
        }
        return new ModelAndView(view);
    }

    @RequestMapping(value = "fbinvitepage.htm", method = RequestMethod.GET)
    public ModelAndView fbInvites(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/fbinvites";
        return new ModelAndView(view);
    }
    
    @RequestMapping(value = "GOSF", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView gosf(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "campaign/gosf";
        model.addAttribute("page", "gosf");
        return new ModelAndView(view);
    }
    
    private String get403HtmlResponse(HttpServletResponse response) throws IOException{
        return this.get403Response(response, "text/html");
    }
    
    private String get403Response(HttpServletResponse response, String contentType) throws IOException {
        return this.getErrorResponse(response, HttpServletResponse.SC_FORBIDDEN, contentType, "Permission Denied. ");
    }
    
    private String getErrorResponse(HttpServletResponse response, Integer status, String contentType, String message)
            throws IOException{
        response.setStatus(status);
        response.setContentType(contentType);
        PrintWriter pw = response.getWriter();
        pw.write(message);
        pw.flush();
        return null;
    }

}
