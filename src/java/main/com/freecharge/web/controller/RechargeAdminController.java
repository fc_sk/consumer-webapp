package com.freecharge.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.customercare.service.CustomerTrailService;
import com.freecharge.customercare.service.CustomerTrailService.AG_REVERSAL_STATUS;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.ActivityLogger;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.cron.PendingRechargeFulfillment;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleExecutorService;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/rechargeRequestStatus/*")
public class RechargeAdminController extends BaseAdminController {

       @Autowired
       private PendingRechargeFulfillment pendingRechargeFulfillment;

       private static Logger logger = LoggingFactory.getLogger(RechargeAdminController.class);

       private static final String RECHARGE_STATUS = "rechargeStatus";

       public static final String REVERSAL_HANDLER_VIEW = "/admin/rechargeRequestStatus/reversalHandler.htm";

       public static final String AG_PREFERENCE_URL = "/admin/rechargeRequestStatus/agpreference.htm";

       @Autowired
       private RechargeSchedulerService rechargeSchedulerService;

       @Autowired
       private InService inService;

       @Autowired
       private AdminHelper adminHelper;

       @Autowired
       private BillPaymentService        billPaymentService;

       @Autowired
       private FulfillmentScheduleExecutorService scheduleExecutor;

       @Autowired
       private PaymentTransactionService paymentTransactionService;

       @Autowired
       private CTActionControlService ctActionControlService;

       @Autowired
       private RechargeMetricService metricService;
       
       @Autowired
       private KestrelWrapper kestrelWrapper;
       
       @Autowired
       private OrderIdSlaveDAO orderidDao;
       
       @Autowired
       private OrderService orderService;

       @Autowired
       private CustomerTrailService customerTrailService;
       
       public static final String RECHARGE_REQUEST_ID = "requestId";
       
       public static final String ORDER_ID = "orderId";
       public static final String PRODUCT_TYPE = "productType";
       public static final String REVERSAL_TYPE_ORDERID = "orderid";
       public static final String REVERSAL_TYPE_REQUESTID = "requestid";
       
       @RequestMapping(value = "getStatusCheck", method = RequestMethod.GET)
       public final String getRechargeStatus(@RequestParam final Map<String, String> mapping, final Model model,
                     final HttpServletRequest request, final HttpServletResponse response) {
              response.setContentType("application/json");
              String orderId = mapping.get(RechargeAdminController.ORDER_ID);  
              String productType = mapping.get(RechargeAdminController.PRODUCT_TYPE);
              logger.info("Product Type for Status Check for orderId:"+orderId+" is "+productType);
              logger.info(ActivityLogger.RechargeStatus + "initiated by "+ getCurrentAdminUser(request)+" for Order Id "+orderId);
              if (productType == null) {
            	  model.addAttribute(RECHARGE_STATUS, "No data found to do status check"); 
            	  return "jsonView";
              }
              if(rechargeSchedulerService.isEligibleForStatusCheck(orderId)) {
                  logger.info("Order: "+orderId+" is eligible for status check.");
                  try {
                      if (FCUtil.isRechargeProductType(productType)) {
                          logger.info("Started recharge status(recharge-product) check process for orderId : " + orderId);
                          InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId);
                          rechargeSchedulerService.checkStatusAndUpdate(orderId, StatusCheckType.Relaxed, inTransactionData.getInResponse(),false);
                      } else if (FCUtil.isBillPostpaidType(productType)) {
                          logger.info("Started recharge status(bill-postpaid) check process for orderId : " + orderId);
                          kestrelWrapper.enqueueForPostpaidStatusCheck(orderId);
                      } else if (FCUtil.isUtilityPaymentType(productType)) {
                          String billId = orderidDao.getBillIdForOrderId(orderId, productType);
                          String lookupId = orderidDao.getLookupIdForOrderId(orderId);
                          kestrelWrapper.enqueueForBillPayStatusCheck(billId, lookupId, StatusCheckType.Relaxed);
                          logger.info("Started recharge status(bill-payment) check process for orderId : " + orderId);
                      }
                      model.addAttribute(RECHARGE_STATUS, "Successfuly updated the status, click Ok to refresh the page");
                  } catch (Exception ex) {
                      logger.error("Exception raised during status check api from customertrail for orderId " + orderId, ex);
                      model.addAttribute(RECHARGE_STATUS, "Exception Raised Please Try After Some Time");
                  }
              } else {
                  logger.info("Order: "+orderId+" is not eligible for status check.");
                  model.addAttribute(RECHARGE_STATUS, "Order is more than 5 days old, cannot trigger status check");
              }
              return "jsonView";
       }

	// Reversal handler from Customer trial
	@RequestMapping(value = "reversal-customertrail", method = RequestMethod.POST)
	@ResponseBody
	public final Map<String, String> reversalFromCustomerTrail(
			final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if(!isAuthorizedToUpdateCt(request)) {
			logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel for reversal " + request.getParameter("reversalOrderId"));
			return null;
		}
		
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}

		Map<String, String> reversalResponse = new HashMap<String, String>();
		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		String requestId = request.getParameter("requestId");
		String rechargeStatus = request.getParameter("rechargeStatus");
		String orderId = request.getParameter("reversalOrderId");
		String productType = request.getParameter("reversalProductType");
		logger.info(ActivityLogger.RechargeReversal + "initiated by "+ getCurrentAdminUser(request)+ " for request "+requestId);
		boolean isMobilePostpaid = false;
		boolean isBillPayment = false;
		boolean isRechargeType = false;
		boolean isValidate = true;
		
		if (FCUtil.isEmpty(rechargeStatus) || FCUtil.isEmpty(productType)) {
			reversalResponse.put("message", "Reversal procecss failed : ProductType or Recharge status missing,contact dev");
			reversalResponse.put("status", "failed");
			return reversalResponse;
		}
		
		isRechargeType = FCUtil.isRechargeProductType(productType);
		isMobilePostpaid = FCUtil.isBillPostpaidType(productType);
		isBillPayment = FCUtil.isUtilityPaymentType(productType);
	
		if (isRechargeType) {
			if (FCUtil.isEmpty(requestId)) {
				isValidate = false;
			}
		} else if (isMobilePostpaid) {
			if (FCUtil.isEmpty(orderId)) {
				isValidate = false;
			}
		} else if (isBillPayment) {
		    if (FCUtil.isEmpty(orderId)) {
                isValidate = false;
            }
		}

		if (!isValidate) {
			reversalResponse.put("message", "Reversal procecss failed : requestId or orderId are missing,contact dev");
			reversalResponse.put("status", "failed");
			return reversalResponse;
		}

		double refundAmount = 0.0;
		/*
		 * Checking whether CS guy has sufficient avl-credits to do reversal -
		 * success to failure
		 */
		if (rechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
			PaymentTransaction paymentTransaction = paymentTransactionService
					.getLastPaymentTransactionSentToPG(orderId);
			if (paymentTransaction != null) {
				try {
					refundAmount = adminHelper.getManualRefundAmount(orderId);
				} catch (Exception e) {
					logger.error("Exception while getting refund amount for orderid for reversal case for checking sufficient credits :"
							+ paymentTransaction.getOrderId());
					reversalResponse
							.put("message",
									"Exception while getting refund amount before checking avail-credits of cs-agent to do reversal, pls contact manager.");
					reversalResponse.put("status", "failed");
					return reversalResponse;
				}
				if (!ctActionControlService
						.hasSufficientCreditToDoCashBackOrRefund(
								getCurrentAdminUser(request), refundAmount)) {
					reversalResponse
							.put("message",
									"Can not do reversal:"
											+ getCurrentAdminUser(request)
											+ " does not have sufficient credits to do this reversal(or exception while checking available credits), pls contact manager.");
					reversalResponse.put("status", "failed");
					return reversalResponse;
				}
			}
		}

		Boolean reversalHandleSuccess = null;
		if (isRechargeType) {
			logger.info(String
					.format("Started reversal(rechargetype) handling from customertrail for : [ %s  to  %s ]",
							requestId, rechargeStatus));
			reversalHandleSuccess = pendingRechargeFulfillment
					.updateRechargeStatus(requestId, rechargeStatus, null,null);
			logger.info(String
					.format("Finished reversal(rechargetype) handling from customertrail for : [ %s  to  %s ]",
							requestId, rechargeStatus));
			logger.info("Status changed by user: " + getCurrentAdminUser(request));
		} else if (isMobilePostpaid) {
			logger.info(String
					.format("Started reversal(postpaid) handling from customertrail for : [ %s  to  %s ]",
							orderId, rechargeStatus));
			reversalHandleSuccess = billPaymentService
					.updatePostpaidPaymentStatus(orderId, rechargeStatus, null,null);
			logger.info(String
					.format("Finished reversal(postpaid) handling from customertrail for : [ %s  to  %s ]",
							orderId, rechargeStatus));
			logger.info("Status changed by user: " + getCurrentAdminUser(request));
		} else if (isBillPayment) {
		    logger.info(String
                    .format("Started reversal(billapy) handling from customertrail for : [ %s  to  %s ]",
                            orderId, rechargeStatus));
            reversalHandleSuccess = billPaymentService.reverseBillPayTransactionToFailure(orderId, productType);
            logger.info(String
                    .format("Finished reversal(billpay) handling from customertrail for : [ %s  to  %s ]",
                            orderId, rechargeStatus));
			logger.info("Status changed by user: " + getCurrentAdminUser(request));
		}

		if (reversalHandleSuccess) {
			/* Deducting avail-credits of cs-agent after success */
			if (rechargeStatus.equals("failure")) {
				if (!ctActionControlService.deductAvailableCreditLimits(getCurrentAdminUser(request), refundAmount)) {
					logger.error("Exception on deducting available-credits of cs-agent " + getCurrentAdminUser(request) + " after reversal case");
					reversalResponse.put("message", "Reversal process success, but exception on deducting available-credits of cs-agent, contact manager.");
					reversalResponse.put("status", "success");
					/*
					 * Send a mail if cs-agent has crossed 90% of his max
					 * credits limit
					 */
					if (ctActionControlService.hasCrossedNinetyPercentOfCredits(getCurrentAdminUser(request))) {
						ctActionControlService.emailOnNinetyPercentCreditsReached(getCurrentAdminUser(request));
					}
					return reversalResponse;
				}
			}
			reversalResponse.put("message", "Reversal process success");
			reversalResponse.put("status", "success");
            if (isMobilePostpaid || isBillPayment) {
                resultObjectMap.put(orderId, adminHelper.getResultObjectListEmailContent(orderId, "", rechargeStatus));
            } else if (isRechargeType) {
                if (getOrderId(requestId) != null) {
                    resultObjectMap.put(getOrderId(requestId), adminHelper.getResultObjectListEmailContent(
                            getOrderId(requestId), requestId, rechargeStatus));
                }
            }
			/*
			 * Send a mail if cs-agent has crossed 90% of his max credits limit
			 */
			if (ctActionControlService.hasCrossedNinetyPercentOfCredits(getCurrentAdminUser(request))) {
				ctActionControlService.emailOnNinetyPercentCreditsReached(getCurrentAdminUser(request));
			}
		} else {
			reversalResponse.put("message", "Reversal process failed");
			reversalResponse.put("status", "failed");
		}
		if (resultObjectMap != null && resultObjectMap.size() > 0) {
			adminHelper.sendAlertEmail(resultObjectMap,"Reversal From CustomerTrail", getCurrentAdminUser(request));
		}
		return reversalResponse;
	}

       @RequestMapping(value = "schedule/execute", method = RequestMethod.POST)
       @NoSessionWrite
       public @ResponseBody
       Map<String, String> executeSchedule(final HttpServletRequest request, final HttpServletResponse response) {
              String orderId = request.getParameter("orderId");

              if (StringUtils.isBlank(orderId)) {
                     return ErrorCode.INVALID_ORDER_ID.restResponse();
              }

              boolean executed = scheduleExecutor.executeNow(orderId);

              if (executed) {
                     return ErrorCode.NO_ERROR.restResponse();
              }

              return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
       }

       @RequestMapping(value = "schedule/cancel", method = RequestMethod.POST)
       @NoSessionWrite
       public @ResponseBody
       Map<String, String> cancelSchedule(final HttpServletRequest request, final HttpServletResponse response) {
              String orderId = request.getParameter("orderId");

              if (StringUtils.isBlank(orderId)) {
                     return ErrorCode.INVALID_ORDER_ID.restResponse();
              }

              boolean executed = scheduleExecutor.cancel(orderId);

              if (executed) {
                     return ErrorCode.NO_ERROR.restResponse();
              }

              return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
       }

       /* Reversal tool view */
       @RequestMapping(value = "reversalHandler", method = RequestMethod.GET)
       public final String getReversalToolView(final Model model, final HttpServletRequest request,
                     final HttpServletResponse response) throws Exception {
              if (!hasAccess(request, AdminComponent.REVERSAL_CASE_HANDLER)) {
                     get403HtmlResponse(response);
                     return null;
              }
              model.addAttribute("rechargeStatusList", AG_REVERSAL_STATUS.values());
              model.addAttribute("aggNameList", Arrays.asList("Select aggregator", "oxigen", "euronet"));
              return ViewConstants.REVERSAL_HANDLER_VIEW;
       }

       private String getProductTypeAfterOperatorRetryCheck(String inputId, String productType) {
              Boolean isRechargeAttemptNotFailure = customerTrailService.isRechargeNotFailureForOrder(inputId);
              Boolean isBillPaymentAttemptNotFailure = customerTrailService.isBillPaymentNotFailureForOrder(
                   inputId);
              String finalProductType = customerTrailService.getFinalOperatorRetryProductType(
                   isRechargeAttemptNotFailure, isBillPaymentAttemptNotFailure, productType, inputId);
              return finalProductType;
       }

       @RequestMapping(value = "reversalHandler", method = RequestMethod.POST)
       public final ModelAndView doReveresal(@RequestParam("fileInputIds") final MultipartFile file,
    		   @RequestParam(required = false, value = "inputIds") final String inputIdsString,
    		   @RequestParam(required = false, value = "aggregatorName") final String givenAggregatorName, final Model model,
    		   final HttpServletRequest request, final HttpServletResponse response) throws IOException {

    	   if (!hasAccess(request, AdminComponent.REVERSAL_CASE_HANDLER)) {
    		   get403HtmlResponse(response);
    		   return null;
    	   }

    	   model.addAttribute("rechargeStatusList", AG_REVERSAL_STATUS.values());
    	   model.addAttribute("aggNameList", Arrays.asList("Select aggregator", "oxigen", "euronet"));

    	   logger.debug("Reversal Request for Aggregator : " + givenAggregatorName);
    	   if(FCUtil.isEmpty(FCUtil.trim(givenAggregatorName))) {
    		   model.addAttribute("messages", "Reversal procecss failed : Please select valid aggregator");      
    		   return new ModelAndView(ViewConstants.REVERSAL_HANDLER_VIEW);
    	   } 

    	   List<Map<String, String>> requestList = new ArrayList<Map<String, String>> ();

    	   if(FCUtil.isNotEmpty(FCUtil.trim(inputIdsString))) {
    		   requestList = customerTrailService.getReversalRequestFromText(inputIdsString);
    	   } 

    	   if (FCUtil.isEmpty(requestList)) {
    		   requestList = customerTrailService.getReversalRequestSetFromFile(file);	
    	   } 

    	   if (FCUtil.isEmpty(requestList)) { 
    		   model.addAttribute("messages", "Reversal procecss failed : No valid data found for reversal");      
    		   return new ModelAndView(ViewConstants.REVERSAL_HANDLER_VIEW);
    	   }

    	   String messages = "";

    	   List<String> successStatusChangeList = new ArrayList<String>();
    	   List<String> failureStatusChangeList = new ArrayList<String>();

    	   for (Map<String, String> requestData : requestList)
    	   {

    		   if (FCUtil.isEmpty(requestData.get(RechargeConstants.REQUEST_ID)) || FCUtil.isEmpty(requestData.get(RechargeConstants.RECHARGE_STATUS))) {
    			   logger.info("Inalid Request / Order Id : " + requestData.get(RechargeConstants.REQUEST_ID) 
    			   + " OR RechargeStatus : " + requestData.get(RechargeConstants.RECHARGE_STATUS));
    			   continue;
    		   }

    		   String validRequestId = requestData.get(RechargeConstants.REQUEST_ID);
    		   String requestIdWithRequestCounter = validRequestId;
    		   String validRechargeStatus =  AG_REVERSAL_STATUS.valueOf(requestData.get(RechargeConstants.RECHARGE_STATUS).toUpperCase()).toString();
    		   String agRefId =  requestData.get(RechargeConstants.AG_REF_ID);

    		   Boolean reversalHandleSuccess = false;
    		   validRequestId = validRequestId.substring(validRequestId.indexOf("_") + 1);
    		   if(FCUtil.validateOrderIdFormat(validRequestId)) {

    			   String initialProductType = orderService.getProductTypeForAllRechargeCase(validRequestId);
    			   String retryProductType = getProductTypeAfterOperatorRetryCheck(validRequestId, initialProductType);
    			   String productType = null;
    			   if(!retryProductType.equals(initialProductType)) {
    				   logger.info("Updating productType to "+retryProductType+" for inputId: "+validRequestId);
    				   productType = retryProductType;
    			   } else {
    				   productType = initialProductType;
    			   }
    			   if (FCUtil.isRechargeProductType(productType)) {
    				   InRequest inRequest = inService.getLatestInRequestId(validRequestId); 
    				   if (inRequest == null || inRequest.getRequestId() == null) {
    					   continue;
    				   } 
    				   logger.info(String.format("Started recharge-reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));
    				   reversalHandleSuccess = pendingRechargeFulfillment.updateRechargeStatus(inRequest.getRequestId().toString(), validRechargeStatus, 
    						   givenAggregatorName, agRefId);
    				   logger.info(String.format("Finished reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));	 
    			   } else if (FCUtil.isBillPostpaidType(productType)) {
    				   logger.info(String.format("Started postpaid-reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));
    				   reversalHandleSuccess = billPaymentService.updatePostpaidPaymentStatus(validRequestId, validRechargeStatus, 
    						   givenAggregatorName, agRefId);
    				   logger.info(String.format("FInished reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));                    		 
    			   } else if (FCUtil.isUtilityPaymentType(productType)) {
    				   logger.info(String.format("Started billpay-reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));
    				   if (validRechargeStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
    				       reversalHandleSuccess = billPaymentService.reverseBillPayTransactionToFailure(validRequestId, productType);
    				       logger.info(String.format("FInished billpay-reversal from reversal tool for : [ %s  to  %s ]", validRequestId, validRechargeStatus));
    				   } else {
    				       logger.error("Cannot move billpay txn to success.");
    				   }
    			   }

    		   } else {
    			   /*Here InputId may be requestId */
    			   String requestCounter = requestIdWithRequestCounter.indexOf("_") != -1 ? requestIdWithRequestCounter.substring(0,requestIdWithRequestCounter.indexOf("_")) : null;
    			   InResponse inResponse = inService.getInResponseByRequestId(Long.valueOf(validRequestId));
				if (requestCounter != null) {
					if (inResponse != null && inResponse.getRetryNumber() != null
							&& inResponse.getRetryNumber().equals(Integer.valueOf(requestCounter))) {
						logger.info(String.format("Started reversal from reversal tool with request counter for : [ %s  to  %s ]",
								validRequestId, validRechargeStatus));
						reversalHandleSuccess = pendingRechargeFulfillment.updateRechargeStatus(validRequestId,
								validRechargeStatus, givenAggregatorName, agRefId);
						logger.info(String.format("Finished reversal from reversal tool with request counter for : [ %s  to  %s ]",
								validRequestId, validRechargeStatus));
					}
				} else{
					logger.info(String.format("Started reversal from reversal tool for : [ %s  to  %s ]",
							validRequestId, validRechargeStatus));
					reversalHandleSuccess = pendingRechargeFulfillment.updateRechargeStatus(validRequestId,
							validRechargeStatus, givenAggregatorName, agRefId);
					logger.info(String.format("Finished reversal from reversal tool for : [ %s  to  %s ]",
							validRequestId, validRechargeStatus));
				}
    		   }

    		   if (reversalHandleSuccess) {
    			   successStatusChangeList.add(String.format("Reversal process success : [ %s ]", validRequestId));
    		   } else {
    			   failureStatusChangeList.add(String.format("Reversal process failed : [ %s ]", validRequestId));
    		   }
    	   }

    	   messages += "Status change process done successfully. Click on Show Status to view the details ";
    	   model.addAttribute("messages", messages);
    	   model.addAttribute("successStatusChangeList", successStatusChangeList);
    	   model.addAttribute("failureStatusChangeList", failureStatusChangeList);
    	   model.addAttribute("successCount", successStatusChangeList.size());
    	   model.addAttribute("failureCount", failureStatusChangeList.size());
    	   return new ModelAndView(ViewConstants.REVERSAL_HANDLER_VIEW);
       }

    private String getOrderId(String requestId) {
        if (FCUtil.isEmpty(requestId)) {
            return null;
        }
        InRequest inRequest = inService.getInRequest((Long.parseLong(requestId)));
        if (requestId != null) {
            return inRequest.getAffiliateTransId();
        }
        return null;
    }

       @RequestMapping(value = "agpreference", method = RequestMethod.GET)
       public ModelAndView fetchAggregatorPreference(Model model, HttpServletRequest request,
                     HttpServletResponse response) {
              List<Map<String, Object>> agpreference = inService.fetchAgPreferenceObj();
              ModelAndView view = new ModelAndView(ViewConstants.AG_PREFERENCE);
              if (agpreference != null) {
                     view.addObject("view", agpreference);
              }
              return view;
       }
       
       @RequestMapping(value = "updateAggregatorPreference", method = RequestMethod.POST)
       public String updateAggregatorPreference(@RequestParam final Map<String, Object> map, Model model,
                     HttpServletRequest request, HttpServletResponse response) {
              Boolean responseStatus=true;
              try {
                     model = model.addAttribute("status", "exception");
                     if (map != null && map.size() > 0) {
                            Integer count = 0;
                            for (Map.Entry<String, Object> entry : map.entrySet()) {
                                   if ((entry.getKey().contains("is_") && (entry.getValue()!=null))
                                                 && (Integer.parseInt(entry.getValue().toString()) < 0 || Integer
                                                               .parseInt(entry.getValue().toString()) > 1) && responseStatus) {
                                          responseStatus = false;
                                   }
                                   if (entry.getKey().contains("ag_")) {
                                          if (Integer.parseInt(entry.getValue().toString()) < 0) {
                                                 responseStatus = false;
                                          } else {
                                                 count += Integer.parseInt(entry.getValue().toString());
                                          }
                                   }
                            }
                            if (count != 100 && responseStatus) {
                                   responseStatus = false;
                                   model = model.addAttribute("status", "Please check. Total sum of percentage values of all AG must be 100");
                            }
                     } else {
                            responseStatus = false;
                     }
                     String[] arrayOfAgTxnRatio = map.get(RechargeConstants.AG_TXN_RATIO).toString().split("~");
                     for (int agTxnRatio = 0 ; agTxnRatio < arrayOfAgTxnRatio.length; agTxnRatio++) {
                            for (Map.Entry<String, Object> entry : map.entrySet()) {
                                   if (entry.getKey().contains("ag_") && arrayOfAgTxnRatio[agTxnRatio].contains(entry.getKey().split("_")[1])) {
                                          String agTxnRatiovalue = (arrayOfAgTxnRatio[agTxnRatio].split("-")[0]) + "-" + entry.getValue();
                                          arrayOfAgTxnRatio[agTxnRatio] =agTxnRatiovalue;
                                   } else {
                                          continue;
                                   }
                            }
                     }
                     StringBuffer stringBuffer = new StringBuffer("");
                     for (int agTxnRatio = 0 ; agTxnRatio < arrayOfAgTxnRatio.length; agTxnRatio++) {
                            if(agTxnRatio>0)
                               stringBuffer.append("~"); 
                               stringBuffer.append(arrayOfAgTxnRatio[agTxnRatio]);
                            
                     }
                     map.put(RechargeConstants.AG_TXN_RATIO, stringBuffer.toString());
                     
                     for (Map.Entry<String, Object> entry : map.entrySet()) {
                            if (entry.getKey().contains("ag_")) {
                                   if(map.get(RechargeConstants.AG_TXN_RATIO).toString().contains(entry.getKey().split("_")[1])) {
                                          continue;
                                   } else {
                                          String addNewAgToTxnRatio = map.get(RechargeConstants.AG_TXN_RATIO) + "~" + entry.getKey().split("_")[1] + "-" +entry.getValue();
                                          map.put(RechargeConstants.AG_TXN_RATIO, addNewAgToTxnRatio);
                                   }
                            } 
                     }
                     Integer agpreference = inService.updateAgPreference(map);
                     if (agpreference != null && map.get(RechargeConstants.AG_TXN_RATIO)!=null && responseStatus) {
                            String aggName;
                            String aggPercentageRatio = (String) map.get(RechargeConstants.AG_TXN_RATIO);
                            String operatorId = (String) map.get(RechargeConstants.OPERATOR_ID);
                            String[] aggPercentageObj = aggPercentageRatio.split("~");
                            for (int i = 0; i < aggPercentageObj.length; i++) {
                                   aggName = (aggPercentageObj[i].split("-"))[0];
                                   String key = aggName + "_" + operatorId + "_"
                                                 + RechargeConstants.AGGREGATOR_PREFERENCE;
                                   metricService.deleteAggregatorPreferencekey(key);
                            }
                            model.addAttribute("status", "success");
                            return "jsonView";
                     }
              } catch (Exception e) {
                     logger.error(" exception raised while updating ag preference transaction ", e);
              }
              return "jsonView";
       }
}
