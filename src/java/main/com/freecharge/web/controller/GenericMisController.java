package com.freecharge.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.service.GenericMisDisplayRecords;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.dao.PgMisDisplayRecord;
import com.freecharge.recharge.businessdao.GenericMisData;
import com.freecharge.recharge.services.InAuthenticationFetchService;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/adminPaymentRecharge")
public class GenericMisController {
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private InAuthenticationFetchService inAuthenticationFetchService;
	
	@Autowired
	private FCProperties fcProperties;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
    @NoLogin
	public ModelAndView viewMisLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request) {
		String view = "generic/mis";
		String pageTitle = "FreeCharge generic mis";
	    return new ModelAndView("redirect:/adminPaymentRecharge/searchRecords.htm");
		
		
	} 
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
    @NoLogin
	public ModelAndView adminLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request) {

		String view = "generic/mis";
		if (inAuthenticationFetchService.isMisUserValid(adminUsers)) {
			request.getSession().setAttribute(WebConstants.MIS_USER_SESSION_PARAM, adminUsers);
			return new ModelAndView("redirect:/genericMis/searchRecords.htm");
		} else {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(view);
			mav.addObject("message", "Invalid UserName/Password");
			return mav;
		}
	}
	
	@RequestMapping(value = "searchRecords", method = RequestMethod.GET)
    @NoLogin
	public String showMisScreen(@ModelAttribute("misRequest") GenericMisData misRequestData, HttpServletRequest request, Model model) {
		String view = "genericmis/display";
		int recordsPerPage = 50;
		int numberOfPages = 1;
		int pageCount = 1;

		HttpSession session = request.getSession();
	/*	List<OperatorMaster> operatorMasters = operatorCircleService.getOperators();*/
		List<String> aggregatorName = fcProperties.getPropertyValueList("all.aggregators", ",");
		List<String> paymentgateway = fcProperties.getPropertyValueList("all.paymentGateway", ",");

		if (request.getParameter("iPageNo") != null) {
			pageCount = Integer.parseInt(request.getParameter("iPageNo"));
		}

		misRequestData.setPageNo(pageCount);
		misRequestData.setStartDate(new Date());
		misRequestData.setEndDate(new Date());
		session.setAttribute("misRequest", misRequestData);

		Integer misDataCount = inAuthenticationFetchService.getGenericMisDataCount(misRequestData);
		if (misDataCount % recordsPerPage > 0) {
			numberOfPages = misDataCount / recordsPerPage + 1;
		} else
			numberOfPages = misDataCount / recordsPerPage;

		List<GenericMisDisplayRecords> misData = inAuthenticationFetchService.getGenericMisData(misRequestData, pageCount);

		model.addAttribute("misData", misData);
		model.addAttribute("misRequestData", misRequestData);
		model.addAttribute("paymentgateway", paymentgateway);
		model.addAttribute("inAggrOprCircleMaps", aggregatorName);
		return view;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "searchRecords", method = RequestMethod.POST)
    @NoLogin
	public String showMisReport(@ModelAttribute("misRequest") GenericMisData misRequestData, HttpSession session, Model model) {
		int pageCount = 1;
		int recordsPerPage = 80;
		int numberOfPages = 1;
		String view = "genericmis/display";
		List<String> aggregatorName = fcProperties.getPropertyValueList("all.aggregators", ",");
		List<String> paymentgateway = fcProperties.getPropertyValueList("all.paymentGateway", ",");
		misRequestData.setPageNo(pageCount);
		if (misRequestData.getStartDate() == null && misRequestData.getEndDate() == null) {
			misRequestData.setStartDate(new Date());
			misRequestData.setEndDate(new Date());
		}

		if (misRequestData.getStartDate() != null && misRequestData.getEndDate() == null) {
			misRequestData.setEndDate(new Date());
		}
		Integer misDataCount = inAuthenticationFetchService.getGenericMisDataCount(misRequestData);
		if (misDataCount % recordsPerPage > 0) {
			numberOfPages = misDataCount / recordsPerPage + 1;
		} else
			numberOfPages = misDataCount / recordsPerPage;
		session.setAttribute("misRequest", misRequestData);

		List<GenericMisDisplayRecords> misData = inAuthenticationFetchService.getGenericMisData(misRequestData, pageCount);
		model.addAttribute("misData", misData);
		model.addAttribute("paymentgateway", paymentgateway);
		model.addAttribute("inAggrOprCircleMaps", aggregatorName);
		return view;
	}
	
	
	@RequestMapping(value = "/excelReport", method = RequestMethod.GET)
    @NoLogin
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<PgMisDisplayRecord> excelData = null;
		int pageCount = 500;
		try {
			HttpSession session = request.getSession();
			GenericMisData misRequest = (GenericMisData) session.getAttribute("misRequest");
			Integer prevPageSize = misRequest.getPageSize();
			misRequest.setPageSize(null);
			excelData = (List) inAuthenticationFetchService.getGenericMisData(misRequest, pageCount);
			misRequest.setPageSize(prevPageSize);
			session.setAttribute("misRequest", misRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("genericExcelRevenueSummary", "excelData", excelData);
	}

}
