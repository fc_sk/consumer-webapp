package com.freecharge.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.service.GenericMisService;
import com.freecharge.common.framework.logging.LoggingFactory;

@Controller
@RequestMapping("/transactions/")
public class TransactionController {

	@Autowired
	private GenericMisService genericMisService;
	
	private Logger logger = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "totalSuccessTxns", method = RequestMethod.GET)
    public String getTotalSuccessfulTxns(Model model) {
        try {
            long totalSuccessfulTxns = genericMisService.getTotalSuccessfulTransactions();
            model.addAttribute("RESULT", totalSuccessfulTxns);
            model.addAttribute("STATUS", "SUCCESS");
        } catch (Exception exp) {
            logger.error(" Exception in getTotalSuccessfulTxns : ", exp);
            model.addAttribute("STATUS", "EXCEPTION");
            model.addAttribute("RESULT", null);

            return "jsonView";
        }

        return "jsonView";
    }
}
