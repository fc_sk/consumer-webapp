package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CityMasterWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 1, 2012
 * Time: 4:22:30 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/app/*")
public class CommonController {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Csrf(exclude=true)
    @RequestMapping(value = "getcitylist", method = RequestMethod.POST)
    @NoLogin
    public String getCityList(HttpServletRequest request, HttpServletResponse response, Model model){
        String delegateName = WebConstants.DELEGATE_BEAN_CITYMASTER;
        try{
            WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new CityMasterWebDO());
            model.addAttribute("cityMasterList",((CityMasterWebDO)webContext.getBaseWebDO()).getCityMasterList());
            model.addAttribute("status","success");
        }catch (Exception e){
            logger.error("Exception occured in common controller while retriving cities based on stateId.",e);
            model.addAttribute("status","exception");
        }

        return "jsonView";
    }
}
