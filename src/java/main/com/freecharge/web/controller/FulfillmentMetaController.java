package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.common.util.FCUtil;

@Controller
@RequestMapping("/app/*")
public class FulfillmentMetaController {

    @Autowired
    TxnHomePageDAO txnHomePageDAO;

    @Autowired
    RechargeInitiateController ric;

    @RequestMapping(value = "handlefulfillment", method = RequestMethod.GET)
    public String handleRecharge(HttpServletRequest request, HttpServletResponse response, Model model) {
        String orderId = request.getParameter("orderId");
        if (FCUtil.isEmpty(orderId)) {
            throw new IllegalArgumentException(
                    orderId==null? "Order-ID not passed as request parameter": "Found blank Order-ID");
        }
        TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderId);
        String productType = thp.getProductType();
        switch (productType) {
        case "V":
        case "C":
        case "F":
        case "D":
            return ric.handleRecharge(request, response, model);

        case "M":
            //return "handle/billpayment";
            return ric.handlePostpaid(request, response, model);
            
        case "B":
        case "L":
        case "Y":
        case "E":
        case "I":
        case "G":
//            TODO: This is an amazing crude way of handling this but will soon get fixed.
            return FCUtil.getRedirectAction("/app/recharge.htm?orderId=" + orderId);

        default:
            throw new IllegalStateException("Unsupported product type " + FCUtil.stringify(productType));
        }
    }

}
