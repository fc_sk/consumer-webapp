package com.freecharge.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.adminpage.userban.EmailForm;
import com.freecharge.adminpage.userban.EmailFormValidator;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.ViewConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 24/11/12
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/user/ban")
public class UserBanController {

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
	private UserAuthService userAuthService;

    public static final String BAN_USER_ACTION_LINK = "/admin/user/ban/do.htm";
    public static final String USER_BAN_URL = "/admin/user/ban.htm";
    public static final String UNBLOCK_USER_ACTION_LINK ="/admin/user/ban/undo.htm";
    public static final String RESULT ="result";
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView adminhome(@RequestParam Map<String,String> mapping, @ModelAttribute("command") EmailForm userBanForm, BindingResult bindingResult) {
    	return new ModelAndView(ViewConstants.CUSTOMER_BAN_USER_VIEW, "command", userBanForm);
    }
    
    @RequestMapping(value = "undo")
    public ModelAndView unBan(@RequestParam Map<String, String> params, Model model,@ModelAttribute("command") EmailForm userBanForm, BindingResult bindingResult) {
    	EmailFormValidator formValidator = new EmailFormValidator();
		formValidator.validate(userBanForm, bindingResult);
	
		if (bindingResult.hasErrors()) {
			return new ModelAndView(ViewConstants.CUSTOMER_BAN_USER_VIEW, "command", userBanForm);
		}
    	
    	String email = params.get("email").trim();
        boolean active = userAuthService.unblockUser(email);
        
        if (!active) {
        	model.addAttribute(RESULT, String.format("%s successfully Unblocked.", email));
        } else {
        	model.addAttribute(RESULT, String.format("%s is already Unblocked.", email));
        }
        return new ModelAndView(ViewConstants.CUSTOMER_BAN_USER_RESULT);
    }
    
    @RequestMapping(value = "updateUserCurrentStatus", method = RequestMethod.GET )
    public String updateUserStatus(@RequestParam Map<String, String> params, Model model,HttpServletRequest request, HttpServletResponse response) {
    	
    	try {
    	String email = params.get("email");    	
    	Users userObj = userServiceProxy.getUserByEmailId(email);
        if(userObj.getIsActive()) {
        	boolean inActiveUserCurrentStatus = userAuthService.banUser(email);
        	model.addAttribute(RESULT,"User InActive Successfully");
        } else {
        	boolean activeUserCurrentStatus = userAuthService.unblockUser(email);
        	model.addAttribute(RESULT,"User Active Successfully");
        } 
       } catch(Exception exception) {
    	   model.addAttribute(RESULT,"Please try after some time");
    	   return ("jsonView");
        }
           return "jsonView";
    }
    
    @RequestMapping(value = "UserValidation", method = RequestMethod.GET )
    public String UserValidationCheck(@RequestParam Map<String, String> params, Model model,HttpServletRequest request, HttpServletResponse response) {
    	
    	String password = params.get("password");    
        if("BankRefund".equalsIgnoreCase(password)) {
        	model.addAttribute(RESULT,"success");
        } else {
        	model.addAttribute(RESULT,"fail");
        }
         return "jsonView";
    }

    /**
     * banManyUsers method will ban multiple users at a time by accepting the
     * emailIds from text area
     * 
     * @param params
     * @param model
     * @return
     */
    @RequestMapping(value = "blockmanyusers")
    public ModelAndView banManyUsers(@RequestParam Map<String, String> params, Model model) {
        String email = params.get("emailIds").trim();
        List<String> alreadyBannedEmailIdsList = new ArrayList<>();
        List<String> newlyBannedEmailIdsList = new ArrayList<>();
        List<String> emailsNotFoundList = new ArrayList<>();
        /*
         * entered emailIds will be split based on comma, newline and tabspace
         * \\s*, is also treated as a white space character \s - A whitespace
         * character, short for [ \t\n\x0b\r\f]
         */
        String[] emailIds = email.split("\\s*,\\s*|\\s+");
        for (String emailId : emailIds) {
        	boolean isEmailExists = userServiceProxy.isUserExists(emailId);
            if (isEmailExists) {
                boolean active = userAuthService.banUser(emailId);
                if (!active) {
                    alreadyBannedEmailIdsList.add(emailId);
                } else {
                    newlyBannedEmailIdsList.add(emailId);

                }
                model.addAttribute("alreadyBanned", alreadyBannedEmailIdsList);
                model.addAttribute("newlyBanned", newlyBannedEmailIdsList);
            } else {
                emailsNotFoundList.add(emailId);
                model.addAttribute("notFoundList", emailsNotFoundList);
            }
        }
        return new ModelAndView(ViewConstants.CUSTOMER_BAN_USER_RESULT);
    }

}
 