package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.TestWebDO;


@Controller
@RequestMapping("/app/*")
public class TestController {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public ModelAndView homepoc(HttpServletRequest request, HttpServletResponse response, Model model) {

        String view = "home/view"; 
        String pageTitle = "FreeCharge Home";

        // Setting freecharge home in model
        model.addAttribute("title",pageTitle);


        return new ModelAndView(view);
    }

    @RequestMapping(value = "testingDelegateInt", method = RequestMethod.GET)
    public ModelAndView testDelegate(TestWebDO testWebDO, HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "home/inttest";
        String pageTitle = "FreeCharge Home";
        if (testWebDO == null || testWebDO.getOne() == null)
            return new ModelAndView(view); 
        String delegateName = WebConstants.DELEGATE_BEAN_TEST;

        // Setting freecharge home in model
        try{
            WebContext webContext = DelegateHelper.processRequest(request,response,delegateName,model,testWebDO);
            model.addAttribute("testWebResult",webContext.getBaseWebDO());
            model.addAttribute("title",pageTitle);
        }catch(FCRuntimeException fcexp){
            logger.error("Exception xxxxx ." , fcexp);
        }catch(Exception exp){
            logger.error("Any Exception that could come",exp);
            return new ModelAndView(view);//currently to the same view but have to be redirected to error views.
        }

        
        
        

        return new ModelAndView(view);
    }


}
