package com.freecharge.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.json.JsonResponse;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.tracker.Tracker;
import com.freecharge.useragent.UserLoginEventPublisherHelper;
import com.freecharge.util.TrackerEvent;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletUtil;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.UserDO;

@Controller
@RequestMapping("/app/*")
public class LoginController {
    public static final String LOGIN_ACTION_LINK = "/app/login.htm";

    public static final String METRIC_ACCESS = "loginAccess";
    public static final String METRIC_SERVICE = "login";

    @Autowired
    private Validator validator;

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcproperties;

    @Autowired
    private EPinEncrypt ePinEncrypt;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private WalletService walletService;

    @Autowired
    private WalletUtil walletUtil;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private UserLoginEventPublisherHelper userLoginEventPublisherHelper;

    @Autowired
    private Tracker tracker;
    
    @Autowired
    WalletWrapper walletWrapper;

    public static final String DO_LOGOUT = "doLogout";

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @NoLogin
    public
    @ResponseBody
    String loginDelegate(@ModelAttribute(value = "loginWebDO") LoginWebDO loginWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "loginController");
        tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_ATTEMPT.createTrackingEvent());

        boolean success = false;

        JsonResponse jsonResponse = new JsonResponse();
        JSONArray jsonArray = new JSONArray();
        String delegateName = WebConstants.DELEGATE_BEAN_LOGIN;
        // Setting freecharge home in model
        try {
            ValidationUtils.rejectIfEmpty(bindingResult, "email", "User can not be empty.");
            ValidationUtils.rejectIfEmpty(bindingResult, "password", "Password can not be empty");
            if(loginWebDO.getRememberme() != null && !(loginWebDO.getRememberme().equals("1") || loginWebDO.getRememberme().equals("0"))){
            	bindingResult.addError(new ObjectError("rememberme", "Remember Me feild should only have either 0 or 1"));
            }
            if (bindingResult.hasErrors()) {
                jsonResponse.setStatus("FAIL");
                jsonResponse.setResult(bindingResult.getAllErrors());
            } else {

                /* Check if another user is already logged in */

                if (FCSessionUtil.isAnotherUserLoggedIn(loginWebDO.getEmail())) {
                    jsonResponse.setStatus("ALREADY_LOGGEDIN");
                    jsonResponse.setResult("Another user is already logged in from the same browser. To continue using this account, you will have to sign out the other user and login in again.This is done to protect your account and to ensure the privacy of your information.");
                    jsonArray.add(jsonResponse);
                    return jsonArray + "";
                }

                /*
                     * Code here to set values in PageFormWebDo by delegate
                     */
                WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, loginWebDO);
                boolean isLogin = ((LoginWebDO) webContext.getBaseWebDO()).isLogin();
                if (isLogin) {
                    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "success");
                    String eMail = loginWebDO.getEmail();
                    String firstName = ((LoginWebDO) webContext.getBaseWebDO()).getFirstName();
                    Integer userId = new Integer(((LoginWebDO) webContext.getBaseWebDO()).getUserId());

                    FreechargeSession.setUserRelatedSessionData(eMail, firstName, isLogin, userId,
                            SessionConstants.LoginSource.EMAIL, request.getParameter(FCConstants.IMEI), 
                            request.getParameter(FCConstants.DEVICE_UNIQUE_ID));

                    model.addAttribute("LoginWebDO", webContext.getBaseWebDO());
                    if (loginWebDO.getRememberme().equalsIgnoreCase("1"))
                        setRememberMECookie(response, ((LoginWebDO) webContext.getBaseWebDO()).getUserId());
                    jsonResponse.setStatus("SUCCESS");
                    if (firstName != null && !firstName.isEmpty())
                        firstName = WordUtils.capitalize(firstName);
                    //String freechargeUrl = fcproperties.getProperty(FCConstants.HOSTPREFIX);
                    jsonResponse.setResult(getHeader(firstName));

                    userLoginEventPublisherHelper.publish(request, this);

                    success = true;
                } else {
                    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
                    jsonResponse.setStatus("ERROR");
                    jsonResponse.setResult("Check, mate! Please enter your user name and password correctly.");
                }
            }
            jsonArray.add(jsonResponse);
        } catch (FCRuntimeException fcexp) {
            logger.error("FCRuntimeException in loginDelegate .", fcexp);
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
        } catch (Exception exp) {
            logger.error(" in loginDelegate", exp);
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
        }

        if (success) {
            metricsClient.recordEvent(METRIC_SERVICE, METRIC_ACCESS, "success");
            tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_SUCCESS.createTrackingEvent());
        } else {
            metricsClient.recordEvent(METRIC_SERVICE, METRIC_ACCESS, "failure");
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "login", endTime - startTime);
        return jsonArray + "";
    }

    @Csrf
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request) {
        String view = "redirect:/?logout=yes";

        //All the logout related activities are done in HTTPRequestInterceptor class. This interceptor searches for the below
        //attribute in request and if found, does the logout related activities.
        request.setAttribute(DO_LOGOUT, true);

        return new ModelAndView(view);
    }

    private String getHeader(String firstName) {
        return String.format(
        		"<ul class='clearfix'>" +
            			"<li class='first'>Hi <span id='namediv'>%s</span></li>" +
                        "<li><a class='myaccount-link' href='/app/myrecharges.htm'><i class='icon-account'></i><span class='link-text'>My Account</span></a></li>" +
                        "<li><a href='/app/mycredits.htm'><i class='icon-balance'></i><span class='link-text'>Credits (<span id=\"showbalancediv\"></span>)</span></a></li>" +
                        "<li><a href='http://support.freecharge.in' target='_blank'><i class='icon-support'></i><span class='link-text'>Support</span></a></li>" +
                        "<li class='last'>" +
                        	"<a href='/app/logout.htm?%s=%s'>" +
                            	"<i class='icon-logout'></i><span class='link-text'>Logout</span>" +
                            "</a>" +
                        "</li>" +
                     "</ul>",
                firstName, CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession()
        );
    }

    @RequestMapping(value = "ajaxlogin", method = RequestMethod.POST)
    @NoLogin
    public String ajaxlogin(@ModelAttribute(value = "userDO") UserDO userDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        populatePGOpenNewWindow(model);

        tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_ATTEMPT.createTrackingEvent());
        try {
            String delegateName = WebConstants.DELEGATE_BEAN_PAYMENT_TIME_LOGIN;

            validator.validate(userDO, bindingResult);
            if (!bindingResult.hasErrors()) {
                walletEnabled(model, userDO);
                WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, userDO);
                String loginStatus = ((UserDO) webContext.getBaseWebDO()).getRegistrationStatus();
                if (loginStatus != null) {
                    if (loginStatus.equals("success")) {
                        model.addAttribute("loginSuccess", true);
                        tracker.trackEvent(TrackerEvent.SIGNIN_EMAIL_SUCCESS.createTrackingEvent());
                    } else if (loginStatus.equals("loginfail")) {
                        model.addAttribute("loginErrorMessage", "Email Id and password not correct.");
                    } else {
                        model.addAttribute("loginErrorMessage", "Technical error occured. Please try again later.");
                    }
                }
            } else {
                model.addAttribute("loginErrorMessage", "Email Id and password are required.");

            }
        } catch (Exception exp) {
            model.addAttribute("loginErrorMessage", "Technical error occured. Please try again later.");
            logger.error(exp);
        }

        return "jsonView";

    }

    @RequestMapping(value = "paymenttimesignup", method = RequestMethod.POST)
    @NoLogin
    public String loginasguest(@ModelAttribute(value = "userDO") UserDO userDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        metricsClient.recordEvent(SignupController.METRIC_SERVICE, SignupController.METRIC_ACCESS, "pAttempt");
        tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_ATTEMPT.createTrackingEvent());

        boolean success = false;

        String registrationErrorMessage = "registrationErrorMessage";
        try {
            String delegateName = WebConstants.DELEGATE_BEAN_LOGIN_AS_GUEST;
            validator.validate(userDO, bindingResult);

            if (!bindingResult.hasErrors()) {
                WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, userDO);
                String registrationStatus = ((UserDO) webContext.getBaseWebDO()).getRegistrationStatus();
                if ("success".equals(registrationStatus)) {
                    model.addAttribute("registrationSuccessful", true);

                    userLoginEventPublisherHelper.publish(request, this);

                    success = true;
                    tracker.trackEvent(TrackerEvent.SIGNUP_EMAIL_SUCCESS.createTrackingEvent());
                } else if ("emailExisted".equals(registrationStatus)) {
                    model.addAttribute(registrationErrorMessage, "Your requested email id already existed with us.");
                } else if ("passwordNotMach".equals(registrationStatus)) {
                    model.addAttribute(registrationErrorMessage, "Password and confirm password fields should match.");
                } else {
                    model.addAttribute(registrationErrorMessage, "Technical error occured. Please try again later.");
                }
            } else {
                StringBuffer errors = new StringBuffer();
                for (ObjectError object : bindingResult.getAllErrors()) {
                    errors = errors.append(object.getDefaultMessage() + "<br/>");
                }
                model.addAttribute(registrationErrorMessage, errors.toString());
            }

        } catch (Exception exp) {
            logger.error(exp);
            model.addAttribute(registrationErrorMessage, "Technical error occurred. Please try again later.");
        }

        if (success) {
            metricsClient.recordEvent(SignupController.METRIC_SERVICE, SignupController.METRIC_ACCESS, "pSuccess");
        } else {
            metricsClient.recordEvent(SignupController.METRIC_SERVICE, SignupController.METRIC_ACCESS, "pFailure");
        }

        return "jsonView";
    }

    public void setRememberMECookie(HttpServletResponse response, String userId) {
        String encryptedUserId = null;
        try {
            if (userId != null)
                encryptedUserId = ePinEncrypt.getEncryptedPin(userId);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occured while encrypting userid");
        }
        if (encryptedUserId != null) {
            Cookie userCookie = new Cookie(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_USERID), encryptedUserId);
            userCookie.setMaxAge(Integer.parseInt(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_AGE)));
            userCookie.setPath(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_PATH));

            if (fcproperties.isCookieHttpsOnly()) {
                userCookie.setSecure(true);
            }

            userCookie.setHttpOnly(true);

            //userCookie.setDomain(fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_DOMAINNAME));
            response.addCookie(userCookie);
        }
    }

    public void populatePGOpenNewWindow(Model model) {
        final boolean pgOpenNewWindow = fcproperties.getBooleanProperty(FCConstants.PG_OPEN_NEW_WINDOW);
        model.addAttribute(FCConstants.MODEL_PG_OPEN_NEW_WINDOW, pgOpenNewWindow);
    }

    private void walletEnabled(Model model, UserDO userDO) {
        walletUtil.addWalletSwitches(model);

        if (walletService.isWalletDataDisplayEnabled()) {
            String emailId = userDO.getEmail();
            Users user = userServiceProxy.getUserByEmailId(emailId);
            Amount walletBalance = walletWrapper.getTotalBalance(user.getUserId() == null ? -1 : user.getUserId());
            model.addAttribute("walletBalance", walletBalance.getAmount());
        }
    }
}
