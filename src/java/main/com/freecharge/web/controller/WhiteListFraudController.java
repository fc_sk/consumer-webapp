package com.freecharge.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.adminpage.userban.EmailForm;
import com.freecharge.adminpage.userban.EmailFormValidator;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.FraudWhitelistService;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/user/whitelist")
public class WhiteListFraudController {
    @Autowired
    private FraudWhitelistService fraudWhitelistService;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    public static final String WHITELIST_USER_ACTION_LINK = "/admin/user/whitelist/do.htm";
    public static final String USER_WHITELIST_URL = "/admin/user/whitelist.htm";
    public static final String BLACKLIST_USER_ACTION_LINK ="/admin/user/whitelist/undo.htm";
    public static final String RESULT ="result";
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView adminhome(@RequestParam Map<String,String> mapping, @ModelAttribute("command") EmailForm emailForm, BindingResult bindingResult) {
        return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_VIEW, "command", emailForm);
    }
    
    @RequestMapping(value = "do")
    public ModelAndView whitelist(@RequestParam Map<String, String> params, Model model,@ModelAttribute("command") EmailForm emailForm, BindingResult bindingResult) {
        EmailFormValidator formValidator = new EmailFormValidator();
        formValidator.validate(emailForm, bindingResult);
    
        if (bindingResult.hasErrors()) {
            return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_VIEW, "command", emailForm);
        }
        
        String email = params.get("email").trim();
        Integer userId = null;
        try {
            Users user = userServiceProxy.getUserByEmailId(email);
            userId = user.getUserId();
        } catch (RuntimeException e) {
            model.addAttribute(RESULT, String.format("No user id found for email id : %s", email));
            return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_RESULT);
        }
        boolean success = fraudWhitelistService.whiteListUser(userId);
        
        if (success) {
            model.addAttribute(RESULT, String.format("%s successfully Whitelisted.", email));
        } else {
            model.addAttribute(RESULT, String.format("Failed to whitelist email %s", email));
        }
        return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_RESULT);
    }
    
    @RequestMapping(value = "undo")
    public ModelAndView blacklist(@RequestParam Map<String, String> params, Model model,@ModelAttribute("command") EmailForm emailForm, BindingResult bindingResult) {
        EmailFormValidator formValidator = new EmailFormValidator();
        formValidator.validate(emailForm, bindingResult);
    
        if (bindingResult.hasErrors()) {
            return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_VIEW, "command", emailForm);
        }
        
        String email = params.get("email").trim();
        Integer userId = null;
        try {
            Users user = userServiceProxy.getUserByEmailId(email);
            userId = user.getUserId();
        } catch (RuntimeException e) {
            model.addAttribute(RESULT, String.format("No user id found for email id : %s", email));
            return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_RESULT);
        }
        boolean success = fraudWhitelistService.blacklistUser(userId);
        
        if (success) {
            model.addAttribute(RESULT, String.format("%s successfully Blacklisted.", email));
        } else {
            model.addAttribute(RESULT, String.format("Failed to Blcklist email %s", email));
        }
        return new ModelAndView(ViewConstants.CUSTOMER_WHITELIST_USER_RESULT);
    }
}
