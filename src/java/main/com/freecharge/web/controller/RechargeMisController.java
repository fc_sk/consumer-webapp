package com.freecharge.web.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.recharge.businessdao.MISDisplayRecord;
import com.freecharge.recharge.businessdao.MisRequestData;
import com.freecharge.recharge.services.InAuthenticationFetchService;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/admin/recharge")
public class RechargeMisController extends BaseAdminController {

	@Autowired
	private InAuthenticationFetchService inAuthenticationFetchService;

	@Autowired
	private MISDisplayRecord misDisplayRecord;

	@Autowired
	private OperatorCircleService operatorCircleService;
	
	public static final String RECHARGE_MIS_URL = "/admin/recharge.htm";

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView viewAdminLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request,
                                       HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.RECHARGE_MIS)){
            get403HtmlResponse(response);
            return null;
        }
		String view = "recharge/mis";
		String pageTitle = "FreeCharge payment";
		return new ModelAndView("redirect:/admin/recharge/searchRecords.htm");
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ModelAndView adminLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request) {

		String view = "recharge/mis";
		if (inAuthenticationFetchService.isMisUserValid(adminUsers)) {
			request.getSession().setAttribute(WebConstants.MIS_USER_SESSION_PARAM, adminUsers);
			return new ModelAndView("redirect:/admin/recharge/searchRecords.htm");
		} else {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(view);
			mav.addObject("message", "Invalid UserName/Password");
			return mav;
		}
	}

	@RequestMapping(value = "searchRecords.htm", method = RequestMethod.GET)
	public String showMisScreen(@ModelAttribute("misRequest") MisRequestData misRequestData, HttpServletRequest request, Model model) {
		String view = "mis/display";
		int recordsPerPage = 80;
		int numberOfPages = 1;
		int pageCount = 1;

		HttpSession session = request.getSession();
		List<OperatorMaster> operatorMasters = operatorCircleService.getOperators();
		List<String> aggregatorName = fcProperties.getPropertyValueList("all.aggregators", ",");

		if (request.getParameter("iPageNo") != null) {
			pageCount = Integer.parseInt(request.getParameter("iPageNo"));
		}
		
		try {
		if(session.getAttribute("misRequest")!=null) {
			misRequestData = (MisRequestData)session.getAttribute("misRequest");
		} } 
		catch(Exception e) { 
			
		}
		misRequestData.setPageNo(pageCount);
		session.setAttribute("misRequest", misRequestData);
		misRequestData.setMisStartDate(new Date());
		misRequestData.setMisEndDate(new Date());
		Integer misDataCount = inAuthenticationFetchService.getMisDataCount(misRequestData);
		
		if (misDataCount % recordsPerPage > 0) {
			numberOfPages = misDataCount / recordsPerPage + 1;
		} else
			numberOfPages = misDataCount / recordsPerPage;

		List<MISDisplayRecord> misData = inAuthenticationFetchService.getMisData(misRequestData, pageCount);

		model.addAttribute("misData", misData);
		model.addAttribute("operatorMasters", operatorMasters);
		model.addAttribute("inAggrOprCircleMaps", aggregatorName);
		return view;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "searchRecords.htm", method = RequestMethod.POST)
	public String showMisReport(@ModelAttribute("misRequest") MisRequestData misRequestData, HttpSession session, Model model) {
		int pageCount = 1;
		String view = "mis/display";
		List<OperatorMaster> operatorMasters = operatorCircleService.getOperators();
		List<String> aggregatorName = fcProperties.getPropertyValueList("all.aggregators", ",");
		misRequestData.setPageNo(pageCount);
		if (misRequestData.getMisStartDate() == null && misRequestData.getMisEndDate() == null) {
			misRequestData.setMisStartDate(new Date());
			misRequestData.setMisEndDate(new Date());
		}

		if (misRequestData.getMisStartDate() != null && misRequestData.getMisEndDate() == null) {
			misRequestData.setMisEndDate(new Date());
		}
		session.setAttribute("misRequest", misRequestData);

		List<MISDisplayRecord> misData = inAuthenticationFetchService.getMisData(misRequestData, pageCount);
		model.addAttribute("operatorMasters", operatorMasters); 
		model.addAttribute("misData", misData);
		model.addAttribute("inAggrOprCircleMaps", aggregatorName);
		return view;
	}
	
	@RequestMapping(value = "/excelReport", method = RequestMethod.GET)
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<MISDisplayRecord> excelData = null;
		int pageCount = 500;
		try {
			
			HttpSession session = request.getSession();
			MisRequestData misRequest = (MisRequestData) session.getAttribute("misRequest");
			Integer prevPageSize = misRequest.getPageSize();
			misRequest.setPageSize(null);
			excelData = (List) inAuthenticationFetchService.getMisData(misRequest, pageCount);
			misRequest.setPageSize(prevPageSize);
			session.setAttribute("misRequest", misRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("ExcelRevenueSummary", "excelData", excelData);
	}

}
