package com.freecharge.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.VariableRewardService;

@Controller
@RequestMapping("/reward/*")
public class RewardController {

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private VariableRewardService variableRewardService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "getRewardStatus", method = RequestMethod.GET)
	public @ResponseBody Map<String,Object> getRewardStatus(@RequestParam String orderId,HttpServletRequest request, HttpServletResponse response){
		try {
			return variableRewardService.getRewardStatus(orderId);
		} catch (Exception e) {
			logger.error("error in getRewardStatus for orderId: "+orderId,e);
			return null;
		}
	}
	
	@RequestMapping(value = "processReward", method = RequestMethod.POST)
	@Csrf(exclude=true)
	public @ResponseBody Map<String,Object> processReward(@RequestParam String orderId,HttpServletRequest request, HttpServletResponse response){
		try {
			OrderId orderIdObj = orderService.getByOrderId(orderId);
			if ((FCSessionUtil.getCurrentSession()!=null && orderIdObj!=null && 
				FCSessionUtil.getCurrentSession().getUuid().equals(orderIdObj.getSessionId()))){
				return variableRewardService.processReward(orderId);
			}
		} catch (Exception e) {
			logger.error("error in processReward for orderId: "+orderId,e);
		}
		return null;
    }
	
	
	
	
}
