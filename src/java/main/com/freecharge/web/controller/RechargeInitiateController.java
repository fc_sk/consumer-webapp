package com.freecharge.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.analytics.OrderAnalyticsService;
import com.freecharge.analytics.OrderDetails;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.exception.InvalidOrderIDException;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.RechargeInitiateWebDo;

/**
 * Created by IntelliJ IDEA. User: Toshiba Date: May 11, 2012 Time: 11:14:31 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/app/*")
public class RechargeInitiateController {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RechargeInitiateService rechargeInitiateService;

    @Autowired
    OrderAnalyticsService analyticsService;

    @Autowired
    PaymentService paymentService;

    @Autowired
    InService inService;

    @Autowired
    private OrderIdWriteDAO orderIdDAO;

    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private FCProperties fcproperties;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @NoSessionWrite
    @RequestMapping(value = "recharge", method = RequestMethod.GET)
    public ModelAndView doRecharge(RechargeInitiateWebDo rechargeInitiateWebDo, HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "app/paymentstatus";
        String viewNew = "app/paymentstatus-new";
        
        model.addAttribute("orderId", rechargeInitiateWebDo.getOrderId());
        /*
            LookupId is required for showing deals. Setting it here for the time being.
            While showing recharge status a rest call is made that gets all info. That should
            ideally set the lookupId in js. When that's done remove the line below.
        */
        String dbLookupId = null;
        try {
            dbLookupId = this.orderIdSlaveDAO.getLookupIdForOrderId(rechargeInitiateWebDo.getOrderId());
        } catch (InvalidOrderIDException e) {
            dbLookupId = this.orderIdDAO.getLookupIdForOrderId(rechargeInitiateWebDo.getOrderId());
        }
        final String lookupId = dbLookupId;
        
        model.addAttribute("lookupId", lookupId);

        // YOptima pixel - start
        final RechargeInitiateWebDo riwd = rechargeInitiateService.getRechargeInfo(rechargeInitiateWebDo.getOrderId());
        model.addAttribute("riwd", riwd);
        model.addAttribute("yoptimaEnabled", fcproperties.isYOptimaEnabled());
     // YOptima pixel - end
        model.addAttribute("googleCodeEnabled", fcproperties.isGoogleCodeEnabled());
        model.addAttribute("facebookEnabled", fcproperties.isFacebookCodeEnabled());
        model.addAttribute("rewardsEnabled", this.appConfig.isARDealEnabled());
        model.addAttribute("finalResultSet", true);

        //Free Credits to New User

        model.addAttribute("isZedoEnabled", appConfig.isZedoAdEnabled());

        return new ModelAndView(viewNew);
    }

    @NoSessionWrite
    @RequestMapping(value = "handlerecharge", method = RequestMethod.GET)
    public String handleRecharge(HttpServletRequest request, HttpServletResponse response, Model model) {
        return getFinalPageView(request, model);
    }

    @NoSessionWrite
    @RequestMapping(value = "handlepostpaid", method = RequestMethod.GET)
    public String handlePostpaid(HttpServletRequest request, HttpServletResponse response, Model model) {
        return getFinalPageView(request, model);
    }
    
    private String getFinalPageView(HttpServletRequest request, Model model) {
        String view = "handle/recharge";
        String orderId = request.getParameter("orderId");
        boolean isMobile = false;

        if (MobileUtil.isMobile(request)) {
            isMobile = true;
        }

        try {
            RechargeInitiateWebDo rechargeInitiateWebDo = rechargeInitiateService.getRechargeInfo(orderId);
            PaymentResponseBusinessDO paymentResponseBusinessDO = new PaymentResponseBusinessDO();
            Map<String, Object> resultMap = new HashMap<String, Object>();
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
            dataMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, true);
            resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, dataMap);
            dataMap.put(PaymentConstants.AMOUNT, rechargeInitiateWebDo.getAmount());
            paymentResponseBusinessDO.setResultMap(resultMap);

            // code for google analytics
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            Map<String, Object> map = fs.getSessionData();
            Integer userId = ((Integer) map.get(WebConstants.SESSION_USER_USERID));
            OrderDetails orderDetails = analyticsService.findOrderDetails(rechargeInitiateWebDo.getOrderId(), userId);
            model.addAttribute("orderDetails", orderDetails);
            final RechargeInitiateWebDo rechargeInfo = rechargeInitiateService.getRechargeInfo(rechargeInitiateWebDo.getOrderId());
            model.addAttribute("riwd", rechargeInfo);
            model.addAttribute("finalResultSet", true);
            // Load only in case of Physical coupons
            List<Map<String, Object>> physicalCoupons = voucherService.getCouponDetailsBylookupIdAndCouponType(rechargeInitiateWebDo.getLookupID(), FCConstants.COUPON_TYPE_P);
            if (physicalCoupons != null && !physicalCoupons.isEmpty()) {
                paymentResponseBusinessDO = paymentService.getUserDetailsByUsingOrderId(paymentResponseBusinessDO);
                model.addAttribute("pCouponsSelected", true);
            }

            List<Map<String, Object>> eCoupons = voucherService.getCouponDetailsBylookupIdAndCouponType(rechargeInitiateWebDo.getLookupID(), FCConstants.COUPON_TYPE_E);
            if (eCoupons != null && !eCoupons.isEmpty()) {
                model.addAttribute("eCouponsSelected", true);
            }

            model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE, paymentResponseBusinessDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
            model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, fcproperties.getProperty("application.domain") + fcproperties.getProperty("application.payment.uri.handle.recharge"));
            RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
            model.addAttribute("product", rechargeDetails.getProductName());
            model.addAttribute("operator", rechargeDetails.getOperatorName());
            model.addAttribute("operatorImgUrl", rechargeDetails.getOperatorImageURL());
            model.addAttribute("circle", rechargeDetails.getCircleName());
            model.addAttribute(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
            model.addAttribute(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, true);
            model.addAttribute(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_RECHARGE_MOBILE_NUMBER, dataMap.get(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_RECHARGE_MOBILE_NUMBER));
        } catch (IllegalStateException illegalStateException) {
            return "redirect:../app/home.htm";
        }
        if (isMobile) {
            view = "/m/payment/response/handlezeropaymentresponse";
            return "redirect:../m/payment/response/handlezeropaymentresponse?" + orderId;
        }
        return view;
    }
    
    public boolean isFirstRecharge() {
        if (FreechargeContextDirectory.get().getFreechargeSession().getSessionData().containsKey(FreechargeSession.IS_FIRST_RECHARGE)) {
            return (Boolean) FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(FreechargeSession.IS_FIRST_RECHARGE);
        }

        return false;
    }

    public void removeAffiliateCampaignIdentifier() {
        FreechargeContextDirectory.get().getFreechargeSession().getSessionData().put(FreechargeSession.IS_FIRST_RECHARGE, false);
    }
}
