package com.freecharge.web.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.auth.AWSCredentials;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.service.PgMemcachedService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dao.PgMISRequestDataDAO;
import com.freecharge.payment.dao.PgMisAutenticationFetchDAO;
import com.freecharge.payment.dao.PgMisDisplayRecord;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.entity.PgMisUser;
import com.freecharge.payment.services.PaymentGatewayManagementService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.PaymentGatewayManagementWebDO;

import flexjson.JSONSerializer;

@Controller
@RequestMapping("/admin/payment")
public class PaymentMisController extends BaseAdminController {
    public static final String UPDATE_PG_LINK = "/admin/payment/updatePG.htm";
    public static final String MANAGE_PG_LINK = "/admin/payment/managePG.htm";
    public static final String PAYMENT_ADMIN_URL = "/admin/payment.htm";

    @Autowired
    private PgMisAutenticationFetchDAO      clientAuthentication;

    @Autowired
    private PaymentTransactionService       paymentTransactionService;

    @Autowired
    private AWSCredentials                  s3Credentials;

    @Autowired
    private PaymentGatewayManagementService paymentGatewayManagementService;

    @Autowired
    private PgMemcachedService              pgMemcachedService;

    @Autowired
    private PgReadDAO                       pgReadDAO;

    @Autowired
    private KestrelWrapper                  kestrelClient;

    private final JSONSerializer serializer = new JSONSerializer();

    private static Logger logger = LoggingFactory.getLogger(PaymentMisController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView viewAdminLogin(@ModelAttribute("PgMisUser") PgMisUser pgMisUser, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.PAYMENT_MIS)){
            get403HtmlResponse(response);
            return null;
        }
        String view = "pg/mis";
        String pageTitle = "FreeCharge payment";
        return new ModelAndView("redirect:/admin/payment/searchRecords.htm");

    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView adminLogin(@ModelAttribute("PgMisUser") PgMisUser pgMisUser, HttpServletRequest request) {

        String view = "pg/mis";
        if (paymentTransactionService.isMisUserValid(pgMisUser)) {
            request.getSession().setAttribute(WebConstants.PG_MIS_USER_SESSION_PARAM, pgMisUser);
            return new ModelAndView("redirect:/adminPayment/searchRecords.htm");
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName(view);
            mav.addObject("message", "Invalid ClientID/Password");
            return mav;
        }
    }

    @RequestMapping(value = "searchRecords", method = RequestMethod.GET)
    public String showMisScreen(@ModelAttribute("misRequest") PgMISRequestDataDAO misRequestData,
                                HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.PAYMENT_MIS)){
            return get403HtmlResponse(response);
        }
        String view = "pgmis/display";
        int pageCount = 1;
        int numberOfPages = 1;
        int recordsPerPage = 50;
        HttpSession session = request.getSession();
        if (request.getParameter("iPageNo") != null) {
            pageCount = Integer.parseInt(request.getParameter("iPageNo"));
            misRequestData.setPageNo(pageCount);
        }
        // session.setAttribute("misRequest", misRequestData);
        try {

            if (session.getAttribute("misRequest") != null) {
                misRequestData = (PgMISRequestDataDAO) session.getAttribute("misRequest");
            }
        } catch (Exception e) {

        }
        misRequestData.setPageNo(pageCount);
        misRequestData.setMisStartDate(new Date());
        misRequestData.setMisEndDate(new Date());
        Integer misDataCount = clientAuthentication.getMisDataCount(misRequestData);
        List<PaymentTypeOptions> paymentTypeOptions = paymentTransactionService.findAll();
        if (misDataCount % recordsPerPage > 0) {
            numberOfPages = misDataCount / recordsPerPage + 1;
        } else
            numberOfPages = misDataCount / recordsPerPage;
        session.setAttribute("misRequest", misRequestData); // ////////////
        List<PgMisDisplayRecord> pgmisData = clientAuthentication.getMisData(misRequestData, pageCount);
        model.addAttribute("pgmisData", pgmisData);
        model.addAttribute("paymentTypeOptions", paymentTypeOptions);
        return view;

    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "searchRecords", method = RequestMethod.POST)
    public String showMisReport(@ModelAttribute("misRequest") PgMISRequestDataDAO misRequestData, HttpSession session,
                                Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.PAYMENT_MIS)){
            return get403HtmlResponse(response);
        }
        int pageCount = 1;
        String view = "pgmis/display";
        misRequestData.setPageNo(pageCount);
        if (misRequestData.getMisStartDate() == null && misRequestData.getMisEndDate() == null) {
            misRequestData.setMisStartDate(new Date());
            misRequestData.setMisEndDate(new Date());
        }

        if (misRequestData.getMisStartDate() != null && misRequestData.getMisEndDate() == null) {
            misRequestData.setMisEndDate(new Date());
        }
        session.setAttribute("misRequest", misRequestData);
        List<PaymentTypeOptions> paymentTypeOptions = paymentTransactionService.findAll();
        List<PgMisDisplayRecord> pgmisData = clientAuthentication.getMisData(misRequestData, pageCount);
        model.addAttribute("pgmisData", pgmisData);
        model.addAttribute("paymentTypeOptions", paymentTypeOptions);
        return view;
    }

    @RequestMapping(value = "/excelReport", method = RequestMethod.GET)
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<PgMisDisplayRecord> excelData = null;
        int pageCount = 500;
        try {
            HttpSession session = request.getSession();
            PgMISRequestDataDAO misRequest = (PgMISRequestDataDAO) session.getAttribute("misRequest");
            Integer prevPageSize = misRequest.getPageSize();
            misRequest.setPageSize(null);
            excelData = (List) clientAuthentication.getMisData(misRequest, pageCount);
            misRequest.setPageSize(prevPageSize);
            session.setAttribute("misRequest", misRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("pgExcelRevenueSummary", "excelData", excelData);

    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "managePG")
    public String managePaymentGateway(@ModelAttribute("misRequest") PaymentGatewayManagementWebDO managementWebDO,
                                       HttpSession session, Model model, HttpServletRequest request,
                                       HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.PG_MANAGE)){
            return get403HtmlResponse(response);
        }
        String view = "pgmis/managepg";

        session.setAttribute("misRequest", managementWebDO);
        Map<Integer, String> pgs = paymentGatewayManagementService.getPaymentGateways();
        Map<Integer, String> bks = paymentGatewayManagementService.getBanksGrpMasterForPgManage();
        List<Map<String, Object>> data = null;

        if (managementWebDO != null) {
            if (StringUtils.isBlank(managementWebDO.getBankGrp()) && StringUtils.isBlank(managementWebDO.getProduct()) && StringUtils.isBlank(managementWebDO.getPaymentGateway())) {
                data = paymentGatewayManagementService.getDefaultData();
            } else {
                data = paymentGatewayManagementService.getData(managementWebDO);

            }
        } else {
            data = paymentGatewayManagementService.getDefaultData();
        }

        model.addAttribute("pgBanksGroup", bks);
        model.addAttribute("pgGatewayMaster", pgs);
        model.addAttribute("data", data);
        return view;
    }

    @RequestMapping(value = "/reconuploadform.htm")
    public String showReconPage(HttpServletRequest request, HttpServletResponse response, Model model) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String view = "pgmis/reconform";
        String pgName = request.getParameter("pgname");

        String uploadSuccessUrl = fcProperties.getProperty(FCConstants.HOSTPREFIX) + "/admin/payment/reconuploadsuccess.htm?pgname=" + pgName;
        String reconBucket = fcProperties.getReconBucket();
        String reconUploadDirectory = fcProperties.getReconUploadDirectory();

        String policy = "{\"expiration\": \"2014-01-01T00:00:00Z\", " + "\"conditions\": " + "[ {\"bucket\": \"" + reconBucket + "\"}, " + "[\"starts-with\", \"$key\", \"" + reconUploadDirectory
                + "/\"], " + "{\"acl\": \"private\"}, " + "{\"success_action_redirect\": \"" + uploadSuccessUrl + "\"}, " + "[\"starts-with\", \"$Content-Type\", \"\"], "
                + "[\"content-length-range\", 0, 104857600000] ] " + "}";

        Base64 base64Encoder = new Base64();
        String encodedPolicy = (new String(base64Encoder.encode(policy.getBytes("UTF-8"))).replaceAll("\n", "").replaceAll("\r", ""));

        Mac hmac = Mac.getInstance("HmacSHA1");
        hmac.init(new SecretKeySpec(s3Credentials.getAWSSecretKey().getBytes("UTF-8"), "HmacSHA1"));
        String signature = new String((new Base64()).encode(hmac.doFinal(encodedPolicy.getBytes("UTF-8")))).replaceAll("\n", "");

        String s3PostURL = "https://" + reconBucket + ".s3.amazonaws.com/";

        model.addAttribute("uploadPolicy", encodedPolicy);
        model.addAttribute("accessKey", s3Credentials.getAWSAccessKeyId());
        model.addAttribute("formSignature", signature);
        model.addAttribute("s3PostURL", s3PostURL);
        model.addAttribute("uploadDirectory", reconUploadDirectory);
        model.addAttribute("uploadSuccessUrl", uploadSuccessUrl);
        return view;
    }

    @RequestMapping(value = "/recondownloadform.htm")
    public String showReconReportDownloadForm(HttpServletRequest request, HttpServletResponse response, Model model) {
        model.addAttribute("downloadHandler", fcProperties.getAppRootUrl() + "/admin/payment/getreconfile.htm");

        return "pgmis/reconreportdownloadform";
    }

    @RequestMapping(value = "/getreconfile.htm")
    public ModelAndView handleReconFileDownload(HttpServletRequest request, HttpServletResponse response) {
        String s3Key = request.getParameter("keyName");

        return new ModelAndView("reconFileExportView", "s3Key", s3Key);
    }

    @RequestMapping(value = "/reconuploadsuccess.htm")
    public String handlePGReconFileUpload(HttpServletRequest request, HttpServletResponse response, Model model) {
        String view = "pgmis/reconuploadsuccess";
        String pgName = request.getParameter("pgname");
        String reportFileName = request.getParameter("key");

        logger.info("PG file upload successful; going ahead with enqueue for file name:[ " + reportFileName + "]");

        Map<String, String> reconRequest = new HashMap<String, String>();
        reconRequest.put("reconType", "PG");
        reconRequest.put("reportFileName", reportFileName);
        if (pgName != null && "billdesk".equalsIgnoreCase(pgName)) {
            reconRequest.put("pgName", "BillDesk");
        }
        if (pgName != null && "payu".equalsIgnoreCase(pgName)) {
            reconRequest.put("pgName", "payU");
        }
        if (pgName != null && "icici".equalsIgnoreCase(pgName)) {
            reconRequest.put("pgName", "icici");
        }
        kestrelClient.enqueueReconRequest(reconRequest);

        return view;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "updatePG")
    public String updatePaymentGateway(HttpServletRequest request, HttpSession session, Model model) throws Exception {
        String rid = request.getParameter("rid");
        String gid = request.getParameter("gid");
        String product = request.getParameter("prd");
        String bank = request.getParameter("bank");
        String oldValue = request.getParameter("oldv");
        String newValue = request.getParameter("newv");
        String ipaddress = request.getRemoteAddr();
        String currentAdminUser = getCurrentAdminUser(request);
        if (StringUtils.isBlank(rid) || StringUtils.isBlank("gid")) {
            logger.error("Update on PG is submitted but paramters not complete");
            throw new Exception("unhandled value");
        }

        Map<Integer, String> pgs = paymentGatewayManagementService.getPaymentGateways();
        paymentGatewayManagementService.updatePG(Integer.parseInt(rid), Integer.parseInt(gid));
        pgMemcachedService.setPgRelation(pgReadDAO.getPgRelation());
        paymentGatewayManagementService.createAndSendAlertEmail(product, bank, oldValue, newValue, ipaddress,
                currentAdminUser);

        // Doing this with error level so that audit of this information is
        // always there in logs
        logger.info("Update on PG is submitted. Relation Id : " + rid + " Gateway Changed to : " + gid + " ("
                + pgs.get(Integer.parseInt(gid)) + ")");

        model.addAttribute("gateway", pgs.get(Integer.parseInt(gid)));
        return "jsonView";
    }

}
