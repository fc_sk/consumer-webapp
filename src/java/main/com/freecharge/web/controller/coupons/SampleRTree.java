package com.freecharge.web.controller.coupons;

import gnu.trove.procedure.TIntProcedure;
import net.sf.jsi.Point;
import net.sf.jsi.Rectangle;
import net.sf.jsi.SpatialIndex;
import net.sf.jsi.rtree.RTree;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/21/15
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SampleRTree {
    private SpatialIndex rTree;
    private Map<Integer, Rectangle> rectMap;

    public SampleRTree() {
        rTree = new RTree();
        rTree.init(null);
        rectMap = new HashMap<>();
    }

    public void add(float latitude, float longitude, int id){
        Rectangle r = new Rectangle(latitude, longitude, latitude, longitude);
        this.rTree.add(r, id);
        this.rectMap.put(id, r);
    }

    public Set<Float[]> getNearestN(float latitude, float longitude, int n, int dis){
        final Set<Float[]> result = new HashSet<>();
        this.rTree.nearestN(new Point(latitude, longitude), new TIntProcedure() {
            @Override
            public boolean execute(int i) {
                result.add(new Float[]{(float)i, rectMap.get(i).minX, rectMap.get(i).minY});
                return false;
            }
        }, n, (float)(dis/110.54));
        return result;
    }

    public void clear(){
        rTree = new RTree();
        rTree.init(null);
        rectMap = new HashMap<>();
    }
}
