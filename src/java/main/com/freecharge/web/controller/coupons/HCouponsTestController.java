package com.freecharge.web.controller.coupons;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.csrf.annotations.Csrf;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/21/15
 * Time: 2:06 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/protected/hcoupons/test/*")
public class HCouponsTestController {

    @Autowired
    private SampleRTree sampleRTree;

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public @ResponseBody String addLatLong(@RequestParam String latLongs, HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(latLongs);
        int i=1;
        for (Object obj: json.keySet()){

            JSONObject jb = (JSONObject) json.get(obj);

            sampleRTree.add(((Double)jb.get("Latitude")).floatValue(), ((Double)jb.get("Longitude")).floatValue(), i);
            i++;
        }
        return "OK";
    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public @ResponseBody
    Set<Float[]> getNearest(@RequestParam(defaultValue = "5") Integer n, @RequestParam Float lat,
                            @RequestParam Float lon, @RequestParam(defaultValue = "5") Integer dis){
        return this.sampleRTree.getNearestN(lat, lon, n, dis);
    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "clear", method = RequestMethod.POST)
    public @ResponseBody String clear(){
        this.sampleRTree.clear();
        return "OK";
    }
}
