package com.freecharge.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.autorefund.controller.ManualRefundController;
import com.freecharge.common.util.FCUtil;
import com.freecharge.wallet.CreditsEncashDetailsService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.service.CreditsEncashDetails;
import com.freecharge.web.webdo.CreditsEncashDetailsWebDO;

@Controller
@RequestMapping("/app/onecheck/credits/encash/*")
public class OnecheckCreditEncashBankController {
	private static Logger logger = LoggingFactory.getLogger(OnecheckCreditEncashBankController.class);
	@Autowired
    private CreditsEncashDetailsService creditsEncashDetailsService;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;

	@RequestMapping(value = "data", method = RequestMethod.GET)
    public final String getBankDataView(@RequestParam final Map<String, String> mapping, Object command, final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        String view = "admin/credit/encash/bank/data/viewpage";
        String emailId = mapping.get("emailid");
        CreditsEncashDetailsWebDO creditsEncashDetailsWebDO = new CreditsEncashDetailsWebDO();
        if (FCUtil.isEmpty(emailId)) {
        	model.addAttribute("message", "Login email id needed..");
        	model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        	model.addAttribute("error", "yes");
        	return view;
        }

        logger.info("Request received for edit bank details page for emailId: " + emailId);
        if (!FCUtil.isEmailIdFormat(emailId.trim())) {
        	model.addAttribute("message", "Invalid email Id..");
        	model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        	model.addAttribute("error", "yes");
        	return view;
        }

        Users users = userServiceProxy.getLoggedInUser();
        if (users == null || users.getUserId() == null) {
        	model.addAttribute("message", "Please login to freecharge account.");
        	model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        	model.addAttribute("error", "yes");
        	return view;
        }
        String loginEmail = users.getEmail();
        if (FCUtil.isEmpty(loginEmail)) {
        	model.addAttribute("message", "Please login freecharge account.");
        	model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        	model.addAttribute("error", "yes");
        	return view;
        }
        if (!loginEmail.equals(emailId.trim())) {
        	model.addAttribute("message", "Please login to freecharge account.");
        	model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        	model.addAttribute("error", "yes");
        	return view;
        } 
        
        /*Used logged in email to fetch bank details*/
        List<Map<String, Object>> mapList = creditsEncashDetailsService.getCreditEncashDetails(loginEmail);
        Map<String, Object> map = new HashMap<>();
        
        if (!FCUtil.isEmpty(mapList)) {
        	map = mapList.get(0);
        	creditsEncashDetailsWebDO.setEmailId((String)map.get("email_id"));
            creditsEncashDetailsWebDO.setBankAccountNo((String)map.get("bank_account_no"));
            creditsEncashDetailsWebDO.setBeneficiaryName((String)map.get("beneficiary_name"));
            creditsEncashDetailsWebDO.setIfscCode((String)map.get("ifsc_code"));
        } else {
        	model.addAttribute("message", "Dont have any bank details. For queries please write to us at care@freecharge.in.");
        	model.addAttribute("error", "yes");
        }
        model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
        return view;
    }

	@RequestMapping(value = "data/save", method = RequestMethod.POST)
    public final String saveBankData(final Model model, final HttpServletRequest request,
    		@ModelAttribute("creditsEncashDetailsWebDOForm") CreditsEncashDetailsWebDO creditsEncashDetailsWebDO,
            final HttpServletResponse response) throws IOException {
			String view = "admin/credit/encash/bank/data/viewpage";
			Users users = userServiceProxy.getLoggedInUser();
			if (users == null) {
				model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
		        model.addAttribute("message", "Please login to freecharge account........");
		        return view;
			}
			Integer userId = users.getUserId();
			logger.info("Posting data to update bank details for emailId: " + users.getEmail());
			
			if(userId == null) {
				model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
		        model.addAttribute("message", "Some technical error occurred while updating your bank account details. Please write to us at care@freecharge.in.");
				return view;
			}

			logger.info("Updating bank details for :" + String.valueOf(userId));
			if(creditsEncashDetailsService.isCreditAlreadyEncashed(users.getEmail())) {
				model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
		        model.addAttribute("message", "Already money refunded. For queries please write to us at care@freecharge.in.");
				return view;
			}

			boolean status = false;
			CreditsEncashDetails creditsEncashDetails = new CreditsEncashDetails();
			creditsEncashDetails.setUserId(userId);
			creditsEncashDetails.setEmailId(creditsEncashDetailsWebDO.getEmailId());
			creditsEncashDetails.setBeneficiaryName(creditsEncashDetailsWebDO.getBeneficiaryName());
			creditsEncashDetails.setBankAccountNo(creditsEncashDetailsWebDO.getBankAccountNo());
			creditsEncashDetails.setIfscCode(creditsEncashDetailsWebDO.getIfscCode());
			
			status = creditsEncashDetailsService.updateBankDetails(creditsEncashDetails);

			if (status) {	
		        model.addAttribute("message", "Account Details Updated Successfully........");
			} else {
		        model.addAttribute("message", "Some technical error occurred while updating your bank account details. Please write to us at care@freecharge.in.");
			}
			model.addAttribute("creditsEncashDetailsWebDOForm", creditsEncashDetailsWebDO);
	        return view;
	}
}
