package com.freecharge.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.MockHelper;
import com.freecharge.csrf.annotations.Csrf;

@Controller
@RequestMapping("/dummypg/*")
public class DummyPGController {
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private HomeBusinessDao homeBusinessDao;
    
    private boolean isSane() {
    	if (fcProperties.shouldMockPG()) {
    		return true;
    	} else {
    		logger.warn("[MOCK] MOCK PG is off, but a handler was called!");
    		return false; 
    	}
    }
    
    @RequestMapping(value = "billdesk/*")
    @Csrf(exclude = true)
    public ModelAndView handleBilldeskPay(HttpServletRequest request, HttpServletResponse response) {
    	logger.info("[MOCK] Inside dummy Billdesk PG");
    	
    	if (!isSane())
    		return null;
    			
    	// We get the following as msg parameter in the request:
    	// FREECHARGE|FCVW1301100000025|NA|50.0|HCC|NA|NA|INR|DIRECT|R|freecharge|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://localhost:8080/payment/handlbdkeresposne.htm|1129637547
    	String inMsg = request.getParameter("msg");
    	logger.debug("[MOCK] In incoming request, msg=" + inMsg);
    	
    	// Parse this inMsg
    	String[] params = StringUtils.splitPreserveAllTokens(inMsg, "|");
    	String merchantId = params[0];
    	String customerId = params[1];
    	String amount = params[3];
    	String returnURI = params[21];
    	
    	// We need to return the following:
    	// msg = "MERCHANTID|ARP10234|MSBI0412001668|NA|00000094.00|SBI|22270726|NA|INR|NA|NA|NA|NA|12-12-2004 16:08:56|0300|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|3734835005";

    	// Cook up some dummy params
    	String txnRefNumber = "DUMMY_BILLDESK_1234";
    	
    	// authStatus  - reason
    	// 0300        - Success
    	// 0399        - Invalid authentication at bank
    	// NA          - Invalid input in request message
    	// 0002        - Billdesk is waiting for response from bank
    	// 0001        - Error at Billdesk
    	String authStatus = "0300";

    	// If PG_BDK_RC is set in user's address, change authStatus
   		Map<String, Object> umap = homeBusinessDao.getUserDetailsFromOrderId(customerId);
   		String responseCode = MockHelper.getResponseCodeFromAddressFields(umap, "PG_BDK_RC=");
   		if (responseCode != null) {
   			authStatus = responseCode;
   		}
   		logger.info("[MOCK] returning responseCode=" + authStatus);
    	
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    	String dateStr = sdf.format(new Date());
    	String errStatus = "NA";
    	String errDesc = "NA";
    	String checkSum = "123"; // TBD: Calculate this	
    	
    	String outMsg = merchantId + "|" +
    			customerId + "|" +
    			txnRefNumber + "|" +
    			"NA" + "|" + // Bank Ref number
    			amount + "|" +
    			"SBI" + "|" + // BankId
    			"22270726" + "|" + // Bank Merchant Id (picked up from example)
    			"NA|INR|NA|NA|NA|NA" + "|" +
    			dateStr + "|" +
    			authStatus + "|" + 
    			"NA|NA|NA|NA|NA|NA|NA|NA" + "|" +
    			errStatus + "|" +
    			errDesc + "|" +
    			checkSum;
    	
    	// We need to respond with a POST hence this jugaad
    	// See http://stackoverflow.com/questions/46582/response-redirect-with-post-instead-of-get    	
    	StringBuilder sb = new StringBuilder();
    	sb.append("<html>");
    	sb.append("<body onload='document.forms[\"form\"].submit()'>");
    	sb.append("<form name='form' action='" + returnURI + "' method='post'>");
    	sb.append("<input type='hidden' name='msg' value='" + outMsg + "'>");
    	sb.append("<input type='hidden' name='fc_mock' value='true'>");
    	sb.append("</form>");
    	sb.append("</body>");
    	sb.append("</html>");

    	try {
    		response.setStatus(HttpServletResponse.SC_OK);
    		response.getWriter().write(sb.toString());
    		response.getWriter().flush();
    	} catch (Exception e) {
    		logger.error("For orderId=" + customerId + ", exception while writing to HTTP response: " + e);
    	}
    	
    	// returning null because response already written - see http://static.springsource.org/spring/docs/2.5.6/reference/mvc.html
    	return null;
    } 

    @RequestMapping(value = "ccavenue/*")
    @Csrf(exclude = true)
    public ModelAndView handleCCAvenuePay(HttpServletRequest request, HttpServletResponse response) {
    	logger.info("[MOCK] Inside dummy CCAvenue PG");

    	if (!isSane())
    		return null;
    	
    	// We get the following parameters in the POST request:
    	// [delivery_cust_address, Checksum, billing_cust_address, Amount, billing_cust_country, billing_cust_notes,
    	//  cardOption, delivery_cust_tel, billing_cust_state, Merchant_Id, billing_cust_email, delivery_cust_name,
    	//  billing_cust_city, billing_zip_code, billing_cust_tel, Redirect_Url, billing_cust_name, Order_Id, NonMotoCardType]
    	// Map<String, String[]> inParams = request.getParameterMap();
    	
    	String returnURI = (String) request.getParameter("Redirect_Url");
    	String orderId = (String) request.getParameter("Order_Id");
    	// AuthDesc is the status of the transaction
    	// Y -> Success
    	// N -> Failure
    	String AuthDesc = "Y";
    	// If PG_CCA_RC is set in user's address, change AuthDesc
   		Map<String, Object> umap = homeBusinessDao.getUserDetailsFromOrderId(orderId);
   		String responseCode = MockHelper.getResponseCodeFromAddressFields(umap, "PG_CCA_RC=");
   		if (responseCode != null) {
   			AuthDesc = responseCode;
   		}
   		logger.info("[MOCK] returning responseCode=" + AuthDesc);
    	    	
    	// Now lets make a Map with parameters which we want to send in the response
    	HashMap<String, String> outParams = new HashMap<String, String>();
    	outParams.put("Order_Id", (String) request.getParameter("Order_Id"));
    	outParams.put("Amount", (String) request.getParameter("Amount"));
    	outParams.put("Merchant_Id", (String) request.getParameter("Merchant_Id"));
    	outParams.put("billing_cust_name", (String) request.getParameter("billing_cust_name"));
    	outParams.put("billing_cust_address", (String) request.getParameter("billing_cust_address"));
    	outParams.put("billing_cust_state", (String) request.getParameter("billing_cust_state"));
    	outParams.put("billing_zip_code", (String) request.getParameter("billing_zip_code"));
    	outParams.put("billing_cust_city", (String) request.getParameter("billing_cust_city"));
    	outParams.put("billing_cust_country", (String) request.getParameter("billing_cust_country"));
    	outParams.put("billing_cust_tel", (String) request.getParameter("billing_cust_tel"));
    	outParams.put("billing_cust_email", (String) request.getParameter("billing_cust_email"));
    	outParams.put("Notes", (String) request.getParameter("billing_cust_notes"));
    	outParams.put("delivery_cust_name", (String) request.getParameter("delivery_cust_name"));
    	outParams.put("delivery_cust_address", (String) request.getParameter("delivery_cust_address"));
    	outParams.put("delivery_cust_state", (String) request.getParameter("billing_cust_state"));
    	outParams.put("delivery_cust_city", (String) request.getParameter("billing_cust_city"));
    	outParams.put("delivery_cust_country", (String) request.getParameter("billing_cust_country"));
    	outParams.put("delivery_zip_code", (String) request.getParameter("billing_zip_code"));
    	outParams.put("delivery_cust_tel", (String) request.getParameter("billing_cust_tel"));
    	outParams.put("AuthDesc", AuthDesc); // Get from user's address
    	outParams.put("Checksum", "1234");
    	outParams.put("Merchant_Param", "ABC"); // What's this?
    	outParams.put("nb_bid", "NB_BID"); // ???
    	outParams.put("nb_order_no", "NB_ORDER_NO"); // ???
    	outParams.put("card_category", "CARD_CATEGORY"); // ???
    	outParams.put("bank_name", "BANK_NAME"); // ???

    	StringBuilder sb = new StringBuilder();
    	sb.append("<html>");
    	sb.append("<body onload='document.forms[\"form\"].submit()'>");
    	sb.append("<form name='form' action='" + returnURI + "' method='post'>");
    	for (Map.Entry e : outParams.entrySet()) {
    		sb.append("<input type='hidden' name='" + (String) e.getKey() + "' value='" + (String) e.getValue() + "'>");
    	}
    	sb.append("<input type='hidden' name='fc_mock' value='true'>");
    	sb.append("</form>");
    	sb.append("</body>");
    	sb.append("</html>");

    	try {
    		response.setStatus(HttpServletResponse.SC_OK);
    		response.getWriter().write(sb.toString());
    		response.getWriter().flush();
    	} catch (Exception e) {
    		logger.error("For orderId=" + orderId + ", exception while writing to HTTP response: " + e);
    	}
    	
    	return null;
    }

    @RequestMapping(value = "icicissl/*")
    @Csrf(exclude = true)
    public ModelAndView handleIciciPay(HttpServletRequest request, HttpServletResponse response) {
    	logger.info("[MOCK] Inside dummy ICICI SSL PG");

    	if (!isSane())
    		return null;
    	
    	// The request does not seem to have any data
    	// ICICI has provided some integration jar which we use to communicate with ICICI
    	// Need to figure this out - Arun is working on it
    	
    	return null;
    }
    
    @RequestMapping(value = "payu/*")
    @Csrf(exclude = true)
    public ModelAndView handlePayUPay(HttpServletRequest request, HttpServletResponse response) {
    	logger.info("[MOCK] Inside dummy PayU PG");
    	
    	if (!isSane())
    		return null;

    	// The request contains the following parameters:
    	// [phone, txnid, CCEXPMON, hash, CCEXPYR, firstname, furl, api_version, productinfo, CCVV, drop_category,
    	// amount, CCNAME, pg, user_credentials, email, bankcode, CCNUM, surl, key]
    	
    	String returnURI = (String) request.getParameter("surl");
    	String orderId = (String) request.getParameter("txnid");
    	// status of the transaction = SUCCESS/PENDING/FAILURE
    	String status = "success";
    	// If PG_PAYU_RC is set in user's address, change status
   		Map<String, Object> umap = homeBusinessDao.getUserDetailsFromOrderId(orderId);
   		String responseCode = MockHelper.getResponseCodeFromAddressFields(umap, "PG_PAYU_RC=");
   		if (responseCode != null) {
   			status = responseCode;
   			if (responseCode != "success") {
   				returnURI = (String) request.getParameter("furl");
   			}   				
   		}
   		logger.info("[MOCK] returning responseCode=" + status);
   		String error = "";
   		String errorCode = MockHelper.getResponseCodeFromAddressFields(umap, "PG_PAYU_EC=");
   		if (errorCode != null) {
   			error = errorCode;
   		}
   		logger.info("[MOCK] returning errorCode=" + error);
    	    	
    	// Now lets make a Map with parameters which we want to send in the response
    	HashMap<String, String> outParams = new HashMap<String, String>();
    	outParams.put("mihpayid", "some_payu_unique_id");
    	outParams.put("mode", (String) request.getParameter("pg"));
    	outParams.put("status", status);
    	outParams.put("key", (String) request.getParameter("key"));
    	outParams.put("txnid",  (String) request.getParameter("txnid"));
    	outParams.put("amount", (String) request.getParameter("amount"));
    	outParams.put("productinfo", (String) request.getParameter("productinfo"));
    	outParams.put("firstname", (String) request.getParameter("firstname"));
    	outParams.put("hash", "1234"); // TBD: Do proper Hash calculation
    	outParams.put("Error", error);

    	StringBuilder sb = new StringBuilder();
    	sb.append("<html>");
    	sb.append("<body onload='document.forms[\"form\"].submit()'>");
    	sb.append("<form name='form' action='" + returnURI + "' method='post'>");
    	for (Map.Entry e : outParams.entrySet()) {
    		sb.append("<input type='hidden' name='" + (String) e.getKey() + "' value='" + (String) e.getValue() + "'>");
    	}
    	sb.append("<input type='hidden' name='fc_mock' value='true'>");
    	sb.append("</form>");
    	sb.append("</body>");
    	sb.append("</html>");

    	try {
    		response.setStatus(HttpServletResponse.SC_OK);
            response.addHeader("Content-Type", "text/html");
    		response.getWriter().write(sb.toString());
    		response.getWriter().flush();
    	} catch (Exception e) {
    		logger.error("For orderId=" + orderId + ", exception while writing to HTTP response: " + e);
    	}
    	    	
    	return null;
    }
}
