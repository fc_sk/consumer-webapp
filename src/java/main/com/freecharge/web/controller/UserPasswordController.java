package com.freecharge.web.controller;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/app/*")
public class UserPasswordController {

	@Autowired
	private Validator validator;

	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
    private MetricsClient              metricsClient;
	
	private Logger logger = LoggingFactory.getLogger(getClass());

/*	@NoLogin
	@RequestMapping(value = "forgotpassword", method = RequestMethod.GET)
	public ModelAndView passwordRecovery(HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcForgotPassword");
		String view = "home/forgotpassword";
		String pageTitle = "FreeCharge forgotpassword";
		model.addAttribute("title", pageTitle);
		model.addAttribute("loginWebDO", new LoginWebDO());
		ForgotPasswordWebDo forgotPasswordWebDo = new ForgotPasswordWebDo();
		model.addAttribute("forgotPasswordWebDo", forgotPasswordWebDo);
		return new ModelAndView(view);
	}*/
    @NoLogin
	@RequestMapping(value = "forgotpassword", method = RequestMethod.POST)
	public String forgotPassword(ForgotPasswordWebDo forgotPasswordWebDo, HttpServletRequest request, HttpServletResponse response, Model model) {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "userPasswordController3");
		String view = "home/forgotpassword";
		if (forgotPasswordWebDo==null || FCUtil.isEmpty(forgotPasswordWebDo.getEmail())) {
		    model.addAttribute("status", "fail");
		    model.addAttribute("error", ErrorCode.EMAIL_NOT_PRESENT.getErrorNumberString());
		    return "jsonView";
		}

		String delegateName = WebConstants.DELEGATE_BEAN_FORGOTPASSWORD;
		model.addAttribute("loginWebDO", new LoginWebDO());
		try {
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, forgotPasswordWebDo);
			model.addAttribute("forgotPasswordResult", ((ForgotPasswordWebDo) webContext.getBaseWebDO()).getMessage());
			if(((ForgotPasswordWebDo) webContext.getBaseWebDO())!=null && ((ForgotPasswordWebDo) webContext.getBaseWebDO()).getUserValid()){
				model.addAttribute("status", "success");
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
			}else{
				model.addAttribute("status", "fail");
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "fail");
			}
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			model.addAttribute("forgotPasswordResult",fcProperties.getProperty("error.message"));
			model.addAttribute("status", "fail");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
		} catch (NullPointerException exp) {
			logger.error("NullPointerException raised, stack trace is: ", exp);
			model.addAttribute("forgotPasswordResult",fcProperties.getProperty("error.message"));
			model.addAttribute("status", "fail");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			//return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Any Exception that could come", exp);
			model.addAttribute("forgotPasswordResult",fcProperties.getProperty("error.message"));
			model.addAttribute("status", "fail");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			//return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
			return "jsonView";
		}
		model.addAttribute("emailId",forgotPasswordWebDo.getEmail());
		//return new ModelAndView(view);
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return "jsonView";
	}
/*    @NoLogin
	@RequestMapping(value = "ajaxforgotpassword", method = RequestMethod.POST)
	public String ajaxforgotpassword(@ModelAttribute(value = "forgotPasswordWebDo")ForgotPasswordWebDo forgotPasswordWebDo,BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "userPasswordController4");
		model.addAttribute("forgotPasswordWebDo",null);
				
		try {
			
			response.setContentType("application/json");
			
			ValidationUtils.rejectIfEmpty(bindingResult, "email", "User can not be empty.");
			
			if (bindingResult.hasErrors()) {	
					model.addAttribute("STATUS", "ERROR_EMAIL");
					model.addAttribute("RESULT", "Email is missing");
			} else {
			
				String delegateName = WebConstants.DELEGATE_BEAN_FORGOTPASSWORD;
				WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, forgotPasswordWebDo);
			
				model.addAttribute("STATUS", ((ForgotPasswordWebDo) webContext.getBaseWebDO()).getUserValid());
			}
			
		} catch (FCRuntimeException fcexp) {
			logger.error("FCRuntimeException in ajaxforgotpassword", fcexp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			return "jsonView";
		} catch (Exception exp) {
			logger.error("Exception in ajaxforgotpassword", exp);
			model.addAttribute("STATUS", "EXCEPTION");
			model.addAttribute("RESULT", null);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			return "jsonView";
		}
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return "jsonView";
	}*/


/*    @NoLogin
	@RequestMapping(value = "changepassword", method = RequestMethod.GET)
	public ModelAndView changePasswordPage(HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcChangePasswordPage");
		logger.info(" Change Password request  ");
		String view = "home/changepassword";
		String pageTitle = "FreeCharge changepassword";
		model.addAttribute("loginWebDO", new LoginWebDO());
		model.addAttribute("changepassword", new ChangePasswordWebDO());
		return new ModelAndView(view);
	}*/
/*    @NoLogin
	@RequestMapping(value = "changepassword", method = RequestMethod.POST)
	public ModelAndView changePassword(ChangePasswordWebDO changePasswordWebDO, HttpServletRequest request, HttpServletResponse response, Model model) {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "userPasswordController1");
		String view = "home/changepassword";
		String pageTitle = "FreeCharge changepassword";
		try {
			logger.info("Change Password  form submit by user");
			String delegateName = WebConstants.DELEGATE_BEAN_CHANGE_PASSWORD;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, changePasswordWebDO);
			model.addAttribute("loginWebDO", new LoginWebDO());
			model.addAttribute("changePasswordSuccessMsg", ((ChangePasswordWebDO) (webContext.getBaseWebDO())).getStatus());

			model.addAttribute("title", pageTitle);
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
		} catch (Exception exp) {
			logger.error("Any Exception that could come", exp);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
		}
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return new ModelAndView(view);
	}*/
	@NoLogin
	@RequestMapping(value = "passwordrecovery", method = RequestMethod.GET)
	public ModelAndView passwordreccovery(HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcPasswordRecovery");
		String view = "";
		model.addAttribute("userDO", new UserDO());
        model.addAttribute("loginWebDO", new LoginWebDO());
		try {
			FreechargeSession fs =FreechargeContextDirectory.get().getFreechargeSession();
			Boolean isLoggedin =(Boolean)fs.getSessionData().get(WebConstants.SESSION_USER_IS_LOGIN);
			if(isLoggedin != null && isLoggedin){
                FreechargeSession.setUserRelatedSessionData(null, null, false, null, SessionConstants.LoginSource.EMAIL,
                        request.getParameter(FCConstants.IMEI), request.getParameter(FCConstants.DEVICE_UNIQUE_ID));
			}
			String delegateName = WebConstants.DELEGATE_BEAN_PASSWORD_RECOVERY;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new PasswordRecoveryWebDO());
			//model.addAttribute("loginWebDO", new LoginWebDO());
			logger.info("Password Recovery ");
			model.addAttribute("passwordRecovery", ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())));
			String status = ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())).getStatus();
			model.addAttribute("status", status);
			if (status == null || status.equals("fail")) {
				view = "home/error";
			} else {
				view = "home/passwordrecovery";
			}
			String pageTitle = "FreeCharge passwordrecovery";
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			return new ModelAndView("home/error");
		} catch (Exception exp) {
			logger.error("Any Exception that could come", exp);
			return new ModelAndView("home/error");
		}

		return new ModelAndView(view);
	}
	
    @NoLogin
    @RequestMapping(value = "passwordrecovery", method = RequestMethod.POST)
    public String dopasswordreccovery(
            @ModelAttribute(value = "passwordRecoveryWebDO") PasswordRecoveryWebDO passwordRecoveryWebDO,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcDoPasswordRecovery");
        long startTime = System.currentTimeMillis();
        try {
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "userPasswordController2");
            String pageTitle = "FreeCharge passwordrecovery";
            response.setContentType("application/json");
            model.addAttribute("title", pageTitle);
            validator.validate(passwordRecoveryWebDO, bindingResult);
            if (passwordRecoveryWebDO.getNewPassword() != null && passwordRecoveryWebDO.getConfirmPassword() != null
                    && !passwordRecoveryWebDO.getNewPassword().equals(passwordRecoveryWebDO.getConfirmPassword())) {
                bindingResult.rejectValue("newPassword", "New Password and confirm password fields should match");
            }
            if (bindingResult.hasErrors()) {
                StringBuffer error = new StringBuffer("");
                for (ObjectError errorObject : bindingResult.getAllErrors()) {
                    if (errorObject.getDefaultMessage() != null) {
                        error.append(errorObject.getDefaultMessage()).append("<br/>");
                    } else {
                        error.append(errorObject.getCode()).append("<br/>");
                    }
                }
                model.addAttribute("changePasswordSuccessMsg", error.toString());
                model.addAttribute("status", "required");
                return "jsonView";
            }
            String delegateName = WebConstants.DELEGATE_BEAN_RECOVERED_PASSWORD_CHANGE;
            WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model,
                    passwordRecoveryWebDO);
            model.addAttribute("changePasswordSuccessMsg",
                    ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())).getStatus());
            model.addAttribute("status", "success");
            logger.info("New Password is set to the User "
                    + ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())).getEmail());
            
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
            
            return "jsonView";
        } catch (FCRuntimeException fcexp) {
            logger.error("Exception xxxxx .", fcexp);
            model.addAttribute("error", "Technical error occured. Please try again later.");
            model.addAttribute("status", "exception");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
        } catch (Exception exp) {
            logger.info("Unable to change Password to  the User ");
            logger.error("Any Exception that could come", exp);
            model.addAttribute("error", "Technical error occured. Please try again later.");
            model.addAttribute("status", "exception");
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
        }
        long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
        return "jsonView";
    }
	
/*	@NoLogin
	@RequestMapping(value = "doesuserexist", method = RequestMethod.GET)
	public String doesuserexist(HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcDoesUserExist");
		String pageTitle = "FreeCharge passwordrecovery";
		response.setContentType("application/json");
		try {
			String delegateName = WebConstants.DELEGATE_BEAN_DOESUSEREXIST;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new CheckUserExistanceWebDO());
			model.addAttribute("loginWebDO", new LoginWebDO());
			model.addAttribute("userstatus", ((CheckUserExistanceWebDO) webContext.getBaseWebDO()).getStatusMsg());
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
		} catch (Exception exp) {
			logger.error("exception raised while validating user");
			logger.error("Any Exception that could come", exp);

		}
		return "jsonView";
	}*/

}
