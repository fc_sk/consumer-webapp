package com.freecharge.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import com.freecharge.common.framework.logging.LoggingFactory;

@Controller
@RequestMapping("/app/*")
public class ExceptionHandlerController {

	@Autowired
	private Validator validator;

	private Logger logger = LoggingFactory.getLogger(getClass());


	@ExceptionHandler(NoSuchRequestHandlingMethodException.class)
	public ModelAndView handleNoSuchRequestHandlingMethodException(NoSuchRequestHandlingMethodException ex, HttpServletRequest request) {
		String view = "home/error";
		logger.error("Exception found: " + ex);

		return new ModelAndView(view);
	}

	@ExceptionHandler(NullPointerException.class)
	public ModelAndView handleNullPointerException(NullPointerException ex, HttpServletRequest request) {
		String view = "home/error";
		logger.error("Exception found: " + ex);
		return new ModelAndView(view);
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception ex, HttpServletRequest request) {
		String view = "home/error";
		logger.error("Exception found: " + ex);
		return new ModelAndView(view);
	}

}
