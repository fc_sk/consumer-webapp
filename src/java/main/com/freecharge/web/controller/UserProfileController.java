package com.freecharge.web.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.exception.IdentityAuthorizedException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.invoice.TaxInvoiceDetails;
import com.freecharge.invoice.TaxInvoiceService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.util.FCUtil;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.HomeWebDo;
import com.freecharge.web.webdo.InvoiceWebDO;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.MyContactsListWebDo;
import com.freecharge.web.webdo.MyContactsWebDO;
import com.freecharge.web.webdo.MyRechargesWebDO;
import com.freecharge.web.webdo.RechargeInfoVO;
import com.freecharge.web.webdo.UserDO;
import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.snapdeal.fcpt.txnhistoryview.client.ITxnHistoryViewClient;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetInvoiceDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetTxnDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetInvoiceDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.FulfilmentTxnInfo;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.InsuranceInfo;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.MunicipalInfo;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.TxnHistory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;


@Controller
@RequestMapping("/app/*")
public class UserProfileController {

	@Autowired
	private Validator validator;

    @Autowired
    private FreechargeEventPublisher freechargeEventPublisher;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private MetricsClient              metricsClient;
    
    @Autowired
    private TaxInvoiceService  taxInvoiceService;

	private Logger logger = LoggingFactory.getLogger(getClass());

/*	@RequestMapping(value = "myprofile", method = RequestMethod.GET)
	public ModelAndView myProfile(HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcMyProfile");
		logger.info("entered into the myprofile method of userprofilecontroller ....");
		String view = "home/myprofile";
		String homeview = "home/view";
		String pageTitle = "FreeCharge myprofile";
		try {
			boolean islogin = false;
			String email = null;
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
			}
			if (!islogin || email == null) {
				return new ModelAndView("redirect:/");
			}

			String delegateName = WebConstants.DELEGATE_BEAN_MYPROFILE;
			ProfileWebDO profilewebdo = new ProfileWebDO();
			profilewebdo.setEmail(email);

			logger.info("in my profile method");
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profilewebdo);
			profilewebdo = ((ProfileWebDO) (webContext.getBaseWebDO()));
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			model.addAttribute("userprofile", profilewebdo);
			model.addAttribute("stateMaster", profilewebdo.getStatelist());
			model.addAttribute("title", pageTitle);
			logger.info("Leaving From the myprofile method of userprofilecontroller ....");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
		} catch (Exception exp) {
			logger.error("exception raised while getting profile info , exp details are", exp);
			return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
		}

		return new ModelAndView(view);
	}

	@RequestMapping(value = "updateprofile", method = RequestMethod.POST)
	public ModelAndView updateProfile(@ModelAttribute(value = "userprofile") ProfileWebDO profileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcUpdateProfile");
		logger.info("Entered into  the updateProfile method of userprofilecontroller ....");
		String view = "home/profile";
		long startTime = System.currentTimeMillis();
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userProfileController1");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			boolean islogin = false;
			String email = null;
			Integer userid = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null || !email.equals(profileWebDO.getEmail())) {
				model.addAttribute("errormsg", "Please login with your credentials");
				return new ModelAndView("profileError");
			}
			String delegateName = WebConstants.DELEGATE_BEAN_UPDATEPROFILE;
			String oldpswd = profileWebDO.getOldpassword();
			String newpswd = profileWebDO.getNewpassword();
			String confirmpswd = profileWebDO.getConfirmpassword();
			String psderror = "";
			String nameerror = "";
			if (((newpswd != null && !newpswd.isEmpty() && confirmpswd != null && !confirmpswd.isEmpty()) && newpswd.equals(confirmpswd)) && (oldpswd != null && !oldpswd.isEmpty())) {
				profileWebDO.setIspasswordupdate(true);
				if (confirmpswd.length() < 6 || newpswd.length() < 6 || oldpswd.length() < 6) {
					psderror = "password length should be 6 charecters";
					bindingResult.reject("password field error ");

				}

			}
			if (profileWebDO.getName() == null || profileWebDO.getName().isEmpty() || profileWebDO.getName().length() < 4) {
				bindingResult.reject("name field error ");
				nameerror = " Name should be minimum 4 charecters";
			}
			ValidationUtils.rejectIfEmpty(bindingResult, "mobileNo", "mobileNo not empty.");
			*//*
			 * ValidationUtils.rejectIfEmpty(bindingResult, "dobString", "data of birt not empty."); if(profileWebDO.getDobString() != null){ String datestring=profileWebDO.getDobString(); SimpleDateFormat dateformat=new SimpleDateFormat("dd/MM/yyyy"); try{ Date date=dateformat.parse(datestring); if(date.after(Calendar.getInstance().getTime())) bindingResult.reject("date of birth "); }catch (Exception e) { bindingResult.reject("date error "); } }
			 *//*
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			if (bindingResult.hasErrors()) {
				String dtname = WebConstants.DELEGATE_BEAN_MYPROFILE;
				WebContext webContext = DelegateHelper.processRequest(request, response, dtname, model, profileWebDO);
				profileWebDO = ((ProfileWebDO) (webContext.getBaseWebDO()));
				model.addAttribute("userprofile", profileWebDO);
				model.addAttribute("stateMaster", profileWebDO.getStatelist());
				model.addAttribute("message", "please give valid profile data");
				model.addAttribute("updstatus", false);
				return new ModelAndView(view);
			}
			profileWebDO.setUserid(userid);
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			profileWebDO = ((ProfileWebDO) (webContext.getBaseWebDO()));
			map.put(WebConstants.SESSION_USER_FIRSTNAME, profileWebDO.getName());
			model.addAttribute("userprofile", profileWebDO);
			model.addAttribute("stateMaster", profileWebDO.getStatelist());
			model.addAttribute("updstatus", true);
			model.addAttribute("message", "Yay! Your profile has been updated!");
			logger.info("Leaving From the updateProfile method of userprofilecontroller ....");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			model.addAttribute("message", "your profile not updated ");
			model.addAttribute("updstatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		} catch (Exception exp) {
			logger.error(" freecharge exception raised while updating the profile ", exp);
			model.addAttribute("message", "your profile not updated");
			model.addAttribute("updstatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return new ModelAndView(view);
	}

	@RequestMapping(value = "checkoldpswd", method = RequestMethod.POST)
	public String chedkUseOlderPassword(ProfileWebDO profileWebDO, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "upcChedkUserOlderPassword");
		try {
			response.setContentType("application/json");

			String email = request.getParameter("email");
			String password = request.getParameter("password");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			boolean islogin = false;
			String logedinemail = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				logedinemail = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
			}
			if (!islogin || email == null || !logedinemail.equals(email)) {
				model.addAttribute("loginstatus", false);
				model.addAttribute("message", "Please login with your credentials");
				return "jsonView";
			}
			profileWebDO.setEmail(email);
			profileWebDO.setOldpassword(password);
			String delegateName = WebConstants.DELEGATE_BEAN_CHECKOLDPASSWORD;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			profileWebDO = ((ProfileWebDO) (webContext.getBaseWebDO()));
			model.addAttribute("loginstatus", true);
			boolean ispswdcorrect = profileWebDO.isIsoldpswdcorrect();
			model.addAttribute("oldpswdstatus", ispswdcorrect);
		} catch (Exception e) {
			logger.error("Error in UserProfileController while checking the user oldpassword " , e);
			model.addAttribute("oldpswdstatus", false);
		}
		return "jsonView";
	}

	@RequestMapping(value = "updateaddress", method = RequestMethod.POST)
	public ModelAndView updateAddress(@ModelAttribute(value = "userprofile") ProfileWebDO profileWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
	    String view = "home/profile";
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userProfileController2");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			boolean islogin = false;
			String email = null;
			Integer userid = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null || !email.equals(profileWebDO.getEmail())) {
				model.addAttribute("errormsg", "Please login with your credentials");
				return new ModelAndView("profileError");
			}
			validator.validate(profileWebDO, bindingResult);
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			if (bindingResult.hasErrors()) {
				String dtname = WebConstants.DELEGATE_BEAN_MYPROFILE;
				WebContext webContext = DelegateHelper.processRequest(request, response, dtname, model, profileWebDO);
				profileWebDO = ((ProfileWebDO) (webContext.getBaseWebDO()));
				model.addAttribute("userprofile", profileWebDO);
				model.addAttribute("stateMaster", profileWebDO.getStatelist());
				model.addAttribute("message", "please give valid Address data");
				model.addAttribute("updstatus", false);
				return new ModelAndView(view);
			}
			String delegateName = WebConstants.DELEGATE_BEAN_UPDATEADDRESS;
			profileWebDO.setUserid(userid);
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, profileWebDO);
			profileWebDO = ((ProfileWebDO) (webContext.getBaseWebDO()));
			model.addAttribute("userprofile", profileWebDO);
			model.addAttribute("stateMaster", profileWebDO.getStatelist());
			model.addAttribute("message", "your Address updated successfully");
			model.addAttribute("updstatus", true);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (Exception e) {
			model.addAttribute("updstatus", false);
			logger.error(e);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return new ModelAndView(view);
	}*/

	@RequestMapping(value = "myrecharges", method = RequestMethod.GET)
	public ModelAndView myRecharges(HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "home/myrecharges";
		model.addAttribute("homeWebDo", new HomeWebDo());
		model.addAttribute("userDO", new UserDO());

		logger.info("Entered into the myrecharges Method");
		try {
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			Boolean isloggedin = false;
			Integer userid = null;
			if (map != null) {
				String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				request.setAttribute("email", email);
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
					userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
				}
			}
			if (isloggedin) {
				model.addAttribute("title", "Freecharge Reccharge History");
				String delegateName = WebConstants.DELEGATE_MY_RECHARGES;
				MyRechargesWebDO rechargewebdo = new MyRechargesWebDO();
				rechargewebdo.setUserid(userid);
				logger.info("in my profile method");
				WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, rechargewebdo);
				/*
				 * model.addAttribute("loginWebDO", new LoginWebDO()); model.addAttribute("myprofile", ((MyProfileWebDO) (webContext.getBaseWebDO()))); model.addAttribute("stateMaster", ((MyProfileWebDO) (webContext.getBaseWebDO())).getStateMaster());
				 */
				model.addAttribute("rechargeHistoryList", ((MyRechargesWebDO) (webContext.getBaseWebDO())).getRechargeHistoryList());
				/*
				 * model.addAttribute("totalRecords" , ((MyProfileWebDO) (webContext.getBaseWebDO())).getTotalRecords() ); model.addAttribute("currentPage" , ((MyProfileWebDO) (webContext.getBaseWebDO())).getPageNo()); model.addAttribute("recordsPerPage" , FCConstants.RECORDSPERPAGE);
				 */
				logger.info("Leaving from the myrecharges Method");
				return new ModelAndView(view);
			} else {
				String homeView = "home/view";
				return new ModelAndView("redirect:/app/home.htm");

			}
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			return new ModelAndView("error");
		} catch (Exception exp) {
			logger.error("exception raised while getting profile info , exp details are", exp);
			return new ModelAndView(view);
		}

	}
    
	@RequestMapping(value = "invoice", method = RequestMethod.GET)
	public ModelAndView viewInvoice(HttpServletRequest request, HttpServletResponse response, Model model) {
	    
		String view = "home/invoice";
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Map<String, Object> map = fs.getSessionData();
		String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
		String userId = (String)map.get(WebConstants.USER_ID);
		String txnId = request.getParameter("orderId");
		String txnType = request.getParameter("txnType");
		if (email == null) {
			model.addAttribute("expMsg", "No Session exists");
			return new ModelAndView("home/accessdenied");
		}
		String delegateName = WebConstants.DELEGATE_VIEW_INVOICE;

		try {
			
			//*******START**************
			
			if (StringUtils.isNotEmpty(txnType)) {
				// new flow with THV
				logger.info("Generating invoice from THV response for orderId=" + txnId + "  txnType=" + txnType + "userId=" + userId);
				GetTxnDetailsResponse txnDetailsResponse = taxInvoiceService.getTxnDetails(userId, txnId, txnType);
				TxnHistory txnDetails = txnDetailsResponse.getUserTxnDetails();
				DateFormat walletDisplayDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				if ("TRANSLATOR_CASHBACK".equals(txnDetails.getGlobalTxnType())) {
					model.addAttribute("paymentTime", walletDisplayDateFormat.format(txnDetails.getTransactionDate()));
					model.addAttribute("isWalletCash", Boolean.TRUE);
				} else {		
					if (txnDetails.getGlobalTxnType() != null && FCConstants.TxnTypeToProductNameMap.containsKey(txnDetails.getGlobalTxnType()) && FCConstants.TxnTypeToProductNameMap.get(txnDetails.getGlobalTxnType()).isUtilityPaymentProduct()) {
						if ("TRANSLATOR_METRO".equals(txnDetails.getGlobalTxnType())) {
							model.addAttribute("isMetroPayment", Boolean.TRUE);
						}
						model.addAttribute("isUtilityPayment", Boolean.TRUE);
					}
					
					
					//************* Recharge INFO **********************
				    model.addAttribute("isWalletCash", Boolean.FALSE);
	                
	                RechargeInfoVO rechargeInfo = new RechargeInfoVO();
	                rechargeInfo.setOrderId(txnDetails.getGlobalTxnId());
	                rechargeInfo.setRechargedDate(walletDisplayDateFormat.format(txnDetails.getTransactionDate()));
	                rechargeInfo.setRechargeAmount(txnDetails.getFulfilmentInfo().getAmount().floatValue());
	                model.addAttribute("rechargeInfo", rechargeInfo);
				}

				FulfilmentTxnInfo fulfilmentTxnInfo = txnDetails.getFulfilmentInfo();
				
				boolean isGiftCard = FCConstants.GIFT_CARDS.equalsIgnoreCase(fulfilmentTxnInfo.getCategory());
				if(isGiftCard) {
					model.addAttribute("beneficiaryEmail",fulfilmentTxnInfo.getGcInfo().getBeneficiaryEmail());
					logger.info("OrderId = " + txnId + " isGiftCard = " + isGiftCard + "  beneficiaryEmail = " + txnDetails.getFulfilmentInfo().getGcInfo().getBeneficiaryEmail());
				}

				boolean isInsurance = FCConstants.INSURANCE.equalsIgnoreCase(fulfilmentTxnInfo.getCategory());
				if (isInsurance) {
					logger.info("Order is of type insurance");
					InsuranceInfo insuranceInfo = fulfilmentTxnInfo.getInsuranceInfo();
					logger.info("Insurance info is " + fulfilmentTxnInfo);
					model.addAttribute(FCConstants.INSURANCE_BILLER_NAME, insuranceInfo.getBillerName());
					model.addAttribute(FCConstants.INSURANCE_POLICY_NUMBER, insuranceInfo.getCustomerId());
				}
				
				boolean isMunicipal = FCConstants.MUNICIPAL.equalsIgnoreCase(fulfilmentTxnInfo.getCategory());
				if (isMunicipal) {
					logger.info("Order is of type municipal");
					MunicipalInfo municipalInfo = fulfilmentTxnInfo.getMunicipalInfo();
					logger.info("Municipal info is " + fulfilmentTxnInfo);
					model.addAttribute(FCConstants.MUNICIPALITY_NAME,fulfilmentTxnInfo.getServiceProvider());
					model.addAttribute(FCConstants.CONSUMER_ACCOUNT_NUMBER, municipalInfo.getConsumerCode());
					model.addAttribute(FCConstants.SERVICE_TYPE, municipalInfo.getServiceType());
					model.addAttribute(FCConstants.RECEIPT_NUMBER, municipalInfo.getReceiptNumber());
				}

			    model.addAttribute("orderId", txnDetails.getGlobalTxnId());
			    model.addAttribute("totalAmount", txnDetails.getTxnAmount());
			    model.addAttribute("loginWebDO", new LoginWebDO());
			    model.addAttribute("isGiftCard",isGiftCard);
                model.addAttribute("isInsurance", isInsurance);
                model.addAttribute("isMunicipal", isMunicipal);
			    model.addAttribute("paymentTime", walletDisplayDateFormat.format(txnDetails.getTransactionDate()));
			} else {
				//prev flow
				WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, new InvoiceWebDO());
				InvoiceWebDO invoiceWebDO = (InvoiceWebDO) webContext.getBaseWebDO();
				
				if (invoiceWebDO.getPrimaryProductName() != null && ProductName.WalletCash == invoiceWebDO.getPrimaryProductName()) {
				    DateFormat walletDisplayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				    
				    Timestamp paymentTime = invoiceWebDO.getPaymentTime();
				    
	                model.addAttribute("paymentTime", walletDisplayDateFormat.format(paymentTime));
				    model.addAttribute("isWalletCash", Boolean.TRUE);
				    model.addAttribute("orderId", invoiceWebDO.getOrderId());
				    
				} else {
				    if (invoiceWebDO.getPrimaryProductName() != null && invoiceWebDO.getPrimaryProductName().isUtilityPaymentProduct()) {
				        if (invoiceWebDO.getPrimaryProductName() == ProductName.Metro) {
				        	model.addAttribute("isMetroPayment", Boolean.TRUE);
				        }
				    	model.addAttribute("isUtilityPayment", Boolean.TRUE);
				    }
				    model.addAttribute("isWalletCash", Boolean.FALSE);
	                model.addAttribute("rechargeInfo", invoiceWebDO.getRechargeInfoVO());
	                model.addAttribute("cartInfo", invoiceWebDO.getCartAndCartItemsList());
				}

				model.addAttribute("totalAmount", invoiceWebDO.getTotalAmount());
				model.addAttribute("loginWebDO", new LoginWebDO());
				if(cartService.getPrimaryProduct(invoiceWebDO.getOrderId())==ProductName.HCoupons) {
					model.addAttribute("isHcoupon", true);
				} else {
					model.addAttribute("isHcoupon", false);
				}
			}
        } catch (FCRuntimeException fcexp) {
			logger.error("Exception raised while getting the info for invoice.", fcexp);
			model.addAttribute("expMsg", "Invalid Order Id");
			return new ModelAndView("home/error");

		} catch (IdentityAuthorizedException exp) {
			logger.error("Exception raised while getting the info for invoice.", exp);
			return new ModelAndView("home/accessdenied");
		} catch (Exception exp) {
			logger.error("Exception raised while getting the info for invoice.", exp);
			return new ModelAndView("home/error");
		}
		return new ModelAndView(view);

	}
	
	@RequestMapping(value = "tax/invoice", method = RequestMethod.GET)
	public ModelAndView viewTaxInvoice(HttpServletRequest request, HttpServletResponse response, Model model) {
	    
		String view = "home/invoiceGST";
		String txnId = request.getParameter("txnId");
		String txnType = request.getParameter("txnType");
		String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        String imsToken = FCSessionUtil.getAppCookie(request);
		if (StringUtils.isBlank(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }

        if (StringUtils.isBlank(ipAddress)) {
            ipAddress = "NA";
        }
        
        if (StringUtils.isBlank(userAgent)) {
            userAgent = "NA";
        }

		try {
			TaxInvoiceDetails taxInvoiceDetails = taxInvoiceService.getTaxInvoiceDetails(txnId, txnType, imsToken, userAgent, ipAddress);
			boolean isCoupon = false;
			if(FCConstants.COUPON.equalsIgnoreCase(taxInvoiceDetails.getCategory())){
				isCoupon = true;
			}
			model.addAttribute("taxInvoiceDetails", taxInvoiceDetails);
			model.addAttribute("isCoupon", isCoupon);
			String accountType=FCConstants.txnTypeAccountMap.get(txnType);
			if(Strings.isNullOrEmpty(accountType)) {
				model.addAttribute("accountType", "fcptl");
			} else if (FCConstants.COUPON.equalsIgnoreCase(taxInvoiceDetails.getCategory())) {
				model.addAttribute("accountType", "aspl");
			} else {
				model.addAttribute("accountType", accountType);
			}
		} catch (Exception exp) {
			logger.error("Exception raised while getting the info for invoice.", exp);
			return new ModelAndView("home/error");
		}
		return new ModelAndView(view);

	}
	
	

	@RequestMapping(value = "mycoupons", method = RequestMethod.GET)
	public ModelAndView getMyCoupons(HttpServletRequest request, HttpServletResponse response, Model model) {
		Integer userid = null;
		try {
			String pageTitle = "FreeCharge mycoupons";
			boolean islogin = false;
			String email = null;
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null) {
				return new ModelAndView("redirect:/");
			}
			model.addAttribute("isMyCouponsDisplayEnabled", true);
			model.addAttribute("title", pageTitle);
			model.addAttribute("email", email);
		} catch (Exception e) {
			logger.error("Error in getting coupons for userId= " + userid, e);
		}
		return new ModelAndView("mycoupons");
	}
	
	@RequestMapping(value = "mycontacts", method = RequestMethod.GET)
	public ModelAndView getmyContacts(HttpServletRequest request, HttpServletResponse response, Model model) {
		Integer userid = null;
		try {
			String pageTitle = "FreeCharge mycontacts";
			boolean islogin = false;
			String email = null;
			String delegateName = WebConstants.DELEGATE_BEAN_USERCONTACTS;
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null) {
				return new ModelAndView("redirect:/");
			}
			MyContactsListWebDo contactslistwebdo = new MyContactsListWebDo();
			contactslistwebdo.setEmail(email);
			contactslistwebdo.setUserid(userid);
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, contactslistwebdo);
			contactslistwebdo = ((MyContactsListWebDo) (webContext.getBaseWebDO()));
			model.addAttribute("title", pageTitle);
			model.addAttribute("userconts", contactslistwebdo);
			model.addAttribute("usercontact", new MyContactsWebDO());
		} catch (Exception e) {
			logger.error("Error in getting contacts for userId= " + userid, e);
		}

		return new ModelAndView("home/contacts");
	}

	@RequestMapping(value = "updatecontact", method = RequestMethod.POST)
	public String updateContact(@ModelAttribute(value = "usercontact") MyContactsWebDO mycontactwebdo, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
	    long startTime = System.currentTimeMillis();
	    String view = "contacts";
		response.setContentType("application/json");
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "userProfileController3");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			boolean islogin = false;
			String email = null;
			Integer userid = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null || !email.equals(mycontactwebdo.getEmail())) {
				model.addAttribute("loginstatus", false);
				return "jsonView";
			}
			model.addAttribute("loginstatus", true);
			validator.validate(mycontactwebdo, bindingResult);
			if (bindingResult.hasErrors()) {
				model.addAttribute("status", false);
				model.addAttribute("message", "please give valid contact details");
				return "jsonView";
			}
			String delegateName = WebConstants.DELEGATE_BEAN_UPDATECONTACT;
			mycontactwebdo.setUserid(userid);
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, mycontactwebdo);
			mycontactwebdo = ((MyContactsWebDO) (webContext.getBaseWebDO()));
			model.addAttribute("usercontact", mycontactwebdo);
			model.addAttribute("status", mycontactwebdo.getUpdatestatus());
			model.addAttribute("serviceno", mycontactwebdo.getServiceno());
			model.addAttribute("name", mycontactwebdo.getName());
			model.addAttribute("operator", mycontactwebdo.getOperatorname());
			// model.addAttribute("message", "contact updated");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (Exception e) {
			model.addAttribute("message", "contact not updated");
			model.addAttribute("status", false);
			logger.error("Exception while trying to update user profile:\n" + ExceptionUtils.getFullStackTrace(e));
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return "jsonView";
	}

    private void setPolicyNumberAndBillerNameToModelFromMetaData(Model model, String metadata) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode metaData = objectMapper.readTree(metadata);
        JsonNode fulfillmentMetadata = objectMapper.readTree(metaData.get("fulfillmentMetadata").asText());
        JsonNode displayMetadata = objectMapper.readTree(fulfillmentMetadata.get("displayMetadata").asText());
        String policyNumber = displayMetadata.get(FCConstants.INSURANCE_BILLER_NAME).asText();
        String billerName = displayMetadata.get(FCConstants.INSURANCE_POLICY_NUMBER).asText();
		logger.info("Policy Number " + policyNumber);
		logger.info("Biller name " + billerName);
        model.addAttribute(FCConstants.INSURANCE_BILLER_NAME, policyNumber);
        model.addAttribute(FCConstants.INSURANCE_POLICY_NUMBER, billerName);
    }

}
