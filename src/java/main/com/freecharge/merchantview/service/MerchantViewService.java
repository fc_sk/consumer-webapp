package com.freecharge.merchantview.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.merchantview.entities.ParticipantView;
import com.freecharge.merchantview.entities.ShopoView;
import com.freecharge.merchantview.entities.SplitView;
import com.freecharge.tsm.service.TSMService;
import com.freecharge.views.P2PAndP2MView;
import com.freecharge.wallet.OneCheckWalletService;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.tsm.response.GetTxnByTxnIdResponse;
import com.snapdeal.payments.tsm.response.GetTxnByTxnIdResponseV2;
import com.snapdeal.payments.tsm.response.LinkTxnResponse;
import com.snapdeal.payments.tsm.response.LocalTxnResponse;
import com.snapdeal.payments.view.client.impl.MerchantViewClient;
import com.snapdeal.payments.view.client.impl.RequestViewClient;
import com.snapdeal.payments.view.commons.enums.ActionType;
import com.snapdeal.payments.view.commons.exception.PaymentsViewServiceException;
import com.snapdeal.payments.view.commons.exception.service.PaymentsViewGenericException;
import com.snapdeal.payments.view.commons.request.ClientConfig;
import com.snapdeal.payments.view.merchant.commons.request.GetMerchantViewSearchWithFilterRequest;
import com.snapdeal.payments.view.merchant.commons.request.MerchantViewSearch;
import com.snapdeal.payments.view.merchant.commons.response.GetTransactionsResponse;
import com.snapdeal.payments.view.request.commons.dto.ActionDto;
import com.snapdeal.payments.view.request.commons.dto.RVTransactionDto;
import com.snapdeal.payments.view.request.commons.request.GetRequestViewSearchRequest;
import com.snapdeal.payments.view.request.commons.request.GetUserActionsHistoryRequest;
import com.snapdeal.payments.view.request.commons.response.GetRequestViewResponse;
import com.snapdeal.payments.view.request.commons.response.GetUserActionsHistoryResponse;
import com.snapdeal.payments.view.utils.ClientDetails;

public class MerchantViewService {

	private final Logger logger = LoggingFactory.getLogger(MerchantViewService.class);

	private String clientKey;
	private String clientId;
	private String port;
	private String IP;
	private int apiTimeOut;

	@Autowired
	private MerchantViewClient merchantViewSnapdealClient;

	@Autowired
	private RequestViewClient requestViewSnapdealClient;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private TSMService tsmService;

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public int getApiTimeOut() {
		return apiTimeOut;
	}

	public void setApiTimeOut(int apiTimeOut) {
		this.apiTimeOut = apiTimeOut;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientKey() {
		return clientKey;
	}

	public String getClientId() {
		return clientId;
	}

	public OneCheckWalletService getOneCheckWalletService() {
		return oneCheckWalletService;
	}

	public void setOneCheckWalletService(OneCheckWalletService oneCheckWalletService) {
		this.oneCheckWalletService = oneCheckWalletService;
	}

	public TSMService getTsmService() {
		return tsmService;
	}

	public void setTsmService(TSMService tsmService) {
		this.tsmService = tsmService;
	}

	public MerchantViewClient getMerchantViewSnapdealClient() {
		return merchantViewSnapdealClient;
	}

	public void setMerchantViewSnapdealClient(MerchantViewClient merchantViewSnapdealClient) {
		this.merchantViewSnapdealClient = merchantViewSnapdealClient;
	}

	public RequestViewClient getRequestViewSnapdealClient() {
		return requestViewSnapdealClient;
	}

	public void setRequestViewSnapdealClient(RequestViewClient requestViewSnapdealClient) {
		this.requestViewSnapdealClient = requestViewSnapdealClient;
	}

	public GetTransactionsResponse getTransactionDetailsViaMerchantViewClient(String txnId, String merchantId) {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetMerchantViewSearchWithFilterRequest request = new GetMerchantViewSearchWithFilterRequest();

		// assigning search criteria
		MerchantViewSearch merchantViewSearch = new MerchantViewSearch();
		// merchantViewSearch.setTransactionId(txnId);
		merchantViewSearch.setMerchantTxnId(txnId);
		request.setSearchCriteria(merchantViewSearch);

		// set merchant id
		request.setMerchantId(merchantId);

		// set other parameters
		request.setPage(1);
		request.setLimit(50);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetTransactionsResponse merchantViewResponse = null;

		try {
			logger.info("Request: MV Search with Filter -> " + request);
			merchantViewResponse = merchantViewSnapdealClient.getMerchantViewSearchWithFilter(request);
			// GetTransactionsResponse merchantViewResponse =
			// merchantViewSnapdealClient.getMerchantViewSearch(merchantViewRequest);
			logger.info("Response: MV Search with Filter -> " + merchantViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("The Exception in MV API: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (PaymentsViewGenericException g) {
			logger.error("The Exception in MV API: " + g.getErrCode() + " :: " + g.getErrMsg());
		} catch (Exception x) {
			logger.error("Other Exception in MV API: ", x);
		}

		return merchantViewResponse;
	}

	public GetTransactionsResponse getTransactionDetailsViaMerchantViewClientTest(String orderId, String merchantId) {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetMerchantViewSearchWithFilterRequest request = new GetMerchantViewSearchWithFilterRequest();

		// assigning search criteria
		MerchantViewSearch merchantViewSearch = new MerchantViewSearch();
		// merchantViewSearch.setTransactionId(txnId);
		merchantViewSearch.setOrderId(orderId);
		request.setSearchCriteria(merchantViewSearch);

		// set merchant id
		request.setMerchantId(merchantId);

		// set other parameters
		request.setPage(1);
		request.setLimit(50);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetTransactionsResponse merchantViewResponse = null;

		try {
			logger.info("Request: MV Search with Filter (OrderID) -> " + request);
			merchantViewResponse = merchantViewSnapdealClient.getMerchantViewSearchWithFilter(request);
			// GetTransactionsResponse merchantViewResponse =
			// merchantViewSnapdealClient.getMerchantViewSearch(merchantViewRequest);
			logger.info("Response: MV Search with Filter (OrderID)-> " + merchantViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("The Exception in MV API: (OrderID)" + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (PaymentsViewGenericException g) {
			logger.error("The Exception in MV API: (OrderID)" + g.getErrCode() + " :: " + g.getErrMsg());
		} catch (Exception x) {
			logger.error("Other Exception in MV API: (OrderID) ", x);
		}

		return merchantViewResponse;
	}

	public GetRequestViewResponse getTransactionDetailsViaRequestViewClient(String merchantId) {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetRequestViewSearchRequest request = new GetRequestViewSearchRequest();

		// assigning search criteria
		request.setSrcPartyId(merchantId);

		// set other parameters
		request.setPage(1);
		request.setLimit(50);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetRequestViewResponse requestViewResponse = null;

		try {
			logger.info("Request: RV Search -> " + request);
			requestViewResponse = requestViewSnapdealClient.getRequestViewSearch(request);
			logger.info("Response: RV Search -> " + requestViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("The Exception in RV API: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (Exception x) {
			logger.error("Other Exception in RV API: ", x);
		}

		return requestViewResponse;
	}

	public List<RVTransactionDto> getTransactionsByEmail(String emailId) {

		List<RVTransactionDto> transactionList = null;
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetRequestViewSearchRequest request = new GetRequestViewSearchRequest();

		// finding the user id using the email id
		String userId = "";
		try {
			userId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			logger.info("IMS id (user id): From once check wallet service is -> " + userId);
		} catch (Exception e) {
			logger.error("Exception while fetching user id from email :: " + e.getMessage());
		}

		// assigning search criteria
		request.setSrcPartyId(userId);

		// set other parameters
		request.setPage(1);
		request.setLimit(50);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetRequestViewResponse requestViewResponse = null;

		try {
			logger.info("Request: RV Request View Search (for search by email case) is -> " + request);
			requestViewResponse = requestViewSnapdealClient.getRequestViewSearch(request);
			logger.info("Response: RV Request View Search (for search by email case) is -> " + requestViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("Exception: in RV API: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (Exception x) {
			logger.error("Exception: Other - in RV API: ", x);
		}

		if (requestViewResponse != null) {
			transactionList = requestViewResponse.getRequestViewTransactionDto();
		}

		return transactionList;

	}

	public List<P2PAndP2MView> getShopoTransactionsByEmail(String id, Date startDate, Date endDate, Boolean isEmailId) {

		// the id can either be email id or a phone number

		List<ActionDto> transactionList = null;
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetUserActionsHistoryRequest request = new GetUserActionsHistoryRequest();

		// finding the user id using the email id
		String userId = "";

		if (isEmailId) {
			try {
				userId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(id);
				logger.info("IMS id (user id): From once check wallet service using email id is -> " + userId);
			} catch (Exception e) {
				logger.error("Exception while fetching user id from email :: " + e.getMessage());
			}
		} else {
			try {
				GetUserResponse userResponse = oneCheckWalletService.getUserByMobile(id);
				if (userResponse != null && userResponse.getUserDetails() != null
						&& userResponse.getUserDetails().getUserId() != null) {
					userId = userResponse.getUserDetails().getUserId();
				}
				logger.info("IMS id (user id): From once check wallet service using mobile no is -> " + userId);
			} catch (Exception e) {
				logger.error("Exception while fetching user id from mobile :: " + e.getMessage());
			}
		}

		if (userId == null || "".equals(userId))
			return null;
		// assigning search criteria
		request.setUserId(userId);

		// set other parameters
		request.setStartDate(startDate);
		request.setEndDate(endDate);

		logger.info("Start Date - " + startDate);
		logger.info("End Date - " + endDate);

		List<ActionType> actionTypes = new ArrayList<ActionType>();
		actionTypes.add(ActionType.forvalue("ESCROW_PAYMENT"));
		request.setActionType(actionTypes);

		List<ActionType> referenceTypes = new ArrayList<ActionType>();
		request.setReferenceType(referenceTypes);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetUserActionsHistoryResponse requestViewResponse = null;

		try {
			logger.info("Request: Action History (for shopo - search by email case) is -> " + request + "; "
					+ request.getActionType());
			requestViewResponse = requestViewSnapdealClient.getUserActionsHistory(request);
			logger.info("Response: Action History (for shopo - search by email case) is -> " + requestViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("Exception: in Action History API: Shopo: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (Exception x) {
			logger.error("Exception: Other - in Action History API: Shopo: ", x);
		}

		if (requestViewResponse != null) {
			transactionList = requestViewResponse.getActionsList();
		}

		// non null list is returned always
		List<P2PAndP2MView> shopoTransactionList = getTransactionSummaryFromActionDto(transactionList);
		// sort the list by id
		Collections.sort(shopoTransactionList);
		return shopoTransactionList;
	}

	public List<P2PAndP2MView> getSplitTransactionsByEmail(String emailId, Date startDate, Date endDate) {

		List<ActionDto> transactionList = null;
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetUserActionsHistoryRequest request = new GetUserActionsHistoryRequest();

		// finding the user id using the email id
		String userId = "";
		try {
			userId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			logger.info("IMS id (user id): From once check wallet service is -> " + userId);
		} catch (Exception e) {
			logger.error("Exception while fetching user id from email :: " + e.getMessage());
		}

		// assigning search criteria
		request.setUserId(userId);

		// set other parameters
		request.setStartDate(startDate);
		request.setEndDate(endDate);

		logger.info("Start Date - " + startDate);
		logger.info("End Date - " + endDate);

		List<ActionType> actionTypes = new ArrayList<ActionType>();
		actionTypes.add(ActionType.forvalue("P2P_REQUEST_MONEY"));
		request.setActionType(actionTypes);

		List<ActionType> referenceTypes = new ArrayList<ActionType>();
		referenceTypes.add(ActionType.forvalue("SPLIT_MONEY"));
		request.setReferenceType(referenceTypes);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetUserActionsHistoryResponse requestViewResponse = null;

		try {
			logger.info("Request: Action History (for search by email case) is -> " + request + "; "
					+ request.getActionType());
			requestViewResponse = requestViewSnapdealClient.getUserActionsHistory(request);
			logger.info("Response: Action History (for search by email case) is -> " + requestViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("Exception: in Action History API: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (Exception x) {
			logger.error("Exception: Other - in Action History API: ", x);
		}

		if (requestViewResponse != null) {
			transactionList = requestViewResponse.getActionsList();
		}

		// non null list is returned always
		List<P2PAndP2MView> splitTransactionList = getTransactionSummaryFromActionDto(transactionList);
		// sort the list by id
		Collections.sort(splitTransactionList);
		return splitTransactionList;

	}

	private List<P2PAndP2MView> getTransactionSummaryFromActionDto(List<ActionDto> transactionList) {
		// TODO Auto-generated method stub
		List<P2PAndP2MView> p2PSplitViewList = new ArrayList<P2PAndP2MView>();

		if (transactionList != null) {

			GetUserResponse sourceUserResponse = null;
			// do this only once and set for all
			if (transactionList.size() > 0) {
				sourceUserResponse = oneCheckWalletService.getUserByID(transactionList.get(0).getUserId(), null, null);
				logger.info("Initiator/Source user response is -> " + sourceUserResponse);
			}

			for (ActionDto actionDto : transactionList) {

				P2PAndP2MView p2PSplitView = new P2PAndP2MView();
				JSONParser parser = new JSONParser();
				JSONObject metaData = null;
				JSONObject txnMetadata = null;
				JSONObject displayInfoMetadata = null;
				JSONObject otherPartyData = null;

				try {
					metaData = (JSONObject) parser.parse(actionDto.getMetadata());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Map<String, Object> metaData =
				// this.getParsedMetadata(actionDto.getMetadata());
				logger.info("parsed ActionDto metadata is -> " + metaData);

				if (metaData != null) {

					// Set the Initiator/Source details in view
					if (sourceUserResponse != null) {
						if (sourceUserResponse.getUserDetails() != null) {
							p2PSplitView.setEmail(sourceUserResponse.getUserDetails().getEmailId());
							p2PSplitView.setName(sourceUserResponse.getUserDetails().getFirstName() + " "
									+ sourceUserResponse.getUserDetails().getLastName());
							p2PSplitView.setContact(sourceUserResponse.getUserDetails().getMobileNumber());
						}
					}

					logger.info("Other Party DTO from parsed ActionDto is -> " + metaData.get("otherPartyDTO"));

					// getting other party type
					if (metaData.get("otherPartyDTO") != null) {
						otherPartyData = (JSONObject) metaData.get("otherPartyDTO");
						logger.info("Parsed otherPartyDTO json -> " + otherPartyData);
						if (otherPartyData != null) {
							p2PSplitView.setOtherPartyType((String) otherPartyData.get("partyType"));
						}
					}

					// get reference id
					String referenceId = (String) metaData.get("referenceId");
					// set this in View
					p2PSplitView.setReferenceId(referenceId);

					logger.info("Transaction Metadata from parsed ActionDto is -> " + metaData.get("txnMetaData"));

					// parsing txn Metadata
					if (metaData.get("txnMetaData") != null) {
						txnMetadata = (JSONObject) metaData.get("txnMetaData");
						logger.info("Parsed txnMetaData json -> " + txnMetadata);

						if (txnMetadata != null) {
							logger.info(
									"Display info: form parsed txnMetaData json -> " + txnMetadata.get("displayInfo"));

							// parsing displayinfo
							if (txnMetadata.get("displayInfo") != null) {

								try {
									displayInfoMetadata = (JSONObject) parser
											.parse((String) txnMetadata.get("displayInfo"));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								logger.info("Parsed displyinfo json -> " + displayInfoMetadata);

								// get source and destination diplay info
								JSONObject json_sourceDisplayInfo = null;
								JSONObject json_destDisplayInfo = null;
								if (displayInfoMetadata != null) {
									if (displayInfoMetadata.get("sourceDisplayInfo") != null) {
										if ((displayInfoMetadata.get("sourceDisplayInfo")) instanceof String) {
											// parse
											try {
												json_sourceDisplayInfo = (JSONObject) parser
														.parse((String) displayInfoMetadata.get("sourceDisplayInfo"));
											} catch (ParseException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										} else {
											json_sourceDisplayInfo = (JSONObject) displayInfoMetadata
													.get("sourceDisplayInfo");
										}
									}
									if (displayInfoMetadata.get("destDisplayInfo") != null) {

										if ((displayInfoMetadata.get("destDisplayInfo")) instanceof String) {
											// parse
											try {
												json_destDisplayInfo = (JSONObject) parser
														.parse((String) displayInfoMetadata.get("destDisplayInfo"));
											} catch (ParseException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										} else {
											json_destDisplayInfo = (JSONObject) displayInfoMetadata
													.get("destDisplayInfo");
										}
									}

									if (json_sourceDisplayInfo != null) {
										logger.info("Info: name and number from source display info -> "
												+ json_sourceDisplayInfo.get("profileName") + "::"
												+ json_sourceDisplayInfo.get("mobileNo"));
										p2PSplitView.setName((String) json_sourceDisplayInfo.get("profileName"));
										p2PSplitView.setContact((String) json_sourceDisplayInfo.get("mobile"));
									}

									if (json_destDisplayInfo != null) {
										logger.info("Info: contact name and mobile from dest display info -> "
												+ json_destDisplayInfo.get("contactName") + "::"
												+ json_destDisplayInfo.get("mobileNo"));
										p2PSplitView
												.setOtherPartyName((String) json_destDisplayInfo.get("contactName"));
										p2PSplitView.setOtherPartyContact((String) json_destDisplayInfo.get("mobile"));
									}

								}

							}
						}
					}

					// register txn amount; only if its non null
					if (metaData.get("txnAmount") != null) {
						p2PSplitView.setTxnAmount(
								new BigDecimal((Double) metaData.get("txnAmount"), MathContext.UNLIMITED));
					}
					p2PSplitView.setTxnType((String) metaData.get("referenceType"));
				}
				p2PSplitView.setOrderId(actionDto.getActionId());
				p2PSplitView.setTimeStamp(actionDto.getActionInitiationTimestamp());
				p2PSplitView.setStatus(actionDto.getActionState());
				p2PSplitView.setRequestType(actionDto.getActionViewState());
				p2PSplitView.setOtherPartyType("USER");
				p2PSplitViewList.add(p2PSplitView);
			}
		}
		return p2PSplitViewList;
	}

	public List<RVTransactionDto> getTransactionsByOrderId(String orderId) {

		List<RVTransactionDto> transactionList = null;
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);

		// initializing request object
		GetRequestViewSearchRequest request = new GetRequestViewSearchRequest();

		// assigning search criteria
		request.setFcTxnId(orderId);
		// set other parameters
		request.setPage(1);
		request.setLimit(50);

		// set cleint config
		request.setClientConfig(clientConfig);
		GetRequestViewResponse requestViewResponse = null;

		try {
			logger.info("Request: RV Request View Search (for search by order id case) is -> " + request);
			requestViewResponse = requestViewSnapdealClient.getRequestViewSearch(request);
			logger.info("Response: RV Request View Search (for search by order id case) is -> " + requestViewResponse);
		} catch (PaymentsViewServiceException e) {
			logger.error("Exception: in RV API: " + e.getErrCode() + " :: " + e.getErrMsg(), e);
		} catch (Exception x) {
			logger.error("Exception: Other - in RV API: " + x.getMessage());
		}

		if (requestViewResponse != null) {
			transactionList = requestViewResponse.getRequestViewTransactionDto();
		}

		return transactionList;

	}

	private Map<String, Object> getParsedMetadata(String globalTxnMetaData) {

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> metaData = new HashMap<String, Object>();
		try {
			if (globalTxnMetaData != null) {
				metaData = mapper.readValue(globalTxnMetaData, new TypeReference<Map<String, String>>() {
				});
			}
		} catch (IOException e) {
			logger.error("Error parsing Global Meta-Data to JSON", e);
			// e.printStackTrace();
		}
		return metaData;

	}

	public List<P2PAndP2MView> getP2PAndP2MByOrderId(String orderIdPramValue) {

		List<P2PAndP2MView> p2PAndP2MViewList = null;
		GetTxnByTxnIdResponseV2 tsmResponse = tsmService.getP2PAndP2MResponse(orderIdPramValue);
		logger.info("TSM Response for P2P is ->" + tsmResponse);

		if (tsmResponse != null) {

			p2PAndP2MViewList = new ArrayList<P2PAndP2MView>();
			// Map<String, Object> metaData =
			// this.getParsedMetadata(tsmResponse.getGlobalTxnMetaData());

			JSONParser parser = new JSONParser();
			JSONObject metaData = new JSONObject();
			if (tsmResponse.getGlobalTxnMetaData() != null) {
				if ((tsmResponse.getGlobalTxnMetaData() instanceof String)) {
					// parse
					try {
						metaData = (JSONObject) parser.parse((String) tsmResponse.getGlobalTxnMetaData());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else {
					logger.error("Global Metadata is not in right format");
					return null;
				}
			}

			logger.info("parsed TSM metadata is -> " + metaData);

			P2PAndP2MView p2PAndP2MView = new P2PAndP2MView();

			// JSONParser parser = new JSONParser();
			JSONObject json = null;

			if (metaData != null) {
				logger.info("Display info from parsed TSM metadata is -> " + metaData.get("displayInfo"));

				// set errorCode field for Failed transactions, else N/A
				if ("FAILED".equals(tsmResponse.getGlobalTxnState())) {
					p2PAndP2MView.setErrorCode((String) metaData.get("errCode"));
				} else {
					p2PAndP2MView.setErrorCode("N/A");
				}

				if (metaData.get("displayInfo") != null) {

					try {
						json = (JSONObject) parser.parse((String) metaData.get("displayInfo"));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					logger.info("Display info: parsed json -> " + json);

					if (json != null) {
						logger.info("Display info: source display info -> " + json.get("sourceDisplayInfo"));
						// if its string then parse else simply cast
						JSONObject json_sourceDisplayInfo = null;

						if (json.get("sourceDisplayInfo") != null) {
							if ((json.get("sourceDisplayInfo")) instanceof String) {
								// parse
								try {
									json_sourceDisplayInfo = (JSONObject) parser
											.parse((String) json.get("sourceDisplayInfo"));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								json_sourceDisplayInfo = (JSONObject) json.get("sourceDisplayInfo");
							}
						}

						JSONObject json_destDisplayInfo = null;
						if (json.get("destDisplayInfo") != null) {
							if ((json.get("destDisplayInfo")) instanceof String) {
								// parse
								try {
									json_destDisplayInfo = (JSONObject) parser
											.parse((String) json.get("destDisplayInfo"));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								json_destDisplayInfo = (JSONObject) json.get("destDisplayInfo");
							}
						}

						if (json_sourceDisplayInfo != null) {
							logger.info("Info: name and number from source display info -> "
									+ json_sourceDisplayInfo.get("profileName") + "::"
									+ json_sourceDisplayInfo.get("mobile"));
							p2PAndP2MView.setName((String) json_sourceDisplayInfo.get("profileName"));
							p2PAndP2MView.setContact((String) json_sourceDisplayInfo.get("mobile"));

						}

						if (json_destDisplayInfo != null) {
							logger.info("Info: contact name and mobile from dest display info -> "
									+ json_destDisplayInfo.get("contactName") + "::"
									+ json_destDisplayInfo.get("mobile"));
							p2PAndP2MView.setOtherPartyName((String) json_destDisplayInfo.get("contactName"));
							p2PAndP2MView.setOtherPartyContact((String) json_destDisplayInfo.get("mobile"));

						}
					}
				}
				// fetch email, and name using IMS id for source and override it
				// in view
				GetUserResponse sourceUserResponse = oneCheckWalletService
						.getUserByID((String) metaData.get("senderIMSId"), null, null);
				logger.info("Source user response is -> " + sourceUserResponse);
				if (sourceUserResponse != null) {
					if (sourceUserResponse.getUserDetails() != null) {
						p2PAndP2MView.setEmail(sourceUserResponse.getUserDetails().getEmailId());
						p2PAndP2MView.setName(sourceUserResponse.getUserDetails().getFirstName() + " "
								+ sourceUserResponse.getUserDetails().getLastName());
					}
				}

				// fetch email, and name using IMS id for destination and
				// override it in view
				GetUserResponse destUserResponse = oneCheckWalletService
						.getUserByID((String) metaData.get("receiverId"), null, null);
				logger.info("Destination user response is -> " + destUserResponse);
				if (destUserResponse != null) {
					if (destUserResponse.getUserDetails() != null) {
						p2PAndP2MView.setOtherPartyEmail(destUserResponse.getUserDetails().getEmailId());
						p2PAndP2MView.setOtherPartyName(destUserResponse.getUserDetails().getFirstName() + " "
								+ destUserResponse.getUserDetails().getLastName());
					}
				}

				// capture whether its a P2P or P2M transaction and add paytag
				// field
				if ("PAYTAG".equals((String) metaData.get("toHandleType"))) {
					p2PAndP2MView.setOtherPartyType("MERCHANT");
					p2PAndP2MView.setPaytag((String) metaData.get("toHandle"));
				} else {
					p2PAndP2MView.setOtherPartyType("USER");
					p2PAndP2MView.setPaytag("N/A");
				}

				// fetching domain (P2P, P2M)
				JSONObject globalEventContextJson = null;
				try {
					if (metaData.get("eventContext") != null) {
						globalEventContextJson = (JSONObject) parser.parse((String) metaData.get("eventContext"));
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("Display info: parsed event context in json -> " + globalEventContextJson);

				if (globalEventContextJson != null) {
					logger.info("Display Info: domain in global event context metadata -> "
							+ globalEventContextJson.get("domain"));
					// add the domain to View
					p2PAndP2MView.setDomain((String) globalEventContextJson.get("domain"));
				}
			}

			// Do anything you want to do with local metadata here!
			// getting the ip address and location
			List<LocalTxnResponse> localTxns = tsmResponse.getLocalTxns();
			String localMetadata = null;
			if (localTxns != null) {
				if (localTxns.get(0) != null) {
					localMetadata = localTxns.get(0).getMetadata();
				}
			}

			// parse local metadata if its not null
			json = null;
			if (localMetadata != null) {
				try {
					json = (JSONObject) parser.parse(localMetadata);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("Display info: parsed local metadata in json -> " + json);
				if (json != null) {
					logger.info("Request Metadata in local metadata is -> " + json.get("requestMetadata"));
					// got the response; use it to fetch the ip address and
					// location
					if (json.get("requestMetadata") != null) {
						JSONObject subJson = null;
						try {
							subJson = (JSONObject) parser.parse((String) json.get("requestMetadata"));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						logger.info("Display info: request metadata in subJson -> " + subJson);

						if (subJson != null) {
							JSONObject eventContextJson = null;
							logger.info("Display info: event context in request metadata is -> "
									+ subJson.get("eventContext"));

							if (subJson.get("eventContext") != null) {
								{
									try {
										eventContextJson = (JSONObject) parser
												.parse((String) subJson.get("eventContext"));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
								logger.info("Display info: event context Json -> " + eventContextJson);

								if (eventContextJson != null) {
									logger.info(
											"device metadata in event context is -> " + eventContextJson.get("device"));

									if (eventContextJson.get("device") != null) {
										JSONObject deviceJson = null;
										if ((eventContextJson.get("device")) instanceof String) {
											// parse
											try {
												deviceJson = (JSONObject) parser
														.parse((String) eventContextJson.get("device"));
											} catch (ParseException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										} else {
											deviceJson = (JSONObject) eventContextJson.get("device");
										}

										if (deviceJson != null) {
											logger.info("geo location is -> " + deviceJson.get("gpsLocation"));
											logger.info("Ip address is -> " + deviceJson.get("ipAddress"));
										}
									}
								}
							}
						}
					}
				}
			}

			p2PAndP2MView.setOrderId(tsmResponse.getGlobalTxnId());
			p2PAndP2MView.setTimeStamp(tsmResponse.getCreatedTime());
			// Missing: migration status
			// Missing: running balance
			p2PAndP2MView.setStatus(tsmResponse.getGlobalTxnState());
			p2PAndP2MView.setTxnAmount(tsmResponse.getGlobalTxnAmount());
			p2PAndP2MView.setRequestType(tsmResponse.getGlobalTxnType());
			// Missing: idempotency id
			// Missing: Started at
			// Missing: Completed at
			// Missing other party migration status
			// Missing other party wallet balance

			p2PAndP2MViewList.add(p2PAndP2MView);
		}
		return p2PAndP2MViewList;
	}

	@PostConstruct
	public void init() throws Exception {

		try {
			ClientDetails.init(IP, port, clientKey, clientId, apiTimeOut);
		} catch (Exception e) {
			logger.error("Exception while initializing Merchant View Client Config", e);
		}

	}

	public SplitView getSplitDetailsByOrderId(String orderIdPramValue) {
		// TODO Auto-generated method stub
		// Using TSM to get SPLIT details and rendering it to the SplitView

		SplitView splitView = null;

		GetTxnByTxnIdResponseV2 tsmResponse = tsmService.getP2PSplitResponse(orderIdPramValue);
		logger.info("TSM Response for P2P SPLIT is ->" + tsmResponse);

		if (tsmResponse != null) {

			splitView = new SplitView();
			// Map<String, Object> metaData =
			// this.getParsedMetadata(tsmResponse.getGlobalTxnMetaData());

			JSONParser parser = new JSONParser();
			JSONObject json = null;
			JSONObject metaData = null;

			try {
				metaData = (JSONObject) parser.parse((String) tsmResponse.getGlobalTxnMetaData());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			logger.info("parsed TSM global metadata is -> " + metaData);

			if (metaData != null) {

				// set errorCode field if overall SPLIT has FAILED, else N/A
				if ("FAILED".equals(tsmResponse.getGlobalTxnState())) {
					splitView.setErrorCode((String) metaData.get("errCode"));
				} else {
					splitView.setErrorCode("N/A");
				}

				// set split txn id, amount, requester contribution and name,
				// email, phone from sender ims id

				GetUserResponse requestorUserResponse = oneCheckWalletService
						.getUserByID((String) metaData.get("senderIMSId"), null, null);
				logger.info("Requestor user response is -> " + requestorUserResponse);

				if (requestorUserResponse != null) {
					if (requestorUserResponse.getUserDetails() != null) {
						splitView.setEmail(requestorUserResponse.getUserDetails().getEmailId());
						splitView.setName(requestorUserResponse.getUserDetails().getFirstName() + " "
								+ requestorUserResponse.getUserDetails().getLastName());
						splitView.setContact(requestorUserResponse.getUserDetails().getMobileNumber());
					}
				}

				// setting requester share
				splitView.setShare(
						new BigDecimal((Double) metaData.get("requestorContribution"), MathContext.UNLIMITED));
				// setting global txn id
				splitView.setOrderId((String) tsmResponse.getGlobalTxnId());
				// setting amount
				splitView.setAmount(new BigDecimal((Double) metaData.get("amount"), MathContext.UNLIMITED));
				// setting status
				splitView.setStatus(tsmResponse.getGlobalTxnState());
				// setting Txn Type
				splitView.setDomain(tsmResponse.getGlobalTxnType());
				// set TimeStamp
				splitView.setTimeStamp(tsmResponse.getCreatedTime());

				logger.info("Split Participant Metadata List from parsed TSM metadata is -> "
						+ metaData.get("splitParticipantMetadataList"));

				List<ParticipantView> participantViewList = null;
				// split participant metadata list has most of the info we need
				if (metaData.get("splitParticipantMetadataList") != null) {

					participantViewList = new ArrayList<ParticipantView>();
					// now we will get a list of String
					// parsing it one by one

					List<JSONObject> participantMetadataList = (ArrayList<JSONObject>) metaData
							.get("splitParticipantMetadataList");

					for (JSONObject participantMetadata : participantMetadataList) {

						ParticipantView participantView = new ParticipantView();
						json = participantMetadata;
						logger.info("Participant Metadata: parsed json -> " + json);

						if (json != null) {

							// check if participantIdType is MOBILE or IMS_ID
							if ("IMS_ID".equals((String) json.get("participantIdType"))) {
								// fetch name, mobile and email from IMS

								GetUserResponse participantUserResponse = oneCheckWalletService
										.getUserByID((String) json.get("participantId"), null, null);
								logger.info("Participant user response is -> " + participantUserResponse);

								if (participantUserResponse != null) {
									if (participantUserResponse.getUserDetails() != null) {
										participantView.setOtherPartyEmail(
												participantUserResponse.getUserDetails().getEmailId());
										participantView.setOtherPartyName(
												participantUserResponse.getUserDetails().getFirstName() + " "
														+ participantUserResponse.getUserDetails().getLastName());
										participantView.setOtherPartyContact(
												participantUserResponse.getUserDetails().getMobileNumber());
									}
								}

							} else {
								// use the mobile number itself. IMS id is not
								// available now, shall be implemented later
								participantView.setOtherPartyContact((String) json.get("participantId"));
							}

							// set other parameters
							// participantView.setTransactionTime((Long)
							// json.get("transactionTime"));
							participantView
									.setTxnAmount(new BigDecimal((Double) json.get("amount"), MathContext.UNLIMITED));
							// need to be fetched from downlink txns later
							// participantView.setRequestType("P2P_REQUEST_MONEY");
							// set participant transaction id
							participantView.setTransactionId((String) json.get("participantTxnId"));

							// hit p2p&p2m by order id for each participant to
							// get the information
							List<P2PAndP2MView> participantDetails = getP2PAndP2MByOrderId(
									(String) json.get("participantTxnId"));

							if (participantDetails.size() > 0 && participantDetails.get(0) != null) {
								// this list will have only one entry
								P2PAndP2MView participantDetailsView = participantDetails.get(0);

								// set errorCode field for Failed transactions
								participantView.setErrorCode(participantDetailsView.getErrorCode());
								participantView.setStatus(participantDetailsView.getStatus());
								participantView.setOtherPartyName(participantDetailsView.getOtherPartyName());
								participantView.setOtherPartyEmail(participantDetailsView.getOtherPartyEmail());
								participantView.setTransactionTime(participantDetailsView.getTimeStamp());
								participantView.setRequestType(participantDetailsView.getRequestType());
							} else {
								logger.info("Fetching data from P2P/P2M by order id didn't gave anything");
							}
							// next call TSM again for each of the participant
							// id to get all remaining details
							// use Link Transactions to
							// 1. Fetch TSM response
							// 2. Populate participant transaction types
							// *****************************************//

							// everything is set now in SplitView, lets add it
							// to the list
							participantViewList.add(participantView);
						}
					}
					// set participantMetadataList to splitView
					splitView.setParticipantViewList(participantViewList);
				}
			}
		}
		return splitView;
	}

}
