package com.freecharge.merchantview.entities;

import java.math.BigDecimal;
import java.util.Date;

public class ParticipantView {
	
		//Participant txn info
		private String status;
		private String errorCode;
		private BigDecimal txnAmount;
		private String requestType;
		private String idempotencyId;
		private Date startedAt;
		private Date completedAt;
		private String otherPartyType;
		private String paytag;
		private String otherPartyName;
		private String otherPartyEmail;
		private String otherPartyContact;
		private String otherPartyMigrationStatus;
		private BigDecimal otherPartyWalletBalance;
		private Date transactionTime;
		private String transactionId;
		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		public BigDecimal getTxnAmount() {
			return txnAmount;
		}
		public void setTxnAmount(BigDecimal txnAmount) {
			this.txnAmount = txnAmount;
		}
		public String getRequestType() {
			return requestType;
		}
		public void setRequestType(String requestType) {
			this.requestType = requestType;
		}
		public String getIdempotencyId() {
			return idempotencyId;
		}
		public void setIdempotencyId(String idempotencyId) {
			this.idempotencyId = idempotencyId;
		}
		public Date getStartedAt() {
			return startedAt;
		}
		public void setStartedAt(Date startedAt) {
			this.startedAt = startedAt;
		}
		public Date getCompletedAt() {
			return completedAt;
		}
		public void setCompletedAt(Date completedAt) {
			this.completedAt = completedAt;
		}
		public String getOtherPartyType() {
			return otherPartyType;
		}
		public void setOtherPartyType(String otherPartyType) {
			this.otherPartyType = otherPartyType;
		}
		public String getPaytag() {
			return paytag;
		}
		public void setPaytag(String paytag) {
			this.paytag = paytag;
		}
		public String getOtherPartyName() {
			return otherPartyName;
		}
		public void setOtherPartyName(String otherPartyName) {
			this.otherPartyName = otherPartyName;
		}
		public String getOtherPartyEmail() {
			return otherPartyEmail;
		}
		public void setOtherPartyEmail(String otherPartyEmail) {
			this.otherPartyEmail = otherPartyEmail;
		}
		public String getOtherPartyContact() {
			return otherPartyContact;
		}
		public void setOtherPartyContact(String otherPartyContact) {
			this.otherPartyContact = otherPartyContact;
		}
		public String getOtherPartyMigrationStatus() {
			return otherPartyMigrationStatus;
		}
		public void setOtherPartyMigrationStatus(String otherPartyMigrationStatus) {
			this.otherPartyMigrationStatus = otherPartyMigrationStatus;
		}
		public BigDecimal getOtherPartyWalletBalance() {
			return otherPartyWalletBalance;
		}
		public void setOtherPartyWalletBalance(BigDecimal otherPartyWalletBalance) {
			this.otherPartyWalletBalance = otherPartyWalletBalance;
		}
		public Date getTransactionTime() {
			return transactionTime;
		}
		public void setTransactionTime(Date transactionTime) {
			this.transactionTime = transactionTime;
		}
		public String getTransactionId() {
			return transactionId;
		}
		public void setTransactionId(String transactionId) {
			this.transactionId = transactionId;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((completedAt == null) ? 0 : completedAt.hashCode());
			result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
			result = prime * result + ((idempotencyId == null) ? 0 : idempotencyId.hashCode());
			result = prime * result + ((otherPartyContact == null) ? 0 : otherPartyContact.hashCode());
			result = prime * result + ((otherPartyEmail == null) ? 0 : otherPartyEmail.hashCode());
			result = prime * result + ((otherPartyMigrationStatus == null) ? 0 : otherPartyMigrationStatus.hashCode());
			result = prime * result + ((otherPartyName == null) ? 0 : otherPartyName.hashCode());
			result = prime * result + ((otherPartyType == null) ? 0 : otherPartyType.hashCode());
			result = prime * result + ((otherPartyWalletBalance == null) ? 0 : otherPartyWalletBalance.hashCode());
			result = prime * result + ((paytag == null) ? 0 : paytag.hashCode());
			result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
			result = prime * result + ((startedAt == null) ? 0 : startedAt.hashCode());
			result = prime * result + ((status == null) ? 0 : status.hashCode());
			result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
			result = prime * result + ((transactionTime == null) ? 0 : transactionTime.hashCode());
			result = prime * result + ((txnAmount == null) ? 0 : txnAmount.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ParticipantView other = (ParticipantView) obj;
			if (completedAt == null) {
				if (other.completedAt != null)
					return false;
			} else if (!completedAt.equals(other.completedAt))
				return false;
			if (errorCode == null) {
				if (other.errorCode != null)
					return false;
			} else if (!errorCode.equals(other.errorCode))
				return false;
			if (idempotencyId == null) {
				if (other.idempotencyId != null)
					return false;
			} else if (!idempotencyId.equals(other.idempotencyId))
				return false;
			if (otherPartyContact == null) {
				if (other.otherPartyContact != null)
					return false;
			} else if (!otherPartyContact.equals(other.otherPartyContact))
				return false;
			if (otherPartyEmail == null) {
				if (other.otherPartyEmail != null)
					return false;
			} else if (!otherPartyEmail.equals(other.otherPartyEmail))
				return false;
			if (otherPartyMigrationStatus == null) {
				if (other.otherPartyMigrationStatus != null)
					return false;
			} else if (!otherPartyMigrationStatus.equals(other.otherPartyMigrationStatus))
				return false;
			if (otherPartyName == null) {
				if (other.otherPartyName != null)
					return false;
			} else if (!otherPartyName.equals(other.otherPartyName))
				return false;
			if (otherPartyType == null) {
				if (other.otherPartyType != null)
					return false;
			} else if (!otherPartyType.equals(other.otherPartyType))
				return false;
			if (otherPartyWalletBalance == null) {
				if (other.otherPartyWalletBalance != null)
					return false;
			} else if (!otherPartyWalletBalance.equals(other.otherPartyWalletBalance))
				return false;
			if (paytag == null) {
				if (other.paytag != null)
					return false;
			} else if (!paytag.equals(other.paytag))
				return false;
			if (requestType == null) {
				if (other.requestType != null)
					return false;
			} else if (!requestType.equals(other.requestType))
				return false;
			if (startedAt == null) {
				if (other.startedAt != null)
					return false;
			} else if (!startedAt.equals(other.startedAt))
				return false;
			if (status == null) {
				if (other.status != null)
					return false;
			} else if (!status.equals(other.status))
				return false;
			if (transactionId == null) {
				if (other.transactionId != null)
					return false;
			} else if (!transactionId.equals(other.transactionId))
				return false;
			if (transactionTime == null) {
				if (other.transactionTime != null)
					return false;
			} else if (!transactionTime.equals(other.transactionTime))
				return false;
			if (txnAmount == null) {
				if (other.txnAmount != null)
					return false;
			} else if (!txnAmount.equals(other.txnAmount))
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "ParticipantView [status=" + status + ", errorCode=" + errorCode + ", txnAmount=" + txnAmount
					+ ", requestType=" + requestType + ", idempotencyId=" + idempotencyId + ", startedAt=" + startedAt
					+ ", completedAt=" + completedAt + ", otherPartyType=" + otherPartyType + ", paytag=" + paytag
					+ ", otherPartyName=" + otherPartyName + ", otherPartyEmail=" + otherPartyEmail
					+ ", otherPartyContact=" + otherPartyContact + ", otherPartyMigrationStatus="
					+ otherPartyMigrationStatus + ", otherPartyWalletBalance=" + otherPartyWalletBalance
					+ ", transactionTime=" + transactionTime + ", transactionId=" + transactionId + "]";
		}
}
