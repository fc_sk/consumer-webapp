package com.freecharge.merchantview.entities;

import java.math.BigDecimal;
import java.util.Date;

public class ShopoView {
	
	//Shopo Initiator info
	private String name;
	private String email;
	private String contact;
	private String txnId;
	private Date timeStamp;
	private String migrationStatus;
	private BigDecimal runningBalance;
	private String platform;
	private String state;
	private String status;
	private String type;
	private BigDecimal amount;
	
	//global details
	private String destEmail;
	private String destName;
	private String destContact;
	private Long holdingDuration;
	private String idempotencyId;
	private String walletTxnId;
	private String txnDisplayId;
	private BigDecimal finalizedAmount;
	private BigDecimal cancelledAmount;
	
	//order details
	private String orderName;
	private String orderId;
	private Long quantity;
	private BigDecimal itemPrice;
	private String seller;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getMigrationStatus() {
		return migrationStatus;
	}
	public void setMigrationStatus(String migrationStatus) {
		this.migrationStatus = migrationStatus;
	}
	public BigDecimal getRunningBalance() {
		return runningBalance;
	}
	public void setRunningBalance(BigDecimal runningBalance) {
		this.runningBalance = runningBalance;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getDestEmail() {
		return destEmail;
	}
	public void setDestEmail(String destEmail) {
		this.destEmail = destEmail;
	}
	public String getDestName() {
		return destName;
	}
	public void setDestName(String destName) {
		this.destName = destName;
	}
	public String getDestContact() {
		return destContact;
	}
	public void setDestContact(String destContact) {
		this.destContact = destContact;
	}
	public Long getHoldingDuration() {
		return holdingDuration;
	}
	public void setHoldingDuration(Long holdingDuration) {
		this.holdingDuration = holdingDuration;
	}
	public String getIdempotencyId() {
		return idempotencyId;
	}
	public void setIdempotencyId(String idempotencyId) {
		this.idempotencyId = idempotencyId;
	}
	public String getWalletTxnId() {
		return walletTxnId;
	}
	public void setWalletTxnId(String walletTxnId) {
		this.walletTxnId = walletTxnId;
	}
	public String getTxnDisplayId() {
		return txnDisplayId;
	}
	public void setTxnDisplayId(String txnDisplayId) {
		this.txnDisplayId = txnDisplayId;
	}
	public BigDecimal getFinalizedAmount() {
		return finalizedAmount;
	}
	public void setFinalizedAmount(BigDecimal finalizedAmount) {
		this.finalizedAmount = finalizedAmount;
	}
	public BigDecimal getCancelledAmount() {
		return cancelledAmount;
	}
	public void setCancelledAmount(BigDecimal cancelledAmount) {
		this.cancelledAmount = cancelledAmount;
	}
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((cancelledAmount == null) ? 0 : cancelledAmount.hashCode());
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((destContact == null) ? 0 : destContact.hashCode());
		result = prime * result + ((destEmail == null) ? 0 : destEmail.hashCode());
		result = prime * result + ((destName == null) ? 0 : destName.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((finalizedAmount == null) ? 0 : finalizedAmount.hashCode());
		result = prime * result + ((holdingDuration == null) ? 0 : holdingDuration.hashCode());
		result = prime * result + ((idempotencyId == null) ? 0 : idempotencyId.hashCode());
		result = prime * result + ((itemPrice == null) ? 0 : itemPrice.hashCode());
		result = prime * result + ((migrationStatus == null) ? 0 : migrationStatus.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((orderName == null) ? 0 : orderName.hashCode());
		result = prime * result + ((platform == null) ? 0 : platform.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((runningBalance == null) ? 0 : runningBalance.hashCode());
		result = prime * result + ((seller == null) ? 0 : seller.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result + ((txnDisplayId == null) ? 0 : txnDisplayId.hashCode());
		result = prime * result + ((txnId == null) ? 0 : txnId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((walletTxnId == null) ? 0 : walletTxnId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopoView other = (ShopoView) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (cancelledAmount == null) {
			if (other.cancelledAmount != null)
				return false;
		} else if (!cancelledAmount.equals(other.cancelledAmount))
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (destContact == null) {
			if (other.destContact != null)
				return false;
		} else if (!destContact.equals(other.destContact))
			return false;
		if (destEmail == null) {
			if (other.destEmail != null)
				return false;
		} else if (!destEmail.equals(other.destEmail))
			return false;
		if (destName == null) {
			if (other.destName != null)
				return false;
		} else if (!destName.equals(other.destName))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (finalizedAmount == null) {
			if (other.finalizedAmount != null)
				return false;
		} else if (!finalizedAmount.equals(other.finalizedAmount))
			return false;
		if (holdingDuration == null) {
			if (other.holdingDuration != null)
				return false;
		} else if (!holdingDuration.equals(other.holdingDuration))
			return false;
		if (idempotencyId == null) {
			if (other.idempotencyId != null)
				return false;
		} else if (!idempotencyId.equals(other.idempotencyId))
			return false;
		if (itemPrice == null) {
			if (other.itemPrice != null)
				return false;
		} else if (!itemPrice.equals(other.itemPrice))
			return false;
		if (migrationStatus == null) {
			if (other.migrationStatus != null)
				return false;
		} else if (!migrationStatus.equals(other.migrationStatus))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (orderName == null) {
			if (other.orderName != null)
				return false;
		} else if (!orderName.equals(other.orderName))
			return false;
		if (platform == null) {
			if (other.platform != null)
				return false;
		} else if (!platform.equals(other.platform))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (runningBalance == null) {
			if (other.runningBalance != null)
				return false;
		} else if (!runningBalance.equals(other.runningBalance))
			return false;
		if (seller == null) {
			if (other.seller != null)
				return false;
		} else if (!seller.equals(other.seller))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (txnDisplayId == null) {
			if (other.txnDisplayId != null)
				return false;
		} else if (!txnDisplayId.equals(other.txnDisplayId))
			return false;
		if (txnId == null) {
			if (other.txnId != null)
				return false;
		} else if (!txnId.equals(other.txnId))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (walletTxnId == null) {
			if (other.walletTxnId != null)
				return false;
		} else if (!walletTxnId.equals(other.walletTxnId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ShopoView [name=" + name + ", email=" + email + ", contact=" + contact + ", txnId=" + txnId
				+ ", timeStamp=" + timeStamp + ", migrationStatus=" + migrationStatus + ", runningBalance="
				+ runningBalance + ", platform=" + platform + ", state=" + state + ", status=" + status + ", type="
				+ type + ", amount=" + amount + ", destEmail=" + destEmail + ", destName=" + destName + ", destContact="
				+ destContact + ", holdingDuration=" + holdingDuration + ", idempotencyId=" + idempotencyId
				+ ", walletTxnId=" + walletTxnId + ", txnDisplayId=" + txnDisplayId + ", finalizedAmount="
				+ finalizedAmount + ", cancelledAmount=" + cancelledAmount + ", orderName=" + orderName + ", orderId="
				+ orderId + ", quantity=" + quantity + ", itemPrice=" + itemPrice + ", seller=" + seller + "]";
	}
}
