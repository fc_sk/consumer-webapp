package com.freecharge.merchantview.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SplitView {
		//Split Initiator info
		private String name;
		private String email;
		private String contact;
		private String orderId;
		private Date timeStamp;
		private String migrationStatus;
		private BigDecimal runningBalance;
		private BigDecimal share;
		private String domain;
		private String status;
		private String errorCode;
		private BigDecimal amount;
		
		private List<ParticipantView> participantViewList;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getContact() {
			return contact;
		}

		public void setContact(String contact) {
			this.contact = contact;
		}

		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public Date getTimeStamp() {
			return timeStamp;
		}

		public void setTimeStamp(Date timeStamp) {
			this.timeStamp = timeStamp;
		}

		public String getMigrationStatus() {
			return migrationStatus;
		}

		public void setMigrationStatus(String migrationStatus) {
			this.migrationStatus = migrationStatus;
		}

		public BigDecimal getRunningBalance() {
			return runningBalance;
		}

		public void setRunningBalance(BigDecimal runningBalance) {
			this.runningBalance = runningBalance;
		}

		public BigDecimal getShare() {
			return share;
		}

		public void setShare(BigDecimal share) {
			this.share = share;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public List<ParticipantView> getParticipantViewList() {
			return participantViewList;
		}

		public void setParticipantViewList(List<ParticipantView> participantViewList) {
			this.participantViewList = participantViewList;
		}
		
		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((amount == null) ? 0 : amount.hashCode());
			result = prime * result + ((contact == null) ? 0 : contact.hashCode());
			result = prime * result + ((domain == null) ? 0 : domain.hashCode());
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
			result = prime * result + ((migrationStatus == null) ? 0 : migrationStatus.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
			result = prime * result + ((participantViewList == null) ? 0 : participantViewList.hashCode());
			result = prime * result + ((runningBalance == null) ? 0 : runningBalance.hashCode());
			result = prime * result + ((share == null) ? 0 : share.hashCode());
			result = prime * result + ((status == null) ? 0 : status.hashCode());
			result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SplitView other = (SplitView) obj;
			if (amount == null) {
				if (other.amount != null)
					return false;
			} else if (!amount.equals(other.amount))
				return false;
			if (contact == null) {
				if (other.contact != null)
					return false;
			} else if (!contact.equals(other.contact))
				return false;
			if (domain == null) {
				if (other.domain != null)
					return false;
			} else if (!domain.equals(other.domain))
				return false;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equals(other.email))
				return false;
			if (errorCode == null) {
				if (other.errorCode != null)
					return false;
			} else if (!errorCode.equals(other.errorCode))
				return false;
			if (migrationStatus == null) {
				if (other.migrationStatus != null)
					return false;
			} else if (!migrationStatus.equals(other.migrationStatus))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (orderId == null) {
				if (other.orderId != null)
					return false;
			} else if (!orderId.equals(other.orderId))
				return false;
			if (participantViewList == null) {
				if (other.participantViewList != null)
					return false;
			} else if (!participantViewList.equals(other.participantViewList))
				return false;
			if (runningBalance == null) {
				if (other.runningBalance != null)
					return false;
			} else if (!runningBalance.equals(other.runningBalance))
				return false;
			if (share == null) {
				if (other.share != null)
					return false;
			} else if (!share.equals(other.share))
				return false;
			if (status == null) {
				if (other.status != null)
					return false;
			} else if (!status.equals(other.status))
				return false;
			if (timeStamp == null) {
				if (other.timeStamp != null)
					return false;
			} else if (!timeStamp.equals(other.timeStamp))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "SplitView [name=" + name + ", email=" + email + ", contact=" + contact + ", orderId=" + orderId
					+ ", timeStamp=" + timeStamp + ", migrationStatus=" + migrationStatus + ", runningBalance="
					+ runningBalance + ", share=" + share + ", domain=" + domain + ", status=" + status + ", errorCode="
					+ errorCode + ", amount=" + amount + ", participantViewList=" + participantViewList + "]";
		}
}
