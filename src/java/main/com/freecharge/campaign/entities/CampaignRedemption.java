package com.freecharge.campaign.entities;


import java.util.Date;
import java.util.Map;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Created by vishal on 26/11/15.
 */

@Document(collection = "campaignRedemption")
public class CampaignRedemption {
    @Id
    private String            id;
    private Integer           campaignId;
    private String            redemptionKey;
    private String            redemptionValue;
    private String            trigger;
    private Map<String, Date> successOrderIds;
    private Integer           count;
    private Date              createdAt;
    private Date              updatedAt;

    public CampaignRedemption() {
    }

    public CampaignRedemption(Integer campaignId, String redemptionKey, String redemptionValue, String trigger,
            Map<String, Date> successOrderIds) {
        this.campaignId = campaignId;
        this.redemptionKey = redemptionKey;
        this.redemptionValue = redemptionValue;
        this.trigger = trigger;
        this.successOrderIds = successOrderIds;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getRedemptionKey() {
        return redemptionKey;
    }

    public void setRedemptionKey(String redemptionKey) {
        this.redemptionKey = redemptionKey;
    }

    public String getRedemptionValue() {
        return redemptionValue;
    }

    public void setRedemptionValue(String redemptionValue) {
        this.redemptionValue = redemptionValue;
    }

    public Map<String, Date> getSuccessOrderIds() {
        return successOrderIds;
    }

    public void setSuccessOrderIds(Map<String, Date> successOrderIds) {
        this.successOrderIds = successOrderIds;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

