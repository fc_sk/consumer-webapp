package com.freecharge.blacklistdomain.dao;

import java.util.List;
import java.util.Map;

import com.freecharge.blacklistdomain.service.BlacklistDomainService.BlacklistType;

/**
 * Interface for BlacklistDomain DAO
 * This interface is implemented by the BlacklistDomainDBDAO and BlacklistDomainCachingDAO
 * 
 * One should always use the BlacklistDomainCachingDAO for the application.
 * This should happen by default because BlacklistDomainCachingDAO has been annotated as @Primary
 */
public interface IBlacklistDomainDAO {

    public long addBlacklistedDomain(BlacklistType type, String domain);

    public List<String> getBlacklistedDomainByType(BlacklistType type);

    public void deleteBlacklistedDomain(BlacklistType type, String domain);

    public List<Map<String, Object>>  getAllBlacklistedDomain();
}
