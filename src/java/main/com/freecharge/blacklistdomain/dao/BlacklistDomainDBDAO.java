package com.freecharge.blacklistdomain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.freecharge.blacklistdomain.service.BlacklistDomainService.BlacklistType;

@Repository("blacklistDomainDBDAO")
public class BlacklistDomainDBDAO implements IBlacklistDomainDAO {

    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertBlacklistedDomain;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertBlacklistedDomain = new SimpleJdbcInsert(dataSource).withTableName("blacklisted_domain")
                .usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    @Override
    public long addBlacklistedDomain(final BlacklistType type, final String domain) {

        if (getBlacklistedDomainByTypeAndName(type, domain).size() > 0) {
            return 0;
        }

        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("type", type.toString());
        insertParams.put("domain", domain);
        Number primaryKey = insertBlacklistedDomain.executeAndReturnKey(insertParams);
        return primaryKey.longValue();
    }

    @Override
    public List<String> getBlacklistedDomainByType(final BlacklistType type) {
        String sql = "SELECT domain FROM blacklisted_domain WHERE type = :type";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type.toString());
        List<String> rows = jt.queryForList(sql, params, String.class);
        return rows;
    }

    public List<String> getBlacklistedDomainByTypeAndName(final BlacklistType type, final String domain) {
        String sql = "SELECT domain FROM blacklisted_domain WHERE domain = :domain and type = :type";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("domain", domain);
        params.put("type", type.toString());
        List<String> rows = jt.queryForList(sql, params, String.class);
        return rows;
    }

    @Override
    public void deleteBlacklistedDomain(final BlacklistType type, final String domain) {
        String sql = "DELETE FROM blacklisted_domain WHERE domain = :domain and type = :type";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("domain", domain);
        params.put("type", type.toString());
        jt.update(sql, params);
    }

    @Override
    public List<Map<String, Object>> getAllBlacklistedDomain() {

        String sql = "SELECT type, domain FROM blacklisted_domain order by type";
        Map<String, Object> params = new HashMap<String, Object>();
        List<Map<String, Object>> rows = jt.queryForList(sql, params);

        if (rows.size() == 0) {
            return null;
        }
        return rows;
    }
}
