package com.freecharge.blacklistdomain.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.freecharge.blacklistdomain.BlacklistDomainCacheManager;
import com.freecharge.blacklistdomain.service.BlacklistDomainService.BlacklistType;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This class provides a caching layer to the BlacklistDomain DB DAO In the application,
 */
@Primary
@Repository("blacklistDomainCachingDAO")
public class BlacklistDomainCachingDAO implements IBlacklistDomainDAO {

    private Logger              logger = LoggingFactory.getLogger(this.getClass());

    @Autowired
    private BlacklistDomainDBDAO        blacklistDomainDBDAO;

    @Autowired
    private BlacklistDomainCacheManager blacklistDomainCacheManager;

    @Override
    public long addBlacklistedDomain(final BlacklistType type, final String domain) {
        // First save the new BlacklistedDomain in DB
        // If successful,
        // Refresh a key-value "blacklistedDomain_<TYPE_ID>" in cache
        // Else,
        // throw exception
        long id = blacklistDomainDBDAO.addBlacklistedDomain(type, domain);

        try {
            if (id > 0) {
                blacklistDomainCacheManager.deleteBlacklistedDomainByType(type.getId());
                blacklistDomainCacheManager.setBlacklistedDomainByType(type.getId(), 
                        blacklistDomainDBDAO.getBlacklistedDomainByType(type));
            }
        } catch (RuntimeException e) {
            logger.error("error while refreshing blacklistedDomain from cache for type : " + type, e);
        }
        return id;
    }

    @Override
    public List<String> getBlacklistedDomainByType(final BlacklistType type) {
        // Get value of "blacklistedDomain_<TYPE>" from cache
        // If found,
        // return it
        // Else,
        // Get a list of BlacklistedDomain from DB
        // Put these in cache with key "blacklistedDomain_<TYPE_ID>"
        // return the list
        List<String> blacklistDomain = null;
        try {
            blacklistDomain = blacklistDomainCacheManager.getBlacklistedDomainByType(type.getId());
        } catch (RuntimeException e) {
            logger.error("Error getting blacklistedDomain from cache: ", e);
        }

        if (blacklistDomain == null) {
            blacklistDomain = blacklistDomainDBDAO.getBlacklistedDomainByType(type);
            if (blacklistDomain != null) {
                try {
                    blacklistDomainCacheManager.setBlacklistedDomainByType(type.getId(), blacklistDomain);
                } catch (RuntimeException e) {
                    logger.error("Error setting enabled growth events from cache: ", e);
                }
            }
        }
        return blacklistDomain;
    }

    @Override
    public List<Map<String, Object>> getAllBlacklistedDomain() {
        return blacklistDomainDBDAO.getAllBlacklistedDomain();
    }

    @Override
    public void deleteBlacklistedDomain(final BlacklistType type, final String domain) {
        // First delete the BlacklistedDomain name from DB
        // If successful,
        // Delete cached key-value "blacklistedDomain_<TYPE_ID>"
        // Else,
        // throw exception
        blacklistDomainDBDAO.deleteBlacklistedDomain(type, domain);

        try {
            blacklistDomainCacheManager.deleteBlacklistedDomainByType(type.getId());
            blacklistDomainCacheManager.setBlacklistedDomainByType(type.getId(), 
                    blacklistDomainDBDAO.getBlacklistedDomainByType(type));
        } catch (RuntimeException e) {
            logger.error("error while deleting blacklistedDomain from cache for type : " + type, e);
        }
    }
}
