package com.freecharge.blacklistdomain;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.freecharge.blacklistdomain.service.BlacklistDomainService.BlacklistType;
import com.freecharge.common.cache.CacheManager;

/**
 * This class has all the get and set methods for all growth_events cache entries
 */
@Repository("blacklistDomainCacheManager")
public class BlacklistDomainCacheManager extends CacheManager {

    private static String BLACKLISTED_DOMAIN_PREFIX = "blacklistedDomain_";

    public void setBlacklistedDomainByType(final int type, final List<String> domainList) {
        set(BLACKLISTED_DOMAIN_PREFIX + type, domainList);
    }

    public List<String> getBlacklistedDomainByType(final int type) {
        return (List<String>) get(BLACKLISTED_DOMAIN_PREFIX + type);
    }

    public void deleteBlacklistedDomainByType(final int type) {
        delete(BLACKLISTED_DOMAIN_PREFIX + type);
    }

    // --------------- Flushing the cache ---------------
    public void flush() {
        delete(BLACKLISTED_DOMAIN_PREFIX + BlacklistType.SPELLING_MISTAKE.getId());
        delete(BLACKLISTED_DOMAIN_PREFIX + BlacklistType.INCORRECT_DOMAIN.getId());
    }
}
