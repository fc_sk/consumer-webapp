package com.freecharge.blacklistdomain.entity;

import java.util.Date;

/**
 * This table stores list of blacklisted domains along with its BlacklistType [ SPELLING_MISTAKE,  INCORRECT_DOMAIN ].
 */
public class BlacklistDomainEntity {
    private long id;
    private String type;
    private String domain;
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(final String domain) {
        this.domain = domain;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(final Date createdOn) {
        this.createdOn = createdOn;
    }

    public String toString() {
        return "[BlacklistDomainEntity: id : " + id + ", type : " + type + ", domain : " + domain + "]";
    }
}
