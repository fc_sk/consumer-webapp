package com.freecharge.blacklistdomain.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.blacklistdomain.dao.IBlacklistDomainDAO;
import com.freecharge.common.framework.properties.AppConfigService;

@Service
public class BlacklistDomainService {

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private IBlacklistDomainDAO blacklistDomainDao;

    public static enum BlacklistType {
        SPELLING_MISTAKE(1, "Spelling Mistake", AppConfigService.BLACKLIST_WRONG_SPELLED_DOMAIN),
        INCORRECT_DOMAIN(2, "Incorrect Domain", AppConfigService.BLACKLIST_INCORRECT_DOMAIN);

        private String description;
        private String appConfigProperty;
        private Integer id;

        private BlacklistType(final Integer id, final String description, final String appConfigProperty) {
            this.id = id;
            this.description = description;
            this.appConfigProperty = appConfigProperty;
        }

        public Integer getId() {
            return id;
        }

        public String getDescription() {
            return description;
        }

        public String getConfigProperty() {
            return appConfigProperty;
        }

        public static BlacklistType valueById(final Integer id) {
            for (BlacklistType type : BlacklistType.values()) {
                if (type.getId().equals(id)) {
                    return type;
                }
            }
            return null;
        }
    }

    public boolean isIncorrectDomain(final String emailId) {
        return isDomainValid(emailId, AppConfigService.BLACKLISTED_DOMAIN_CONFIG,
                BlacklistType.INCORRECT_DOMAIN);
    }

    public boolean isDomainSpelledWrong(final String emailId) {
        return isDomainValid(emailId, AppConfigService.BLACKLISTED_DOMAIN_CONFIG,
                BlacklistType.SPELLING_MISTAKE);
    }

    public boolean isDomainValid(final String emailId, final String configProperty, final BlacklistType type) {

        if (appConfigService.isChildPropertyEnabled(AppConfigService.BLACKLISTED_DOMAIN_CONFIG,
                type.getConfigProperty())) {
            List<String> blackListedDomainList =  getBlacklistedDomainByType(type);
            String userEmaildomain = emailId.substring(emailId.indexOf("@") + 1).toLowerCase();
            if (blackListedDomainList != null && !blackListedDomainList.isEmpty()
                    && blackListedDomainList.contains(userEmaildomain)) {
                return false;
            }
        }

        return true;
    }

    public long addBlacklistedDomain(final BlacklistType type, final String domain) {
        return blacklistDomainDao.addBlacklistedDomain(type, domain);
    };

    public List<Map<String, Object>> getAllBlacklistedDomain() {
        return blacklistDomainDao.getAllBlacklistedDomain();
    }

    public void deleteBlacklistedDomain(final BlacklistType type, final String domain) {
        blacklistDomainDao.deleteBlacklistedDomain(type, domain);
    }

    public List<String> getBlacklistedDomainByType(final BlacklistType type) {
        return blacklistDomainDao.getBlacklistedDomainByType(type);
    }

}
