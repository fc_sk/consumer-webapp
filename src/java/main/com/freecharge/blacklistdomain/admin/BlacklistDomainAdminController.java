package com.freecharge.blacklistdomain.admin;


import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.blacklistdomain.BlacklistDomainCacheManager;
import com.freecharge.blacklistdomain.entity.BlacklistDomainEntity;
import com.freecharge.blacklistdomain.service.BlacklistDomainService;
import com.freecharge.blacklistdomain.service.BlacklistDomainService.BlacklistType;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/blacklistdomain/*")
public class BlacklistDomainAdminController extends BaseAdminController {

    public static final String BLACKLIST_DOMAIN_HOME_VIEW = "/admin/blacklistdomain/home.htm";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private BlacklistDomainCacheManager blacklistDomainCacheManager;

    @Autowired
    private BlacklistDomainService blacklistDomainService;

    public static final String BLACKLISTED_DOMAIN = "blacklistedDomain";
    public static final String BLACKLIST_TYPE = "blacklistType";

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String home(final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        if (!hasAccess(request, AdminComponent.BLACKLIST_DOMAIN)) {
            return get403HtmlResponse(response);
        }

        populateModel(model);

        return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addDomain(@ModelAttribute("blacklistDomainEntity") @Valid final BlacklistDomainEntity
            blacklistDomainEntity, final HttpServletRequest request, final HttpServletResponse response,
            final Model model) throws IOException {

        if (!hasAccess(request, AdminComponent.BLACKLIST_DOMAIN)) {
            return get403HtmlResponse(response);
        }

        if (FCUtil.isEmpty(blacklistDomainEntity.getDomain()) || FCUtil.isEmpty(blacklistDomainEntity.getType())) {
            logger.error("Invalid Input.  Domain or Blacklist Type is empty");
            model.addAttribute("errors", "Invalid Blacklist Type : " + blacklistDomainEntity.getType()
                    + " or Domain : " + blacklistDomainEntity.getDomain());
            populateModel(model);
            return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
        }

        BlacklistType type = BlacklistType.valueOf(blacklistDomainEntity.getType());

        if (type == null) {
            logger.error("Invalid Blacklisted Domain Type : " + blacklistDomainEntity.getType());
            model.addAttribute("errors", "Invalid Blacklisted Domain Type : " + blacklistDomainEntity.getType());
            populateModel(model);
            return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
        }

        long id = blacklistDomainService
                .addBlacklistedDomain(BlacklistType.valueOf(blacklistDomainEntity.getType()),
                        blacklistDomainEntity.getDomain().toLowerCase());

        if (id <= 0) {
            logger.error("Domain : " + blacklistDomainEntity.getDomain() + " already exists under Type : "
                    + blacklistDomainEntity.getType());
            model.addAttribute("message", "Domain : " + blacklistDomainEntity.getDomain() + " already exists "
                    + "under Type : " + blacklistDomainEntity.getType());
            populateModel(model);
            return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
        }

        model.addAttribute("message", "Domain : " + blacklistDomainEntity.getDomain() + " saved successfully under"
                + " Type : " + blacklistDomainEntity.getType());

        populateModel(model);
        return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteDomain(@ModelAttribute("blacklistDomainEntity") @Valid final BlacklistDomainEntity
            blacklistDomainEntity, final HttpServletRequest request, final HttpServletResponse response,
            final Model model) throws IOException {

        if (!hasAccess(request, AdminComponent.BLACKLIST_DOMAIN)) {
            return get403HtmlResponse(response);
        }

        BlacklistType type = BlacklistType.valueOf(blacklistDomainEntity.getType());

        if (type == null) {
            logger.error("Invalid Blacklisted Domain Type : " + blacklistDomainEntity.getType());
            model.addAttribute("errors", "Invalid Blacklisted Domain Type : " + blacklistDomainEntity.getType());
            populateModel(model);
            return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
        }

        blacklistDomainService.deleteBlacklistedDomain(BlacklistType.valueOf(blacklistDomainEntity.getType()),
                blacklistDomainEntity.getDomain());

        model.addAttribute("message", "Domain : " + blacklistDomainEntity.getDomain() + " deleted successfully "
                + "from Type : " + blacklistDomainEntity.getType());

        populateModel(model);
        return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
    }

    // Flush the cached Blacklisted domain list for all BLACKLIST_TYPEs
    @RequestMapping(value = "flushCache", method = RequestMethod.POST)
    public String flushCache(final Model model, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {

        if (!hasAccess(request, AdminComponent.BLACKLIST_DOMAIN)) {
            return get403HtmlResponse(response);
        }

        blacklistDomainCacheManager.flush();
        model.addAttribute("message", "Cache Flushed successfully.");

        return ViewConstants.BLACKLIST_DOMAIN_ADMIN_HOME_VIEW;
    }

    private void populateModel(final Model model) {
        model.addAttribute(BLACKLIST_TYPE, Arrays.asList(BlacklistType.values()));
        model.addAttribute(BLACKLISTED_DOMAIN, blacklistDomainService.getAllBlacklistedDomain());
    }
}
