package com.freecharge.hcoupon.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;

@Service
public class HCouponFulfilmentService {
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private PricingService pricingService;
	
	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private KestrelWrapper kestrelWrapper;
	
	public void enqueueForfulfilment(String orderId) {
		/*
    	 * Create the user_transaction_history entry and push to queue for fulfilment
    	 */
    	Integer userId = orderService.getUserIdForOrder(orderId);
    	Float amountPaid = pricingService.getPayableAmountByOrderId(orderId).floatValue();
        Map<String, Object> userDetail = new HashMap<>();
        userDetail.put("amount", amountPaid);
        userDetail.put("mobileno", "");
        userDetail.put("productType", FCConstants.PRODUCT_TYPE_HCOUPON);
        userDetail.put("operatorName", "");
        userDetail.put("circleName", "");
        userDetail.put("userId", userId);
        userTransactionHistoryService.createOrUpdateRechargeHistory(orderId, userDetail, RechargeConstants.IN_UNDER_PROCESS_CODE);
        kestrelWrapper.enqueueHCouponForFulfilment(orderId);
	}
}
