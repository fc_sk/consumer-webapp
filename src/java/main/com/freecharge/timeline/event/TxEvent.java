package com.freecharge.timeline.event;

import java.util.Date;
import java.util.Map;

public class TxEvent {
	
	private Date timeStamp;
	private String eventName;
	private String channel;
	private Map<String, String> eventFieldValueMap;
	
	public TxEvent(Date timeStamp, String eventName, String channel, Map<String, String> eventFieldValueMap) {
		super();
		this.timeStamp = timeStamp;
		this.eventName = eventName;
		this.channel = channel;
		this.eventFieldValueMap = eventFieldValueMap;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Map<String, String> getEventFieldValueMap() {
		return eventFieldValueMap;
	}

	public void setEventFieldValueMap(Map<String, String> eventFieldValueMap) {
		this.eventFieldValueMap = eventFieldValueMap;
	}

	@Override
	public String toString() {
		return "TxEvent [timeStamp=" + timeStamp + ", eventName=" + eventName + ", channel=" + channel
				+ ", eventFieldValueMap=" + eventFieldValueMap + "]";
	}
	
	
	
}
