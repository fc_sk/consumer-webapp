package com.freecharge.timeline.event;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;

@Controller
@RequestMapping("admin/timeline")
public class TransactionEventController {
	
	private static Logger logger = LoggingFactory.getLogger(TransactionEventController.class);
	
	@Autowired
    private EventService eventService;
	
	@Autowired
    private UserServiceProxy userServiceProxy;
	
    @RequestMapping(value = "/events", method = RequestMethod.GET, produces = "application/json")
    @NoLogin
    public @ResponseBody TxnEventWrapper getAllTxEvents(@RequestParam(value = "email") String email, 
    		@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate) throws Exception {
    	
    	logger.info("getAllTxEvents for email: " + email + "| fromDate : " + fromDate + "| toDate :" + toDate);
    	
    	try {
			return eventService.getAllTxnEvents(email, fromDate, toDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Exception occured in getAllTxEvents", e);
			throw e;
		}
    }

}
