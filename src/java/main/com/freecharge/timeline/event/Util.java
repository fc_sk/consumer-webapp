package com.freecharge.timeline.event;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;

public class Util {
	
    private static Logger logger = LoggingFactory.getLogger(Util.class);
	
	@SafeVarargs
	public static List<TxEvent> mergeEvents(List<TxEvent>... txnEventLists) {
		List<TxEvent> mergedEventList = new ArrayList<TxEvent>();
		if(txnEventLists == null) 
			return mergedEventList;
		
		List<List<TxEvent>> eventLists = new ArrayList<List<TxEvent>>();
		for(List<TxEvent> txnEventList : txnEventLists) {
			if(txnEventList == null || txnEventList.size() == 0) 
				continue;
			eventLists.add(txnEventList);
		}
		
		logger.debug("MergeEvents: Found " + eventLists.size() + " to merge");
		ArrayList<Integer> indexList = new ArrayList<Integer>();

        for (int i = 0; i < eventLists.size(); ++i)
            indexList.add(0);
		
		while(indexList.size() > 0) {
			
			int maxIndex = findMaxIndex(eventLists, indexList);
			
			mergedEventList.add(eventLists.get(maxIndex).get(indexList.get(maxIndex)));
			indexList.set(maxIndex, indexList.get(maxIndex) + 1 );
			
			if(indexList.get(maxIndex) >= eventLists.get(maxIndex).size()) {
				eventLists.remove(maxIndex);
				indexList.remove(maxIndex);
			}
		}
		
		logger.debug("Txn Events After Merge : " + mergedEventList.size());
		return mergedEventList;
	}
	
	
	private static int findMaxIndex(List<List<TxEvent>> eventLists, ArrayList<Integer> indexList) {
		
		int maxIndex = 0;
		for(int i = 0; i < indexList.size(); ++ i) {
			
			if(eventLists.get(i).get(indexList.get(i)).getTimeStamp().compareTo(eventLists.get(maxIndex).get(indexList.get(maxIndex)).getTimeStamp()) > 0) {
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	public static Date parseDate(String dateInString) throws ParseException {

        if (dateInString == null || dateInString.equals("null") || dateInString.isEmpty())
            throw new ParseException("Date is null", 0);

        Date date;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            logger.debug("Exception while parsing String :" + dateInString + " to Date");
            throw e;
        }

        return date;
    }
	
	public static void main(String [] args) {
		List<TxEvent> list1 = new ArrayList<TxEvent>();
		List<TxEvent> list2 = new ArrayList<TxEvent>();
		List<TxEvent> list3 = new ArrayList<TxEvent>();

		for(int i = 0; i < 3; ++ i) {
			addTxEvent(list1);
			addTxEvent(list2);
			addTxEvent(list3);
		}
	}
	
	public static void addTxEvent(List<TxEvent> list) {
		
		TxEvent txEvent = new TxEvent(new Timestamp(new Date().getTime()), "List1Event", null, new HashMap<String, String>());
		list.add(txEvent);
	}
	
}
