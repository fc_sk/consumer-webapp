package com.freecharge.timeline.event;

import java.util.Comparator;

import com.freecharge.app.domain.entity.jdbc.InRequest;

public class RechargeAttempt extends InRequest {
	
	private String aggrName;
	private String responseCode;
	private String responseMessage;
	
	public String getAggrName() {
		return aggrName;
	}
	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

    @Override
	public String toString() {
		return "RechargeAttempt [aggrName=" + aggrName + ", responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", toString()=" + super.toString() + "]";
	}

	public static class RechargeAttemptCreatedTimeComparator implements Comparator<RechargeAttempt> {
        @Override
        public int compare(RechargeAttempt rechargeAttempt1, RechargeAttempt rechargeAttempt2) {
            return rechargeAttempt2.getCreatedTime().compareTo(rechargeAttempt1.getCreatedTime());
        }

    }
}
