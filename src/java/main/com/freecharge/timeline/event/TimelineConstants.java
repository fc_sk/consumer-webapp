package com.freecharge.timeline.event;

public class TimelineConstants {

    public static String PAYMENT_TXN_EVENT_NAME = "Payment Attempt";
    public static String RECHARGE_TXN_EVENT_NAME = "Recharge Attempt";
    public static String BILL_PAYMENT_TXN_EVENT_NAME = "Bill Payment Attempt";
    public static String NULL_STRING = "null";
  
}
