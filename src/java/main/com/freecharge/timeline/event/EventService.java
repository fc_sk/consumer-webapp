package com.freecharge.timeline.event;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction.PaymentTransactionSentToPGComparator;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.timeline.event.RechargeAttempt.RechargeAttemptCreatedTimeComparator;

public class EventService {

	@Autowired
    private InService inService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private BillPaymentService billPaymentService;
    
    private static Logger logger = LoggingFactory.getLogger(EventService.class);
    
    public TxnEventWrapper getAllTxnEvents(String email, String fromDate, String toDate) throws Exception {
    	
    	List<TxEvent> paymentTxnEvents = getAllPaymentTxnEvents(email, fromDate, toDate);
    	List<TxEvent> rechargeTxnEvents = getAllRechargeTxnEvents(email, fromDate, toDate);
    	List<TxEvent> billPaymentEvents = getAllBillPaymentEvents(email, fromDate, toDate);
    	
    	List<TxEvent> txnEvents = Util.mergeEvents(paymentTxnEvents, rechargeTxnEvents, billPaymentEvents);
    	
    	TxnEventWrapper txnEventWrapper = new TxnEventWrapper();
    	txnEventWrapper.setTxnEventList(txnEvents);
    	
    	return txnEventWrapper;
    }
    
    public List<TxEvent> getAllPaymentTxnEvents(String email, String fromDate, String toDate) {
    	
    	Timestamp fromTimestamp = null;
		Timestamp toTimestamp = null;
		List<PaymentTransaction> txnList = null;
		List<PaymentTransaction> paymentTxnList = new ArrayList<PaymentTransaction>();
		
		try {
			if(fromDate == null || toDate == null || 
					fromDate.equals(TimelineConstants.NULL_STRING) || toDate.equals(TimelineConstants.NULL_STRING)) {
				txnList = paymentTransactionService.getAllPaymentTxSentToPGForEmail(email);
			}
			else {
				fromTimestamp = new Timestamp(Long.valueOf(fromDate).longValue());
				toTimestamp = new Timestamp(Long.valueOf(toDate).longValue());
				
				logger.debug("fromTimestamp: " + fromTimestamp + "|toTimestamp :" + toTimestamp);
				txnList = paymentTransactionService.getAllPaymentTxSentToPGForEmailFromDate(email, fromTimestamp, toTimestamp);		
			}
			paymentTxnList.addAll(txnList);
		} catch (Exception e) {
			logger.error("Exception in PaymentEventsRetrieval", e);
		}
		
		logger.debug("paymentTxnList size for email: " + email + " is " + paymentTxnList.size());
		
//		Collections.sort(paymentTxnList, new PaymentTransactionSentToPGComparator());
 		
		return convertPaymentToEvents(paymentTxnList);		
	}
	
	public List<TxEvent> getAllRechargeTxnEvents(String email, String fromDate, String toDate) {
	   
		Timestamp fromTimestamp = null;
		Timestamp toTimestamp = null;
		List<RechargeAttempt> txnList = null;
		List<RechargeAttempt> rechargeAttemptList = new ArrayList<RechargeAttempt>();

		try {
			if(fromDate == null || toDate == null || 
					fromDate.equals(TimelineConstants.NULL_STRING) || toDate.equals(TimelineConstants.NULL_STRING)) {
				txnList = inService.getAllRechargesForEmail(email);
			}
			else {
				fromTimestamp = new Timestamp(Long.valueOf(fromDate).longValue());
				toTimestamp = new Timestamp(Long.valueOf(toDate).longValue());
				
				logger.debug("fromTimestamp: " + fromTimestamp + "|toTimestamp :" + toTimestamp);
				txnList = inService.getAllRechargesForEmailFromDate(email, fromTimestamp, toTimestamp);		
			}
			rechargeAttemptList.addAll(txnList);
		} catch (Exception e) {
			logger.error("Exception in RechargeEventsRetrieval", e);
		}
		
		
		logger.debug("rechargeAttemptList size for email: " + email + " is " + rechargeAttemptList.size());
//		Collections.sort(rechargeAttemptList, new RechargeAttemptCreatedTimeComparator());
		
		return convertRechargesToEvents(rechargeAttemptList);
	}
	
	public List<TxEvent> getAllBillPaymentEvents(String email, String fromDate, String toDate) {
		   
		Timestamp fromTimestamp = null;
		Timestamp toTimestamp = null;
		List<BillPaymentStatus> txnList = null;
		List<BillPaymentStatus> billPaymentList = new ArrayList<BillPaymentStatus>();

		try {
			if(fromDate == null || toDate == null || 
					fromDate.equals(TimelineConstants.NULL_STRING) || toDate.equals(TimelineConstants.NULL_STRING)) {
				txnList = billPaymentService.getAllBillPaymentsForEmail(email);
			}
			else {
				fromTimestamp = new Timestamp(Long.valueOf(fromDate).longValue());
				toTimestamp = new Timestamp(Long.valueOf(toDate).longValue());
				
				logger.debug("fromTimestamp: " + fromTimestamp + "|toTimestamp :" + toTimestamp);
				txnList = billPaymentService.getAllBillPaymentsForEmailAndDate(email, fromTimestamp, toTimestamp);		
			}
			billPaymentList.addAll(txnList);
		} catch (Exception e) {
			logger.error("Exception in Bill Payments Retrieval", e);
		}
		
		
		logger.debug("Bill Payment List size for email: " + email + " is " + billPaymentList.size());
		
		return convertBillPaymentsToEvents(billPaymentList);
	}
	
	public static List<TxEvent> convertPaymentToEvents(List<PaymentTransaction> paymentTxnList) {
			
		ArrayList<TxEvent> txnEventList = new ArrayList<TxEvent>();
		
		for(PaymentTransaction txn : paymentTxnList) {
			TxEvent txnEvent = new TxEvent(txn.getSetToPg(), TimelineConstants.PAYMENT_TXN_EVENT_NAME, null, new HashMap<String, String>());
			
			Map<String, String> eventFieldValueMap = txnEvent.getEventFieldValueMap();
			
			eventFieldValueMap.put("orderId", txn.getOrderId());
			eventFieldValueMap.put("mtxnId", txn.getMerchantTxnId());
			eventFieldValueMap.put("payTxnId", txn.getPaymentTxnId() == null ? null : txn.getPaymentTxnId().toString());
			eventFieldValueMap.put("amount", txn.getAmount() == null ? null : txn.getAmount().toString());
			eventFieldValueMap.put("pg", txn.getPaymentGateway());
			eventFieldValueMap.put("paymentOption", txn.getPaymentOption());
			eventFieldValueMap.put("sentToPg", txn.getSetToPg() == null ? null : String.valueOf(txn.getSetToPg().getTime()));
			eventFieldValueMap.put("receivedFromPg", txn.getRecievedToPg() == null ? null : String.valueOf(txn.getRecievedToPg().getTime()));
			eventFieldValueMap.put("successStatus", txn.getIsSuccessful() == null ? null : txn.getIsSuccessful().toString());
			
			txnEventList.add(txnEvent);
		}
		
		return txnEventList;
	}
	
	public static List<TxEvent> convertRechargesToEvents(List<RechargeAttempt> rechargeAttemptList) {
		
		ArrayList<TxEvent> txnEventList = new ArrayList<TxEvent>();
		
		for(RechargeAttempt attempt : rechargeAttemptList) {
			TxEvent txnEvent = new TxEvent(attempt.getCreatedTime(), TimelineConstants.RECHARGE_TXN_EVENT_NAME, null, new HashMap<String, String>());
			
			Map<String, String> eventFieldValueMap = txnEvent.getEventFieldValueMap();
			eventFieldValueMap.put("request_id", attempt.getRequestId() == null ? null : attempt.getRequestId().toString());
			eventFieldValueMap.put("orderId", attempt.getAffiliateTransId());
			eventFieldValueMap.put("productName", FCConstants.productTypeToNameMap.get(attempt.getProductType()));
			eventFieldValueMap.put("operator", attempt.getOperator());
			eventFieldValueMap.put("aggregator", attempt.getAggrName());
			eventFieldValueMap.put("circle", attempt.getCircle());
			eventFieldValueMap.put("amount", attempt.getAmount() == null ? null : attempt.getAmount().toString());
			eventFieldValueMap.put("recharge_plan", attempt.getRechargePlan());
			eventFieldValueMap.put("created_time", attempt.getCreatedTime() == null ? null : String.valueOf(attempt.getCreatedTime().getTime()));
			eventFieldValueMap.put("aggregator", attempt.getAggrName());
			eventFieldValueMap.put("response_code", attempt.getResponseCode());
			eventFieldValueMap.put("response_msg", attempt.getResponseMessage());
			txnEventList.add(txnEvent);
		}
		
		return txnEventList;
	}
	
	public static List<TxEvent> convertBillPaymentsToEvents(List<BillPaymentStatus> billPayments) {
		
		ArrayList<TxEvent> txnEventList = new ArrayList<TxEvent>();
		
		for(BillPaymentStatus txn : billPayments) {
			TxEvent txnEvent = new TxEvent(txn.getCreatedAt(), TimelineConstants.BILL_PAYMENT_TXN_EVENT_NAME, null, new HashMap<String, String>());
			
			Map<String, String> eventFieldValueMap = txnEvent.getEventFieldValueMap();
			
			eventFieldValueMap.put("OrderID", txn.getAffiliateTransID());
			eventFieldValueMap.put("BillPaymentGateway", txn.getBillPaymentGateway());
			eventFieldValueMap.put("Operator", txn.getOperatorName());
			eventFieldValueMap.put("Amount", txn.getAmount() == null ? null : txn.getAmount().toString());
			eventFieldValueMap.put("PostPaid Account", txn.getPostpaidAccountNumber());
			eventFieldValueMap.put("PostPaid Number", txn.getPostpaidNumber());
			eventFieldValueMap.put("Product Type", txn.getProductType());
			eventFieldValueMap.put("Circle", txn.getCircleName());
			eventFieldValueMap.put("successStatus", txn.getSuccess() == null ? null : txn.getSuccess().toString());
			
			txnEventList.add(txnEvent);
		}
		
		return txnEventList;
	}

	
}
