package com.freecharge.timeline.event;

import java.util.List;

public class TxnEventWrapper {
	
	public List<TxEvent> txnEventList;

	public List<TxEvent> getTxnEventList() {
		return txnEventList;
	}

	public void setTxnEventList(List<TxEvent> txnEventList) {
		this.txnEventList = txnEventList;
	}
	
}
