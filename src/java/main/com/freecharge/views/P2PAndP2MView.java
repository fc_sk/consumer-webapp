package com.freecharge.views;

import java.math.BigDecimal;
import java.util.Date;

public class P2PAndP2MView implements Comparable<P2PAndP2MView>{
	//customer info
	private String name;
	private String email;
	private String contact;
	private String orderId;
	private Date timeStamp;
	private String migrationStatus;
	private BigDecimal runningBalance;
	private String domain;
	private String referenceId;
	
	//txn info
	private String status;
	private String errorCode;
	private BigDecimal txnAmount;
	private String txnType;
	private String requestType;
	private String idempotencyId;
	private Date startedAt;
	private Date completedAt;
	private String otherPartyType;
	private String paytag;
	private String otherPartyName;
	private String otherPartyEmail;
	private String otherPartyContact;
	private String otherPartyMigrationStatus;
	private BigDecimal otherPartyWalletBalance;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getMigrationStatus() {
		return migrationStatus;
	}
	public void setMigrationStatus(String migrationStatus) {
		this.migrationStatus = migrationStatus;
	}
	public BigDecimal getRunningBalance() {
		return runningBalance;
	}
	public void setRunningBalance(BigDecimal runningBalance) {
		this.runningBalance = runningBalance;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public BigDecimal getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getIdempotencyId() {
		return idempotencyId;
	}
	public void setIdempotencyId(String idempotencyId) {
		this.idempotencyId = idempotencyId;
	}
	public Date getStartedAt() {
		return startedAt;
	}
	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}
	public Date getCompletedAt() {
		return completedAt;
	}
	public void setCompletedAt(Date completedAt) {
		this.completedAt = completedAt;
	}
	public String getOtherPartyType() {
		return otherPartyType;
	}
	public void setOtherPartyType(String otherPartyType) {
		this.otherPartyType = otherPartyType;
	}
	public String getPaytag() {
		return paytag;
	}
	public void setPaytag(String paytag) {
		this.paytag = paytag;
	}
	public String getOtherPartyName() {
		return otherPartyName;
	}
	public void setOtherPartyName(String otherPartyName) {
		this.otherPartyName = otherPartyName;
	}
	public String getOtherPartyEmail() {
		return otherPartyEmail;
	}
	public void setOtherPartyEmail(String otherPartyEmail) {
		this.otherPartyEmail = otherPartyEmail;
	}
	public String getOtherPartyContact() {
		return otherPartyContact;
	}
	public void setOtherPartyContact(String otherPartyContact) {
		this.otherPartyContact = otherPartyContact;
	}
	public String getOtherPartyMigrationStatus() {
		return otherPartyMigrationStatus;
	}
	public void setOtherPartyMigrationStatus(String otherPartyMigrationStatus) {
		this.otherPartyMigrationStatus = otherPartyMigrationStatus;
	}
	public BigDecimal getOtherPartyWalletBalance() {
		return otherPartyWalletBalance;
	}
	public void setOtherPartyWalletBalance(BigDecimal otherPartyWalletBalance) {
		this.otherPartyWalletBalance = otherPartyWalletBalance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((completedAt == null) ? 0 : completedAt.hashCode());
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime * result + ((idempotencyId == null) ? 0 : idempotencyId.hashCode());
		result = prime * result + ((migrationStatus == null) ? 0 : migrationStatus.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((otherPartyContact == null) ? 0 : otherPartyContact.hashCode());
		result = prime * result + ((otherPartyEmail == null) ? 0 : otherPartyEmail.hashCode());
		result = prime * result + ((otherPartyMigrationStatus == null) ? 0 : otherPartyMigrationStatus.hashCode());
		result = prime * result + ((otherPartyName == null) ? 0 : otherPartyName.hashCode());
		result = prime * result + ((otherPartyType == null) ? 0 : otherPartyType.hashCode());
		result = prime * result + ((otherPartyWalletBalance == null) ? 0 : otherPartyWalletBalance.hashCode());
		result = prime * result + ((paytag == null) ? 0 : paytag.hashCode());
		result = prime * result + ((referenceId == null) ? 0 : referenceId.hashCode());
		result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
		result = prime * result + ((runningBalance == null) ? 0 : runningBalance.hashCode());
		result = prime * result + ((startedAt == null) ? 0 : startedAt.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result + ((txnAmount == null) ? 0 : txnAmount.hashCode());
		result = prime * result + ((txnType == null) ? 0 : txnType.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		P2PAndP2MView other = (P2PAndP2MView) obj;
		if (completedAt == null) {
			if (other.completedAt != null)
				return false;
		} else if (!completedAt.equals(other.completedAt))
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (idempotencyId == null) {
			if (other.idempotencyId != null)
				return false;
		} else if (!idempotencyId.equals(other.idempotencyId))
			return false;
		if (migrationStatus == null) {
			if (other.migrationStatus != null)
				return false;
		} else if (!migrationStatus.equals(other.migrationStatus))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (otherPartyContact == null) {
			if (other.otherPartyContact != null)
				return false;
		} else if (!otherPartyContact.equals(other.otherPartyContact))
			return false;
		if (otherPartyEmail == null) {
			if (other.otherPartyEmail != null)
				return false;
		} else if (!otherPartyEmail.equals(other.otherPartyEmail))
			return false;
		if (otherPartyMigrationStatus == null) {
			if (other.otherPartyMigrationStatus != null)
				return false;
		} else if (!otherPartyMigrationStatus.equals(other.otherPartyMigrationStatus))
			return false;
		if (otherPartyName == null) {
			if (other.otherPartyName != null)
				return false;
		} else if (!otherPartyName.equals(other.otherPartyName))
			return false;
		if (otherPartyType == null) {
			if (other.otherPartyType != null)
				return false;
		} else if (!otherPartyType.equals(other.otherPartyType))
			return false;
		if (otherPartyWalletBalance == null) {
			if (other.otherPartyWalletBalance != null)
				return false;
		} else if (!otherPartyWalletBalance.equals(other.otherPartyWalletBalance))
			return false;
		if (paytag == null) {
			if (other.paytag != null)
				return false;
		} else if (!paytag.equals(other.paytag))
			return false;
		if (referenceId == null) {
			if (other.referenceId != null)
				return false;
		} else if (!referenceId.equals(other.referenceId))
			return false;
		if (requestType == null) {
			if (other.requestType != null)
				return false;
		} else if (!requestType.equals(other.requestType))
			return false;
		if (runningBalance == null) {
			if (other.runningBalance != null)
				return false;
		} else if (!runningBalance.equals(other.runningBalance))
			return false;
		if (startedAt == null) {
			if (other.startedAt != null)
				return false;
		} else if (!startedAt.equals(other.startedAt))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (txnAmount == null) {
			if (other.txnAmount != null)
				return false;
		} else if (!txnAmount.equals(other.txnAmount))
			return false;
		if (txnType == null) {
			if (other.txnType != null)
				return false;
		} else if (!txnType.equals(other.txnType))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "P2PAndP2MView [name=" + name + ", email=" + email + ", contact=" + contact + ", orderId=" + orderId
				+ ", timeStamp=" + timeStamp + ", migrationStatus=" + migrationStatus + ", runningBalance="
				+ runningBalance + ", domain=" + domain + ", referenceId=" + referenceId + ", status=" + status
				+ ", errorCode=" + errorCode + ", txnAmount=" + txnAmount + ", txnType=" + txnType + ", requestType="
				+ requestType + ", idempotencyId=" + idempotencyId + ", startedAt=" + startedAt + ", completedAt="
				+ completedAt + ", otherPartyType=" + otherPartyType + ", paytag=" + paytag + ", otherPartyName="
				+ otherPartyName + ", otherPartyEmail=" + otherPartyEmail + ", otherPartyContact=" + otherPartyContact
				+ ", otherPartyMigrationStatus=" + otherPartyMigrationStatus + ", otherPartyWalletBalance="
				+ otherPartyWalletBalance + "]";
	}
	
	@Override
	public int compareTo(P2PAndP2MView o) {
		// TODO Auto-generated method stub
		return (this.orderId).compareTo(o.orderId);
	}
	
	
}
