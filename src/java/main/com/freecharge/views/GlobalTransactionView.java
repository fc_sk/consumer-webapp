package com.freecharge.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import com.snapdeal.payments.tsm.response.GetTxnByTxnIdResponse;
import com.snapdeal.payments.tsm.response.GetTxnByTxnIdResponseV2;
import com.snapdeal.payments.tsm.response.LocalTxnResponse;

public class GlobalTransactionView {
	private List<ArrayList<TransactionSummary>> localTxnList;
	private List<LocalTxnResponse> localTxnListPG;
	private GetTxnByTxnIdResponseV2 globalTxnResponse;
	private Map<String, Object> globalTxnMetadataMap;
	private String merchantName;

	public List<ArrayList<TransactionSummary>> getLocalTxnList() {
		return localTxnList;
	}

	public void setLocalTxnList(List<ArrayList<TransactionSummary>> localTxnList) {
		this.localTxnList = localTxnList;
	}

	
	public List<LocalTxnResponse> getLocalTxnListPG() {
		return localTxnListPG;
	}

	public void setLocalTxnListPG(List<LocalTxnResponse> localTxnListPG) {
		this.localTxnListPG = localTxnListPG;
	}

	public GetTxnByTxnIdResponseV2 getGlobalTxnResponse() {
		return globalTxnResponse;
	}

	public void setGlobalTxnResponse(GetTxnByTxnIdResponseV2 globalTxnResponse) {
		this.globalTxnResponse = globalTxnResponse;
	}

	public Map<String, Object> getGlobalTxnMetadataMap() {
		return globalTxnMetadataMap;
	}

	public void setGlobalTxnMetadataMap(Map<String, Object> globalTxnMetadataMap) {
		this.globalTxnMetadataMap = globalTxnMetadataMap;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	

	

}
