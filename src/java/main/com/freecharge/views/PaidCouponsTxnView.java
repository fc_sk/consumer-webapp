package com.freecharge.views;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class PaidCouponsTxnView {
	
	private Map<String, Object> globalTxnMetadataMap;
	private List<Map<String, Object>> localPGWalletTxns;
	private List<Map<String, Object>> localCouponsTxns;
	private Date createdDate;
	
	public Map<String, Object> getGlobalTxnMetadataMap() {
		return globalTxnMetadataMap;
	}
	
	public void setGlobalTxnMetadataMap(Map<String, Object> globalTxnMetadataMap) {
		this.globalTxnMetadataMap = globalTxnMetadataMap;
	}
	
	public List<Map<String, Object>> getLocalPGWalletTxns() {
		return localPGWalletTxns;
	}
	
	public void setLocalPGWalletTxns(List<Map<String, Object>> localPGWalletTxns) {
		this.localPGWalletTxns = localPGWalletTxns;
	}
	
	public List<Map<String, Object>> getLocalCouponsTxns() {
		return localCouponsTxns;
	}
	
	public void setLocalCouponsTxns(List<Map<String, Object>> localCouponsTxns) {
		this.localCouponsTxns = localCouponsTxns;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
