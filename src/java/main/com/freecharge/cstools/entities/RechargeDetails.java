package com.freecharge.cstools.entities;

public class RechargeDetails {
	
	private boolean isForcedRefund;
	private boolean isReverselCase;
	private String status;
	private String productName;
	private Long requestId;
	private Float amount;
	private String mobile;
	private String operator;
	private String operatorTxnId;
	private String aggregator;
	private String aggregatorTxnId;
	private String circle;
	private String plan;
	public boolean isForcedRefund() {
		return isForcedRefund;
	}
	public void setForcedRefund(boolean isForcedRefund) {
		this.isForcedRefund = isForcedRefund;
	}
	public boolean isReverselCase() {
		return isReverselCase;
	}
	public void setReverselCase(boolean isReverselCase) {
		this.isReverselCase = isReverselCase;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getRequestId() {
		return requestId;
	}
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperatorTxnId() {
		return operatorTxnId;
	}
	public void setOperatorTxnId(String operatorTxnId) {
		this.operatorTxnId = operatorTxnId;
	}
	public String getAggregator() {
		return aggregator;
	}
	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}
	public String getAggregatorTxnId() {
		return aggregatorTxnId;
	}
	public void setAggregatorTxnId(String aggregatorTxnId) {
		this.aggregatorTxnId = aggregatorTxnId;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggregator == null) ? 0 : aggregator.hashCode());
		result = prime * result + ((aggregatorTxnId == null) ? 0 : aggregatorTxnId.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((circle == null) ? 0 : circle.hashCode());
		result = prime * result + (isForcedRefund ? 1231 : 1237);
		result = prime * result + (isReverselCase ? 1231 : 1237);
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		result = prime * result + ((operatorTxnId == null) ? 0 : operatorTxnId.hashCode());
		result = prime * result + ((plan == null) ? 0 : plan.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((requestId == null) ? 0 : requestId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RechargeDetails other = (RechargeDetails) obj;
		if (aggregator == null) {
			if (other.aggregator != null)
				return false;
		} else if (!aggregator.equals(other.aggregator))
			return false;
		if (aggregatorTxnId == null) {
			if (other.aggregatorTxnId != null)
				return false;
		} else if (!aggregatorTxnId.equals(other.aggregatorTxnId))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (circle == null) {
			if (other.circle != null)
				return false;
		} else if (!circle.equals(other.circle))
			return false;
		if (isForcedRefund != other.isForcedRefund)
			return false;
		if (isReverselCase != other.isReverselCase)
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		if (operatorTxnId == null) {
			if (other.operatorTxnId != null)
				return false;
		} else if (!operatorTxnId.equals(other.operatorTxnId))
			return false;
		if (plan == null) {
			if (other.plan != null)
				return false;
		} else if (!plan.equals(other.plan))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (requestId == null) {
			if (other.requestId != null)
				return false;
		} else if (!requestId.equals(other.requestId))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RechargeDetails [isForcedRefund=" + isForcedRefund + ", isReverselCase=" + isReverselCase + ", status="
				+ status + ", productName=" + productName + ", requestId=" + requestId + ", amount=" + amount
				+ ", mobile=" + mobile + ", operator=" + operator + ", operatorTxnId=" + operatorTxnId + ", aggregator="
				+ aggregator + ", aggregatorTxnId=" + aggregatorTxnId + ", circle=" + circle + ", plan=" + plan + "]";
	}
}
