package com.freecharge.cstools.entities;

import java.util.List;

public class RefundDetailsWrapper {
	
	private List<RefundDetails> refundDetailsList;
	private String status;
	private String message;
	
	public List<RefundDetails> getRefundDetailsList() {
		return refundDetailsList;
	}
	public void setRefundDetailsList(List<RefundDetails> refundDetailsList) {
		this.refundDetailsList = refundDetailsList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((refundDetailsList == null) ? 0 : refundDetailsList.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefundDetailsWrapper other = (RefundDetailsWrapper) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (refundDetailsList == null) {
			if (other.refundDetailsList != null)
				return false;
		} else if (!refundDetailsList.equals(other.refundDetailsList))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RefundDetailsWrapper [refundDetailsList=" + refundDetailsList + ", status=" + status + ", message="
				+ message + "]";
	}
}
