package com.freecharge.cstools.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class PaymentTransaction {

	private Integer paymentTxnId;
	private String transactionId;
	private String orderId;
	private Date setToPg;
	private Date recievedToPg;
	private Double amount;
	private String merchantTxnId;
	private Boolean isSuccessful;
	private String paymentGateway;
	private String paymentMode;
	private String pgMessage;
	private Integer paymentRequestId;
	private Integer paymentResponseId;
	private Integer paymentType;
	private String paymentOption;
	private Date pgTransactionTime;
	private Set<String> whereFields = new HashSet<String>();

	public Integer getPaymentTxnId() {
		return paymentTxnId;
	}

	public void setPaymentTxnId(Integer paymentTxnId) {
		this.paymentTxnId = paymentTxnId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getSetToPg() {
		return setToPg;
	}

	public void setSetToPg(Date setToPg) {
		this.setToPg = setToPg;
	}

	public Date getRecievedToPg() {
		return recievedToPg;
	}

	public void setRecievedToPg(Date recievedToPg) {
		this.recievedToPg = recievedToPg;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPgMessage() {
		return pgMessage;
	}

	public void setPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
	}

	public Integer getPaymentRequestId() {
		return paymentRequestId;
	}

	public void setPaymentRequestId(Integer paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	public Integer getPaymentResponseId() {
		return paymentResponseId;
	}

	public void setPaymentResponseId(Integer paymentResponseId) {
		this.paymentResponseId = paymentResponseId;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public Date getPgTransactionTime() {
		return pgTransactionTime;
	}

	public void setPgTransactionTime(Date pgTransactionTime) {
		this.pgTransactionTime = pgTransactionTime;
	}

	public Set<String> getWhereFields() {
		return whereFields;
	}

	public void setWhereFields(Set<String> whereFields) {
		this.whereFields = whereFields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((isSuccessful == null) ? 0 : isSuccessful.hashCode());
		result = prime * result + ((merchantTxnId == null) ? 0 : merchantTxnId.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((paymentGateway == null) ? 0 : paymentGateway.hashCode());
		result = prime * result + ((paymentMode == null) ? 0 : paymentMode.hashCode());
		result = prime * result + ((paymentOption == null) ? 0 : paymentOption.hashCode());
		result = prime * result + ((paymentRequestId == null) ? 0 : paymentRequestId.hashCode());
		result = prime * result + ((paymentResponseId == null) ? 0 : paymentResponseId.hashCode());
		result = prime * result + ((paymentTxnId == null) ? 0 : paymentTxnId.hashCode());
		result = prime * result + ((paymentType == null) ? 0 : paymentType.hashCode());
		result = prime * result + ((pgMessage == null) ? 0 : pgMessage.hashCode());
		result = prime * result + ((pgTransactionTime == null) ? 0 : pgTransactionTime.hashCode());
		result = prime * result + ((recievedToPg == null) ? 0 : recievedToPg.hashCode());
		result = prime * result + ((setToPg == null) ? 0 : setToPg.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		result = prime * result + ((whereFields == null) ? 0 : whereFields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentTransaction other = (PaymentTransaction) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (isSuccessful == null) {
			if (other.isSuccessful != null)
				return false;
		} else if (!isSuccessful.equals(other.isSuccessful))
			return false;
		if (merchantTxnId == null) {
			if (other.merchantTxnId != null)
				return false;
		} else if (!merchantTxnId.equals(other.merchantTxnId))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (paymentGateway == null) {
			if (other.paymentGateway != null)
				return false;
		} else if (!paymentGateway.equals(other.paymentGateway))
			return false;
		if (paymentMode == null) {
			if (other.paymentMode != null)
				return false;
		} else if (!paymentMode.equals(other.paymentMode))
			return false;
		if (paymentOption == null) {
			if (other.paymentOption != null)
				return false;
		} else if (!paymentOption.equals(other.paymentOption))
			return false;
		if (paymentRequestId == null) {
			if (other.paymentRequestId != null)
				return false;
		} else if (!paymentRequestId.equals(other.paymentRequestId))
			return false;
		if (paymentResponseId == null) {
			if (other.paymentResponseId != null)
				return false;
		} else if (!paymentResponseId.equals(other.paymentResponseId))
			return false;
		if (paymentTxnId == null) {
			if (other.paymentTxnId != null)
				return false;
		} else if (!paymentTxnId.equals(other.paymentTxnId))
			return false;
		if (paymentType == null) {
			if (other.paymentType != null)
				return false;
		} else if (!paymentType.equals(other.paymentType))
			return false;
		if (pgMessage == null) {
			if (other.pgMessage != null)
				return false;
		} else if (!pgMessage.equals(other.pgMessage))
			return false;
		if (pgTransactionTime == null) {
			if (other.pgTransactionTime != null)
				return false;
		} else if (!pgTransactionTime.equals(other.pgTransactionTime))
			return false;
		if (recievedToPg == null) {
			if (other.recievedToPg != null)
				return false;
		} else if (!recievedToPg.equals(other.recievedToPg))
			return false;
		if (setToPg == null) {
			if (other.setToPg != null)
				return false;
		} else if (!setToPg.equals(other.setToPg))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		if (whereFields == null) {
			if (other.whereFields != null)
				return false;
		} else if (!whereFields.equals(other.whereFields))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentTransaction [paymentTxnId=" + paymentTxnId + ", transactionId=" + transactionId + ", orderId="
				+ orderId + ", setToPg=" + setToPg + ", recievedToPg=" + recievedToPg + ", amount=" + amount
				+ ", merchantTxnId=" + merchantTxnId + ", isSuccessful=" + isSuccessful + ", paymentGateway="
				+ paymentGateway + ", paymentMode=" + paymentMode + ", pgMessage=" + pgMessage + ", paymentRequestId="
				+ paymentRequestId + ", paymentResponseId=" + paymentResponseId + ", paymentType=" + paymentType
				+ ", paymentOption=" + paymentOption + ", pgTransactionTime=" + pgTransactionTime + ", whereFields="
				+ whereFields + "]";
	}
}