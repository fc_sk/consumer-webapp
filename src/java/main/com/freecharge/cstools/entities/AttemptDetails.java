package com.freecharge.cstools.entities;

import java.sql.Timestamp;

public class AttemptDetails {
	private Long requestId;
	private String productType;
	private String aggregatorName;
	private String aggregatorTxnId;
	private String operatorTxnId;
	private String status;
	private String aggregatorResponseCode;
	private String aggregatorResponseMessage;
	private Timestamp requestTime;
	private Timestamp responseTime;
	
	public Long getRequestId() {
		return requestId;
	}
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getAggregatorName() {
		return aggregatorName;
	}
	public void setAggregatorName(String aggregatorName) {
		this.aggregatorName = aggregatorName;
	}
	public String getAggregatorTxnId() {
		return aggregatorTxnId;
	}
	public void setAggregatorTxnId(String aggregatorTxnId) {
		this.aggregatorTxnId = aggregatorTxnId;
	}
	public String getOperatorTxnId() {
		return operatorTxnId;
	}
	public void setOperatorTxnId(String operatorTxnId) {
		this.operatorTxnId = operatorTxnId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAggregatorResponseCode() {
		return aggregatorResponseCode;
	}
	public void setAggregatorResponseCode(String aggregatorResponseCode) {
		this.aggregatorResponseCode = aggregatorResponseCode;
	}
	public String getAggregatorResponseMessage() {
		return aggregatorResponseMessage;
	}
	public void setAggregatorResponseMessage(String aggregatorResponseMessage) {
		this.aggregatorResponseMessage = aggregatorResponseMessage;
	}
	public Timestamp getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}
	public Timestamp getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aggregatorName == null) ? 0 : aggregatorName.hashCode());
		result = prime * result + ((aggregatorResponseCode == null) ? 0 : aggregatorResponseCode.hashCode());
		result = prime * result + ((aggregatorResponseMessage == null) ? 0 : aggregatorResponseMessage.hashCode());
		result = prime * result + ((aggregatorTxnId == null) ? 0 : aggregatorTxnId.hashCode());
		result = prime * result + ((operatorTxnId == null) ? 0 : operatorTxnId.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + ((requestId == null) ? 0 : requestId.hashCode());
		result = prime * result + ((requestTime == null) ? 0 : requestTime.hashCode());
		result = prime * result + ((responseTime == null) ? 0 : responseTime.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttemptDetails other = (AttemptDetails) obj;
		if (aggregatorName == null) {
			if (other.aggregatorName != null)
				return false;
		} else if (!aggregatorName.equals(other.aggregatorName))
			return false;
		if (aggregatorResponseCode == null) {
			if (other.aggregatorResponseCode != null)
				return false;
		} else if (!aggregatorResponseCode.equals(other.aggregatorResponseCode))
			return false;
		if (aggregatorResponseMessage == null) {
			if (other.aggregatorResponseMessage != null)
				return false;
		} else if (!aggregatorResponseMessage.equals(other.aggregatorResponseMessage))
			return false;
		if (aggregatorTxnId == null) {
			if (other.aggregatorTxnId != null)
				return false;
		} else if (!aggregatorTxnId.equals(other.aggregatorTxnId))
			return false;
		if (operatorTxnId == null) {
			if (other.operatorTxnId != null)
				return false;
		} else if (!operatorTxnId.equals(other.operatorTxnId))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		if (requestId == null) {
			if (other.requestId != null)
				return false;
		} else if (!requestId.equals(other.requestId))
			return false;
		if (requestTime == null) {
			if (other.requestTime != null)
				return false;
		} else if (!requestTime.equals(other.requestTime))
			return false;
		if (responseTime == null) {
			if (other.responseTime != null)
				return false;
		} else if (!responseTime.equals(other.responseTime))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AttemptDetails [requestId=" + requestId + ", productType=" + productType + ", aggregatorName="
				+ aggregatorName + ", aggregatorTxnId=" + aggregatorTxnId + ", operatorTxnId=" + operatorTxnId
				+ ", status=" + status + ", aggregatorResponseCode=" + aggregatorResponseCode
				+ ", aggregatorResponseMessage=" + aggregatorResponseMessage + ", requestTime=" + requestTime
				+ ", responseTime=" + responseTime + "]";
	}
}
