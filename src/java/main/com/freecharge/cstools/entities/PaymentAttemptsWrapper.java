package com.freecharge.cstools.entities;

import java.util.List;

public class PaymentAttemptsWrapper {
	
	private List<PaymentTransaction> paymentAttempts;
	private String status;
	private String message;
	
	public List<PaymentTransaction> getPaymentAttempts() {
		return paymentAttempts;
	}
	public void setPaymentAttempts(List<PaymentTransaction> paymentAttempts) {
		this.paymentAttempts = paymentAttempts;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((paymentAttempts == null) ? 0 : paymentAttempts.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentAttemptsWrapper other = (PaymentAttemptsWrapper) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (paymentAttempts == null) {
			if (other.paymentAttempts != null)
				return false;
		} else if (!paymentAttempts.equals(other.paymentAttempts))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "PaymentAttemptsWrapper [paymentAttempts=" + paymentAttempts + ", status=" + status + ", message="
				+ message + "]";
	}
}
