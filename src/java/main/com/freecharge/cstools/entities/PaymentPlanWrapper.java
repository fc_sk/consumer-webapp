package com.freecharge.cstools.entities;

import java.math.BigDecimal;

public class PaymentPlanWrapper {
	
	private BigDecimal pgAmount;
	private BigDecimal walletAmount;
	private String pgName;
	private String paymentName;
	private String paymentOption;
	private String status;
	private String message;
	
	public BigDecimal getPgAmount() {
		return pgAmount;
	}
	public void setPgAmount(BigDecimal pgAmount) {
		this.pgAmount = pgAmount;
	}
	public BigDecimal getWalletAmount() {
		return walletAmount;
	}
	public void setWalletAmount(BigDecimal walletAmount) {
		this.walletAmount = walletAmount;
	}
	public String getPgName() {
		return pgName;
	}
	public void setPgName(String pgName) {
		this.pgName = pgName;
	}
	public String getPaymentName() {
		return paymentName;
	}
	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((paymentName == null) ? 0 : paymentName.hashCode());
		result = prime * result + ((paymentOption == null) ? 0 : paymentOption.hashCode());
		result = prime * result + ((pgAmount == null) ? 0 : pgAmount.hashCode());
		result = prime * result + ((pgName == null) ? 0 : pgName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((walletAmount == null) ? 0 : walletAmount.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentPlanWrapper other = (PaymentPlanWrapper) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (paymentName == null) {
			if (other.paymentName != null)
				return false;
		} else if (!paymentName.equals(other.paymentName))
			return false;
		if (paymentOption == null) {
			if (other.paymentOption != null)
				return false;
		} else if (!paymentOption.equals(other.paymentOption))
			return false;
		if (pgAmount == null) {
			if (other.pgAmount != null)
				return false;
		} else if (!pgAmount.equals(other.pgAmount))
			return false;
		if (pgName == null) {
			if (other.pgName != null)
				return false;
		} else if (!pgName.equals(other.pgName))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (walletAmount == null) {
			if (other.walletAmount != null)
				return false;
		} else if (!walletAmount.equals(other.walletAmount))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PaymentPlanWrapper [pgAmount=" + pgAmount + ", walletAmount=" + walletAmount + ", pgName=" + pgName
				+ ", paymentName=" + paymentName + ", paymentOption=" + paymentOption + ", status=" + status
				+ ", message=" + message + "]";
	}
}
