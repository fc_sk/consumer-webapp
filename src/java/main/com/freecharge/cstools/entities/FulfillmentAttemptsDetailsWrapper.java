package com.freecharge.cstools.entities;

import java.util.List;

public class FulfillmentAttemptsDetailsWrapper {
	
	private List<AttemptDetails> attemptDetailsList;
	private String fulfillmentType;
	private String status;
	private String message;
	
	public List<AttemptDetails> getAttemptDetailsList() {
		return attemptDetailsList;
	}
	public void setAttemptDetailsList(List<AttemptDetails> attemptDetailsList) {
		this.attemptDetailsList = attemptDetailsList;
	}
	public String getFulfillmentType() {
		return fulfillmentType;
	}
	public void setFulfillmentType(String fulfillmentType) {
		this.fulfillmentType = fulfillmentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attemptDetailsList == null) ? 0 : attemptDetailsList.hashCode());
		result = prime * result + ((fulfillmentType == null) ? 0 : fulfillmentType.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FulfillmentAttemptsDetailsWrapper other = (FulfillmentAttemptsDetailsWrapper) obj;
		if (attemptDetailsList == null) {
			if (other.attemptDetailsList != null)
				return false;
		} else if (!attemptDetailsList.equals(other.attemptDetailsList))
			return false;
		if (fulfillmentType == null) {
			if (other.fulfillmentType != null)
				return false;
		} else if (!fulfillmentType.equals(other.fulfillmentType))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FulfillmentAttemptsDetailsWrapper [attemptDetailsList=" + attemptDetailsList + ", fulfillmentType="
				+ fulfillmentType + ", status=" + status + ", message=" + message + "]";
	}
}
