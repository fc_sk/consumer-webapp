package com.freecharge.cstools.entities;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class BillDetails {

	private boolean isForcedRefund;
	private boolean isReverselCase;
	private String authStatus;
	private String authDetail;
	private String productType;
	private long id;
	private String status;
	private String postpaidNumber;
	private String postpaidAccountNumber;
	private BigDecimal amount;
	private String circleName;
	private String operatorName;
	private String billPaymentGateway;
	private String gatewayReferenceNumber;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	public boolean isForcedRefund() {
		return isForcedRefund;
	}

	public void setForcedRefund(boolean isForcedRefund) {
		this.isForcedRefund = isForcedRefund;
	}

	public boolean isReverselCase() {
		return isReverselCase;
	}

	public void setReverselCase(boolean isReverselCase) {
		this.isReverselCase = isReverselCase;
	}

	public String getAuthStatus() {
		return authStatus;
	}

	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	public String getAuthDetail() {
		return authDetail;
	}

	public void setAuthDetail(String authDetail) {
		this.authDetail = authDetail;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPostpaidNumber() {
		return postpaidNumber;
	}

	public void setPostpaidNumber(String postpaidNumber) {
		this.postpaidNumber = postpaidNumber;
	}

	public String getPostpaidAccountNumber() {
		return postpaidAccountNumber;
	}

	public void setPostpaidAccountNumber(String postpaidAccountNumber) {
		this.postpaidAccountNumber = postpaidAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getBillPaymentGateway() {
		return billPaymentGateway;
	}

	public void setBillPaymentGateway(String billPaymentGateway) {
		this.billPaymentGateway = billPaymentGateway;
	}

	public String getGatewayReferenceNumber() {
		return gatewayReferenceNumber;
	}

	public void setGatewayReferenceNumber(String gatewayReferenceNumber) {
		this.gatewayReferenceNumber = gatewayReferenceNumber;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((authDetail == null) ? 0 : authDetail.hashCode());
		result = prime * result + ((authStatus == null) ? 0 : authStatus.hashCode());
		result = prime * result + ((billPaymentGateway == null) ? 0 : billPaymentGateway.hashCode());
		result = prime * result + ((circleName == null) ? 0 : circleName.hashCode());
		result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((gatewayReferenceNumber == null) ? 0 : gatewayReferenceNumber.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (isForcedRefund ? 1231 : 1237);
		result = prime * result + (isReverselCase ? 1231 : 1237);
		result = prime * result + ((operatorName == null) ? 0 : operatorName.hashCode());
		result = prime * result + ((postpaidAccountNumber == null) ? 0 : postpaidAccountNumber.hashCode());
		result = prime * result + ((postpaidNumber == null) ? 0 : postpaidNumber.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((updatedAt == null) ? 0 : updatedAt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillDetails other = (BillDetails) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (authDetail == null) {
			if (other.authDetail != null)
				return false;
		} else if (!authDetail.equals(other.authDetail))
			return false;
		if (authStatus == null) {
			if (other.authStatus != null)
				return false;
		} else if (!authStatus.equals(other.authStatus))
			return false;
		if (billPaymentGateway == null) {
			if (other.billPaymentGateway != null)
				return false;
		} else if (!billPaymentGateway.equals(other.billPaymentGateway))
			return false;
		if (circleName == null) {
			if (other.circleName != null)
				return false;
		} else if (!circleName.equals(other.circleName))
			return false;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (gatewayReferenceNumber == null) {
			if (other.gatewayReferenceNumber != null)
				return false;
		} else if (!gatewayReferenceNumber.equals(other.gatewayReferenceNumber))
			return false;
		if (id != other.id)
			return false;
		if (isForcedRefund != other.isForcedRefund)
			return false;
		if (isReverselCase != other.isReverselCase)
			return false;
		if (operatorName == null) {
			if (other.operatorName != null)
				return false;
		} else if (!operatorName.equals(other.operatorName))
			return false;
		if (postpaidAccountNumber == null) {
			if (other.postpaidAccountNumber != null)
				return false;
		} else if (!postpaidAccountNumber.equals(other.postpaidAccountNumber))
			return false;
		if (postpaidNumber == null) {
			if (other.postpaidNumber != null)
				return false;
		} else if (!postpaidNumber.equals(other.postpaidNumber))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BillDetails [isForcedRefund=" + isForcedRefund + ", isReverselCase=" + isReverselCase + ", authStatus="
				+ authStatus + ", authDetail=" + authDetail + ", productType=" + productType + ", id=" + id
				+ ", status=" + status + ", postpaidNumber=" + postpaidNumber + ", postpaidAccountNumber="
				+ postpaidAccountNumber + ", amount=" + amount + ", circleName=" + circleName + ", operatorName="
				+ operatorName + ", billPaymentGateway=" + billPaymentGateway + ", gatewayReferenceNumber="
				+ gatewayReferenceNumber + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}
}
