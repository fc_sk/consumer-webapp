package com.freecharge.cstools.entities;

public class FulfillmentDetailsWrapper {
	
	private RechargeDetails rechargeDetails;
	private BillDetails billDetails;
	private String fulfillmentType;
	private String status;
	private String message;
	
	public RechargeDetails getRechargeDetails() {
		return rechargeDetails;
	}
	public void setRechargeDetails(RechargeDetails rechargeDetails) {
		this.rechargeDetails = rechargeDetails;
	}
	public BillDetails getBillDetails() {
		return billDetails;
	}
	public void setBillDetails(BillDetails billDetails) {
		this.billDetails = billDetails;
	}
	public String getFulfillmentType() {
		return fulfillmentType;
	}
	public void setFulfillmentType(String fulfillmentType) {
		this.fulfillmentType = fulfillmentType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billDetails == null) ? 0 : billDetails.hashCode());
		result = prime * result + ((fulfillmentType == null) ? 0 : fulfillmentType.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((rechargeDetails == null) ? 0 : rechargeDetails.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FulfillmentDetailsWrapper other = (FulfillmentDetailsWrapper) obj;
		if (billDetails == null) {
			if (other.billDetails != null)
				return false;
		} else if (!billDetails.equals(other.billDetails))
			return false;
		if (fulfillmentType == null) {
			if (other.fulfillmentType != null)
				return false;
		} else if (!fulfillmentType.equals(other.fulfillmentType))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (rechargeDetails == null) {
			if (other.rechargeDetails != null)
				return false;
		} else if (!rechargeDetails.equals(other.rechargeDetails))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "FulfillmentDetailsWrapper [rechargeDetails=" + rechargeDetails + ", billDetails=" + billDetails
				+ ", fulfillmentType=" + fulfillmentType + ", status=" + status + ", message=" + message + "]";
	}
}
