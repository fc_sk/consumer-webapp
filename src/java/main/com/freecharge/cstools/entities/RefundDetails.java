package com.freecharge.cstools.entities;

import java.math.BigDecimal;
import java.util.Date;

public class RefundDetails {
	
	private String orderId;
	private String merchantTxnId;
	private BigDecimal amount;
	private String status;
	private String refundedTo;
	private String pgResponseDescription;
	private String pgRefundReferenceNumber;
	private Date refundTimeStamp;

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMerchantTxnId() {
		return merchantTxnId;
	}
	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRefundedTo() {
		return refundedTo;
	}
	public void setRefundedTo(String refundedTo) {
		this.refundedTo = refundedTo;
	}
	public String getPgResponseDescription() {
		return pgResponseDescription;
	}
	public void setPgResponseDescription(String pgResponseDescription) {
		this.pgResponseDescription = pgResponseDescription;
	}
	public String getPgRefundReferenceNumber() {
		return pgRefundReferenceNumber;
	}
	public void setPgRefundReferenceNumber(String pgRefundReferenceNumber) {
		this.pgRefundReferenceNumber = pgRefundReferenceNumber;
	}
	public Date getRefundTimeStamp() {
		return refundTimeStamp;
	}
	public void setRefundTimeStamp(Date refundTimeStamp) {
		this.refundTimeStamp = refundTimeStamp;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((merchantTxnId == null) ? 0 : merchantTxnId.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((pgRefundReferenceNumber == null) ? 0 : pgRefundReferenceNumber.hashCode());
		result = prime * result + ((pgResponseDescription == null) ? 0 : pgResponseDescription.hashCode());
		result = prime * result + ((refundTimeStamp == null) ? 0 : refundTimeStamp.hashCode());
		result = prime * result + ((refundedTo == null) ? 0 : refundedTo.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefundDetails other = (RefundDetails) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (merchantTxnId == null) {
			if (other.merchantTxnId != null)
				return false;
		} else if (!merchantTxnId.equals(other.merchantTxnId))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (pgRefundReferenceNumber == null) {
			if (other.pgRefundReferenceNumber != null)
				return false;
		} else if (!pgRefundReferenceNumber.equals(other.pgRefundReferenceNumber))
			return false;
		if (pgResponseDescription == null) {
			if (other.pgResponseDescription != null)
				return false;
		} else if (!pgResponseDescription.equals(other.pgResponseDescription))
			return false;
		if (refundTimeStamp == null) {
			if (other.refundTimeStamp != null)
				return false;
		} else if (!refundTimeStamp.equals(other.refundTimeStamp))
			return false;
		if (refundedTo == null) {
			if (other.refundedTo != null)
				return false;
		} else if (!refundedTo.equals(other.refundedTo))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RefundDetails [orderId=" + orderId + ", merchantTxnId=" + merchantTxnId + ", amount=" + amount
				+ ", status=" + status + ", refundedTo=" + refundedTo + ", pgResponseDescription="
				+ pgResponseDescription + ", pgRefundReferenceNumber=" + pgRefundReferenceNumber + ", refundTimeStamp="
				+ refundTimeStamp + "]";
	}
}
