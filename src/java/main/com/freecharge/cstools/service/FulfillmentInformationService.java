package com.freecharge.cstools.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.service.InService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.cstools.entities.AttemptDetails;
import com.freecharge.cstools.entities.BillDetails;
import com.freecharge.cstools.entities.FulfillmentAttemptsDetailsWrapper;
import com.freecharge.cstools.entities.RechargeDetails;
import com.freecharge.cstools.entities.FulfillmentDetailsWrapper;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;

@Service("fulfillmentInformationService")
public class FulfillmentInformationService {

	private static final Logger logger = LoggerFactory.getLogger(FulfillmentInformationService.class);

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private InService inService;

	@Autowired
	private BillPaymentService billPaymentService;

	public FulfillmentDetailsWrapper getFulfillmentDetails(String orderId) {

		FulfillmentDetailsWrapper fulfillmentDetailsWrapper = new FulfillmentDetailsWrapper();

		// setting forced refund status
		UserTransactionHistory userTransactionHistory = userTransactionHistoryService
				.findUserTransactionHistoryByOrderId(orderId);
		logger.info("User Transaction History:: " + userTransactionHistory.toString());
		boolean isForcedRefund = false;
		if (userTransactionHistory != null && userTransactionHistory.getTransactionStatus()
				.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND)) {
			isForcedRefund = true;
		}

		if (FCUtil.isRechargeProductType(userTransactionHistory.getProductType())) {
			// Prepaid - Recharge
			fulfillmentDetailsWrapper.setFulfillmentType("Prepaid");

			// fetching details from in service
			InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId);
			logger.info("Recharge Details fetched are ->" + ((inTransactionData!=null)?inTransactionData.toString():inTransactionData));

			if (inTransactionData != null) {

				RechargeDetails rechargeDetails = new RechargeDetails();
				rechargeDetails.setForcedRefund(isForcedRefund);

				// set the parameters from in-transaction data
				if (inTransactionData.getInResponse() != null) {
					// change this later to actually give meaningful status
					// instead of just codes
					rechargeDetails.setStatus(inTransactionData.getInResponse().getInRespCode());
					rechargeDetails.setRequestId(inTransactionData.getInResponse().getRequestId());
					rechargeDetails.setOperatorTxnId(inTransactionData.getInResponse().getOprTransId());
					rechargeDetails.setAggregator(inTransactionData.getInResponse().getAggrName());
					rechargeDetails.setAggregatorTxnId(inTransactionData.getInResponse().getAggrTransId());
				}

				String productName = null;
				String reversalToSuccess = RechargeConstants.RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG;
				String reversalToFailure = RechargeConstants.RECHARGE_REVERSAL_IN_RESP_MSG;
				if (inTransactionData.getInResponse() != null && (reversalToSuccess
						.equalsIgnoreCase(inTransactionData.getInResponse().getInRespMsg())
						|| reversalToFailure.equalsIgnoreCase(inTransactionData.getInResponse().getInRespMsg()))) {
					// reversal case
					rechargeDetails.setReverselCase(true);
				}

				if (inTransactionData.getInRequest() != null) {
					rechargeDetails.setAmount(inTransactionData.getInRequest().getAmount());
					rechargeDetails.setOperator(inTransactionData.getInRequest().getOperator());
					rechargeDetails.setCircle(inTransactionData.getInRequest().getCircle());
					rechargeDetails.setMobile(inTransactionData.getInRequest().getSubscriberNumber());
					rechargeDetails.setPlan(inTransactionData.getInRequest().getRechargePlan());

					productName = FCConstants.productTypeToNameMap
							.get(inTransactionData.getInRequest().getProductType());
					rechargeDetails.setProductName(productName);
				}

				fulfillmentDetailsWrapper.setRechargeDetails(rechargeDetails);
				fulfillmentDetailsWrapper.setStatus("SUCCESS");
			}
		} else if (FCUtil.isBillPostpaidType(userTransactionHistory.getProductType())) {
			// Postpaid - Bill
			fulfillmentDetailsWrapper.setFulfillmentType("Postpaid");

			BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
			logger.info("Bill Details fetched are ->" + ((billPaymentStatus!=null)?billPaymentStatus.toString():billPaymentStatus));

			if (billPaymentStatus != null) {

				BillDetails billDetails = new BillDetails();
				billDetails.setForcedRefund(isForcedRefund);

				// set the parameters from bill payment status object
				billDetails.setAmount(billPaymentStatus.getAmount());
				billDetails.setAuthDetail(billPaymentStatus.getAuthDetail());
				billDetails.setAuthStatus(billPaymentStatus.getAuthStatus());
				billDetails.setBillPaymentGateway(billPaymentStatus.getBillPaymentGateway());
				billDetails.setCircleName(billPaymentStatus.getCircleName());
				billDetails.setCreatedAt(billPaymentStatus.getCreatedAt());
				billDetails.setGatewayReferenceNumber(billPaymentStatus.getGatewayReferenceNumber());
				billDetails.setId(billPaymentStatus.getId());
				billDetails.setOperatorName(billPaymentStatus.getOperatorName());
				billDetails.setPostpaidAccountNumber(billPaymentStatus.getPostpaidAccountNumber());
				billDetails.setPostpaidNumber(billPaymentStatus.getPostpaidNumber());
				billDetails.setProductType(billPaymentStatus.getProductType());
				billDetails.setUpdatedAt(billPaymentStatus.getUpdatedAt());
				billDetails.setStatus((billPaymentStatus.getSuccess()!=null)?(billPaymentStatus.getSuccess()? "SUCCESS" : "FAILED"): null);

				if (RechargeConstants.POSTPAID_REVERSAL_AUTH_STATUS.equals(billPaymentStatus.getAuthStatus())
						|| RechargeConstants.POSTPAID_SUCCESS_REVERSAL_AUTH_STATUS
								.equals(billPaymentStatus.getAuthStatus())) {
					// reversal case
					billDetails.setReverselCase(true);
				}

				fulfillmentDetailsWrapper.setBillDetails(billDetails);
				fulfillmentDetailsWrapper.setStatus("SUCCESS");
			}
		} else {
			fulfillmentDetailsWrapper.setStatus("FAILED");
			fulfillmentDetailsWrapper.setMessage("No Details Found");
		}
		return fulfillmentDetailsWrapper;
	}

	public FulfillmentAttemptsDetailsWrapper getFulfillmentAttemptsDetails(String orderId) {

		FulfillmentAttemptsDetailsWrapper fulfillmentDetailsAttemptsWrapper = new FulfillmentAttemptsDetailsWrapper();
		List<AttemptDetails> attemptDetailsList = new ArrayList<AttemptDetails>();

		UserTransactionHistory userTransactionHistory = userTransactionHistoryService
				.findUserTransactionHistoryByOrderId(orderId);
		logger.info("User Transaction History:: " + ((userTransactionHistory!=null)?userTransactionHistory.toString():userTransactionHistory));

		if (FCUtil.isRechargeProductType(userTransactionHistory.getProductType())) {
			// Prepaid - Recharge
			fulfillmentDetailsAttemptsWrapper.setFulfillmentType("Prepaid");

			List<InTransactions> inTransactionsList = null;
			inTransactionsList = inService.getInTransactionsListByOrder(orderId);
			if (!inTransactionsList.isEmpty()) {
				Collections.reverse(inTransactionsList);
				for (InTransactions inTransaction : inTransactionsList) {
					// set the transaction attempt details
					AttemptDetails attemptDetails = new AttemptDetails();
					attemptDetails.setAggregatorName(inTransaction.getAggrName());
					attemptDetails.setAggregatorResponseCode(inTransaction.getAggrResponseCode());
					attemptDetails.setAggregatorResponseMessage(inTransaction.getAggrResponseMessage());
					attemptDetails.setAggregatorTxnId(inTransaction.getAggrReferenceNo());
					attemptDetails.setOperatorTxnId(inTransaction.getAggrOprRefernceNo());
					attemptDetails.setProductType(userTransactionHistory.getProductType());
					attemptDetails.setRequestId(inTransaction.getFkRequestId());
					attemptDetails.setRequestTime(inTransaction.getRequestTime());
					attemptDetails.setResponseTime(inTransaction.getResponseTime());
					attemptDetails.setStatus(inTransaction.getTransactionStatus());
					attemptDetailsList.add(attemptDetails);
				}
				fulfillmentDetailsAttemptsWrapper.setAttemptDetailsList(attemptDetailsList);
				fulfillmentDetailsAttemptsWrapper.setStatus("SUCCESS");
			} else {
				fulfillmentDetailsAttemptsWrapper.setStatus("FAILED");
				fulfillmentDetailsAttemptsWrapper.setMessage("No attempt details found");
			}
		} else if (FCUtil.isBillPostpaidType(userTransactionHistory.getProductType())) {
			// Postpaid - Bill
			fulfillmentDetailsAttemptsWrapper.setFulfillmentType("Postpaid");
			// fetch the bill payment attempts
			BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
			List<BillPaymentAttempt> billPayAttempts = billPaymentService
					.findBillPaymentAttemptsByStatusId(billPaymentStatus.getId());

			if (!billPayAttempts.isEmpty()) {
				for (BillPaymentAttempt billPayAttempt : billPayAttempts) {
					// set the bill payment attempt details
					AttemptDetails attemptDetails = new AttemptDetails();
					attemptDetails.setAggregatorResponseCode(billPayAttempt.getAuthStatus());
					attemptDetails.setAggregatorResponseMessage(billPayAttempt.getAuthDetail());
					attemptDetails.setProductType(userTransactionHistory.getProductType());
					attemptDetails.setRequestId(billPayAttempt.getId());
					attemptDetails.setRequestTime(billPayAttempt.getCreatedAt());
					attemptDetails.setStatus((billPayAttempt.getSuccess()!=null)?(billPayAttempt.getSuccess()? "SUCCESS" : "FAILED"): null);
					attemptDetailsList.add(attemptDetails);
				}
			} else {
				fulfillmentDetailsAttemptsWrapper.setStatus("FAILED");
				fulfillmentDetailsAttemptsWrapper.setMessage("No attempt details found");
			}
		}
		return fulfillmentDetailsAttemptsWrapper;
	}
}
