package com.freecharge.cstools.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.freecharge.cstools.entities.RefundDetails;
import com.freecharge.cstools.entities.RefundDetailsWrapper;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.services.PaymentTransactionService;

@Service("refundInformationService")
public class RefundInformationService {

	private static final Logger logger = LoggerFactory.getLogger(RefundInformationService.class);

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	public RefundDetailsWrapper getRefundDetails(String orderId) {
		
		RefundDetailsWrapper refundDetailsWrapper = new RefundDetailsWrapper();
		List<RefundDetails> refundDetailsList = new ArrayList<RefundDetails>();
		
		// fetch refund details
		List<PaymentRefundTransaction> paymentRefundTransactions = paymentTransactionService.getPaymentRefundTransactions(orderId);
		logger.info("The refund details fetched from payment transaction service are -> " + paymentRefundTransactions);
		if(!paymentRefundTransactions.isEmpty()){
			//set refund details
			for(PaymentRefundTransaction paymentRefundTransaction: paymentRefundTransactions){
				RefundDetails refundDetails = new RefundDetails();
				refundDetails.setAmount(paymentRefundTransaction.getRefundedAmount().getAmount());
				refundDetails.setMerchantTxnId(paymentRefundTransaction.getPaymentTransaction().getMerchantTxnId());
				refundDetails.setOrderId(paymentRefundTransaction.getPaymentTransaction().getOrderId());
				refundDetails.setPgRefundReferenceNumber(paymentRefundTransaction.getPgTransactionReferenceNo());
				refundDetails.setPgResponseDescription(paymentRefundTransaction.getPgResponseDescription());
				refundDetails.setRefundedTo(paymentRefundTransaction.getRefundTo());
				refundDetails.setRefundTimeStamp(paymentRefundTransaction.getReceivedFromPG());
				refundDetails.setStatus(paymentRefundTransaction.getStatus().getStatusString());
				refundDetailsList.add(refundDetails);
			}
			refundDetailsWrapper.setRefundDetailsList(refundDetailsList);
			refundDetailsWrapper.setStatus("SUCCESS");
		}else{
			refundDetailsWrapper.setStatus("FAILED");
			refundDetailsWrapper.setMessage("No Refund details found");
		}
		return refundDetailsWrapper;
	}
}
