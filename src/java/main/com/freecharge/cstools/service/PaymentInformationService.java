package com.freecharge.cstools.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.util.FCUtil;
import com.freecharge.cstools.entities.PaymentAttemptsWrapper;
import com.freecharge.cstools.entities.PaymentPlanWrapper;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;

@Service("paymentInformationService")
public class PaymentInformationService {

	private static final Logger logger = LoggerFactory.getLogger(PaymentInformationService.class);

	@Autowired
	private PaymentPlanService ppService;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private EasyDBWriteDAO easyDBWriteDAO;

	private enum status {
		EXCEPTION, FAILED, SUCCESS
	}

	public List<com.freecharge.cstools.entities.PaymentTransaction> getProcessedTransactionAttemptList(
			List<PaymentTransaction> paymentTransactionAttemptList) {
		List<com.freecharge.cstools.entities.PaymentTransaction> paymentAttemptList = new ArrayList<com.freecharge.cstools.entities.PaymentTransaction>();
		for (PaymentTransaction pt : paymentTransactionAttemptList) {
			com.freecharge.cstools.entities.PaymentTransaction paymentTransaction = new com.freecharge.cstools.entities.PaymentTransaction();
			paymentTransaction.setPaymentTxnId(pt.getPaymentTxnId());
			paymentTransaction.setTransactionId(pt.getTransactionId());
			paymentTransaction.setOrderId(pt.getOrderId());
			paymentTransaction.setSetToPg(pt.getSetToPg());
			paymentTransaction.setRecievedToPg(pt.getRecievedToPg());
			paymentTransaction.setAmount(pt.getAmount());
			paymentTransaction.setMerchantTxnId(pt.getMerchantTxnId());
			paymentTransaction.setIsSuccessful(pt.getIsSuccessful());
			paymentTransaction.setPaymentGateway(pt.getPaymentGateway());
			paymentTransaction.setPaymentMode(pt.getPaymentMode());
			paymentTransaction.setPgMessage(pt.getPgMessage());
			paymentTransaction.setPaymentRequestId(pt.getPaymentRequestId());
			paymentTransaction.setPaymentResponseId(pt.getPaymentResponseId());
			paymentTransaction.setPaymentType(pt.getPaymentType());
			paymentTransaction.setPaymentOption(pt.getPaymentOption());
			paymentTransaction.setPgTransactionTime(pt.getPgTransactionTime());
			paymentAttemptList.add(paymentTransaction);
		}
		logger.info("Transformed payment attempt list is -> " + paymentAttemptList.toString());
		return paymentAttemptList;
	}

	public PaymentPlanWrapper getPaymentPlanInformation(String orderId) {

		PaymentPlanWrapper paymentPlanWrapper = new PaymentPlanWrapper();
		PaymentPlan paymentPlan = null;
		try {
			logger.info("Request for payment plan service call is :: " + orderId);
			paymentPlan = ppService.getPaymentPlan(orderId);
			logger.info("Response from Payment Plan service is:: " + paymentPlan.toString());
		} catch (PaymentPlanNotFoundException e) {
			logger.error("Exception while fetching playment plan from service", e);
			paymentPlanWrapper.setStatus(status.EXCEPTION.name());
			paymentPlanWrapper.setMessage(e.getMessage());
		}

		if (paymentPlan != null) {

			paymentPlanWrapper.setPgAmount(
					(paymentPlan.getPgAmount() != null) ? paymentPlan.getPgAmount().getAmount() : new BigDecimal(0));
			paymentPlanWrapper.setWalletAmount((paymentPlan.getWalletAmount() != null)
					? paymentPlan.getWalletAmount().getAmount() : new BigDecimal(0));

			// call other services to fetch additional information that is
			// needed and wrap it in PaymentPlanWrapper Object

			// call paymentTransactionService to get
			// paymentTransactionAttemptList
			List<PaymentTransaction> paymentTransactionAttemptList = paymentTransactionService
					.getAllPaymentTransactionByAcsOrder(orderId);

			logger.info("Payment Transaction Attempt List fetched for order id:: "
					+ paymentTransactionAttemptList.toString());
			// set PaymentTypeMaster
			PaymentTypeMaster paymentTypeMaster = new PaymentTypeMaster();
			if (paymentTransactionAttemptList != null && !paymentTransactionAttemptList.isEmpty()) {
				paymentTypeMaster.setPaymentTypeMasterId(paymentTransactionAttemptList.get(0).getPaymentType());
				List<PaymentTypeMaster> paymentTypeMasters = easyDBWriteDAO.get(paymentTypeMaster);
				paymentTypeMaster = FCUtil.getFirstElement(paymentTypeMasters);
			}
			logger.info("Payment Type Master fetched for order id:: " + paymentTypeMaster.toString());
			paymentPlanWrapper.setPaymentName(paymentTypeMaster.getName());

			// set PG Name
			if (paymentTransactionAttemptList != null && !paymentTransactionAttemptList.isEmpty()) {
				paymentPlanWrapper.setPgName(paymentTransactionAttemptList.get(0).getPaymentGateway());
			}

			// set PaymentTypeOption
			PaymentTypeOptions paymentTypeOptions = new PaymentTypeOptions();
			if ((paymentTransactionAttemptList != null)
					&& (paymentTransactionAttemptList.get(0).getPaymentOption() != null)) {
				paymentTypeOptions.setCode(paymentTransactionAttemptList.get(0).getPaymentOption());
				List<PaymentTypeOptions> paymentTypeOptionsList = easyDBWriteDAO.get(paymentTypeOptions);
				paymentTypeOptions = FCUtil.getFirstElement(paymentTypeOptionsList);
			}
			logger.info("Payment Type Option fetched for order id:: " + paymentTypeOptions.toString());
			paymentPlanWrapper.setPaymentOption(paymentTypeOptions.getDisplayName());
			paymentPlanWrapper.setStatus(status.SUCCESS.name());
		} else {
			paymentPlanWrapper.setStatus(status.FAILED.name());
			paymentPlanWrapper.setMessage("Payment Plan details not found");
		}
		return paymentPlanWrapper;
	}

	public PaymentAttemptsWrapper getPaymentAttemptsInformation(String orderId) {

		PaymentAttemptsWrapper paymentAttemptsWrapper = new PaymentAttemptsWrapper();
		logger.info("Request for Payment Transaction Service is :: " + orderId);
		List<PaymentTransaction> paymentTransactionAttemptList = paymentTransactionService
				.getAllPaymentTransactionByAcsOrder(orderId);
		logger.info("Response from Payment Transaction Service is :: " + paymentTransactionAttemptList);
		// call service to convert the Payment Transaction dos entity to
		// customised entity
		List<com.freecharge.cstools.entities.PaymentTransaction> paymentAttemptsList = this
				.getProcessedTransactionAttemptList(paymentTransactionAttemptList);
		if (paymentTransactionAttemptList == null) {
			paymentAttemptsWrapper.setStatus(status.FAILED.name());
			paymentAttemptsWrapper.setMessage("No Payment Attempt details found");
			return paymentAttemptsWrapper;
		}
		paymentAttemptsWrapper.setStatus(status.SUCCESS.name());
		paymentAttemptsWrapper.setPaymentAttempts(paymentAttemptsList);
		return paymentAttemptsWrapper;
	}
}
