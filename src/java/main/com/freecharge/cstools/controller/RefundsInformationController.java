package com.freecharge.cstools.controller;

import javax.ws.rs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.cstools.entities.RefundDetailsWrapper;
import com.freecharge.cstools.service.RefundInformationService;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@RestController
@RequestMapping("rest/cstools/refundsinfo/*")
public class RefundsInformationController {

	private static final Logger logger = LoggerFactory.getLogger(FulfillmentInformationController.class);

	@Autowired
	RefundInformationService refundInformationService;

	@NoLogin
	@NoSessionWrite
	@Authenticate(api = "refundsinfo.getRefundDetails")
	@RequestMapping(value = "refunddetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody RefundDetailsWrapper getRefundDetails(@QueryParam("orderId") String orderId) {

		RefundDetailsWrapper refundDetailsWrapper = refundInformationService.getRefundDetails(orderId);
		logger.info("Response received from refund information service is -> " + refundDetailsWrapper.toString());
		return refundDetailsWrapper;
	}
}
