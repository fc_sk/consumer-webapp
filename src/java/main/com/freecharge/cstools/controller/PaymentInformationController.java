package com.freecharge.cstools.controller;

import javax.ws.rs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.cstools.entities.PaymentAttemptsWrapper;
import com.freecharge.cstools.entities.PaymentPlanWrapper;
import com.freecharge.cstools.service.PaymentInformationService;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@RestController
@RequestMapping("rest/cstools/paymentsinfo/*")
public class PaymentInformationController {
	
    private static final Logger logger = LoggerFactory.getLogger(PaymentInformationController.class);

    @Autowired
    private PaymentInformationService paymentInformationService;
    
    @NoLogin
    @NoSessionWrite
    @Authenticate(api="paymentsinfo.getPaymentPlanInformation")
    @RequestMapping(value = "paymentplan", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody PaymentPlanWrapper getPaymentPlanInformation(@QueryParam("orderId") String orderId) {
    	
    	logger.info("Request for Payment Plan Information is -> " + orderId);
    	PaymentPlanWrapper paymentPlanWrapper = paymentInformationService.getPaymentPlanInformation(orderId);
    	logger.info("Response for Payment Plan Information is -> " + paymentPlanWrapper);	
    	return paymentPlanWrapper;
    }
    
    @NoLogin
    @NoSessionWrite
    @Authenticate(api="paymentsinfo.getPaymentAttempts")
    @RequestMapping(value="paymentattempts", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody PaymentAttemptsWrapper getPaymentAttempts(@QueryParam("orderId") String orderId){
    	
    	logger.info("Request for Payment Attempts Information is -> " + orderId);
    	PaymentAttemptsWrapper paymentAttemptsWrapper = paymentInformationService.getPaymentAttemptsInformation(orderId);
    	logger.info("Response for Payment Attempts Information is -> " + paymentAttemptsWrapper);
    	return paymentAttemptsWrapper;
    }
}