package com.freecharge.cstools.controller;

import javax.ws.rs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.cstools.entities.FulfillmentAttemptsDetailsWrapper;
import com.freecharge.cstools.entities.FulfillmentDetailsWrapper;
import com.freecharge.cstools.service.FulfillmentInformationService;
import com.freecharge.web.interceptor.annotation.Authenticate;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

@RestController
@RequestMapping("rest/cstools/fulfillmentinfo/*")
public class FulfillmentInformationController {

	private static final Logger logger = LoggerFactory.getLogger(FulfillmentInformationController.class);

	@Autowired
	FulfillmentInformationService fulfillmentInformationService;

	@NoLogin
	@NoSessionWrite
	@Authenticate(api = "fulfillmentinfo.getFulfillmentDetails")
	@RequestMapping(value = "fulfillmentdetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody FulfillmentDetailsWrapper getFulfillmentDetails(@QueryParam("orderId") String orderId) {

		FulfillmentDetailsWrapper fulfillmentDetailsWrapper = fulfillmentInformationService
				.getFulfillmentDetails(orderId);
		logger.info(
				"Response received from fulfillment information service is -> " + fulfillmentDetailsWrapper.toString());
		return fulfillmentDetailsWrapper;
	}

	@NoLogin
	@NoSessionWrite
	@Authenticate(api = "fulfillmentinfo.getAttemptDetails")
	@RequestMapping(value = "attemptdetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody FulfillmentAttemptsDetailsWrapper getFulfillmentAttemptsDetails(
			@QueryParam("orderId") String orderId) {

		FulfillmentAttemptsDetailsWrapper fulfillmentAttemptsDetailsWrapper = fulfillmentInformationService
				.getFulfillmentAttemptsDetails(orderId);
		logger.info("Response received from fulfillment information service for  is -> "
				+ fulfillmentAttemptsDetailsWrapper.toString());
		return fulfillmentAttemptsDetailsWrapper;
	}
}
