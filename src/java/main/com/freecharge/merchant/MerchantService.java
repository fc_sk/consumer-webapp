package com.freecharge.merchant;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.dao.IAffiliateProfileDAO;
import com.freecharge.app.domain.dao.IStateMasterDAO;
import com.freecharge.app.domain.dao.UserProfileDAO;
import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.AffiliateProfile;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.businessdo.LoginBusinessDO.UserType;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.ISessionService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.merchant.bean.Merchant;
import com.freecharge.merchant.bean.MerchantLoginDO;
import com.freecharge.web.util.WebConstants;

/**
 * Class that exposes merchant related abstractions for
 * controllers and other business entities. Any entity
 * interested in merchant details etc., should use this service
 * and nothing else.
 * @author arun
 *
 */
@Service
public class MerchantService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    
    @Autowired
    private UserAuthService authService;
    
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private UserProfileDAO userProfileDao;
    
    @Autowired
    private UsersDAO usersDao;
    
    @Autowired
    private IStateMasterDAO stateMasterDao;
    
    @Autowired
    private CityMasterDAO cityMasterDao;
    
    @Autowired
    private IAffiliateProfileDAO affiliateProfileDao;
    
    @Autowired
    private ISessionService sessionService;
    
    @Autowired
    @Qualifier("couponServiceProxy")
    private ICouponService couponServiceProxy;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    public boolean isMerchantLoggedIn() {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        
        if (fs == null) {
            return false;
        }
        
        Map<String, Object> sessionData = fs.getSessionData();
        
        if (sessionData == null || sessionData.isEmpty()) {
            return false;
        }
        
        if (!sessionData.containsKey(WebConstants.Merchant.IS_LOGGED_IN)) {
            return false;
        }
        int userId = (Integer) sessionData.get(WebConstants.Merchant.USER_ID);
        
        List<AffiliateProfile> profileList = affiliateProfileDao.findByUserId(userId);
        if(profileList==null || profileList.size()==0) {
        	return false;
        }
        return (Boolean) sessionData.get(WebConstants.Merchant.IS_LOGGED_IN);
    }
    
    
    public boolean authenticateMerchantAndSetSession(MerchantLoginDO merchantLoginDO) {
        LoginBusinessDO loginBizDO = new LoginBusinessDO();
        loginBizDO.setEmail(merchantLoginDO.getUserName());
        loginBizDO.setPassword(merchantLoginDO.getPassword());

        boolean authSuccess = false;
        
        try {
            authService.doLogin(loginBizDO);
            
            if (loginBizDO.isLogin() && loginBizDO.getUserType() == UserType.Merchant) {
                List<AffiliateProfile> profileList = affiliateProfileDao.findById(loginBizDO.getAffiliateProfileId());
                AffiliateProfile affiliateProfile = profileList.get(0);

                CouponMerchant merchant = couponServiceProxy.getCouponMerchant(Integer.parseInt(affiliateProfile.getAffliateUniqueId()));

                if (merchant != null) {
                    authSuccess = true;
                    FreechargeSession.setMerchantRelatedSessionData(loginBizDO.getEmail(), true, Integer.parseInt(loginBizDO.getUserId()), "");
                }
                
            }
        } catch (Exception e) {
            logger.error(
                    "System error while merchant login; redirecting to home page. User ID: ["
                            + String.valueOf(merchantLoginDO.getUserName())
                            + "]", e);
            authSuccess = false;
        }
        return authSuccess;
    }
    
    public void logoutMerchant() {
        FreechargeSession.resetMerchantSessionData();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        sessionService.deleteSession(fs.getUuid());
    }
    
    /**
     * Retrieve {@link CouponMerchant} bean from session. It is expected
     * that the merchant is logged in and a valid session exists.
     * @return
     */
    public Merchant getMerchantFromSession() {
        Merchant toReturn = new Merchant();
        
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> sessionData = fs.getSessionData();

        int userId = (Integer) sessionData.get(WebConstants.Merchant.USER_ID);
        
        List<AffiliateProfile> profileList = affiliateProfileDao.findByUserId(userId);
        AffiliateProfile affiliateProfile = profileList.get(0);

        CouponMerchant couponMerchant = couponServiceProxy.getCouponMerchant(Integer.parseInt(affiliateProfile.getAffliateUniqueId()));

        toReturn.setCouponMerchant(couponMerchant);
        toReturn.setMerchantProfile(affiliateProfile);
        
        return toReturn;
    }
    
    /*
     * Retrieves the list of all merchants
     */
    @Transactional(readOnly=true)
	public List<CouponMerchant> getAllMerchants() {

		List<CouponMerchant> merchantList = couponServiceProxy.getAllMerchants();
		return merchantList;

	}

    @Transactional
	public String createMerchantLogin(String merchantId, String merchantName,
			String userName) {

		String result = null;
		java.util.Date date = new java.util.Date();
		try {
			couponServiceProxy.updateMerchantReference(merchantId);
			Long userId = userServiceProxy.getUserIdByEmailId(userName);
			if(userId==null || userId==0) {
				result = "User Not Exist";
				logger.error("User Not Exist for merchant mapping");
				return result;
			}
            List<AffiliateProfile> profileList = affiliateProfileDao.findByUserIdAndAffliateUniqueId(userId.intValue(), merchantId);
            if(profileList.size() == 0){
                AffiliateProfile affiliate_profile = new AffiliateProfile();
                affiliate_profile.setAffliateUniqueId(merchantId);
                affiliate_profile.setCompany(merchantName);
                affiliate_profile.setContactName(merchantName);
                affiliate_profile.setEmail("");
                affiliate_profile.setPhone("");
                affiliate_profile.setExt("");
                affiliate_profile.setFax("");
                affiliate_profile.setAddress1("");
                affiliate_profile.setAddress2("");
                affiliate_profile.setCity("");
                affiliate_profile.setState("");
                affiliate_profile.setPostalCode("");
                affiliate_profile.setSiteName(merchantName);
                affiliate_profile.setCreatedDate((new Timestamp(
    					date.getTime())));
                affiliate_profile.setUserId(userId.intValue());
                affiliateProfileDao.insertAffiliateProfile(affiliate_profile);
            }
            
            result = "user succesfully created";
            
		} catch (Exception e) {
			result = "An error occured while creating merchant login";
			logger.error("Exception occured while creating merchant login ", e);
		}
		return result;
	}
}
