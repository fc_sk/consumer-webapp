package com.freecharge.merchant.bean;

import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.app.domain.entity.AffiliateProfile;
import com.freecharge.app.domain.entity.Users;

/**
 * A uber bean that captures everything about
 * a merchant.
 * @author arun
 *
 */
public class Merchant {
    private CouponMerchant couponMerchant;
    private AffiliateProfile merchantProfile;
    
    public CouponMerchant getCouponMerchant() {
        return couponMerchant;
    }
    public void setCouponMerchant(CouponMerchant couponMerchant) {
        this.couponMerchant = couponMerchant;
    }
    
    public AffiliateProfile getMerchantProfile() {
        return merchantProfile;
    }
    public void setMerchantProfile(AffiliateProfile merchantProfile) {
        this.merchantProfile = merchantProfile;
    }
}
