package com.freecharge.merchant.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.merchant.MerchantService;
import com.freecharge.merchant.bean.MerchantLoginDO;
import com.freecharge.web.util.ViewConstants;

/**
 * Entry point for merchant pages.
 * @author arun
 *
 */
@Controller
public class MerchantController {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private UserAuthService authService;
    
    @Autowired
    private MerchantService merchantService;
    
    private final String MERCHANT_HOME = "/merchant/home.htm";
    private final String MERCHANT_LOGIN = "/merchant/login.htm";
    private final String LOGIN_HANDLER = "/api/v2/identity/merchant/login";
    private final String REDEMPTION_HOME = "/merchant/redemptionform.htm";
    private final String MERCHANT_LOGOUT = "/merchant/logout.htm";
    
    private interface JSPConstants {
        public final String LOGIN_HANDLER = "merchantLoginAction";
        public final String LOGIN_BEAN = "merchantLoginDO";
        public final String IS_LOGIN_PAGE = "isLoginPage";
    }
    
    @RequestMapping(value = MERCHANT_HOME)
    public String merchantHome(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (merchantService.isMerchantLoggedIn()) {
            return FCUtil.getRedirectAction(REDEMPTION_HOME);
        }
        
        return FCUtil.getRedirectAction(MERCHANT_LOGIN);
    }
    
    @RequestMapping(value = MERCHANT_LOGIN)
    public ModelAndView merchantLogin(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (merchantService.isMerchantLoggedIn()) {
            logger.info("Merchant already logged in; redirecting to coupon redemption page");
            return new ModelAndView(FCUtil.getRedirectAction(REDEMPTION_HOME));
        }
        model.addAttribute(JSPConstants.IS_LOGIN_PAGE, true);
        model.addAttribute(JSPConstants.LOGIN_HANDLER, LOGIN_HANDLER);
        model.addAttribute(JSPConstants.LOGIN_BEAN, new MerchantLoginDO());
        
        return new ModelAndView(ViewConstants.MERCHANT_LOGIN);
    }
    
    @RequestMapping(value = MERCHANT_LOGOUT)
    public ModelAndView handleLogout(HttpServletRequest request, HttpServletResponse response, Model model) {
        merchantService.logoutMerchant();
        
        return new ModelAndView(FCUtil.getRedirectAction(MERCHANT_HOME));
    }
    
//    @RequestMapping(value = LOGIN_HANDLER)
//    public String handleLogin(@ModelAttribute(value = JSPConstants.LOGIN_BEAN) MerchantLoginDO merchantLoginDO, HttpServletRequest request, HttpServletResponse response, Model model) {
//        boolean authSuccess = merchantService.authenticateMerchantAndSetSession(merchantLoginDO);
//        
//        if (authSuccess) {
//            logger.info("Successfully authenticated merchant user ID: [" + merchantLoginDO.getUserName() + "]");
//            return FCUtil.getRedirectAction(REDEMPTION_HOME);
//        } else {
//            logger.info("Authentication failed for merchant user ID: [" + merchantLoginDO.getUserName() + "]");
//            return FCUtil.getRedirectAction(MERCHANT_LOGIN);
//        }
//    }
 }