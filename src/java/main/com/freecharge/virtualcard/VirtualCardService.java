package com.freecharge.virtualcard;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.mojo.MojoClient;
import com.freecharge.mojo.MojoConfig;
import com.freecharge.mojo.core.*;
import com.freecharge.virtualcard.exception.FundPostException;
import com.freecharge.virtualcard.exception.VCardStatusUpdateFailedException;
import com.freecharge.virtualcard.exception.VirtualCardException;
import com.freecharge.virtualcard.model.FundPostRefundResponse;
import com.freecharge.wallet.OneCheckWalletService;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by Vivek on 19/11/15.
 */

@Service("virtualCardService")
public class VirtualCardService {

    private static Logger           logger                                  = LoggingFactory.getLogger(VirtualCardService.class); //TODO:Make corresponding changes in Logger

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private MojoConfig mojoConfig;

    @Autowired
    private OneCheckWalletService   oneCheckWalletService;

    @Autowired
    MojoClient                      mojoClient;

    private final static String     STATUS_SUCCESS                          = "Success";
    private final static String     STATUS_FAILED                           = "Failed";

    private final static String     STATUS_UPDATE_SUCCESS_RESPONSE_CODE     = "E-000";
    private final static String     STATUS_UPDATE_SUCCESS_RESPONSE          = "Status updated successfully";
    public final static String     ENABLE_CARD_STRING                      = "Enable";
    public final static String     DISABLE_CARD_STRING                     = "Disable";
    public final static String      TXN_LIST_STRING                         = "transactionList";
    public final static String      ALLOW_REVERSAL_STRING                    = "allowReversal";
    private final static String     TRANSACTION_REF_NO_STRING               = "transactionRefNo";
    private final static String     REFUND_AMOUNT_STRING                    = "refundAmount";
    private final static String     CARD_HASH_ID_STRING                     = "cardHashId";
    public final static String     VC_REFUND_STATUS_STRING                 = "refundStatus";
    public final static String     VC_REFUND_MESSAGE_STRING                = "refundMessage";

    public List<Map<String,Object>> getCardsForEmail(String email)
    {
        List<Map<String,Object>> responseListMap = new ArrayList<Map<String,Object>>();
        VCardDataResponse vCardDataResponse = null;
        String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        logger.info("Fetching VCard data for userId: "+fcWalletId);
        try {
            vCardDataResponse= mojoClient.getCardDataForUser(fcWalletId);
            logger.info("Successfully fetched VCard data for userId: "+fcWalletId);
            if(vCardDataResponse!=null)
            {
                for(VCardDataResult vCardDataResult: vCardDataResponse.getvCardDataResults())
                {
                    Map<String,Object> resultMap = new HashMap<String,Object>();
                    resultMap.put("emailId",email);
                    resultMap.put("userId",vCardDataResult.getWalletUserId());
                    resultMap.put("agCode",vCardDataResult.getAgCode().toString());
                    resultMap.put("agUserId",vCardDataResult.getAgUserId());
                    resultMap.put("agCardId",vCardDataResult.getAgCardId());
                    resultMap.put("userStatus",vCardDataResult.getUserStatus().toString());
                    resultMap.put("issuedOn",vCardDataResult.getMmCardExpiry().getIssued());
                    resultMap.put("expiry",vCardDataResult.getMmCardExpiry().getExpiry());
                    resultMap.put("createdOn",vCardDataResult.getCreatedOn().toString());
                    resultMap.put("lastUpdated",vCardDataResult.getLastUpdated().toString());
                    if(vCardDataResult.getUserStatus() == MojoEnums.UserStatus.ACTIVE)
                    {
                        resultMap.put("changeStatus",DISABLE_CARD_STRING);
                    }
                    else if(vCardDataResult.getUserStatus() == MojoEnums.UserStatus.INACTIVE || vCardDataResult.getUserStatus() == MojoEnums.UserStatus.BLOCKED)
                    {
                        resultMap.put("changeStatus",ENABLE_CARD_STRING);
                    }
                    responseListMap.add(resultMap);
                }
            }
        }
        catch (Exception e) {
            logger.error("Error in fetching VCard data for userId: "+fcWalletId+", error: "+e);
            logger.error("Stack Trace for getCardsForEmail() API is -> ",e);
            responseListMap = null;
        }
        return responseListMap;

    }

    public List<Map<String,Object>> getTxnForUser(String email)
    {
        List<Map<String,Object>> responseListMap = new ArrayList<Map<String,Object>>();
        VCardPaymentTxnResponse vCardPaymentTxnResponse = null;
        String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        logger.info("Fetching VCard txns for userId: "+fcWalletId);
        try {
            vCardPaymentTxnResponse= mojoClient.getTxnForUser(fcWalletId);
            logger.info("Successfully fetched VCard txns for userId: "+fcWalletId);
            if(vCardPaymentTxnResponse!=null)
            {
                for(VCardPaymentTxnResult vCardPaymentTxnResult: vCardPaymentTxnResponse.getPaymentTxn())
                {
                    Map<String,Object> resultMap = new HashMap<String,Object>();
                    resultMap.put("userId",vCardPaymentTxnResult.getWalletUserId());
                    resultMap.put("agCode",vCardPaymentTxnResult.getAgCode().toString());
                    resultMap.put("agUserId",vCardPaymentTxnResult.getAgUserId());
                    resultMap.put("agCardId",vCardPaymentTxnResult.getAgCardId());
                    resultMap.put("agTxnRefNum",vCardPaymentTxnResult.getAgTxnRefNum());
                    resultMap.put("agNetworkCode",vCardPaymentTxnResult.getAgNetworkCode().toString());
                    resultMap.put("agTraceNumber",vCardPaymentTxnResult.getAgTraceNumber());
                    resultMap.put("agMerchantCategoryCode",vCardPaymentTxnResult.getAgMerchantCategoryCode());
                    resultMap.put("agMerchantId",vCardPaymentTxnResult.getAgMerchantId());
                    resultMap.put("agMerchantName",vCardPaymentTxnResult.getAgMerchantName());
                    resultMap.put("agTerminalId",vCardPaymentTxnResult.getAgTerminalId());
                    resultMap.put("txnType",vCardPaymentTxnResult.getTxnType().toString());
                    resultMap.put("walletTxnId",vCardPaymentTxnResult.getWalletTxnId());
                    resultMap.put("amount",vCardPaymentTxnResult.getAmount().toString());
                    resultMap.put("status",vCardPaymentTxnResult.getStatus().toString());
                    resultMap.put("createdOn",vCardPaymentTxnResult.getCreatedOn().toString());
                    resultMap.put("lastUpdated",vCardPaymentTxnResult.getLastUpdated().toString());
                    responseListMap.add(resultMap);
                }
            }
        }
        catch (Exception e) {
            logger.error("Error in fetching VCard txns for userId: "+fcWalletId+", error: "+e);
            responseListMap = null;
        }
        return responseListMap;
    }

    public Map<String,Object> updateStatus(String email, MojoEnums.UserStatus userStatus)
    {
        Map<String,Object> responseMap = new HashMap<String,Object>();
        boolean isUpdated = false;
        String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        logger.info("Updating status to: "+userStatus.toString()+" for userId: "+fcWalletId);
        try{
            MojoResponse mojoResponse = mojoClient.updateStatus(fcWalletId,userStatus.getUserStatusId());
            if(mojoResponse.getErrorCode().equals(STATUS_UPDATE_SUCCESS_RESPONSE_CODE))
            {
                responseMap.put(STATUS_SUCCESS,STATUS_UPDATE_SUCCESS_RESPONSE);
                logger.info("Successfully updated status to: "+userStatus.toString()+" for userId: "+fcWalletId);
            }
            else
            {
                responseMap.put(STATUS_FAILED,mojoResponse.getErrorMessage());
                throw new VCardStatusUpdateFailedException(mojoResponse.getErrorMessage());
            }
        }
        catch (Exception e) {
            logger.error("Status update failed due to unknown exception: "+e);
            responseMap = null;
        }
        return responseMap;
    }

    public boolean hasActiveVCard(String email)
    {
        String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        VCardDataResponse vCardDataResponse = null;
        boolean result = false;
        logger.info("Fetching VCard data for userId: "+fcWalletId);
        try {
            vCardDataResponse= mojoClient.getCardDataForUser(fcWalletId);
            logger.info("Successfully fetched VCard data for userId: "+fcWalletId);
            if(null!=vCardDataResponse)
            {
                for(VCardDataResult vCardDataResult: vCardDataResponse.getvCardDataResults()) {
                    if (vCardDataResult.getUserStatus() == MojoEnums.UserStatus.ACTIVE) {
                        result = true;
                        break;
                    }
                }
            }
        }
        catch(Exception e) {
            logger.error("Error in fetching VCard data for userId: "+fcWalletId+", error: "+e);
            result = true;
        }
        return result;
    }

    public boolean hasVCard(String email)
    {
        String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        VCardDataResponse vCardDataResponse = null;
        boolean result = false;
        logger.info("Fetching VCard data for userId: "+fcWalletId);
        try {
            vCardDataResponse= mojoClient.getCardDataForUser(fcWalletId);
            logger.info("Successfully fetched VCard data for userId: "+fcWalletId);
            if(null!=vCardDataResponse)
            {
                for(VCardDataResult vCardDataResult: vCardDataResponse.getvCardDataResults()) {
                    if (vCardDataResult.getUserStatus() == MojoEnums.UserStatus.ACTIVE ||
                            vCardDataResult.getUserStatus() == MojoEnums.UserStatus.BLOCKED ||
                            vCardDataResult.getUserStatus() == MojoEnums.UserStatus.INACTIVE) {
                        result = true;
                        break;
                    }
                }
            }
        }
        catch(Exception e) {
            logger.error("Error in fetching VCard data for userId: "+fcWalletId+", error: "+e);
            logger.error("Stack Trace for hasVcard() API is -> ",e);
            result = false;
        }
        return result;
    }

    public String getUserWalletIdFromCardHashId(String cardHashId)
    {
        UserFromCardResponse userFromCardResponse = null;
        logger.info("Fetching user from VCard Hash Id");
        try {
            userFromCardResponse = mojoClient.getUserFromCardHashId(cardHashId);
            logger.info("Successfully fetched user details for card hash id: "+cardHashId);
        }
        catch (Exception e) {
            logger.error("Error in fetching user details for card hash id: "+cardHashId);
            userFromCardResponse = null;
        }
        return userFromCardResponse.getWalletUserId();
    }

    public Map<String,Object> getTransactionByTxnRefId(String txnRefId) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        List<Map<String,Object>> responseListMap = new ArrayList<Map<String,Object>>();
        boolean allowReversal = true;
        VCardPaymentTxnResponse vCardPaymentTxnResponse = null;
        logger.info("Fetching VCard txn details for txnRefId: "+txnRefId);
        try {
            vCardPaymentTxnResponse = mojoClient.getTxnForAgTxnRefId(txnRefId);
            if(null != vCardPaymentTxnResponse.getPaymentTxn() && !vCardPaymentTxnResponse.getPaymentTxn().isEmpty()) {
                logger.info("Successfully fetched VCard txn details for txnRefId: " + txnRefId);
                for(VCardPaymentTxnResult vCardPaymentTxnResult: vCardPaymentTxnResponse.getPaymentTxn())
                {
                    Map<String,Object> resultMap = new HashMap<String,Object>();
                    resultMap.put("userId",vCardPaymentTxnResult.getWalletUserId());
                    resultMap.put("agCode",vCardPaymentTxnResult.getAgCode().toString());
                    resultMap.put("agUserId",vCardPaymentTxnResult.getAgUserId());
                    resultMap.put("agCardId",vCardPaymentTxnResult.getAgCardId());
                    resultMap.put("agTxnRefNum",vCardPaymentTxnResult.getAgTxnRefNum());
                    resultMap.put("agNetworkCode",vCardPaymentTxnResult.getAgNetworkCode().toString());
                    resultMap.put("agTraceNumber",vCardPaymentTxnResult.getAgTraceNumber());
                    resultMap.put("agMerchantCategoryCode",vCardPaymentTxnResult.getAgMerchantCategoryCode());
                    resultMap.put("agMerchantId",vCardPaymentTxnResult.getAgMerchantId());
                    resultMap.put("agMerchantName",vCardPaymentTxnResult.getAgMerchantName());
                    resultMap.put("agTerminalId",vCardPaymentTxnResult.getAgTerminalId());
                    if(vCardPaymentTxnResult.getTxnType() == MojoEnums.TxnType.REV) {
                        allowReversal = false;
                    }
                    resultMap.put("txnType",vCardPaymentTxnResult.getTxnType().toString());
                    resultMap.put("walletTxnId",vCardPaymentTxnResult.getWalletTxnId());
                    resultMap.put("amount",vCardPaymentTxnResult.getAmount().toString());
                    resultMap.put("status",vCardPaymentTxnResult.getStatus().toString());
                    resultMap.put("createdOn",vCardPaymentTxnResult.getCreatedOn().toString());
                    resultMap.put("lastUpdated",vCardPaymentTxnResult.getLastUpdated().toString());
                    responseListMap.add(resultMap);
                }
            }
        }
        catch (Exception e) {
            logger.error("Error in fetching VCard txn details for txnRefId: "+txnRefId+", exception: "+e);
            responseListMap = null;
        }
        responseMap.put(TXN_LIST_STRING, responseListMap);
        responseMap.put(ALLOW_REVERSAL_STRING, allowReversal);
        return responseMap;
    }

    private Map<String, Object> refundVCardTxn(String txnRefId, String cardHashId, Double refundAmount) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        FundPostRefundResponse virtualCardRefundResponse = new FundPostRefundResponse();
        RestTemplate restTemplate = new RestTemplate();
        JSONObject jsonRequestObject = new JSONObject();
        try {
            jsonRequestObject.put(TRANSACTION_REF_NO_STRING, txnRefId);
            jsonRequestObject.put(REFUND_AMOUNT_STRING, refundAmount);
            jsonRequestObject.put(CARD_HASH_ID_STRING, cardHashId);
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonRequest = jsonRequestObject.toString();
            HttpClient client = new HttpClient();
            PostMethod postMethod = new PostMethod(fcProperties.getProperty(FCProperties.FINAPP_VC_REFUND_URL));
            StringRequestEntity requestEntity = new StringRequestEntity(jsonRequest, "application/json", "UTF-8");
            postMethod.setRequestEntity(requestEntity);
            int statusCode = client.executeMethod(postMethod);
            String refundResponse = postMethod.getResponseBodyAsString();
            if(statusCode==200){
                virtualCardRefundResponse = objectMapper.readValue(refundResponse,
                        FundPostRefundResponse.class);
                responseMap.put(VC_REFUND_STATUS_STRING, STATUS_SUCCESS);
                responseMap.put(VC_REFUND_MESSAGE_STRING, "Successfully refunded to cardHashId: " + cardHashId);
            }else{
                JSONObject response = new JSONObject(refundResponse);
                JSONObject exceptionObj = response.getJSONObject("exception");
                FundPostException exception = objectMapper.readValue(exceptionObj.toString(),FundPostException.class);
                throw exception;
            }
        }
        catch (JSONException jsonException) {
            String error = "Error in parsing JSON request for txnRefID: "+txnRefId;
            logger.error(error, jsonException);
            responseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            responseMap.put(VC_REFUND_MESSAGE_STRING, error);
        }
        catch (UnsupportedEncodingException e) {
            String error = "Error in encoding JSON request to String for txnRefID: "+txnRefId;
            logger.error(error, e);
            responseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            responseMap.put(VC_REFUND_MESSAGE_STRING, error);
        }
        catch (JsonParseException e) {
            String error = "Error in parsing JSON response for txnRefID: "+txnRefId;
            logger.error(error, e);
            responseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            responseMap.put(VC_REFUND_MESSAGE_STRING, error);
        }
        catch (FundPostException exception) {
            String error = "Error in refunding VC txn from Fundpost for txnRefId: "+txnRefId;
            logger.error(error, exception);
            responseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            responseMap.put(VC_REFUND_MESSAGE_STRING, error);
        }
        catch (Exception e) {
            String error = "Error in refunding VC txn for txnRefId: "+txnRefId;
            logger.error(error, e);
            responseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            responseMap.put(VC_REFUND_MESSAGE_STRING, error);
        }
        return responseMap;
    }

    public Map<String, Object> refundVCardTxnByTxnRefId(String txnRefId) {
        Map<String, Object> refundResponseMap = new HashMap<String, Object>();
        Map<String,Object> transactionMap = getTransactionByTxnRefId(txnRefId);
        if(null != transactionMap && !transactionMap.isEmpty()) {
            List<Map<String, Object>> transactionList = (List<Map<String, Object>>) transactionMap.get(TXN_LIST_STRING);
            if(null != transactionList && !transactionList.isEmpty()) {
                String cardHashId = (String) transactionList.get(0).get("agCardId");
                Double refundAmount = Double.parseDouble((String)transactionList.get(0).get("amount"));
                String userId = (String) transactionList.get(0).get("userId");
                boolean isRefunded = false;
                try {
                    List<TransactionSummary> walletTransactions = oneCheckWalletService.findTransactionsByReference(txnRefId, userId);
                    if(null != walletTransactions && !walletTransactions.isEmpty()) {
                        for (TransactionSummary transactionSummary : walletTransactions) {
                            if (transactionSummary.getTransactionType() == TransactionType.CREDIT_REFUND) {
                                isRefunded = true;
                            }
                        }
                        if(!isRefunded){
                            return refundVCardTxn(txnRefId, cardHashId, refundAmount);
                        }
                        else {
                            refundResponseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
                            refundResponseMap.put(VC_REFUND_MESSAGE_STRING, "Wallet already refunded for txnRefId: "
                                    +txnRefId);
                        }
                    }
                    else {
                        refundResponseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
                        refundResponseMap.put(VC_REFUND_MESSAGE_STRING, "Wallet debit transaction not found for txnRefId: "
                                +txnRefId);
                    }
                }
                catch(Exception e) {
                    refundResponseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
                    refundResponseMap.put(VC_REFUND_MESSAGE_STRING, "Exception in fetching wallet transactions for " +
                            "txnRefId: "+txnRefId);
                }
            }
            else {
                refundResponseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
                refundResponseMap.put(VC_REFUND_MESSAGE_STRING, "No Vcard transactions found for " +
                        "txnRefId: "+txnRefId);
            }
        }
        else {
            refundResponseMap.put(VC_REFUND_STATUS_STRING, STATUS_FAILED);
            refundResponseMap.put(VC_REFUND_MESSAGE_STRING, "Exception in fetching vcard transactions for " +
                    "txnRefId: "+txnRefId);
        }
        return refundResponseMap;
    }
}