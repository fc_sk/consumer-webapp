package com.freecharge.virtualcard.model;

/**
 * Created by Vivek on 29/01/16.
 */
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.snapdeal.payments.sdmoney.service.model.RefundEntityBalance;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundPostRefundResponse {

    private String transactionId;
    private java.util.Date transactionTimestamp;
    private java.util.List<RefundEntityBalance> listEntity;
    private BigDecimal runningBalance;
    @JsonProperty("transactionId")
    public String getTransactionId() {
        return transactionId;
    }
    @JsonProperty("transactionTimestamp")
    public java.util.Date getTransactionTimestamp() {
        return transactionTimestamp;
    }
    @JsonProperty("listEntity")
    public java.util.List<RefundEntityBalance> getListEntity() {
        return listEntity;
    }
    @JsonProperty("runningBalance")
    public BigDecimal getRunningBalance() {
        return runningBalance;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setTransactionTimestamp(java.util.Date transactionTimestamp) {
        this.transactionTimestamp = transactionTimestamp;
    }

    public void setListEntity(java.util.List<RefundEntityBalance> listEntity) {
        this.listEntity = listEntity;
    }

    public void setRunningBalance(BigDecimal runningBalance) {
        this.runningBalance = runningBalance;
    }


    public String toString() {
        return "FundPostRefundResponse(transactionId=" + getTransactionId() + ", transactionTimestamp="
                + getTransactionTimestamp() + ", listEntity=" + getListEntity() + ", runningBalance="
                + getRunningBalance() + ")";
    }

}

