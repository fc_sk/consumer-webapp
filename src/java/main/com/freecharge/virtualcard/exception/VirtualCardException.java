package com.freecharge.virtualcard.exception;

/**
 * Created by Vivek on 19/11/15.
 */
public class VirtualCardException extends RuntimeException {

    public VirtualCardException (String errorMsg) { super(errorMsg); }
    public VirtualCardException (Exception e) {
        super(e);
    }
}
