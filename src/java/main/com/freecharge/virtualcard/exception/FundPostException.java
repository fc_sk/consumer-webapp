package com.freecharge.virtualcard.exception;

/**
 * Created by Vivek on 02/02/16.
 */

public class FundPostException extends Exception {
    private static final long serialVersionUID = 41L;

    public FundPostException(String msg){
        super(msg);
    }

    public FundPostException(String msg, Throwable cause){
        super(msg, cause);
    }
}
