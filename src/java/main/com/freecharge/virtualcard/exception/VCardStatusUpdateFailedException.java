package com.freecharge.virtualcard.exception;

/**
 * Created by Vivek on 20/11/15.
 */
public class VCardStatusUpdateFailedException extends VirtualCardException{
    public VCardStatusUpdateFailedException (String errorMsg) { super(errorMsg); }
    public VCardStatusUpdateFailedException (Exception e) {
        super(e);
    }
}
