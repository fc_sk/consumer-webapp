package com.freecharge.eventprocessing.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.coupon.service.CouponServiceProxy;
import com.freecharge.eventprocessing.event.FulfilmentEvent;
import com.freecharge.order.service.IOrderMetaDataService;
import com.freecharge.order.service.model.OrderMetaData;

/**
 * This fulfilment event listener records the coupon counts in mongo db. Which are later used for coupon fraud control.
 * @author shirish
 *
 */
@Component
public class CouponOptinEventListener implements ApplicationListener<FulfilmentEvent> {
    private Logger logger = LoggingFactory.getLogger(CouponOptinEventListener.class);

    @Autowired
    private IOrderMetaDataService orderMetaDataService;
    
    @Autowired
	private AppConfigService appConfigService;
    
    @Autowired
    @Qualifier("couponServiceProxy")
    private ICouponService couponServiceProxy;
    
    @Override
    public void onApplicationEvent(FulfilmentEvent fulfilmentEvent) {
        try {
            if (fulfilmentEvent.isSuccessfulRecharge()) {
            	
            	List<Map<String, Object>> eCouponsData = fulfilmentEvent.geteCoupons();
               if (eCouponsData != null &&  !eCouponsData.isEmpty()){
            	   updateCouponOptinCounts(eCouponsData, fulfilmentEvent.getOrderId());
               }
            }
        } catch (Exception e) {
            logger.error("Exception occured while recording coupon optin count for fulfilmentevent - " + fulfilmentEvent, e);
        }
    }
    
    private void updateCouponOptinCounts(
			List<Map<String, Object>> eCouponsData, String orderId) {
    	if(!appConfigService.isCouponFraudCheckEnabled()){
    		return;
    	}
    	Map<Integer, Integer> couponQuantityMap = new HashMap<>();
		for (Map<String, Object> couponData : eCouponsData){
			if (couponData != null){
				String couponStoreIdStr = FCUtil.getStringValue(couponData.get("couponStoreId"));
				if (couponStoreIdStr != null){
					String countStr = FCUtil.getStringValue(couponData.get("count"));
					if (FCUtil.isInteger(countStr)  && FCUtil.isInteger(couponStoreIdStr)){
						int count = 1;
						int couponStoreId = Integer.parseInt(couponStoreIdStr);
						Integer quantity = couponQuantityMap.get(couponStoreId);
						if (quantity == null){
							quantity = 0;
						}
						quantity = quantity + count;
						couponQuantityMap.put(couponStoreId, quantity);
					} // end if
				} // end if
			} else{
				logger.error("Coupon data null while saving coupon history for order id: " + orderId);
			} // end else
		} // end for
		
		OrderMetaData orderMetaData = orderMetaDataService.getOrderMetaData(orderId);
		if (orderMetaData != null){
			couponServiceProxy.recordCouponOptinCount(couponQuantityMap, orderMetaData.getUserCookie(), 
					orderMetaData.getUserEmail(), orderMetaData.getProfileNo(), orderMetaData.getIpAddress(),orderMetaData.getImeiNumber(), Integer.toString(orderMetaData.getUserId()));	
		} else{
			logger.warn("OrderMetaData is null for orderId: " + orderId);
		}
    	
	}
}
