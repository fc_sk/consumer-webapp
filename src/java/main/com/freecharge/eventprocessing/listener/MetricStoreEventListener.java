package com.freecharge.eventprocessing.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.centaur.CentaurServiceProxy;
import com.freecharge.centaur.service.common.RequestType;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.eventprocessing.event.MetricStoreEvent;

public class MetricStoreEventListener implements ApplicationListener<MetricStoreEvent>{
	private final Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

//	@Autowired
//    private CentaurServiceProxy centaurService;

	@Override 
	public void onApplicationEvent(MetricStoreEvent metricStoreEvent) {

		try{
			List<BlockedCoupon> blockedCouponList = couponServiceProxy.getBlockedCouponListForOrder(metricStoreEvent.getOrderId()); 
			Double ordersWithCoupon = (double) 0;
			Double couponCount = (double) 0;
			Map<Integer, Double> metricValues = new HashMap<Integer, Double>();
			if(blockedCouponList!=null && blockedCouponList.size() > 0) {
				ordersWithCoupon++;
				for(BlockedCoupon blockedCoupon : blockedCouponList) {
					List<BlockedCouponCode> blockedCodes = blockedCoupon.getBlockedCodes();
					couponCount = couponCount + blockedCodes.size();
				}
				metricValues.put(metricStoreEvent.getCouponOrderCountId(),ordersWithCoupon);
				metricValues.put(metricStoreEvent.getTotalCouponCountId(), couponCount);
			}
			metricValues.put(metricStoreEvent.getOrderCountId(),(double)1);
			Integer bucketId =  null;
//					centaurService.getBucketId(RequestType.VISIT, metricStoreEvent.getExperimentId(), FCSessionUtil.getIpAddress());
			logger.error("bucketId: "+bucketId);
//			centaurService.recordData(null, bucketId, null, metricValues);
		} catch (Exception e) {
			logger.error("Exception caught while triggering store metric event");
		}

	}

}
