package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

public class MetricStoreEvent  extends ApplicationEvent{
	public MetricStoreEvent(Object source)  {
        super(source);
    }
	
	private String orderId;
	private int couponOrderCountId;
	private int totalCouponCountId;
	private int orderCountId;
	private int experimentId;
	
	
	public int getExperimentId() {
		return experimentId;
	}
	public void setExperimentId(int experimentId) {
		this.experimentId = experimentId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public int getCouponOrderCountId() {
		return couponOrderCountId;
	}
	public void setCouponOrderCountId(int couponOrderCountId) {
		this.couponOrderCountId = couponOrderCountId;
	}
	public int getTotalCouponCountId() {
		return totalCouponCountId;
	}
	public void setTotalCouponCountId(int totalCouponCountId) {
		this.totalCouponCountId = totalCouponCountId;
	}
	public int getOrderCountId() {
		return orderCountId;
	}
	public void setOrderCountId(int orderCountId) {
		this.orderCountId = orderCountId;
	}

	

	
	
}