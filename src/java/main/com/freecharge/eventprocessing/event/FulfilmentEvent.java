package com.freecharge.eventprocessing.event;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationEvent;

/**
 * User: abhi
 * Date: 2/12/13
 * Time: 1:22 PM
 */
public class FulfilmentEvent extends ApplicationEvent {
    public FulfilmentEvent(Object source) {
        super(source);
    }

    private int rechargeAmount;
    private String rechargeNumber;
    private List<Map<String, Object>> physicalCoupons;
    private List<Map<String, Object>> eCoupons;
    private String productType;
    private String orderId;
    private Integer couponDeliveryCityId;
    private boolean successfulRecharge;
    private Date txnFulfilmentCreatedTime;

    public int getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(int rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(String rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

    public List<Map<String, Object>> getPhysicalCoupons() {
        return physicalCoupons;
    }

    public void setPhysicalCoupons(List<Map<String, Object>> physicalCoupons) {
        this.physicalCoupons = physicalCoupons;
    }

    public List<Map<String, Object>> geteCoupons() {
        return eCoupons;
    }

    public void seteCoupons(List<Map<String, Object>> eCoupons) {
        this.eCoupons = eCoupons;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCouponDeliveryCityId() {
        return couponDeliveryCityId;
    }

    public void setCouponDeliveryCityId(Integer couponDeliveryCityId) {
        this.couponDeliveryCityId = couponDeliveryCityId;
    }

    public boolean isSuccessfulRecharge() {
        return successfulRecharge;
    }

    public void setSuccessfulRecharge(boolean successfulRecharge) {
        this.successfulRecharge = successfulRecharge;
    }

    public Date getTxnFulfilmentCreatedTime() {
        return txnFulfilmentCreatedTime;
    }

    public void setTxnFulfilmentCreatedTime(Date txnFulfilmentCreatedTime) {
        this.txnFulfilmentCreatedTime = txnFulfilmentCreatedTime;
    }

    @Override
    public String toString() {
        return "FulfilmentEvent{" +
                "rechargeAmount=" + rechargeAmount +
                ", rechargeNumber='" + rechargeNumber + '\'' +
                ", physicalCoupons=" + physicalCoupons +
                ", eCoupons=" + eCoupons +
                ", productType='" + productType + '\'' +
                ", orderId='" + orderId + '\'' +
                ", couponDeliveryCityId=" + couponDeliveryCityId +
                ", successfulRecharge=" + successfulRecharge +
                ", txnFulfilmentCreatedTime=" + txnFulfilmentCreatedTime +
                '}';
    }
}
