package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

/**
 * User: abhi
 * Date: 6/12/13
 * Time: 3:34 PM
 */
public class FreeFundApplicationEvent extends ApplicationEvent {
    private int userId;
    private int freeFundClassId;

    public FreeFundApplicationEvent(Object source) {
        super(source);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFreeFundClassId() {
        return freeFundClassId;
    }

    public void setFreeFundClassId(int freeFundClassId) {
        this.freeFundClassId = freeFundClassId;
    }

    @Override
    public String toString() {
        return "FreeFundApplicationEvent{" +
                "userId=" + userId +
                ", freeFundClassId=" + freeFundClassId +
                '}';
    }
}
