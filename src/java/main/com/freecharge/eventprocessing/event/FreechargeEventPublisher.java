package com.freecharge.eventprocessing.event;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.eventprocessing.FulfillmentEvent;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.useragent.UserAgentController;

@Service
public class FreechargeEventPublisher implements ApplicationEventPublisherAware {
   
    private Logger logger = LoggingFactory.getLogger(getClass());
    
	private ApplicationEventPublisher publisher;
    @Autowired
    protected MetricsClient metricsClient;
    
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		long startTime = System.currentTimeMillis();
		this.publisher = publisher;
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency("FreechargeEventPublisher", "publishTime", endTime - startTime);
	}

	public void publish(ApplicationEvent event) {
        if (event instanceof UserLoginEvent) {
            metricsClient.recordEvent(UserAgentController.METRIC_SERVICE_NAME, UserLoginEvent.METRICS_EVENT_PUBLISH, "eventPublished");
        }

		this.publisher.publishEvent(event);
	}
	
	public void publishRechargeFailureEvent(String orderId, String mobileNumber) {
        RechargeFailureEvent rechargeFailureEvent = new RechargeFailureEvent(this);
        rechargeFailureEvent.setOrderId(orderId);
        rechargeFailureEvent.setNumber(mobileNumber);
        logger.info("Publishing recharge failure event for order Id : " + orderId);
        this.publish(rechargeFailureEvent);
    }

    /**
     * Publishes RechargeSuccessEvent
     * 
     * @param rechargeInfo
     *            SuccessfulRechargeInfo
     */
    public void publishRechargeSuccessEvent(SuccessfulRechargeInfo rechargeInfo, String lookupId) {
        RechargeSuccessEvent rechargeSuccessEvent = new RechargeSuccessEvent(this);
        rechargeSuccessEvent.setNumber(rechargeInfo.getMobileNo());
        rechargeSuccessEvent.setOrderId(rechargeInfo.getOrderID());
        rechargeSuccessEvent.setAmount(rechargeInfo.getAmount());
        rechargeSuccessEvent.setLookupId(lookupId);
        logger.info("Publishing recharge success event for order Id : " + rechargeInfo.getOrderID());
        this.publish(rechargeSuccessEvent);
    }
    
    public void publishFulfillmentEvent(TransactionStatus transactionStatus, UserDetails userDetails) {
       try {
    	   FulfillmentEvent fulfillmentEvent = new FulfillmentEvent(this);
           fulfillmentEvent.setTransactionStatus(transactionStatus);
           fulfillmentEvent.setUserDetails(userDetails);
           logger.info("Publishing fulfillment event");
           this.publish(fulfillmentEvent);
           logger.info("Published fulfillment event");
       }catch(Exception ex) {
    	   logger.error("Exception publishing FulfillmentEvent : ",ex);
       }
      
    }

}