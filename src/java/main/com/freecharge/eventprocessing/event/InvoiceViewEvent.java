package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

/**
 * User: abhi
 * Date: 10/12/13
 * Time: 5:27 PM
 */
public class InvoiceViewEvent extends ApplicationEvent {
    public InvoiceViewEvent(Object source) {
        super(source);
    }

    private int userId;
    private boolean viewed;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    @Override
    public String toString() {
        return "InvoiceViewEvent{" +
                "userId=" + userId +
                ", viewed=" + viewed +
                '}';
    }
}
