package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

public class PaymentSuccessEvent extends ApplicationEvent {

	private static final long serialVersionUID = -5308299518665062983L;

	private String orderId;
	
	private String lookupId;

	private String sessionId;

	public PaymentSuccessEvent(Object source) {
		super(source);
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getLookupId() {
		return lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}