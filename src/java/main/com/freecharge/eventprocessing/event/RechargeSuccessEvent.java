package com.freecharge.eventprocessing.event;

import com.freecharge.common.util.Amount;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/27/13
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class RechargeSuccessEvent extends FreefundBaseEvent {
    private String orderId;

    private String lookupId;

    private String sessionId;

    private String number;

    private Amount amount;

    public RechargeSuccessEvent(Object source) {
        super(source);
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
