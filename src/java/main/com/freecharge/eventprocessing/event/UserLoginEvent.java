package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

/**
 * User: abhi
 * Date: 8/11/13
 * Time: 1:49 PM
 */
public class UserLoginEvent extends ApplicationEvent {
    public static final String LOGGER_PREFIX = "USER_AGENT";

    public static final String METRICS_EVENT_PUBLISH = "eventPublish";

    private String userAgent;
    private int userId;

    public UserLoginEvent(Object source) {
        super(source);
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
