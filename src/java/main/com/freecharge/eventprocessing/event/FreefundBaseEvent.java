/**
 * 
 */
package com.freecharge.eventprocessing.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author ravi
 *
 */
public class FreefundBaseEvent extends ApplicationEvent {

    private static final long serialVersionUID = -1135517643800167150L;

    public FreefundBaseEvent(Object source) {
        super(source);
    }

}
