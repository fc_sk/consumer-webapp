package com.freecharge.promo.util;

import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.fraud.FreefundFraudConstant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PromocodeConstants {

    public static final List<String> PROMOCODE_UNIQUE_MAPPING_KEY_LIST = new ArrayList<>(Arrays.asList(
            FreefundFraudConstant.EMAIL,
            FCConstants.IMEI_NUMBER,
            FreefundFraudConstant.SERVICE_NUMBER,
            FreefundFraudConstant.PROFILE_ID));
    public static final List<String> PROMOCODE_DISCOUNT_TYPE_LIST = new ArrayList<>(Arrays.asList("FLAT", "PERCENT"));
    public static final List<String> PROMOCODE_TYPE_LIST = new ArrayList<>(
            Arrays.asList("generic-discount-promocode", "generic-cashback-promocode",
                    "all-payment-options", "redeem-to-fcbalance", "cashback-promocode"));
    public static final List<String> PROMOCODE_SPONSORSHIP_LIST = new ArrayList<>(
            Arrays.asList("INTERNAL", "EXTERNAL", "CLIENT_SPONSORED", "FREECHARGE_SPONSORED", "CO_PAID"));
    public static final String PROMOCODE_GENERATION_MAX_RETRY = "promocode.generation.max.retry";
    public static final String PROMO_DISTRIBUTION_API_CODES = "promoDistributionApiCodes";
    public static final String MSG_UNIQUE_KEY_VALUE_EMAIL_FRAUD = "msg.unique.key.value.email.fraud";
    public static final String MSG_UNIQUE_KEY_VALUE_PHONE_FRAUD = "msg.unique.key.value.phone.fraud";
    public static final String PROMOCODE_SERVICE = "PromocodeService";
    public static final String MAPPING_KEY = "mappingKey";
    public static final String KEY_VALUE = "keyValue";
    public static final String PROMOCODE = "promocode";
    public static final String DUPLICATE_REQUEST_ID = "duplicateRequestId";
    public static final int MAX_GENERATE_ATTEMPTS_REISSUE_PROMOCODE = 3;
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String ERROR_CODE = "errorCode";
    public static final String ORDER_ID = "order_id";
    public static final String REISSUED_PROMOCODE_ID = "reissued_promocode_id";
    public static final String PARENT_PROMOCODE_ID = "parent_promocode_id";
    public static final String PARENT_FREEFUND_CLASS_ID = "parent_freefund_class_id";
    public static final String EXTEND_VALID_UPTO_BY_DAYS = "extend.failed.codes.validitybydays";
    public static final String RECHARGE_FAILURE_REISSUE_PROMOCODE_FROM = "reissue.promocode.from";
    public static final String PROMO_SPONSORSHIP_INTERNAL = "INTERNAL";

    public static final String SUCCESS = "Success";
    public static final String ADDITIONAL_REWARD = "additionalReward";
    public static float ZERO_MAX_DISCOUNT = 0;
    public static float ZERO_DISCOUNT = 0;
    public static long NO_CONDITION_SET = -1;
    public static final Map<String,String> promocodeCampaignMappingKeys = FCUtil.createMap(FreefundFraudConstant.SERVICE_NUMBER, "phone_no", FreefundFraudConstant.ORDERID, "orderid", FreefundFraudConstant.EMAIL, "fk_user_id");

    public static final String REQUEST_ID = "requestId";
    public static final String CAMPAIGN_ID = "campaignId";
    public static final String AFFILIATE_ID = "affiliateId";
    public static final String PROMO_TYPE = "promoType";
    public static final String PROMO_SPONSORSHIP = "promoSponsorship";
    public static final String PREFIX = "prefix";
    public static final String CODE_LENGTH = "codeLength";
    public static final String DISCOUNT_TYPE = "discountType";
    public static final String DISCOUNT_VALUE = "discountValue";
    public static final String MAX_DISCOUNT_VALUE = "maxDiscountValue";
    public static final String MIN_RECHARGE_VALUE = "minRechargeValue";
    public static final String APPLY_CONDITION_ID = "applyConditionId";
    public static final String PAYMENT_CONDITION_ID = "paymentConditionId";
    public static final String REDEEM_CONDITON_ID = "redeemConditionId";
    public static final String SUCCESS_MESSAGE_ON_APPLY = "successMessageApply";
    public static final String SUCCESS_MESSAGE_ON_RECHARGE = "successMessageRecharge";
    public static final String UNIQUE_MAPPING_KEY = "mappingKey";
    public static final String UNIQUE_MAPPING_VALUE = "keyValue";
    public static final String DESCRIPTION = "description";
    public static final String VALID_FROM = "validFrom";
    public static final String VALID_UPTO = "validUpto";
    public static final String FORCE_CREATE = "forceCreate";
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String STATUS_PENDING = "Pending";
    public static final String STATUS_UNDER_PROCESS = "UnderProcess";
    public static final String STATUS_COMPLETE = "Completed";
    public static final String RESPONSE_ID = "responseId";
    private static final String MAX_REDEMPTIONS = "maxRedemptions";
    public static final String FREEFUND_VALUE = "freefundValue";
    public static final String PROMO_ENTITY = "promoEntity";
    public static final String MAX_DISCOUNT = "maxDiscount";
    public static final String SUCCESS_MESSAGE = "successMessage";
    public static final String RECHARGE_SUCCESS_MESSAGE = "rechargeSuccessMessage";
    public static final String CAMPAIGN_SPONSORSHIP = "campaignSponsorship";
    public static final Map<String, String> promocodeFreefundMap = FCUtil
            .createMap(DESCRIPTION, DESCRIPTION,
                    MAX_REDEMPTIONS, MAX_REDEMPTIONS, MIN_RECHARGE_VALUE, MIN_RECHARGE_VALUE, DISCOUNT_VALUE,
                    FREEFUND_VALUE, PROMO_TYPE, PROMO_ENTITY, VALID_FROM, VALID_FROM, VALID_UPTO, VALID_UPTO,
                    DISCOUNT_TYPE, DISCOUNT_TYPE, MAX_DISCOUNT_VALUE, MAX_DISCOUNT, APPLY_CONDITION_ID,
                    APPLY_CONDITION_ID, PAYMENT_CONDITION_ID, PAYMENT_CONDITION_ID, REDEEM_CONDITON_ID,
                    REDEEM_CONDITON_ID, PREFIX, PREFIX, CODE_LENGTH, CODE_LENGTH, SUCCESS_MESSAGE_ON_APPLY,
                    SUCCESS_MESSAGE, SUCCESS_MESSAGE_ON_RECHARGE, RECHARGE_SUCCESS_MESSAGE, PROMO_SPONSORSHIP,
                    CAMPAIGN_SPONSORSHIP);
}
