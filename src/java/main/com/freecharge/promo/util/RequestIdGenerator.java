package com.freecharge.promo.util;

import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.SecureRandom;

@Service("requestIdGenerator")
public class RequestIdGenerator {

    private SecureRandom random;

    /*SecureRandom objects are expensive to initialize. Hence Init.*/
    public void init() {
        random = new SecureRandom();
    }

    /**
     *  This works by choosing 130 bits from a cryptographically secure random bit generator, and encoding them in base-32.
     *  128 bits is considered to be cryptographically strong, but each digit in a base 32 number can encode 5 bits,
     *  so 128 is rounded up to the next multiple of 5.
     *  This encoding is compact and efficient, with 5 random bits per character.
     *  Compare this to a random UUID, which only has 3.4 bits per character in standard layout, and only 122 random bits in total.
     */
    public String nextRequestId() {
        return new BigInteger(100, random).toString(32);
    }
}
