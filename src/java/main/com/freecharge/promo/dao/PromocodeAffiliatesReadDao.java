package com.freecharge.promo.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.AffiliateDataRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeAffiliates;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeAffiliatesReadDao {
    private static Logger logger = LoggingFactory.getLogger(PromocodeAffiliatesReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PromocodeAffiliates> getAffiliateForId(String affiliateId) {
        List<PromocodeAffiliates> promocodeAffiliatesList = new ArrayList<>();
        try{
            String queryString = "select id, affiliate_id, affiliate_name, affiliate_type, created_at, created_by, " +
                    "updated_at, updated_by from promocode_affiliates where affiliate_id=:affiliateId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("affiliateId", affiliateId);

            promocodeAffiliatesList = this.jdbcTemplate.query(queryString, paramMap, new AffiliateDataRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch affiliate details for affiliateId " + affiliateId, e);
        }
        return promocodeAffiliatesList;
    }
}
