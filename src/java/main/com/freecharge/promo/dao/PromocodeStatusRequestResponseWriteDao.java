package com.freecharge.promo.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeStatusRequestResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class PromocodeStatusRequestResponseWriteDao {
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertPromoStatusRequestResponse;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromoStatusRequestResponse = new SimpleJdbcInsert(dataSource).withTableName(
                "promocode_status_request_response").usingGeneratedKeyColumns("id","n_last_updated", "n_created");
    }

    public int insert(PromocodeStatusRequestResponse promocodeStatusRequestResponse) {
        SqlParameterSource parameters = promocodeStatusRequestResponse.getMapSqlParameterSource();
        Number returnParam = this.insertPromoStatusRequestResponse.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }
}
