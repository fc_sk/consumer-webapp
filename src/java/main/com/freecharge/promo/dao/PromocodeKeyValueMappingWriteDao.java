package com.freecharge.promo.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeKeyValueMapping;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Service
public class PromocodeKeyValueMappingWriteDao {

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertPromocodeKeyValueMapping;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocodeKeyValueMapping = new SimpleJdbcInsert(dataSource).withTableName(
                "promocode_key_value_mapping").usingGeneratedKeyColumns("id","n_last_updated", "n_created");
    }

    public int insert(PromocodeKeyValueMapping promocodeKeyValueMapping){
        SqlParameterSource parameters = promocodeKeyValueMapping.getMapSqlParameterSource();
        Number returnParam = this.insertPromocodeKeyValueMapping.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public int delete(long id) {
        String sql = "delete from promocode_key_value_mapping where id =: id";
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        return this.jdbcTemplate.update(sql, params);
    }
}
