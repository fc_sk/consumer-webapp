package com.freecharge.promo.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.PromocodeGenerationRequestResponseRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeGenerationRequestResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeGenerationRequestResponseReadDao {
    private static Logger logger = LoggingFactory.getLogger(PromocodeGenerationRequestResponseReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PromocodeGenerationRequestResponse> getPromocodeGenerationRequestResponseMappingsForPromocodeId
            (long promocodeId) {
        List<PromocodeGenerationRequestResponse> promocodeGenerationRequestResponseList = null;
        try{
            String queryString = "Select id,promocode_id,request_id,request_data,response_id,response_data,created_at from " +
                    "promocode_generation_request_response where promocode_id=:promocodeId";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("promocodeId", promocodeId);
            promocodeGenerationRequestResponseList = this.jdbcTemplate.query(queryString, paramMap,
                    new PromocodeGenerationRequestResponseRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch promo affiliation request response details for promocodeId: " + promocodeId, e);
        }
        return promocodeGenerationRequestResponseList;
    }

    public List<PromocodeGenerationRequestResponse> getPromocodeGenerationRequestResponseMappingsForRequestId(String requestId) {
        List<PromocodeGenerationRequestResponse> promocodeGenerationRequestResponseList = null;
        try{
            String queryString = "Select id,promocode_id,request_id,request_data,response_id,response_data,created_at from " +
                    "promocode_generation_request_response where request_id=:requestId";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("requestId", requestId);
            promocodeGenerationRequestResponseList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeGenerationRequestResponseRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch promo affiliation request response details for requestId: " + requestId, e);
        }
        return promocodeGenerationRequestResponseList;
    }

    public List<PromocodeGenerationRequestResponse> getPromocodeGenerationRequestResponseMappingsForResponseId(String responseId) {
        List<PromocodeGenerationRequestResponse> promocodeGenerationRequestResponseList = null;
        try{
            String queryString = "Select id,promocode_id,request_id,request_data,response_id,response_data,created_at " +
                    "from promocode_generation_request_response where response_id=:responseId";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("responseId", responseId);
            promocodeGenerationRequestResponseList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeGenerationRequestResponseRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch promo affiliation request response details for response id: " + responseId, e);
        }
        return promocodeGenerationRequestResponseList;
    }

}
