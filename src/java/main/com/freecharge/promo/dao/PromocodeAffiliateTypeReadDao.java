package com.freecharge.promo.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeAffiliateTypeReadDao {
    private static Logger logger = LoggingFactory.getLogger(PromocodeReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public String getConfigurablePromocodeProperties(String affiliateType) {
        String query = "Select configurable_properties from promocode_affiliate_type where affiliate_type =:affiliateType";
        Map<String, Object> params = new HashMap<>();
        params.put("affiliateType", affiliateType);
        try {
            List<Map<String, Object>> list = jdbcTemplate.queryForList(query, params);
            if (!FCUtil.isEmpty(list)) {
                return (String) list.get(0).get("configurable_properties");
            }
            return null;
        } catch (DataAccessException e) {
            logger.error("Error while fetching data", e);
            return null;
        }
    }
}

