package com.freecharge.promo.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeGenerationRequestResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class PromocodeGenerationRequestResponseWriteDao {

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertPromocodeGenerationRequestResponse;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocodeGenerationRequestResponse = new SimpleJdbcInsert(dataSource).withTableName(
                "promocode_generation_request_response").usingGeneratedKeyColumns("id","n_last_updated", "n_created");
    }

    public int insert(PromocodeGenerationRequestResponse promocodeGenerationRequestResponse){
        SqlParameterSource parameters = promocodeGenerationRequestResponse.getMapSqlParameterSource();
        Number returnParam = this.insertPromocodeGenerationRequestResponse.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

}
