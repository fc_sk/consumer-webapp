package com.freecharge.promo.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.PromocodeKeyValueMappingRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeKeyValueMapping;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeKeyValueMappingReadDao {

    private static Logger logger = LoggingFactory.getLogger(PromocodeKeyValueMappingReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PromocodeKeyValueMapping> getPromocodeKeyValueMapping(long promocodeId) {
        List<PromocodeKeyValueMapping> promocodeKeyValueMappingList = null;
        try{
            String queryString = "Select id, promocode_id, mapping_key, key_value from promocode_key_value_mapping " +
                    "where promocode_id=:promocodeId";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("promocodeId", promocodeId);
            promocodeKeyValueMappingList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeKeyValueMappingRowMapper());
        } catch (DataAccessException e){
            logger.error("Failed to fetch Promocode key value mapping details", e);
        }
        return promocodeKeyValueMappingList;
    }

    public List<PromocodeKeyValueMapping> getPromocodeKeyValueMappingWithStatus(String mappingKey, String keyValue, String status) {
        List<PromocodeKeyValueMapping> promocodeKeyValueMappingList = null;
        try{
            String queryString = "select pkvm.id, pkvm.promocode_id, pkvm.mapping_key, pkvm.key_value from promocode_key_value_mapping pkvm " +
                    "inner join promocode p on pkvm.promocode_id = p.promocode_id where pkvm.mapping_key =:mappingKey " +
                    "and pkvm.key_value =:keyValue and p.current_status =:status";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("status", status);
            paramMap.put("mappingKey", mappingKey);
            paramMap.put("keyValue", keyValue);
            promocodeKeyValueMappingList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeKeyValueMappingRowMapper());
        } catch (DataAccessException e){
            logger.error("Failed to fetch Promocode key value mapping details", e);
        }
        return promocodeKeyValueMappingList;
    }
}
