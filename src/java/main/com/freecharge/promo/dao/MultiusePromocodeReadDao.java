package com.freecharge.promo.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MultiusePromocodeReadDao {
    private static Logger logger = LoggingFactory.getLogger(MultiusePromocodeReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Map<String, Object>> getPromocodeBlockedOrRedeemedStatus(String promocode) {
        try {
            String sqlQuery = "select status, created_date as updated_at " +
                    "from multiuse_promocode where promocode =:promocode and status is not null";
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("promocode", promocode);
            return this.jdbcTemplate.queryForList(sqlQuery, paramsMap);
        } catch (DataAccessException e) {
            logger.error("Error while fetching status for promocode: " + promocode + " from multiuse_promocode" + e);
            return null;
        }
    }

    public List<Map<String, Object>> getMultiusePromocodeDetailStatusForOrder(String promocode, String orderId) {
        try {
            String sqlQuery = "select status from multiuse_promocode where promocode =:promocode and order_id =:orderId";
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("promocode", promocode);
            paramsMap.put("orderId", orderId);
            return this.jdbcTemplate.queryForList(sqlQuery, paramsMap);
        } catch (DataAccessException e) {
            logger.error("Error while fetching id for promocode: " + promocode + " from multiuse_promocode" + e);
            return null;
        }
    }
}
