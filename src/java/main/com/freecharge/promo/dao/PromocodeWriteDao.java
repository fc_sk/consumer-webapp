package com.freecharge.promo.dao;

import com.freecharge.app.domain.dao.BaseDao;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.Promocode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Service
public class PromocodeWriteDao extends BaseDao{

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertPromocode;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocode = new SimpleJdbcInsert(dataSource).withTableName(
                "promocode").usingGeneratedKeyColumns("promocode_id", "n_last_updated", "n_created");
    }

    public int insert(Promocode promocode){
        SqlParameterSource parameters = promocode.getMapSqlParameterSource();
        Number returnParam = this.insertPromocode.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public int updateStatus(String freefundCode, String status) {
        try {
            String sql = "update promocode set current_status=:status where promocode=:freefundCode";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("freefundCode", freefundCode);
            paramMap.put("status", status);

            int count = this.jdbcTemplate.update(sql, paramMap);

            if (count <= 0) {
                logger.error("No Rows affected on updating in_request for id = " + freefundCode);
            }

            return count;
        } catch (RuntimeException re) {
            logger.error("Error updating promocode status for " + freefundCode , re);
        }
        return 0;
    }
}
