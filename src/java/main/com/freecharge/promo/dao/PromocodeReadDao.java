package com.freecharge.promo.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.PromocodeRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.promo.entity.Promocode;
import com.google.common.base.Optional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeReadDao {

    private static Logger logger = LoggingFactory.getLogger(PromocodeReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public Optional<Promocode> getPromoFromPromocode(String promocode) {
        List<Promocode> promoList;
        try{
            String queryString = "Select promocode_id, promocode, promo_type, promo_sponsorship, campaign_id, current_status, " +
                    "prefix, code_length, discount_type, discount_value, max_discount_value, min_recharge_value, " +
                    "apply_condition_id, payment_condition_id, redeem_condition_id, success_message_apply, " +
                    "success_message_recharge, description, has_unique_mapping, valid_from, valid_upto, created_at, " +
                    "created_by, updated_at, updated_by, max_redemptions from promocode where promocode=:promocode";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("promocode", promocode);
            promoList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());
            if(!FCUtil.isEmpty(promoList)) {
                return Optional.of(promoList.get(0));
            }
        } catch (DataAccessException e){
            logger.error("Failed to fetch Promo details", e);
        }
        return Optional.absent();
    }

    public Optional<Promocode> getActivePromoFromPromocode(String promocode) {
        List<Promocode> promoList;
        try{
            String queryString = "Select promocode_id, promocode, promo_type, promo_sponsorship, campaign_id, current_status, " +
                    "prefix, code_length, discount_type, discount_value, max_discount_value, min_recharge_value, " +
                    "apply_condition_id, payment_condition_id, redeem_condition_id, success_message_apply, " +
                    "success_message_recharge, description, has_unique_mapping, valid_from, valid_upto, created_at, " +
                    "created_by, updated_at, updated_by, max_redemptions from promocode where promocode=:promocode and " +
                    "valid_from <= current_timestamp() and valid_upto >= current_timestamp()";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("promocode", promocode);
            promoList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());
            if(!FCUtil.isEmpty(promoList)) {
                return Optional.of(promoList.get(0));
            }
        } catch (DataAccessException e){
            logger.error("Failed to fetch Promo details", e);
        }
        return Optional.absent();
    }
}
