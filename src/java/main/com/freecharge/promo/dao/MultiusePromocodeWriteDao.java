package com.freecharge.promo.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.MultiusePromocode;

@Repository
public class MultiusePromocodeWriteDao {

    private static Logger logger = LoggingFactory.getLogger(MultiusePromocodeWriteDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertMultiusePromocode;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertMultiusePromocode = new SimpleJdbcInsert(dataSource)
                .withTableName("multiuse_promocode")
                .usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public int insert(MultiusePromocode multiusePromocode) {
        SqlParameterSource parameters = multiusePromocode.getMapSqlParameterSource();
        Number returnParam = this.insertMultiusePromocode.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public int redeemMultiusePromocode(String orderId, String promocode,
                                       Timestamp updatedAt) {
        String sql = "update multiuse_promocode set status = 'REDEEMED', updated_at =:updatedAt, used_date =:updatedAt "
                + "where order_id =:orderId and promocode =:promocode and status = 'BLOCKED'";
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("updatedAt", updatedAt);
        paramsMap.put("orderId", orderId);
        paramsMap.put("promocode", promocode);
        try {
            return this.jdbcTemplate.update(sql, paramsMap);
        } catch (DataAccessException e) {
            logger.error("Error while updating multiuse promocode status for recharge success" + e);
            return -1;
        }
    }

    public int unblockMultiusePromocode(String orderId, String promocode,
                                        Timestamp updatedAt) {
        String sql = "update multiuse_promocode set order_id = null, status = null, updated_at =:updatedAt, "
                 + "used_date = null where order_id =:orderId and promocode =:promocode and status = 'BLOCKED'";
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("updatedAt", updatedAt);
        paramsMap.put("orderId", orderId);
        paramsMap.put("promocode", promocode);
        try {
            return this.jdbcTemplate.update(sql, paramsMap);
        } catch (DataAccessException e) {
            logger.error("Error while updating multiuse promocode status for recharge failure" + e);
            return -1;
        }
    }
}
