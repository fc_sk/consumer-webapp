package com.freecharge.promo.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.PromocodeStatusRequestResponseRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.promo.entity.PromocodeStatusRequestResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeStatusRequestResponseReadDao {
    private static Logger logger = LoggingFactory.getLogger(PromocodeStatusRequestResponseReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PromocodeStatusRequestResponse> getRequestResponseForRequestId(String requestId) {
        List<PromocodeStatusRequestResponse> promocodeStatusRequestResponse = new ArrayList<>();
        try{
            String queryString = "select id, request_id, request, response, created_at, created_by, updated_at, " +
                    "updated_by from promocode_status_request_response where request_id=:requestId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("requestId", requestId);

            promocodeStatusRequestResponse = this.jdbcTemplate.query(queryString, paramMap, new PromocodeStatusRequestResponseRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch requestResponse data for requestId" + requestId, e);
        }
        return promocodeStatusRequestResponse;
    }
}
