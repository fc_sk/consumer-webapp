package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.promo.rest.error.ErrorCode.FreefundErrorCode;
import com.freecharge.promo.util.PromocodeConstants;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class represents the status request for a promocode
 */
public class PromocodeStatusRequest extends AbstractDO implements AbstractPromoCodeRequest {
    private String affiliateId;
    private String requestId;
    private String promocode;
    private String mappingKey;
    private String keyValue;

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getMappingKey() {
        return mappingKey;
    }

    public void setMappingKey(String mappingKey) {
        this.mappingKey = mappingKey;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    @Override
    public String toString() {
        Map<String, Object> errorData = new HashMap<>();
        errorData.put(PromocodeConstants.AFFILIATE_ID, affiliateId);
        errorData.put(PromocodeConstants.REQUEST_ID, requestId);
        errorData.put(FCConstants.PROMOCODE, promocode);
        errorData.put(PromocodeConstants.MAPPING_KEY, mappingKey);
        errorData.put(PromocodeConstants.KEY_VALUE, keyValue);
        return FCStringUtils.mapToJson(errorData);
    }

    @Override
    public Map<String, Object> validate() {
        Map<String, Object> errorMap = validateForNotNull();
        if (!FCUtil.isEmpty(errorMap)) {
            return errorMap;
        }
        return null;
    }

    private Map<String, Object> validateForNotNull() {
        Map<String, Object> responseMap = new HashMap<>();
        if (FCUtil.isEmpty(requestId) || FCUtil.isEmpty(affiliateId) || FCUtil.isEmpty(promocode)) {
            responseMap.put("errorMessage",
                    "Found empty one of the fields: requestId: " + requestId + " , affiliateId: " + affiliateId + " , "
                            + "promocode: " + promocode);
            responseMap.put("errorCode", FreefundErrorCode.INVALID_REQUEST);
        }
        return responseMap;
    }

    @Override
    public Map<String, Object> toMap() {
        Field[] fields = getClass().getDeclaredFields();
        Map<String, Object> result = new LinkedHashMap<>();
        try {
            for (Field each : fields) {
                each.setAccessible(true);
                Object val = each.get(this);
                result.put(each.getName(), val);
            }
            return result;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}