package com.freecharge.promo.entity;


import com.freecharge.common.framework.basedo.AbstractDO;

public class PromocodeAffiliates extends AbstractDO{
    private String affiliateId;
    private String affiliateName;
    private String affiliateType;

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public String getAffiliateName() {
        return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
        this.affiliateName = affiliateName;
    }

    public String getAffiliateType() {
        return affiliateType;
    }

    public void setAffiliateType(String affiliateType) {
        this.affiliateType = affiliateType;
    }
}
