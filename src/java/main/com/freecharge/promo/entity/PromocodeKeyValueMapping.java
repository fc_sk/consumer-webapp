package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PromocodeKeyValueMapping extends AbstractDO{

    private long id;
    private long promocodeId;
    private String mappingKey;
    private String keyValue;

    public PromocodeKeyValueMapping() {
    }

    public PromocodeKeyValueMapping(long id, long promocodeId, String mappingKey, String keyValue) {
        this.id = id;
        this.promocodeId = promocodeId;
        this.mappingKey = mappingKey;
        this.keyValue = keyValue;
    }

    public long getId() {
        return id;
    }

    public long getPromocodeId() {
        return promocodeId;
    }

    public String getMappingKey() {
        return mappingKey;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPromocodeId(long promocodeId) {
        this.promocodeId = promocodeId;
    }

    public void setMappingKey(String mappingKey) {
        this.mappingKey = mappingKey;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("promocode_id", this.promocodeId);
        mapSqlParameterSource.addValue("mapping_key", this.mappingKey);
        mapSqlParameterSource.addValue("key_value", this.keyValue);
        return mapSqlParameterSource;
    }
}
