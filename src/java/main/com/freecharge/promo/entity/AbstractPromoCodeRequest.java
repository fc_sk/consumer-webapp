package com.freecharge.promo.entity;

import java.util.Map;

public interface AbstractPromoCodeRequest {

    public Map<String, Object> validate();
}
