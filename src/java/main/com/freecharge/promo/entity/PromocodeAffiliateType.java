package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;

public class PromocodeAffiliateType extends AbstractDO {

    int id;
    String affiliateType;
    String text;

    public PromocodeAffiliateType(){}

    public PromocodeAffiliateType(String affiliateType, String text) {
        this.affiliateType = affiliateType;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAffiliateType() {
        return affiliateType;
    }

    public void setAffiliateType(String affiliateType) {
        this.affiliateType = affiliateType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
