package com.freecharge.promo.entity;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;
import com.freecharge.promo.rest.error.ErrorCode.FreefundErrorCode;

import com.freecharge.promo.util.PromocodeConstants;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class represents the promocode creation request
 */
public class PromocodeCreateRequest extends AbstractDO implements AbstractPromoCodeRequest {

    private String requestId;
    private String affiliateId;
    private String campaignId;
    private String promoType;
    private String promoSponsorship;
    private String prefix;
    private String codeLength;
    private String discountType;
    private String discountValue;
    private String maxDiscountValue;
    private String minRechargeValue;
    private String applyConditionId;
    private String paymentConditionId;
    private String redeemConditionId;
    private String successMessageApply;
    private String successMessageRecharge;
    private String mappingKey;
    private String keyValue;
    private String description;
    private String validFrom;
    private String validUpto;
    private boolean forceCreate;
    private String maxRedemptions;

    public PromocodeCreateRequest(){}

    public PromocodeCreateRequest(Map<String, Object> promocodeRequestMap) {
        this.requestId = promocodeRequestMap.get(PromocodeConstants.REQUEST_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.REQUEST_ID)) : null;
        this.campaignId = promocodeRequestMap.get(PromocodeConstants.CAMPAIGN_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.CAMPAIGN_ID)) : null;
        this.affiliateId = promocodeRequestMap.get(PromocodeConstants.AFFILIATE_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.AFFILIATE_ID)) : null ;
        this.promoType = promocodeRequestMap.get(PromocodeConstants.PROMO_TYPE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.PROMO_TYPE)): null;
        this.promoSponsorship = promocodeRequestMap.get(PromocodeConstants.PROMO_SPONSORSHIP) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.PROMO_SPONSORSHIP)): null;
        this.prefix = promocodeRequestMap.get(PromocodeConstants.PREFIX) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.PREFIX)): null;
        this.codeLength = promocodeRequestMap.get(PromocodeConstants.CODE_LENGTH) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.CODE_LENGTH)): null;
        this.discountType = promocodeRequestMap.get(PromocodeConstants.DISCOUNT_TYPE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.DISCOUNT_TYPE)): null;
        this.discountValue = promocodeRequestMap.get(PromocodeConstants.DISCOUNT_VALUE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.DISCOUNT_VALUE)): null;
        this.maxDiscountValue = promocodeRequestMap.get(PromocodeConstants.MAX_DISCOUNT_VALUE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.MAX_DISCOUNT_VALUE)): null;
        this.minRechargeValue = promocodeRequestMap.get(PromocodeConstants.MIN_RECHARGE_VALUE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.MIN_RECHARGE_VALUE)): null;
        this.applyConditionId = promocodeRequestMap.get(PromocodeConstants.APPLY_CONDITION_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.APPLY_CONDITION_ID)): null;
        this.paymentConditionId = promocodeRequestMap.get(PromocodeConstants.PAYMENT_CONDITION_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.PAYMENT_CONDITION_ID)): null;
        this.redeemConditionId = promocodeRequestMap.get(PromocodeConstants.REDEEM_CONDITON_ID) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.REDEEM_CONDITON_ID)): null;
        this.successMessageApply = promocodeRequestMap.get(PromocodeConstants.SUCCESS_MESSAGE_ON_APPLY) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.SUCCESS_MESSAGE_ON_APPLY)): null;
        this.successMessageRecharge = promocodeRequestMap.get(PromocodeConstants.SUCCESS_MESSAGE_ON_RECHARGE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.SUCCESS_MESSAGE_ON_RECHARGE)): null;
        this.mappingKey = promocodeRequestMap.get(PromocodeConstants.MAPPING_KEY) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.MAPPING_KEY)): null;
        this.keyValue = promocodeRequestMap.get(PromocodeConstants.KEY_VALUE) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.KEY_VALUE)): null;
        this.description = promocodeRequestMap.get(PromocodeConstants.DESCRIPTION) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.DESCRIPTION)): null;
        this.validFrom = promocodeRequestMap.get(PromocodeConstants.VALID_FROM) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.VALID_FROM)): null;
        this.validUpto = promocodeRequestMap.get(PromocodeConstants.VALID_UPTO) != null ?
                String.valueOf(promocodeRequestMap.get(PromocodeConstants.VALID_UPTO)): null;
        if ("true".equals(promocodeRequestMap.get(PromocodeConstants.FORCE_CREATE))) {
            this.forceCreate = true;
        } else {
            this.forceCreate = false;
        }
        this.maxRedemptions = (String) promocodeRequestMap.get("maxRedemptions");
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setPromoType(String promoType) {
        this.promoType = promoType;
    }

    public void setPromoSponsorship(String promoSponsorship) {
        this.promoSponsorship = promoSponsorship;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setCodeLength(String codeLength) {
        this.codeLength = codeLength;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public void setMaxDiscountValue(String maxDiscountValue) {
        this.maxDiscountValue = maxDiscountValue;
    }

    public void setMinRechargeValue(String minRechargeValue) {
        this.minRechargeValue = minRechargeValue;
    }

    public void setApplyConditionId(String applyConditionId) {
        this.applyConditionId = applyConditionId;
    }

    public void setPaymentConditionId(String paymentConditionId) {
        this.paymentConditionId = paymentConditionId;
    }

    public void setRedeemConditionId(String redeemConditionId) {
        this.redeemConditionId = redeemConditionId;
    }

    public void setSuccessMessageApply(String successMessageApply) {
        this.successMessageApply = successMessageApply;
    }

    public void setSuccessMessageRecharge(String successMessageRecharge) {
        this.successMessageRecharge = successMessageRecharge;
    }

    public void setMappingKey(String mappingKey) {
        this.mappingKey = mappingKey;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    public void setForceCreate(boolean forceCreate) {
        this.forceCreate = forceCreate;
    }

    public void setMaxRedemptions(String maxRedemptions) {
        this.maxRedemptions = maxRedemptions;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getPromoType() {
        return promoType;
    }

    public String getPromoSponsorship() {
        return promoSponsorship;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCodeLength() {
        return codeLength;
    }

    public String getDiscountType() {
        return discountType;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public String getMaxDiscountValue() {
        return maxDiscountValue;
    }

    public String getMinRechargeValue() {
        return minRechargeValue;
    }

    public String getApplyConditionId() {
        return applyConditionId;
    }

    public String getPaymentConditionId() {
        return paymentConditionId;
    }

    public String getRedeemConditionId() {
        return redeemConditionId;
    }

    public String getSuccessMessageApply() {
        return successMessageApply;
    }

    public String getSuccessMessageRecharge() {
        return successMessageRecharge;
    }

    public String getMappingKey() {
        return mappingKey;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public String getDescription() {
        return description;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public boolean getForceCreate() {
        return forceCreate;
    }

    public String getMaxRedemptions() {
        return maxRedemptions;
    }

    @Override
    public Map<String, Object> validate() {
        Map<String, Object> errorMap = validateForNotNull();
        if (!FCUtil.isEmpty(errorMap)) {
            return errorMap;
        }
        return null;
    }

    private Map<String, Object> validateForNotNull() {
        Map<String, Object> responseMap = new HashMap<>();
        if (FCUtil.isEmpty(requestId) || FCUtil.isEmpty(affiliateId) || FCUtil.isEmpty(campaignId)) {
            responseMap.put("errorMessage", "Found empty one of the fields: requestId, affiliateId, campaignId");
            responseMap.put("errorCode", FreefundErrorCode.INVALID_REQUEST);
        }
        return responseMap;
    }

    public boolean isGeneric() {
        return FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(promoType)
                || FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(promoType);
    }

    @Override
    public Map<String, Object> toMap() {
        Field[] fields = getClass().getDeclaredFields();
        Map<String, Object> result = new LinkedHashMap<>();
        try {
            for (Field each: fields) {
                each.setAccessible(true);
                Object val = each.get(this);
                result.put(each.getName(), val);
            }
            return result;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
