package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import java.util.Date;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PromocodeGenerationRequestResponse extends AbstractDO{

    private long id;
    private long promocodeId;
    private String affiliateId;
    private String requestId;
    private String requestData;
    private String responseId;
    private String responseData;
    private Date createdAt;

    public PromocodeGenerationRequestResponse() {
    }

    public PromocodeGenerationRequestResponse(long id, long promocodeId, String affiliateId, String requestId,
                                              String requestData, String responseId, String responseData, Date createdAt) {
        this.id = id;
        this.promocodeId = promocodeId;
        this.affiliateId = affiliateId;
        this.requestId = requestId;
        this.requestData = requestData;
        this.responseId = responseId;
        this.responseData = responseData;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getPromocodeId() {
        return promocodeId;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getRequestData() {
        return requestData;
    }

    public String getResponseId() {
        return responseId;
    }

    public String getResponseData() {
        return responseData;
    }
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPromocodeId(long promocodeId) {
        this.promocodeId = promocodeId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("promocode_id", this.promocodeId);
        mapSqlParameterSource.addValue("request_id", this.requestId);
        mapSqlParameterSource.addValue("request_data", this.requestData);
        mapSqlParameterSource.addValue("response_id", this.responseId);
        mapSqlParameterSource.addValue("response_data", this.responseData);
        mapSqlParameterSource.addValue("created_at", this.createdAt);
        return mapSqlParameterSource;
    }
}
