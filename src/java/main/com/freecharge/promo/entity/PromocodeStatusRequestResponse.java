package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PromocodeStatusRequestResponse extends AbstractDO{
    private long id;
    private String requestId;
    private String request;
    private String response;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("request_id", this.requestId);
        mapSqlParameterSource.addValue("request", this.request);
        mapSqlParameterSource.addValue("response", this.response);
        return mapSqlParameterSource;
    }
}
