package com.freecharge.promo.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import java.sql.Timestamp;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class MultiusePromocode extends AbstractDO {

    private long id;
    private long userId;
    private long campaignId;
    private String promocode;
    private String orderId;
    private String status;
    private String serviceNumber;
    private Timestamp createdDate;
    private Timestamp usedDate;
    private Timestamp updatedAt;

    public MultiusePromocode(){};

    public MultiusePromocode(long userId, long campaignId, String promocode, String orderId,
                             String status, String serviceNumber,
                             Timestamp createdDate, Timestamp updatedAt) {
        this.userId = userId;
        this.campaignId = campaignId;
        this.promocode = promocode;
        this.orderId = orderId;
        this.status = status;
        this.serviceNumber = serviceNumber;
        this.createdDate = createdDate;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Timestamp getUsedDate() {
        return usedDate;
    }

    public void setUsedDate(Timestamp usedDate) {
        this.usedDate = usedDate;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("user_id", this.getUserId());
        mapSqlParameterSource.addValue("campaign_id", this.getCampaignId());
        mapSqlParameterSource.addValue("promocode", this.getPromocode());
        mapSqlParameterSource.addValue("order_id", this.getOrderId());
        mapSqlParameterSource.addValue("status", this.getStatus());
        mapSqlParameterSource.addValue("service_number", this.getServiceNumber());
        mapSqlParameterSource.addValue("created_date", this.getCreatedDate());
        mapSqlParameterSource.addValue("used_date", this.getUsedDate());
        mapSqlParameterSource.addValue("update_at", this.getUpdatedAt());
        return mapSqlParameterSource;
    }
}
