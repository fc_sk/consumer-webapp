package com.freecharge.promo.entity;

import com.freecharge.promo.util.PromocodeConstants;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.freecharge.common.framework.basedo.AbstractDO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 * Entity class for Promocode
 */
public class Promocode extends AbstractDO {
    private long promocodeId;
    private String promocode;
    private String promoType;
    private String promoSponsorship;
    private long campaignId;
    private String currentStatus;
    private String prefix;
    private int codeLength;
    private String discountType;
    private float discountValue;
    private float maxDiscountValue;
    private float minRechargeValue;
    private long applyConditionId;
    private long paymentConditionId;
    private long redeemConditionId;
    private String successMessageApply;
    private String successMessageRecharge;
    private String description;
    private Boolean hasUniqueMapping;
    private Date validFrom;
    private Date validUpto;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    private int maxRedemptions;

    public Promocode() {
    }

    public Promocode(Map<String, Object> promocodeMap) {
        this.promocodeId = (long) promocodeMap.get("promocodeId");
        this.promocode = (String) promocodeMap.get("promocode");
        this.promoType = (String) promocodeMap.get("promoType");
        this.promoSponsorship = (String) promocodeMap.get("promoSponsorship");
        this.campaignId = (long) promocodeMap.get("campaignId");
        this.currentStatus = (String) promocodeMap.get("currentStatus");
        this.prefix = (String) promocodeMap.get("prefix");
        this.codeLength = (int) promocodeMap.get("codeLength");
        this.discountType = (String) promocodeMap.get("discountType");
        this.discountValue = (float) promocodeMap.get("discountValue");
        this.maxDiscountValue = (float) promocodeMap.get("maxDiscountValue");
        this.minRechargeValue = (float) promocodeMap.get("minRechargeValue");
        this.applyConditionId = (long) promocodeMap.get("applyConditionId");
        this.paymentConditionId = (long) promocodeMap.get("paymentConditionId");
        this.redeemConditionId = (long) promocodeMap.get("redeemConditionId");
        this.successMessageApply = (String) promocodeMap.get("successMessageApply");
        this.successMessageRecharge = (String) promocodeMap.get("successMessageRecharge");
        this.description = (String) promocodeMap.get("description");
        this.hasUniqueMapping = (Boolean) promocodeMap.get("hasUniqueMapping");
        this.validFrom = (Date) promocodeMap.get("validFrom");
        this.validUpto = (Date) promocodeMap.get("validUpto");
        this.createdAt = (Date) promocodeMap.get("createdAt");
        this.createdBy = (String) promocodeMap.get("createdBy");
        this.updatedAt = (Date) promocodeMap.get("updatedAt");
        this.updatedBy = (String) promocodeMap.get("updatedBy");
        this.maxRedemptions = (int) promocodeMap.get("maxRedemptions");

    }

    public long getPromocodeId() {
        return promocodeId;
    }

    public void setPromocodeId(long promocodeId) {
        this.promocodeId = promocodeId;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getPromoType() {
        return promoType;
    }

    public void setPromoType(String promoType) {
        this.promoType = promoType;
    }

    public String getPromoSponsorship() {
        return promoSponsorship;
    }

    public void setPromoSponsorship(String promoSponsorship) {
        this.promoSponsorship = promoSponsorship;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(int codeLength) {
        this.codeLength = codeLength;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public float getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(float discountValue) {
        this.discountValue = discountValue;
    }

    public float getMaxDiscountValue() {
        return maxDiscountValue;
    }

    public void setMaxDiscountValue(float maxDiscountValue) {
        this.maxDiscountValue = maxDiscountValue;
    }

    public long getApplyConditionId() {
        return applyConditionId;
    }

    public void setApplyConditionId(long applyConditionId) {
        this.applyConditionId = applyConditionId;
    }

    public long getPaymentConditionId() {
        return paymentConditionId;
    }

    public void setPaymentConditionId(long paymentConditionId) {
        this.paymentConditionId = paymentConditionId;
    }

    public long getRedeemConditionId() {
        return redeemConditionId;
    }

    public void setRedeemConditionId(long redeemConditionId) {
        this.redeemConditionId = redeemConditionId;
    }

    public String getSuccessMessageApply() {
        return successMessageApply;
    }

    public void setSuccessMessageApply(String successMessageApply) {
        this.successMessageApply = successMessageApply;
    }

    public String getSuccessMessageRecharge() {
        return successMessageRecharge;
    }

    public void setSuccessMessageRecharge(String successMessageRecharge) {
        this.successMessageRecharge = successMessageRecharge;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHasUniqueMapping() {
        return hasUniqueMapping;
    }

    public void setHasUniqueMapping(Boolean hasUniqueMapping) {
        this.hasUniqueMapping = hasUniqueMapping;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(Date validUpto) {
        this.validUpto = validUpto;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public float getMinRechargeValue() {
        return minRechargeValue;
    }

    public void setMinRechargeValue(float minRechargeValue) {
        this.minRechargeValue = minRechargeValue;
    }

    public int getMaxRedemptions() {
        return maxRedemptions;
    }

    public void setMaxRedemptions(int maxRedemptions) {
        this.maxRedemptions = maxRedemptions;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("promocode", this.promocode);
        mapSqlParameterSource.addValue("promo_type", this.promoType);
        mapSqlParameterSource.addValue("promo_sponsorship", this.promoSponsorship);
        mapSqlParameterSource.addValue("campaign_id", this.campaignId);
        mapSqlParameterSource.addValue("current_status", this.currentStatus);
        mapSqlParameterSource.addValue("prefix", this.prefix);
        mapSqlParameterSource.addValue("code_length", this.codeLength);
        mapSqlParameterSource.addValue("discount_type", this.discountType);
        mapSqlParameterSource.addValue("discount_value", this.discountValue);
        mapSqlParameterSource.addValue("max_discount_value", this.maxDiscountValue);
        mapSqlParameterSource.addValue("min_recharge_value", this.minRechargeValue);
        mapSqlParameterSource.addValue("apply_condition_id", this.applyConditionId);
        mapSqlParameterSource.addValue("payment_condition_id", this.paymentConditionId);
        mapSqlParameterSource.addValue("redeem_condition_id", this.redeemConditionId);
        mapSqlParameterSource.addValue("success_message_apply", this.successMessageApply);
        mapSqlParameterSource.addValue("success_message_recharge", this.successMessageRecharge);
        mapSqlParameterSource.addValue("description", this.description);
        mapSqlParameterSource.addValue("has_unique_mapping", this.hasUniqueMapping);
        mapSqlParameterSource.addValue("valid_from", this.validFrom);
        mapSqlParameterSource.addValue("valid_upto", this.validUpto);
        mapSqlParameterSource.addValue("created_at", this.createdAt);
        mapSqlParameterSource.addValue("created_by", this.createdBy);
        mapSqlParameterSource.addValue("updated_at", this.updatedAt);
        mapSqlParameterSource.addValue("updated_by", this.updatedBy);
        mapSqlParameterSource.addValue("max_redemptions", this.maxRedemptions);
        return mapSqlParameterSource;
    }

    @Override
    public Map<String, Object> toMap() {
        Field[] fields = getClass().getDeclaredFields();
        Map<String, Object> result = new LinkedHashMap<>();
        try {
            for (Field each : fields) {
                each.setAccessible(true);
                Object value = each.get(this);
                if (null != value) {
                    result.put(each.getName(), value.toString());
                }
            }
            //maxRedemptions = 0 indicates a generic promocode
            if (maxRedemptions == 0) {
                result.remove("maxRedemptions");
            }
            return result;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
