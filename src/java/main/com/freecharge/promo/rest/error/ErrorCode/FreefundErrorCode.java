package com.freecharge.promo.rest.error.ErrorCode;

import com.google.common.collect.ImmutableMap;
import java.util.Map;

public enum FreefundErrorCode {
    /**
     * Error code numbers are *always* of length 5. They should adhere to below
     * guidelines.
     *
     * Error code format is "TTXXX".
     *
     * Where
     *
     * TT = Two digits that represent a module. XXX = Exact error code.
     *
     * "11111" = Success (No error).<br>
     * "11XXX" = FreeFund.<br>*/


     /*
     * Promocode Distribution APIs' Error codes
     */
    INVALID_DATA(11000, false), AFFILIATE_ID_INVALID(11001, false), PROMOCODE_NON_EXISTENT(110002, false),
    UNIQUE_KEY_ALREADY_MAPPED(11003, false), INVALID_CAMPAIGN_ID(11004, false), INVALID_REQUEST(11005, false),
    DUPLICATE_REQUEST(11006, false), FAILURE_IN_GENERATING_UNIQUE_CODE(11007, false),
    PROMOCODE_KEY_MAPPING_ERROR(11008, false), MISSING_MAPPING_KEY_VALUE(11009, false),
    KEY_VALUE_NOT_MAPPED_WITH_PROMOCODE(11011, false), INVALID_MAPPING_KEY(11012, false), REFERRAL_SHUT_DOWN(11226, false),
    PERMISSION_DENIED(11227, false), ERR_PROMO_TYPE_NOT_SUPPORTED(11228, false), PROMOCODE_CREATION_LIMIT_REACHED(11228, false);

    private final String CALL_STATUS = "callStatus";
    private final String FAILURE     = "failure";
    private final String SUCCESS     = "success";
    private final String ERROR_CODE  = "errorCode";

    final int            errorNumber;
    final Boolean        callStatus;

    private FreefundErrorCode(int errorNumber, Boolean callStatus) {
        this.errorNumber = errorNumber;
        this.callStatus = callStatus;
    }

    public int getErrorNumber() {
        return this.errorNumber;
    }

    public String getErrorNumberString() {
        return Integer.toString(this.errorNumber);
    }

    public Map<String, String> restResponse() {
        String statusString = FAILURE;

        if (callStatus) {
            statusString = SUCCESS;
        }

        return ImmutableMap.of(CALL_STATUS, statusString, ERROR_CODE, this.getErrorNumberString());
    }
}