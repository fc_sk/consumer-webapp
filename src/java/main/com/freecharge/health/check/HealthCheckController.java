package com.freecharge.health.check;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.freecharge.app.domain.dao.jdbc.LocationReadDao;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.web.interceptor.annotation.NoSession;



/**
 * @author bhaskerghosh
 *
 */
@Controller
public class HealthCheckController {
		
	@Autowired
    AmazonDynamoDBAsync dynamoClientMum;	
	@Autowired
	LocationReadDao locationReadDao;
	@Autowired
    private AppConfigService appConfigService;
	@Autowired
    private FCProperties            fcProperties;
	
	private static final Logger logger = LoggingFactory.getLogger(HealthCheckController.class);
	
	/**
	 * @return boolean
	 * 
	 * generic application healthCheck API
	 * 
	 */
	@RequestMapping(value = "health")
    @ResponseStatus(value = HttpStatus.OK)
	@NoSession
    @Csrf(exclude = true)
    @NoLogin
    @ResponseBody
    public boolean healthCheck() {
        return true;
    }
	
	/**
	 * @return boolean
	 * 
	 * performs deephealth check i.e connectivity with different dependent services
	 * 
	 */
	@RequestMapping(value = "deephealth")
	@NoSession
    @NoLogin
    @Csrf(exclude = true)
    @ResponseBody
    public boolean deepHealthCheck() {
		long start = System.currentTimeMillis();
		boolean retVal = true;
		try {
			// dynamo
			retVal = retVal && verifyDynamoConnection();
			// mysql
			retVal = retVal && verifyMySqlConnection();
			// appconfig
		    retVal = retVal && verifyAppConfig();
		    
		    // Remote Services
		    retVal = true;
		    retVal = retVal && testTelnetConnection("IMS", fcProperties.getProperty(fcProperties.IMS_CLIENT_URL), fcProperties.getIntProperty(fcProperties.IMS_CLIENT_PORT));
		    retVal = retVal && testTelnetConnection("CampaignService", fcProperties.getProperty(fcProperties.CAMPAIGN_SERVICE_URI));
		    retVal = retVal && testTelnetConnection("Onecheck", fcProperties.getProperty(fcProperties.ONE_CHECK_SERVICE_HOST_URL));
		    
		} catch (Exception e) {
			logger.error("Exception occured during deephealthcheck.", e);
		}
		
		System.out.println("DeepHealthCheck RT = " + (System.currentTimeMillis() - start) + "ms");
		
        return retVal;
    }

	private boolean verifyAppConfig() {
		boolean retVal = true;
		try {
			String androidAppDeepLinkAsset = appConfigService.getAppDeepLinkSiteAssociation("android");
			if (StringUtils.isEmpty(androidAppDeepLinkAsset)) {
				retVal = false;
				logger.error("Verification of Appconfig Connection failed.");
			}
		} catch (Exception e) {
			retVal = false;
			logger.error("Verification of Appconfig Connection failed.", e);
		}
		if (retVal) {
			logger.info("Verification of Appconfig Connection Successful.");
		}
		return retVal;
	}

	private boolean verifyMySqlConnection() {
		boolean retVal = true;
		try {
			List<CityMaster> cities = locationReadDao.getCity(7);
		} catch (Exception e) {
			retVal = false;
			logger.error("Verification of MySqlConnnection failed.", e);
		}
		if (retVal) {
			logger.info("Verification of MySqlConnnection Successful.");
		}
		return retVal;
	}

	private boolean verifyDynamoConnection() {
		boolean retVal = true;
		try {
			ListTablesResult tableList = dynamoClientMum.listTables();
			if (tableList == null || CollectionUtils.isEmpty(tableList.getTableNames())) {
				retVal = false;
				logger.error("Verification of DynamoConnnection failed.");
			}
		} catch (Exception e) {
			retVal = false;
			logger.error("Verification of DynamoConnnection failed.", e);
		}
		if (retVal) {
			logger.error("Verification of DynamoConnnection Success.");
		}
		return retVal; 
	}
	
	public static void main(String[] args) throws NumberFormatException, Exception {		
		HealthCheckController hcc = new HealthCheckController();
		System.out.println(hcc.testTelnetConnection("OCheck", "http://enterprise.smsgupshup.com/GatewayAPI/rest"));
	}
	
	private boolean testTelnetConnection(String remoteServiceName, String hostUrl) throws NumberFormatException, Exception {
		String ip = null;
		String port = null;
		
		String temp = null;
		String[] proto_ip_port = hostUrl.split("//");
		if (proto_ip_port.length>1) {			
			temp = proto_ip_port[1].split("/")[0];	// https://1.2.3.4:1234/abc
		} else {
			temp = proto_ip_port[0].split("/")[0];	// 1.2.3.4:1234/abc
		}
		
		
		String[] ip_port = temp.split(":");
		if (ip_port.length>1) {			// 1.2.3.4:1234/abc
			ip = ip_port[0];
			port = ip_port[1].split("/")[0];
		} else {
			ip = ip_port[0].split("/")[0];	// 1.2.3.4
		}
		
		if (StringUtils.isEmpty(port)) {
			if (hostUrl.startsWith("https")) {
				port = "443";
			} else {
				port = "80";
			}
		}
		
		return testTelnetConnection(remoteServiceName, ip, Integer.parseInt(port));
		
	}
	
	
	private boolean testTelnetConnection(String remoteServiceName, String ip, int port) throws Exception {
		boolean retVal = false;
		try {
            Socket client=new Socket();   
            client.connect(new InetSocketAddress(ip,port),1000); 
            logger.info("Connected with " + remoteServiceName + " ip "+ip+" and port "+port);
            client.close();
            retVal = true;
        } catch(Exception e) {
        	logger.error("Not Connected with " + remoteServiceName);
        	throw e;
        }
		return retVal;
	}
	

}
