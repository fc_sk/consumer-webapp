package com.freecharge.cardstorage;

import java.util.Map;

public interface ICardStorageService {

	/**
	 * Return a map of the list of cards against given userID. The key "cards"
	 * should point to a List<Map<String, String>>, where keys in each map
	 * should be used from the CardStorageKey class.
	 * @param userID
	 * @return
	 */
	public Map<String, Object> list(int userID);

	/**
	 * Return card token as a result of adding a card to storage.
	 * @param userID
	 * @param addCardDetails
	 * @return
	 */
	public Map<String, String> add(int userID, AddCardDetails addCardDetails, CardMetaData addCardMeta);



	/**
	 * Return card token as a result of adding a card to storage.
	 * @param userID
	 * @param addCardDetails
	 * @return
	 */
	public Map<String, String> add(String userID, AddCardDetails addCardDetails, CardMetaData addCardMeta);

	/**
	 * Return a map as a result of deleting a card from storage.
	 * @param userID
	 * @param cardToken
	 * @return
	 */
	public Map<String, Object> delete(int userID, String cardToken);

	/**
	 * Return a map of card details for given token. The keys should
	 * be used from the CardStorageKey class.
	 * @param customerID
	 */
	public Map<String, String> get(int userID, String cardToken, String orderID);

}
