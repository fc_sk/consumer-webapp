package com.freecharge.cardstorage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.common.easydb.EasyDBReadDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;

@Component
public class CardStorageService {
    @Autowired
    private EasyDBReadDAO            easyDBReadDAO;

    @Autowired
    private AppConfigService         appConfigService;

    @Autowired
    MetricsClient                    metricsClient;

    @Autowired
    @Qualifier("card-and-meta")
    ICardStorageService              cardStorageService;

    @Autowired
    private FreechargeEventPublisher freechargeEventPublisher;

    private final Logger             log             = LoggingFactory.getLogger(getClass());

    private static final String      METRICS_SERVICE = "cardStorageController";
    private static final String      METRIC_ADD_CARD = "addCard";
    private static final String      METRIC_SUCCESS  = "success";
    private static final String      METRIC_FAILURE  = "failure";

    public boolean isUserPreferencePresent(Integer userId) {
        UserCardStorageChoicePreference userCardStorageChoicePreference = new UserCardStorageChoicePreference();

        userCardStorageChoicePreference.setUserId(userId);

        return !easyDBReadDAO.get(userCardStorageChoicePreference).isEmpty();
    }

    public boolean addCard(PaymentRequestCardDetails paymentRequestCardDetails, Integer userId, String channel, String paymentOption) {
        log.info("Initiating add of a card for user id: " + userId);

        if (appConfigService.isCardStorageEnabled()) {
            // detect whether save-card requested
            final String cardSaveBool = String.valueOf(paymentRequestCardDetails.isSaveCard());
            final String cardSaveName = paymentRequestCardDetails.getSaveCardName();
            if ("true".equalsIgnoreCase(cardSaveBool)) {
                // detected save-debit-card request, so try saving it
                AddCardDetails addCardDetails = new AddCardDetails();
                addCardDetails.setNickname(cardSaveName);
                setExpiry(paymentRequestCardDetails, addCardDetails, paymentOption);
                addCardDetails.setCardNumber(paymentRequestCardDetails.getCardNumber());
                addCardDetails.setNameOnCard(paymentRequestCardDetails.getCardHolderName());
                addCardDetails.setChannel(channel);
                CardMetaData cardMeta = resolveCardMeta(cardSaveName, paymentRequestCardDetails.isDebitCard(),
                        FCUtil.trim(paymentRequestCardDetails.getCardNumber()), FCUtil.trim(paymentOption),
                        paymentRequestCardDetails.getCardExpMonth(),
                        FCUtil.trim(paymentRequestCardDetails.getCardExpYear()));
                if (cardMeta != null) {
                    if (paymentRequestCardDetails.isDebitCard()) {
                        cardMeta.setCardType("DEBIT");
                    } else {
                        cardMeta.setCardType("CREDIT");
                    }
                }

                // Log any exceptions and proceed as usual, because we
                // do not want the flow to be interrupted when card
                // storage is unavailable.
                try {
                    cardStorageService.add(userId, addCardDetails, cardMeta);
                } catch (RuntimeException e) {
                    final String msg = String.format(
                            "Error adding card details to storage for userId=%s, AddCardDetails=%s", userId,
                            addCardDetails.toString());
                    log.error(msg, e);
                    metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_CARD, METRIC_FAILURE);
                    return false;
                }
            }
        }
        log.info("Completed add of a card for user id: " + userId);
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_CARD, METRIC_SUCCESS);
        return true;
    }


    public boolean addCard(PaymentRequestCardDetails paymentRequestCardDetails, String userId, String channel, String paymentOption) {
        log.info("Initiating add of a card for user id: " + userId);
        AddCardDetails addCardDetails = new AddCardDetails();
        if (appConfigService.isCardStorageEnabled()) {

            CardMetaData cardMeta=prepareCarsDetails(addCardDetails,paymentRequestCardDetails,paymentOption,channel);
            // Log any exceptions and proceed as usual, because we
            // do not want the flow to be interrupted when card
            // storage is unavailable.
            try {
                cardStorageService.add(userId, addCardDetails, cardMeta);
            } catch (RuntimeException e) {
                final String msg = String.format(
                        "Error adding card details to storage for userId=%s, AddCardDetails=%s", userId,
                        addCardDetails.toString());
                log.error(msg, e);
                metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_CARD, METRIC_FAILURE);
                return false;
            }
        }
        log.info("Completed Virtual Credit card add  for user id: " + userId);
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_CARD, METRIC_SUCCESS);
        return true;
    }

    /**
     * Handles special cases for SBI Maestro where the expiry dates are
     * pre-configured in PayU. For other Maestro check if user has given input
     * and save the same else set default values.
     * 
     * @param requestMap
     *            the request map from client.
     * @param addCardDetails
     *            the object in which the expiry data is to be set.
     */
    private void setExpiry(PaymentRequestCardDetails cardDetails, AddCardDetails addCardDetails, String paymentOption) {
        if (MobileUtil.isInteger(cardDetails.getCardExpMonth()) && MobileUtil.isInteger(cardDetails.getCardExpYear())) {
            addCardDetails.setCardExpiryMonth(Integer.parseInt(cardDetails.getCardExpMonth().trim()));
            addCardDetails.setCardExpiryYear(Integer.parseInt(cardDetails.getCardExpYear().trim()));
        } else {
            if (paymentOption != null
                    && (paymentOption.equals(PaymentConstants.MAESTRO_CITI) || paymentOption
                            .equals(PaymentConstants.MAESTRO_SBI))) {
                // No valid card exp data entered by user. Setting it to default
                // month and year.
                addCardDetails.setCardExpiryMonth(Integer.parseInt(PaymentConstants.MAESTRO_DEFAULT_EXPIRY_MONTH));
                addCardDetails.setCardExpiryYear(Integer.parseInt(PaymentConstants.MAESTRO_DEFAULT_EXPIRY_YEAR));
            }
        }
    }

    private CardMetaData resolveCardMeta(String nickname, boolean isDebitCard, String cardNumber, String pyop,
            String expiryMonth, String expiryYear) {
        if (isDebitCard) {
            switch (pyop) {
                case "DSME":
                    return CardMetaData.create(nickname, cardNumber, "DEBIT", "MAESTRO", "SBI");
                case "DCME":
                    return CardMetaData.create(nickname, cardNumber, "DEBIT", "MAESTRO", "Non-SBI");
                case "DVI":
                    return CardMetaData.createUnknown(nickname, cardNumber, "DEBIT", "VISA");
                case "DMC":
                    return CardMetaData.createUnknown(nickname, cardNumber, "DEBIT", "MASTERCARD");
                case "DRU":
                    return CardMetaData.createUnknown(nickname, cardNumber, "DEBIT", "RUPAY");
                default:
                    log.info("Expected pyop to be either of DSME/DCME/DVI/DMC but found: " + String.valueOf(pyop));
                    CardMetaData inferredCardMeta = CardMetaData.inferBrandAndCreateDebitCardMeta(nickname, cardNumber,
                            expiryYear, expiryMonth);

                    log.info("Inferred card brand:[ " + inferredCardMeta.getCardBrand() + "]");
                    return inferredCardMeta;
            }

        } else {
            String cardTypeBrand = PaymentUtil.getCreditCardType(cardNumber);
            if (PaymentConstants.VISA_CC.equals(cardTypeBrand)) {
                return CardMetaData.createUnknown(nickname, cardNumber, "CREDIT", "VISA");
            }
            if (PaymentConstants.MASTER_CC.equals(cardTypeBrand)) {
                return CardMetaData.createUnknown(nickname, cardNumber, "CREDIT", "MASTERCARD");
            }
            throw new IllegalArgumentException("Expected a valid MasterCard or VISA credit card, but found "
                    + cardTypeBrand);
        }
    }

    private CardMetaData prepareCarsDetails(AddCardDetails addCardDetails,PaymentRequestCardDetails paymentRequestCardDetails,String paymentOption,String channel) {
        CardMetaData cardMeta=new CardMetaData();
        // detect whether save-card requested
        final String cardSaveBool = String.valueOf(paymentRequestCardDetails.isSaveCard());
        final String cardSaveName = paymentRequestCardDetails.getSaveCardName();
        if ("true".equalsIgnoreCase(cardSaveBool)) {
            // detected save-debit-card request, so try saving it

            addCardDetails.setNickname(cardSaveName);
            setExpiry(paymentRequestCardDetails, addCardDetails, paymentOption);
            addCardDetails.setCardNumber(paymentRequestCardDetails.getCardNumber());
            addCardDetails.setNameOnCard(paymentRequestCardDetails.getCardHolderName());
            addCardDetails.setChannel(channel);
            cardMeta = resolveCardMeta(cardSaveName, paymentRequestCardDetails.isDebitCard(),
                    FCUtil.trim(paymentRequestCardDetails.getCardNumber()), FCUtil.trim(paymentOption),
                    paymentRequestCardDetails.getCardExpMonth(),
                    FCUtil.trim(paymentRequestCardDetails.getCardExpYear()));
            if (cardMeta != null) {
                if (paymentRequestCardDetails.isDebitCard()) {
                    cardMeta.setCardType("DEBIT");
                } else {
                    cardMeta.setCardType("CREDIT");
                }
            }


        }
        return cardMeta;
    }
}
