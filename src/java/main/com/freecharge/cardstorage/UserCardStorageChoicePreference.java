package com.freecharge.cardstorage;

import java.util.Collection;
import java.util.LinkedList;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.CustomDataType;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * An object encapsulating users' preference as to whether the card storage option should be enabled by default
 * or left to his/her judgement.
 */
public class UserCardStorageChoicePreference implements DBEntity {
    public static final String USER_CARD_STORAGE_CHOICE_PREFERENCE_TABLE = "user_card_storage_choice_preference";
    @ColumnMapping(name="id")
    private Integer id;
    @ColumnMapping(name="fk_users_id")
    private Integer userId;
    @ColumnMapping(name="preference")
    @CustomDataType(dataType = Preference.class, getter = "getStatus")
    private Preference preference;

    private Collection<String> whereFields = new LinkedList<String>();

    public enum Preference {
        DO_NOT_CHOOSE("doNotChoose");

        private String choice;

        private Preference(String choice) {
            this.choice = choice;
        }

        public String getChoice() {
            return choice;
        }

        public static Preference get(String value) {
            for (Preference preference : values()) {
                if (value.equals(preference.getChoice())) {
                    return preference;
                }
            }
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Preference getPreference() {
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }

    @Override
    public String getTableName() {
        return USER_CARD_STORAGE_CHOICE_PREFERENCE_TABLE;
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        throw new UnsupportedOperationException("Cannot update user card storage choice preference, " +
                "operation not permitted");
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    public void addToWhereFields(String whereField) {
        this.whereFields.add(whereField);
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("fk_users_id", this.getUserId());
        mapSqlParameterSource.addValue("preference", this.getPreference().getChoice());
        return mapSqlParameterSource;
    }
}
