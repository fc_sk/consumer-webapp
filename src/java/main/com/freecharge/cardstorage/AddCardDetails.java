package com.freecharge.cardstorage;

import java.util.Calendar;

import com.freecharge.common.framework.basedo.AbstractDO;

public class AddCardDetails extends AbstractDO {

	private String cardNumber;
	private int cardExpiryMonth;
	private int cardExpiryYear;
	private String nameOnCard;
	private String nickname;
	private String channel;

	public AddCardDetails() {
		// default constructor required for JavaBeans
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getCardExpiryMonth() {
		return cardExpiryMonth;
	}

	public void setCardExpiryMonth(int cardExpiryMonth) {
		if (cardExpiryMonth < 1 || cardExpiryMonth > 12) {
			throw new IllegalArgumentException(
					"Expected card expiry month in the range 1 to 12, found " + cardExpiryMonth);
		}
		this.cardExpiryMonth = cardExpiryMonth;
	}

	public int getCardExpiryYear() {
		return cardExpiryYear;
	}

	private static final int MAX_CARD_LIFE_YEARS = 30;
	private static final int MAX_CARD_FINAL_YEAR = 2049;

	public void setCardExpiryYear(int cardExpiryYear) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int maxYear = Math.max(year + MAX_CARD_LIFE_YEARS, MAX_CARD_FINAL_YEAR);
		if (cardExpiryYear < year || cardExpiryYear > maxYear) {
			throw new IllegalArgumentException("Expected card expiry year to be in the range "
		+ year + " to " + maxYear + ", found " + cardExpiryYear);
		}
		this.cardExpiryYear = cardExpiryYear;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

	
}
