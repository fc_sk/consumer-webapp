package com.freecharge.cardstorage;

import org.codehaus.jackson.annotate.JsonProperty;

public class CardDetailsDo {
    
    @JsonProperty("card_bin")
    private String cardBinHead;
    
    @JsonProperty("card_fingerprint")
    private String cardFingerprint;
    
    @JsonProperty("status")
    private String status;
    
    @JsonProperty("issuer")
    private String issuer;

    public String getCardBinHead() {
        return this.cardBinHead;
    }

    public void setCardBinHead(String cardBinHead) {
        this.cardBinHead = cardBinHead;
    }

    public String getCardFingerprint() {
        return this.cardFingerprint;
    }

    public void setCardFingerprint(String cardFingerprint) {
        this.cardFingerprint = cardFingerprint;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssuer() {
        return this.issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

}
