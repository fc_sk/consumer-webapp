package com.freecharge.cardstorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.payment.services.TransactionTypeHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.service.BinOfferService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.DebitATMStatusHandler;
import com.freecharge.payment.services.ITTPHandler;
import com.freecharge.payment.services.PaymentOptionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.rest.model.FPSParams;
import com.freecharge.wallet.OneCheckWalletService;

@Service
@Qualifier("card-and-meta")
public class CardStorageServiceMetaDecorator implements ICardStorageServiceV2 {

    private static final String CACHE_KEY = "DEBIT_ATM_PRODUCTS";

    private static final String DELIMITER = "|";

    private final Logger        logger    = LoggingFactory.getLogger(getClass());

    @Autowired
    @Qualifier("card-storage")
    ICardStorageService         orig;

    @Autowired
    CardStorageDAO              cardStorageDAO;

    @Autowired
    BinOfferService             binOfferService;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    DebitATMStatusHandler       klickpayGatewayHandler;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    ITTPHandler                 ittpHandler;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    TransactionTypeHandler transactionTypeHandler;
    
    @Autowired
    UserServiceProxy            userServiceProxy;

    @Autowired
    OneCheckWalletService       oneCheckWalletService;

    @Autowired
    PaymentOptionService        paymentOptionService;

    @Autowired
    FCProperties                fcProperties;

    @Autowired
    PaymentsCache               paymentsCache;
    
    @Autowired
    AppConfigService            configService;

    @Override
    public Map<String, Object> list(int userID) {
        logger.info("Inside v2 cardstorage list ");
        Map<String, Object> result = orig.list(userID);
        @SuppressWarnings("unchecked")
        List<Map<String, String>> cards = (List<Map<String, String>>) result.get("cards");
        try {
            backfillCardMeta(userID, cards);
        } catch (RuntimeException e) {
            logger.error("Error updating local database", e);
        }
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        List<Map<String, String>> orderedCards = new ArrayList<Map<String, String>>();
        if (StringUtils.isEmpty(oneCheckUserId)) {
            result.put("cards", orderedCards);
            return result;
        }
        boolean isDebitATMEligible = populateDebitATMValues(cards, oneCheckUserId);
        for (Map<String, String> each : cards) {
            orderedCards.add(each);
            each.remove(CardStorageKey.CARD_FINGERPRINT);
        }
        result.put("cards", orderedCards);
        return result;
    }

    @Override
    public Map<String, Object> list(int userID, FPSParams fpsParams) {

        Map<String, Object> result = orig.list(userID);
        @SuppressWarnings("unchecked")
        List<Map<String, String>> cards = (List<Map<String, String>>) result.get("cards");
        try {
            backfillCardMeta(userID, cards);
        } catch (RuntimeException e) {
            logger.error("Error updating local database", e);
        }
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        List<Map<String, String>> orderedCards = new ArrayList<Map<String, String>>();
        if (StringUtils.isEmpty(oneCheckUserId)){
            result.put("cards", orderedCards);
            return result;
            }
        boolean isDebitATMEligible = populateDebitATMValues(cards, oneCheckUserId);
        for (Map<String, String> each : cards) {
            String cardNumber = each.get(CardStorageKey.CARD_IS_IN);
            if (!StringUtils.isEmpty(cardNumber)) {
                logger.info("Checking Debit+ATM status for " + cardNumber);
                boolean isITTPEligible = populateITTPValues(each, oneCheckUserId, fpsParams);
                if (isITTPEligible || isDebitATMEligible) {
                    orderedCards.add(0, each);
                } else {
                    orderedCards.add(each);
                }
            }
            each.remove(CardStorageKey.CARD_FINGERPRINT);
        }
        result.put("cards", orderedCards);
        return result;
    }

    @Override
    public Map<String, Object> listV4(int userID, FPSParams fpsParams,String productType) {

        Map<String, Object> result = orig.list(userID);
        @SuppressWarnings("unchecked")
        List<Map<String, String>> cards = (List<Map<String, String>>) result.get("cards");
        try {
            backfillCardMeta(userID, cards);
        } catch (RuntimeException e) {
            logger.error("Error updating local database", e);
        }
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        List<Map<String, Object>> orderedCards = new ArrayList<Map<String, Object>>();
        if (StringUtils.isEmpty(oneCheckUserId)){
            result.put("cards", orderedCards);
            return result;
        }
        boolean isDebitATMEligible = populateDebitATMValues(cards, oneCheckUserId);
        for (Map<String, String> each : cards) {
            String cardNumber = each.get(CardStorageKey.CARD_IS_IN);
            if (!StringUtils.isEmpty(cardNumber)) {
                logger.info("Checking Debit+ATM status for " + cardNumber);
                boolean isITTPEligible = populateITTPValues(each, oneCheckUserId, fpsParams);
                Map<String,Object> cartMap=new HashMap<>();
                cartMap.putAll(each);
                if (isITTPEligible || isDebitATMEligible) {
                    orderedCards.add(0, cartMap);
                } else {
                    orderedCards.add(cartMap);
                }
            }
        }
        populateTransactionTypeValues(cards,orderedCards,fpsParams,productType);
        for (Map<String, String> each : cards) {
            each.remove(CardStorageKey.CARD_FINGERPRINT);
        }
        result.put("cards", orderedCards);
        return result;
    }


    private void populateTransactionTypeValues(List<Map<String, String>> cards,List<Map<String, Object>> orderedCards,
                                               FPSParams fpsParams,String productType){
        Map<String,Map<String,String>> cardVsTransactionTypeMap=transactionTypeHandler.getTransactionTypeStatus(cards,fpsParams,productType);
        logger.info("After getTransactionTypeStatus call");
        logger.info("cardVsTransactionTypeMap is "+cardVsTransactionTypeMap);
        for(Map<String, Object> each :orderedCards){
            String cardFingerprint = (String) each.get(CardStorageKey.CARD_FINGERPRINT);
            Map<String,String> transactionMap=new HashMap<>();
            Map<String,Object> finalTransactionMap=new HashMap<>();
            if(cardVsTransactionTypeMap!=null && cardVsTransactionTypeMap.containsKey(cardFingerprint)) {
                transactionMap = cardVsTransactionTypeMap.get(cardFingerprint);
                logger.info("transactionMap is "+transactionMap);
                finalTransactionMap.put(CardStorageKey.TRANSACTION_TYPE_STATUS,transactionMap);
                logger.info("finalTransactionMap is "+finalTransactionMap);

                each.putAll(finalTransactionMap);
            }
        }
        logger.info("After populateTransactionTypeValues call");
    }

    private boolean populateITTPValues(Map<String, String> card, String oneCheckUserId, FPSParams fpsParams){
        
        boolean isITTPEligible = false;
        //Version Control for ANDROID
        if (CardStorageKey.ANDROID_PLATFORM.equalsIgnoreCase(fpsParams.getPlatformType())) {
            JSONObject androidVersionObject = configService.getCustomProp(FCConstants.ITTP_ANDROID_VERSION_KEY);
            Object androidVersionEnabled = androidVersionObject.get(FCConstants.ITTP_ANDROID_VERSION);
            if (Integer.parseInt(fpsParams.getAppVersion()) < Integer.parseInt(String.valueOf(androidVersionEnabled))) {
                isITTPEligible = false;
                card.put(CardStorageKey.IS_ITTP_ELIGIBLE, Boolean.toString(isITTPEligible));
                return isITTPEligible;
            }
        }

      //Version Control for IOS
        if (CardStorageKey.IOS_PLATFORM.equalsIgnoreCase(fpsParams.getPlatformType())) {
            JSONObject iosVersionObject = configService.getCustomProp(FCConstants.ITTP_IOS_VERSION_KEY);
            Object iosVersionEnabled = iosVersionObject.get(FCConstants.ITTP_IOS_VERSION);
            if (Integer.parseInt(fpsParams.getAppVersion()) < Integer.parseInt(String.valueOf(iosVersionEnabled))) {
                isITTPEligible = false;
                card.put(CardStorageKey.IS_ITTP_ELIGIBLE, Boolean.toString(isITTPEligible));
                return isITTPEligible;
            }
        }
        
        Map<String, Object> ittpStatusMap = ittpHandler.getStatusForITTP(card, oneCheckUserId, fpsParams);
        if (ittpStatusMap.containsKey("status")) {
            isITTPEligible = (Boolean)ittpStatusMap.get("status");
        }
        
        card.put(CardStorageKey.IS_ITTP_ELIGIBLE, Boolean.toString(isITTPEligible));
        if (isITTPEligible) {
            String ittpProducts = null;
                String[] ittpProductsList = paymentOptionService
                        .getProductListFromCCS(fcProperties.getCcsITTPProductsList());
                if (ittpProductsList != null) {
                    ittpProducts = StringUtils.join(ittpProductsList, ",");
                }
            if (!StringUtils.isEmpty(ittpProducts)) {
                card.put("products", ittpProducts);
            }
        }
        return isITTPEligible;
    }
    
    private boolean populateDebitATMValues(List<Map<String, String>> cards, String oneCheckUserId){
        boolean isDebitATMEligible = false;
        
        List<CardDetailsDo> cardDetailsDos = new ArrayList<CardDetailsDo>();
        
        for(Map<String, String> each : cards){
            CardDetailsDo cardDo = new CardDetailsDo();
            cardDo.setCardBinHead(each.get(CardStorageKey.CARD_IS_IN).substring(0, 6));
            cardDo.setCardFingerprint(each.get(CardStorageKey.CARD_FINGERPRINT));
            cardDetailsDos.add(cardDo);
        }
        List<CardDetailsDo> debitATMStatusList = klickpayGatewayHandler.isDebitATMEnabled(
                oneCheckUserId, cardDetailsDos);
        
        for (Map<String, String> each: cards) {
            CardDetailsDo cardDetail = FCUtil.searchCardInResponse(each.get(CardStorageKey.CARD_FINGERPRINT), debitATMStatusList);
            if(cardDetail==null)
                continue;
            
            each.put(CardStorageKey.IS_DEBIT_ATM_ELIGIBLE, cardDetail.getStatus());
            isDebitATMEligible = Boolean.parseBoolean(cardDetail.getStatus());
            if (isDebitATMEligible) {
                String cardIssuer = cardDetail.getIssuer();
                String cachedProducts = null;
                if (!StringUtils.isEmpty(cardIssuer)) {
                    cachedProducts = paymentsCache.get(CACHE_KEY + DELIMITER + cardIssuer);
                    if (cachedProducts == null) {
                        String[] products = paymentOptionService
                                .getProductListFromCCS(fcProperties.getCcsDebitAtmList() + "." + cardIssuer);
                        if (products == null) {
                            products = paymentOptionService.getProductListFromCCS(fcProperties.getCcsDebitAtmList());
                        }
                        if (products != null) {
                            cachedProducts = StringUtils.join(products, ",");
                            paymentsCache.set(CACHE_KEY + DELIMITER + cardIssuer, cachedProducts);
                        } else {
                            paymentsCache.set(CACHE_KEY + DELIMITER + cardIssuer, "");
                        }
                        String cachedKeys = paymentsCache.get(PaymentConstants.DEBIT_ATM_PRODUCTS_CACHE_KEY);
                        if (StringUtils.isEmpty(cachedKeys)) {
                            paymentsCache.set(PaymentConstants.DEBIT_ATM_PRODUCTS_CACHE_KEY,
                                    CACHE_KEY + DELIMITER + cardIssuer);
                        } else {
                            paymentsCache.set(PaymentConstants.DEBIT_ATM_PRODUCTS_CACHE_KEY,
                                    cachedKeys + "," + CACHE_KEY + DELIMITER + cardIssuer);
                        }
                    }
                } else {
                    String[] debitAtmList = paymentOptionService
                            .getProductListFromCCS(fcProperties.getCcsDebitAtmList());
                    if (debitAtmList != null) {
                        cachedProducts = StringUtils.join(debitAtmList, ",");
                    }
                }
                if (!StringUtils.isEmpty(cachedProducts)) {
                    each.put("products", cachedProducts);
                }
            } 
        }
        return isDebitATMEligible;
    }
    
    @Override
    public Map<String, String> add(int userID, AddCardDetails addCardDetails, CardMetaData addCardMeta) {
        Map<String, String> result = orig.add(userID, addCardDetails, addCardMeta);
        if (!result.containsKey(CardStorageKey.CARD_REFERENCE)) {
            return new HashMap<String, String>();
        }
        addCardMeta.setCardReference(result.get(CardStorageKey.CARD_REFERENCE));
        try {
            Number key = cardStorageDAO.saveCardMeta(addCardMeta);
            if (key == null) {
                CardMetaData cardMeta = cardStorageDAO.findCardByReference(addCardMeta.getCardReference());
                key = cardMeta == null ? null : cardMeta.getCardMetaID();
            }
            if (key != null) {
                cardStorageDAO.insertUpdateUserCardMeta(userID, key.intValue());
            }
        } catch (RuntimeException e) {
            logger.error("Error update local database", e);
        }
        return result;
    }

    @Override
    public Map<String, String> add(String userID, AddCardDetails addCardDetails, CardMetaData addCardMeta) {
        Map<String, String> result = orig.add(userID, addCardDetails, addCardMeta);
        return  processCardAddResponse(result,addCardMeta);
    }

    @Override
    public Map<String, Object> delete(int userID, String cardToken) {
        Map<String, Object> result = orig.delete(userID, cardToken);
        Object deletedObject = result.get("deleted");
        Object cardRefObject = result.get(CardStorageKey.CARD_REFERENCE);
        if (deletedObject != null && (Boolean) deletedObject && cardRefObject != null) {
            try {
                cardStorageDAO.deleteUserCardMeta(userID, (String) cardRefObject);
            } catch (RuntimeException e) {
                logger.error(String.format("Error deleting user_card_meta entry given userID=%d, cardReference='%s'",
                        userID, (String) cardRefObject), e);
            }
        }
        return result;
    }

    /**
     * Card storage get() call is a definitive evidence that a user tried to pay
     * using a saved card. This function simply logs the call and backfills the
     * card metadata table.
     */
    @SuppressWarnings("unused")
    @Override
    public Map<String, String> get(int userID, String cardToken, String orderID) {
        Map<String, String> storedCard = orig.get(userID, cardToken, orderID);
        try {
            // backfill meta tables
            backfillCardMeta(userID, Collections.singletonList(storedCard));
            // populate usage table
            final String cardType = storedCard.get(CardStorageKey.CARD_TYPE);
            final String cardBrand = storedCard.get(CardStorageKey.CARD_BRAND);
            final String cardIssuer = storedCard.get(CardStorageKey.CARD_ISSUER);
            // cardStorageDAO.logCardStorageGetCall(orderID, cardType,
            // cardBrand, cardIssuer);
        } catch (RuntimeException e) {
            logger.error("Error updating local database", e);
        }
        return storedCard;
    }

    private void backfillCardMeta(int userID, List<Map<String, String>> storedCards) {
        // convert list of maps --> map (key: card-reference) of maps
        Map<String, Map<String, String>> storedCardsByReference = new LinkedHashMap<String, Map<String, String>>();
        for (Map<String, String> stored : storedCards) {
            storedCardsByReference.put(stored.get(CardStorageKey.CARD_REFERENCE), stored);
        }
        // obtain list of card references
        List<String> cardReferences = new ArrayList<String>(storedCardsByReference.keySet());
        // fetch all card meta for given card references
        Map<String, CardMetaData> existing = cardStorageDAO.findCardByReference(cardReferences);
        // determine stored cards with their metadata absent in local DB
        for (String each : cardReferences) {
            Number key = null;
            CardMetaData cardMeta = null;
            if (existing.get(each) == null) { // does not exist
                Map<String, String> storedCard = storedCardsByReference.get(each);
                cardMeta = new CardMetaData();
                cardMeta.setCardReference(storedCard.get(CardStorageKey.CARD_REFERENCE));
                cardMeta.setCardType(storedCard.get(CardStorageKey.CARD_TYPE));
                cardMeta.setCardBrand(storedCard.get(CardStorageKey.CARD_BRAND));
                cardMeta.setCardIssuer(storedCard.get(CardStorageKey.CARD_ISSUER));
                cardMeta.setCardBin(storedCard.get(CardStorageKey.CARD_IS_IN));
                cardMeta.setNicknameEmpty(FCUtil.isEmpty((String) storedCard.get(CardStorageKey.NICK_NAME)));
                /*
                 * Insert one by one, because we need the generated key to
                 * populate user_card_meta table. JDBC (hence SpringJDBC)
                 * doesn't guarantee to return generated keys for batch inserts!
                 */
                key = cardStorageDAO.insertMissingCardMeta(cardMeta);
            } else {
                cardMeta = existing.get(each);
                key = cardMeta == null ? null : cardMeta.getCardMetaID();
            }
            if (key != null) {
                int cardMetaID = key.intValue();
                cardStorageDAO.insertUpdateUserCardMeta(userID, cardMetaID);
            } else {
                String msg = String.format("Card meta data (userID=%d) neither exists in DB nor can be persisted: %s",
                        userID, cardMeta);
                logger.error(msg);
            }
        }
    }

    private Map<String,String> processCardAddResponse(Map<String,String> result,CardMetaData addCardMeta){
        if (!result.containsKey(CardStorageKey.CARD_REFERENCE)) {
            return new HashMap<String, String>();
        }
        addCardMeta.setCardReference(result.get(CardStorageKey.CARD_REFERENCE));
        try {
            Number key = cardStorageDAO.saveCardMeta(addCardMeta);
            if (key == null) {
                CardMetaData cardMeta = cardStorageDAO.findCardByReference(addCardMeta.getCardReference());
                key = cardMeta == null ? null : cardMeta.getCardMetaID();
            }
        } catch (RuntimeException e) {
            logger.error("Error update local database", e);
        }
        return result;
    }

}
