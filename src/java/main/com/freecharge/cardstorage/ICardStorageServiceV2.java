package com.freecharge.cardstorage;

import java.util.Map;

import com.freecharge.rest.model.FPSParams;

public interface ICardStorageServiceV2 extends ICardStorageService {
    /**
     * Return a map of the list of cards against given userID. The key "cards"
     * should point to a List<Map<String, String>>, where keys in each map
     * should be used from the CardStorageKey class. It also checks for the fps score
     * for each card that is eligible for ITTP option.
     * @param userID
     * @param fpsParams
     * @return
     */
    public Map<String, Object> list(int userID, FPSParams fpsParams);

    public Map<String, Object> listV4(int userID, FPSParams fpsParams,String productType);



}
