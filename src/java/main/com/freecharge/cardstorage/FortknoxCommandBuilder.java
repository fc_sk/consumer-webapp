package com.freecharge.cardstorage;

import java.util.concurrent.Callable;

import com.freecharge.common.timeout.ExternalClientCallCommand;
import com.freecharge.common.timeout.TimeoutKey;
import com.freecharge.fortknox.FortKnoxClient;
import com.freecharge.fortknox.core.AddCardBean;
import com.freecharge.fortknox.core.AddCardResult;
import com.freecharge.fortknox.core.Card;
import com.freecharge.fortknox.core.DeleteCardResult;
import com.freecharge.fortknox.core.ListCardResult;

public final class FortknoxCommandBuilder {

    public static ExternalClientCallCommand<ListCardResult> getListCardCommand(final FortKnoxClient fortKnoxClient,
            final String customerId) {
        return new ExternalClientCallCommand<ListCardResult>("Payment.FortKnox", "cardList",
                new Callable<ListCardResult>() {
                    @Override
                    public ListCardResult call() throws Exception {
                        return fortKnoxClient.listCards(customerId);
                    }
                }, TimeoutKey.FORTKNOX_LIST_CARD);
    }

    public static ExternalClientCallCommand<AddCardResult> getAddCardCommand(final FortKnoxClient fortKnoxClient,
            final AddCardBean bean, String channel) {
        return new ExternalClientCallCommand<AddCardResult>("Payment.FortKnox", "cardAdd." + channel,
                new Callable<AddCardResult>() {
                    @Override
                    public AddCardResult call() throws Exception {
                        return fortKnoxClient.addCard(bean);
                    }

                }, TimeoutKey.FORTKNOX_ADD_CARD);
    }

    public static ExternalClientCallCommand<DeleteCardResult> getDeleteCardCommand(final FortKnoxClient fortKnoxClient,
            final String cardToken, final String customerId) {
        return new ExternalClientCallCommand<DeleteCardResult>("Payment.FortKnox", "cardDelete",
                new Callable<DeleteCardResult>() {
                    @Override
                    public DeleteCardResult call() throws Exception {
                        return fortKnoxClient.deleteCard(cardToken, customerId);
                    }
                }, TimeoutKey.FORTKNOX_DELETE_CARD);
    }

    public static ExternalClientCallCommand<Card> getGetCardCommand(final FortKnoxClient fortKnoxClient,
            final String cardToken, final String customerId) {
        return new ExternalClientCallCommand<Card>("Payment.FortKnox", "cardGet", new Callable<Card>() {
            @Override
            public Card call() throws Exception {
                return fortKnoxClient.getCard(cardToken, customerId);
            }
        }, TimeoutKey.FORTKNOX_GET_CARD);
    }

}
