package com.freecharge.cardstorage;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.wallet.OneCheckWalletService;

@Service
@Qualifier("card-storage")
public class FKMigrationCardStorageService extends AbstractCardStorageService {

    private static final Logger        logger = Logger.getLogger(FKMigrationCardStorageService.class);

    @Autowired
    @Qualifier("juspay-card-storage")
    private JuspayCardStorageService   juspayCardStorageService;

    @Autowired
    @Qualifier("fk-card-storage")
    private FortKnoxCardStorageService fortKnoxCardStorageService;

    @Autowired
    private AppConfigService           appConfig;
    
    @Autowired
    OneCheckWalletService oneCheckWalletService;

    @Override
    public Map<String, Object> list(int userID) {
        if (!oneCheckWalletService.isLoggedInOCUser(null)) {
            Map<String, Object> result = new LinkedHashMap<String, Object>();
            List<Map<String, String>> cards = new ArrayList<Map<String, String>>();
            logger.error("User token seems to be expired. Sending empty cards for user Id : " + userID);
            result.put("cards", cards);
            return result;
        }
        if (shouldGoThroughFortKnox()) {
            logger.info("Using fortknox client to list the cards for user Id " + userID);
            return fortKnoxCardStorageService.list(userID);
        }
        logger.info("Using juspay client to list the cards for user Id " + userID);
        return juspayCardStorageService.list(userID);
    }

    @Override
    public Map<String, String> add(int userID, AddCardDetails addCardDetails, CardMetaData addCardMeta) {
        if (shouldGoThroughFortKnox()) {
            logger.info("Using fortknox client to add the card for user Id " + userID);
            return fortKnoxCardStorageService.add(userID, addCardDetails, addCardMeta);
        }
        logger.info("Using juspay client to add the card for user Id " + userID);
        return juspayCardStorageService.add(userID, addCardDetails, addCardMeta);
    }

    @Override
    public Map<String, String> add(String userId, AddCardDetails addCardDetails, CardMetaData addCardMeta) {
        Map<String,String> result=new HashMap<>();
        if (shouldGoThroughFortKnox()) {
            logger.info("Using fortknox client to add the card for user Id " + userId);
            return fortKnoxCardStorageService.add(userId, addCardDetails, addCardMeta);
        }
        return result;

    }

    @Override
    public Map<String, Object> delete(int userID, String cardToken) {
        if (shouldGoThroughFortKnox()) {
            logger.info("Using fortknox client to delete the card for user Id " + userID);
            return fortKnoxCardStorageService.delete(userID, cardToken);
        }
        logger.info("Using juspay client to delete the card for user Id " + userID);
        return juspayCardStorageService.delete(userID, cardToken);
    }

    @Override
    public Map<String, String> get(int userID, String cardToken, String orderID) {
        if (shouldGoThroughFortKnox()) {
            logger.info("Using fortknox client to get the card for user Id " + userID);
            return fortKnoxCardStorageService.get(userID, cardToken, orderID);
        }
        logger.info("Using juspay client to get the card for user Id " + userID);
        return juspayCardStorageService.get(userID, cardToken, orderID);
    }

    // TODO: Handle using the appconfig.
    private boolean shouldGoThroughFortKnox() {

        JSONObject configJson = appConfig.getCustomProp(AppConfigService.FORTKNOX_CARD_MIGRATION);

        if (configJson == null) {
            return false;
        }

        String enabledString = (String) configJson.get("enabled");
        if (StringUtils.isNotBlank(enabledString)) {
            if (!Boolean.parseBoolean(enabledString)) {
                return false;
            }
        }

        String maxValueString = (String) configJson.get("value");
        Integer count = null;
        if (StringUtils.isNotBlank(maxValueString)) {
            count = Integer.parseInt(maxValueString);
        }
        if (count == null) {
            return false;
        }
        int rand = RandomUtils.nextInt(9999);
        if (rand % count == 0) {
            return true;
        }
        return false;
    }
}
