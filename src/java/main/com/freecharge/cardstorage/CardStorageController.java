package com.freecharge.cardstorage;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.payment.exception.HTTPAbortException;
import com.freecharge.payment.exception.VirtualCardStorageServiceException;
import com.freecharge.payment.model.InstantCreditCardApiErrorCodes;
import com.freecharge.payment.services.InstantCreditCardHandler;
import com.freecharge.web.webdo.*;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;

import java.util.Map;

@Controller
@RequestMapping("/app/*")
public class CardStorageController {

    private final Logger        log               = LoggingFactory.getLogger(getClass());
    public static final String  METRICS_SERVICE   = "cardStorageController";
    public static final String  METRIC_ADD_CARD   = "addCard";
    public static final String  METRIC_ADD_VC_CARD= "addVCCard";
    public static final String  METRIC_CARDS_LIST = "listCards";
    public static final String  METRIC_ATTEMPT    = "attempt";
    public static final String  METRIC_SUCCESS    = "success";
    public static final String  METRIC_FAILURE    = "failure";

    @Autowired
    @Qualifier("card-and-meta")
    ICardStorageService         cardStorageService;

    @Autowired
    private CardStorageService  cardService;

    @Autowired
    private AppConfigService    appConfigService;

    @Autowired
    private MetricsClient       metricsClient;

    @Autowired
    private InstantCreditCardHandler  instantCreditCardHandler;

    private static final String NOLOGIN           = "{\"status\": \"failure\", \"message\": \"User not logged in\"}";
    private static final String FAILURE           = "{\"status\": \"failure\", \"message\": \"%s\"}";
    private static final String SUCCESS           = "{\"status\": \"success\", \"payload\": %s}";

    @RequestMapping(value = "cards/list", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String list() {
        Integer userID = FCSessionUtil.getLoggedInUserId();
        if (userID == null) {
            return NOLOGIN;
        }
        try {
            if (appConfigService.isCardStorageEnabled()) {
                String json = JSONObject.toJSONString(cardStorageService.list(userID));
                return String.format(SUCCESS, json);
            } else {
                return String.format(FAILURE, "Card storage disabled");
            }
        } catch (RuntimeException e) {
            log.error("Error in card storage list()", e);
            return String.format(FAILURE, "Error fetching list of stored cards");
        }
    }

    @RequestMapping(value = "card/{cardToken}/delete", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String delete(@PathVariable String cardToken) {
        Integer userID = FCSessionUtil.getLoggedInUserId();
        if (userID == null) {
            return NOLOGIN;
        }
        try {
            String json = JSONObject.toJSONString(cardStorageService.delete(userID, cardToken));
            return String.format(SUCCESS, json);
        } catch (RuntimeException e) {
            log.error("Error in card storage delete()", e);
            return String.format(FAILURE, "Error deleting stored card");
        }
    }

    @RequestMapping(value = "card/add", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    boolean add(CardDetailsWebDO cardWebDO) {
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_CARD, METRIC_ATTEMPT);
        Integer userId = FCSessionUtil.getLoggedInUserId();
        if (userId == null) {
            return false;
        }
        PaymentRequestCardDetails paymentRequestCardDetails = PaymentUtil.convertCardDetailsToPaymentRequestCardDetails(cardWebDO);
        return cardService.addCard(paymentRequestCardDetails, userId, null, cardWebDO.getPyop());
    }


    @RequestMapping(value = "card/store/vcc", method = RequestMethod.POST, produces = "application/json")
    @Csrf(exclude =true)
    @NoLogin
    public @ResponseBody VccCardStorageResponse  createAndStoreVccCards(@RequestBody VccCardStorageRequest vccCardStorageRequest) {
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_ADD_VC_CARD, METRIC_ATTEMPT);
        log.info("card fetch and save processing started with  Serial No. = "+vccCardStorageRequest.getApplicationSerNo()+" user_id ="+vccCardStorageRequest.getUserId());
        String userId=vccCardStorageRequest.getUserId();
        VccCardStorageResponse vccCardStorageResponse=new VccCardStorageResponse();
        if (StringUtils.isEmpty(userId)) {
            vccCardStorageResponse.setResponseCode(InstantCreditCardApiErrorCodes.USER_ID_INVALID.getErrorCode());
            vccCardStorageResponse.setResponseMessage(InstantCreditCardApiErrorCodes.USER_ID_INVALID.getErrorMessage());
            return vccCardStorageResponse;
        }
        if(StringUtils.isEmpty(vccCardStorageRequest.getApplicationSerNo())){
            vccCardStorageResponse.setResponseCode(InstantCreditCardApiErrorCodes.APP_SERIAL_INVALID.getErrorCode());
            vccCardStorageResponse.setResponseMessage(InstantCreditCardApiErrorCodes.APP_SERIAL_INVALID.getErrorMessage());
            return vccCardStorageResponse;

        }
        instantCreditCardHandler.validateChecksum(vccCardStorageRequest);
        InstantCreditCardResponse instantCreditCardResponse =instantCreditCardHandler.fetchInstantCreditCardDetail(vccCardStorageRequest);
        PaymentRequestCardDetails paymentRequestCardDetails=instantCreditCardHandler.convertCardDetailsToPaymentRequestCardDetails(instantCreditCardResponse,vccCardStorageResponse);
        instantCreditCardHandler.saveInstantCreditCard(paymentRequestCardDetails,userId);
        vccCardStorageResponse.setResponseCode(InstantCreditCardApiErrorCodes.SUCCESS.getErrorCode());
        vccCardStorageResponse.setResponseMessage(InstantCreditCardApiErrorCodes.SUCCESS.getErrorMessage());
        log.info("card successfully processed for Serial No. = "+vccCardStorageRequest.getApplicationSerNo()+" user_id ="+vccCardStorageRequest.getUserId());
        return vccCardStorageResponse;
    }


    @ExceptionHandler(VirtualCardStorageServiceException.class)
    private @ResponseBody
    VccCardStorageResponse VirtualCardStorageServiceException(VirtualCardStorageServiceException e) {
        VccCardStorageResponse vccCardStorageResponse=new VccCardStorageResponse();
        vccCardStorageResponse.setResponseCode(e.getErrorCode());
        vccCardStorageResponse.setResponseMessage(e.getErrorMessage());
        if(e!=null)
            log.error("Cause : ",e.getCause());
        return vccCardStorageResponse;

    }
}
