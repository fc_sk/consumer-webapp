package com.freecharge.cardstorage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.FCProperties;

@Service
public abstract class AbstractCardStorageService implements ICardStorageService {

	@Autowired
	protected FCProperties fcProps;

	protected String getCustomerID(int userID) {
		return fcProps.getMode() + "-" + userID;
	}

}
