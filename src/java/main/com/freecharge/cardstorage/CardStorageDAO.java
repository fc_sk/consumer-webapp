package com.freecharge.cardstorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.util.FCUtil;

public class CardStorageDAO {
    private SimpleJdbcInsert insertUserCardStorageChoicePreference;
    private NamedParameterJdbcTemplate jt;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertUserCardStorageChoicePreference = new SimpleJdbcInsert(dataSource).withTableName(UserCardStorageChoicePreference.USER_CARD_STORAGE_CHOICE_PREFERENCE_TABLE).usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    private static final String INSERT_CARD_SQL = "INSERT INTO" +
    		" card_storage (order_id, card_type, card_brand, card_issuer, access_ts)" +
    		" VALUES (:order_id, :card_type, :card_brand, :card_issuer, :ts)" +
    		" ON DUPLICATE KEY UPDATE" +
    		" card_type=:card_type," +
    		" card_brand=:card_brand," +
    		" card_issuer=:card_issuer," +
    		" access_ts=:ts";

    public void logCardStorageGetCall(String orderID,
    		String cardType, String cardBrand, String cardIssuer) {
    	Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("order_id", orderID);
        insertParams.put("card_type", cardType);
        insertParams.put("card_brand", cardBrand);
        insertParams.put("card_issuer", cardIssuer);
        insertParams.put("ts", new Timestamp(System.currentTimeMillis()));
        jt.update(INSERT_CARD_SQL, insertParams);
    }

    private static final String INSERT_META_SQL = "INSERT INTO" +
    		" card_meta (card_ref, card_type, card_brand, card_issuer, card_bin, nickname_empty, created_ts)" +
    		" VALUES (:card_ref, :card_type, :card_brand, :card_issuer, :card_bin, :nickname_empty, :ts)" +
    		" ON DUPLICATE KEY UPDATE" +
    		" card_type=:card_type," +
    		" card_brand=:card_brand," +
    		" card_issuer=:card_issuer," +
    		" card_bin=:card_bin," +
    		" nickname_empty=:nickname_empty," +
    		" created_ts=:ts";

    public Number saveCardMeta(CardMetaData cardMeta) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	if(StringUtils.isEmpty(cardMeta.getCardReference())){
    	    return null;
    	}
    	params.put("card_ref", cardMeta.getCardReference());
    	params.put("card_type", cardMeta.getCardType());
    	params.put("card_brand", cardMeta.getCardBrand());
    	params.put("card_issuer", cardMeta.getCardIssuer());
    	params.put("card_bin", cardMeta.getCardBin());
    	params.put("nickname_empty", cardMeta.isNicknameEmpty());
    	params.put("ts", new Timestamp(new Date().getTime()));
    	KeyHolder holder = new GeneratedKeyHolder();
    	jt.update(INSERT_META_SQL, new MapSqlParameterSource(params), holder);
    	List<Map<String, Object>> keys = holder.getKeyList();
    	if (keys.size() > 1) {
    		Map<String, Object> p1 = keys.get(0);
    		Map<String, Object> p2 = keys.get(1);
    		Long k1 = (Long) p1.get(p1.keySet().iterator().next());
    		Long k2 = (Long) p1.get(p2.keySet().iterator().next());
    		return Math.min(k1, k2);
    	}
    	return holder.getKey();
    }

    private static final String INSERT_MISSING_META_SQL = "INSERT INTO" +
    		" card_meta (card_ref, card_type, card_brand, card_issuer, card_bin, nickname_empty, created_ts)" +
    		" SELECT * FROM (SELECT :card_ref AS card_ref, :card_type AS card_type, :card_brand AS card_brand, :card_issuer AS card_issuer, :card_bin AS card_bin, :nickname_empty AS nickname_empty, :ts AS created_ts) AS tmp" +
    		" WHERE NOT EXISTS (" +
    		"       SELECT card_ref FROM card_meta WHERE card_ref = :card_ref" +
    		" ) LIMIT 1";

    /**
     * Inserts a record only when missing. Return the auto-incremented primary
     * key on successful insert, null otherwise.
     * @param cardMeta
     * @return
     */
    public Number insertMissingCardMeta(CardMetaData cardMeta) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("card_ref", cardMeta.getCardReference());
    	params.put("card_type", cardMeta.getCardType());
    	params.put("card_brand", cardMeta.getCardBrand());
    	params.put("card_issuer", cardMeta.getCardIssuer());
    	params.put("card_bin", cardMeta.getCardBin());
    	params.put("nickname_empty", cardMeta.isNicknameEmpty());
    	params.put("ts", new Timestamp(new Date().getTime()));
    	KeyHolder holder = new GeneratedKeyHolder();
    	jt.update(INSERT_MISSING_META_SQL, new MapSqlParameterSource(params), holder);
    	return holder.getKey();
    }

    private static final String FIND_META_SQL_FORMAT = "SELECT" +
    		" card_meta_id, card_ref, card_type, card_brand, card_issuer" +
    		" FROM card_meta" +
    		" WHERE card_ref IN (%s)";

    /**
     * Given one or more cardReference objects, return a map of cardReference
     * to CarMetadata objects from the DB.
     * @param cardReferences
     * @return
     */
    public Map<String, CardMetaData> findCardByReference(final List<String> cardReferences) {
    	if (cardReferences.isEmpty()) {
    		return new HashMap<String, CardMetaData>();
    	}
    	Map<String, Object> params = new HashMap<String, Object>();
    	StringBuilder csv = new StringBuilder();
    	for (int i = 0; i < cardReferences.size(); i++) {
    		params.put("card_ref" + i, cardReferences.get(i));
    		csv.append((i > 0? ", ": "") + ":card_ref" + i);
    	}
    	String sql = String.format(FIND_META_SQL_FORMAT, csv.toString());
    	List<CardMetaData> rows = jt.query(sql, params, new RowMapper<CardMetaData>() {
    		@Override
    		public CardMetaData mapRow(ResultSet rs, int rowNum)
    				throws SQLException {
    			CardMetaData meta = new CardMetaData();
    			meta.setCardMetaID(rs.getInt("card_meta_id"));
    			meta.setCardReference(rs.getString("card_ref"));
    			meta.setCardType(rs.getString("card_type"));
    			meta.setCardBrand(rs.getString("card_brand"));
    			meta.setCardIssuer(rs.getString("card_issuer"));
    			return meta;
    		}
		});
    	Map<String, CardMetaData> result = new LinkedHashMap<String, CardMetaData>();
    	for (CardMetaData each: rows) {
    		result.put(each.getCardReference(), each);
    	}
    	return result;
    }

    public CardMetaData findCardByReference(final String cardReference) {
    	return findCardByReference(Collections.singletonList(cardReference))
    			.get(cardReference);
    }

    private static final String INSERT_UPDATE_USER_CARD_META = "INSERT INTO" +
    		" user_card_meta (fk_user_id, fk_card_meta_id, created_ts)" +
    		" VALUES (:user_id, :card_meta_id, :ts)" +
    		" ON DUPLICATE KEY UPDATE" +
    		" created_ts=:ts";

    public void insertUpdateUserCardMeta(int userID, int cardMetaID) {
    	//Diabling inserts as query taking 3sec and no read is happening from this legacy table
//    	Map<String, Object> params = new HashMap<String, Object>();
//    	params.put("user_id", userID);
//    	params.put("card_meta_id", cardMetaID);
//    	params.put("ts", new Timestamp(new Date().getTime()));
//    	jt.update(INSERT_UPDATE_USER_CARD_META, params);
    }

    private static final String FIND_CARD_META_ID_BY_REF = "SELECT" +
    		" card_meta_id FROM card_meta WHERE card_ref = :card_ref";

    private static final String DELETE_USER_CARD_META = "DELETE" +
    		" FROM user_card_meta" +
    		" WHERE fk_user_id = :user_id AND fk_card_meta_id = :card_meta_id";

    public void deleteUserCardMeta(int userID, String cardReference) {
    	if (!FCUtil.isEmpty(cardReference)) {
    		List<Long> ids = jt.queryForList(FIND_CARD_META_ID_BY_REF,
    				Collections.singletonMap("card_ref", cardReference), Long.class);
    		if (!ids.isEmpty()) {
    			long cardMetaID = ids.get(0);
    			jt.update(DELETE_USER_CARD_META, FCUtil.createMap(
    					"user_id", userID,
    					"card_meta_id", cardMetaID));
    		}
    	}
    }

    public long insertUserCardStorageChoicePreference(UserCardStorageChoicePreference userCardStorageChoicePreference) {
        Number primaryKey = insertUserCardStorageChoicePreference.execute(userCardStorageChoicePreference.getMapSqlParameterSource());
        return primaryKey.longValue();
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(CardStorageDAO.class.getSimpleName());
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        CardStorageDAO cardStorageDAO = ((CardStorageDAO) classPathXmlApplicationContext.getBean(CardStorageDAO.getBeanName()));

        UserCardStorageChoicePreference userCardStorageChoicePreference = new UserCardStorageChoicePreference();
        userCardStorageChoicePreference.setPreference(UserCardStorageChoicePreference.Preference.DO_NOT_CHOOSE);
        userCardStorageChoicePreference.setUserId(1);
        cardStorageDAO.insertUserCardStorageChoicePreference(userCardStorageChoicePreference);

        classPathXmlApplicationContext.close();
    }
}
