package com.freecharge.cardstorage;

public final class CardStorageKey {

	public static final String NICK_NAME         = "nick_name";
	public static final String CARD_BIN          = "card_bin";
	public static final String CARD_REFERENCE    = "card_reference";
	public static final String CARD_TOKEN        = "card_token";
	public static final String CARD_EXPIRY_MONTH = "card_expiry_month";
	public static final String CARD_EXPIRY_YEAR  = "card_expiry_year";
	public static final String CARD_IS_IN        = "card_is_in";
	public static final String CARD_NUMBER       = "card_number";
	public static final String CARD_TYPE         = "card_type";
	public static final String CARD_ISSUER       = "card_issuer";
	public static final String CARD_BRAND        = "card_brand";
	public static final String NAME_ON_CARD      = "name_on_card";
	public static final String CARD_FINGERPRINT        = "card_fingerprint";
	
	// auxiliary attributes
    public static final String MAYBE_ELIGIBLE_BIN_BASED_OFFER = "maybe_eligible_bin_based_offer";
    public static final String IS_CARD_EXPIRE                 = "is_card_expire";
    public static final String IS_DEBIT_ATM_ELIGIBLE          = "atm_pin";
    public static final String IS_ITTP_ELIGIBLE               = "ittp_eligible";
    public static final String TRANSACTION_TYPE_STATUS		  = "transaction_type_status";
    public static final String IOS_PLATFORM                   = "IOS";
    public static final String ANDROID_PLATFORM               = "ANDROID";

}
