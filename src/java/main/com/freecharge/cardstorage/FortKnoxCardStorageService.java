package com.freecharge.cardstorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.fortknox.Environment;
import com.freecharge.fortknox.FortKnoxClient;
import com.freecharge.fortknox.core.AddCardBean;
import com.freecharge.fortknox.core.AddCardResult;
import com.freecharge.fortknox.core.Card;
import com.freecharge.fortknox.core.DeleteCardResult;
import com.freecharge.fortknox.core.ListCard;
import com.freecharge.fortknox.core.ListCardResult;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.wallet.OneCheckWalletService;

@Service
@Qualifier("fk-card-storage")
public class FortKnoxCardStorageService extends AbstractCardStorageService {

    private final Logger   log = LoggingFactory.getLogger(getClass());

    @Autowired
    private CardStorageDAO cardStorageDAO;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private ExternalClientCallService externalClientCallService;

    private void LOG(long startMillis, String message) {
        log.info("[" + (System.currentTimeMillis() - startMillis) + "ms] " + message);
    }

    private void LOG(long startMillis, String message, Throwable e) {
        log.error("[" + (System.currentTimeMillis() - startMillis) + "ms] " + message, e);
    }

    private FortKnoxClient initForKnoxClient(String oneCheckUserId) {
        FortKnoxClient client = null;
        if (oneCheckUserId != null) {
            Environment fkEnv = Environment.QA;
            if (fcProps.isProdMode() || fcProps.isPreProdMode()) {
                fkEnv = Environment.PROD;
            }
            client = new FortKnoxClient(fcProperties.getOneCheckFortknoxAccessKey(),
                    fcProperties.getOneCheckFortknoxSecretKey(), fcProperties.getFortknoxConnectTimeout(),
                    fcProperties.getFortknoxReadTimeout(), fkEnv);
        } else {
            Environment fkEnv = Environment.QA;
            if (fcProps.isProdMode() || fcProps.isPreProdMode()) {
                fkEnv = Environment.PROD;
            }
            client = new FortKnoxClient(fcProperties.getFortknoxAccessKey(), fcProperties.getFortknoxSecretKey(),
                    fcProperties.getFortknoxConnectTimeout(), fcProperties.getFortknoxReadTimeout(), fkEnv);
        }
        return client;
    }

    private ArrayList<ListCard> removeExpiredCards(List<ListCard> cards) {

        ArrayList<ListCard> cardsList = new ArrayList<ListCard>();
        if (cards == null) {
            return null;
        }
        for (ListCard each : cards) {
            if (!Boolean.parseBoolean(each.getExpired())) {
                cardsList.add(each);
            }
        }
        return cardsList;
    }

    @Override
    public Map<String, Object> list(int userID) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        List<Map<String, String>> cards = new ArrayList<Map<String, String>>();
        if(!appConfigService.isCardStorageEnabled()){
            result.put("cards", cards);
            return result;
        }
        String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        FortKnoxClient fortKnoxClient = initForKnoxClient(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        
        final long startMillis = System.currentTimeMillis();
        ListCardResult listCardresult = externalClientCallService
                .executeWithTimeOut(FortknoxCommandBuilder.getListCardCommand(fortKnoxClient, customerId));
        LOG(startMillis, "List-cards response (userID=" + userID + "): " + listCardresult);
        if(listCardresult == null){
            result.put("cards", cards);
            return result;
        }
        List<String> cardReferences = new ArrayList<String>();
        ArrayList<ListCard> cardsList = removeExpiredCards(listCardresult.getCards());
        listCardresult.setCards(cardsList);
        if (listCardresult.getCards() != null) {

            for (ListCard each : listCardresult.getCards()) {
                cardReferences.add(each.getCardReference());
            }
            Map<String, CardMetaData> allMeta = cardStorageDAO.findCardByReference(cardReferences);
            for (ListCard each : listCardresult.getCards()) {
                boolean isCardSaveNameIsValid = PaymentUtil.isWelFormedName(each.getNickName());
                if (!isCardSaveNameIsValid) {
                    each.setNickName("");
                }
                final Map<String, String> cardDetailsMap = listCardToMap(each, allMeta.get(each.getCardReference()));

                // Removing the fields 'card_expiry_month' and 'card_expiry_year'
                // from card list call.
                cardDetailsMap.remove(CardStorageKey.CARD_EXPIRY_MONTH);
                cardDetailsMap.remove(CardStorageKey.CARD_EXPIRY_YEAR);
                if (isWellFormedCard(cardDetailsMap)) {
                    cards.add(cardDetailsMap);
                } else {
                    log.error("Found un-wellformed card; not returning");
                }
            }
        }
        result.put("cards", cards);
        return result;
    }

    /**
     * Validates whether the card that we got back from JusPay is goot to use,
     * i.e., the number is not blank, type is not blank etc.,
     * 
     * @return
     */
    private Boolean isWellFormedCard(Map<String, String> cardDetailsMap) {
        if (cardDetailsMap.isEmpty()) {
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_REFERENCE)) {
            log.error("No card reference");
            return false;
        }

        final String cardReference = cardDetailsMap.get(CardStorageKey.CARD_REFERENCE);

        if (StringUtils.isBlank(cardReference)) {
            log.error("Card reference is empty!");
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_NUMBER)) {
            log.error("No card number for reference : [" + cardReference + "]");
            return false;
        }

        if (StringUtils.isBlank(cardDetailsMap.get(CardStorageKey.CARD_NUMBER))) {
            log.error("Blank card number for reference : [" + cardReference + "]");
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_TYPE)) {
            log.error("No card type for reference : [" + cardReference + "]");
            return false;
        }

        if (StringUtils.isBlank(cardDetailsMap.get(CardStorageKey.CARD_TYPE))) {
            log.error("Empty card type for reference : [" + cardReference + "]");
            return false;
        }

        return true;
    }

    public Map<String, String> toMap(Card card, CardMetaData cardMeta) {
        // fix guessed values if incomplete
        String expiryMonth = card.getCardExpiryMonth();
        String expiryYear = card.getCardExpiryYear();
        // use Juspay's inferred data only if unavailable in Freecharge database
        String cardType = card.getCardType();
        String cardBrand = card.getCardBrand();
        String cardIssuer = card.getCardIssuer();
        
        // prepare result
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.NICK_NAME, card.getNickName());
        result.put(CardStorageKey.CARD_REFERENCE, card.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, card.getCardToken());
        result.put(CardStorageKey.CARD_EXPIRY_MONTH, expiryMonth);
        result.put(CardStorageKey.CARD_EXPIRY_YEAR, expiryYear);
        result.put(CardStorageKey.CARD_IS_IN, card.getCardIsIn());
        result.put(CardStorageKey.CARD_NUMBER, card.getCardNumber());
        result.put(CardStorageKey.CARD_TYPE, cardType);
        result.put(CardStorageKey.CARD_ISSUER, cardIssuer);
        result.put(CardStorageKey.CARD_BRAND, cardBrand);
        result.put(CardStorageKey.NAME_ON_CARD, card.getNameOnCard());
        result.put(CardStorageKey.CARD_FINGERPRINT , card.getCardFingerPrint());
        return result;
    }

    public Map<String, String> listCardToMap(ListCard listCard, CardMetaData cardMeta) {
        // use Juspay's inferred data only if unavailable in Freecharge database
        String cardType = listCard.getCardType();
        String cardBrand = listCard.getCardBrand();
        String cardIssuer = listCard.getCardIssuer();
        // prepare result
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.NICK_NAME, listCard.getNickName());
        result.put(CardStorageKey.CARD_REFERENCE, listCard.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, listCard.getCardToken());
        result.put(CardStorageKey.CARD_IS_IN, listCard.getCardIsIn());
        result.put(CardStorageKey.CARD_NUMBER, listCard.getCardMask());
        result.put(CardStorageKey.CARD_TYPE, cardType);
        result.put(CardStorageKey.CARD_ISSUER, cardIssuer);
        result.put(CardStorageKey.CARD_BRAND, cardBrand);
        result.put(CardStorageKey.CARD_FINGERPRINT, listCard.getCardFingerPrint());
        return result;
    }

    @Override
    public Map<String, String> add(int userID, AddCardDetails addCardDetails, CardMetaData cardMeta) {
        if(!appConfigService.isCardStorageEnabled()){
            return new HashMap<String, String>();
        }
        String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        FortKnoxClient fortKnoxClient = initForKnoxClient(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        boolean isCardSaveNameIsValid = PaymentUtil.isWelFormedName(addCardDetails.getNickname());
        if (!isCardSaveNameIsValid) {
            addCardDetails.setNickname("");
        }
        
        AddCardBean bean = new AddCardBean();
        bean.setCustomerId(customerId);
        // TODO: Do we need to send the emailId?
        bean.setCustomerEmail("");
        bean.setCardNumber(addCardDetails.getCardNumber());
        bean.setExpiryYear(addCardDetails.getCardExpiryYear());
        bean.setExpiryMonth(addCardDetails.getCardExpiryMonth());
        bean.setNameOnCard(addCardDetails.getNameOnCard());
        bean.setNickName(addCardDetails.getNickname());
        
        final long startMillis = System.currentTimeMillis();
        AddCardResult addCardResult = externalClientCallService.executeWithTimeOut(
                FortknoxCommandBuilder.getAddCardCommand(fortKnoxClient, bean, addCardDetails.getChannel()));
        if(addCardResult == null){
            throw new RuntimeException("Error calling addCard for (userID=" + userID + ")"); 
        }
        LOG(startMillis, "Add-card response (userID=" + userID + "): " + addCardResult);
        if (cardMeta == null) {
            log.error("Add-card metadata not available for (userID=" + userID + "), cardDetails="
                    + addCardDetails.toString());
        } else {
            final long beforeSave = System.currentTimeMillis();
            try {
                cardMeta.setCardReference(addCardResult.getCardReference());
                cardStorageDAO.saveCardMeta(cardMeta);
            } catch (RuntimeException e) {
                LOG(beforeSave, "Error saving card metadata (userID=" + userID + "): " + cardMeta.toString(), e);
            }
        }
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.CARD_REFERENCE, addCardResult.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, addCardResult.getCardToken());
        return result;
    }

    @Override
    public Map<String, String> add(String userID, AddCardDetails addCardDetails, CardMetaData cardMeta) {
        if(!appConfigService.isCardStorageEnabled()){
            return new HashMap<String, String>();
        }
        String customerId="";
        if (userID != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = userID;
        }
        return saveCard(addCardDetails,customerId,cardMeta);
    }


    @Override
    public Map<String, Object> delete(int userID, String cardToken) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        FortKnoxClient fortKnoxClient = initForKnoxClient(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        final long startMillis = System.currentTimeMillis();
        DeleteCardResult deleteCardResult = externalClientCallService.executeWithTimeOut(
                FortknoxCommandBuilder.getDeleteCardCommand(fortKnoxClient,cardToken, customerId));
        if(deleteCardResult == null){
            LOG(startMillis, "Error in Delete-card response (userID=" + userID + ", cardToken=" + cardToken + "): "
                    + deleteCardResult);
            throw new RuntimeException("Error calling deleteCard for (userID=" + userID + "), cardToken=" + cardToken + ")");
        }
        LOG(startMillis,
                "Delete-card response (userID=" + userID + " , cardToken=" + cardToken + "): " + deleteCardResult);
        result.put(CardStorageKey.CARD_TOKEN, deleteCardResult.getCardToken());
        result.put("deleted", deleteCardResult.isDeleted());
        result.put(CardStorageKey.CARD_REFERENCE, deleteCardResult.getCardReference());
        return result;
    }

    @Override
    public Map<String, String> get(int userID, String cardToken, String orderID) {

        String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        FortKnoxClient fortKnoxClient = initForKnoxClient(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        log.info("Requesting card with cardToken=" + cardToken + " userID=" + userID + " orderID=" + orderID);
        Card card  = externalClientCallService.executeWithTimeOut(
                FortknoxCommandBuilder.getGetCardCommand(fortKnoxClient,cardToken, customerId));
        Map<String, String> result = new LinkedHashMap<String, String>();
        if(card == null){
            log.error("Some error while getting the card details for card token " + cardToken);
            return result;
        }
        result.put(CardStorageKey.CARD_TOKEN, card.getCardToken());
        String cardReference = card.getCardReference();
        Map<String, CardMetaData> allMeta = cardStorageDAO
                .findCardByReference(Collections.singletonList(cardReference));
        result.putAll(toMap(card, allMeta.get(cardReference)));
        return result;
    }

    private Map<String,String> saveCard( AddCardDetails addCardDetails, String customerId, CardMetaData cardMeta){
        FortKnoxClient fortKnoxClient = initForKnoxClient(customerId);
        boolean isCardSaveNameIsValid = PaymentUtil.isWelFormedName(addCardDetails.getNickname());
        if (!isCardSaveNameIsValid) {
            addCardDetails.setNickname("");
        }

        AddCardBean bean = new AddCardBean();
        bean.setCustomerId(customerId);
        // TODO: Do we need to send the emailId?
        bean.setCustomerEmail("");
        bean.setCardNumber(addCardDetails.getCardNumber());
        bean.setExpiryYear(addCardDetails.getCardExpiryYear());
        bean.setExpiryMonth(addCardDetails.getCardExpiryMonth());
        bean.setNameOnCard(addCardDetails.getNameOnCard());
        bean.setNickName(addCardDetails.getNickname());
        bean.setForceSave(true);

        final long startMillis = System.currentTimeMillis();
        AddCardResult addCardResult = externalClientCallService.executeWithTimeOut(
                FortknoxCommandBuilder.getAddCardCommand(fortKnoxClient, bean, addCardDetails.getChannel()));
        if(addCardResult == null){
            throw new RuntimeException("Error calling addCard for (userID=" + customerId + ")");
        }
        LOG(startMillis, "Add-card response (userID=" + customerId + "): " + addCardResult);
        if (cardMeta == null) {
            log.error("Add-card metadata not available for (userID=" + customerId + "), cardDetails="
                    + addCardDetails.toString());
        } else {
            final long beforeSave = System.currentTimeMillis();
            try {
                cardMeta.setCardReference(addCardResult.getCardReference());
                cardStorageDAO.saveCardMeta(cardMeta);
            } catch (RuntimeException e) {
                LOG(beforeSave, "Error saving card metadata (userID=" + customerId + "): " + cardMeta.toString(), e);
            }
        }
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.CARD_REFERENCE, addCardResult.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, addCardResult.getCardToken());
        return result;
    }
}
