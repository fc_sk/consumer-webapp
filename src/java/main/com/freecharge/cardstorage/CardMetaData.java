package com.freecharge.cardstorage;

import org.apache.commons.validator.routines.CreditCardValidator;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;

public class CardMetaData extends AbstractDO {

	private int cardMetaID;
	private String cardReference;
	private String cardType;
	private String cardBrand;
	private String cardIssuer;
	private String cardBin;
	private boolean nicknameEmpty;

	public static CardMetaData create(String nickname, String cardNumber, String type, String brand, String issuer) {
		CardMetaData meta = new CardMetaData();
		meta.setCardType(type);
		meta.setCardBrand(brand);
		meta.setCardIssuer(issuer);
		meta.setCardBin(FCUtil.left(cardNumber, 6));
		meta.setNicknameEmpty(FCUtil.isEmpty(nickname));
		return meta;
	}

	public static CardMetaData createUnknown(String nickname, String cardNumber, String type, String brand) {
		return create(nickname, cardNumber, type, brand, "UNKNOWN");
	}
	
	/**
	 * This method, on a best effort basis, guesses the card brand (VISA/Master etc.,) and the issuer.
	 * @param nickName
	 * @param cardNumber
	 * @return
	 */
    public static CardMetaData inferBrandAndCreateDebitCardMeta(String nickName, String cardNumber, String expiryYear,
            String expiryMonth) {

        if(CreditCardValidator.VISA_VALIDATOR.isValid(cardNumber)) {
            return createUnknown(nickName, cardNumber, "DEBIT", "VISA");
        }
	     
        if(CreditCardValidator.MASTERCARD_VALIDATOR.isValid(cardNumber)) {
            return CardMetaData.createUnknown(nickName, cardNumber, "DEBIT", "MASTERCARD");
        }
        /*
         * IF expiry year is *not* 2049 then it means user has entered some expiry year
         * best guess now is that it's a Non-SBI Maestro 
         */
        return CardMetaData.create(nickName, cardNumber, "DEBIT", "MAESTRO", "Non-SBI");
	}

	
	public int getCardMetaID() {
		return cardMetaID;
	}
	public void setCardMetaID(int cardMetaID) {
		this.cardMetaID = cardMetaID;
	}
	public String getCardReference() {
		return cardReference;
	}
	public void setCardReference(String cardReference) {
		this.cardReference = cardReference;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardBrand() {
		return cardBrand;
	}
	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}
	public String getCardIssuer() {
		return cardIssuer;
	}
	public void setCardIssuer(String cardIssuer) {
		this.cardIssuer = cardIssuer;
	}
	public String getCardBin() {
		return cardBin;
	}
	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}
	public boolean isNicknameEmpty() {
		return nicknameEmpty;
	}
	public void setNicknameEmpty(boolean nicknameEmpty) {
		this.nicknameEmpty = nicknameEmpty;
	}

}
