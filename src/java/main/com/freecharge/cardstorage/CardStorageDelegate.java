package com.freecharge.cardstorage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;

@Service
@EnableAsync
public class CardStorageDelegate {
    private final Logger       logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private CardStorageService cardStorageService;

    @Async
    public void addCard(PaymentRequestCardDetails paymentRequestCardDetails, Integer userId, String channel, String pyop) {
        logger.info("Starting add card asyncronously for user id : " + userId);
        cardStorageService.addCard(paymentRequestCardDetails, userId, channel, pyop);
        logger.info("Completed add card asyncronously for user id : " + userId);
    }
}
