package com.freecharge.cardstorage;

import in.juspay.Environment;
import in.juspay.PaymentService;
import in.juspay.request.AddCardRequest;
import in.juspay.request.DeleteCardRequest;
import in.juspay.request.GetCardRequest;
import in.juspay.request.ListCardsRequest;
import in.juspay.response.AddCardResponse;
import in.juspay.response.DeleteCardResponse;
import in.juspay.response.GetCardResponse;
import in.juspay.response.ListCardsResponse;
import in.juspay.response.StoredCard;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.CloudWatchMetricService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.OneCheckWalletService;

@Service
@Qualifier("juspay-card-storage")
public class JuspayCardStorageService extends AbstractCardStorageService {

    private final Logger   log = LoggingFactory.getLogger(getClass());

    @Autowired
    private CardStorageDAO cardStorageDAO;

    @Autowired
    private MetricsClient  metricsClient;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private CloudWatchMetricService cwService;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    public JuspayCardStorageService() {
        // this property is required because JusPay is hosted on AWS behind ELB
        // where the resolved IP addresses are ephemeral
        java.security.Security.setProperty("networkaddress.cache.ttl", "60");
    }

    private void LOG(long startMillis, String message) {
        log.info("[" + (System.currentTimeMillis() - startMillis) + "ms] " + message);
    }

    private void LOG(long startMillis, String message, Throwable e) {
        log.error("[" + (System.currentTimeMillis() - startMillis) + "ms] " + message, e);
    }

    private PaymentService makeJuspayService(String oneCheckUserId) {
        PaymentService paymentService = null;
        if (oneCheckUserId != null) {
            String id = fcProps.getProperty(FCConstants.ONE_CHECK_JUSPAY_CARDSTORAGE_ID);
            String key = fcProps.getProperty(FCConstants.ONE_CHECK_JUSPAY_CARDSTORAGE_KEY);
            String timeOut = fcProps.getProperty(FCConstants.JUSPAY_CARDSTORAGE_CALL_TIMEOUT);
            paymentService = new PaymentService().withEnvironment(Environment.PRODUCTION).withKey(key).withMerchantId(id);
            paymentService.setConnectionTimeout(Integer.parseInt(timeOut));
            paymentService.setReadTimeout(Integer.parseInt(timeOut));
        } else {
            String id = fcProps.getProperty(FCConstants.JUSPAY_CARDSTORAGE_ID);
            String key = fcProps.getProperty(FCConstants.JUSPAY_CARDSTORAGE_KEY);
            String timeOut = fcProps.getProperty(FCConstants.JUSPAY_CARDSTORAGE_CALL_TIMEOUT);
            paymentService = new PaymentService().withEnvironment(Environment.PRODUCTION).withKey(key).withMerchantId(id);
            paymentService.setConnectionTimeout(Integer.parseInt(timeOut));
            paymentService.setReadTimeout(Integer.parseInt(timeOut));
        }
        return paymentService;
    }

    private ArrayList<StoredCard> removeExpiredCards(ArrayList<StoredCard> cards) {

        ArrayList<StoredCard> cardsList = new ArrayList<StoredCard>();
        if (cards == null) {
            return null;
        }
        for (StoredCard each : cards) {
            if (!cardHasExpired(each.getCardExpMonth(), each.getCardExpYear())) {
                cardsList.add(each);
            }
        }
        return cardsList;
    }

    private boolean cardHasExpired(String cardExpMonth, String cardExpYear) {
        try {
            int year = Integer.parseInt(cardExpYear);
            int month = Integer.parseInt(cardExpMonth);
            if (year > DateTime.now().year().get()) {
                return false;
            }
            // Allow the current month
            if (year == DateTime.now().year().get() && month >= DateTime.now().monthOfYear().get()) {
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            log.error("Invalid expiry", e);
        }
        return false;
    }

    @Override
    public Map<String, Object> list(int userID) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        List<Map<String, String>> cards = new ArrayList<Map<String, String>>();
    	if(!appConfigService.isCardStorageEnabled()){
    	    result.put("cards", cards);
    		return result;
    	}
        String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        PaymentService juspayService =  makeJuspayService(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        ListCardsRequest listCardsRequest = new ListCardsRequest().withCustomerId("" + customerId);
        final long startMillis = System.currentTimeMillis();
        ListCardsResponse listCardsResponse = null;
        try {
            listCardsResponse = juspayService.listCards(listCardsRequest);
            metricSuccess(startMillis, "cardList");
            LOG(startMillis, "List-cards response (userID=" + userID + "): " + listCardsResponse);
        } catch (RuntimeException e) {
            metricFailure(startMillis, "cardList");
            LOG(startMillis, "Error in List-cards response (userID=" + userID + "): " + listCardsResponse, e);
            throw e;
        }
        ArrayList<StoredCard> cardsList = removeExpiredCards(listCardsResponse.cards);
        listCardsResponse.setCards(cardsList);

        List<String> cardReferences = new ArrayList<String>();
        for (StoredCard each : listCardsResponse.cards) {
            cardReferences.add(each.getCardReference());
        }
        Map<String, CardMetaData> allMeta = cardStorageDAO.findCardByReference(cardReferences);
        for (StoredCard each : listCardsResponse.cards) {
            boolean isCardSaveNameIsValid = PaymentUtil.isWelFormedName(each.getNickname());
            if (!isCardSaveNameIsValid) {
                each.setNickname("");
            }
            final Map<String, String> cardDetailsMap = listCardsToMap(each, allMeta.get(each.getCardReference()));
            
            // Removing the fields 'card_expiry_month' and 'card_expiry_year'
            // from card list call.
            cardDetailsMap.remove(CardStorageKey.CARD_EXPIRY_MONTH);
            cardDetailsMap.remove(CardStorageKey.CARD_EXPIRY_YEAR);
            if (isWellFormedCard(cardDetailsMap)) {
                cards.add(cardDetailsMap);
            } else {
                log.error("Found un-wellformed card; not returning");
            }
        }
        result.put("cards", cards);
        return result;
    }

    /**
     * Validates whether the card that we got back from JusPay is goot to use,
     * i.e., the number is not blank, type is not blank etc.,
     * 
     * @return
     */
    private Boolean isWellFormedCard(Map<String, String> cardDetailsMap) {
        if (cardDetailsMap.isEmpty()) {
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_REFERENCE)) {
            log.error("No card reference");
            return false;
        }

        final String cardReference = cardDetailsMap.get(CardStorageKey.CARD_REFERENCE);

        if (StringUtils.isBlank(cardReference)) {
            log.error("Card reference is empty!");
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_NUMBER)) {
            log.error("No card number for reference : [" + cardReference + "]");
            return false;
        }

        if (StringUtils.isBlank(cardDetailsMap.get(CardStorageKey.CARD_NUMBER))) {
            log.error("Blank card number for reference : [" + cardReference + "]");
            return false;
        }

        if (!cardDetailsMap.containsKey(CardStorageKey.CARD_TYPE)) {
            log.error("No card type for reference : [" + cardReference + "]");
            return false;
        }

        if (StringUtils.isBlank(cardDetailsMap.get(CardStorageKey.CARD_TYPE))) {
            log.error("Empty card type for reference : [" + cardReference + "]");
            return false;
        }

        return true;
    }

    public Map<String, String> toMap(StoredCard card, CardMetaData cardMeta) {
        // fix guessed values if incomplete
        String expiryMonth = card.getCardExpMonth();
        String expiryYear = card.getCardExpYear();
        // use Juspay's inferred data only if unavailable in Freecharge database
        String cardType = cardMeta != null ? cardMeta.getCardType() : card.getCardType();
        String cardBrand = cardMeta != null ? cardMeta.getCardBrand() : card.getCardBrand();
        String cardIssuer = cardMeta != null ? cardMeta.getCardIssuer() : card.getCardIssuer();
        // prepare result
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.NICK_NAME, card.getNickname());
        result.put(CardStorageKey.CARD_REFERENCE, card.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, card.getCardToken());
        result.put(CardStorageKey.CARD_EXPIRY_MONTH, expiryMonth);
        result.put(CardStorageKey.CARD_EXPIRY_YEAR, expiryYear);
        result.put(CardStorageKey.CARD_IS_IN, card.getCardIsin());
        result.put(CardStorageKey.CARD_NUMBER, card.getCardNumber());
        result.put(CardStorageKey.CARD_TYPE, cardType);
        result.put(CardStorageKey.CARD_ISSUER, cardIssuer);
        result.put(CardStorageKey.CARD_BRAND, cardBrand);
        result.put(CardStorageKey.NAME_ON_CARD, card.getNameOnCard());
        return result;
    }

    public Map<String, String> listCardsToMap(StoredCard card, CardMetaData cardMeta) {
        // use Juspay's inferred data only if unavailable in Freecharge database
        String cardType = cardMeta != null ? cardMeta.getCardType() : card.getCardType();
        String cardBrand = cardMeta != null ? cardMeta.getCardBrand() : card.getCardBrand();
        String cardIssuer = cardMeta != null ? cardMeta.getCardIssuer() : card.getCardIssuer();
        // prepare result
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.NICK_NAME, card.getNickname());
        result.put(CardStorageKey.CARD_REFERENCE, card.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, card.getCardToken());
        result.put(CardStorageKey.CARD_IS_IN, card.getCardIsin());
        result.put(CardStorageKey.CARD_NUMBER, card.getCardNumber());
        result.put(CardStorageKey.CARD_TYPE, cardType);
        result.put(CardStorageKey.CARD_ISSUER, cardIssuer);
        result.put(CardStorageKey.CARD_BRAND, cardBrand);
        return result;
    }

    @Override
    public Map<String, String> add(int userID, AddCardDetails addCardDetails, CardMetaData cardMeta) {
    	if(!appConfigService.isCardStorageEnabled()){
    		return new HashMap<String, String>();
    	}
    	String customerId = null;
        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        PaymentService juspayService =  makeJuspayService(oneCheckUserId);
        if (oneCheckUserId != null && appConfigService.isOneCheckFKEnabled()) {
            customerId = oneCheckUserId;
        } else {
            customerId = getCustomerID(userID);
        }
        boolean isCardSaveNameIsValid = PaymentUtil.isWelFormedName(addCardDetails.getNickname());
        if (!isCardSaveNameIsValid) {
            addCardDetails.setNickname("");
        }
        AddCardRequest addCardRequest = new AddCardRequest().withNickname(addCardDetails.getNickname())
                .withNameOnCard(addCardDetails.getNameOnCard())
                .withCardExpMonth("" + addCardDetails.getCardExpiryMonth())
                .withCardExpYear("" + addCardDetails.getCardExpiryYear())
                .withCardNumber("" + addCardDetails.getCardNumber()).withCustomerEmail("") // we
                                                                                           // do
                                                                                           // not
                                                                                           // share
                                                                                           // user's
                                                                                           // email
                                                                                           // address
                                                                                           // w/anybody
                .withCustomerId("" + customerId);
        final long startMillis = System.currentTimeMillis();
        AddCardResponse addCardResponse = null;
        try {
            addCardResponse = juspayService.addCard(addCardRequest);
            metricSuccess(startMillis, "cardAdd."+addCardDetails.getChannel());
            LOG(startMillis, "Add-card response (userID=" + userID + "): " + addCardResponse);
            if (cardMeta == null) {
                log.error("Add-card metadata not available for (userID=" + userID + "), cardDetails="
                        + addCardDetails.toString());
            } else {
                final long beforeSave = System.currentTimeMillis();
                try {
                    cardMeta.setCardReference(addCardResponse.getCardReference());
                    cardStorageDAO.saveCardMeta(cardMeta);
                } catch (RuntimeException e) {
                    LOG(beforeSave, "Error saving card metadata (userID=" + userID + "): " + cardMeta.toString(), e);
                }
            }
        } catch (RuntimeException e) {
            metricFailure(startMillis, "cardAdd");
            LOG(startMillis, "Error in Add-card response (userID=" + userID + "): " + addCardResponse, e);
            throw e;
        }
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.CARD_REFERENCE, addCardResponse.getCardReference());
        result.put(CardStorageKey.CARD_TOKEN, addCardResponse.getCardToken());
        return result;
    }

    @Override
    public Map<String, String> add(String userID, AddCardDetails addCardDetails, CardMetaData cardMeta){
        Map<String,String> result=new HashMap<>();
        return result;
    }

    @Override
    public Map<String, Object> delete(int userID, String cardToken) {
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        if (!isCardOwnedByUser(userID, cardToken)) {
            result.put("deleted", false);

            metricsClient.recordEvent("Payment.JusPay", "cardDelete", "IllegalAccess");

            return result;
        }

        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        PaymentService juspayService =  makeJuspayService(oneCheckUserId);
        DeleteCardRequest deleteCardRequest = new DeleteCardRequest().withCardToken(cardToken);
        final long startMillis = System.currentTimeMillis();
        DeleteCardResponse deleteCardResponse = null;
        try {
            log.info("Deleting card with cardToken=" + cardToken + " userID=" + userID);
            deleteCardResponse = juspayService.deleteCard(deleteCardRequest);
            metricSuccess(startMillis, "cardDelete");
            LOG(startMillis, "Delete-card response (userID=" + userID + " , cardToken=" + cardToken + "): "
                    + deleteCardResponse);
        } catch (RuntimeException e) {
            metricFailure(startMillis, "cardDelete");
            LOG(startMillis, "Error in Delete-card response (userID=" + userID + ", cardToken=" + cardToken + "): "
                    + deleteCardResponse, e);
            throw e;
        }
        result.put(CardStorageKey.CARD_TOKEN, deleteCardResponse.cardToken);
        result.put("deleted", deleteCardResponse.deleted);
        result.put(CardStorageKey.CARD_REFERENCE, deleteCardResponse.cardReference);
        return result;
    }

    private boolean isCardOwnedByUser(int userId, String cardToken) {
        final String CARD_KEY = "cards";
        final String CARD_TOKEN = "card_token";

        if (StringUtils.isBlank(cardToken)) {
            log.error("Empty or null cardToken passed for userId: " + String.valueOf(userId));
            return false;
        }

        try {
            Map<String, Object> listCardResponse = list(userId);

            if (listCardResponse.isEmpty() || !listCardResponse.containsKey(CARD_KEY)) {
                return false;
            }

            List<Map<String, String>> cardList = (List<Map<String, String>>) listCardResponse.get(CARD_KEY);

            if (cardList == null || cardList.isEmpty()) {
                return false;
            }

            for (Map<String, String> cardMeta : cardList) {
                String cardMetaToken = cardMeta.get(CARD_TOKEN);
                if (StringUtils.isNotBlank(cardMetaToken)) {
                    if (StringUtils.equals(cardToken, cardMetaToken)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception while validating card ownership : ", e);
            metricsClient.recordEvent("Payment.JusPay", "ValidateOwnership", "Exception");

            if (e.getCause() != null && e.getCause() instanceof SocketTimeoutException) {
                metricsClient.recordEvent("Payment.JusPay", "ValidateOwnership", "SocketTimeoutException");
                return true;
            }
        }

        return false;
    }

    @Override
    public Map<String, String> get(int userID, String cardToken, String orderID) {

        if (!isCardOwnedByUser(userID, cardToken)) {
            metricsClient.recordEvent("Payment.JusPay", "cardGet", "IllegalAccess");
            IllegalAccessException iae = new IllegalAccessException("UserID: [" + userID
                    + "] does not own cardToken: [" + cardToken + "]");

            throw new RuntimeException(iae);
        }

        String email = userServiceProxy.getEmailByUserId(userID);
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        PaymentService juspayService =  makeJuspayService(oneCheckUserId);
        GetCardRequest getCardRequest = new GetCardRequest().withCardToken(cardToken);
        final long startMillis = System.currentTimeMillis();
        GetCardResponse getCardResponse = null;
        try {
            log.info("Requesting card with cardToken=" + cardToken + " userID=" + userID + " orderID=" + orderID);
            getCardResponse = juspayService.getCard(getCardRequest);
            metricSuccess(startMillis, "cardGet");
        } catch (RuntimeException e) {
            metricFailure(startMillis, "cardGet");
            LOG(startMillis, "Error in Get-card response (userID=" + userID + ", cardToken=" + cardToken + ", orderID="
                    + orderID + "): " + getCardResponse, e);
            throw e;
        }

        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put(CardStorageKey.CARD_TOKEN, getCardResponse.getCardToken());
        StoredCard scard = getCardResponse.getCard();
        String cardReference = scard.getCardReference();
        Map<String, CardMetaData> allMeta = cardStorageDAO
                .findCardByReference(Collections.singletonList(cardReference));
        result.putAll(toMap(scard, allMeta.get(cardReference)));
        return result;
    }

    private void metricFailure(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Exception");
    }

    private void metricSuccess(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String methodName, final String eventName) {
        String serviceName = "Payment.JusPay";
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;
        
        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
        cwService.recordLatency(serviceName, finalMethodName, latency);
    }
}
