package com.freecharge.tracker;

import org.json.simple.JSONObject;

/**
 * User: abhi
 * Date: 5/9/13
 * Time: 6:38 PM
 */
public class TrackerEventVo {
    private String eventName;
    private String val0;
    private String val1;
    private String val2;
    private String val3;
    private String val4;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getVal0() {
        return val0;
    }

    public void setVal0(String val0) {
        this.val0 = val0;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public String getVal3() {
        return val3;
    }

    public void setVal3(String val3) {
        this.val3 = val3;
    }

    public String getVal4() {
        return val4;
    }

    public void setVal4(String val4) {
        this.val4 = val4;
    }

    public String asJsonString(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("eventName", eventName);
        jsonObject.put("val0", val0);
        jsonObject.put("val1", val1);
        jsonObject.put("val2", val2);
        jsonObject.put("val3", val3);
        jsonObject.put("val4", val4);
        return jsonObject.toJSONString();
    }
}
