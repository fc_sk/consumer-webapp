package com.freecharge.tracker;

/**
 * Created by sateesh on 11/9/14.
 */
public class TrackerContextDictionary {

    private static ContextThreadLocal<TrackerContext> local = new ContextThreadLocal<TrackerContext>();

    public static TrackerContext get() {
        return local.get();
    }

    public static void set(TrackerContext context) {
        local.set(context);
    }

    public static void invalidate() {
        local.remove();
        local.set(null);
    }

}
