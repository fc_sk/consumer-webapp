package com.freecharge.tracker;

import java.io.Serializable;

/**
 * Created by sateesh on 11/9/14.
 */
public class TrackerContext implements Serializable {
    private static final long serialVersionUID = -2020288076468657375L;

    private String visitId;

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }
}
