package com.freecharge.tracker;

/**
 * Created by sateesh on 11/9/14.
 */
public class ContextThreadLocal<T> extends ThreadLocal<T> {

    private T context;

    public T getContext() {
        return context;
    }

    public void setContext(T context) {
        this.context = context;
    }
}
