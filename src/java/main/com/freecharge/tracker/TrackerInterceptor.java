package com.freecharge.tracker;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.util.CookieUtil;
import com.freecharge.web.interceptor.FCBaseInterceptor;
import com.google.common.base.Strings;

/**
 * User: abhi
 * Date: 19/9/13
 * Time: 6:21 PM
 */
public class TrackerInterceptor extends FCBaseInterceptor {
    private Logger logger = LoggingFactory.getLogger(TrackerInterceptor.class);

    @Autowired
    private FCProperties fcproperties;

    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, Object handler) throws Exception {
        try {
            // initializing the tracker context
            TrackerContext trackerContext = new TrackerContext();
            TrackerContextDictionary.set(trackerContext);
            // setting the visit id
            setVisitId(request);
        } catch (RuntimeException e) {
            logger.error("Exception while handling tracking interceptor call", e);
        }
        return true;
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // invalidating the tracker context
        TrackerContextDictionary.invalidate();
    }


    private void setVisitId(HttpServletRequest request) {
        try {
            Cookie cookie = CookieUtil.getCookie(request, fcproperties.getVisitTrackingCookieName());
            if (cookie != null) {
                String visitId = Strings.nullToEmpty(cookie.getValue());
                TrackerContextDictionary.get().setVisitId(visitId);
            }
        } catch (Exception e) {
            logger.error("Exception while trying to set visit id in request interceptor", e);
        }
    }

}
