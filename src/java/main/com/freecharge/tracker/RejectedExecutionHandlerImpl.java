package com.freecharge.tracker;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import com.freecharge.commons.platform.metrics.MetricsLogger;
import com.freecharge.tracker.client.util.Constants;

/**
 * Created by sateesh on 18/7/14.
 */
public class RejectedExecutionHandlerImpl implements RejectedExecutionHandler {

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        MetricsLogger.recordCount(Constants.METRIC_TRACKER_SERVICE, Constants.METRIC_THREAD_REJECTION, 1);
    }
}
