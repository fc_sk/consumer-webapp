package com.freecharge.tracker;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.tracker.client.api.TrackerService;
import com.freecharge.tracker.client.payload.CampaignTracker;
import com.freecharge.tracker.client.payload.ClientDetails;
import com.freecharge.tracker.client.payload.ClientTracker;
import com.freecharge.tracker.client.payload.VisitEvent;
import com.freecharge.tracker.client.payload.VisitLookUp;
import com.freecharge.tracker.client.payload.VisitOrder;
import com.freecharge.tracker.client.payload.VisitTracker;
import com.freecharge.tracker.client.payload.VisitUser;
import com.google.common.base.Strings;

/**
 * User: abhi
 * Date: 20/8/13
 * Time: 1:41 PM
 * Helper class for tracking.
 */
@Component
public class Tracker {
    private Logger logger = LoggerFactory.getLogger(Tracker.class);

    public static final String LOGGER_PREFIX = "Tracking error";
    private static final int EVENT_NAME_SIZE_LIMIT = 20;
    @Autowired
    private TrackerService trackingService;
    @Autowired
    private AppConfigService appConfigService;
    @Autowired
    private FCProperties fcProperties;
    private ThreadPoolExecutor poolExecutor;
    @Value("${tracker.threadPool.queueSize}")
    private Integer poolQueueSize;
    @Value("${tracker.threadPool.thread.minCount}")
    private Integer minThreadCount;
    @Value("${tracker.threadPool.thread.maxCount}")
    private Integer maxThreadCount;
    @Value("${tracker.threadPool.thread.alive}")
    private Integer threadAliveTime;
    private boolean isTrackerEnabled;

    @PostConstruct
    private void init() {
        RejectedExecutionHandlerImpl rejectionHandler = new RejectedExecutionHandlerImpl();
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        poolExecutor = new ThreadPoolExecutor(minThreadCount, maxThreadCount, threadAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(poolQueueSize), threadFactory, rejectionHandler);
        trackingService.setThreadPoolExecutor(poolExecutor);
        isTrackerEnabled = (fcProperties.isProdMode()) ? true : false;
    }

    public void trackEvent(TrackerEventVo trackerEventVo) {
        try {

            if (isValidEvent(trackerEventVo)) {
                VisitEvent visitEvent = new VisitEvent();
                visitEvent.setCreatedEpoch(System.currentTimeMillis());
                visitEvent.setEventName(trackerEventVo.getEventName());
                visitEvent.setVal0(trackerEventVo.getVal0());
                visitEvent.setVal1(trackerEventVo.getVal1());
                visitEvent.setVal2(trackerEventVo.getVal2());
                visitEvent.setVal3(trackerEventVo.getVal3());
                visitEvent.setVal4(trackerEventVo.getVal4());

                final String visitId = Strings.nullToEmpty(TrackerContextDictionary.get().getVisitId());

                if ("".equals(visitId)) {
                    logger.error(LOGGER_PREFIX +  " : Empty visit id while trying to track event " + trackerEventVo.getEventName());
                } else {

                    visitEvent.setVisitTrackerId(visitId);

                    logTrackingEvent(visitEvent.toJson());

                    if (isTrackerEnabled) {
                        if (appConfigService.isTrackingSqsEnabled()) {
                            trackingService.trackEvent(visitEvent);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while trying to track event - " + trackerEventVo.getEventName(), e);
        }
    }

    public void trackUser(int userId) {
        try {
            VisitUser visitUser = new VisitUser();
            visitUser.setCreatedEpoch(System.currentTimeMillis());
            visitUser.setUserId(userId);

            final String visitId = Strings.nullToEmpty(TrackerContextDictionary.get().getVisitId());

            if ("".equals(visitId)) {
                logger.error(LOGGER_PREFIX + " : Empty visit id while trying to track user - " + userId);
            } else {

                visitUser.setVisitTrackerId(visitId);

                logTrackingEvent(visitUser.toJson());

                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackUser(visitUser);
                    }
                }

            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while trying to track user - " + userId, e);
        }
    }

    public void trackOrderId(String orderId) {
        try {
            VisitOrder visitOrder = new VisitOrder();
            visitOrder.setOrderId(orderId);
            visitOrder.setCreatedEpoch(System.currentTimeMillis());

            final String visitId = Strings.nullToEmpty(TrackerContextDictionary.get().getVisitId());

            if ("".equals(visitId)) {
                logger.error(LOGGER_PREFIX + " : Empty visit id while trying to track order - " + orderId);
            } else {
                visitOrder.setVisitTrackerId(visitId);

                logTrackingEvent(visitOrder.toJson());

                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        logger.info("Tracking order id - " + orderId);
                        trackingService.trackOrder(visitOrder);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track order - " + orderId, e);
        }
    }

    public void trackLookupId(String lookupId) {
        try {
            VisitLookUp visitLookUp = new VisitLookUp();
            visitLookUp.setLookUpId(lookupId);
            visitLookUp.setCreatedEpoch(System.currentTimeMillis());

            final String visitId = Strings.nullToEmpty(TrackerContextDictionary.get().getVisitId());

            if ("".equals(visitId)) {
                logger.error(LOGGER_PREFIX + " : Empty visit id while trying to track lookup - " + lookupId);
            } else {
                visitLookUp.setVisitTrackerId(visitId);

                logTrackingEvent(visitLookUp.toJson());

                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackVisitLookUp(visitLookUp);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track lookup - " + lookupId, e);
        }
    }

    public void trackClientDetails(ClientDetails clientDetails) {
        try {
            if ("".equals(clientDetails.getClientTrackerId()) || "".equals(clientDetails.getVisitTrackerId())) {
                if ("".equals(clientDetails.getClientTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty client id while trying to track client details - " + clientDetails);
                }

                if ("".equals(clientDetails.getVisitTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty visit id while trying to track client details - " + clientDetails);
                }
            } else {
                logTrackingEvent(clientDetails.toJson());
                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackClientDetails(clientDetails);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track client details - " + clientDetails, e);
        }
    }

    public void trackCampaign(CampaignTracker campaignTracker) {
        try {
            if ("".equals(campaignTracker.getCampaignTrackerId()) || "".equals(campaignTracker.getClientTrackerId())) {
                if ("".equals(campaignTracker.getCampaignTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty campaign id while trying to track campaign details - " + campaignTracker);
                }

                if ("".equals(campaignTracker.getClientTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty client id while trying to track campaign details - " + campaignTracker);
                }
            } else {
                logTrackingEvent(campaignTracker.toJson());
                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackCampaign(campaignTracker);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track campaign details - " + campaignTracker, e);
        }
    }

    public void trackClient(ClientTracker clientTracker) {
        try {
            if ("".equals(clientTracker.getTrackerId())) {
                if ("".equals(clientTracker.getTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty client id while trying to track client tracker - " + clientTracker);
                }

            } else {
                logTrackingEvent(clientTracker.toJson());
                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackClient(clientTracker);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track client tracker - " + clientTracker, e);
        }
    }

    public void trackVisit(VisitTracker visitTracker) {
        try {
            if ("".equals(visitTracker.getClientTrackerId()) || "".equals(visitTracker.getTrackerId())) {
                if ("".equals(visitTracker.getTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty visit id while trying to track visit details - " + visitTracker);
                }

                if ("".equals(visitTracker.getClientTrackerId())) {
                    logger.error(LOGGER_PREFIX + " : Empty client id while trying to track visit details - " + visitTracker);
                }
            } else {
                logTrackingEvent(visitTracker.toJson());
                if (isTrackerEnabled) {
                    if (appConfigService.isTrackingSqsEnabled()) {
                        trackingService.trackVisit(visitTracker);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(LOGGER_PREFIX + " : Error while tyring to track visit details - " + visitTracker, e);
        }
    }

    private boolean isValidEvent(TrackerEventVo trackerEventVo) {
        String eventName = Strings.nullToEmpty(trackerEventVo.getEventName()).trim();
        trackerEventVo.setEventName(eventName);

        if (eventName.equals("")) {
            logger.error(LOGGER_PREFIX + " - Empty tracking name");
            return false;
        }

        if (eventName.length() > EVENT_NAME_SIZE_LIMIT) {
            logger.error(LOGGER_PREFIX + " - Event name - " + eventName + String.format(" exceeds %d character size limit", EVENT_NAME_SIZE_LIMIT));
            return false;
        }

        return validateEventValues(trackerEventVo.getVal0())
                && validateEventValues(trackerEventVo.getVal1())
                && validateEventValues(trackerEventVo.getVal2())
                && validateEventValues(trackerEventVo.getVal3())
                && validateEventValues(trackerEventVo.getVal4());
    }

    private boolean validateEventValues(String value) {
        if (Strings.nullToEmpty(value).length() > EVENT_NAME_SIZE_LIMIT) {
            logger.error(LOGGER_PREFIX + " - Event value - " + value + String.format(" exceeds %d character size limit", EVENT_NAME_SIZE_LIMIT));
            return false;
        }

        return true;
    }

    private void logTrackingEvent(String trackingEvent) {
        if (fcProperties.shouldLogTrackingEvents()) {
            logger.info("(Tracking Event) - " + trackingEvent);
        }
    }

}
