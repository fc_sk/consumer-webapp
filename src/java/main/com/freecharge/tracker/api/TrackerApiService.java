package com.freecharge.tracker.api;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.tracker.TrackerEventVo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service("trackerApiService")
public class TrackerApiService {
    private Logger logger = LoggingFactory.getLogger(TrackerApiService.class);
    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private AsyncTrackerApi trackerApi;

    @Autowired
    private FCProperties fcProperties;

    public void trackEvent(final TrackerEventVo eventVo, final String visitId){
        if (FCStringUtils.isBlank(visitId)){
            logger.warn(String.format("[TRACKER_API] empty visit id for event: %s", eventVo.getEventName()));
            return;
        }
        try {
            long callStartTime = System.currentTimeMillis();
            this.trackerApi.track(visitId, eventVo).get((long)fcProperties.trackerApiRequestExecutionTime(),
                    TimeUnit.MILLISECONDS);
            metricsClient.recordLatency("trackerApi", "trackTime", System.currentTimeMillis() - callStartTime);
        }catch (InterruptedException ie){
            logger.error("[TRACKER_API] InterruptedException", ie);
        }catch (ExecutionException ee){
            logger.error("[TRACKER_API] ExecutionException", ee);
        }catch (TimeoutException te){
            logger.error("[TRACKER_API] TimeoutException", te);
        }catch (Exception e){
            logger.error("[TRACKER_API] OtherException", e);
        }
    }
}
