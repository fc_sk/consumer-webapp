package com.freecharge.tracker.api;

import com.freecharge.tracker.TrackerEventVo;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;

public interface ITrackerApi {
    @POST("/visitevent")
    Object track(@Header("Cookie") String cookie, @Header("Content-Type") String contentType, @Body TrackerEventVo eventVo);
}
