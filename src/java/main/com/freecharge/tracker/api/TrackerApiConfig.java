package com.freecharge.tracker.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.properties.FCProperties;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.ApacheClient;
import retrofit.converter.JacksonConverter;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/5/15
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class TrackerApiConfig {
    @Autowired
    private FCProperties fcProperties;

    @Bean
    public ITrackerApi trackerApi() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        RestAdapter restAdapter = new RestAdapter.Builder().
                //setLogLevel(RestAdapter.LogLevel.FULL).
                setEndpoint(fcProperties.getTrackerApiUrl()).
                setClient(new ApacheClient(trackerHttpClient())).
                setConverter(new JacksonConverter(objectMapper)).build();

        return restAdapter.create(ITrackerApi.class);
    }

    private HttpClient trackerHttpClient(){
        PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager();
        connectionManager.setMaxTotal(fcProperties.trackerApiMaximumConnections());
        connectionManager.setDefaultMaxPerRoute(fcProperties.trackerApiMaximumConnectionsPerRoute());

        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, fcProperties.trackerApiConnectionTimeout());
        HttpConnectionParams.setSoTimeout(params, fcProperties.trackerApiSocketTimeout());
        params.setLongParameter(ClientPNames.CONN_MANAGER_TIMEOUT, fcProperties.trackerApiConnManagerTimeout());

        return new DefaultHttpClient(connectionManager, params);
    }
}
