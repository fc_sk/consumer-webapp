package com.freecharge.tracker.api;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.tracker.TrackerEventVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class AsyncTrackerApi {
    @Autowired
    private ITrackerApi trackerApi;

    @Autowired
    private FCProperties fcProperties;

    private ExecutorService executorService;

    @PostConstruct
    private void init() {
        executorService = Executors.newFixedThreadPool(fcProperties.trackerApiThreadPoolSize());
    }

    public Future<Object> track(final String visitId, final TrackerEventVo eventVo){
        Callable<Object> callable = new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return trackerApi.track("fc.tv="+visitId, "application/json", eventVo);
            }
        };
        return executorService.submit(callable);
    }

    @PreDestroy
    private void cleanUp() {
        //We do not care for clean shutdown as this is experimental.
        executorService.shutdownNow();
    }
}
