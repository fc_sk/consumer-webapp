package com.freecharge.tracker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by sateesh on 1/9/14.
 */
@Component
public class TrackerProp {

    @Value("${tracker.server.hostName}")
    private String hostName;
    @Value("${tracker.server.port}")
    private String port;
    @Value("${tracker.server.defaultApi}")
    private String defaultApi;
    @Value("${tracker.server.http.protocol}")
    private String httpProtocol;
    @Value("${tracker.server.visitApi}")
    private String visitApi;

    public String getHostName() {
        return hostName;
    }

    public String getPort() {
        return port;
    }

    public String getDefaultApi() {
        return defaultApi;
    }

    public String getHttpProtocol() {
        return httpProtocol;
    }

    public String getVisitApi() {
        return visitApi;
    }
}
