package com.freecharge.tracker;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

/**
 * User: sateesh
 */
@Controller
public class TrackerUtil {
    private static Logger logger = LoggingFactory.getLogger(TrackerUtil.class);

    public static final String EVENT_NAME_PARAM = "eventName";
    public static final String EVENT_VAL0_PARAM = "val0";
    public static final String EVENT_VAL1_PARAM = "val1";
    public static final String EVENT_VAL2_PARAM = "val2";
    public static final String EVENT_VAL3_PARAM = "val3";
    public static final String EVENT_VAL4_PARAM = "val4";
    public static final String CAMPAIGN_PARAM_NAME = "utm_campaign";
    public static final String CLIENT_DETAILS_PARAM_USER_AGENT = "userAgent";
    public static final String CLIENT_DETAILS_PARAM_REFERRER = "referrer";
    public static final String CLIENT_DETAILS_PARAM_PARAMS = "params";
    private static String TRACKER_JS_ACTION;
    private static final String CHANNEL_ID_PARAM = "channelId";
    private static final String COLON = ":";
    private static final String DOUBLE_SLASH = "//";
    @Autowired
    private FCProperties fcProperties;
    @Autowired
    private TrackerProp trackerProp;

    @PostConstruct
    private void init() {
        if (fcProperties.isPreProdMode() || fcProperties.isProdMode()) {
            TRACKER_JS_ACTION = trackerProp.getHttpProtocol() + COLON + DOUBLE_SLASH + trackerProp.getHostName() + trackerProp.getDefaultApi() + "?v=1";
        } else {
            TRACKER_JS_ACTION = trackerProp.getHttpProtocol() + COLON + DOUBLE_SLASH + trackerProp.getHostName() + ":" + trackerProp.getPort() + trackerProp.getDefaultApi() + "?v=1";
        }
        logger.info("Tracker Url is " + TRACKER_JS_ACTION);
    }

    public static String getTrackerUrl(int channelId) {
        return TRACKER_JS_ACTION + "&" + CHANNEL_ID_PARAM + "=" + channelId;
    }

    public String getVisitApi() {
        String trackerVisitApi = null;
        if (fcProperties.isPreProdMode() || fcProperties.isProdMode()) {
            trackerVisitApi = trackerProp.getHttpProtocol() + COLON + DOUBLE_SLASH + trackerProp.getHostName() + trackerProp.getVisitApi();
        } else {
            trackerVisitApi = trackerProp.getHttpProtocol() + COLON + DOUBLE_SLASH + trackerProp.getHostName() + ":" + trackerProp.getPort() + trackerProp.getVisitApi();
        }
        return trackerVisitApi;
    }

}