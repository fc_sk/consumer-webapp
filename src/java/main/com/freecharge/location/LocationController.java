package com.freecharge.location;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.coupon.service.CouponDummyDataManager;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.tracker.TrackerEventVo;
import com.freecharge.tracker.api.TrackerApiService;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/11/14
 * Time: 3:42 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/app/location/*")
public class LocationController {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppCityService appCityService;

    @Autowired
    private TrackerApiService trackerApiService;

    @Autowired
    private AppConfigService appConfig;

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "setloc", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> setLocationNew(
                                            @RequestParam(defaultValue = "") String imsi,
                                            @RequestParam(defaultValue = "") String app_uid,
                                            @RequestParam(defaultValue = "") String device_id,
                                            @RequestParam(defaultValue = "") String advertise_id,
                                            @RequestParam(defaultValue = "") String imei,
                                            @RequestParam(defaultValue = "1") Integer fcversion,
                                            @RequestBody String latLong, HttpServletRequest request) {
    	boolean isHyperLocalEnabled = false;
    	Map<String, Object> data = new HashMap<>();
    	data.put("isHyperLocalEnabled", isHyperLocalEnabled);
    	
    	if (CouponDummyDataManager.isDummyCouponMode()){
        	return data;
    	}

    	try {

			Map<String, String> requestBodyData = getRequestBodyData(latLong);
			data = handleLocation("", imsi, app_uid, device_id, advertise_id, imei, requestBodyData.get("lat"),
                    requestBodyData.get("longi"), fcversion);
		} catch (Exception e) {
			logger.error("Error parsing setloc, " + e);
		}
    	return data;
    }
    
    private Map<String, String> getRequestBodyData(String latLong) {
    	Map<String, String> data = new HashMap<>();
    	if (latLong == null){
    		return data;
    	}
    	String[] requestList = latLong.split("&");
    	for (String request : requestList){
    		String[] keyValue = request.split("=");
    		if (keyValue.length > 0){
    			data.put(keyValue[0], keyValue[1]);    			
    		}
    	}
		return data;
	}

	@NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "set", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> setLocation(@RequestParam(defaultValue = "") String address,
                                            @RequestParam(defaultValue = "") String imsi,
                                            @RequestParam(defaultValue = "") String app_uid,
                                            @RequestParam(defaultValue = "") String device_id,
                                            @RequestParam(defaultValue = "") String advertise_id,
                                            @RequestParam(defaultValue = "1") Integer fcversion,
                                            @RequestParam(defaultValue = "") String imei, HttpServletRequest request) {
    	return handleLocation(address, imsi, app_uid, device_id, advertise_id,
				imei, request.getParameter("lat"), request.getParameter("long"), fcversion);
    }

	private Map<String, Object> handleLocation(String address, String imsi, String app_uid, String device_id,
                                               String advertise_id, String imei, String lat, String longi,
                                               int appVersion) {
		boolean isHyperLocalEnabled = false;
    	Map<String, Object> data = new HashMap<>();
    	data.put("isHyperLocalEnabled", isHyperLocalEnabled);
        //First log this info.
        logger.debug(String.format("Logging_Location: %s -- IMEI: %s -- IMSI: %s -- DeviceId: %s -- AdvertiserId: %s " +
                "--- UID: %s", address, imei, imsi, device_id, advertise_id, app_uid));

        //setting dummy visit id to track coupon experiment events
        if (FCStringUtils.isBlank(imei)){
            TrackerEventVo eventVo = new TrackerEventVo();
            eventVo.setEventName("app_no_imei");
            eventVo.setVal0(device_id);
            eventVo.setVal1(app_uid);
            this.trackerApiService.trackEvent(eventVo, FCConstants.DUMMY_VISIT_ID);
            return data;
        }

        if (!isLatLongValid(lat, longi)){
            TrackerEventVo eventVo = new TrackerEventVo();
            eventVo.setEventName("app_no_latlong");
            eventVo.setVal0(imei);
            eventVo.setVal1(app_uid);
            this.trackerApiService.trackEvent(eventVo, FCConstants.DUMMY_VISIT_ID);
        }

        logger.info(String.format("location_coordinates: Latitude: %s and longitude: %s", lat,
                longi));

        if (FCStringUtils.isBlank(address) || address.equalsIgnoreCase("null")){
            TrackerEventVo eventVo = new TrackerEventVo();
            eventVo.setEventName("app_no_addr");
            eventVo.setVal0(imei);
            eventVo.setVal1(app_uid);
            this.trackerApiService.trackEvent(eventVo, FCConstants.DUMMY_VISIT_ID);
        }

        if (appConfig.isLocationSavingEnabled()){
        	isHyperLocalEnabled = this.appCityService.saveLocation(imei, app_uid, imsi, device_id, advertise_id, address,
                    lat, longi);
        }
        data.put("isHyperLocalEnabled", isHyperLocalEnabled);

        if (!this.appConfig.isMarsEnabled()){
            data.put("isHyperLocalEnabled", false);
            return data;
        }

        //Check conditions to decide whether to show mars or not
        Integer conditionId = null;
        try {
            Map<String, Object> couponConfig = this.appConfig.getCouponsConfig();
            conditionId = Integer.parseInt((String) couponConfig.get("location.conditionId"));
        }catch (Exception e){
            logger.debug("no app version condition set for saving location. Ignoring.");
        }
        
        return data;
	}

    private boolean isLatLongValid(String lat, String longi){
        return !FCStringUtils.isBlank(lat)
                || FCStringUtils.isBlank(longi)
                || lat.equals("0.0")
                || longi.equals("0.0");

    }
}
