package com.freecharge.location;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.sns.LocationSNSService;
import com.freecharge.sns.bean.LocationBean;
import net.sf.jsi.Rectangle;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/9/15
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("appLocationService")
public class AppCityService {
    private final Logger logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private LocationCacheManager locationCacheManager;

    @Autowired
    private LocationSNSService locationSNSService;


    public boolean saveLocation(String imei, String uid, String imsi, String deviceId, String advertiserId,
                             String rawAddress, String latitude, String longitude){
        String city = null;
        JSONObject addressObject = null;
        if (!FCStringUtils.isBlank(rawAddress)){
            try {
                addressObject = FCUtil.getAddressFromAppLocation(rawAddress);
                logger.debug(String.format("address parsing successful for raw address: %s", rawAddress));
                city = (String) addressObject.get("city");
            }catch (Exception e){
                logger.error(String.format("app_address_parse_error: imei: %s and address: %s", imei, rawAddress), e);
            }
        }
        if (city == null){
            //Could not get city from address for some reason. Try from lat long
            try {
                city = this.getCityForLatLong(Float.parseFloat(latitude), Float.parseFloat(longitude));
            }catch (Exception e){
                logger.error(String.format("error_parsing_latlong %s, %s imei: %s", latitude, longitude, imei));
            }
        }
        if (!FCUtil.isValidCity(city)){
            //Returning as failed to get city from both address and lat long
            logger.info(String.format("Unable to get city from address and lat long for %s, %s and imei: %s",
                    latitude, longitude, imei));
            return false;
        }

        //Save city in dynamo for usage
        try {
            this.locationCacheManager.saveCityForImei(imei, city);
        }catch (Exception e){
            logger.error(String.format("unable_to_save_city %s for imei: %s", city, imei));
        }
        if (addressObject!=null){
            try {
                //Now push to FDN
                //Create LocationBean
                LocationBean locationBean = new LocationBean();
                locationBean.setCity((String) addressObject.get("city"));
                locationBean.setAddress((String) addressObject.get("address"));
                locationBean.setImei(imei);
                locationBean.setRawAddress(rawAddress);
                locationBean.setAddressParts((List<Map<String,Object>>) addressObject.get("address_parts"));
                locationBean.setUid(uid);
                locationBean.setDeviceId(deviceId);
                locationBean.setImsi(imsi);
                locationBean.setAdvertiserId(advertiserId);
                //now push
                this.locationSNSService.publish("{\"message\" : "+LocationBean.getJsonString(locationBean)+"}");
            }catch (Exception e){
                logger.error(String.format("error_publishing_location_to_fdn for imei: %s", imei), e);
            }
        }
        boolean isHyperLocalEnabled = false;
        //TODO: Move this to config in future
        if ("Mumbai".equalsIgnoreCase(city)){
        	isHyperLocalEnabled = true;
        }
        return isHyperLocalEnabled;
    }

    public String getCityForImei(String imei){
        try {
            return this.locationCacheManager.getCityForImei(imei);
        }catch (Exception e){
            logger.warn(String.format("Unable to get city for imei: %s", imei), e);
        }
        return null;
    }

    private String getCityForLatLong(float lat, float lon){
        Rectangle rect = new Rectangle(lat, lon, lat, lon);
        for (String city: cityRects.keySet()){
            if (rect.containedBy(cityRects.get(city))){
                return city;
            }
        }
        return null;
    }

    public static Map<String, Rectangle> cityRects = new HashMap<>();
    
    static {
        cityRects.put("bangalore", new Rectangle(12.8330f, 77.4703f, 13.1984f, 77.7439f)); //Bangalore
        cityRects.put("mumbai", new Rectangle(19.540505f, 73.241878f, 18.858336f, 72.696682f)); //Mumbai
        cityRects.put("pune", new Rectangle(18.611340f, 73.957169f, 18.447189f, 73.746918f)); //Pune
        cityRects.put("delhi", new Rectangle(28.883820f, 77.343689f, 28.404181f, 76.838310f)); //Delhi
        cityRects.put("chennai", new Rectangle(13.151480f, 80.306709f, 12.967300f, 80.184631f)); //Chennai
        cityRects.put("kolkata", new Rectangle(22.678860f, 88.449928f, 22.421021f, 88.188263f)); //Kolkata
        cityRects.put("hyderabad", new Rectangle(17.542480f, 78.609993f, 17.253830f, 78.280952f)); //Hyderabad
        cityRects.put("surat", new Rectangle(21.261681f, 72.906509f, 21.078480f, 72.701622f)); //Surat
        cityRects.put("jaipur", new Rectangle(27.007389f, 75.854759f, 26.806789f, 75.725433f)); //Jaipur
    }
}
