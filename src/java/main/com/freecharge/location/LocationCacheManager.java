package com.freecharge.location;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/9/15
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("locationCacheManager")
public class LocationCacheManager {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private static String TABLE_NAME = "appCity";
    private static String IMEI_KEY = "imei";
    private static String CITY_KEY = "city";

    @Autowired
    private AmazonDynamoDBAsync dynamoClient;

    @Autowired
    MetricsClient metricsClient;

    public String getCityForImei(String imei){
        long start = System.currentTimeMillis();
        GetItemResult result = this.dynamoClient.getItem(new GetItemRequest(TABLE_NAME,
                ImmutableMap.<String, AttributeValue>of(IMEI_KEY, new AttributeValue(imei))));
        long  end = System.currentTimeMillis();
        this.metricsClient.recordLatency("DynamoDB", "appCityGet", end-start);
        try {
            if (result!=null && result.getItem()!=null){
                return result.getItem().get("city").getS();
            }
        }catch (Exception e){
            logger.error("city_get_error_from_dynamodb for imei"+imei, e);
        }
        return null;
    }

    public void saveCityForImei(String imei, String city){
        long start = System.currentTimeMillis();
        Map<String, AttributeValue> itemData = new HashMap<>();
        itemData.put(IMEI_KEY, new AttributeValue(imei));
        itemData.put(CITY_KEY, new AttributeValue(city));
        this.dynamoClient.putItem(new PutItemRequest(TABLE_NAME, itemData));
        long  end = System.currentTimeMillis();
        this.metricsClient.recordLatency("DynamoDB", "appCityPut", end-start);
    }
}
