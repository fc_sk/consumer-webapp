package com.freecharge.refund.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.RefundResponse;

/**
 * 
 * Controller for handing refund requests directly from web
 * 
 */
@Controller
@RequestMapping("/rest/refund")
public class FCRefundController {

    private Logger            logger = LoggingFactory.getLogger(getClass());

    @Autowired
    PaymentTransactionService paymentTransactionService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    OrderService              orderService;

    @Autowired
    WalletService             walletService;

    /**
     * Handler method to handle refund requests.<br>
     * Method takes the order id for which refund is being claimed and performs
     * validations before processing refund. If all validations succeed, refund
     * is processed to the bank from which money was paid and the user is
     * intimated about the same
     * 
     * @param orderId
     * @param model
     * @return RefundResponse
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.POST)
    public @ResponseBody RefundResponse handleRefundRequest(@PathVariable String orderId, Model model) {
        logger.info("Starting refund processing for order : " + orderId);
        RefundResponse refundResponse = new RefundResponse();
        PaymentTransaction paymentTransaction = paymentTransactionService
                .getSuccessfulPaymentTransactionByOrderId(orderId);
        validateRefundRequest(paymentTransaction, refundResponse);
        if (refundResponse.getResponseCode() == null) {
            try {
                logger.info("Refunding Order : " + orderId);
                refundResponse = walletService.refundToPgForPaymentMxtnId(paymentTransaction,
                        paymentTransaction.getAmount(), "");
                if (refundResponse.isSuccess()) {
                    logger.info("Refund for order : " + orderId + " processed successfully");
                    refundResponse.setResponseCode(ErrorCode.REFUND_SUCCESS.getErrorNumber());
                    refundResponse.setSuccess(true);
                } else {
                    logger.info("Refund for order : " + orderId + " failed");
                    refundResponse.setResponseCode(ErrorCode.REFUND_FAILED.getErrorNumber());
                    refundResponse.setSuccess(false);
                }

            } catch (Exception exception) {
                logger.info("Exception refunding order  : " + orderId, exception);
                refundResponse.setSuccess(false);
                refundResponse.setResponseMessage("Refund failed. Please try later");
                refundResponse.setResponseCode(ErrorCode.REFUND_FAILED.getErrorNumber());
            }
        }
        return refundResponse;
    }

    /**
     * Method to validate refund request. Validations include <br>
     * 1. There exists a valid payment transaction for the given order<br>
     * 2. User claiming the refund should be the owner of the order<br>
     * 3. Payment must be received for the order<br>
     * 
     * In case of failure of any validation, refund response is updated with
     * "isSuccess" to failure and corresponding error code is set for response
     * code
     * 
     * @param paymentTransaction
     * @param refundResponse
     */
    private void validateRefundRequest(PaymentTransaction paymentTransaction, RefundResponse refundResponse) {
        logger.info("Validating refund request for valid payment transaction");
        if (paymentTransaction == null) {
            refundResponse.setResponseCode(ErrorCode.REFUND_FAILED.getErrorNumber());
            refundResponse.setResponseMessage("Invalid Order Id. No Payment Transaction found for given order");
            return;
        }

        Users user = userServiceProxy.getUserFromOrderId(paymentTransaction.getOrderId());
        if (user == null) {
            refundResponse.setResponseCode(ErrorCode.REFUND_FAILED_INVALID_USER.getErrorNumber());
            refundResponse.setResponseMessage("Invalid User. No user exists for given order");
            return;
        }

        Users loggedInUser = userServiceProxy.getLoggedInUser();
        if (loggedInUser == null) {
            refundResponse.setResponseCode(ErrorCode.REFUND_FAILED_INVALID_USER.getErrorNumber());
            refundResponse.setResponseMessage("Invalid User. User needs to be logged in for refund");
            return;
        }
        logger.info("Validating refund request for user authenticity");
        logger.info("User of order : " + user != null ? user.getUserId() : "");
        logger.info("Logged in user : " + loggedInUser != null ? loggedInUser.getUserId() : "");
        if (user != null && loggedInUser != null && !loggedInUser.getUserId().equals(user.getUserId())) {
            refundResponse.setResponseCode(ErrorCode.REFUND_FAILED_INVALID_USER.getErrorNumber());
            refundResponse.setResponseMessage("Invalid User. Given order does not belong to logged in user");
            return;
        }

        logger.info("Validating refund request to check if payment has been received");
        if (!paymentTransaction.getSuccessful()) {
            refundResponse.setResponseCode(ErrorCode.REFUND_FAILED_UNSUCCESSFUL_PAYMENT.getErrorNumber());
            refundResponse.setResponseMessage("Payment has not been received for the given order");
            return;
        }
    }
}
