package com.freecharge.customercare.service;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.platform.metrics.MetricsClient;

@Service
public class AdminMetricService {

    @Autowired
    private MetricsClient metricsClient;
    
    public void recordTypeOfException(long startMillis, final String className, String methodName, Exception exception) {
    	Throwable throwableException = ExceptionUtils.getRootCause(exception);
    	if (throwableException != null) {
    		metricFailure(startMillis, className, methodName, throwableException.getClass().getSimpleName());
    	} else {
    		metricFailure(startMillis, className, methodName, exception.getClass().getSimpleName());
    	}
    }
	
    public void metricFailure(final long startMillis, final String className, final String methodName, final String eventName) {
        recordMetric(startMillis, className, methodName, eventName);
    }

    public void metricSuccess(final long startMillis, final String className, final String methodName) {
        recordMetric(startMillis, className, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String className, final String methodName, final String eventName) {
        String serviceName = className;
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;
        
        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
    }
}
