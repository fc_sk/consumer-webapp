package com.freecharge.customercare.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.ConsumerCreditConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.rest.user.ConsumerCreditTxnSummary;
import com.freecharge.rest.user.DebitEMITxnSummary;
import com.freecharge.reward.util.StringUtil;
import com.snapdeal.fcpt.txnhistoryview.client.impl.TxnHistoryViewClientImpl;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetDebitTxnHistoryViewRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnHistoryViewResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.TxnHistory;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.payments.consumercredit.dto.ConsumerCreditUserProfile;
import com.snapdeal.payments.consumercredit.dto.TransactionInfoDTO;
import com.snapdeal.payments.consumercredit.dto.UserTransactionDetailsDTO;
import com.snapdeal.payments.consumercredit.enumsv2.CreditTransactionType;
import com.snapdeal.payments.consumercredit.enumsv2.EntityType;
import com.snapdeal.payments.consumercredit.impl.CreditServiceClient;
import com.snapdeal.payments.consumercredit.requestV2.GetProfileRequest;
import com.snapdeal.payments.consumercredit.requestV2.GetUserTransactionsRequest;
import com.snapdeal.payments.consumercredit.responseV2.GetProfileResponse;
import com.snapdeal.payments.consumercredit.responseV2.GetUserTransactionsResponse;
import com.snapdeal.payments.consumercredit.util.ClientDetails;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

public class ConsumerCreditService {
	
	private final Logger logger = LoggingFactory.getLogger(ConsumerCreditService.class);
	
	@Autowired
	private CreditServiceClient creditServiceClient;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	private TxnHistoryViewClientImpl txnHistoryViewClientImpl;
	
	private String clientId;
	private String clientKey;
	private String host;
	private String port;
	private int timeOut;
	
	public List<ConsumerCreditTxnSummary> getUserTransactions(final String emailId, final Date startDate, final Date endDate) {
		logger.info("Fetching user credit transaction details for emailId " + emailId);
		List<ConsumerCreditTxnSummary> txnSummaryList = null;
 		final UserDetailsDTO userDetails = this.userServiceProxy.getUserDetailsByEmailId(emailId);
		if (userDetails != null && !StringUtil.isBlank(userDetails.getUserId()) && startDate != null) {
			GetUserTransactionsRequest request = new GetUserTransactionsRequest();
			request.setImsId(userDetails.getUserId());
			request.setLastTimestamp(String.valueOf(startDate.getTime()));
			request.setPagesize(100);
			request.setPageOffset(0);
			try {
				GetUserTransactionsResponse response = this.creditServiceClient.getUserTransactions(request);
				if (response != null) {
					txnSummaryList = this.getCreditSummaryTransactions(response);
					this.setUserProfileInfo(txnSummaryList, userDetails.getUserId());
					logger.info("Consumer credit txn details for user id " + userDetails.getUserId() + " is " + response.toString());
				} else {
					logger.info("Consumer credit txn details is null for user id " + userDetails.getUserId());
				}
			} catch (Exception e) {
				logger.error("Error while fetching consumer credit details for emailId " + emailId, e);
			}
		} else {
			logger.info("Not able to fetch user info for email id " + emailId);
		}
		return txnSummaryList;
	}
	
	public List<DebitEMITxnSummary> getUserDebitEMITransactions(final String emailId, final Date startDate, final Date endDate) {
		logger.info("Fetching user debit transaction details for emailId " + emailId);
		List<DebitEMITxnSummary> txnSummaryList = null;
 		final UserDetailsDTO userDetails = this.userServiceProxy.getUserDetailsByEmailId(emailId);
		if (userDetails != null && !StringUtil.isBlank(userDetails.getUserId()) && startDate != null) {
			GetDebitTxnHistoryViewRequest request = new GetDebitTxnHistoryViewRequest();
			request.setUserId(userDetails.getUserId());
			request.setStartDate(startDate);
			request.setPageSize(100);
			request.setPageOffset(0);
			try {
				GetTxnHistoryViewResponse response = this.txnHistoryViewClientImpl.getUserDebitTxns(request);
				if (response != null) {
					txnSummaryList = this.getDebitEMITransactions(response);
					logger.info("debit emi txn details for user id " + userDetails.getUserId() + " is " + response.toString());
				} else {
					logger.info("debit emi txn details is null for user id " + userDetails.getUserId());
				}
			} catch (Exception e) {
				logger.error("Error while fetching debit emi details for emailId " + emailId + "Exception "+e.getMessage());
			}
		} else {
			logger.info("Not able to fetch user info for email id " + emailId);
		}
		return txnSummaryList;
	}	
	
	private List<DebitEMITxnSummary> getDebitEMITransactions(final GetTxnHistoryViewResponse response) {
		List<DebitEMITxnSummary> txnSummaryList = null;
		if (response != null && CollectionUtils.isNotEmpty(response.getGlobalTransactions())) {
			txnSummaryList = new ArrayList<>();
			for (TxnHistory txnHistory : response.getGlobalTransactions()) {
				if(txnHistory.getDebitInfo() != null) {
					final DebitEMITxnSummary debitEMITxnSummary = new DebitEMITxnSummary();
					
					debitEMITxnSummary.setMerchantName(ConsumerCreditConstants.DEBIT_MERCHANT_NAME);
					debitEMITxnSummary.setMerchantId(ConsumerCreditConstants.DEBIT_MERCHANT_ID);
					debitEMITxnSummary.setFcOrderId(txnHistory.getGlobalTxnId());
					debitEMITxnSummary.setInterestRate(txnHistory.getDebitInfo().getInterestRate());
					debitEMITxnSummary.setTxnStatus(txnHistory.getTxnStatus().toString());	
					debitEMITxnSummary.setLenderTxnId(txnHistory.getDebitInfo().getApplicationId());
					debitEMITxnSummary.setLenderName(ConsumerCreditConstants.DEBIT_LENDER_NAME);
					debitEMITxnSummary.setEmiTenure(txnHistory.getDebitInfo().getTenure());
					debitEMITxnSummary.setLoanDestination(ConsumerCreditConstants.LOAN_DESTINATION_WALLET);
					
					if(txnHistory.getDebitInfo().getLoanAmount() != null) {
						debitEMITxnSummary.setTxnAmount(String.valueOf(txnHistory.getDebitInfo().getLoanAmount()));
						debitEMITxnSummary.setLoanAmount(String.valueOf(txnHistory.getDebitInfo().getLoanAmount()));
					}
					
					if(txnHistory.getDebitInfo().getEmiAmount() != null) {
						debitEMITxnSummary.setEmiAmount(String.valueOf(txnHistory.getDebitInfo().getEmiAmount()));
					}
					
					if(txnHistory.getTransactionDate() != null) {
						debitEMITxnSummary.setTimestamp(String.valueOf(txnHistory.getTransactionDate().getTime()));
					}
					
					txnSummaryList.add(debitEMITxnSummary);
				}else {
					logger.error("No Debit Info found for txn id : " +txnHistory.getGlobalTxnId());
				}
			}
		}
		return txnSummaryList;
	} 
	
	private List<ConsumerCreditTxnSummary> getCreditSummaryTransactions(final GetUserTransactionsResponse response) {
		List<ConsumerCreditTxnSummary> txnSummaryList = null;
		if (response != null && CollectionUtils.isNotEmpty(response.getUserTransactions())) {
			txnSummaryList = new ArrayList<>();
			for (UserTransactionDetailsDTO userTxnsDTO : response.getUserTransactions()) {
				final ConsumerCreditTxnSummary creditTxnSummary = new ConsumerCreditTxnSummary();
				if (userTxnsDTO.getTransactionAmount() != null) {
					creditTxnSummary.setTxnAmount(String.valueOf(userTxnsDTO.getTransactionAmount().doubleValue()));
				}
				if (userTxnsDTO.getTransactionTimestamp() !=  null) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
					creditTxnSummary.setTimestamp(dateFormat.format(userTxnsDTO.getTransactionTimestamp()).toString());
				}
				
				if (userTxnsDTO.getProcessingFee() != null) {
					creditTxnSummary.setProcessingFee(String.valueOf(userTxnsDTO.getProcessingFee().doubleValue()));
				}
				creditTxnSummary.setFcOrderId(userTxnsDTO.getTransactionId());
				creditTxnSummary.setLenderTxnId(userTxnsDTO.getEntityReferenceId());
				creditTxnSummary.setLenderId(userTxnsDTO.getEntityId());
				
			
				creditTxnSummary.setMerchantName(userTxnsDTO.getMerchantName());
				creditTxnSummary.setMerchantId(userTxnsDTO.getMerchantId());
				creditTxnSummary.setLenderName(userTxnsDTO.getLenderName());
				creditTxnSummary.setTxnStatus(userTxnsDTO.getTransactionStatus());
				creditTxnSummary.setEmiAmount(userTxnsDTO.getEmi());
				creditTxnSummary.setEmiTenure(userTxnsDTO.getTenure());
				creditTxnSummary.setInterestRate(userTxnsDTO.getRoi());
				if(userTxnsDTO.getDisbursalType() != null && userTxnsDTO.getDisbursalType().equals(CreditTransactionType.WALLET_DISBURSAL)) {
					creditTxnSummary.setLoanDestination(ConsumerCreditConstants.LOAN_DESTINATION_WALLET);
				}else if(userTxnsDTO.getDisbursalType() != null && userTxnsDTO.getDisbursalType().equals(CreditTransactionType.BANK_DISBURSAL)) {
					creditTxnSummary.setLoanDestination(ConsumerCreditConstants.LOAN_DESTINATION_BANK);
				}
				
				if ((userTxnsDTO.getTransactionAmount()) != null && (userTxnsDTO.getProcessingFee() != null)) {
					creditTxnSummary.setLoanAmount(String.valueOf(userTxnsDTO.getTransactionAmount().add(userTxnsDTO.getProcessingFee())));
				}
				
				txnSummaryList.add(creditTxnSummary);
			}
		}
		return txnSummaryList;
	} 
	
	private void setUserProfileInfo(final List<ConsumerCreditTxnSummary> txnList, final String userId) {
		String creditAvailabilityStatus = StringUtils.EMPTY;
		String totalCreditLimit = StringUtils.EMPTY;
		String availableCreditLimit = StringUtils.EMPTY;
		String bankDetails = StringUtils.EMPTY;
		String bankAccountNo = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(txnList)) {
			final ConsumerCreditUserProfile userProfile = this.getConsumerCreditUserProfile(userId);
			if (userProfile != null && userProfile.getLoCProductDetails() != null) {
				if (userProfile.getLoCProductDetails().getApplicationStatus() != null) {
					creditAvailabilityStatus = userProfile.getLoCProductDetails().getApplicationStatus().toString();
				}
				if (userProfile.getLoCProductDetails().getTotalCreditLimit() != null) {
					totalCreditLimit = String.valueOf(userProfile.getLoCProductDetails().getTotalCreditLimit().doubleValue());
				}
				if (userProfile.getLoCProductDetails().getAvailableCreditLimit() != null) {
					availableCreditLimit = String.valueOf(userProfile.getLoCProductDetails().getAvailableCreditLimit().doubleValue());
				}
				bankDetails = userProfile.getLoCProductDetails().getBankName();
				bankAccountNo = userProfile.getLoCProductDetails().getBankAccountNo();
				for (ConsumerCreditTxnSummary consumerCreditTxnSummary : txnList) {
					consumerCreditTxnSummary.setCreditAvailabilityStatus(creditAvailabilityStatus);
					consumerCreditTxnSummary.setCurrentAvailableCredit(availableCreditLimit);
					consumerCreditTxnSummary.setTotalApprovedCredit(totalCreditLimit);
					if(consumerCreditTxnSummary.getLoanDestination() != null && consumerCreditTxnSummary.getLoanDestination().equals("BANK")) {
						consumerCreditTxnSummary.setBankDetails(bankDetails);
						consumerCreditTxnSummary.setAccountNumber(bankAccountNo);
					}
				}
			}
		}
	}
	
	private ConsumerCreditUserProfile getConsumerCreditUserProfile(final String userId) {
		final GetProfileRequest request = new GetProfileRequest();
		ConsumerCreditUserProfile userProfile = null;
		request.setEntityId(userId);
		request.setEntityType(EntityType.USER);
		try {
			final GetProfileResponse response = this.creditServiceClient.getProfile(request);
			if (response != null) {
				logger.info("Consumer credit profile details for user id " + userId + " is " + response.toString());
				userProfile = (ConsumerCreditUserProfile) response.getUserProfile();
			} else {
				logger.info("Consumer credit profile details is null for user id " + userId);
			}
		} catch (Exception e) {
			logger.error("Error while fetching user profile for userId : " + userId, e);
		}
		return userProfile;
	}
	
	private TransactionInfoDTO getUserTxnInfo(final String txnString) {
		ObjectMapper mapper = new ObjectMapper();
		TransactionInfoDTO txnInfoDTO = null;
		try {
			txnInfoDTO = mapper.readValue(txnString, TransactionInfoDTO.class);
		} catch (IOException e) {
			logger.error("Error while converting to TransactionInfoDTO ", e);
		} catch (Exception e) {
			logger.error("Unknown error while converting to TransactionInfoDTO ", e);
		}
		return txnInfoDTO;
	}
	
	@PostConstruct
	private void init() throws Exception {
		try {
			ClientDetails.init(host, port, timeOut, clientId, clientKey);
		} catch (Exception e) {
			logger.error("Exception while initializing consumer credit Client Config ", e);
		}
		txnHistoryViewClientImpl = new TxnHistoryViewClientImpl();
	}

	public CreditServiceClient getCreditServiceClient() {
		return creditServiceClient;
	}

	public void setCreditServiceClient(CreditServiceClient creditServiceClient) {
		this.creditServiceClient = creditServiceClient;
	}

	public UserServiceProxy getUserServiceProxy() {
		return userServiceProxy;
	}

	public void setUserServiceProxy(UserServiceProxy userServiceProxy) {
		this.userServiceProxy = userServiceProxy;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientKey() {
		return clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public int getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
}
