package com.freecharge.customercare.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.service.InService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xpath.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.freecharge.admin.entity.LifelineInfo;
import com.freecharge.admin.entity.UserPointHistory;
import com.freecharge.admin.model.RewardHistoryWrapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.customercare.entity.ReferralEntity;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.google.gson.Gson;


@Service
public class CustomerTrailService {

	@Autowired
	private InService inService;

	@Autowired
	private BillPaymentService billPaymentService;

	@Autowired
	protected FCProperties fcProperties;
	
    @Autowired
    private AdminMetricService adminMetricService;
    
    @Autowired
    protected UserServiceProxy userServiceProxy;
    
	private final Logger logger = LoggingFactory.getLogger(CustomerTrailService.class);
	private static final String campaignHistoryURI = "/api/getcampaignhistory?";
	private static final String fetchFromOrderArg = "orderId=";
	private static final String referralFetchFromCodeURI = "/getreferralcodedetails?";	
	private static final String referralFetchFromCodeArg = "referralCode=";
	private static final String referralFetchFromUserURI = "/getreferralhistorybyuserid?";	
	private static final String fetchFromUserArg = "userId=";
	private static final String lifeLineFetchRelativeUri = "/getUserPointsList?";
	
	public RewardHistoryWrapper getRewardHistoryListFromOrderId(String orderId) {
		
		logger.info("Fetching Campaign History for OrderID: " + orderId);
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		
		restArg.append(fetchFromOrderArg).append(orderId);
		restUri.append(fcProperties.getProperty(FCProperties.CAMPAIGN_SERVICE_URI)).append(campaignHistoryURI).append(restArg);
		
		logger.info("Campaign History Fetch URI: " + restUri);
		
		RewardHistoryWrapper rewardHistoryWrapper = new RewardHistoryWrapper();
		StringBuilder status = new StringBuilder();
		
		long startMillis = System.currentTimeMillis();
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			String stringObj = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("stringObj: " + stringObj + " received from campaign service for order id: " + orderId);
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			ObjectReader objectReader = objectMapper.reader(RewardHistoryWrapper.class);
			RewardHistoryWrapper receivedRewardHistoryWrapper = objectReader.readValue(stringObj);
			
			logger.info("receivedRewardHistoryWrapper: " + receivedRewardHistoryWrapper + " received from campaign service for order id: " + orderId);
			
			if(receivedRewardHistoryWrapper != null) {
				status.append(receivedRewardHistoryWrapper.getStatus());
				rewardHistoryWrapper = receivedRewardHistoryWrapper;
			}
			else {
				status.append("Unexpected NULL response in Campaign History Retrieval");
			}
			
		} catch (RestClientException e) {
			status.append("Service threw an exception while fetching campaign history results via rest call for order id:" + orderId);
			logger.error(status, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getRewardHistoryListFromOrderId", e);
		} catch (URISyntaxException e) {
			status.append("Service threw an exception while forming URI for fetching campaign history results via rest call for order id:" + orderId);
			logger.error(status, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getRewardHistoryListFromOrderId", e);
		} catch (Exception e) {
			status.append("Unexpected exception while fetching campaign history results for order id:" + orderId);
			logger.error(status, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getCampaignHistoryResultsFromOrderId", e);
		}
		adminMetricService.metricSuccess(startMillis, this.getClass().getName(), "getRewardHistoryListFromOrderId");
		rewardHistoryWrapper.setStatus(status.toString());
		return rewardHistoryWrapper;
	}
	
	public ReferralEntity getReferralDetailsFromCode(String referralCode) {
		
		logger.info("Fetching Referral Details for Referral Code: " + referralCode);
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		
		restArg.append(referralFetchFromCodeArg).append(referralCode);
		restUri.append(fcProperties.getProperty(FCProperties.REFERRAL_SERVICE_URI)).append(referralFetchFromCodeURI).append(restArg);
		
		logger.info("Referral Details Fetch URI: " + restUri);
		String stringObject = null;
		ReferralEntity referralEntity = null;
		
		long startMillis = System.currentTimeMillis();
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			stringObject = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("Received Json Object: " + stringObject + " for Referral Code: " + referralCode);
			
			referralEntity = new Gson().fromJson(stringObject, ReferralEntity.class);
			logger.info("Referral Details: " + referralEntity + " received from campaign service for referral code: " + referralCode);
			
		} catch (RestClientException e) {
			logger.error("Service threw an exception while fetching Referral Details via rest call for referralCode:" + referralCode, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromCode", e);
		} catch (URISyntaxException e) {
			logger.error("Service threw an exception while forming URI for fetching Referral Details via rest call for referralCode:" + referralCode, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromCode", e);
		} catch (Exception e) {
			logger.error("Unexpected exception while fetching Referral Details for referralCode:" + referralCode, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromCode", e);
		}
		
		adminMetricService.metricSuccess(startMillis, this.getClass().getName(), "getReferralDetailsFromCode");
		return referralEntity;
	}

	public List<ReferralEntity> getReferralDetailsFromReferrerEmail(String email) {
		
		logger.info("Fetching Referral Details for ReferrerEmail: " + email);
		
		long userId = userServiceProxy.getUserIdByEmailId(email);
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();

		restArg.append(fetchFromUserArg).append(userId);
		restUri.append(fcProperties.getProperty(FCProperties.REFERRAL_SERVICE_URI)).append(referralFetchFromUserURI).append(restArg);
		
		logger.info("Referral Details Fetch URI: " + restUri);
		List<ReferralEntity> referralEntityList = null;
		String stringObj = null;
		
		long startMillis = System.currentTimeMillis();
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			stringObj = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("Received String Object:" + stringObj + " for Referrer Email: " + email);
			

			ObjectMapper mapper = new ObjectMapper();
			referralEntityList = mapper.readValue(stringObj, TypeFactory.defaultInstance().
					                    constructCollectionType(List.class, ReferralEntity.class));

			logger.info("Referral Details: " + referralEntityList + " received from campaign service for referrer email: " + email);
			
		} catch (RestClientException e) {
			logger.error("Service threw an exception while fetching Referral Details via rest call ReferrerEmail:" + email, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromReferrerEmail", e);
		} catch (URISyntaxException e) {
			logger.error("Service threw an exception while forming URI for fetching Referral Details via rest call for ReferrerEmail:" + email, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromReferrerEmail", e);
		} catch (Exception e) {
			logger.error("Unexpected exception while fetching Referral Details for ReferrerEmail:" + email, e);
			adminMetricService.recordTypeOfException(startMillis,  this.getClass().getName(), "getReferralDetailsFromReferrerEmail", e);
		}
		
		adminMetricService.metricSuccess(startMillis, this.getClass().getName(), "getReferralDetailsFromReferrerEmail");
		return referralEntityList;
	}
	
	public LifelineInfo getLifelineHistoryFromEmail(String email) {
		
		logger.info("Fetching Lifeline Details for Email: " + email);
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		
		restArg.append("emailId=").append(email);
		restUri.append(fcProperties.getProperty(FCProperties.LIFELINE_SERVICE_URI)).append(lifeLineFetchRelativeUri).append(restArg);
		
		logger.info("Lifeline Details Fetch URI: " + restUri);
		LifelineInfo lifelineInfo = null;
		String stringObject = null;
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			stringObject = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("Received Json Object: " + stringObject + " for Email: " + email);
			
			lifelineInfo = new Gson().fromJson(stringObject, LifelineInfo.class);
			convertEpochToDate(lifelineInfo);
			
			logger.info("Lifeline Details: " + lifelineInfo + " received from lifeline service for email: " + email);
		} catch (RestClientException e) {
			logger.error("Service threw an exception while fetching Lifeline Details via rest call Email:" + email, e);
		} catch (URISyntaxException e) {
			logger.error("Service threw an exception while forming URI for fetching Lifeline Details via rest call for Email:" + email, e);
		} catch (Exception e) {
			logger.error("Unexpected exception while fetching Referral Details for Email:" + email, e);
		}
		return lifelineInfo;
	}
	
	private void convertEpochToDate(LifelineInfo lifelineInfo) {
	
		if(lifelineInfo != null) {
			if(lifelineInfo.getPointHistoryList() != null) {
				
				for(UserPointHistory userPointHistory : lifelineInfo.getPointHistoryList()) {
					String createdOn = userPointHistory.getCreatedOn();
					String updatedOn = userPointHistory.getUpdatedOn();
					userPointHistory.setCreatedOn(createdOn == null ? null : (new Date(Long.parseLong(createdOn))).toString());
					userPointHistory.setUpdatedOn(updatedOn == null ? null : (new Date(Long.parseLong(updatedOn))).toString());
				}
			}
		}
	}

	public Boolean isRechargeNotFailureForOrder(String orderId) {
		List<InTransactions> inTransactionsList = null;
        Boolean isRechargeAttemptNotFailure = false;
		inTransactionsList = inService.getInTransactionsListByOrder(orderId);
        if( !FCUtil.isEmpty(inTransactionsList)) {
            Collections.reverse(inTransactionsList);
            logger.info("Latest inTransaction for orderId: " + orderId + " is: " + inTransactionsList.get(0).
                    toString());
            isRechargeAttemptNotFailure = isRechargeNotFailure(inTransactionsList.get(0));
            logger.info("isRechargeAttemptNotFailure: " + isRechargeAttemptNotFailure);
        }
		return isRechargeAttemptNotFailure;
	}

	public Boolean isBillPaymentNotFailureForOrder( String orderId) {
        Boolean isBillPaymentAttemptNotFailure = false;        
		BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
        if( null != billPaymentStatus) {
            logger.info("billPaymentStatus for orderId: " + orderId + " is: " + billPaymentStatus.toString());
            isBillPaymentAttemptNotFailure = isBillPaymentNotFailure(billPaymentStatus);
            logger.info("isBillPaymentAttemptNotFailure: " + isBillPaymentAttemptNotFailure);
        }
		return isBillPaymentAttemptNotFailure;
	}

	public Boolean isRechargeNotFailure(InTransactions inTransaction) {
		Boolean result = false;
		if( null != inTransaction) {
			if (null == inTransaction.getAggrResponseCode()
					|| AggregatorInterface.SUCCESS_OR_PENDING_AGGREGATOR_RESPONSES
							.contains(inTransaction.getAggrResponseCode())) {
				result = true;
			}
		}
		return result;
	}

	public Boolean isBillPaymentNotFailure(BillPaymentStatus billPaymentStatus) {
		Boolean result = false;
		if (null == billPaymentStatus.getAuthStatus() || AggregatorInterface.SUCCESS_OR_PENDING_AGGREGATOR_RESPONSES
				.contains(billPaymentStatus.getAuthStatus())) {
			result = true;
		}
		return result;
	}

	public String getFinalOperatorRetryProductType(Boolean isRechargeAttemptSuccessful,
   		Boolean isBillPaymentAttemptSuccessful, String productType, String orderId) {
		if(FCUtil.isRechargeProductType(productType)) {
			if(!isRechargeAttemptSuccessful && isBillPaymentAttemptSuccessful) {
                logger.info("Changing productType to M for orderId: "+orderId);
				return "M";
			}
		} else if(FCUtil.isBillPostpaidType(productType)) {
			if(!isBillPaymentAttemptSuccessful && isRechargeAttemptSuccessful) {
                logger.info("Changing productType to V for orderId: "+orderId);
				return "V";
			}
		}
		return productType;
	}
	

	public  List<Map<String, String>> getReversalRequestSetFromFile(final MultipartFile file) throws IOException {

		List<Map<String, String>> requestList = new ArrayList<Map<String, String>> ();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line;

		while ((line = bufferedReader.readLine()) != null) {

			Map<String, String> requestSet = getReversalData(line);
			if(isDuplicateReversalData(requestList, requestSet) ) {
				logger.info("Reversal Valid Reqeust reached as : " + requestSet);
				requestList.add(requestSet);
			}
		}
		return requestList;
	}

	public List<Map<String, String>> getReversalRequestFromText(String inputIdsString) {

		String[] inputIds = inputIdsString.split("\n");
		logger.debug("Reversal InputRawString : " + inputIdsString);

		List<Map<String, String>> requestList = new ArrayList<Map<String, String>> ();

		for (String line : inputIds) {

			Map<String, String> requestSet = getReversalData(line.replaceAll("\\p{Cc}", ""));

			if(isDuplicateReversalData(requestList, requestSet) ) {
				logger.info("Reversal Valid Reqeust reached as : " + requestSet);
				requestList.add(requestSet);
			}

		}
		return requestList;
	}	
	
	public boolean isDuplicateReversalData(List<Map<String, String>> requestList, Map<String, String> requestSet) { 
		if(!FCUtil.isEmpty(requestSet)) {
			
			for(Map<String, String> request : requestList) {
				if(request.get(RechargeConstants.REQUEST_ID).equalsIgnoreCase(requestSet.get(RechargeConstants.REQUEST_ID))) {
					logger.info("Duplicate Reversal Request arrived for requestId : " + requestSet);
					return false;
				}
			}
			
			return true;
		}
		
		return false;
	}
		 
	public Map<String, String> getReversalData(String rawData) {
		String[] column = rawData.split(",");
		if(column.length < 3){
			logger.error("Invalid Reversal Request Data arrived. Request Id or Status missing with data : " + rawData);
			return null;
		}

		String requestId = FCUtil.trim(column[0]);
		if(FCUtil.isEmpty(requestId)) {
			logger.info("Invalid  Reversal Request arrived for requestId : " + requestId);
			return null;
		}

		String agRefId = FCUtil.trim(column[1]);

		String rechargeStatus = FCUtil.trim(column[2]);
		if(FCUtil.isEmpty(rechargeStatus) || !AG_REVERSAL_STATUS.isValid(rechargeStatus)) {
			logger.info("Invalid Reversal Request arrived for requestId : " + requestId + " with rechargeStatus : " + rechargeStatus);
			return null;
		}

		Map<String, String> requestSet = new HashMap<String, String>();
		requestSet.put(RechargeConstants.REQUEST_ID, requestId);
		requestSet.put(RechargeConstants.AG_REF_ID, agRefId);
		requestSet.put(RechargeConstants.RECHARGE_STATUS, rechargeStatus);
		return requestSet;
	}
	
	public static enum AG_REVERSAL_STATUS {
		SUCCESS, FAILED;

		public static boolean isValid(String value) {
			for (AG_REVERSAL_STATUS status : AG_REVERSAL_STATUS.values()) {
				if (status.toString().equalsIgnoreCase(value)) {
					return true;
				}
			}
			return false;
		}	
	};

}
