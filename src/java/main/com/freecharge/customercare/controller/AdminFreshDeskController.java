package com.freecharge.customercare.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.api.exception.InvalidPathValueException;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.AbstractRestController;
import com.freecharge.wallet.WalletService;

/*These APIs are used for Freshdesk Integration*/

@Controller
@RequestMapping("/admin/external")
public class AdminFreshDeskController extends AbstractRestController {
	public static final String ORDER_ID_NOT_PRESENT = "Order Id is not present";
	public static final String ORDER_ID_INVALID = "Invalid OrderId";
	private final Logger logger = LoggingFactory.getLogger(AdminFreshDeskController.class);

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private InService inService;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private WalletService walletService;
	
	@Autowired
	private CartService cartService;

	@Autowired
	private FreefundService freefundService;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	/*
	 * This API to get recharge data against orderId Output Format will be Json
	 * Request : Order Id Response: Recharge Status,Recharge date, Recharge
	 * Number,Amount,Operator Txn Id , Product name.
	 */
	@RequestMapping(value = "/transaction/{orderId}/rechargedata", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getRechargeDataByOrderId(@PathVariable String orderId,
			ModelMap model) throws InvalidPathValueException {
		if (FCUtil.isEmpty(orderId)) {
			return getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA);
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			return getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID);
		}

		Map<String, Object> rechargeDataMap = new HashMap<>();
		UserTransactionHistory userTransactionHistory = userTransactionHistoryService
				.findUserTransactionHistoryByOrderId(orderId);
		if (userTransactionHistory != null) {
			String createdOn = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss")
					.format(userTransactionHistory.getCreatedOn());
			rechargeDataMap.put("RechargeDate", createdOn);
			rechargeDataMap.put("RechargeNumber",
					userTransactionHistory.getSubscriberIdentificationNumber());
			rechargeDataMap.put("RechargeAmount",
					userTransactionHistory.getTransactionAmount());
			InTransactionData inTransactionData = inService
					.getLatestInTransactionData(orderId);
			if (inTransactionData != null) {
				if (inTransactionData.getInResponse() != null) {
					rechargeDataMap.put("OperatorTxnId", inTransactionData
							.getInResponse().getOprTransId());
				}
			}

			rechargeDataMap.put("RechargeStatus", RechargeUtil
					.getRechargeStatusMessage(userTransactionHistory
							.getTransactionStatus()));

			rechargeDataMap.put("ProductName", ProductMaster.ProductName
					.fromPrimaryProductType(userTransactionHistory
							.getProductType()));
			
			String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
			rechargeDataMap.put("Email", (email != null) ? email : " ");
		}
		return rechargeDataMap;
	}

	/*
	 * This API to get payment data against orderId Output Format will be Json
	 * Request : Order Id Response: Payment Status,Amount,Txn date,Wallet
	 * balance
	 */
	@RequestMapping(value = "/transaction/{orderId}/paymentdata", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getPaymentDataByOrderId(@PathVariable String orderId,
			ModelMap model) throws InvalidPathValueException {
		if (FCUtil.isEmpty(orderId)) {
			return getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA);
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			return getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID);
		}
		Map<String, Object> paymentDataMap = new HashMap<>();
		PaymentTransaction paymentTransaction = paymentTransactionService
				.getLatestPaymentTransactionThroughPGByOrderId(orderId);
		if (paymentTransaction != null) {
			paymentDataMap
					.put("PaymentStatus",
							paymentTransaction.getIsSuccessful() ? "Success"
									: "Failed");
			paymentDataMap.put("Amount", paymentTransaction.getAmount());
			paymentDataMap.put("TransactionDate", (paymentTransaction
					.getRecievedToPg() != null) ? paymentTransaction
					.getRecievedToPg().toString() : " ");
			Double balance = walletService.findWalletBalanceByOrderId(orderId);
			if (balance != null) {
				paymentDataMap.put("WalletBalance", balance.toString());
			} else {
				paymentDataMap.put("WalletBalance", " ");
			}
		}
		return paymentDataMap;
	}

	/*
	 * API to get freefund related data 
	 * Request : OrderId 
	 * Response : Map Object in json format
	 * Response map : Promocode,PromocodeStatus,Campaign,
	 * UsedEmailId,UsedMobileNo,UsedDate,WalletBalance
	 */

	@RequestMapping(value = "/transaction/{orderId}/freefunddata", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getFreefundDataByOrderId(@PathVariable String orderId,
			ModelMap model) throws InvalidPathValueException {
		if (FCUtil.isEmpty(orderId)) {
			return getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA);
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			return getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID);
		}
		Map<String, Object> freefundDataMap = new HashMap<>();
		FreefundCoupon freefundCoupon = freefundService
				.getFreefundCouponObjectByOrderId(orderId);
		if (freefundCoupon != null) {
			freefundDataMap.put("Promocode", freefundCoupon.getFreefundCode());
			freefundDataMap.put("PromocodeStatus", freefundCoupon.getStatus());
			freefundDataMap.put("UsedEmailId", freefundCoupon.getUsedEmailId());
			freefundDataMap.put("UsedMobileNo",
					freefundCoupon.getUsedServiceNumber());
			freefundDataMap.put("UsedDate",
					(freefundCoupon.getUsedDate() != null) ? freefundCoupon
							.getUsedDate().toString() : " ");
			FreefundClass freefundClass = freefundService
					.getFreefundClass(freefundCoupon.getFreefundClassId());

			freefundDataMap.put("Campaign",
					(freefundClass != null) ? freefundClass.getName() : " ");
			Double balance = walletService.findWalletBalanceByOrderId(orderId);
			if (balance != null) {
				freefundDataMap.put("WalletBalance", balance.toString());
			} else {
				freefundDataMap.put("WalletBalance", " ");
			}
		}
		return freefundDataMap;
	}

	/*
	 * API for refund related data. 
	 * Request : OrderId 
	 * Response : List of Maps in Json format
	 * These Maps contains Refund-to-Wallet and Bank-refund data  
	 * Response map : RefundStatus,RefundTo,
	 * RefundAmount,RefundDate,WalletBalance.
	 */
	@RequestMapping(value = "/transaction/{orderId}/refunddata", method = RequestMethod.GET)
	public @ResponseBody
	List<Map<String, Object>> getRefundDataByOrderId(
			@PathVariable String orderId, ModelMap model)
			throws InvalidPathValueException {
		List<Map<String, Object>> refundDataList = new ArrayList<>();

		if (FCUtil.isEmpty(orderId)) {
			refundDataList.add(getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA));
			return refundDataList;
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			refundDataList.add(getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID));
			return refundDataList;
		}

		List<PaymentRefundTransaction> paymentRefundTransactions = this.paymentTransactionService
				.getPaymentRefundTransactions(orderId);
		if (!FCUtil.isEmpty(paymentRefundTransactions)) {
			for (PaymentRefundTransaction paymentRefundTransaction : paymentRefundTransactions) {
				Map<String, Object> refundDataMap = new HashMap<>();
				refundDataMap.put("RefundStatus",
						paymentRefundTransaction.getStatus());
				refundDataMap.put("RefundTo",
						paymentRefundTransaction.getRefundTo());
				refundDataMap
						.put("RefundAmount",
								(paymentRefundTransaction.getRefundedAmount() != null) ? paymentRefundTransaction
										.getRefundedAmount().getAmount() : " ");
				refundDataMap.put("RefundInitiatedOn", (paymentRefundTransaction
						.getSentToPG() != null) ? paymentRefundTransaction
						.getSentToPG().toString() : " ");
				Double balance = walletService.findWalletBalanceByOrderId(orderId);
				if (balance != null) {
					refundDataMap.put("WalletBalance", balance.toString());
				} else {
					refundDataMap.put("WalletBalance", " ");
				}

				/*Getting next successful transactions*/
				String successOrderIds = " ";
				UserTransactionHistory userTransactionHistory = userTransactionHistoryService
						.findUserTransactionHistoryByOrderId(orderId);
				if (userTransactionHistory != null) {
					List<String> successUserTransactionHistoryList = userTransactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userTransactionHistory.getFkUserId(), userTransactionHistory.getLastUpdated(), 3);
					if (!FCUtil.isEmpty(successUserTransactionHistoryList)) {
						successOrderIds = StringUtils.join(successUserTransactionHistoryList, ",");
					}	
				}
				refundDataMap.put("NextSuccessOrderIds", successOrderIds);
				refundDataList.add(refundDataMap);
			}
		}
		return refundDataList;
	}
	
	/*
	 * API for coupon data. 
	 * Request : OrderId 
	 * Response : List of Maps in Json format
	 * These Maps contains coupon details  
	 * Response map : CampaignName,CouponCode,
	 * CouponValue,OptinType,Price.
	 */
	@RequestMapping(value = "/transaction/{orderId}/coupondata", method = RequestMethod.GET)
	public @ResponseBody
	List<Map<String, Object>> getCouponDataByOrderId(
			@PathVariable String orderId, ModelMap model)
			throws InvalidPathValueException {
		List<Map<String, Object>> couponDataList = new ArrayList<>();

		if (FCUtil.isEmpty(orderId)) {
			couponDataList.add(getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA));
			return couponDataList;
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			couponDataList.add(getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID));
			return couponDataList;
		}

		List<CouponHistory> couponHistoryList = null;
        try {
			couponHistoryList = couponServiceProxy.getCouponHistoryList(Arrays.asList(orderId));
		} catch (InvalidParameterException e) {
			logger.error("Exception while fetching coupons from freshdesk couponapi : " + orderId, e);
		}
		
        if (FCUtil.isEmpty(couponHistoryList)) {
        	return couponDataList;
        }
        
        for (CouponHistory couponHistory : couponHistoryList) {
        	Map<String, Object> couponMap = new HashMap<>();
        	couponMap.put("CampaignName", couponHistory.getCampaignName());
        	couponMap.put("CouponCode", couponHistory.getCouponCode());
        	couponMap.put("CouponValue", String.valueOf(couponHistory.getCouponValue()));
        	couponMap.put("OptinType", couponHistory.getOptinType());
        	couponMap.put("Price", String.valueOf(couponHistory.getPrice()));
        	couponDataList.add(couponMap);	
        }
        return couponDataList;
   }

	/*
	 * API for success payment list.
	 * Request : OrderId.
	 * Response : List of Maps in Json format.
	 * These Maps contains payment details.
	 * Response map : MtxnId,Amount,
	 * TransactionDate,PG.
	 */
	@RequestMapping(value = "/transaction/{orderId}/successpaymentlist", method = RequestMethod.GET)
	public @ResponseBody
	List<Map<String, Object>> getPaymentTransactionsByOrderId(@PathVariable String orderId,
			ModelMap model) throws InvalidPathValueException {
		List<Map<String, Object>> paymentDataMapList = new ArrayList<>();
		if (FCUtil.isEmpty(orderId)) {
			paymentDataMapList.add(getValidationErrorMap(ORDER_ID_NOT_PRESENT,
					ErrorCode.EMPTY_REQUEST_DATA));
			return paymentDataMapList;
		}

		if (!FCUtil.validateOrderIdFormat(orderId)) {
			paymentDataMapList.add(getValidationErrorMap(ORDER_ID_INVALID,
					ErrorCode.INVALID_ORDER_ID));
			return paymentDataMapList;
		}
		
		List<PaymentTransaction> paymentTransactionList = paymentTransactionService.getSuccessfulPaymentTransactionThroughPGByOrderId(orderId);
		if (FCUtil.isEmpty(paymentTransactionList)) {
			return null;
		}

		for (PaymentTransaction paymentTransaction : paymentTransactionList) {
			if (paymentTransaction != null) {
				Map<String, Object> paymentDataMap = new HashMap<>();
				paymentDataMap.put("MtxnId", paymentTransaction.getMerchantTxnId());
				paymentDataMap.put("Amount", paymentTransaction.getAmount());
				paymentDataMap.put("TransactionDate", (paymentTransaction.getRecievedToPg() != null) ? paymentTransaction.getRecievedToPg().toString() : " ");
				paymentDataMap.put("PG", paymentTransaction.getPaymentGateway());
				paymentDataMapList.add(paymentDataMap);
			}
		}
		return paymentDataMapList;
	}
	
	private Map<String, Object> getValidationErrorMap(String errorMsg,
			ErrorCode errorCode) {
		Map<String, Object> errorMap = new HashMap<>();
		errorMap.put("status", "failure");
		errorMap.put("Errors", errorMsg);
		errorMap.put("ErrorCode", errorCode.getErrorNumberString());
		return errorMap;
	}
}
