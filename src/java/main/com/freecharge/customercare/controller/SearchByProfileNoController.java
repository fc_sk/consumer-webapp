package com.freecharge.customercare.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.controller.CsOneCheckBankTransferController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.model.ProfileNumberTransaction;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.util.ViewConstants;
import com.snapdeal.ims.response.GetUserResponse;

@Controller
@RequestMapping("/admin/customertrail/*")
public class SearchByProfileNoController extends BaseAdminController {
	private static Logger logger = LoggingFactory.getLogger(SearchByProfileNoController.class);
    private static final String           PROFILENO = "profileNo";
    
    @Autowired
    private UserTransactionHistoryService transactionHistoryService;

    @Autowired
    private CartService                   cartService;

    @Autowired
    private InService                     inService;

    @Autowired
    private PaymentTransactionService     paymentTransactionService;
    
    @Autowired
    private BillPaymentService        billPaymentService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
	@Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @RequestMapping(value = "searchByProfileNo")
    public String getCustomerTrail(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
            get403HtmlResponse(response);
            return null;
        }

        String profileNo = null;
        if (mapping.get(PROFILENO) == null || mapping.get(PROFILENO).isEmpty()) {
            model.addAttribute("exception", "Profile Number not entered");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        profileNo = mapping.get(PROFILENO).trim();

        /* To display last 3 months data only */
        Calendar fromDate = Calendar.getInstance();
        fromDate.add(Calendar.DATE, -90);
        Timestamp fromDateTime = new Timestamp(fromDate.getTime().getTime());

        List<ProfileNumberTransaction> profileNumberTransactionList = new ArrayList<>();
        //List<Users> users = userServiceProxy.getUsersByProfileMobileNo(profileNo);
        //Use IMS to get the User object corresponding to mobile number
        GetUserResponse userResponse = oneCheckWalletService.getUserByMobile(profileNo);
        //for (Users user : users) {
        //there is only one user for a mobile number in IMS
        if(null!=userResponse && null!=userResponse.getUserDetails() && null!=userResponse.getUserDetails().getUserId()){
        	List<UserTransactionHistory> userTransactionHistoryList = transactionHistoryService
                    .findUserTransactionHistoryByUserId(userResponse.getUserDetails().getFcUserId(), fromDateTime);
            Collections.reverse(userTransactionHistoryList);
            for (UserTransactionHistory userTransactionHistory : userTransactionHistoryList) {
                ProfileNumberTransaction profileNumberTransaction = getProfileNumberTransactionObject(userTransactionHistory);
                profileNumberTransactionList.add(profileNumberTransaction);
            }
        } 
       
        /*Getting email id.*/
        model.addAttribute("user", userResponse);
        model.addAttribute("emailIdMigrationStatusMap", getmigrationStatusMapForIMSUser(userResponse));
        model.addAttribute("profileNoEntered", profileNo);
        model.addAttribute("profileNumberTransactionList", profileNumberTransactionList);
        return ViewConstants.CUSTOMER_TRAIL_BY_PROFILENO;
    }

    private ProfileNumberTransaction getProfileNumberTransactionObject(
            final UserTransactionHistory userTransactionHistory) {
        PaymentTransaction paymentTransaction = paymentTransactionService
                .getLastPaymentTransactionSentToPG(userTransactionHistory.getOrderId());
        ProfileNumberTransaction profileNumberTransactionInfo = new ProfileNumberTransaction();
        profileNumberTransactionInfo.setOrderId(userTransactionHistory.getOrderId());
        profileNumberTransactionInfo.setUserId(userTransactionHistory.getFkUserId()); 
        profileNumberTransactionInfo.setEmailId(userServiceProxy.getUserByUserId(userTransactionHistory.getFkUserId())
                .getEmail());
        profileNumberTransactionInfo.setCreatedOn(userTransactionHistory.getCreatedOn());
        profileNumberTransactionInfo.setRechargeAmount(String.valueOf(userTransactionHistory.getTransactionAmount()));
        profileNumberTransactionInfo.setServiceProvider(userTransactionHistory.getServiceProvider());
        profileNumberTransactionInfo.setPromocode(getPromocodeByOrderId(userTransactionHistory.getOrderId()));
        profileNumberTransactionInfo.setProductName(FCConstants.productTypeToNameMap.get(userTransactionHistory
                .getProductType()));
        if (paymentTransaction != null) {
            if (paymentTransaction.getIsSuccessful()) {
                profileNumberTransactionInfo.setPaymentStatus("Success");
            } else {
                profileNumberTransactionInfo.setPaymentStatus("Failed");
            }

            profileNumberTransactionInfo.setPaymentGateway(paymentTransaction.getPaymentGateway());
        }

        /* Check if it is a postpaid */
        if (ProductMaster.ProductName.MobilePostpaid.getProductType().equals(userTransactionHistory.getProductType())) {
            profileNumberTransactionInfo.setRechargeStatus(RechargeUtil
                    .getPostpaidRechargeStatusMessage(userTransactionHistory.getTransactionStatus()));
            profileNumberTransactionInfo.setAggregator(getBillPaymentAG(userTransactionHistory.getOrderId()));
        } else {/* If normal recharge */
            profileNumberTransactionInfo.setRechargeStatus(RechargeUtil.getRechargeStatusMessage(userTransactionHistory
                    .getTransactionStatus()));
            InTransactionData inTransactionData = inService.getLatestInTransactionData(userTransactionHistory.getOrderId());
            if (inTransactionData != null) {
                profileNumberTransactionInfo.setAggregator(inTransactionData.getInResponse().getAggrName());
            }
        }
        return profileNumberTransactionInfo;
    }

    private String getPromocodeByOrderId(final String orderId) {
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
        if (cartBusinessDo != null) {
            List<CartItemsBusinessDO> cartItemsBusinessDOList = cartBusinessDo.getCartItems();
            if (cartItemsBusinessDOList != null && (!cartItemsBusinessDOList.isEmpty())) {
                for (CartItemsBusinessDO cartItemsBusinessDO : cartItemsBusinessDOList) {
                    if ((cartItemsBusinessDO.getProductMasterId() == ProductMaster.ProductName.FreeFund.getProductId())
                            && cartItemsBusinessDO.getEntityId().equals("FreeFund")) {
                        return cartItemsBusinessDO.getDisplayLabel();
                    }
                }
            }
        }
        return null;
    }
    
    private String getBillPaymentAG(String orderId) {
        BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
        if (billPaymentStatus != null) {
            return billPaymentStatus.getBillPaymentGateway();
        }
        return " ";
    }

	private Map<String, String> getmigrationStatusMapForIMSUser(GetUserResponse userResponse) {
		Map<String, String> migrationStatusMap = new HashMap<>();
		if (null == userResponse || null == userResponse.getUserDetails()
				|| null == userResponse.getUserDetails().getEmailId()) {
			return null;
		}
		try {
			MigrationStatus migrationStatus = oneCheckWalletService
					.getFcWalletMigrationStatus(userResponse.getUserDetails().getEmailId(), null);
			if (migrationStatus != null) {
				migrationStatusMap.put(userResponse.getUserDetails().getEmailId(),
						migrationStatus.getMigrationStatus());
			} else {
				migrationStatusMap.put(userResponse.getUserDetails().getEmailId(), "Migration status not available");
			}
		} catch (Exception ex) {
			logger.error("Exception on searchby profile no, while migration status check "
					+ userResponse.getUserDetails().getEmailId(), ex);
			migrationStatusMap.put(userResponse.getUserDetails().getEmailId(), "Migration status not available");
		}
		return migrationStatusMap;
	}
    
    private Map<String, String> getmigrationStatusMap(List<Users> userList) {
    	Map<String, String> migrationStatusMap = new HashMap<>();
    	if (FCUtil.isEmpty(userList)) {
    		return null;
    	}
    	for (Users user : userList) {
    		try {
    			MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(user.getEmail(), null);
    			if (migrationStatus != null) {
    				migrationStatusMap.put(user.getEmail(), migrationStatus.getMigrationStatus());
    			} else {
    				migrationStatusMap.put(user.getEmail(), "Migration status not available");
    			} 
    		} catch (Exception ex) {
    			logger.error("Exception on searchby profile no, while migration status check " + user.getEmail(), ex);
    			migrationStatusMap.put(user.getEmail(), "Migration status not available");
    		}
    	}
    	return migrationStatusMap;
    }
}