package com.freecharge.customercare.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/customertrail/getTransactions*")
public class SingleInputBoxAdminController extends BaseAdminController {
    public static final String SINGLE_INPUT_BOX              = "searchInput";
    public static final String CUSTOMER_TRAIL_ORDERID_LINK   = "/admin/customertrail/getcustomertrail.htm";
    public static final String CUSTOMER_TRAIL_MOBILENO_LINK  = "/admin/mobile_input_transaction/do.htm";
    public static final String CUSTOMER_TRAIL_EMAILID_LINK   = "/admin/email_input_transaction/do.htm";
    public static final String CUSTOMER_TRAIL_PROMOCODE_LINK = "/admin/customertrail/getorderidbycouponcode";
    
    @RequestMapping(value = "singleTextBox")
    public String getCustomerTrail(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
            get403HtmlResponse(response);
            return null;
        }

        if (mapping.get(SINGLE_INPUT_BOX) == null || mapping.get(SINGLE_INPUT_BOX).isEmpty()) {
            model.addAttribute("exception", "Valid search Item required(order-id,email-id,mobile-no,promocode)");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        String searchInput = mapping.get(SINGLE_INPUT_BOX).trim();

        Boolean isOrderId = FCUtil.validateOrderIdFormat(searchInput);
        if (isOrderId) {
        	return String.format("redirect:%s?orderId=%s", CUSTOMER_TRAIL_ORDERID_LINK, searchInput);
        }
        
        Boolean isEmailId = FCUtil.isEmailIdFormat(searchInput);
        if (isEmailId) {
            return String.format("redirect:%s?email=%s", CUSTOMER_TRAIL_EMAILID_LINK, searchInput);
        }

        Boolean isMobileNo = FCUtil.isMobileNoFormat(searchInput);
        if (isMobileNo) {
            return String.format("redirect:%s?mobile=%s", CUSTOMER_TRAIL_MOBILENO_LINK, searchInput);
        }
        return String.format("redirect:%s?promocode=%s", CUSTOMER_TRAIL_PROMOCODE_LINK, searchInput);
    }
}
