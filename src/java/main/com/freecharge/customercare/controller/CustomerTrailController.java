package com.freecharge.customercare.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.util.CollectionUtils;

import com.freecharge.admin.CustomerTrailDetails.UPITransactionDetailView;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.dao.BbpsTransactionDAO;
import com.freecharge.admin.deals.BinOfferData;
import com.freecharge.admin.entity.BbpsTransactionDetails;
import com.freecharge.admin.entity.LifelineInfo;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.model.RewardHistoryWrapper;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.admin.service.UPITransactionService;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.model.RedemptionInfo;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.api.coupon.service.web.model.CouponStore.CouponNature;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.TxnCrossSell;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.CrossSellHistory;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.OrderFulfillmentDetails;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.domain.entity.jdbc.TransactionFulfilment;
import com.freecharge.app.domain.entity.jdbc.TransactionHomePage;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.CouponReissueService;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.CrossSellHistoryService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.MyAccountService;
import com.freecharge.app.service.PersonalDetailsService;
import com.freecharge.app.service.TxnCrossSellService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.campaignhistory.CampaignHistoryService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.easydb.EasyDBReadDAO;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.customercare.entity.ReferralEntity;
import com.freecharge.customercare.service.CustomerTrailService;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freebill.service.BillPostpaidValidation;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.dos.entity.ReIssuedPromocode;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.freefund.services.ReIssuedPromocodesService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillTransactionLedger;
import com.freecharge.merchantview.entities.ShopoView;
import com.freecharge.merchantview.entities.SplitView;
import com.freecharge.merchantview.service.MerchantViewService;
import com.freecharge.mongo.repos.PromocodeFailMsgRepository;
import com.freecharge.mongo.repos.PromocodeUsageRepository;
import com.freecharge.mongo.repos.UserRepository;
import com.freecharge.payment.dos.entity.PaymentGatewayMaster;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.RechargeStatus;
import com.freecharge.recharge.RechargeStatusService;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.tsm.service.TSMService;
import com.freecharge.views.GlobalTransactionView;
import com.freecharge.views.P2PAndP2MView;
import com.freecharge.views.PaidCouponsTxnView;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletItemsDAO;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletItems;
import com.freecharge.wallet.service.WalletTransaction;
import com.freecharge.web.service.PromocodeCampaignService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.mongodb.DBObject;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.sdmoney.service.model.Balance;
import com.freecharge.common.comm.google.FetchGoogleUTMService;
import com.freecharge.common.comm.google.FetchUTMKey;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 22/10/12 Time: 10:09 AM To
 * change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/customertrail/*")
public class CustomerTrailController extends BaseAdminController {
	private final Logger logger = LoggingFactory.getLogger(CustomerTrailController.class);

	public static final String ORDER_ID_PARAM_NAME = "orderId";
	public static final String LOOKUP_ID_PARAM_NAME = "lookupid";
	public static final String BBPS_REFERENCE_ID = "bbpsReferenceId";
	public static final String GOOGLE_REFERENCE_ID = "googleReferenceId";
	public static final String PROMO_CODE_PARAM_NAME = "promocode";
	public static final String RESET_FREEFUND_CODE_PARAM_NAME = "freefundcode";
	public static final String CUSTOMER_TRAIL_LINK = "/admin/customertrail/home.htm";
	public static final String CUSTOMER_TRAIL_RESULT_LINK = "/admin/customertrail/getcustomertrail.htm";
	public static final String METRIC_SERVICE_RESET = "reset";
	private static final String METRIC_NAME = "resetPromocode";
	private static final String PROMOCODE_ORDERID = "order_id";
	private static final String PROMOCODE = "promocode";
	private static final String PROMOCODE_FAILED_MSG = "failedMsg";
	private static final String CALLED_FROM = "calledFrom";
	private static final String EVENTS_LIST = "eventsList";
	private static final String STRING_PARAMS_PAGE_NAME = "pageName";
	private static final String STRING_PAGE_NAME = "Customer Trail";
	private static final String COUPON_FETCH_FAIL_MSG = "Failed to fetch Coupons opted, please try again "
			+ "after some time!";
	public static final String TXN_ID_PARAM_NAME = "transactionId";
	public static final String TXN_TYPE_PARAM_NAME = "transactionType";
	public static final String TXN_MOBILENO_PARAM_NAME = "mobileNo";
	public static final String UPIID_PARAM_NAME = "upiId";

	@Autowired
	private InService inService;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private EasyDBWriteDAO easyDBWriteDAO;

	@Autowired
	private EasyDBReadDAO easyDBReadDAO;

	@Autowired
	private CouponService couponService;

	@Autowired
	public LanguageUtility languageUtility;

	@Autowired
	OrderIdSlaveDAO orderidDao;

	@Autowired
	CrosssellService crosssellService;

	@Autowired
	FreefundService freefundService;

	@Autowired
	private CartService cartService;

	@Autowired
	private CartItemsDAO cartItemsDAO;

	@Autowired
	private CommonService commonService;

	@Autowired
	private PaymentPlanService ppService;

	@Autowired
	WalletItemsDAO walletItemsDao;

	@Autowired
	TxnCrossSellService txnCrossSellService;

	@Autowired
	WalletService walletService;

	@Autowired
	private CouponReissueService couponReissueService;

	@Autowired
	private BinOfferService binOfferService;

	@Autowired
	private VoucherService voucherService;

	@Autowired
	private FreefundService freeFundService = new FreefundService();

	@Autowired
	private BillPaymentService billPaymentService;

	@Autowired
	private MetricsClient metricsClient;

	@Autowired
	private MyAccountService myAccountService;

	@Autowired
	private PersonalDetailsService personalDetailsService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TxnHomePageService txnHomePageService;

	@Autowired
	private CrossSellHistoryService crossSellHistoryService;

	@Autowired
	private PromocodeFailMsgRepository promocodeFailMsgRepository;

	@Autowired
	private RechargeStatusService reshargeStatusService;

	@Autowired
	private OrderIdReadDAO orderIdDAO;

	@Autowired
	private FulfillmentScheduleService scheduleService;

	@Autowired
	private BillPostpaidValidation billPostpaidValidation;

	@Autowired
	private UserTransactionHistoryDAO userTransactionHistoryDAO;

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private CTActionControlService ctActionControlService;

	@Autowired
	private ReIssuedPromocodesService reIssuedPromocodesService;

	@Autowired
	private PromocodeUsageRepository promocodeUsageRepository;

	@Autowired
	private PromocodeCampaignService promocodeCampaignService;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	private OrderIdReadDAO orderIdReadDAO;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private AdminHelper adminHelper;

	@Autowired
	private TSMService tSMService;

	@Autowired
	private MerchantViewService merchantViewService;

	@Autowired
	private CustomerTrailService customerTrailService;

	@Autowired
	private CampaignHistoryService campaignHistoryService;

	@Autowired
	private UPITransactionService upiTransactionService;

	@Autowired
	private BbpsTransactionDAO bbpsTransactionDao;
	
	@Autowired
	private FetchGoogleUTMService utmService;

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String home(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}
		model.addAttribute(STRING_PARAMS_PAGE_NAME, STRING_PAGE_NAME);
		return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
	}

	@RequestMapping(value = "getcustomertrail")
	public String getCustomerTrail(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}

		if (isAuthorizedToUpdateCt(request)) {
			model.addAttribute("isAuthorize", "yes");
		} else {
			model.addAttribute("isAuthorize", "no");
		}

		Map<String, String> productTypeMap = getProductTypeMap();
		model.addAttribute("productTypeMap", productTypeMap);
		/* Customer trail Action status */
		String calledFrom = null;
		if (mapping.get(CustomerTrailController.CALLED_FROM) != null) {
			calledFrom = mapping.get(CustomerTrailController.CALLED_FROM).trim();
		}

		if (calledFrom == null || !(calledFrom.equals("customerTrail"))) {
			if (!ctActionControlService.hasSufficientViewLimits(getCurrentAdminUser(request))) {
				List<String> messages = new LinkedList<String>();
				messages.add(getCurrentAdminUser(request)
						+ " has crossed view limits(or exception while checking available view limits), please contact manager.");
				model.addAttribute("messages", messages);
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}
		}

		try {
			String orderIdPramValue = null;
			String lookUpIdParamValue = null;
			String bbpsReferenceId = null;
			String googleReferenceId =null;
			

			if (mapping.get(CustomerTrailController.ORDER_ID_PARAM_NAME) != null) {
				orderIdPramValue = mapping.get(CustomerTrailController.ORDER_ID_PARAM_NAME).trim();
			}
			
			if (mapping.get(CustomerTrailController.LOOKUP_ID_PARAM_NAME) != null) {
				lookUpIdParamValue = mapping.get(CustomerTrailController.LOOKUP_ID_PARAM_NAME).trim();
			}

			/* Customer trail Action status */
			List<String> messages = new LinkedList<String>();
			if (mapping.get("manualRefundStatus") != null) {
				messages.add(mapping.get("manualRefundStatus"));
			}
			/* End of action status */
			if (lookUpIdParamValue != null) {
				String orderIdParameter = orderidDao.getOrderIdForLookUpId(lookUpIdParamValue);
				orderIdPramValue = orderIdParameter;
			}

			if (mapping.get(CustomerTrailController.BBPS_REFERENCE_ID) != null) {
				bbpsReferenceId = mapping.get(CustomerTrailController.BBPS_REFERENCE_ID).trim();
			}
			
			if(mapping.get(CustomerTrailController.GOOGLE_REFERENCE_ID) != null)
				googleReferenceId=mapping.get(CustomerTrailController.GOOGLE_REFERENCE_ID).trim();
			
			if(bbpsReferenceId != null && orderIdPramValue == null) {
				logger.info("BBPS REFERENCE ID = "+bbpsReferenceId);
				BbpsTransactionDetails transactionDetails = new BbpsTransactionDetails();
				transactionDetails = bbpsTransactionDao.getTransactionDetailsFromBbpsReferenceNumber(bbpsReferenceId);
				orderIdPramValue = transactionDetails.getOrderId();
				model.addAttribute("bbpsTransactionDetail", transactionDetails);
			}
			
			if(bbpsReferenceId == null && orderIdPramValue != null) {
				logger.info("ORDER ID = "+orderIdPramValue);
				BbpsTransactionDetails transactionDetails = new BbpsTransactionDetails();
				transactionDetails = bbpsTransactionDao.getBbpsReferenceNumberFromOrderId(orderIdPramValue);
				if(transactionDetails.getBbpsReferenceId() != null) {
					logger.info("BBPS REFERENCE ID FOR HIT BY ORDER ID WHICH IS BBPS ENABLED"+transactionDetails.getBbpsReferenceId());
					model.addAttribute("bbpsTransactionDetail", transactionDetails);
					
				}
			}
			
			if(googleReferenceId != null && orderIdPramValue == null ) {
				logger.info("Google Refernce ID = "+googleReferenceId);
				/* Google Recharge Info */
				Map<String, String> googleRechargeMap = new HashMap<String, String>();
				try {
					googleRechargeMap = utmService.fetchOrderAttributes(FetchUTMKey.REF_ID,googleReferenceId);
				logger.info("fetched result from utmservice");
				} catch(Exception e) {
					logger.error("Error in fetching source and reference from UTM service");
				}
				if(!CollectionUtils.isEmpty(googleRechargeMap)) {
				orderIdPramValue=googleRechargeMap.get("order_id").trim();	
				logger.info("OrderId fetched from UTM service is : "+orderIdPramValue);
				}
			}

			// if orderIdPramValue is still null, return to the Home View Page
			if (orderIdPramValue == null) {
				messages.add("Kindly give Non-Null order id to process.");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}

			OrderId orderId = new OrderId();
			String msg = null;
			orderId.setOrderId(orderIdPramValue);
			List<OrderId> orderIds = easyDBWriteDAO.get(orderId);
			model.addAttribute("messages", messages);

			if (orderIds.size() > 1) {
				messages.add(
						String.format("More than one order found for order id %s, contact the dev team immediately.",
								orderIdPramValue));
			} else if (orderIds.size() == 0) {
				// Non FC Order Ids
				// Check if this order id corresponds to Wallet Integrations assuming order id
				// entered is a global transaction id!

				// call TSM Service
				List<GlobalTransactionView> globalTransactionViewList = tSMService
						.getCompleteTransactionView(orderIdPramValue);

				if (globalTransactionViewList.size() != 0) {
					model.addAttribute("globalTransactionViewList", globalTransactionViewList);
					// integrating campaign history with TSM
					RewardHistoryWrapper rewardHistoryWrapper = customerTrailService
							.getRewardHistoryListFromOrderId(orderIdPramValue);
					model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
					logger.info("Reward History List received for TSM: " + rewardHistoryWrapper);
					return ViewConstants.SD_CUSTOMER_TRAIL_LISTING;
				}

				// Look for P2P & P2M transactions
				List<P2PAndP2MView> p2pAndP2MViewList = merchantViewService.getP2PAndP2MByOrderId(orderIdPramValue);
				if (p2pAndP2MViewList != null && p2pAndP2MViewList.size() > 0) {
					model.addAttribute("p2pAndP2MViewList", p2pAndP2MViewList);

					// integrating campaign history with RV
					RewardHistoryWrapper rewardHistoryWrapper = customerTrailService
							.getRewardHistoryListFromOrderId(orderIdPramValue);
					model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
					logger.info("Reward History List received for RV: " + rewardHistoryWrapper);
					return ViewConstants.P2P_P2M_ORDER_ID_VIEW;
				}
				// Look for Split transactions

				SplitView splitView = merchantViewService.getSplitDetailsByOrderId(orderIdPramValue);
				if (splitView != null) {
					model.addAttribute("splitView", splitView);

					// integrating reward history
					RewardHistoryWrapper rewardHistoryWrapper = customerTrailService
							.getRewardHistoryListFromOrderId(orderIdPramValue);
					model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
					logger.info("Reward History List received for Split: " + rewardHistoryWrapper);

					// return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
					return ViewConstants.SPLIT_BY_ORDER_ID_VIEW;
				}

				// Look for SHOPO Transactions
				List<ShopoView> shopoViewList = tSMService.getShopoTransactionDetailsByOrderId(orderIdPramValue);
				logger.info("SHOPO set of transactions received from TSM are -> " + shopoViewList);
				if (shopoViewList != null && !shopoViewList.isEmpty()) {
					model.addAttribute("shopoViewList", shopoViewList);

					// integrating campaign history with Shopo
					RewardHistoryWrapper rewardHistoryWrapper = customerTrailService
							.getRewardHistoryListFromOrderId(orderIdPramValue);
					model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
					logger.info("Reward History List received for Shopo: " + rewardHistoryWrapper);
					return ViewConstants.SHOPO_ORDER_ID_VIEW;
				}

				// No transaction found anywhere corresponding to this id
				messages.add("Sorry, No transaction exists for the given orderid.");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}

			List<Map<String, Object>> ListOfMapOfReissueCouponObj = couponReissueService.getAllAvailableCouponReissue();
			List<String> listOfCouponVal = new ArrayList<String>();
			if (ListOfMapOfReissueCouponObj != null && !ListOfMapOfReissueCouponObj.isEmpty()) {
				for (Map<String, Object> listOfMap : ListOfMapOfReissueCouponObj) {
					Integer coupinId = (Integer) listOfMap.get("coupon_store_id");
					String campaignName = (String) listOfMap.get("campaign_name");
					Integer couponValue = (Integer) (listOfMap.get("coupon_value"));
					String couponType = (String) listOfMap.get("coupon_type");
					String couponInfo = campaignName + " * Rs." + couponValue + " *" + couponType + "*" + coupinId;
					listOfCouponVal.add(couponInfo);
				}
			}
			orderId = FCUtil.getFirstElement(orderIds);
			if (orderId == null) {
				msg = "OrderId is null";
				messages.add(msg);
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}

			model.addAttribute("order", orderId);
			model.addAttribute("orderidentered", orderIdPramValue);
			model.addAttribute("lookupIdentered", lookUpIdParamValue);
			model.addAttribute("reissueCouponObjList", listOfCouponVal);
			
			/* Google Recharge Info */
			Map<String, String> googleRechargeDetailMap = new HashMap<String, String>();
			Map<String, String> result =new HashMap<String, String>();
			try {
			result = utmService.fetchOrderAttributes(FetchUTMKey.ORDER_ID,orderId.getOrderId());
			logger.info("fetched result from utmservice");
			} catch(Exception e) {
				logger.error("Error in fetching source and reference from UTM service");
			}
		
			
			if(!CollectionUtils.isEmpty(result)) {
				logger.info("result ### : "+result);
				String ReferenceId = result.get("utm_content").trim();
				String Source = result.get("utm_source").trim();
	            if(StringUtils.isEmpty(ReferenceId) || StringUtils.isEmpty(Source)) {
	            	logger.info("Reference id or Source is empty....."+"utm content----->"+ReferenceId+"utm_source----->"+Source);
	          	
	            }
	            else {
	            	logger.info("Reference Id-->"+ReferenceId+"   Source---->"+Source);
	            	googleRechargeDetailMap.put("ReferenceId", ReferenceId);
	            	googleRechargeDetailMap.put("Source", Source);
	            }
	          
			if(!googleRechargeDetailMap.isEmpty())
			model.addAttribute("googleRechargeDetailMap", googleRechargeDetailMap);
			else 
				logger.info("No data Received");
			}

			/* Channel with channel-Id with */
			String channelName = FCUtil.getChannelTypeAsDescriptor(String.valueOf(orderId.getChannelId()));
			model.addAttribute("channelName", channelName + " (" + orderId.getChannelId() + ")");

			PaymentTransaction paymentTransaction = paymentTransactionService
					.getLastPaymentTransactionSentToPG(orderId.getOrderId());
			if (paymentTransaction != null) {
				model.addAttribute("paymentTransaction", paymentTransaction);
			}

			List<PaymentTransaction> allPaymentTransaction = paymentTransactionService
					.getAllPaymentTransactionSentToPG(orderId.getOrderId());
			if (allPaymentTransaction != null) {
				for (PaymentTransaction successfullPaymentTransaction : allPaymentTransaction) {
					if (successfullPaymentTransaction.getIsSuccessful())
						model.addAttribute("paymentTransaction", successfullPaymentTransaction);
				}
				model.addAttribute("allPaymentTransaction", allPaymentTransaction);
			}

			// PaymentGatewayMaster for displaying name of payment gateway
			// master
			// this code will take care of the case where both ag and pg are null

			TransactionHomePage transactionHomePage = new TransactionHomePage();
			transactionHomePage.setLookupId(orderId.getLookupId());
			List<TransactionHomePage> transactionHomePages = easyDBWriteDAO.get(transactionHomePage);
			if (transactionHomePages.size() > 1) {
				messages.add(String.format(
						"More than one transaction home page details found for order id %s, contact the dev team immediately.",
						orderIdPramValue));
			}

			transactionHomePage = FCUtil.getFirstElement(transactionHomePages);

			if (transactionHomePage == null) {
				transactionHomePage = new TransactionHomePage();
			}
			UserTransactionHistory userTransactionHistory = userTransactionHistoryService
					.findUserTransactionHistoryByOrderId(orderId.getOrderId());
			if (userTransactionHistory == null) {
				userTransactionHistory = new UserTransactionHistory();
				userTransactionHistory.setProductType(transactionHomePage.getProductType());
				userTransactionHistory.setServiceProvider(transactionHomePage.getOperatorName());
				userTransactionHistory.setServiceRegion(transactionHomePage.getCircleName());
				userTransactionHistory.setOrderId(orderId.getOrderId());
			}
			model.addAttribute("userTransaction", userTransactionHistory);
			boolean forcedRefund = false;
			if (userTransactionHistory.getTransactionStatus()
					.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND)) {
				forcedRefund = true;
			}
			model.addAttribute("forcedRefund", forcedRefund);
			model.addAttribute("transactionHomePage", transactionHomePage);
			if (FCUtil.isRechargeProductType(userTransactionHistory.getProductType())) {
				model.addAttribute("finalProductType", userTransactionHistory.getProductType());
				setIntransactionData(orderId, model, userTransactionHistory.getProductType());
			} else if (FCUtil.isBillPostpaidType(userTransactionHistory.getProductType())) {
				model.addAttribute("finalProductType", userTransactionHistory.getProductType());
				/* Getting billpayment postpaid operator */
				BillPaymentStatus billPaymentStatus = billPaymentService
						.findBillPaymentStatusByOrderId(orderId.getOrderId());
				model.addAttribute("billPaymentStatus", billPaymentStatus);
				if (billPaymentStatus != null) {
					List<TxnHomePage> TxnHomePageList = txnHomePageService
							.findTxnHomePageByLookupId(orderId.getLookupId());
					if (TxnHomePageList.size() > 1) {
						messages.add(String.format(
								"More than one txnhomepage details found for order id %s, contact the dev team immediately.",
								orderIdPramValue));
					}

					TxnHomePage txnHomePage = FCUtil.getFirstElement(TxnHomePageList);
					BillTxnHomePage billTxnHomePage = txnHomePage.getBillTxnHomePage();

					/*
					 * flow will go inside if condition in the case when we are retrying prepaid
					 * failure to postpaid again
					 */

					if (billTxnHomePage == null) {

						BillPaymentCommonValidationApiResponse billResponse = billPostpaidValidation.validate(
								String.valueOf(billPaymentStatus.getFk_operator_master_id()),
								billPaymentStatus.getPostpaidNumber(), null, billPaymentStatus.getAmount());
						billTxnHomePage = BillTxnHomePage.createForMobilePostpaid(billPaymentStatus.getPostpaidNumber(),
								billResponse, billPaymentStatus.getOperatorName());
						txnHomePage.setBillTxnHomePage(billTxnHomePage);
					}
					model.addAttribute("postpaidMerchantName", userTransactionHistory.getServiceProvider());

					BillPaymentAttempt billPaymentAttempt = billPaymentService
							.findBillPaymentAttemptByStatusId(billPaymentStatus.getId());
					model.addAttribute("billPaymentAttempt", billPaymentAttempt);
					setIntransactionData(orderId, model, userTransactionHistory.getProductType());
					if (RechargeConstants.POSTPAID_REVERSAL_AUTH_STATUS.equals(billPaymentStatus.getAuthStatus())
							|| RechargeConstants.POSTPAID_SUCCESS_REVERSAL_AUTH_STATUS
									.equals(billPaymentStatus.getAuthStatus())) {
						logger.info("Reversed through ct reversal" + billPaymentStatus.getAffiliateTransID());
						model.addAttribute("isPostpaidReversalCase", "reversalCase");
					}
				}
			} else if (FCUtil.isUtilityPaymentType((userTransactionHistory.getProductType()))) {
				String billId = orderidDao.getBillIdForOrderId(orderId.getOrderId(),
						userTransactionHistory.getProductType());

				BillTransactionLedger billTransactionLedger = null;
				if (billId != null) {
					List<BillTransactionLedger> billDetails = billPayServiceRemote.getBillTransactionLedgers(billId);
					if (billDetails != null) {
						billTransactionLedger = billDetails.get(0);
					}
				}
				model.addAttribute("billDetails", billTransactionLedger);
				model.addAttribute("finalProductType", userTransactionHistory.getProductType());
				model.addAttribute("isUtilitiesPayment", true);
			}

			TransactionFulfilment transactionFulfilment = new TransactionFulfilment();
			if ((transactionHomePage != null) && (transactionHomePage.getId() != null)) {
				transactionFulfilment.setTxnHomePageId(transactionHomePage.getId());
				List<TransactionFulfilment> transactionFulfilments = easyDBWriteDAO.get(transactionFulfilment);
				transactionFulfilment = FCUtil.getFirstElement(transactionFulfilments);

				if (transactionFulfilments.size() > 1) {
					messages.add(String.format(
							"More than one transaction fulfilment details found for order id %s, contact the dev team immediately.",
							orderIdPramValue));
				}
			}
			model.addAttribute("transactionFulfilment", transactionFulfilment);

			CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId.getOrderId());

			/* For getting user registered address */
			UserProfile userProfile = null;
			Users user = null;
			logger.info("Cart Business Do response for order id param " + orderIdPramValue + " is "
					+ cartToInspect.toString());
			if (cartToInspect.getUserId() != null) {
				userProfile = userServiceProxy.getUserProfile(cartToInspect.getUserId());
				if ((userProfile != null) && (userProfile.getFkUserId() != null)) {
					user = userServiceProxy.getUserByUserId(userProfile.getFkUserId());
				} else {
					user = userServiceProxy.getUserByUserId(cartToInspect.getUserId());
				}
				if (user != null) {
					try {
						String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
						if (fcWalletId != null) {
							Balance balance = oneCheckWalletService.getFCWalletAccountBalance(fcWalletId);
							GetUserResponse getUserResponse = oneCheckWalletService.getUserByID(fcWalletId, null, null);

							if (null != getUserResponse && null != getUserResponse.getUserDetails()) {
								user.setMobileNo(getUserResponse.getUserDetails().getMobileNumber());
							}
							model.addAttribute("balance", balance);
							if (balance != null) {
								model.addAttribute("walletbalance", balance.getTotalBalance());
							}
						} else {
							model.addAttribute("walletbalance",
									walletService.findBalance(cartToInspect.getUserId()).getAmount());
						}
					} catch (Exception e) {
						logger.error(
								"Exception when fetching sdIdentity and/or wallet balance of user: " + user.getEmail());
					}
				}
				logger.info("User is not found for order value param : " + orderIdPramValue);
			}
			model.addAttribute("user", user);
			model.addAttribute("userProfile", userProfile);

			CartBusinessDo cartBusinessDo = new CartBusinessDo();
			if (transactionHomePage.getLookupId() == null) {
				cartBusinessDo = cartService.getCartByOrderId(orderIdPramValue);
			} else {
				cartBusinessDo = cartService.getCartWithNoExpiryCheck(transactionHomePage.getLookupId());
			}
			model.addAttribute("cart", cartBusinessDo);

			Boolean isPaidCouponsSelected = checkPaidCouponSelected(cartBusinessDo);
			int paidCouponsTotalPrice = 0;
			if (isPaidCouponsSelected) {
				paidCouponsTotalPrice = getTotalPaidCouponsPriceInCart(cartBusinessDo);
				model.addAttribute("isPaidCouponsSelected", isPaidCouponsSelected);
				model.addAttribute("paidCouponTotalPrice", paidCouponsTotalPrice);
			}

			List<CartItemsBusinessDO> cartItemList = cartBusinessDo.getCartItems();
			for (CartItemsBusinessDO cartItemBusinessDo : cartItemList) {
				if (cartBusinessDo != null && cartItemBusinessDo.getProductMasterId() == 8
						&& cartItemBusinessDo.getDeleted() == false) {
					FreefundCoupon freefundCoupon = freefundService
							.getFreefundCoupon((long) cartItemBusinessDo.getItemId());
					if (freefundCoupon != null) {
						FreefundClass freefundClass = freefundService
								.getFreefundClass(freefundCoupon.getFreefundCode());
						model.addAttribute("freefundClass", freefundClass);
						List<String> orderidlist = orderIdReadDAO
								.getOrderIdListForCoupon(freefundCoupon.getFreefundCode());
						if (orderidlist != null && orderidlist.size() >= 1) {
							String promoOrderId = orderidlist.get(0);
							model.addAttribute("promoOrderId", orderidlist);
						}
						model.addAttribute("freefundCoupon", freefundCoupon);
					}
				}
			}

			List<PaymentRefundTransaction> paymentRefundTransactions = this.paymentTransactionService
					.getPaymentRefundTransactions(orderIdPramValue);
			model.addAttribute("refunds", paymentRefundTransactions);

			List<CouponStore> couponsSelected = null;

			if (cartBusinessDo != null) {
				List<CouponStore> couponStores = couponService.getAllCouponStores(cartBusinessDo);
				Map<Integer, String> codesMap = new HashMap<Integer, String>();
				Map<Integer, Integer> quantityMap = new HashMap<Integer, Integer>();
				List<RedemptionInfo> redeemInfo = new ArrayList<RedemptionInfo>();
				List<CouponStore> couponsFulFilled = new ArrayList<CouponStore>();
				Map<String, Object> codeRedeemInfoMap = null;
				/* Map for total price for a coupon */
				Map<Integer, Integer> paidCouponPriceMap = new HashMap<Integer, Integer>();

				for (CouponStore couponStore : couponStores) {
					if (couponStore != null) {
						Integer txnHomePageId = transactionHomePage.getId();
						Integer couponQuantity = null;
						/*
						 * Date after which coupon_store_id column is added in optin history table.Data
						 * after 23-dec-2013 we are picking user selected coupons from coupon optin
						 * tables.
						 */
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date storeIdEnteredDate = formatter.parse("2013-12-24 00:00:00");

						if (cartBusinessDo.getCreatedOn().compareTo(storeIdEnteredDate) > 0) {
							List<CouponHistory> couponHistoryList = new ArrayList<CouponHistory>();
							List<CrossSellHistory> crosssellHistoryList = new ArrayList<CrossSellHistory>();

							if (CouponNature.fromDbString(couponStore.getCouponNature()) == CouponNature.CrossSell) {
								crosssellHistoryList = crossSellHistoryService
										.getCrossSellOptinHistory(orderId.getOrderId(), couponStore.getCouponStoreId());
							} else {
								couponHistoryList = couponServiceProxy.getCouponsOptinHistory(orderId.getOrderId(),
										couponStore.getCouponStoreId());
							}

							if ((couponHistoryList == null || couponHistoryList.isEmpty())
									&& (crosssellHistoryList == null || crosssellHistoryList.isEmpty())) {
								continue;
							}
							couponsFulFilled.add(couponStore);
							if (couponHistoryList != null && (!couponHistoryList.isEmpty())) {
								couponQuantity = couponHistoryList.size();
								codeRedeemInfoMap = getNewCouponCodeRedeemInfoMap(couponStore, orderId.getOrderId());
								codesMap.putAll((Map<Integer, String>) codeRedeemInfoMap.get("codesMap"));
								redeemInfo.addAll((List<RedemptionInfo>) codeRedeemInfoMap.get("redeemInfo"));
							} else {
								couponQuantity = crosssellHistoryList.size();
								codeRedeemInfoMap = getNewCrosssellCodeRedeemInfoMap(couponStore, orderId.getOrderId());
								codesMap.putAll((Map<Integer, String>) codeRedeemInfoMap.get("codesMap"));
								redeemInfo.addAll((List<RedemptionInfo>) codeRedeemInfoMap.get("redeemInfo"));
							}
						} else {/* Code before date dec-24-2013 */
							List<TxnCrossSell> crossSellList = getTxnCrosssellList(txnHomePageId, couponStore);
							if (crossSellList == null || crossSellList.isEmpty()) {
								continue;
							}
							couponsFulFilled.add(couponStore);
							couponQuantity = crossSellList.get(0).getQuantity();
							codeRedeemInfoMap = getCodeRedeemInfoMap(couponStore, orderId.getOrderId());
							codesMap.putAll((Map<Integer, String>) codeRedeemInfoMap.get("codesMap"));
							redeemInfo.addAll((List<RedemptionInfo>) codeRedeemInfoMap.get("redeemInfo"));
						}
						quantityMap.put(couponStore.getCouponStoreId(), couponQuantity);
						paidCouponPriceMap.put(couponStore.getCouponStoreId(), couponStore.getPrice() * couponQuantity);
					}
				}

				if (couponsFulFilled != null && !couponsFulFilled.isEmpty()) {
					for (CouponStore couponStore : couponsFulFilled) {
						if (couponStore.getCouponType().equals("C")) {
							List<CartItemsBusinessDO> CartItemsBusinessDO = cartBusinessDo.getCartItems();
							if (CartItemsBusinessDO != null && !CartItemsBusinessDO.isEmpty()) {
								for (CartItemsBusinessDO cartItemBusinessDO : CartItemsBusinessDO) {
									Integer value = cartItemBusinessDO.getPrice().intValue();
									if (couponStore.getCampaignName()
											.equalsIgnoreCase(cartItemBusinessDO.getDisplayLabel())) {
										couponStore.setCouponValue(value);
										// couponStore.setCouponStoreId(1);
									}
								}
							}
						}
					}
				}
				model.addAttribute("mcoupons", redeemInfo);
				model.addAttribute("codesMap", codesMap);
				model.addAttribute("coupons", couponsFulFilled);
				model.addAttribute("quantityMap", quantityMap);
				model.addAttribute("paidCouponPriceMap", paidCouponPriceMap);
				couponsSelected = couponsFulFilled;
			}

			OrderFulfillmentDetails orderFulfillmentDetails = new OrderFulfillmentDetails();
			orderFulfillmentDetails.setOrderId(orderId.getOrderId());
			List<OrderFulfillmentDetails> orderFulfillmentDetailsList = easyDBWriteDAO.get(orderFulfillmentDetails);
			if (orderFulfillmentDetailsList != null) {
				model.addAttribute("orderFulfillmentDetailsList", orderFulfillmentDetailsList);
			}

			/* Reissued coupon details */
			List<Map<String, Object>> reissuedCouponsMapList = voucherService
					.getReissuedCouponMaps(orderId.getOrderId());
			List<Map<String, Object>> reissuedCouponsDataMapList = new ArrayList<Map<String, Object>>();
			SimpleDateFormat noMilliSecondsFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (reissuedCouponsMapList != null && reissuedCouponsMapList.size() > 0) {
				for (Map<String, Object> couponMap : reissuedCouponsMapList) {
					Map<String, Object> reissuedCouponsMap = new HashMap<String, Object>();
					CouponStore couponStore = new CouponStore();
					reissuedCouponsMap.put("couponId", couponMap.get("reissued_coupon_id"));
					reissuedCouponsMap.put("couponQuantity", couponMap.get("reissued_coupon_quantity"));
					reissuedCouponsMap.put("createdDate",
							(noMilliSecondsFormatter.format((Timestamp) couponMap.get("created_on"))));
					couponStore = couponService.getCouponStore((Integer) couponMap.get("reissued_coupon_id"));
					if (couponStore != null) {
						reissuedCouponsMap.put("campaigName", couponStore.getCampaignName());
						reissuedCouponsMap.put("couponValue", couponStore.getCouponValue());
						reissuedCouponsMap.put("couponType", couponStore.getCouponType());
					}
					reissuedCouponsDataMapList.add(reissuedCouponsMap);
				}
			}
			try {
				List<com.freecharge.api.coupon.service.model.CouponHistory> couponHistory = couponServiceProxy
						.getCouponHistoryList(Arrays.asList(orderIdPramValue));
				model.addAttribute("couponOptinHistory", couponHistory);
			} catch (Exception e) {
				logger.error("Caught exception while fetching coupons opted for order id " + orderId.getOrderId(), e);
				model.addAttribute("couponOptError", COUPON_FETCH_FAIL_MSG);
			}
			model.addAttribute("reissuedCouponsMapList", reissuedCouponsDataMapList);
			/* End of Reissued coupon details */

			/* Get PG_TYPE map against mtxnId */
			model.addAttribute("pgTypeMap", paymentTransactionService.getMtxnIdPGTypeMap(orderId.getOrderId()));

			PaymentTransaction paymentTransactionAttempt = new PaymentTransaction();
			paymentTransactionAttempt.setOrderId(orderId.getOrderId());
			List<PaymentTransaction> paymentTransactionAttemptList = paymentTransactionService
					.getAllPaymentTransactionByAcsOrder(orderId.getOrderId());
			Calendar currentCal = Calendar.getInstance();
			currentCal.add(Calendar.HOUR, -2);
			Date twoHoursPrior = currentCal.getTime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			String twoHours = simpleDateFormat.format(twoHoursPrior);
			model.addAttribute("manualRefundCutoffTime", twoHours);

			// /////////
			// List<PaymentTransaction> paymentTransactionAttemptList =
			// easyDBWriteDAO.get(paymentTransactionAttempt);
			List<WalletTransaction> walletTransationResult = walletService
					.findTransactionTypeByOrderId(orderId.getOrderId());
			model.addAttribute("walletTransactionList", walletTransationResult);
			if (paymentTransactionAttemptList != null) {
				model.addAttribute("paymentTransactionAttemptList", paymentTransactionAttemptList);
			}

			PaymentTypeMaster paymentTypeMaster = new PaymentTypeMaster();
			if (paymentTransactionAttemptList != null && !paymentTransactionAttemptList.isEmpty()) {
				paymentTypeMaster.setPaymentTypeMasterId(paymentTransactionAttemptList.get(0).getPaymentType());
				List<PaymentTypeMaster> paymentTypeMasters = easyDBWriteDAO.get(paymentTypeMaster);
				paymentTypeMaster = FCUtil.getFirstElement(paymentTypeMasters);
			}
			model.addAttribute("paymentTypeMaster", paymentTypeMaster);

			PaymentTypeOptions paymentTypeOptions = new PaymentTypeOptions();
			if ((paymentTransactionAttemptList != null)
					&& (paymentTransactionAttemptList.get(0).getPaymentOption() != null)) {
				paymentTypeOptions.setCode(paymentTransactionAttemptList.get(0).getPaymentOption());
				List<PaymentTypeOptions> paymentTypeOptionsList = easyDBWriteDAO.get(paymentTypeOptions);
				paymentTypeOptions = FCUtil.getFirstElement(paymentTypeOptionsList);
			}
			model.addAttribute("paymentTypeOption", paymentTypeOptions);

			PaymentGatewayMaster paymentGatewayMaster = new PaymentGatewayMaster();
			if (paymentTransactionAttemptList != null && !paymentTransactionAttemptList.isEmpty()) {
				// paymentTransactionService.getPaymentGatewayMasterForCode(paymentTransactionAttemptList.get(0).getPaymentGateway());
				paymentGatewayMaster.setName(paymentTransactionAttemptList.get(0).getPaymentGateway());
				model.addAttribute("paymentGatewayMaster", paymentGatewayMaster);
			}

			Map<String, CityMaster> cityMap = commonService.getCitiesAsMap();
			model.addAttribute(WebConstants.ATTR_CITY_MAP, cityMap);
			Map<Integer, com.freecharge.app.domain.entity.StateMaster> stateMap = commonService.getStateMasterMap();
			model.addAttribute(WebConstants.ATTR_STATE_MAP, stateMap);

			try {
				PaymentPlan paymentPlan = ppService.getPaymentPlan(orderIdPramValue);
				model.addAttribute("paymentPlan", paymentPlan);
			} catch (PaymentPlanNotFoundException ppnfe) {
				logger.info("Payment plan not found for order ID: [" + orderIdPramValue + "]");
			}

			// -----Start of getting all special-deals------------
			String lookupId = orderidDao.getLookupIdForOrderId(orderId.getOrderId());
			Cart cart = cartService.getCartByLookUpNoExpiryCheck(lookupId);
			List<CartItems> cartItemsList = cartService.getCartItemsByCartId(cart.getCartId());

			// Getting Binoffer instant
			List<BinOfferData> binOfferInstantList = new ArrayList<BinOfferData>();
			for (CartItems cartItems : cartItemsList) {
				if (FCConstants.ENTITY_NAME_FOR_FREEFUND.equals(cartItems.getEntityId())
						&& cartItems.getFlProductMasterId() == 9) {
					BinOffer binOffer = new BinOffer();
					binOffer = binOfferService.getBinOffer(cartItems.getFkItemId());
					List<WalletTransaction> walletTransactionList = walletService.getWalletTxn(orderId.getOrderId(),
							Wallet.FundSource.RECHARGE_FUND.toString());
					Double transactionAmount = null;
					String status = null;

					if (walletTransactionList != null && walletTransactionList.size() > 0) {
						WalletTransaction walletTransaction = walletTransactionList.get(0);
						if (walletTransaction != null) {
							transactionAmount = walletTransaction.getTransactionAmount();
							status = walletTransaction.getTransactionType();
						}
					}

					BinOfferData binOfferData = new BinOfferData();
					if (binOffer != null) {
						binOfferData.setBinOffer(binOffer);
						binOfferData.setAmount(String.valueOf(transactionAmount));
						binOfferData.setStatus(status);
						binOfferInstantList.add(binOfferData);
					}
					break;
				}
			}

			// Getting Bin based cash back
			List<BinOfferData> binOfferCashBackList = new ArrayList<BinOfferData>();
			List<WalletTransaction> walletTransactionList = walletService.getWalletTxn(orderId.getOrderId(),
					Wallet.FundSource.CASHBACK.toString());
			if (walletTransactionList != null) {
				for (WalletTransaction walletTransaction : walletTransactionList) {
					WalletItems walletItems = walletService.getWalletItems(walletTransaction.getWalletTransactionId());
					if (walletItems != null) {
						BinOffer binOffer = binOfferService.getBinOffer(walletItems.getfkItemId());
						BinOfferData binOfferData = new BinOfferData();
						if (binOffer != null) {
							binOfferData.setBinOffer(binOffer);
							binOfferData.setAmount(String.valueOf(walletTransaction.getTransactionAmount()));
							binOfferData.setStatus(walletTransaction.getTransactionType());
							binOfferCashBackList.add(binOfferData);
							break;
						}
					}
				}
			}

			model.addAttribute("binOfferInstantList", binOfferInstantList);
			model.addAttribute("binOfferCashBackList", binOfferCashBackList);

			/* Getting campaign history from the campaign history api */

			RewardHistoryWrapper rewardHistoryWrapper = customerTrailService
					.getRewardHistoryListFromOrderId(orderId.getOrderId());
			model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
			logger.info("Reward History List received:" + rewardHistoryWrapper);

			/* Getting new promocode/reward history for provided orderid */
			// List<RewardHistory> rewardHistoryList =
			// getRewardHistoryByOrderId(orderId.getOrderId());
			// List<Map<String, Object>> failedRewardHistoryMapList= new
			// ArrayList<Map<String, Object>>();
			// if (rewardHistoryList != null && rewardHistoryList.size() > 0) {
			// for (RewardHistory rewardHistory : rewardHistoryList) {
			// if(!rewardHistory.isConditionStatus()) {
			// Map<String, Object> rewardHistoryMap = new HashMap<String, Object>();
			// rewardHistoryMap.put("orderId", rewardHistory.getOrderId());
			// rewardHistoryMap.put("serviceNumber", rewardHistory.getServiceNumber());
			// rewardHistoryMap.put("campaignId", rewardHistory.getCampaignId());
			// rewardHistoryMap.put("campaign", rewardHistory.getCampaignName());
			// rewardHistoryMap
			// .put("createdDate",
			// (noMilliSecondsFormatter.format(rewardHistory.getTimestamp())));
			// rewardHistoryMap.put("trigger", rewardHistory.getTrigger());
			// Map<String, Date> pastSuccessfulOrders =
			// campaignRedemptionService.getSuccessfulOrders(rewardHistory.getCampaignId(),
			// rewardHistory.getRedemptionKey(), rewardHistory.getRedemptionValue(),
			// rewardHistory.getTrigger());
			// if(pastSuccessfulOrders!=null && pastSuccessfulOrders.size()!=0)
			// {
			// rewardHistoryMap.put("pastOrderIds",pastSuccessfulOrders);
			// }
			// String rhMsg = rewardHistory.getConditionFailureMessage();
			// if (FCUtil.isEmpty(rhMsg)) {
			// rhMsg = rewardHistory.getConditionFailureDetailedMessage();
			// }
			// rewardHistoryMap.put("details", rhMsg);
			// rewardHistoryMap.put("productType", rewardHistory.getProductType());
			//
			// failedRewardHistoryMapList.add(rewardHistoryMap);
			// }
			// }
			// }
			//
			// //model.addAttribute("rewardHistoryList", rewardHistoryList);
			// model.addAttribute("rewardHistoryMapList", failedRewardHistoryMapList);

			/* Getting failed promocode details */
			DBObject failedPromocodeDBObject = promocodeFailMsgRepository.getPromocodeFailMsg(orderId.getOrderId());
			Map<String, String> failedPromocodeDataMap = new HashMap<>();

			if (failedPromocodeDBObject != null) {
				if (failedPromocodeDBObject.get(PROMOCODE) != null) {
					failedPromocodeDataMap.put("promocode", String.valueOf(failedPromocodeDBObject.get(PROMOCODE)));
				}
				if (failedPromocodeDBObject.get(PROMOCODE_FAILED_MSG) != null) {
					failedPromocodeDataMap.put("failedMsg",
							String.valueOf(failedPromocodeDBObject.get(PROMOCODE_FAILED_MSG)));
				}
				String orderIdForFailedPromocodeObject = String.valueOf(failedPromocodeDBObject.get(PROMOCODE_ORDERID));
				if (failedPromocodeDBObject.get(PROMOCODE_ORDERID) != null) {
					failedPromocodeDataMap.put("orderId", orderIdForFailedPromocodeObject);
				}

				List<ReIssuedPromocode> reIssuedPromocodeList = reIssuedPromocodesService
						.getReIssuedPromocodeForOrderId(orderIdForFailedPromocodeObject);
				if (!FCUtil.isEmpty(reIssuedPromocodeList)) {
					ReIssuedPromocode reIssuedPromocode = FCUtil.getFirstElement(reIssuedPromocodeList);
					FreefundCoupon freefundCoupon = freefundService
							.getFreefundCoupon(reIssuedPromocode.getReIssuedPromocodeId());
					failedPromocodeDataMap.put("reIssuedPromocode", freefundCoupon.getFreefundCode());
				} else {
					failedPromocodeDataMap.put("reIssuedPromocode", "Not Issued.");
				}
				model.addAttribute("failedPromocodeDataMap", failedPromocodeDataMap);
			}

			/*
			 * Get recharge schedule details.
			 */
			model.addAttribute("shouldShowAsQueued", false);
			if (StringUtils.isNotBlank(orderId.getOrderId())) {
				if (scheduleService.isInScheduledState(orderId.getOrderId())) {
					model.addAttribute("shouldShowAsQueued", true);
				}

				List<FulfillmentScheduleDO> scheduleList = scheduleService.getSchedule(orderId.getOrderId());

				model.addAttribute("rechargeScheduleList", scheduleList);
			}

			/* FRCH to SD user Migration status */
			if (user != null) {
				MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(user.getEmail(),
						null);
				if (migrationStatus != null) {
					String userMigrationStatus = migrationStatus.getMigrationStatus();
					model.addAttribute("fcSdUserMigration", userMigrationStatus + "("
							+ adminHelper.getStatusMsgForUserMigration(userMigrationStatus) + ")");
					model.addAttribute("userMigrationToolTipText",
							adminHelper.getToolTipMsgForUserMigration(userMigrationStatus));
				}
			}
			/* App version */
			Integer appVersion = orderidDao.getMobileAppVersion(orderId.getOrderId());
			if (appVersion != null) {
				model.addAttribute("appversion", String.valueOf(appVersion));
			}

			/* Deduct view limits by 1 */
			if (calledFrom == null || !(calledFrom.equals("customerTrail"))) {
				if (!ctActionControlService.deductAValiableViewLimitsByOne(getCurrentAdminUser(request))) {
					messages.add("Exception while deducting cs-agent's view counts, pls contact manager.");
				}

				/* Send a mail if cs-agent has crossed 90% of his max views limit */
				if (ctActionControlService.hasCrossedNinetyPercentOfViews(getCurrentAdminUser(request))) {
					ctActionControlService.emailOnNinetyPercentViewReached(getCurrentAdminUser(request));
				}
			}
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			model.addAttribute("exception", sw.toString());
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		return ViewConstants.CUSTOMER_TRAIL_LISTING;
	}

	private Map<String, String> getProductTypeMap() {
		Map<String, String> productTypeMap = new HashMap<>();
		productTypeMap.put("V", ProductMaster.ProductName.fromPrimaryProductType("V").getLabel());
		productTypeMap.put("D", ProductMaster.ProductName.fromPrimaryProductType("D").getLabel());
		productTypeMap.put("C", ProductMaster.ProductName.fromPrimaryProductType("C").getLabel());
		productTypeMap.put("F", ProductMaster.ProductName.fromPrimaryProductType("F").getLabel());
		productTypeMap.put("T", ProductMaster.ProductName.fromPrimaryProductType("T").getLabel());
		productTypeMap.put("M", ProductMaster.ProductName.fromPrimaryProductType("M").getLabel());
		productTypeMap.put("B", ProductMaster.ProductName.fromPrimaryProductType("B").getLabel());
		productTypeMap.put("L", ProductMaster.ProductName.fromPrimaryProductType("L").getLabel());
		productTypeMap.put("Y", ProductMaster.ProductName.fromPrimaryProductType("Y").getLabel());
		productTypeMap.put("E", ProductMaster.ProductName.fromPrimaryProductType("E").getLabel());
		productTypeMap.put("I", ProductMaster.ProductName.fromPrimaryProductType("I").getLabel());
		productTypeMap.put("G", ProductMaster.ProductName.fromPrimaryProductType("G").getLabel());
		productTypeMap.put("Z", ProductMaster.ProductName.fromPrimaryProductType("Z").getLabel());
		productTypeMap.put("P", ProductMaster.ProductName.fromPrimaryProductType("P").getLabel());
		productTypeMap.put("H", ProductMaster.ProductName.fromPrimaryProductType("H").getLabel());
		productTypeMap.put("R", ProductMaster.ProductName.fromPrimaryProductType("R").getLabel());
		productTypeMap.put("X", ProductMaster.ProductName.fromPrimaryProductType("X").getLabel());
		return productTypeMap;
	}

	private int getTotalPaidCouponsPriceInCart(CartBusinessDo cartBusinessDo) {
		int paidCouponTotalPrice = 0;
		for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo.getCartItems()) {
			if (cartItemsBusinessDO.getProductMasterId() == ProductMaster.ProductName.PriceCoupons.getProductId()) {
				paidCouponTotalPrice += cartItemsBusinessDO.getPrice() * cartItemsBusinessDO.getQuantity();
			}
		}
		return paidCouponTotalPrice;
	}

	private Boolean checkPaidCouponSelected(CartBusinessDo cartBusinessDo) {
		Boolean isPaidCouponsSelected = false;
		for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo.getCartItems()) {
			if (cartItemsBusinessDO.getProductMasterId() == ProductMaster.ProductName.PriceCoupons.getProductId()) {
				isPaidCouponsSelected = true;
				break;
			}
		}
		return isPaidCouponsSelected;
	}

	private Map<String, Object> getNewCrosssellCodeRedeemInfoMap(final CouponStore couponStore, final String orderId) {
		List<String> crosssellCodes = null;
		List<RedemptionInfo> redeemInfo = new ArrayList<RedemptionInfo>();
		Map<Integer, String> codesMap = new HashMap<Integer, String>();
		crosssellCodes = crossSellHistoryService.getCrosssellCodeByOrderIdCouponId(orderId,
				couponStore.getCouponStoreId());
		if (couponStore.getIsMCoupon()) {
			for (String crosssellCode : crosssellCodes) {
				List<RedemptionInfo> redemptionForCoupon = couponServiceProxy
						.findByCouponCodeAndMerchantId(crosssellCode, couponStore.getMerchantId().toString());
				if (redemptionForCoupon != null) {
					redeemInfo.addAll(redemptionForCoupon);
				}
			}
		}

		String codesStr = "";
		for (String crosssellCode : crosssellCodes) {
			codesStr += "[" + crosssellCode + "]" + " ";
		}

		codesMap.put(couponStore.getCouponStoreId(), codesStr);

		Map<String, Object> codeRedeemInfoMap = new HashMap<String, Object>();
		codeRedeemInfoMap.put("redeemInfo", redeemInfo);
		codeRedeemInfoMap.put("codesMap", codesMap);
		return codeRedeemInfoMap;
	}

	private Map<String, Object> getNewCouponCodeRedeemInfoMap(final CouponStore couponStore, final String orderId) {
		List<String> couponCodes = null;
		List<RedemptionInfo> redeemInfo = new ArrayList<RedemptionInfo>();
		Map<Integer, String> codesMap = new HashMap<Integer, String>();
		couponCodes = couponServiceProxy.getCouponsCodeByOrderIdCouponId(orderId, couponStore.getCouponStoreId());
		if (couponStore.getIsMCoupon() && couponCodes != null) {
			for (String couponCode : couponCodes) {
				List<RedemptionInfo> redemptionForCoupon = couponServiceProxy.findByCouponCodeAndMerchantId(couponCode,
						couponStore.getMerchantId().toString());
				if (redemptionForCoupon != null) {
					redeemInfo.addAll(redemptionForCoupon);
				}
			}
		}

		String codesStr = "";
		if (couponCodes != null) {
			for (String couponCode : couponCodes) {
				codesStr += "[" + couponCode + "]" + " ";
			}
		}

		codesMap.put(couponStore.getCouponStoreId(), codesStr);

		Map<String, Object> codeRedeemInfoMap = new HashMap<String, Object>();
		codeRedeemInfoMap.put("redeemInfo", redeemInfo);
		codeRedeemInfoMap.put("codesMap", codesMap);
		return codeRedeemInfoMap;
	}

	private Map<String, Object> getCodeRedeemInfoMap(final CouponStore couponStore, final String orderId) {
		List<CouponCode> couponCodes = new ArrayList<CouponCode>();
		List<RedemptionInfo> redeemInfo = new ArrayList<RedemptionInfo>();
		Map<Integer, String> codesMap = new HashMap<Integer, String>();
		if (couponStore.getIsUnique()) {
			couponCodes = couponService.getCouponCodesFromCouponStoreAndOrderId(couponStore.getCouponStoreId(),
					orderId);
			if (couponStore.getIsMCoupon() && couponCodes != null) {
				for (CouponCode couponCode : couponCodes) {
					List<RedemptionInfo> redemptionForCoupon = couponServiceProxy.findByCouponCodeAndMerchantId(
							couponCode.getCouponCode(), couponStore.getMerchantId().toString());
					if (redemptionForCoupon != null) {
						redeemInfo.addAll(redemptionForCoupon);
					}
				}
			}
		} else {
			couponCodes = couponService
					.getCouponCodesFromCouponStoreIdForNonUniqueCoupons(couponStore.getCouponStoreId());
		}

		String codesStr = "";
		if (couponCodes != null) {
			for (CouponCode couponCode : couponCodes) {
				codesStr += "[" + couponCode.getCouponCode() + "]" + " ";
			}
		}
		codesMap.put(couponStore.getCouponStoreId(), codesStr);

		Map<String, Object> codeRedeemInfoMap = new HashMap<String, Object>();
		codeRedeemInfoMap.put("redeemInfo", redeemInfo);
		codeRedeemInfoMap.put("codesMap", codesMap);
		return codeRedeemInfoMap;
	}

	private List<TxnCrossSell> getTxnCrosssellList(final Integer txnHomePageId, final CouponStore couponStore) {
		List<TxnCrossSell> crossSellList = new ArrayList<TxnCrossSell>();
		if (CouponNature.fromDbString(couponStore.getCouponNature()) == CouponNature.CrossSell) {
			crossSellList = txnCrossSellService.getActiveCrossSellByTxnHomePageIdAndcrossSellId(txnHomePageId,
					couponStore.getFkCrosssellId());
		} else {
			crossSellList = txnCrossSellService.getActiveItemByTxnHomePageIdAndCouponId(txnHomePageId,
					couponStore.getCouponStoreId());
		}
		return crossSellList;
	}

	private boolean isGenericPromoEntity(String promoEntity) {
		return FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(promoEntity)
				|| FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(promoEntity);
	}

	@RequestMapping(value = "getreferraldetailsbyemail")
	public String getReferralDetailsByEmail(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}

		String email = mapping.get("referrerEmail").trim();
		List<ReferralEntity> referralEntityList = customerTrailService.getReferralDetailsFromReferrerEmail(email);
		model.addAttribute("referralEntityList", referralEntityList);

		return ViewConstants.REFERRER_EMAIL_RESULT;
	}

	@RequestMapping(value = "getlifelinedetailsbyemail")
	public String getLifelineDetailsByEmail(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}

		String email = mapping.get("lifelineEmail").trim();
		LifelineInfo lifelineInfo = customerTrailService.getLifelineHistoryFromEmail(email);
		model.addAttribute("lifelineInfo", lifelineInfo);

		return ViewConstants.LIFELINE_EMAIL_RESULT;
	}

	// Get orderId by CouponCode(Promocode/freefund code feature)
	@RequestMapping(value = "getorderidbycouponcode")
	public String getorderIdByCouponCode(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}

		String couponCode = mapping.get("promocode").trim();
		model.addAttribute("promocodeentered", couponCode);
		String messages = null;
		String orderId = null;

		ReferralEntity referralEntity = customerTrailService.getReferralDetailsFromCode(couponCode);
		model.addAttribute("referralEntity", referralEntity);

		FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(couponCode);
		if (freefundCoupon != null) {
			String freefundCode = freefundCoupon.getFreefundCode();
			FreefundClass freefundClass = freefundService.getFreefundClass(freefundCode);
			setRedeemConditionIdForFreefundClass(freefundClass);
			model.addAttribute("freefundCoupon", freefundCoupon);
			model.addAttribute("freefundClass", freefundClass);

			if (FreefundClass.PROMO_ENTITY_CASHBACK.equals(freefundCoupon.getPromoEntity())) {
				List<ReIssuedPromocode> reIssuedPromocodeList = reIssuedPromocodesService
						.getReIssuedPromocodeForParentPromocodeId(freefundCoupon.getId());
				ReIssuedPromocode reIssuedPromocode = FCUtil.getFirstElement(reIssuedPromocodeList);
				if (reIssuedPromocode != null) {
					FreefundCoupon reIssuedFreefundCoupon = freeFundService
							.getFreefundCoupon(reIssuedPromocode.getReIssuedPromocodeId());
					model.addAttribute("reIssuedPromocode", reIssuedFreefundCoupon.getFreefundCode());
					if (reIssuedFreefundCoupon.getStatus() != null) {
						model.addAttribute("reIssuedPromocodeStatus", reIssuedFreefundCoupon.getStatus());
					} else {
						model.addAttribute("reIssuedPromocodeStatus", "ACTIVE");
					}
				}
			}
			List<String> orderidlist = orderIdReadDAO.getAllOrderIdListAttemptedForCouponCode(couponCode);
			if (orderidlist != null && orderidlist.size() >= 1) {
				for (String orderid : orderidlist) {
					RechargeStatus rechargeStatusOfOrderId = reshargeStatusService.getRechargeStatus(orderid);
					if (RechargeStatus.RECHARGE_SUCCESSFUL.getMessage().equals(rechargeStatusOfOrderId.getMessage())) {
						orderId = orderid;
						break;
					}
				}
				messages = couponCode + " " + "Promocode is already used by the orderid : " + orderId;
				Collections.reverse(orderidlist);
			} else {
				if (FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(freefundCoupon.getPromoEntity())
						&& freefundCoupon.getUsedEmailId() != null) {
					messages = couponCode + " is used by " + freefundCoupon.getUsedEmailId();
				} else if (FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon.getPromoEntity())) {
					messages = couponCode + " is a Generic Cashback PromoCode";
				} else if (FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(freefundCoupon.getPromoEntity())) {
					messages = couponCode + " is a Generic Discount PromoCode";
				} else {
					messages = couponCode + " is not used";
				}
			}
			model.addAttribute("attemptedByOrderIds", orderidlist);

			// get promocode usage details
			if (isGenericPromoEntity(freefundClass.getPromoEntity())) {
				model.addAttribute("promocodeType", "genericCode");
			}

		} else {
			messages = couponCode + " " + " is not a valid promocode/freefund code";
		}
		model.addAttribute("messages", messages);
		Map<String, String> reedemedOrderIdMap = new HashMap<>();
		if (orderId != null) {
			List<OrderId> orderIdList = orderIdDAO.getByOrderId(orderId);
			OrderId orderIdForPromocode = FCUtil.getFirstElement(orderIdList);
			if (orderIdForPromocode != null) {
				reedemedOrderIdMap.put("orderIdGeneratedDate", String.valueOf(orderIdForPromocode.getCreatedOn()));
			}
			reedemedOrderIdMap.put("redeemedOrderId", orderId);
			model.addAttribute("reedemedOrderIdMap", reedemedOrderIdMap);
		}
		model.addAttribute("reedemedOrderIdMap", reedemedOrderIdMap);
		return ViewConstants.CUSTOMER_TRAIL_PROMOCODE_RESULT;
	}

	private void setRedeemConditionIdForFreefundClass(FreefundClass freefundClass) {
		String dataMapString = freefundClass.getDatamap();
		if (dataMapString != null) {
			Map dataMap = FCStringUtils.jsonToMap(dataMapString);
			freefundClass.setRedeemConditionId((String) dataMap.get("redeemConditionId"));
		}
	}

	// Promocode/freefundcode reset option
	@RequestMapping(value = "resetfreefundorpromocode", method = RequestMethod.GET)
	public @ResponseBody String resetFreefundOrPromocode(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request) {
		String promoCode = mapping.get("promoCode").trim();
		model.addAttribute("promocodeentered", promoCode);
		String messages = null;
		boolean isredeemReset = false;
		FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(promoCode);
		if (freefundCoupon != null) {
			if (freefundCoupon.getStatus().equalsIgnoreCase("redeemed")) {
				isredeemReset = true;
			} else {
				isredeemReset = false;
			}
			freefundService.resetCouponFromCSTool(freefundCoupon);
			freefundCoupon = freefundService.getFreefundCoupon(promoCode);
			model.addAttribute("freefundCoupon", freefundCoupon);
			messages = promoCode + " Unblocked successfully. Ready to re-use.";
			if (isredeemReset) {
				metricsClient.recordEvent(METRIC_SERVICE_RESET, METRIC_NAME, "unblockedFromRedeemed");
			} else {
				metricsClient.recordEvent(METRIC_SERVICE_RESET, METRIC_NAME, "unblockedFromBlocked");
			}

		} else {
			messages = promoCode + " is not a valid promocode/freefund code";
		}
		model.addAttribute("messages", messages);
		return messages;
	}

	@RequestMapping(value = "paidcoupons", method = RequestMethod.POST)
	public String getPaidCouponsInfo(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request) {
		final String globalTxnId = mapping.get("globalTxnId").trim();
		final PaidCouponsTxnView couponsTxnView = this.tSMService.getPaidCouponsTxnView(globalTxnId);
		if (couponsTxnView == null) {
			model.addAttribute("messages", "No coupon info found for entered id");
		}
		model.addAttribute("paidCouponsTxnView", couponsTxnView);
		model.addAttribute("globaltxnidentered", globalTxnId);
		return ViewConstants.PAID_COUPNS_LISTING;
	}

	private void setOperatorRetryData(OrderId orderID, Model model, String finalProductType,
			InTransactionData inTransactionData, List<InTransactions> inTransactionsList,
			BillPaymentStatus billPaymentStatus) {
		logger.info("Entered setOperatorRetryData:1456");
		String orderId = orderID.getOrderId();
		try {
			Boolean isRechargeAttemptSuccessful = false;
			Boolean isBillPaymentAttemptSuccessful = false;
			List<RechargeRetryData> operaratorRetryDatas = inService.getRechargeRetryDataForOrder(orderId);
			if (!FCUtil.isEmpty(operaratorRetryDatas)) {
				logger.info("List of operator retry data for orderid: " + orderId + " : "
						+ operaratorRetryDatas.toString());
				model.addAttribute("isOperatorRetry", true);
				model.addAttribute("operaratorRetryDatas", operaratorRetryDatas);
				if (!FCUtil.isEmpty(inTransactionsList)) {
					logger.info("Latest inTransaction for orderId: " + orderId + " is: "
							+ inTransactionsList.get(0).toString());
					isRechargeAttemptSuccessful = customerTrailService.isRechargeNotFailure(inTransactionsList.get(0));
					logger.info("isRechargeAttemptSuccessful: " + isRechargeAttemptSuccessful);
				}
				if (null != billPaymentStatus) {
					logger.info("billPaymentStatus for orderId: " + orderId + " is: " + billPaymentStatus.toString());
					isBillPaymentAttemptSuccessful = customerTrailService.isBillPaymentNotFailure(billPaymentStatus);
					logger.info("isBillPaymentAttemptSuccessful: " + isBillPaymentAttemptSuccessful);
				}
				String productType = customerTrailService.getFinalOperatorRetryProductType(isRechargeAttemptSuccessful,
						isBillPaymentAttemptSuccessful, finalProductType, orderId);
				if (FCUtil.isBillPostpaidType(productType)) {
					model.addAttribute("billPaymentStatus", billPaymentStatus);
					BillPaymentAttempt billPaymentAttempt = billPaymentService
							.findBillPaymentAttemptByStatusId(billPaymentStatus.getId());
					model.addAttribute("postpaidMerchantName", billPaymentStatus.getOperatorName());
					model.addAttribute("billPaymentAttempt", billPaymentAttempt);
					model.addAttribute("finalProductType", "M");
				} else if (FCUtil.isRechargeProductType(productType)) {
					model.addAttribute("finalProductType", "V");
					model.addAttribute("inTransactionData", inTransactionData);
					model.addAttribute("inTransactionsList", inTransactionsList);
				}
			}
		} catch (Exception e) {
			logger.error("Caught exception while fetching operator retry data for order id " + orderId, e);
		}
	}

	private void setIntransactionData(OrderId orderId, Model model, String finalProductType) {
		InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId.getOrderId());
		BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId.getOrderId());

		List<InTransactions> inTransactionsList = null;
		String productName = null;
		String reversalToSuccess = RechargeConstants.RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG;
		String reversalToFailure = RechargeConstants.RECHARGE_REVERSAL_IN_RESP_MSG;

		if (inTransactionData != null) {
			model.addAttribute("inTransactionData", inTransactionData);

			if (inTransactionData.getInResponse() != null
					&& (reversalToSuccess.equalsIgnoreCase(inTransactionData.getInResponse().getInRespMsg())
							|| reversalToFailure.equalsIgnoreCase(inTransactionData.getInResponse().getInRespMsg()))) {
				model.addAttribute("isReverselCase", "reversalCase");
			}
			/* Getting all recharge attempts */
			inTransactionsList = inService.getInTransactionsListByOrder(orderId.getOrderId());
			Collections.reverse(inTransactionsList);
			model.addAttribute("inTransactionsList", inTransactionsList);
			/* Getting product name */
			if (inTransactionData.getInRequest() != null) {
				productName = FCConstants.productTypeToNameMap.get(inTransactionData.getInRequest().getProductType());
				model.addAttribute("productName", productName);
				model.addAttribute("rechargeProductType", inTransactionData.getInRequest().getProductType());
			}
		}

		if (billPaymentStatus != null) {
			List<BillPaymentAttempt> billPayAttempts = billPaymentService
					.findBillPaymentAttemptsByStatusId(billPaymentStatus.getId());
			model.addAttribute("postpaidAttemptsDataList", billPayAttempts);
			model.addAttribute("billPaymentStatus", billPaymentStatus);
		}
		setOperatorRetryData(orderId, model, finalProductType, inTransactionData, inTransactionsList,
				billPaymentStatus);
	}

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml",
				"web-delegates.xml");
		EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));
		PaymentTransactionService paymentTransactionService = ((PaymentTransactionService) ctx
				.getBean("paymentTransactionService"));
		InService inService = ((InService) ctx.getBean("inService"));
	}

	public InService getInService() {
		return inService;
	}

	public void setInService(InService inService) {
		this.inService = inService;
	}

	public PaymentTransactionService getPaymentTransactionService() {
		return paymentTransactionService;
	}

	public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
		this.paymentTransactionService = paymentTransactionService;
	}

	public EasyDBWriteDAO getEasyDBWriteDAO() {
		return easyDBWriteDAO;
	}

	public void setEasyDBWriteDAO(EasyDBWriteDAO easyDBWriteDAO) {
		this.easyDBWriteDAO = easyDBWriteDAO;
	}

	// @RequestMapping(value = "getRewardHistoryByOrderId", method =
	// RequestMethod.GET, produces = "application/json")
	// public @ResponseBody List<RewardHistory> getRewardHistoryByOrderId(
	// HttpServletRequest request) {
	// String orderId = request.getParameter("orderId");
	// return getRewardHistoryByOrderId(orderId);
	// }
	//
	// public List<RewardHistory> getRewardHistoryByOrderId(String orderId) {
	// List<CampaignHistory> campaignHistories =
	// this.campaignHistoryRepository.findByOrderId(orderId);
	// return getRewardHistoryFromCampaignHistory(campaignHistories);
	// }

	// private List<RewardHistory>
	// getRewardHistoryFromCampaignHistory(List<CampaignHistory> campaignHistory) {
	// logger.info("Size of campaign history : " + campaignHistory.size());
	// List<RewardHistory> rewardHistory = new ArrayList<>();
	// RewardHistory rewardHistoryObj = null;
	// for (CampaignHistory campaignHistoryObj : campaignHistory) {
	// logger.info("Campaign History : " + campaignHistoryObj);
	// logger.info("Campaign History : " + campaignHistoryObj.getId());
	// rewardHistoryObj = new RewardHistory();
	// rewardHistoryObj.setUserEmail(campaignHistoryObj.getUserEmail());
	// rewardHistoryObj.setUserId(campaignHistoryObj.getUserId());
	// rewardHistoryObj.setOrderId(campaignHistoryObj.getOrderId());
	// rewardHistoryObj.setServiceNumber(campaignHistoryObj.getServiceNumber());
	// rewardHistoryObj.setProductType(campaignHistoryObj.getProductType());
	// rewardHistoryObj.setImeiNumber(campaignHistoryObj.getImeiNumber());
	// rewardHistoryObj.setDeviceUniqueId(campaignHistoryObj.getDeviceUniqueId());
	// rewardHistoryObj.setCampaignId(campaignHistoryObj.getCampaignId());
	// rewardHistoryObj.setCampaignName(campaignHistoryObj.getCampaignName());
	// // rewardHistoryObj.setCardHash(campaignHistoryObj.getCardHash());
	// if (null != campaignHistoryObj.getConditionResult()) {
	// rewardHistoryObj.setConditionStatus(campaignHistoryObj.getConditionResult().getStatus());
	// }
	// rewardHistoryObj.setTrigger(campaignHistoryObj.getTrigger());
	// rewardHistoryObj.setTimestamp(campaignHistoryObj.getCreatedAt());
	//
	// boolean showPastOrders = false;
	// // boolean showRetriggerNotification = false;
	// Map<Integer, String[]> detailsMap = new HashMap<>();
	// if (!((null == campaignHistoryObj.getConditionResult()) ? false :
	// campaignHistoryObj.getConditionResult().getStatus())) {
	// RuleResult ruleResult;
	// if (null == campaignHistoryObj.getConditionResult()) {
	// ruleResult = null;
	// } else {
	// ruleResult = campaignHistoryObj.getConditionResult().getFailedRule();
	// }
	// String [] rewardDetails = new String[3];
	// // check if failed because of fraud check conditions
	// if (null != ruleResult) {
	// if (InTxnDataConstants.USER_ID.equals(ruleResult.getKey()) ||
	// InTxnDataConstants.SERVICE_NUMBER.equals(ruleResult.getKey()) ||
	// InTxnDataConstants.DEVICE_UNIQUE_ID.equals(ruleResult.getKey()) ||
	// InTxnDataConstants.IMEI_NUMBER.equals(ruleResult.getKey()) ||
	// InTxnDataConstants.CARD_HASH.equals(ruleResult.getKey())) {
	//
	// showPastOrders = true; // show 'show past orders' button only if fraud
	// condition failed
	// rewardHistoryObj.setRedemptionKey(ruleResult.getKey());
	// // rewardHistoryObj.setRedemptionValue(ruleResult.getActualValue());
	//
	// if (InTxnDataConstants.USER_ID.equals(ruleResult.getKey())) {
	// rewardHistoryObj.setRedemptionValue(campaignHistoryObj.getUserId().toString());
	// } else if (InTxnDataConstants.SERVICE_NUMBER.equals(ruleResult.getKey())) {
	// rewardHistoryObj.setRedemptionValue(campaignHistoryObj.getServiceNumber());
	// } else if (InTxnDataConstants.DEVICE_UNIQUE_ID.equals(ruleResult.getKey())) {
	// rewardHistoryObj.setRedemptionValue(campaignHistoryObj.getDeviceUniqueId());
	// } else if (InTxnDataConstants.IMEI_NUMBER.equals(ruleResult.getKey())) {
	// rewardHistoryObj.setRedemptionValue(campaignHistoryObj.getImeiNumber());
	// } else if (InTxnDataConstants.CARD_HASH.equals(ruleResult.getKey())) {
	// rewardHistoryObj.setRedemptionValue(campaignHistoryObj.getCardHash());
	// }
	//
	// /* details.append("Fraud Condition Failure. Max redemption for campaign
	// reached for " + ruleResult.getKey() +
	// " : " + ruleResult.getActualValue() + ", whereas max no of times allowed is :
	// " + ruleResult.getAllowedValues().get(0));*/
	// rewardHistoryObj.setRewardStatusDetailMap(null);
	// } else {
	// rewardHistoryObj.setRedemptionKey(null);
	// rewardHistoryObj.setRedemptionValue(null);
	// rewardHistoryObj.setRewardStatusDetailMap(null);
	// }
	// rewardHistoryObj.setConditionFailureMessage(ruleResult.getFailureMessage());
	// rewardHistoryObj.setConditionFailureDetailedMessage(ruleResult.getDetailedFailureMessage());
	// } else {
	// rewardHistoryObj.setConditionFailureMessage("condition process failed");
	// rewardHistoryObj.setConditionFailureDetailedMessage("condition process
	// failed");
	// }
	// } else {
	// // showRetriggerNotification = true; // show 'retrigger notification' button
	// only if conditions satisfied and reward was successful
	// Map<Integer, Boolean> showNotificationStatus = new HashMap<>();
	// /*Map<Integer, Boolean> rewardStatusMap = new HashMap<>();
	// Map<Integer, Map<String, String>> tempRewardValueMap = new HashMap<>();
	// if (null != campaignHistoryObj.getRewardProcessResults() &&
	// !campaignHistoryObj.getRewardProcessResults().isEmpty()) {
	// campaignHistoryObj.getRewardProcessResults().keySet().forEach(rewardId -> {
	// RewardProcessResult rewardProcessResult =
	// campaignHistoryObj.getRewardProcessResults().get(rewardId);
	// List<AbstractProcessResult> abstractProcessResults =
	// rewardProcessResult.getProcessResults();
	// if (null != abstractProcessResults) {
	// logger.info("abstract process results size is: " +
	// abstractProcessResults.size());
	// abstractProcessResults.forEach(abstractProcessResult -> {
	// StringBuilder multipleRewardsDetails = new StringBuilder("All conditions
	// satisfied. ");
	// logger.info("current abstract process result is: " + abstractProcessResult);
	// String[] rewardDetails = new String[3];
	// Integer rewardMasterId = abstractProcessResult.getChildRewardMasterId();
	// rewardDetails[0] =
	// this.rewardServiceClient.getRewardById(rewardMasterId).getName();
	// rewardDetails[1] = abstractProcessResult.getType().toString();
	// rewardStatusMap.put(rewardMasterId,
	// abstractProcessResult.getStatus().isSucesss());
	// showNotificationStatus.put(rewardMasterId,
	// rewardProcessResult.getStatus().isSucesss());
	// if (abstractProcessResult.getStatus().isSucesss()) {
	// if (rewardProcessResult.getStatus().isSucesss()) {
	// multipleRewardsDetails.append("Reward processed successfully.");
	// } else {
	// multipleRewardsDetails.append("Reward processed successfully. " +
	// "But, one of the reward which is part of this Reward Family is Failed. Parent
	// Reward Id is: " + rewardProcessResult.getRewardId());
	// }
	// } else {
	// multipleRewardsDetails.append("Reward processing failed. Reward Id is: " +
	// abstractProcessResult.getChildRewardMasterId() + ". Reason : " +
	// abstractProcessResult.getMessage());
	// }
	// rewardDetails[2] = multipleRewardsDetails.toString();
	// detailsMap.put(rewardMasterId, rewardDetails);
	//
	// try {
	// tempRewardValueMap.put(rewardMasterId,
	// campaignService.getRewardValueMap(rewardProcessResult,
	// campaignHistoryObj.getCampaignId(), campaignHistoryObj.getOrderId()));
	// } catch (Exception e) {
	// logger.info("error setting reward value map");
	// }
	// });
	// } else {
	// StringBuilder details = new StringBuilder("All conditions satisfied. ");
	// rewardStatusMap.put(rewardProcessResult.getRewardId(),
	// rewardProcessResult.getStatus().isSucesss());
	// String[] rewardDetails = new String[3];
	// showNotificationStatus.put(rewardProcessResult.getRewardId(),
	// rewardProcessResult.getStatus().isSucesss());
	// if (rewardProcessResult.getStatus().isSucesss()) {
	// details.append("Reward processed successfully.");
	// } else {
	// details.append("Reward processing failed. Reward Id is: " +
	// rewardProcessResult.getRewardId() + ". Reason : " +
	// rewardProcessResult.getMessage());
	// }
	// Reward reward =
	// this.rewardServiceClient.getRewardById(rewardProcessResult.getRewardId());
	// rewardDetails[0] = reward.getName();
	// rewardDetails[1] = reward.getRewardType().toString();
	// rewardDetails[2] = details.toString();
	// detailsMap.put(rewardProcessResult.getRewardId(), rewardDetails);
	// try {
	// tempRewardValueMap.put(rewardProcessResult.getRewardId(),
	// campaignService.getRewardValueMap(rewardProcessResult,
	// campaignHistoryObj.getCampaignId(), campaignHistoryObj.getOrderId()));
	// } catch (Exception e) {
	// logger.info("error setting reward value map");
	// }
	// }
	// });
	// }*/
	// /*rewardHistoryObj.setRewardValueMap(tempRewardValueMap);
	// rewardHistoryObj.setRewardStatusMap(rewardStatusMap);*/
	// rewardHistoryObj.setRewardStatusDetailMap(detailsMap);
	// //rewardHistoryObj.setShowRetriggerNotificationMap(showNotificationStatus);
	// }
	// rewardHistoryObj.setShowPastOrders(showPastOrders);
	//
	// // adding reward history object to final list
	// rewardHistory.add(rewardHistoryObj);
	// }
	// return rewardHistory;
	// }

	// Request handler for search transaction details using UPI Txn ID (VPA).
	@RequestMapping(value = "upi")
	public String getCustomerUpiTxnDetailTrail(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}

		String transactionId = "";
		String transactionType = "";
		String mobileNo = "";
		List<String> messages = new LinkedList<String>();

		if (mapping.get(CustomerTrailController.TXN_ID_PARAM_NAME) != null) {
			transactionId = mapping.get(CustomerTrailController.TXN_ID_PARAM_NAME).trim();
		}
		if (mapping.get(CustomerTrailController.TXN_TYPE_PARAM_NAME) != null) {
			transactionType = mapping.get(CustomerTrailController.TXN_TYPE_PARAM_NAME).trim();
		}
		if (mapping.get(CustomerTrailController.TXN_MOBILENO_PARAM_NAME) != null) {
			mobileNo = mapping.get(CustomerTrailController.TXN_MOBILENO_PARAM_NAME).trim();
		}

		if (transactionId.isEmpty() || transactionType.isEmpty() || mobileNo.isEmpty()) {
			messages.add("Transaction id or transaction type or MobileNo is not valid");
			model.addAttribute("messages", messages);
			return ViewConstants.CUSTOMER_TRAIL_UPITXN_DETAILS;
		}

		UPITransactionDetailView upiTransactionDetails = upiTransactionService.getUpiTransactionByDetails(mobileNo,
				transactionId, transactionType);

		model.addAttribute("upiTransactionDetails", upiTransactionDetails);
		model.addAttribute("requestedTransactionId", transactionId);
		return ViewConstants.CUSTOMER_TRAIL_UPITXN_DETAILS;
	}

	// Request handler for search transactions using UPI ID (VPA).
	@RequestMapping(value = "upiId")
	public String getCustomerUpiTrail(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}

		List<String> errMessage = new LinkedList<String>();
		String upiId = "";
		if (mapping.get(CustomerTrailController.UPIID_PARAM_NAME) != null) {
			upiId = mapping.get(CustomerTrailController.UPIID_PARAM_NAME).trim();
			model.addAttribute("upiIdEntered", upiId);
		}
		if (upiId.isEmpty()) {
			errMessage.add("UPI Id is blank.");
			model.addAttribute("messages", errMessage);
		} else {
			model.addAttribute("upiTransactionsList", upiTransactionService.getUPITransactionsViewDataByVpa(upiId));
		}
		return ViewConstants.CUSTOMER_TRAIL_UPITXN_LIST;
	}
}
