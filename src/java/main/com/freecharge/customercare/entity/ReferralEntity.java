package com.freecharge.customercare.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class ReferralEntity {

	@JsonProperty("emailId")
	@SerializedName("emailId")
	private String referrerEmail;
	
	@JsonProperty("referralCode")
	@SerializedName("referralCode")
	private String referralCode;
	
	@JsonProperty("cashbackEarned")
	@SerializedName("cashbackEarned")
	private String referrerCashbackEarned;
	
	@JsonProperty("orderIdList")
	@SerializedName("orderIdList")
	private List<String> referredOrders;
	
	@JsonProperty("status")
	@SerializedName("status")
	private String status;
	
	public String getReferrerEmail() {
		return referrerEmail;
	}
	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public String getReferrerCashbackEarned() {
		return referrerCashbackEarned;
	}
	public void setReferrerCashbackEarned(String referrerCashbackEarned) {
		this.referrerCashbackEarned = referrerCashbackEarned;
	}
	public List<String> getReferredOrders() {
		return referredOrders;
	}
	public void setReferredOrders(List<String> referredOrders) {
		this.referredOrders = referredOrders;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ReferralEntity [referrerEmail=" + referrerEmail + ", referralCode=" + referralCode
				+ ", referrerCashbackEarned=" + referrerCashbackEarned + ", referredOrders=" + referredOrders
				+ ", status=" + status + "]";
	}	
	
}
