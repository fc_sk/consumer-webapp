package com.freecharge.customercare.entity;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ReferralEntityCode {

	@SerializedName("Email Id")
	private String referrerEmail;
	
	@SerializedName("referralCode")
	private String referralCode;
	
	@SerializedName("Total cashback earned")
	private String referrerCashbackEarned;
	
	@SerializedName("OrderId List")
	private List<String> referredOrders;

	@SerializedName("message")
	private String message;
	
	@SerializedName("status")
	private String status;
	
	public String getReferrerEmail() {
		return referrerEmail;
	}

	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getReferrerCashbackEarned() {
		return referrerCashbackEarned;
	}

	public void setReferrerCashbackEarned(String referrerCashbackEarned) {
		this.referrerCashbackEarned = referrerCashbackEarned;
	}

	public List<String> getReferredOrders() {
		return referredOrders;
	}

	public void setReferredOrders(List<String> referredOrders) {
		this.referredOrders = referredOrders;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ReferralEntityCode [referrerEmail=" + referrerEmail + ", referralCode=" + referralCode
				+ ", referrerCashbackEarned=" + referrerCashbackEarned + ", referredOrders=" + referredOrders
				+ ", message=" + message + ", status=" + status + "]";
	}
	
	
}
