package com.freecharge.customercare.entity;

import java.util.List;

public class ReferralEntityWrapper {

	private List<ReferralEntityEmail> emailReferralEntities;
	private List<ReferralEntityCode>  codeReferralEntities;
	private String status;
	
	public List<ReferralEntityEmail> getEmailReferralEntities() {
		return emailReferralEntities;
	}
	public void setEmailReferralEntities(List<ReferralEntityEmail> emailReferralEntities) {
		this.emailReferralEntities = emailReferralEntities;
	}
	public List<ReferralEntityCode> getCodeReferralEntities() {
		return codeReferralEntities;
	}
	public void setCodeReferralEntities(List<ReferralEntityCode> codeReferralEntities) {
		this.codeReferralEntities = codeReferralEntities;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "ReferralEntityWrapper [emailReferralEntities=" + emailReferralEntities + ", codeReferralEntities="
				+ codeReferralEntities + ", status=" + status + "]";
	}
	
	
	
}
