package com.freecharge.gupshuphlr.service;

public class SMSGupshupResponse {

	private String requestId;
	private String dest;
	private String status;
	private String reason;
	private String homeCountry;
	private String homeOperator;
	private String homeCircle;
	private String portingStatus;
	private String errorCode;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	public String getHomeOperator() {
		return homeOperator;
	}

	public void setHomeOperator(String homeOperator) {
		this.homeOperator = homeOperator;
	}

	public String getHomeCircle() {
		return homeCircle;
	}

	public void setHomeCircle(String homeCircle) {
		this.homeCircle = homeCircle;
	}

	public String getPortingStatus() {
		return portingStatus;
	}

	public void setPortingStatus(String portingStatus) {
		this.portingStatus = portingStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "SMSGupshupResponse [requestId=" + requestId + ", dest=" + dest + ", status=" + status + ", reason="
				+ reason + ", homeCountry=" + homeCountry + ", homeOperator=" + homeOperator + ", homeCircle="
				+ homeCircle + ", portingStatus=" + portingStatus + ", errorCode=" + errorCode + "]";
	}

}
