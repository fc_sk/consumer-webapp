package com.freecharge.gupshuphlr.service;

import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.hlr.service.HLRResponse;
import com.freecharge.hlr.service.HLRService;
import com.freecharge.hlr.service.HLRService.HLR_SERVICE;
import com.freecharge.hlr.service.HLRService.STATUS;
import com.freecharge.hlr.service.HLRServiceInterface;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.fulfillment.db.HLRResponseDAO;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class SMSGupshupHLRService implements HLRServiceInterface{
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	private MetricsClient metricsClient;
	
	@Autowired
	private HLRResponseDAO hlrResponseDao;

	private Map<String, String> gupshupCircleMapping = ImmutableMap.<String, String> builder()
			.put("Andhra Pradesh", "Andhra Pradesh").put("Assam", "Assam").put("Bihar and Jharkhand", "Bihar")
			.put("Chennai", "Chennai").put("Delhi", "Delhi").put("Gujarat", "Gujarat").put("Haryana", "Haryana")
			.put("Himachal Pradesh", "Himachal Pradesh").put("Jammu & Kashmir", "JK").put("Karnataka", "Karnataka")
			.put("Kerala", "Kerala").put("Kolkata", "Kolkata").put("Maharashtra", "Maharashtra")
			.put("Madhya Pradesh", "Madhya Pradesh").put("Mumbai", "Mumbai").put("North East", "North East")
			.put("Orissa", "Orissa").put("Punjab", "Punjab").put("Rajasthan", "Rajasthan")
			.put("Tamil Nadu", "Tamil Nadu").put("UP-East", "Uttar Pradesh (E)").put("UP-West", "Uttar Pradesh (W)")
			.put("West Bengal", "West Bengal").build();

	private Map<String, String> gupshupOperatorMapping = ImmutableMap.<String, String> builder()
			.put("Vodafone", "Vodafone").put("Airtel", "Airtel").put("BSNL", "BSNL")
			.put("Tata Teleservices", "Tata Docomo GSM").put("nil", "Nil").put("MTS", "MTS").put("RCOM", "Reliance-GSM")
			.put("Aircel", "Aircel").put("IDEA", "Idea").put("Videocon", "Videocon Mobile").put("Loop Telecom", "Loop")
			.put("Reliance Jio", "JIO").build();

	public SMSGupshupResponse getGupshupResponse(String serviceNumber) {

		String url = FCConstants.SMSGUPSHUPHLR_URL;
		HttpClient client = new HttpClient();

		HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();

		connectionParams.setConnectionTimeout(2000);
		connectionParams.setSoTimeout(6000);
		client.getHttpConnectionManager().setParams(connectionParams);

		GetMethod getMethod = new GetMethod(url);

		String userId = FCConstants.SMSGUPSHUPHLR_USERID;
		String password = FCConstants.SMSGUPSHUPHLR_PWD;
		String method = FCConstants.SMSGUPSHUPHLR_METHOD;
		String version = FCConstants.SMSGUPSHUPHLR_VERSION;
		String format = FCConstants.SMSGUPSHUPHLR_FORMAT;

		getMethod.setQueryString(
				new NameValuePair[] { new NameValuePair("userid", userId), new NameValuePair("password", password),
						new NameValuePair("phone_no", serviceNumber), new NameValuePair("method", method),
						new NameValuePair("v", version), new NameValuePair("format", format) });

		long start = System.currentTimeMillis();
		SMSGupshupResponse smsGupshupResponse = null;
		try {
			int response = client.executeMethod(getMethod);

			if (response != HttpStatus.SC_OK) {
				logger.error("Got Http status call " + response);
				return null;
			}
			String resp = getMethod.getResponseBodyAsString();
			logger.info("Got SMSGupshupHLR Response for " + serviceNumber + " is " + resp);

			Type type = new TypeToken<Map<String, String>>() {
			}.getType();
			Map<String, Object> retMap = new Gson().fromJson(resp, type);

			if (!FCUtil.isEmpty(retMap)) {
				if (retMap.containsKey(SMSGupshupConstants.STATUS)) {
					if ("SUCCESS".equals(retMap.get(SMSGupshupConstants.STATUS))) {
						smsGupshupResponse = new SMSGupshupResponse();
						smsGupshupResponse.setStatus((String) retMap.get(SMSGupshupConstants.STATUS));
						smsGupshupResponse.setRequestId((String) retMap.get(SMSGupshupConstants.REQUEST_ID));
						smsGupshupResponse.setDest((String) retMap.get(SMSGupshupConstants.DEST));
						smsGupshupResponse.setHomeCountry((String) retMap.get(SMSGupshupConstants.HOME_COUNRTY));
						smsGupshupResponse.setHomeOperator((String) retMap.get(SMSGupshupConstants.HOME_OPERATOR));
						smsGupshupResponse.setHomeCircle((String) retMap.get(SMSGupshupConstants.HOME_CIRCLE));
						smsGupshupResponse.setPortingStatus((String) retMap.get(SMSGupshupConstants.PORTING_STATUS));
					} else {
						smsGupshupResponse = new SMSGupshupResponse();
						smsGupshupResponse.setStatus((String) retMap.get(SMSGupshupConstants.STATUS));
						smsGupshupResponse.setRequestId((String) retMap.get(SMSGupshupConstants.REQUEST_ID));
						if (retMap.containsKey(SMSGupshupConstants.ERROR_CODE)) {
							smsGupshupResponse.setErrorCode((String) retMap.get(SMSGupshupConstants.ERROR_CODE));
						}

					}
				}
			}

		} catch (Exception e) {
			logger.error("Caught exception at SMSGupshupHLRService is ", e);
		} finally {
			logger.info("Total time for SMSGupShupHLR call is " + (System.currentTimeMillis() - start));
		}

		return smsGupshupResponse;
	}

	public String getFCOperator(String operator) {
		if (gupshupOperatorMapping.containsKey(operator))
			return gupshupOperatorMapping.get(operator);
		else
			return null;
	}

	public String getFCCircle(String circle) {
		if (gupshupCircleMapping.containsKey(circle))
			return gupshupCircleMapping.get(circle);
		else
			return null;
	}

	 public static void main(String args[]) {
	        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("commonApplicationContext.xml");
	        SMSGupshupHLRService service = (SMSGupshupHLRService) ctx.getBean("sMSGupShupHLRService");
	        SMSGupshupResponse response = service.getGupshupResponse("9051543117");
	        System.out.println(response);

	    }

	@Override
	public HLRResponse getHLRResponse(String serviceNumber, String orderId, Integer operatorId, Integer circleId) {
		HLRResponse hlrResponse = null;
		
		if (appConfigService.isSMSGupshupHLREnabled()) {
			metricsClient.recordCount("HLRService", "SMSGupshupHLRService.RequestCount", 1);
			SMSGupshupResponse smsGupshupResponse = getGupshupResponse(serviceNumber);
			logger.info("SMSGupshupResponse =" + smsGupshupResponse);
			if (smsGupshupResponse!=null && "SUCCESS".equalsIgnoreCase(smsGupshupResponse.getStatus().trim())) {
				hlrResponse = new HLRResponse();
				hlrResponse.setOperator(getFCOperator(smsGupshupResponse.getHomeOperator()));
				hlrResponse.setCircle(getFCCircle(smsGupshupResponse.getHomeCircle()));
				hlrResponse.setErrorCode(HLRService.STATUS.SUCCESS.getStatusId());				
				try {
					hlrResponseDao.logHLRSuccess(hlrResponse, smsGupshupResponse.getRequestId(), serviceNumber, orderId,
							operatorId, circleId, STATUS.SUCCESS, HLR_SERVICE.SMSGUPSHUP);
				} catch (Exception e) {
					logger.error("Error in logging SMSGupshupResponse for orderId:" + orderId, e);
				}
				return hlrResponse;
			}
			if(smsGupshupResponse!=null){
				hlrResponse = new HLRResponse();
				hlrResponse.setErrorCode(HLRService.STATUS.FAILURE.getStatusId());
				hlrResponse.setErrorMsg(smsGupshupResponse.getReason());
			}
			logHLRResponse(smsGupshupResponse,serviceNumber,orderId,operatorId,circleId);			
			metricsClient.recordCount("HLRService", "SMSGupshupHLRService.ErrorCount", 1);
		}	
		
		return hlrResponse;
		
	}
	

	private void logHLRResponse(SMSGupshupResponse smsGupshupResponse, String serviceNumber, String orderId,
			Integer operatorId, Integer circleId) {
		try {
			if (smsGupshupResponse != null) {
				hlrResponseDao.logHLRFailure(smsGupshupResponse.getRequestId(), smsGupshupResponse.getReason(),
						serviceNumber, orderId, operatorId, circleId, STATUS.FAILURE, HLR_SERVICE.SMSGUPSHUP);
			} else {
				hlrResponseDao.logHLRTimeout(orderId, serviceNumber, operatorId, circleId, STATUS.TIMEOUT,
						HLR_SERVICE.SMSGUPSHUP);

			}
		} catch (Exception e) {
			logger.error("Error in logging SMSGupshupResponse for orderId:" + orderId, e);
		}
		
	}
}
