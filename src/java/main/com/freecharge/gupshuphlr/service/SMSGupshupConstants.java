package com.freecharge.gupshuphlr.service;

public class SMSGupshupConstants {

	public static final String REQUEST_ID = "RequestID";
	public static final String DEST = "Dest";
	public static final String STATUS = "Status";
	public static final String REASON = "Reason";
	public static final String HOME_COUNRTY = "HomeCountry";
	public static final String HOME_OPERATOR = "HomeOperator";
	public static final String HOME_CIRCLE = "HomeCircle";
	public static final String PORTING_STATUS = "PortingStatus";
	public static final String ERROR_CODE = "errorCode";
	public static final String DETAILS = "details";
}
