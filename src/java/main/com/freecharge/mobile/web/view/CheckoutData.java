package com.freecharge.mobile.web.view;

import javax.validation.Valid;

import com.freecharge.analytics.OrderDetails;
import com.freecharge.mobile.web.view.payment.PaymentOptions;

public class CheckoutData {
	@Valid
	private PaymentOptions paymentOptions;
	private String lookupId;
	private Double totalAmount;
	private Boolean isPaymentSuccessful;
	private String orderId;
	private String productType;
	private String rechargeResponse;
	private String number;
	private Integer rechargeAmount;
	private String coupons;
	private String operator;
	private String circle;
	private Boolean contactDetailsReq;
	private ShippingDetails shippingDetails;
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	private OrderDetails orderDetails;
	private String deliveryState;
	private String deliveryCountry;
	private String deliveryCity;
	
	
	public String getDeliveryCity() {
		return deliveryCity;
	}
	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}
	public String getDeliveryState() {
		return deliveryState;
	}
	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}
	public String getDeliveryCountry() {
		return deliveryCountry;
	}
	public void setDeliveryCountry(String deliveryCountry) {
		this.deliveryCountry = deliveryCountry;
	}
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	
	public PaymentOptions getPaymentOptions() {
		return paymentOptions;
	}
	public void setPaymentOptions(PaymentOptions paymentOptions) {
		this.paymentOptions = paymentOptions;
	}
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Boolean getIsPaymentSuccessful() {
		return isPaymentSuccessful;
	}
	public void setIsPaymentSuccessful(Boolean isPaymentSuccessful) {
		this.isPaymentSuccessful = isPaymentSuccessful;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getRechargeResponse() {
		return rechargeResponse;
	}
	public void setRechargeResponse(String rechargeResponse) {
		this.rechargeResponse = rechargeResponse;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Integer getRechargeAmount() {
		return rechargeAmount;
	}
	public void setRechargeAmount(Integer rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}
	public String getCoupons() {
		return coupons;
	}
	public void setCoupons(String coupons) {
		this.coupons = coupons;
	}
	public Boolean getContactDetailsReq() {
		return contactDetailsReq;
	}
	public void setContactDetailsReq(Boolean contactDetailsReq) {
		this.contactDetailsReq = contactDetailsReq;
	}
	public ShippingDetails getShippingDetails() {
		return shippingDetails;
	}
	public void setShippingDetails(ShippingDetails shippingDetails) {
		this.shippingDetails = shippingDetails;
	}
}
