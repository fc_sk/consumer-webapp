package com.freecharge.mobile.web.view;

public class City {
	Integer cityMasterId;
	private String cityName;
	
	public Integer getCityMasterId() {
		return cityMasterId;
	}
	public void setCityMasterId(Integer cityMasterId) {
		this.cityMasterId = cityMasterId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}	
}
