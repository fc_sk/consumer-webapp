package com.freecharge.mobile.web.view.payment;

import java.util.List;

import com.freecharge.mobile.validator.annotation.NetBankCode;

public class InternetBanking {

	@NetBankCode
	private String bankCode;
	
	private List<Bank> bankList;
	
	private boolean isSelected;

	public List<Bank> getBankList() {
		return bankList;
	}

	public void setBankList(List<Bank> bankList) {
		this.bankList = bankList;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
}
