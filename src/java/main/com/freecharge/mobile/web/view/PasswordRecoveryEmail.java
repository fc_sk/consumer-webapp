package com.freecharge.mobile.web.view;

import com.freecharge.mobile.validator.annotation.EmailIdExist;


public class PasswordRecoveryEmail {
	@EmailIdExist
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
