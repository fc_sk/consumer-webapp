package com.freecharge.mobile.web.view.payment;

import com.freecharge.common.util.Amount;

public class Wallet {
    private Amount balance;

    public Amount getBalance() {
        return balance;
    }

    public void setBalance(Amount balance) {
        this.balance = balance;
    }

    public static enum FundDestination {
        RECHARGE,
        HCOUPON;
    }
}
