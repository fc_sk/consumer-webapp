package com.freecharge.mobile.web.view.payment;

public class CardStorage {
	
	 private boolean isSaveCardSelected;
	 private String saveCardName;
	 private String debitCardToken;
	 private String cardType;
	 private String cardReference;
	 
	 public String getCardReference() {
		return cardReference;
	}
	public void setCardReference(String cardReference) {
		this.cardReference = cardReference;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getDebitCardToken() {
		return debitCardToken;
	}
	public void setDebitCardToken(String debitCardToken) {
		this.debitCardToken = debitCardToken;
	}
	private String creditCardToken;
	 
	public String getCreditCardToken() {
		return creditCardToken;
	}
	public void setCreditCardToken(String credutCardToken) {
		this.creditCardToken = credutCardToken;
	}
	
	public boolean isSaveCardSelected() {
		return isSaveCardSelected;
	}
	public void setSaveCardSelected(boolean isSaveCardSelected) {
		this.isSaveCardSelected = isSaveCardSelected;
	}
	public String getSaveCardName() {
		return saveCardName;
	}
	public void setSaveCardName(String saveCardName) {
		this.saveCardName = saveCardName;
	}

}
