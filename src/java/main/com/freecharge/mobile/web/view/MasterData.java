package com.freecharge.mobile.web.view;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.mobile.util.MapperUtil;

public class MasterData {
	@Autowired 
	private CommonService commonService;
	
	@Autowired
	private OperatorCircleService circleService;
	
	public List<OperatorMaster> getDataCardOperators() {
		return commonService.getDatacardOperatorMasterList();
	}
	
	public String getDataCardOperatorId(String operatorName) {
		String operatorId = null;
		List<OperatorMaster> operators = getDataCardOperators();
		for (OperatorMaster operator : operators){
			if (operator != null && operatorName !=null && operator.getName().equalsIgnoreCase(operatorName)){
				operatorId = String.valueOf(operator.getOperatorMasterId());
				break;
			}
		}
		return operatorId;
	}
	
	public List<OperatorMaster> getDthOperators() {
		return commonService.getDTHOperatorMasterList();
	}
	
	public List<OperatorMaster> getMobileOperators() {
		return commonService.getMobileOperatorMasterList();
	}	
	
	public String getMobileOperatorId(String operatorName) {
		String operatorId = null;
		List<OperatorMaster> operators = getMobileOperators();
		for (OperatorMaster operator : operators){
			if (operator != null && operatorName !=null && operator.getName().equalsIgnoreCase(operatorName)){
				operatorId = String.valueOf(operator.getOperatorMasterId());
				break;
			}
		}
		return operatorId;
	}
	
	public String getDthOperatorId(String operatorName) {
		String operatorId = null;
		List<OperatorMaster> operators = getDthOperators();
		for (OperatorMaster operator : operators){
			if (operator != null && operatorName !=null && operator.getName().equalsIgnoreCase(operatorName)){
				operatorId = String.valueOf(operator.getOperatorMasterId());
				break;
			}
		}
		return operatorId;
	}
	
	
	public List<CircleMaster> getAllCircles() {
		return circleService.getCircles();
	}	

	
	 public List<StateMaster> getStateMasterList() {
		 return  commonService.getStateMasterList();
	 }

	public List<Circle> getMobileCircles() {
		List<CircleMaster> circleMasters = getAllCircles();
		List<Circle> circles = new ArrayList<Circle>();
		for (CircleMaster circleMaster : circleMasters) {
			if (circleMaster.getIsActive() && !circleMaster.getName().equalsIgnoreCase("ALL")) {
				Circle circle = new Circle();
				circle.setCircleId(String.valueOf(circleMaster
						.getCircleMasterId()));
				circle.setName(circleMaster.getName());
				circles.add(circle);
			}
		}
		return circles;
	}

	public List<City> getCitiesForState(Integer stateId) {
		CityMasterBusinessDO cityMasterBusinessDO = new CityMasterBusinessDO();
		cityMasterBusinessDO.setStateId(stateId);
		commonService.getCityMasterList(cityMasterBusinessDO);
		List<CityMaster> cityMasters = cityMasterBusinessDO.getCityMasterList();
		List<City> cityList = new ArrayList<City>();
		for (CityMaster cityMaster : cityMasters) {
			City city = new City();
			city =  MapperUtil.map(cityMaster, City.class);	
			cityList.add(city);
		}
		return cityList;
	}
	
}
