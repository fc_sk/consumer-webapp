package com.freecharge.mobile.web.view.payment;

import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.NotBlank;

import com.freecharge.mobile.validator.annotation.DebitCardTypeNumber;

@DebitCardTypeNumber
public class DebitCard {
	@NotBlank(message = "Card Type is required")
	private String cardType;
	
	@NotBlank(message = "Card Number is required")
	@Size(max=19, min=16, message="You are missing digits.")
	@CreditCardNumber(message = "Card number is InValid.")
	private String cardNumber;
	
	private String expiryYear;
	
	private String expiryMonth;
	
	@NotBlank(message = "Name on the card is required")
	@Pattern(regexp="[A-Za-z ]*", message="Name should contain letters only.") 
	@Size(max=50, message="Name should contain maximum 50 letters.")
	private String name;
	
	private String cvvNumber;
	
	private List<String> yearList;
	
	private List<DebitCardType> cardTypeList;
	
	private boolean isSelected;
	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}
	public String getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCvvNumber() {
		return cvvNumber;
	}
	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}
	public List<String> getYearList() {
		return yearList;
	}
	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}
	public List<DebitCardType> getCardTypeList() {
		return cardTypeList;
	}
	public void setCardTypeList(List<DebitCardType> cardTypeList) {
		this.cardTypeList = cardTypeList;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}	
}
