package com.freecharge.mobile.web.view;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class User {
	private String title;
	
	@NotBlank(message = "First Name can not be blank")
	@Size(min=4, max=25, message= "Name field should contain minimum 4 and maximum 25 characters.")

	@Pattern(regexp="[A-Za-z ]*", message="Name field should contain letters only.")
	private String firstName;
	
	
	@NotBlank(message = "Mobile Number can not be blank")
	@Size(min=10, max=10, message= "Mobile number should contain 10 digits.")
	@Pattern(regexp="[0-9]*", message="Mobile Number should contains only numbers.")
	private String mobileNo;
	
	@NotBlank(message = "Email can not be blank")
	@Email(message="Invalid email address")
	private String email;
	
	@NotBlank(message = "Password can not be blank")
	@Size(min=6, message="Password field should contain minimum 6 characters.")
	private String password;
	
	private String confirmPassword;
	

	@Size(max=50,message="Address field should be maximum 50 characters.")
	private String address1;
	

	@Size(max=50,message="Area field should be maximum 50 characters.")
	private String area;
	
	@Size(max=50,message="Landmark field should be maximum 50 characters.")
	private String landmark;
	
	private Integer stateId;
	
	private Integer cityId;

	private List<City> cityList;
	

	@Size(min=6, max=6, message="Pincode number should contain only 6 digits")
	@Pattern(regexp="[0-9]*", message="Pincode field should contains only numbers.")
	private String postalCode;

	private String signupFrom;
	
	private Map<String, String[]> fromParamMap;
	
	private MasterData masterData;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public MasterData getMasterData() {
		return masterData;
	}
	
	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	public String getSignupFrom() {
		return signupFrom;
	}
	public void setSignupFrom(String signupFrom) {
		this.signupFrom = signupFrom;
	}
	public Map<String, String[]> getFromParamMap() {
		return fromParamMap;
	}
	public void setFromParamMap(Map<String, String[]> fromParamMap) {
		this.fromParamMap = fromParamMap;
	}
	
}
