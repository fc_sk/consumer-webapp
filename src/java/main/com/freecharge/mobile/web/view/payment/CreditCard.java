package com.freecharge.mobile.web.view.payment;

import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.NotBlank;

import com.freecharge.mobile.validator.annotation.CreditCardInput;

@CreditCardInput
public class CreditCard {
	
	@NotBlank(message = "Card Number is required")
	@Size(max=19, min=16, message="You are missing digits.")
	@CreditCardNumber(message = "Card number is InValid.")
	private String cardNumber;
	
	@NotBlank(message = "Card expiry year is required")
	private String expiryYear;
	
	@NotBlank(message = "Card expiry month is required")
	private String expiryMonth;
	
	@NotBlank(message = "Name on the card is required")
	@Pattern(regexp="[A-Za-z ]*", message="Name field should contains letters only.")
	@Size(max=50, message="Name should contain maximum 50 letters.")
	private String name;
	
	@NotBlank(message = "CVV number is required")
	@Size(max=3, min=3, message="CVV field should contain exactly 3 numbers")
	private String cvvNumber;
	
	private boolean isSelected;
	
	private List<String> yearList;
	
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCvvNumber() {
		return cvvNumber;
	}

	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}

	public String getExpiryYear() {
		return expiryYear;
	}

	public void setExpiryYear(String expiryYear) {
		this.expiryYear = expiryYear;
	}

	public String getExpiryMonth() {
		return expiryMonth;
	}

	public void setExpiryMonth(String expiryMonth) {
		this.expiryMonth = expiryMonth;
	}

	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

}

