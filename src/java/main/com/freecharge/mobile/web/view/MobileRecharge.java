package com.freecharge.mobile.web.view;

import com.freecharge.mobile.validator.annotation.MobileNumber;
import com.freecharge.mobile.validator.annotation.MobileOperator;
import com.freecharge.mobile.validator.annotation.MobileRechargeAmount;

public class MobileRecharge {
	@MobileNumber
	private String mobileNumber;
	
	@MobileRechargeAmount
	private String amount;
	
	@MobileOperator
	private String operator;
	
	private String circle;
	
	private MasterData masterData;
	
	private String rechargetype;
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public MasterData getMasterData() {
		return masterData;
	}
	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}

    public String getRechargeType() {
        return rechargetype;
    }

    public void setRechargetype(String rechargetype) {

        this.rechargetype = rechargetype;
    }
	
}
