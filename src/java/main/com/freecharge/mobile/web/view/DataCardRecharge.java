package com.freecharge.mobile.web.view;

import com.freecharge.mobile.validator.annotation.DatacardNumber;
import com.freecharge.mobile.validator.annotation.DatacardOperator;
import com.freecharge.mobile.validator.annotation.DatacardRechargeAmount;

public class DataCardRecharge {
	@DatacardNumber
	private String datacardNumber;
	
	@DatacardRechargeAmount
	private String datacardAmount;
	
	@DatacardOperator
	private String datacardOperator;
	
	private String datacardCircle;
	
	private MasterData masterData;
	
	public String getDatacardNumber() {
		return datacardNumber;
	}
	public void setDatacardNumber(String datacardNumber) {
		this.datacardNumber = datacardNumber;
	}
	public MasterData getMasterData() {
		return masterData;
	}
	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}
	public String getDatacardAmount() {
		return datacardAmount;
	}
	public void setDatacardAmount(String datacardAmount) {
		this.datacardAmount = datacardAmount;
	}
	public String getDatacardOperator() {
		return datacardOperator;
	}
	public void setDatacardOperator(String datacardOperator) {
		this.datacardOperator = datacardOperator;
	}
	public String getDatacardCircle() {
		return datacardCircle;
	}
	public void setDatacardCircle(String datacardCircle) {
		this.datacardCircle = datacardCircle;
	}
	
}
