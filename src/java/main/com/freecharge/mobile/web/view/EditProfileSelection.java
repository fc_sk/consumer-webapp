package com.freecharge.mobile.web.view;

public class EditProfileSelection {
	private boolean isProfileSelected;
	private boolean isPasswordSelected;
	private boolean isAddressSelected;
	
	public boolean isProfileSelected() {
		return isProfileSelected;
	}
	public void setProfileSelected(boolean isProfileSelected) {
		this.isProfileSelected = isProfileSelected;
	}
	public boolean isPasswordSelected() {
		return isPasswordSelected;
	}
	public void setPasswordSelected(boolean isPasswordSelected) {
		this.isPasswordSelected = isPasswordSelected;
	}
	public boolean isAddressSelected() {
		return isAddressSelected;
	}
	public void setAddressSelected(boolean isAddressSelected) {
		this.isAddressSelected = isAddressSelected;
	}
}
