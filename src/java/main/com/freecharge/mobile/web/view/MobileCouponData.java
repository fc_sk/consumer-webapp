package com.freecharge.mobile.web.view;

import java.util.List;
import java.util.Map;

import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.api.coupon.service.web.model.VoucherCat;

public class MobileCouponData {
	private List<VoucherCat> categories;
	private Map<String, List<CouponVO>> categoryMap;
	
	public List<VoucherCat> getCategories() {
		return categories;
	}
	public void setCategories(List<VoucherCat> categories) {
		this.categories = categories;
	}
	public Map<String, List<CouponVO>> getCategoryMap() {
		return categoryMap;
	}
	public void setCategoryMap(Map<String, List<CouponVO>> categoryMap) {
		this.categoryMap = categoryMap;
	}
	
}
