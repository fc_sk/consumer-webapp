package com.freecharge.mobile.web.view;

import java.util.Map;

public class MobileLogin {
	private String loginEmail;
	private String loginPassword;
	private String loginFrom;
	private String loginError;
	private Map<String, String[]> fromParamMap;
	public Map<String, String[]> getFromParamMap() {
		return fromParamMap;
	}
	public void setFromParamMap(Map<String, String[]> fromParamMap) {
		this.fromParamMap = fromParamMap;
	}
	public String getLoginEmail() {
		return loginEmail;
	}
	public void setLoginEmail(String loginEmail) {
		this.loginEmail = loginEmail;
	}
	public String getLoginPassword() {
		return loginPassword;
	}
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}
	public String getLoginError() {
		return loginError;
	}
	public void setLoginError(String loginError) {
		this.loginError = loginError;
	}
	public String getLoginFrom() {
		return loginFrom;
	}
	public void setLoginFrom(String loginFrom) {
		this.loginFrom = loginFrom;
	}
	
}
