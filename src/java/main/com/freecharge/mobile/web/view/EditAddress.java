package com.freecharge.mobile.web.view;

import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;

public class EditAddress {
	private String area;
	private String address;
	private String landmar;
	private String pincode;
	private Integer state;
	private Integer city;
	private List<StateMaster> statelist;
	private List<CityMaster> citylist;
	
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLandmar() {
		return landmar;
	}
	public void setLandmar(String landmar) {
		this.landmar = landmar;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Integer getCity() {
		return city;
	}
	public void setCity(Integer city) {
		this.city = city;
	}
	public List<StateMaster> getStatelist() {
		return statelist;
	}
	public void setStatelist(List<StateMaster> statelist) {
		this.statelist = statelist;
	}
	public List<CityMaster> getCitylist() {
		return citylist;
	}
	public void setCitylist(List<CityMaster> citylist) {
		this.citylist = citylist;
	}
}
