package com.freecharge.mobile.web.view;


public class OperatorCircle {
	private String operatorId;
	private Circle circle;
	private Boolean isOperatorUp;
	
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	
	public Circle getCircle() {
		return circle;
	}
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	
    public Boolean getIsOperatorUp() {
        return isOperatorUp;
    }
    public void setIsOperatorUp(Boolean isOperatorUp) {
        this.isOperatorUp = isOperatorUp;
    }
}
