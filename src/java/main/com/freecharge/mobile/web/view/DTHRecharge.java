package com.freecharge.mobile.web.view;

import com.freecharge.mobile.validator.annotation.DTHInput;

@DTHInput
public class DTHRecharge {

	private String dthNumber;

	private String dthAmount;

	private String dthOperator;

	private MasterData masterData;
	
	public String getDthNumber() {
		return dthNumber;
	}

	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}

	public String getDthOperator() {
		return dthOperator;
	}

	public void setDthOperator(String dthOperator) {
		this.dthOperator = dthOperator;
	}

	public MasterData getMasterData() {
		return masterData;
	}

	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}

	public String getDthAmount() {
		return dthAmount;
	}

	public void setDthAmount(String dthAmount) {
		this.dthAmount = dthAmount;
	}	
}
