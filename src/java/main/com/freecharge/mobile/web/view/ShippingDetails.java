package com.freecharge.mobile.web.view;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class ShippingDetails {

    @NotBlank(message = "Firstname can not be blank")
    private String firstName;
    @NotBlank(message = "Address can not be blank")
    private String address1;private String cityName;
    @NotBlank(message = "Postalcode can not be blank")
    private String postalCode;
    private String stateName;
    private String countryName;
    @NotNull(message = "State can not be null")
    private Integer stateId;
    private Integer countryId;
    private String street;
    @NotBlank(message = "Area can not be blank")
    private String area;
    private String landmark;
    @NotBlank(message = "title can not be blank")
    private String title;
    private Integer cityId;
    private List<City> cityList;
    private MasterData masterData;
    
	
	private Map<String, Object> miscCheckoutDetailsMap;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Map<String, Object> getMiscCheckoutDetailsMap() {
		return miscCheckoutDetailsMap;
	}
	public void setMiscCheckoutDetailsMap(Map<String, Object> miscCheckoutDetailsMap) {
		this.miscCheckoutDetailsMap = miscCheckoutDetailsMap;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	public MasterData getMasterData() {
		return masterData;
	}
	public void setMasterData(MasterData masterData) {
		this.masterData = masterData;
	}
	
}
