package com.freecharge.mobile.web.view.payment;

import javax.validation.Valid;

import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

public class PaymentOptions {
	
	//For partial payment
	private WalletEligibility walletEligibility;
	private PaymentBreakup paymentBreakup;
	//end of partial payment
	
	private Wallet wallet;
	
	@Valid
	private CreditCard creditCard;
	
	@Valid
	private DebitCard debitCard;
 
	@Valid
	private InternetBanking netBanking;

	@Valid
	private CardStorage cardStorage;
	
	public CardStorage getCardStorage() {
		return cardStorage;
	}

	public void setCardStorage(CardStorage cardStorage) {
		this.cardStorage = cardStorage;
	}

	private Boolean usePartialPayment;
	
	public Boolean getUsePartialPayment() {
		return usePartialPayment;
	}

	public void setUsePartialPayment(Boolean usePartialPayment) {
		this.usePartialPayment = usePartialPayment;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public DebitCard getDebitCard() {
		return debitCard;
	}

	public void setDebitCard(DebitCard debitCard) {
		this.debitCard = debitCard;
	}

	public InternetBanking getNetBanking() {
		return netBanking;
	}

	public void setNetBanking(InternetBanking netBanking) {
		this.netBanking = netBanking;
	}
	
	//getter & setter for partial payment


	public PaymentBreakup getPaymentBreakup() {
		return paymentBreakup;
	}

	public WalletEligibility getWalletEligibility() {
		return walletEligibility;
	}

	public void setWalletEligibility(WalletEligibility walletEligibility) {
		this.walletEligibility = walletEligibility;
	}

	public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
		this.paymentBreakup = paymentBreakup;
	}
	//end of partial payment
}
