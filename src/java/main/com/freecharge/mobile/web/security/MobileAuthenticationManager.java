package com.freecharge.mobile.web.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;

public class MobileAuthenticationManager implements AuthenticationManager{

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		List<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
		auths.add(new GrantedAuthorityImpl("ROLE_FC_USER"));
		if (authentication.getName().equals("test")){
			 return new UsernamePasswordAuthenticationToken(
				     "testa",
				     "testp",
				     auths);
		}
		return null;
	}


}
