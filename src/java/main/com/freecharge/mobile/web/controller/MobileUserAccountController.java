package com.freecharge.mobile.web.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.MyAccountService;
import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.CaptchaService;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.validator.ChangePasswordValidator;
import com.freecharge.mobile.validator.MyProfileValidator;
import com.freecharge.mobile.validator.ProfileAddressValidator;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.EditAddress;
import com.freecharge.mobile.web.view.EditPassword;
import com.freecharge.mobile.web.view.EditProfile;
import com.freecharge.mobile.web.view.EditProfileSelection;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.AddCashWebDO;
import com.freecharge.web.webdo.HomeWebDo;
import com.freecharge.web.webdo.MyContactsListWebDo;
import com.freecharge.web.webdo.MyContactsWebDO;
import com.freecharge.web.webdo.MyRechargesWebDO;
import com.freecharge.web.webdo.UserDO;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileUserAccountController {

	@Autowired
	private MyAccountService myAccountService;
	
	@Autowired
	private WalletService walletService;
	 
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	 
	@Autowired
	private InService inService;
	
	@Autowired
	private EditProfileSelection editProfileSelection;
	
	@Autowired
    private CommonService commonService;
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Autowired
    private AppConfigService appConfigService;
	
	@Autowired
    private CaptchaService captchaService;
	
	@Autowired
    private MetricsClient              metricsClient;
	
	@Autowired
	WalletWrapper walletWrapper;
	
	private Logger logger = LoggingFactory.getLogger(getClass());

	@RequestMapping(value = "myaccount")
	public ModelAndView myaccount(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		return new ModelAndView("mobile/myaccount/view", model);
	}

	@RequestMapping(value = "mycredits")
	public ModelAndView fcBalance(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Object userIdObject = map.get(WebConstants.SESSION_USER_USERID);
        int userID = userIdObject == null ? -1 : (Integer) userIdObject;
        Amount balance = walletWrapper.getTotalBalance(userID);
        Map<String, Object> lastEntry = walletService.findLastEntry(userID);
        
        if (request.getParameter("Paymentstatus") != null && map.get("paymentStatus") != null) {
            model.addAttribute("paymentStatus", true);
            map.remove("paymentStatus");
        } else {
            model.addAttribute("paymentStatus", false);
        }
        model.addAttribute("balance", balance.getAmount());
        model.addAttribute("lastEntry", lastEntry);
       
        
		return new ModelAndView("mobile/mycredits/view", model);
	}


	@RequestMapping(value = "addcash")
    public ModelAndView addcash(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	    model.addAttribute("addCashWebDo", new AddCashWebDO());
        model.addAttribute("isAddCash",request.getParameter("isAddCash"));
	    return new ModelAndView("mobile/addcash/view", model);
	}
	
	@RequestMapping(value = "freefund")
    public ModelAndView freeFund(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
	    if(appConfigService.isRecaptchaEnabled()) {
            model.addAttribute(WebConstants.FORCE_CAPTCHA, captchaService.shouldCheckCaptcha(fs));
        } 
        return new ModelAndView("mobile/freefund/view", model);
    }
	
	@RequestMapping(value = "editprofile")
    public ModelAndView editProfile(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	        ProfileBusinessDO profilebusinessdo = getProfileBusinessDo();
	        EditProfile editProfile = MobileUtil.initializeProfile(profilebusinessdo);
            String usertitle[] = { "Mr", "Ms", "Mrs" };
            model.addAttribute("usertitle", usertitle);
            model.addAttribute(editProfile);
        return new ModelAndView("mobile/editprofile/view", model);
    }

    private ProfileBusinessDO getProfileBusinessDo() {
        String email = null;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) 
        {
            email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
        }   
            
      ProfileBusinessDO profilebusinessdo=new ProfileBusinessDO();
      profilebusinessdo.setEmail(email);
      myAccountService.getProfile(profilebusinessdo);
        return profilebusinessdo;
    }
	
	@RequestMapping(value = "change-password")
    public ModelAndView changePassword(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	       model.addAttribute(new EditPassword());
        return new ModelAndView("mobile/change-password/view", model);
    }
	
	@RequestMapping(value = "edit-address")
    public ModelAndView editAddress(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	        ProfileBusinessDO profilebusinessdo = getProfileBusinessDo();
	        EditAddress editAddress = MobileUtil.initializeAddress(profilebusinessdo);
	        model.addAttribute(editAddress); 
        return new ModelAndView("mobile/edit-address/view", model);
    }
	
	@RequestMapping(value = "credits-activity")
    public ModelAndView balanceActivity(HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
	    
	    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Object userIdObject = map.get(WebConstants.SESSION_USER_USERID);
        int userID = userIdObject == null ? -1 : (Integer) userIdObject;
        Map<String, Object> lastEntry = walletService.findLastEntry(userID);
        List<Map<String, Object>> rows = walletService.getBalanceRows(userID, lastEntry,true);
        if (rows != null){
        	for(int i=0;i<rows.size();i++)
            {
                DateFormat walletDisplayDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String transactionTime = (String)rows.get(i).get("created_ts");   
               //Timestamp lastEntryTime = (Timestamp)  rows.get(i).get("created_ts");
                Date date = new Date(transactionTime);
                String formattedLastEntryTime = walletDisplayDateFormat.format(date);
                rows.get(i).put("created_ts", formattedLastEntryTime);
                BigDecimal txnamount = (BigDecimal)rows.get(i).get("TXN_AMOUNT");   
                BigDecimal newbalance = (BigDecimal)rows.get(i).get("NEW_BALANCE");   
                rows.get(i).put("TXN_AMOUNT", txnamount.toBigInteger());
                rows.get(i).put("NEW_BALANCE", newbalance.toBigInteger());
            }
        }
        model.addAttribute("rows", rows);
        return new ModelAndView("mobile/balance-activity/view", model);
    }
	
	@RequestMapping(value = "mycontacts")
	public ModelAndView getmyContacts(HttpServletRequest request, HttpServletResponse response, Model model) {
		try {
			String pageTitle = "FreeCharge mycontacts";
			boolean islogin = false;
			String email = null;
			Integer userid = null;
			String delegateName = WebConstants.DELEGATE_BEAN_USERCONTACTS;
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			if (!islogin || email == null) {
				return new ModelAndView("redirect:/");
			}
			MyContactsListWebDo contactslistwebdo = new MyContactsListWebDo();
			contactslistwebdo.setEmail(email);
			contactslistwebdo.setUserid(userid);
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, contactslistwebdo);
			contactslistwebdo = ((MyContactsListWebDo) (webContext.getBaseWebDO()));
			model.addAttribute("title", pageTitle);
			model.addAttribute("userconts", contactslistwebdo);
			model.addAttribute("usercontact", new MyContactsWebDO());
		} catch (Exception e) {
			logger.error(e);
		}

		return new ModelAndView("mobile/mycontacts/view");
	}
	
	@RequestMapping(value = "myprofile", method = RequestMethod.GET)
	public ModelAndView myProfile(HttpServletRequest request, HttpServletResponse response, Model model) {
		logger.info("entered into the myprofile method of userprofilecontroller ....");
		String view = "mobile/myprofile/view";
		try {
			ProfileBusinessDO profilebusinessdo = getProfileBusinessDo();
			
			EditProfile editProfile = MobileUtil.initializeProfile(profilebusinessdo);
			EditAddress editAddress = MobileUtil.initializeAddress(profilebusinessdo);
			EditPassword editPassword = new EditPassword();		
			
			model.addAttribute("editProfile",editProfile);
			model.addAttribute("editPassword",editPassword);
			model.addAttribute("editAddress",editAddress);
	
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			logger.info("Leaving From the myprofile method of userprofilecontroller ....");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
		} catch (Exception exp) {
			logger.error("exception raised while getting profile info , exp details are", exp);
			return new ModelAndView(view);// currently to the same view but have
											// to be redirected to error views.
		}
		return new ModelAndView(view);
	}
	
	@RequestMapping(value = "myrecharges", method = RequestMethod.GET)
	public ModelAndView myRecharges(HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "mobile/myrecharges/view";
		model.addAttribute("homeWebDo", new HomeWebDo());
		model.addAttribute("userDO", new UserDO());

		logger.info("Entered into the myrecharges Method");
		try {
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			Boolean isloggedin = false;
			Integer userid = null;
			if (map != null) {
				String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				request.setAttribute("email", email);
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
					userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
				}
			}
			if (isloggedin) {
				model.addAttribute("title", "Freecharge Reccharge History");
				String delegateName = WebConstants.DELEGATE_MY_RECHARGES;
				MyRechargesWebDO rechargewebdo = new MyRechargesWebDO();
				rechargewebdo.setUserid(userid);
				logger.info("in my profile method");
				WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, rechargewebdo);
				/*
				 * model.addAttribute("loginWebDO", new LoginWebDO()); model.addAttribute("myprofile", ((MyProfileWebDO) (webContext.getBaseWebDO()))); model.addAttribute("stateMaster", ((MyProfileWebDO) (webContext.getBaseWebDO())).getStateMaster());
				 */
				model.addAttribute("rechargeHistoryList", ((MyRechargesWebDO) (webContext.getBaseWebDO())).getRechargeHistoryList());
				/*
				 * model.addAttribute("totalRecords" , ((MyProfileWebDO) (webContext.getBaseWebDO())).getTotalRecords() ); model.addAttribute("currentPage" , ((MyProfileWebDO) (webContext.getBaseWebDO())).getPageNo()); model.addAttribute("recordsPerPage" , FCConstants.RECORDSPERPAGE);
				 */
				logger.info("Leaving from the myrecharges Method");
				return new ModelAndView(view);
			} else {
				//String homeView = "home/view";
				return new ModelAndView("redirect:/");

			}
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			return new ModelAndView("error");
		} catch (Exception exp) {
			logger.error("exception raised while getting profile info , exp details are", exp);
			return new ModelAndView(view);
		}

	}
	
	@RequestMapping(value = "mobile-updateprofile", method = RequestMethod.POST)
	public ModelAndView updateProfile(@ModelAttribute(value = "editProfile") @Valid EditProfile editProfile, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {	
		String view = "mobile/editprofile/view";		
		String message = languageUtility.getLocaleMessage("m.profile.edit.success",null);
		String errmessage = languageUtility.getLocaleMessage("m.profile.edit.failure",null);
		long startTime = System.currentTimeMillis();
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "mobileUserAccountController3");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			String email = null;
			Integer userid = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			
			editProfileSelection.setProfileSelected(true);
			editProfileSelection.setPasswordSelected(false);
			editProfileSelection.setAddressSelected(false);
			
			MyProfileValidator myProfileValidator = new MyProfileValidator();
			myProfileValidator.validate(editProfile, bindingResult);
			
			ProfileBusinessDO profilebusinessdo=new ProfileBusinessDO();
			profilebusinessdo.setEmail(email);
			myAccountService.getProfile(profilebusinessdo);
			
			EditAddress editAddress = MobileUtil.initializeAddress(profilebusinessdo);
			EditPassword editPassword = new EditPassword();
	
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			model.addAttribute("editPassword",editPassword);
			model.addAttribute("editAddress",editAddress);
			
			model.addAttribute("editProfileSelection",editProfileSelection);
				
			if (bindingResult.hasErrors()) {
			    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
				return new ModelAndView(view);
			}
			/* if no validation error set new form field values to businessdo*/
			profilebusinessdo.setTitel(editProfile.getTitle());
			profilebusinessdo.setName(editProfile.getName());
			profilebusinessdo.setMobileNo(editProfile.getMobileNo());
			profilebusinessdo.setIspasswordupdate(false);
			profilebusinessdo.setUserid(userid);
			myAccountService.updateProfile(profilebusinessdo);
			/*Setting values to models after updating the database profile */
			model.addAttribute("editProfile",editProfile);
			model.addAttribute("updprofilestatus", true);
			model.addAttribute("message", message);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			model.addAttribute("errmessage", errmessage);
			model.addAttribute("updprofilestatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		} catch (Exception exp) {
			logger.error(" freecharge exception raised while updating the profile ", exp);
			model.addAttribute("errmessage", errmessage);
			model.addAttribute("updprofilestatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return new ModelAndView(view);
	}
	
	@RequestMapping(value = "mobile-editpassword", method = RequestMethod.POST)
	public ModelAndView changeAddress(@ModelAttribute(value = "editPassword") @Valid EditPassword editPassword, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "mobile/change-password/view";	
		String message = languageUtility.getLocaleMessage("m.password.edit.success",null);
		String errmessage = languageUtility.getLocaleMessage("m.password.edit.failure",null);
		long startTime = System.currentTimeMillis();
		boolean ispswdcorrect =false;
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "mobileUserAccountController1");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			String email = null;
			Integer userid = null;		
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			
			editProfileSelection.setPasswordSelected(true);
			editProfileSelection.setProfileSelected(false);
			editProfileSelection.setAddressSelected(false);

			model.addAttribute("editProfileSelection",editProfileSelection);
			ChangePasswordValidator changePasswordValidator = new ChangePasswordValidator();
			changePasswordValidator.validate(editPassword, bindingResult);
			String oldpswd = editPassword.getOldpassword();
			/*Check entered old password is exist in database or not */
			if (oldpswd != null && (!(oldpswd.isEmpty()))) {
				ProfileBusinessDO profileBusinessDO = new ProfileBusinessDO();
				profileBusinessDO.setEmail(email);
				profileBusinessDO.setOldpassword(oldpswd);
				profileBusinessDO.setUserid(userid);
				myAccountService.checkUserOldPassword(profileBusinessDO);
				ispswdcorrect = profileBusinessDO.isIsoldpswdcorrect();				
				if (ispswdcorrect == false) {
					
					bindingResult.rejectValue("oldpassword", "m.provide.oldpassword");
				}
			}
			/* Get profile from database for email*/		
			ProfileBusinessDO profilebusinessdo=new ProfileBusinessDO();
			profilebusinessdo.setEmail(email);
			myAccountService.getProfile(profilebusinessdo);
			
			EditProfile editProfile = MobileUtil.initializeProfile(profilebusinessdo);
			EditAddress editAddress = MobileUtil.initializeAddress(profilebusinessdo);
			model.addAttribute("editProfile",editProfile);
			model.addAttribute("editAddress",editAddress);
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			
			/*Check for validation error */
			if (bindingResult.hasErrors()) {
			    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "fail");
				return new ModelAndView(view);
			}
			
			/* if no validation error set businessdo */
			profilebusinessdo.setOldpassword(editPassword.getOldpassword());
			profilebusinessdo.setNewpassword(editPassword.getNewpassword());
			profilebusinessdo.setConfirmpassword(editPassword.getConfirmpassword());
			profilebusinessdo.setIspasswordupdate(true);
			profilebusinessdo.setUserid(userid);
			myAccountService.updateProfile(profilebusinessdo);	
			editPassword.setConfirmpassword(null);
			editPassword.setNewpassword(null);
			editPassword.setOldpassword(null);
			model.addAttribute("editPassword",editPassword);
			model.addAttribute("updpaswstatus", true);
			model.addAttribute("message", message);
			logger.info("Leaving From the updateProfile method of userprofilecontroller ....");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			model.addAttribute("errmessage", errmessage);
			model.addAttribute("updpaswstatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
		} catch (Exception exp) {
			logger.error(" freecharge exception raised while updating the profile ", exp);
			model.addAttribute("errmessage", errmessage);
			model.addAttribute("updpaswstatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return new ModelAndView(view);		
	}
	
	@RequestMapping(value = "mobile-updateaddress", method = RequestMethod.POST)
	public ModelAndView updateAddress(@ModelAttribute(value = "editAddress") EditAddress editAddress, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "mobile/edit-address/view";
		String message = languageUtility.getLocaleMessage("m.address.edit.success",null);
		String errmessage = languageUtility.getLocaleMessage("m.address.edit.failure",null);
		long startTime = System.currentTimeMillis();
		
		try {		
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "mobileUserAccountController2");
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			String email = null;
			Integer userid = null;
			if (map != null && map.containsKey(WebConstants.SESSION_USER_IS_LOGIN) && map.containsKey(WebConstants.SESSION_USER_EMAIL_ID)) {
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				userid = (Integer) map.get(WebConstants.SESSION_USER_USERID);
			}
			
			editProfileSelection.setAddressSelected(true);
			editProfileSelection.setPasswordSelected(false);
			editProfileSelection.setProfileSelected(false);
			
			model.addAttribute("editProfileSelection",editProfileSelection);
			/* Get profile from database for email*/		
			ProfileBusinessDO profilebusinessdo=new ProfileBusinessDO();
			profilebusinessdo.setEmail(email);
			myAccountService.getProfile(profilebusinessdo);
			
			ProfileAddressValidator profileAddressValidator = new ProfileAddressValidator();
			profileAddressValidator.validate(editAddress, bindingResult);
			
			EditProfile editProfile = MobileUtil.initializeProfile(profilebusinessdo);			
			EditPassword editPassword = new EditPassword();
			model.addAttribute("editProfile",editProfile);
			model.addAttribute("editPassword",editPassword);
			String usertitle[] = { "Mr", "Ms", "Mrs" };
			model.addAttribute("usertitle", usertitle);
			
			/* Getting state and city list and set into editAddress */
			editAddress.setStatelist(profilebusinessdo.getStatelist());
			CityMasterBusinessDO baseBusinessDO = new CityMasterBusinessDO();
			baseBusinessDO.setStateId(editAddress.getState());
			commonService.getCityMasterList(baseBusinessDO);
			editAddress.setCitylist(baseBusinessDO.getCityMasterList());
			
			/*Check for validation error */
			if (bindingResult.hasErrors()) {
			    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "fail");
				return new ModelAndView(view);
			}
		
			/* if no validation error set businessdo */
			profilebusinessdo.setAddress(editAddress.getAddress());
			profilebusinessdo.setArea(editAddress.getArea());
			profilebusinessdo.setLandmar(editAddress.getLandmar());
			profilebusinessdo.setState(editAddress.getState().toString());
			profilebusinessdo.setCity(editAddress.getCity().toString());
			profilebusinessdo.setPincode(editAddress.getPincode());
			profilebusinessdo.setStatelist(editAddress.getStatelist());
			profilebusinessdo.setCitylist(editAddress.getCitylist());
			profilebusinessdo.setUserid(userid);
			myAccountService.updateAddress(profilebusinessdo);	
			model.addAttribute("editAddress",editAddress);
			model.addAttribute("updaddressstatus", true);
			model.addAttribute("message", message);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "success");
		} catch (Exception e) {
			logger.error(e);
			model.addAttribute("errmessage", errmessage);
			model.addAttribute("updaddressstatus", false);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "updateProfile", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "updateProfile", endTime - startTime);
		return new ModelAndView(view);
	}
}
