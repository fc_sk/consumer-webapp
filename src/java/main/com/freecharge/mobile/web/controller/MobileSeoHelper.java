package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.seo.businessdo.SEOAttributesBusinessDO;
import com.freecharge.seo.service.NewSEOService;
import com.freecharge.web.util.WebConstants;

@Component
public class MobileSeoHelper {
    
    @Autowired
    NewSEOService newSEOService;
    
	private static final Logger logger = LoggingFactory.getLogger(MobileSeoHelper.class);
	
	public void handleRechargeSEO(HttpServletRequest request, Model model){
        try {
            if (!StringUtils.isEmpty(request.getParameter(WebConstants.PRODUCT_TYPE))) {
                SEOAttributesBusinessDO seoBusinessDO = newSEOService.populateSeoAttributes(
                        request.getParameter(WebConstants.PRODUCT_TYPE), request.getParameter(WebConstants.OPERATOR),
                        Integer.toString(FCConstants.CHANNEL_ID_MOBILE));
                model.addAttribute("seoAttributes", seoBusinessDO);
            }
			
			String startUrl = String.valueOf(request.getAttribute("javax.servlet.forward.request_uri"));
            if (startUrl != null && !startUrl.equalsIgnoreCase("null") && startUrl.startsWith("/m")
            		&& !isMobileRechargeUrl(startUrl)){
            	startUrl = startUrl.replaceFirst("/m", "");
            	if (startUrl.length() > 1){
            		model.addAttribute("rechargeStartUrl", startUrl);
            	}
            }
            
			model.addAttribute("page", "recharge");
		} catch (Exception e) {
			logger.error("Error occured while handling SEO for mobile site: " + e.getMessage());
		}
	}

	private static boolean isMobileRechargeUrl(String startUrl) {
		return (startUrl.equals("mobile-recharge") || startUrl.equals("dth-recharge") || startUrl.equals("datacard-recharge"));
	}
}
