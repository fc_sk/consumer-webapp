package com.freecharge.mobile.web.controller;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.helper.PaymentOptionsHelper;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.ShippingDetails;
import com.freecharge.payment.services.FraudCheckService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Map;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileProfileController {
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private MobileCheckoutService mobileCheckoutService;
	
	@Autowired
	private WalletService walletService;
	
	@Autowired
	private PaymentOptionsHelper paymentOptionsHelper;
	
	@Autowired
	private MasterData masterData;
	
	@Autowired
    private CartService cartService;
	
	@Autowired
    private FreefundService freefundService;
	
	@Autowired
    private FraudCheckService fraudCheckService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private PricingService pricingService;

	@Autowired
	private MetricsClient metricsClient;
	
	@Autowired
	private WalletWrapper walletWrapper;
	
	@RequestMapping(value = "saveAddress", method = RequestMethod.POST)
	public ModelAndView saveAddressPost(@ModelAttribute(value = "shippingDetails") ShippingDetails shippingDetails , BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mpcSaveAddressPost");

		try {
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Boolean isloggin = false;
			String email = "";
			String lookupId = "";
			if (fs != null) {
				Map map = fs.getSessionData();

				if (map != null) {
					email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
					if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
						isloggin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
					}
				}
			}
			if (isloggin) {
				validator.validate(shippingDetails, bindingResult);
				if (!bindingResult.hasErrors()) {
					MobileUtil.formatShippingDetails(shippingDetails);
					Map<String, Object> miscCheckoutDetailsMap = shippingDetails.getMiscCheckoutDetailsMap();
					if (miscCheckoutDetailsMap != null) {
						String isEdit = (String) miscCheckoutDetailsMap.get("isEdit");
					
						if (!StringUtils.isEmpty(isEdit) && isEdit.compareToIgnoreCase("yes") == 0) {
							AddressBusinessDO addressBusinessDO =  MobileUtil.initializeAddressBusinessDO(shippingDetails);
							registrationService.saveAddress(addressBusinessDO);
						}
					
						WalletEligibility walletEligibility = new WalletEligibility();

						PaymentBreakup paymentBreakup = new PaymentBreakup();
						lookupId = (String) miscCheckoutDetailsMap.get("lookupId");
						
						// For partial payment
						if (walletService.isWalletDataDisplayEnabled()) {	
							walletEligibility = paymentOptionsHelper.getWalletEligibility(lookupId);
							paymentBreakup = paymentOptionsHelper.getPaymentBreakup(lookupId);
						}
						
						CheckoutData checkoutData = new CheckoutData(); 
						checkoutData = mobileCheckoutService.proceedWithCheckout(checkoutData,
																		lookupId, 
																		Double.parseDouble((String) miscCheckoutDetailsMap.get("totalAmount")), 
																		walletEligibility,
																		paymentBreakup,
																		(String) miscCheckoutDetailsMap.get("serviceNumber"), 
																		Float.parseFloat((String) miscCheckoutDetailsMap.get("rechargeAmount")), 
																		(String) miscCheckoutDetailsMap.get("productType"));
						
						Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
						model.addAttribute("payableTotalAmount",payableTotalAmount);
						model.addAttribute("checkoutData", checkoutData);
						return new ModelAndView("mobile/promocode/view", model);
					}
				} else {
					StringBuffer errors = new StringBuffer();
					for (ObjectError object : bindingResult.getAllErrors()) {
						errors = errors.append(object.getDefaultMessage() + "<br/>");
					}
					repopulateMasterData(shippingDetails);
					model.addAttribute("errors", errors.toString());
					return new ModelAndView("mobile/shippingdetails/view");
				}
			} else {
				model.addAttribute("status", "sessionExpired");
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
		

		} catch (Exception e) {
			logger.error("Exception caught in MobileProfileController while saving address", e);
			model.addAttribute("status", "fail");
			return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
		}
		
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	
	@RequestMapping(value = "paymentPage", method = RequestMethod.POST)
    public ModelAndView PaymentPageRedirectPost(@ModelAttribute(value = "checkoutData") CheckoutData checkoutData , BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mpcPaymentPageRedirectPost");
        try {
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            Boolean isloggin = false;
            String email = "";
            String lookupId = "";
            if (fs != null) {
                Map map = fs.getSessionData();

                if (map != null) {
                    email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
                    if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                        isloggin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                    }
                }
            }
            if (isloggin) {
                
                if (!bindingResult.hasErrors()) {
                 
                        WalletEligibility walletEligibility = new WalletEligibility();

                        PaymentBreakup paymentBreakup = new PaymentBreakup();
                        lookupId = checkoutData.getLookupId();
                        
                        // For partial payment
                        if (walletService.isWalletDataDisplayEnabled()) {   
                            walletEligibility = paymentOptionsHelper.getWalletEligibility(lookupId);
                            paymentBreakup = paymentOptionsHelper.getPaymentBreakup(lookupId);
                        }
                        
                        //Code for check partial payment eligibiliy in case of freefund apply
                        
                        CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                        
                        Object userIdObject = fs.getSessionData().get(
                                WebConstants.SESSION_USER_USERID);
                        int userId = userIdObject == null ? -1 : (Integer) userIdObject;
                         
                        Double totalPayableAmount = pricingService
                                .getPayableAmount(cartBusinessDo);
                        
                        Amount walletBalance = walletWrapper.getTotalBalance(userId);
                        
                        walletEligibility = walletService.calculateEligibility(
                                userId, new Amount(totalPayableAmount), cartBusinessDo, walletBalance);
                        
                        logger.info("Checkout Data lookupid:"+lookupId+"totalPayableAmount:"+totalPayableAmount+"producttype:"+checkoutData.getProductType()+"number:"+checkoutData.getNumber()+"RechargeAmount:"+checkoutData.getRechargeAmount());
                        
                        
                        checkoutData = mobileCheckoutService.proceedWithCheckout(checkoutData,
                                                                        lookupId, 
                                                                        totalPayableAmount, 
                                                                        walletEligibility,
                                                                        paymentBreakup,
                                                                        checkoutData.getNumber(), 
                                                                        (float)checkoutData.getRechargeAmount(), 
                                                                        checkoutData.getProductType());
                        
                        
                        //Code for check wallet eligibiliy in case of freefund apply
                        Double totalPayAmount = pricingService.getPayableAmount(cartBusinessDo);
                        model.addAttribute("payableTotalAmount",totalPayAmount);
                        model.addAttribute("allowablePGList", freefundService.getAllowablePGList(cartBusinessDo, null));
                        Users curUser = this.userServiceProxy.getLoggedInUser();
                        FreefundCoupon freefundCoupon = cartService.getFreefundCoupon(cartBusinessDo);
                        if (totalPayAmount.equals(0.0) && freefundCoupon!=null){
                        model.addAttribute("showFBConnect", this.fraudCheckService.shouldShowFbConnect(curUser,
                                freefundCoupon.getFreefundClassId(), request));
                        }
                        model.addAttribute("checkoutData", checkoutData);
                        return new ModelAndView("mobile/payment/payment-options-view", model);
                    }
                 
            } else {
                model.addAttribute("status", "sessionExpired");
                return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
            }
        

        } catch (Exception e) {
            logger.error("Exception caught in MobileProfileController after promo code page and proceed to payment page", e);
            model.addAttribute("status", "fail");
            return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
        }
        
        return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
    }
	
	
	@RequestMapping(value = "paymentPage", method = RequestMethod.GET)
    public ModelAndView PaymentPageRedirectGet(CheckoutData checkoutData,
            BindingResult result, HttpServletRequest request,
            HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mpcPaymentPageRedirectGet");
        return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
    }
    

	@RequestMapping(value = "saveAddress", method = RequestMethod.GET)
	public ModelAndView saveAddressGet(CheckoutData checkoutData,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mpcSaeAddressGet");
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	private void repopulateMasterData(ShippingDetails shippingDetails) {
		shippingDetails.setMasterData(masterData);
		if (shippingDetails.getStateId() != null){
			shippingDetails.setCityList(masterData.getCitiesForState(shippingDetails.getStateId()));
		}
	}
	
}
