package com.freecharge.mobile.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.UserService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.mobile.web.view.DTHRecharge;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.rest.validators.AmountFormatValidator;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX +  "/*")
public class DTHRechargeController {

    @Autowired
    private MasterData masterData;

    @Autowired
    private MobileCheckoutService checkoutService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MobileSeoHelper mobileSeoHelper;

    @Autowired
    private RechargeMetricService rechargeMetricService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private OperatorAlertService alertService;
    
    private final Logger logger = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "dth-recharge", method = RequestMethod.GET)
    public ModelAndView dthRecharge(Model model, HttpServletRequest request) {
        String view = "mobile/dth-recharge-full";
        DTHRecharge dthRecharge = RechargeUtil.getDthRecharge(request, masterData);
        model.addAttribute("dthRecharge", dthRecharge);
        mobileSeoHelper.handleRechargeSEO(request, model);
        return new ModelAndView(view);
    }

    @RequestMapping(value = "do-dth-recharge", method = RequestMethod.GET)
    public ModelAndView doDTHRechargeGet(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
    }

    @RequestMapping(value = "do-dth-recharge", method = RequestMethod.POST)
    public ModelAndView doDTHRecharge(
            @ModelAttribute("dthRecharge") @Valid DTHRecharge dthRecharge,
            BindingResult result,ModelMap model,  HttpServletRequest request,
            HttpServletResponse response) {
        final String DEFAULT_VIEW = "mobile/dth-recharge-full";

        dthRecharge.setMasterData(masterData);
        
        if (result.hasErrors()) {
            return new ModelAndView(DEFAULT_VIEW);
        }

        String dthNumber = dthRecharge.getDthNumber();
        String dthAmount = dthRecharge.getDthAmount();
        String dthOperatorId = dthRecharge.getDthOperator();
        
        if (StringUtils.isBlank(dthNumber)) {
            logger.error("Null or empty dthNumber");
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (!StringUtils.isNumeric(dthNumber)) {
            logger.error("Invalid dthNumber format");
            return new ModelAndView(DEFAULT_VIEW);
        }

        ErrorCode amountValidation = AmountFormatValidator.validate(dthAmount);
        
        if (amountValidation != ErrorCode.NO_ERROR) {
            logger.error("amount validation failed with: " + amountValidation.getErrorNumberString());
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (StringUtils.isBlank(dthOperatorId)) {
            logger.error("Null or empty operator");
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        SuccessfulRechargeInfo previousRecharge = userService.getLastSuccessfulRechargeByNumber(dthNumber);
        
        if (previousRecharge != null) {
            logger.error("Repeat recharge blocked for number: " + dthNumber);
            model.addAttribute("retryBlock", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (rechargeMetricService.isInvalidDenominationNumber(ProductName.DTH.getProductType(), dthNumber, dthOperatorId, new Double(dthAmount), RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "InvalidDenominationNumber");
            model.addAttribute("denominationFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (rechargeMetricService.isPermanentFailure(ProductName.DTH.getProductType(), dthOperatorId, dthNumber)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "PermanentFailure");
            model.addAttribute("permanentFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (alertService.isOperatorInAlert(Integer.parseInt(dthOperatorId))) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "OperatorAlert");
            model.addAttribute("operatorFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        logger.debug("Passed validation for DTH Recharge for DTH Number:"+dthNumber);
        logger.info("DTH recharge value entered :"+"number:"+dthNumber+" "+"Amount:"+dthAmount+" "+"operator Id:"+dthOperatorId+" "+"Is Remember ?:true");

        createCookies(response, dthNumber, dthAmount, dthOperatorId);

        String lookupId = checkoutService.saveTxnHomePage(dthNumber, Integer.parseInt(dthAmount), FCConstants.productMasterMap
                .get(FCConstants.DTH_PRODUCT_MASTER_ID), dthOperatorId, "1");
        if(StringUtils.isEmpty(lookupId)){
            return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
        }

        String successView = MobileURLConstants.MOBILE_COUPONS_SELECT_REDIRECT + "?lid=" + lookupId;
        return new ModelAndView(successView);
    }

    private void createCookies(HttpServletResponse response, String dthNumber,
            String dthAmount, String dthOperatorId) {
        Cookie dthNumberCookie = new Cookie("dthNumberCookie", dthNumber);
        Cookie dthAmountCookie = new Cookie("dthAmountCookie", dthAmount);
        Cookie dthOperatorCookie = new Cookie("dthOperatorCookie", dthOperatorId);
        dthNumberCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
        dthOperatorCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
        dthAmountCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
        response.addCookie(dthNumberCookie);
        response.addCookie(dthAmountCookie);
        response.addCookie(dthOperatorCookie);
    }
}
