package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.mobile.constant.MobileURLConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileCartController {
	
	@NoLogin
	@RequestMapping(value = "cart", method = RequestMethod.GET)
	public ModelAndView selectCouponsGet(@RequestParam String lookupId, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		//TODO: validate lookup id belongs to session.
		String view = "mobile/cart";
		model.addAttribute("lookupId", lookupId);
		return new ModelAndView(view);
	}
}
