package com.freecharge.mobile.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.addcash.service.AddCashService;
import com.freecharge.addcash.service.exception.AddCashProductToCartException;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.exception.PersonalDetailException;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.AddCashWebDO;

@Controller
@RequestMapping("/m/*")
public class MobileWalletController {

	@Autowired 
	private AddCashService addCashService;
    
    @Autowired
	private MobileCheckoutService checkoutService;
    
    @Autowired
    private WalletService walletService;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private FreefundService freefundService;
    
    @Autowired
    WalletWrapper walletWrapper;
    
 
    
    
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    //TODO: Add behind login interceptor.
	@RequestMapping(value = "addcashproducttocart", method = RequestMethod.POST)
    public ModelAndView addCash(@ModelAttribute(value = "addCashWebDo") AddCashWebDO addCashWebDO, HttpServletRequest request, HttpServletRequest response,ModelMap model) {
	 	String view = "mobile/payment/payment-options-view";
	 	
	 	try {
			if(addCashWebDO == null || addCashWebDO.getAddCashAmount() == null || !MobileUtil.isInteger(addCashWebDO.getAddCashAmount())){
				// TODO handle error page.
				view = "mobile/mycredits/view";
				getMybalaceData(model);
				model.addAttribute("errorMessage","Invalid Cash Amount");
				return new ModelAndView(view, model);
			}
			try {
				String lookupId = addCashService.addProductToCartForAddCash(addCashWebDO.getAddCashAmount());
				String sessionId = getSessionId();
				String amount = addCashWebDO.getAddCashAmount();
				CheckoutData checkoutData;
				try {
					checkoutData = checkoutService.initiateAddCashCheckout(FCConstants.WALLET_FUND_PRODUCT_TYPE, sessionId, lookupId, amount);
				} catch (PersonalDetailException e) {
					logger.error("Mobile Site: Error in saving personal details.");
					return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
				}
				model.addAttribute("checkoutData", checkoutData);
				CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
	            List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
	            model.addAttribute("allowablePGList", allowablePGList);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				model.addAttribute("isAddCash",true);
			} catch (AddCashProductToCartException e) {
				view = "mobile/mycredits/view";
				getMybalaceData(model);
				model.addAttribute("errorMessage",e.getErrorMessage());
			}
		} catch (CartExpiredException e) {
			logger.info("Cart Expired: " + e);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
    	return new ModelAndView(view, model);
    }

    private void getMybalaceData(ModelMap model)
    {
    	FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Object userIdObject = map.get(WebConstants.SESSION_USER_USERID);
        int userID = userIdObject == null ? -1 : (Integer) userIdObject;
        Amount balance = walletWrapper.getTotalBalance(userID);
        Map<String, Object> lastEntry = walletService.findLastEntry(userID);
        List<Map<String, Object>> rows = walletService.getBalanceRows(userID, lastEntry,true);
        model.addAttribute("balance", balance.getAmount());
        model.addAttribute("lastEntry", lastEntry);
        model.addAttribute("rows", rows);
        model.addAttribute("addCashWebDo", new AddCashWebDO());
    }

	private String getSessionId() {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		if (fs != null){
			return fs.getUuid();
		}
		return null;
	}
	 	    
	    
	
	    
	   
}
