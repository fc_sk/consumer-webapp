package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.web.util.GoogleAnalyticsUtil;
import com.freecharge.mobile.web.util.MobileUtil;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileGAController {

	@RequestMapping(value = "ga", method = RequestMethod.GET)
	public void ga(ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		String requestString = MobileUtil.getRequestStringWithoutQuestionMark(request);
		GoogleAnalyticsUtil.hitGAUsingImp(requestString, response);
	}
}
