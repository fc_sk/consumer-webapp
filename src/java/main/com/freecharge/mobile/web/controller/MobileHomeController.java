package com.freecharge.mobile.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.mobile.web.view.DTHRecharge;
import com.freecharge.mobile.web.view.DataCardRecharge;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.MobileRecharge;
import com.freecharge.platform.metrics.MetricsClient;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileHomeController {
    private Logger logger = LoggingFactory.getLogger(MobileHomeController.class);

    @Autowired
    private MasterData masterData;
    
    @Autowired
    private MobileSeoHelper mobileSeoHelper;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private AppConfigService appConfig;

    public ModelAndView homeDelegate(HttpServletRequest request,
            HttpServletResponse response, Model model, String stripType) {

        logger.debug("Old mobile home user agent - " + request.getHeader("User-Agent"));
        metricsClient.recordEvent("pageAccess", "oldMobileHomePage", "click");

        String version = request.getParameter("version");
        String view = "mobile/mobile-home-page";
        boolean isValidQueryString = GenericValidator.isUrl(request.getQueryString());
        if(version != null && ( version.equals("2") || version.equals("3") || version.equals("4")) ) {
            view = "mobile/mobile-home-page-new";

            MobileRecharge mobileRecharge = RechargeUtil.getMobileRecharge(request, masterData);
            model.addAttribute("mobileRecharge", mobileRecharge);
            mobileSeoHelper.handleRechargeSEO(request, model);

            DTHRecharge dthRecharge = RechargeUtil.getDthRecharge(request, masterData);
            model.addAttribute("dthRecharge", dthRecharge);
            mobileSeoHelper.handleRechargeSEO(request, model);

            DataCardRecharge dataRecharge = RechargeUtil.getDatacardRecharge(request, masterData);
            model.addAttribute("dataCardRecharge", dataRecharge);
            mobileSeoHelper.handleRechargeSEO(request, model);
        }

        model.addAttribute("isHomePage", true);
        model.addAttribute("stripType", stripType);
        model.addAttribute("isValidQueryString", isValidQueryString);
        checkAndSetAppCookies(request, response);
        model.addAttribute("fingerPrintingEnabled", appConfig.isFingerPrintingEnabled());
        return new ModelAndView(view);
    }

    @RequestMapping(value = { "home" })
    public Void homeLanding(HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        /*return homeDelegate(request, response, model, "home");*/
        response.sendRedirect("/mobile");
        return null;
    }

    @RequestMapping(value = { "did4" })
    public ModelAndView did4Landing(HttpServletRequest request,
            HttpServletResponse response, Model model) {
        return homeDelegate(request, response, model, "did4");
    }

    @RequestMapping(value = { "mcd" })
    public ModelAndView mcdLanding(HttpServletRequest request,
            HttpServletResponse response, Model model) {
        return homeDelegate(request, response, model, "mcd");
    }

    @RequestMapping(value = { "amazon" })
    public ModelAndView amazonLanding(HttpServletRequest request,
            HttpServletResponse response, Model model) {
        return homeDelegate(request, response, model, "amazon");
    }

    @RequestMapping(value = { "cart-expired" })
    public ModelAndView cartExpired(HttpServletRequest request,
            HttpServletResponse response, Model model) {
        String view = "mobile/mobile-cart-expired-page";
        return new ModelAndView(view);
    }
    
    @RequestMapping(value = { "new" })
    public Void newHomePage(HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        /*String view = "mobile/new";

        logger.debug("New mobile home user agent - " + request.getHeader("User-Agent"));
        metricsClient.recordEvent("pageAccess", "newMobileHomePage", "click");
        model.addAttribute("isRupayEnabled", appConfig.isRupayEnabled());

        model.addAttribute("releaseType", "general");
        model.addAttribute("fingerPrintingEnabled", appConfig.isFingerPrintingEnabled());

        return new ModelAndView(view);*/
        response.sendRedirect("/mobile");
        return null;
    }

    private void checkAndSetAppCookies(HttpServletRequest request,
            HttpServletResponse response) {
        String channel = request.getParameter(MobileConstants.REQUEST_PARAM_NAME_CHANNEL_ID);
        String appType = request.getParameter(MobileConstants.REQUEST_PARAM_NAME_APP_TYPE);
        if (MobileUtil.isValidChannel(channel)){
            Cookie channelCookie = new Cookie(MobileConstants.COOKIE_NAME_CHANNEL_ID, channel);
            response.addCookie(channelCookie);
        }

        if (MobileUtil.isValidAppType(appType)){
            Cookie appTypeCookie = new Cookie(MobileConstants.COOKIE_NAME_APP_TYPE, appType);
            response.addCookie(appTypeCookie);
        }
    }
}
