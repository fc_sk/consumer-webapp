package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.web.controller.LoginController;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileLogoutController {
	private Logger logger = LoggingFactory.getLogger(getClass());

	@Csrf
	@RequestMapping(value = "logout")
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		String view = "redirect:/m";

        //All the logout related activities are done in HTTPRequestInterceptor class. This interceptor searches for the below
        //attribute in request and if found, does the logout related activities.
        request.setAttribute(LoginController.DO_LOGOUT, true);

        return new ModelAndView(view);     
	}
}
