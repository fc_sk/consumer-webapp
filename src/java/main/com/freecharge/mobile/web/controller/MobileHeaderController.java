package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.constant.MobileURLConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileHeaderController {
private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@RequestMapping(value = "header")
    public ModelAndView doLogin(HttpServletRequest request, HttpServletResponse response, Model model) {
    	return new ModelAndView("mobile/mobile-header");
    }
	
}
