package com.freecharge.mobile.web.controller.payment;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.helper.PaymentOptionsHelper;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/payment/response/*")
public class MobilePaymentResponseController {
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private MobileCheckoutService checkoutService;

	@Autowired
    private FCProperties fcProperties;
	
	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
	private PaymentOptionsHelper paymentOptionsHelper;
	
	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;
	 
	@Autowired
    private CartService cartService;
	    
    @Autowired
    private FreefundService freefundService;
	
    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private PricingService pricingService;
    
	@Autowired
    @Qualifier("billDeskGatewayHandler")
    private IGatewayHandler bdGatewayHandler;
	
	@Csrf(exclude=true)
	@RequestMapping(value = "handlbdkeresposne", method = RequestMethod.POST)
	public ModelAndView handleBillDeskResponse(Model model,
			HttpServletRequest request, HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
	    logPGResponseMetric("bdk");
		String paymentGateway = PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE;
		String view = handleResponse(model, request, paymentGateway);
		metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE,
                System.currentTimeMillis() - startTime);
		return new ModelAndView(view);
	}
	
	@Csrf(exclude=true)
    @RequestMapping(value = "handleklickpayresponse", method = RequestMethod.POST)
    public ModelAndView handleKlickpayResponse(Model model,
            HttpServletRequest request, HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric("kpay");
        String paymentGateway = PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE;
        String view = handleResponse(model, request, paymentGateway);
        metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE,
                System.currentTimeMillis() - startTime);
        return new ModelAndView(view);
    }
	
	@Csrf(exclude=true)
	@RequestMapping(value = "handleicsresponse", method = RequestMethod.POST)
	public ModelAndView handleICICISSLResponse(Model model,
			HttpServletRequest request, HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
	    logPGResponseMetric("ics");
		String paymentGateway = PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE;
		String view = handleResponse(model, request, paymentGateway);
		metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE,
                System.currentTimeMillis() - startTime);
		return new ModelAndView(view);
	}
	
	@Csrf(exclude=true)
	@RequestMapping(value = "handleccaresposne", method = RequestMethod.POST)
	public ModelAndView handleCCAvenueResponse(Model model,
			HttpServletRequest request, HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
	    logPGResponseMetric("cca");
		String paymentGateway = PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE;
		String view = handleResponse(model, request, paymentGateway);
		metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE,
                System.currentTimeMillis() - startTime);
		return new ModelAndView(view);
	}
	
	@Csrf(exclude=true)
	@RequestMapping(value = "handlepayuresponse", method = RequestMethod.POST)
	public ModelAndView handlePayUResponse(Model model,
			HttpServletRequest request, HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
	    logPGResponseMetric("payu");
		String paymentGateway = PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE;
		String view = handleResponse(model, request, paymentGateway);
		metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE,
                System.currentTimeMillis() - startTime);
		return new ModelAndView(view);
	}
	
	@Csrf(exclude=true)
    @RequestMapping(value = "handlezeropaymentresponse", method = RequestMethod.GET)
    public ModelAndView handleZeroPaymentResponse(Model model,
            HttpServletRequest request, HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
        String view = "mobile/payment/response/payment-response-view";
        String orderId = request.getParameter("orderId");
        
        if(MobileUtil.isNewMobileAppChannel(request))
        {
             return new ModelAndView(MobileURLConstants.MOBILE_APP_STSTUS+"/true/"+orderId+"/false/result");
        }
        
        CheckoutData checkoutData = new CheckoutData();
        if(orderId!=null)
        {
        RechargeDetails rechargeDetails = userTransactionHistoryService.getRechargeDetailsByOrderID(orderId);
        checkoutData.setNumber(rechargeDetails.getServiceNumber());
        checkoutData.setOrderId(orderId);
        
        checkoutData.setRechargeAmount(Integer.parseInt(rechargeDetails.getAmount()));
        model.addAttribute("checkoutData", checkoutData);
        }
        metricsClient.recordLatency("Payment", "Old.Response." + PaymentConstants.ZEROPAY_PAYMENT_OPTION,
                System.currentTimeMillis() - startTime);
        return new ModelAndView(view);
    }

	private String handleResponse(Model model, HttpServletRequest request,
			String paymentGateway) {
		String view = "mobile/payment/response/payment-response-view";
		CheckoutData checkoutData=null;
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		logger.info("Response recieved for " + paymentGateway + " gateway for session id "+fs.getUuid());
        Map<String,String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
        try {
        	Integer paymentTypeCode = new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD);
        	
			checkoutData = checkoutService.handlePaymentResponse(requestMap, paymentGateway, paymentTypeCode);
			
			String orderNo = checkoutData.getOrderId();

			if (checkoutService.isAddCashPaymentResponse(orderNo)){
                
                if(MobileUtil.isNewMobileAppChannel(request))
                {
                    return MobileURLConstants.MOBILE_APP_STSTUS+"/true/null/true/result";
                }
                
                return MobileURLConstants.MOBILE_BALANCE_REDIRECT;
            }
			
			
			logger.info("Mobile order no after checkoutService.handlePaymentResponse: " + orderNo);
			Boolean isPaymentSuccessful = checkoutData.getIsPaymentSuccessful();
	        String pg = request.getParameter(FCConstants.PG);
	        Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
            model.addAttribute("payableTotalAmount",payableTotalAmount);
	        CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
            List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, pg);
            model.addAttribute("allowablePGList", allowablePGList);
           
            
			model.addAttribute("checkoutData", checkoutData);
			if (!isPaymentSuccessful){
				CheckoutData checkoutDataNew = reinitCheckoutData(checkoutData);
				allowablePGList.add(PaymentConstants.ALL);
				model.addAttribute("allowablePGList", allowablePGList);
				 model.addAttribute("checkoutData", checkoutDataNew);
				 view =  "mobile/payment/payment-options-view";
			}else {
				model.addAttribute("finalResultSet", true);
				model.addAttribute("finalResultPage", true); // used by campaignTracking.jsp
				model.addAttribute("mapAmount", checkoutData.getTotalAmount());
				
				view = "mobile/payment/response/payment-response-view";
			}
			
		} catch (NumberFormatException e) {
			logger.error("Error occured while processing payment response for session id:" + fs.getUuid());
			logger.error(e.getMessage());
		} catch (FCPaymentPortalException e) {
			logger.error("Error occured while processing payment response for session id:" + fs.getUuid());
			logger.error(e.getMessage());
		} catch (FCPaymentGatewayException e) {
			logger.error("Error occured while processing payment response for session id:" + fs.getUuid());
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error("Error occured while processing payment response for session id:" + fs.getUuid());
			logger.error(e.getMessage());
		}
        
        
        
        if(MobileUtil.isNewMobileAppChannel(request))
        {
            if(checkoutData!=null)
            {
             return MobileURLConstants.MOBILE_APP_STSTUS+"/"+checkoutData.getIsPaymentSuccessful()+"/"+checkoutData.getOrderId()+"/false/result";
            }
            else
            {
                return MobileURLConstants.MOBILE_APP_STSTUS+"/true/null/false/result";
            }
        }
        
		return view;
	}
	
	
	//If Payment fails set checkout data for partial payment.
	private CheckoutData reinitCheckoutData(CheckoutData checkoutData) throws CartExpiredException {
		String lookupid = checkoutData.getLookupId();
		//For partial payment and Wallet Eligibility
		if (checkoutData.getPaymentOptions().getWalletEligibility() == null) {
			checkoutData.getPaymentOptions().setWalletEligibility(paymentOptionsHelper.getWalletEligibility(lookupid));
		}
		if (checkoutData.getPaymentOptions().getPaymentBreakup() == null) {
			checkoutData.getPaymentOptions().setPaymentBreakup(paymentOptionsHelper.getPaymentBreakup(lookupid));
		}
		if (checkoutData.getPaymentOptions().getWalletEligibility().getEligibleForPartialPayment() && checkoutData.getProductType() != FCConstants.WALLET_FUND_PRODUCT_TYPE) {
			checkoutData.getPaymentOptions().setUsePartialPayment(Boolean.TRUE);
		} else {
			checkoutData.getPaymentOptions().setUsePartialPayment(Boolean.FALSE);
		}
		return checkoutData;
	}
	
	/**
     * Method logs the PG from which the response is received
     * 
     * @param responseMsg
     */
	private void logPGResponseMetric(String pg) {
		metricsClient.recordEvent("Payment", "PG.Response", pg);
	}
}
