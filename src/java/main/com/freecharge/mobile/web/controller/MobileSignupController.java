package com.freecharge.mobile.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.City;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.User;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.useragent.UserLoginEventPublisherHelper;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.controller.SignupController;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileSignupController {
    public static final String METRIC_NAME = "mSignUpAccess";
    @Autowired
	private MasterData masterData;
	
	private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppConfigService appConfigService;
	
	@Autowired
	private RegistrationService registrationService;

    @Autowired
    private UserLoginEventPublisherHelper userLoginEventPublisherHelper;

    @Autowired
    private MetricsClient metricsClient;

	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public ModelAndView signUp(HttpServletRequest request,
			HttpServletResponse response, Model model) {
	    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "mobileSignupController1");
        //New signup view as default as appconfig is removed
		String view = "mobile/signup/new_view";
		User user = new User();
		user.setMasterData(masterData);
		model.addAttribute("user", user);
		
		// String pageTitle = "FreeCharge Sign up";
		return new ModelAndView(view);
	}
	
	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public ModelAndView signUpPost(@ModelAttribute("user") User user, HttpServletRequest request,
			HttpServletResponse response, Model model) {
	    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "mobileSignupController2");
        //New signup view as default as appconfig is removed
        String view = "mobile/signup/new_view";
		user.setMasterData(masterData);
		model.addAttribute("user", user);
		return new ModelAndView(view);
	}

	@RequestMapping(value = "city-list", method = RequestMethod.GET)
	public @ResponseBody List<City> getCityListForState(@RequestParam Integer stateId) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mscGetCityListForState");
		return masterData.getCitiesForState(stateId);
	}
	
	@RequestMapping(value = "adduser", method = RequestMethod.GET)
	public ModelAndView registrationGet(HttpServletRequest request, HttpServletResponse response) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mscRegistrationGet");
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
		
	@RequestMapping(value = "adduser", method = RequestMethod.POST)
	public String registration(@ModelAttribute("user") @Valid User user, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mscRegistration");
        //New signup view as default as appconfig is removed
        String view = "mobile/signup/new_view";
		String homeView = MobileURLConstants.MOBILE_HOME_REDIRECT;
		if (bindingResult.hasErrors()) {
			repopulateMasterData(user);
			return view;
		}

		 if (FCSessionUtil.isAnotherUserLoggedIn(user.getEmail())) {
             bindingResult.rejectValue("email","ALREADY_LOGGEDIN","Another user is already logged in from the same browser. To continue using this account, you will have to sign out the other user and login in again.This is done to protect your account and to ensure the privacy of your information.");
             return view;

         }
		 
		RegisterBusinessDO registerBusinessDo =  MapperUtil.map(user, RegisterBusinessDO.class);
		if(registerBusinessDo.getPassword().length() < 6 || registerBusinessDo.getPassword().length() > 40){
			bindingResult.rejectValue("password","INCORRECT_PASSWORD_LENGTH","Your password should be between 6-40 characters");
            return view;
		}

		registerBusinessDo.setCountryId(99);
		registerBusinessDo.setConfirmPassword(registerBusinessDo.getPassword());
		
		registerBusinessDo.setFkAffiliateProfileId(MobileUtil.getMobileAffiliate(request));
		boolean registrationStatus = registrationService.registerOnBothDB(registerBusinessDo);
		if (registrationStatus) {
			registrationService.sendSuccessfulRegistrationMail(registerBusinessDo);	
			storeSession(request, response);

            userLoginEventPublisherHelper.publish(request, this);
            metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "success");
            metricsClient.recordEvent(SignupController.METRIC_SERVICE, METRIC_NAME, "success");

			try {
				if (user.getSignupFrom() == null || user.getSignupFrom().isEmpty())
				{
					response.sendRedirect("/m");
					return null;
				}
				else{
					request.getRequestDispatcher(user.getSignupFrom()).forward(request, response);	
					return null;
				}
			} catch (IOException e) {
				logger.error("Error doing signup: ", e);
				metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
			} catch (ServletException e) {
			    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "exception");
				logger.error("Error doing signup: ", e);
			}
			return homeView;
		} else {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "signup", "fail");
            metricsClient.recordEvent(SignupController.METRIC_SERVICE, METRIC_NAME, "failure");
			if (registerBusinessDo.getRegistrationStatus().compareToIgnoreCase(WebConstants.EMAIL_ID_EXISTS) == 0) {
				repopulateMasterData(user);
				bindingResult.rejectValue("email", "Email Exists", "Email id already exists");
			} else{
				repopulateMasterData(user);
				bindingResult.rejectValue("email", "Email Exists", "Error occured while registration");
			}
		}
		return view;
	}

	private void storeSession(HttpServletRequest request,
			HttpServletResponse response) {
		SessionManager sessionManager = (SessionManager) SpringBeanLocator.getInstance().getSpringBean(FCConstants.BEAN_DEFINATION_NAME_SESSIONMANAGER);
		sessionManager.storeSession(request, response);
	}

	private void repopulateMasterData(User user) {
		user.setMasterData(masterData);
		if (user.getStateId() != null){
			user.setCityList(masterData.getCitiesForState(user.getStateId()));
		}
	}
}
