package com.freecharge.mobile.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.freecharge.common.util.FCConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.ForgotPasswordService;
import com.freecharge.common.businessdo.ForgotPasswordBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.util.MapperUtil;
import com.freecharge.mobile.web.view.PasswordRecoveryEmail;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.PasswordRecoveryWebDO;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileResetPasswordController {
	
	 @Autowired
	 private ForgotPasswordService forgotPasswordService;
	 
	 @Autowired
	 private EPinEncrypt ePinEncrypt;
	 
	 @Autowired
	 private MetricsClient              metricsClient;
	 
	 private Logger logger = LoggingFactory.getLogger(getClass());

	@RequestMapping(value = "mobile-changepasswordpage", method = RequestMethod.GET)
	public ModelAndView changePasswordPage(HttpServletRequest request, Model model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mrpcChangePasswordPage");
		String passwordResetView = "mobile/reset-password";
		System.out.println("Entered to reset:");
		PasswordRecoveryEmail passwordRecoveryEmail = new PasswordRecoveryEmail();
		model.addAttribute("passwordRecoveryEmail", passwordRecoveryEmail);
	    return new ModelAndView(passwordResetView);
	}
	
	@RequestMapping(value = "mobile-changepassword", method = RequestMethod.POST)
	public ModelAndView changePassword(@ModelAttribute("passwordRecoveryEmail") @Valid PasswordRecoveryEmail passwordRecoveryEmail, BindingResult result, ModelMap model, HttpServletRequest request) throws Exception {
		String passwordResetView = "mobile/reset-password-response";
		String errorView = "mobile/reset-password";
		long startTime = System.currentTimeMillis();
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "mobileResetPasswordController1");
		if (result.hasErrors()) {
			StringBuffer error = new StringBuffer("");
			for(ObjectError errorObject:result.getAllErrors()){
				if(errorObject.getDefaultMessage()!=null){
					error.append(errorObject.getDefaultMessage()).append(" ");
				}else{
					error.append(errorObject.getCode()).append(" ");
				}
			}
			model.addAttribute("errorMsg",error.toString());
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "fail");
			return new ModelAndView(errorView); 
		}
		
		ForgotPasswordBusinessDO forgotPasswordBusinessDO = MapperUtil.map(passwordRecoveryEmail, ForgotPasswordBusinessDO.class);
		logger.info("entered Forgot business DO : "+forgotPasswordBusinessDO.getEmail());
		try {
			forgotPasswordService.getPassword(forgotPasswordBusinessDO);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
		}  catch (FCRuntimeException fcexp) {
			logger.error("Exception occurred in change password  .", fcexp);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			return new ModelAndView("home/error");
		} catch (Exception exp) {
			logger.error("Any Exception that could come", exp);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			return new ModelAndView("home/error");
		}
		String emailId = forgotPasswordBusinessDO.getEmail();
		model.addAttribute("emailId", emailId);
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return new ModelAndView(passwordResetView);
		 
	}
	
	@RequestMapping(value = "mobile-passwordrecovery", method = RequestMethod.POST)
	public ModelAndView  passwordreccovery1(@ModelAttribute(value = "passwordrecovery") @Valid PasswordRecoveryWebDO passwordRecoveryWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) {
		String view = "mobile/passwordrecovery-result";
		String validationFailView = "mobile/passwordrecovery";
		long startTime = System.currentTimeMillis();
		model.addAttribute("passwordRecovery",passwordRecoveryWebDO);
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "click");
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "mobileResetPasswordController2");
		if (bindingResult.hasErrors()) {	
			StringBuffer error = new StringBuffer("");
			for(ObjectError errorObject:bindingResult.getAllErrors()){
				if(errorObject.getDefaultMessage()!=null){
					error.append(errorObject.getDefaultMessage()).append(" ");
				}else{
					error.append(errorObject.getCode()).append(" ");
				}
			}
			model.addAttribute("errorMsg",error.toString());
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "fail");
			return new ModelAndView(validationFailView); 
		}
		
		try {
			passwordRecoveryWebDO.setEmail(passwordRecoveryWebDO.getEncryptEmail());
			String delegateName = WebConstants.DELEGATE_BEAN_RECOVERED_PASSWORD_CHANGE;
			WebContext webContext = DelegateHelper.processRequest(request, response, delegateName, model, passwordRecoveryWebDO);
			String statusMessage = ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())).getStatus();
			model.addAttribute("changePasswordSuccessMsg", statusMessage);
			if ("expairedLink".equals(statusMessage)) {
				view = "mobile/expiredlink";
			}
			model.addAttribute("status","success");
			logger.info("New Password is set to the User " + ((PasswordRecoveryWebDO) (webContext.getBaseWebDO())).getEmail());
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "success");
		} catch (FCRuntimeException fcexp) {
			logger.error("Exception xxxxx .", fcexp);
			model.addAttribute("error","Technical error occured. Please try again later.");
			model.addAttribute("status", "exception");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
		} catch (Exception exp) {
			logger.info("Unable to change Password to  the User ");
			logger.error("Any Exception that could come", exp);
			model.addAttribute("error","Technical error occured. Please try again later.");
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "changePassword", "exception");
			model.addAttribute("status", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "changePassword", endTime - startTime);
		return new ModelAndView(view);
	}
}