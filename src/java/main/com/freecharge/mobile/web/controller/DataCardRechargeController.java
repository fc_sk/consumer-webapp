package com.freecharge.mobile.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.MobileOperatorCircleService;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.mobile.web.view.DataCardRecharge;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.OperatorCircle;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.rest.validators.AmountFormatValidator;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class DataCardRechargeController {
	@Autowired
	private MasterData masterData;

	@Autowired
	private MobileOperatorCircleService mobileOperatorCircleService;

	@Autowired
	private MobileCheckoutService checkoutService;
	
    @Autowired
    private UserService userService;
    
    @Autowired
    private MobileSeoHelper mobileSeoHelper;

    @Autowired
    private RechargeMetricService rechargeMetricService;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private OperatorAlertService alertService;

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@RequestMapping(value = "datacard-recharge", method = RequestMethod.GET)
	public ModelAndView datacardRecharge(Model model,
			HttpServletRequest request) {
		String view = "mobile/datacard-recharge-full";
		DataCardRecharge dataRecharge = RechargeUtil.getDatacardRecharge(request, masterData);
		model.addAttribute("dataCardRecharge", dataRecharge);
		mobileSeoHelper.handleRechargeSEO(request, model);
		return new ModelAndView(view);
	}

	@RequestMapping(value = "do-datacard-recharge", method = RequestMethod.GET)
	public ModelAndView doDatacardRechargeGet(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-datacard-recharge", method = RequestMethod.POST)
	public ModelAndView doDatacardRecharge(
			@ModelAttribute("dataCardRecharge") @Valid DataCardRecharge datacardRecharge,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		datacardRecharge.setMasterData(masterData);
        final String DEFAULT_VIEW = "mobile/datacard-recharge-full";

        if (result.hasErrors()) {
			return new ModelAndView(DEFAULT_VIEW);
		}

		String datacardNumber = datacardRecharge.getDatacardNumber();
		String datacardAmount = datacardRecharge.getDatacardAmount();
		String datacardOperatorId = datacardRecharge.getDatacardOperator();
		String datacardCircle = datacardRecharge.getDatacardCircle();
		
        if (StringUtils.isBlank(datacardNumber)) {
            logger.error("Null or empty datacardNumber");
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (!StringUtils.isNumeric(datacardNumber)) {
            logger.error("Invalid datacardNumber format");
            return new ModelAndView(DEFAULT_VIEW);
        }

        ErrorCode amountValidation = AmountFormatValidator.validate(datacardAmount);
        
        if (amountValidation != ErrorCode.NO_ERROR) {
            logger.error("amount validation failed with: " + amountValidation.getErrorNumberString());
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (StringUtils.isBlank(datacardOperatorId)) {
            logger.error("Null or empty operator");
            return new ModelAndView(DEFAULT_VIEW);
        }
		
        SuccessfulRechargeInfo previousRecharge = userService.getLastSuccessfulRechargeByNumber(datacardAmount);
        
        if (previousRecharge != null) {
            logger.error("Repeat recharge blocked for number: " + datacardAmount);
            model.addAttribute("retryBlock", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (rechargeMetricService.isInvalidDenominationNumber(ProductName.DataCard.getProductType(), datacardNumber, datacardOperatorId, new Double(datacardAmount), RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "InvalidDenominationNumber");
            model.addAttribute("denominationFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (rechargeMetricService.isInvalidDenomination(datacardOperatorId, datacardCircle, new Double(datacardNumber), RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE, ProductMaster.ProductName.DataCard.getProductType())) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "InvalidDenomination");
            model.addAttribute("denominationFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (rechargeMetricService.isPermanentFailure(ProductName.DataCard.getProductType(), datacardOperatorId, datacardNumber)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "PermanentFailure");
            model.addAttribute("permanentFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (alertService.isOperatorInAlert(Integer.parseInt(datacardOperatorId))) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "OperatorAlert");
            model.addAttribute("operatorFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

		logger.debug("Passed validation for Datacard Recharge for Datacard Number:"+datacardNumber);
		logger.info("Datacard recharge value entered :"+"number:"+datacardNumber+" "+"Amount:"+datacardAmount+" "+"operator Id:"+datacardOperatorId+" "+"circel:"+datacardCircle+" "+"Is Remember ?:true");
		
		Cookie datacardRememberCookie = new Cookie("datacardRememberCookie", "true");
		response.addCookie(datacardRememberCookie);
			createCookies(response, datacardNumber, datacardAmount, datacardOperatorId, datacardCircle);
		String lookupId = checkoutService.saveTxnHomePage(datacardNumber, Integer.parseInt(datacardAmount), FCConstants.productMasterMap
				.get(FCConstants.DATACARD_PRODUCT_MASTER_ID), datacardOperatorId, datacardCircle);
		if(StringUtils.isEmpty(lookupId)){
			return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
		}
		String successView = MobileURLConstants.MOBILE_COUPONS_SELECT_REDIRECT + "?lid=" + lookupId;
		return new ModelAndView(successView);
	}

	/**
	 * Input mobileNumber for which operator and circle data is required.
	 * 
	 * @param mobileNumber
	 *            mobile number request parameter sent by client.
	 * @return returns JSON format of OperatorCircle. ReponseBody annotation
	 *         takes care of converting java object to JSON.
	 */
	@RequestMapping(value = "datacard-circles")
	public @ResponseBody
	OperatorCircle datacardCircles(@RequestParam String datacardNumber) {
		if (datacardNumber == null || datacardNumber.length() < 4) {
			return null;
		}
		return getOperatorCircle(datacardNumber);
	}

	private OperatorCircle getOperatorCircle(String datacardNumber) {
		OperatorCircleBusinessDO operatorCicleBDO = new OperatorCircleBusinessDO();
		operatorCicleBDO.setPrefix(datacardNumber);
		operatorCicleBDO.setProductType("3");
		return mobileOperatorCircleService.getOperatorCircle(operatorCicleBDO);
	}
	
	private void createCookies(HttpServletResponse response, String number,
			String amount, String operatorId, String circleId) {
		Cookie datacardNumberCookie = new Cookie("datacardNumberCookie", number);
		Cookie datacardAmountCookie = new Cookie("datacardAmountCookie", amount);
		Cookie datacardOperatorCookie = new Cookie("datacardOperatorCookie", operatorId);
		Cookie datacardCircleCookie = new Cookie("datacardCircleCookie", circleId);
		datacardNumberCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		datacardOperatorCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		datacardAmountCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		datacardCircleCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		response.addCookie(datacardNumberCookie);
		response.addCookie(datacardAmountCookie);
		response.addCookie(datacardOperatorCookie);
		response.addCookie(datacardCircleCookie);
	}
}
