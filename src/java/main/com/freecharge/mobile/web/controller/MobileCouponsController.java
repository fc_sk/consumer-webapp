package com.freecharge.mobile.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.CaptchaService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.coupon.service.CouponDummyDataManager;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.exception.PersonalDetailException;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.MobileCouponData;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileCouponsController {
	@Autowired
    private VoucherService voucherService;
	
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private MobileCheckoutService checkoutService;
	
	@Autowired
    private AppConfigService appConfigService;
	
	@Autowired
	private MasterData masterData;
	
	@Autowired
    private CaptchaService captchaService;
	
   static final String FOOD_AND_BEVERGES = "Food-n-Beverages";
	
	@RequestMapping(value = "coupons")
	public @ResponseBody MobileCouponData coupons(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		if (CouponDummyDataManager.isDummyCouponMode()){
    		return CouponDummyDataManager.getDummyOldMobileSiteCouponsData();
    	}
		
		MobileUtil.setIPAddressInSession(request);
		
		Map<String, List<CouponVO>> couponsCategoryMap = voucherService.getAllActiveCouponsWithOrdering(FCConstants.CHANNEL_ID_MOBILE,1);
		List<VoucherCat> categories = voucherService.getAllActiveVoucherCat();
		Collections.sort(categories, new VoucherCat.CategoryDetailComparator());
		categories = setFoodCoupanFirst(categories);
		removeEmptyCategories(couponsCategoryMap, categories);
		MobileCouponData mobileCouponData = new MobileCouponData();
		mobileCouponData.setCategories(categories);
		mobileCouponData.setCategoryMap(couponsCategoryMap);
		return mobileCouponData;
	}
	
	
	private List<VoucherCat> setFoodCoupanFirst(final List<VoucherCat> categories) {
		List<VoucherCat> categoriesNew = new ArrayList<VoucherCat>(categories);
		if(categories!=null)
		{
			for(VoucherCat voucherCat:categories)
			{
				if(voucherCat!=null && voucherCat.getCatDetail().equals(FOOD_AND_BEVERGES))
				{
					categoriesNew.remove(voucherCat);
					categoriesNew.add(0, voucherCat);
			    }
			}
		}
		return categoriesNew;
		
	}

	@RequestMapping(value = "select-coupons", method = RequestMethod.GET)
	public ModelAndView selectCouponsGet(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		String view = "mobile/mobile-coupons-select";
		String lookupId = request.getParameter("lid");
		if (StringUtils.isEmpty(lookupId)){
			return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
		}
		CheckoutData checkoutData = checkoutService.getCheckoutDataForCouponSelect(lookupId);
		model.addAttribute("checkoutData", checkoutData);
		return new ModelAndView(view);
	}
	
	@RequestMapping(value = "save-coupons", method = RequestMethod.GET)
	public ModelAndView saveCouponsGet(CheckoutData checkoutData,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "save-coupons", method = RequestMethod.POST)
	public ModelAndView saveCoupons(CheckoutData checkoutData,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		try {
			String lookupId = checkoutData.getLookupId();
			String couponList = checkoutData.getCoupons();
			if (lookupId == null){
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			try {
				checkoutData = checkoutService.initiateCheckout(lookupId, couponList);
				if (checkoutData.getContactDetailsReq()) {

					checkoutData.getShippingDetails().setMasterData(masterData);
					checkoutData.getShippingDetails().setCityList(masterData.getCitiesForState(checkoutData.getShippingDetails().getStateId()));
					model.addAttribute("shippingDetails",checkoutData.getShippingDetails());
					return new ModelAndView("mobile/shippingdetails/view");
				}
			} catch (PersonalDetailException e) {
				logger.error("Mobile Site: Error in saving personal details.");
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount",payableTotalAmount);
			model.addAttribute("checkoutData", checkoutData);
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
	        if(appConfigService.isRecaptchaEnabled()) {
	            model.addAttribute(WebConstants.FORCE_CAPTCHA, captchaService.shouldCheckCaptcha(fs));
	        } 
		} catch (CartExpiredException e) {
			logger.info("Cart Expired: " + e);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
		return new ModelAndView("mobile/promocode/view", model);
	}

	private void removeEmptyCategories(
			Map<String, List<CouponVO>> couponsCategoryMap,
			List<VoucherCat> categories) {
		List<VoucherCat> emptyCategories = new ArrayList<VoucherCat>();
		for (VoucherCat category: categories){
			if (couponsCategoryMap.get(category.getCatName()) == null || couponsCategoryMap.get(category.getCatName()).isEmpty()){
				emptyCategories.add(category);
			}
		}
		categories.removeAll(emptyCategories);
	}
}
