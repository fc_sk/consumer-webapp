package com.freecharge.mobile.web.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.exception.FraudDetectedException;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCInfraUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.GSTService;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.helper.PaymentOptionsHelper;
import com.freecharge.mobile.service.util.CommonUtil;
import com.freecharge.mobile.web.util.ConvenienceFeeUtil;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.services.PaymentGatewayHandler.RequestParameters;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.bean.GSTFee;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.zeropayment.ZeroPaymentController;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/payment/request/*")
public class MobilePaymentRequestController {
	
	
	private final Logger logger = LoggingFactory.getLogger(getClass());
	public static final String        METRICS_SERVICE            = "Payment";
    public static final String        METRIC_PAYMENT             = "Attempt";
    public static final String        METRIC_ATTEMPT             = "attempt";
    public static final String		  METRIC_REQUEST			 = "Request";
    public static final String		  METRIC_REQUEST_START		 = "start";
    public static final String		  METRIC_REQUEST_END		 = "end";

	@Autowired
	private MobileCheckoutService checkoutService;
	
	@Autowired
	private com.freecharge.app.service.CheckoutService checkoutService2;

	@Autowired
	protected FCProperties fcProperties;

	@Autowired
	private PaymentOptionsHelper paymentOptionsHelper;
	
	@Autowired
    private CartService cartService;
    
    @Autowired
    private FreefundService freefundService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private PaymentSNSService paymentSNSService;

    @Autowired
    private OrderIdReadDAO orderIdReadDAO;

    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private ConvenienceFeeUtil convenienceFeeUtil;
    
    @Autowired
    private GSTService gstService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    
	@RequestMapping(value = "do-credit-card-pay", method = RequestMethod.GET)
	public ModelAndView paymentOptionsGet(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-credit-card-pay", method = RequestMethod.POST)
	public ModelAndView paymentOptions(@Valid CheckoutData checkoutData,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
	    
	    long startTime = System.currentTimeMillis();
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_CREDITCARD, METRIC_ATTEMPT);
		logPaymentRequestStartMetric(request);
		String errorView = "mobile/payment/payment-options-view";
		
		Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
		 
		try {
			String fraudDetecedView = "mobile/payment/fraud-detected-view";
			
			// For Mobile Android APP Redirection
			if(isNEWMobileAPP)
            {
			     fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
			     errorView = MobileConstants.paymentAttemptGeneralError();
            }
			
			if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "NullValidationFail");
			    
			   // For Mobile APP Redirection
			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    return new ModelAndView(errorView);
                }
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
				
			}
			
			if (checkoutData.getPaymentOptions().getCreditCard() == null){
                metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "NoData");

				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				
				// For Mobile APP Redirection
				if(isNEWMobileAPP)
		         {
		             errorView = MobileConstants.paymentAttemptCheckoutBeanError();;
		         }
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			checkoutData.getPaymentOptions().getCreditCard().setSelected(true);
			if (result.hasErrors()) {
                metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "ValidationFailure");
                
			    // For Mobile APP Redirection
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getFieldError().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
				reinitCheckoutData(checkoutData);
				 CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
	             List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
	             model.addAttribute("allowablePGList", allowablePGList);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			logger.debug("Passed validation for credit card entries:"+"Service Number:"+checkoutData.getNumber()+" "+"Creditcard Num:"+checkoutData.getPaymentOptions().getCreditCard().getCardNumber());
			logger.info("Payment details using creditcard for:"+" service number:"+checkoutData.getNumber()+" "+"Creditcard Num:"+checkoutData.getPaymentOptions().getCreditCard().getCardNumber());
			
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
			try {
				Map prbdo = checkoutService2
						.doCreditCardPayment(checkoutData, checkoutData
								.getPaymentOptions().getCardStorage(), checkoutData
								.getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request),checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
				model.put("actionUrl", prbdo.get("actionUrl"));
				model.put("resultMap", prbdo);
				logRequestToPGMetric((String)prbdo.get("actionUrl"), request);
				metricsClient.recordEvent("Payment.Mobile.Attempt", "CC", "Succes");
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_CREDITCARD,
                        System.currentTimeMillis() - startTime);
                logPaymentRequestCompleteMetric(request);
				return new ModelAndView("mobile/payment/request/payment-request-view");
			} catch(FraudDetectedException rc) {
	              metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "FraudAttempt");
	              logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
	              logPaymentRequestCompleteMetric(request);
				return new ModelAndView(fraudDetecedView);
			} catch (Exception e) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "Exception");
				logger.error("Error occured while paying using Credit Card for lookup id: " + checkoutData.getLookupId(), e);
				logger.error(e.getMessage());
			}
			//If exception throw setting checkout data.
			reinitCheckoutData(checkoutData);
			model.addAttribute("checkoutData", checkoutData);
			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount",payableTotalAmount);
		} catch (CartExpiredException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "ExpiredCart");
			logger.info("Cart Expired: " + e);
			
			if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
			logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
		
        metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Error", "GenericFailure");
        logPaymentRequestCompleteMetric(request);
		return new ModelAndView(errorView);
	}
	
	@RequestMapping(value = "do-save-credit-card-pay", method = RequestMethod.POST)
	public ModelAndView savePaymentOptions(CheckoutData checkoutData,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_CREDITCARD, METRIC_ATTEMPT);
	    logPaymentRequestStartMetric(request);
	    Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
		String errorView = "mobile/payment/payment-options-view";
		try {
			String fraudDetecedView = "mobile/payment/fraud-detected-view";
			
			if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
			
			if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
	             metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "NullValidationFail");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    logPaymentRequestCompleteMetric(request);
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			if (checkoutData.getPaymentOptions().getCreditCard() == null){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "NoData");
			    
			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
               
				reinitCheckoutData(checkoutData);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			checkoutData.getPaymentOptions().getCreditCard().setSelected(true);
			if (result.hasErrors()) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "ValidationFailure");
			    
			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    
			    CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
                model.addAttribute("allowablePGList", allowablePGList);
				reinitCheckoutData(checkoutData);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			logger.debug("Passed validation for credit card entries:"+"Service Number:"+checkoutData.getNumber()+" "+"Creditcard Num:"+checkoutData.getPaymentOptions().getCreditCard().getCardNumber());
			logger.info("Payment details using creditcard for:"+" service number:"+checkoutData.getNumber()+" "+"Creditcard Num:"+checkoutData.getPaymentOptions().getCreditCard().getCardNumber());
			
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
			try {
				Map prbdo = checkoutService2
						.doSaveCreditCardPayment(checkoutData, checkoutData
								.getPaymentOptions().getCardStorage(), checkoutData
								.getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request),checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
				model.put("actionUrl", prbdo.get("actionUrl"));
				model.put("resultMap", prbdo);
				logRequestToPGMetric((String)prbdo.get("actionUrl"), request);
                metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save", "Succes");
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_CREDITCARD,
                        System.currentTimeMillis() - startTime);
                logPaymentRequestCompleteMetric(request);
				return new ModelAndView("mobile/payment/request/payment-request-view");
			} catch(FraudDetectedException rc) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "FraudAttempt");

				logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(fraudDetecedView);
			} catch (Exception e) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "Exception");

				logger.error("Error occured while paying using Credit Card for lookup id: " + checkoutData.getLookupId(), e);
				logger.error(e.getMessage());
			}
			//If exception throw setting checkout data.
			reinitCheckoutData(checkoutData);
			model.addAttribute("checkoutData", checkoutData);
			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount",payableTotalAmount);
		} catch (CartExpiredException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "ExpiredCart");

			logger.info("Cart Expired: " + e);
			if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
			logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
		
        metricsClient.recordEvent("Payment.Mobile.Attempt", "CC.Save.Error", "GenericFailure");
        logPaymentRequestCompleteMetric(request);
		return new ModelAndView(errorView);
	}

	private void reinitCheckoutData(CheckoutData checkoutData) throws CartExpiredException {
		String productType = checkoutData.getProductType();
		String lookupid = checkoutData.getLookupId();
		
		if (checkoutData.getPaymentOptions().getCreditCard() == null){
			checkoutData.getPaymentOptions().setCreditCard(paymentOptionsHelper.getCreditCard());
		}
		if (checkoutData.getPaymentOptions().getDebitCard() == null){
			checkoutData.getPaymentOptions().setDebitCard(paymentOptionsHelper.getDebitCard(productType));
		}
		
		if (checkoutData.getPaymentOptions().getDebitCard() != null && checkoutData.getPaymentOptions().getDebitCard().getCardTypeList() == null
				&& productType != null){
			checkoutData.getPaymentOptions().getDebitCard().setCardTypeList(paymentOptionsHelper.getDebitCardTypes(productType));
		}
		
		if (checkoutData.getPaymentOptions().getNetBanking() == null){
			checkoutData.getPaymentOptions().setNetBanking(paymentOptionsHelper.getNetBanking(productType));
		}
		if (checkoutData.getPaymentOptions().getWallet() == null){
			checkoutData.getPaymentOptions().setWallet(paymentOptionsHelper.getWalletData());
		}
		checkoutData.getPaymentOptions().getCreditCard().setYearList(CommonUtil.getYearListForCards());
		checkoutData.getPaymentOptions().getDebitCard().setYearList(CommonUtil.getYearListForCards());
		checkoutData.getPaymentOptions().getDebitCard().setCardTypeList(paymentOptionsHelper.getDebitCardTypes(productType));
		checkoutData.getPaymentOptions().getNetBanking().setBankList(paymentOptionsHelper.getNetBankingBanks(productType));
		//For partial payment and Wallet Eligibility
		if (checkoutData.getPaymentOptions().getWalletEligibility() == null) {
			checkoutData.getPaymentOptions().setWalletEligibility(paymentOptionsHelper.getWalletEligibility(lookupid));
		}
		if (checkoutData.getPaymentOptions().getPaymentBreakup() == null) {
			checkoutData.getPaymentOptions().setPaymentBreakup(paymentOptionsHelper.getPaymentBreakup(lookupid));
		}
	}
	
	@RequestMapping(value = "do-debit-card-pay", method = RequestMethod.GET)
	public ModelAndView paymentOptionDebitCardGet(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-debit-card-pay", method = RequestMethod.POST)
	public ModelAndView paymentOptionDebitCard(@Valid CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

	    final long startTime = System.currentTimeMillis();
	    logPaymentRequestStartMetric(request);
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_DEBITCARD, METRIC_ATTEMPT);
	    String errorView = "mobile/payment/payment-options-view";
		Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
		try {
			String fraudDetecedView = "mobile/payment/fraud-detected-view";
			
			if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
			
			if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
		        metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "GenericFailure");
			    
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                logPaymentRequestCompleteMetric(request);
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			if (checkoutData.getPaymentOptions().getDebitCard() == null){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "NoData");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                 
				reinitCheckoutData(checkoutData);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			checkoutData.getPaymentOptions().getDebitCard().setSelected(true);
			if (result.hasErrors()) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "ValidationFailure");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                if(cartBusinessDo == null)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
	            List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
	            model.addAttribute("allowablePGList", allowablePGList);
				reinitCheckoutData(checkoutData);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
			try {
				Map<String, String> requestMap = checkoutService2.doDebitCardPayment(checkoutData, checkoutData.getPaymentOptions().getCardStorage(), checkoutData
						.getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request),checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
				model.put("actionUrl", requestMap.get("actionUrl"));
				model.put("resultMap", requestMap);
				logRequestToPGMetric((String)requestMap.get("actionUrl"), request);
                metricsClient.recordEvent("Payment.Mobile.Attempt", "DC", "Succes");
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_DEBITCARD,
                        System.currentTimeMillis() - startTime);
                logPaymentRequestCompleteMetric(request);
				return new ModelAndView("mobile/payment/request/payment-request-view");

			} catch(FraudDetectedException rc) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "FraudAttempt");

			    logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
			    logPaymentRequestCompleteMetric(request);
				return new ModelAndView(fraudDetecedView);
			} catch (Exception e) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "Exception");

				logger.error("Error occured while paying using Debit Card for lookup id: " + checkoutData.getLookupId(), e);
				logger.error(e.getMessage());
				reinitCheckoutData(checkoutData);
			}
			checkoutData.setIsPaymentSuccessful(false);
		} catch (CartExpiredException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "ExpiredCart");

			logger.info("Cart Expired: " + e);
			if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
			logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
		
        metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Error", "GenericFailure");
        logPaymentRequestCompleteMetric(request);
		return new ModelAndView(errorView);
	}	
	
	@RequestMapping(value = "do-save-debit-card-pay", method = RequestMethod.GET)
	public ModelAndView paymentOptionSaveDebitCard(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-save-debit-card-pay", method = RequestMethod.POST)
	public ModelAndView paymentOptionSaveDebitCard(CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

	    final long startTime = System.currentTimeMillis();
	    logPaymentRequestStartMetric(request);
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_DEBITCARD, METRIC_ATTEMPT);
	    String errorView = "mobile/payment/payment-options-view";
	    Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
		try {
			String fraudDetecedView = "mobile/payment/fraud-detected-view";
			
			if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
			
			if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "NullValidationFail");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    logPaymentRequestCompleteMetric(request);
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			if (checkoutData.getPaymentOptions().getDebitCard() == null){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "NoData");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
				reinitCheckoutData(checkoutData);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			checkoutData.getPaymentOptions().getDebitCard().setSelected(true);
			if (result.hasErrors()) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "ValidationFailure");

			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
                model.addAttribute("allowablePGList", allowablePGList);
				reinitCheckoutData(checkoutData);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
			PaymentRequestBusinessDO prbdo;
			try {
				Map<String, String> requestMap = checkoutService2.doSaveDebitCardPayment(checkoutData, checkoutData.getPaymentOptions().getCardStorage(), checkoutData
						.getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request),checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
				model.put("actionUrl", requestMap.get("actionUrl"));
				model.put("resultMap", requestMap);
				logRequestToPGMetric((String)requestMap.get("actionUrl"), request);
				metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save", "Succes");
				metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_DEBITCARD,
                        System.currentTimeMillis() - startTime);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView("mobile/payment/request/payment-request-view");

			} catch(FraudDetectedException rc) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "FraudAttempt");

				logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(fraudDetecedView);
			} catch (Exception e) {
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "Exception");

				logger.error("Error occured while paying using Debit Card for lookup id: " + checkoutData.getLookupId(), e);
				logger.error(e.getMessage());
			}
			//If any exception setting checkoutdata.
			reinitCheckoutData(checkoutData);
			model.addAttribute("checkoutData", checkoutData);
			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount",payableTotalAmount);
		} catch (CartExpiredException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "ExpiredCart");

			logger.info("Cart Expired: " + e);
			if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
			logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}

		metricsClient.recordEvent("Payment.Mobile.Attempt", "DC.Save.Error", "GenericFailure");
		logPaymentRequestCompleteMetric(request);
		return new ModelAndView(errorView);
	}	
	
	@RequestMapping(value = "do-fc-balance-pay", method = RequestMethod.GET)
	public ModelAndView paymentOptionFCBalanceGet(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-fc-balance-pay", method = RequestMethod.POST)
	public ModelAndView paymentOptionFCBalance(@Valid CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
	    
	    final long startTime = System.currentTimeMillis();
	    logPaymentRequestStartMetric(request);
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_WALLET, METRIC_ATTEMPT);
		String view = "mobile/payment/payment-options-view";
		String fraudDetecedView = "mobile/payment/fraud-detected-view";
		
		Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
        
        if(isNEWMobileAPP)
        {
            view = MobileConstants.paymentAttemptGeneralError();
        }
		
		if(checkoutData==null||checkoutData.getLookupId()==null || StringUtils.isBlank(checkoutData.getLookupId()))
		{
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "NullValidationFail");

            if(isNEWMobileAPP)
            {
		        view = MobileConstants.paymentAttemptCheckoutBeanError();
		        logPaymentRequestCompleteMetric(request);
                return new ModelAndView(view);
            }
            logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
		}
		if (checkoutData.getProductType().equals(FCConstants.WALLET_FUND_PRODUCT_TYPE)) {
		    metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "AddCash");
		    view = MobileConstants.paymentAttemptCheckoutBeanError();
		    logPaymentRequestCompleteMetric(request);
            return new ModelAndView(view);
		}
		Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
		try {
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Map requestMap = checkoutService2.doWalletPayment(checkoutData, null, remoteIp, MobileUtil.getMobileChannel(request), appVersion);
			Map resultMap = (Map) requestMap.get("resultMap");
			if(resultMap==null || requestMap.get(PaymentConstants.IS_METHOD_SUCCESSFUL).equals("false"))
			{
	            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "DelegateFailure");

			    view = "mobile/payment/response/payment-error-view";
			    
			    if(isNEWMobileAPP)
	            {
	                view = MobileConstants.paymentAttemptCheckoutBeanError();
	                logPaymentRequestCompleteMetric(request);
	                return new ModelAndView(view);
	            }
			    logPaymentRequestCompleteMetric(request);
			    return new ModelAndView(view);
			    			    
			}
			
			String hashKey = (String) resultMap.get(PaymentConstants.HASH);// (String) prbdo.getRequestMap().get(PaymentConstants.HASH);
			String merchantId = String.valueOf(resultMap.get(PaymentConstants.MERCHANT_ORDER_ID)); // (String) prbdo.getRequestMap().get(PaymentConstants.MERCHANT_ORDER_ID);
			String userId = String.valueOf(resultMap.get("userId"));// prbdo.getUserId(); // (String) prbdo.getRequestMap().get(WebConstants.SESSION_USER_USERID);
			Map<String, String> responseParams = checkoutService.doFCBalancePay(checkoutData.getLookupId(), hashKey, merchantId, userId);
			responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, merchantId);
			responseParams.put(PaymentConstants.USER_ID, userId);
			checkoutData = checkoutService.handlePaymentResponse(responseParams, PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE , Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET));
			Boolean isPaymentSuccessful = checkoutData.getIsPaymentSuccessful();
			model.addAttribute("checkoutData", checkoutData);

			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount", payableTotalAmount);
			//publishing Payment Attempt Status
			notifyPayment(checkoutData,isPaymentSuccessful);
			if (!isPaymentSuccessful){
			    CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
                model.addAttribute("allowablePGList", allowablePGList);
                metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "PaymentFailure");
                view =  "mobile/payment/payment-options-view";
			}else {
				model.addAttribute("finalResultSet", true);
				model.addAttribute("finalResultPage", true); // used by campaignTracking.jsp
				model.addAttribute("mapAmount", checkoutData.getTotalAmount());
				view = "mobile/payment/response/payment-response-view";

                metricsClient.recordEvent("Payment.Mobile.Attempt", "FW", "Succes");
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_WALLET,
                        System.currentTimeMillis() - startTime);

		        if(isNEWMobileAPP)
		        {
		            view = MobileURLConstants.MOBILE_APP_STSTUS+"/"+checkoutData.getIsPaymentSuccessful()+"/"+checkoutData.getOrderId()+"/false/result";
		        }
			}
		} catch (NumberFormatException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "NumberFormatException");

			logger.error("Error occured while parsing payment type to number PaymentType: " + PaymentConstants.PAYMENT_TYPE_WALLET + " Lookup Id: " + checkoutData.getLookupId(), e);
			logger.error(e.getMessage());
		} catch (FCPaymentPortalException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "PaymentPortalException");

			logger.error("Error occured while paying using FCBalance for LookupID : " + checkoutData.getLookupId(), e);
			logger.error(e.getMessage());
		} catch (FCPaymentGatewayException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "PGException");
		    
			logger.error("Error occured while paying using FCBalance for LookupID : " + checkoutData.getLookupId(), e);
			logger.error(e.getMessage());
		} catch(FraudDetectedException rc) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "FraudAttempt");

            logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
            logPaymentRequestCompleteMetric(request);
			return new ModelAndView(fraudDetecedView);
		} catch (Exception e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "FW.Error", "Exception");

			logger.error("Error occured while paying using FCBalance for LookupID : " + checkoutData.getLookupId(), e);
			logger.error(e.getMessage());
		}
		logPaymentRequestCompleteMetric(request);
		return new ModelAndView(view);
	}	
	
	@RequestMapping(value = "do-net-banking-pay", method = RequestMethod.GET)
	public ModelAndView paymentOptionNetBankingGet(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
	@RequestMapping(value = "do-net-banking-pay", method = RequestMethod.POST)
	public ModelAndView paymentOptionNetBanking(@Valid CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
	        HttpServletResponse response) {

	    final long startTime = System.currentTimeMillis();
	    logPaymentRequestStartMetric(request);
	    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_NETBANKING, METRIC_ATTEMPT);
	    String errorView = "mobile/payment/payment-options-view";
	    Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
		try {
			String fraudDetecedView = "mobile/payment/fraud-detected-view";
			
			if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
			
			if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
			    metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "NullValidationFail");

			    if(isNEWMobileAPP)
                {
			        errorView = MobileConstants.paymentAttemptCheckoutBeanError();
			        logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
			    logPaymentRequestCompleteMetric(request);
				return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
			}
			if (checkoutData.getPaymentOptions().getNetBanking() == null){
                metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "NoData");
			    
			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
				reinitCheckoutData(checkoutData);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			checkoutData.getPaymentOptions().getNetBanking().setSelected(true);
			if (result.hasErrors()) {
                metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "ValidationFailure");
			    
			    if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
				reinitCheckoutData(checkoutData);
				Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
				CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
	            List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
	            model.addAttribute("allowablePGList", allowablePGList);
				model.addAttribute("payableTotalAmount",payableTotalAmount);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(errorView);
			}
			
			String remoteIp = FCInfraUtils.getSecureCookie(request);
			Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
			String fingerPrint = request.getParameter(FCConstants.IMEI);
			try {
				Map<String, String> requestMap = checkoutService2.doNetBankingPayment(checkoutData, checkoutData
						.getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request), checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion, fingerPrint);
				model.put("actionUrl", requestMap.get("actionUrl"));
				model.put("resultMap", requestMap);
				logRequestToPGMetric((String)requestMap.get("actionUrl"), request);
				metricsClient.recordEvent("Payment.Mobile.Attempt", "NB", "Succes");
				metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_NETBANKING,
                        System.currentTimeMillis() - startTime);
				logPaymentRequestCompleteMetric(request);
                return new ModelAndView("mobile/payment/request/payment-request-view");
			} catch(FraudDetectedException rc) {
                metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "FraudAttempt");
			    logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
				logPaymentRequestCompleteMetric(request);
				return new ModelAndView(fraudDetecedView); 
			} catch (Exception e) {
                metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "Exception");
				logger.error("Error occured while paying using NetBanking for LookupID : " + checkoutData.getLookupId(), e);
				logger.error(e.getMessage());
			}
			//If exception throws set model
			reinitCheckoutData(checkoutData);
			Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
			model.addAttribute("payableTotalAmount",payableTotalAmount);
		} catch (CartExpiredException e) {
            metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "ExpiredCart");
			logger.info("Cart Expired: " + e);
			if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
			logPaymentRequestCompleteMetric(request);
			return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
		}
        metricsClient.recordEvent("Payment.Mobile.Attempt", "NB.Error", "GenericFailure");
        logPaymentRequestCompleteMetric(request);
		return new ModelAndView(errorView);
	}	
	
	@RequestMapping(value = "do-hdfc-debit-pay", method = RequestMethod.GET)
    public ModelAndView paymentOptionHdfcDebitGet(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
    }
    
    @RequestMapping(value = "do-hdfc-debit-pay", method = RequestMethod.POST)
    public ModelAndView paymentOptionHdfcDebit(@Valid CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
            HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        logPaymentRequestStartMetric(request);
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD, METRIC_ATTEMPT);
        String errorView = "mobile/payment/payment-options-view";
        
        Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
        try {
            String fraudDetecedView = "mobile/payment/fraud-detected-view";
            
            if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
            
            if (checkoutData == null || checkoutData.getPaymentOptions() == null || StringUtils.isBlank(checkoutData.getLookupId())){
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                reinitCheckoutData(checkoutData);
                Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
                model.addAttribute("payableTotalAmount",payableTotalAmount);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
          
            if (result.hasErrors()) {
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                reinitCheckoutData(checkoutData);
                Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
                model.addAttribute("payableTotalAmount",payableTotalAmount);
                CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
                model.addAttribute("allowablePGList", allowablePGList);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
            
            String remoteIp = FCInfraUtils.getSecureCookie(request);
            Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
            try {
                Map<String, String> requestMap = checkoutService2.dohdfcDebitPayment(checkoutData, checkoutData
                        .getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request), checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
                model.put("actionUrl", requestMap.get("actionUrl"));
                model.put("resultMap", requestMap);
                logRequestToPGMetric((String)requestMap.get("actionUrl"), request);
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD,
                        System.currentTimeMillis() - startTime);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView("mobile/payment/request/payment-request-view");
            } catch(FraudDetectedException rc) {
                logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(fraudDetecedView); 
            } catch (Exception e) {
                logger.error("Error occured while paying using NetBanking for LookupID : " + checkoutData.getLookupId(), e);
                logger.error(e.getMessage());
            }
            //If exception throws set model
            reinitCheckoutData(checkoutData);
            Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
            model.addAttribute("payableTotalAmount",payableTotalAmount);
        } catch (CartExpiredException e) {
            logger.info("Cart Expired: " + e);
            if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
            logPaymentRequestCompleteMetric(request);
            return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
        }
        logPaymentRequestCompleteMetric(request);
        return new ModelAndView(errorView);
    }
	
	
	@RequestMapping(value = "do-zero-payment-pay", method = RequestMethod.GET)
    public ModelAndView paymentOptionZeroPaymentGet(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
    }
    
    
    @RequestMapping(value = "do-zero-payment-pay", method = RequestMethod.POST)
    public ModelAndView paymentOptionZeroPay(@Valid CheckoutData checkoutData, BindingResult result, ModelMap model, HttpServletRequest request,
            HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        logPaymentRequestStartMetric(request);
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + PaymentConstants.PAYMENT_TYPE_ZEROPAY, METRIC_ATTEMPT);
        String errorView = "mobile/payment/payment-options-view";
        
        Boolean isNEWMobileAPP = MobileUtil.isNewMobileAppChannel(request);
        try {
            String fraudDetecedView = "mobile/payment/fraud-detected-view";
            
            if(isNEWMobileAPP)
            {
                 fraudDetecedView = MobileConstants.paymentAttemptFruadDetectionError();
                 errorView = MobileConstants.paymentAttemptGeneralError();
            }
            
            if (checkoutData == null || StringUtils.isBlank(checkoutData.getLookupId())){
                
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanError();
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
            }
            
            
            if (result.hasErrors()) {
                if(isNEWMobileAPP)
                {
                    errorView = MobileConstants.paymentAttemptCheckoutBeanValidationError(result.getAllErrors().toString());
                    logPaymentRequestCompleteMetric(request);
                    return new ModelAndView(errorView);
                }
                reinitCheckoutData(checkoutData);
                Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
                CartBusinessDo cartBusinessDo = cartService.getCart(checkoutData.getLookupId());
                List<String> allowablePGList = freefundService.getAllowablePGList(cartBusinessDo, null);
                model.addAttribute("allowablePGList", allowablePGList);
                model.addAttribute("payableTotalAmount",payableTotalAmount);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
            
            String remoteIp = FCInfraUtils.getSecureCookie(request);
            Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
            try {
                Map<String, String> requestMap = checkoutService2.doZeroPayment(checkoutData, checkoutData
                        .getLookupId(), null, remoteIp, MobileUtil.getMobileChannel(request), checkoutData.getPaymentOptions().getUsePartialPayment(), appVersion);
                model.put("actionUrl", requestMap.get("actionUrl"));
                model.put("resultMap", requestMap);
                metricsClient.recordLatency("Payment", "Old.Request." + PaymentConstants.PAYMENT_TYPE_ZEROPAY,
                        System.currentTimeMillis() - startTime);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView("mobile/payment/request/payment-request-view");
            } catch(FraudDetectedException rc) {
                logger.warn(rc.getCustomMsg()+ ":"+ checkoutData.getLookupId(), rc);
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(fraudDetecedView); 
            } catch (Exception e) {
                logger.error("Error occured while paying using Zero Payment for LookupID : " + checkoutData.getLookupId(), e);
                logger.error(e.getMessage());
            }
            //If exception throws set model
            reinitCheckoutData(checkoutData);
            Amount payableTotalAmount = new Amount(checkoutData.getTotalAmount());
            model.addAttribute("payableTotalAmount",payableTotalAmount);
        } catch (CartExpiredException e) {
            logger.info("Cart Expired: " + e);
            if(isNEWMobileAPP)
            {
                errorView = MobileConstants.paymentAttemptCartExpireError();
                logPaymentRequestCompleteMetric(request);
                return new ModelAndView(errorView);
            }
            logPaymentRequestCompleteMetric(request);
            return new ModelAndView(MobileConstants.CART_EXPIRED_VIEW);
        }
        logPaymentRequestCompleteMetric(request);
        return new ModelAndView(errorView);
    }

	private void logPaymentRequestStartMetric(HttpServletRequest request) {
		metricsClient.recordEvent(METRIC_PAYMENT, METRIC_REQUEST + "."
				+ MobileUtil.getMobileChannel(request), METRIC_REQUEST_START);
	}

	private void logPaymentRequestCompleteMetric(HttpServletRequest request) {
		metricsClient.recordEvent(METRIC_PAYMENT, METRIC_REQUEST + "."
				+ MobileUtil.getMobileChannel(request), METRIC_REQUEST_END);
	}
	
	/**
     * Method logs the PG to which the request should be redirected
     * 
     * @param responseMsg
     */
	private void logRequestToPGMetric(String pgUrl, HttpServletRequest request) {
		if(StringUtils.isEmpty(pgUrl)){
			return;
		}
		String redirectingToPG = null;
		if (pgUrl.equals(fcProperties
				.getProperty(PaymentConstants.PAYU_SUBMIT_URL))) {
			redirectingToPG = "payu";
		} else if (pgUrl.contains("ccavenue")) {
			redirectingToPG = "cca";
		} else if(!pgUrl.equals(ZeroPaymentController.ZERO_PAY_ACTION)){
			redirectingToPG = "bdk";
		}
		if(redirectingToPG != null){
			metricsClient.recordEvent("Payment", "PG.Request." + MobileUtil.getMobileChannel(request), redirectingToPG);
		}
	}
	
	  private void notifyPayment(CheckoutData checkoutData,boolean isPaymentSuccessful) {
	        logger.info("paymentSNSService publish triggered for orderId : " + checkoutData.getOrderId());
	        try {
	            String orderId=checkoutData.getOrderId();
	            PaymentAlertBean paymentAlertBean = new PaymentAlertBean();
	            paymentAlertBean.setMtxnId(checkoutData.getOrderId());
	            paymentAlertBean.setOrderId(checkoutData.getOrderId());
	            paymentAlertBean.setMessageType(isPaymentSuccessful ? PaymentStatus.PAYMENT_SUCCESS.getStatus()
	                    : PaymentStatus.PAYMENT_FAILURE.getStatus());

            Integer paymentCode = paymentTransactionService.getPaymentTxnDetails(orderId).get(0).getPaymentType();
	            String paymentTypeName = PaymentType.PaymentTypeName.fromPaymentTypeCode(
	                  paymentCode).toString();
	            paymentAlertBean.setPaymentType(paymentTypeName);

	          
	            paymentAlertBean.setChannel(FCUtil.getChannelFromChannelType(getChannelFromOrderId(orderId)));
	            if (!StringUtils.isEmpty(paymentTransactionService.getPaymentTxnDetails(orderId).get(0).getPaymentGateway())) {
	               paymentAlertBean.setPaymentGateway(paymentTransactionService.getPaymentTxnDetails(orderId).get(0).getPaymentGateway());
	            }
	            
	            CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(orderId);
	            
	            convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, paymentAlertBean);
	            
	            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
	            paymentAlertBean.setProduct(primaryProduct.name());
	            paymentAlertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(orderId).longValue());
	            
	            com.freecharge.payment.dos.entity.PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlan(orderId);
	            paymentAlertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
	            PaymentPlan paymentPlan = new PaymentPlan();
	            paymentPlan.setPg(fcPaymentPlan.getPgAmount().getAmount().doubleValue());
	            paymentPlan.setCredits(fcPaymentPlan.getWalletAmount().getAmount().doubleValue());
	            paymentAlertBean.setPaymentPlan(paymentPlan);
	            
	            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
	            String operator = userTransactionHistory.getServiceProvider();
	            //Set GST
	            gstService.populateGST(paymentAlertBean, primaryProduct,operator);

	            paymentSNSService.publish(orderId, "{\"message\" : " + PaymentAlertBean.getJsonString(paymentAlertBean) + "}");
	            logger.info("Alert :"+PaymentAlertBean.getJsonString(paymentAlertBean));
	        } catch (Exception e) {
	            logger.error("paymentSNSService publish failure for orderId : " + checkoutData.getOrderId());
	        }
	    }

    private String getChannelFromOrderId(String orderId) {
        try {
            if (orderId != null && orderId.length() >= 4) {
                String fourthChar = String.valueOf(orderId.charAt(3));
                return fourthChar;
            }
        } catch (Exception e) {
            logger.error("Exception on getting channel from order id ", e);
        }
        return null;
    }
}
