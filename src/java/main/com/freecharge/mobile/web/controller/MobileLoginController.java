package com.freecharge.mobile.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.framework.util.SessionConstants;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.SessionManager;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.web.view.MobileLogin;
import com.freecharge.mobile.web.view.User;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.useragent.UserLoginEventPublisherHelper;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.controller.LoginController;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileLoginController {
    public static final String METRIC_NAME = "mLoginAcess";
    private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private UserAuthService userAuthService;

	@Autowired 
	private SessionManager sessionManager;

    @Autowired
    private UserLoginEventPublisherHelper userLoginEventPublisherHelper;

    @Autowired
    private MetricsClient metricsClient;

	@RequestMapping(value = "login")
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mlcLogin");
		MobileLogin login = new MobileLogin();
		login.setLoginFrom(getFrom(request));
		login.setFromParamMap(getParameterMap(request));
		User user = getUser(login);
		model.addAttribute("mobileLogin", login);
		model.addAttribute("user", user);
		return new ModelAndView("mobile/login/view", model);
	}
	

	@RequestMapping(value = "handle-login")
	public ModelAndView handleLogin(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mlcHandleLogin");
		MobileLogin login = new MobileLogin();
		login.setLoginFrom(getFrom(request));
		login.setFromParamMap(getParameterMap(request));
		model.addAttribute("mobileLogin", login);
		User user = getUser(login);
		model.addAttribute("user", user);
		return new ModelAndView("mobile/login/view", model);
	}

	private User getUser(MobileLogin login){
		User user = new User();
		user.setFromParamMap(login.getFromParamMap());
		user.setSignupFrom(login.getLoginFrom());
		return user;
	}
	
	private String getFrom(HttpServletRequest request) {
		String from = (String) request.getAttribute("loginFrom");
		return from;
	}
	
	private Map<String, String[]> getParameterMap(HttpServletRequest request){
		return request.getParameterMap();
	}
	
	@RequestMapping(value = "do-mobile-login", method = RequestMethod.GET)
	public ModelAndView doLoginGet(MobileLogin login, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}

	@RequestMapping(value = "do-mobile-login", method = RequestMethod.POST)
	public ModelAndView doLogin(MobileLogin login, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
	    
	    
		doLogin(login, request, response);
		if (login.getLoginError() != null && !login.getLoginError().isEmpty()){
			User user = getUser(login);
			model.addAttribute("user", user);
			return new ModelAndView("mobile/login/view");
		}
		try {
			if (login.getLoginFrom() == null || login.getLoginFrom().isEmpty())
			{
				response.sendRedirect("/m");
			}
			else{
				request.getRequestDispatcher(login.getLoginFrom()).forward(request, response);				
			}
		} catch (ServletException e) {
			logger.error("Error doing login: ", e);
		} catch (IOException e) {
			logger.error("Error doing login: ", e);
		}
		return null;
	}
	
/*	@RequestMapping(value = "is-logged-in")
	public @ResponseBody Boolean isLoggedIn(MobileLogin login, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, FCConstants.METRICS_CONTROLLER_USED, "mlcIsLoggedIn");
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		Boolean isLoggedIn = false;
		if (fs != null) {
			Map<String, Object> sessionData = fs.getSessionData();
			if (sessionData != null){
				Object isLoggedInObj = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
				if (isLoggedInObj != null){
					isLoggedIn = (Boolean) isLoggedInObj;
				}
			}
		}
		return isLoggedIn;
	}*/

	private void doLogin(MobileLogin login, HttpServletRequest request,
			HttpServletResponse response) {
	    long startTime = System.currentTimeMillis();
		try {
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "click");
		    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "mobileLogin");
			if (login.getLoginEmail() == null || login.getLoginEmail().trim().isEmpty()){
				login.setLoginError("Enter a valid user name (registered e-mail Id).");
			} else if(login.getLoginPassword() == null || login.getLoginPassword().trim().isEmpty()){
				login.setLoginError("Enter password.");
			}
			
			else if (FCSessionUtil.isAnotherUserLoggedIn(login.getLoginEmail())) {
			    login.setLoginError("Another user is already logged in from the same browser. To continue using this account, you will have to sign out the other user and login in again.This is done to protect your account and to ensure the privacy of your information.");
            }
			else
			{
				LoginBusinessDO loginBusinessDO = new LoginBusinessDO();
				loginBusinessDO.setEmail(login.getLoginEmail());
				loginBusinessDO.setPassword(login.getLoginPassword());
				userAuthService.doLogin(loginBusinessDO);
				if (loginBusinessDO.isLogin()) {
					userAuthService.doUserProfile(loginBusinessDO);
					Integer userId = Integer.parseInt(loginBusinessDO.getUserId());
					String firstName = getFirstNameWithCapitalLetter(loginBusinessDO.getFirstName());
					FreechargeSession.setUserRelatedSessionData(
							loginBusinessDO.getEmail(),
							firstName,
							loginBusinessDO.isLogin(), userId, SessionConstants.LoginSource.EMAIL,
							request.getParameter(FCConstants.IMEI), request.getParameter(FCConstants.DEVICE_UNIQUE_ID));
					sessionManager.storeSession(request, response);

                    metricsClient.recordEvent(LoginController.METRIC_SERVICE, METRIC_NAME, "success");
                    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "success");
                    userLoginEventPublisherHelper.publish(request, this);
				}else{
                    metricsClient.recordEvent(LoginController.METRIC_SERVICE, METRIC_NAME, "failure");
                    metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "fail");
					login.setLoginError("Check user name and password.");
				}	
			}
		} catch (Exception e) {
			logger.error("Error in doing login for user : " + login.getLoginEmail(), e);
			metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "login", "exception");
		}
		long endTime = System.currentTimeMillis();
        metricsClient.recordLatency(HomeController.METRICS_PAGE_ACCESS, "login", endTime - startTime);
	}

	private String getFirstNameWithCapitalLetter(String firstName) {
		if(firstName!=null && !firstName.isEmpty()){
	    	firstName = WordUtils.capitalize(firstName);
		}
		return firstName;
	}
}
