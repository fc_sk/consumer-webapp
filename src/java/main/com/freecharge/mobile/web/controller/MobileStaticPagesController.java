package com.freecharge.mobile.web.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.mobile.constant.MobileURLConstants;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileStaticPagesController {

    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private AppConfigService appConfigService;
    
    @RequestMapping(value = "pepsicampaign", method = RequestMethod.GET)
    public String pepsicampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String view = "mobile/mobile-pepsipromotion";
        
        if(appConfigService.isPepsiCampaignEnabled()) {
            return view;
        } else {
            return this.get403HtmlResponse(response);
        }
    }
    
    @RequestMapping(value = "didcampaign", method = RequestMethod.GET)
    public String didcampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String view = "mobile/mobile-didpromotion";
        
        if(appConfigService.isDIDCampaignEnabled()) {
            return view;
        } else {
            return this.get403HtmlResponse(response);
        }
    }

    @RequestMapping(value = "mcdcampaign", method = RequestMethod.GET)
    public String ncdcampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String view = "mobile/mobile-mcdpromotion";
        
        if(appConfigService.isMCDCampaignEnabled()) {
            return view;
        } else {
            return this.get403HtmlResponse(response);
        }
    }

    @RequestMapping(value = "appinstall", method = RequestMethod.GET)
    public String appcampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-appinstall";        
    }

    @RequestMapping(value = "appufb", method = RequestMethod.GET)
    public String appufb(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-appufb";
    }

    @RequestMapping(value = "googleplusone", method = RequestMethod.GET)
    public String googleplusone(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-googleplusone";
    }

    @RequestMapping(value = "myt10", method = RequestMethod.GET)
    public String youtubecampaign(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-youtubecampaign";
    }

    @RequestMapping(value = "cbsuccess", method = RequestMethod.GET)
    public String cbsuccess(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-cbsuccess";
    }

    @RequestMapping(value = "mobilenf", method = RequestMethod.GET)
    public String mobilenf(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        return "mobile/mobile-notification";
    }

    private String get403HtmlResponse(HttpServletResponse response) throws IOException{
        return this.get403Response(response, "text/html");
    }
    
    private String get403Response(HttpServletResponse response, String contentType) throws IOException {
        return this.getErrorResponse(response, HttpServletResponse.SC_FORBIDDEN, contentType, "Permission Denied. ");
    }
    
    private String getErrorResponse(HttpServletResponse response, Integer status, String contentType, String message)
            throws IOException{
        response.setStatus(status);
        response.setContentType(contentType);
        PrintWriter pw = response.getWriter();
        pw.write(message);
        pw.flush();
        return null;
    }
       
}
