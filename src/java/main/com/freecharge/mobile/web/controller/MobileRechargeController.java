package com.freecharge.mobile.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.MobileOperatorCircleService;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.MobileRecharge;
import com.freecharge.mobile.web.view.OperatorCircle;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.rest.validators.AmountFormatValidator;
import com.freecharge.rest.validators.ServiceNumberFormatValidator;

@Controller
@RequestMapping(MobileURLConstants.MOBILE_SITE_PREFIX + "/*")
public class MobileRechargeController {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private MobileOperatorCircleService mobileOperatorCircleService;

	@Autowired
	private MasterData masterData;

	@Autowired
	private MobileCheckoutService checkoutService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private MobileSeoHelper mobileSeoHelper;

    @Autowired
    private RechargeMetricService rechargeMetricService;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private OperatorAlertService alertService;
    
	@RequestMapping(value = "mobile-recharge", method = RequestMethod.GET)
	public ModelAndView mobileRecharge(Model model, HttpServletRequest request) {
		String view = "mobile/mobile-recharge-full";
		MobileRecharge mobileRecharge = RechargeUtil.getMobileRecharge(request, masterData);
		model.addAttribute("mobileRecharge", mobileRecharge);
		mobileSeoHelper.handleRechargeSEO(request, model);
		return new ModelAndView(view);
	}

	/**
	 * Input mobileNumber for which operator and circle data is required.
	 * 
	 * @param mobileNumber
	 *            mobile number request parameter sent by client.
	 * @return returns JSON format of OperatorCircle. ReponseBody annotation
	 *         takes care of converting java object to JSON.
	 */
	@RequestMapping(value = "mobile-circles")
	public @ResponseBody
	OperatorCircle mobileCircles(@RequestParam String mobileNumber) {
		if (mobileNumber == null || mobileNumber.length() < 4) {
			return null;
		}
		return getOperatorCircle(mobileNumber);
	}

	private OperatorCircle getOperatorCircle(String mobileNumber) {
		OperatorCircleBusinessDO operatorCicleBDO = new OperatorCircleBusinessDO();
		operatorCicleBDO.setPrefix(mobileNumber);
		operatorCicleBDO.setProductType("1");
		return mobileOperatorCircleService.getOperatorCircle(operatorCicleBDO);
	}

	@RequestMapping(value = "do-mobile-recharge", method = RequestMethod.GET)
	public ModelAndView doMobileRechargeGet(HttpServletRequest request,	HttpServletResponse response) {
		return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
	}
	
    @RequestMapping(value = "do-mobile-recharge", method = RequestMethod.POST)
    public ModelAndView doMobileRecharge(@Valid MobileRecharge mobileRecharge, BindingResult result, ModelMap model,
            HttpServletRequest request, HttpServletResponse response) {
        String DEFAULT_VIEW = "mobile/mobile-recharge-full";

        mobileRecharge.setMasterData(masterData);

        if (result.hasErrors()) {
            return new ModelAndView(DEFAULT_VIEW);
        }
        String number = mobileRecharge.getMobileNumber();
        String amount = mobileRecharge.getAmount();
        String operatorId = mobileRecharge.getOperator();
        String circleId = mobileRecharge.getCircle();
        String rechargeType = mobileRecharge.getRechargeType();

        ErrorCode validationResult = ServiceNumberFormatValidator.validate(number, 10);

        if (validationResult != ErrorCode.NO_ERROR) {

            logger.error("Mobile Number validation failed with: " + validationResult.getErrorNumberString());
            return new ModelAndView(DEFAULT_VIEW);
        }

        ErrorCode amountValidation = AmountFormatValidator.validate(amount);

        if (amountValidation != ErrorCode.NO_ERROR) {
            logger.error("amount validation failed with: " + amountValidation.getErrorNumberString());
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (StringUtils.isBlank(operatorId)) {
            logger.error("Null or empty operator");
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (StringUtils.isBlank(circleId)) {
            logger.error("Null or empty circleId");
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (StringUtils.isBlank(rechargeType)) {
            rechargeType = FCConstants.RECHARGE_TYPE_TOPUP;
        }

        SuccessfulRechargeInfo previousRecharge = userService.getLastSuccessfulRechargeByNumber(number);

        if (previousRecharge != null) {
            logger.error("Repeat recharge blocked for number: " + number);
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "Repeat");
            model.addAttribute("retryBlock", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (rechargeMetricService.isInvalidDenominationNumber(ProductName.Mobile.getProductType(), number, operatorId, new Double(amount), rechargeType)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "InvalidDenominationNumber");
            model.addAttribute("denominationFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (rechargeMetricService.isInvalidDenomination(operatorId, circleId, new Double(amount), rechargeType, ProductMaster.ProductName.Mobile.getProductType())) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "InvalidDenomination");
            model.addAttribute("denominationFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        if (rechargeMetricService.isPermanentFailure(ProductName.Mobile.getProductType(), operatorId, number)) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "PermanentFailure");
            model.addAttribute("permanentFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        
        if (rechargeMetricService.isEGFailureNumber(number, operatorId, new Double(amount))) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "EG");
            model.addAttribute("egFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }
        

        if (alertService.isOperatorInAlert(Integer.parseInt(operatorId))) {
            metricsClient.recordEvent("Recharge.mobile", "Save.ServerBlock", "OperatorAlert");
            model.addAttribute("operatorFailure", Boolean.TRUE);
            return new ModelAndView(DEFAULT_VIEW);
        }

        logger.debug("Passed validation for mobile numer:" + number);
        logger.info("Mobile recharge values entered :" + "Number:" + number + " " + "Amount:" + amount + " "
                + "operatorID:" + operatorId + " " + "Circle Id:" + circleId + " " + "Is Remember?true");

        Cookie mobileRememberCookie = new Cookie("mobileRememberCookie", "true");
        response.addCookie(mobileRememberCookie);
        // Add the rechargeType to freecharge session.
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        fs.setRechargePlanType(rechargeType);
        createCookies(response, number, amount, operatorId, circleId, rechargeType);
        String lookupId = checkoutService.saveTxnHomePage(number, Integer.parseInt(amount),
                FCConstants.productMasterMap.get(FCConstants.MOBILE_PRODUCT_MASTER_ID), operatorId, circleId);
        if (StringUtils.isEmpty(lookupId)) {
            return new ModelAndView(MobileURLConstants.MOBILE_HOME_REDIRECT);
        }

        String successView = MobileURLConstants.MOBILE_COUPONS_SELECT_REDIRECT + "?lid=" + lookupId;
        return new ModelAndView(successView);
    }

	private void createCookies(HttpServletResponse response, String number,
			String amount, String operatorId, String circleId, String rechargeType) {
		Cookie mobileNumberCookie = new Cookie("mobileNumberCookie", number);
		Cookie mobileAmountCookie = new Cookie("mobileAmountCookie", amount);
		Cookie mobileOperatorCookie = new Cookie("mobileOperatorCookie", operatorId);
		Cookie mobileCircleCookie = new Cookie("mobileCircleCookie", circleId);
		Cookie mobileRechargeTypeCookie = new Cookie("mobileRechargeTypeCookie", rechargeType);
		mobileNumberCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		mobileOperatorCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		mobileAmountCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		mobileCircleCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		mobileRechargeTypeCookie.setMaxAge(MobileConstants.COOKIE_MAX_AGE);
		response.addCookie(mobileNumberCookie);
		response.addCookie(mobileAmountCookie);
		response.addCookie(mobileOperatorCookie);
		response.addCookie(mobileCircleCookie);
		response.addCookie(mobileRechargeTypeCookie);
	}
}
