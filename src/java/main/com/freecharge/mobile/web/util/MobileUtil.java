package com.freecharge.mobile.web.util;

import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import net.sourceforge.wurfl.core.Device;
import net.sourceforge.wurfl.core.WURFLHolder;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.businessdo.PersonalDetailsBusinessDO;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.mobile.web.view.EditAddress;
import com.freecharge.mobile.web.view.EditProfile;
import com.freecharge.mobile.web.view.ShippingDetails;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.util.CookieUtil;
import com.freecharge.web.tld.FCPropertiesReader;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;

public class MobileUtil {
    private final static Logger logger = LoggingFactory.getLogger(MobileUtil.class);

    public static final String UNKNOWN_IP_IDENTIFIER = "unknown";

    private static final String[] IP_HEADERS = {
        "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR"
    };

    public static void setIPAddressInSession(HttpServletRequest httpRequest) {
		String ipAddresss = MobileUtil.getRealClientIpAddress(httpRequest);
    	FreechargeSession session = FCSessionUtil.getCurrentSession();
    	if (session != null){
    		if (ipAddresss != null){
    			session.setIpAddress(ipAddresss);
    		} else{
    			logger.warn("IPAddress is null while setting in session.");
    		}
    			
    	} else{
    		logger.warn("Session is null while coupons were loaded.");
    	}
	}
    
    public static boolean isNewMobilePayment() {
        FreechargeSession session = FCSessionUtil.getCurrentSession();
        Boolean isMobileWeb = (Boolean) session.getSessionData().get(PaymentConstants.IS_MOBILE_WEB);
        
        return BooleanUtils.isTrue(isMobileWeb);
    }
    
    public static boolean isMobileAppPayment() {
        FreechargeSession session = FCSessionUtil.getCurrentSession();
        Boolean isMobileApp = (Boolean) session.getSessionData().get(PaymentConstants.IS_MOBILE_APP);
        
        return BooleanUtils.isTrue(isMobileApp);
    }
    
    public static boolean isMobilePaymentUsingOldPayments(PaymentRequestBusinessDO paymentRequestBDO) {
        if (isNewMobilePayment()) {
            return false;
        }
        boolean isMobilePaymentUsingOldPayments = false;
        if (paymentRequestBDO != null && paymentRequestBDO.getRequestMap() != null) {
            @SuppressWarnings("rawtypes")
            Map requestMap = paymentRequestBDO.getRequestMap();
            if (requestMap != null && requestMap.get(FCConstants.CHANNEL_KEY) != null) {
                Integer channel = (Integer) requestMap.get(FCConstants.CHANNEL_KEY);
                /*
                 * Set response url to '/m/payment/response/*' for
                 *      1. Old mobile site
                 *      2. Mobile apps going through old payments flow.
                 */
                if (channel == FCConstants.CHANNEL_ID_MOBILE
                        || ((channel == FCConstants.CHANNEL_ID_ANDROID_APP || channel == FCConstants.CHANNEL_ID_IOS_APP || channel == FCConstants.CHANNEL_ID_WINDOWS_APP || channel == FCConstants.CHANNEL_ID_MOOLAH_APP) && !PaymentUtil
                                .isNewPaymentsFlow())) {
                    isMobilePaymentUsingOldPayments = true;
                }
            }
        }
        return isMobilePaymentUsingOldPayments;
    }
    
    public static boolean isMobileInterceptEnabled(){
        String booleanStr = FCPropertiesReader.value(FCProperties.MOBILE_SITE_INTERCEPTOR_KEY); 
        return isTrue(booleanStr);
    }

    private static boolean isTrue(String value){
        boolean isTrue = false;
        if (value != null && value.equalsIgnoreCase("true")){
            isTrue = true;
        }
        return isTrue;
    }


    public static final boolean isMobile(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        boolean isWireLess = false;
        WURFLHolder wurflHolder = (WURFLHolder) SpringBeanLocator.getInstance()
                .getSpringBean("wurflHolder");
        Device device = wurflHolder.getWURFLManager().getDeviceForRequest(
                request);
        if (userAgent != null && userAgent.toLowerCase().contains("mobile")){
            isWireLess = true;
        }
        else if (userAgent != null &&
                ((userAgent.toLowerCase().contains("faraday")) ||
                        (userAgent.toLowerCase().contains("phantomjs")))) {
            // This is to make sure SEO-JS's UA is recognized as a desktop browser
            // http://getseojs.com/ and brombone.com

            isWireLess = false;
        }
        else{
            if (device != null) {
                String isWireLessStr = device.getCapability("is_wireless_device");
                try {
                    if (isWireLessStr != null){
                        isWireLess = Boolean.parseBoolean(isWireLessStr);
                    }
                } catch (Exception e) {
                    logger.error("Error classifying device as mobile, " + e.getMessage());
                }
            }
        }
        boolean isTablet = isTablet(device);
        // If tablet mark isWireless as false. Since we don't want mobile site to open on tablets.
        isWireLess = isWireLess && !isTablet;
        return isWireLess;
    }

    public static boolean isTablet(Device device) {
        boolean isTablet = false;
        try {
            if (device != null) {
                String isTabletStr = device.getCapability("is_tablet");
                if (isTabletStr != null) {
                    isTablet = Boolean.parseBoolean(isTabletStr);
                }
            }
        } catch (Exception e) {
            logger.error("Error classifying device as tablet, "
                    + e.getMessage());
        }
        return isTablet;
    }

    public static final String getRequestString(HttpServletRequest request){
        Enumeration en = request.getParameterNames();
        String str = "?";
        boolean first = true;
        while(en.hasMoreElements()){
            String paramName = (String)en.nextElement();
            String paramValue = request.getParameter(paramName);
            if (!first){
                str = str + "&";
            }
            str = str + paramName + "=" + URLEncoder.encode(paramValue);
            first = false;
        }
        if(str.length() == 1){
            str = "";
        }
        return str;
    }

    public static final String getRequestStringWithoutQuestionMark(HttpServletRequest request){
        Enumeration en = request.getParameterNames();
        String str = "";
        boolean first = true;
        while(en.hasMoreElements()){
            String paramName = (String)en.nextElement();
            String paramValue = request.getParameter(paramName);
            if (!first){
                str = str + "&";
            }
            str = str + paramName + "=" + URLEncoder.encode(paramValue);
            first = false;
        }
        if(str.length() == 1){
            str = "";
        }
        return str;
    }

    public static final Device getDevice(HttpServletRequest request) {
        WURFLHolder wurflHolder = (WURFLHolder) SpringBeanLocator.getInstance()
                .getSpringBean("wurflHolder");
        Device device = wurflHolder.getWURFLManager().getDeviceForRequest(
                request);
        return device;
    }

    public static final boolean isValidChannel(String s){
        boolean isValid = false;
        if (s != null && isInteger(s)){
            int channel = Integer.parseInt(s);
            if (channel > 0 && channel < 4){
                isValid = true;
            }
        }
        return isValid;
    }

    public static final boolean isValidAppType(String s){
        boolean isValid = false;
        if (s != null && hasString(MobileConstants.VALID_APP_TYPES, s.toLowerCase())){
            isValid = true;
        }
        return isValid;
    }

    public static boolean hasString(String a[], String s){
        boolean hasString = false;
        if (a != null){
            for (String as : a){
                if (as.equals(s)){
                    hasString = true;
                    break;
                }
            }
        }
        return hasString;
    }

    public static boolean isInteger(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        return true;
    }

    public static final int getMobileChannel(HttpServletRequest request){
        int channel = getMobileChannelCookieValue(request);

        if (channel == 0) {
            return FCConstants.CHANNEL_ID_MOBILE;
        }

        return channel;
    }

    public static int getMobileChannelCookieValue(HttpServletRequest request) {
        Cookie channelCookie = CookieUtil.getCookie(request, MobileConstants.COOKIE_NAME_CHANNEL_ID);
        if (channelCookie != null) {
            return Ints.tryParse(Strings.nullToEmpty(channelCookie.getValue()));
        }

        return 0;
    }
    
    
    public static final boolean isNewMobileAppChannel(HttpServletRequest request){
        // return mobile site channel if not mobile app.
        int channel =  getMobileChannel(request);
            if((channel==FCConstants.CHANNEL_ID_ANDROID_APP && getMobileAppVersion(request) >3) || channel == FCConstants.CHANNEL_ID_IOS_APP || channel == FCConstants.CHANNEL_ID_WINDOWS_APP || channel == FCConstants.CHANNEL_ID_MOOLAH_APP)
            {
               return true;
            }
        
        return false;
                
        //return channel;
    }

    public static final int getMobileAppVersion(HttpServletRequest request){
        // return mobile site channel if not mobile app.
        int version = FCConstants.VERSION_MOBILE_APP;
        Cookie versionCookie = CookieUtil.getCookie(request, MobileConstants.COOKIE_NAME_MOBILE_APP_VERSION);
        if (versionCookie != null && isInteger(versionCookie.getValue())){
            version = Integer.parseInt(versionCookie.getValue());
        }
        return version;
    }

    public static final Integer getMobileAppVersionFromRequest(HttpServletRequest request) {
        // return mobile site channel if not mobile app.
        Integer version = FCConstants.VERSION_MOBILE_APP;
        String versionString = request.getParameter(MobileConstants.COOKIE_NAME_MOBILE_APP_VERSION);
        if (!FCUtil.isEmpty(versionString) && isInteger(versionString)) {
            version = Integer.parseInt(versionString);
        }
        return version;
    }

    public static final int getMobileAffiliate(HttpServletRequest request){
        // return mobile site affilate id if not mobile app.
        int affiliateId = FCConstants.AFFILIATE_ID_MOBILE;
        int channel = getMobileChannel(request);
        if (channel == FCConstants.CHANNEL_ID_ANDROID_APP){
            affiliateId = FCConstants.AFFILIATE_ID_ANDROID_APP;
        }
        else if (channel == FCConstants.CHANNEL_ID_IOS_APP){
            affiliateId = FCConstants.AFFILIATE_ID_IOS_APP;
        }
        else if (channel == FCConstants.CHANNEL_ID_WINDOWS_APP){
            affiliateId = FCConstants.AFFILIATE_ID_WINDOWS_APP;
        } 
        else if (channel == FCConstants.CHANNEL_ID_MOOLAH_APP) {
            affiliateId = FCConstants.AFFILIATE_ID_MOOLAH_APP;
        }
        else if (channel == FCConstants.CHANNEL_ID_TIZEN_PHONE) {
            affiliateId = FCConstants.AFFILIATE_ID_TIZEN_PHONE;
        } else if (channel == FCConstants.CHANNEL_ID_UWP_APP) {
        	affiliateId = FCConstants.AFFILIATE_ID_UWP_APP;
        }
        return affiliateId;
    }

    //Code needed in profile module update	
    public static EditProfile initializeProfile(ProfileBusinessDO profileBusinessDO) {
        EditProfile editProfile = new EditProfile();
        editProfile.setEmail(profileBusinessDO.getEmail());
        editProfile.setTitle(profileBusinessDO.getTitle());
        editProfile.setName(profileBusinessDO.getName());
        editProfile.setMobileNo(profileBusinessDO.getMobileNo());
        return editProfile;
    }

    public static EditAddress initializeAddress(ProfileBusinessDO profileBusinessDO) {
        EditAddress editAddress = new EditAddress();
        editAddress.setAddress(profileBusinessDO.getAddress());
        editAddress.setArea(profileBusinessDO.getArea());
        editAddress.setLandmar(profileBusinessDO.getLandmar());
        if(StringUtils.isNotBlank(profileBusinessDO.getState()))
        {
          editAddress.setState(Integer.valueOf(profileBusinessDO.getState()));
        }
        if(StringUtils.isNotBlank(profileBusinessDO.getCity()))
        {
          editAddress.setCity(Integer.valueOf(profileBusinessDO.getCity()));	
        }
        editAddress.setStatelist(profileBusinessDO.getStatelist());
        editAddress.setCitylist(profileBusinessDO.getCitylist());
        editAddress.setPincode(profileBusinessDO.getPincode());
        return editAddress;
    }


    //Code needed in shipping details module 	
    public static ShippingDetails initializeShippingDetails(PersonalDetailsBusinessDO personalDetailsBusinessDO) {
        ShippingDetails shippingDetails = new ShippingDetails();

        UserContactDetail userContactDetail = personalDetailsBusinessDO.getUserContactDetail();
        if (userContactDetail != null) {
            shippingDetails.setTitle(userContactDetail.getTitle());
            shippingDetails.setFirstName(userContactDetail.getFirstName());
            shippingDetails.setAddress1(userContactDetail.getAddress1());
            shippingDetails.setArea(userContactDetail.getArea());
            shippingDetails.setLandmark(userContactDetail.getLandmark());
            shippingDetails.setStateId(userContactDetail.getStateId());
            shippingDetails.setCityId(userContactDetail.getCityId());	
            shippingDetails.setPostalCode(userContactDetail.getPostalCode());

            Map<String, Object> miscContactDetailsMap = new HashMap<String, Object>();
            miscContactDetailsMap.put("userId", userContactDetail.getUserId());
            miscContactDetailsMap.put("email", userContactDetail.getEmail());
            miscContactDetailsMap.put("serviceNumber", userContactDetail.getServiceNumber());
            miscContactDetailsMap.put("userProfileId", userContactDetail.getUserProfileId());
            miscContactDetailsMap.put("userRechargeContactId", userContactDetail.getUserRechargeContactId());
            miscContactDetailsMap.put("mobile", userContactDetail.getMobile());
            miscContactDetailsMap.put("txnFulfilmentId", userContactDetail.getTxnFulfilmentId());
            miscContactDetailsMap.put("isGuest", userContactDetail.getGuest());
            miscContactDetailsMap.put("txnHomePageId", userContactDetail.getTxnHomePageId());

            shippingDetails.setMiscCheckoutDetailsMap(miscContactDetailsMap);
        }
        return shippingDetails;
    }

    public static AddressBusinessDO initializeAddressBusinessDO(ShippingDetails shippingDetails) {
        AddressBusinessDO addressBusinessDO = new AddressBusinessDO();

        if (shippingDetails != null) {

            addressBusinessDO.setAddress(shippingDetails.getAddress1());
            addressBusinessDO.setArea(shippingDetails.getArea());
            addressBusinessDO.setCityId(shippingDetails.getCityId());
            addressBusinessDO.setCountryId(99);
            addressBusinessDO.setFirstName(shippingDetails.getFirstName());
            addressBusinessDO.setLandmark(shippingDetails.getLandmark());
            addressBusinessDO.setPostalCode(shippingDetails.getPostalCode());
            addressBusinessDO.setStateId(shippingDetails.getStateId());
            addressBusinessDO.setTitle(shippingDetails.getTitle());

            Map<String, Object> miscCheckoutDetailsMap = shippingDetails.getMiscCheckoutDetailsMap(); 
            if ( miscCheckoutDetailsMap != null) {
                addressBusinessDO.setTxnfulfilmentId(Integer.parseInt((String)miscCheckoutDetailsMap.get("txnFulfilmentId")));
                addressBusinessDO.setTxnHomePageId(Integer.parseInt((String) miscCheckoutDetailsMap.get("txnHomePageId")));
                addressBusinessDO.setUserId(Integer.parseInt((String) miscCheckoutDetailsMap.get("userId")));
                addressBusinessDO.setUserProfileId(Integer.parseInt((String) miscCheckoutDetailsMap.get("userProfileId")));
                addressBusinessDO.setUserRechargeContactId(Integer.parseInt((String) miscCheckoutDetailsMap.get("userRechargeContactId")));
                addressBusinessDO.setActionParam((String) miscCheckoutDetailsMap.get("actionParam"));
                addressBusinessDO.setLookupID((String) miscCheckoutDetailsMap.get("lookupId"));
                addressBusinessDO.setDefaultProfile((String) miscCheckoutDetailsMap.get("defaultProfile"));

                String isEdit = (String) miscCheckoutDetailsMap.get("isEdit");
                if(isEdit != null && !StringUtils.isEmpty(isEdit)){
                    addressBusinessDO.setIsEdit(isEdit);
                }else{
                    addressBusinessDO.setIsEdit("yes");
                }
            }
        }
        return addressBusinessDO;
    }

    public static void formatShippingDetails(ShippingDetails shippingDetails) {
        shippingDetails.setFirstName(shippingDetails.getFirstName().trim());
        shippingDetails.setAddress1(shippingDetails.getAddress1().trim());
        shippingDetails.setArea(shippingDetails.getArea().trim());
        shippingDetails.setPostalCode(shippingDetails.getPostalCode().trim());
        if (!StringUtils.isEmpty(shippingDetails.getLandmark()))
            shippingDetails.setLandmark(shippingDetails.getLandmark().trim());
    }

     /*
     * For getting client IP Address.
     * This might give you comma separated IP Addresses which include Proxy server address like '14.195.0.63, 70.39.186.60'
     */
    public static String getClientIpAddr(HttpServletRequest request) {
        String forwardedForIpHeader = Strings.nullToEmpty(request.getHeader("X-Forwarded-For"));
        if ("".equals(forwardedForIpHeader)) {
            logger.error("Empty X-Forwarded-For header");
        } else {
            logger.debug("Ip header is - " + forwardedForIpHeader);
        }

        for (String ipHeader : IP_HEADERS) {
            String ip = Strings.nullToEmpty(request.getHeader(ipHeader));

            if ("".equals(ip) || UNKNOWN_IP_IDENTIFIER.equals(ip)) {
                continue;
            }

            return ip;
        }

        return "";
    }

	/**
     * Ip from the client has command separated list in many cases where nginx rules are set, in those cases pick the last ip address.
     * @param request
     * @return
     */
    public static String getRealClientIpAddress(HttpServletRequest request) {
        String commaSeparatedIps = Strings.nullToEmpty(getClientIpAddr(request));
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedIps, ",");
        String ipAddress = null;
        if (tokenizer.hasMoreTokens()){
        	ipAddress = tokenizer.nextToken();
        }
        return ipAddress;
    }

    public static boolean isEmailId(String emailId) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-]+\\.)+[\\w]+[\\w]$";
        boolean valid = emailId.matches(EMAIL_REGEX);
        return valid;
    }

    public static boolean isValidMobileOperator(String operatorId, List<OperatorMaster> mobileOpeartorList) {
        for (OperatorMaster operator : mobileOpeartorList){
            if (operator != null && String.valueOf(operator.getOperatorMasterId()).equals(operatorId)){
                return true;
            }
        }
        return false;
    }
    
    public static boolean isValidMobileCircle(String circleId, List<CircleMaster> mobileCircleList) {
        for (CircleMaster circle : mobileCircleList) {
            if (circle != null &&  String.valueOf(circle.getCircleMasterId()).equals(circleId)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidDthOperator(String dthOperatorId, List<OperatorMaster> dthOpeartorList) {
        for (OperatorMaster operator : dthOpeartorList){
            if (operator != null && String.valueOf(operator.getOperatorMasterId()).equals(dthOperatorId)){
                return true;
            }
        }
        return false;
    } 

    public static boolean isValidDatacardOperator(String datacardOperatorId, 
            List<OperatorMaster> datacardOpeartorList) {
        for (OperatorMaster operator : datacardOpeartorList) {
            if (operator != null
                    && String.valueOf(operator.getOperatorMasterId()).equals(
                            datacardOperatorId)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidDatacardCircle(String circleId, List<CircleMaster> mobileCircleList) {
        for (CircleMaster circle : mobileCircleList) {
            if (circle != null
                    && String.valueOf(circle.getCircleMasterId()).equals(
                            circleId)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isMobileAppChannel(HttpServletRequest request)
    {
        int channel =  getMobileChannel(request);
        return isAppChannel(channel);
        
    }

    public static boolean isAppChannel(Integer channel){
    	boolean isAppChannel = false;
    	if (channel == null){
    		isAppChannel = false;
        } else if (channel == FCConstants.CHANNEL_ID_ANDROID_APP || channel == FCConstants.CHANNEL_ID_IOS_APP
                || channel == FCConstants.CHANNEL_ID_WINDOWS_APP || channel == FCConstants.CHANNEL_ID_MOOLAH_APP
                || channel == FCConstants.CHANNEL_ID_TIZEN_PHONE || channel == FCConstants.CHANNEL_ID_UWP_APP) {
    		isAppChannel =  true;
        }
    	return isAppChannel;
    }
    
    public static String getMobileAppRedirectUrl(Boolean isPaySuccess, String orderId, Boolean isAddCashPayment) {
        return String.format("/app/FcAppStatus/%s/%s/%s/result", isPaySuccess, orderId, isAddCashPayment);
    }
}
