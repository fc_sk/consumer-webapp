package com.freecharge.mobile.web.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.web.webdo.FeeCharge;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component
public class ConvenienceFeeUtil {

	@Autowired
	private ConvinenceFeeDetailsDao convenienceFeeDao;

	private final Logger logger = LoggingFactory.getLogger(getClass());

	public static final Integer CONVENIENCE_FEE_ID = 420;

	private boolean isConvenienceFeeItem(CartItemsBusinessDO cartItemDO) {
		return cartItemDO.getProductMasterId() == CONVENIENCE_FEE_ID;
	}

	private String getFeeIdIfConvenienceFeeApplicable(CartItemsBusinessDO cartItemDO) {
		String feeId = null;
		if (isConvenienceFeeItem(cartItemDO)) {
			feeId = cartItemDO.getEntityId();
			logger.info("Got the feeId: " + feeId + " for the cartItem: " + cartItemDO.toString());
		}

		return feeId;
	}

	public FeeCharge getFeeDetailsForCartItem(CartItemsBusinessDO cartItemDO) {
		FeeCharge feeCharge = null;
		String feeId = getFeeIdIfConvenienceFeeApplicable(cartItemDO);
		if (!StringUtils.isEmpty(feeId)) {
			feeCharge = convenienceFeeDao.getFeeDetails(feeId);
			if (feeCharge == null) {
				logger.warn("Got feeCharge as null for cartItem eligible for convenienceFee charges. CartItem: "
						+ cartItemDO.toString() + " and feeId: " + feeId);
			}
		}
		return feeCharge;
	}

	public FeeCharge getFeeDetailsForLookUpId(String lookupId) {
		FeeCharge feeCharge = convenienceFeeDao.getFeeDetails(lookupId);
		if (feeCharge == null) {
			logger.warn(
					"Got feeCharge as null for cartItem eligible for convenienceFee charges. lookupId: " + lookupId);
		}
		return feeCharge;
	}

	public void populateFeeChargeInPaymentBean(CartBusinessDo cartBusinessDO, PaymentAlertBean paymentAlertBean) {
		/*if (cartBusinessDO != null) {
			for (CartItemsBusinessDO cartItemDO : cartBusinessDO.getCartItems()) {
				FeeCharge feeCharge = getFeeDetailsForCartItem(cartItemDO);
				if (feeCharge != null) {
					paymentAlertBean.setFeeCharge(feeCharge);
					break;
				}
			}
		}*/
		populateFeeChargeInPaymentBean(cartBusinessDO.getLookupID() , paymentAlertBean);
	}

	public void populateFeeChargeInPaymentBean(String lookupId, PaymentAlertBean paymentAlertBean) {
		if (StringUtils.isEmpty(lookupId)) {
			return;
		}
		FeeCharge feeCharge = getFeeDetailsForLookUpId(lookupId);
		if (feeCharge != null) {
			paymentAlertBean.setFeeCharge(feeCharge);

		}

	}
}
