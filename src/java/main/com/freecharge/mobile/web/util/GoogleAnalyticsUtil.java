package com.freecharge.mobile.web.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.web.view.CheckoutData;

public class GoogleAnalyticsUtil {

	  private static final String GA_ACCOUNT = "MO-12939857-1";
	  private static final String GA_PIXEL = "/ga.jsp";
	  private static final String GOOGLE_ANALYTICS = "http://www.google-analytics.com";
	  private final static Logger logger = LoggingFactory.getLogger(GoogleAnalyticsUtil.class);
	  
	// 1x1 transparent GIF
	  private static final byte[] GIF_DATA = new byte[] {
	      (byte)0x47, (byte)0x49, (byte)0x46, (byte)0x38, (byte)0x39, (byte)0x61,
	      (byte)0x01, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x80, (byte)0xff,
	      (byte)0x00, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0x00, (byte)0x00,
	      (byte)0x00, (byte)0x2c, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00,
	      (byte)0x01, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x02,
	      (byte)0x02, (byte)0x44, (byte)0x01, (byte)0x00, (byte)0x3b
	  };
	  
	  public static String googleAnalyticsGetImageUrl(
	      HttpServletRequest request) throws UnsupportedEncodingException {
	    StringBuilder url = new StringBuilder();
	    url.append(GA_PIXEL + "?");
	    url.append("utmac=").append(GA_ACCOUNT);
	    url.append("&utmn=").append(Integer.toString((int) (Math.random() * 0x7fffffff)));

	    String referer = request.getHeader("referer");
	    String query = request.getQueryString();
	    String path = (String) request.getAttribute("requestUri");

	    if (referer == null || "".equals(referer)) {
	      referer = "-";
	    }
	    url.append("&utmr=").append(URLEncoder.encode(referer, "UTF-8"));

	    if (path != null) {
	      if (query != null) {
	        path += "?" + query;
	      }
	      url.append("&utmp=").append(URLEncoder.encode(path, "UTF-8"));
	    }

	    url.append("&guid=ON");

	    return url.toString().replace("&", "&amp;");
	  }
	  
	public static String googleAnalyticsGetImageUrlForTransaction(
			HttpServletRequest request) throws UnsupportedEncodingException {
		StringBuilder url = new StringBuilder();
		CheckoutData checkoutData = (CheckoutData) request
				.getAttribute("checkoutData");
		String urlParameters = "v=1&tid=" + GA_ACCOUNT + "&cid="
				+ Integer.toString((int) (Math.random() * 0x7fffffff))
				+ "&t=transaction";
		url.append("&ti=").append(checkoutData.getOrderId());
		url.append("&ta=").append("Freecharge");
		url.append("&tr=").append(checkoutData.getTotalAmount());
		url.append("&ts=").append(
				checkoutData.getOrderDetails().getShippingCharges());
		url.append("&tt=").append("0.00");
		urlParameters = urlParameters + url;
		return urlParameters;
	}

	public static void hitGAUsingImp(String urlParameters, HttpServletResponse response) {
		try {
			URL newurl = new URL(GOOGLE_ANALYTICS);
			HttpURLConnection connection = (HttpURLConnection) newurl
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(urlParameters.getBytes().length));
			connection.setUseCaches(false);
			connection.setReadTimeout(100);
			connection.setConnectTimeout(100);

			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			connection.disconnect();
		} catch (Exception e) {
			logger.error("Error occured in hitting Google Analytics: "
					+ e.getMessage());
		}
		try {
			writeGifData(response);
		} catch (IOException e) {
			logger.error("Error occured in writing image for GA img tag: "
					+ e.getMessage());
		}
	}
	
	// Writes the bytes of a 1x1 transparent gif into the response.
	  private static void writeGifData(HttpServletResponse response) throws IOException {
	    response.addHeader(
	        "Cache-Control",
	        "private, no-cache, no-cache=Set-Cookie, proxy-revalidate");
	    response.addHeader("Pragma", "no-cache");
	    response.addHeader("Expires", "Wed, 17 Sep 1975 21:32:10 GMT");
	    ServletOutputStream output = response.getOutputStream();
	    output.write(GIF_DATA);
	    output.flush();
	  }

}
