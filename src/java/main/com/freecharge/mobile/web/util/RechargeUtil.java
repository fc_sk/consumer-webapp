package com.freecharge.mobile.web.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.NameValuePair;

import com.freecharge.mobile.web.view.DTHRecharge;
import com.freecharge.mobile.web.view.DataCardRecharge;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.MobileRecharge;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.util.CookieUtil;
import com.freecharge.web.util.WebConstants;

public class RechargeUtil {

    private static final String RECHARGE_RESPONSE_SUCCESS_00   = "00";
    private static final String RECHARGE_RESPONSE_SUCCESS_0    = "0";
    private static final String RECHARGE_RESPONSE_UNDERPROCESS = "08";
    private static final String RECHARGE_RESPONSE_PENDING      = "-1";
    private static final String POSTPAID_RECHARGE_SUCCESS_00   = "00";
    private static final String POSTPAID_RECHARGE_FAILED       = "55";
    
    public static MobileRecharge getMobileRecharge(HttpServletRequest request, MasterData masterData) {
        MobileRecharge mobileRecharge = new MobileRecharge();
        autoFillMobileData(request, mobileRecharge, masterData);
        mobileRecharge.setMasterData(masterData);
        return mobileRecharge;
    }

    private static void autoFillMobileData(HttpServletRequest request,
            MobileRecharge mobileRecharge, MasterData masterData) {
        String operator = request.getParameter(WebConstants.OPERATOR);
        String operatorId = null;
        if (operator != null){
            operator = operator.replaceAll("_", " ");
            operatorId = masterData.getMobileOperatorId(operator);
        }
        if (operator != null && operatorId != null){
            mobileRecharge.setOperator(operatorId);
        }else{
            autoFillMobileFromCookies(request, mobileRecharge, masterData);
        }
    }

    private static void autoFillMobileFromCookies(HttpServletRequest request,
            MobileRecharge mobileRecharge, MasterData masterData) {
        Cookie mobileNumberCookie = CookieUtil.getCookie(request, "mobileNumberCookie");
        Cookie mobileAmountCookie = CookieUtil.getCookie(request, "mobileAmountCookie");
        Cookie mobileOperatorCookie = CookieUtil.getCookie(request, "mobileOperatorCookie");
        Cookie mobileCircleCookie = CookieUtil.getCookie(request, "mobileCircleCookie");
        
        if (mobileNumberCookie != null){
            mobileRecharge.setMobileNumber(mobileNumberCookie.getValue());  
        }
        
        if (mobileOperatorCookie != null && MobileUtil.isValidMobileOperator(mobileOperatorCookie.getValue(),
                masterData.getMobileOperators())){
            mobileRecharge.setOperator(mobileOperatorCookie.getValue());
        }
        
        if (mobileAmountCookie != null){
            mobileRecharge.setAmount(mobileAmountCookie.getValue());    
        }
        
        if (mobileCircleCookie != null && MobileUtil.isValidMobileCircle(mobileCircleCookie.getValue(), 
                masterData.getAllCircles())){
            mobileRecharge.setCircle(mobileCircleCookie.getValue());    
        }        
    }

    public static DTHRecharge getDthRecharge(HttpServletRequest request, MasterData masterData) {
        DTHRecharge dthRecharge = new DTHRecharge();
        autoFillDTHData(request, dthRecharge, masterData);
        dthRecharge.setMasterData(masterData);
        return dthRecharge;
    }

    private static void autoFillDTHData(HttpServletRequest request,
            DTHRecharge dthRecharge, MasterData masterData) {
        String operator = request.getParameter(WebConstants.OPERATOR);
        String operatorId = null;
        if (operator != null) {
            operator = operator.replaceAll("_", " ");
            operatorId = masterData.getDthOperatorId(operator);
        }   
        if (operator != null && operatorId != null) {
            dthRecharge.setDthOperator(operatorId);
        } else {
            autoFillDTHFromCookies(request, dthRecharge, masterData);
        }
    }
    
    private static void autoFillDTHFromCookies(HttpServletRequest request,
            DTHRecharge dthRecharge, MasterData masterData) {
        Cookie dthNumberCookie = CookieUtil.getCookie(request, "dthNumberCookie");
        Cookie dthAmountCookie = CookieUtil.getCookie(request, "dthAmountCookie");
        Cookie dthOperatorCookie = CookieUtil.getCookie(request, "dthOperatorCookie");
        
        if (dthNumberCookie != null){
            dthRecharge.setDthNumber(dthNumberCookie.getValue());
        }
        
        if (dthOperatorCookie != null && MobileUtil.isValidDthOperator(dthOperatorCookie.getValue(),
                masterData.getDthOperators())){
            dthRecharge.setDthOperator(dthOperatorCookie.getValue());   
        }
        
        if (dthAmountCookie != null){
            dthRecharge.setDthAmount(dthAmountCookie.getValue());   
        }
    }
    
    public static DataCardRecharge getDatacardRecharge(HttpServletRequest request, MasterData masterData) {
        DataCardRecharge dataRecharge = new DataCardRecharge();
        autoFillDataCardData(request, dataRecharge, masterData);
        dataRecharge.setMasterData(masterData);
        return dataRecharge;
    }

    private static void autoFillDataCardData(HttpServletRequest request,
            DataCardRecharge dataRecharge, MasterData masterData) {
        String operator = request.getParameter(WebConstants.OPERATOR);
        String operatorId = null;
        if (operator != null) {
            operator = operator.replaceAll("_", " ");
            operatorId = masterData.getDataCardOperatorId(operator);
        }
        if (operator != null && operatorId != null) {
            dataRecharge.setDatacardOperator(operatorId);
        } else {
            autoFillDataCardFromCookies(request, dataRecharge, masterData);
        }
    }
    
    private static void autoFillDataCardFromCookies(HttpServletRequest request,
            DataCardRecharge dataRecharge, MasterData masterData) {
        Cookie datacardNumberCookie = CookieUtil.getCookie(request,
                "datacardNumberCookie");
        Cookie datacardAmountCookie = CookieUtil.getCookie(request,
                "datacardAmountCookie");
        Cookie datacardOperatorCookie = CookieUtil.getCookie(request,
                "datacardOperatorCookie");
        Cookie datacardCircleCookie = CookieUtil.getCookie(request,
                "datacardCircleCookie");
        if (datacardNumberCookie != null) {
            dataRecharge.setDatacardNumber(datacardNumberCookie.getValue());
        }
        if (datacardOperatorCookie != null
                && MobileUtil.isValidDatacardOperator(datacardOperatorCookie.getValue(), 
                        masterData.getDataCardOperators())) {
            dataRecharge.setDatacardOperator(datacardOperatorCookie.getValue());
        }
        if (datacardAmountCookie != null) {
            dataRecharge.setDatacardAmount(datacardAmountCookie.getValue());
        }
        if (datacardCircleCookie != null
                && MobileUtil.isValidDatacardCircle(datacardCircleCookie.getValue(), 
                        masterData.getAllCircles())) {
            dataRecharge.setDatacardCircle(datacardCircleCookie.getValue());
        }
    }
 
    public static String getRechargeStatusMessage(final String transactionStatusCode) {
        if (RECHARGE_RESPONSE_SUCCESS_00.equals(transactionStatusCode)
                || RECHARGE_RESPONSE_SUCCESS_0.equals(transactionStatusCode)) {
            return "Success";
        }
        if (RECHARGE_RESPONSE_UNDERPROCESS.equals(transactionStatusCode)) {
            return "Underprocess";
        }
        if (RECHARGE_RESPONSE_PENDING.equals(transactionStatusCode)) {
            return "Pending";
        }
        return "Failed";
    }

    public static String getPostpaidRechargeStatusMessage(final String transactionStatusCode) {
        if (POSTPAID_RECHARGE_SUCCESS_00.equals(transactionStatusCode)) {
            return "Success";
        } else if (POSTPAID_RECHARGE_FAILED.equals(transactionStatusCode)) {
            return "Failed";
        }
        return "Pending";
    }
    
    public static boolean isFailedAggrResponseCode(String responseCode) {
        return responseCode != null && !AggregatorInterface.SUCCESS_OR_PENDING_AGGREGATOR_RESPONSES.contains(responseCode);
    }
    
    public static boolean isSuccessAggrResponseCode(String responseCode) {
        return responseCode != null && AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(responseCode);
    }
    
    public static boolean isUnderProcessAggrResponseCode(String responseCode) {
        return AggregatorInterface.RECHARGE_PENDING_AGGREGATOR_RESPONSE.contains(responseCode);
    }
    
    public static boolean isFailedInResponseCode(String responseCode) {
        return responseCode != null && !AggregatorInterface.SUCCESS_OR_PENDING_RESPONSES.contains(responseCode);
    }
    
    public static boolean isSuccessInResponseCode(String responseCode) {
        return responseCode != null && AggregatorInterface.SUCCESS_IN_RESPONSE_CODE.equals(responseCode);
    }
    
    public static boolean isUnderProcessInResponseCode(String responseCode) {
        return RechargeConstants.IN_UNDER_PROCESS_CODE.equals(responseCode);
    }
    
    public static String getRechargeStatusFromAggCode(String responseCode) {
        String rechargeStatus = null;
        if (isSuccessAggrResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_SUCCESS;
        } else if (isFailedAggrResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_FAILED;
        } else if (isUnderProcessAggrResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
        }
        return rechargeStatus;
    }
    
    public static String getRechargeStatusFromInResponseCode(String responseCode) {
        String rechargeStatus = null;
        if (isSuccessInResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_SUCCESS;
        } else if (isFailedInResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_FAILED;
        } else if (isUnderProcessInResponseCode(responseCode)) {
            rechargeStatus = RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
        }
        return rechargeStatus;
    }
    
    public static String createSubmitData(NameValuePair[] nameValuePair, String encoding) {
        StringBuilder stringBuilder = new StringBuilder();

        String ampersand = "";

        if (nameValuePair == null) {
            return null;
        }

        for (int i = 0; i < nameValuePair.length; i++) {

            String name = nameValuePair[i].getName();
            String value = nameValuePair[i].getValue();
            try {
                stringBuilder.append(ampersand);
                stringBuilder.append(URLEncoder.encode(name, encoding));
                stringBuilder.append('=');
                stringBuilder.append(URLEncoder.encode(value, encoding));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            ampersand = "&";
        }

        return stringBuilder.toString();
    }
    
    public static String getAggregatorName(String gatewayName) {
        if (gatewayName == null) {
            return null;
        }
        gatewayName = gatewayName.toLowerCase();
        switch (gatewayName) {
            case RechargeConstants.AGGR_NAME_OXIGEN:
            case PaymentConstants.BILL_PAYMENT_OXIGEN_GATEWAY_LOWER:
                return RechargeConstants.AGGR_NAME_OXIGEN;
            case RechargeConstants.AGGR_NAME_EURONET:
            case PaymentConstants.BILL_PAYMENT_EURONET_GATEWAY_LOWER:
                return RechargeConstants.AGGR_NAME_EURONET;
            default:
                return gatewayName;
        }
    }
}
