package com.freecharge.mobile.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

public class MapperUtil {
	private static Mapper mapper = new DozerBeanMapper();
	
	public static <T> T map(Object o, Class<T> c){
		return mapper.map(o, c);
	}
	
	public static <T, G> List<T> map(List<G> oList, Class<T> c){
		if (oList == null){
			return null;
		}
		List<T> newList = new ArrayList<>();
		for (G g : oList){
			newList.add(mapper.map(g, c));
		}
		return newList;
	}
}
