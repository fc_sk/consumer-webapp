package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.DatacardNumber;

public class DatacardNumberValidator implements ConstraintValidator<DatacardNumber, String>{
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(DatacardNumber datacardNumber) {
		// nothing to do here.
	}
	@Override
	public boolean isValid(String datacardNumber, ConstraintValidatorContext context) {	
	
		String numberRequired = languageUtility.getLocaleMessage("m.datacard.number.required",null);
		String fieldOnlyNumbers = languageUtility.getLocaleMessage("m.datacard.numfield.only.numbers",null);
		String numberRange = languageUtility.getLocaleMessage("m.datacard.number.range",null);
		
		if (datacardNumber == null || datacardNumber.isEmpty()){
			ValidationUtil.setErrorMessage(numberRequired, context);
			return false;			
		} 
		Pattern p = Pattern.compile("^\\d+$");
        Matcher m = p.matcher(datacardNumber);
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(fieldOnlyNumbers, context);
			return false;
		} else if (datacardNumber.length()!=10) {
			ValidationUtil.setErrorMessage(numberRange, context);
			return false;
		}
		return true;
	}
}
