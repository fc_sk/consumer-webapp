package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.ValidConfirmPassword;
import com.freecharge.mobile.web.view.User;

public class ValidConfirmPasswordValidator implements ConstraintValidator<ValidConfirmPassword, Object>{
	
	@Autowired
	public LanguageUtility languageUtility;
	
	
	@Override
	public void initialize(final ValidConfirmPassword validConfirmPassword) {
		// Nothing to do here.
	} 
	@Override
	public boolean isValid(Object object , ConstraintValidatorContext context) {
		String passwordfieldmatch = languageUtility.getLocaleMessage("m.password.field.match",null);
		
		User user = (User)object;
		String password = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		if (!(password.equals(confirmPassword))) {
			ValidationUtil.setErrorMessage(passwordfieldmatch, "confirmPassword", context);
			return false;
		}
		return true;
	}
}
