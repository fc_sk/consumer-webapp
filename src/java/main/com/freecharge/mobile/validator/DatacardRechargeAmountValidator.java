package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.DatacardRechargeAmount;

public class DatacardRechargeAmountValidator implements ConstraintValidator<DatacardRechargeAmount, String> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(DatacardRechargeAmount amount) {
		// nothing to do here.
	}
	
	@Override
	public boolean isValid(String amount, ConstraintValidatorContext context) {
		String amountRequired = languageUtility.getLocaleMessage("m.amount.required", null);
		String amountOnlyNumbers = languageUtility.getLocaleMessage("m.amount.only.numbers", null);
		
		String amountRangeFirstMsg = languageUtility.getLocaleMessage("m.datacard.amount.range.firstmsg", null);
		String amountRangeSecondMsg = languageUtility.getLocaleMessage("m.datacard.amount.range.secondmsg", null);
		
		if (amount == null || (amount.toString()).isEmpty()) {
			ValidationUtil.setErrorMessage(amountRequired, context);
			return false;
		}
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(amount);
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(amountOnlyNumbers,context);
			return false;
		} else if (Integer.parseInt(amount) < FCConstants.MIN_RECHARGE_AMOUNT || Integer.parseInt(amount) > FCConstants.MAX_RECHARGE_AMOUNT_DATACARD) {
			ValidationUtil.setErrorMessage(amountRangeFirstMsg + " " + FCConstants.MIN_RECHARGE_AMOUNT + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DATACARD,context);
			return false;
		}
		return true;
	}
}
