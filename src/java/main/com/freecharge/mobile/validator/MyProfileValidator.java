package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.web.view.EditProfile;
import com.freecharge.web.webdo.ProfileWebDO;

public class MyProfileValidator implements Validator {
	private Logger logger = LoggingFactory.getLogger(getClass());

	@Override
	public boolean supports(Class<?> aClass) {
		return ProfileWebDO.class.equals(aClass);
	}
		
	@Override
	public void validate(Object o, Errors errors) {		
		EditProfile editProfile = (EditProfile)o;
		String name = editProfile.getName();
		String mobileNo = editProfile.getMobileNo();
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "m.name.required","#m.name.required#");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobileNo", "m.number.required","#m.number.required#");
		
        if (name != null && !(name.isEmpty())) {
        	Pattern namePattern = Pattern.compile("[A-Za-z ]*");
            Matcher nameMatcher = namePattern.matcher(name.trim());
        	if (!nameMatcher.matches()) {
        		errors.rejectValue("name", "m.name.only.alphabets");
        	}
        }
        
       if (mobileNo != null && !(mobileNo.isEmpty())) {
    	   Pattern p = Pattern.compile("^\\d+$");
   		   Matcher m = p.matcher(mobileNo.trim());
    	   if (!(m.matches())) {
    		   errors.rejectValue("mobileNo", "m.mob.numbfield.only.numbers");  
    	   } else {
    		   if (mobileNo.trim().length() < 10 || mobileNo.trim().length() > 10) {
    			   errors.rejectValue("mobileNo", "m.mob.numb.range");  
    		   }
    	   }
       }             
	}	
}
