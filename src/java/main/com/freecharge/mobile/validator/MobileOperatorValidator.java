package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.MobileOperator;

public class MobileOperatorValidator implements ConstraintValidator<MobileOperator, String>{
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(MobileOperator operator) {
		// nothing to do here.
	}
	@Override
	public boolean isValid(String operator, ConstraintValidatorContext context) {
	
		String operatorRequired = languageUtility.getLocaleMessage("m.mobile.operator.required",null);
		
		if (operator == null || operator.isEmpty()){
			ValidationUtil.setErrorMessage(operatorRequired, context);
			return false;		
		}
		return true;
	}
}
