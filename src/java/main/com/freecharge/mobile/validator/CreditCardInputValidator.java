package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.CreditCardInput;
import com.freecharge.mobile.web.view.payment.CreditCard;

public class CreditCardInputValidator implements ConstraintValidator<CreditCardInput, Object> {
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(final CreditCardInput debitCardInput) {
		// Nothing to do here.
	} 
	@Override
	public boolean isValid(Object object , ConstraintValidatorContext context) {
		
		//For Internationalization
		String creditOnlyNumbers = languageUtility.getLocaleMessage("m.creditcardnum.field.only.numbers", null);
		String cvvOnlyNumbers = languageUtility.getLocaleMessage("m.cvv.only.numb", null);
		
		CreditCard creditCard = (CreditCard)object;
		String creditCardNumber = (creditCard.getCardNumber()).trim();
		String cvvNumber = (creditCard.getCvvNumber()).trim();
		boolean isValid = true;

		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(creditCardNumber);
		
		Pattern pcvv = Pattern.compile("^\\d+$");
		Matcher mcvv = pcvv.matcher(cvvNumber);
			
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(creditOnlyNumbers, "cardNumber", context);
			isValid = false;
		}
		
		if (!(mcvv.matches())) {
			ValidationUtil.setErrorMessage(cvvOnlyNumbers, "cvvNumber", context);
			isValid = false;
		}
		
		if(!isValid) {
			return false;
		}
		return true;
	}
}
