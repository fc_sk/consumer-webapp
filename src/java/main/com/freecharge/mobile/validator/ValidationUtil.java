package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidatorContext;

public class ValidationUtil {
	public static void setErrorMessage(String message, ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
	}
	
	public static void setErrorMessage(String message, String fieldName, ConstraintValidatorContext context) {
		context.disableDefaultConstraintViolation();
		context.buildConstraintViolationWithTemplate(message).addNode(fieldName).addConstraintViolation();
	}
}
