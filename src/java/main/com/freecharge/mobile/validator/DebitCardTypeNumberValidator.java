package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.DebitCardTypeNumber;
import com.freecharge.mobile.web.view.payment.DebitCard;
import com.freecharge.payment.util.PaymentConstants;

public class DebitCardTypeNumberValidator implements ConstraintValidator<DebitCardTypeNumber, Object> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(final DebitCardTypeNumber debitCardTypeNumber) {
		// Nothing to do here.
	} 
	@Override
	public boolean isValid(Object object , ConstraintValidatorContext context) {
		//For Internationalization 
		String expMonthRequired = languageUtility.getLocaleMessage("m.exp.month.required",null);
		String expYearRequired = languageUtility.getLocaleMessage("m.exp.year.required", null);
		String cvvRequired = languageUtility.getLocaleMessage("m.cvv.required", null);
		String cvvOnlyNumbers = languageUtility.getLocaleMessage("m.cvv.only.numb", null);
		String cvvRange = languageUtility.getLocaleMessage("m.cvv.range", null);
		String debitOnlyNumbers = languageUtility.getLocaleMessage("m.debitcardnum.field.only.numbers", null);
		String visaNumbStarts = languageUtility.getLocaleMessage("m.visacard.num.starts", null);
		String masterNumbStarts = languageUtility.getLocaleMessage("m.mastercard.card.num", null);
		
		DebitCard debitCard = (DebitCard) object;
		String debitCardNumber = (debitCard.getCardNumber()).trim();
		String debitCardType = debitCard.getCardType();
		String cvvNumber = (debitCard.getCvvNumber()).trim();
		String expMonth = debitCard.getExpiryMonth();
		String expYear = debitCard.getExpiryYear();

		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(debitCardNumber);
		
		Pattern pcvv = Pattern.compile("^\\d+$");
		Matcher mcvv = pcvv.matcher(cvvNumber);
			
		boolean isValid = true;
						
		if (!(PaymentConstants.MAESTRO_SBI.equals(debitCardType)) && !(PaymentConstants.MAESTRO_CITI.equals(debitCardType))) {
			if (expMonth == null || expMonth.isEmpty()) {
				ValidationUtil.setErrorMessage(expMonthRequired,"expiryMonth", context);
				isValid = false;		
			}
			if (expYear == null || expYear.isEmpty()) {
				ValidationUtil.setErrorMessage(expYearRequired,"expiryYear", context);
				isValid = false;
			}
			
			if (cvvNumber == null || cvvNumber.isEmpty()) {
				ValidationUtil.setErrorMessage(cvvRequired,"cvvNumber", context);
				isValid = false;
			} else if (!(mcvv.matches())) {
				ValidationUtil.setErrorMessage(cvvOnlyNumbers, "cvvNumber", context);
				isValid = false;
			} else if (cvvNumber.length() != 3) {
				ValidationUtil.setErrorMessage(cvvRange, "cvvNumber", context);
				isValid = false;
			}
		}
			
	
		if (debitCardNumber == null || (debitCardNumber.isEmpty())) 
		{
			isValid = false;
		} else if (!(m.matches())) {
			ValidationUtil.setErrorMessage(debitOnlyNumbers, "cardNumber", context);
			isValid = false;
		} else {
			if (PaymentConstants.VISA_DC.equals(debitCardType)) {		
				if (Integer.parseInt(debitCardNumber.substring(0,1))!=4) {
					ValidationUtil.setErrorMessage(visaNumbStarts,"cardNumber",context);
					isValid = false;
				} 
			} else if (PaymentConstants.MASTER_DC.equals(debitCardType)) {
				if (Integer.parseInt(debitCardNumber.substring(0,1))!=5) {
					ValidationUtil.setErrorMessage(masterNumbStarts, "cardNumber",context);
					isValid = false;
				} 
			}
		}
		
		if(!isValid) {
			return false;
		}
		return true;
	}
}
