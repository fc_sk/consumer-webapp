package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.DatacardOperator;

public class DatacardOperatorValidator implements ConstraintValidator<DatacardOperator, String> {
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(DatacardOperator datacardOperator) {
		// nothing to do here.
	}
	
	@Override
	public boolean isValid(String datacardOperator, ConstraintValidatorContext context) {
		
		String operatorRequired = languageUtility.getLocaleMessage("m.datacard.operator.required",null);
		
		if (datacardOperator == null || datacardOperator.isEmpty()){
			ValidationUtil.setErrorMessage(operatorRequired, context);
			return false;			
		}
		return true;
	}

}
