package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.MobileRechargeAmount;

public class MobileRechargeAmountValidator implements ConstraintValidator<MobileRechargeAmount, String>{
	
	@Autowired
	public LanguageUtility languageUtility;
	
	
	@Override
	public void initialize(MobileRechargeAmount amount) {
		// nothing to do here.
	}
	
	@Override
	public boolean isValid(String amount, ConstraintValidatorContext context) {
	
		String amountRequired = languageUtility.getLocaleMessage("m.amount.required",null);
		String amountOnlyNumbers = languageUtility.getLocaleMessage("m.amount.only.numbers",null);
		
		String amountRangeFirst = languageUtility.getLocaleMessage("m.mobile.amount.range.firstmsg",null);
		String amountRangeSecond = languageUtility.getLocaleMessage("m.mobile.amount.range.secondmsg",null);
				
		if (amount == null || (amount.toString()).isEmpty()) {
			ValidationUtil.setErrorMessage(amountRequired, context);
			return false;
		}
		
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(amount);
		
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(amountOnlyNumbers ,context);			
			return false;
		} else if (Integer.parseInt(amount) < FCConstants.MIN_RECHARGE_AMOUNT ||Integer.parseInt(amount) > FCConstants.MAX_RECHARGE_AMOUNT_MOBILE ) {
			ValidationUtil.setErrorMessage(amountRangeFirst + " "+ FCConstants.MIN_RECHARGE_AMOUNT + " " + amountRangeSecond + " " + FCConstants.MAX_RECHARGE_AMOUNT_MOBILE, context);
			return false;
		}
		return true;	
	}
}
