package com.freecharge.mobile.validator;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.web.view.EditPassword;
import com.freecharge.web.webdo.ProfileWebDO;

public class ChangePasswordValidator implements Validator {
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Override
	public boolean supports(Class<?> aClass) {
		return ProfileWebDO.class.equals(aClass);
	}
		
	@Override
	public void validate(Object o, Errors errors) {
		EditPassword editPassword = (EditPassword)o;
		String newPassword = editPassword.getNewpassword();
		String confirmPassword = editPassword.getConfirmpassword();
	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"oldpassword", "m.oldpassword.requrired", "#m.oldpassword.required#");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newpassword", "m.newpassword.required", "#m.newpassword.required#");	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmpassword", "m.confirmpassword.required", "#m.confirmpassword.required#");
		
		if (newPassword != null && !(newPassword.isEmpty())) {
			if (newPassword.length() < 6) {
				errors.rejectValue("newpassword", "m.newpassword.min");
			}
		}
       
       if ((newPassword != null && !(newPassword.isEmpty()))&&(confirmPassword != null && !(confirmPassword.isEmpty()))) {
    	   if (!(newPassword.equals(confirmPassword))) {
    		   errors.rejectValue("confirmpassword", "m.passwords.match");  
    	   }	   
       }
	}	
}
