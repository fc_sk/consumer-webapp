package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.CheckUserBusinessDO;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.EmailIdExist;

public class EmailIdExistValidator implements ConstraintValidator<EmailIdExist, String> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Autowired
	private UserAuthService userAuthService;
	
	@Override
	public void initialize(final EmailIdExist emailIdExist) {
		// Nothing to do here.
	} 
	
	@Override
	public boolean isValid(String email , ConstraintValidatorContext context) {
		String haveNotRegistered = languageUtility.getLocaleMessage("m.have.not.been.registered", null);
		boolean isVaild = true;
		String statusMsg = checkEmailExist(email);
		if (email == null || email.isEmpty()) {
			isVaild = false;
			ValidationUtil.setErrorMessage("Invalid email id.",context);
		} else if (!statusMsg.equals("0")) {
			ValidationUtil.setErrorMessage(haveNotRegistered,context);
			isVaild = false;
		}
		if (!isVaild) {
			return false;
		}
		return true;
	}
	
	private String checkEmailExist(String email) {
		CheckUserBusinessDO checkUserBusinessDO = new CheckUserBusinessDO();
		checkUserBusinessDO.setEmail(email);
		userAuthService.validateUser(checkUserBusinessDO);
		return checkUserBusinessDO.getStatusMsg();		
	}	
}
