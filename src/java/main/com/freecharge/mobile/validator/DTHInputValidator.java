package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.CommonService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.DTHInput;
import com.freecharge.mobile.web.view.DTHRecharge;

public class DTHInputValidator implements ConstraintValidator<DTHInput, Object> {
	@Autowired
	private CommonService commonService;
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(final DTHInput validDTHRecharge) {
		// Nothing to do here.
	} 
	
	@Override
	public boolean isValid(Object object , ConstraintValidatorContext context) {
		boolean isValidNumber = true;
		boolean isValidAmount = true;
		boolean isValid = true;
		
		String numberRequired = languageUtility.getLocaleMessage("m.dth.number.required",null);
		String operatorRequired = languageUtility.getLocaleMessage("m.dth.operator.required",null);
		String fieldOnlyNumbers = languageUtility.getLocaleMessage("m.dth.numbfield.only.numbers",null);
		String amountRequired = languageUtility.getLocaleMessage("m.amount.required", null);
		String amountOnlyNumbers = languageUtility.getLocaleMessage("m.amount.only.numbers", null);
		
		String numberRangeBigtv = languageUtility.getLocaleMessage("m.dth.number.range",new Object[]{"2","12"});
		String numberRangeDishtv = languageUtility.getLocaleMessage("m.dth.number.range",new Object[]{"0","11"});
		String numberRangeSuntv = languageUtility.getLocaleMessage("m.dth.number.range",new Object[]{"1 or 4,","11"});
		String numberRangeAirteltv = languageUtility.getLocaleMessage("m.dth.number.range",new Object[]{"3","10"});
		String numberRangeTatasky = languageUtility.getLocaleMessage("m.dth.number.range",new Object[]{"1","10"});
		String numberRangeVideocon = languageUtility.getLocaleMessage("m.dth.number.range.videocon",null);
		String amountRangeFirstMsg = languageUtility.getLocaleMessage("m.dth.amount.range.firstmsg",null);
		String amountRangeSecondMsg = languageUtility.getLocaleMessage("m.dth.amount.range.secondmsg",null);
		String forLabel = languageUtility.getLocaleMessage("m.for.label", null);
		
		DTHRecharge dthRecharge = (DTHRecharge) object;
		String dthOperator = dthRecharge.getDthOperator();
		String serviceNo = dthRecharge.getDthNumber();
		String dthAmount = dthRecharge.getDthAmount();
				
		if (dthOperator == null || dthOperator.isEmpty()){
			ValidationUtil.setErrorMessage(operatorRequired, "dthOperator", context);
			isValid = false;		
		}
		
		if (serviceNo == null || serviceNo.isEmpty() ) {			
			ValidationUtil.setErrorMessage(numberRequired, "dthNumber" ,context);
			isValid = false;
		} 
		
		if (dthAmount == null || dthAmount.isEmpty() ) {			
			ValidationUtil.setErrorMessage(amountRequired, "dthAmount" ,context);
			isValid = false;
		} 
		
		if (!isValid){
			return false;
		}
		
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(dthAmount);	
		
		Pattern pNumber = Pattern.compile("^\\d+$");
        Matcher mNumber = pNumber.matcher(serviceNo);
        
		if (!(mNumber.matches())) {
			ValidationUtil.setErrorMessage(fieldOnlyNumbers +" ", "dthNumber" ,context);
			isValid = false;
		} 
			
		int amount = -1;
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(amountOnlyNumbers +" ", "dthAmount",context);
			isValidAmount = false;
		} else{
			amount = Integer.parseInt(dthAmount);
		}
		if (!isValid){
			return isValid;
		}
		
		OperatorMaster operator = commonService.getDthOperator(dthOperator);
		String operatorName	= operator.getName();
		String operatorCode = operator.getOperatorCode();
		
		if (operatorCode.equalsIgnoreCase("Big Tv")) {
			if (serviceNo.length()!=12 || Integer.parseInt(serviceNo.substring(0,1))!=2) {
				ValidationUtil.setErrorMessage(numberRangeBigtv+" "+ operatorName, "dthNumber", context);
				isValidNumber=false;
			}
			if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_BIG_TV || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_BIG_TV ) {
				ValidationUtil.setErrorMessage(amountRangeFirstMsg +" "+ FCConstants.MIN_RECHARGE_AMOUNT_DTH_BIG_TV +" "+ amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_BIG_TV +" "+ forLabel+" " + operatorName, "dthAmount", context);
				isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true;
	    } else if (operatorCode.equalsIgnoreCase("Dish Tv"))  {
	    	if (serviceNo.length()!=11 || Integer.parseInt(serviceNo.substring(0,1))!=0) {
	    		ValidationUtil.setErrorMessage(numberRangeDishtv + " " + operatorName, "dthNumber", context);
	    		isValidNumber=false;
	    	}
	    	if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_DISH_TV || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_DISH_TV ) {
	    		ValidationUtil.setErrorMessage(amountRangeFirstMsg + " " + FCConstants.MIN_RECHARGE_AMOUNT_DTH_DISH_TV + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_DISH_TV + " " + forLabel + " " + operatorName, "dthAmount", context);
	    		isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true;
	    	
	    } else if (operatorCode.equalsIgnoreCase("Sun Tv")) {
	    	if (serviceNo.length()!=11 || (Integer.parseInt(serviceNo.substring(0,1))!=1 && Integer.parseInt(serviceNo.substring(0,1))!=4)) {
	    		ValidationUtil.setErrorMessage(numberRangeSuntv +" " + operatorName, "dthNumber",context);
	    		isValidNumber=false;
	    	}
	    	if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_SUN_TV || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_SUN_TV ) {
	    		ValidationUtil.setErrorMessage(amountRangeFirstMsg + " " + FCConstants.MIN_RECHARGE_AMOUNT_DTH_SUN_TV + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_SUN_TV + " " + forLabel + " " + operatorName, "dthAmount", context);	
	    		isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true;
			
	    } else if (operatorCode.equalsIgnoreCase("Airtel Tv")) {
   		 	if (serviceNo.length()!=10 || Integer.parseInt(serviceNo.substring(0,1))!=3) {
   		 		ValidationUtil.setErrorMessage(numberRangeAirteltv + " " + operatorName, "dthNumber",context);
   		 		isValidNumber=false;
   		 	}	
   		 	
   		 	if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_AIRTEL_TV || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_AIRTEL_TV ) {
   		 		ValidationUtil.setErrorMessage(amountRangeFirstMsg  + " " + FCConstants.MIN_RECHARGE_AMOUNT_DTH_AIRTEL_TV + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_AIRTEL_TV + " " + forLabel + " " + operatorName, "dthAmount", context);
   		 		isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true;
   		 	
	    } else if (operatorCode.equalsIgnoreCase("Tata Sky")) {
   			if (serviceNo.length()!=10 || Integer.parseInt(serviceNo.substring(0,1))!=1) {
   				ValidationUtil.setErrorMessage(numberRangeTatasky + " " + operatorName, "dthNumber",context);
   				isValidNumber=false;
   			}
   			if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_TATA_SKY || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_TATA_SKY ) {
   				ValidationUtil.setErrorMessage(amountRangeFirstMsg + " " + FCConstants.MIN_RECHARGE_AMOUNT_DTH_TATA_SKY + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_TATA_SKY + " "+ forLabel + " " + operatorName, "dthAmount", context);
   				isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true; 			
	    } else if (operatorCode.equalsIgnoreCase("Videocon Tv")) {
   			if (serviceNo.length() < 2 || serviceNo.length() > 14) {
   				ValidationUtil.setErrorMessage(numberRangeVideocon + " " + operatorName, "dthNumber",context);
   				isValidNumber=false;
   			}
   			if (amount < FCConstants.MIN_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H || amount > FCConstants.MAX_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H ) {			
   				ValidationUtil.setErrorMessage(amountRangeFirstMsg + " " + FCConstants.MIN_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H + " " + amountRangeSecondMsg + " " + FCConstants.MAX_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H +" "+ forLabel +" " + operatorName, "dthAmount", context);
   				isValidAmount = false;
			}
			if (!isValidNumber || !isValidAmount) {
				return false;
			}
			return true; 			
	    }			
		return true;
	}	
}
