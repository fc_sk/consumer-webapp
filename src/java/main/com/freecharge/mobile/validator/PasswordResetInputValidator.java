package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.PasswordResetInput;
import com.freecharge.web.webdo.PasswordRecoveryWebDO;

public class PasswordResetInputValidator implements ConstraintValidator<PasswordResetInput, Object> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(final PasswordResetInput passwordResetInput) {
		// Nothing to do here.
	} 
	
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		PasswordRecoveryWebDO passwordRecoveryWebDO = (PasswordRecoveryWebDO)object;
		String newPassword = passwordRecoveryWebDO.getNewPassword();
		String confirmPassword = passwordRecoveryWebDO.getConfirmPassword();
		boolean isValid = true;
		
		String resetPswCannotBlank = languageUtility.getLocaleMessage("m.reset.password.cannot.blank",null);
		String resetPassMin = languageUtility.getLocaleMessage("m.password.contains.min",null);
		String resetPassMax = languageUtility.getLocaleMessage("m.password.contains.max",null);
		String confirmPswCannotBlank = languageUtility.getLocaleMessage("m.confirm.password.cannot.blank",null);
		String passFieldMustmatch = languageUtility.getLocaleMessage("m.password.field.must.match",null);
		
		
		if (newPassword == null || newPassword.isEmpty()) {
			ValidationUtil.setErrorMessage(resetPswCannotBlank, "newPassword", context);
			isValid = false;
		} else if (newPassword.length()<6) {
			ValidationUtil.setErrorMessage(resetPassMin, "newPassword", context);
			isValid = false;
		} else if (newPassword.length()>30) {
			ValidationUtil.setErrorMessage(resetPassMax, "newPassword", context);
			isValid = false;
		}
		else if (confirmPassword == null || confirmPassword.isEmpty()) {
			ValidationUtil.setErrorMessage(confirmPswCannotBlank, "confirmPassword", context);
			isValid = false;
		} else if (!newPassword.equals(confirmPassword)) {
			ValidationUtil.setErrorMessage(passFieldMustmatch, "confirmPassword", context);
			isValid = false;
		}
		if (!isValid)
		{
			return false;
		}
		return true;
	}
}
