package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.MobileCircle;

public class MobileCircleValidator implements ConstraintValidator<MobileCircle, String> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Override
	public void initialize(MobileCircle circle) {
		// nothing to do here.
	}
	
	@Override
	public boolean isValid(String circle, ConstraintValidatorContext context) {
		String circleRequired = languageUtility.getLocaleMessage("m.mobile.circle.required",null);
		
		if (circle == null || circle.isEmpty()){
			ValidationUtil.setErrorMessage(circleRequired, context);
			return false;
		}
		return true;
	}
}
