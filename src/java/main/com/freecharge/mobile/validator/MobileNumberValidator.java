package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.MobileNumber;

public class MobileNumberValidator implements ConstraintValidator<MobileNumber, String> {

	@Autowired
	public LanguageUtility languageUtility;

	@Override
	public void initialize(MobileNumber mobileNumber) {
		// nothing to do here.
	}

	@Override
	public boolean isValid(String mobileNumber, ConstraintValidatorContext context) {
		String mobileRequired = languageUtility.getLocaleMessage("m.mobile.number.required",null);
		String mobileOnlyNumbers = languageUtility.getLocaleMessage("m.mob.numbfield.only.numbers", null);
		String mobileNumbRange = languageUtility.getLocaleMessage("m.mob.numb.range", null);
		
		if (mobileNumber == null || mobileNumber.isEmpty()){
			ValidationUtil.setErrorMessage(mobileRequired, context);
            return false;			
		} 
		
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(mobileNumber);
		if (!(m.matches())) {
			ValidationUtil.setErrorMessage(mobileOnlyNumbers, context);
			return false;
		} else if (mobileNumber.length()!=10) {
			ValidationUtil.setErrorMessage(mobileNumbRange, context);
			return false;
		}
		return true;	
	}    
}