package com.freecharge.mobile.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.mobile.web.view.EditAddress;
import com.freecharge.web.webdo.ProfileWebDO;

public class ProfileAddressValidator implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return ProfileWebDO.class.equals(aClass);
	}
		
	@Override
	public void validate(Object o, Errors errors) {
		EditAddress editAddress = (EditAddress)o;
		String pincode = editAddress.getPincode();
		Integer city = editAddress.getCity();

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "m.address.required", "#m.address.required#");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "area", "m.area.required", "#m.area.required#");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pincode", "m.pincode.required", "#m.pincode.required#");
		
		if (city == null || (city.toString()).isEmpty()) {
			errors.rejectValue("city", "m.select.any.city");  
		}
	
		if (pincode != null && !(pincode.isEmpty())) {
			Pattern p = Pattern.compile("^\\d+$");
			Matcher m = p.matcher(pincode.trim()); 
			if (!(m.matches())) {
	    		   errors.rejectValue("pincode", "m.pincode.only.numb");  
	    	  } else {
	    		   if (pincode.trim().length() > 6) {
	    			   errors.rejectValue("pincode", "m.pincode.range");  
	    		   }
	    	   }		
		}		
	}
}
