package com.freecharge.mobile.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.freecharge.mobile.validator.MobileRechargeAmountValidator;

@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = MobileRechargeAmountValidator.class)
@Documented
public @interface  MobileRechargeAmount {

	String message() default "Invalid Mobile Recharge amount";

    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}