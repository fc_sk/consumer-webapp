package com.freecharge.mobile.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.freecharge.mobile.validator.ValidConfirmPasswordValidator;

@Target( {TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ValidConfirmPasswordValidator.class)
@Documented
public @interface ValidConfirmPassword {
	String message() default "Password fields should match";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}

