package com.freecharge.mobile.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.util.LanguageUtility;
import com.freecharge.mobile.validator.annotation.NetBankCode;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;

public class NetBankCodeValidator implements ConstraintValidator<NetBankCode, String> {
	
	@Autowired
	public LanguageUtility languageUtility;
	
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	
	@Override
	public void initialize(NetBankCode bankCode) {
		// nothing to do here.
	}
	@Override
	public boolean isValid(String bankCode, ConstraintValidatorContext context) {
		//For Internationalization
		String bankRequired = languageUtility.getLocaleMessage("m.bank.required", null);
		String invalidBank = languageUtility.getLocaleMessage("m.invalid.bank", null);
		
		boolean isValid = true;
		if (bankCode == null || bankCode.isEmpty()) {
			ValidationUtil.setErrorMessage(bankRequired, context);
			isValid = false;	
		} else if(paymentTransactionService.getBankMasterForRequest(bankCode, Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING)) == null) {
			ValidationUtil.setErrorMessage(invalidBank, context);
			isValid = false;	
		}
		if(!isValid) {
			return false;
		}
		return true;
	}
}
