package com.freecharge.mobile.constant;

public class MobileConstants {
	public static final int COOKIE_MAX_AGE = Integer.MAX_VALUE;
	public static final int MOBILE_CHANNEL_ID = 2;
	
	public static final String COOKIE_NAME_CHANNEL_ID="fcChannel";
	public static final String COOKIE_NAME_MOBILE_APP_VERSION="fcversion";
	public static final String COOKIE_NAME_APP_TYPE="fcAppType";
	
	public static final String REQUEST_PARAM_NAME_CHANNEL_ID="fcChannel";
	public static final String REQUEST_PARAM_NAME_APP_TYPE="fcAppType";
	
	public static final String MOBILE_APP_TYPE_ANDROID = "android";
	public static final String MOBILE_APP_TYPE_IOS = "ios";
	public static final String MOBILE_APP_TYPE_BLACK_BERRY = "blackberry";
	public static final String MOBILE_APP_TYPE_SYMBIAN = "symbian";
	public static final String MOBILE_APP_TYPE_WINDOWS_MOBILE = "windowsmobile";
	public static final String MOBILE_APP_TYPE_TIZEN = "tizen";
	
	public static final String[] VALID_APP_TYPES = new String[] {MOBILE_APP_TYPE_ANDROID, 
				MOBILE_APP_TYPE_BLACK_BERRY, MOBILE_APP_TYPE_IOS, 
					MOBILE_APP_TYPE_SYMBIAN, MOBILE_APP_TYPE_WINDOWS_MOBILE, MOBILE_APP_TYPE_TIZEN}; 
	
	public static final String CART_EXPIRED_VIEW="mobile/mobile-cart-expired-page";
	
	
	
    public static final int      SUCCESS                        = 0;
    public static final int      PAYMENT_ATTEMPT_ERROR          = 1;
    public static final int      CART_EXPIRE                    = 2;
    public static final int      FRAUD_DETECTION                = 3;
    public static final int      CHECKOUT_BEAN_ERROR            = 0;
    public static final int      CHECKOUT_BEAN_VALIDATION_ERROR = 1;
    public static final int      GENERAL_ERROR                  = 4;


        
        public static final String ERROR_TECHNICAL="Technical Error Occured please try again after some time";
        
        
        public static String paymentAttemptCheckoutBeanError()
        {
            return MobileURLConstants.MOBILE_APP_PAYMENT_ERROR+"/"+PAYMENT_ATTEMPT_ERROR+"?errorcode="+CHECKOUT_BEAN_ERROR+"&errorstring="+ERROR_TECHNICAL;
        }
        
        public static String paymentAttemptGeneralError()
        {
           return  MobileURLConstants.MOBILE_APP_PAYMENT_ERROR+"/"+PAYMENT_ATTEMPT_ERROR+"?errorcode="+GENERAL_ERROR+"&errorstring="+ERROR_TECHNICAL;
        }
        
        public static String paymentAttemptFruadDetectionError()
        {
            return MobileURLConstants.MOBILE_APP_PAYMENT_ERROR+"/"+FRAUD_DETECTION+"?errorcode="+GENERAL_ERROR+"&errorstring="+ERROR_TECHNICAL;
        }
        
        public static String paymentAttemptCheckoutBeanValidationError(String errorStatus)
        {
            return MobileURLConstants.MOBILE_APP_PAYMENT_ERROR+"/"+PAYMENT_ATTEMPT_ERROR+"?errorcode="+CHECKOUT_BEAN_VALIDATION_ERROR+"&errorstring="+errorStatus;
        }
        
        public static String paymentAttemptCartExpireError()
        {
            return MobileURLConstants.MOBILE_APP_PAYMENT_ERROR+"/"+CART_EXPIRE+"?errorcode="+GENERAL_ERROR+"&errorstring="+ERROR_TECHNICAL;
        }
        

}

