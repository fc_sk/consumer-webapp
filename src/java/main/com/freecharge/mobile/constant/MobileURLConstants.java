package com.freecharge.mobile.constant;

public class MobileURLConstants {
	public static final String MOBILE_SITE_PREFIX = "/m";
	public static final String MOBILE_HOME_REDIRECT = "redirect:" + MOBILE_SITE_PREFIX;
	public static final String MOBILE_COUPONS_SELECT_REDIRECT = "redirect:" + MOBILE_SITE_PREFIX + "/select-coupons";
	public static final String MOBILE_BALANCE_PREFIX = "/m/mycredits";
	public static final String MOBILE_BALANCE_REDIRECT = "redirect:" + MOBILE_BALANCE_PREFIX;
	public static final String MOBILE_APP_STSTUS = "redirect:" + "/app/FcAppStatus";
	public static final String MOBILE_APP_PAYMENT_ERROR = "redirect:" + "/app/FcAppError";
			
}
