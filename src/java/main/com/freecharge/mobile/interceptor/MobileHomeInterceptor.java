package com.freecharge.mobile.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.wurfl.core.Device;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.mobile.web.util.MobileUtil;

public class MobileHomeInterceptor extends AbstractMobileInterceptor {
	private final Logger logger = LoggingFactory.getLogger(getClass());

	/*private String[] MOBILE_VALID_URLS = new String[] {"/airtel-prepaid-mobile-recharge","/online-mobile-recharge","/online-dth-recharge",
			"/online-datacard-recharge", "/airtel-prepaid-mobile-recharge", "/aircel-prepaid-mobile-recharge", "/vodafone-prepaid-mobile-recharge",
			"/bsnl-prepaid-mobile-recharge", "/tata-docomo-online-recharge", "/idea-prepaid-mobile-recharge", "/tata-indicom-walky-prepaid-mobile-recharge",
			"/loop-prepaid-mobile-recharge", "/mtnl-trump-prepaid-mobile-recharge", "/reliance-cdma-prepaid-mobile-recharge", "/reliance-gsm-prepaid-mobile-recharge",
			"/tata-indicom-prepaid-mobile-recharge", "/uninor-prepaid-mobile-recharge", "/mts-prepaid-mobile-recharge", "/videocon-prepaid-mobile-recharge",
			"/virgin-mobile-online-recharge", "/tata-sky-online-recharge"};*/
	
			
	@Override
	public boolean doMobilePreHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception{
        String queryString = request.getQueryString();
        
	    boolean processFurther = true;
		if(request.getParameter("desktop")!=null && request.getParameter("desktop").equals("true"))
		{
			return processFurther;
		}
		if (MobileUtil.isMobile(request)) {
			Device device = MobileUtil.getDevice(request);
			if (device != null) {
				logger.info("Forwarding to mobile site. Mobile Device Id: " + device.getId() +" Mobile Device User Agent: "
						+ device.getUserAgent());
			}
			String redirectUrl = getRedirectUrl(request);
			response.sendRedirect(redirectUrl);
			processFurther = false;
		}
		return processFurther;
	}

	private String getRedirectUrl(HttpServletRequest request) {
        /*
         * Amazing hack to get campaign pages working
         */
	    String queryString = request.getQueryString();
	    if (StringUtils.isNotEmpty(queryString)) {
	    	boolean isValidQueryString = GenericValidator.isUrl(queryString);
	    	if(isValidQueryString) {
	        if (queryString.contains("extName=mcdGiftFestival&extHeading")) {
	            return MobileURLConstants.MOBILE_SITE_PREFIX + "/mcd";
	        }
	        if (queryString.contains("extName=DanceIndiaDance4")) {
	            return MobileURLConstants.MOBILE_SITE_PREFIX + "/did4";
	        }
	        if (queryString.contains("extName=Amazon")) {
	            return MobileURLConstants.MOBILE_SITE_PREFIX + "/amazon";
	        }
	    }
	 }
		String startUrl = String.valueOf(request.getAttribute("javax.servlet.forward.request_uri"));
		String redirectUrl = MobileURLConstants.MOBILE_SITE_PREFIX + MobileUtil.getRequestString(request);
		if (startUrl.length() > 1){
			redirectUrl = MobileURLConstants.MOBILE_SITE_PREFIX + startUrl;
		}
		return redirectUrl;
	}
}
