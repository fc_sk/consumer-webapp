package com.freecharge.mobile.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mobile.web.util.MobileUtil;

public class PasswordResetIntercepror extends AbstractMobileInterceptor {
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	@Override
	public void doMobilePostHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (MobileUtil.isMobile(request)) {
			String status = (String)modelAndView.getModel().get("status");
			modelAndView.setView(null);
			if (status == null || status.equals("fail")) {
				modelAndView.setViewName("home/error");
			} else if (status.equals("ExpairedLink")) {
				modelAndView.setViewName("mobile/expiredlink");
			} else if (status.equals("InvalidLink")) {
				modelAndView.setViewName("mobile/invalidlink");
			} else if (status.equals("ValidLink")) {
				modelAndView.setViewName("mobile/passwordrecovery");
			}
		}
	}
}
