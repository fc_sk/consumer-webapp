package com.freecharge.mobile.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.mobile.web.util.MobileUtil;

public abstract class AbstractMobileInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if (!MobileUtil.isMobileInterceptEnabled()){
			// if mobile interception is not enabled return to next interceptor.
			return true; 
		}
		return doMobilePreHandle(request, response, handler);
	}
	
	public boolean doMobilePreHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception{
		return true;
	}
	
	public void doMobilePostHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception{
		// do nothing;
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception{
		// do nothing;
		if (!MobileUtil.isMobileInterceptEnabled()){
			// if mobile interception is not enabled return to next interceptor.
			return; 
		}
		doMobilePostHandle( request, response, handler, modelAndView);
	}
}
