package com.freecharge.mobile.interceptor;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.mobile.constant.MobileURLConstants;
import com.freecharge.web.interceptor.AbstractLoginInterceptor;

public class MobileLoginInterceptor extends AbstractLoginInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if (isLoggedIn()) {
			return true;
		}
		request.setAttribute("loginFrom", request.getRequestURI());
		RequestDispatcher rd = request.getRequestDispatcher(MobileURLConstants.MOBILE_SITE_PREFIX + "/handle-login");
		rd.forward(request, response);
		return false;
	}
}
