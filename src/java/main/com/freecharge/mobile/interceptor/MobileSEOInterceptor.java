package com.freecharge.mobile.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class MobileSEOInterceptor extends HandlerInterceptorAdapter {
    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) 
            throws Exception {
        /* Reason : Optimization for Google Mobile search results 
         * JIRA TASK - https://freecharge.atlassian.net/browse/FC-2752 
         * https://developers.google.com/webmasters/mobile-sites/mobile-seo/configurations/dynamic-serving
         */
    	if (request.getRequestURI().contains("/m/home") || request.getRequestURI().contains("/m/new")) {
    		response.setHeader("Vary", "User-Agent");
    	}
        
    }
}
