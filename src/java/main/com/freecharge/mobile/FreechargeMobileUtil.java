package com.freecharge.mobile;

import javax.servlet.http.HttpServletRequest;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;

public class FreechargeMobileUtil {
	public static boolean isMobileRequest(HttpServletRequest request) {
		
		Device currentDevice = DeviceUtils.getCurrentDevice(request);
		boolean mobile = currentDevice.isMobile();
		return mobile;
	}
}
