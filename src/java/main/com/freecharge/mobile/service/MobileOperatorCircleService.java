package com.freecharge.mobile.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.mobile.web.view.Circle;
import com.freecharge.mobile.web.view.MasterData;
import com.freecharge.mobile.web.view.OperatorCircle;

public class MobileOperatorCircleService {
	
	@Autowired
	private OperatorCircleService operatorCircleService;
	
	@Autowired
	private MasterData masterData;
	
	public OperatorCircle getOperatorCircle(
			OperatorCircleBusinessDO operatorCicleBDO) {
		operatorCircleService.getOperatorCircleList(operatorCicleBDO);
		List<Map<String, Object>> data = operatorCicleBDO.getPrefixData();
		OperatorCircle operatorCircle = new OperatorCircle();
		for (Map<String, Object> columnData : data) {
			String circleId = String.valueOf(columnData.get("circleMasterId"));
			String circleName = String.valueOf(columnData.get("circleName"));
			String operatorId = String.valueOf(columnData
					.get("operatorMasterId"));
			if (circleId != null
					&& circleName != null
					&& (operatorId != null && !operatorId.equals("null") && !operatorId
							.isEmpty())) {
				Circle circle = new Circle();
				circle.setCircleId(circleId);
				circle.setName(circleName);
				operatorCircle.setCircle(circle);
				operatorCircle.setOperatorId(operatorId);
				return operatorCircle;
			}
		}
		return operatorCircle;
	}
}
