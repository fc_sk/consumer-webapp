package com.freecharge.mobile.service.helper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.TxnCrossSellService;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;

public class CrossSellHelper {
	private static Logger logger = LoggingFactory.getLogger(CrossSellHelper.class);
	@Autowired
	private TxnCrossSellService txnCrossSellService;
	
	public void saveCrossSell(String couponsList, TxnHomePageBusinessDO txnHomePage){
		TxnCrossSellBusinessDo txnCrossSellBusinessDo = new TxnCrossSellBusinessDo();
		txnCrossSellBusinessDo.setHomePageId(txnHomePage.getTxnHomePageId());
		txnCrossSellBusinessDo.setType("coupon");
		txnCrossSellBusinessDo.setProType(txnHomePage.getProductType());
		txnCrossSellBusinessDo.setLookupID(txnHomePage.getLookupId());
		txnCrossSellBusinessDo.setServiceNo(txnHomePage.getServiceNumber());
		txnCrossSellBusinessDo.setAmount(txnHomePage.getAmount());
		txnCrossSellBusinessDo.setOperatorName(txnHomePage.getOperatorName());
		txnCrossSellBusinessDo.setItemIdArray(couponsList);
		txnCrossSellService.saveAllTxnCrossSellService(txnCrossSellBusinessDo);
	}
}
