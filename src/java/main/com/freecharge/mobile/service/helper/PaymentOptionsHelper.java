package com.freecharge.mobile.service.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.service.util.CommonUtil;
import com.freecharge.mobile.web.view.payment.Bank;
import com.freecharge.mobile.web.view.payment.CreditCard;
import com.freecharge.mobile.web.view.payment.DebitCard;
import com.freecharge.mobile.web.view.payment.DebitCardType;
import com.freecharge.mobile.web.view.payment.InternetBanking;
import com.freecharge.mobile.web.view.payment.Wallet;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.util.WebConstants;

public class PaymentOptionsHelper {

	@Autowired
	private WalletService walletService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private FreefundService freefundService;
	
	@Autowired
    private UserServiceProxy userServiceProxy;

	@Autowired
    private PaymentTransactionService paymentTransactionService;
	
	@Autowired
    private PricingService pricingService;
	
	@Autowired
	WalletWrapper walletWrapper;
	
	public DebitCard getDebitCard(String productType) {
		DebitCard dc = new DebitCard();
		dc.setYearList(CommonUtil.getYearListForCards());
		List<DebitCardType> dcTypes = getDebitCardTypes(productType);
		dc.setCardTypeList(dcTypes);
		return dc;
	}

	public List<DebitCardType> getDebitCardTypes(String productType) {
		List<DebitCardType> dcTypes = new ArrayList<DebitCardType>();
		String key = productType + "_" + FCConstants.DEFAULT_AFFILIATE_ID;
		List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate
				.get(key);
		if (ptmbs != null) {
			for (PaymentTypeMasterBusinessDO ptmb : ptmbs) {
				if (String.valueOf(ptmb.getId()).equals(
						PaymentConstants.PAYMENT_TYPE_DEBITCARD)) {
					List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation
							.get(ptmb.getRelationId());
					for (PaymentTypeOptionBusinessDO ptob : ptobs) {
						DebitCardType dcType = new DebitCardType();
						dcType.setCode(ptob.getCode());
						dcType.setName(ptob.getDisplayName());
						if (ptob.getCode() != null && ptob.getCode().equals(PaymentConstants.MAESTRO_CITI)){
							//TODO: check if the display name can be changed in DB.
							dcType.setName("Maestro");
						}
						dcTypes.add(dcType);
					}
				}
			}
		}
		return dcTypes;
	}
	
	public InternetBanking getNetBanking(String productType){
		InternetBanking netBanking = new InternetBanking();
		netBanking.setBankList(getNetBankingBanks(productType));
		return netBanking;
	}
	
	public List<Bank> getNetBankingBanks(String productType) {
		List<Bank> banks = new ArrayList<Bank>();
		String key = productType + "_" + FCConstants.DEFAULT_AFFILIATE_ID;
		List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate
				.get(key);
		if (ptmbs != null) {
			for (PaymentTypeMasterBusinessDO ptmb : ptmbs) {
				if (String.valueOf(ptmb.getId()).equals(
						PaymentConstants.PAYMENT_TYPE_NETBANKING)) {
					List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation
							.get(ptmb.getRelationId());
					for (PaymentTypeOptionBusinessDO ptob : ptobs) {
						/*DebitCardType dcType = new DebitCardType();
						dcType.setCode(ptob.getCode());
						dcType.setName(ptob.getDisplayName());
						dcTypes.add(dcType);*/
						Bank bank = new Bank();
						bank.setBankCode(ptob.getCode());
						bank.setBankName(ptob.getDisplayName());
						banks.add(bank);
					}
				}
			}
		}
		return banks;
	}

	public CreditCard getCreditCard() {
		CreditCard cc = new CreditCard();
		cc.setYearList(CommonUtil.getYearListForCards());
		return cc;
	}

	public Wallet getWalletData() {
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		if (!walletService.isWalletPayEnabled()
				|| !walletService.isWalletDataDisplayEnabled()) {
			return null;
		}
		Wallet wallet = new Wallet();
		if (walletService.isWalletDataDisplayEnabled()) {
			Object userIdObject = fs.getSessionData().get(
					WebConstants.SESSION_USER_USERID);
			Amount walletBalance = walletWrapper.getTotalBalance(userIdObject == null ? -1
							: (Integer) userIdObject);
			wallet.setBalance(walletBalance);
		}
		return wallet;
	}

	public double getTransactionAmount(String merchantOrderId) {
		List<PaymentTransaction> paymentTransactions = paymentTransactionService
				.getPaymentTransactionByMerchantOrderId(merchantOrderId);
		return paymentTransactions.get(0).getAmount();
	}
	
	//For partial payment
	public WalletEligibility getWalletEligibility(String lookupid) throws CartExpiredException{
		CartBusinessDo cartBusinessDo = cartService.getCart(lookupid);
		if (cartBusinessDo == null){
			throw new CartExpiredException();
		}
		WalletEligibility walletEligibility = new WalletEligibility();
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		Boolean isloggedin = false;
		if (fs != null) {
			Map map = fs.getSessionData();
			if (map != null) {
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map
							.get(WebConstants.SESSION_USER_IS_LOGIN);
				}
			}
		}
		if (isloggedin != null && isloggedin) {
			Double totalPayableAmount = pricingService
					.getPayableAmount(cartBusinessDo);
			if (walletService.isWalletDataDisplayEnabled()) {
				Object userIdObject = fs.getSessionData().get(
						WebConstants.SESSION_USER_USERID);
				int userId = userIdObject == null ? -1 : (Integer) userIdObject;
				// calculate wallet eligibility only when no freefund is present
				// otherwise both wallet eligibilities are set to false.
				Amount walletBalance = walletWrapper.getTotalBalance(userId);
				
				if (!freefundService.containsFreeFund(cartBusinessDo)) {
					walletEligibility = walletService.calculateEligibility(
							userId, new Amount(totalPayableAmount), cartBusinessDo, walletBalance);
				}
			}
		}
		return walletEligibility;
	}
	
    public PaymentBreakup getPaymentBreakup(String lookupid) {
        CartBusinessDo cartBusinessDo = cartService.getCart(lookupid);
        Double totalPayableAmount = pricingService.getPayableAmount(cartBusinessDo);

        Users user = userServiceProxy.getLoggedInUser();
        
        Amount walletBalance = walletWrapper.getTotalBalance(user.getUserId());

        WalletEligibility walletEligibility = walletService.calculateEligibility(user.getUserId(), new Amount(
                totalPayableAmount), cartBusinessDo, walletBalance);
        PaymentBreakup paymentBreakup = walletService.calculatePaymentBreakup(user.getUserId(), new Amount(
                totalPayableAmount), cartBusinessDo, walletEligibility, walletBalance);

        return paymentBreakup;
    }
}
