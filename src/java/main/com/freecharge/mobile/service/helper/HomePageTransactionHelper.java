package com.freecharge.mobile.service.helper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;

public class HomePageTransactionHelper {
	@Autowired
	private TxnHomePageService txnHomePageService;
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	public TxnHomePageBusinessDO saveHomePageTransaction(String serviceNumber,
			String operatorId, String circleId, String amount,
			String sessionId, String lookupId, String productType) {
		logger.debug("Started Saving HomePageTransaction in saveHomePageTransaction() for service number"+serviceNumber);
		TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
		txnHomePageBusinessDO.setServiceNumber(serviceNumber);
		txnHomePageBusinessDO.setProductType(productType);
		txnHomePageBusinessDO.setLookupId(lookupId);
		txnHomePageBusinessDO.setSessionId(sessionId);
		txnHomePageBusinessDO.setAmount(Float.parseFloat(amount));
		// TOOD: Oh yeah, circlename is basically circle id. lets change this
		// when we port mobile changes to web.
		txnHomePageBusinessDO.setCircleName(circleId);
		// TOOD: Oh yeah, operatorname is basically operator id. lets change
		// this when we port mobile changes to web.
		txnHomePageBusinessDO.setOperatorName(operatorId);
		txnHomePageService.saveTransactionInfo(txnHomePageBusinessDO);
		logger.debug("finished saving HomePageTransaction in saveHomePageTransaction() for service number"+serviceNumber);
		return txnHomePageBusinessDO;
	}
	
	public TxnHomePageBusinessDO getHomePageTransaction(String lookupId) {
		return txnHomePageService.getTransactionDetails(lookupId);
	}
}
