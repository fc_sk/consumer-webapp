package com.freecharge.mobile.service.helper;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.web.webdo.CommonSessionWebDo;

public class SessionHelper {
	public void saveDataInSession(TxnHomePageBusinessDO txnHomePage) {
		CommonSessionWebDo commonSessionWebDo = new CommonSessionWebDo();
		commonSessionWebDo.setRechargeAmount(String.valueOf(txnHomePage
				.getAmount()));
		if(txnHomePage.getProductType().equals(ProductMaster.ProductName.MobilePostpaid.getProductType())) {
			commonSessionWebDo.setPostpaidAmount(String.valueOf(txnHomePage.getAmount()));
		}
		commonSessionWebDo.setRechargeMobileNumber(txnHomePage
				.getServiceNumber());
		commonSessionWebDo.setType(txnHomePage.getProductType());
		commonSessionWebDo.setOperatorName(txnHomePage.getOperatorName());
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_type", commonSessionWebDo.getType());
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_rechargeAmount", commonSessionWebDo.getRechargeAmount());
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_postpaidAmount", commonSessionWebDo.getPostpaidAmount());
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_mobNum", commonSessionWebDo.getRechargeMobileNumber());
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_operatorName", commonSessionWebDo.getOperatorName());
        fs.getSessionData().put(txnHomePage.getLookupId() + "_CommonSessionPojo_primaryAmount", commonSessionWebDo.getPrimaryAmount());
        
	}
	
	public void saveLookupIdInSession(String lookupId){
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		fs.getSessionData().put(lookupId + "_CommonSessionPojo_type", ProductMaster.ProductName.HCoupons.getProductType());
	}
}
