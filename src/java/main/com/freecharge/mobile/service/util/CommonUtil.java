package com.freecharge.mobile.service.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.sns.bean.GSTFee;

public class CommonUtil {
	
	
	
	public static final List<String> getYearListForCards(){
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		int maxYear = 2048;
		int difference = maxYear - currentYear;
		List<String> ccYearList = new ArrayList<String>();
		for (int i = 0; i <= difference; ++i) {
			ccYearList.add(String.valueOf(currentYear + i));
		}
		return ccYearList;
	}
	
	public static final GSTFee getGSTAmount(Double amount, ProductName productName)
	{
		Double originalAmount = amount/1.18d;
		Double cgst = (amount - originalAmount)/2;
		Double sgst = (amount - originalAmount)/2;
		Double igst = 0d;
		Double rCgst = Math.round(cgst*100.0)/100.0;
		Double rSgst = Math.round(sgst*100.0)/100.0;
		//Double rIgst = Math.round(igst*100.0)/100.0;
		Double rIgst = igst;
		GSTFee gstFee = new GSTFee();
		gstFee.setCgst(rCgst);
		gstFee.setSgst(rSgst);
		gstFee.setIgst(rIgst);
		Double OrginalAmt = (amount - (rCgst +rSgst + rIgst ));
		Double rOrginalAmt = Math.round(OrginalAmt*100.0)/100.0;
		gstFee.setOriginalAmount(rOrginalAmt);
		return gstFee;
	}
}
