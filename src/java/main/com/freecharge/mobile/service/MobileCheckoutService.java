package com.freecharge.mobile.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.analytics.OrderAnalyticsService;
import com.freecharge.analytics.OrderDetails;
import com.freecharge.antifraud.AntiFraudDAO;
import com.freecharge.antifraud.FraudDetection;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.PersonalDetailsService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.TxnCrossSellService;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.businessdo.PersonalDetailsBusinessDO;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.exception.CartExpiredException;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.TxnCrosssellItemsVO;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.service.exception.PersonalDetailException;
import com.freecharge.mobile.service.helper.CrossSellHelper;
import com.freecharge.mobile.service.helper.HomePageTransactionHelper;
import com.freecharge.mobile.service.helper.PaymentOptionsHelper;
import com.freecharge.mobile.service.helper.SessionHelper;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.mobile.web.view.ShippingDetails;
import com.freecharge.mobile.web.view.payment.CreditCard;
import com.freecharge.mobile.web.view.payment.DebitCard;
import com.freecharge.mobile.web.view.payment.InternetBanking;
import com.freecharge.mobile.web.view.payment.PaymentOptions;
import com.freecharge.mobile.web.view.payment.Wallet;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.services.IPaymentType;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTypeFactory;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;
import com.freecharge.tracker.Tracker;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletUtil;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CartItemsWebDO;
import com.freecharge.web.webdo.CartWebDo;
import com.freecharge.web.webdo.CrosssellWebDo;
import com.freecharge.web.webdo.PaymentTypeMasterWebDO;
import com.freecharge.web.webdo.PaymentTypeOptionWebDO;

public class MobileCheckoutService {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private RechargeInitiateService rechargeInitiateService;

	@Autowired
	private TxnFulFilmentService txnFulFilmentService;

	@Autowired
	private WalletService walletService;

	@Autowired
	private PersonalDetailsService personalDetailsService;

	@Autowired
	private CartService cartService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private CrosssellService crosssellService;

	@Autowired
	private OrderAnalyticsService analyticsService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private VoucherService voucherService;

	@Autowired
	private AntiFraudDAO afDAO;

	@Autowired
	private FraudDetection fd;

	@Autowired
	private OrderIdSlaveDAO orderIdDAO;

	@Autowired
	private FreefundService freeFundService;

	@Autowired
	private PaymentTypeFactory paymentTypeFactory;

	@Autowired
	private HomeBusinessDao homeBusinessDao;

	@Autowired
	private PaymentPlanService paymentPlanService;
	
	@Autowired
	private PaymentSuccessAction paymentAction;

	// TODO: Helper classes. Should be merged with respective service classes
	// after mobile site goes live.
	@Autowired
	private HomePageTransactionHelper homePageTransactionHelper;
	@Autowired
	private SessionHelper sessionHelper;
	@Autowired
	private CrossSellHelper crossSellHelper;
	@Autowired
	private PaymentOptionsHelper paymentOptionsHelper;
	@Autowired
	private TxnHomePageService txnHomePageService;

	@Autowired
	private TxnCrossSellService txnCrossSellService;
	
	@Autowired
	private OperatorCircleService operatorCircleService;
	
	@Autowired
	private Tracker tracker;
	
	@Autowired
	    private OrderIdWriteDAO orderIdWriteDAO;
	
	 @Autowired
	    BillPaymentService billPaymentService;
	
	@Transactional
	public CheckoutData initiateCheckout(String lookupId, String couponsList) throws PersonalDetailException, CartExpiredException {
		TxnHomePageBusinessDO txnHomePage = homePageTransactionHelper.getHomePageTransaction(lookupId);
		if (txnHomePage == null){
			logger.error("Could not find txnHomePage for lookupId: " + lookupId);
			throw new PersonalDetailException();
		}
		return initiateCheckout(lookupId, couponsList, txnHomePage);
	}
	
	@Transactional
	public CheckoutData initiateCheckout(TxnHomePageBusinessDO txnHomePageBusinessDO, String couponsList) throws PersonalDetailException, CartExpiredException {
	    return initiateCheckout(txnHomePageBusinessDO.getLookupId(), couponsList, txnHomePageBusinessDO);
	}

	public CheckoutData initiateAddCashCheckout(String productType,
			String sessionId, String lookupId, String amount) throws PersonalDetailException, CartExpiredException {
		TxnHomePageBusinessDO txnHomePage =txnHomePageService.getTransactionDetails(lookupId);
		if (txnHomePage == null){
			logger.error("Could not find txnHomePage for lookupId: " + lookupId);
			throw new PersonalDetailException();
		}
		return initiateCheckout(lookupId, "", txnHomePage);
	}

	private CheckoutData initiateCheckout(String lookupId, String couponsList, 
			TxnHomePageBusinessDO txnHomePage) throws PersonalDetailException, CartExpiredException {
		logger.debug("Initiate checkout using initiateCheckout() for service number:"+txnHomePage.getServiceNumber());
		//saveSessionAndCartData(couponsList, txnHomePage);
		sessionHelper.saveDataInSession(txnHomePage);
		PersonalDetailsBusinessDO personalDetailsBDO = new PersonalDetailsBusinessDO();
		personalDetailsBDO.setLookupID(lookupId);
		
		CheckoutData checkoutData = new CheckoutData();
		
		Map returnMap = savePersonalDetails(personalDetailsBDO);

		Double amount = (Double)returnMap.get("totalAmount");
		personalDetailsBDO = (PersonalDetailsBusinessDO)returnMap.get("personalDetails");
		
		if (personalDetailsBDO.getContactDetailsReq()) {
        	checkoutData.setContactDetailsReq(true);
        	
    		// Copy over the shipping details
    		ShippingDetails shippingDetails = MobileUtil.initializeShippingDetails(personalDetailsBDO);
        	//ShippingDetails shippingDetails = MapperUtil.map(personalDetailsBDO.getUserContactDetail(), ShippingDetails.class);

    		Map<String, Object> miscCheckoutDetailsMap = shippingDetails.getMiscCheckoutDetailsMap();
    		if (miscCheckoutDetailsMap == null) 
    			miscCheckoutDetailsMap = new HashMap<String, Object>();
    		
      		// Adding other checkout related data
    		
    		miscCheckoutDetailsMap.put("defaultProfile", personalDetailsBDO.getDefaultProfile());
    		miscCheckoutDetailsMap.put("lookupId", personalDetailsBDO.getLookupID());
    		miscCheckoutDetailsMap.put("totalAmount", amount);
    		miscCheckoutDetailsMap.put("serviceNumber", txnHomePage.getServiceNumber());
    		miscCheckoutDetailsMap.put("rechargeAmount", txnHomePage.getAmount());
    		miscCheckoutDetailsMap.put("productType", txnHomePage.getProductType());
    		miscCheckoutDetailsMap.put("walletEligibility", personalDetailsBDO.getWalletEligibility());
    		miscCheckoutDetailsMap.put("paymentBreakup",personalDetailsBDO.getPaymentBreakup());
  
    		shippingDetails.setMiscCheckoutDetailsMap(miscCheckoutDetailsMap);
    		checkoutData.setShippingDetails(shippingDetails);
        
		} else {

			checkoutData = proceedWithCheckout(checkoutData, lookupId, amount, personalDetailsBDO.getWalletEligibility() ,
					personalDetailsBDO.getPaymentBreakup(), txnHomePage.getServiceNumber(), txnHomePage.getAmount(),
					txnHomePage.getProductType());
			checkoutData.setContactDetailsReq(false);
		}
		return checkoutData;
	}

	private CheckoutData getCheckoutData(String lookupId, Double amount,
			String productType) {
		CheckoutData checkoutData = getCheckoutData(productType);
		checkoutData.setTotalAmount(amount);
		checkoutData.setLookupId(lookupId);
		checkoutData.setProductType(productType);
		return checkoutData;
	}
	
	public String saveTxnHomePage(String number, Integer amount,
			String productType, String operator, String circle){
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		String sessionId = fs.getUuid();
		String lookupId = FCUtil.getLookupID();
		tracker.trackLookupId(lookupId);
		TxnHomePageBusinessDO txnHomePage = homePageTransactionHelper
					.saveHomePageTransaction(number, operator, circle, String.valueOf(amount),
							sessionId, lookupId, productType);	
		sessionHelper.saveDataInSession(txnHomePage);
		TxnCrossSellBusinessDo bdo = new TxnCrossSellBusinessDo();
		bdo.setLookupID(lookupId);
		bdo.setHomePageId(txnHomePage.getTxnHomePageId());
		bdo.setServiceNo(txnHomePage.getServiceNumber());
		bdo.setProType(txnHomePage.getProductType());
		bdo.setOperatorName(txnHomePage.getOperatorName());
		txnCrossSellService.saveAllTxnCrossSellService(bdo);
		return lookupId;
	}

	public CheckoutData getCheckoutDataForCouponSelect(String lookupId) {
		TxnHomePageBusinessDO txnHomePage =  homePageTransactionHelper.getHomePageTransaction(lookupId);
		CheckoutData checkoutData = new CheckoutData();
		checkoutData.setLookupId(lookupId);
		checkoutData.setRechargeAmount(txnHomePage.getAmount().intValue());
		checkoutData.setNumber(txnHomePage.getServiceNumber());
		OperatorMaster operatorMaster = operatorCircleService.getOperator(txnHomePage.getOperatorName());
		checkoutData.setOperator(String.valueOf(operatorMaster.getOperatorMasterId()));
		CircleMaster circle = operatorCircleService.getCircle(txnHomePage.getCircleName());
		if(circle != null) {
		    checkoutData.setCircle(String.valueOf(circle.getCircleMasterId()));
		}
		checkoutData.setProductType(txnHomePage.getProductType());
		return checkoutData;
	}
	
	public CheckoutData getCheckoutData(String productType) {
		CheckoutData checkoutData = new CheckoutData();
		Wallet wallet = paymentOptionsHelper.getWalletData();
		CreditCard creditCard = paymentOptionsHelper.getCreditCard();
		DebitCard debitCard = paymentOptionsHelper.getDebitCard(productType);
		InternetBanking netBanking = paymentOptionsHelper
				.getNetBanking(productType);
		PaymentOptions paymentOptions = new PaymentOptions();
		paymentOptions.setWallet(wallet);
		paymentOptions.setCreditCard(creditCard);
		paymentOptions.setDebitCard(debitCard);
		paymentOptions.setNetBanking(netBanking);
	
		checkoutData.setProductType(productType);
		checkoutData.setPaymentOptions(paymentOptions);
		
		return checkoutData;
	}


	private Map savePersonalDetails(BaseBusinessDO baseBusinessDO) throws PersonalDetailException, CartExpiredException {
		Double totalAmount = null;
		ProductPaymentBusinessDO paydo = new ProductPaymentBusinessDO();
		Map<String, Object> returnMap = new HashMap<String, Object>();

		PersonalDetailsBusinessDO personalDetailsBDO = (PersonalDetailsBusinessDO) baseBusinessDO;
		personalDetailsBDO.setLookupID(personalDetailsBDO.getLookupID());
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(personalDetailsBDO.getLookupID(), fs);
        String rechargeAmount = FCSessionUtil.getRechargeAmount(personalDetailsBDO.getLookupID(), fs);
		String status = null;
		String lookupId = personalDetailsBDO.getLookupID();
		try {
			WebContext webContext = new WebContext();
			webContext.setBaseBusinessDO(personalDetailsBDO);
			status = personalDetailsService
					.getOrCreatePersonalDetails(baseBusinessDO);
		} catch (Exception exp) {
			status = "fail";
		}
		if (status == null || status.equals("fail") || status.equals("withoutlogin")) {
			throw new PersonalDetailException();
		}
		personalDetailsBDO.setSatus(status);
		CartBusinessDo cartBusinessDo = cartService.getCart(personalDetailsBDO
				.getLookupID());
		List<TxnCrosssellItemsVO> croseSellIdList = cartService
				.getTxnCrossSellIdByHomePageID(personalDetailsBDO
						.getTxnHomePageId());
		personalDetailsBDO.setCart(cartBusinessDo);
		personalDetailsBDO.setTxnCrossSellIdList(croseSellIdList);
		if (personalDetailsBDO.getContactDetailsReq())
			commonService.getStateMasterList(personalDetailsBDO);

		List<CrosssellBusinessDo> crosssellBusinessDos = null;
		if (rechargeAmount != null) {
            Float amt = Float.parseFloat(rechargeAmount);
            String productType = type == null ? "V" : type;
			crosssellBusinessDos = crosssellService.getAllCrosssellCoupon(amt,
					productType);
			personalDetailsBDO.setCrosssellBusinessDos(crosssellBusinessDos);
		}
		Boolean isloggedin = false;
		if (fs != null) {
			Map map = fs.getSessionData();
			if (map != null) {
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map
							.get(WebConstants.SESSION_USER_IS_LOGIN);
				}
			}
		}
		if (isloggedin != null && isloggedin) {
			if (personalDetailsBDO.getContactDetailsReq() && personalDetailsBDO.getUserContactDetail() != null){
				Integer stateId = personalDetailsBDO.getUserContactDetail()
						.getStateId();
				CityMasterBusinessDO cityMasterBusinessDO = new CityMasterBusinessDO();
				cityMasterBusinessDO.setStateId(stateId);
				commonService.getCityMasterList(cityMasterBusinessDO);
				personalDetailsBDO.setCityMasterList(cityMasterBusinessDO
						.getCityMasterList());
			}
            if (rechargeAmount != null) {
                Float amt = Float.parseFloat(rechargeAmount);
                String productType = type == null ? "V" : type;
				personalDetailsBDO.setProductType(productType);
				paymentService.getPaymentTypeOptionsForWeb(productType,
						FCConstants.DEFAULT_AFFILIATE_ID);
			}
			UserContactDetail contactDetail = personalDetailsBDO
					.getUserContactDetail();
			if (contactDetail != null) {
                fs.getSessionData()
                        .put(personalDetailsBDO.getLookupID() + "_" + FCConstants.SESSION_DATA_USERCONTACTDETAIL
                                + "_userId", contactDetail.getUserId());
			}

			personalDetailsBDO.setWalletEnabled(walletService
					.isWalletPayEnabled());
			personalDetailsBDO.setWalletDisplayEnabled(walletService
					.isWalletDataDisplayEnabled());
			// For partial payment

			WalletEligibility walletEligibility = new WalletEligibility();
			walletEligibility.setEligibleForPartialPayment(true);
			walletEligibility.setEligibleForFullWalletPayment(true);
			personalDetailsBDO.setWalletEligibility(walletEligibility);

			returnMap.put("personalDetails", personalDetailsBDO);
		}

		if (cartBusinessDo != null) {
			CartWebDo cartWebDo = new CartWebDo();
			cartWebDo.setId(cartBusinessDo.getId());
			cartWebDo.setLookupID(cartBusinessDo.getLookupID());
			cartWebDo.setOpen(cartBusinessDo.getOpen());
			cartWebDo.setOrderId(cartBusinessDo.getOrderId());
			cartWebDo.setUserId(cartBusinessDo.getUserId());
			Double totalPrice = 0d;

			List<CartItemsWebDO> list = new ArrayList<CartItemsWebDO>();
			for (CartItemsBusinessDO cibo : cartBusinessDo.getCartItems()) {
				if (cibo.getDeleted())
					continue;
				CartItemsWebDO ciw = new CartItemsWebDO();
				ciw.setDisplayLabel(cibo.getDisplayLabel());
				ciw.setId(cibo.getId());
				ciw.setImgUrl("");
				ciw.setItemId(cibo.getItemId());
				ciw.setItemTableName(cibo.getEntityId());
				ciw.setLookupID(cibo.getLookupID());
				ciw.setPrice(cibo.getPrice());
				ciw.setProductId(cibo.getProductMasterId());
				ciw.setMerchantName("Testing");
				ciw.setQuantity(cibo.getQuantity());

				Integer productType = FCConstants.productMasterType.get(cibo
						.getProductMasterId());
				String productTypeStr = FCConstants.productTypeMap
						.get(productType);
				ciw.setProductType(productTypeStr);

				if (!(ciw.getItemTableName() != null && ciw.getItemTableName()
						.equalsIgnoreCase("site_merchant"))) {
					totalPrice += cibo.getPrice();
				}

				if (ciw.getItemTableName() != null
						&& ciw.getItemTableName().equalsIgnoreCase("FreeFund")) {
					totalPrice -= 30;
				}

				list.add(ciw);
			}

			cartWebDo.setItemsList(list);
			cartWebDo.setTotalCartPrice(totalPrice);

			// TODO: Check what is to be done with cart.
			// checkoutData.setCart(cartWebDo);
			// checkoutData.setTotalAmount(totalPrice);
			totalAmount = totalPrice;
			// personalDetailsWebDO.setCart(cartWebDo);
			paydo.setAmount(totalPrice);
		}
		List<CrosssellBusinessDo> crosssellBusinessDoList = personalDetailsBDO
				.getCrosssellBusinessDos();
		// List<CrosssellWebDo> crosssellWebDoList = new
		// ArrayList<CrosssellWebDo>();
		Map<String, CrosssellWebDo> crosselwebdomap = new HashMap<String, CrosssellWebDo>();
		if (crosssellBusinessDoList != null) { // No Active crosssell exist
			for (CrosssellBusinessDo crosssellBusinessDo : crosssellBusinessDoList) {
				CrosssellWebDo crosssellWebDo = new CrosssellWebDo();
				crosssellWebDo.setCategory(crosssellBusinessDo.getCategory());
				crosssellWebDo.setDescription(crosssellBusinessDo
						.getDescription());
				crosssellWebDo.setHeadDescription(crosssellBusinessDo
						.getHeadDescription());
				crosssellWebDo.setId(crosssellBusinessDo.getId());
				crosssellWebDo.setImgurl(crosssellBusinessDo.getImgurl());
				crosssellWebDo.setLookupID(crosssellBusinessDo.getLookupID());
				crosssellWebDo.setOrder(crosssellBusinessDo.getOrder());
				crosssellWebDo.setPrice(crosssellBusinessDo.getPrice());
				crosssellWebDo
						.setRefundable(crosssellBusinessDo.isRefundable());
				crosssellWebDo.setMaxuses(crosssellBusinessDo.getMaxuses());
				crosssellWebDo.setOthercondition(crosssellBusinessDo
						.getOthercondition());
				crosssellWebDo.setPrimaryproduct(crosssellBusinessDo
						.getPrimaryproduct());
				crosssellWebDo.setPrimaryproductamount(crosssellBusinessDo
						.getPrimaryproductamount());
				crosssellWebDo.setTitle(crosssellBusinessDo.getTitle());
				crosssellWebDo.setClassName(crosssellBusinessDo.getClassName());
				crosssellWebDo.setTemplate(crosssellBusinessDo.getTemplate());
				crosssellWebDo.setTnc(crosssellBusinessDo.getTnc());
				crosssellWebDo.setMailTnc(crosssellBusinessDo.getMailTnc());
				crosssellWebDo.setMailSubject(crosssellBusinessDo
						.getMailSubject());
				crosssellWebDo.setMailImgUrl(crosssellBusinessDo
						.getMailImgUrl());
				crosselwebdomap.put(crosssellBusinessDo.getId().toString(),
						crosssellWebDo);
				// crosssellWebDoList.add(crosssellWebDo);
			}
		}
		/*
		 * if(fs != null){ Map map = fs.getSessionData(); if (map != null) { if
		 * ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
		 * isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN); }
		 * } }
		 */
		if (isloggedin != null && isloggedin) {
			List<PaymentTypeMasterWebDO> ptmws = new ArrayList<PaymentTypeMasterWebDO>();

			String key = personalDetailsBDO.getProductType() + "_"
					+ FCConstants.DEFAULT_AFFILIATE_ID;
			List<PaymentTypeMasterBusinessDO> ptmbs = FCConstants.paymentTypesForProductAffiliate
					.get(key);
			if (ptmbs != null) {
				for (PaymentTypeMasterBusinessDO ptmb : ptmbs) {
					PaymentTypeMasterWebDO ptmw = new PaymentTypeMasterWebDO();
					ptmw.setCode(ptmb.getCode());
					ptmw.setId(ptmb.getId());
					ptmw.setName(ptmb.getName());
					List<PaymentTypeOptionBusinessDO> ptobs = FCConstants.paymentTypesForRelation
							.get(ptmb.getRelationId());
					List<PaymentTypeOptionWebDO> ptows = new ArrayList<PaymentTypeOptionWebDO>();
					for (PaymentTypeOptionBusinessDO ptob : ptobs) {
						PaymentTypeOptionWebDO ptow = new PaymentTypeOptionWebDO();
						ptow.setCode(ptob.getCode());
						ptow.setDisplayImg(ptob.getDisplayImg());
						ptow.setDisplayName(ptob.getDisplayName());
						ptow.setId(ptob.getId());
						ptow.setImgUrl(ptob.getImgUrl());
						ptows.add(ptow);
					}
					ptmw.setOptions(ptows);
					ptmws.add(ptmw);
				}
			}
			// personalDetailsWebDO.setPaymentTypes(ptmws);
			// checkoutData.setPaymentData(ptmws);
		}
		// paydo.setAffiliateid(checkoutData.getCart().get)
		returnMap.put("totalAmount", totalAmount);
		//return totalAmount;
		return returnMap;
	}

	public boolean isAddCashPaymentResponse(String orderNo)  {
        ProductName primaryProduct = cartService.getPrimaryProduct(orderNo);
        /*
         * Don't initiate recharge in case of 
         * wallet cash product.
         */
        if (ProductName.WalletCash == primaryProduct) {
        	return true;
        }
        
        return false;
	}
	
	public CheckoutData handlePaymentResponse(Map<String, String> requestMap,
			String paymentGateway, Integer paymentTypeCode)
			throws FCPaymentPortalException, FCPaymentGatewayException,
			Exception {
		PaymentResponseBusinessDO paymentResponse = new PaymentResponseBusinessDO();
		paymentResponse.setRequestMap(requestMap);
		paymentResponse.setPg(paymentGateway);
		paymentResponse.setPaymentType(paymentTypeCode);

		IPaymentType paymentType = paymentTypeFactory.getPaymentType(paymentResponse.getPaymentType());
		paymentType.handleResponse(paymentResponse);
		paymentService.getUserDetailsByUsingOrderId(paymentResponse);
		Map<String, Object> dataMap = (Map<String, Object>) paymentResponse.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
		
		String orderNo = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
		
		if (this.isAddCashPaymentResponse(orderNo)) {
		    CheckoutData returnData =  new CheckoutData();
		    returnData.setOrderId(orderNo);
		    
		    return returnData;
		}
		
		logger.debug("Order no here is: " + orderNo);
		Boolean ispaymentsucces = Boolean.valueOf((String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));

		Map<String, Object> map = null;
		map = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderNo);

        CheckoutData checkoutData = getCheckoutData(String.valueOf(map.get("productType")));
 
		if (!ispaymentsucces) {
			checkoutData.setIsPaymentSuccessful(false);
		} else {
			String lookupId = (String) map.get("lookupId");
			logger.info("lookupId key value" + lookupId);
			logger.debug("Order no here is: " + orderNo);
			Float rechargeAmount = Float.parseFloat(map.get("amount")
					.toString());
			logger.debug("Recharge Amount here is: " + rechargeAmount);
			dataMap.put(PaymentConstants.AMOUNT, rechargeAmount);
			paymentResponse.getResultMap().put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, dataMap);
			OrderDetails orderDetails = doAnalytics(orderNo);
			checkoutData.setOrderDetails(orderDetails);
			doAnalytics(orderNo);
			Boolean isPaymentAlreadyAcknolodged = Boolean.valueOf((String)dataMap.get(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED));
			
			if (!isPaymentAlreadyAcknolodged) {
			    String mtxnId = (String) dataMap.get(PaymentConstants.MERCHANT_TXN_ID);
	            paymentAction.onPaymentSuccess(orderNo, mtxnId);
			}
			
			checkoutData.setIsPaymentSuccessful(true);
			checkoutData.setLookupId(lookupId);
			
			logger.debug("Finished handling payment response in mobile CheckoutService.handlePaymentResponse() for order id: " + orderNo);
		}

		PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(orderNo);
		checkoutData.setOrderId(orderNo);
		checkoutData.setTotalAmount(paymentPlan.getTotalAmount().getAmount().doubleValue());
		return checkoutData;
	}
	
	private OrderDetails doAnalytics(String orderNo) {
	    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
	    Map<String, Object> map = fs.getSessionData();
	    Integer userId = ((Integer) map.get(WebConstants.SESSION_USER_USERID));
	    OrderDetails orderDetails = analyticsService.findOrderDetails(orderNo, userId);
	    return orderDetails;
	}

	public Map<String, String> doFCBalancePay(String lookupId, String hash,
			String merchantId, String userIdStr) {
		logger.debug("Starting processing of wallet pay");
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		Map<String, Object> map = fs.getSessionData();
		Object userIdObject = map.get(WebConstants.SESSION_USER_USERID);

		Map<String, String> responseParams = new HashMap<String, String>();

		String merchantOrderId = "";

		try {
			if (hash != null) {
				merchantOrderId = merchantId;
				int userId = Integer.parseInt(userIdStr);
				if (WalletUtil.doesHashMatch(merchantOrderId, userId, hash)) {
					if (userId != (Integer) userIdObject) {
						responseParams.put(PaymentConstants.RESULT,
								String.valueOf(false));
						responseParams
								.put(PaymentConstants.MESSAGE,
										"user id passed is not the same as the logged in user");
					} else {
					    String orderId = orderIdDAO.getOrderIdForLookUpId(lookupId);
						String callerReference = orderId
								+ RechargeConstants.RECHARGE_TYPE;

						WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
						walletDebitRequest.setUserID(((Integer) userIdObject));
						walletDebitRequest.setMtxnId(merchantOrderId);
						walletDebitRequest
								.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
						walletDebitRequest.setCallerReference(callerReference);
						walletDebitRequest.setFundSource("");
						walletDebitRequest.setFundDestination(Wallet.FundDestination.RECHARGE.toString());
						walletDebitRequest.setMetadata("");

						String transactionId = walletService
								.pay(walletDebitRequest);// Todo
						logger.info("wallet_txn ID " + transactionId
								+ "merchantOrderId" + merchantOrderId);
						logger.info("wallet_txn ID " + transactionId
								+ "merchantOrderId" + merchantOrderId);
						responseParams.put(PaymentConstants.TRANSACTION_ID,
								transactionId);
						// todo - fix this, there must be a better way to do
						// this
						double transactionAmount = paymentOptionsHelper
								.getTransactionAmount(merchantOrderId);
						responseParams.put(PaymentConstants.AAMOUNT,
								String.valueOf(transactionAmount));
						responseParams.put(PaymentConstants.RESULT,
								String.valueOf(true));
						responseParams.put(PaymentConstants.MERCHANT_ORDER_ID,
								merchantOrderId);
					}
				} else {
					responseParams.put(PaymentConstants.RESULT,
							String.valueOf(false));
					responseParams.put(PaymentConstants.MESSAGE,
							"hash mismatch");
				}
			} else {
				responseParams.put(PaymentConstants.RESULT,
						String.valueOf(false));
				responseParams.put(PaymentConstants.MESSAGE,
						"hash key not found");
			}
		} catch (Exception e) {
			logger.error(e);
		}
		hash = WalletUtil.createHash(merchantOrderId, Integer.parseInt(userIdStr));
		responseParams.put(PaymentConstants.HASH, hash);
		logger.debug("Done processing of wallet pay");
		return responseParams;
	}
	
	public CheckoutData proceedWithCheckout(CheckoutData checkoutData, String lookUpId, Double totalAmount,  WalletEligibility walletEligibility, PaymentBreakup paymentBreakup, String serviceNumber, Float amount, String productType) {

		logger.debug("ProceedWithCheckout for service number :"+ serviceNumber);
		checkoutData = getCheckoutData(lookUpId, totalAmount,
				productType);

		checkoutData.setNumber(serviceNumber);

		checkoutData.getPaymentOptions().setWalletEligibility(walletEligibility);
		checkoutData.getPaymentOptions().setPaymentBreakup(paymentBreakup);
		
		if (walletEligibility.getEligibleForPartialPayment() && !((checkoutData.getProductType()).equals(FCConstants.WALLET_FUND_PRODUCT_TYPE))) {
			checkoutData.getPaymentOptions().setUsePartialPayment(Boolean.TRUE);
		} else {
			checkoutData.getPaymentOptions().setUsePartialPayment(Boolean.FALSE);
		}

		if (amount != null){
			checkoutData.setRechargeAmount(amount.intValue());
		}
		return checkoutData;
	}

    /*
     * Create the cart item for the lookupid.
     */
    public void createCartAndInitiateCheckout(TxnHomePageBusinessDO txnHomePageBusinessDO , TxnCrossSellBusinessDo bdo) {
        bdo.setLookupID(txnHomePageBusinessDO.getLookupId());
        logger.info("lookup id received in createCartandInitiateCheckout is " + txnHomePageBusinessDO.getLookupId());
        // Note that the coupons selected will always be null.
        bdo.setItemIdArray("");
        bdo.setType("coupon");
        bdo.setHomePageId(txnHomePageBusinessDO.getTxnHomePageId());
        bdo.setServiceNo(txnHomePageBusinessDO.getServiceNumber());
        bdo.setProType(txnHomePageBusinessDO.getProductType());
        bdo.setOperatorName(txnHomePageBusinessDO.getOperatorName());

        // Create the cart item entry.
        logger.info("About to go into saveAllTxnCrossSellService");
		txnCrossSellService.saveAllTxnCrossSellService(bdo);
        // initiate the checkout
        try {
            this.initiateCheckout(txnHomePageBusinessDO, /*CouponsList=*/"");
        } catch (PersonalDetailException e) {
            logger.error("TurboRecharge : Got PersonalDetailException", e);
        } catch (CartExpiredException e) {
            logger.error("TurboRecharge : Got CartExpiredException", e);
        }
    }
}
