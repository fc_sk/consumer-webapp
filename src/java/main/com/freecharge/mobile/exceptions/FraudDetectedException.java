package com.freecharge.mobile.exceptions;

public class FraudDetectedException extends Exception {
	private String customMsg;
	
	public String getCustomMsg() {
		return customMsg;
	}
	
	public void setCustomMsg(String customMsg) {
		this.customMsg = customMsg;
	}

	public FraudDetectedException(String customMsg) {
		this.customMsg = customMsg;
	}
}
