package com.freecharge.platform.metrics;

import java.util.Arrays;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.freecharge.common.framework.properties.FCProperties;

/**
 * A wrapper class around AWS cloud watch that
 * exposes FreeCharge specific utility methods
 * to gather metrics. Over a period of time
 * this could even replace {@link MetricsStatsDClient}.
 * @author arun
 *
 */
@Service
public class CloudWatchMetricService {
    @Autowired
    private AmazonCloudWatch cloudWatch;
    
    @Autowired
    private FCProperties fcProperties;
    
    public void recordDenominationMetric(String product, String operatorId, String circleId, String rechargePlan, String metricName) {
        Dimension od = new Dimension();
        od.setName("operator");
        od.setValue(operatorId);
        
        Dimension cd = new Dimension();
        cd.setName("circle");
        cd.setValue(circleId);
        
        Dimension pd = new Dimension();
        pd.setName("product");
        pd.setValue(product);
        
        Dimension ptd = new Dimension();
        ptd.setName("planType");
        ptd.setValue(rechargePlan);

        MetricDatum metric = new MetricDatum();
        metric.setDimensions(Arrays.asList(od, cd, pd, ptd));
        metric.setMetricName(metricName);
        metric.setValue(1d);
        metric.setUnit(StandardUnit.Count);
        metric.setTimestamp(new DateTime().toDate());
        
        PutMetricDataRequest putRequest = new PutMetricDataRequest();
        putRequest.setNamespace(fcProperties.getMode());
        putRequest.setMetricData(Arrays.asList(metric));
        
        cloudWatch.putMetricData(putRequest);
    }
    
    public void recordLatency(String serviceName, String metricName, Long latency) {
        String cwMetricName = serviceName + ":" + metricName;
        
        MetricDatum metric = new MetricDatum();
        metric.setMetricName(cwMetricName);
        metric.setValue(latency.doubleValue());
        metric.setUnit(StandardUnit.Milliseconds);
        metric.setTimestamp(new DateTime().toDate());
        
        PutMetricDataRequest putRequest = new PutMetricDataRequest();
        putRequest.setNamespace(fcProperties.getMode());
        putRequest.setMetricData(Arrays.asList(metric));
        
        cloudWatch.putMetricData(putRequest);
    }
}
