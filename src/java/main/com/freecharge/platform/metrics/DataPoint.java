package com.freecharge.platform.metrics;

import java.io.Serializable;
import java.util.Date;

/**
 * An immutable object that contains a metric value at
 * a specific instance in time.
 * @author arun
 *
 */
public class DataPoint implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private final Double value;
    private final Date time;
    
    public DataPoint(Date time, Double value) {
        this.time = new Date(time.getTime());
        this.value = new Double(value.doubleValue());
    }
    
    public Date getTime() {
        return time;
    }
    
    public Double getValue() {
        return value;
    }
}
