package com.freecharge.platform.metrics;

import java.util.Arrays;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.freecharge.common.framework.properties.FCProperties;

/**
 * A {@link MetricsClient} that publishes metrics to 
 * AWS CloudWatch {@link http://aws.amazon.com/cloudwatch/}
 * @author arun
 *
 */
public class CloudWatchMetricsClient {
    
    @Autowired
    AmazonCloudWatch cloudWatch;

    @Autowired
    private FCProperties properties; 

    
    /*public void recordCount(String serviceName, String metricName, Integer count) {
        String finalMetricName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-");
        
        MetricDatum metric = new MetricDatum();
        metric.setMetricName(finalMetricName);
        metric.setValue(count.doubleValue());
        metric.setUnit(StandardUnit.Count);
        metric.setTimestamp(new DateTime().toDate());
        
        PutMetricDataRequest putRequest = new PutMetricDataRequest();
        putRequest.setNamespace(properties.getMode());
        putRequest.setMetricData(Arrays.asList(metric));
        
        cloudWatch.putMetricData(putRequest);
    }

    public void recordLatency(String serviceName, String metricName, Long latency) {
        String finalMetricName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-");
        
        MetricDatum metric = new MetricDatum();
        metric.setMetricName(finalMetricName);
        metric.setValue(latency.doubleValue());
        metric.setUnit(StandardUnit.Milliseconds);
        metric.setTimestamp(new DateTime().toDate());
        
        PutMetricDataRequest putRequest = new PutMetricDataRequest();
        putRequest.setNamespace(properties.getMode());
        putRequest.setMetricData(Arrays.asList(metric));
        
        cloudWatch.putMetricData(putRequest);
    }

    public void recordEvent(String serviceName, String metricName, String eventName) {
        String finalMetricName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-") + "." + eventName.replaceAll("\\s+", "-");
                
        MetricDatum metric = new MetricDatum();
        metric.setMetricName(finalMetricName);
        metric.setValue(new Double(1));
        metric.setUnit(StandardUnit.Count);
        metric.setTimestamp(new DateTime().toDate());
        
        PutMetricDataRequest putRequest = new PutMetricDataRequest();
        putRequest.setNamespace(properties.getMode());
        putRequest.setMetricData(Arrays.asList(metric));
        
        cloudWatch.putMetricData(putRequest);
    }

    public void timeStampEvent(String key, String eventName) {
        throw new RuntimeException("Not Implemented");
    }

    public void timeStampEvent(String key, String eventName, Date timeStamp) {
        throw new RuntimeException("Not Implemented");
    }

    public Date retrieveTimeStamp(String key, String eventName) {
        throw new RuntimeException("Not Implemented");
    }

    public void recordLatency(String serviceName, String metricName, String key, String eventName) {
        throw new RuntimeException("Not Implemented");
    }*/

}
