package com.freecharge.platform.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;

/**
 * A cache to hold timestamp values with expiry. This cache will be internally used by {@link MetricsStatsDClient} 
 * to implement latency between two events that take place in different processes/threads.
 * @author arun
 *
 */
@Component
class MetricsCache extends RedisCacheManager {
    @Autowired
    private RedisTemplate<String, Object> metricsCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.metricsCacheTemplate;
    }
}
