package com.freecharge.platform.metrics;

import java.util.Date;


/**
 * This class exposes APIs to record different types of metrics. For now
 * developer can record Count, Latency and Event.  
 * @author arun
 *
 */
public interface MetricsClient {
    
    public interface EventName {
        public final String Proceed = "Proceed";
        public final String PayAttempt = "PayAttempt";
        public final String PaySuccess = "PaySuccess";
        public final String RechargeAttempt = "RechargeAttempt";
        public final String RechargePending = "RechargePending";
        public final String RechargeSuccess = "RechargeSuccess";
        public final String RechargeFailure = "RechargeFailure";
        public final String RefundComplete = "RefundComplete";
        public final String FulfillmentComplete = "FulfillmentComplete";
        public final String BillPaymentAttempt = "BillPaymentAttempt";
    }
    
    /**
     * Use this to count the number of occurrences of an event. E.g., number of
     * retries needed to get a successful http response, number of payment 
     * attempts. E.g., If you want to record the number of recharge attempts made for some order ID then use
     * recordCount("RechargeService", "AirtelRetry", 3);
     *   
     * @param serviceName A string to group related metrics under one name. E.g., "PaymentService", "RechargeService"
     * @param metricName Name of the metric. E.g., "PayUSuccess", "EuronetSuccess".
     * @param count Metric value.
     */
    public void recordCount(String serviceName, String metricName, Integer count);
    
    /**
     * Use this to record the latency of an event. E.g., latency of aggregator
     * APIs, latency of fetching user profile from DB. E.g., if you want to record the latecy of Airtel Recharges from
     * Euronet then use recordLatency("RechargeService", "Euronet.Airtel", 12000)
     * @param serviceName A string to group related metrics under one name. E.g., "PaymentService", "RechargeService"
     * @param metricName Name of the metric. E.g., "PayUSuccessLatency", "EuronetSuccessLatency".
     * @param latency Latency in millisecond. 
     */
    public void recordLatency(String serviceName, String metricName, Long latency);
    
    /**
     * Use this to record the occurrence of an event. E.g., To record the fact that
     * an HTTP get responded 200, some aggregator responding with "Under Progress". E.g., if you would like to
     * count various types of response codes obtained from Euronet then use recordEvent("RechargeService", "Euronet" responseCode.toString());
     * @param ServiceName A string to group related metrics under one name. E.g., "PaymentService", "RechargeService"
     * @param metricName Name of the metric. E.g., "PayU", "Euronet".
     * @param eventName The event name you are interested in. "UnderProcess", "Pending".
     */
    public void recordEvent(String ServiceName, String metricName, String eventName);
    
    /**
     * Same as the other overloaded method that uses current timestamp as the event time.
     * @param eventName
     */
    public void timeStampEvent(String key, String eventName);
    
    /**
     * This is an utility method to record the timestamp of an event in a temporary store with timeout of 2 hours. One can later
     * retrieve the timestamp of a specific event for a given key. This is to help record time lag between events that take place
     * in different processes and are spread over time. E.g., If one were to record the latency between payment and recharge success
     * for a given order ID it's not possible without going to DB. Using timestamping this can be achieved in following manner.
     * 1. In payment success code block call "timeStampEvent(orderId, "paymentSuccess");
     * 2. In recharge success code block do call ts = retrieveTimeStamp(orderId, "paymentSuccess")
     * 3. Now call recordLatency("userFlow", "RechargeSuccess", currentTimeStamp - ts);
     * @param eventName
     */
    public void timeStampEvent(String key, String eventName, Date timeStamp);
    
    /**
     * Retrieve the timestamp of an event for a given key.
     * @param key
     * @param eventName
     * @return
     */
    public Date retrieveTimeStamp(String key, String eventName);
    
    /**
     * Retrives the timestamp of an event for a key and record the latency by calculating time difference between that timestamp and 
     * the current timestamp. This method silently returns in case it's unable to retrieve the timestamp for the eventName.
     * @param serviceName
     * @param metricName
     * @param key
     * @param eventName
     */
    public void recordLatency(String serviceName, String metricName, String key, String eventName);
}
