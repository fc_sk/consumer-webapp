package com.freecharge.platform.metrics.collector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

import com.freecharge.platform.metrics.DataPoint;

import flexjson.JSONDeserializer;

/**
 * This is a metrics collector that pulls data from
 * graphite for a given expression.
 * @author arun
 *
 */
public class GraphiteMetricsCollector {
    static JSONDeserializer<List<Map<String, Object>> > deserializer = new JSONDeserializer<>();
    
    /**
     * Retrieves a list of {@link DataPoint} given a metric string and duration string.
     * @param metricString
     * @param from This is duration string; could be of the form "-60min"
     * @return
     * @throws IOException 
     * @throws HttpException 
     */
    public List<DataPoint> retrieve(String metricString, String from) {
    	HttpClient client = new HttpClient();        
        GetMethod graphiteCall = new GetMethod("http://stats1.sg1.freecharge.com:8083/render");
        

        NameValuePair[] statusCheckSubmitData = {
                new NameValuePair("target", metricString),
                new NameValuePair("format", "json"),
                new NameValuePair("from", from)
            };
        
        graphiteCall.setQueryString(statusCheckSubmitData);
        
        try {
            client.executeMethod(graphiteCall);
            String responseString = graphiteCall.getResponseBodyAsString();

            List<Map<String, Object>> responseArray =  deserializer.deserialize(responseString);

            List<List<Object>> dataPointObjects = new ArrayList<>();
            
            for (Map<String, Object> responseElement : responseArray) {
                for (Entry<String, Object> responseEntry : responseElement.entrySet()) {
                    if (responseEntry.getKey().equals("datapoints")) {
                        dataPointObjects = (List<List<Object>>) responseEntry.getValue();
                    }
                }
            }
            
            List<DataPoint> dataPoints = new ArrayList<>();
            
            for (List<Object> dataPoint : dataPointObjects) {
                Double metricValue = (Double) dataPoint.get(0);
                Integer timeStamp = (Integer) dataPoint.get(1);
                
                if (metricValue != null && timeStamp != null) {
                    dataPoints.add(new DataPoint(new Date(timeStamp.longValue() * 1000), metricValue));
                }
            }
            
            return dataPoints;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Retrieves a Map of Target and the corresponding {@link DataPoint}s for a given metric string and duration string.
     * @param metricString
     * @param from This is duration string; could be of the form "-60min"
     * @return
     * @throws IOException 
     * @throws HttpException 
     */
    public Map< String,List<DataPoint>>  retrieveMapofTargetandDataPoints(String metricString, String from) {
    	HttpClient client = new HttpClient();   
        GetMethod graphiteCall = new GetMethod("http://stats1.freecharge.in:8083/render");
        

        NameValuePair[] statusCheckSubmitData = {
                new NameValuePair("target", metricString),
                new NameValuePair("format", "json"),
                new NameValuePair("from", from)
            };
        
        graphiteCall.setQueryString(statusCheckSubmitData);
        
        try {
        	
            client.executeMethod(graphiteCall);
            String responseString = graphiteCall.getResponseBodyAsString();

            List<Map<String, Object>> responseArray =  deserializer.deserialize(responseString);

            List<List<Object>> dataPointObjects = new ArrayList<>();
            String targetKey = "";
            Map< String,List<DataPoint>>  targetDatapointsMap = new HashMap<String,List<DataPoint>>();
                        
            for (Map<String, Object> responseElement : responseArray) {
            	System.out.println(responseElement.keySet());
                for (Entry<String, Object> responseEntry : responseElement.entrySet()) {
                	targetKey = responseEntry.getKey().equals("target")? (String)responseEntry.getValue() : ""; 
                	if (responseEntry.getKey().equals("datapoints")) {
                        dataPointObjects = (List<List<Object>>) responseEntry.getValue();
                    }                	
                }
                if(!targetKey.equals("") && !targetDatapointsMap.containsKey(targetKey)){
                	List<DataPoint> dataPoints = new ArrayList<>();                    
                    for (List<Object> dataPoint : dataPointObjects) {
                        Double metricValue = (Double) dataPoint.get(0);
                        Integer timeStamp = (Integer) dataPoint.get(1);
                        
                        if (metricValue != null && timeStamp != null) {
                            dataPoints.add(new DataPoint(new Date(timeStamp.longValue() * 1000), metricValue));
                        }
                    }
                    targetDatapointsMap.put(targetKey,dataPoints);
            	}
            }
            return targetDatapointsMap;
        } catch (IOException e) {
//            TODO: Log error
        	e.printStackTrace();
        }
        
        return new HashMap<>();
    }
    
    public static void main(String[] args) throws Exception {
        GraphiteMetricsCollector gmc = new GraphiteMetricsCollector();
        
        String failureMetricString = "summarize(sumSeries(" +
                "stats.counts.application.consumer1_freecharge_in.Recharge.euronet.Vodafone.Failure_increment," +
                "stats.counts.application.consumer2_freecharge_in.Recharge.euronet.Vodafone.Failure_increment), \"5min\")";
        
        String from = "-1400min";
        
        List<DataPoint> dataPoints = gmc.retrieve(failureMetricString, from);
        
        for (DataPoint dataPoint : dataPoints) {
            System.out.println(dataPoint.getTime() + " : " + dataPoint.getValue());
        }
    }
}
