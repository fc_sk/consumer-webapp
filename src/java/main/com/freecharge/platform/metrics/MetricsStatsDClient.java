package com.freecharge.platform.metrics;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.freecharge.common.framework.logging.LoggingFactory;

import com.timgroup.statsd.StatsDClient;


/**
 * A {@link MetricsClient} that sends the metrics to the statsd daemon, which forwards
 * it to the Graphite/Carbon metrics server.
**/

public class MetricsStatsDClient implements MetricsClient { 
      
    private final int METRIC_TIMEOUT_HOURS = 5;
    
    private static Logger logger = LoggingFactory.getLogger(MetricsStatsDClient.class);

    @Autowired
    private StatsDClient statsDClient;
    
    @Autowired
    private MetricsCache metricsCache;
    
    @Override
    public void recordCount(String serviceName, String metricName, Integer count) {
        String finalMetricName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-") + "_count";
        statsDClient.count(finalMetricName, count);
    }

    @Override
    public void recordLatency(String serviceName, String metricName, Long latency) {
        String finalMerticName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-") + "_timer";
        statsDClient.recordExecutionTime(finalMerticName, latency);
    }

    @Override
    public void recordEvent(String serviceName, String metricName, String eventName) {
        String finalMetricName = serviceName.replaceAll("\\s+", "-") + "." + metricName.replaceAll("\\s+", "-") + "." + eventName.replaceAll("\\s+", "-") + "_increment";
        statsDClient.increment(finalMetricName);
    }

    @Override
    public void timeStampEvent(String key, String eventName) {
        timeStampEvent(key, eventName, new Date());
    }

    @Override
    public void timeStampEvent(String key, String eventName, Date timeStamp) {
        metricsCache.set(key + "_" + eventName, timeStamp, METRIC_TIMEOUT_HOURS, TimeUnit.HOURS);
    }

    @Override
    public Date retrieveTimeStamp(String key, String eventName) {
        return (Date) metricsCache.get(key + "_" + eventName);
    }

    @Override
    public void recordLatency(String serviceName, String metricName, String key, String eventName) {
        Date ts = null;
        try {
            ts = this.retrieveTimeStamp(key, eventName);
        } catch (Exception e) {
            logger.error("Caught exception on recording latency", e);
        }
        if (ts != null) {
            this.recordLatency(serviceName, metricName, System.currentTimeMillis() - ts.getTime());
        }
    }
}
