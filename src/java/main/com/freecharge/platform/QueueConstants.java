package com.freecharge.platform;

public interface QueueConstants {
    public final String ENTRY_TIME = "entryTime";
    public final String ORDER_ID = "orderId";
    public final String STATUS_CHECK_TYPE = "statusCheckType";
}
