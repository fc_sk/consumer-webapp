package com.freecharge.platform.cache;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.FactoryBean;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class DenominationCacheFactory implements FactoryBean<Cache<String, Boolean>> {
    private int expireAfterWriteMinutes;
    private int maximumSize;

    @Override
    public Cache<String, Boolean> getObject() throws Exception {
        return CacheBuilder.newBuilder().maximumSize(maximumSize).expireAfterWrite(expireAfterWriteMinutes, TimeUnit.MINUTES).build();
    }

    @Override
    public Class<?> getObjectType() {
        return Cache.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
    
    public void setExpireAfterWriteMinutes(int expireAfterWriteMinutes) {
        this.expireAfterWriteMinutes = expireAfterWriteMinutes;
    }
    
    public void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }
}
