package com.freecharge.platform.cache;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.rest.recharge.RechargeResourceController;
import com.google.common.cache.Cache;

/**
 * An in memory cache to be used by {@link RechargeResourceController}.
 * @author arun
 *
 */
@Service
public class RechargeResourceCache {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RechargeMetricService rechargeMetricService;
    
    @Autowired
    private OperatorAlertService alertService;
    
    @Autowired
    @Qualifier("denominationCache")
    private Cache<String, Boolean> denominationCache;
    
    private final String AIRTEL_COUNT_KEY_PREFIX = "ATCK_";
    private final String AIRTEL_PERCENT_KEY_PREFIX = "ATPK_";
    private final String BSNL_KEY_PREFIX = "BSNLK_";
    private final String GENERIC_KEY_PREFIX = "GKP_";
    private final String INVALID_DENOMINATION_NUMBER_KEY_PREFIX = "IDN_";
    private final String PERMANENT_FAILURE_NUMBER_KEY_PREFIX = "PFN_";
    private final String EG_FAILURE_NUMBER_KEY_PREFIX = "EGN_";
    
    public boolean isAirtelInvalidDenomination(final String operator, final String circle, final Double amount) {
        String cacheKey = AIRTEL_COUNT_KEY_PREFIX + operator + "_" + circle + "_" + String.valueOf(amount);
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isAirtelInvalidDenomination(operator, circle, amount);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
    
    public boolean isBsnlInvalidDenomination(final String operator, final String circle, final Double amount, final String rechargePlanType) { 
        String cacheKey = BSNL_KEY_PREFIX + operator + "_" + circle + "_" + String.valueOf(amount) + "_" + rechargePlanType;
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isBsnlInvalidDenomination(operator, circle, amount, rechargePlanType);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
    
    public boolean isAirtelInvalidDenominationCheck(final String operator, final String circle, final Double amount) {
        String cacheKey = AIRTEL_PERCENT_KEY_PREFIX + operator + "_" + circle + "_" + String.valueOf(amount);
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isAirtelInvalidDenominationCheck(operator, circle, amount);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
    
    public Boolean isInvalidDenomination(final String operatorId, final String circleId, final Double amount, final String rechargeType, final String productType) {
        String cacheKey = GENERIC_KEY_PREFIX + operatorId + "_" + circleId + "_" + String.valueOf(amount) + "_" + rechargeType + "_" + productType;
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isInvalidDenomination(operatorId, circleId, amount, rechargeType, productType);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
    
    public boolean isInvalidDenominationNumber(final String productType, final String serviceNumber, final String operator, final Double amount, final String rechargeType) {
        String cacheKey = INVALID_DENOMINATION_NUMBER_KEY_PREFIX + productType + "_" + serviceNumber + "_" + operator + "_" + String.valueOf(amount) + "_" + rechargeType;
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isInvalidDenominationNumber(productType, serviceNumber, operator, amount, rechargeType);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
        
    }
    
    public boolean isPermanentFailure(final String productType, final String operator, final String serviceNumber) {
        String cacheKey = PERMANENT_FAILURE_NUMBER_KEY_PREFIX + productType + "_" + operator + "_" + serviceNumber;

        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isPermanentFailure(productType, operator, serviceNumber);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
    
    public boolean isEGFailureNumber(final String serviceNumber, final String operator, final Double amount) {
        String cacheKey = EG_FAILURE_NUMBER_KEY_PREFIX + serviceNumber + "_" + operator + "_" + String.valueOf(amount);
        
        try {
            Boolean returnValue = denominationCache.get(cacheKey, new Callable<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    return rechargeMetricService.isEGFailureNumber(serviceNumber, operator, amount);
                }
            });
            
            return returnValue;
        } catch (ExecutionException ee) {
            logger.error("Ignoring exception ", ee);
            return false;
        }
    }
}
