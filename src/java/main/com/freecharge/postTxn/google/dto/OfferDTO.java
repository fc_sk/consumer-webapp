package com.freecharge.postTxn.google.dto;

import java.io.Serializable;

public class OfferDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String code;
	private String discountType;
	private String rebateType;
	private String benefit;

	private String benefitCap;
	private String userType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getRebateType() {
		return rebateType;
	}

	public void setRebateType(String rebateType) {
		this.rebateType = rebateType;
	}

	public String getBenefit() {
		return benefit;
	}

	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}

	public String getBenefitCap() {
		return benefitCap;
	}

	public void setBenefitCap(String benefitCap) {
		this.benefitCap = benefitCap;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OfferDTO [code=").append(code).append(", discountType=").append(discountType)
				.append(", rebateType=").append(rebateType).append(", benefit=").append(benefit).append(", benefitCap=")
				.append(benefitCap).append(", userType=").append(userType).append("]");
		return builder.toString();
	}

}
