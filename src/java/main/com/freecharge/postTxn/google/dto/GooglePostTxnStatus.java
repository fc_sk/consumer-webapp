package com.freecharge.postTxn.google.dto;

public enum GooglePostTxnStatus {
	SUCCESSFUL,CANCELLED,PENDING,FAILED,MODIFIED,APP_INSTALL
}
