package com.freecharge.postTxn.google.dto;

import java.io.Serializable;

public class MobileRechargeDetailDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String errorCode;
    private PlanParamDTO planParams;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public PlanParamDTO getPlanParams() {
        return planParams;
    }

    public void setPlanParams(PlanParamDTO planParams) {
        this.planParams = planParams;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MobileRechargeDetailDTO [errorCode=").append(errorCode).append(", planParams=")
                .append(planParams).append("]");
        return builder.toString();
    }


}
