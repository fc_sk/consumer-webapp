package com.freecharge.postTxn.google.dto;

import org.apache.commons.lang.StringUtils;

public enum GoogleSurface {

    ANDROID_MSITE("2"), ANDROID_APP("3");

    GoogleSurface(String surfaceCode) {
        this.surfaceCode = surfaceCode;
    }

    private String surfaceCode;

    public String getSurfaceCode() {
        return surfaceCode;
    }


    public static String getSurfaceByChannel(String channel) {

        if (StringUtils.isEmpty(channel)) {
            return null;
        }

        switch (channel) {

            case "2":
                return ANDROID_MSITE.name();

            case "3":
                return ANDROID_APP.name();

            default:
                return null;

        }
    }

}
