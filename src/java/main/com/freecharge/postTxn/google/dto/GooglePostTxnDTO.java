package com.freecharge.postTxn.google.dto;

import java.io.Serializable;

public class GooglePostTxnDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String status;
    private String createTime;
    private String surface;
    private MobileRechargeDetailDTO mobileRechargeDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MobileRechargeDetailDTO getMobileRechargeDetails() {
        return mobileRechargeDetails;
    }

    public void setMobileRechargeDetails(MobileRechargeDetailDTO mobileRechargeDetails) {
        this.mobileRechargeDetails = mobileRechargeDetails;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GooglePostTxnDTO [status=").append(status).append(", createTime=").append(createTime).append(", surface=").append(surface)
                .append(", mobileRechargeDetails=").append(mobileRechargeDetails).append("]");
        return builder.toString();
    }

}
