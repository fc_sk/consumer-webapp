package com.freecharge.postTxn.google.dto;

import java.io.Serializable;

public class GoogleInstallRefererRequest implements Serializable {


    private GooglePostTxnStatus status;

    private String referenceId;


    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }


    public GooglePostTxnStatus getStatus() {
        return status;
    }

    public void setStatus(GooglePostTxnStatus status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return new StringBuilder().append("GoogleInstallRefererRequest [status=").append(status)
                .append(", referenceId=").append(referenceId).append("]").toString();
    }
}
