package com.freecharge.postTxn.google.dto;

import java.io.Serializable;

public class PlanParamDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String planId;
	private String operatorId;
	private String subregionId;
	private String amount;

	private String price;
	private String currency;
	private String phone;
	private OfferDTO offer;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getSubregionId() {
		return subregionId;
	}

	public void setSubregionId(String subregionId) {
		this.subregionId = subregionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public OfferDTO getOffer() {
		return offer;
	}

	public void setOffer(OfferDTO offer) {
		this.offer = offer;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PlanParamDTO [planId=").append(planId).append(", operatorId=").append(operatorId)
				.append(", subregionId=").append(subregionId).append(", amount=").append(amount).append(", price=")
				.append(price).append(", currency=").append(currency).append(", phone=").append(phone)
				.append(", offer=").append(offer).append("]");
		return builder.toString();
	}

}
