package com.freecharge.postTxn.google.response;

import java.io.Serializable;

public class GoogleInstallRefererResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private GoogleReError error;

    private GoogleReResponse data;

    public GoogleReError getError() {
        return error;
    }

    public void setError(GoogleReError error) {
        this.error = error;
    }

    public GoogleReResponse getData() {
        return data;
    }

    public void setData(GoogleReResponse data) {
        this.data = data;
    }


}
