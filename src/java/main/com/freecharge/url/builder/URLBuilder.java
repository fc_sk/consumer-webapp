package com.freecharge.url.builder;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.AppContextManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;

public class URLBuilder {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private UserServiceProxy userServiceProxy;

    private String domain;

    private String resource;

    private Boolean secure;

    private Map<String, String> queryParams;

    /**
     * Starting point for creating url.
     * @param domain
     * @param resource
     * @param secure
     * @param mappings
     * @param fixedParams
     * @param formData
     * @param extraParams
     * @param user
     */
    public URLBuilder(String domain, String resource, Boolean secure, Map<String, Map<String, Object>> mappings,
                      Map<String, Object> fixedParams, Map<String, String> formData,
                      Map<String, Object> extraParams, Users user) {
        //Setting domain, resource and secure
        this.domain = domain;
        this.secure = secure;
        this.resource = resource;

        //Set userService
        ApplicationContext ctx = AppContextManager.getAppContext();
        this.userServiceProxy = (UserServiceProxy) ctx.getBean(UserServiceProxy.class);

        //Initialize queryParams
        this.queryParams = new HashMap<>();

        //Getting default value userMap
        Map<String, String> userMap = this.prepareAndGetUserMap(user);
        Boolean hasSlash = resource.charAt(resource.length()-1) == '/';

        //Iterating over mappings to prepare queryParams.
        //Mapping will have key[our identifier] and a Map[this will have client identifier and isPartOfResource value]
        //isPartOfResource makes sense only for non user attributes and hence used in case of any extraParams
        //such as {uniqueId: {key: 'uuid', isPartOfResource: true}}
        if (mappings!=null && mappings.size()>0){
            for (Entry<String, Map<String, Object>> entry : mappings.entrySet()){
                Map<String, Object> paramMap = entry.getValue();
                String key = (String) paramMap.get("key");
                if (userMap.containsKey(entry.getKey())){
                    queryParams.put(key, userMap.get(entry.getKey()));
                }else if (extraParams!=null && extraParams.containsKey(entry.getKey())) {
                    Boolean isPartOfResource = Boolean.valueOf((String) paramMap.get("isPartOfResource"));
                    if (isPartOfResource){
                        this.resource += hasSlash?String.valueOf(extraParams.get(entry.getKey())):"/"+
                                extraParams.get(entry.getKey());
                    }else {
                        String valPrefix = paramMap.get("prefix")!=null?(String) paramMap.get("prefix"):"";
                        queryParams.put(key, valPrefix+String.valueOf(extraParams.get(entry.getKey())));
                    }
                }
            }
        }

        //Add fixed params to query params. These are constant values and will remain same for each call
        if (fixedParams!=null && fixedParams.size()>0) {
            for (Entry<String, Object> entry : fixedParams.entrySet()){
                this.queryParams.put(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }

        //Check and add formData. Any keys in mappings having same name  as in formData
        // will be overridden with those of formData
        if (formData!=null && formData.size()>0) {
            //Clean formData
            formData.remove("deal_id");
            formData.remove("activity");
            formData.remove("action_id");
            formData.remove("undefined");
            formData.remove("ar_thread_id");

            //Add form values
            this.queryParams.putAll(formData);
        }
    }

    public URLBuilder(String domain, String resource, Boolean secure, Map<String, Map<String, Object>> mappings,
                      Map<String, Object> fixedParams, Map<String, Object> extraParams, Users user) {
        this(domain, resource, secure, mappings, fixedParams, null, extraParams, user);
    }

    /**
     * This method prepares the userMap with default values fetched from userProfile.
     * @param user
     * @return
     */
    private Map<String, String> prepareAndGetUserMap(Users user){
        Map<String, String> userMap = new HashMap<>();
        UserProfile profile = this.userServiceProxy.getUserProfile(user.getUserId());
        userMap.put(URLConstants.EMAIL, user.getEmail());
        String [] name = profile.getFirstName().split(" ");
        userMap.put(URLConstants.FIRST_NAME, name[0]);
        userMap.put(URLConstants.LAST_NAME, name.length>1?name[1]:"");
        userMap.put(URLConstants.MOBILE_NO, user.getMobileNo());
        userMap.put(URLConstants.DOB, user.getDob()!=null?user.getDob().toString():"");
        userMap.put(URLConstants.GENDER, profile.getGender());
        return userMap;
    }

    public String getUri(){
        return (this.secure?"https":"http") + "://" + this.domain +
                (this.resource.charAt(0)=='/'?this.resource:'/'+this.resource);
    }

    public String getFullUrl(){
        String uriPart = getUri();
        String queryString = "";
        for (String key : this.queryParams.keySet()){
            String value = this.queryParams.get(key);
            try {
                value = URLEncoder.encode(this.queryParams.get(key), "UTF-8");
            }catch (UnsupportedEncodingException ue) {
                logger.warn("Error encoding to utf-8 for value: "+this.queryParams.get(key));
            }
            queryString+="&"+key+"="+value;
        }
        return uriPart + "?" + (queryString.length()>1?queryString.substring(1):queryString);
    }

    public Map<String, String> getQueryParams(){
        return this.queryParams;
    }
}
