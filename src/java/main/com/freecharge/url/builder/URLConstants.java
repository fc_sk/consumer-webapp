package com.freecharge.url.builder;

public class URLConstants { 

	public static final String SLASH = "/";
	public static final String HTTP = "http://";
	public static final String HTTPS = "https://";
	public static final String WWW = "www";
	public static final String DOT = ".";
	public static final String QUESTION = "?";
	public static final String AMP = "&";
	public static final String EQUAL_TO = "=";
	public static final String OPEN_SQ_BR = "[";
	public static final String CLOSE_SQ_BR = "]";
	public static final String COMMA = ",";	
	
	//from users table
	public static final String EMAIL = "email";
	public static final String MOBILE_NO = "mobile_no";
	public static final String DOB = "dob";
	public static final String GENDER = "morf";
	
	//user profile table
	public static final String ADDRESS = "address1";
	public static final String LANDMARK = "landmark";
	public static final String AREA = "area";
	public static final String STREET = "street";
	public static final String POSTAL_CODE = "postal_code";
	public static final String CITYID = "city";
	public static final String CITY_NAME = "city_name";
	public static final String FIRST_NAME = "first_name";
	public static final String TITLE = "title";
	public static final String LAST_NAME = "last_name";
	
	public static final String URL_PARAMS_MAP = "urlParamMap";
	
}
