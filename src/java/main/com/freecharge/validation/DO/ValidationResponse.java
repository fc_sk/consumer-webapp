/**
 * 
 */
package com.freecharge.validation.DO;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class holds the values for Euronet Validation Response for Number
 * Validations
 * 
 * @author fcaa17922
 *
 */
public class ValidationResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ResponseCode")
	public String responseCode;

	@JsonProperty("ResponseMessage")
	public String responseMessage;

	@JsonProperty("CustomerType")
	public String customerType;

	@JsonProperty("SPCode")
	public String sPCode;

	@JsonProperty("SSPCode")
	public String sSPCode;

	@JsonProperty("ServiceType")
	public String serviceType;

	public ValidationResponse(String responseCode, String responseMessage) {
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public ValidationResponse() {

	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	@JsonProperty("ResponseCode")
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	@JsonProperty("ResponseMessage")
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            the customerType to set
	 */
	@JsonProperty("CustomerType")
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return the sPCode
	 */
	public String getsPCode() {
		return sPCode;
	}

	/**
	 * @param sPCode
	 *            the sPCode to set
	 */
	@JsonProperty("SPCode")
	public void setsPCode(String sPCode) {
		this.sPCode = sPCode;
	}

	/**
	 * @return the sSPCode
	 */
	public String getsSPCode() {
		return sSPCode;
	}

	/**
	 * @param sSPCode
	 *            the sSPCode to set
	 */
	@JsonProperty("SSPCode")
	public void setsSPCode(String sSPCode) {
		this.sSPCode = sSPCode;
	}

	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * @param serviceType
	 *            the serviceType to set
	 */
	@JsonProperty("ServiceType")
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ValidationResponse [responseCode=").append(responseCode).append(", responseMessage=")
				.append(responseMessage).append(", customerType=").append(customerType).append(", sPCode=")
				.append(sPCode).append(", sSPCode=").append(sSPCode).append(", serviceType=").append(serviceType)
				.append("]");
		return builder.toString();
	}

}
