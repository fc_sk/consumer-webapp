/**
 * 
 */
package com.freecharge.validation.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.validation.DO.ValidationResponse;
import com.freecharge.validation.dto.EuronetJioValidationRequest;

/**
 * This service class is used to validate number from Euronet
 * 
 * @author fcaa17922
 *
 */
public class EuronetValidationService extends AggregatorService implements ValidationInterface {

	private static Logger logger = LoggingFactory.getLogger(EuronetValidationService.class);
	
	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private AppConfigService appConfigService;

	private HttpConnectionManagerParams connectionManagerConfig;
	private String jioNumberValidationURL;
	private int validationTimeout;
	private HostConfiguration euronetHostConfig;
	
	public void init() {
		logger.info("Calling init method euronet service");
		euronetHostConfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HOST), 443, "https");
	}
	
	public HostConfiguration getEuronetHostConfig() {
		return euronetHostConfig;
	}

	public void setEuronetHostConfig(HostConfiguration euronetHostConfig) {
		this.euronetHostConfig = euronetHostConfig;
	}
	
	public HttpConnectionManagerParams getConnectionManagerConfig() {
		return connectionManagerConfig;
	}

	public void setConnectionManagerConfig(HttpConnectionManagerParams connectionManagerConfig) {
		this.connectionManagerConfig = connectionManagerConfig;
	}

	public String getJioNumberValidationURL() {
		return jioNumberValidationURL;
	}

	public void setJioNumberValidationURL(String jioNumberValidationURL) {
		this.jioNumberValidationURL = jioNumberValidationURL;
	}

	public int getValidationTimeout() {
		return validationTimeout;
	}

	public void setValidationTimeout(int validationTimeout) {
		this.validationTimeout = validationTimeout;
	}

	@Override
	public ValidationResponse validateNumber(String consumerNo, String amount) {

		logger.info("Posting validation request for : " + consumerNo + " to Euronet with Amount :  " + amount + " to : "
				+ jioNumberValidationURL);

		final SimpleHttpConnectionManager euronetConnectionManager = new SimpleHttpConnectionManager(true);

		
		euronetConnectionManager.setParams(connectionManagerConfig);
		HttpClient httpClient = new HttpClient(euronetConnectionManager);
		httpClient.setHostConfiguration(euronetHostConfig);
		PostMethod method = new PostMethod(jioNumberValidationURL);

		
		EuronetJioValidationRequest request = new EuronetJioValidationRequest();
		request.setAmount(amount);
		request.setConsumerNo(consumerNo);
		request.setMerchantCode("EUR");
		request.setMerchantRefNo("test123");
		
		ObjectMapper MAPPER = new ObjectMapper();
		String msg = null;
		try {
			msg = MAPPER.writeValueAsString(request);
			logger.info("Raw Request to euronet for jio validation:" + msg);
			StringRequestEntity requestEntity = new StringRequestEntity(msg, "application/json", "UTF-8");
			method.setRequestEntity(requestEntity);
		} catch (JsonProcessingException e) {
			logger.error("Got Exception while json processing request params : " + msg, e);
		} catch (UnsupportedEncodingException e) {
			logger.error("Got Exception :: unsupported params : " + msg, e);
		}
		try {
			int configTimeout = getConnectionTimeoutFromAppConfig();
			if(configTimeout > 0){
				validationTimeout = configTimeout;
				connectionManagerConfig.setConnectionTimeout(validationTimeout);
			}
			Integer statusCode = executeWithTimeout(httpClient, method, validationTimeout, false, "Recharge", "PlanValidation");
			String rawResponse = method.getResponseBodyAsString();
			logger.info("Raw Response from euronet : " + rawResponse);
			if (statusCode.equals(HttpStatus.SC_OK)) {
				ObjectMapper mapper = new ObjectMapper();
				return mapper.readValue(rawResponse, ValidationResponse.class);
			} else {
				logger.info("Got HTTP Response Code : " + statusCode + " for euronet post request " + msg);
			}
		} catch (IOException e) {
			logger.error("Got IO Exception while recharging for euronet request : " + msg, e);
		} catch (Exception e) {
			logger.error("Got Exception while recharging for euronet request : " + msg, e);
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	private int getConnectionTimeoutFromAppConfig() {
		int euronetConnectionTimeout = 0;
		try {
			String returnValue = (String) FCUtil.getCustomPropertyFromAppConfig(
					appConfigService.getCustomProp(appConfigService.VALIDATE_RECHARGE_CUSTOM_CONFIG).toJSONString(),
					"euronet.connection.timeout");
			logger.info("The euronet connection time out value for Validation API is : " + returnValue);
			euronetConnectionTimeout = Integer.parseInt(returnValue);
		} catch (Exception e) {
			logger.error("An error occured trying to get the timeout details from App config", e);
		}
		return euronetConnectionTimeout;
	}
	
	
	@Override
	public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
			List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
			ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
			throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
			IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getWalletBalance() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Double getBalanceWarnThreshold() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Double getBalanceCriticalThreshold() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBalanceCachekey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBalanceUpdateTimeCachekey() {
		// TODO Auto-generated method stub
		return null;
	}
}
