package com.freecharge.validation.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.validation.DO.ValidationResponse;
import com.google.common.collect.ImmutableList;

/**
 * This class is used to forward the validation request to the corresponding
 * aggregator (for now its Euronet only)
 * 
 * @author fcaa17922
 *
 */
@Service
public class CommonValidationService {

	private static Logger logger = LoggingFactory.getLogger(CommonValidationService.class);

	@Autowired
	@Qualifier("euronetValidationService")
	private ValidationInterface validationService;
	
	final List<String> invalidCodes = ImmutableList.of("WP","WN","NP","ECI","CI","PPP","PPN","WM","WQ");

	/**
	 * This method is used to forward the request to corresponding aggregator
	 * implementation classes to perform the validation
	 * 
	 * @param consumerNo
	 * @param amount
	 * @return validationResponse - the validation response from aggregator
	 */
	public ValidationResponse validateNumber(String consumerNo, String amount) {
		logger.info("Inside [CommonValidationService] validateNumber method");
		ValidationResponse validationResponse = new ValidationResponse(RechargeConstants.SUCCESS_RESPONSE_CODE, RechargeConstants.TRANSACTION_STATUS_SUCCESS);
		
		ValidationResponse response = validationService.validateNumber(consumerNo, amount);
		logger.info("Response Service Type : " + response.getServiceType());
		logger.info("POSTPAID".equalsIgnoreCase(response.getServiceType()) ? "true":"false");
		if(response != null && "POSTPAID".equalsIgnoreCase(response.getServiceType())){
			validationResponse.setResponseCode(RechargeConstants.JIO_POSTPAID_ERROR_CODE);
			validationResponse.setResponseMessage(RechargeConstants.JIO_POSTPAID_ERROR_CODE_MESSAGE);
		} else {
			if (response != null && invalidCodes.contains(response.getResponseCode())) {
				if ("WN".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_NON_PRIME_PLAN);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PRIME_PLAN_MESSAGE);
				} else if ("WP".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PRIME_PLAN);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_NON_PRIME_PLAN_MESSAGE);
				} else if ("NP".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PLAN_ERROR_CODE);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PLAN_ERROR_CODE_MESSAGE);
				} else if ("CI".equals(response.getResponseCode()) || "ECI".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(ErrorCode.INVALID_MOBILE_NUMBER.getErrorNumberString());
					validationResponse.setResponseMessage(ErrorCode.INVALID_MOBILE_NUMBER.getErrorMessage());
				} else if ("PPP".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_NON_PRIME_ONLY_ERROR_CODE);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_NON_PRIME_ONLY_ERROR_MESSAGE);
				} else if ("PPN".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_PRIME_ONLY_ERROR_CODE);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_PRIME_ONLY_ERROR_MESSAGE);
				} else if("WM".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_ONLY_ERROR_CODE);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PLAN_JIO_PHONE_ONLY_ERROR_MESSAGE);
				} else if("WQ".equals(response.getResponseCode())) {
					validationResponse.setResponseCode(RechargeConstants.INVALID_JIO_PLAN_PRIME_NON_PRIME_ONLY_ERROR_CODE);
					validationResponse.setResponseMessage(RechargeConstants.INVALID_JIO_PLAN_PRIME_NON_PRIME_ONLY_ERROR_MESSAGE);
				}
			}
		}
		return validationResponse;
	}

}
