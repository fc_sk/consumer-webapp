package com.freecharge.validation.services;

import com.freecharge.validation.DO.ValidationResponse;

/**
 * Validation Interface for Aggregator
 * 
 * @author fcaa17922
 *
 */
public interface ValidationInterface {
	/**
	 * Used to validate a given number with Aggregator
	 * 
	 * @param consumerNo
	 * @param amount
	 * @return validationResponse - the validation response from aggregator
	 */
	public ValidationResponse validateNumber(String consumerNo, String amount);

}
