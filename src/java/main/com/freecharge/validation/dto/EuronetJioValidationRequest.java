package com.freecharge.validation.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class EuronetJioValidationRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String merchantCode;
	private String merchantRefNo;
	private String consumerNo;
	private Float amount;
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getMerchantRefNo() {
		return merchantRefNo;
	}
	public void setMerchantRefNo(String merchantRefNo) {
		this.merchantRefNo = merchantRefNo;
	}
	public String getConsumerNo() {
		return consumerNo;
	}
	public void setConsumerNo(String consumerNo) {
		this.consumerNo = consumerNo;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		try {
			this.amount = Float.parseFloat(amount);
		}catch (Exception e) {
			this.amount = 100f;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EuronetJioValidationRequest [merchantCode=").append(merchantCode).append(", merchantRefNo=")
				.append(merchantRefNo).append(", consumerNo=").append(consumerNo).append(", amount=").append(amount)
				.append("]");
		return builder.toString();
	}
}
