package com.freecharge.BilldeskTransactionBackFill;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.freecharge.app.domain.dao.ProductMasterDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse;
import com.freecharge.infrastructure.billpay.types.FulfillBillResponse.Status;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.order.service.dao.BillpayUnknownStatusDao;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.s3.model.DownloadFileRequest;
import com.freecharge.s3.model.DownloadFileResponse;
import com.freecharge.s3.model.DownloadFileResponseCode;
import com.freecharge.s3.service.ImageServiceImpl;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.BillpayUnknownStatus;

@Service
public class BilldeskTransactionBackFillService {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private BBPSDetailsDao bbpsDetailsDao;

    @Autowired
    private BillpayUnknownStatusDao billpayUnknownStatusDao;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

    @Autowired
    private ImageServiceImpl imageService;
    
    @Autowired
    private FulfillmentService   fulfillmentService;  
    
    @Autowired
    private BillPaymentService billpaymentService;

    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceClient;
    
    @Autowired
	private OrderIdSlaveDAO orderIdSlaveDAO;
	

	@Autowired
	private KestrelWrapper kestrelWrapper;

    private String filePath = "unknown_txn/Unknown_Txn_";
    private static final String BILLDESK_UTILITIES_INPUT_FILE_PATH = "unknown_txn/Billdesk_Utilities_Unknown_Txn_";
    private static final String BILLDESK_BBPS_UTILITIES_INPUT_FILE_PATH = "unknown_txn/Billdesk_Bbps_Utilities_Unknown_Txn_";
    private static final String BILLDESK_POSTPAID_INPUT_FILE_PATH = "unknown_txn/Billdesk_Postpaid_Unknown_Txn_";
    private String copyFilePath = "/tmp/Unknown_Txn_";
    private static final String UTILITIES_COPY_FILE_PATH = "/tmp/Unknown_Utilities_Txn";
    private static final String BBPS_UTILITIES_COPY_FILE_PATH = "/tmp/Unknown_Bbps_Utilities_Txn";
    private static final String POSTPAID_COPY_FILE_PATH = "/tmp/Unknown_Postpaid_Txn";

    private static final String File_PATH = "/tmp/unknown_txn/output/Unknown_Txn_";
    
    private static final String BILLDESK_UTILITIES_OUTPUT_FILE_PATH = "/tmp/billdesk_unknown_txn/output/Unknown_Txn_";
    private static final String BILLDESK_BBPS_UTILITIES_OUTPUT_FILE_PATH = "/tmp/billdesk_bbps_unknown_txn/output/Unknown_Txn_";
    private static final String BILLDESK_POSTPAID_OUTPUT_FILE_PATH = "/tmp/billdesk_postpaid_txn/output/Unknown_Txn_";

    private static final String Ext1 = ".csv";
    private static final String Ext2 = ".txt";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final Object [] FILE_HEADER = {"TRANSACTION ID", "BBPS REF ID", "RECHARGE STATUS", "ORDER ID", "PROCESS RESULT"};
    private static final Object [] BILLDESK_UTILITIES_FILE_HEADER = {"ReconId", "Status", "OrderID", "Final Status"};
    private static final Object [] BILLDESK_BBPS_UTILITIES_FILE_HEADER = {"ReconId", "BBPS Ref No", "Status", "OrderID", "Final Status"};
    private static final Object [] BILLDESK_POSTPAID_FILE_HEADER = {"ReconId", "Status", "OrderID", "Final Status"};
    
    private static final String [] FILE_HEADER_MAPPING = {"BANK ID","BILLER ID", "AUTH 1", "AUTH 2", "AUTH 3", "AUTH 4", "AUTH 5", "PAYMENT DATE", "TRANSACTION ID", "BBPS REF ID", "BILL STATUS", "RECHARGE STATUS", "FAILURE REASON", "RECHARGE FAILUREREASON"};
    private static final String [] BILLDESK_UTILITIES_FILE_HEADER_MAPPING = {"Sr.No." , "LineContent" , "Transaction Reference No" , "Bank Reference No" , "Transaction Amount" , "Transaction Date" , "Status"};
    private static final String [] BILLDESK_POSTPAID_FILE_HEADER_MAPPING = {"Sr.No." , "LineContent" , "Transaction Reference No" , "Bank Reference No" , "Transaction Amount" , "Transaction Date" , "Status"};
    private static final String [] BILLDESK_BBPS_UTILITIES_FILE_HEADER_MAPPING = {"Sr.No." , "LineContent" , "Transaction Reference No" , "Bank Reference No" , "Transaction Amount" , "Transaction Date" , "Status"};
    
    private static final String TRANSACTION_ID = "TRANSACTION ID";
    private static final String BBPS_REF_ID = "BBPS REF ID";
    private static final String RECHARGE_STATUS = "RECHARGE STATUS";
    
    private static final String TRANSACTION_REF_NUMBER = "Transaction Reference No";
    private static final String TRANSACTION_STATUS = "Status";
    
    private static final List<String> FF_STATUS = new ArrayList<String>(Arrays.asList("08","09"));

   

    public BBPSTransactionBackFillResponseDTO processUnknownBdkTxn(){
        BBPSTransactionBackFillResponseDTO backFillResponseDTO = new BBPSTransactionBackFillResponseDTO();
        try {
            DownloadFileResponse downloadFileResponse = downloadFileFromS3();
            if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                logger.info("File Not Found");
                return backFillResponseDTO;
            }
            String outputFilePath = process(downloadFileResponse.getFile().getAbsolutePath());
            logger.info("Output File Path : "+outputFilePath);
            if(StringUtils.isEmpty(outputFilePath)){
                logger.info("Something Went Wrong While Processing File");
                return backFillResponseDTO;
            }
            backFillResponseDTO.setFileName(outputFilePath);
        } catch (IOException e) {
            logger.error("Getting Exception While Processing File ", e);
        }
        return backFillResponseDTO;
    }
    
    public Map<String,String> updateUtilitiesStatus(BillPayTxnList txnList){
            Map<String,String> result = new HashMap<String, String>();
	    	for(BillPayTxn billPayTxn: txnList.getBillpayTxnList()) {	
	    	try {
	    		UserTransactionHistory uth = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(billPayTxn.getOrderId());
	    		if(uth==null || !FF_STATUS.contains(uth.getTransactionStatus()) || !FCUtil.isUtilityPaymentType(uth.getProductType())) {
	    			logger.info("UTH txn not in updated state"+uth.getTransactionStatus());
					 result.put(billPayTxn.getOrderId(), "Invalid UTH state");
	    			continue;
	    		}	    		
                String billId = orderIdSlaveDAO.getBillIdForOrderId(uth.getOrderId(), uth.getProductType());
                if("SUCCESS".equalsIgnoreCase(billPayTxn.getStatus())) {
						FulfillBillResponse billResponse = billPayServiceClient.updateUnknownBilldeskTxnSuccess(billId);
						logger.info("Billtxn and BillTxnLedger Updated as Success"+billResponse +" for orderId"+ billPayTxn.getOrderId());
						if(billResponse!=null && FulfillBillResponse.Status.Success.name().equalsIgnoreCase(billResponse.getResponseStatus().name())) {
							if(!StringUtils.isEmpty(billPayTxn.getBbpsRefNum())) {
								 bbpsDetailsDao.updateBBPSReferenceNoByLookupId(billPayTxn.getLookupId(),
					                		 billPayTxn.getBbpsRefNum());
							     logger.info("BBPS Details Updated for lookupId"+billPayTxn.getLookupId()+" and orderID:"+billPayTxn.getOrderId());
							}
						    userTransactionHistoryDAO.updateStatus(billPayTxn.getOrderId(), "00");
						    logger.info("Updated UTH as success "+billPayTxn.getOrderId());
					        kestrelWrapper.enqueueForFFNotificationRetry(billPayTxn.getOrderId());
						    logger.info("Enqueued for FF "+billPayTxn.getOrderId());
					        result.put(billPayTxn.getOrderId(), "Success");
						}else {
				        	result.put(billPayTxn.getOrderId(), "FF Failed");
						}

                }else if ("Failed".equalsIgnoreCase(billPayTxn.getStatus())||"Failure".equalsIgnoreCase(billPayTxn.getStatus())||"Fail".equalsIgnoreCase(billPayTxn.getStatus())){
					FulfillBillResponse billResponse = billPayServiceClient.updateUnknownBilldeskTxnFailure(billId);
					logger.info("Billtxn and BillTxnLedger Updated as Fail"+billResponse +" for orderId"+ billPayTxn.getOrderId());
					if( billResponse!=null && FulfillBillResponse.Status.Success.name().equalsIgnoreCase(billResponse.getResponseStatus().name())) {
						if(!StringUtils.isEmpty(billPayTxn.getBbpsRefNum())) {
							 bbpsDetailsDao.updateBBPSReferenceNoByLookupId(billPayTxn.getLookupId(),
				                		 billPayTxn.getBbpsRefNum());
						     logger.info("BBPS Details Updated for lookupId"+billPayTxn.getLookupId()+" and orderID:"+billPayTxn.getOrderId());
						}
					    userTransactionHistoryDAO.updateStatus(billPayTxn.getOrderId(), "07");
					    logger.info("Updated UTH as fail "+billPayTxn.getOrderId());
				        kestrelWrapper.enqueueForFFNotificationRetry(billPayTxn.getOrderId());
					    logger.info("Enqueued for FF "+billPayTxn.getOrderId());
				        result.put(billPayTxn.getOrderId(), "Success");
					}else {
			        	result.put(billPayTxn.getOrderId(), "FF Failed");
					}

                }else {
                	logger.error("Invalid payload "+billPayTxn);
					 result.put(billPayTxn.getOrderId(), "Invalid Data");
                }
	    	} catch (Exception e) {
	    		logger.error("Getting Error for orderId for markingPendingTxn::"+billPayTxn.getOrderId());
				result.put(billPayTxn.getOrderId(), "GotException"+e.getMessage());
			}
	    }
    	return result;
    }
    
    public BBPSTransactionBackFillResponseDTO processBdkTxnV2(BilldeskBackfillTypes backfillTypes, String processingDate){
        BBPSTransactionBackFillResponseDTO backFillResponseDTO = new BBPSTransactionBackFillResponseDTO();
        try {
            String outputFilePath = null;
            if(backfillTypes.equals(BilldeskBackfillTypes.UTILITIES)) {
            	DownloadFileResponse downloadFileResponse = downloadBilldeskBackfillFileFromS3(BILLDESK_UTILITIES_INPUT_FILE_PATH, processingDate, UTILITIES_COPY_FILE_PATH);
                if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                    logger.info("File Not Found");
                    return backFillResponseDTO;
                }
            	outputFilePath = processBilldeskUtilitesTxn(downloadFileResponse.getFile().getAbsolutePath(), processingDate);
            }else if(backfillTypes.equals(BilldeskBackfillTypes.BBPS_UTILITIES)){
            	DownloadFileResponse downloadFileResponse = downloadBilldeskBackfillFileFromS3(BILLDESK_BBPS_UTILITIES_INPUT_FILE_PATH,processingDate, BBPS_UTILITIES_COPY_FILE_PATH);
                if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                    logger.info("File Not Found");
                    return backFillResponseDTO;
                }
            	outputFilePath = processBilldeskBBPSUtilitesTxn(downloadFileResponse.getFile().getAbsolutePath(), processingDate);
            }else if(backfillTypes.equals(BilldeskBackfillTypes.POSTPAID)) {
            	DownloadFileResponse downloadFileResponse = downloadBilldeskBackfillFileFromS3(BILLDESK_POSTPAID_INPUT_FILE_PATH,processingDate, POSTPAID_COPY_FILE_PATH);
                if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                    logger.info("File Not Found");
                    return backFillResponseDTO;
                }
            	outputFilePath = processBilldeskPostpaidTxn(downloadFileResponse.getFile().getAbsolutePath(), processingDate);
            }else {
            	logger.error("Found unknown backfill transaction type + " +backfillTypes + " couldn't process anything");
            	return backFillResponseDTO;
            }
            
            logger.info("Output File Path : "+outputFilePath);
            if(StringUtils.isEmpty(outputFilePath)){
                logger.info("Something Went Wrong While Processing File");
                return backFillResponseDTO;
            }
            backFillResponseDTO.setFileName(outputFilePath);
        } catch (IOException e) {
            logger.error("Getting Exception While Processing File ", e);
        }
        return backFillResponseDTO;
    }

    private DownloadFileResponse downloadFileFromS3(){
        DownloadFileRequest downloadFileRequest = new DownloadFileRequest();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        downloadFileRequest.setS3Path(filePath+date+Ext1);
        logger.info("File Name : "+downloadFileRequest.getS3Path());
        DownloadFileResponse downloadFileResponse = null;
        try{
            downloadFileResponse = imageService.downloadFile(downloadFileRequest);
            if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                logger.error("File Not Found With extension " +Ext1 );

                downloadFileRequest.setS3Path(filePath+date+Ext2);
                downloadFileResponse = imageService.downloadFile(downloadFileRequest);
                if (downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                    logger.error("File Not Found With extension " + Ext2);
                    return downloadFileResponse;
                }
            }

            if(downloadFileResponse.getFile() !=null){
                File copiedFilePath = copyFile(downloadFileResponse, date);
                downloadFileResponse.setFile(copiedFilePath);
                logger.info("DownloadFileResponse "+downloadFileResponse.getFile());
            }
        }catch(Exception ex){
            logger.error("File Not Found On s3", ex);
        }
        return downloadFileResponse;
    }
    
    private DownloadFileResponse downloadBilldeskBackfillFileFromS3(String inputFilePath, String processingDate,  String outFilePath){
        DownloadFileRequest downloadFileRequest = new DownloadFileRequest();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        downloadFileRequest.setS3Path(inputFilePath+processingDate+Ext1);
        logger.info("File Name : "+downloadFileRequest.getS3Path());
        DownloadFileResponse downloadFileResponse = null;
        try{
            downloadFileResponse = imageService.downloadFile(downloadFileRequest);
            if(downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                logger.error("File Not Found With extension " +Ext1 );

                downloadFileRequest.setS3Path(inputFilePath+date+Ext2);
                downloadFileResponse = imageService.downloadFile(downloadFileRequest);
                if (downloadFileResponse == null || (!StringUtils.isEmpty(downloadFileResponse.getCode()) && downloadFileResponse.getCode().equalsIgnoreCase(DownloadFileResponseCode.FAILURE.getCode()))) {
                    logger.error("File Not Found With extension " + Ext2);
                    return downloadFileResponse;
                }
            }

            if(downloadFileResponse.getFile() !=null){
                File copiedFilePath = copyDownloadedFile(downloadFileResponse, date, outFilePath);
                downloadFileResponse.setFile(copiedFilePath);
                logger.info("DownloadFileResponse "+downloadFileResponse.getFile());
            }
        }catch(Exception ex){
            logger.error("File Not Found On s3", ex);
        }
        return downloadFileResponse;
    }

    private File copyFile(DownloadFileResponse downloadFileResponse, String date){
        File file = null;
        try {
            logger.info("DownloadFileResponse : " +downloadFileResponse.getFile());
            file = new File(copyFilePath+date+Ext1);
            /*FileReader fr = new FileReader(downloadFileResponse.getFile());
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter(file, true);
            String s;
            while ((s = br.readLine()) != null) { // read a line
                fw.write(s); // write to output file
                fw.flush();
            }
            br.close();
            fw.close();*/
            Path temp  = Files.move(Paths.get(downloadFileResponse.getFile().getAbsolutePath()), Paths.get(file.getAbsolutePath()), REPLACE_EXISTING);
           // boolean isFileCopied = downloadFileResponse.getFile().renameTo(file);
            logger.info("Move Result "+temp);
            if(temp !=null) {
                logger.info("file copied "+downloadFileResponse.getFile());
              //  Files.delete(Paths.get(downloadFileResponse.getFile().getAbsolutePath()));
            }else{
                logger.info("file copied failed");
                return downloadFileResponse.getFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
    
    private File copyDownloadedFile(DownloadFileResponse downloadFileResponse, String date, String copyFilePath){
        File file = null;
        try {
            logger.info("DownloadFileResponse : " +downloadFileResponse.getFile());
            file = new File(copyFilePath+date+Ext1);
            Path temp  = Files.move(Paths.get(downloadFileResponse.getFile().getAbsolutePath()), Paths.get(file.getAbsolutePath()), REPLACE_EXISTING);
            logger.info("Move Result "+temp);
            if(temp !=null) {
                logger.info("file copied "+downloadFileResponse.getFile());
            }else{
                logger.info("file copied failed");
                return downloadFileResponse.getFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private String process(String fileName) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        String outFilePath = File_PATH+date+ UUID.randomUUID().toString() +Ext1;
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        try {
            Path path=Paths.get(outFilePath);
            Files.createDirectories(path.getParent());
            fileWriter = new FileWriter(outFilePath);
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.newBuilder().withDelimiter('|').withRecordSeparator('\n').build());
            csvFilePrinter.printRecord(FILE_HEADER);
            fileReader = new FileReader(fileName);
            csvFileParser = new CSVParser(fileReader, CSVFormat.newBuilder().withHeader(FILE_HEADER_MAPPING).withDelimiter('|').build());
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            logger.info("Processing records");
            List finalStatus;
            for (CSVRecord csvRecord : csvRecords) {
                // Accessing values by Header names
                String reconId = csvRecord.get(TRANSACTION_ID);
                String bbpsRefNo = csvRecord.get(BBPS_REF_ID);
                String status = csvRecord.get(RECHARGE_STATUS);
                Boolean isBbps = true;
                if(StringUtils.isEmpty(bbpsRefNo)|| bbpsRefNo.equalsIgnoreCase("null")){ 
                	isBbps = false;
                }
                
                finalStatus = new ArrayList();
                finalStatus.add(reconId);
                finalStatus.add(bbpsRefNo);
                finalStatus.add(status);
                UpdateTransactionStatusResponse transactionStatusResponse = new UpdateTransactionStatusResponse();
                transactionStatusResponse.setSuccessfulUpdate(false);
                if(StringUtils.isEmpty(reconId)|| reconId.equalsIgnoreCase("null") || StringUtils.isEmpty(status) ){
                    finalStatus.add("Can't Update as either reconId or status is Empty");
                    csvFilePrinter.printRecord(finalStatus);
                    continue;
                }else if("SUCCESS".equalsIgnoreCase(status)) {
                	transactionStatusResponse = updateTransactionStatusToSuccess(reconId, bbpsRefNo, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                } else if("PENDING".equalsIgnoreCase(status)) {
                	continue; //Do nothing for such entries
                }else if("FAILURE".equalsIgnoreCase(status) || "FAILED".equalsIgnoreCase(status)) {
                	transactionStatusResponse = updateTransactionStatusToFailure(reconId, bbpsRefNo, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                }
                finalStatus.add(transactionStatusResponse.getOrderId());
                if(transactionStatusResponse.isSuccessfulUpdate()){
                    finalStatus.add("SUCCESS");
                    logger.info("Frg Id : "+reconId+" BBPS Ref No : "+bbpsRefNo+ " Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status);
                }else {
                    finalStatus.add("FAILED");
                    logger.info("Frg Id : "+reconId+" BBPS Ref No : "+bbpsRefNo+ " Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status+" Failed");
                }
                csvFilePrinter.printRecord(finalStatus);
            }
        }catch (Exception e){
            logger.error("Exception Caught ", e);
        }finally
        {
            if(csvFilePrinter!=null)
                csvFilePrinter.close();
            if(fileReader!=null)
                fileReader.close();
            if(fileWriter!=null){
                fileWriter.close();
            }
        }
        return outFilePath;
    }
    
    private String processBilldeskBBPSUtilitesTxn(String fileName, String processingDate) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        String outFilePath = BILLDESK_BBPS_UTILITIES_OUTPUT_FILE_PATH+processingDate+ UUID.randomUUID().toString() +Ext1;
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        try {
        	Path path=Paths.get(outFilePath);
            Files.createDirectories(path.getParent());
            fileWriter = new FileWriter(outFilePath);
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.newBuilder().withDelimiter(',').withRecordSeparator('\n').build());
            csvFilePrinter.printRecord(BILLDESK_BBPS_UTILITIES_FILE_HEADER);
            fileReader = new FileReader(fileName);
            csvFileParser = new CSVParser(fileReader, CSVFormat.newBuilder().withHeader(BILLDESK_POSTPAID_FILE_HEADER_MAPPING).withDelimiter(',').build());
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            logger.info("Processing records");
            List finalStatus;
            for (CSVRecord csvRecord : csvRecords) {
                // Accessing values by Header names
                String reconId = csvRecord.get(1);
                String bbpsRefNo = csvRecord.get(6);
                String status = csvRecord.get(4);
                Boolean isBbps = true;
                if(StringUtils.isEmpty(bbpsRefNo)|| bbpsRefNo.equalsIgnoreCase("null")){ 
                	isBbps = false;
                }
                
                finalStatus = new ArrayList();
                finalStatus.add(reconId);
                finalStatus.add(bbpsRefNo);
                finalStatus.add(status);
                UpdateTransactionStatusResponse transactionStatusResponse = new UpdateTransactionStatusResponse();
                transactionStatusResponse.setSuccessfulUpdate(false);
                if(StringUtils.isEmpty(reconId)|| reconId.equalsIgnoreCase("null") || StringUtils.isEmpty(status) ){
                    finalStatus.add("Can't Update as either reconId or status is Empty");
                    csvFilePrinter.printRecord(finalStatus);
                    continue;
                }else if(status.equalsIgnoreCase("S")) {
                	transactionStatusResponse = updateTransactionStatusToSuccess(reconId, bbpsRefNo, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                } else if(status.equalsIgnoreCase("PENDING")) {
                	continue; //Do nothing for such entries
                }else if(status.equalsIgnoreCase("F")) {
                	transactionStatusResponse = updateTransactionStatusToFailure(reconId, bbpsRefNo, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                }
                finalStatus.add(transactionStatusResponse.getOrderId());
                if(transactionStatusResponse.isSuccessfulUpdate()){
                    finalStatus.add("SUCCESS");
                    logger.info("Frg Id : "+reconId+" BBPS Ref No : "+bbpsRefNo+ " Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status);
                }else {
                    finalStatus.add("FAILED");
                    logger.info("Frg Id : "+reconId+" BBPS Ref No : "+bbpsRefNo+ " Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status+" Failed");
                }
                csvFilePrinter.printRecord(finalStatus);
            }
        }catch (Exception e){
            logger.error("Exception Caught ", e);
        }finally
        {
            if(csvFilePrinter!=null)
                csvFilePrinter.close();
            if(fileReader!=null)
                fileReader.close();
            if(fileWriter!=null){
                fileWriter.close();
            }
        }
        return outFilePath;
    }
    
    private String processBilldeskPostpaidTxn(String fileName, String processingDate) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        String outFilePath = BILLDESK_POSTPAID_OUTPUT_FILE_PATH+processingDate+ UUID.randomUUID().toString() +Ext1;
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        try {
        	Path path=Paths.get(outFilePath);
            Files.createDirectories(path.getParent());
            fileWriter = new FileWriter(outFilePath);
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.newBuilder().withDelimiter(',').withRecordSeparator('\n').build());
            csvFilePrinter.printRecord(BILLDESK_POSTPAID_FILE_HEADER);
            fileReader = new FileReader(fileName);
            csvFileParser = new CSVParser(fileReader, CSVFormat.newBuilder().withHeader(BILLDESK_POSTPAID_FILE_HEADER_MAPPING).withDelimiter(',').build());
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            logger.info("Processing records");
            List finalStatus;
            for (CSVRecord csvRecord : csvRecords) {
                // Accessing values by Header names
                String reconId = csvRecord.get(2);
                String status = csvRecord.get(6);
                Boolean isBbps = false;
                
                finalStatus = new ArrayList();
                finalStatus.add(reconId);
                finalStatus.add(status);
                
                String orderId = null; //Find order from reconId
                UpdateTransactionStatusResponse transactionStatusResponse = new UpdateTransactionStatusResponse();
                transactionStatusResponse.setSuccessfulUpdate(false);
                if(StringUtils.isEmpty(reconId)|| reconId.equalsIgnoreCase("null") || StringUtils.isEmpty(status) ){
                    finalStatus.add("Can't Update as either reconId or status is Empty");
                    csvFilePrinter.printRecord(finalStatus);
                    continue;
                }else if(status.equalsIgnoreCase("SUCCESS")) {
                	transactionStatusResponse = billpaymentService.backfillSuccessBilldeskPostpaid(orderId);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                } else if(status.equalsIgnoreCase("PENDING")) {
                	continue; //Do nothing for such entries
                }else if(status.equalsIgnoreCase("FAILURE")) {
                	transactionStatusResponse = billpaymentService.backfillFailureBilldeskPostpaid(orderId);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                }
                finalStatus.add(transactionStatusResponse.getOrderId());
                if(transactionStatusResponse.isSuccessfulUpdate()){
                    finalStatus.add("SUCCESS");
                    logger.info("Frg Id : "+reconId+" Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status);
                }else {
                    finalStatus.add("FAILED");
                    logger.info("Frg Id : "+reconId+" Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status+" Failed");
                }
                csvFilePrinter.printRecord(finalStatus);
            }
        }catch (Exception e){
            logger.error("Exception Caught ", e);
        }finally
        {
            if(csvFilePrinter!=null)
                csvFilePrinter.close();
            if(fileReader!=null)
                fileReader.close();
            if(fileWriter!=null){
                fileWriter.close();
            }
        }
        return outFilePath;
    }
    
    
    
    private String processBilldeskPostpaidTxnTest(String fileName, String processingDate) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        String outFilePath = BILLDESK_POSTPAID_OUTPUT_FILE_PATH+processingDate+ UUID.randomUUID().toString() +Ext1;
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        try {
        	Path path=Paths.get(outFilePath);
            Files.createDirectories(path.getParent());
            fileWriter = new FileWriter(outFilePath);
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.newBuilder().withDelimiter('|').withRecordSeparator('\n').build());
            csvFilePrinter.printRecord(BILLDESK_POSTPAID_FILE_HEADER);
            fileReader = new FileReader(fileName);
            csvFileParser = new CSVParser(fileReader, CSVFormat.newBuilder().withHeader(BILLDESK_POSTPAID_FILE_HEADER_MAPPING).withDelimiter('|').build());
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            logger.info("Processing records");
            logger.info("Processing records");
            List finalStatus;
            for (CSVRecord csvRecord : csvRecords) {
                // Accessing values by Header names
                String reconId = csvRecord.get(2);
                String status = csvRecord.get(6);
                Boolean isBbps = false;
                
                finalStatus = new ArrayList();
                finalStatus.add(reconId);
                finalStatus.add(status);
                
                String orderId = null; //Find order from reconId
                UpdateTransactionStatusResponse transactionStatusResponse = new UpdateTransactionStatusResponse();
                transactionStatusResponse.setSuccessfulUpdate(false);
                if(StringUtils.isEmpty(reconId)|| reconId.equalsIgnoreCase("null") || StringUtils.isEmpty(status) ){
                    finalStatus.add("Can't Update as either reconId or status is Empty");
                    csvFilePrinter.printRecord(finalStatus);
                    continue;
                }else if(status.equalsIgnoreCase("SUCCESS")) {
                	transactionStatusResponse = billpaymentService.backfillSuccessBilldeskPostpaid(orderId);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                } else if(status.equalsIgnoreCase("PENDING")) {
                	continue; //Do nothing for such entries
                }else if(status.equalsIgnoreCase("FAILURE")) {
                	transactionStatusResponse = billpaymentService.backfillFailureBilldeskPostpaid(orderId);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                }
                finalStatus.add(transactionStatusResponse.getOrderId());
                if(transactionStatusResponse.isSuccessfulUpdate()){
                    finalStatus.add("SUCCESS");
                    logger.info("Frg Id : "+reconId+" Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status);
                }else {
                    finalStatus.add("FAILED");
                    logger.info("Frg Id : "+reconId+" Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status+" Failed");
                }
                csvFilePrinter.printRecord(finalStatus);
            }
        }catch (Exception e){
            logger.error("Exception Caught ", e);
        }finally
        {
            if(csvFilePrinter!=null)
                csvFilePrinter.close();
            if(fileReader!=null)
                fileReader.close();
            if(fileWriter!=null){
                fileWriter.close();
            }
        }
        return outFilePath;
    }
    
    private List<List<String>> readRows(Iterator rows) {
		List<List<String>> data = new ArrayList<List<String>>();
		while(rows.hasNext()){
			HSSFRow myRow = (HSSFRow) rows.next();
			Iterator cellIter = myRow.cellIterator();
			List<String> rowData = new ArrayList<String>();
			while(cellIter.hasNext()){
				HSSFCell myCell = (HSSFCell) cellIter.next();
				rowData.add(StringUtils.trim(myCell.toString()));
			}
			data.add(rowData);
			
		}
		return data;
   }
    
    private String processBilldeskUtilitesTxn(String fileName, String processingDate) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String date = sdf.format(new Date());
        String outFilePath = BILLDESK_UTILITIES_OUTPUT_FILE_PATH+processingDate+ UUID.randomUUID().toString() +Ext1;
        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;
        FileReader fileReader = null;
        CSVParser csvFileParser = null;

        try {
        	Path path=Paths.get(outFilePath);
            Files.createDirectories(path.getParent());
            fileWriter = new FileWriter(outFilePath);
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.newBuilder().withDelimiter(',').withRecordSeparator('\n').build());
            csvFilePrinter.printRecord(BILLDESK_UTILITIES_FILE_HEADER);
            fileReader = new FileReader(fileName);
            csvFileParser = new CSVParser(fileReader, CSVFormat.newBuilder().withHeader(BILLDESK_UTILITIES_FILE_HEADER_MAPPING).withDelimiter(',').build());
            List<CSVRecord> csvRecords = csvFileParser.getRecords();
            logger.info("Processing records");
            List finalStatus;
            for (CSVRecord csvRecord : csvRecords) {
                String reconId = csvRecord.get(2);
                String status = csvRecord.get(6);
                Boolean isBbps = false;
                
                finalStatus = new ArrayList();
                finalStatus.add(reconId);
                finalStatus.add(status);
                UpdateTransactionStatusResponse transactionStatusResponse = new UpdateTransactionStatusResponse();
                transactionStatusResponse.setSuccessfulUpdate(false);
                if(StringUtils.isEmpty(reconId)|| reconId.equalsIgnoreCase("null") || StringUtils.isEmpty(status) ){
                    finalStatus.add("Can't Update as either reconId or status is Empty");
                    csvFilePrinter.printRecord(finalStatus);
                    continue;
                }else if(status.equalsIgnoreCase("SUCCESS")) {
                	transactionStatusResponse = updateTransactionStatusToSuccess(reconId, null, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                } else if(status.equalsIgnoreCase("PENDING")) {
                	continue; //Do nothing for such entries
                }else if(status.equalsIgnoreCase("FAILURE")) {
                	transactionStatusResponse = updateTransactionStatusToFailure(reconId, null, isBbps);
    				try {
    					if (transactionStatusResponse != null && transactionStatusResponse.isSuccessfulUpdate()) {
    						fulfillmentService.doFulfillment(transactionStatusResponse.getOrderId(), true, true, false);
    					} else {
    						logger.info("transactionStatusResponse is NULL");
    					}
    				} catch (Exception ex) {
    					logger.info("Exception processing fulfillment for frgId : " + reconId+", TRACE : "+ex);
    				}
                }
                finalStatus.add(transactionStatusResponse.getOrderId());
                if(transactionStatusResponse.isSuccessfulUpdate()){
                    finalStatus.add("SUCCESS");
                    logger.info("Frg Id : "+reconId+ " Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status);
                }else {
                    finalStatus.add("FAILED");
                    logger.info("Frg Id : "+reconId+" Order Id : "+transactionStatusResponse.getOrderId()+" Status : "+status+" Failed");
                }
                csvFilePrinter.printRecord(finalStatus);
            }
        }catch (Exception e){
            logger.error("Exception Caught ", e);
        }finally
        {
            if(csvFilePrinter!=null)
                csvFilePrinter.close();
            if(fileReader!=null)
                fileReader.close();
            if(fileWriter!=null){
                fileWriter.close();
            }
        }
        return outFilePath;
    }


    @Transactional(rollbackFor = Exception.class)
    private UpdateTransactionStatusResponse updateTransactionStatusToSuccess(String frgId, String bbpsRefNo, Boolean isBbps){
        UpdateTransactionStatusResponse updateTransactionStatusResponse = new UpdateTransactionStatusResponse();
        updateTransactionStatusResponse.setSuccessfulUpdate(false);
        BillpayUnknownStatus billpayUnknownStatus = billpayUnknownStatusDao.getUnknownStatusTxn(frgId);
        if(billpayUnknownStatus == null) {
            logger.info("BillpayUnknownStatus returning null for Frg Id "+frgId);
            return updateTransactionStatusResponse;
        }
        try {
            logger.info("BillpayUnknownStatus "+billpayUnknownStatus.toString() + " For "+frgId);
            updateTransactionStatusResponse.setOrderId(billpayUnknownStatus.getOrderId());
            FulfillBillResponse fulfillBillResponse = billPayServiceClient.updateUnknownBilldeskTxnSuccess(billpayUnknownStatus.getBillId());
            if(fulfillBillResponse == null) {
                logger.info("FulfillBillResponse returning null for Biller Id "+billpayUnknownStatus.getBillId());
                return updateTransactionStatusResponse;
            }
            logger.info("FulfillBillResponse "+fulfillBillResponse.toString() + " For "+frgId);
            if(isBbps) {
            	BBPSTransactionDetails bbpsTransactionDetails = new BBPSTransactionDetails();
                bbpsTransactionDetails.setAggregatorTxnId(frgId);
                bbpsTransactionDetails.setIsBbpsActive(true);
                bbpsTransactionDetails.setBbpsReferenceNumber(bbpsRefNo);
                bbpsDetailsDao.updateBBPSReferenceNo(bbpsTransactionDetails);
            }
            billpayUnknownStatusDao.updateStatus(frgId);
            userTransactionHistoryService.updateStatus(billpayUnknownStatus.getOrderId(), "00");
            updateTransactionStatusResponse.setSuccessfulUpdate(true);
        } catch (ParseException e) {
            logger.error("Error While  parsing", e);
        }catch (Exception ex){
            logger.error("Error While updating in DB", ex);
        }
        return updateTransactionStatusResponse;
    }
    
    @Transactional(rollbackFor = Exception.class)
    private UpdateTransactionStatusResponse updateTransactionStatusToFailure(String frgId, String bbpsRefNo, Boolean isBbps){
        UpdateTransactionStatusResponse updateTransactionStatusResponse = new UpdateTransactionStatusResponse();
        updateTransactionStatusResponse.setSuccessfulUpdate(false);
        BillpayUnknownStatus billpayUnknownStatus = billpayUnknownStatusDao.getUnknownStatusTxn(frgId);
        if(billpayUnknownStatus == null) {
            logger.info("BillpayUnknownStatus returning null for Frg Id "+frgId);
            return updateTransactionStatusResponse;
        }
        try {
            logger.info("BillpayUnknownStatus "+billpayUnknownStatus.toString() + " For "+frgId);
            updateTransactionStatusResponse.setOrderId(billpayUnknownStatus.getOrderId());
            FulfillBillResponse fulfillBillResponse = billPayServiceClient.updateUnknownBilldeskTxnFailure(billpayUnknownStatus.getBillId());
            if(fulfillBillResponse == null) {
                logger.info("FulfillBillResponse returning null for Biller Id "+billpayUnknownStatus.getBillId());
                return updateTransactionStatusResponse;
            }
            logger.info("FulfillBillResponse "+fulfillBillResponse.toString() + " For "+frgId);
            if(isBbps) {
            	 BBPSTransactionDetails bbpsTransactionDetails = new BBPSTransactionDetails();
                 bbpsTransactionDetails.setAggregatorTxnId(frgId);
                 bbpsTransactionDetails.setIsBbpsActive(true);
                 bbpsTransactionDetails.setBbpsReferenceNumber(bbpsRefNo);
                 bbpsDetailsDao.updateBBPSReferenceNo(bbpsTransactionDetails);
            }
            billpayUnknownStatusDao.updateStatus(frgId);
            userTransactionHistoryService.updateStatus(billpayUnknownStatus.getOrderId(), "07");
            updateTransactionStatusResponse.setSuccessfulUpdate(true);
        } catch (ParseException e) {
            logger.error("Error While  parsing", e);
        }catch (Exception ex){
            logger.error("Error While updating in DB", ex);
        }
        return updateTransactionStatusResponse;
    }
}
