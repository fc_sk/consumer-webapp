package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;
import java.util.Map;

public class UpdateReconMetaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> orderIdToReconIdMap;
	private String cred;
	private boolean forceUpdate;

	public Map<String, String> getOrderIdToReconIdMap() {
		return orderIdToReconIdMap;
	}

	public void setOrderIdToReconIdMap(Map<String, String> orderIdToReconIdMap) {
		this.orderIdToReconIdMap = orderIdToReconIdMap;
	}

	public String getCred() {
		return cred;
	}

	public void setCred(String cred) {
		this.cred = cred;
	}

	public boolean isForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(boolean forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	@Override
	public String toString() {
		return "UpdateReconMetaDTO [orderIdToReconIdMap=" + orderIdToReconIdMap + ", cred=" + cred + ", forceUpdate="
				+ forceUpdate + "]";
	}

}
