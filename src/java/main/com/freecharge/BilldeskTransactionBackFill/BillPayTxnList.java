package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;
import java.util.List;

public class BillPayTxnList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<BillPayTxn> billpayTxnList;

	public List<BillPayTxn> getBillpayTxnList() {
		return billpayTxnList;
	}

	public void setBillpayTxnList(List<BillPayTxn> billpayTxnList) {
		this.billpayTxnList = billpayTxnList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillPayTxnList [billpayTxnList=").append(billpayTxnList).append("]");
		return builder.toString();
	}
}
