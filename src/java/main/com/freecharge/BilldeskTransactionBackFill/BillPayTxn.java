package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;

public class BillPayTxn implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String lookupId;
	private String status;
	private String orderId;
	private String aggName;
	private String bbpsRefNum;
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getAggName() {
		return aggName;
	}
	public void setAggName(String aggName) {
		this.aggName = aggName;
	}
	public String getBbpsRefNum() {
		return bbpsRefNum;
	}
	public void setBbpsRefNum(String bbpsRefNum) {
		this.bbpsRefNum = bbpsRefNum;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillPayTxn [lookupId=").append(lookupId).append(", status=").append(status).append(", orderId=")
				.append(orderId).append(", aggName=").append(aggName).append(", bbpsRefNum=").append(bbpsRefNum).append("]");
		return builder.toString();
	}
}
