package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;

public class UpdateTransactionStatusResponse implements Serializable {

    private boolean successfulUpdate;

    private  String orderId;

    public UpdateTransactionStatusResponse() {
    }

    public UpdateTransactionStatusResponse(boolean successfulUpdate, String orderId) {
        this.successfulUpdate = successfulUpdate;
        this.orderId = orderId;
    }

    public boolean isSuccessfulUpdate() {
        return successfulUpdate;
    }

    public void setSuccessfulUpdate(boolean successfulUpdate) {
        this.successfulUpdate = successfulUpdate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
