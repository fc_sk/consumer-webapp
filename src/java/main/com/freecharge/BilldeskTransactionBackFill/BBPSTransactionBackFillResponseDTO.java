package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;

public class BBPSTransactionBackFillResponseDTO implements Serializable {

    private String fileName;

    public BBPSTransactionBackFillResponseDTO() {
    }

    public BBPSTransactionBackFillResponseDTO(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "BBPSTransactionBackFillResponseDTO{" +
                "fileName='" + fileName + '\'' +
                '}';
    }
}
