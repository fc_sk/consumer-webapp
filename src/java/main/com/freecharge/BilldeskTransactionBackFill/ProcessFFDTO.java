package com.freecharge.BilldeskTransactionBackFill;

import java.io.Serializable;
import java.util.Set;

public class ProcessFFDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<String> orderIds;
	private String cred;

	public Set<String> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(Set<String> orderIds) {
		this.orderIds = orderIds;
	}

	public String getCred() {
		return cred;
	}

	public void setCred(String cred) {
		this.cred = cred;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProcessFFDTO [orderIds=").append(orderIds).append("]");
		return builder.toString();
	}

}
