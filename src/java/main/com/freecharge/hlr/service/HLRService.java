package com.freecharge.hlr.service;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.collector.RechargeMetricService;

@Service
public class HLRService {

	private static final Object HLR_INVALID_NUMBER_MSG = "hlrInvalidNumberMsg";
	private static final Object PRIORITY_LIST = "hlrPriority";
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
    private AppConfigService appConfig;
	@Autowired
	private RechargeMetricService rechargeMetricService;
	
	@Autowired
	private HLRServiceFactory hlrServiceFactory;
	
	public static enum STATUS {
		SUCCESS(0, "Success"), FAILURE(1, "Failure"), TIMEOUT(2, "Timeout");
		private final int statusId;
		private final String statusMsg;

		private STATUS(int statusId, String statusMsg) {
			this.statusId = statusId;
			this.statusMsg = statusMsg;
		}

		public int getStatusId() {
			return statusId;
		}

		public String getStatusMsg() {
			return statusMsg;
		}
	};
	
	public static enum HLR_SERVICE {
		MGAGE(1, "Mgage"), SMSGUPSHUP(2, "SMSGupshup");
		private final int hlrId;
		private final String hlrService;

		private HLR_SERVICE(int hlrId, String hlrService) {
			this.hlrId = hlrId;
			this.hlrService = hlrService;
		}

		public int getHlrId() {
			return hlrId;
		}

		public String getHlrService() {
			return hlrService;
		}
	};

	public HLRResponse getResponse(String serviceNumber, String orderId, Integer operatorId, Integer circleId) {

		HLRResponse hlrResponse = null;
		String mgageReason = null;
		
		String hlrPriority = getHLRPriority();
		
		if(!FCUtil.isEmpty(hlrPriority)){
			StringTokenizer priorityList = new StringTokenizer(hlrPriority, ",");
			
			while(priorityList.hasMoreElements()){
				String hlrServiceName = priorityList.nextElement().toString();
				HLRServiceInterface hlrServiceImpl = hlrServiceFactory.getHLRProvider(hlrServiceName);
				hlrResponse = hlrServiceImpl.getHLRResponse(serviceNumber, orderId, operatorId, circleId);
				if(hlrResponse!=null && hlrResponse.getErrorCode()==STATUS.SUCCESS.getStatusId()){
					return hlrResponse;
				}
				if(hlrServiceName.equals(HLR_SERVICE.MGAGE.getHlrService())){
					if(isInvalidNumber(hlrResponse.getErrorMsg())){
						mgageReason = hlrResponse.getErrorMsg();
					}
				}
			}
			if(!FCUtil.isEmpty(mgageReason)&&isInvalidNumber(mgageReason)){
				rechargeMetricService.blockInvalidNumber(serviceNumber);
			}
			logger.info("No HLR Response found for orderId:" + orderId);
			
		}	

		return hlrResponse;
	}

	private String getHLRPriority() {
		String priority = null;
		JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);

		if (configJson != null) {
			priority = (String) configJson.get(PRIORITY_LIST);
		}
		return priority;
	}
	

	private boolean isInvalidNumber(String reason) {
		JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);

		if (configJson != null) {
			final String enabledEventListString = (String) configJson.get(HLR_INVALID_NUMBER_MSG);
			logger.info("Enabled Reason List is "+enabledEventListString);

			if (!StringUtils.isBlank(enabledEventListString)) {
				List<String> eventList = Arrays.asList(enabledEventListString.split(","));

				if (eventList.contains(reason)) {
					return true;
				}

			}
		}

		return false;
	}	

}
