package com.freecharge.hlr.service;

public interface HLRServiceInterface {

	public static final String MGAGE_HLR = "mgage";
	public static final String GUPSHUP_HLR ="smsGupshup";
	
	public HLRResponse getHLRResponse(String serviceNumber,String orderId,Integer operatorId,Integer circleId);
}
