package com.freecharge.hlr.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class HLRServiceFactory  implements ApplicationContextAware {

	private ApplicationContext applicationContext;
	
	public HLRServiceInterface getHLRProvider(String hlrName){
		if (this.getApplicationContext().containsBean(hlrName.toLowerCase()+"hlr"))
			return (HLRServiceInterface) this.getApplicationContext().getBean(hlrName.toLowerCase()+"hlr");
		else
			return null;
	}

	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.applicationContext = ctx;
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
