package com.freecharge.adminpage.userban;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class EmailFormValidator implements Validator {
	 
	@Override
	public boolean supports(Class<?> aClass) {
		return EmailForm.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Email Id field cannot be blank.", "Email Id field cannot be blank.");
	}

}
