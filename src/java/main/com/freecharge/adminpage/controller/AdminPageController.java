package com.freecharge.adminpage.controller;

import java.io.IOException;
import java.util.*;

import com.freecharge.admin.component.GoogleClient;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.service.AccessControlUtil;
import com.freecharge.admin.service.AdminUserService;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.mongodb.DBObject;
import com.freecharge.admin.service.AccessControlService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.web.util.ViewConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

@Controller
@RequestMapping("/admin")
public class AdminPageController extends BaseAdminController{

    @Autowired
    private GoogleClient googleClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    AdminUserService adminUserService;

    @Autowired
    private CTActionControlService  ctActionControlService;

    @Autowired 
    AccessControlService accessControlService;
    
    private Logger logger = LoggingFactory.getLogger(AdminPageController.class);
    private static final String DO_LOGOUT = "doLogout";
    private static final String STRING_PARAMS_CODE="code";
    private static final String STRING_PARAMS_EMAIL="email";
    private static final String STRING_PARAMS_SESSEION_DATA_MAP="Session Data Map";
    private static final String STRING_PARAMS_STATE="state";
    private static final String STRING_PARAMS_LOGGED_IN="loggedIn";
    private static final String STRING_PARAMS_USER_OBJECTS="User Object";
    private static final String STRING_PARAMS_NAME="name";
    private static final String STRING_PARAMS_MAP="map";
    private static final String STRING_PARAMS_USER="user";
    private static final String STRING_COMPONENT_FIRST_LETTERS="componentFirstLetters";
    private static final String STRING_PARAMS_PAGE_NAME = "pageName";
    private static final String STRING_PAGE_NAME = "Freecharge Admin";

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String doLogin(@RequestParam(defaultValue = "/admin") String next, HttpServletRequest request){
        String curUserEmail = getCurrentAdminUser(request);
        if(!FCStringUtils.isBlank(curUserEmail)){
            return String.format("redirect:%s", ADMIN_HOME);
        }
        String redirectUri = fcProperties.getProperty(FCProperties.GOOGLE_REDIRECT_ADMIN_URI);
        return String.format("redirect:%s", googleClient.getAuthorizeUrl(redirectUri, next));
    }

    @RequestMapping(value = "/gcallback", method = RequestMethod.GET)
    public String googleCallback(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        String curUserEmail = getCurrentAdminUser(request);
        if(!FCStringUtils.isBlank(curUserEmail)){
            return String.format("redirect:%s", ADMIN_HOME);
        }
        String code = params.get(STRING_PARAMS_CODE);
        if(FCStringUtils.isBlank(code)){
            return this.get403HtmlResponse(response);
        }
        String redirectUri = fcProperties.getProperty(FCProperties.GOOGLE_REDIRECT_ADMIN_URI);
        Map<String, Object> tokenMap = this.googleClient.getAccessTokenMap(code, redirectUri);
        if (tokenMap == null){
            return this.get403HtmlResponse(response);
        }
        Map<String, Object> user = this.googleClient.getUserProfile((String)tokenMap.get("access_token"));

        //Converting email in user object to lowercase as google acts stupid sometimes.
        if (user.get(STRING_PARAMS_EMAIL) != null){
            user.put(STRING_PARAMS_EMAIL, ((String)user.get(STRING_PARAMS_EMAIL)).toLowerCase());
        }
        //Adding user if his/her email is in root email or fethcing  users 
        String userEmail = this.adminUserService.getOrCreateAdminUser(user, tokenMap);
        if (userEmail!=null){
            //Login the user
            FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
            Map<String, Object> map = freechargeSession.getSessionData();
            logger.error(STRING_PARAMS_SESSEION_DATA_MAP+map);
            if (map != null) {
                map.put(FCConstants.ADMIN_CUR_USER_EMAIL, user.get(STRING_PARAMS_EMAIL));
            }
            return String.format("redirect:%s", params.get(STRING_PARAMS_STATE));
        }else {
            return this.get403HtmlResponse(response);
        }
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response){
        request.getSession().invalidate();
        /*
         * All the logout related activities are done in HTTPRequestInterceptor
         * class. This interceptor searches for the below attribute in request
         * and if found, does the logout related activities.
         */
        request.setAttribute(DO_LOGOUT, true);
        return "redirect:/admin";
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAdminPage(HttpServletRequest request, HttpServletResponse response,Model model) throws IOException {
        String curUserEmail = getCurrentAdminUser(request);
        List<String> components;
        List<String> categories;
        Set<String> componentFirstLetters;
        String userType=FCConstants.ADMIN_USER_TYPE_REGULAR_READER;
        this.accessControlService.addDefaultComponents();
        if(FCStringUtils.isBlank(curUserEmail)){
            model.addAttribute(STRING_PARAMS_LOGGED_IN, false);
            return ViewConstants.ADMIN_PAGE_HOME_VIEW;
        }
        DBObject userObj = this.adminUserService.getAdminUserByEmail(curUserEmail);
        logger.info(STRING_PARAMS_USER_OBJECTS+userObj);
        model.addAttribute(STRING_PARAMS_LOGGED_IN, true);
        if(userObj.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)!=null)
        {
            userType=userObj.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).toString();
        }
        if(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN.equals(userType)){
            categories=this.accessControlService.getListOfStringFromDBObjectList(AccessControlUtil.STRING_PARAMS_CATEGORY_NAME,this.accessControlService.fetchAllCategories());
            components=this.accessControlService.getListOfStringFromDBObjectList(STRING_PARAMS_NAME, this.accessControlService.fetchAllComponents());
        }
        else{
            categories=(List<String>)userObj.get(AccessControlUtil.STRING_PARAMS_CATEGORIES);
            components=(List<String>)userObj.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
        }
        logger.debug("User Categories : "+categories);
        model.addAttribute(AccessControlUtil.STRING_PARAMS_CATEGORIES,categories);
        
        if(components==null){
            components=new ArrayList<String>();
        }

        logger.debug(AccessControlUtil.STRING_PARAMS_COMPONENTS+": " + components);
        Map<String,String> componentMap = this.accessControlService.getAdminHomeUrlsMap(components);
        componentFirstLetters = getComponentFirstCharacters(componentMap.keySet());

        model.addAttribute(AccessControlUtil.STRING_PARAMS_COMPONENTS, componentMap);
        model.addAttribute(STRING_PARAMS_MAP,this.accessControlService.getComponentCategoryMap(components));
        model.addAttribute(STRING_PARAMS_USER,userObj);
        model.addAttribute(STRING_COMPONENT_FIRST_LETTERS,componentFirstLetters);
        model.addAttribute(STRING_PARAMS_PAGE_NAME,STRING_PAGE_NAME);
        /*
         * Setting default maxCredits and maxViews limits in
         * customertrail_control repo for new email
         */
        ctActionControlService.updateCTControlRepoByDefault(curUserEmail);
        return ViewConstants.ADMIN_PAGE_HOME_VIEW;
    }

    public Set<String> getComponentFirstCharacters(Set<String> components)
    {
        Set<String> componentFirstCharacters = new TreeSet<>();
        Iterator<String> componentIterator = components.iterator();
        while(componentIterator.hasNext())
        {
            String componentName = componentIterator.next();
            String firstLetter = Character.toString(componentName.charAt(0));
            componentFirstCharacters.add(firstLetter);
        }
        return componentFirstCharacters;
    }

    public static final String ADMIN_HOME = "/admin";
}