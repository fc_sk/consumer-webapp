package com.freecharge.revenueleak;

import com.freecharge.revenueleak.DetectRevenueLeakService.RevenueLeakReason;

public class RevenueLeak {
	
	public RevenueLeak(String orderId,boolean rechargeSuccess , double netLoss, RevenueLeakReason reason ){
		this.setNetLoss(netLoss);
		this.setOrderId(orderId);
		this.setReason(reason);
		this.setRechargeSuccess(rechargeSuccess);
 	}
	
	private String orderId;
	private boolean rechargeSuccess;
 	private double netLoss;
	private RevenueLeakReason reason;

  	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public boolean isRechargeSuccess() {
		return rechargeSuccess;
	}
	public void setRechargeSuccess(boolean rechargeSuccess) {
		this.rechargeSuccess = rechargeSuccess;
	}
	public double getNetLoss() {
		return netLoss;
	}
	public void setNetLoss(double netLoss) {
		this.netLoss = netLoss;
	}
	public RevenueLeakReason getReason() {
		return reason;
	}
	public void setReason(RevenueLeakReason reason) {
		this.reason = reason;
	}
	
}
