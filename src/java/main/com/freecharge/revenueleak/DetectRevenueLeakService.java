package com.freecharge.revenueleak;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletTransaction;
 
@Component("detectRevenueLeakService")
public class DetectRevenueLeakService {
	
	private static final Logger logger = LoggingFactory.getLogger(DetectRevenueLeakService.class);

	@Autowired
    private InService inService;
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    @Autowired
    private InReadDAO inReadDAO; 	
    @Autowired
    private WalletService walletService;

    @Autowired
    private CartService cartService;
    @Autowired
    private PricingService pricingService;
	@Autowired
	EmailService emailService;
	@Autowired
    BillPaymentService billPaymentService;
	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
    public static enum RevenueLeakReason {
        EXCESS_WALLET_CREDIT_NO_RECHARGE,
        COUPON_ASSIGNED_NO_RECHARGE,
        LESS_AMOUNT_PAID_RECHARGE_SUCCESS,
        COUPON_ASSIGNED_MORE_THAN_AMOUNT_RECHARGE_SUCCESS,
        WALLET_INVALID_FUND_SOURCE,
        EXCESS_WALLET_CREDIT_RECHARGE_SUCCESS,
        EXCESS_WALLET_CREDIT_RECHARGE_FAILED,
        COUPON_ASSIGNED_RECHARGE_FAILED
    } 
      
    private List<RevenueLeak> revenueLoss = new ArrayList<RevenueLeak>();
    private List<String>orderIdwithInvalidFundSrc = new ArrayList<String>();
	private List<String>orderIdwithInvalidFundDes = new ArrayList<String>();
	
	
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date getDate(String dateString) throws ParseException {
        try{
        	return new SimpleDateFormat(DATE_FORMAT).parse(dateString);
        }catch(Exception e){
        	logger.error("Invalid date " + dateString);
        	throw e;
        }
    }
	
	public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        DetectRevenueLeakService detectRevenueLeakService = ((DetectRevenueLeakService) ctx.getBean("detectRevenueLeakService"));

        Date start = DetectRevenueLeakService.getDate(args[0]);
        Date end = DetectRevenueLeakService.getDate(args[1]);

        logger.info(String.format("Revenue Leak Service from %s to %s", start, end));

        try {
        	detectRevenueLeakService.initiate(start, end);
        } catch (Exception e) {
            logger.error("Exception while revenue leak service execution", e);
        } finally {
            ctx.close();
            System.exit(0);
        }
    }
	
	public void initiate(Date startDate, Date endDate){
		//Verify recharges in one hour window
		Date windowStart = startDate; 
		Date windowEnd = DateUtils.addHours(windowStart, 1).before(endDate) == true ? DateUtils.addHours(windowStart, 1): endDate;
		
		while( windowStart.before(windowEnd) && !windowEnd.after(endDate)){
			try {
				verifyRecharge(windowStart, windowEnd);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("Exception in verifyRecharge block of initiate for time interval "+  windowStart
						+ " to " + windowEnd + " Exception message is  " +e.getMessage());
			}
			try{
				validateWalletFundSourceAndDestination(windowStart, windowEnd);
			}catch(Exception e){
				logger.error("Exception in validateWalletFundSourceAndDestination block of initiate for time interval "+  windowStart
						+ " to " + windowEnd + " Exception message is  " +e.getMessage());
			}
			windowStart = windowEnd;
			windowEnd = DateUtils.addHours(windowStart, 1).before(endDate) == true ? DateUtils.addHours(windowStart, 1): endDate;
		}
		sendEmail(startDate, endDate);
	}
	
	
	private void verifyRecharge(Date startDate, Date endDate) throws Exception {
		logger.info("STARTING VerifyRecharge between dates: "+ startDate.toString() +" to "+ endDate.toString());

		try{
			//Get all payment txn during given time window
	        List<PaymentTransaction> paymentTransactions =  paymentTransactionService.getAllPaymentTxnWithTimeLimit(startDate, endDate);

	        if (paymentTransactions == null) {
	    		logger.info("Returning paymentTransactions is null");
	            return;
	        }
	        
	        logger.info("paymentTransactions count is " + paymentTransactions.size());
	        //group payment txns by orderId
	        Map<String, List<PaymentTransaction>> paymnentsTxnGrpByOrderId = new HashMap<String, List<PaymentTransaction>>();
	        for(PaymentTransaction txn : paymentTransactions){
	        	String orderId = txn.getOrderId();
	        	if(paymnentsTxnGrpByOrderId.get(orderId) == null){
	        		paymnentsTxnGrpByOrderId.put(orderId, new ArrayList<PaymentTransaction>());
	        	}
	        	paymnentsTxnGrpByOrderId.get(orderId).add(txn);
	        }
	        
	        //iterate over map to validate payments and recharge for each orderId
	        for (Entry<String, List<PaymentTransaction>> entry : paymnentsTxnGrpByOrderId.entrySet())
	        {
	        	processOrderForRevenueLeak(entry.getKey(), entry.getValue());
	        }
		}
		catch(Exception e){
			logger.error("Exception occured in VerifyRecharge method in DetectRevenueJobUtil class");
		}
        logger.info("Returning from verifyRecharge");
     }
	
	private void processOrderForRevenueLeak(String orderId, List<PaymentTransaction> paymentTxns) throws IOException{
        logger.info("Entering into processOrderForRevenueLeak for orderId : " +orderId);

        try{
        	boolean rechargeAttempted = false;
        	boolean rechargeSuccess = false;
    		double rechargeAmount = 0;
        	InRequest inRequest = inService.getInRequest(orderId);
        	if(inRequest != null){
        		rechargeAttempted = true;
        		InResponse inResponse = inReadDAO.getInResponseByOrderID(orderId); 
        		rechargeAmount = inRequest.getAmount();
                 if(inResponse != null && inService.isSuccessful(inResponse)){
                	rechargeSuccess = true;                	
                }
        	}else{
        		BillPaymentStatus status = billPaymentService.findBillPaymentStatusByOrderId(orderId);
        		if(status != null){
            		rechargeAttempted = true;
            		rechargeAmount = status.getAmount().doubleValue(); 
            		if(status.getSuccess()){
            			rechargeSuccess = true;
            		}
        		}
        	}
        	
      		double amountPaidThroughBank = getAmountDepositedThroughBank(paymentTxns);
      		double amountPaidThroughWallet = getAmountPaidThroughWallet(paymentTxns);
      		List<WalletTransaction> walletTxns = getWalletTxns(paymentTxns);
      		
      		
      		double walletCreditbyPayment = getWalletAmountByFundSource(walletTxns, FundSource.CASH_FUND) +   		
      	  			getWalletAmountByFundSource(walletTxns, FundSource.RECHARGE_FUND);
      	  	double walletCreditByRefund = getWalletAmountByFundSource(walletTxns, FundSource.REFUND);
      	  	double walletCreditByCashback = getWalletAmountByFundSource(walletTxns, FundSource.CASHBACK);
      	  	double walletDebitByRecharge = getWalletAmtByFundDest(walletTxns, FundDestination.RECHARGE);
      	  	double walletDebitByrevoke = getWalletAmtByFundDest(walletTxns, FundDestination.REVOKE_FUND);
      	  		
      	  	double	netDebit =  walletDebitByRecharge + walletDebitByrevoke;
      	  	double	netCredit = walletCreditbyPayment + walletCreditByRefund +  walletCreditByCashback;
     
      		double netLoss = 0;
     		double couponAmount = getCouponAmount(orderId); 
    		
        	//no recharge attempted. Validate payment, wallet and coupons
        	if(!rechargeAttempted){
        		if(netCredit - netDebit > amountPaidThroughBank){
        			//wallet credit more than amount deposited through bank transaction
        			netLoss = (netCredit - netDebit) - (amountPaidThroughBank);
         			revenueLoss.add(new RevenueLeak(orderId, false, netLoss, RevenueLeakReason.EXCESS_WALLET_CREDIT_NO_RECHARGE));
        		}
         		if(couponAmount > 0){
        			//coupon assigned recharge failure
         			revenueLoss.add(new RevenueLeak(orderId, false, couponAmount, RevenueLeakReason.COUPON_ASSIGNED_NO_RECHARGE));
        		}
        	}else{
          		double amountPayable = pricingService.getPayableAmountByOrderId(orderId);
                if(rechargeSuccess){
                	if(amountPayable > amountPaidThroughWallet ){
                		//handle revenue leak
                		netLoss = amountPayable - amountPaidThroughWallet;
             			revenueLoss.add(new RevenueLeak(orderId, true, netLoss, RevenueLeakReason.LESS_AMOUNT_PAID_RECHARGE_SUCCESS));
                	}
             		netLoss = (netCredit - netDebit - walletCreditByCashback) - (amountPaidThroughBank - amountPaidThroughWallet);
                 	if(netLoss > 0 ){
             			revenueLoss.add(new RevenueLeak(orderId, true, netLoss, RevenueLeakReason.EXCESS_WALLET_CREDIT_RECHARGE_SUCCESS));
                	}
                	double maxCouponsAmt = getMaxCouponsAmt(orderId, rechargeAmount);
                	if(couponAmount > maxCouponsAmt){
                		netLoss = couponAmount - maxCouponsAmt;
             			revenueLoss.add(new RevenueLeak(orderId, true, netLoss, RevenueLeakReason.COUPON_ASSIGNED_MORE_THAN_AMOUNT_RECHARGE_SUCCESS));
                	}
                 	
                }else{
             		netLoss = (netCredit - netDebit - walletCreditByCashback) - amountPaidThroughBank ;
                 	if(netLoss > 0){
             			revenueLoss.add(new RevenueLeak(orderId, false, netLoss, RevenueLeakReason.EXCESS_WALLET_CREDIT_RECHARGE_FAILED));
                	}
                	if(couponAmount > 0){
            			//coupon assigned recharge failure
             			revenueLoss.add(new RevenueLeak(orderId, false, couponAmount, RevenueLeakReason.COUPON_ASSIGNED_RECHARGE_FAILED));
                	}
                 }	
        	}
        }
        catch(Exception e){
        	logger.error("Exception occured in processOrderForRevenueLeak for orderId : " +orderId,e);
        }
        logger.info("Exiting from processOrderForRevenueLeak for orderId : " +orderId);
     }
	
	private List<WalletTransaction> getWalletTxns(List<PaymentTransaction> paymentTxns){
        logger.info("Entering into getWalletTxns");
		List<WalletTransaction> walletTxns = new ArrayList<WalletTransaction>();
		Set<String> paymentAttemptIds = new TreeSet<String>();
		for(PaymentTransaction txn : paymentTxns){
			paymentAttemptIds.add(txn.getMerchantTxnId());
		}
 		for(String paymentAttemptId : paymentAttemptIds){
			List<WalletTransaction> txn = walletService.findTransaction(paymentAttemptId, TransactionType.ANY);
			if(txn != null || txn.size() > 0){
				walletTxns.addAll(txn);
			}
		} 
        logger.info("Exiting from  getWalletTxns");
		return walletTxns;
 	}
	
	//net amount of successful txns in payment_txn table where gateway is not wallet
	//This amount is deposited to wallet
	private double getAmountDepositedThroughBank(List<PaymentTransaction> paymentTxns){
		logger.info("Entering into getAmountDepositedThroughBank");
		double totalAmountPaid = 0;
		for(PaymentTransaction paymentTxn : paymentTxns){
			if(paymentTxn.getIsSuccessful() && !paymentTxn.getPaymentGateway().equals("fw")){
				totalAmountPaid += paymentTxn.getAmount();
			}
		}
		logger.info("Exiting from getAmountDepositedThroughBank");
		return totalAmountPaid;
	}
	
	//net amount of successful txns in payment_txn table where gateway is wallet
	//This is the actual amount that is paid for recharge;
	private double getAmountPaidThroughWallet(List<PaymentTransaction> paymentTxns){
		logger.info("Enteing into getAmountPaidThroughWallet");
		double totalAmountPaid = 0;
		for(PaymentTransaction paymentTxn : paymentTxns){
			if(paymentTxn.getIsSuccessful() && paymentTxn.getPaymentGateway().equals("fw")){
				totalAmountPaid += paymentTxn.getAmount();
			}
		}
		logger.info("Exiting into getAmountPaidThroughWallet");
		return totalAmountPaid;
	}
	
	private double getCouponAmount(String orderId){
		logger.info("Entreing into getCouponAmount for orderId " + orderId);

		List<String> orderIds = new ArrayList<String>();
		orderIds.add(orderId);

		List<CouponHistory> coupons =  couponServiceProxy.getCoupons(orderIds);
		double totalCouponAmt = 0;
		if(coupons!=null) {
			for(CouponHistory coupon : coupons){
				totalCouponAmt +=coupon.getCouponValue();
			}
		}
		logger.info("Exiting into getCouponAmount for orderId "+ orderId);
		return totalCouponAmt;
	}
	
	double getMaxCouponsAmt(String orderId, double rechargeAmt){
		logger.info("Entreing into getMaxCouponsAmt for orderId " + orderId);

	    CartBusinessDo cart = cartService.getCartByOrderId(orderId);
		cartService.getCouponScaleUpFactor(cart);
		int couponScaleUpFactor = cartService.getCouponScaleUpFactor(cart);
		double maxCouponAmt  = 0;
 		maxCouponAmt = rechargeAmt - rechargeAmt%50 + 50;
		logger.info("Exiting into getMaxCouponsAmt for orderId "+ orderId);
		return maxCouponAmt * couponScaleUpFactor;
	}
	
	private void validateWalletFundSourceAndDestination(Date from, Date to){
		 List<WalletTransaction> walletTxns = walletService.getWalletTxnByDateRange(from, to);
  		 
		 if( walletTxns!= null && walletTxns.size() > 0){
			 //handle invalid fund source
			 for(WalletTransaction txn : walletTxns){
				 try{
					 if(txn.getTransactionType().equals("deposit") && isNullOrEmpty(txn.getFundSource())){
	 					 logger.info(txn.getOrderId()+ " " + txn.getTransactionType() + "  "+ txn.getFundSource() + "\n");
	 					orderIdwithInvalidFundSrc.add(txn.getOrderId());
					 }else if(txn.getTransactionType().equals("withdrawal") && isNullOrEmpty(txn.getFundDestination())){
	 					 logger.info(txn.getOrderId()+ " " + txn.getTransactionType() + "  "+ txn.getFundDestination() + "\n");
						 orderIdwithInvalidFundDes.add(txn.getOrderId()) ;
					 }
				 }catch(Exception e){
					 logger.error("Exception in validateWalletFundSourceAndDestination for txn = " + txn);
				 }
  				 
			 }
		 }
	}
	
	private boolean isNullOrEmpty(String str){
		if(str == null || str.trim().length() == 0 || str.equals("NULL"))
			return true;
		else
			return false;
	}
	
	private double getWalletAmountByFundSource(List<WalletTransaction> walletTxns , FundSource source){
		double amount = 0;
		for(WalletTransaction txn : walletTxns){
			if( txn.getFundSource() != null && txn.getFundSource().equals(source.toString()))
				amount += txn.getTransactionAmount();
		}
		return amount;
	}
	
	private double getWalletAmtByFundDest(List<WalletTransaction> walletTxns, FundDestination destination){
		double amount = 0;
		for(WalletTransaction txn : walletTxns){
			if(txn.getFundDestination()!= null && txn.getFundDestination().equals(destination.toString()))
				amount += txn.getTransactionAmount();
		}
		return amount;
	}
	
	private void sendEmail(Date startDate, Date endDate){
		logger.info("Entering into SendEmail");
		try{
			//group payment txns by orderId
	        Map<RevenueLeakReason, List<RevenueLeak>> revenueLeakOrdersByType = new HashMap<RevenueLeakReason, List<RevenueLeak>>();
	        for(RevenueLeak order : revenueLoss){
	        	RevenueLeakReason reason = order.getReason();
	        	if(revenueLeakOrdersByType.get(reason) == null){
	        		revenueLeakOrdersByType.put(reason, new ArrayList<RevenueLeak>());
	        	}
	        	revenueLeakOrdersByType.get(reason).add(order);
	        }
	        
			String br = "<br>\n";
			String space = " ";
			StringBuilder content = new StringBuilder();
			content.append("Revenue Leak Orders by type" + br + br);
	         
	        for (Map.Entry<RevenueLeakReason, List<RevenueLeak>> entry : revenueLeakOrdersByType.entrySet()) {
	        	RevenueLeakReason key = entry.getKey();
	        	List<RevenueLeak> values = entry.getValue();
	        	double netLoss = 0;
	        	content.append(br);
	        	content.append(key);
	        	content.append(br);
	            for(RevenueLeak val : values){
	            	netLoss += val.getNetLoss();
	            	content.append(val.getOrderId() + space + val.getNetLoss() + br);
	            }
	            content.append("Net Loss for "+ key +space + netLoss);
	        }
	        
			content.append(br + br + "Orders with invalidFundSoure" + br + br);
			for(String orderId : orderIdwithInvalidFundSrc){
				content.append(orderId + ",");
			}
			
			content.append(br + br+ "Orders with invalidFundDestination" + br + br);
			for(String orderId : orderIdwithInvalidFundDes){
				content.append(orderId + ",");
			}
			
	        EmailBusinessDO ebdo = new EmailBusinessDO();
	        ebdo.setFrom(fcProperties.getProperty(FCConstants.REVENUELEAK_EMAIL_NOTIFICATION_FROM));
			ebdo.setReplyTo(fcProperties.getProperty(FCConstants.REVENUELEAK_EMAIL_NOTIFICATION_FROM));
			ebdo.setTo(fcProperties.getProperty(FCConstants.REVENUELEAK_EMAIL_NOTIFICATION_TO));
 	 		ebdo.setSubject("Revenue Leak Report " + startDate + " - " + endDate);
	 		ebdo.setContent(content.toString());
	 		logger.info(content.toString());
			emailService.sendNonVelocityEmail(ebdo);			
		}catch(Exception e){
			logger.error("Exception occured in SendEmail", e);
		}
		logger.info("Exiting from SendEmail");
	}
	
}
