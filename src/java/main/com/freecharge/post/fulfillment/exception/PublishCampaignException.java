package com.freecharge.post.fulfillment.exception;

public class PublishCampaignException extends Exception{

	
	private static final long serialVersionUID = 1L;

	public PublishCampaignException() {
		super();
	}

	public PublishCampaignException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PublishCampaignException(String message, Throwable cause) {
		super(message, cause);
	}

	public PublishCampaignException(String message) {
		super(message);
	}

	public PublishCampaignException(Throwable cause) {
		super(cause);
	}

	
}
