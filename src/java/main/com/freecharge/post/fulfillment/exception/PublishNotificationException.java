package com.freecharge.post.fulfillment.exception;

public class PublishNotificationException extends Exception{

	
	private static final long serialVersionUID = 1L;

	public PublishNotificationException() {
		super();
	}

	public PublishNotificationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PublishNotificationException(String message, Throwable cause) {
		super(message, cause);
	}

	public PublishNotificationException(String message) {
		super(message);
	}

	public PublishNotificationException(Throwable cause) {
		super(cause);
	}

	
}
