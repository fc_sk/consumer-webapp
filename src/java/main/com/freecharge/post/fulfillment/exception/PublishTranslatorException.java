package com.freecharge.post.fulfillment.exception;

public class PublishTranslatorException extends Exception{

	
	private static final long serialVersionUID = 1L;

	public PublishTranslatorException() {
		super();
	}

	public PublishTranslatorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PublishTranslatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public PublishTranslatorException(String message) {
		super(message);
	}

	public PublishTranslatorException(Throwable cause) {
		super(cause);
	}

	
}
