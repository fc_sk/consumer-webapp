package com.freecharge.post.fulfillment.state;

public final class PostFulfillmentConstants {

	public static final String SUCCESS_DOER = "DoneBySkippedActivity";
	public static final String FAILED_DOER_RETRY_EXHAUST = "FailedMarkedBySkippedActivity, retry exhaust";
	public static final String FAILED_DOER_INVALID_ENTRY = "FailedMarkedBySkippedActivity, Invalid Entry";

	
}
