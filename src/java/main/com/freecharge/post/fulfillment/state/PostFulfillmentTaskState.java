package com.freecharge.post.fulfillment.state;

public enum PostFulfillmentTaskState {
	CAMPAIGN_NOTIFICATION_MISSED("Missed Campaign: %s"), CAMPAIGN_NOTIFICATION_IN_PROGRESS ("InProgress Campaign: %s"),
	CAMPAIGN_NOTIFICATION_PROCESSED("Processed Campaign: %s"), CAMPAIGN_NOTIFICATION_FAILED ("Failed Campaign: %s"), 
	COMM_NOTIFICATION_MISSED("Missed Campaign: %s"), COMM_NOTIFICATION_IN_PROGRESS("InProgress Failed: %s"),
	COMM_NOTIFICATION_PROCESSED("Processed Comm: %s"), COMM_NOTIFICATION_FAILED("Comm Failed: %s"), 
	TSM_NOTIFICATION_MISSED("Missed TSM: %s"), TSM_NOTIFICATION_IN_PROGRESS("InProgess TSM: %s"),
	TSM_NOTIFICATION_PROCESSED("Processed TSM: %s"), TSM_NOTIFICATION_FAILED("Failed TSM: %s");
	
	private String msg;

	private PostFulfillmentTaskState(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

}
