package com.freecharge.google;

public class GoogleCreditsDO {
	private String googleSerial;
	private String googleCode;
	private String orderId;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getGoogleSerial() {
		return googleSerial;
	}
	public void setGoogleSerial(String googleSerial) {
		this.googleSerial = googleSerial;
	}
	public String getGoogleCode() {
		return googleCode;
	}
	public void setGoogleCode(String googleCode) {
		this.googleCode = googleCode;
	}
	
	
}
