package com.freecharge.google;

import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;

/**
 * @author fcaa17922
 * The cache is used to hold google credits, which are received from Euronet
 * with timeout of 5 minutes.
 * 
 *
 */
@Component
public class GoogleCreditsCache extends RedisCacheManager{
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
    private RedisTemplate<String, Object> googleCreditsCacheTemplate;
	
	private static final String GOOGLE_CREDITS_CACHE_PREFIX = "googleCreditsCache_";
	
	
	@Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        this.googleCreditsCacheTemplate.setValueSerializer(new GenericToStringSerializer<>(String.class));
        return this.googleCreditsCacheTemplate;
    }
	
	/**
	 * Wrapper method used to set the cache value for google credits for display purpose.
	 * @param orderId
	 * @param googleSerial
	 * @param googleCode
	 */
	public void noteGoogleCredits(String orderId, String googleSerial, String googleCode) {
        recordAndSetExpiry(orderId, googleSerial, googleCode);
    }
	
	/**
	 * Used to create a key to set and set the cache value
	 * @param orderId
	 * @param googleSerial
	 * @param googleCode
	 */
	private void recordAndSetExpiry(String orderId, String googleSerial, String googleCode) {
		String cacheKey =  getGoogleCacheKey(orderId);
		String cacheValue =  googleSerial+"_"+googleCode;
		try {
        	setGoogleCreditsCache(cacheKey, cacheValue);
        } catch (Exception e) {
            logger.error("Exception while setting [" + cacheKey + "]", e);
        }
    }

	/**
	 * Used to set the value to the cache with an expiry time
	 * @param cacheKey
	 * @param cacheValue
	 */
	private void setGoogleCreditsCache(String cacheKey,  String cacheValue) {
    	Date expireAt = new DateTime().plusMinutes(5).toDate();
        this.set(cacheKey, cacheValue);
        this.expireAt(cacheKey, expireAt);
        logger.info("Cache successfully added with key ["+cacheKey+"] and cache value as:["+cacheValue+"] and expiry date as ["+expireAt+"]");
    }
	
	/**
	 * Used to get the google cache value using a key aka orderId
	 * @param orderId
	 * @return
	 */
	public GoogleCreditsDO getGoogleCreditsFromCache(String orderId) {
		String keyToFetch = getGoogleCacheKey(orderId);
		GoogleCreditsDO googleCreditsDO = null;
		try {
			Object obj = get(keyToFetch);
			if (obj != null) {
				String gCreditsStrValue = (String) obj;
				googleCreditsDO = getObjectFromString(gCreditsStrValue);
			}
			return googleCreditsDO;
		} catch (Exception e) {
			logger.error("An error occurred trying to fetch the cache value using key : [" + orderId + "]", e);
		}
		return null;
	}
	
	/**
	 * Used to convert an string to an object, in this case the Google Credits Object
	 * @param value
	 * @return
	 */
	private GoogleCreditsDO getObjectFromString(String value){
		GoogleCreditsDO googleCreditsDO = null;
		String[] splitStr = value.split("_");
		if(splitStr.length == 2){
			googleCreditsDO = new GoogleCreditsDO();
			googleCreditsDO.setGoogleSerial(splitStr[0]);
			googleCreditsDO.setGoogleCode(splitStr[1].replace("-", " "));
		}
		return googleCreditsDO;
	}
	
	public void clearGoogleCrditsCache(String orderId) {
        String cacheKey = getGoogleCacheKey(orderId);
        delete(cacheKey);
    }
	
	private String getGoogleCacheKey(String orderId){
		return GOOGLE_CREDITS_CACHE_PREFIX+orderId;
	}
}
