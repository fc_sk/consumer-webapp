package com.freecharge.google;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.PostRechargeService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.web.webdo.PostRechargeDetailsWebDo;
import com.freecharge.web.webdo.RechargeInitiateWebDo;

/**
 * Service class used for Google Credits 
 * Used to get the post recharge details and the google credits from the cache 
 * @author fcaa17922
 *
 */
@Service
public class GoogleCreditsService {
	private final Logger  logger = LoggingFactory.getLogger(getClass());
	
	private static final String SUCCESS_ACTION = "success";
	
	@Autowired
	private GoogleCreditsCache googleCreditsCache;
	
	@Autowired
    private InService inService;
	
	@Autowired
	private RechargeSchedulerService rechargeSchedulerService;
	
	@Autowired
    AggregatorFactory aggregatorFactory;
	
	@Autowired
	private PostRechargeService postRechargeService;
	
	
	/**
	 * Used to fetch the google credits post recharge details to display on the front end
	 * @param orderId
	 * @param rechargeInitiateWebDo
	 * @param rechargeDetails
	 * @return Map<String, Object> 
	 * 							- return the google credits post recharge data
	 */
	public Map<String, Object> getGoogleCreditsPostRechargeData(String orderId, RechargeDetails rechargeDetails,
			RechargeInitiateWebDo rechargeInitiateWebDo) {
		Map<String, Object> attributeMap = new HashMap<>();
		if (!FCUtil.isGoogleCreditRechargeType(rechargeInitiateWebDo.getProductType())) {
			return attributeMap;
		}
		PostRechargeDetailsWebDo postRechargeDetails = postRechargeService.getGoogleCreditsPostRechargeDetails(orderId, rechargeInitiateWebDo, rechargeDetails);
		if(postRechargeDetails != null){
			attributeMap.put("actionBucket", postRechargeDetails.getActionBucket());
			attributeMap.put("title", postRechargeDetails.getTitle());
			attributeMap.put("reasonMsg", postRechargeDetails.getReasonMsg());
			attributeMap.put("nextMsg", postRechargeDetails.getNextMsg());
			attributeMap.put("how_to_use_web", postRechargeDetails.getHowToUseWeb());
			attributeMap.put("how_to_use_android", postRechargeDetails.getHowToUseAndroid());
		}
		if (postRechargeDetails != null && SUCCESS_ACTION.equalsIgnoreCase(postRechargeDetails.getActionBucket())) {
			GoogleCreditsDO creditsDO = getGoogleCredits(orderId);
			if (creditsDO != null) {
				attributeMap.put("googleSerial", creditsDO.getGoogleSerial());
				attributeMap.put("googleCode", creditsDO.getGoogleCode());
			}
		}
		
		return attributeMap;
	}

	/**
	 * Used to get the google credits details from the cache
	 * @param orderId
	 * @return GoogleCreditsDO 
	 * 				- the google credits object to return
	 */
	public GoogleCreditsDO getGoogleCredits(String orderId){
		logger.info("Inside the get google credits method for orderId : [" + orderId + "]");
		return googleCreditsCache.getGoogleCreditsFromCache(orderId);
	}
	
	/**
	 * Used to save the google credits code to cache
	 * @param orderId
	 * @param googleSerial
	 * @param googleCode
	 */
	public void saveGoogleCreditsCache(String orderId, String googleSerial, String googleCode){
		googleCreditsCache.noteGoogleCredits(orderId, googleSerial, googleCode);
	}
	
	
	/**
	 * Used to get the google code and serial from Euronet in case of trigger from admin panel
	 * @param orderId
	 */
	public boolean triggerGoogleCodeResend(String orderId) {
		try {
			InTransactionData inTransaction = inService.getLatestInTransactionData(orderId);
			if (inTransaction == null || inTransaction.getInRequest() == null
					|| inTransaction.getInResponse() == null) {
				logger.error("No recharge transaction data found for [" + orderId + "]. Not initiating status check");
			}
			InResponse initialResponse = inTransaction.getInResponse();

			logger.info("Processing Google Code refetch for [ " + initialResponse.getAggrName()
					+ "]: aggregator tranasction ID: " + initialResponse.getAggrName() + " with initial response of : "
					+ initialResponse.getAggrRespCode());

			RechargeDo rechargeDo = rechargeSchedulerService.createRechargeDO(initialResponse);
			return rechargeSchedulerService.refetchGoogleCredits(initialResponse, rechargeDo, aggregatorFactory);
		} catch (Exception e) {
			logger.error("An error occurred trying to get the current status from aggregatr", e);
		}
		return false;

	}
}
