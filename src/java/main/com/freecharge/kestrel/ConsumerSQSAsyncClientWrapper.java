package com.freecharge.kestrel;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;

public class ConsumerSQSAsyncClientWrapper
{

    public ConsumerSQSAsyncClientWrapper()
    {
    	
    }

    public static AmazonSQSAsyncClient get(BasicAWSCredentials awsCredentials, String regionName)
    {
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxConnections(200);
        
        AmazonSQSAsyncClient amazonSQSAsyncClient = (AmazonSQSAsyncClient) AmazonSQSAsyncClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(regionName)
        		.withClientConfiguration(configuration).build();
        
        return amazonSQSAsyncClient;
    }
}