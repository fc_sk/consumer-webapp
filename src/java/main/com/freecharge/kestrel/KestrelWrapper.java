package com.freecharge.kestrel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.util.FCUtil;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.kestrel.dao.KestrelQueueDao;
import com.freecharge.payment.services.binrange.CardBin;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.platform.QueueConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.exception.RechargeEnqueFailedException;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

import flexjson.JSONSerializer;

@Component
/**
 * A wrapper class for queueing
 */
public class KestrelWrapper {
    private JSONSerializer jsonSerializer = new JSONSerializer();
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    @Qualifier("producerSQSClient")
    AmazonSQSAsync sqsClient;
    
    
    @Autowired
    @Qualifier("producerSQSMumbaiClient")
    AmazonSQSAsync sqsMumbaiClient;
    
    @Autowired
    AppConfigService appConfig;
        
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private KestrelQueueDao kestrelQueueDao;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private RechargeStatusIdempotencyCache idempotencyMap;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private PaymentRefund paymentRefund;

    private final int PAYMENT_SYNC_QUEUE_LINGER_PERIOD = 60;
    
    public void enqueueToSQS(Map<String, String> dataMap, String queueName, int timeOut) {
        dataMap.put(QueueConstants.ENTRY_TIME, String.valueOf(System.currentTimeMillis()));
        String payLoad = serializeDataMap(dataMap);
              
        long startTime = System.currentTimeMillis();
        this.logger.debug("About to enqueue payLoad: [" + payLoad + "] into SQS queue: [" + queueName + "]");
        
        SendMessageRequest sendRequest = new SendMessageRequest();
        sendRequest.setQueueUrl(this.getSQSQueueEndPoint(queueName)); 
        sendRequest.setMessageBody(payLoad);
       
        logger.info("queue endpoint : " + sendRequest.getQueueUrl());
        try {
            this.sqsClient.sendMessage(sendRequest);
            
            this.metricsClient.recordLatency("SQS", "EnqueueLatency." + String.valueOf(queueName), System.currentTimeMillis() - startTime);
            this.logger.info("Successfully enqueued payLoad: [" + payLoad + "] into SQS queue: [" + this.getSQSQueueEndPoint(queueName) + "]. ");
        } catch (Exception e) {
            this.metricsClient.recordEvent("SQS", "Enqueue." + queueName, "Error");
            this.kestrelQueueDao.logEnqueueFailure(queueName, payLoad.toString());
            this.logger.error(String.format("SQS Queue Error:Error while queuing %s in %s", payLoad, queueName), e);
            if (this.fcProperties.getRechargeQueue().equals(queueName)) {
                throw new RechargeEnqueFailedException(e);
            }
        }
    }
    
    public void enqueueToMumbaiSQS(Map<String, String> dataMap, String queueName, int timeOut) {
        dataMap.put(QueueConstants.ENTRY_TIME, String.valueOf(System.currentTimeMillis()));
        String payLoad = serializeDataMap(dataMap);
              
        long startTime = System.currentTimeMillis();
        this.logger.debug("About to enqueue payLoad: [" + payLoad + "] into SQS queue: [" + queueName + "]");
        
        SendMessageRequest sendRequest = new SendMessageRequest();
        sendRequest.setQueueUrl(this.getSQSQueueMumbaiEndPoint(queueName)); 
        sendRequest.setMessageBody(payLoad);
       
        logger.info("queue endpoint : " + sendRequest.getQueueUrl());
        try {
            this.sqsMumbaiClient.sendMessage(sendRequest);
            
            this.metricsClient.recordLatency("SQS", "EnqueueLatency." + String.valueOf(queueName), System.currentTimeMillis() - startTime);
            this.logger.info("Successfully enqueued payLoad: [" + payLoad + "] into SQS queue: [" + this.getSQSQueueMumbaiEndPoint(queueName) + "]. ");
        } catch (Exception e) {
            this.metricsClient.recordEvent("SQS", "Enqueue." + queueName, "Error");
            this.kestrelQueueDao.logEnqueueFailure(queueName, payLoad.toString());
            this.logger.error(String.format("SQS Queue Error:Error while queuing %s in %s", payLoad, queueName), e);
            if (this.fcProperties.getRechargeQueue().equals(queueName)) {
                throw new RechargeEnqueFailedException(e);
            }
        }
    }
    
	private String serializeDataMap(Map<String, String> dataMap) {
		Gson gsonObj = new Gson();
		return gsonObj.toJson(dataMap);
	}

	public void enqueueFulfillment(Map<String, String> dataMap) {
        if (!idempotencyMap.isInQueue(dataMap.get(PaymentConstants.ORDER_ID_KEY), fcProperties.getFulfillmentQueue())) {
            idempotencyMap.markEnqueue(dataMap.get(PaymentConstants.ORDER_ID_KEY), fcProperties.getFulfillmentQueue());
            enqueueToMumbaiSQS(dataMap, fcProperties.getFulfillmentQueue(), fcProperties.getKestrelTimeOut());
        } else {
            logger.info("Order ID is in fulfillment queue; not going ahead with enqueue:["
                    + String.valueOf(dataMap.get(PaymentConstants.ORDER_ID_KEY)) + "]");
        }
    }
    
    private void enqueueRecharge(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getRechargeQueue(), fcProperties.getKestrelTimeOut());
    }
    
    private void enqueueHCoupon(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getHCouponQueue(), fcProperties.getKestrelTimeOut());
    }
    
    private void enqueueRechargeRetry(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getRechargeRetryQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueRechargeAsync(String orderId) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getRechargeQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getRechargeQueue());
            
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
            
            this.enqueueRecharge(queueItem);
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
        }
    }
    
    public void enqueueRechargeRetryAsync(String orderId) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getRechargeRetryQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getRechargeRetryQueue());
            
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
            enqueueRechargeRetry(queueItem);
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue in recharge retry:[" + String.valueOf(orderId) + "]");
        }
    }

    private void enqueuePaySyncData(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getPaySyncQueue(), fcProperties.getKestrelTimeOut());
    }
    

    public void enqueueForSyncPayment(String payLoad, String activityType) {
        String setKey = payLoad + "_" + activityType;
        if (!idempotencyMap.isInQueue(setKey.toString(), fcProperties.getPaySyncQueue())) {
            idempotencyMap.markEnqueue(setKey.toString(), fcProperties.getPaySyncQueue(), PAYMENT_SYNC_QUEUE_LINGER_PERIOD, TimeUnit.MINUTES);

            Map<String, String> queueItem = new HashMap<>();
            queueItem.put(PaymentConstants.PAY_TXN_ID, payLoad);    
            queueItem.put(FCConstants.ACTIVITY_TYPE, activityType);
            
            enqueuePaySyncData(queueItem);
        }
    }
    
    private void enqueueCardBinRangeInsert(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getCardBinQueue(), fcProperties.getKestrelTimeOut());
    }
    

    public void enqueueForCardBinRangeInsert(CardBin cardBin, Boolean payStatus) {
        try {
            String binHead = StringUtils.left(cardBin.getCardBin(), 6);
            String binTail = StringUtils.right(cardBin.getCardBin(), 4);
            if (binHead == null || binTail == null) {
                metricsClient.recordEvent("Payment", "Binlist", "nullBins");
                return;
            }
            Map<String, String> queueItem = new HashMap<>();
            queueItem.put(PaymentConstants.CARD_BIN_HEAD, binHead);
            queueItem.put(PaymentConstants.CARD_BIN_TAIL, binTail);
            queueItem.put(PaymentConstants.PAYMENT_STATUS, String.valueOf(payStatus));
            enqueueCardBinRangeInsert(queueItem);
        } catch (Exception e) {
            metricsClient.recordEvent("Payment", "Binlist", "consumerFail");
        }
    }

    public void enqueuePostpaidFulfillment(String orderId) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getPostpaidFulfillmentQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getPostpaidFulfillmentQueue());
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
            enqueueToMumbaiSQS(queueItem, fcProperties.getPostpaidFulfillmentQueue(), fcProperties.getKestrelTimeOut());
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
        }
    }

    public void enqueueBillayFulfillment(String orderId, boolean nodalFlag) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getBillpayFulfillmentQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getBillpayFulfillmentQueue());
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
            queueItem.put(PaymentConstants.NODAL_FLAG, String.valueOf(nodalFlag));
            enqueueToMumbaiSQS(queueItem, fcProperties.getBillpayFulfillmentQueue(), fcProperties.getKestrelTimeOut());
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
        }
    }

    
    public void enqueueForFFNotificationRetry(String orderId) {
        logger.info("enqueuing:[" + String.valueOf(orderId) + "]");

        try {
        	if (!idempotencyMap.isInQueue(orderId, fcProperties.getFFNotificationRetryQueue())) {
                idempotencyMap.markEnqueue(orderId, fcProperties.getFFNotificationRetryQueue());
                Map<String, String> queueItem = new HashMap<String, String>();
                queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
                enqueueToMumbaiSQS(queueItem, fcProperties.getFFNotificationRetryQueue(), fcProperties.getKestrelTimeOut());
                logger.info("enqued:[" + String.valueOf(orderId) + "]");

            } else {
                logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
            }
        }catch (Exception e) {
            logger.info("Exception enqueuing :[" + String.valueOf(orderId) + "]");
		}
    }
    
    public void enqueueForFFSkippedTask(String orderId) {
        logger.info("enqueuing:[" + String.valueOf(orderId) + "]");

        try {
        	if (!idempotencyMap.isInQueue(orderId, fcProperties.getSkippedFFTaskQueue())) {
                idempotencyMap.markEnqueue(orderId, fcProperties.getSkippedFFTaskQueue());
                Map<String, String> queueItem = new HashMap<String, String>();
                queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
                enqueueToMumbaiSQS(queueItem, fcProperties.getSkippedFFTaskQueue(), fcProperties.getKestrelTimeOut());
                logger.info("enqued:[" + String.valueOf(orderId) + "]");

            } else {
                logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
            }
        }catch (Exception e) {
            logger.info("Exception enqueuing :[" + String.valueOf(orderId) + "]");
		}
    }
    
    
    public void enqueueForUnknownStatusProcessing(String orderId) {
        logger.info("enqueuing:[" + String.valueOf(orderId) + "]");

        try {
        	if (!idempotencyMap.isInQueue(orderId, fcProperties.getUnknownStatusProcessorQueue())) {
                idempotencyMap.markEnqueue(orderId, fcProperties.getUnknownStatusProcessorQueue());
                Map<String, String> queueItem = new HashMap<String, String>();
                queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
                enqueueToMumbaiSQS(queueItem, fcProperties.getUnknownStatusProcessorQueue(), fcProperties.getKestrelTimeOut());
                logger.info("enqued:[" + String.valueOf(orderId) + "]");

            } else {
                logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
            }
        }catch (Exception e) {
            logger.info("Exception enqueuing :[" + String.valueOf(orderId) + "]");
		}
    }

    public void decideAndEnqueueRefund(String merchantTxnId, Double refundAmount, String refundType) {
        Map<String, String> queueItem = new HashMap<String, String>();
        queueItem.put(PaymentConstants.ORDER_ID_KEY, merchantTxnId);
        queueItem.put(PaymentConstants.REFUND_TYPE_KEY, refundType);
        if(shouldEnqueueForNewRefund() && paymentRefund.isEligibleForNewRefund(merchantTxnId)) {
            queueItem.put(PaymentConstants.REFUND_AMOUNT_KEY, Double.toString(refundAmount));
            enqueueNewRefund(queueItem);
        } else {
            enqueueRefund(queueItem);
        }
    }

    public void enqueueNewRefund(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getNewRefundQueue(), fcProperties.getKestrelTimeOut());
    }

    public void enqueueNewRefundWithParams(String mtxnId, String refundType, Double refundAmount) {
        Map<String, String> queueItem = new HashMap<String, String>();
        queueItem.put(PaymentConstants.ORDER_ID_KEY, mtxnId);
        queueItem.put(PaymentConstants.REFUND_TYPE_KEY, refundType);
        queueItem.put(PaymentConstants.REFUND_AMOUNT_KEY, Double.toString(refundAmount));
        enqueueNewRefund(queueItem);
    }

    public void enqueueRefund(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getRefundQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueForHCouponFailure(String merchantTransactionId) {
        Map<String, String> queueItem = new HashMap<String, String>();
        queueItem.put(PaymentConstants.ORDER_ID_KEY, merchantTransactionId);
        queueItem.put(PaymentConstants.REFUND_TYPE_KEY, PaymentConstants.HCOUPON_REFUND);

        this.enqueueRefund(queueItem);
    }
    
    public void enqueuePaymentRefund(String merchantTransactionId, Double refundAmount) {
        decideAndEnqueueRefund(merchantTransactionId, refundAmount, PaymentConstants.PAYMENT_SUCCESS);
    }
    
    public void enqueueBillPaymentRefund(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getBillPaymentRefundQueue(), fcProperties.getKestrelTimeOut());
    }

    public void enqueueTest(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getTestQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueReconRequest(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getReconQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueHCouponForFulfilment(String orderId) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getHCouponQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getHCouponQueue());
            
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
            
            this.enqueueHCoupon(queueItem);;
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + String.valueOf(orderId) + "]");
        }
    }
    
    public void enqueueForStatusCheck(String orderId, StatusCheckType statusCheckType) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getStatusCheckQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getStatusCheckQueue());
            Map<String, String> statusCheckRequest = new HashMap<>();
            statusCheckRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
            statusCheckRequest.put(QueueConstants.STATUS_CHECK_TYPE, statusCheckType.toString());
            
            enqueueToMumbaiSQS(statusCheckRequest, fcProperties.getStatusCheckQueue(), fcProperties.getKestrelTimeOut());
        }
    }
    
    public void enqueueForQuickStatusCheck(String orderId, StatusCheckType statusCheckType) {
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getQuickStatusCheckQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getQuickStatusCheckQueue());
            Map<String, String> statusCheckRequest = new HashMap<>();
            statusCheckRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
            statusCheckRequest.put(QueueConstants.STATUS_CHECK_TYPE, statusCheckType.toString());
            
            enqueueToMumbaiSQS(statusCheckRequest, fcProperties.getQuickStatusCheckQueue(), fcProperties.getKestrelTimeOut());
        }
    }
    
    public void enqueueForBillPayStatusCheck(String billId, String lookupid, StatusCheckType statusCheckType) {
        if (!idempotencyMap.isInQueue(billId, fcProperties.getBillPayStatusCheckQueue())) {
            idempotencyMap.markEnqueue(billId, fcProperties.getBillPayStatusCheckQueue());
            Map<String, String> statusCheckRequest = new HashMap<>();
            statusCheckRequest.put(PaymentConstants.BILL_ID_KEY, billId);
            statusCheckRequest.put(PaymentConstants.LOOKUP_ID_KEY, lookupid);
            statusCheckRequest.put(QueueConstants.STATUS_CHECK_TYPE, statusCheckType.toString());
            
            enqueueToMumbaiSQS(statusCheckRequest, fcProperties.getBillPayStatusCheckQueue(), fcProperties.getKestrelTimeOut());
        }
    }

    private String getSQSQueueEndPoint(String queueName) {
        final String queueEndPoint =  fcProperties.getConsumerSQSQueueEndPoint() + 
                "/" + appConfig.getStackID() + "_" + queueName;
        return queueEndPoint;
    }
    
    private String getSQSQueueMumbaiEndPoint(String queueName) {
        final String queueEndPoint =  fcProperties.getConsumerSQSQueueMumbaiEndPoint() + 
                "/" + appConfig.getStackID() + "_" + queueName;
        return queueEndPoint;
    }
    
    public void enqueueForPostpaidStatusCheck(String orderId) {
        logger.info("start with bill payment status check order id for enqueue " +  orderId);
        if (!idempotencyMap.isInQueue(orderId, fcProperties.getPostpaidStatusCheckQueue())) {
            idempotencyMap.markEnqueue(orderId, fcProperties.getPostpaidStatusCheckQueue());
            Map<String, String> statusCheckRequest = new HashMap<>();
            statusCheckRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
            enqueueToMumbaiSQS(statusCheckRequest, fcProperties.getPostpaidStatusCheckQueue(), fcProperties.getKestrelTimeOut());
        } else {
            logger.info("order id is already en queued " +  orderId);
        }
    }

    public void enqueueNotification(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getNotificationQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueIVRSCall(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getIVRSQueue(), fcProperties.getKestrelTimeOut());
    }

    public void enqueueReward(RechargeRequestWebDo rechargeRequest) {
        if (!idempotencyMap.isInQueue(rechargeRequest.getOrderId(), fcProperties.getRechargeAsRewardQueue())) {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + rechargeRequest.getOrderId() + "]");
            idempotencyMap.markEnqueue(rechargeRequest.getOrderId(), fcProperties.getRechargeAsRewardQueue());
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put("requestId", rechargeRequest.getOrderId());
            
            this.enqueueRewardRecharge(queueItem);
        } else {
            logger.info("Order ID is in queue; not going ahead with enqueue:[" + rechargeRequest.getOrderId() + "]");
        }
    }
    
    private void enqueueRewardRecharge(Map<String, String> dataMap) {       
        String payLoad = serializeDataMap(dataMap);
        String queueName = fcProperties.getRechargeAsRewardQueue();
        long startTime = System.currentTimeMillis();
        this.logger.info("About to enqueue payLoad: [" + payLoad + "] into SQS queue: [" + queueName + "]");

        SendMessageRequest sendRequest = new SendMessageRequest();
        sendRequest.setQueueUrl(this.getSQSQueueMumbaiEndPoint(queueName));
        sendRequest.setMessageBody(payLoad);

        try {
            this.sqsMumbaiClient.sendMessage(sendRequest);

            this.metricsClient.recordLatency("SQS", "EnqueueLatency." + String.valueOf(queueName),
                    System.currentTimeMillis() - startTime);
            this.logger.info("Successfully enqueued payLoad: [" + payLoad + "] into SQS queue: ["
                    + this.getSQSQueueMumbaiEndPoint(queueName) + "]. ");
        } catch (Exception e) {
            this.metricsClient.recordEvent("SQS", "Enqueue." + queueName, "Error");
            this.kestrelQueueDao.logEnqueueFailure(queueName, payLoad.toString());
            this.logger.error(String.format("SQS Queue Error:Error while queuing %s in %s", payLoad, queueName), e);
        }
    }
    
    public void enqueueRewardRechargeResponse(Map<String, String> dataMap) {
    	enqueueToMumbaiSQS(dataMap, fcProperties.getRechargeAsRewardResponseQueue(), fcProperties.getKestrelTimeOut());
    }

    public void enqueueForFailureFulfilment(String orderId) {
        logger.info("Updating UTH for order " + orderId + " to refund initiated");
        userTransactionHistoryService.updateStatus(orderId, PaymentConstants.TRANSACTION_STATUS_REFUND_INITIATED);
        Map<String, String> queueItem = new HashMap<String, String>();
        queueItem.put(PaymentConstants.ORDER_ID_KEY, orderId);
        enqueueToMumbaiSQS(queueItem, fcProperties.getFailureFulfilmentQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public void enqueueUPUpdate(String pendingOrder) {
        Map<String, String> datamap = new HashMap<String, String>();
        datamap.put(PaymentConstants.ORDER_ID_KEY, pendingOrder);
        enqueueToMumbaiSQS(datamap, fcProperties.getUPUpdateQueue(), fcProperties.getKestrelTimeOut());
    }
    
    public boolean isEnqueuedForFulfilment(String orderId) {
        return idempotencyMap.isInQueue(orderId, fcProperties.getFulfillmentQueue()) ||
               idempotencyMap.isInQueue(orderId, fcProperties.getRechargeQueue()) || 
               idempotencyMap.isInQueue(orderId, fcProperties.getPostpaidFulfillmentQueue()) || 
               idempotencyMap.isInQueue(orderId, fcProperties.getBillpayFulfillmentQueue());
    }
    
    public void enqueueForFFAdminRefunds(Map<String, String> dataMap) {
        logger.info("enqueuing:"+dataMap);
        try {

        if (!idempotencyMap.isInQueue(dataMap.get(PaymentConstants.ORDER_ID_KEY), fcProperties.getFFAdminRefundQueue())) {
            idempotencyMap.markEnqueue(dataMap.get(PaymentConstants.ORDER_ID_KEY), fcProperties.getFFAdminRefundQueue());
            enqueueToMumbaiSQS(dataMap, fcProperties.getFFAdminRefundQueue(), fcProperties.getKestrelTimeOut());
        } else {
            logger.info("Order ID is in fulfillment queue; not going ahead with enqueue:["
                    + String.valueOf(dataMap.get(PaymentConstants.ORDER_ID_KEY)) + "]");
        }
        }catch (Exception e) {
            logger.info("Exception enqueuing :"+dataMap);
		}

    }

    public boolean shouldEnqueueForNewRefund() {
        Integer randomInt = FCUtil.randomIntBetween(0, 100);
        try {
            if (randomInt < appConfig.getNewRefundPercentage()) {
                return true;
            }
        } catch(Exception e) {
            logger.error("Exception when fetching new refund percentage, sending for old refund");
            return false;
        }
        return false;
    }
}
