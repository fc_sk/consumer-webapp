package com.freecharge.kestrel.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 * Insert any errors while queueing up in kestrel into DB.
 */
public class KestrelQueueDao {
    private SimpleJdbcInsert insertEnqueueFailure;

    private static final String ENQUEUE_FAILURE_TABLE = "enqueue_failure";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        insertEnqueueFailure = new SimpleJdbcInsert(dataSource).withTableName(ENQUEUE_FAILURE_TABLE).usingGeneratedKeyColumns("enqueue_failure_id", "n_last_updated", "n_created");
    }

    public long logEnqueueFailure(String queueName, String payload) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("queue_name", queueName);
        params.put("payload", payload);
        params.put("created_ts", new Timestamp(new Date().getTime()));
        Number primaryKey = insertEnqueueFailure.execute(params);
        return primaryKey.longValue();
    }
}
