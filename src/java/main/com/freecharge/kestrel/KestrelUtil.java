package com.freecharge.kestrel;

public class KestrelUtil {
    public static String getKestrelBlockingReadCommand(String queueName, long timeOut) {
        return String.format("%s/t=%d", queueName, timeOut);
    }
    public static class Test {

        @org.junit.Test
        public void test() {
            String data = "rechargeQueue/t=12";
            org.junit.Assert.assertEquals(getKestrelBlockingReadCommand("rechargeQueue", 12), data);
        }

    }
}
