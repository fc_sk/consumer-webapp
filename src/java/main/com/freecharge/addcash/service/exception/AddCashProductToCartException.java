package com.freecharge.addcash.service.exception;

public class AddCashProductToCartException extends Exception {
	
	private String errorMessage;

	public AddCashProductToCartException(String errorString) {
		// TODO Auto-generated constructor stub
		this.errorMessage = errorString;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
		

}
