package com.freecharge.addcash.service;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.addcash.service.exception.AddCashProductToCartException;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.tracker.Tracker;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.Wallet.LimitStatus;
import com.freecharge.wallet.service.WalletLimitRequest;
import com.freecharge.wallet.service.WalletLimitResponse;
import com.freecharge.web.webdo.CommonSessionWebDo;

public class AddCashService {
	@Autowired
    private WalletService walletService;
	
    @Autowired
    private CartService cartService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private TxnHomePageService transactionHomePageService;
    
    @Autowired
    private Tracker tracker;
    
    private Logger logger = LoggingFactory.getLogger(getClass());
    
	public String addProductToCartForAddCash(String addCashAmount) throws AddCashProductToCartException{
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Integer userId = userServiceProxy.getLoggedInUser().getUserId();

        Amount amountToFund = new Amount(Double.parseDouble(addCashAmount));

        WalletLimitRequest limitRequest = new WalletLimitRequest();
        limitRequest.setAmountToTest(amountToFund);
        limitRequest.setFundSource(Wallet.FundSource.CASH_FUND);
        limitRequest.setTransactionType(Wallet.TransactionType.DEPOSIT);
        limitRequest.setUserId(userId.longValue());

        WalletLimitResponse limitResponse = walletService.evaluateWalletLimit(limitRequest);

        if (LimitStatus.Failure == limitResponse.getLimitStatus()) {
            String errorString = "Something wrong; got limit failure on addcashproducttocart controller for userId:[ " + userId + "] fund amount:[ " + amountToFund + "]. Failure reason: ["
                    + limitResponse.getDescriptionString() + "]. Redirecting user back to mybalance page";
            logger.error(errorString);
            
            throw new AddCashProductToCartException(limitResponse.getDescriptionString());
        }

        String lookupId = FCUtil.getLookupID();
        tracker.trackLookupId(lookupId);
        cartService.saveCartForWalletFund(lookupId, userId, amountToFund);

        CommonSessionWebDo commonSessionWebDo = new CommonSessionWebDo();
        commonSessionWebDo.setType(FCConstants.WALLET_FUND_PRODUCT_TYPE);
        fs.getSessionData().put(lookupId + "_CommonSessionPojo_type", commonSessionWebDo.getType());
        fs.getSessionData().put(lookupId + "_CommonSessionPojo_rechargeAmount", commonSessionWebDo.getRechargeAmount());
        fs.getSessionData().put(lookupId + "_CommonSessionPojo_postpaidAmount", commonSessionWebDo.getPostpaidAmount());
        fs.getSessionData().put(lookupId + "_CommonSessionPojo_mobNum", commonSessionWebDo.getRechargeMobileNumber());
        fs.getSessionData().put(lookupId + "_CommonSessionPojo_operatorName", commonSessionWebDo.getOperatorName());

        String sessionId = fs.getUuid();
        Users user = userServiceProxy.getUserByUserId(userId);
        String userContactNumber = null;
        if(user!=null){
        	userContactNumber = user.getMobileNo();
        }
        TxnHomePage transactionHomePage = new TxnHomePage();
        transactionHomePage.setSessionId(sessionId);
        transactionHomePage.setFkProductId(ProductName.WalletCash.getProductId());
        transactionHomePage.setLookupId(lookupId);
        transactionHomePage.setProductType(FCConstants.WALLET_FUND_PRODUCT_TYPE);
        transactionHomePage.setCreatredOn(new Timestamp(System.currentTimeMillis()));
        transactionHomePage.setAmount(amountToFund.getAmount().floatValue());
        transactionHomePage.setServiceNumber(userContactNumber);

        transactionHomePageService.saveTransactionHomePage(transactionHomePage);
        
        UserContactDetail ucD = new UserContactDetail();
        ucD.setMobile(userContactNumber);
        ucD.setUserId(userId);
       
        fs.getSessionData().put(lookupId + "_" + FCConstants.SESSION_DATA_USERCONTACTDETAIL + "_userId", ucD.getUserId());
        return lookupId;
	}
}
