package com.freecharge.addcash.service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.campaign.ICampaignService;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.condition.Result;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.promo.rest.error.ErrorCode.FreefundErrorCode;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.wallet.OneCheckWalletService;
import com.google.common.primitives.Ints;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PromocodeAddCashService {

    private final Logger logger                        = LoggingFactory.getLogger(getClass());

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private ICampaignService campaignServiceClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private UserServiceProxy userServiceProxy;


    public Result validatePromocode(String lookupId, int userId, Map<String, Object> data) {
        if (FCUtil.isNotEmpty(lookupId)) {
            Long freefundCouponId = freefundService.getFreefundCouponIdForLookupId(lookupId);
            FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(freefundCouponId);
            if(freefundCoupon == null) {
                logger.info("No promocode found for given code " +
                        freefundCoupon.getFreefundCode() +
                        "Return failure, as this might be an illegal block call");
                Result result = new Result();
                result.setSuccess(false);
                result.setFailureMessage(fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                result.setFailureMessage(fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                result.setErrorCode(FreefundErrorCode.PROMOCODE_NON_EXISTENT.toString());
            }

            // If the code is in 'Redeemed/Blocked' state.
            if ((FCConstants.BLOCKED.equalsIgnoreCase(freefundCoupon.getStatus()))
                    || FCConstants.REDEEMED.equalsIgnoreCase(freefundCoupon.getStatus())) {
                Result result = new Result();
                result.setErrorCode(ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                result.setFailureMessage(fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
                result.setFailureMessage(fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
                result.setSuccess(false);
                return result;
            }

            if (userId == 0) {
                Result result = new Result();
                result.setSuccess(false);
                result.setFailureMessage("userId invalid");
                result.setDetailedFailureMessage("userId invalid.");
                result.setErrorCode(FreefundErrorCode.INVALID_REQUEST.toString());
                return result;
            }

            HashMap<String, Object> paymentInfo = new HashMap<String, Object>();
            logger.info("Card num " + String.valueOf(data.get("card_num")));
            if (data.containsKey("fingerPrint") && data.get("fingerPrint") != null) {
                paymentInfo.put(InTxnDataConstants.CARD_BIN, data.get("card_is_in"));
                paymentInfo.put(InTxnDataConstants.CARD_HASH, data.get("fingerPrint"));
            }
            paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, data.get(InTxnDataConstants.PAYMENT_TYPE));
            paymentInfo.put(InTxnDataConstants.PG_AMOUNT, data.get(InTxnDataConstants.PG_AMOUNT));
            paymentInfo.put(InTxnDataConstants.WALLET_AMOUNT, ((BigDecimal)data.get(InTxnDataConstants.WALLET_AMOUNT)).doubleValue());
            paymentInfo.put(FCConstants.PROMOCODE_STATUS, "BLOCK_PROMOCODE");

            try {
                logger.info("Populating payment data to intxndata for lookupid " + lookupId);

                campaignServiceClient.saveInTxnData1(lookupId,
                        FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID, paymentInfo);
            }catch (Exception e){
                logger.error("Error saving intxndata in payment gateway handler: ", e);
            }

            CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                    lookupId, freefundCoupon.getFreefundClassId().intValue(),
                    "BLOCK_PROMOCODE", FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID);
            if (!campaignResult.getStatus()) {
                Result result = new Result();
                result.setSuccess(false);
                result.setErrorCode(ErrorCode.APPLY_FRAUD_DETECTED.toString());
                result.setFailureMessage(campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                result.setDetailedFailureMessage(
                        campaignResult.getConditionResult().getFailedRule().getDetailedFailureMessage());
                return result;
            } else {
                Result result = new Result();
                result.setSuccess(true);
                return result;
            }
            } else {
            Result result = new Result();
            result.setSuccess(false);
            result.setFailureMessage("LookupId invalid");
            result.setDetailedFailureMessage("LookupId invalid.");
            result.setErrorCode(FreefundErrorCode.INVALID_REQUEST.toString());
            return result;
        }
    }

    public void updateLookupIdForOrderId (String lookupId, String orderId) {
        Map<String, Object> intxnMap = new HashMap<>();
//                campaignServiceClient.getIntxnData(lookupId, FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID);
//        if (FCUtil.isEmpty(intxnMap)) {
//            return;
//        }
        try {
            logger.info("Populating payment data to intxndata for lookupid " + lookupId + " with order id " + orderId);
            intxnMap.put(InTxnDataConstants.UNIQUE_ID, orderId);
            intxnMap.put(InTxnDataConstants.ORDER_ID, orderId);
            campaignServiceClient.saveInTxnData1(lookupId,
                    FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID, intxnMap);
        }catch (Exception e){
            logger.error("Error saving intxndata in payment gateway handler: ", e);
        }
    }

    public Map<String, Object> setIntxnDataForApplyPromocode(FreefundClass freefundClass,
                                                             FreefundCoupon freefundCoupon,
                                                             Users user, String txnChannel, String imeiNumber,
                                                             String advertisementId, String appVersion,
                                                             String deviceUniqueId, String imsi, boolean isEmulator,
                                                             String ip, String amount, String visitId, String clientId) {
        HashMap<String, Object> transactionInfo = new HashMap<>();
        transactionInfo.put(InTxnDataConstants.PROMOCODE, freefundCoupon.getFreefundCode());
        transactionInfo.put(InTxnDataConstants.FF_CLASS_ID, freefundCoupon.getFreefundClassId().intValue());
        Integer userId=user.getUserId();
        transactionInfo.put(InTxnDataConstants.USER_ID, userId);
        //new user condition
        List<String> orders= userTransactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 1);
        if (null == orders || orders.isEmpty()) {
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
        } else {
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
        }
        transactionInfo.put(InTxnDataConstants.USER_EMAIL, user.getEmail());

        MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(user.getEmail(), null);
        String oneCheckWalletId = null;
        try {
            logger.info("Fetching oneCheckWalletId for user " + userId);
            oneCheckWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
        } catch (Exception e) {
            logger.error("error fetching oneCheckWalletId for email " + user.getEmail(), e);
        }
        if (null != migrationStatus) {
            transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
        }
        if (null != oneCheckWalletId) {
            transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
        }
        if (null != txnChannel) {
            transactionInfo.put(InTxnDataConstants.CHANNEL, Ints.tryParse(txnChannel));
        }

        transactionInfo.put(InTxnDataConstants.IP_ADDRESS, ip);
        transactionInfo.put(InTxnDataConstants.ADVERTISEMENT_ID, advertisementId);
        logger.info("Advertisement Id " + advertisementId);
        if (null != appVersion) {
            transactionInfo.put(InTxnDataConstants.APP_VERSION, Ints.tryParse(appVersion));
        }
        transactionInfo.put(InTxnDataConstants.DEVICE_UNIQUE_ID, deviceUniqueId);
        transactionInfo.put(InTxnDataConstants.IMEI_NUMBER, imeiNumber);
        transactionInfo.put(InTxnDataConstants.IMSI, imsi);
        transactionInfo.put(InTxnDataConstants.IS_EMULATOR, isEmulator);
        transactionInfo.put(InTxnDataConstants.ADD_CASH_AMOUNT, amount != null?Double.parseDouble(amount):(Double)0.0);
        transactionInfo.put(FCConstants.PROMOCODE_STATUS, "APPLY_PROMOCDE");
        transactionInfo.put(InTxnDataConstants.TRACKER_VISIT_ID, visitId);
        transactionInfo.put(InTxnDataConstants.TRACKER_CLIENT_ID, clientId);
        return transactionInfo;
    }
}
