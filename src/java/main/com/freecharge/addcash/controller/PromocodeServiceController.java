package com.freecharge.addcash.controller;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.addcash.DO.UserAndOrderIdDO;
import com.freecharge.addcash.service.PromocodeAddCashService;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.service.CartService;
import com.freecharge.campaign.ICampaignService;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.CaptchaService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.condition.Result;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.dos.web.FreefundRequestWebDO;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.freefund.services.PromocodeRedeemDetailsService;
import com.freecharge.freefund.web.controller.FreeFundController;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.OneCheckWalletService;
import com.google.gson.Gson;

@Controller
public class PromocodeServiceController {
    private final Logger logger                        = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private ICampaignService campaignServiceClient;

    @Autowired
    private CartService cartService;

    @Autowired
    private PromocodeAddCashService promocodeAddCashService;

    @Autowired
    private PromocodeRedeemDetailsService promocodeRedeemDetailsService;

    @Autowired
    protected OrderIdSlaveDAO orderIdSlaveDAO;


    @RequestMapping(value = "/promocodeservice/promocode/applypromo", method = RequestMethod.POST, produces = "application/json")
    public String validateAndApplyPromocodeInAddCash(
            @ModelAttribute(value = "freefundRequestWebDO") final FreefundRequestWebDO freeFundRequestWebDO,
            final HttpServletRequest request,final Model model) {
        try {
            logger.info("Apply request for " + freeFundRequestWebDO.getCouponCode() + " from " +
                    freeFundRequestWebDO.getLid());
            if (FCUtil.isEmpty(freeFundRequestWebDO.getLid())) {
                freeFundRequestWebDO.setLid(UUID.randomUUID().toString());
            }
            String ip = MobileUtil.getRealClientIpAddress(request);
            if (!appConfigService.isPromocodeEnabled()) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.PROMOCODE_FEATURE_DISABLED.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_PROMOCODE_DOWN));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.PROMOCODE_FEATURE_DISABLED.toString());
                return "jsonView";
            }

            com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getLoggedInUser();
            Map<String, String> userDetils =
                    oneCheckWalletService.getUserByToken(request.getHeader("token"),
                            request.getHeader("userAgent"), ip);
            if (user == null && FCUtil.isEmpty(userDetils.get("emailId"))) {
                    logger.info("User not found in session data for applied promocode "
                            + freeFundRequestWebDO.getCouponCode());
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                    model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.DEFAULT_ERROR.toString());
                    return "jsonView";
            } else if (user == null) {
                user = userServiceProxy.getUserByEmailId(userDetils.get("emailId"));
            }

            String imeiNumber = request.getParameter(FCConstants.IMEI);
            String advertisementId = request.getParameter("advertise_id");
            String clientId = null;
            String visitId = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null && cookies.length > 0) {
                for (Cookie cookie : cookies) {
                    if ("fc.tc3".equalsIgnoreCase(cookie.getName())) {
                        String clientCookie = cookie.getValue();
                        if (!FCUtil.isEmpty(clientCookie)) {
                            String[] clientCookieSplit = clientCookie.split(";");
                            if (clientCookieSplit.length > 0) {
                                clientId = clientCookieSplit[0];
                            }
                        }
                    } else if ("fc.tv".equalsIgnoreCase(cookie.getName())) {
                        visitId = cookie.getValue();
                    }
                }
            }
            String deviceUniqueId = request.getParameter(FCConstants.DEVICE_UNIQUE_ID);

            String imsi = request.getParameter(FCConstants.IMSI);
            Boolean isEmulator = "TRUE".equalsIgnoreCase(request.getParameter(FCConstants.IS_EMULATOR));

            String txnChannel = request.getParameter(FCConstants.FC_CHANNEL);
            String appVersion = request.getParameter(FCConstants.FC_ANDROID_APP_VERSION);

            logger.debug("channel id: " + txnChannel + " App version: " + appVersion);
            if (!FCUtil.isEmpty(txnChannel) && "APP".equals(FCUtil.getChannelTypeAsDescriptor(txnChannel))) {
                int minVersion = getMinVersion();
                logger.debug("Minimum version set " + minVersion + " for channel " + txnChannel + " ,received app "
                        + "version " + appVersion + " for " + freeFundRequestWebDO.getCouponCode());
                if (!FCUtil.isEmpty(appVersion)) {
                    int reqAppVersion = Integer.parseInt(appVersion);
                    if (reqAppVersion != -1 && reqAppVersion < minVersion) {
                        return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                    }
                } else {
                    return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                }
            }

            if (!checkCouponDataSanity(freeFundRequestWebDO.getCouponCode())) {
                logger.info("coupon validation failure");
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.EMPTY_CODE_ERROR.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.EMPTY_CODE_ERROR.toString());
                return "jsonView";
            }

            FreefundCoupon freefundCoupon = freefundService
                    .getFreefundCoupon(freeFundRequestWebDO.getCouponCode());
            if (freefundCoupon == null) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.EMPTY_CODE_ERROR.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.EMPTY_CODE_ERROR.toString());
                return "jsonView";
            }
//            if (appConfigService.isRecaptchaEnabled()) {
////                boolean shouldCheckCaptcha = captchaService.shouldCheckCaptcha(fcSession);
////                SimpleReturn simpleReturn = captchaService.isCaptchValid(request);
////                recordCaptcha(shouldCheckCaptcha, simpleReturn.isValid());
////                if (shouldCheckCaptcha && !simpleReturn.isValid()) {
////                    if (freefundCoupon != null) {
////                        logger.info(freefundCoupon.getFreefundCode() + " [ " + freefundCoupon.getFreefundClassId()
////                                + " ] " + " reCaptchaService validation failure " + " Reason : "
////                                + simpleReturn.getObjMap());
////                    } else {
////                        logger.info(" reCaptchaService validation failure. Reason : " + simpleReturn.getObjMap());
////                    }
////                    model.addAttribute("status", "failure");
////                    model.addAttribute("errorCode", ErrorCode.CAPTCHA_VALIDATION_FAIL.getErrorNumber());
////                    model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INCORRECT_CAPTCHA));
////                    model.addAttribute(WebConstants.FORCE_CAPTCHA, true);
////                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
////                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
////                            ErrorCode.CAPTCHA_VALIDATION_FAIL.toString());
////                    return "jsonView";
////                }
////                /*
////                 * Reset captcha only if its a valid code and captcha validated
////                 * first time
////                 */
////                boolean resetCaptcha = (freefundCoupon != null) && simpleReturn.isValid();
////                model.addAttribute(WebConstants.FORCE_CAPTCHA, captchaService.forceCaptcha(fcSession, resetCaptcha));
////                captchaService.markTokenUsed(fcSession, true);
////            }

            boolean isFreefundCode = "redeem-to-fcbalance".equals(freefundCoupon.getPromoEntity());
            if (isFreefundCode) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
                model.addAttribute("errors", "This promocode is not applicable for this transaction. " +
                        "Please apply it under freefund section");
                logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                        + " freefund code applied in add cash flow");
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.INVALID_CODE.toString());
                return "jsonView";
            }


            if ("REDEEMED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_REDEEMED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                }
                return "jsonView";
            }

            if ("BLOCKED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                }
                return "jsonView";
            }

            FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
            Map<String, Object> transactionInfo =
                    promocodeAddCashService.setIntxnDataForApplyPromocode(freefundClass, freefundCoupon, user,
                            txnChannel, imeiNumber, advertisementId, appVersion, deviceUniqueId, imsi, isEmulator, ip,
                            freeFundRequestWebDO.getAmount(), visitId, clientId);
            transactionInfo.put(InTxnDataConstants.LOOKUP_ID, freeFundRequestWebDO.getLid());
            try {
                logger.info("LookUpId for campaign_service: "+freeFundRequestWebDO.getLid());
                campaignServiceClient.saveInTxnData1(freeFundRequestWebDO.getLid(),
                        FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID, transactionInfo);
            }catch (Exception e){
                logger.error("error saving intxndata in freefund service: ", e);
            }

            CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                    freeFundRequestWebDO.getLid(), freefundClass.getId(),
                    "APPLY_PROMOCODE", FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID);
            if (!campaignResult.getStatus()) {
                model.addAttribute("status", "failure");
                // Intentionally confusing message to user
                model.addAttribute("errorCode", ErrorCode.APPLY_FRAUD_DETECTED.getErrorNumber());
                model.addAttribute("errors",
                        campaignResult.getConditionResult().getFailedRule().getDetailedFailureMessage());
                logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId()
                        + "] " + campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                return "jsonView";
            } else {
                FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
                freeFundRequestBusinessDO.setEmail(user.getEmail());
                freeFundRequestBusinessDO.setCouponCode(freeFundRequestWebDO.getCouponCode().trim());
                freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
                freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
                freeFundRequestBusinessDO.setLookupID(freeFundRequestWebDO.getLid());

                freefundService.saveCartWithFreeFundForAddCash(freeFundRequestBusinessDO, user.getUserId());
                model.addAttribute("status", "success");
                model.addAttribute("lookupId", freeFundRequestWebDO.getLid());
                Map dataMap = setSuccessMessageForCode(freefundClass,
                        "You have successfully applied" + freefundCoupon.getFreefundCode()
                                + ", your cash back will be processed within 24hrs .");
                model.addAttribute("successMsg", dataMap.get("successMessage"));
                return "jsonView";
            }
        } catch (Exception e) {
            logger.error("Exception in promocode/freefund code path : ", e);
            model.addAttribute("status", "failure");
            model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                    FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                    ErrorCode.UNKNOWN_ERROR.toString());
            return "jsonView";
        }
    }

    private Map setSuccessMessageForCode(final FreefundClass freefundClass, final String defaultMessage) {
        Map dataMap = new HashMap();
        String datamapjson = freefundClass.getDatamap();
        if (datamapjson != null && datamapjson.length() != 0) {
            dataMap = FCStringUtils.jsonToMap(datamapjson);
        }
        if (dataMap.get("successMessage") == null || "".equals(dataMap.get("successMessage").toString().trim())) {

            dataMap.put("successMessage", defaultMessage);
        }
        return dataMap;
    }

    private boolean checkCouponDataSanity(final String couponCode) {
        if (FCUtil.isEmpty(couponCode)) {
            return false;
        }
        return true;
    }

    public int getMinVersion() {
        JSONObject versionObject = appConfigService.getCustomProp(FCConstants.PROMOCODE_APP_VERSION);
        Object minVersionStr = versionObject.get(FCConstants.ANDROID_VERSION_KEY);
        if (minVersionStr != null && !FCUtil.isEmpty(String.valueOf(minVersionStr))) {
            return Integer.parseInt(String.valueOf(minVersionStr));
        } else {
            return -1;
        }
    }

    private void recordCaptcha(boolean shouldCheckCaptcha, boolean valid) {
        if (shouldCheckCaptcha) {
            metricsClient.recordEvent(FreeFundController.METRIC_CAPTCHA_SERVICE,
                    FreeFundController.METRIC_CAPTCHA_ENTERED, "entered");
            if (valid) {
                metricsClient.recordEvent(FreeFundController.METRIC_CAPTCHA_SERVICE,
                        FreeFundController.METRIC_CAPTCHA_SUCCESS, "success");
            } else {
                metricsClient.recordEvent(FreeFundController.METRIC_CAPTCHA_SERVICE,
                        FreeFundController.METRIC_CAPTCHA_FAIL, "fail");
            }
        }
    }

    public String getAndroidVersionInvalidErrorObjectAndLogMetrics(final Model model) {
        model.addAttribute("status", "failure");
        model.addAttribute("errorCode", ErrorCode.ANDROID_APP_VERSION_INVALID.getErrorNumber());
        model.addAttribute("errors", fcProperties.getProperty(FCProperties.INVALID_ANDROID_APP_VERSION));
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                ErrorCode.ANDROID_APP_VERSION_INVALID.toString());
        return "jsonView";
    }


    @RequestMapping(value = "/promocodeservice/promocode/deletepromo", method = RequestMethod.POST, produces = "application/json")
    public String deletePromocode (
            @ModelAttribute(value = "freefundRequestWebDO") final FreefundRequestWebDO freeFundRequestWebDO,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) {
        logger.info("Processing request to remove freefund code : " + freeFundRequestWebDO.getCouponCode()
                + " for lookup id : " + freeFundRequestWebDO.getPromoId());
        FreefundCoupon freefundCoupon = freefundService
                .getFreefundCoupon(freeFundRequestWebDO.getCouponCode());

        String ip = MobileUtil.getRealClientIpAddress(request);
        com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getLoggedInUser();
        Map<String, String> userDetails =
                oneCheckWalletService.getUserByToken(request.getHeader("token"),
                        request.getHeader("userAgent"), ip);
        if (user == null && FCUtil.isEmpty(userDetails.get("emailId"))) {
            logger.info("User not found in session data for applied promocode "
                    + freeFundRequestWebDO.getCouponCode());
            model.addAttribute("status", "failure");
            model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
            model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                    FreeFundController.METRIC_FREEFUND_APPLY_FAIL,
                    ErrorCode.DEFAULT_ERROR.toString());
            return "jsonView";

        } else if (user == null) {
            user = userServiceProxy.getUserByEmailId(userDetails.get("emailId"));
        }

        CartBusinessDo cartBusinessDo = cartService.getCart(freeFundRequestWebDO.getPromoId());
        if(cartBusinessDo == null){
            logger.info("No cart found for given lookup id " +
                    freeFundRequestWebDO.getPromoId());
            model.addAttribute("status", FCConstants.ERROR);
            model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                    FreeFundController.METRIC_FREEFUND_REMOVE_FAIL,
                    ErrorCode.DEFAULT_ERROR.toString());
            return "jsonView";
        }

        if(freefundCoupon == null){ // Even if the code is not present in cart, still return SUCCESS
            logger.info("No promocode found for given code " +
                    freeFundRequestWebDO.getCouponCode() +
                    "Return Success, as this might be an unwanted promocode remove call");
            model.addAttribute("status", FCConstants.FAILURE);
            model.addAttribute("message", fcProperties.getProperty(FCProperties.MSG_NO_CODE_TO_REMOVE));
            return "jsonView";
        }

        // If the code is in 'Redeemed/Blocked' state, it should NOT be removed from cart.

        if ((FCConstants.BLOCKED.equalsIgnoreCase(freefundCoupon.getStatus())) || FCConstants.REDEEMED.equalsIgnoreCase(freefundCoupon.getStatus())) {
            model.addAttribute("status", FCConstants.ERROR);
            model.addAttribute("errorCode", ErrorCode.CODE_NOT_REMOVED_ERROR.getErrorNumber());
            model.addAttribute("errors", freefundCoupon.getFreefundCode() + " "
                    + fcProperties.getProperty(FCProperties.MSG_CODE_NOT_REMOVED));

            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                    FreeFundController.METRIC_FREEFUND_REMOVE_FAIL,
                    "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                            + ErrorCode.CODE_NOT_REMOVED_ERROR.toString());

            return "jsonView";
        }

        logger.info("Releasing coupon " + freeFundRequestWebDO.getCouponCode() + " from cart : " + cartBusinessDo.getId());

        freefundService.removeFreefundCode(freefundCoupon.getFreefundClassId(),
                freefundCoupon.getFreefundCode(),
                cartBusinessDo, null,
                null, null,
                user.getUserId(), user.getEmail());
        model.addAttribute("status", FCConstants.SUCCESS);
        model.addAttribute("message", fcProperties.getProperty(FCProperties.MSG_CODE_REMOVED));
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_REMOVE,
                "ffclass-" + (freefundCoupon.getFreefundClassId() != null ? freefundCoupon.getFreefundClassId(): ""));

        return "jsonView";
    }

    @NoLogin
    @Csrf(exclude= true)
    @RequestMapping(value = "/promocodeservice/promocode/redeem",
            method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String markPromocodeRedeemed (
             @RequestBody String json,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) {
        UserAndOrderIdDO userAndOrderIdDO = new Gson().fromJson(json, UserAndOrderIdDO.class);
        FreefundCoupon freefundCoupon = freefundService.getFreefundCouponObjectByAddCashOrderId(
                userAndOrderIdDO.getOrderId());
        if (freefundCoupon == null) {
            freefundCoupon = freefundService.getFreefundCoupon(userAndOrderIdDO.getPromocode());
            if (freefundCoupon == null) {
                Result result = new Result();
                result.setSuccess(false);
                result.setFailureMessage("Error fetching promocode for given orderId " + userAndOrderIdDO.getOrderId());
                return new Gson().toJson(result);
            }
        }
        logger.info("Received request for freefund code " + freefundCoupon.getFreefundCode()
                + " and user " + userAndOrderIdDO.getEmailId()
                + " from order id " + userAndOrderIdDO.getOrderId());
        int userId = oneCheckWalletService.getOneCheckUserDetailsFromIMS(userAndOrderIdDO.getEmailId()).getFcUserId();
        freefundService.markPromocodeRedeemed(freefundCoupon, userId,
                userAndOrderIdDO.getOrderId());
        promocodeRedeemDetailsService.insertIntoPromocodeRedeemDetailsForAddCash(freefundCoupon, userAndOrderIdDO.getOrderId(),
                userId, userAndOrderIdDO.getAmount());
        return "{\"result\":\"success\"}";
    }

    @NoLogin
    @Csrf(exclude= true)
    @RequestMapping(value = "/promocodeservice/promocode/unblock",
            method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String unblockPromocode (
            @RequestBody String json,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) {
        UserAndOrderIdDO userAndOrderIdDO = new Gson().fromJson(json, UserAndOrderIdDO.class);
        FreefundCoupon freefundCoupon = freefundService.getFreefundCouponObjectByAddCashOrderId(
                userAndOrderIdDO.getOrderId());
        if (freefundCoupon == null) {
            Result result = new Result();
            result.setSuccess(false);
            result.setFailureMessage("Error fetching promocode for given orderId " + userAndOrderIdDO.getOrderId());
            return new Gson().toJson(result);
        }
        logger.info("Received request for freefund code " + freefundCoupon.getFreefundCode()
                + " and user " + userAndOrderIdDO.getEmailId()
                + " from order id " + userAndOrderIdDO.getOrderId());
        int userId = userServiceProxy.getUserByEmailId(userAndOrderIdDO.getEmailId()).getUserId();
        freefundService.unblockPromocode(freefundCoupon, new Integer(userId), userAndOrderIdDO.getOrderId());
        promocodeRedeemDetailsService.insertIntoPromocodeRedeemDetailsForAddCash(freefundCoupon, userAndOrderIdDO.getOrderId(),
                userId, userAndOrderIdDO.getAmount());
        return "{\"result\":\"success\"}";
    }

    //Blocking addcash promocode
    @NoLogin
    @Csrf(exclude= true)
    @RequestMapping(value = "/promocodeservice/promocode/block",
            method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String blockPromocode (
            @RequestBody String json,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) {

    	UserAndOrderIdDO userAndOrderIdDO = new Gson().fromJson(json, UserAndOrderIdDO.class);
    	logger.info("Started blocking promocode for add cash orderId : " + userAndOrderIdDO.getOrderId());
    	Result result = new Result();
    	result.setSuccess(false);

    	try {
        	FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(userAndOrderIdDO.getPromocode());
            if (freefundCoupon == null) {
                freefundCoupon = freefundService.getFreefundCouponByUsedEmailId(userAndOrderIdDO.getOrderId());
                if (freefundCoupon == null) {
                    result.setFailureMessage("Error fetching promocode for given orderId " + userAndOrderIdDO.getOrderId());
                    return new Gson().toJson(result);
                }
            }
            FreefundRequestBusinessDO freefundRequestBusinessDO = new FreefundRequestBusinessDO();
            freefundRequestBusinessDO.setLookupID(userAndOrderIdDO.getOrderId());
            freefundRequestBusinessDO.setCouponCode(freefundCoupon.getFreefundCode());
            freefundRequestBusinessDO.setCouponId(freefundCoupon.getId());

            freefundRequestBusinessDO.setEmail(userAndOrderIdDO.getEmailId());
            freefundRequestBusinessDO.setFreefundCoupon(freefundCoupon);

            /*Getting mobile number*/
            freefundRequestBusinessDO.setServiceNumber(
                    oneCheckWalletService.getOneCheckUserDetailsFromIMS(userAndOrderIdDO.getEmailId()).getMobileNumber());
            freefundRequestBusinessDO.setOfferType(1);
            boolean blockSuccess =  freefundService.blockCouponCode(freefundRequestBusinessDO);
            if (blockSuccess) {
            	result.setSuccess(true);
            	return new Gson().toJson(result);
            }
    	} catch (Exception ex) {
    		logger.error("Exception while blocking the code " + userAndOrderIdDO.getOrderId(), ex);
            result.setFailureMessage("Exception while blocking promocode for given orderId " + userAndOrderIdDO.getOrderId());
            return new Gson().toJson(result);
    	}
    	 result.setFailureMessage("Error blocking promocode for given orderId " + userAndOrderIdDO.getOrderId());
         return new Gson().toJson(result);
    }
}
