package com.freecharge.cookie;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.CookieHandler;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.web.interceptor.FCBaseInterceptor;

/**
 * Used to set permanent cookie value in session object.
 * @author shirish
 *
 */
public class PermanentCookieInterceptor extends FCBaseInterceptor {
    private Logger logger = LoggingFactory.getLogger(PermanentCookieInterceptor.class);

    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, Object handler) throws Exception {
        try {
            FreechargeSession session = FCSessionUtil.getCurrentSession();
            if (session != null){
            	String permanentCookie = CookieHandler.checkAndSetPermanentCookie(request, response);
            	session.setPermanentCookie(permanentCookie);
            } else{
            	logger.error("Could not set permanent cook since session is null.");
            }
        } catch (Exception e) {
            logger.error("Exception while reading and setting permanent cookie value in session.", e);
        }
        return true;
    }

}
