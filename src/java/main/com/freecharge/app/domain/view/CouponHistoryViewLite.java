package com.freecharge.app.domain.view;

public class CouponHistoryViewLite implements Comparable<CouponHistoryViewLite>{

	private String campaignName;
	private int couponValue;
	private int faceValue;
	private String termsAndConditions;
	private String couponType;
	private String couponNature;
	private String couponImagePath;
	private String appCouponImagePath;
	private int couponPosition;
	private Boolean isComplimentary;
	private String validityDate;
	private Boolean isUnique;
	private Boolean isMCoupon;
	private String shortDescription;
	private String couponHyperlink;
	private String categoryName;
	private String couponCode;
	private String blockTime;
	private String merchantTitle;
	private String merchantImagePath;
	private String merchantUrl;
	private String orderId;
	private String createdOn;
	private Integer userId;
	private Integer couponStoreId;
	private Integer voucherCatId;
	private Integer price;
	private String optinType;
	private Boolean printable;
 	
	@Override
	public int compareTo(CouponHistoryViewLite other) {
		if (other == null || other.getCreatedOn() == null || createdOn == null){
			return -1;
		}
		return createdOn.compareTo(other.getCreatedOn());
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public int getCouponValue() {
		return couponValue;
	}

	public void setCouponValue(int couponValue) {
		this.couponValue = couponValue;
	}

	public int getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(int faceValue) {
		this.faceValue = faceValue;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getCouponNature() {
		return couponNature;
	}

	public void setCouponNature(String couponNature) {
		this.couponNature = couponNature;
	}

	public String getCouponImagePath() {
		return couponImagePath;
	}

	public void setCouponImagePath(String couponImagePath) {
		this.couponImagePath = couponImagePath;
	}

	public int getCouponPosition() {
		return couponPosition;
	}

	public void setCouponPosition(int couponPosition) {
		this.couponPosition = couponPosition;
	}

	public Boolean getIsComplimentary() {
		return isComplimentary;
	}

	public void setIsComplimentary(Boolean isComplimentary) {
		this.isComplimentary = isComplimentary;
	}

	public String getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(String validityDate) {
		this.validityDate = validityDate;
	}

	public Boolean getIsUnique() {
		return isUnique;
	}

	public void setIsUnique(Boolean isUnique) {
		this.isUnique = isUnique;
	}

	public Boolean getIsMCoupon() {
		return isMCoupon;
	}

	public void setIsMCoupon(Boolean isMCoupon) {
		this.isMCoupon = isMCoupon;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getCouponHyperlink() {
		return couponHyperlink;
	}

	public void setCouponHyperlink(String couponHyperlink) {
		this.couponHyperlink = couponHyperlink;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getBlockTime() {
		return blockTime;
	}

	public void setBlockTime(String blockTime) {
		this.blockTime = blockTime;
	}

	public String getMerchantTitle() {
		return merchantTitle;
	}

	public void setMerchantTitle(String merchantTitle) {
		this.merchantTitle = merchantTitle;
	}

	public String getMerchantImagePath() {
		return merchantImagePath;
	}

	public void setMerchantImagePath(String merchantImagePath) {
		this.merchantImagePath = merchantImagePath;
	}

	public String getMerchantUrl() {
		return merchantUrl;
	}

	public void setMerchantUrl(String merchantUrl) {
		this.merchantUrl = merchantUrl;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCouponStoreId() {
		return couponStoreId;
	}

	public void setCouponStoreId(Integer couponStoreId) {
		this.couponStoreId = couponStoreId;
	}

	public Integer getVoucherCatId() {
		return voucherCatId;
	}

	public void setVoucherCatId(Integer voucherCatId) {
		this.voucherCatId = voucherCatId;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getOptinType() {
		return optinType;
	}

	public void setOptinType(String optinType) {
		this.optinType = optinType;
	}

	public Boolean getPrintable() {
		return printable;
	}

	public void setPrintable(Boolean printable) {
		this.printable = printable;
	}

	public String getAppCouponImagePath() {
		return appCouponImagePath;
	}

	public void setAppCouponImagePath(String appCouponImagePath) {
		this.appCouponImagePath = appCouponImagePath;
	}
}
