package com.freecharge.app.domain.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import com.freecharge.coupon.service.CouponServiceProxy;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.model.CouponHistoryMaster;
import com.freecharge.api.coupon.service.model.CouponImage;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.mobile.util.MapperUtil;

@Service
public class CouponHistoryViewMapper {
	
	@Autowired
	private CouponServiceProxy couponServiceProxy;
	
	@Autowired
	private FCProperties fcProperties;
	
	private static Logger logger = LoggingFactory.getLogger(CouponHistoryViewMapper.class);

	public CouponHistoryView mapCouponHistory(CouponHistory couponHistory){
		CouponHistoryView couponHistoryView = MapperUtil.map(couponHistory, com.freecharge.app.domain.view.CouponHistoryView.class);
		couponHistoryView.setPrintable(getIsPrintable(couponHistory));
		couponHistoryView.setMinRedemAmount(couponHistory.getMinRedemAmt());
		logger.info("couponId from couponHistory="+couponHistory.getCouponStoreId());
		//couponHistoryView.setHowRedeemOffer(couponServiceProxy.getHowRedeemOffer(couponHistory.getCouponStoreId()));
		return couponHistoryView;
		
	}
	public CouponHistoryView mapCouponHistoryMaster(CouponHistoryMaster couponHistoryMaster){
		logger.info("In mapCouponHistoryMaster..");
		CouponHistoryView couponHistoryView=new CouponHistoryView();
		couponHistoryView.setCampaignName(couponHistoryMaster.getCouponHistory().getCampaignName());
		couponHistoryView.setCouponValue(couponHistoryMaster.getCouponHistory().getCouponValue());
		couponHistoryView.setFaceValue(couponHistoryMaster.getCouponHistory().getFaceValue());
		couponHistoryView.setZoneName(couponHistoryMaster.getCouponHistory().getZoneName());
		couponHistoryView.setCityNameList(couponHistoryMaster.getCouponHistory().getCityNameList());
		couponHistoryView.setTermsAndConditions(couponHistoryMaster.getCouponHistory().getTermsAndConditions());
		couponHistoryView.setCouponType(couponHistoryMaster.getCouponHistory().getCouponType());
		couponHistoryView.setCouponNature(couponHistoryMaster.getCouponHistory().getCouponNature());
		couponHistoryView.setCouponImagePath(couponHistoryMaster.getCouponHistory().getCouponImagePath());
		couponHistoryView.setCouponPosition(couponHistoryMaster.getCouponHistory().getCouponPosition());
		couponHistoryView.setIsComplimentary(couponHistoryMaster.getCouponHistory().getIsComplimentary());
		couponHistoryView.setValidityDate(couponHistoryMaster.getCouponHistory().getValidityDate());
		couponHistoryView.setIsUnique(couponHistoryMaster.getCouponHistory().getIsUnique());
		couponHistoryView.setIsMCoupon(couponHistoryMaster.getCouponHistory().getIsMCoupon());
		couponHistoryView.setShortDescription(couponHistoryMaster.getCouponHistory().getShortDescription());
		couponHistoryView.setCouponHyperlink(couponHistoryMaster.getCouponHistory().getCouponHyperlink());
		couponHistoryView.setCategoryName(couponHistoryMaster.getCouponHistory().getCategoryName());
		couponHistoryView.setCouponCode(couponHistoryMaster.getCouponHistory().getCouponCode());
		couponHistoryView.setBlockTime(couponHistoryMaster.getCouponHistory().getBlockTime());
		couponHistoryView.setMerchantTitle(couponHistoryMaster.getCouponHistory().getMerchantTitle());
		couponHistoryView.setMerchantImagePath(couponHistoryMaster.getCouponHistory().getMerchantImagePath());
		couponHistoryView.setMerchantUrl(couponHistoryMaster.getCouponHistory().getMerchantUrl());
		couponHistoryView.setOrderId(couponHistoryMaster.getCouponHistory().getOrderId());
		couponHistoryView.setCreatedOn(couponHistoryMaster.getCouponHistory().getCreatedOn());
		couponHistoryView.setUserId(couponHistoryMaster.getCouponHistory().getUserId());
		couponHistoryView.setCouponStoreId(couponHistoryMaster.getCouponHistory().getCouponStoreId());
		couponHistoryView.setVoucherCatId(couponHistoryMaster.getCouponHistory().getVoucherCatId());
		couponHistoryView.setPrice(couponHistoryMaster.getCouponHistory().getPrice());
		couponHistoryView.setOptinType(couponHistoryMaster.getCouponHistory().getOptinType());
		//couponHistoryView.setN_last_updated(couponHistoryMaster.getCouponHistory().getN_last_updated());
		couponHistoryView.setCreatedOn(couponHistoryMaster.getCouponHistory().getCreatedOn());
		couponHistoryView.setIs_used(couponHistoryMaster.getCouponHistory().getIs_used());
		couponHistoryView.setHowRedeemOffer(couponHistoryMaster.getCouponStore().getHowRedeemOffer());
		logger.info("Returning from mapCouponHistoryMaster..");
		return couponHistoryView;
		
	}
	
	private boolean getIsPrintable(CouponHistory couponHistory) {
		boolean printable = false;
		try{
			HashMap<String,String> metaDataMap =  couponHistory.getMetaDataMap();
			if(metaDataMap != null){
				printable = couponHistory.IsPrintable();
			}
		}catch(Exception e){
			logger.error("an error ocurred in CouponHistoryViewMapper for couponHistory : " + couponHistory);
		}
		return printable;
	}
	
	public CouponHistoryViewLite mapCouponHistoryLite(CouponHistory couponHistory, Map<Integer, List<CouponImage>> couponImages){
		CouponHistoryViewLite couponHistoryView = MapperUtil.map(couponHistory, com.freecharge.app.domain.view.CouponHistoryViewLite.class);
		List<CouponImage> images = couponImages.get(couponHistory.getCouponStoreId());
		if (images != null){
			//TODO:Shirish refactor this
			for (CouponImage image : images){
				if (image != null && image.getResolution().equals(fcProperties.getAppimageResolution()) && image.getChannel().equals("app") ){
					String couponImagePath = fcProperties.getAbsoluteImgUrl("content/images/coupon/app/" + image.getImageName());
					couponHistoryView.setAppCouponImagePath(couponImagePath);
				}
			}
		}
		String tnCUrl = "/rest/user/coupon-tnc?id=" + couponHistory.getId();
		couponHistoryView.setTermsAndConditions(fcProperties.getCouponTnCUrl(tnCUrl));
		couponHistoryView.setPrintable(getIsPrintable(couponHistory));
		return couponHistoryView;
		
	}
}
