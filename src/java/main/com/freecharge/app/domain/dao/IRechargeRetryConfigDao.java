package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.RechargeRetryConfig;

public interface IRechargeRetryConfigDao {
    
	public List<RechargeRetryConfig> getAllRechargeRetryConfig();
}
