package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.CrossSellHistory;
import com.freecharge.common.framework.logging.LoggingFactory;
@Component
public class CrossSellHistoryReadDAO {
	private static Logger logger = LoggingFactory.getLogger(CrossSellHistoryReadDAO.class);
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<CrossSellHistory> getCrossSellsByOrderId(String orderId) {
		List<CrossSellHistory> crosssells = null;
		try {
			String queryString = "select * from crosssell_optin_history where order_id=:orderId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);
			crosssells = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(CrossSellHistory.class));
		} catch (RuntimeException re) {
			logger.error("Error reading CrossSellOptInHistory from database for order id: " + orderId, re);
		}
		return crosssells;
	}
	
	public List<CrossSellHistory> getCrossSellsByOrderIds(List<String> orderIds) {
		List<CrossSellHistory> crosssells = null;
		try {
			String queryString = "select * from crosssell_optin_history where order_id in (:orderId) order by created_on desc";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderIds);
			crosssells = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(CrossSellHistory.class));
		} catch (RuntimeException re) {
			logger.error("Error reading CrossSellOptInHistory from database for order id: " + orderIds, re);
		}
		return crosssells;
	}
	
    public List<CrossSellHistory> getCrossSellOptinHistory(String orderId, Integer couponStoreId) {
        List<CrossSellHistory> crosssells = null;
        try {
            String queryString = "select * from crosssell_optin_history where order_id=:orderId and coupon_store_id=:couponStoreId ";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("orderId", orderId);
            paramMap.put("couponStoreId", couponStoreId);
            crosssells = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(
                    CrossSellHistory.class));
        } catch (RuntimeException re) {
            logger.error("Error reading CrossSellOptInHistory from database for order id: " + orderId, re);
        }
        return crosssells;
    }

    public List<String> getCrosssellCodeByOrderIdCouponId(String orderId, Integer couponStoreId) {
        List<String> couponCodes = null;
        try {
            String queryString = "select coupon_code from crosssell_optin_history where order_id=:orderId and coupon_store_id=:couponStoreId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("orderId", orderId);
            paramMap.put("couponStoreId", couponStoreId);
            couponCodes = jdbcTemplate.queryForList(queryString, paramMap, String.class);
        } catch (RuntimeException e) {
            logger.error(
                    "Error reading coupon codes from coupon_optin_history table for order id and coupon store id : "
                            + orderId + "," + couponStoreId, e);
        }
        return couponCodes;
    }
}
