package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.TxnCrossSell;

/**
 * Interface for TxnCrossSellDAO.
 * @author MyEclipse Persistence Tools
 */

public interface ITxnCrossSellDAO {
		
	 /**
	 * Find all TxnCrossSell entities with a specific property value.  
	 
	  @param propertyName the name of the TxnCrossSell property to query
	  @param value the property value to match
	  	  @return List<TxnCrossSell> found by query
	 */
	public List<TxnCrossSell> findByProperty(String propertyName, Object value
		);
	public List<TxnCrossSell> findByTxnHomePageId(Object txnHomePageId
		);
	public List<TxnCrossSell> findByCouponId(Object couponId
		);
	public List<TxnCrossSell> findByCorssSellId(Object corssSellId
		);
	public List<TxnCrossSell> findByIsDeleted(Object isDeleted
		);
	/**
	 * Find all TxnCrossSell entities.
	  	  @return List<TxnCrossSell> all TxnCrossSell entities
	 */
	public List<TxnCrossSell> findAll(
		);

    public Integer save(TxnCrossSell txnCrossSell);

    public Boolean updateTxnCrosssell(TxnCrossSell txnCrossSell);
    
    public void save(List<TxnCrossSell> txnCrossSellList) ;
}