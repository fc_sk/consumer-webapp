package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.dao.jdbc.UserSlaveDao;
import com.freecharge.app.domain.entity.PincodeMapping;
import com.freecharge.app.domain.entity.jdbc.mappers.PincodeMappingsMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class PincodeMappingReadDao {
	
	private NamedParameterJdbcTemplate jdbcTemplate;
    private static Logger logger = LoggingFactory.getLogger(UserSlaveDao.class);
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PincodeMapping> getPincodeMappingByPincode(String pincode) {
        try {
            logger.debug("request received for pincode mapping for pincode = "+ pincode);
            
            String queryString = "select * from pincode_mapping where pincode = :pincode";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("pincode", pincode);
            List<PincodeMapping> pincodeMapping = this.jdbcTemplate.query(queryString, paramMap, new PincodeMappingsMapper());
            if(pincodeMapping != null && pincodeMapping.size() > 0)
                return pincodeMapping;
            else
                return null;
        } catch (RuntimeException re) {
            logger.error("Failed for pincode =" + pincode, re);
            throw re;
        }
    }

}
