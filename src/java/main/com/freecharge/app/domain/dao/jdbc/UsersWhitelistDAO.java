package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class UsersWhitelistDAO {
    private static Logger logger = LoggingFactory.getLogger(UsersWhitelistDAO.class);
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    public Integer updateWhitelistedForUser(Integer userId, Boolean isWhitelisted) {
        try {
            logger.debug("Request comes into the updateWhitelistedForUser method of the class UsersWhitelistDAO, the request input parameter type UserId "+userId + " Whitelist :" + isWhitelisted);
            String updateQuery = "insert into users_whitelist (user_id, whitelisted) values(:userId, :whitelisted) on duplicate key update whitelisted=:whitelisted";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("whitelisted", isWhitelisted);
            paramMap.put("userId", userId);
            int count = this.jdbcTemplate.update(updateQuery, paramMap);
            logger.debug("Request goes out from the updateWhitelistedForUser method of the class UsersWhitelistDAO, the request  return type is Integer "+count);
            return count;
        } catch (RuntimeException re) {
            logger.error("updateWhitelistedForUser(Integer userId, Boolean isWhitelisted) failed for user id: " + userId, re);
        }
        return 0;
    }
    
    public Boolean isWhitelisted(Integer userId) {
        try {
            logger.debug("Request comes into the isWhitelisted method of the class UsersWhitelistDAO, the request input parameter type UserId "+userId);
            String updateQuery = "select whitelisted from users_whitelist where user_id=:userId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("userId", userId);
            Integer isWhitelisted = this.jdbcTemplate.queryForInt(updateQuery, paramMap);
            logger.debug("Request goes out from the isWhitelisted method of the class UsersWhitelistDAO, the request  return type is Integer "+isWhitelisted);
            return isWhitelisted == null ? false : (isWhitelisted != 0);
        } catch (EmptyResultDataAccessException e) {
            return false;
        } catch (RuntimeException re) {
            logger.error("isWhitelisted(Integer userId) failed for user id: " + userId, re);
        }
        return false;
    }
}
