package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.framework.logging.LoggingFactory;


/**
 * A data access object (DAO) providing persistence and search support for Users
 * entities. Transaction control of the save(), update() and delete() operations
 * must be handled externally by senders of these methods or must be manually
 * added to each of these methods for data to be persisted to the JPA datastore.
 * 
 * @see com.freecharge.app.domain.dao.Users
 * @author Rananjay
 */

public class UsersDAO  extends CommonDao<Users>implements IUsersDAO {
	// property constants
	public static final String FK_AFFILIATE_PROFILE_ID = "fkAffiliateProfileId";
	public static final String TYPE = "type";
    public static final String USERID = "userId";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String MOBILE_NO = "mobileNo";
	public static final String IS_ACTIVE = "isActive";
	public static final String IS_LOGGEDIN = "isLoggedin";
	public static final String MORF = "morf";
	public static final String FORGOT_PASSWORD_KEY = "forgotPasswordKey";
	public static final String FK_REFFERRED_BY_USER_ID = "fkRefferredByUserId";

    private static Logger logger = LoggingFactory.getLogger(UsersDAO.class);





    protected Class<Users> getDomainClass() {
        return Users.class;
    }

	/**
	 * Find all Users entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Users property to query
	 * @param value
	 *            the property value to match
	 * @return List<Users> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<Users> findByProperty(String propertyName, final Object value) {
		logger.debug("Request comes into the findByProperty method of the class UsersDAO, the request parameter type is Integer "+propertyName+" object "+value);
		logger.info("finding Users instance with property: "+ propertyName + ", value: " + value, null);
		try {
			final String queryString = "select model from Users model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<Users> list=query.list();
			logger.debug("Request goes out from the findByProperty method of the class UsersDAO, the request  return type is List<Users> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}
	
	public Users findByID(Object userId) {
	    List<Users> usersList = findByProperty(USERID, userId);
	    
	    if (usersList.size() != 1) {
	        throw new IllegalStateException("Did not find exactly one user for ID [" + userId + "]");
	    }
	    
        return usersList.get(0);
	}
	
	public List<Users> findByFkAffiliateProfileId(Object fkAffiliateProfileId) {
		return findByProperty(FK_AFFILIATE_PROFILE_ID, fkAffiliateProfileId);
	}

	public List<Users> findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List<Users> findByEmail(Object email) {
		return findByProperty(EMAIL, email);
	}

	public List<Users> findByPassword(Object password) {
		return findByProperty(PASSWORD, password);
	}

	public List<Users> findByMobileNo(Object mobileNo) {
		return findByProperty(MOBILE_NO, mobileNo);
	}

	public List<Users> findByIsActive(Object isActive) {
		return findByProperty(IS_ACTIVE, isActive);
	}

	public List<Users> findByIsLoggedin(Object isLoggedin) {
		return findByProperty(IS_LOGGEDIN, isLoggedin);
	}

	public List<Users> findByMorf(Object morf) {
		return findByProperty(MORF, morf);
	}

	public List<Users> findByForgotPasswordKey(Object forgotPasswordKey) {
		return findByProperty(FORGOT_PASSWORD_KEY, forgotPasswordKey);
	}

	public List<Users> findByFkRefferredByUserId(Object fkRefferredByUserId) {
		return findByProperty(FK_REFFERRED_BY_USER_ID, fkRefferredByUserId);
	}

	/**
	 * Find all Users entities.
	 * 
	 * @return List<Users> all Users entities
	 */
	@SuppressWarnings("unchecked")
	public List<Users> findAll() {
		logger.debug("Request comes into the findAll method of the class UsersDAO, the request has no input parameter");
		logger.info("finding all Users instances", null);
		try {
			final String queryString = "select model from Users model";
			Query query = getSession().createQuery(queryString);
			List<Users> list=query.list();
			logger.debug("Request goes out from the findAll method of the class UsersDAO, the request  return type is List<Users> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}
	}

}