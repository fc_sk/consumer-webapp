package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.SiteAutosuggestPrefix;

/**
 * Interface for SiteAutosuggestPrefixDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ISiteAutosuggestPrefixDAO {
	
	/**
	 * Find all SiteAutosuggestPrefix entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SiteAutosuggestPrefix property to query
	 * @param value
	 *            the property value to match
	 * @return List<SiteAutosuggestPrefix> found by query
	 */
	public List<SiteAutosuggestPrefix> findByProperty(String propertyName,
			Object value);

	public List<SiteAutosuggestPrefix> findByPrefix(Object prefix);

	public List<SiteAutosuggestPrefix> findByFkCircleOperatorMappingId(
			Object fkCircleOperatorMappingId);

	public List<SiteAutosuggestPrefix> findByFkProductMasterId(
			Object fkProductMasterId);

	public List<SiteAutosuggestPrefix> findByIsActive(Object isActive);

	/**
	 * Find all SiteAutosuggestPrefix entities.
	 * 
	 * @return List<SiteAutosuggestPrefix> all SiteAutosuggestPrefix entities
	 */
	public List<SiteAutosuggestPrefix> findAll();
}