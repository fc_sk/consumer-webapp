package com.freecharge.app.domain.dao.jdbc;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.CrossSellLeadData;
import com.freecharge.common.framework.logging.LoggingFactory;


public class CrossSellLeadDataWriteDAO {
	private static Logger logger = LoggingFactory.getLogger(CrosssellReadDAO.class);
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertCrosssellLeadData;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertCrosssellLeadData = new SimpleJdbcInsert(dataSource).withTableName("cross_sell_lead_data").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	public CrossSellLeadData create(CrossSellLeadData crossSellLeadData) {
		try {
			
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(crossSellLeadData);
			
			Number number = this.insertCrosssellLeadData.executeAndReturnKey(parameters);
			
			crossSellLeadData.setId(number.intValue());

			return crossSellLeadData;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
}