package com.freecharge.app.domain.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.freecharge.api.coupon.common.CouponConstants;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.app.domain.entity.jdbc.mappers.CouponCartItemMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

@Repository
public class CouponCartDao {
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(CouponCartDao.class);
	@Autowired
	@Qualifier("jdbcReadDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<CouponCartItem> getCouponCartItems(String lookupId) {
		logger.info("Request comes into the getCouponCartItems method of the class CouponCartDao, the request  parameter type is string " + lookupId);
		final String queryString = "select ci.fl_product_master_id, ci.fk_item_id, ci.quantity, ci.price from cart c, cart_items ci where c.lookup_id=:lookupId "
				+ " and c.cart_id = ci.fk_cart_id and ci.fl_product_master_id in (:pricedCouponProductMasterId, :couponProductMasterId,:hCouponProductMasterId)";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("lookupId", lookupId);
		paramMap.put("pricedCouponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_PRICED_COUPONS);
		paramMap.put("couponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_COUPONS);
		paramMap.put("hCouponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_HCOUPONS);
		List<CouponCartItem> results = this.jdbcTemplate.query(queryString,
				paramMap, new CouponCartItemMapper());
		return results;
	}
	
	public Long getFreefundCouponIdInCartForLookupId(String lookupId) {
        logger.info("Getting freefund coupon id for lookupid : " + lookupId);
        String queryString = "select ci.fk_item_id from cart_items ci, cart c where c.lookup_id = :lookupId and c.cart_id = ci.fk_cart_id and ci.entity_id = 'FreeFund'";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("lookupId", lookupId);
        
        List<Long> result = this.jdbcTemplate.queryForList(queryString, paramMap,
                Long.class);
        return (result == null || result.size() == 0)? null : result.get(0);
        
    }
	
	public List<Map<String, Object>> getMastreMestroPaymentTypeStatus(String lookupId) {
		logger.info("Request comes into the getMastreMestroPaymentTypeStatus method of the class CouponCartDao, the request  parameter type is string " + lookupId);
		List<Map<String, Object>> couponList = null;
		List<Map<String, Object>> objectMap = new ArrayList<Map<String, Object>>();
		try {
			String queryForMasterMetroStatus = "select * from cart cart, cart_items item where item.fk_cart_id = cart.cart_id and cart.lookup_id = :lookupId and item.fl_product_master_id = 7 and item.is_deleted = 0";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("lookupId", lookupId);
			couponList = this.jdbcTemplate.queryForList(queryForMasterMetroStatus, paramMap);
		} catch (Exception e) {
			logger.error("getCouponDetailsForMail failed", e);
		}
		return couponList;
	}
	
	public List<Integer> getCouponIdsFromCartBylookupId(String lookupId) {
		logger.info("Request comes into the getCouponIdsFromCartBylookupId method of the class CouponCartDao, the request  parameter type is string " + lookupId);
		List<Integer> couponList = null;
		try {
			String queryWithCouponCodes = "SELECT ci.fk_item_id as couponStoreId FROM cart c JOIN cart_items ci on c.cart_id = ci.fk_cart_id" + 
											" WHERE c.lookup_id = :lookupId and ci.is_deleted = 0 and ci.fl_product_master_id in (:pricedCouponProductMasterId, :couponProductMasterId,:hCouponProductMasterId)";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("lookupId", lookupId);
			paramMap.put("pricedCouponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_PRICED_COUPONS);
			paramMap.put("couponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_COUPONS);
			paramMap.put("hCouponProductMasterId", CouponConstants.PRODUCT_MASTER_ID_HCOUPONS);
			couponList = this.jdbcTemplate.queryForList(queryWithCouponCodes, paramMap,Integer.class);
		} catch (Exception e) {
			logger.error("getCouponIdsFromCartBylookupId failed", e);
		}
		logger.debug("Request goes out from the getCouponIdsFromCartBylookupId method of the class CouponReadDAO, the request  return type is List<Integer> ");
		return couponList;
	}

}
