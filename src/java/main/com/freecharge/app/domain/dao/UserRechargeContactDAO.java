package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.common.framework.logging.LoggingFactory;


public class UserRechargeContactDAO extends CommonDao<UserRechargeContact>implements IUserRechargeContactDAO {
    private static Logger logger = LoggingFactory.getLogger(UserRechargeContactDAO.class);

	@Override
	protected Class<UserRechargeContact> getDomainClass() {
		return UserRechargeContact.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserRechargeContact> findByProperty(String propertyName, final Object value) {
		logger.debug("Request comes into the findByProperty method of the class UserRechargeContactDAO, the request parameters type is String "+propertyName+"Object "+value);
		logger.info("finding UserRechargeContact instance with property: "+ propertyName + ", value: " + value, null);
		try {
			final String queryString = "select model from UserRechargeContact model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<UserRechargeContact> list=query.list();
			logger.debug("Request goes out from the updateUserRechargeHistoryRechargeStatus method of the class UserRechargeHistoryDAO, the request  return type is List<UserRechargeContact> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}
}
