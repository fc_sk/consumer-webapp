package com.freecharge.app.domain.dao.jdbc;

import static com.freecharge.common.util.FCStringUtils.mapToJson;
import static com.freecharge.common.util.FCUtil.isNotEmpty;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.FreefundClassRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.FreefundRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.promo.dao.PromocodeReadDao;
import com.freecharge.promo.entity.Promocode;
import com.freecharge.promo.util.PromocodeConstants;
import com.google.common.base.Optional;

public class FreefundReadDAO {
    private static Logger logger = LoggingFactory.getLogger(FreefundReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private DataSource dataSource;

    public static final String ERRORS = "errors";

    @Autowired
    private PromocodeReadDao promocodeReadDao;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
    }

    public List<FreefundClass> getFreefundClasses() {
        logger.debug("request goes inside getFreefundClasses");
        String queryString = "SELECT * FROM freefund_class where is_active = 1";

        Map<String, Object> paramMap = new HashMap<String, Object>();

        List<FreefundClass> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundClassRowMapper());

        logger.debug("request goes inside getFreefundClasses. FreefundClasses found " + list.size());
        return list;

    }

    public boolean isBinOfferItem(Long cartFkItemId) {
        FreefundCoupon freefundCoupon = getFreefundCoupon(cartFkItemId);
        if (freefundCoupon != null && freefundCoupon.getFreefundClassId() == 1) {
            return true;
        }
        return false;
    }

    public FreefundCoupon getFreefundCoupon(Long couponId) {
        logger.debug("request goes inside getFreefundCoupon");
        String queryString = "SELECT * FROM freefund_coupon WHERE freefund_coupon_id = "
                + couponId;
        FreefundCoupon freefundCoupon = null;
        Map<String, Object> paramMap = new HashMap<String, Object>();

        List<FreefundCoupon> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundRowMapper());
        if (list != null && list.size() > 0) {
            freefundCoupon = list.get(0);
            if(freefundCoupon != null) {
                loadFreefundClass(freefundCoupon);
            }
        }
        return freefundCoupon;
    }

    private void loadFreefundClass(FreefundCoupon freefundCoupon) {
        if(freefundCoupon != null) {
            FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundClassId());
            //TODO - Redo this method completely to get properties from promocode object
            getFreefundClass(freefundClass, freefundCoupon.getFreefundCode());
            if(freefundClass !=null && freefundClass.getPromoEntity() != null) {
                freefundCoupon.setPromoEntity(freefundClass.getPromoEntity());
                freefundCoupon.setMinRechargeValue(freefundClass.getMinRechargeValue());
                freefundCoupon.setFreefundValue(freefundClass.getFreefundValue());
            }
        }
    }

    private FreefundClass getFreefundClass(FreefundClass freefundClass, String freefundCode) {
        return getFreefundClassWithPromocodePropertiesUpdated(freefundClass, freefundCode);
    }

    private FreefundClass getFreefundClassWithPromocodePropertiesUpdated(FreefundClass freefundClass, String code) {
        Optional<Promocode> promocodeOptional = promocodeReadDao.getPromoFromPromocode(code);
        Promocode promocode;
        if (promocodeOptional.isPresent()) {
            promocode = promocodeOptional.get();

            if (isNotEmpty(promocode.getDiscountType())) {
                freefundClass.setDiscountType(promocode.getDiscountType());
            }

            if (promocode.getMinRechargeValue() > PromocodeConstants.ZERO_DISCOUNT) {
                freefundClass.setMinRechargeValue(promocode.getMinRechargeValue());
            }

            if (isNotEmpty(promocode.getPromoType())) {
                freefundClass.setPromoEntity(promocode.getPromoType());
            }

            if (isNotEmpty(promocode.getSuccessMessageApply())) {
                freefundClass.setSuccessMessage(promocode.getSuccessMessageApply());
            }

            if (isNotEmpty(promocode.getSuccessMessageRecharge())) {
                freefundClass.setRechargeSuccessMessage(promocode.getSuccessMessageRecharge());
            }

            if (promocode.getMaxDiscountValue() > PromocodeConstants.ZERO_MAX_DISCOUNT) {
                freefundClass.setMaxDiscount(promocode.getMaxDiscountValue());
            }

            if (promocode.getDiscountValue() > PromocodeConstants.ZERO_DISCOUNT) {
                freefundClass.setFreefundValue(promocode.getDiscountValue());
            }

            if (promocode.getApplyConditionId() > PromocodeConstants.NO_CONDITION_SET) {
                freefundClass.setApplyConditionId(promocode.getApplyConditionId());
            }

            if (promocode.getPaymentConditionId() > PromocodeConstants.NO_CONDITION_SET) {
                freefundClass.setPaymentConditionId(promocode.getPaymentConditionId());
            }

            if (promocode.getRedeemConditionId() > PromocodeConstants.NO_CONDITION_SET) {
                String dataMapStr = freefundClass.getDatamap();
                Map<String, String> dataMap = new HashMap<String, String>();
                if(!FCUtil.isEmpty(dataMapStr)) {
                    dataMap = FCUtil.stringToHashMap(dataMapStr);

                }
                dataMap.put("redeemConditionId", String.valueOf(promocode.getRedeemConditionId()));
                freefundClass.setDatamap(mapToJson(dataMap));
            }

            if (promocode.getValidFrom() != null) {
                freefundClass.setValidFrom(dateToCalendar(promocode.getValidFrom()));
            }

            if (promocode.getValidUpto() != null) {
                freefundClass.setValidUpto(dateToCalendar(promocode.getValidUpto()));
            }
        }
        return freefundClass;
    }
    
    private static Calendar dateToCalendar(Date date)
    {
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	return cal;
    }

    public List<String> getRedeemedFreefundCouponForEmail(String emailId, Long freefundClassId) {
        List<String> list = new ArrayList<String>();
        try{
            logger.debug("Redeemed Freefund Coupon retrieval starts for emailId : " + emailId + ", freefundClassId : " + freefundClassId);
            String queryString = "SELECT freefund_code FROM freefund_coupon WHERE used_email_id = :emailId and "
                    + "freefund_class_id = :freefundClassId and status = 'REDEEMED'";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("emailId", emailId);
            paramMap.put("freefundClassId", freefundClassId);

            list = this.jdbcTemplate.queryForList(queryString, paramMap, String.class);
            logger.info("Retrieved Redeemed FreefundCoupon List for email : " + emailId + ", class : " + freefundClassId);

        } catch (Exception e){
            logger.error("Retrieval of Redeemed FreefundCoupon List failed for emailId : " + emailId + ", class : "
                    + freefundClassId, e);
        }
        return list;
    }

    public List<String> getRedeemedFreefundCouponForServiceNumber(String serviceNumber, Long freefundClassId) {
        List<String> list = new ArrayList<String>();
        try{
            logger.debug("Redeemed Freefund Coupon retrieval starts for ServiceNumber : " + serviceNumber + ", freefundClassId : " + freefundClassId);
            String queryString = "SELECT freefund_code FROM freefund_coupon WHERE used_service_number  = :serviceNumber and "
                    + "freefund_class_id = :freefundClassId and status = 'REDEEMED'";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("serviceNumber", serviceNumber);
            paramMap.put("freefundClassId", freefundClassId);

            list = this.jdbcTemplate.queryForList(queryString, paramMap, String.class);
            logger.info("Retrieved Redeemed FreefundCoupon List for serviceNumber : " + serviceNumber + ", class : " + freefundClassId);

        } catch (Exception e){
            logger.error("Retrieval of Redeemed FreefundCoupon List failed for serviceNumber : " + serviceNumber + ", class : "
                    + freefundClassId, e);
        }

        return list;
    }

    public FreefundCoupon getAvailableCoupon(final String couponCode) {
        logger.debug("request goes inside getFreefundCoupon");
        String queryString = "SELECT * FROM freefund_coupon WHERE freefund_code = :freefundCode and used_email_id is null "
                + "and used_date is null and used_service_number is null and status is null " ;
        FreefundCoupon freefundCoupon = null;
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("freefundCode", couponCode);

        List<FreefundCoupon> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundRowMapper());
        if (list != null && list.size() > 0) {
            freefundCoupon = list.get(0);
            if(freefundCoupon != null) {
                loadFreefundClass(freefundCoupon);
            }
        }
        return freefundCoupon;
    }

    public FreefundCoupon getFreefundCoupon(String couponCode) {
        logger.debug("request goes inside getFreefundCoupon");
        String queryString = "SELECT * FROM freefund_coupon WHERE freefund_code = :freefundCode" ;
        FreefundCoupon freefundCoupon = null;
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("freefundCode", couponCode);

        List<FreefundCoupon> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundRowMapper());
        if (list != null && list.size() > 0) {
            freefundCoupon = list.get(0);
            if(freefundCoupon != null) {
                loadFreefundClass(freefundCoupon);
            }
        }
        return freefundCoupon;
    }

    public List<String> getFreefundCoupons(List<String> couponCodes){
        try {
            String queryString = "SELECT freefund_code FROM freefund_coupon WHERE freefund_code in (:freefundCodes)";

            Map<String, Object> paramMap = new HashMap<>();
            List<String> freefundCodes = new ArrayList<>();
            List<String> tempCodes = new ArrayList<>();
            int count = 0;
            while (count < couponCodes.size()) {
                if ((count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE) < couponCodes.size()) {
                    tempCodes = couponCodes.subList(count, count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE);
                    count = count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE;
                } else {
                    tempCodes = couponCodes.subList(count , couponCodes.size());
                    count = couponCodes.size();
                }
                paramMap.put("freefundCodes", tempCodes);
                freefundCodes.addAll(this.jdbcTemplate.queryForList(
                        queryString,
                        paramMap,
                        String.class));
            }
            return freefundCodes;
        } catch (Exception e) {
            logger.error("Failed to fetch freefund coupons from freefund codes ", e);
        }
        return null;
    }

    public FreefundClass getFreefundClass(Long freefundClassId) {
        logger.debug("request goes inside getFreefundCoupon");
        String queryString = "SELECT * FROM freefund_class WHERE id = :freefundClass" ;

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("freefundClass", freefundClassId);

        List<FreefundClass> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundClassRowMapper());
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public FreefundClass getFreefundClass(String name) {
        logger.debug("request goes inside getFreefundCoupon");
        String queryString = "SELECT * FROM freefund_class WHERE name = :name" ;

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", name);

        List<FreefundClass> list = this.jdbcTemplate.query(
                queryString,
                paramMap,
                new FreefundClassRowMapper());
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }


    public Integer getPromocodeIdByCode(String couponCode) {
        String queryString = "select freefund_coupon_id from freefund_coupon where freefund_code = :couponCode";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("couponCode", couponCode);
        List<Integer> result = this.jdbcTemplate.queryForList(queryString, paramMap,
                Integer.class);
        return (result == null || result.size() == 0)? -1: result.get(0);
    }

    public boolean isFreefundCodeRedeemedOrBlocked(String couponCode) {
        FreefundCoupon freefundCoupon = getFreefundCoupon(couponCode);
        if (FCUtil.isNotEmpty(freefundCoupon.getStatus())
                && (freefundCoupon.getStatus().equalsIgnoreCase(FCConstants.REDEEMED)
                || freefundCoupon.getStatus().equalsIgnoreCase(FCConstants.BLOCKED))) {
            return true;
        }
        return false;
    }

    public boolean isFreefundCodesCampaignOfUniqueType(String couponCode) {
        FreefundCoupon freefundCoupon = getFreefundCoupon(couponCode);
        if ( FreefundClass.FREEFUND_UNIQUE_CODE_TYPE_LIST.contains(freefundCoupon.getPromoEntity())) {
            return true;
        }
        return false;
    }

    public Optional<Map<String, Object>> getAffiliateIdForFreefundClass(long id) {
        String query = "Select id, affiliate_id from freefund_class where id=:id";
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id", id);
        try {
            List<Map<String, Object>> result = this.jdbcTemplate.queryForList(query, paramsMap);
            if(!FCUtil.isEmpty(result)) {
                return Optional.of(result.get(0));
            }
        } catch (DataAccessException e) {
            logger.error("Error while fetching affiliateId for freefund class with id: " + id);
        }
        return Optional.absent();
    }

    public List<String> getFreefundCoupons(List<String> couponCodes, long freefundClassId) {
        try {
            String queryString = "SELECT freefund_code FROM freefund_coupon " +
                    "WHERE freefund_class_id=:freefundClassId and freefund_code in (:freefundCodes)";

            Map<String, Object> paramMap = new HashMap<>();
            List<String> freefundCodes = new ArrayList<>();
            List<String> tempCodes = new ArrayList<>();
            int count = 0;
            while (count < couponCodes.size()) {
                if ((count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE) < couponCodes.size()) {
                    tempCodes = couponCodes.subList(count, count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE);
                    count = count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE;
                } else {
                    tempCodes = couponCodes.subList(count , couponCodes.size());
                    count = couponCodes.size();
                }
                paramMap.put("freefundCodes", tempCodes);
                paramMap.put("freefundClassId", freefundClassId);
                freefundCodes.addAll(this.jdbcTemplate.queryForList(
                        queryString,
                        paramMap,
                        String.class));
            }
            return freefundCodes;
        } catch (Exception e) {
            logger.error("Failed to fetch freefund coupons from freefund codes ", e);
        }
        return null;
    }

    public List<String> getRedeemedFreefundCoupons(List<String> couponCodes, long freefundClassId) {
        try {
            String queryString = "SELECT freefund_code FROM freefund_coupon " +
                    "WHERE freefund_class_id=:freefundClassId and freefund_code in (:freefundCodes) and status='REDEEMED'";

            Map<String, Object> paramMap = new HashMap<>();
            List<String> freefundCodes = new ArrayList<>();
            List<String> tempCodes = new ArrayList<>();
            int count = 0;
            while (count < couponCodes.size()) {
                if ((count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE) < couponCodes.size()) {
                    tempCodes = couponCodes.subList(count, count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE);
                    count = count + FCConstants.RETRIEVE_BATCH_SIZE_FREEFUND_CODE;
                } else {
                    tempCodes = couponCodes.subList(count , couponCodes.size());
                    count = couponCodes.size();
                }
                paramMap.put("freefundCodes", tempCodes);
                paramMap.put("freefundClassId", freefundClassId);
                freefundCodes.addAll(this.jdbcTemplate.queryForList(
                        queryString,
                        paramMap,
                        String.class));
            }
            return freefundCodes;
        } catch (Exception e) {
            logger.error("Failed to fetch freefund coupons from freefund codes ", e);
        }
        return null;
    }

    public int getCodeCountForFreefundClassId(long freefundClassId) {
        try {
            String queryString = "SELECT count(*) FROM freefund_coupon " +
                    "WHERE freefund_class_id=:freefundClassId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("freefundClassId", freefundClassId);
            int codes = this.jdbcTemplate.queryForInt(
                    queryString,
                    paramMap);

            return codes;
        } catch (Exception e) {
            logger.error("Failed to fetch freefund coupons from freefund codes ", e);
        }
        return 0;
    }
}
