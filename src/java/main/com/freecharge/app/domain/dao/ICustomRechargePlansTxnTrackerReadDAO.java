package com.freecharge.app.domain.dao;

import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;

public interface ICustomRechargePlansTxnTrackerReadDAO {
	public CustomRechargePlanTxnTracker findByLookupId(String lookupId);
}
