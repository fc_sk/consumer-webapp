package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.common.framework.logging.LoggingFactory;

public class RequestLookupMapDAO {
	private static Logger logger = LoggingFactory.getLogger(RequestLookupMapDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	private SimpleJdbcInsert insertJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.insertJdbcTemplate = new SimpleJdbcInsert(dataSource).withTableName("request_lookup_map")
				.usingGeneratedKeyColumns("n_last_updated", "n_created");
	}

	public void insert(int merchantId, String requestId, String lookupId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("merchant_id", merchantId);
		paramMap.put("request_id", requestId);
		paramMap.put("lookup_id", lookupId);
		this.insertJdbcTemplate.execute(paramMap);
	}

	public String getLookupIdForRequest(int merchantId, String requestId) {
		try {
			String query = "SELECT lookup_id from request_lookup_map where request_id=:requestId and merchant_id=:merchantId";
			Map<String, Object> params = new HashMap<>();
			params.put("merchantId", merchantId);
			params.put("requestId", requestId);
			return this.jdbcTemplate.queryForObject(query, params, String.class);
		} catch (Exception e) {
			logger.error("Unable to fetch lookup id for " + requestId);
		}
		return null;
	}
}
