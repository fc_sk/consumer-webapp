package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.common.businessdo.HomeBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Interface for TxnHomePageDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ITxnHomePageDAO {

    /**
     * Find all TxnHomePage entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the TxnHomePage property to query
     * @param value
     *            the property value to match
     * @return List<TxnHomePage> found by query
     */
    public List<TxnHomePage> findByProperty(String propertyName, Object value);

    public List<TxnHomePage> findByServiceNumber(Object serviceNumber);

    public List<TxnHomePage> findByOperatorName(Object operatorName);

    public List<TxnHomePage> findByCircleName(Object circleName);

    public List<TxnHomePage> findByAmount(Object amount);

    public List<TxnHomePage> findByProductType(Object productType);

    public List<TxnHomePage> findBySessionId(Object sessionId);

    public List<TxnHomePage> findByLookupId(Object lookupId);

    /**
     * Find all TxnHomePage entities.
     * 
     * @return List<TxnHomePage> all TxnHomePage entities
     */
    public List<TxnHomePage> findAll();

    public Integer saveTransactionInfo(TxnHomePage txnHomePage);

    public List<HomeBusinessDo> getTxnHomePageDetailsByLookUpId(String lookupId, BaseBusinessDO baseBusinessDO);

    public Integer findTxnHomePageIdBySessionId(Object lookupId);

    public int updateTxnHomePage(String lookupId, String newSessionId);

}
