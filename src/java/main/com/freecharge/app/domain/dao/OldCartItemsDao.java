package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.common.framework.logging.LoggingFactory;

@Repository
public class OldCartItemsDao {
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(OldCartItemsDao.class);
	
	@Autowired
	@Qualifier("jdbcReadDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<CartItems> getCartItemsOld(Integer cartId) {
		logger.info("Request comes into the getCartItemsOld method of the class OldCartItemsDao, the request  parameter type is string " + cartId);
		List<CartItems> cartItems = null;
		try {
			String queryString = "select * from cart_items_old where fk_cart_id=:cartId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("cartId", cartId);
			cartItems = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(CartItems.class));
		} catch (RuntimeException re) {
			logger.error("Error reading CartItemsOld from database for cartId: " + cartId, re);
		}
		return cartItems;
	}
}
