package com.freecharge.app.domain.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.AccessDetails;
import com.freecharge.app.domain.entity.AccessInfo;
import com.freecharge.app.domain.entity.jdbc.mappers.AccessInfoMapper;

@Component
public class ApiAuthenticatorDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public AccessInfo getAccessInfo(String accessKey) {
        String query = "select id, access_key, secret_key from access_info where access_key = :accessKey";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("accessKey", accessKey);
        List<AccessInfo> accessInfos = this.jdbcTemplate.query(query, paramMap, new AccessInfoMapper());
        return accessInfos==null || accessInfos.isEmpty()?null:accessInfos.get(0);
    }

    public List<AccessDetails> getAccessDetails(String accessKey) {
        String query = "select id, access_key, api, is_active from access_details where access_key = :accessKey";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("accessKey", accessKey);
        List<AccessDetails> accessDetails = this.jdbcTemplate.query(query, paramMap, new AccessDetailsMapper());
        return accessDetails==null?new ArrayList<AccessDetails>() : accessDetails;
    }

}
