package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.freecharge.app.domain.entity.HomepageTxnDetails;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * HomepageTxnDetails entities. Transaction control of the save(), update() and
 * delete() operations must be handled externally by senders of these methods or
 * must be manually added to each of these methods for data to be persisted to
 * the JPA datastore.
 * 
 
 * @author Rananjay
 */

public class HomepageTxnDetailsDao extends CommonDao<HomepageTxnDetails> implements IHomepageTxnDetailsDAO {
	// property constants
	public static final String SESSION_UNIQUE_ID = "sessionUniqueId";
	public static final String LOOKUP_ID = "lookupId";
	public static final String PRODUCT_SERVICE_NUMBER = "productServiceNumber";
	public static final String EMAIL = "email";
	public static final String COUPON = "coupon";
	public static final String IS_LOGGEDIN = "isLoggedin";
	public static final String MYACCOUNT_ACCESSED = "myaccountAccessed";
	public static final String ALIAS_USED = "aliasUsed";
	public static final String AMOUNT = "amount";
	public static final String PRODUCT_SELECTED = "productSelected";

    private static Logger logger = LoggingFactory.getLogger(HomepageTxnDetailsDao.class);

	protected Class<HomepageTxnDetails> getDomainClass() {
        return HomepageTxnDetails.class;
    }
	/**
	 * Find all HomepageTxnDetails entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the HomepageTxnDetails property to query
	 * @param value
	 *            the property value to match
	 * @return List<HomepageTxnDetails> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<HomepageTxnDetails> findByProperty(String propertyName,
			final Object value) {
		logger.debug("Request comes into the findByProperty method of the class HomepageTxnDetailsDao, the request input parameters are String propertyName:  "+propertyName+" and Object value:"+value);
		logger.info(
				"finding HomepageTxnDetails instance with property: "
						+ propertyName + ", value: " + value,null);
		try {
			final String queryString = "select model from HomepageTxnDetails model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<HomepageTxnDetails> homepageTxnDetails = query.list();
			logger.debug("Request goes out from the findByProperty method of the class HomepageTxnDetailsDao, the request return type is List<HomepageTxnDetails> :"+homepageTxnDetails);
			return homepageTxnDetails;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

	public List<HomepageTxnDetails> findBySessionUniqueId(Object sessionUniqueId) {
		return findByProperty(SESSION_UNIQUE_ID, sessionUniqueId);
	}

	public List<HomepageTxnDetails> findByLookupId(Object lookupId) {
		return findByProperty(LOOKUP_ID, lookupId);
	}

	public List<HomepageTxnDetails> findByProductServiceNumber(
			Object productServiceNumber) {
		return findByProperty(PRODUCT_SERVICE_NUMBER, productServiceNumber);
	}

	public List<HomepageTxnDetails> findByEmail(Object email) {
		return findByProperty(EMAIL, email);
	}

	public List<HomepageTxnDetails> findByCoupon(Object coupon) {
		return findByProperty(COUPON, coupon);
	}

	public List<HomepageTxnDetails> findByIsLoggedin(Object isLoggedin) {
		return findByProperty(IS_LOGGEDIN, isLoggedin);
	}

	public List<HomepageTxnDetails> findByMyaccountAccessed(
			Object myaccountAccessed) {
		return findByProperty(MYACCOUNT_ACCESSED, myaccountAccessed);
	}

	public List<HomepageTxnDetails> findByAliasUsed(Object aliasUsed) {
		return findByProperty(ALIAS_USED, aliasUsed);
	}

	public List<HomepageTxnDetails> findByAmount(Object amount) {
		return findByProperty(AMOUNT, amount);
	}

	public List<HomepageTxnDetails> findByProductSelected(Object productSelected) {
		return findByProperty(PRODUCT_SELECTED, productSelected);
	}

	/**
	 * Find all HomepageTxnDetails entities.
	 * 
	 * @return List<HomepageTxnDetails> all HomepageTxnDetails entities
	 */
	@SuppressWarnings("unchecked")
	public List<HomepageTxnDetails> findAll() {
		logger.debug("Request comes into the findAll method of the class HomepageTxnDetailsDao");
		logger.info("finding all HomepageTxnDetails instances",null);
		try {
			final String queryString = "select model from HomepageTxnDetails model";
			Query query = getSession().createQuery(queryString);
			List<HomepageTxnDetails> homepageTxnDetails = query.list();
			logger.debug("Request goes out from the findAll method of the class HomepageTxnDetailsDao, the request return type is List<HomepageTxnDetails> :"+homepageTxnDetails);
			return homepageTxnDetails;
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}
	}
      public void updateHomepage(HomepageTxnDetails homepageTxnDetails){
    	  logger.debug("Request comes into the updateHomepage method of the class HomepageTxnDetailsDao, the request input parameters is HomepageTxnDetails homepageTxnDetails : "+homepageTxnDetails);
          logger.info("Updating HometxDetails ...");
          try{
         Session session=      getSession();
         session .update(homepageTxnDetails);
         session.flush();
          }
          catch(Exception e){
              logger.error("exception raised while updating the Hometxdetails , exp details are " + e);
          }
        /*  String hql="update  HomepageTxnDetails set operator=:operator , circle=:circle where homepageTxnDetailsId=:homepageTxnDetailsId ";
        Query query=  getSession().createQuery(hql);
        query.setParameter("operator",homepageTxnDetails.getOperator());
        query.setParameter("circle",homepageTxnDetails.getCircle());
        query.setParameter("homepageTxnDetailsId",homepageTxnDetails.getHomepageTxnDetailsId());
        int res=    query.executeUpdate();   */
          logger.debug("Request goes out from the updateHomepage method of the class HomepageTxnDetailsDao, the request return type is void");
    }
}
