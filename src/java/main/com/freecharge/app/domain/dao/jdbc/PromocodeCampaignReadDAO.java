package com.freecharge.app.domain.dao.jdbc;

import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.PromocodeCampaignRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class PromocodeCampaignReadDAO {

	private NamedParameterJdbcTemplate jdbcTemplate;

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired 
	FreefundReadDAO freefundReadDAO;
	
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<PromocodeCampaign> getPromocodeCampaignByUserId(Long userId, Long couponId) {
	    List<PromocodeCampaign> list = new ArrayList<PromocodeCampaign>();
	    try{
	        logger.info("Promocode retrieval starts for couponId : " + couponId + ", userId : " + userId);
	        String queryString = "select * from promocode_campaign where fk_promocode_id = :couponId and fk_user_id=:userId";

	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        paramMap.put("couponId", couponId);
	        paramMap.put("userId", userId);

	        list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeCampaignRowMapper());
	        logger.info("Promocode Retrieval succeed for couponId : " + couponId + ", userId : " + userId);

	    } catch (Exception e){
	        logger.error("Promocode Retrieval failed for couponId : " + couponId + ", userId : " + userId, e);
	    }

	    return list;
	}

	public List<String> getRedeemedPromocodeOrderIdForUser(Long userId, Long freefundClassId) {
	    List<String> list = new ArrayList<String>();
	    try{
	        logger.info("Redeemed Promocode retrieval starts for userId : " + userId);
	        String queryString = "select pc.orderid from promocode_campaign pc inner join freefund_coupon fc "
	                + "on pc.fk_promocode_id = fc.freefund_coupon_id where pc.fk_user_id = :userId "
	                + "and fc.freefund_class_id = :freefundClassId and pc.status = 'REDEEMED'";

	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        paramMap.put("userId", userId);
	        paramMap.put("freefundClassId", freefundClassId);

	        list = this.jdbcTemplate.queryForList(queryString, paramMap, String.class);

	        logger.info("Promocode Retrieval succeed for userId : " + userId);

	    } catch (Exception e){
	        logger.error("Redeemed Promocode Retrieval failed for userId : " + userId, e);
	    }
	    return list;
	}

	public List<String> getRedeemedPromocodeOrderIdForServiceNumber(String serviceNumber, Long freefundClassId) {
	    List<String> list = new ArrayList<String>();
	    try{
	        logger.info("Redeemed Promocode retrieval starts for serviceNumber : " + serviceNumber);
	        String queryString = "select pc.orderid from promocode_campaign pc inner join freefund_coupon fc "
	                + "on pc.fk_promocode_id = fc.freefund_coupon_id where pc.phone_no = :serviceNumber "
	                + "and fc.freefund_class_id = :freefundClassId and pc.status = 'REDEEMED'";

	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        paramMap.put("serviceNumber", serviceNumber);
	        paramMap.put("freefundClassId", freefundClassId);

	        list = this.jdbcTemplate.queryForList(queryString, paramMap, String.class);

	        logger.info("Retrieved Redeemed FreefundCoupon List for serviceNumber : " + serviceNumber);

	    } catch (Exception e){
	        logger.error("Redeemed Promocode Retrieval failed for serviceNumber : " + serviceNumber, e);
	    }

	    return list;
	}

    public PromocodeCampaign getPromocodeCampaignFromFreeFund(Long promocodeId) {

        String queryString = "select * from promocode_campaign where fk_promocode_id=:promocodeId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("promocodeId", promocodeId);

        List<PromocodeCampaign> promocodeCampaigns = this.jdbcTemplate.query(queryString, paramMap,
                new PromocodeCampaignRowMapper());
        if (promocodeCampaigns == null || promocodeCampaigns.isEmpty()) {
            return null;
        }
        return promocodeCampaigns.get(0);
    }
    
    public List<PromocodeCampaign> getAllPromocodeCampaignFromFreeFund(Long promocodeId) {

        String queryString = "select * from promocode_campaign where fk_promocode_id=:promocodeId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("promocodeId", promocodeId);

        List<PromocodeCampaign> promocodeCampaigns = this.jdbcTemplate.query(queryString, paramMap,
                new PromocodeCampaignRowMapper());
        if (promocodeCampaigns == null || promocodeCampaigns.isEmpty()) {
            return null;
        }
        return promocodeCampaigns;
    }

    public List<Map<String, Object>> getPromocodeStatus(String key, String value, long promocodeId) {
        List<Map<String, Object>> list = new ArrayList<>();
        try{
            logger.debug("Promocode status retrieval starts for " + key + " : " + value);
            String queryString = "select status, created_date as statusDate from promocode_campaign " +
                    "where fk_promocode_id =:promocodeId and " + key + " =:value and status is not null";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("value", value);
            paramMap.put("promocodeId", promocodeId);
            list = this.jdbcTemplate.queryForList(queryString, paramMap);
            logger.debug("Retrieved Promocode status list for " + key + " : " + value);
        } catch (DataAccessException e){
            logger.error("Promocode Status Retrieval failed for " + key + " : " + value, e);
        }
        return list;
    }

    public List<Map<String, Object>> getPromocodeUsageDetails(long promocodeId, String key, String value) {
        List<Map<String, Object>> list = new ArrayList<>();
        try{
            logger.info("Promocode usage details retrieval starts for " + key + " : " + value);
            String queryString = "select pc.phone_no, " +
                    "pc.fk_user_id, " +
                    "pc.orderid, " +
                    "pc.status, " +
                    "pc.created_date as statusDate,:value " +
                    "from promocode_campaign pc " +
                    "where fk_promocode_id =:promocodeId " +
                    "and " + key + " =:value " +
                    "and status is not null " +
                    "order by created_date desc ";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("value", value);
            paramMap.put("promocodeId", promocodeId);
            list = this.jdbcTemplate.queryForList(queryString, paramMap);
            logger.info("Retrieved Promocode usage details for " + key + " : " + value);
        } catch (DataAccessException e){
            logger.error("Promocode usage details retrieval failed for " + key + " : " + value, e);
        }
        return list;
    }

	public PromocodeCampaign getPromocodeCampaignByOrderId(String orderId) {
		String queryString = "select * from promocode_campaign where orderid=:orderId";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		List<PromocodeCampaign> promocodeCampaigns = this.jdbcTemplate.query(queryString, paramMap,
				new PromocodeCampaignRowMapper());
		if (promocodeCampaigns == null || promocodeCampaigns.isEmpty()) {
			return null;
		}
		return promocodeCampaigns.get(0);
	}
}