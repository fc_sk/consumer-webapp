package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.Users;

/**
 * Interface for UsersDAO.
 * 
 * @author Rananjay
 */

public interface IUsersDAO {
	
	/**
	 * Find all Users entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Users property to query
	 * @param value
	 *            the property value to match
	 * @return List<Users> found by query
	 */
	public List<Users> findByProperty(String propertyName, Object value);

	public List<Users> findByFkAffiliateProfileId(Object fkAffiliateProfileId);

	public List<Users> findByType(Object type);

	public List<Users> findByEmail(Object email);

	public List<Users> findByPassword(Object password);

	public List<Users> findByMobileNo(Object mobileNo);

	public List<Users> findByIsActive(Object isActive);

	public List<Users> findByIsLoggedin(Object isLoggedin);

	public List<Users> findByMorf(Object morf);

	public List<Users> findByForgotPasswordKey(Object forgotPasswordKey);

	public List<Users> findByFkRefferredByUserId(Object fkRefferredByUserId);

	/**
	 * Find all Users entities.
	 * 
	 * @return List<Users> all Users entities
	 */
	public List<Users> findAll();
}