package com.freecharge.app.domain.dao;

import java.util.Date;
import java.util.List;

import com.freecharge.app.domain.entity.RechargeRetryStatus;

public interface IRechargeRetryStatusDao {
	public RechargeRetryStatus getRechargeRetryStatus(String orderId);
	public void updateRechargeRetryStatus(RechargeRetryStatus rechargeRetryStatus);
	public boolean createRechargeRetryStatus(RechargeRetryStatus rechargeRetryStatus);
	public List<RechargeRetryStatus> getActiveRetryRechargeStatusOrderIds(Date start, Date end);
}
