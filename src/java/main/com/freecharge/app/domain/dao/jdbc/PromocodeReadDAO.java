package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.Promocode;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.PromocodeRowMapper;

public class PromocodeReadDAO {

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<Promocode> getAll() {
		String queryString = "SELECT * FROM promocode ";

		Map<String, Object> paramMap = new HashMap<String, Object>();

		List<Promocode> list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());

		return list;
	}

	public Promocode getPromocode(Integer promocodeId) {
		String queryString = "select * from promocode where promocode_id = :promocodeId";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("promocodeId", promocodeId);

		List<Promocode> list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());

		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public Promocode getPromocode(String promoCode) {
		String queryString = "select * from promocode where promo_code = :promoCode and valid_from <= current_timestamp() and valid_upto >= current_timestamp()";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("promoCode", promoCode);

		List<Promocode> list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());

		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public Promocode getFirstActivePromocode() {
		String queryString = "select * from promocode where valid_from <= current_timestamp() and valid_upto >= current_timestamp()";

		Map<String, Object> paramMap = new HashMap<String, Object>();

		List<Promocode> list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());

		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public List<Promocode> getConflictingPromos(Promocode promocode) {
		String queryString = "select * from promocode where promo_code != :promoCode and valid_from <= :validUpto and valid_upto >= :validUpto";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("promoCode", promocode.getPromoCode());
		paramMap.put("validUpto", new java.sql.Date(promocode.getValidUpto().getTime()));

		List<Promocode> list = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRowMapper());
		if (list != null && list.size() > 0) {
			return list;
		} else {
			String queryString2 = "select * from promocode where promo_code != :promoCode and valid_from <= :validFrom and valid_upto >= :validFrom";

			Map<String, Object> paramMap2 = new HashMap<String, Object>();
			paramMap2.put("promoCode", promocode.getPromoCode());
			paramMap2.put("validFrom", new java.sql.Date(promocode.getValidFrom().getTime()));

			list = this.jdbcTemplate.query(queryString2, paramMap2, new PromocodeRowMapper());
		}
		return list;
	}

}