package com.freecharge.app.domain.dao;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.AuditTrailFilter;
import com.freecharge.common.framework.logging.LoggingFactory;


/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 19, 2012
 * Time: 10:01:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class AuditTrailFilterDao extends CommonDao<AuditTrailFilter>{

   protected Class<AuditTrailFilter> getDomainClass() {
        return AuditTrailFilter.class;
    }

    private static Logger logger = LoggingFactory.getLogger(AuditTrailFilterDao.class);

    public AuditTrailFilter getById(int id) {
        return (AuditTrailFilter) getSession().get(AuditTrailFilter.class, id);
    }

    @SuppressWarnings("unchecked")
    public AuditTrailFilter findByProperty(String propertyName,
                                                 final Object value) {
    	logger.debug("Request comes into the findByProperty method of the class AuditTrailFilterDao, the request input parameters is String "+propertyName+" Object "+value);
        try {
            final String queryString = "select model from AuditTrailFilter model where model."
                    + propertyName + "= :propertyValue";
            Query query = getSession().createQuery(queryString);
            query.setParameter("propertyValue", value);
            AuditTrailFilter auditTrailFilter = (AuditTrailFilter)query.uniqueResult();
            logger.debug("Request goes out from the findByProperty method of the class AuditTrailFilterDao, the request return type is AuditTrailFilter "+auditTrailFilter);
            return auditTrailFilter;
        } catch (RuntimeException re) {
            throw re;
        }
    }

    public AuditTrailFilter findByUrl(Object url) {
        return findByProperty("url", url);
    }

}
