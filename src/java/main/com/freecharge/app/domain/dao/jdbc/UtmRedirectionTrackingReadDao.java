package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmRedirectionTrackingReadDao {

    private static Logger logger = LoggingFactory.getLogger(UtmTrackerReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Map<String, Object>> getShortCodeData(String shortCode) {
        List<Map<String, Object>> utmRedirectionTrackingDetailsList = new ArrayList<>();
        try{
            String queryString = "select date(created_at) as date, count(case when channel_id = 1 " +
                    "then utm_redirection_id end) as web, count(case when channel_id = 2 then utm_redirection_id end) as mobileWeb," +
                    " count(case when channel_id = 3 then utm_redirection_id end) as android, count(case when " +
                    "channel_id = 5 then utm_redirection_id end) as ios, count(case when channel_id = 6 then " +
                    "utm_redirection_id end) as windows, count(case when channel_id = 0 or channel_id is null then utm_redirection_id end) as nulls" +
                    ", count(*) as total from utm_redirection_tracking_details where " +
                    "utm_short_code = :shortCode group by date(created_at) order by date(created_at) desc";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("shortCode", shortCode);

            utmRedirectionTrackingDetailsList = this.jdbcTemplate.queryForList(queryString, paramMap);

        } catch (Exception e){
            logger.error("Failed to fetch utm short code details for short code " + shortCode, e);
        }
        return utmRedirectionTrackingDetailsList;
    }
}
