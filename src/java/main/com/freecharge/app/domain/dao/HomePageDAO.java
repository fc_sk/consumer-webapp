package com.freecharge.app.domain.dao;

import java.sql.Date;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.freecharge.app.domain.entity.jdbc.CartItemsMetadata;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.util.JsonUtil;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.rest.recharge.TxnHomePageBO;

@Component
public class HomePageDAO {
    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertTxnHomePage;
    private SimpleJdbcInsert insertCart;
    private SimpleJdbcInsert insertCartItems;
    private SimpleJdbcInsert insertUserRechargeContact;
    private SimpleJdbcInsert insertTxnFulfillment;
    
    private final int CART_EXPIRY_PERIOD_DAYS = 1; // one day.
    private final String HANDLING_CHARGES_DISPLAY = "Handling Charges";
    private final Float ZERO_HANDLING_CHARGE = new Float(0);

    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jt = new NamedParameterJdbcTemplate(dataSource);
        
        this.insertTxnHomePage = new SimpleJdbcInsert(dataSource).withTableName("txn_home_page").usingGeneratedKeyColumns("id", "creatred_on", "n_last_updated", "n_created");
        
        this.insertCart = new SimpleJdbcInsert(dataSource).withTableName("cart").usingGeneratedKeyColumns("cart_id", "n_last_updated", "n_created");
        this.insertCartItems = new SimpleJdbcInsert(dataSource).withTableName("cart_items").usingGeneratedKeyColumns("cart_items_id", "n_last_updated", "n_created");
        
        this.insertUserRechargeContact = new SimpleJdbcInsert(dataSource).withTableName("user_recharge_contact").usingGeneratedKeyColumns("user_recharge_contact_id", "updated_on", "n_last_updated", "n_created");
        this.insertTxnFulfillment = new SimpleJdbcInsert(dataSource).withTableName("txn_fulfilment").usingGeneratedKeyColumns("id", "updated_at", "n_last_updated", "n_created");
    }
    
    public TxnHomePageBO saveInfoAndCreateCart(TxnHomePage txnHomePage, String userId, String userEmail, String rechargeType, Integer planId, String planCategory) {
            Map<String, Object> homePageParams = new HashMap<String, Object>();
            homePageParams.put("service_number", txnHomePage.getServiceNumber());
            homePageParams.put("operator_name", txnHomePage.getOperatorName());
            homePageParams.put("circle_name", txnHomePage.getCircleName());
            homePageParams.put("amount", txnHomePage.getAmount());
            homePageParams.put("product_type", txnHomePage.getProductType());

            homePageParams.put("session_id", txnHomePage.getSessionId());
            homePageParams.put("lookup_id", txnHomePage.getLookupId());

            homePageParams.put("fk_product_master_id", txnHomePage.getFkProductId());
            homePageParams.put("fk_circle_master_id", txnHomePage.getFkCircleId());
            homePageParams.put("fk_operator_master_id", txnHomePage.getFkOperatorId());

            Number homePageId = insertTxnHomePage.executeAndReturnKey(homePageParams);

            DateTime now = new DateTime();
            DateTime tomorrow = now.plusDays(CART_EXPIRY_PERIOD_DAYS);

            Map<String, Object> cartParams = new HashMap<String, Object>();
            cartParams.put("created_on", new Date(now.getMillis()));
            cartParams.put("expires_on", new Date(tomorrow.getMillis()));
            cartParams.put("fk_user_id", userId);
            cartParams.put("lookup_id", txnHomePage.getLookupId());

            Number cartId = insertCart.executeAndReturnKey(cartParams);

            {
                Map<String, Object> rechargeCartItem = new HashMap<String, Object>();
                rechargeCartItem.put("added_on", new Date(now.getMillis()));
                rechargeCartItem.put("fk_cart_id", cartId.toString());
                rechargeCartItem.put("fl_product_master_id", txnHomePage.getFkProductId());
                rechargeCartItem.put("display_label", txnHomePage.getServiceNumber());
                rechargeCartItem.put("is_deleted", 0);
                rechargeCartItem.put("price", txnHomePage.getAmount());
                rechargeCartItem.put("quantity", 1);
                rechargeCartItem.put("entity_id", rechargeType);
                CartItemsMetadata cartItemsMetadata = new CartItemsMetadata();
                cartItemsMetadata.setPlanId(planId);
                cartItemsMetadata.setPlanCategory(planCategory);
                rechargeCartItem.put("metadata", JsonUtil.getJson(cartItemsMetadata));

                Map<String, Object> handlingCharge = new HashMap<String, Object>();
                handlingCharge.put("added_on", new Date(now.getMillis()));
                handlingCharge.put("fk_cart_id", cartId.toString());
                handlingCharge.put("fl_product_master_id", ProductName.Charges.getProductId());
                handlingCharge.put("display_label", HANDLING_CHARGES_DISPLAY);
                handlingCharge.put("is_deleted", 0);
                handlingCharge.put("price", ZERO_HANDLING_CHARGE);
                handlingCharge.put("quantity", 1);

                Map<String, Object>[] cartItems = new HashMap[2];
                cartItems[0] = rechargeCartItem;
                cartItems[1] = handlingCharge;

                insertCartItems.executeBatch(cartItems);
            }

            {
                Map<String, Object> rechargeContactRow = new HashMap<String, Object>();
                rechargeContactRow.put("created_on", new Date(now.getMillis()));
                rechargeContactRow.put("fk_circle_master_id", txnHomePage.getFkCircleId());
                rechargeContactRow.put("fk_operator_master_id", txnHomePage.getFkOperatorId());
                rechargeContactRow.put("fk_product_master_id", txnHomePage.getFkProductId());
                rechargeContactRow.put("fk_users_id", userId);
                rechargeContactRow.put("is_active", 1);
                rechargeContactRow.put("last_recharge_amount", txnHomePage.getAmount());
                rechargeContactRow.put("service_number", txnHomePage.getServiceNumber());

                insertUserRechargeContact.execute(rechargeContactRow);
            }

            {
                Map<String, Object> fulfillmentRow = new HashMap<String, Object>();
                fulfillmentRow.put("created_at", new Date(now.getMillis()));
                fulfillmentRow.put("email", userEmail);
                fulfillmentRow.put("txn_home_page_id", homePageId.toString());
                fulfillmentRow.put("is_guest", false);

                insertTxnFulfillment.executeAndReturnKey(fulfillmentRow);
            }

            return new TxnHomePageBO(txnHomePage.getLookupId(), homePageId.toString());
    }
}
