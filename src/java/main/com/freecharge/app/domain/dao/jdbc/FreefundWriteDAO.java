package com.freecharge.app.domain.dao.jdbc;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FreefundWriteDAO {

	private static Logger logger = LoggingFactory.getLogger(FreefundWriteDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private SimpleJdbcInsert insertFreefundClass;

	private DataSource dataSource;

	public static final String ERRORS = "errors";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
        this.insertFreefundClass = new SimpleJdbcInsert(dataSource).withTableName("freefund_class").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}
	
    public int upload(Map<String, Object>[] freeFundCodesMapBatchValues){
        //INSERT INTO freefund_coupon(freefund_code,generated_date,freefund_value,min_recharge_value,promo_entity) VALUES ('hdfcdebittest',now(),40,50,'hdfc-debitcard');
        String sql = "insert into freefund_coupon(freefund_class_id, freefund_code,generated_date,freefund_value,min_recharge_value,promo_entity) "
                + "values(:freefundClassId, :freefundCode,now(),:freefundValue,:minRechargeValue,:promoEntity)";
        int[] rows = this.jdbcTemplate.batchUpdate(sql, freeFundCodesMapBatchValues);
        return rows.length;
    }

	public FreefundClass createFreefundClass(FreefundClass freefundClass) {
		SqlParameterSource parameters = new BeanPropertySqlParameterSource(freefundClass);

		Number number = this.insertFreefundClass.executeAndReturnKey(parameters);

		freefundClass.setId(number.intValue());

		return freefundClass;
	}

	public boolean saveFreefundClass(FreefundClass freefundClass) {
		if (freefundClass != null) {
			String sql = "update freefund_class set name=:name,description=:description,valid_from=:validFrom,"
					+ "valid_upto=:validUpto, max_redemptions=:maxRedemptions, promo_entity=:promoEntity, "
					+ "min_recharge_value=:minRechargeValue, freefund_value=:freefundValue, discount_type=:discountType, "
					+ "max_discount=:maxDiscount, apply_condition_id=:applyConditionId, payment_condition_id=:paymentConditionId, "
					+ "campaign_sponsorship=:campaignSponsorship, campaign_description=:campaignDescription, datamap=:datamap, isNewCampaign=:isNewCampaign where id=:id";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", freefundClass.getId());
			paramMap.put("name", freefundClass.getName());
            paramMap.put("isNewCampaign", freefundClass.getIsNewCampaign());
			paramMap.put("maxRedemptions", freefundClass.getMaxRedemptions());
			paramMap.put("promoEntity", freefundClass.getPromoEntity());
			paramMap.put("minRechargeValue", freefundClass.getMinRechargeValue());
			paramMap.put("freefundValue", freefundClass.getFreefundValue());
			paramMap.put("description", freefundClass.getDescription());
			paramMap.put("validFrom", freefundClass.getValidFrom());
			paramMap.put("validUpto", freefundClass.getValidUpto());
			paramMap.put("discountType", freefundClass.getDiscountType());
			paramMap.put("maxDiscount", freefundClass.getMaxDiscount());
			paramMap.put("applyConditionId", freefundClass.getApplyConditionId());
			paramMap.put("paymentConditionId", freefundClass.getPaymentConditionId());
			paramMap.put("campaignSponsorship", freefundClass.getCampaignSponsorship());
			paramMap.put("campaignDescription", freefundClass.getCampaignDescription());
            paramMap.put("datamap", freefundClass.getDatamap());
			this.jdbcTemplate.update(sql, paramMap);
			return true;
		} else {
			return false;
		}
	}
	public boolean insert(FreefundCoupon freefundCode) {
		//INSERT INTO freefund_coupon(freefund_code,generated_date,freefund_value,min_recharge_value,promo_entity) VALUES ('hdfcdebittest',now(),40,50,'hdfc-debitcard');
		String sql = "insert into freefund_coupon(freefund_class_id, freefund_code,generated_date,freefund_value,min_recharge_value,promo_entity) "
				+ "values(:freefundClassId,:freefundCode,now(),:freefundValue,:minRechargeValue,:promoEntity)";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("freefundClassId", freefundCode.getFreefundClassId());
		paramMap.put("freefundCode", freefundCode.getFreefundCode());
		paramMap.put("freefundValue", freefundCode.getFreefundValue());
		paramMap.put("minRechargeValue", freefundCode.getMinRechargeValue());
		paramMap.put("promoEntity", freefundCode.getPromoEntity());
		
		this.jdbcTemplate.update(sql, paramMap);
		return true;
	}
	
	public boolean blockCouponCode(String coupon, Long couponCodeId, String email, String serviceNumber) {
	    
	    logger.info("blockCouponCode for coupon: " + coupon + ", couponCodeId: " + couponCodeId + ", serviceNumber: " + serviceNumber);
	    String sql = "update freefund_coupon set used_email_id =:used_email_id ,used_service_number=:used_service_number, used_date=now(), status = 'BLOCKED'"+
                " where freefund_coupon_id=:freefund_coupon_id and status is null ";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("used_email_id", email);
        paramMap.put("used_service_number", serviceNumber);
        paramMap.put("freefund_coupon_id", couponCodeId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        if (updateSuccessful) {
            logger.info("Successfully blocked couponCode for id  " +  couponCodeId);
        } else {
            logger.info("Failed to block  couponCode for id  " +  couponCodeId + " number of rows updated " + updateCount);
        }
        return updateSuccessful;
	}
	
	public boolean releaseCouponCode(Long couponCodeId, boolean shouldForce) {
	    logger.info("releaseCouponCode for couponCodeId: " + couponCodeId);
	    
	    String sql = "update freefund_coupon set used_email_id = null ,used_service_number=null, used_date=null, status=null "+
                " where freefund_coupon_id=:freefund_coupon_id and status != 'REDEEMED'";
	    
        if(shouldForce) {
            sql = "update freefund_coupon set used_email_id = null ,used_service_number=null, used_date=null, status=null "+
                    " where freefund_coupon_id=:freefund_coupon_id";
        }
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("freefund_coupon_id", couponCodeId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        
        if (updateSuccessful) {
            logger.info("Successfully released couponCode for id  " +  couponCodeId);
        } else {
            logger.info("Failed to release  couponCode for id  " +  couponCodeId + " number of rows updated " + updateCount);
        }

        return updateSuccessful;
	}
	
	public boolean markFreefundRedeemed(Long couponCodeId) {
        logger.info("markFreefundRedeemed for couponCodeId: " + couponCodeId);
        String sql = "update freefund_coupon set status = 'REDEEMED' "+
                " where freefund_coupon_id=:freefund_coupon_id and status = 'BLOCKED' ";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("freefund_coupon_id", couponCodeId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;

        if (updateSuccessful) {
            logger.info("Successfully redeemed couponCode for id  " +  couponCodeId);
        } else {
            logger.info("Failed to redeem  couponCode for id  " +  couponCodeId + " number of rows updated " + updateCount);
        }

        return updateSuccessful;
    }
	
	public boolean deleteBinOffer(Integer id) { 
		String sql = "UPDATE freefund_coupon SET freefund_class_id =-1 WHERE freefund_coupon_id =:id ";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", id);
		
		this.jdbcTemplate.update(sql, paramMap);
		return true;
	}

	public void blockCodes(List<String> freefundCoupons, long freefundClassId) {
		String sql = "update freefund_coupon set status='BLOCKED' "+
				" where freefund_class_id=:freefundClassId and freefund_code in (:freefundCoupons)";

		logger.info("Block Codes Query: " + sql);
		logger.info("freefundCoupons: " + freefundCoupons);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("freefundClassId", freefundClassId);
		paramMap.put("freefundCoupons", freefundCoupons);

		int updateCount = this.jdbcTemplate.update(sql, paramMap);
		final boolean updateSuccessful = updateCount != 0;

		if (updateSuccessful) {
			logger.info("Successfully blocked freefundClass for id  " +  freefundClassId);
		} else {
			logger.info("Failed to block couponCodes for id  " +  freefundCoupons +
					" number of rows updated " + updateCount);
		}
	}

	public void deleteCodes(List<String> couponcodes, long freefundClassId) {
		String sql = "delete from freefund_coupon "+
				" where freefund_class_id=:freefundClassId and freefund_code in (:freefundCoupons)";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("freefundClassId", freefundClassId);
		paramMap.put("freefundCoupons", couponcodes);

		int updateCount = this.jdbcTemplate.update(sql, paramMap);
		final boolean updateSuccessful = updateCount != 0;

		if (updateSuccessful) {
			logger.info("Successfully deleted freefund codes for id  " +  freefundClassId);
		} else {
			logger.info("Failed to delete couponCodes for id  " +  freefundClassId + " number of rows updated "
					+ updateCount);
		}
	}
}