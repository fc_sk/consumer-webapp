package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.CrosssellRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class CrosssellReadDAO {
	private static Logger logger = LoggingFactory.getLogger(CrosssellReadDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	/**
	 * Find all Crosssell entities.
	 * 
	 * @return List<Crosssell> all Crosssell entities
	 */
	@SuppressWarnings("unchecked")
	public List<Crosssell> findAll() {
		logger.info("finding all Crosssell instances", null);
		List<Crosssell> list = new ArrayList<Crosssell>();
//		try {
//			String queryString = "SELECT csl.id, csl.xml, csl.label,csl. category, csl.created_date, csl.created_by, csl.modified_on, csl.modified_by, csl.effective_from, " +
//					"csl.valid_upto, csl.count_uses, csl.price, csl.is_active " +
//					"FROM crosssell csl JOIN coupon_store cs ON csl.id = cs.fk_crosssell_id WHERE cs.coupon_type='C' " +
//					"AND cs.is_active =1 AND csl.effective_from <= current_date() AND csl.valid_upto >= current_date() order by cs.coupon_position";
//
//			Map<String, Object> paramMap = new HashMap<String, Object>();
//
//			list = this.jdbcTemplate.query(queryString, paramMap, new CrosssellRowMapper());
//
//		} catch (RuntimeException re) {
//			logger.info("find all failed", re);
//			throw re;
//		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Crosssell> getAllCrosssell() {
		logger.debug("request goes inside getAllCrosssell");
		List<Crosssell> list = new ArrayList<Crosssell>();
//		try {
//			String queryString = "SELECT csl.id, csl.xml, csl.label,csl. category, csl.created_date, csl.created_by, csl.modified_on, csl.modified_by, csl.effective_from, " +
//					"csl.valid_upto, csl.count_uses, csl.price, cs.is_active " +
//					"FROM crosssell csl JOIN coupon_store cs ON csl.id = cs.fk_crosssell_id WHERE cs.coupon_type='C'  order by cs.coupon_position";
//
//			Map<String, Object> paramMap = new HashMap<String, Object>();
//
//			list = this.jdbcTemplate.query(queryString, paramMap, new CrosssellRowMapper());
//			
//			logger.debug("request goes inside getAllCrosssell. Crosssells found " + list.size() );
//		} catch (RuntimeException re) {
//			logger.error("find all failed", re);
//			throw re;
//		}
		return list;
		
	}

	public Crosssell getCrosssell(Integer id) {
		logger.info("finding all Crosssell instances", null);
		try {
			String queryString = "select * from crosssell where id = :id";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", id);

			List<Crosssell> list = this.jdbcTemplate.query(queryString, paramMap, new CrosssellRowMapper());

			if (list != null && list.size() > 0)
				return list.get(0);
			else
				return null;
		} catch (RuntimeException re) {
			logger.info("find all failed", re);
			throw re;
		}
	}

	public List<Crosssell> getCrosssellCouponByProperty(Integer amount, String category) {
		logger.debug("finding all getCrosssellCouponByProperty instances for amount = " + amount + " and category = " + category);
		try {
			final String queryString = "select * from crosssell where price = :amount and category= :category ";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("amount", amount);
			paramMap.put("category", category);

			List<Crosssell> list = this.jdbcTemplate.query(queryString, paramMap, new CrosssellRowMapper());

			return list;
		} catch (RuntimeException re) {
			logger.info("find all failed", re);
			throw re;
		}

	}
	
	public List<Map<String, Object>> getCrossSellDetailsBylookupId(String lookupId) {
		logger.debug("Request comes into the getCrossSellDetailsBylookupId method of the class CrosssellReadDAO" + lookupId);
		List<Map<String, Object>> crosssellList = null;
//		try {
//			/*String queryWithCrossSell = "select tcs.corss_sell_id as crosssellid ,tcs.quantity as count , " +
//					"crs.xml as crosssellxml , crs.price as crosssellamount , cs.coupon_store_id as couponStoreId ," +
//					"tcs.is_complimentary as isComplimentary ,cs.campaign_name as couponName ,cs.coupon_value as couponValue ," +
//					" cs.coupon_type as couponType, cc.coupon_code as couponCode , cs.coupon_image_path as couponImagePath ,cs.validity_date as expireDate " +
//					"from crosssell crs,txn_home_page thp ,txn_cross_sell tcs,coupon_store cs ,coupon_inventory ci , " +
//					"coupon_code cc " +
//					"where thp.lookup_id = :lookupId and tcs.txn_home_page_id = thp.id and " +
//					"tcs.corss_sell_id <> 0 and crs.id = tcs.corss_sell_id and cs.fk_crosssell_id = crs.id and " +
//					"ci.fk_coupon_store_id = cs.coupon_Store_Id and cc.fk_coupon_inventory_id = ci.coupon_inventory_id ";*/
//			String queryWithCrossSell = "select tcs.corss_sell_id as crosssellid, tcs.quantity as count, crs.xml as crosssellxml, " +
//					"crs.price as crosssellamount, cs.coupon_store_id as couponStoreId, tcs.is_complimentary as isComplimentary, " +
//					"cs.campaign_name as couponName, cs.coupon_value as couponValue, cs.coupon_type as couponType, " +
//					"cs.coupon_image_path as couponImagePath, cs.validity_date as expireDate, cs.is_unique as isUnique, " +
//					"crs.label as crossSellLabel, crs.effective_from as crossSellValidFrom, crs.valid_upto as crossSellValidTo, cs.coupon_type as couponType," +
//					"cs.city_id_list as cityIdList, cs.is_active as couponIsActive, cs.is_m_coupon as isMCoupon " +
//					"from crosssell crs join txn_cross_sell tcs on crs.id = tcs.corss_sell_id " +
//					"join txn_home_page thp on tcs.txn_home_page_id = thp.id " +
//					"join coupon_store cs on cs.fk_crosssell_id = crs.id " +
//					"join coupon_inventory ci on ci.fk_coupon_store_id = cs.coupon_Store_Id " +
//					"where thp.lookup_id = :lookupId and  tcs.corss_sell_id <> 0 and tcs.is_deleted = 0";
//
//			Map<String, Object> paramMap = new HashMap<String, Object>();
//			paramMap.put("lookupId", lookupId);
//
//			crosssellList = this.jdbcTemplate.queryForList(queryWithCrossSell, paramMap);
//
//		} catch (Exception e) {
//			logger.error("getCrossSellDetailsBylookupId failed", e);
//		}
		logger.debug("Request goes out from the getCrossSellDetailsBylookupId method of the class CrosssellReadDAO" + lookupId);
		return crosssellList;
	}
//	public Integer getPriceFromCrosssellbyCrosssellId(Integer id) {
//		logger.debug("Request comes into the getPriceFromCrosssellbyCrosssellId method of the class CrosssellReadDAO" + id);
//		final String queryString = "select * from crosssell c join coupon_store cs on c.id=cs.fk_crosssell_id = :id ";
//		try {
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//		paramMap.put("id", id);
//		List<Crosssell> crosssell = this.jdbcTemplate.query(queryString, paramMap, new CrosssellRowMapper());
//		return Integer.parseInt(crosssell.get(0).toString());
//	} catch (RuntimeException re) {
//		logger.info("find all failed", re);
//		throw re;
//	 }	
//			
//  }
	
}