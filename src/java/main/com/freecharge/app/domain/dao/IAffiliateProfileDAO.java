package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.AffiliateProfile;

/**
 * Interface for AffiliateProfileDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IAffiliateProfileDAO {
	
	/**
	 * Find all AffiliateProfile entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AffiliateProfile property to query
	 * @param value
	 *            the property value to match
	 * @return List<AffiliateProfile> found by query
	 */
	public List<AffiliateProfile> findByProperty(String propertyName,
			Object value);

	public List<AffiliateProfile> findByAffliateUniqueId(Object affliateUniqueId);

	public List<AffiliateProfile> findByCompany(Object company);

	public List<AffiliateProfile> findByContactName(Object contactName);

	public List<AffiliateProfile> findByEmail(Object email);

	public List<AffiliateProfile> findByPhone(Object phone);

	public List<AffiliateProfile> findByExt(Object ext);

	public List<AffiliateProfile> findByFax(Object fax);

	public List<AffiliateProfile> findByAddress1(Object address1);

	public List<AffiliateProfile> findByAddress2(Object address2);

	public List<AffiliateProfile> findByCity(Object city);

	public List<AffiliateProfile> findByState(Object state);

	public List<AffiliateProfile> findByPostalCode(Object postalCode);

	public List<AffiliateProfile> findBySiteName(Object siteName);
	
	public List<AffiliateProfile> findById(Object siteName);

	public List<AffiliateProfile> findByUserId(Object userId);
	/**
	 * Find all AffiliateProfile entities.
	 * 
	 * @return List<AffiliateProfile> all AffiliateProfile entities
	 */
	public List<AffiliateProfile> findAll();
	
	public int insertAffiliateProfile(AffiliateProfile affiliateProfile);
	
	public Integer updateAffiliateProfile(Integer userId,Integer id);
	
	public List<AffiliateProfile> findByUserIdAndAffliateUniqueId(Integer userId,String affliateUniqueId);
}