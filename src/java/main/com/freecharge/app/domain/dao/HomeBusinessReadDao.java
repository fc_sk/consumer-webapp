package com.freecharge.app.domain.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.freecharge.app.domain.entity.Users;

public class HomeBusinessReadDao extends BaseDao {

    /**
     * User Authentication using email and password.
     *
     * @param email    email property to query
     * @param password password property to query
     * @return Users found by query
     */
    
    @SuppressWarnings("unchecked")
    public Users getOrdinaryUser(String email) {
        Users user = null;
        Session session = getSession();

        Criteria criteria = session.createCriteria(Users.class);
        criteria.add(Restrictions.eq("email", email));
        criteria.add(Restrictions.eq("isActive", true));
        List<Users> userList = criteria.list();
        if (userList.size() == 1) {
            user = (Users) userList.get(0);
        }
        return user;
    }

    /**
     * User Authentication using email and password.
     *
     * @param email    email property to query
     * @param password password property to query
     * @return Users found by query
     */
    
    @SuppressWarnings("unchecked")
    public Users getAuthenticateUser(String email, String password) {
        Users user = null;
        Session session = getSession();

        Criteria criteria = session.createCriteria(Users.class);
        criteria.add(Restrictions.eq("email", email));
        criteria.add(Restrictions.eq("password", password));
        criteria.add(Restrictions.eq("isActive", true));
        List<Users> userList = criteria.list();
        if (userList.size() == 1) {
            user = (Users) userList.get(0);
        }
        return user;
    }

}
