package com.freecharge.app.domain.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: April 21, 2012 Time: 12:21:04
 * PM To change this template use File | Settings | File Templates.
 */
public abstract class CommonDao<T extends Serializable> extends BaseDao implements ICommonDao<T> {

	protected Class<T> entityClass = getDomainClass();

    protected final Logger logger = LoggingFactory.getLogger(getClass().getName());
//	private static Logger logger = LoggingFactory.getLogger(SiteAutosuggestPrefixDAO.class);

	/**
	 * Method to return the class of the POJO object
	 */
	protected abstract Class<T> getDomainClass();

	public T create(T t) {
		this.getSession().save(t);
		return t;
	}

	public T read(Long id) {
		return (T) this.getSession().get(entityClass, id);
	}
	
	
	public T read(Integer id) {
		return (T) this.getSession().get(entityClass, id);
	}

	public T update(T t) {
		return (T) this.getSession().merge(t);
	}

	public void delete(T t) {
		t = (T) this.getSession().merge(t);
		this.getSession().delete(t);
	}

	public List<T> findAll() {
		try {
			final String queryString = "from " + entityClass.getName();
			logger.info("this.getSession()====" + this.getSession());
			Query query = this.getSession().createQuery(queryString);
			return query.list();
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}
	}

	public List<T> findByHQL(String qry, List paramList) {
		// EntityManagerHelper.log("finding Some Desirable FcOperator instances",
		// Level.INFO,null);
		try {

			int count = StringUtils.countMatches(qry, "?");
			Query query = this.getSession().createQuery(qry);
			for (int i = 0, j = 0; i < count && j < paramList.size(); i++, j++) {
				Object pram = paramList.get(j);
				query.setParameter(i, pram);
			}
			return query.list();
		} catch (RuntimeException re) {
			// EntityManagerHelper.log("find Desirable failed", Level.SEVERE,
			// re);
			throw re;
		}
	}

}
