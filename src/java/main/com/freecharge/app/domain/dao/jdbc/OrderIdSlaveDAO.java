package com.freecharge.app.domain.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.admin.CustomerTrailDetails.OrderIdOidMapper;
import com.freecharge.admin.CustomerTrailDetails.OrderUserIdMapper;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.mappers.CartItemsRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdLidMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.TxnHomePageMapper;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.exception.InvalidOrderIDException;
import com.freecharge.recharge.util.RechargeConstants;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class OrderIdSlaveDAO {

    private static Logger logger = LoggingFactory.getLogger(OrderIdSlaveDAO.class);
    
    private final static String APP_VERSION = "appVersion";
    
    private final static String TABLE_NAME = "app_version_order";
    
    private final static String ORDER_ID_FIELD = "orderId";

    private NamedParameterJdbcTemplate jdbcTemplate;
    
    private NamedParameterJdbcTemplate masterJdbcTemplate;
    
    @Autowired
	AmazonDynamoDBAsync dynamoClient;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    @Autowired
    public void setMasterDataSource(DataSource dataSource) {
        this.masterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    /**
     * This has been deliberately placed here since for some reason
     * the query in CartDAO times out.
     * @param orderId
     * @return
     */
    public List<CartItemsBusinessDO> getCartItemsByOrderId(String orderId) {
        String sql = "select ci.* from " +
                "cart_items ci, order_id oi, cart c where " +
                "oi.order_id = :orderId and oi.lookup_id = c.lookup_id and c.cart_id = ci.fk_cart_id;";
        
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("orderId", orderId);
        List<CartItemsBusinessDO> cartItemsList = this.jdbcTemplate.query(sql, param, new CartItemsRowMapper());
        
        return cartItemsList;
    }
    
    public List<CartItemsBusinessDO> getCartItemsByLookupId(String lookupId) {
        String sql = "select ci.* from " +
                "cart_items ci, cart c where " +
                "c.cart_id = ci.fk_cart_id and " +
                "c.lookup_id = :lookupId;";
        
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("lookupId", lookupId);
        List<CartItemsBusinessDO> cartItemsList = this.jdbcTemplate.query(sql, param, new CartItemsRowMapper());

        if (cartItemsList == null || cartItemsList.isEmpty()) {
            return this.masterJdbcTemplate.query(sql, param, new CartItemsRowMapper());
        }

        return cartItemsList;
    }
    
    public String getRechargeTypeByLookupId(String lookupId) {
        List<Map<String, Object>> resultList = getRechargeTypeDataByLookupId(lookupId);
        return getRechargeTypeResult(resultList);
    }
    public String getRechargeTypeByOrderId(String orderId) {
        List<Map<String, Object>> resultList = getRechargeTypeDataByOrderId(orderId);
        return getRechargeTypeResult(resultList);
    }

	private String getRechargeTypeResult(List<Map<String, Object>> resultList) {
		if (resultList == null || resultList.isEmpty()) {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        }
        
        String rechargeType = (String) resultList.get(0).get("recharge_type");
        
        if (StringUtils.isBlank(rechargeType)) {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        } else if (RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL.equals(rechargeType)) {
            return RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL;
        } else {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        }
	}
    
    private List<Map<String, Object>> getRechargeTypeDataByOrderId(
			String orderId) {
		String query = "select ci.entity_id as recharge_type from cart_items ci, cart c, order_id o where " +
                "c.lookup_id = o.lookup_id and o.order_id = :orderId and " +
                "c.cart_id = ci.fk_cart_id and " +
                "ci.fl_product_master_id in (1, 2, 3);";

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("orderId", orderId);
        List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(query, param);
		return resultList;
	}

	private List<Map<String, Object>> getRechargeTypeDataByLookupId(
			String lookupId) {
		String query = "select ci.entity_id as recharge_type from cart_items ci, cart c where " +
                "c.lookup_id = :lookupId and " +
                "c.cart_id = ci.fk_cart_id and " +
                "ci.fl_product_master_id in (1, 2, 3);";

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("lookupId", lookupId);
        List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(query, param);
		return resultList;
	}
    
    public boolean hasCoupons(String orderId) {
        String sql = "select ci.* from cart_items ci, cart c, order_id oid where " +
                "oid.order_id = :orderId and " +
                "oid.lookup_id = c.lookup_id and " +
                "c.cart_id = ci.fk_cart_id and " +
                "ci.is_deleted = 0 and " +
                "(ci.fl_product_master_id = 4 or ci.fl_product_master_id = 15)and " +
                "ci.quantity > 0;";
        
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("orderId", orderId);
        List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(sql, param);
        
        if (resultList != null && !resultList.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    
    public Integer getUserIdFromOrderId(String orderId){
        String sql = "select c.fk_user_id from order_id oid, cart c where c.lookup_id = oid.lookup_id and order_id = :orderId";
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("orderId", orderId);
        List<String> userIdList = this.jdbcTemplate.query(sql, param, new OrderUserIdMapper());
        if (userIdList == null || userIdList.size() == 0){
        	logger.error("userId list is empty in slave for orderId: " + orderId);
            userIdList = this.masterJdbcTemplate.query(sql, param, new OrderUserIdMapper());
        }
        if (userIdList == null || userIdList.size() == 0){
        	logger.error("userId list is empty in master for orderId: " + orderId);
            return null;
        }
        Integer userIdInt = Integer.parseInt(userIdList.get(0));
        return userIdInt;
    }
    
    public String getOrderIdForLookUpId(String lookup_id) {
        String sql = "select o.order_id from order_id o where o.lookup_id = :lookup_id";
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("lookup_id", lookup_id);
        List<String> orderids = this.jdbcTemplate.query(sql, param, new OrderIdOidMapper());
        
        if (orderids == null || orderids.isEmpty()) {
            orderids = this.masterJdbcTemplate.query(sql, param, new OrderIdOidMapper());
        }
        
        String orderId = null;
        if (orderids != null && orderids.size() >0 ) {
            orderId = orderids.get(0); 
        }
        return orderId;
    }
    
    public String getLookupIdForOrderId(String orderId) {
        String queryString = "select lookup_id from order_id o where o.order_id = :orderId";
        logger.info("Inside OrderId dao: " + orderId);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        
        List<String> lidList = this.jdbcTemplate.query(queryString, paramMap, new OrderIdLidMapper());
        
        if(lidList == null || lidList.isEmpty()) {
            lidList = this.masterJdbcTemplate.query(queryString, paramMap, new OrderIdLidMapper());
        }
        if(lidList == null || lidList.isEmpty()) {
            throw new InvalidOrderIDException("Received empty set of lookup ids for order ID : " + orderId);
        }
        return lidList.get(0);
    }
    
    public String getBillIdForOrderId(String orderId, String productType) {
    	Integer productId = FCConstants.productMasterMapReverse.get(productType); 
        String queryString = "select ci.display_label from " +
        		"cart_items ci, cart c, order_id oi where " +
        		"c.cart_id = ci.fk_cart_id and oi.lookup_id = c.lookup_id " +
        		"and is_deleted = 0 and ci.fl_product_master_id=:productId and " +
        		"oi.order_id = :orderId;";
        
        String alternateQueryString = "select ci.display_label from " +
        		"cart_items_old ci, cart c, order_id oi where " +
        		"c.cart_id = ci.fk_cart_id and oi.lookup_id = c.lookup_id " +
        		"and is_deleted = 0 and ci.fl_product_master_id=:productId and " +
        		"oi.order_id = :orderId;";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        paramMap.put("productId", productId);
        
        List<String> billIdList = this.jdbcTemplate.query(queryString, paramMap, new RowMapper<String>() {
           @Override
            public String mapRow(ResultSet rs, int row) throws SQLException {
                return rs.getString("display_label");
            }
        });
        
        if (billIdList == null || billIdList.isEmpty()) {
            billIdList = this.masterJdbcTemplate.query(queryString, paramMap, new RowMapper<String>() {
                @Override
                 public String mapRow(ResultSet rs, int row) throws SQLException {
                     return rs.getString("display_label");
                 }
             });

        }
        if (billIdList == null || billIdList.isEmpty()) {
        	//check in alternate table 
        	List<String> alternateBillIdList = this.jdbcTemplate.query(alternateQueryString, paramMap, new RowMapper<String>() {
                @Override
                 public String mapRow(ResultSet rs, int row) throws SQLException {
                     return rs.getString("display_label");
                 }
             });
        	
        	if (alternateBillIdList == null || alternateBillIdList.isEmpty()) {
	            //throw new InvalidOrderIDException("Received empty set of bill Ids for order ID : " + orderId);
	        	//not throwing exception here anymore. Instead logging it in and returning back
	        	logger.error("Received empty set of bill Ids for order ID : " + orderId);
	        	return null;
        	}
        	return alternateBillIdList.get(0);
        }
        return billIdList.get(0);
    }
    
    public ProductName getProductName(String orderId) {
        String queryString = "select fk_product_master_id from order_id where order_id = :orderId;";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        
        Integer productId;
        try {
            productId = this.jdbcTemplate.queryForInt(queryString, paramMap);
        } catch (Exception e) {
            logger.info("Querying for productname in master for "+orderId+" due to non-availability in slave");
            productId = this.masterJdbcTemplate.queryForInt(queryString, paramMap);
        }
        
        return ProductName.fromProductId(productId);
    }

    public String getChannelId(String lookupId) {
        logger.debug("Getting ChannelId for lookupID: " + lookupId);
        String queryString = "select channel_id from order_id where lookup_id = :lookupId;";
        Map<String, Object> paramMap = new HashMap();
        paramMap.put("lookupId", lookupId);

        List<Integer> channelIdList = this.jdbcTemplate.queryForList(queryString, paramMap, Integer.class);
        if (channelIdList == null || channelIdList.isEmpty()) {
            channelIdList = this.masterJdbcTemplate.queryForList(queryString, paramMap, Integer.class);
        }
        if(channelIdList != null && !channelIdList.isEmpty()) {
            logger.debug("ChannelIDList not null: " + lookupId);
            Integer channelId = FCUtil.getFirstElement(channelIdList);
            logger.debug("Returning channel Id " + channelId.toString());
            return channelId.toString();
        }
        return null;
    }
    
    public TxnHomePageBusinessDO getTxnHomePageByOrderId(String orderId) {
        String lookupId = this.getLookupIdForOrderId(orderId);
        TxnHomePageBusinessDO txnDAO = getHomePageByLookupId(lookupId);
		if (txnDAO != null) {
			txnDAO.setLookupId(lookupId);
			txnDAO.setLookupID(lookupId);
		}
        return txnDAO;
    }

    public TxnHomePageBusinessDO getHomePageByLookupId(String lookupId) {
        String queryString = "select " +
        		"id, service_number, operator_name, circle_name, " +
        		"amount, product_type, session_id, lookup_id, " +
        		"creatred_on, fk_product_master_id, fk_circle_master_id, " +
        		"fk_operator_master_id, fk_bill_txn_home_page_id " +
        		"from " +
        		"txn_home_page where " +
        		"lookup_id = :lookupId;";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("lookupId", lookupId);
        
        List<TxnHomePageBusinessDO> homePageList = this.jdbcTemplate.query(queryString, paramMap, new TxnHomePageMapper());
        
        if (homePageList.size() > 1) {
            throw new IllegalStateException("More than one home page found for lookupID: [" + lookupId + "]");
        } else if (homePageList.size() == 0) {
            // Fetching from master
            homePageList = this.masterJdbcTemplate.query(queryString, paramMap, new TxnHomePageMapper());
        }
        
        return homePageList.get(0);
    }

    /*This also get valid product type for Postpaid-retry case*/
    public String getProductTypeForAllRechargeCase(String orderId) {
    	try {
    		UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                    .findUserTransactionHistoryByOrderId(orderId);
        	if (userTransactionHistory == null) {
        		TxnHomePageBusinessDO txnHomePageBusinessDO = this.getTxnHomePageByOrderId(orderId);
        		if (txnHomePageBusinessDO != null) {
        			return txnHomePageBusinessDO.getProductType();
        		}
        	}
        	return userTransactionHistory.getProductType();
    	} catch (Exception ex) {
    		logger.error("Exception while getting product type : "+ orderId , ex);
    		return null;
    	}
    }    
    
    public Integer getMobileAppVersion(String orderId) {
    	List<Map<String, Object>> appVarsionOrderMapList = null;
        try {
        		logger.info("Couldn't find anything in ddb, will check in mysql");
        		String sql = "select app_version from app_version_order where order_id = :orderid";
                Map<String, Object> paramMap = new HashMap<String, Object>();
                paramMap.put("orderid", orderId);
                appVarsionOrderMapList = this.jdbcTemplate.queryForList(sql, paramMap);
                if (FCUtil.isEmpty(appVarsionOrderMapList)) {
                	logger.info("Empty app_version_order records for orderid "+ orderId);
                	return null;
                }
            
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting app version : " + orderId,
                    e);
            return null;
        }

        return (Integer) appVarsionOrderMapList.get(0).get("app_version");
    }
    
}