package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.AirtelRecharge;
import com.freecharge.common.framework.logging.LoggingFactory;


/**
 * A data access object (DAO) providing persistence and search support for
 * AffiliateProfile entities. Transaction control of the save(), update() and
 * delete() operations must be handled externally by senders of these methods or
 * must be manually added to each of these methods for data to be persisted to
 * the JPA datastore.
 *

 * @author Manoj
 */

public class AirtelRechargeDAO extends CommonDao<AirtelRecharge>  {
	// property constants
	public static final String AIRTEL_RECHARGE_ID = "airtelRechargeId";
	public static final String AIRTEL_RECHARGE_TITLE = "airtelRechargeTitle";
	public static final String AIRTEL_RECHARGE_DESC = "airtelRechargeDesc";
	public static final String AIRTEL_RECHARGE_AMOUNT = "airtelRechargeAmount";
	public static final String ADDED_ON = "addedOn";


    private static Logger logger = LoggingFactory.getLogger(AirtelRechargeDAO.class);


    protected Class<AirtelRecharge> getDomainClass() {
           return AirtelRecharge.class;
       }



	/**
	 * Find all AffiliateProfile entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the AffiliateProfile property to query
	 * @param value
	 *            the property value to match
	 * @return List<AffiliateProfile> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AirtelRecharge> findByProperty(String propertyName,
			final Object value) {
		logger.debug("Request comes into the findByProperty method of the class AirtelRechargeDAO, the request input parameters is String "+propertyName+" Object "+value);
		logger.info("finding AffiliateProfile instance with property: "
						+ propertyName + ", value: " + value,null);
		try {
			final String queryString = "select model from AffiliateProfile model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<AirtelRecharge> airtelRecharges = query.list();
			logger.debug("Request goes out from the findByProperty method of the class AirtelRechargeDAO, the request return type is List<AirtelRecharge> "+airtelRecharges);
			return airtelRecharges;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

	public List<AirtelRecharge> findByAirtelRechargeId(Object airtelRechargeId) {
		return findByProperty(AIRTEL_RECHARGE_ID, airtelRechargeId);
	}

	public List<AirtelRecharge> findByAirtelRechargeTitle(Object airtelRechargeTitle) {
		return findByProperty(AIRTEL_RECHARGE_TITLE, airtelRechargeTitle);
	}

	public List<AirtelRecharge> findByAirtelRechargeDesc(Object airtelRechargeDesc) {
		return findByProperty(AIRTEL_RECHARGE_DESC, airtelRechargeDesc);
	}

	public List<AirtelRecharge> findByairtelRechargeAmount(Object airtelRechargeAmount) {
		return findByProperty(AIRTEL_RECHARGE_AMOUNT, airtelRechargeAmount);
	}

	public List<AirtelRecharge> findByAddedOn(Object addedOn) {
		return findByProperty(ADDED_ON, addedOn);
	}



	/**
	 * Find all AffiliateProfile entities.
	 *
	 * @return List<AffiliateProfile> all AffiliateProfile entities
	 */
	@SuppressWarnings("unchecked")
	public List<AirtelRecharge> findAll() {
		logger.debug("Request comes into the findAll method of the class AirtelRechargeDAO ");
		logger.info("finding all AffiliateProfile instances",null);
		try {
			final String queryString = "select model from AirtelRecharge model";
			Query query = getSession().createQuery(queryString);
			List<AirtelRecharge> airtelRecharges = query.list();
			logger.debug("Request goes out from the findAll method of the class AirtelRechargeDAO, the request return type is List<AirtelRecharge> "+airtelRecharges);
			return airtelRecharges;
		} catch (RuntimeException re) {
			logger.error("find all failed",re);
			throw re;
		}
	}

}