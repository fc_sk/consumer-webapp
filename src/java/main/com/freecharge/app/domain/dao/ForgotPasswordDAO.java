package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.businessdo.ForgotPasswordBusinessDO;
import com.freecharge.common.businessdo.PasswordRecoveryBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/3/12
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordDAO extends BaseDao implements IForgotPasswordDAO {
	private Logger logger = LoggingFactory.getLogger(getClass());
    public List<UserProfile> getPassword(BaseBusinessDO businessDO) throws Exception {
    	logger.debug("Request comes into the getPassword method of the class ForgotPasswordDAO, the request input parameters is BaseBusinessDO businessDO:"+businessDO);
    	List<UserProfile> usersList = null;
    	try{
    		ForgotPasswordBusinessDO forgotPasswordBusinessDO = (ForgotPasswordBusinessDO) businessDO;
	    	String email = forgotPasswordBusinessDO.getEmail();
	    	Session session = getSession();
	        Criteria criteria = session.createCriteria(UserProfile.class);
	        criteria.add(Restrictions.eq("isDefault", true));
	        criteria.createCriteria("users").add(Restrictions.eq("email",email));
	        criteria.setFetchMode("users", org.hibernate.FetchMode.JOIN);
	        
	        usersList = criteria.list();
    	}catch(Exception e){
    		logger.error("Error occured while getting user and user profile for forgot password "+e);
    	}
    	logger.debug("Request goes out from the getPassword method of the class ForgotPasswordDAO, the request return type is List<UserProfile> :"+usersList);
        return usersList;
    }

    public Boolean changePassword(BaseBusinessDO baseBusinessDO) {
    	logger.debug("Request comes into the changePassword method of the class ForgotPasswordDAO, the request input parameters is BaseBusinessDO businessDO:"+baseBusinessDO);
       String hqlQuery = "update Users set password=:password,forgotPasswordKey=NULL,forgotPasswordExpiry=NULL where email=:email and userId=:userId and forgotPasswordKey=:forgotPasswordKey";
       org.hibernate.Session session=getSession();
       Integer value=session.createQuery(hqlQuery).setParameter("password",((PasswordRecoveryBusinessDO)baseBusinessDO).getNewPassword()).
    		   setParameter("email",((PasswordRecoveryBusinessDO)baseBusinessDO).getEmail()).
    		   setParameter("userId",Integer.parseInt(((PasswordRecoveryBusinessDO)baseBusinessDO).getUserId()))
    		   .setParameter("forgotPasswordKey",((PasswordRecoveryBusinessDO)baseBusinessDO).getEncryptValue())
    		   .executeUpdate();
       logger.debug("Request goes out from the changePassword method of the class ForgotPasswordDAO, the request return type is boolean :"+value);
        if(value == 1){
            return true;
        }else{
            return false;
        }

    }
    public void updateUser(Users user){
    	logger.debug("Request comes into the updateUser method of the class ForgotPasswordDAO, the request input parameters is Users user:"+user);
    	getSession().update(user);
    	logger.debug("Request goes out from the updateUser method of the class ForgotPasswordDAO, the request return type is void :");
    }

	@Override
	public List<Users> getUser(Integer userid, String encValue) {
		logger.debug("Request comes into the getUser method of the class ForgotPasswordDAO, the request input parameters are Integer userid : "+userid+" and String encValue : "+encValue);
		String hqlQuery=" from Users  where  userId=:userid and forgotPasswordKey=:forgotPasswordKey ";
        org.hibernate.Session session=getSession();
        List<Users> list=    session.createQuery(hqlQuery).setParameter("userid",userid).setParameter("forgotPasswordKey", encValue).list();
        logger.debug("Request goes out from the getUser method of the class ForgotPasswordDAO, the request return type is List<Users> :"+list);
        return list;
	}

}
