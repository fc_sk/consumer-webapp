package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;

/**
 * Interface for CartItemsDAO.
 * @author MyEclipse Persistence Tools
 */

public interface ICartItemsDAO {
	
	public List<CartItems> findByProperty(String propertyName, Object value
		);
	/**
	 * Find all CartItems entities.
	  	  @return List<CartItems> all CartItems entities
	 */
	public List<CartItems> findByFkCart(Cart fkCartId);

    public Integer save(CartItems cartItems);

    public boolean isFreeFundPresent(CartItems cartItems);
}