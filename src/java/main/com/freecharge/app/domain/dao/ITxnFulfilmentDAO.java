package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Interface for TxnFulfilmentDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ITxnFulfilmentDAO {

	/**
	 * Find all TxnFulfilment entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TxnFulfilment property to query
	 * @param value
	 *            the property value to match
	 * @return List<TxnFulfilment> found by query
	 */
	public List<TxnFulfilment> findByProperty(String propertyName, Object value);

	public List<TxnFulfilment> findByTxnHomePageId(Object txnHomePageId);

	public List<TxnFulfilment> findByEmail(Object email);

	public List<TxnFulfilment> findByAddressMobile(Object addressMobile);

	public List<TxnFulfilment> findByIsGuest(Object isGuest);

	public List<TxnFulfilment> findByBillingAdd(Object billingAdd);

	public List<TxnFulfilment> findByBillingCity(Object billingCity);

	public List<TxnFulfilment> findByBillingCountry(Object billingCountry);

	public List<TxnFulfilment> findByBillingPincode(Object billingPincode);

	public List<TxnFulfilment> findByDeliveryAdd(Object deliveryAdd);

	public List<TxnFulfilment> findByDeliveryCity(Object deliveryCity);

	public List<TxnFulfilment> findByDeliveryCountry(Object deliveryCountry);

	public List<TxnFulfilment> findByDeliveryPincode(Object deliveryPincode);

	public List<TxnFulfilment> findByDeliveryName(Object deliveryName);

	public List<TxnFulfilment> findByTransactionStatus(Object transactionStatus);

	public List<TxnFulfilment> findByOrderId(Object orderId);

	/**
	 * Find all TxnFulfilment entities.
	 * 
	 * @return List<TxnFulfilment> all TxnFulfilment entities
	 */
	public List<TxnFulfilment> findAll();

	public Integer saveFirstName(BaseBusinessDO baseBusinessDO);

	public Integer save(TxnFulfilment txnFulFilment);

	public Integer updateAddress(BaseBusinessDO baseBusinessDO);

	public TxnFulfilment updateFulfilmentDB(TxnFulfilment txnFulFilment);

	public void updateOrderId(Integer fullFilmentId, String orderId);

}