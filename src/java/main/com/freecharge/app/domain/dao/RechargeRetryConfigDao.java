package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.RechargeRetryConfig;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class RechargeRetryConfigDao extends BaseDao implements IRechargeRetryConfigDao{

	private static Logger logger = LoggingFactory.getLogger(RechargeRetryConfigDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("jdbcReadDataSource")
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public List<RechargeRetryConfig> getAllRechargeRetryConfig() {
		logger.info("Getting all recharge_retry_config");
		List<RechargeRetryConfig> rechargeRetryConfigs = null;
		try {
			String queryString = "select * from recharge_retry_config";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			rechargeRetryConfigs = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(RechargeRetryConfig.class));
		} catch (RuntimeException re) {
			logger.error("Error reading recharge_retry_config.", re);
		}
		logger.info("Finished getting all recharge_retry_config");
		return rechargeRetryConfigs;
	}

}
