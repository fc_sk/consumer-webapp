package com.freecharge.app.domain.dao.jdbc;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.FailedLogin;
import com.freecharge.app.domain.entity.jdbc.mappers.FailedLoginMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;

@Component("failedLoginReadDAO")
public class FailedLoginReadDAO {
    private static final Logger        LOGGER     = LoggingFactory.getLogger(FailedLoginWriteDAO.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private static final String        START_TIME = "start";
    private static final String        END_TIME   = "end";

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public int getAllFailedLoginsCount() {
        LOGGER.info("Fetching total failed logins count");
        String sql = "select count(*) from failed_logins";
        SqlParameterSource paramMap = null;
        return this.namedParameterJdbcTemplate.queryForInt(sql, paramMap);
    }

    public List<FailedLogin> getFailedLoginsFromIp(final String ip, final Timestamp start, final Timestamp end) {
        LOGGER.info(String.format("Fetching failed logins for ip: %s between %s and %s", ip, start, end));
        String sqlFormat = "select id, username, ip_address, attempted attempted from failed_logins where "
                + "ip_address=:%s and attempted between :%s and :%s";
        String sql = String.format(sqlFormat, FailedLogin.FAILED_LOGIN_IP_ADDRESS, START_TIME, END_TIME);
        Map<String, ? extends Object> params = FCUtil.createMap(FailedLogin.FAILED_LOGIN_IP_ADDRESS, ip, START_TIME,
                start, END_TIME, end);
        return this.namedParameterJdbcTemplate.query(sql, params, FailedLoginMapper.FAILED_LOGIN_MAPPER);
    }

    public List<FailedLogin> getAllFailedLoginsForUserEmail(final String userEmail, final Timestamp start,
            final Timestamp end) {
        LOGGER.info("Fetching failed logins for email: " + userEmail);
        String sqlFormat = "select id, username, ip_address, attempted from failed_logins where username=:%s and "
                + "attempted between :%s and :%s";
        String sql = String.format(sqlFormat, FailedLogin.FAILED_LOGIN_USER_EMAIL, START_TIME, END_TIME);
        Map<String, ? extends Object> params = FCUtil.createMap(FailedLogin.FAILED_LOGIN_USER_EMAIL, userEmail,
                START_TIME, start, END_TIME, end);
        return this.namedParameterJdbcTemplate.query(sql, params, FailedLoginMapper.FAILED_LOGIN_MAPPER);
    }

    public List<FailedLogin> getAllFailedLoginsForIpAndEmail(final String ip, final String userEmail,
            final Timestamp start, final Timestamp end) {
        LOGGER.info(String.format("Fetching failed logins for email: %s and ip: %s", userEmail, ip));
        String sqlFormat = "select id, username, ip_address, attempted from failed_logins where username=:%s and "
                + "ip_address=:%s and attempted between :%s and :%s";
        String sql = String.format(sqlFormat, FailedLogin.FAILED_LOGIN_USER_EMAIL,
                FailedLogin.FAILED_LOGIN_IP_ADDRESS, START_TIME, END_TIME);
        Map<String, ? extends Object> params = FCUtil.createMap(FailedLogin.FAILED_LOGIN_USER_EMAIL, userEmail,
                FailedLogin.FAILED_LOGIN_IP_ADDRESS, ip, START_TIME, start, END_TIME, end);
        return this.namedParameterJdbcTemplate.query(sql, params, FailedLoginMapper.FAILED_LOGIN_MAPPER);
    }
}
