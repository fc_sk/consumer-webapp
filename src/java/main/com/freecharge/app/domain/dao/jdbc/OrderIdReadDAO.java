package com.freecharge.app.domain.dao.jdbc;

import com.freecharge.admin.CustomerTrailDetails.OrderIdOidMapper;
import com.freecharge.admin.CustomerTrailDetails.OrderUserIdMapper;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.mappers.CartItemsRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdLidMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdListMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdRowMapper;
import com.freecharge.app.service.CommonService;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.types.MTNLLandLineBill;
import com.freecharge.infrastructure.billpay.types.RelianceEnergyMumbai;
import com.freecharge.infrastructure.billpay.types.Bill.ServiceProvider;
import com.freecharge.recharge.exception.InvalidOrderIDException;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * A data access object (DAO) providing persistence and search support for OrderId entities. Transaction control of the save(), update() and delete() operations must be handled externally by senders of these methods or must be manually added to each of these methods for data to be persisted to the JPA datastore.
 * 
 * @see
 * @author MyEclipse Persistence Tools
 */
@Component
public class OrderIdReadDAO {
	private static Logger logger = LoggingFactory.getLogger(OrderIdReadDAO.class);
	
	public static final String LOOKUP_ID = "lookupId";
	public static final String SESSION_ID = "sessionId";
	public static final String ORDER_ID = "orderId";
	public static final String FK_PRODUCT_MASTER_ID = "fkProductMasterId";
	public static final String FK_AFFLIATE_PROFILE_ID = "fkAffliateProfileId";
	public static final String ORDER_ID_ORIGIONAL = "orderIdOrigional";

	@Autowired
    private CommonService commonService;

	private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private FreefundReadDAO freefundReadDao;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;


    @Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

		
	public List<OrderId> getOrderListByDate(Date date) {
		logger.debug("Request comes into the getOrderIDDetails method of the class OrderIdDAO, the request  input parameters  String "+date);
		// List<OrderidGenerator> orderidGenerator=new ArrayList<OrderidGenerator>();
		String queryString = "select * from order_id where created_on >= :date";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("date", date);
		
		List<OrderId> list = this.jdbcTemplate.query(queryString, paramMap, new OrderIdRowMapper());
		
		logger.debug("Request goes out from the getOrderIDDetails method of the class OrderIdDAO, the request  return type is List<OrderId> "+list);
		return list;
	}

	public String getOrderIdByAllParams(String sessionId, String lookupId, int affiliateId, int producMasterId, int channelId) {
		logger.debug("Request comes into the getOrderIDDetails method of the class OrderIdDAO, the request  input parameters  String "+ sessionId+" String "+ lookupId+" int "+ affiliateId+" int "+producMasterId+" int "+ channelId);
		
		String queryString = "select order_id from  order_id where session_id = :sessionId and " + 
				"lookup_id = :lookupId and fk_affiliate_profile_id = :affiliateId " + 
				"and fk_product_master_id = :productMasterId and channel_id = :channelId ";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("sessionId", sessionId);
		paramMap.put("lookupId", lookupId);
		paramMap.put("affiliateId", affiliateId);
		paramMap.put("productMasterId", producMasterId);
		paramMap.put("channelId", channelId);

		List<Map<String, Object>> list = this.jdbcTemplate.queryForList(queryString, paramMap);
		
		String orderId = null;
		
		if (list != null && list.size() > 0) {
			orderId = (String)list.get(0).get("order_id");
		}

		logger.debug("Request goes out from the getOrderIDDetails method of the class OrderIdDAO, the request  return type is String "+orderId);
		return orderId;
	}

    public List<OrderId> getAllMatchingOrderIds(String orderId) {
        String queryString = "select * from order_id where order_id like :orderId order by created_on desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        String likeOrderIdParam = orderId + "%";
        paramMap.put("orderId", likeOrderIdParam);

        List<OrderId> orderIds = this.jdbcTemplate.query(queryString, paramMap, new OrderIdRowMapper());

        return orderIds;
    }
    
    public List<OrderId> getByOrderId(String orderId) {
        String queryString = "select * from order_id where order_id = :orderId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);

        List<OrderId> orderIds = this.jdbcTemplate.query(queryString, paramMap, new OrderIdRowMapper());

        return orderIds;
    }
    
    public String getLookupIdForOrderId(String orderId) {
		logger.debug("Request comes into the getCartByOrderId method of the class OrderIdReadDAO, the request input parameters is orderId :" + orderId);
		
		String queryString = "select lookup_id from order_id o where o.order_id = :orderId";
		
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        
        List<String> lidList = this.jdbcTemplate.query(queryString, paramMap, new OrderIdLidMapper());
        
        if(lidList == null || lidList.isEmpty()) {
        	throw new InvalidOrderIDException("Received empty set of lookup ids for order ID : " + orderId);
        }
		return lidList.get(0);
    }
	
	public String getOrderIdForLookUpId(String lookup_id) {
		String sql = "select o.order_id from order_id o where o.lookup_id = :lookup_id";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("lookup_id", lookup_id);
		List<String> orderids = this.jdbcTemplate.query(sql, param, new OrderIdOidMapper());
		
		String orderId = null;
		if (orderids != null && orderids.size() >0 ) {
			orderId = orderids.get(0); 
		}
		return orderId;
	}

	public List<String> getOrderIdListForLookUpId(String lookup_id) {
		String sql = "select o.order_id from order_id o where o.lookup_id = :lookup_id";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("lookup_id", lookup_id);
		List<String> orderids = this.jdbcTemplate.query(sql, param, new OrderIdOidMapper());
		return orderids;
	}
	
	public String getOrderIDByLookupID(String LOOKUP_ID) {
		String queryString = "select order_id from order_id where lookup_id = :lookupid";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("lookupid", LOOKUP_ID);
		List<String> result = this.jdbcTemplate.queryForList(queryString, paramMap,
				String.class);
		return (result == null || result.size() == 0)? null: result.get(0);		
	}			

	public static final String RECHARGE_DETAILS_SQL = "" +
			"SELECT pm.product_name, thp.fk_operator_master_id, thp.operator_name, thp.circle_name, " +
			" thp.amount, thp.service_number, thp.creatred_on, thp.fk_circle_master_id" +
			" FROM product_master pm, txn_home_page thp, order_id oid" +
			" WHERE thp.fk_product_master_id = pm.product_master_id" +
			"   AND oid.lookup_id = thp.lookup_id" +
			"   AND oid.order_id = :order_id";

	public RechargeDetails getRechargeDetailsByOrderID(String orderID) {
		final Map<Integer, OperatorMaster> operatorMasterMap = commonService.getOperatorMasters();
		Map<String, Object> params = Collections.singletonMap("order_id", (Object) orderID);
		List <RechargeDetails> rechargeList = jdbcTemplate.query(RECHARGE_DETAILS_SQL, params, new RowMapper<RechargeDetails>() {
			@Override
			public RechargeDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
				RechargeDetails rd = new RechargeDetails();
				rd.setProductName(rs.getString("product_name"));
				rd.setOperatorName(rs.getString("operator_name"));
				OperatorMaster opm = operatorMasterMap.get(rs.getInt("fk_operator_master_id"));
				rd.setOperatorImageURL(opm!=null? opm.getImgUrl(): "");
				rd.setCircleName(rs.getString("circle_name"));
                rd.setCircleId(rs.getString("fk_circle_master_id"));
				rd.setAmount(rs.getString("amount"));
				rd.setServiceNumber(rs.getString("service_number"));
				rd.setRechargedOn(rs.getDate("creatred_on"));
                rd.setOperatorMasterId(rs.getString("fk_operator_master_id"));
				return rd;
			}
		});
		RechargeDetails rechargeDetails = null;
		if (rechargeList != null && rechargeList.size() > 0){
			rechargeDetails = rechargeList.get(0);
		}
		return rechargeDetails;
	}
	
	public Integer getUserIdFromOrderId(String orderId){
		String sql = "select c.fk_user_id from order_id oid, cart c where c.lookup_id = oid.lookup_id and order_id = :orderId";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("orderId", orderId);
		List<String> userIdList = this.jdbcTemplate.query(sql, param, new OrderUserIdMapper());
		if (userIdList == null || userIdList.size() == 0){
			return null;
		}
		Integer userIdInt = Integer.parseInt(userIdList.get(0));
		return userIdInt;
	}
	
	public List<String> getOrderIdsFromUserId(Integer userId){
		List<String> orderIdList = null;
		try {
			String sql = "select oid.order_id as order_id from order_id oid, cart c where c.lookup_id = oid.lookup_id and c.fk_user_id = :userId order by oid.created_on desc";
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("userId", userId);
			orderIdList = this.jdbcTemplate.query(sql, param, new OrderIdOidMapper());
			if (orderIdList == null || orderIdList.size() == 0){
				return null;
			}
		} catch (DataAccessException e) {
			logger.error("Error retrieving order ids for user id: " + userId);
		}
		return orderIdList;
	}

    public boolean hasCoupons(String orderId) {
        try {
            String sql = "select ci.* from cart_items ci, cart c, order_id oid where " +
                    "oid.order_id = :orderId and " +
                    "oid.lookup_id = c.lookup_id and " +
                    "c.cart_id = ci.fk_cart_id and " +
                    "ci.is_deleted = 0 and " +
                    " (ci.fl_product_master_id = 4 or ci.fl_product_master_id = 15 ) and " +
                    "ci.quantity > 0;";
            
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("orderId", orderId);
            List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(sql, param);
            
            if (resultList != null && !resultList.isEmpty()) {
                return true;
            } else {
                return false;
            }
            
        } catch (DataAccessException e) {
            logger.error("Error while checking whether an order ID has coupons for orderID:[ " + orderId + "]");
            throw new RuntimeException(e);
        }
    }
    
    public String getRechargeType(String lookupId) {
        String query = "select ci.entity_id as recharge_type from cart_items ci, cart c where " +
        		"c.lookup_id = :lookupId and " +
        		"c.cart_id = ci.fk_cart_id and " +
        		"ci.fl_product_master_id in (1, 2, 3);";

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("lookupId", lookupId);
        List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(query, param);
        
        if (resultList == null || resultList.isEmpty()) {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        }
        
        String rechargeType = (String) resultList.get(0).get("recharge_type");
        
        if (StringUtils.isBlank(rechargeType)) {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        } else if (RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL.equals(rechargeType)) {
            return RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL;
        } else {
            return RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE;
        }
    }
    
    /**
     * This has been deliberately placed here since for some reason
     * the query in CartDAO times out.
     * @param orderId
     * @return
     */
    public List<CartItemsBusinessDO> getCartItemsByOrderId(String orderId) {
        try {
            String sql = "select ci.* from " +
            		"cart_items ci, order_id oi, cart c where " +
            		"oi.order_id = :orderId and oi.lookup_id = c.lookup_id and c.cart_id = ci.fk_cart_id;";
            
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("orderId", orderId);
            List<CartItemsBusinessDO> cartItemsList = this.jdbcTemplate.query(sql, param, new CartItemsRowMapper());
            
            return cartItemsList;
        } catch (DataAccessException e) {
            logger.error("Error while fetching cart items for OrderID:[ " + orderId + "]");
            throw new RuntimeException(e);
        }
    }
    
    public List<CartItemsBusinessDO> getCartItemsByLookupId(String lookupId) {
        try {
            String sql = "select ci.* from " +
            		"cart_items ci, cart c where " +
            		"c.cart_id = ci.fk_cart_id and " +
            		"c.lookup_id = :lookupId;";
            
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("lookupId", lookupId);
            List<CartItemsBusinessDO> cartItemsList = this.jdbcTemplate.query(sql, param, new CartItemsRowMapper());
            
            return cartItemsList;
        } catch (DataAccessException e) {
            logger.error("Error while fetching cart items for lookupID:[ " + lookupId + "]");
            throw new RuntimeException(e);
        }
    }

    public List<String> getOrderIdListForCoupon(String couponCode) {
        logger.debug("Request comes into the getOrderIdListForCoupon method of the class FreefundReadDAO, the request input "
                + "parameters is couponCode :" + couponCode);

        if (freefundReadDao.isFreefundCodeRedeemedOrBlocked(couponCode)) {
            String queryString = "select distinct(o.order_id) from " +
                    "order_id o, cart c, cart_items ci where  " +
                    "ci.entity_id = 'FreeFund' and " +
                    "c.lookup_id = o.lookup_id and  " +
                    "c.cart_id = ci.fk_cart_id and " +
                    "ci.fk_item_id =:freefundCouponId and ci.fl_product_master_id = 8";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("freefundCouponId", freefundReadDao.getPromocodeIdByCode(couponCode));

            return this.jdbcTemplate.query(queryString, paramMap, new OrderIdListMapper());
        } else {
            return null;
        }
    }

    /**
     * Get list of all orders against unique couponCode (promo_entity: "cashback-promocode", "all-payment-options")
     * @param couponCode
     * @return
     */
    public List<String> getOrderIdListWithUniqueCodesUsed(final String couponCode) {
        logger.debug("Request comes into the getOrderIdListForCouponRestting method of the class FreefundReadDAO, " +
                "the request input parameters is couponCode :" + couponCode);

        if (freefundReadDao.isFreefundCodeRedeemedOrBlocked(couponCode) &&
                freefundReadDao.isFreefundCodesCampaignOfUniqueType(couponCode)) {
            String queryString = "select distinct(o.order_id) from "
                    + "order_id o, cart c, cart_items ci where  "
                    + "ci.entity_id = 'FreeFund' and "
                    + "c.lookup_id = o.lookup_id and  "
                    + "c.cart_id = ci.fk_cart_id and "
                    + "ci.fk_item_id = :freefundCouponId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("freefundCouponId", freefundReadDao.getPromocodeIdByCode(couponCode));

            return this.jdbcTemplate.query(queryString, paramMap, new OrderIdListMapper());
        } else {
            return null;
        }
    }


    /* Getting list of all orderids attempted for a promocode */
    public List<String> getAllOrderIdListAttemptedForCouponCode(String couponCode) {
        logger.debug("Request comes into the getAllOrderIdListForCouponCode method of the class FreefundReadDAO, the request input "
                + "parameters is couponCode :" + couponCode);

        if (freefundReadDao.isFreefundCodeRedeemedOrBlocked(couponCode) &&
                freefundReadDao.isFreefundCodesCampaignOfUniqueType(couponCode)) {
            String queryString = "select distinct(o.order_id) from "
                    + "order_id o, cart c, cart_items ci where  " + "ci.entity_id = 'FreeFund' and "
                    + "c.lookup_id = o.lookup_id and  " + "c.cart_id = ci.fk_cart_id and "
                    + "ci.fk_item_id = :freefundCouponId order by o.created_on DESC limit 30";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("freefundCouponId", freefundReadDao.getPromocodeIdByCode(couponCode));

            return this.jdbcTemplate.query(queryString, paramMap, new OrderIdListMapper());
        } else {
            return null;
        }
    }
    
    public RechargeDetails getRechargeDetailsFromHomePageByOrderID(String orderID) {
        final Map<Integer, OperatorMaster> operatorMasterMap = commonService.getOperatorMasters();
        
        Map<String, Object> params = Collections.singletonMap("order_id", (Object) orderID);
        List <RechargeDetails> rechargeList = jdbcTemplate.query(RECHARGE_DETAILS_SQL, params, new RowMapper<RechargeDetails>() {
            @Override
            public RechargeDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
                RechargeDetails rd = new RechargeDetails();
                int productMasterId = rs.getInt("fk_product_master_id");
                rd.setProductName(FCConstants.productTypeToNameMap.get(FCConstants.productMasterMap.get(productMasterId)));
                rd.setOperatorName(rs.getString("operator_name"));
                OperatorMaster opm = operatorMasterMap.get(rs.getInt("fk_operator_master_id"));
                rd.setOperatorImageURL(opm!=null? opm.getImgUrl(): "");
                rd.setCircleName(rs.getString("circle_name"));
                rd.setCircleId(rs.getString("fk_circle_master_id"));
                rd.setAmount(rs.getString("amount"));
                rd.setServiceNumber(rs.getString("service_number"));
                rd.setRechargedOn(rs.getDate("creatred_on"));
                rd.setOperatorMasterId(rs.getString("fk_operator_master_id"));
                return rd;
            }
        });
        RechargeDetails rechargeDetails = null;
        if (rechargeList != null && rechargeList.size() > 0){
            rechargeDetails = rechargeList.get(0);
        }
        String productType = null;
        if (rechargeDetails != null && rechargeDetails.getProductName() != null) {
            productType = FCConstants.productNameToTypeMap.get(rechargeDetails.getProductName());
        }
        if (productType != null && FCUtil.isUtilityPaymentType(productType)) {
        	BillerDetails billPayOperatorMaster = commonService.getBillPayOperatorMasterByOperatorId(Integer.parseInt(rechargeDetails.getOperatorMasterId()));
            if (billPayOperatorMaster != null) {
                rechargeDetails.setOperatorImageURL(billPayOperatorMaster.getImgUrl());
            }
            BillWithSubscriberNumber bill = null;
            try {
                bill = billPayServiceRemote.getBillDetails(rechargeDetails.getServiceNumber());
            } catch (Exception e) {
                logger.error("Error while fetching bill details for orderId " + orderID + ", billTransactionId :" + rechargeDetails.getServiceNumber() , e);
                return null;
            }
            ServiceProvider serviceProvider = bill.getServiceProvider();
            if (serviceProvider != null ) {
                if (serviceProvider.equals(ServiceProvider.RelianceEnergyMumbai)) {
                    RelianceEnergyMumbai rel = (RelianceEnergyMumbai) bill;
                    rechargeDetails.setBillingCycle(rel.getBillingCycle());
                } else if (serviceProvider.equals(ServiceProvider.MTNLLandLine)) {
                    MTNLLandLineBill mtnl = (MTNLLandLineBill) bill;
                    rechargeDetails.setAccountNumber(mtnl.getAdditionalInfo2());
                }
            }
            rechargeDetails.setServiceNumber(bill.getAdditionalInfo1());
            rechargeDetails.setCircleName(bill.getCircleName());
            rechargeDetails.setCircleId(bill.getCircleId() == null ? "" : bill.getCircleId().toString());
        }
        if(productType!=null && FCConstants.PRODUCT_TYPE_HCOUPON.equals(productType)) {
            rechargeDetails.setServiceNumber("Deals");
        }
        return rechargeDetails;
    }
    
}

