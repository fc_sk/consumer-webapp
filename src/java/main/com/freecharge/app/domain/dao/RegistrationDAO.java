package com.freecharge.app.domain.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserDetails;
import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;

/**
 * Created with IntelliJ IDEA.
 * User: abc
 * Date: 5/3/12
 * Time: 2:17 PM
 * To change this template use File | Settings | File Templates.
 */

public class RegistrationDAO extends BaseDao implements IRegistrationDAO{
	@Autowired
    EPinEncrypt ePinEncrypt;
    private static Logger logger = LoggingFactory.getLogger(RegistrationDAO.class);


    @Autowired
    private UserServiceProxy userServiceProxy;
    
    public List<UserContactDetail> getUserContactDetails(BaseBusinessDO baseBusinessDO) {
    	logger.debug("Request comes into the getUserInfo method of the class RegistrationDAO, the request input parameter types are BaseBusinessDO "+baseBusinessDO);
        List<UserContactDetail> infoList = new ArrayList();
        try{
        Query query= getSession().createQuery("select users.userId,users.email,userprofile.firstName," +
                "userprofile.address1,userrechargecontact.serviceNumber,userprofile.userProfileId," +
                "userrechargecontact.userRechargeContactId,citymaster.cityName,userprofile.postalCode," +
                "statemaster.stateName,countrymaster.countryName,statemaster.stateMasterId," +
                "countrymaster.countryMasterId,users.mobileNo,userprofile.street,userprofile.area,userprofile.landmark,userprofile.title,userprofile.fkCityMasterId,userprofile.lastUpdate from Users users, UserProfile userprofile," +
                " UserRechargeContact userrechargecontact,StateMaster statemaster,CountryMaster countrymaster, CityMaster citymaster " +
                "where users.email=? and userrechargecontact.serviceNumber=?" +
                " and users.userId = userrechargecontact.fkUsersId and " +
                "users.userId =userprofile.fkUserId and " +
                "userprofile.fkUserRechargeContactId=userrechargecontact.userRechargeContactId " +
                "and userprofile.fkStateMasterId=statemaster.stateMasterId and userprofile.fkCityMasterId=citymaster.cityMasterId and countrymaster.countryMasterId=userprofile.fkCountryMasterId order by userprofile.lastUpdate desc");
        
        query.setParameter(0, ((RegisterBusinessDO)baseBusinessDO).getEmail());
        query.setParameter(1, ((RegisterBusinessDO)baseBusinessDO).getServiceNumber());
        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();

        for (List<Object> list : ObjectList) {
            UserContactDetail ucd = new UserContactDetail();

            ucd.setAddress1(list.get(3)+"");
            ucd.setCityName(list.get(7) + "");
            ucd.setCountryId((Integer)list.get(12));
            ucd.setCountryName(list.get(10)+"");
            ucd.setEmail(list.get(1)+"");
            ucd.setFirstName(list.get(2)+"");
            ucd.setMobile(list.get(13)+"");
            ucd.setPostalCode(list.get(8)+"");
            ucd.setServiceNumber(list.get(4)+"");
            ucd.setStateId((Integer)list.get(11));
            ucd.setStateName(list.get(9)+"");
            ucd.setUserId((Integer)list.get(0));
            ucd.setUserProfileId((Integer)list.get(5));
            ucd.setUserRechargeContactId((Integer)list.get(6));
            ucd.setStreet(list.get(14)+"");
            ucd.setArea(list.get(15)+"");
            ucd.setLandmark(list.get(16)+"");
            ucd.setTitle(list.get(17)+"");
            ucd.setCityId((Integer)list.get(18));

            infoList.add(ucd);
        }
        }catch(Exception e){
            logger.error("");
        }
        logger.debug("Request goes out from the getUserInfo method of the class RegistrationDAO, the request  return type is List<UserContactDetail> "+infoList);
        return infoList;
    }
    
    public List<Users> getUserInfoByEmailid(BaseBusinessDO baseBusinessDO) {
    	logger.debug("Request comes into the getUserInfoByEmailid method of the class RegistrationDAO, the request input parameter types are BaseBusinessDO "+baseBusinessDO);
        List<Users> list=null;
        try {
            logger.debug("Validation of user: parameters :: email = " + ((RegisterBusinessDO)baseBusinessDO).getEmail());
            Query query = getSession().createQuery("from  Users where email=?");
            query.setParameter(0, ((RegisterBusinessDO)baseBusinessDO).getEmail());
            list = (List<Users>) query.list();

        } catch (Exception e) {
            logger.error("Error in getUser : " + e.fillInStackTrace());
            e.printStackTrace();
            return list;
        }
        logger.debug("Request goes out from the getUserInfoByEmailid method of the class RegistrationDAO, the request  return type is List<Users> "+list);
        return list;
        
    }

    public List<StateMaster> getStateMasterList() {
        List<StateMaster> list = null;
        try {
            Query query = getSession().createQuery("from  StateMaster statemaster where statemaster.isActive=" + true + " ORDER BY statemaster.stateName ASC");
            list = (List<StateMaster>) query.list();
        } catch (Exception e) {
            logger.error("Error while taking states from state master:", e);
            return list;
        }
        return list;
    }

    public List<StateMaster> getStateNameByStateId(Integer stateId) {
    	logger.debug("Request comes into the getStateNameByStateId method of the class RegistrationDAO, the request parameter type is Integer "+stateId);
        List<StateMaster> list=null;
        try {
            logger.debug("getting State from state master ");
            Query query = getSession().createQuery("from  StateMaster statemaster where statemaster.isActive="+true+" and statemaster.stateMasterId="+stateId);
            list = (List<StateMaster>) query.list();

        } catch (Exception e) {
            logger.error("Error while taking state from state id. : " + e.fillInStackTrace());
            e.printStackTrace();
            return list;
        }
        logger.debug("Request goes out from the getStateNameByStateId method of the class RegistrationDAO, the request  return type is List<StateMaster> "+list);
        return list;
    }

    public Integer saveTransactionDetails(BaseBusinessDO baseBusinessDO) {
    	logger.debug("Request comes into the saveTransactionDetails method of the class RegistrationDAO, the request parameter type is BaseBusinessDO "+baseBusinessDO);
        logger.debug("saving transaction details of user at payment time.");
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO)baseBusinessDO;
        TxnFulfilment txnFulfilment=new TxnFulfilment();
        txnFulfilment.setTxnHomePageId(registerBusinessDO.getTxnHomePageId());
        txnFulfilment.setEmail(registerBusinessDO.getEmail());
        txnFulfilment.setAddressMobile(registerBusinessDO.getMobileNo());
        //txnFulfilment.setIsGuest(true);
        txnFulfilment.setDeliveryAdd(registerBusinessDO.getAddress1());
        txnFulfilment.setDeliveryName(registerBusinessDO.getFirstName());
        txnFulfilment.setFirstName(registerBusinessDO.getFirstName());
        //txnFulfilment.setDeliveryStreet(registerBusinessDO.getStreet());
        txnFulfilment.setDeliveryArea(registerBusinessDO.getArea());
        txnFulfilment.setDeliveryLandmark(registerBusinessDO.getLandmark());
        txnFulfilment.setDeliveryFkCityMasterId(registerBusinessDO.getCityId());
        txnFulfilment.setDeliveryFkStateMasterId(registerBusinessDO.getStateId());
        txnFulfilment.setDeliveryPincode(registerBusinessDO.getPostalCode());
        txnFulfilment.setDeliveryFkCountryMasterId(registerBusinessDO.getCountryId());
        txnFulfilment.setFkUserProfileId(registerBusinessDO.getUserProfileId());
        txnFulfilment.setCreatedAt(new Timestamp((new Date()).getTime()));
        try{
        	Integer id=(Integer)this.getSession().save(txnFulfilment);
        	logger.debug("Request goes out from the saveTransactionDetails method of the class RegistrationDAO, the request  return type is Integer "+id);
            return id;               
        }catch (Exception e){
            logger.error("Error occured while saving transaction details of user as guest."+e);
            return 0;
        }
    }

    public List<CountryMaster> getCountryMasterList() {
    	logger.debug("Request comes into the getCountryMasterList method of the class RegistrationDAO, the request  has no input parameters ");
        List<CountryMaster> list=null;
        try {
            logger.debug("getting countrys from country master ");
            Query query = getSession().createQuery("from  CountryMaster countrymaster where countrymaster.isActive="+true);
            list = (List<CountryMaster>) query.list();

        } catch (Exception e) {
            logger.error("Error while taking countrys from country master : " + e.fillInStackTrace());
            e.printStackTrace();
            return list;
        }
        logger.debug("Request goes out from the getCountryMasterList method of the class RegistrationDAO, the request  return type is List<CountryMaster> "+list);
        return list;
    }


    public List<CountryMaster> getCountryNameByCountryId(Integer countryId) {
    	logger.debug("Request comes into the getCountryNameByCountryId method of the class RegistrationDAO, the request  has no input parameters ");
        List<CountryMaster> list=null;
        try {
            logger.debug("getting Country from country master ");
            Query query = getSession().createQuery("from  CountryMaster countrymaster where countrymaster.isActive="+true+" and countrymaster.countryMasterId="+countryId);
            list = (List<CountryMaster>) query.list();

        } catch (Exception e) {
            logger.error("Error while taking country list based on country id : " + e.fillInStackTrace());
            e.printStackTrace();
            return list;
        }
        logger.debug("Request goes out from the getCountryNameByCountryId method of the class RegistrationDAO, the request  return type is List<CountryMaster> "+list);
        return list;
    }

    public List<UserContactDetail> getUserContactDetails(String email, String serviceNumber) {
    	logger.debug("Request comes into the getUserContactDetails method of the class RegistrationDAO, the request  input parameters are String "+ email+" String "+serviceNumber);
        List<UserContactDetail> infoList = new ArrayList<UserContactDetail>();
        UserDetails userDetails=userServiceProxy.getUserProfileByEmailId(email);
        if(userDetails!=null) {
	        Query query= getSession().createQuery("select userrechargecontact.userRechargeContactId from UserRechargeContact userrechargecontact " +
	                "where userrechargecontact.serviceNumber=?" +
	                " and userrechargecontact.fkUsersId=?");
	
	        query.setParameter(0, serviceNumber);
	        query.setParameter(1, userDetails.getUserId());
	        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
	
	        for (List<Object> list : ObjectList) {
	            UserContactDetail ucd = new UserContactDetail();
	
	            ucd.setAddress1("");
	            ucd.setCityName("");
	            ucd.setCountryId(null);
	            ucd.setCountryName("");
	            ucd.setEmail(email);
	            ucd.setFirstName(userDetails.getName());
	            ucd.setMobile(userDetails.getMobileNo());
	            ucd.setPostalCode("");
	            ucd.setServiceNumber(serviceNumber);
	            ucd.setStateId(null);
	            ucd.setStateName("");
	            ucd.setUserId(userDetails.getUserId());
	            ucd.setUserProfileId(null);
	            ucd.setUserRechargeContactId((Integer)list.get(0));
	            ucd.setStreet("");
	            ucd.setArea("");
	            ucd.setLandmark("");
	            ucd.setTitle("");
	            ucd.setCityId(null);
	
	            infoList.add(ucd);
	        }
        }
        logger.debug("Request goes out from the saveUser method of the class RegistrationDAO, the request  return type is List<UserContactDetail> "+infoList);

        return infoList;
    }

    //todo - fix this
    public List<UserContactDetail> getUserContactDefaultDetails(String email) {
        logger.debug("Request comes into the getUserContactDefaultDetails method of the class RegistrationDAO, the request  input parameters are String " + email);
        List<UserContactDetail> infoList = new ArrayList();
        UserDetails userProfile = userServiceProxy.getUserProfileByEmailId(email);
        if(userProfile!=null) {
	        UserContactDetail ucd = new UserContactDetail();
	        ucd.setUserId((Integer) userProfile.getUserId());
	        ucd.setEmail(email);
	        ucd.setFirstName(userProfile.getName());
	        ucd.setAddress1("");
	        ucd.setCityName("");
	        ucd.setPostalCode("");
	        ucd.setStateName("");
	        ucd.setCountryName("");
	        ucd.setStateId(null);
	        ucd.setCountryId(null);
	        ucd.setMobile(userProfile.getMobileNo());
	        ucd.setUserProfileId(null);
	        ucd.setStreet("");
	        ucd.setArea("");
	        ucd.setLandmark("");
	        ucd.setTitle("");
	        ucd.setCityId(null);
	        infoList.add(ucd);
        }

        logger.debug("Request goes out from the getUserContactDefaultDetails method of the class RegistrationDAO, the request  return type is List<UserContactDetail> " + infoList);
        return infoList;
    }
    
    @Transactional
    public Integer saveUserRechargeContact(UserRechargeContact userRechargeContact) {
    	logger.debug("Request comes into the saveUserRechargeContact method of the class RegistrationDAO, the request  input parameters are UserRechargeContact "+ userRechargeContact);
        logger.debug("Saving UserRechargeContact ..");
        Session session=null;
        try{
            session = this.getSession();
            Integer id=(Integer)session.save(userRechargeContact);
            logger.debug("Request goes out from the saveUserRechargeContact method of the class RegistrationDAO, the request  return type is Integer "+id);
            return id;
        }catch(Exception e){
            logger.error("Exception occured while saving UserRechargeContact .. ",e);
            return 0;
        }
    }

	@Override
	public List<UserRechargeContact> getUserRechargeContact(String email,String serviceNumber) {
		logger.debug("Request comes into the getUserRechargeContact method of the class RegistrationDAO, the request  input parameters are String "+email+" String "+serviceNumber);
		logger.debug("Getting user recarge contact details..");
		List<UserRechargeContact> infoList = new ArrayList();
		try {
			Long userIdLong = 0L; //userServiceProxy.getUserIdByEmailId(email);
			Integer userId = userIdLong.intValue();
			if(userId!=null) {
				final String queryString = "select urc.serviceNumber,urc.fkProductMasterId,urc.fkCircleMasterId,urc.fkOperatorMasterId,urc.userRechargeContactId " +
						"from UserRechargeContact urc where urc.serviceNumber=:serviceNumber and urc.fkUsersId=:userId";
				Query query = this.getSession().createQuery(queryString);
				query.setParameter("userId", userId);
				query.setParameter("serviceNumber", serviceNumber);
				List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
				for (List<Object> list : ObjectList) {
					UserRechargeContact userRechargeContact = new UserRechargeContact();
					userRechargeContact.setFkUsersId(userId);
					userRechargeContact.setServiceNumber(list.get(0)+"");
					userRechargeContact.setFkProductMasterId((Integer)list.get(1));
					userRechargeContact.setFkCircleMasterId((Integer)list.get(2));
					userRechargeContact.setFkOperatorMasterId((Integer)list.get(3));
					userRechargeContact.setUserRechargeContactId((Integer)list.get(4));
					infoList.add(userRechargeContact);
				}
			}
			
		} catch (RuntimeException re) {
			logger.error("Exception occured while getting userrechargecontact details.",re);
			throw re;
		}
		// TODO Auto-generated method stub
		logger.debug("Request goes out from the getUserRechargeContact method of the class RegistrationDAO, the request  return type is List<UserRechargeContact> "+infoList);
		return infoList;
	}

}
