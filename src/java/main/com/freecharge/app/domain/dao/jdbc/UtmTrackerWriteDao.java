package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.UtmTracker;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmTrackerWriteDao {
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertUtmTracker;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertUtmTracker = new SimpleJdbcInsert(dataSource).withTableName("utm_campaign_details").usingGeneratedKeyColumns("utm_campaign_id", "n_last_updated", "n_created");
    }

    public int insert(UtmTracker utmTracker) {
        SqlParameterSource parameters = utmTracker.getMapSqlParameterSource();
        Number returnParam = this.insertUtmTracker.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public void editCampaign(UtmTracker utmTracker) {
        String query = "update utm_campaign_details set fc_master=:fcMaster, utm_campaign_name=:campaignName, " +
                "utm_medium=:medium, utm_source=:source, utm_keyword=:keyword, utm_url=:url, utm_content=:content," +
                "forward_url=:forwardUrl, updated_by=:updatedBy, updated_At=now() where utm_campaign_id=:campaignId";
        Map<String, Object> params = new HashMap<>();
        params.put("campaignId", utmTracker.getCampaignId());
        params.put("fcMaster", utmTracker.getFcMaster());
        params.put("campaignName", utmTracker.getCampaignName());
        params.put("medium", utmTracker.getMedium());
        params.put("source", utmTracker.getSource());
        params.put("keyword", utmTracker.getKeyword());
        params.put("url", utmTracker.getUrl());
        params.put("content", utmTracker.getContent());
        params.put("forwardUrl", utmTracker.getForwardUrl());
        params.put("createdBy", utmTracker.getCreated_by());
        params.put("updatedBy", utmTracker.getUpdated_by());

        this.jdbcTemplate.update(query, params);
    }
}
