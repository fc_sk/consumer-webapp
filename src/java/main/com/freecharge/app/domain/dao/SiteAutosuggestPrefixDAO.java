package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.SiteAutosuggestPrefix;
import com.freecharge.common.framework.logging.LoggingFactory;


/**
 * A data access object (DAO) providing persistence and search support for
 * SiteAutosuggestPrefix entities. Transaction control of the save(), update()
 * and delete() operations must be handled externally by senders of these
 * methods or must be manually added to each of these methods for data to be
 * persisted to the JPA datastore.
 * 
 * @see com.freecharge.app.domain.dao.SiteAutosuggestPrefix
 * @author Rananjay
 */

public class SiteAutosuggestPrefixDAO  extends CommonDao<SiteAutosuggestPrefix> implements ISiteAutosuggestPrefixDAO {
	// property constants
	public static final String PREFIX = "prefix";
	public static final String FK_CIRCLE_OPERATOR_MAPPING_ID = "fkCircleOperatorMappingId";
	public static final String FK_PRODUCT_MASTER_ID = "fkProductMasterId";
	public static final String IS_ACTIVE = "isActive";

	private static Logger logger = LoggingFactory.getLogger(SiteAutosuggestPrefixDAO.class);


    protected Class<SiteAutosuggestPrefix> getDomainClass() {
        return SiteAutosuggestPrefix.class;
    }


	/**
	 * Find all SiteAutosuggestPrefix entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the SiteAutosuggestPrefix property to query
	 * @param value
	 *            the property value to match
	 * @return List<SiteAutosuggestPrefix> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<SiteAutosuggestPrefix> findByProperty(String propertyName,final Object value) {
		logger.debug("Request comes into the findByProperty method of the class SiteAutosuggestPrefixDAO, the request input parameter is "+ propertyName + ", Object: " + value, null);
		logger.info("finding SiteAutosuggestPrefix instance with property: "
						+ propertyName + ", value: " + value,null);
		try {
			final String queryString = "select model from SiteAutosuggestPrefix model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<SiteAutosuggestPrefix> list=query.list();
			logger.debug("Request goes out from the findByProperty method of the class SiteAutosuggestPrefixDAO, the request  return type is List<SiteAutosuggestPrefix> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

	public List<SiteAutosuggestPrefix> findByPrefix(Object prefix) {
		return findByProperty(PREFIX, prefix);
	}

	public List<SiteAutosuggestPrefix> findByFkCircleOperatorMappingId(
			Object fkCircleOperatorMappingId) {
		return findByProperty(FK_CIRCLE_OPERATOR_MAPPING_ID,
				fkCircleOperatorMappingId);
	}

	public List<SiteAutosuggestPrefix> findByFkProductMasterId(
			Object fkProductMasterId) {
		return findByProperty(FK_PRODUCT_MASTER_ID, fkProductMasterId);
	}

	public List<SiteAutosuggestPrefix> findByIsActive(Object isActive) {
		return findByProperty(IS_ACTIVE, isActive);
	}

	/**
	 * Find all SiteAutosuggestPrefix entities.
	 * 
	 * @return List<SiteAutosuggestPrefix> all SiteAutosuggestPrefix entities
	 */
	@SuppressWarnings("unchecked")
	public List<SiteAutosuggestPrefix> findAll() {
		logger.debug("Request comes into the findAll method of the class SiteAutosuggestPrefixDAO, the request has no input parameters");
		logger.info("finding all SiteAutosuggestPrefix instances",null);
		try {
			final String queryString = "select model from SiteAutosuggestPrefix model";
			Query query = getSession().createQuery(queryString);
			List<SiteAutosuggestPrefix> list=query.list();
			logger.debug("Request goes out from the findByProperty method of the class SiteAutosuggestPrefixDAO, the request  return type is List<SiteAutosuggestPrefix> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find all failed",re);
			throw re;
		}
	}

}