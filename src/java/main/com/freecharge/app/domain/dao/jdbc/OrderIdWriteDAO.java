package com.freecharge.app.domain.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.object.StoredProcedure;

import com.freecharge.admin.CustomerTrailDetails.OrderUserIdMapper;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdLidMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIdRowMapper;
import com.freecharge.app.service.CommonService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.types.Bill.ServiceProvider;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.types.MTNLLandLineBill;
import com.freecharge.infrastructure.billpay.types.RelianceEnergyMumbai;
import com.freecharge.payment.services.PaymentStatusDetails;
import com.freecharge.payment.services.PaymentStatusDetails.PaymentStatus;
import com.freecharge.recharge.exception.InvalidOrderIDException;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;

public class OrderIdWriteDAO {
	private static Logger logger = LoggingFactory.getLogger(OrderIdWriteDAO.class);
	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertOrderId;
	
	private DataSource dataSource;

	@Autowired
    private CommonService commonService;
	
	@Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertOrderId = new SimpleJdbcInsert(dataSource).withTableName("order_id").usingGeneratedKeyColumns("pk+order_id", "n_last_updated", "n_created");
	}

	public OrderId create(OrderId orderId) {
		try {
			
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(orderId);
			
			Number number = this.insertOrderId.executeAndReturnKey(parameters);
			
			orderId.setPkOrderId(number.intValue());
			return orderId;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public OrderId update(OrderId orderId) {
		try {
			
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(orderId);
			
			String sql = "update order_id set order_id = :orderId where pk_order_id = :pkOrderId";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			return orderId;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
    public String getLookupIdForOrderId(String orderId) {
        String queryString = "select lookup_id from order_id o where o.order_id = :orderId";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        
        List<String> lidList = this.jdbcTemplate.query(queryString, paramMap, new OrderIdLidMapper());
        
        if(lidList == null || lidList.isEmpty()) {
            throw new InvalidOrderIDException("Received empty set of lookup ids for order ID : " + orderId);
        }
        return lidList.get(0);
    }

    public Integer getUserIdFromOrderId(String orderId){
        String sql = "select c.fk_user_id from order_id oid, cart c where c.lookup_id = oid.lookup_id and order_id = :orderId";
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("orderId", orderId);
        List<String> userIdList = this.jdbcTemplate.query(sql, param, new OrderUserIdMapper());
        if (userIdList == null || userIdList.size() == 0){
            return null;
        }
        Integer userIdInt = Integer.parseInt(userIdList.get(0));
        return userIdInt;
    }
    
	/**
	 * returns a hash map having the updated CouponCode list and list of CouponStores which failed in the process. the keys are "successList" and "failureList". If failure list is null... assume everything gone well.
	 * 
	 * @param couponStoreId
	 * @param quantity
	 * @param orderId
	 * @param isComplimentary
	 * 
	 * @return HashMap <String, Object> couponCodeIds and errors.
	 */
	public String generateOrderId(String sessionId, String lookupId, Integer productMasterId , Integer affiliateProfileId , Integer channelId, String orderIdPrefix) {
		logger.debug("Request comes into the generateOrderId method of the class OrderIdWriteDAO with request parameter orderIdPrefix = " + orderIdPrefix);
		String orderId = null;
		try {
			GenerateOrderId generateOrderId = new GenerateOrderId(dataSource);

			orderId = generateOrderId.execute(sessionId, lookupId, productMasterId, affiliateProfileId , channelId, orderIdPrefix);

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

		logger.debug("Request goes out from the generateOrderId method of the class OrderIdWriteDAO with orderId = " + orderId);
		return orderId;

	}

	private class GenerateOrderId extends StoredProcedure {
		private static final String SQL = "generateOrderId";

		public GenerateOrderId(DataSource dataSource) {
			setDataSource(dataSource);
			setSql(SQL);

			declareParameter(new SqlParameter("sessionId", Types.VARCHAR));
			declareParameter(new SqlParameter("lookupId", Types.VARCHAR));
			declareParameter(new SqlParameter("productMasterId", Types.INTEGER));
			declareParameter(new SqlParameter("affiliateProfileId", Types.INTEGER));
			declareParameter(new SqlParameter("channelId", Types.INTEGER));
			declareParameter(new SqlParameter("orderIdPrefix", Types.VARCHAR));

			declareParameter(new SqlOutParameter("orderId", Types.VARCHAR));

			compile();
		}

		public String execute(String sessionId, String lookupId, Integer productMasterId , Integer affiliateProfileId , Integer channelId, String orderIdPrefix) {

			Map<String, Object> inParams = new HashMap<String, Object>();

			inParams.put("sessionId", sessionId);
			inParams.put("lookupId", lookupId);
			inParams.put("productMasterId", productMasterId);
			inParams.put("affiliateProfileId", affiliateProfileId);
			inParams.put("channelId", channelId);
			inParams.put("orderIdPrefix", orderIdPrefix);

			Map<String, Object> outParams = execute(inParams);

			if (outParams != null && outParams.keySet().size() > 0) {
				return (String) outParams.get("orderId");
			} else {
				return null;
			}
		}
	}
	
	    public String getOrderIdByAllParams(String sessionId, String lookupId, int affiliateId, int producMasterId, int channelId) {
	        logger.debug("Request comes into the getOrderIDDetails method of the class OrderIdDAO, the request  input parameters  String "+ sessionId+" String "+ lookupId+" int "+ affiliateId+" int "+producMasterId+" int "+ channelId);
	        
	        String queryString = "select order_id from  order_id where session_id = :sessionId and " + 
	                "lookup_id = :lookupId and fk_affiliate_profile_id = :affiliateId " + 
	                "and fk_product_master_id = :productMasterId and channel_id = :channelId ";

	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        
	        paramMap.put("sessionId", sessionId);
	        paramMap.put("lookupId", lookupId);
	        paramMap.put("affiliateId", affiliateId);
	        paramMap.put("productMasterId", producMasterId);
	        paramMap.put("channelId", channelId);

	        List<Map<String, Object>> list = this.jdbcTemplate.queryForList(queryString, paramMap);
	        
	        String orderId = null;
	        
	        if (list != null && list.size() > 0) {
	            orderId = (String)list.get(0).get("order_id");
	        }

	        logger.debug("Request goes out from the getOrderIDDetails method of the class OrderIdDAO, the request  return type is String "+orderId);
	        return orderId;
	    }

	    public List<OrderId> getByOrderId(String orderId) {
	        String queryString = "select * from order_id where order_id = :orderId";
	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        paramMap.put("orderId", orderId);

	        List<OrderId> orderIds = this.jdbcTemplate.query(queryString, paramMap, new OrderIdRowMapper());

	        return orderIds;
	    }
	    
	    public static final String RECHARGE_DETAILS_SQL = "" +
	            "SELECT thp.fk_product_master_id, thp.fk_operator_master_id, thp.operator_name, thp.circle_name, " +
	            " thp.amount, thp.service_number, thp.creatred_on, thp.fk_circle_master_id" +
	            " FROM txn_home_page thp, order_id oid" +
	            " WHERE  oid.lookup_id = thp.lookup_id" +
	            " AND oid.order_id = :order_id";

	    public RechargeDetails getRechargeDetailsFromHomePageByOrderID(String orderID) {
	        final Map<Integer, OperatorMaster> operatorMasterMap = commonService.getOperatorMasters();
	        
	        Map<String, Object> params = Collections.singletonMap("order_id", (Object) orderID);
	        List <RechargeDetails> rechargeList = jdbcTemplate.query(RECHARGE_DETAILS_SQL, params, new RowMapper<RechargeDetails>() {
	            @Override
	            public RechargeDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
	                RechargeDetails rd = new RechargeDetails();
	                int productMasterId = rs.getInt("fk_product_master_id");
	                rd.setProductName(FCConstants.productTypeToNameMap.get(FCConstants.productMasterMap.get(productMasterId)));
	                rd.setOperatorName(rs.getString("operator_name"));
	                OperatorMaster opm = operatorMasterMap.get(rs.getInt("fk_operator_master_id"));
	                rd.setOperatorImageURL(opm!=null? opm.getImgUrl(): "");
	                rd.setCircleName(rs.getString("circle_name"));
	                rd.setCircleId(rs.getString("fk_circle_master_id"));
	                rd.setAmount(rs.getString("amount"));
	                rd.setServiceNumber(rs.getString("service_number"));
	                rd.setRechargedOn(rs.getDate("creatred_on"));
	                rd.setOperatorMasterId(rs.getString("fk_operator_master_id"));
	                return rd;
	            }
	        });
	        RechargeDetails rechargeDetails = null;
	        if (rechargeList != null && rechargeList.size() > 0){
	            rechargeDetails = rechargeList.get(0);
	        }
	        String productType = null;
	        if (rechargeDetails != null && rechargeDetails.getProductName() != null) {
	            productType = FCConstants.productNameToTypeMap.get(rechargeDetails.getProductName());
	        }
	        if (productType != null && FCUtil.isUtilityPaymentType(productType)) {
	        	BillerDetails billPayOperatorMaster = commonService.getBillPayOperatorMasterByOperatorId(Integer.parseInt(rechargeDetails.getOperatorMasterId()));
	            if (billPayOperatorMaster != null) {
	                rechargeDetails.setOperatorImageURL(billPayOperatorMaster.getImgUrl());
	            }
	            BillWithSubscriberNumber bill = null;
	            try {
	                bill = billPayServiceRemote.getBillDetails(rechargeDetails.getServiceNumber());
	            } catch (Exception e) {
	                logger.error("Error while fetching bill details for orderId " + orderID + ", billTransactionId :" + rechargeDetails.getServiceNumber() , e);
	                return null;
	            }
	            ServiceProvider serviceProvider = bill.getServiceProvider();
	            if (serviceProvider != null ) {
    	            if (serviceProvider.equals(ServiceProvider.RelianceEnergyMumbai)) {
    	                RelianceEnergyMumbai rel = (RelianceEnergyMumbai) bill;
    	                rechargeDetails.setBillingCycle(rel.getBillingCycle());
    	            } else if (serviceProvider.equals(ServiceProvider.MTNLLandLine)) {
    	                MTNLLandLineBill mtnl = (MTNLLandLineBill) bill;
    	                rechargeDetails.setAccountNumber(mtnl.getAdditionalInfo2());
    	            }
	            }
	            rechargeDetails.setServiceNumber(bill.getAdditionalInfo1());
	            rechargeDetails.setCircleName(bill.getCircleName());
	            rechargeDetails.setCircleId(bill.getCircleId() == null ? "" : bill.getCircleId().toString());
	        }
	        if(productType!=null && FCConstants.PRODUCT_TYPE_HCOUPON.equals(productType)) {
	        	rechargeDetails.setServiceNumber("Deals");
	        }
	        return rechargeDetails;
	    }
	    
	    
	    //Calling it from master because in case of mobile app wallet payment if slave is lagging it is giving exception
	    public ProductName getProductName(String orderId) {
	        String queryString = "select fk_product_master_id from order_id where order_id = :orderId;";
	        
	        Map<String, Object> paramMap = new HashMap<String, Object>();
	        paramMap.put("orderId", orderId);
	        
	        List<Integer> productIds = this.jdbcTemplate.queryForList(queryString, paramMap,Integer.class);
	        int productId = (productIds != null && productIds.size() > 0 ) ? productIds.get(0) : 0;
	        if(productId == 0){
	            queryString = "select reward_recharge_id from reward_recharge where order_id = :orderId;";
	            List<Integer> rechargeRewardIds = this.jdbcTemplate.queryForList(queryString, paramMap,Integer.class);
	            if(rechargeRewardIds != null && rechargeRewardIds.size() > 0 && rechargeRewardIds.get(0) > 0){
	                productId = ProductName.RechargeReward.getProductId();
	            }
	        }
	        return ProductName.fromProductId(productId);
	    }
	    
        public PaymentStatusDetails isPaymentSuccessful(String lookupId) {
            String queryString = "select pt.is_successful as success, pt.order_id as oid from payment_txn pt, order_id oi where oi.lookup_id = :lookupId and oi.order_id = pt.order_id;";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("lookupId", lookupId);
            
            List<PaymentStatusDetails> payResultList = this.jdbcTemplate.query(queryString, paramMap, new RowMapper<PaymentStatusDetails>() {
                                              @Override
                                              public PaymentStatusDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
                                                  PaymentStatusDetails result = new PaymentStatusDetails();
                                                  
                                                  Object successObj = rs.getObject("success");
                                                  
                                                  if (successObj == null) {
                                                      result.setPaymentStatus(PaymentStatus.Unknown);
                                                  } else {
                                                      Boolean isPaySuccessful = (Boolean) successObj;
                                                      
                                                      if (isPaySuccessful) {
                                                          result.setPaymentStatus(PaymentStatus.Success);
                                                      } else {
                                                          result.setPaymentStatus(PaymentStatus.Failure);
                                                      }
                                                      
                                                  }
                                                  
                                                  result.setOrderId(rs.getString("oid"));
                                                  
                                                  return result;
                                              }
                                          });
            
            if (payResultList == null || payResultList.isEmpty()) {
                return unknownPaymentStatus();
            }
            
            /*
             * First search for success, then failure and if neither
             * is found then return unknown.
             */
            for (PaymentStatusDetails paymentResult : payResultList) {
                if (paymentResult.getPaymentStatus() == PaymentStatus.Success) {
                    return paymentResult;
                }
            }

            for (PaymentStatusDetails paymentResult : payResultList) {
                if (paymentResult.getPaymentStatus() == PaymentStatus.Failure) {
                    return paymentResult;
                }
            }
            
            return unknownPaymentStatus();
	    }

    public Boolean isOrderSuccessful(String orderId) {

        String queryString = "select pt.is_successful as success, pt.order_id as oid from payment_txn pt, order_id oi where oi.order_id = :order_id and oi.order_id = pt.order_id;";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("order_id", orderId);

        List<PaymentStatusDetails> payResultList = this.jdbcTemplate.query(queryString, paramMap,
                new RowMapper<PaymentStatusDetails>() {
                    @Override
                    public PaymentStatusDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
                        PaymentStatusDetails result = new PaymentStatusDetails();

                        Object successObj = rs.getObject("success");

                        if (successObj == null) {
                            result.setPaymentStatus(PaymentStatus.Unknown);
                        } else {
                            Boolean isPaySuccessful = (Boolean) successObj;

                            if (isPaySuccessful) {
                                result.setPaymentStatus(PaymentStatus.Success);
                            } else {
                                result.setPaymentStatus(PaymentStatus.Failure);
                            }

                        }

                        result.setOrderId(rs.getString("oid"));

                        return result;
                    }
                });

        if (payResultList == null || payResultList.isEmpty()) {
            return null;
        }

        /*
         * First search for success, then failure and if neither is found then
         * return unknown.
         */
        for (PaymentStatusDetails paymentResult : payResultList) {
            if (paymentResult.getPaymentStatus() == PaymentStatus.Success) {
                return Boolean.TRUE;
            }
        }

        for (PaymentStatusDetails paymentResult : payResultList) {
            if (paymentResult.getPaymentStatus() == PaymentStatus.Failure) {
                return Boolean.FALSE;
            }
        }

        return null;

    }

       private PaymentStatusDetails unknownPaymentStatus() {
            PaymentStatusDetails unknown = new PaymentStatusDetails();
            unknown.setPaymentStatus(PaymentStatus.Unknown);
            
            return unknown;
        }
}