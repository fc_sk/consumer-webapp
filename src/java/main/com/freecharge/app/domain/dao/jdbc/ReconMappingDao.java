package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.ReconMapping;
@Component
public class ReconMappingDao {

	private final Logger logger = Logger.getLogger(getClass());
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
	
	public Integer insertReconMapping(ReconMapping reconMapping) {
		logger.info("ReconMappingDao::insertReconMapping("+reconMapping+")");
		Integer count = -1;
		Integer emailId = new Integer(-1);
		try {
			final String queryString = "insert into recon_mapping(recon_id,order_id) "
					+ "values (:reconId, :orderId)";
			MapSqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("reconId", reconMapping.getReconId())
					.addValue("orderId", reconMapping.getOrderId());
			
			KeyHolder keyHolder = new GeneratedKeyHolder();
			
			count = jdbcTemplate.update(queryString, parameters);
			emailId=new Integer(keyHolder.getKey().intValue());
		} catch (RuntimeException re) {
			logger.error("createEmail failed", re);
			return emailId;
		}
		return emailId;
	}
	
	
	public ReconMapping getReconMapping(String reconId) {
		ReconMapping result = null;
		try {
			final String queryString = "select * from recon_mapping where recon_id = :reconId";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("reconId", reconId);

			List<ReconMapping> reconMappingList = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(ReconMapping.class));
			if(reconMappingList == null || reconMappingList.size() == 0){
				logger.error("no recon mapping exist for reconId: " + reconId);
				return null;
			}
			if(reconMappingList.size() > 1){
				logger.error("more than one recon details exist for reconId: " + reconId);
			}
			
			return reconMappingList.get(0);
		} catch (RuntimeException re) {
			logger.error("Exception in get getReconMapping by reconId : " + reconId, re);
//			throw re;
		}
		return result;
	}
}
