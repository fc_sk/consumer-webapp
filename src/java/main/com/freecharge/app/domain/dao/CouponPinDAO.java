package com.freecharge.app.domain.dao;


import java.util.List;

import org.apache.log4j.Logger;

import com.freecharge.app.domain.entity.winpin.CouponPin;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * CouponPin entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @author MyEclipse Persistence Tools
 */

public class CouponPinDAO extends CommonDao<CouponPin> implements ICouponPinDAO {

    public static final String FK_COUPAN_TYPE_ID = "fkCoupanTypeId";
    public static final String CF_ACTION_STATUS = "cfActionStatus";
    public static final String CF_ACTIVE_FLAG = "cfActiveFlag";
    public static final String CF_ARCHIVE_STATUS = "cfArchiveStatus";
    public static final String CF_CREATED_BY_ID = "cfCreatedById";
    public static final String CF_LAST_UPDATED_BY_ID = "cfLastUpdatedById";
    public static final String CPN_ACTIVE = "cpnActive";
    public static final String CPN_BLOCK = "cpnBlock";
    public static final String CPN_USED = "cpnUsed";
    public static final String CPN_IN_RECHARGE = "cpnInRecharge";
    public static final String CPN_IS_REDEEMED = "cpnIsRedeemed";
    public static final String CPN_LOCK_STATUS = "cpnLockStatus";
    public static final String CPN_PIN = "cpnPin";
    public static final String CPN_SR_NO = "cpnSrNo";
    public static final String CPN_USABLE = "cpnUsable";
    public static final String ORDER_ID = "orderId";
    public static final String PG_STATUS = "pgStatus";
    public static final String RECHARGE_STATUS = "rechargeStatus";
    public static final String UNIQUE_ID = "uniqueId";
    private static Logger logger = LoggingFactory.getLogger(CouponPinDAO.class);



    protected Class<CouponPin> getDomainClass() {
        return CouponPin.class;  //To change body of implemented methods use File | Settings | File Templates.
    }

    
    /**
     * Find all CouponPin entities with a specific property value.
     *
     * @param propertyName the name of the CouponPin property to query
     * @param value        the property value to match
     * @return List<CouponPin> found by query
     */
    @SuppressWarnings("unchecked")
    public List<CouponPin> findByProperty(String propertyName,
                                          final Object value) {
    	logger.debug("Request comes into the findByProperty method of the class CouponPinDAO, the request input parameters are String propertyName"+propertyName+" and Object value:"+value);

        logger.info("finding CouponPin instance with property: " + propertyName + ", value: " + value);
        try {
            final String queryString = "select model from CouponPin model where model."
                    + propertyName + "= :propertyValue";
            org.hibernate.Query query = getSession().createQuery(queryString);
            query.setParameter("propertyValue", value);
            List<CouponPin> couponPins = query.list();
			logger.debug("Request goes out from the findByProperty method of the class CouponPinDAO, the request return type is List<CouponPin> :"+couponPins);
			return couponPins;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }

    }

    public List<CouponPin> findByFkCoupanTypeId(Object fkCoupanTypeId) {
        return findByProperty(FK_COUPAN_TYPE_ID, fkCoupanTypeId);
    }

    public List<CouponPin> findByCfActionStatus(Object cfActionStatus) {
        return findByProperty(CF_ACTION_STATUS, cfActionStatus);
    }

    public List<CouponPin> findByCfActiveFlag(Object cfActiveFlag) {
        return findByProperty(CF_ACTIVE_FLAG, cfActiveFlag);
    }

    public List<CouponPin> findByCfArchiveStatus(Object cfArchiveStatus) {
        return findByProperty(CF_ARCHIVE_STATUS, cfArchiveStatus);
    }

    public List<CouponPin> findByCfCreatedById(Object cfCreatedById) {
        return findByProperty(CF_CREATED_BY_ID, cfCreatedById);
    }

    public List<CouponPin> findByCfLastUpdatedById(Object cfLastUpdatedById) {
        return findByProperty(CF_LAST_UPDATED_BY_ID, cfLastUpdatedById);
    }

    public List<CouponPin> findByCpnActive(Object cpnActive) {
        return findByProperty(CPN_ACTIVE, cpnActive);
    }

    public List<CouponPin> findByCpnBlock(Object cpnBlock) {
        return findByProperty(CPN_BLOCK, cpnBlock);
    }

    public List<CouponPin> findByCpnUsed(Object cpnUsed) {
        return findByProperty(CPN_USED, cpnUsed);
    }

    public List<CouponPin> findByCpnInRecharge(Object cpnInRecharge) {
        return findByProperty(CPN_IN_RECHARGE, cpnInRecharge);
    }

    public List<CouponPin> findByCpnIsRedeemed(Object cpnIsRedeemed) {
        return findByProperty(CPN_IS_REDEEMED, cpnIsRedeemed);
    }

    public List<CouponPin> findByCpnLockStatus(Object cpnLockStatus) {
        return findByProperty(CPN_LOCK_STATUS, cpnLockStatus);
    }

    public List<CouponPin> findByCpnPin(Object cpnPin) {
        return findByProperty(CPN_PIN, cpnPin);
    }

    public List<CouponPin> findByCpnSrNo(Object cpnSrNo) {
        return findByProperty(CPN_SR_NO, cpnSrNo);
    }

    public List<CouponPin> findByCpnUsable(Object cpnUsable) {
        return findByProperty(CPN_USABLE, cpnUsable);
    }

    public List<CouponPin> findByOrderId(Object orderId) {
        return findByProperty(ORDER_ID, orderId);
    }

    public List<CouponPin> findByPgStatus(Object pgStatus) {
        return findByProperty(PG_STATUS, pgStatus);
    }

    public List<CouponPin> findByRechargeStatus(Object rechargeStatus) {
        return findByProperty(RECHARGE_STATUS, rechargeStatus);
    }

    public List<CouponPin> findByUniqueId(Object uniqueId) {
        return findByProperty(UNIQUE_ID, uniqueId);
    }

    public CouponPin findOneCpnPin(String cpnPin) {
    	logger.debug("Request comes into the findOneCpnPin method of the class CouponPinDAO, the request input parameters is String cpnPin:"+cpnPin);
        CouponPin couponPin = null;
        List<CouponPin> couponPins = findByCpnPin(cpnPin);
        if (couponPins.size() > 0)
            couponPin = findByCpnPin(cpnPin).get(0);
        logger.debug("Request goes out from the findOneCpnPin method of the class CouponPinDAO, the request return type is CouponPin :"+couponPin);
        return couponPin;
    }

    /**
	 * Find all CouponPin entities.
	 *
	 * @return List<CouponPin> all CouponPin entities
	 */
	

}