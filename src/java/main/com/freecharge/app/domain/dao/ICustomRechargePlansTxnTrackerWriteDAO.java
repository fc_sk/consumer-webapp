package com.freecharge.app.domain.dao;

import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;

public interface ICustomRechargePlansTxnTrackerWriteDAO {
	public void savePlanDetails(CustomRechargePlanTxnTracker customRechargePlanTxnTracker );
}
