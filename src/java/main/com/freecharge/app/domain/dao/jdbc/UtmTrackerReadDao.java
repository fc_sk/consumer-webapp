package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.UtmTracker;
import com.freecharge.app.domain.entity.jdbc.mappers.UtmTrackerRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmTrackerReadDao {
    private static Logger logger = LoggingFactory.getLogger(UtmTrackerReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<UtmTracker> getUtmCampaign(String key, String value) {
        List<UtmTracker> utmTrackerList = new ArrayList<>();
        try{
            String queryString = "select * from utm_campaign_details where " +  key  + "=:value";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("value", value);

            utmTrackerList = this.jdbcTemplate.query(queryString, paramMap, new UtmTrackerRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch utm tracker details", e);
        }
        return utmTrackerList;
    }

    public List<UtmTracker> getAllCampaigns() {
        List<UtmTracker> utmTrackerList = new ArrayList<>();
        try{
            String queryString = "select * from utm_campaign_details";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            utmTrackerList = this.jdbcTemplate.query(queryString, paramMap, new UtmTrackerRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch utm tracker details", e);
        }
        return utmTrackerList;
    }

    public List<UtmTracker> getUtmCampaignforMaster(String fcMaster, String campaignName) {
        List<UtmTracker> utmTrackerList = new ArrayList<>();
        try{
            String queryString = "select * from utm_campaign_details where fc_master=:fcMaster and utm_campaign_name=:campaignName";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("fcMaster", fcMaster);
            paramMap.put("campaignName", campaignName);

            utmTrackerList = this.jdbcTemplate.query(queryString, paramMap, new UtmTrackerRowMapper());
        } catch (Exception e){
            logger.error("Failed to fetch utm tracker details", e);
        }
        return utmTrackerList;
    }
}
