package com.freecharge.app.domain.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.AccessDetails;

public class AccessDetailsMapper implements RowMapper<AccessDetails> {

    @Override
    public AccessDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        AccessDetails accessDetails = new AccessDetails();
        accessDetails.setId(rs.getInt("id"));
        accessDetails.setAccessKey(rs.getString("access_key"));
        accessDetails.setApi(rs.getString("api"));
        accessDetails.setActive(rs.getBoolean("is_active"));
        return accessDetails;
    }

}
