package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.ProductMaster;

/**
 * Interface for ProductMasterDAO.
 * @author MyEclipse Persistence Tools
 */

public interface IProductMasterDAO {
		
	public List<ProductMaster> findByProperty(String propertyName, Object value
		);
	public List<ProductMaster> findByProductName(Object productName
		);
	public List<ProductMaster> findByImgUrl(Object imgUrl
		);
	public List<ProductMaster> findByFkAffiliateProfileId(Object fkAffiliateProfileId
		);
	public List<ProductMaster> findByIsActive(Object isActive
		);
	public List<ProductMaster> findByProductType(Object productType
		);
	public List<ProductMaster> findByIsRefundable(Object isRefundable
		);
	/**
	 * Find all ProductMaster entities.
	  	  @return List<ProductMaster> all ProductMaster entities
	 */
	public List<ProductMaster> findAll(
		);	
}