package com.freecharge.app.domain.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.admin.CustomerTrailDetails.TxnHomePageIdMapper;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.common.businessdo.HomeBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;

public class TxnHomePageDAO extends CommonDao<TxnHomePage> implements ITxnHomePageDAO {
    // property constants
    public static final String SERVICE_NUMBER = "serviceNumber";
    public static final String OPERATOR_NAME = "operatorName";
    public static final String CIRCLE_NAME = "circleName";
    public static final String AMOUNT = "amount";
    public static final String PRODUCT_TYPE = "productType";
    public static final String SESSION_ID = "sessionId";
    public static final String LOOKUP_ID = "lookupId";

    private static Logger logger = LoggingFactory.getLogger(TxnHomePageDAO.class);

    protected Class<TxnHomePage> getDomainClass() {
        return TxnHomePage.class;
    }

    /**
     * Find all TxnHomePage entities with a specific property value.
     * 
     * @param propertyName
     *            the name of the TxnHomePage property to query
     * @param value
     *            the property value to match
     * @return List<TxnHomePage> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TxnHomePage> findByProperty(String propertyName, final Object value) {
        logger.debug(String.format("Entered TxnHomePageDAO.findByProperty(propertyName=%s, value=%s)", propertyName,
                value));
        try {
            final String queryString = "select model from TxnHomePage model where model." + propertyName
                    + "= :propertyValue";
            Query query = getSession().createQuery(queryString);
            query.setParameter("propertyValue", value);
            List<TxnHomePage> list = query.list();
            logger.debug("Exiting TxnHomePageDAO.findByProperty() = List<TxnHomePage>" + list);
            return list;
        } catch (RuntimeException re) {
            logger.error("Find by property name failed", re);
            throw re;
        }
    }

    public List<TxnHomePage> findByServiceNumber(Object serviceNumber) {
        return findByProperty(SERVICE_NUMBER, serviceNumber);
    }

    public List<TxnHomePage> findByOperatorName(Object operatorName) {
        return findByProperty(OPERATOR_NAME, operatorName);
    }

    public List<TxnHomePage> findByCircleName(Object circleName) {
        return findByProperty(CIRCLE_NAME, circleName);
    }

    public List<TxnHomePage> findByAmount(Object amount) {
        return findByProperty(AMOUNT, amount);
    }

    public List<TxnHomePage> findByProductType(Object productType) {
        return findByProperty(PRODUCT_TYPE, productType);
    }

    public List<TxnHomePage> findBySessionId(Object sessionId) {
        return findByProperty(SESSION_ID, sessionId);
    }

    public List<TxnHomePage> findByLookupId(Object lookupId) {
        return findByProperty(LOOKUP_ID, lookupId);
    }

    /**
     * Find all TxnHomePage entities.
     * 
     * @return List<TxnHomePage> all TxnHomePage entities
     */
    @SuppressWarnings("unchecked")
    public List<TxnHomePage> findAll() {
        logger.debug("Entered TxnHomePageDAO.findAll()");
        try {
            final String queryString = "select model from TxnHomePage model";
            Query query = getSession().createQuery(queryString);
            List<TxnHomePage> list = query.list();
            logger.debug("Exiting TxnHomePageDAO.findByProperty() = List<TxnHomePage> " + list);
            return list;
        } catch (RuntimeException re) {
            logger.error("find all failed", re);
            throw re;
        }
    }

    public Integer saveTransactionInfo(TxnHomePage txnHomePage) {
        logger.debug(String.format("Entered TxnHomePageDAO.saveTransactionInfo(txnHomePage = %s)", txnHomePage));
        Session session = null;
        try {
            session = this.getSession();
            Integer homePageId = (Integer) session.save(txnHomePage);
            logger.debug("Exiting TxnHomePageDAO.saveTransactionInfo() = Integer" + homePageId);
            return homePageId;
        } catch (HibernateException ex) {
            logger.error("Error occured while saving home page tranction info.." + ex);
            return 0;
        }
    }

    /**
     * Find TxnHomePage Data For lookupID
     * 
     * @return List<HomeBusinessDo> one HomeBusinessDo with LookupId
     */

    public List<HomeBusinessDo> getTxnHomePageDetailsByLookUpId(String lookupId, BaseBusinessDO homeBusinessDO) {
        logger.debug(String.format(
                "Request comes into TxnHomePageDAO.getTxnHomePageDetailsByLookUpId(lookupId=%s, homeBusinessDO=%s)",
                lookupId, homeBusinessDO));
        Session session = null;
        try {
            session = this.getSession();
            String sql = ""
                    + "SELECT"
                    + "   product.product_master_id, txn.service_number ,txn.amount, operator.operator_master_id,"
                    + "   circle.circle_master_id,txn.id AS txnHomePageId, operator.name"
                    + " FROM txn_home_page txn,circle_master AS circle,product_master AS product,operator_master AS operator"
                    + " WHERE product.product_master_id = txn.fk_product_master_id"
                    + "   AND circle.circle_master_id=txn.fk_circle_master_id "
                    + "   AND operator.operator_master_id=txn.fk_operator_master_id AND txn.lookup_id=:lookupId";
            Query query = session.createSQLQuery(sql);
            query.setParameter("lookupId", lookupId);
            List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
            List<HomeBusinessDo> homeBusinessDos = new ArrayList<HomeBusinessDo>();
            for (List<Object> list : ObjectList) {

                int prodId = Integer.parseInt(list.get(0).toString());

                if (prodId == 1) {
                    ((HomeBusinessDo) homeBusinessDO).setMobileNumber((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setAmount((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayServiceNo((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayOperatorName((list.get(6).toString()));
                    Double displayAmount = Double.parseDouble((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayAmount(displayAmount.intValue());

                } else if (prodId == 2) {
                    ((HomeBusinessDo) homeBusinessDO).setDthNumber((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDthAmount((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayServiceNo((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayOperatorName((list.get(6).toString()));
                    Double displayAmount = Double.parseDouble((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayAmount(displayAmount.intValue());
                } else if (prodId == 3) {
                    ((HomeBusinessDo) homeBusinessDO).setDataCardNumber((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDataCardAmount((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayServiceNo((list.get(1).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayOperatorName((list.get(6).toString()));
                    Double displayAmount = Double.parseDouble((list.get(2).toString()));
                    ((HomeBusinessDo) homeBusinessDO).setDisplayAmount(displayAmount.intValue());
                }
                ((HomeBusinessDo) homeBusinessDO).setTxnHomePageId(Integer.parseInt(list.get(5).toString()));
                ((HomeBusinessDo) homeBusinessDO).setOperator((list.get(3).toString()));
                ((HomeBusinessDo) homeBusinessDO).setCircle(((list.get(4).toString())));
                ((HomeBusinessDo) homeBusinessDO).setProductMasterId(prodId);
                homeBusinessDos.add(((HomeBusinessDo) homeBusinessDO));

            }
            logger.debug("Exiting TxnHomePageDAO.getTxnHomePageDetailsByLookUpId()=List<HomeBusinessDo>"
                    + homeBusinessDos);
            return homeBusinessDos;

        } catch (Exception ex) {
            logger.error("Error occured while saving home page tranction info" + ex);
            return null;
        }
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(TxnHomePageDAO.class.getSimpleName());
    }

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public BigDecimal findAmountByLookupID(String lookupID) {
        String queryString = "SELECT amount FROM txn_home_page WHERE lookup_id = :lookupid";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("lookupid", lookupID);
        List<BigDecimal> result = this.jdbcTemplate.queryForList(queryString, paramMap, BigDecimal.class);
        return (result == null || result.size() == 0) ? null : result.get(0);
    }

    public String getLookupIdByTxnHomePageId(String txnHomePageId) {
        String query = "select model.lookup_id from txn_home_page model where model.id = :txnHomePageId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("txnHomePageId", txnHomePageId);
        List<String> lookupIdList = this.jdbcTemplate.query(query, paramMap, new TxnHomePageIdMapper());

        String lookupId = null;
        if (lookupIdList != null && lookupIdList.size() > 0) {
            lookupId = lookupIdList.get(0);
        }
        return lookupId;
    }

    public Integer findTxnHomePageIdBySessionId(final Object value) {
        final String queryString = "select id from txn_home_page  where lookup_id = :propertyValue";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("propertyValue", value);
        Integer id = this.jdbcTemplate.queryForInt(queryString, paramMap);
        return id;
    }
    
    
    public TxnHomePage findTxnHomePageByOrderID(final String orderID) {
        final String sqlTHPFromOID =
         "SELECT model FROM TxnHomePage model, OrderId oid " +
         "WHERE oid.lookupId= model.lookupId AND oid.orderId = :orderID";
        Session session = null;
        try {
            session = this.getSession();
            Query query = session.createQuery(sqlTHPFromOID);
            query.setParameter("orderID", orderID);
            return (TxnHomePage) query.uniqueResult();
        } catch (HibernateException e) {
            String msg = String.format("Error retrieving TxnHomePage instance for orderID=%s",
                    FCUtil.stringify(orderID));
            logger.error(msg, e);
            throw new IllegalStateException(msg, e);
        }
    }

    public BigDecimal findAmountByOrderID(final String orderID) {
        final String sqlAmountFromOID =
         "SELECT model.amount FROM txn_home_page model, order_id oid " +
         "WHERE oid.lookup_id= model.lookup_id AND oid.order_id = :orderID";
        try {
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("orderID", orderID);
            List<BigDecimal> result = this.jdbcTemplate.queryForList(sqlAmountFromOID, paramMap, BigDecimal.class);
            return (result == null || result.size() == 0) ? null : result.get(0);
        } catch (HibernateException e) {
            String msg = String.format("Error retrieving TxnHomePage.Amount instance for orderID=%s",
                    FCUtil.stringify(orderID));
            logger.error(msg, e);
            throw new IllegalStateException(msg, e);
        }
    }

    public TxnHomePage findTxnHomePageByLookUpId(String lookupID) {
        final String sql = "SELECT model FROM TxnHomePage model WHERE model.lookupId = :lookup_id";
        Session session = null;

        try {
            session = this.getSession();
            Query query = session.createQuery(sql);
            query.setParameter("lookup_id", lookupID);
            return (TxnHomePage) query.uniqueResult();
        } catch (HibernateException e) {
            String msg = String.format("Error retrieving TxnHomePage instance for lookupID=%s",
                    FCUtil.stringify(lookupID));
            logger.error(msg, e);
            throw new IllegalStateException(msg, e);
        }
    }

    public int updateTxnHomePage(String lookupId, String sessionId) {
        try {
            final String sql = "UPDATE txn_home_page SET session_id=:sessionId WHERE lookup_Id=:lookupId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("lookupId", lookupId);
            paramMap.put("sessionId", sessionId);
            int status = this.jdbcTemplate.update(sql, paramMap);
            if (status <= 0) {
                logger.error("No rows affected in txn_home_page table for sessionId: " + sessionId
                        + "with Respect to LookupId: " + lookupId);
            }
            return status;
        } catch (RuntimeException re) {
            logger.error("failed to update the txnhomepage table with sessionId:" + sessionId, re);
            throw re;
        }
    }
}
