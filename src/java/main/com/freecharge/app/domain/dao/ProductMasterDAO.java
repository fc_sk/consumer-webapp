package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 	* A data access object (DAO) providing persistence and search support for ProductMaster entities.
 	 		* Transaction control of the save(), update() and delete() operations must be handled externally by senders of these methods 
 		  or must be manually added to each of these methods for data to be persisted to the JPA datastore.	
 	 * @see com.freecharge.app.domain.entity.ProductMaster
  * @author MyEclipse Persistence Tools 
 */

public class ProductMasterDAO extends CommonDao<ProductMaster>   implements IProductMasterDAO{
	//property constants
	public static final String PRODUCT_NAME = "productName";
	public static final String IMG_URL = "imgUrl";
	public static final String FK_AFFILIATE_PROFILE_ID = "fkAffiliateProfileId";
	public static final String IS_ACTIVE = "isActive";
	public static final String PRODUCT_TYPE = "productType";
	public static final String IS_REFUNDABLE = "isRefundable";
	public static final String PRODUCT_MASTER_ID = "productMasterId";
	
    private static Logger logger = LoggingFactory.getLogger(ProductMasterDAO.class);

    protected Class<ProductMaster> getDomainClass() {
        return ProductMaster.class;  //To change body of implemented methods use File | Settings | File Templates.
    }
    @SuppressWarnings("unchecked")
       public List<ProductMaster> findByProperty(String propertyName,
                                             final Object value) {
    	logger.debug("Request comes into the findByProperty method of the class ProductMasterDAO, the request  input parameters are String "+propertyName+" Object "+value);

           logger.info("finding CouponPin instance with property: " + propertyName + ", value: " + value);
           try {
               final String queryString = "select model from ProductMaster model where model."
                       + propertyName + "= :propertyValue";
               org.hibernate.Query query = getSession().createQuery(queryString);
               query.setParameter("propertyValue", value);
               List<ProductMaster> list=query.list();
               logger.debug("Request goes out from the findByProperty method of the class ProductMasterDAO, the request  return type is List<ProductMaster> "+list);
               return list;
           } catch (RuntimeException re) {
               logger.error("find by property name failed", re);
               throw re;
           }

       }
    
	public List<ProductMaster> findByProductName(Object productName
	) {
		return findByProperty(PRODUCT_NAME, productName
		);
	}
	
	public List<ProductMaster> findByImgUrl(Object imgUrl
	) {
		return findByProperty(IMG_URL, imgUrl
		);
	}
	
	public List<ProductMaster> findByFkAffiliateProfileId(Object fkAffiliateProfileId
	) {
		return findByProperty(FK_AFFILIATE_PROFILE_ID, fkAffiliateProfileId
		);
	}
	
	public List<ProductMaster> findByIsActive(Object isActive
	) {
		return findByProperty(IS_ACTIVE, isActive
		);
	}
	
	public List<ProductMaster> findByProductType(Object productType
	) {
		return findByProperty(PRODUCT_TYPE, productType
		);
	}
	
	public List<ProductMaster> findByIsRefundable(Object isRefundable
	) {
		return findByProperty(IS_REFUNDABLE, isRefundable
		);
	}
	
	public ProductMaster findByProductMasterId(Integer productMasterId) {
    	logger.debug("Request comes into the findByProductMasterId method of the class ProductMasterDAO for productMasterId : " + productMasterId);
		
        final String queryString = "select model from ProductMaster model where model.productMasterId = :productMasterIdValue";
        org.hibernate.Query query = getSession().createQuery(queryString);
        query.setParameter("productMasterIdValue", productMasterId);

        ProductMaster uniqueResult = (ProductMaster) query.uniqueResult();
        logger.debug("Request goes out from the findByProductMasterId method of the class ProductMasterDAO, the request  return type is ProductMaster " + uniqueResult);
        return uniqueResult;
	}
	
}