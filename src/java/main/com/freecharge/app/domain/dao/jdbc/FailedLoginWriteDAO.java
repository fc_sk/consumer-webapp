package com.freecharge.app.domain.dao.jdbc;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.FailedLogin;
import com.freecharge.common.framework.logging.LoggingFactory;


@Component("failedLoginWriteDAO")
public class FailedLoginWriteDAO {
    private static final Logger LOGGER = LoggingFactory.getLogger(FailedLoginWriteDAO.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private SimpleJdbcInsert simpleJdbcInsert;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("failed_logins").usingGeneratedKeyColumns(FailedLogin.FAILED_LOGIN_ID, "n_last_updated", "n_created");
    }

    public FailedLogin insertFailedLogin(final FailedLogin failedLogin) {
        LOGGER.info("Inserting a failed login attempt : " + failedLogin);
        SqlParameterSource sqlParameterSource = failedLogin.getMapSqlParameterSource();
        Number primaryKey = this.simpleJdbcInsert.executeAndReturnKey(sqlParameterSource);
        failedLogin.setFailedLoginId(primaryKey.intValue());
        return failedLogin;
    }

    public void deleteOlderFailedLogins(final Timestamp timestamp) {
        LOGGER.info("Deleting all failed login attempts older than " + timestamp);
        String sqlFormat = "DELETE FROM failed_logins WHERE attempted < :%s";
        String sql = String.format(sqlFormat, FailedLogin.FAILED_LOGIN_ATTEMPTED_TIME);
        Map<String, Timestamp> params = Collections.singletonMap(FailedLogin.FAILED_LOGIN_ATTEMPTED_TIME, timestamp);
        namedParameterJdbcTemplate.update(sql, params);
    }
}
