package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.domain.entity.jdbc.mappers.UsersMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * User: abhi
 * Date: 13/11/13
 * Time: 12:07 PM
 */
public class UserSlaveDao {
    private NamedParameterJdbcTemplate jdbcTemplate;
    private static Logger logger = LoggingFactory.getLogger(UserSlaveDao.class);
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Users> getUserByPartialEmailId(String partialEmailId) {
        try {
            logger.debug("Parameter is PartialEmailId="+ partialEmailId);
            String queryString = "select * from users where email like :emailId limit 10";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            String likeEmailIdParam = partialEmailId + "%";
            paramMap.put("emailId", likeEmailIdParam);
            
            List<Users> users = this.jdbcTemplate.query(queryString, paramMap, new UsersMapper());
            logger.debug("Returns List Of Users Objects="+users);
            if(users != null && users.size() > 0)
                return users;
            else
                return null;
        } catch (RuntimeException re) {
            logger.error("Failed for partialEmailId=" + partialEmailId, re);
            throw re;
        }
    }
}
