package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.service.GenericMisDisplayRecords;
import com.freecharge.recharge.businessdao.GenericMisData;

public interface GenericMisDataFetchDAO {

	public List<GenericMisDisplayRecords> getMisData(GenericMisData misRequestData, int pageNumber);

	public Integer getMisDataCount(GenericMisData misRequestData);
	
	public long getTotalTransactionByStatus(int transactionStatus);

	public long getTotalTxnsByRechargeStatus(String aggrRespCode);

}
