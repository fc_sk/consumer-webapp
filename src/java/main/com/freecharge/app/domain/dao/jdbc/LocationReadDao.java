package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.api.coupon.util.CouponUtil;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.CountryMaster;
import com.freecharge.app.domain.entity.jdbc.StateMaster;
import com.freecharge.app.domain.entity.jdbc.ZoneMaster;
import com.freecharge.app.domain.entity.jdbc.mappers.CityMasterMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.CountryMasterMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.StateMasterMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.ZoneMasterRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class LocationReadDao {
	private static Logger logger = LoggingFactory.getLogger(LocationReadDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

	public List<CityMaster> getCitiesByCityIds(String commaSeparatedCityIds) {
		try {
			logger.debug("Request comes into the getCities method of the class LocationReadDao, the request parameter has no input parameters ");
			String queryString = "select * from city_master where city_master_id in ( :cityIdList ) order by city_name";
			
			String[] cityIdslist = commaSeparatedCityIds.split("\\s*,\\s*");
			Set<Integer> ids = new HashSet<>();
			for(String val : cityIdslist){
				ids.add(Integer.parseInt(val));
			}
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("cityIdList", ids);
			List<CityMaster> cities = this.jdbcTemplate.query(queryString, paramMap, new CityMasterMapper());
			logger.debug("Request goes out from the getCities method of the class LocationReadDao, the request  return type is List<CityMaster> ");
			return cities;
		} catch (RuntimeException re) {
			logger.error("getAllSiteUsers failed", re);
			throw re;
		}
	}
	
	public List<CityMaster> getCities() {
		try {
			logger.debug("Request comes into the getCities method of the class LocationReadDao, the request parameter has no input parameters ");
			String queryString = "select * from city_master";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			List<CityMaster> cities = this.jdbcTemplate.query(queryString, paramMap, new CityMasterMapper());
			logger.debug("Request goes out from the getCities method of the class LocationReadDao, the request  return type is List<CityMaster> ");
			return cities;
		} catch (RuntimeException re) {
			logger.error("getAllSiteUsers failed", re);
			throw re;
		}
	}
	
	public List<CityMaster> getCity(Integer cityId) {
		try {
			logger.debug("Request comes into the getCities method of the class LocationReadDao, the request parameter has no input parameters ");
			String queryString = "select * from city_master where city_master_id = :cityId";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("cityId", cityId);
			
			List<CityMaster> cities = this.jdbcTemplate.query(queryString, paramMap, new CityMasterMapper());
			logger.debug("Request goes out from the getCities method of the class LocationReadDao, the request  return type is List<CityMaster> ");
			return cities;
		} catch (RuntimeException re) {
			logger.error("getAllSiteUsers failed", re);
			throw re;
		}
	}

	public List<CountryMaster> getCountries() {
		try {
			logger.debug("Request comes into the getCountries method of the class LocationReadDao, the request parameter has no input parameters ");
			String queryString = "select * from country_master";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			List<CountryMaster> countries = this.jdbcTemplate.query(queryString, paramMap, new CountryMasterMapper());
			logger.debug("Request goes out from the getCountries method of the class LocationReadDao, the request  return type is List<CountryMaster> ");
			return countries;
		} catch (RuntimeException re) {
			logger.error("getAllSiteUsers failed", re);
			throw re;
		}
	}
	
	public List<StateMaster> getStates() {
		try {
			logger.debug("Request comes into the getStates method of the class LocationReadDao, the request parameter has no input parameters ");
			String queryString = "select * from state_master";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			List<StateMaster> states = this.jdbcTemplate.query(queryString, paramMap, new StateMasterMapper());
			logger.debug("Request goes out from the getStates method of the class LocationReadDao, the request  return type is List<StateMaster> ");
			return states;
		} catch (RuntimeException re) {
			logger.error("getAllSiteUsers failed", re);
			throw re;
		}
	}

	public List<StateMaster> getStateWithName(String stateName){
		final String queryString = "select * from state_master where state_name=:stateName";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("stateName", stateName);
		List<StateMaster> results = this.jdbcTemplate.query(queryString,
				paramMap, new StateMasterMapper());
		return results;
	}
	
	public ZoneMaster getZone(int id) {
		ZoneMaster zone = null;
		try {
			String queryString = "select * from zone_master where zone_master_id=:id";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", id);
			zone = this.jdbcTemplate.queryForObject(queryString, paramMap, new ZoneMasterRowMapper());
		}
		catch (RuntimeException re) {
			logger.error("Failed to load zone data.", re);
		}catch (Exception e) {
			logger.error("Failed to load zone data.", e);
		}
		return zone;
	}
	
	public List<ZoneMaster> getAllZones() {
		List<ZoneMaster> zones = null;
		try {
			String queryString = "select * from zone_master";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			zones = this.jdbcTemplate.query(queryString, paramMap, new ZoneMasterRowMapper());
		}
		catch (RuntimeException re) {
			logger.error("Failed to load zone data.", re);
		}catch (Exception e) {
			logger.error("Failed to load zone data.", e);
		}
		return zones;
	}

}
