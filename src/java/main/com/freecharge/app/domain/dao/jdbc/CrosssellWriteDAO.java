package com.freecharge.app.domain.dao.jdbc;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.common.framework.logging.LoggingFactory;


public class CrosssellWriteDAO  {
	private static Logger logger = LoggingFactory.getLogger(CrosssellReadDAO.class);
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertCrosssell;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertCrosssell = new SimpleJdbcInsert(dataSource).withTableName("crosssell").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	public boolean create(Crosssell crosssell) {
		try {
			
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(crosssell);
			
			Number number = this.insertCrosssell.executeAndReturnKey(parameters);
			
			crosssell.setId(number.intValue());

			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public Crosssell update(Crosssell crosssell) {
		try {
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(crosssell);
	
			String sql = "update crosssell set xml = :xml, label = :label, category = :category, created_date = :createdDate, created_by = :createdBy, modified_on = :modifiedOn, "+
						"modified_by = :modifiedBy, effective_from = :effectiveFrom, valid_upto = :validUpto, count_uses = :countUses, price = :price, is_Active = :isActive where id = :id";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			
			if (status <= 0) {
				logger.error("No Rows affected on updating in_request for id = "+crosssell.getId());
			}	
			
			return crosssell;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public Crosssell update(Integer id, Integer quantity) {
		logger.debug("updating count uses in crosssell = ");
		try {
			Crosssell crosssell = new Crosssell();
			crosssell.setId(id);
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(crosssell);
	
			String sql = "update crosssell set count_uses = count_uses + "+quantity+" where id = : id";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			
			if (status <= 0) {
				logger.error("No Rows affected on updating in_request for id = "+crosssell.getId());
			}	
			
			return crosssell;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

}