package com.freecharge.app.domain.dao;

import com.freecharge.app.domain.entity.TestTable;

public class TestDao extends BaseDao{

    public TestTable getById(int id)
    {
        return (TestTable)getSession().load(TestTable.class, id);
    }

    public int create(TestTable t)
    {
        return (Integer) getSession().save(t);
    }


}
