package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.CityMaster;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Jun 1, 2012
 * Time: 12:52:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class CityMasterDAO extends CommonDao<CityMaster> {
    public static final String CITY_NAME = "cityName";
	public static final String STATUS = "status";
    public static final String FK_STATE_MASTER_ID = "fkStateMasterId";
	public static final String FK_COUNTRY_MASTER_ID = "fkCountryMasterId";
	public static final String FK_ZONE_MASTER_ID = "fkZoneMasterId";

    protected Class<CityMaster> getDomainClass() {
        return CityMaster.class;  //To change body of implemented methods use File | Settings | File Templates.
    }

     @SuppressWarnings("unchecked")
       public List<CityMaster> findByProperty(String propertyName,
                                             final Object value) {
               final String queryString = "select model from CityMaster model where model."
                       + propertyName + "= :propertyValue ORDER BY model.cityName";
               org.hibernate.Query query = getSession().createQuery(queryString);
               query.setParameter("propertyValue", value);
               List<CityMaster> cityMasters = query.list();
   				return cityMasters;
           }

     public String getCityNameById(String cityId) {
 			final String queryString = "select city_name from city_master where city_master_id=:city_master_id";
 			
 			Query query = this.getSession().createSQLQuery(queryString);
 			query.setParameter("city_master_id", Integer.parseInt(cityId));
 			List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
 			String cityName="";
             for (List<Object> list : ObjectList) {
             	cityName=list.get(0)+"";
             }
 			return cityName;
 		}
     
    public List<CityMaster> findByCityName(Object cityName) {
           return findByProperty(CITY_NAME, cityName);
       }

    public List<CityMaster> findByStatus(Object status) {
               return findByProperty(STATUS, status);
           }

    public List<CityMaster> findByFkStateMasterId(Object fkStateMasterId) {
               return findByProperty(FK_STATE_MASTER_ID, fkStateMasterId);
           }

    public List<CityMaster> findByFkCountryMasterId(Object fkCountryMasterId) {
               return findByProperty(FK_COUNTRY_MASTER_ID, fkCountryMasterId);
           }

    public List<CityMaster> findByFkZoneMasterId(Object fkZoneMasterId) {
               return findByProperty(FK_ZONE_MASTER_ID, fkZoneMasterId);
           }

    public Map<Integer, CityMaster> getAllCitiesMap(){
        Map<Integer, CityMaster> cities = new HashMap<Integer, CityMaster>();
        for(CityMaster city: findAll()) {
            cities.put(city.getCityMasterId(), city);
        }
        return cities;
    }
}
