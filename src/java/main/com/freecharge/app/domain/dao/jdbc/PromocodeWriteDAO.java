package com.freecharge.app.domain.dao.jdbc;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.Promocode;
import com.freecharge.common.framework.logging.LoggingFactory;

public class PromocodeWriteDAO {
	private static Logger logger = LoggingFactory.getLogger(PromocodeWriteDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	private SimpleJdbcInsert insertPromocode;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocode = new SimpleJdbcInsert(dataSource).withTableName("promocode").usingGeneratedKeyColumns("promocode_id", "n_last_updated", "n_created");
	}

	public boolean create(Promocode promocode) {
		try {

			SqlParameterSource parameters = new BeanPropertySqlParameterSource(promocode);

			Number number = this.insertPromocode.executeAndReturnKey(parameters);

			promocode.setPromocodeId(number.intValue());

			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public boolean update(Promocode promocode) {
		try {
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(promocode);

			String sql = "update promocode set promo_title = :promoTitle, promo_code = :promoCode, success_msg = :successMsg, failure_msg = :failureMsg, valid_from = :validFrom, valid_upto = :validUpto where promocode_id = :promocodeId";

			int status = this.jdbcTemplate.update(sql, parameters);

			if (status <= 0) {
				logger.error("No Rows affected on updating in_request for id = "
						+ promocode.getPromocodeId());
			}

			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

}