
 package com.freecharge.app.domain.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.winpin.CouponType;
import com.freecharge.common.framework.logging.LoggingFactory;

 /**
 * A data access object (DAO) providing persistence and search support for
 * CouponType entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 * 
 * @see
 * @author MyEclipse Persistence Tools
 */
    @Repository
    public class CouponTypeDAO extends CommonDao<CouponType> implements ICouponTypeDAO  {
	// property constants
	public static final String TYPE = "type";
	public static final String NAME = "name";
    private static Logger logger = LoggingFactory.getLogger(CouponTypeDAO.class);


     
     /**
	 * Find all CouponType entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CouponType property to query
	 * @param value
	 *            the property value to match
	 * @return List<CouponType> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CouponType> findByProperty(String propertyName,
			final Object value) {
		logger.debug("Request comes into the findByProperty method of the class CouponTypeDAO, the request input parameters are String propertyName"+propertyName+" and Object value:"+value);

		logger.info("finding CouponPin instance with property: "+ propertyName + ", value: " + value);
		try {
			final String queryString = "select model from CouponPin model where model."
					+ propertyName + "= :propertyValue";
			org.hibernate.Query query =getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<CouponType> couponTypes = query.list();
			logger.debug("Request goes out from the findByProperty method of the class CouponTypeDAO, the request return type is List<CouponType> :"+couponTypes);
			return couponTypes;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
    }

	public List<CouponType> findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List<CouponType> findByName(Object name) {
		return findByProperty(NAME, name);
	}

     protected Class<CouponType> getDomainClass() {
         return CouponType.class;  //To change body of implemented methods use File | Settings | File Templates.
     }

     /**
	 * Find all CouponType entities.
	 *
	 * @return List<CouponType> all CouponType entities
	 */
	@SuppressWarnings("unchecked")
	public List<CouponType> findAll() {

        return findAll();
    }

}