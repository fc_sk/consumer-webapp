package com.freecharge.app.domain.dao.jdbc;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.mappers.migration.BinOfferRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class BinOfferDAO {
	private static Logger logger = LoggingFactory.getLogger(BinOfferDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	private SimpleJdbcInsert insertBinoffer;
	
	private NamedParameterJdbcTemplate slaveJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertBinoffer = new SimpleJdbcInsert(dataSource).withTableName("binbased_offer").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}
	
	@Autowired
    public void setSlaveDataSource(DataSource dataSource) {
        this.slaveJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

	public BinOffer create(BinOffer binOffer) {
		try {

		    BeanPropertySqlParameterSource parameters = new BeanPropertySqlParameterSource(binOffer);
			parameters.registerSqlType("offer_type", Types.VARCHAR);

			Number number = this.insertBinoffer.executeAndReturnKey(parameters);

			binOffer.setId(number.intValue());

			return binOffer;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<BinOffer> getAll() {
		try {
			String queryString = "select * from binbased_offer";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<BinOffer> binOffers = this.slaveJdbcTemplate.query(
					queryString,
						paramMap,
						new BinOfferRowMapper());
			return binOffers;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<BinOffer> getAllActive(Integer id) {
		try {
			String queryString = "select * from binbased_offer where is_active = 1 and valid_from < now() and valid_upto > now() "; 
			if(id != null) {
				queryString += " and id = "+id ;
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<BinOffer> binOffers = this.slaveJdbcTemplate.query(
					queryString,
						paramMap,
						new BinOfferRowMapper());
			return binOffers;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<BinOffer> getAllActive(String bank) {
		try {
			String queryString = "select * from binbased_offer where is_active = 1 and valid_from < now() and valid_upto > now() and bank = :bank"; 
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("bank", bank);
			
			List<BinOffer> binOffers = this.slaveJdbcTemplate.query(
					queryString,
						paramMap,
						new BinOfferRowMapper());
			return binOffers;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public Map<String, BinOffer> getActiveOffers() {
		Map<String, BinOffer> offersMap = new HashMap<String, BinOffer>();
		List<BinOffer> offers = getAllActive((Integer)null);
		for(BinOffer binOffer : offers) {
			if (offersMap.get(binOffer.getOfferKey()) == null) {
				offersMap.put(binOffer.getOfferKey(), binOffer) ;
			}
		}
		return offersMap;
	}

	public List<BinOffer> getAllActive() {
		List<BinOffer> offers = getAllActive((Integer)null);
		return offers;
	}

	public BinOffer getBinOfferById(Integer id) {
		List<BinOffer> offers = getAllActive(id);
		if(offers != null && offers.size() > 0) {
			return offers.get(0);
		}
		return null;
	}
	
	public BinOffer getBinOffer(Integer id) {
		try {
			String queryString = "select * from binbased_offer where id = :id"; 
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", id);
			
			List<BinOffer> binOffers = this.slaveJdbcTemplate.query(
					queryString,
						paramMap,
						new BinOfferRowMapper());
			if(binOffers != null && binOffers.size() > 0) {
				return binOffers.get(0);
			}
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
		return null;
	}

	public List<BinOffer> getAllWithKey(String offerKey) {
		try {
			String queryString = "select * from binbased_offer where offer_key = :offer_key"; 
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("offer_key", offerKey);
			
			List<BinOffer> binOffers = this.slaveJdbcTemplate.query(
					queryString,
						paramMap,
						new BinOfferRowMapper());
			return binOffers;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public void deleteOffersOfkey(String offerKey) {
		try {
			String queryString = "delete from binbased_offer where offer_key = :offer_key"; 
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("offer_key", offerKey);
			
			this.jdbcTemplate.update(queryString, paramMap);
			
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
		
	}

	public void updateStatus(String offerKey, int status) {
		try {
			String queryString = "update binbased_offer set is_active = :is_active where offer_key = :offer_key"; 
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("is_active", status);
			paramMap.put("offer_key", offerKey);
			
			this.jdbcTemplate.update(queryString, paramMap);
			
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
		
	}
	
	public boolean update(BinOffer binOffer) {
		
		try {
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(binOffer);

			String sql = "update binbased_offer set bank = :bank, name = :name, image_url = :imageUrl, bin = :bin, " +
					"offer_key = :offerKey, offer_type = :offerType, discount_type = :discountType, discount = :discount, " +
					" min_recharge_amount = :minRechargeAmount, coupon_factor = :couponFactor,  description = :description," +
					" short_description = :shortDescription, datamap = :datamap, conditions = :conditions, is_active = :isActive," +
					" valid_from = :validFrom, valid_upto = :validUpto where id = :id";

			int status = this.jdbcTemplate.update(sql, parameters);

			if (status <= 0) {
				logger.error("No Rows affected on updating in_request for id = "
						+ binOffer.getId());
			}

			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
}