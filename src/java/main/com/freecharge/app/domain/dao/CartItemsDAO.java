package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * CartItems entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 * 
 * @see com.freecharge.app.domain.entity.CartItems
 * @author MyEclipse Persistence Tools
 */

public class CartItemsDAO extends CommonDao<CartItems> implements ICartItemsDAO {
	// property constants
	public static final String FK_CART = "cart";
	public static final String FK_CART_ID = "cart.cartId";
	public static final String FK_ITEM_ID = "fkItemId";
	public static final String ENTITY_ID = "entityId";
	public static final String FL_PRODUCT_MASTER_ID = "flProductMasterId";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String PRICE = "price";
	public static final String IS_DELETED = "isDeleted";
	public static final String LOOKUP_ID = "lookupId";
	private static Logger logger = LoggingFactory.getLogger(CartItemsDAO.class);
	
	@Autowired
	private CartDAO cartDAO;

	protected Class<CartItems> getDomainClass() {
		return CartItems.class; // To change body of implemented methods use
								// File | Settings | File Templates.
	}

	@SuppressWarnings("unchecked")
	public List<CartItems> findByProperty(String propertyName, final Object value) {
		logger.debug("Request comes into the findByProperty method of the class CartItemsDAO, the request input parameters is String propertyName:"+propertyName+" and Object value:"+value);
		logger.info("finding CartItems instance with property: " + propertyName + ", value: " + value);
		try {
			final String queryString = "select model from CartItems model where model." + propertyName + "= :propertyValue";
			org.hibernate.Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<CartItems> cartItems = query.list();
			logger.debug("Request goes out from the findByProperty method of the class CartItemsDAO, the request return type is List<AffiliateProfile> "+cartItems);
			return cartItems;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}
	
	public List<CartItems> findByFkCart(Cart cart) {
		return findByProperty(FK_CART, cart);
	}

    public List<CartItems> findByFkCartId(Object fkCartId) {
        return findByProperty(FK_CART_ID, fkCartId);
    }
    
    public Integer save(CartItems cartItems) {
    	logger.debug("Request comes into the save method of the class CartItemsDAO, the request input parameters is CartItems cartItems:"+cartItems);
        Session session =null;
        try{
            session = this.getSession();
            if(session != null){
                return (Integer)session.save(cartItems);
            }else{
                logger.error("Failed to fetch session, cartItems not saved " + cartItems.getCartItemsId() + " Coupon " +
                        " id " + cartItems.getFkItemId());
                return 0;
            }
        }catch(Exception e){
            logger.error("Exception occured while saving cart items with crosssell values..",e);
            return 0;
        }

    }

	@Override
	public boolean isFreeFundPresent(CartItems cartItems) {
		logger.debug("Request comes into the isFreeFundPresent method of the class CartItemsDAO, the request input parameters is CartItems cartItems:"+cartItems);
		
		boolean freeFundExists = false;

		String queryStr = "select model from CartItems model where model.cart=:cart and model.entityId=:entityId and model.isDeleted = "+false+"";
		org.hibernate.Query query = getSession().createQuery(queryStr);
		query.setParameter("cart", cartItems.getCart());
		query.setParameter("entityId", "FreeFund");
		
		List<Object> queryResult =  query.list();
		if(queryResult != null && !queryResult.isEmpty()) {
			freeFundExists = true;
		}
		
		logger.debug("Request goes out from the isFreeFundPresent method of the class CartItemsDAO, the request return type is boolean :"+freeFundExists);
		return freeFundExists;
	}	
	
	public Integer getCouponQuantity(Integer item_id, Integer cartId) {
		Cart cart = cartDAO.findByCartId(cartId);
		String sql = "select model.quantity from CartItems model where model.fkItemId=:item_id AND model.cart=:cart"; 
		org.hibernate.Query query = getSession().createQuery(sql);
		query.setParameter("cart", cart);
		query.setParameter("item_id", item_id);
		List<Integer> results = query.list();
		return (results == null || results.size() == 0)? 0: results.get(0);
	}
}
