package com.freecharge.app.domain.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.framework.logging.LoggingFactory;

public class UserBillPaymentHistoryReadDAO {
    private static Logger logger = LoggingFactory.getLogger(UserBillPaymentHistoryReadDAO.class);

    private NamedParameterJdbcTemplate jt;

    public void setDataSource(DataSource ds) {
        this.jt = new NamedParameterJdbcTemplate(ds);
    }

    private static final String SQL_BILL_PAYMENT_HISTORY_FORMAT = ""
            + "SELECT c.created_on, r.order_id, b.bill_amount, b.postpaid_phone_number, thp.product_type,"
            + " thp.operator_name, s.is_success"
            + "      FROM cart c"
            + "      JOIN order_id r            ON c.lookup_id = r.lookup_id"
            + "      JOIN txn_home_page thp     ON r.lookup_id = thp.lookup_id"
            + "      JOIN bill_txn_home_page b  ON thp.fk_bill_txn_home_page_id = b.id"
            + " LEFT JOIN bill_payment_status s ON r.order_id = s.affiliate_trans_id"
            + " WHERE c.fk_user_id = :user_id order by s.created_at desc LIMIT %d";

    public static class BillPaymentHistory extends AbstractDO {
        public final Date createdOn;
        public final String orderID;
        public final BigDecimal billAmount;
        public final String serviceNumber;
        public final String productType;
        public final String operatorName;
        public final Boolean isSuccess;
        public BillPaymentHistory(Date createdOn, String orderID, BigDecimal billAmount, String serviceNumber,
                String productType, String operatorName, Boolean isSuccess) {
            this.createdOn = createdOn;
            this.orderID = orderID;
            this.billAmount = billAmount;
            this.serviceNumber = serviceNumber;
            this.productType = productType;
            this.operatorName = operatorName;
            this.isSuccess = isSuccess;
        }
    }

    public List<BillPaymentHistory> findBillPaymentHistory(int userID, int rechargeHistoryLimit) {
        return jt.query(String.format(SQL_BILL_PAYMENT_HISTORY_FORMAT, rechargeHistoryLimit),
                Collections.singletonMap("user_id", userID),
                new RowMapper<BillPaymentHistory>() {
            @Override
            public BillPaymentHistory mapRow(ResultSet rs, int counter)
                    throws SQLException {
                Object isSuccess = rs.getObject("is_success");
                return new BillPaymentHistory(rs.getTimestamp("created_on"), rs.getString("order_id"),
                        rs.getBigDecimal("bill_amount"), rs.getString("postpaid_phone_number"),
                        rs.getString("product_type"), rs.getString("operator_name"),
                        isSuccess==null? null: (Boolean) isSuccess);
            }
                });
    }

}
