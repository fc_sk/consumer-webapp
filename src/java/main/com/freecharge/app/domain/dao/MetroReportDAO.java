package com.freecharge.app.domain.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.MetroReportDataEntity;
import com.freecharge.app.domain.entity.jdbc.mappers.MetroReportDataMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class MetroReportDAO {

	private NamedParameterJdbcTemplate jdbcTemplate;

	protected final Logger logger = LoggingFactory.getLogger(getClass()
			.getName());

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	
    
	public List<MetroReportDataEntity> searchMetroTransactionHistoryForDay(Date toDate, Date fromDate, Boolean paymentStatus, String orderId) {
		String toDateStr = null;
		String fromDateStr = null;
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer whereStringBuffer = new StringBuffer(" where p.order_id like 'FCZ%' ");
		String limitString = "";
		
		if (toDate != null){
			toDateStr = format1.format(toDate);	
			whereStringBuffer.append(" and p.sent_to_pg < '").append(toDateStr).append(" 23:59:59' ");
		}
		if (fromDate != null){
			fromDateStr = format1.format(fromDate);
			whereStringBuffer.append(" and p.sent_to_pg > '").append(fromDateStr).append("' ");
		}
		
		if (orderId != null){
			whereStringBuffer.append(" and p.order_id = '").append(orderId).append("' ");
		}
		
		/**
		 * Removed as there is no index on user_transaction_history.subscriber_identification_number (filtering to be handled in java code)
		 * 
		 * if (afcCardNo != null){
			whereStringBuffer.append(" and uth.subscriber_identification_number = '").append(afcCardNo).append("' ");
		}*/
		
		if (paymentStatus != null){
			int isSuccessful = 0;
			if (paymentStatus){
				isSuccessful = 1;
			}
			whereStringBuffer.append(" and p.is_successful = ").append(isSuccessful).append(" ");
		}
		
		String query = "SELECT p.order_id as orderId, uth.fk_user_id as userId, p.payment_type as paymentType, p.sent_to_pg as paymentDate, p.is_successful as paymentStatus, "
				+ "uth.created_on as rechargeDate, uth.transaction_amount as amount, uth.transaction_status as rechargeStatus, "
				+ "uth.subscriber_identification_number as rechargeNo  FROM payment_txn p  left join user_transaction_history uth "
				+ "on p.order_id = uth.order_id  " + whereStringBuffer.toString() + " order by p.order_id"+limitString;
		Map<String, Object> params = new HashMap<>();
		logger.info("Running this query on db: "+query);
        return jdbcTemplate.query(query, params,
                new MetroReportDataMapper());
    }

	public int getNoOfRowsOfMetroReportDataForDate(Date toDate, Date fromDate, Boolean paymentStatus, String orderId) {
		String toDateStr = null;
		String fromDateStr = null;
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer whereStringBuffer = new StringBuffer(" where p.order_id like 'FCZ%' ");


		if (toDate != null){
			toDateStr = format1.format(toDate);
			whereStringBuffer.append(" and p.sent_to_pg < '").append(toDateStr).append(" 23:59:59' ");
		}
		if (fromDate != null){
			fromDateStr = format1.format(fromDate);
			whereStringBuffer.append(" and p.sent_to_pg > '").append(fromDateStr).append("' ");
		}

		if (orderId != null){
			whereStringBuffer.append(" and p.order_id = '").append(orderId).append("' ");
		}

		/**
		 * Removed as there is no index on user_transaction_history.subscriber_identification_number (filtering to be handled in java code)
		 *
		 * if (afcCardNo != null){
		 whereStringBuffer.append(" and uth.subscriber_identification_number = '").append(afcCardNo).append("' ");
		 }*/

		if (paymentStatus != null){
			int isSuccessful = 0;
			if (paymentStatus){
				isSuccessful = 1;
			}
			whereStringBuffer.append(" and p.is_successful = ").append(isSuccessful).append(" ");
		}


		String query = "SELECT COUNT(p.order_id) FROM payment_txn p  left join user_transaction_history uth "
				+ "on p.order_id = uth.order_id  " + whereStringBuffer.toString();
		Map<String, Object> params = new HashMap<>();
		logger.info("Running this query on db: " + query);
		return jdbcTemplate.queryForInt(query, params);
	}
}
