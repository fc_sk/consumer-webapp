package com.freecharge.app.domain.dao;
    import java.util.List;

import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/3/12
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IForgotPasswordDAO {

    public List getPassword(BaseBusinessDO businessDO) throws Exception;

    public Boolean changePassword(BaseBusinessDO baseBusinessDO);

	public void updateUser(Users users);

	public List<Users> getUser(Integer userId, String encValue);
}
