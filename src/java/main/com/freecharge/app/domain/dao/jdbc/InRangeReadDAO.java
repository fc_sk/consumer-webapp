package com.freecharge.app.domain.dao.jdbc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.mappers.InRequestMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InResponseMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OrderIDMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * DAO that's customized for doing range queries against 
 * @author arun
 *
 */
public class InRangeReadDAO {
    private static Logger logger = LoggingFactory.getLogger(InRangeReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<InRequest> getInRequests(Date from, Date to) {
        String fromParam = "from";
        String toParam = "to";
        String query = String.format("select * from in_request where created_time >= :%s and created_time <= :%s", fromParam, toParam);

        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put(fromParam, from);
        params.put(toParam, to);

        List<InRequest> inRequests = this.jdbcTemplate.query(query, params, new InRequestMapper());

        return inRequests;
    }
    
    public List<InResponse> getInResponsesWithTimeRange(Date fromDate, Date toDate) {
        logger.info("[IN MODERATOR] Finding InResponse list from Date Rang : " + fromDate + " and to Date Rang :" + toDate, null);
        try {
            final String queryString = "select * from in_response where updated_time >= :fromDate and updated_time<= :toDate and in_resp_code in('08','-1')";
            SimpleDateFormat simDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("fromDate", simDateFormat.format(fromDate));
            paramMap.put("toDate", simDateFormat.format(toDate));
            // paramMap.put("errorCodes", "08");

            List<InResponse> inResponses = this.jdbcTemplate.query(queryString, paramMap, new InResponseMapper());
            logger.debug("Request goes out from the getInResponsesWithTimeRange method of the class InReadDAO, the request  return type is List<InResponse> " + inResponses.size());
            return inResponses;

        } catch (RuntimeException re) {
            logger.error("[IN MODERATOR] finding InResponse list from Date Range : " + fromDate + " and to Date Rang :" + toDate + " failed : ", re);
            throw re;
        }
    }

    public List<String> getPendingRechargesInTimeRange(Date fromDate, Date toDate) {
        logger.info("[IN MODERATOR] Finding getPendingRechargesInTimeRange list from Date Rang : " + fromDate + " and to Date Rang :" + toDate, null);
        final String queryString = "select distinct(rq.affiliate_trans_id) as order_id from in_request rq, in_response rs where rs.updated_time >= :fromDate and rs.updated_time<= :toDate and rs.in_resp_code = '08' and rs.request_id = rq.request_id";
        SimpleDateFormat simDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("fromDate", simDateFormat.format(fromDate));
        paramMap.put("toDate", simDateFormat.format(toDate));

        List<String> orderIdList = this.jdbcTemplate.query(queryString, paramMap, new OrderIDMapper());
        return orderIdList;
    }
    
    public List<String> getFailedRechargesInTimeRange(Date fromDate, Date toDate) {
        logger.info("[IN MODERATOR] Finding getFailedRechargesInTimeRange list from Date Rang : " + fromDate + " and to Date Rang :" + toDate, null);
        final String queryString = "select distinct(rq.affiliate_trans_id) as order_id from in_request rq, in_response rs where rs.updated_time >= :fromDate and rs.updated_time<= :toDate and rs.in_resp_code not in ('08', '00', '0', '-1') and rs.request_id = rq.request_id";
        SimpleDateFormat simDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("fromDate", simDateFormat.format(fromDate));
        paramMap.put("toDate", simDateFormat.format(toDate));

        List<String> orderIdList = this.jdbcTemplate.query(queryString, paramMap, new OrderIDMapper());
        return orderIdList;
    }
}
