package com.freecharge.app.domain.dao.jdbc;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.CrossSellHistory;
import com.freecharge.common.framework.logging.LoggingFactory;
@Component
public class CrossSellHistoryWriteDAO {
	private static Logger logger = LoggingFactory.getLogger(CrossSellHistoryWriteDAO.class);
	private SimpleJdbcInsert insertCrossSellHistory;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
        insertCrossSellHistory = new SimpleJdbcInsert(dataSource).withTableName("crosssell_optin_history").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	public boolean create(CrossSellHistory crossSellHistory) {
		try {
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(crossSellHistory);
			insertCrossSellHistory.execute(parameters);
			return true;
		}
		catch (RuntimeException re) {
			logger.error("Failed to save cross sell history.", re);
			throw re;
		}
	}
}
