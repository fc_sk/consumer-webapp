package com.freecharge.app.domain.dao;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.freecharge.app.domain.entity.TxnCrossSell;
import com.freecharge.common.framework.logging.LoggingFactory;

public class TxnCrossSellDAO extends CommonDao<TxnCrossSell> implements ITxnCrossSellDAO {
	// property constants
	public static final String TXN_HOME_PAGE_ID = "txnHomePageId";
	public static final String COUPON_ID = "couponId";
	public static final String CORSS_SELL_ID = "corssSellId";
	public static final String IS_DELETED = "isDeleted";

	private static Logger logger = LoggingFactory.getLogger(TxnCrossSellDAO.class);

	protected Class<TxnCrossSell> getDomainClass() {
		return TxnCrossSell.class;
	}

	/**
	 * Find all TxnCrossSell entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the TxnCrossSell property to query
	 * @param value
	 *            the property value to match
	 * @return List<TxnCrossSell> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<TxnCrossSell> findByProperty(String propertyName, final Object value) {
		logger.debug("Request comes into the findByProperty method of the class TxnCrossSellDAO, the request input parameter type is Integer "+propertyName+" Object"+ value);
		logger.info("finding TxnCrossSell instance with property: " + propertyName + ", value: " + value, null);
		try {
			final String queryString = "select model from TxnCrossSell model where model." + propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<TxnCrossSell> list=query.list();
			logger.debug("Request goes out from the findByProperty method of the class TxnCrossSellDAO, the request  return type is List<TxnCrossSell> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<TxnCrossSell> findByTxnHomePageId(Object txnHomePageId) {
		return findByProperty(TXN_HOME_PAGE_ID, txnHomePageId);
	}

	public List<TxnCrossSell> findByCouponId(Object couponId) {
		return findByProperty(COUPON_ID, couponId);
	}

	public List<TxnCrossSell> findByCorssSellId(Object corssSellId) {
		return findByProperty(CORSS_SELL_ID, corssSellId);
	}

	public List<TxnCrossSell> findByIsDeleted(Object isDeleted) {
		return findByProperty(IS_DELETED, isDeleted);
	}

	/**
	 * Find all TxnCrossSell entities.
	 * 
	 * @return List<TxnCrossSell> all TxnCrossSell entities
	 */
	@SuppressWarnings("unchecked")
	public List<TxnCrossSell> findAll() {
		logger.debug("Request comes into the findAll method of the class TxnCrossSellDAO, the request has no input parameters");
		logger.info("finding all TxnCrossSell instances", null);
		try {
			final String queryString = "select model from TxnCrossSell model";
			Query query = getSession().createQuery(queryString);
			List<TxnCrossSell> list=query.list();
			logger.debug("Request goes out from the findAll method of the class TxnCrossSellDAO, the request  return type is List<TxnCrossSell> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}
	}

	public Integer save(TxnCrossSell txnCrossSell) {
		logger.debug("Request comes into the save method of the class TxnCrossSellDAO, the request input parameter is TxnCrossSell"+txnCrossSell);
		Session session = null;
		try {
			session = this.getSession();
			if (session != null) {
				Integer id=(Integer) session.save(txnCrossSell);
				logger.debug("Request goes out from the save method of the class TxnCrossSellDAO, the request  return type is Integer "+id);
				return id;
			} else {
				logger.debug("Request goes out from the save method of the class TxnCrossSellDAO, the request  return type is Integer "+0);
				throw new IllegalStateException("Could not save txnCrossSell because hibernate session was null");
			}
		} catch (Exception e) {
			logger.error("Exception occured while saving txncrosssell with crosssell values..", e);
			throw e;
		}
	}

	public Boolean updateTxnCrosssell(TxnCrossSell txnCrossSell) {
		logger.debug("Request comes into the updateTxnCrosssell method of the class TxnCrossSellDAO, the request input parameter is TxnCrossSell"+txnCrossSell);
		Session session = null;
		try {
			session = getSession();
			session.update(txnCrossSell);
			logger.debug("Request goes out from the updateTxnCrosssell method of the class TxnCrossSellDAO, the request  return type is boolean "+true);
			return true;

		} catch (Exception e) {
			logger.error("Exception occured while updating txncrosssell with crosssell values..", e);
			return false;
		}
	}

	public void save(List<TxnCrossSell> txnCrossSellList) {
		logger.debug("Request comes into the save method of the class TxnCrossSellDAO, the request input parameter is List<TxnCrossSell>"+txnCrossSellList);
		Session session = null;
		try {
			session = this.getSession();
			if (session != null) {
				Iterator<TxnCrossSell> ite = txnCrossSellList.iterator();
				while (ite.hasNext()) {
					TxnCrossSell txnCrossSell = ite.next();
					session.save(txnCrossSell);
				}
			}
			logger.debug("Request goes out from the save method of the class TxnCrossSellDAO, the request  return type is void ");
		} catch (Exception e) {
			logger.error("Exception occured while saving txncrosssell with crosssell values..", e);
		}
	}

	public void markIsDeleted(Integer homePageId) {
		logger.debug("Request comes into the markIsDeleted method of the class TxnCrossSellDAO, the request input parameter is Integer"+homePageId);
		logger.info("Marking all the coupons as deleted in txn_crosssell table");
		try {
			final String queryString = String.format("update TxnCrossSell set isDeleted=1 where txnHomePageId=%d and couponId != 0", homePageId);
			Query query = getSession().createQuery(queryString);
			query.executeUpdate();
			logger.debug("Request goes out from the markIsDeleted method of the class TxnCrossSellDAO, the request  return type is void ");
		} catch (RuntimeException re) {
			logger.info("exception raised while marking as isdeleted true");
		}
	}

	public List<TxnCrossSell> getActiveItemByTxnHomePageIdAndCouponId(Integer txnHomePageId, Integer couponId) {

		logger.debug("Request comes into the getActiveItemByTxnHomePageIdAndCouponId method of the class TxnCrossSellDAO, the request input parameters is Integer txnHomePageId:"+txnHomePageId.toString()+" and Integer couponId:"+couponId.toString());

		try {
			final String queryString = "select model from TxnCrossSell model where model.txnHomePageId=:txnHomePageId and model.couponId=:couponId and model.isDeleted = "+false;
			org.hibernate.Query query = getSession().createQuery(queryString);
			query.setParameter("txnHomePageId", txnHomePageId);
			query.setParameter("couponId", couponId);
			List<TxnCrossSell> txnCrossSellItems = query.list();
			logger.debug("Request goes out from the getCartItemByCartIdAndFkItemId method of the class TxnCrossSellDAO, the request return type is List<TxnCrossSell> "+txnCrossSellItems);
			return txnCrossSellItems;
		} catch (RuntimeException re) {
			logger.error("getActiveItemByTxnHomePageIdAndCouponId failed", re);
			throw re;
		}
	}
	
    public List<TxnCrossSell> getActiveCrossSellByTxnHomePageIdAndcrossSellId(Integer txnHomePageId, Integer crossSellId) {

        logger.debug("Request comes into the getActiveCrossSellByTxnHomePageIdAndCouponId method of the class TxnCrossSellDAO, the request input parameters is Integer txnHomePageId:"+txnHomePageId.toString()+" and Integer crossSellId:"+crossSellId.toString());

        try {
            final String queryString = "select model from TxnCrossSell model where model.txnHomePageId=:txnHomePageId and model.corssSellId=:corssSellId and model.isDeleted = "+false;
            org.hibernate.Query query = getSession().createQuery(queryString);
            query.setParameter("txnHomePageId", txnHomePageId);
            query.setParameter("corssSellId", crossSellId);
            List<TxnCrossSell> txnCrossSellItems = query.list();
            logger.debug("Request goes out from the getCartItemByCartIdAndFkItemId method of the class TxnCrossSellDAO, the request return type is List<TxnCrossSell> "+txnCrossSellItems);
            return txnCrossSellItems;
        } catch (RuntimeException re) {
            logger.error("getActiveItemByTxnHomePageIdAndCouponId failed", re);
            throw re;
        }
    }
	
}