package com.freecharge.app.domain.dao;

import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: Apr 30, 2012
 * Time: 10:37:15 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IHomeBusinessDao {
    public Users getAuthenticateUser(String email, String password);

    public List getProfileInfo(BaseBusinessDO baseBusinessDO);

    public void updateProfile(List userAndProfile,boolean isupdatepass);
    
    public boolean checkUserPassword(BaseBusinessDO baseBusinessDO);
    
    public void updateAddress(List list);

    public List<List<Object>> getMycontacts(Integer userid);
    
    public List getAllProducts();

    public Integer changePassword(BaseBusinessDO baseBusinessDO);

    public List<Users> validateUser(BaseBusinessDO baseBusinessDO);

    public Map getUserDetailsFromOrderId(String orderId);
    
    public List<List<Object>> getInvoiceInfoObjects(String orderId);
    
    
    
    
    
}
