package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.common.framework.logging.LoggingFactory;

public class OperatorCircleWriteDAO {
    
    private static Logger logger = LoggingFactory.getLogger(OperatorCircleWriteDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static String commaSeparatedString(List<?> list) {
        StringBuilder sb = new StringBuilder();
        for (Object each: list) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(each);
        }
        return sb.toString();
    }
    
    public void setInOprAggrPriority(List<Integer> circleOperatorMappingIDs, String priority, String username) {
        try {
            logger.debug("Request comes into the setInOprAggrPriority method of the class OperatorCircleWriteDAO, the" +
                    " request input parameters is List<Integer> "+circleOperatorMappingIDs);
            String queryString = "UPDATE in_oppr_aggr_priority SET aggr_priority = :priority, update_time = NOW(), " +
                    "updated_by = :username WHERE fk_circle_operator_mapping_id IN (" +
            commaSeparatedString(circleOperatorMappingIDs) + ")";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("priority", priority);
            paramMap.put("username", username);

            int rows = jdbcTemplate.update(queryString, paramMap);
            logger.debug("Request goes out from the setCircleOperatorMapping method of the class" +
                    " OperatorCircleWriteDAO; number of rows updated: "+rows);
        } catch (RuntimeException re) {
            logger.error("setInOprAggrPriority failed", re);
            throw re;
        }
    }
    
    private static final String SQL_BATCH_UPDATE_AG_PRIORITY = "UPDATE in_oppr_aggr_priority SET aggr_priority = :aggr_priority, update_time = NOW(), " +
                    "updated_by = :username WHERE fk_circle_operator_mapping_id = :fk_circle_operator_mapping_id";

    /**
     * The only argument is a list of maps such that each map contains
     * "active" (boolean), "prefix" (String) and "comid" (integer) values
     * @param prefixes
     * @return
     */
    public int[] batchUpdateAGPriority(List<CircleOperatorMapping> circleOperatorMappings, String username, String defaultAggregator) {
        List<Map<String, Object>> batchValues = new ArrayList<Map<String, Object>>();
        for (CircleOperatorMapping circleOperatorMapping :circleOperatorMappings) {
            String aggregator = null;
            if(defaultAggregator != null && ("euronet".equals(defaultAggregator) || "oxigen".equals(defaultAggregator))) {
                aggregator = defaultAggregator;
            } else {
                aggregator = circleOperatorMapping.getAggrPriority();
            }
            if(aggregator != null) {
                Map<String, Object> paramMap = new HashMap<String, Object>();
                paramMap.put("username", username);
                paramMap.put("fk_circle_operator_mapping_id", circleOperatorMapping.getCircleOperatorMappingId());
                paramMap.put("aggr_priority", aggregator);
                batchValues.add(paramMap);
            }
        }
        return jdbcTemplate.batchUpdate(SQL_BATCH_UPDATE_AG_PRIORITY, batchValues.toArray((HashMap<String, Object>[]) new HashMap[0]));
    }

    private static final String SQL_UPDATE_PREFIXES = "UPDATE site_autosuggest_prefix SET is_active = :active, " +
            "fk_circle_operator_mapping_id = :comid WHERE prefix = :prefix and fk_product_master_id= :productMasterId";

    /**
     * The only argument is a list of maps such that each map contains
     * "active" (boolean), "prefix" (String) and "comid" (integer) values
     * @param prefixes
     * @return
     */
    public int[] updatePrefix(List<Map<String, Object>> prefixes) {
        @SuppressWarnings("unchecked")
        Map<String, ?>[] batchParams = prefixes.toArray((HashMap<String, ?>[]) new HashMap[0]);
        return jdbcTemplate.batchUpdate(SQL_UPDATE_PREFIXES, prefixes.toArray(batchParams));
    }

    private static final String SQL_INSERT_PREFIXES = "INSERT INTO site_autosuggest_prefix (prefix, " +
            "fk_circle_operator_mapping_id, fk_product_master_id, is_active) VALUES " +
            "(:prefix, :comid, :productMasterId, :active)";

    public int[] insertPrefix(List<Map<String, Object>> prefixes) {
        @SuppressWarnings("unchecked")
        Map<String, ?>[] batchParams = prefixes.toArray((HashMap<String, ?>[]) new HashMap[0]);
        return jdbcTemplate.batchUpdate(SQL_INSERT_PREFIXES, prefixes.toArray(batchParams));
    }
    
    public void insertOperatorCircleBlock(int operatorId, int circleId, Boolean isBlocked, String userEmail){
    	String SQL_INSERT_OPERATOR_CIRCLE_BLOCK = "INSERT INTO operator_circle_block(operator_id, circle_id, is_blocked, user_email) VALUES(:operatorId, :circleId, :isBlocked, :userEmail)";
    	Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("operatorId", operatorId);
        paramMap.put("circleId", circleId);
        paramMap.put("isBlocked", isBlocked);
        paramMap.put("userEmail", userEmail);
        jdbcTemplate.update(SQL_INSERT_OPERATOR_CIRCLE_BLOCK, paramMap);
    	
    }

    public void deleteOperatorCircleBlock(int operatorId, int circleId){
    	String DELETE_OPERATOR_CIRCLE_BLOCK = "DELETE FROM operator_circle_block where operator_id = :operatorId and circle_id = :circleId";
    	Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("operatorId", operatorId);
        paramMap.put("circleId", circleId);
        jdbcTemplate.update(DELETE_OPERATOR_CIRCLE_BLOCK, paramMap);
    }
    
    public void updateOperatorCircleBlock(int operatorId, int circleId, Boolean isBlocked, String userEmail){
    	String UPDATE_OPERATOR_CIRCLE_BLOCK = "UPDATE operator_circle_block set is_blocked = :isBlocked, user_email = :userEmail where operator_id = :operatorId and circle_id = :circleId";
    	Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("operatorId", operatorId);
        paramMap.put("circleId", circleId);
        paramMap.put("isBlocked", isBlocked);
        paramMap.put("userEmail", userEmail);
        jdbcTemplate.update(UPDATE_OPERATOR_CIRCLE_BLOCK, paramMap);
    }
    
}
