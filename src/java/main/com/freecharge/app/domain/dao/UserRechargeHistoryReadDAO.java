package com.freecharge.app.domain.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.DateTime;

import com.freecharge.app.domain.entity.UserRechargeHistory;

public class UserRechargeHistoryReadDAO extends CommonDao<UserRechargeHistory> {
    @Override
    protected Class<UserRechargeHistory> getDomainClass() {
        return UserRechargeHistory.class;
    }

    public int getRechargeHistoryCountSince(int userId, DateTime since) {
        Session session = getSession();
        String sqlquery = "select count(1) as rcount from user_recharge_history where fk_users_id = :userId and updated_on >= :fromDate";
        Query query = session.createSQLQuery(sqlquery);
        query.setParameter("userId", userId);
        query.setParameter("fromDate", new Timestamp(since.getMillis()));
        List resultList = query.list();
        return ((BigInteger) resultList.get(0)).intValue();
    }
}
