package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;

/**
 * Interface for CartDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ICartDAO {

	public Cart findByLookupId(String lookupId);

	public Cart findByCartId(Integer cartId);

	public Integer saveCart(Cart cart);

	public void saveCartItems(List<CartItems> cartItems,Integer cart_Id);

	public void deleteCartItems(Integer cartId);
	
	public List getCartByLookUpId(String lookupId);
}