package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.UserRechargeContact;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.domain.entity.jdbc.mappers.NUserProfileMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.UserProfileMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.UsersMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class UserWriteDao {

	private static Logger logger = LoggingFactory.getLogger(UserWriteDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertUsers;
	private SimpleJdbcInsert insertUserProfile;
	private SimpleJdbcInsert insertUserRechargeContactWithId;
	private SimpleJdbcInsert insertUserRechargeContactNewWithId;
	private SimpleJdbcInsert insertUserRechargeContact;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertUsers = new SimpleJdbcInsert(dataSource).withTableName("users").usingGeneratedKeyColumns("user_id", "n_last_updated", "n_created");
        this.insertUserProfile = new SimpleJdbcInsert(dataSource).withTableName("user_profile").usingGeneratedKeyColumns("user_profile_id", "n_last_updated", "n_created");
		this.insertUserRechargeContactNewWithId = new SimpleJdbcInsert(dataSource).withTableName("user_recharge_contact_new").usingGeneratedKeyColumns("n_last_updated", "n_created");
	}

	public Users insertUser(Users users) {
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(users);
        Number userId = this.insertUsers.executeAndReturnKey(parameters);
        users.setUserId(userId.intValue());
        return users;
	}

	public Users updateUser(Users users) {
		try {
			logger.debug("Request comes into the updateUser method of the class UserWriteDao, the request input parameter type Users "+users);
			String sql = "update users set fk_affiliate_profile_id= :fkAffiliateProfileId, type = type, email = :email, password = :password, mobile_no = :mobileNo, dob = :dob, is_active = :isActive, " + "date_added = :dateAdded, last_loggedin = :lastLoggedin, is_loggedin = :isLoggedin, morf = :morf, last_update = :lastUpdate, forgot_password_key = :forgotPasswordKey, forgot_password_expiry = :forgotPasswordExpiry, fk_refferred_by_user_id = :fkRefferredByUserId " + "where user_id = :userId";

			SqlParameterSource parameters = new BeanPropertySqlParameterSource(users);
			int returnParam = this.jdbcTemplate.update(sql, parameters);
			logger.debug("Request goes out from the updateUser method of the class UserWriteDao, the request  return type is Users "+users);
			return users;
		} catch (RuntimeException re) {
			logger.error("updateUser(Users users) failed", re);
			logger.error("updateUser(Users users) failed for user id: " + users.getUserId());
			throw re;
		}
	}

	public Users updateUserWithoutPassword(Users users) {
		try {
			logger.debug("Request comes into the updateUserWithoutPassword method of the class UserWriteDao, the request input parameter type Users "+users);
			String sql = "update users set fk_affiliate_profile_id= :fkAffiliateProfileId, type = type, email = :email, mobile_no = :mobileNo, dob = :dob, is_active = :isActive, " + "date_added = :dateAdded, last_loggedin = :lastLoggedin, is_loggedin = :isLoggedin, morf = :morf, last_update = :lastUpdate, forgot_password_key = :forgotPasswordKey, forgot_password_expiry = :forgotPasswordExpiry, fk_refferred_by_user_id = :fkRefferredByUserId " + "where user_id = :userId";

			SqlParameterSource parameters = new BeanPropertySqlParameterSource(users);
			int returnParam = this.jdbcTemplate.update(sql, parameters);
			logger.debug("Request goes out from the updateUserWithoutPassword method of the class UserWriteDao, the request  return type is Users "+users);
			return users;
		} catch (RuntimeException re) {
			logger.error("updateUserWithoutPassword(Users users) failed", re);
			logger.error("updateUserWithoutPassword(Users users) failed for user id: " + users.getUserId());
			throw re;
		}
	}

	public int[] insertUsers(List<Users> users) {
		try {
			logger.debug("Request comes into the insertUsers method of the class UserWriteDao, the request input parameter type List<Users> "+users);
			String queryString = "insert into users (user_id, fk_affiliate_profile_id, type, email, password, mobile_no, dob, is_active, date_added, last_loggenin, is_loggedin, morf, last_update, forgot_password_key, forgot_password_expiry, fk_refferred_bu_user_id) " + "values (:userId, :fkAffiliateProfileId, :type, :email, :password, :mobileNo, :dob, :isActive, :dateAdded, :lastLoggedin, :isLoggedin, :morf, :lastUpdate, :forgotPasswordKey, :forgotPasswordExpiry, :fkRefferredByUserId)";

			SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(users.toArray());

			int[] updateCounts = this.jdbcTemplate.batchUpdate(queryString, batch);
			logger.debug("Request goes out from the updateUserWithoutPassword method of the class UserWriteDao, the request  return type is int[] "+updateCounts);
			return updateCounts;
		} catch (RuntimeException re) {
			logger.error("insertUsers(List<Users> users) failed", re);
			throw re;
		}
	}

	public UserProfile insertUserProfile(UserProfile userProfile) {
		try {
			logger.debug("Request comes into the insertUserProfile method of the class UserWriteDao, the request input parameter type UserProfile "+userProfile);
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(userProfile);
			Number userProfileId = this.insertUserProfile.executeAndReturnKey(parameters);
			userProfile.setUserProfileId(userProfileId.intValue());
			logger.debug("Request goes out from the updateUserWithoutPassword method of the class UserWriteDao, the request  return type is UserProfile "+userProfile);
			return userProfile;
		} catch (RuntimeException re) {
			logger.error("insertUserProfile(UserProfile userProfile) failed", re);
			logger.error("insertUserProfile(UserProfile userProfile) failed for user id: " + userProfile.getFkUserId());
			throw re;
		}
	}

	public UserProfile updateUserProfile(UserProfile userProfile) {
		try {
			logger.debug("Request comes into the updateUserProfile method of the class UserWriteDao, the request input parameter type UserProfile "+userProfile);
			String sql = "update user_profile set user_profile_id = :userProfileId, fk_user_id = :fkUserId, fk_user_recharge_contact_id = :fkUserRechargeContactId, nick_alias = :nickAlias, address1 = :address1, landmark = :landmark, city = :city, postal_code = :postalCode, "
					+ "fk_state_master_id = :fkStateMasterId, fk_country_master_id = :fkCountryMasterId, is_active = :isActive, is_default = :isDefault, created_on = :createdOn, last_update = :lastUpdate, first_name = :firstName, last_name = :lastName, title = :title, area = :area, street = :street " + "where fk_user_id = :fkUserId";

			SqlParameterSource parameters = new BeanPropertySqlParameterSource(userProfile);
			int returnParam = this.jdbcTemplate.update(sql, parameters);
			logger.debug("Request goes out from the updateUserProfile method of the class UserWriteDao, the request  return type is UserProfile "+userProfile);
			return userProfile;
		} catch (RuntimeException re) {
			logger.error("updateUserProfile(UserProfile userProfile) failed", re);
			logger.error("updateUserProfile(UserProfile userProfile) failed for user id: " + userProfile.getFkUserId());
			throw re;
		}
	}

	public UserProfile updateDefaultUserProfile(UserProfile userProfile) {
		try {
			logger.debug("Request comes into the updateDefaultUserProfile method of the class UserWriteDao, the request input parameter type UserProfile "+userProfile);
			String sql = "update user_profile set user_profile_id = :userProfileId, fk_user_id = :fkUserId, fk_user_recharge_contact_id = :fkUserRechargeContactId, nick_alias = :nickAlias, address1 = :address1, landmark = :landmark, city = :city, postal_code = :postalCode, "
					+ "fk_state_master_id = :fkStateMasterId, fk_country_master_id = :fkCountryMasterId, is_active = :isActive, is_default = :isDefault, created_on = :createdOn, last_update = :lastUpdate, first_name = :firstName, last_name = :lastName, title = :title, area = :area, street = :street " 
					+ "where fk_user_id = :fkUserId and is_default = 1";

			SqlParameterSource parameters = new BeanPropertySqlParameterSource(userProfile);
			int returnParam = this.jdbcTemplate.update(sql, parameters);
			logger.debug("Request goes out from the updateDefaultUserProfile method of the class UserWriteDao, the request  return type is UserProfile "+userProfile);
			return userProfile;
		} catch (RuntimeException re) {
			logger.error("updateDefaultUserProfile(UserProfile userProfile) failed", re);
			logger.error("updateDefaultUserProfile(UserProfile userProfile) failed for user id: " + userProfile.getFkUserId());
			throw re;
		}
	}

	
	public int[] insertUserProfiles(List<UserProfile> userProfiles) {
		try {
			logger.debug("Request comes into the insertUserProfiles method of the class UserWriteDao, the request input parameter type List<UserProfile> "+userProfiles);
			String queryString = "insert into user_profile (user_profile_id, fk_user_id, fk_user_recharge_contact_id, nick_alias, address1, landmark, city, postal_code, fk_state_master_id, fk_country_master_id, is_active, is_default, created_on, last_update, first_name, last_name, title, area, street) "
					+ "values (userProfileId, :fkUserId, :fkUserRechargeContactId, :nickAlias, :address1, :landmark, :city, :postalCode, :fkStateMasterId, :fkCountryMasterId, :isActive, :isDefault, :createdOn, :lastUpdate, :firstName, :lastName, :title, :area, :street)";

			SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(userProfiles.toArray());

			int[] updateCounts = this.jdbcTemplate.batchUpdate(queryString, batch);
			logger.debug("Request comes into the insertUserProfiles method of the class UserWriteDao, the request input parameter type int[] "+updateCounts);
			return updateCounts;
		} catch (RuntimeException re) {
			logger.error("insertUserProfiles(List<UserProfile> userProfiles) failed", re);
			throw re;
		}
	}
	
	public UserRechargeContact insertUserRechargeContactWithId(UserRechargeContact contact) {
		try {
			logger.debug("Request comes into the insertUserRechargeContactWithId method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(contact);
			int returnParam = this.insertUserRechargeContactWithId.execute(parameters);
			logger.debug("Request comes into the insertUserRechargeContactWithId method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			return contact;
		} catch (RuntimeException re) {
			logger.error("insertUserRechargeContactWithId(UserRechargeContact contact) failed", re);
			logger.error("insertUserRechargeContactWithId(UserRechargeContact contact) failed for  id: " + contact.getUserRechargeContactId());
			throw re;
		}
	}

	public UserRechargeContact insertUserRechargeContactNewWithId(UserRechargeContact contact) {
		try {
			logger.debug("Request comes into the insertUserRechargeContactNewWithId method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(contact);
			int returnParam = this.insertUserRechargeContactNewWithId.execute(parameters);
			logger.debug("Request comes into the insertUserRechargeContactNewWithId method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			return contact;
		} catch (RuntimeException re) {
			logger.error("insertUserRechargeContactWithId(UserRechargeContact contact) failed", re);
			logger.error("insertUserRechargeContactWithId(UserRechargeContact contact) failed for  id: " + contact.getUserRechargeContactId());
			throw re;
		}
	}

	
	public UserRechargeContact insertUserRechargeContact(UserRechargeContact contact) {
		try {
			logger.debug("Request comes into the insertUserRechargeContact method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(contact);
			Number returnParam = this.insertUserRechargeContact.executeAndReturnKey(parameters);
			
			contact.setUserRechargeContactId(returnParam.intValue());
			logger.debug("Request comes into the UserRechargeContact method of the class UserWriteDao, the request input parameter type UserRechargeContact "+contact);
			return contact;
		} catch (RuntimeException re) {
			logger.error("insertUserRechargeContact(UserRechargeContact contact)  failed", re);
			logger.error("insertUserRechargeContact(UserRechargeContact contact)  failed for id: " + contact.getUserRechargeContactId());
			throw re;
		}
	}
	
	//Update profile number
	public void updateUserByProfileNumber(Integer userId, String alertMobileNo) {
			String sqlQuery = "update users set mobile_no=:alertMobileNo where user_id=:userId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", userId);
			paramMap.put("alertMobileNo", alertMobileNo);
			int updatedRows = this.jdbcTemplate.update(sqlQuery, paramMap);
	}
	
	/*UserReadDao realted functions begins from here*/
	
	public UserProfile getUserProfile(Integer userId) {
        try {
            logger.debug("Request comes into the getUserProfile method of the class userWriteDao, the request input parameters Integer "+userId);
            String queryString = "select * from user_profile where fk_user_id = :fkUserId";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("fkUserId", userId);
            
            List<UserProfile> userProfiles = this.jdbcTemplate.query(queryString, paramMap, new UserProfileMapper());
            logger.debug("Request goes out from the getUserProfile method of the class userWriteDao, the request  return type is UserProfile "+userProfiles);
            if(userProfiles != null && userProfiles.size() > 0)
                return userProfiles.get(0);
            else
                return null;
        } catch (RuntimeException re) {
            logger.error("getUserProfile(Integer userId) failed", re);
            throw re;
        }
    }

    public UserProfile getDefaultUserProfile(Integer userId) {
        try {
            logger.debug("Request comes into the getDefaultUserProfile method of the class userWriteDao, the request input parameters Integer "+userId);
            String queryString = "select * from user_profile where fk_user_id = :fkUserId and is_default = 1";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("fkUserId", userId);
            
            List<UserProfile> userProfiles = this.jdbcTemplate.query(queryString, paramMap, new UserProfileMapper());
            logger.debug("Request goes out from the getDefaultUserProfile method of the class userWriteDao, the request  return type is UserProfile "+userProfiles);
            if(userProfiles != null && userProfiles.size() > 0)
                return userProfiles.get(0);
            else
                return null;
        } catch (RuntimeException re) {
            logger.error("getDefaultUserProfile(Integer userId) failed", re);
            throw re;
        }
    }

    public Users getUserForUpdate(String emailId) {
        try {
            logger.debug("Input emailId: " + emailId);
            String queryString = "select * from users where email = :emailId for UPDATE";
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("emailId", emailId);
            
            List<Users> users = this.jdbcTemplate.query(queryString, paramMap, new UsersMapper());
            logger.debug("Request goes out from the getUserForUpdate method of the class userWriteDao, the request  return type is Users "+users);
            if(users != null && users.size() > 0)
                return users.get(0);
            else
                return null;
        } catch (RuntimeException re) {
            logger.error("getUserForUpdate() failed for emailId = " + emailId, re);
            throw re;
        }
    }
    
    public int updateUserEmailId(Integer userId, String emailId) {
        int updatedRows = 0;
            String sqlQuery = "update users set email=:emailId where user_id=:userId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("userId", userId);
            paramMap.put("emailId", emailId);
            updatedRows = this.jdbcTemplate.update(sqlQuery, paramMap);
        return updatedRows;
    }
}



