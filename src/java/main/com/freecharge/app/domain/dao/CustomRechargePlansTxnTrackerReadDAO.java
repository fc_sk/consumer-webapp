package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;
import com.freecharge.app.domain.entity.jdbc.mappers.CustomRechargePlanTxnTrackerRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class CustomRechargePlansTxnTrackerReadDAO implements ICustomRechargePlansTxnTrackerReadDAO {
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(CustomRechargePlansTxnTrackerReadDAO.class);
	
	@Autowired
	@Qualifier("jdbcReadDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public CustomRechargePlanTxnTracker findByLookupId(String lookupId) {
		String selectQuery = "select * from custom_recharge_plans_txn_track where "
				+ CustomRechargePlanTxnTrackerRowMapper.ColumnName.LOOKUP_ID.getValue()
				+ " = "
				+":lookupId";
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("lookupId", lookupId);
		List<CustomRechargePlanTxnTracker> customRechargePlanTxnTrackers = this.jdbcTemplate.query(selectQuery, paramMap, new CustomRechargePlanTxnTrackerRowMapper());
		if(customRechargePlanTxnTrackers != null && !customRechargePlanTxnTrackers.isEmpty()) {
			logger.info(String.format("Total %d entry found for lookup id : %s .Returning 1st entry.", customRechargePlanTxnTrackers.size(), lookupId));
			return customRechargePlanTxnTrackers.get(0);
		}
		return null;
	}

}
