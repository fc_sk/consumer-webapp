package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;
import com.freecharge.app.domain.entity.jdbc.mappers.CustomRechargePlanTxnTrackerRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class CustomRechargePlansTxnTrackerWriteDAO implements ICustomRechargePlansTxnTrackerWriteDAO {
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(CustomRechargePlansTxnTrackerWriteDAO.class);
	
	@Autowired
	@Qualifier("dataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Override
	public void savePlanDetails(CustomRechargePlanTxnTracker customRechargePlanTxnTracker) {
		try {
			String insertQuery = "insert into custom_recharge_plans_txn_track "
					+ "("
					+ CustomRechargePlanTxnTrackerRowMapper.ColumnName.LOOKUP_ID.getValue() + ","
					+ CustomRechargePlanTxnTrackerRowMapper.ColumnName.FULFILLMENT_IDF.getValue() + ","
					+ CustomRechargePlanTxnTrackerRowMapper.ColumnName.AMOUNT.getValue()
					+ ")"
					+ " values (:lookupId, :fulfillmentIdf, :amount)";
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("lookupId", customRechargePlanTxnTracker.getLookupId());
			paramMap.put("fulfillmentIdf", customRechargePlanTxnTracker.getFulfillmentIdf());
			paramMap.put("amount", customRechargePlanTxnTracker.getAmount());
			int rows = this.jdbcTemplate.update(insertQuery, paramMap);
			logger.info(String.format("Inserted %d row with lookup id: %s, fulfillmentIdf : %s, amount : %f", rows, customRechargePlanTxnTracker.getLookupId(), customRechargePlanTxnTracker.getFulfillmentIdf(), customRechargePlanTxnTracker.getAmount()));
		}catch (Exception e) {
			logger.error("Unable to insert into custom_recharge_plans_txn_track because of exception : ", e);
			throw e;
		}
	}

}
