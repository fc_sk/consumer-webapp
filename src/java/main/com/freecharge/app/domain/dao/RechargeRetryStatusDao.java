package com.freecharge.app.domain.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.RechargeRetryStatus;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class RechargeRetryStatusDao extends BaseDao implements IRechargeRetryStatusDao{

	private static Logger logger = LoggingFactory.getLogger(RechargeRetryStatusDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private SimpleJdbcInsert insertRechargeRetryStatus;
	
	@Autowired
	@Qualifier("dataSource")
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        insertRechargeRetryStatus = new SimpleJdbcInsert(dataSource).withTableName("recharge_retry_status").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	@Override
	public RechargeRetryStatus getRechargeRetryStatus(String orderId) {
		logger.info("Getting all recharge_retry_status");
		RechargeRetryStatus rechargeRetryStatus = null; 
		List<RechargeRetryStatus> rechargeRetryStatusList = null;
		try {
			
			String queryString = "select * from recharge_retry_status where order_id = :orderId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);
			rechargeRetryStatusList = jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(RechargeRetryStatus.class));
		} catch (RuntimeException re) {
			logger.error("Error reading recharge_retry_status.", re);
		}
		logger.info("Finished getting all recharge_retry_status");
		if (rechargeRetryStatusList != null && !rechargeRetryStatusList.isEmpty()){
			rechargeRetryStatus = rechargeRetryStatusList.get(0);
		}
		return rechargeRetryStatus;	
	}

	@Override
	public void updateRechargeRetryStatus(RechargeRetryStatus rechargeRetryStatus) {
		logger.debug("Updating recharge_retry_status");
		try {
			
			String queryString = "update recharge_retry_status set order_id  = :orderId, retry_count_remaining= :retryCountRemaning, " +
					"retry_status = :retryStatus where id= :id";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", rechargeRetryStatus.getId());
			paramMap.put("orderId", rechargeRetryStatus.getOrderId());
			paramMap.put("retryCountRemaning", rechargeRetryStatus.getRetryCountRemaining());
			paramMap.put("retryStatus", rechargeRetryStatus.isRetryComplete());
			jdbcTemplate.update(queryString, paramMap);
		} catch (RuntimeException re) {
			logger.error("Error reading recharge_retry_status.", re);
		}
		logger.debug("Finished updating recharge_retry_status");
	}
	
	@Override
	public boolean createRechargeRetryStatus(RechargeRetryStatus rechargeRetryStatus) {
	    SqlParameterSource parameters = new BeanPropertySqlParameterSource(rechargeRetryStatus);
	    int rowsInserted = insertRechargeRetryStatus.execute(parameters);
	    
	    return rowsInserted == 1;
	}
	
	@Override
    public List<RechargeRetryStatus> getActiveRetryRechargeStatusOrderIds(Date start, Date end) {
        logger.info("Getting all recharge retry status order ids for time duration from " + start + " to " + end);
        List<RechargeRetryStatus> rechargeRetryStatusList = new ArrayList<RechargeRetryStatus>();
        SimpleDateFormat simDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            String queryString = "select * from recharge_retry_status where created_on>= :start and created_on<= :end and is_retry_complete=0 and retry_count_remaining>0";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("start", simDateFormat.format(start));
            paramMap.put("end", simDateFormat.format(end));
            rechargeRetryStatusList = jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(RechargeRetryStatus.class));
        } catch (RuntimeException re) {
            logger.error("Error reading recharge_retry_status.", re);
        }
        logger.info("Finished getting all recharge_retry_status");
        return rechargeRetryStatusList; 
    }
}
