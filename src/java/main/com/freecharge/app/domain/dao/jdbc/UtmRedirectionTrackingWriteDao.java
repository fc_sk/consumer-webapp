package com.freecharge.app.domain.dao.jdbc;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.UtmRedirectionTrackingDetails;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmRedirectionTrackingWriteDao {

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertUtmTracker;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertUtmTracker = new SimpleJdbcInsert(dataSource).withTableName("utm_redirection_tracking_details").usingGeneratedKeyColumns("utm_redirection_id", "n_last_updated", "n_created");
    }

    public void insert(UtmRedirectionTrackingDetails utmRedirectionTrackingDetails) {
//        SqlParameterSource parameters = utmRedirectionTrackingDetails.getMapSqlParameterSource();
//        this.insertUtmTracker.executeAndReturnKey(parameters);
    }
}
