package com.freecharge.app.domain.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.businessdo.FirstNameBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;


public class TxnFulfilmentDAO extends CommonDao<TxnFulfilment> implements ITxnFulfilmentDAO {
    //property constants
    public static final String TXN_HOME_PAGE_ID = "txnHomePageId";
    public static final String EMAIL = "email";
    public static final String ADDRESS_MOBILE = "addressMobile";
    public static final String IS_GUEST = "isGuest";
    public static final String BILLING_ADD = "billingAdd";
    public static final String BILLING_CITY = "billingCity";
    public static final String BILLING_COUNTRY = "billingCountry";
    public static final String BILLING_PINCODE = "billingPincode";
    public static final String DELIVERY_ADD = "deliveryAdd";
    public static final String DELIVERY_CITY = "deliveryCity";
    public static final String DELIVERY_COUNTRY = "deliveryCountry";
    public static final String DELIVERY_PINCODE = "deliveryPincode";
    public static final String DELIVERY_NAME = "deliveryName";
    public static final String TRANSACTION_STATUS = "transactionStatus";
    public static final String ORDER_ID = "orderId";

    private static Logger logger = LoggingFactory.getLogger(TxnFulfilmentDAO.class);


    protected Class<TxnFulfilment> getDomainClass() {
        return TxnFulfilment.class;
    }


    /**
     * Find all TxnFulfilment entities with a specific property value.
     *
     * @param propertyName the name of the TxnFulfilment property to query
     * @param value        the property value to match
     * @return List<TxnFulfilment> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TxnFulfilment> findByProperty(String propertyName, final Object value
    ) {
    	logger.debug("Request comes into the findByProperty method of the class TxnFulfilmentDAO, the request  input parameters is String "+propertyName+" Object "+ value);
        logger.info("finding TxnFulfilment instance with property: " + propertyName + ", value: " + value, null);
        try {
            final String queryString = "select model from TxnFulfilment model where model."
                    + propertyName + "= :propertyValue";
            Query query = getSession().createQuery(queryString);
            query.setParameter("propertyValue", value);
            List<TxnFulfilment> list=query.list();
            logger.debug("Request goes out from the findByProperty method of the class TxnFulfilmentDAO, the request  return type is List<TxnFulfilment> "+list);
            return list;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public List<TxnFulfilment> findByTxnHomePageId(Object txnHomePageId
    ) {
        return findByProperty(TXN_HOME_PAGE_ID, txnHomePageId
        );
    }

    public List<TxnFulfilment> findByEmail(Object email
    ) {
        return findByProperty(EMAIL, email
        );
    }

    public List<TxnFulfilment> findByAddressMobile(Object addressMobile
    ) {
        return findByProperty(ADDRESS_MOBILE, addressMobile
        );
    }

    public List<TxnFulfilment> findByIsGuest(Object isGuest
    ) {
        return findByProperty(IS_GUEST, isGuest
        );
    }

    public List<TxnFulfilment> findByBillingAdd(Object billingAdd
    ) {
        return findByProperty(BILLING_ADD, billingAdd
        );
    }

    public List<TxnFulfilment> findByBillingCity(Object billingCity
    ) {
        return findByProperty(BILLING_CITY, billingCity
        );
    }

    public List<TxnFulfilment> findByBillingCountry(Object billingCountry
    ) {
        return findByProperty(BILLING_COUNTRY, billingCountry
        );
    }

    public List<TxnFulfilment> findByBillingPincode(Object billingPincode
    ) {
        return findByProperty(BILLING_PINCODE, billingPincode
        );
    }

    public List<TxnFulfilment> findByDeliveryAdd(Object deliveryAdd
    ) {
        return findByProperty(DELIVERY_ADD, deliveryAdd
        );
    }

    public List<TxnFulfilment> findByDeliveryCity(Object deliveryCity
    ) {
        return findByProperty(DELIVERY_CITY, deliveryCity
        );
    }

    public List<TxnFulfilment> findByDeliveryCountry(Object deliveryCountry
    ) {
        return findByProperty(DELIVERY_COUNTRY, deliveryCountry
        );
    }

    public List<TxnFulfilment> findByDeliveryPincode(Object deliveryPincode
    ) {
        return findByProperty(DELIVERY_PINCODE, deliveryPincode
        );
    }

    public List<TxnFulfilment> findByDeliveryName(Object deliveryName
    ) {
        return findByProperty(DELIVERY_NAME, deliveryName
        );
    }

    public List<TxnFulfilment> findByTransactionStatus(Object transactionStatus
    ) {
        return findByProperty(TRANSACTION_STATUS, transactionStatus
        );
    }

    public List<TxnFulfilment> findByOrderId(Object orderId
    ) {
        return findByProperty(ORDER_ID, orderId
        );
    }


    /**
     * Find all TxnFulfilment entities.
     *
     * @return List<TxnFulfilment> all TxnFulfilment entities
     */
    @SuppressWarnings("unchecked")
    public List<TxnFulfilment> findAll(
    ) {
    	logger.debug("Request comes into the findAll method of the class TxnFulfilmentDAO, the request  has no input parameters ");
        logger.info("finding all TxnFulfilment instances", null);
        try {
            final String queryString = "select model from TxnFulfilment model";
            Query query = getSession().createQuery(queryString);
            List<TxnFulfilment> list=query.list();
            logger.debug("Request goes out from the findAll method of the class TxnFulfilmentDAO, the request  return type is List<TxnFulfilment> "+list);
            return list;
        } catch (RuntimeException re) {
            logger.error("find all failed", re);
            throw re;
        }
    }


    public Integer saveFirstName(BaseBusinessDO baseBusinessDO) {
    	logger.debug("Request comes into the saveFirstName method of the class TxnFulfilmentDAO, the request input parameter type is BaseBusinessDO "+baseBusinessDO);
        String queryStr = "update TxnFulfilment txnFulfilment set txnFulfilment.firstName=:firstname where txnFulfilment.txnHomePageId=:txnhomepageid";
        try {
            Query query = this.getSession().createQuery(queryStr);
            query.setParameter("firstname", ((FirstNameBusinessDO) baseBusinessDO).getFirstName());
            query.setParameter("txnhomepageid", ((FirstNameBusinessDO) baseBusinessDO).getTxnHomePageId());
            Integer id=query.executeUpdate();
            logger.debug("Request goes out from the saveFirstName method of the class TxnFulfilmentDAO, the request  return type is List<TxnFulfilment> "+id);
            return id;
        } catch (Exception e) {
            logger.error("Exception while saving the first name..", e);
            return 0;
        }
    }


    public Integer save(TxnFulfilment txnFulFilment) {
        try {
        	logger.debug("Request comes into the save method of the class TxnFulfilmentDAO, the request input parameter type is TxnFulfilment "+txnFulFilment);
        	String txnQuery = "select model from TxnFulfilment model where model.txnHomePageId=:txnHomePageId and model.email=:email";
        	Query query = this.getSession().createQuery(txnQuery);
        	query.setParameter("txnHomePageId", txnFulFilment.getTxnHomePageId());
        	query.setParameter("email", txnFulFilment.getEmail());
        	List<TxnFulfilment> txnFulfilments = query.list();
        	if(txnFulfilments.size() != 0)
        		return txnFulfilments.get(0).getId();
        	Integer id=(Integer) this.getSession().save(txnFulFilment);
        	logger.debug("Request goes out from the save method of the class TxnFulfilmentDAO, the request  return type is Integer "+id);
            return id;
        } catch (Exception exp) {
            logger.error("Exception occured while saving transaction fulfilment data..", exp);
            return 0;
        }
    }


    public Integer updateAddress(BaseBusinessDO baseBusinessDO) {
        try {
        	logger.debug("Request comes into the updateAddress method of the class TxnFulfilmentDAO, the request input parameter type is BaseBusinessDO "+baseBusinessDO);
            String queryStr = "update TxnFulfilment txnFulfilment set txnFulfilment.deliveryAdd=:deliveryAdd,txnFulfilment.deliveryCity=:deliveryCity,txnFulfilment.deliveryFkStateMasterId=:deliveryFkStateMasterId,txnFulfilment.deliveryFkCountryMasterId=:deliveryFkCountryMasterId,txnFulfilment.deliveryPincode=:deliveryPincode where txnFulfilment.id=:txnfulfilmentId";
            Query query = this.getSession().createQuery(queryStr);
            query.setParameter("deliveryAdd", ((AddressBusinessDO) baseBusinessDO).getAddress());
            query.setParameter("deliveryCity", ((AddressBusinessDO) baseBusinessDO).getCity());
            query.setParameter("deliveryFkStateMasterId", ((AddressBusinessDO) baseBusinessDO).getStateId());
            query.setParameter("deliveryFkCountryMasterId", ((AddressBusinessDO) baseBusinessDO).getCountryId());
            query.setParameter("deliveryPincode", ((AddressBusinessDO) baseBusinessDO).getPostalCode());
            query.setParameter("txnfulfilmentId", ((AddressBusinessDO) baseBusinessDO).getTxnfulfilmentId());
            Integer id=query.executeUpdate();
            logger.debug("Request goes out from the updateAddress method of the class TxnFulfilmentDAO, the request  return type is Integer "+id);
            return id;
        } catch (Exception e) {
            logger.error("Exception occured while updating address into txnfulfillment table..", e);
            return 0;
        }
    }


    public TxnFulfilment updateFulfilmentDB(TxnFulfilment txnFulFilment) {
        try {
           return (TxnFulfilment) this.getSession().merge(txnFulFilment);
        } catch (Exception exp) {
            logger.error("Exception occured while saving transaction fulfilment data..", exp);
        }
        return txnFulFilment;
    }
    public void updateOrderId(Integer fullFilmentId ,String orderId ){
    	try {
    		logger.debug("Request comes into the updateOrderId method of the class TxnFulfilmentDAO, the request input parameter type is Integer "+fullFilmentId+" String"+ orderId);
    		logger.info("Start updateing txnfulfillment with order id: "+orderId+" for txnfulfillmentid: "+fullFilmentId+"");
    		
    		String txnQuery = "update TxnFulfilment txf set txf.orderId=:orderId where txf.id=:id";
    		Query query=getSession().createQuery(txnQuery);
    		query.setParameter("orderId", orderId);
    		query.setParameter("id", fullFilmentId);
    		query.executeUpdate();
    		logger.debug("Request goes out from the updateOrderId method of the class TxnFulfilmentDAO, the request  return type is void ");
    	}catch (Exception exp) {
    	logger.error("Exception occured while updating the orderID in txnfullfillment..", exp);
    	}
    	}
    
    
    public int markTransactionSuccessful(String orderId) {
		String SQL = "update TxnFulfilment txf set txf.transactionStatus = :stat where txf.orderId = :id";
		Query query = getSession().createQuery(SQL);
		query.setParameter("stat", FCConstants.SUCCESSFULL_TRANSACTION_STATUS);
		query.setParameter("id", orderId);
		int status = query.executeUpdate();
		return status;			
	}
       
    public int markTransactionFailed(String orderId) {
        String SQL = "update TxnFulfilment txf set txf.transactionStatus = :stat where txf.orderId = :id";
        Query query = getSession().createQuery(SQL);
        query.setParameter("stat", FCConstants.FAILED_TRANSACTION_STATUS);
        query.setParameter("id", orderId);
        int status = query.executeUpdate();
        return status;          
    }

    
/*Update Txnfulfilment address by txnfilfilmentId*/
    public boolean updateFulfilmentAddress(TxnFulfilment txnFulfilment) {
        try {
        	logger.debug("Request comes into the updateFulfilment method of the class TxnFulfilmentDAO, the request input parameter type is TxnFulfilment "+txnFulfilment);
			Session session = this.getSession();
            if (session != null) {
            	String querStr ="update TxnFulfilment txnfulfilment set txnfulfilment.deliveryAdd=:deliveryAdd,txnfulfilment.deliveryArea=:deliveryArea," +
            			"txnfulfilment.deliveryLandmark=:deliveryLandmark, txnfulfilment.deliveryFkCityMasterId=:deliveryCity," +
            			"txnfulfilment.deliveryFkStateMasterId=:deliveryFkStateMasterId,txnfulfilment.deliveryPincode=:deliveryPincode, txnfulfilment.deliveryName=:deliveryName," +
            			"txnfulfilment.updatedAt=:updatedAt,txnfulfilment.firstName=:firstName,txnfulfilment.deliveryName=:deliveryName where txnfulfilment.id=:id";
            	Query query = session.createQuery(querStr);
                
                query.setParameter("deliveryAdd", txnFulfilment.getDeliveryAdd());
                query.setParameter("deliveryArea", txnFulfilment.getDeliveryArea());
                query.setParameter("deliveryLandmark", txnFulfilment.getDeliveryLandmark());
                query.setParameter("deliveryCity", txnFulfilment.getDeliveryFkCityMasterId());
                query.setParameter("deliveryFkStateMasterId", txnFulfilment.getDeliveryFkStateMasterId());
                query.setParameter("deliveryPincode", txnFulfilment.getDeliveryPincode());
                query.setParameter("updatedAt", txnFulfilment.getUpdatedAt()); 
                query.setParameter("firstName", txnFulfilment.getFirstName());
                query.setParameter("deliveryName", txnFulfilment.getDeliveryName());
                query.setParameter("id", txnFulfilment.getId());
                query.setParameter("deliveryName", txnFulfilment.getDeliveryName());
                
                logger.debug("Request goes out from the updateFulfilment method of the class TxnFulfilmentDAO, the request  return type is true ");
                query.executeUpdate();
                
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.error("Exception occured while updating Txnfulfilment .", e);
            return false;
        }
    }
 }