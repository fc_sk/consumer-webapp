package com.freecharge.app.domain.dao.jdbc;

import com.freecharge.app.domain.entity.jdbc.*;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.recharge.util.RechargeConstants;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;

import java.util.HashMap;
import java.util.Map;

public class InWriteDAO {
	private static Logger logger = LoggingFactory.getLogger(InWriteDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertInRequest;
	private SimpleJdbcInsert insertInResponse;
	private SimpleJdbcInsert insertInTransactions;
	private SimpleJdbcInsert insertInErrorCodeMap;
	private SimpleJdbcInsert insertRechargeRetryData;
	private SimpleJdbcInsert insertInRechargeRetry;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertInRequest = new SimpleJdbcInsert(dataSource).withTableName("in_request").usingGeneratedKeyColumns("request_id", "n_last_updated", "n_created");
        this.insertInResponse = new SimpleJdbcInsert(dataSource).withTableName("in_response").usingGeneratedKeyColumns("response_id", "n_last_updated", "n_created");
        this.insertInTransactions = new SimpleJdbcInsert(dataSource).withTableName("in_transactions").usingGeneratedKeyColumns("transaction_id", "n_last_updated", "n_created");
        this.insertInErrorCodeMap = new SimpleJdbcInsert(dataSource).withTableName("in_errorcode_map").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
        this.insertRechargeRetryData = new SimpleJdbcInsert(dataSource).withTableName("recharge_retry_data").usingGeneratedKeyColumns("retry_id", "n_last_updated", "n_created");
        this.insertInRechargeRetry = new SimpleJdbcInsert(dataSource).withTableName("in_recharge_retry").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

	public RechargeRetryData create(RechargeRetryData rechargeRetryData) {
        try {
            SqlParameterSource parameters = new BeanPropertySqlParameterSource(rechargeRetryData);
            Number number = this.insertRechargeRetryData.executeAndReturnKey(parameters);
            rechargeRetryData.setRetryId(number.intValue());
            return rechargeRetryData;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
	
	public InErrorcodeMap create(InErrorcodeMap inErrorcodeMap) {
        try {
            SqlParameterSource parameters = new BeanPropertySqlParameterSource(inErrorcodeMap);
            Number number = this.insertInErrorCodeMap.executeAndReturnKey(parameters);
            inErrorcodeMap.setId(number.intValue());
            return inErrorcodeMap;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
	
	public InRequest create(InRequest inRequest) {
		try {
			logger.debug("Request comes into the create method of the class InWriteDAO, the request parameter type is  InRequest "+inRequest.getAffiliateTransId() +" " +inRequest.getSubscriberNumber() );
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inRequest);
			Number number = this.insertInRequest.executeAndReturnKey(parameters);
			inRequest.setRequestId(number.longValue());
			logger.debug("Request goes out from the create method of the class InWriteDAO, the request  return type is InRequest "+inRequest.getAffiliateTransId() +" " +inRequest.getSubscriberNumber() );
			return inRequest;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public InRequest update(InRequest inRequest) {
		try {
			logger.debug("Request comes into the update method of the class InWriteDAO, the request parameter type is  InRequest "+inRequest.getAffiliateTransId() +" " +inRequest.getSubscriberNumber() );
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inRequest);
			String sql = "update in_request set affiliate_id = :affiliateId, affiliate_trans_id = :affiliateTransId, product_type = :productType, subscriber_number = :subscriberNumber, "
					+"operator = :operator, circle = :circle, amount = :amount, created_time = :createdTime, updated_time = :updatedTime, raw_request = :rawRequest where request_id = :requestId";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			
			if (status <= 0) {
				logger.error("No Rows affected on updating in_request for id = "+inRequest.getRequestId());
			}
			logger.debug("Request goes out from the update method of the class InWriteDAO, the request  return type is InRequest "+inRequest.getAffiliateTransId() +" " +inRequest.getSubscriberNumber() );
			return inRequest;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public InResponse create(InResponse inResponse) {
		try {
			logger.debug("Request comes into the create method of the class InWriteDAO, the request parameter type is  InResponse with request id = "+inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inResponse);
			Number number = this.insertInResponse.executeAndReturnKey(parameters);
			inResponse.setResponseId(number.longValue());
			logger.debug("Request goes out from the create method of the class InWriteDAO, the request  return type is InResponse "+inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			return inResponse;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public InResponse update(InResponse inResponse) {
		try {
			logger.debug("Request comes into the update method of the class InWriteDAO, the request parameter type is  InResponse "+inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inResponse);
			String sql = "update in_response set request_id = :requestId, raw_request = :rawRequest, raw_response = :rawResponse, aggr_trans_id = :aggrTransId, opr_trans_id = :oprTransId, "					
					+"aggr_name = :aggrName, aggr_resp_code = :aggrRespCode, aggr_resp_msg = :aggrRespMsg, in_resp_code = :inRespCode, in_resp_msg = :inRespMsg, retry_number = :retryNumber, "
					+"created_time = :createdTime, updated_time = :updatedTime where response_id= :responseId";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			
			if (status <= 0) {
				logger.error("No Rows affected on updating in_response for id = "+inResponse.getResponseId());
			}
			logger.debug("Request goes out from the update method of the class InWriteDAO, the request  return type is InResponse "+inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			return inResponse;			
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public InTransactions create(InTransactions inTransactions) {
		try {
			logger.debug("Request comes into the create method of the class InWriteDAO, the request parameter type is  InTransactions with request id = "+inTransactions.getFkRequestId());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inTransactions);
			Number number = this.insertInTransactions.executeAndReturnKey(parameters);
			inTransactions.setTransactionId(number.longValue());
			logger.debug("Request goes out from the create method of the class InWriteDAO, the request  return type is InTransactions with request id = "+inTransactions.getFkRequestId());
			return inTransactions;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public InTransactions update(InTransactions inTransaction) {
		try {
			logger.debug("Request comes into the update method of the class InWriteDAO, the request parameter type is  Intransaction "+inTransaction.getFkRequestId());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inTransaction);
			String sql = "update in_transactions set fk_request_id = :fkRequestId, raw_request = :rawRequest, raw_response = :rawResponse, aggr_reference_no = :aggrReferenceNo, aggr_opr_refernce_no = :aggrOprRefernceNo, "					
					+"aggr_name = :aggrName, aggr_response_code = :aggrResponseCode, aggr_response_message = :aggrResponseMessage, request_time = :requestTime, response_time = :responseTime ,"
					+" transaction_status = :transactionStatus, request_process_time= :requestProcessTime where transaction_id= :transactionId";
			
			int status = this.jdbcTemplate.update(sql, parameters);
			
			if (status <= 0) {
				logger.error("No Rows affected on updating in_response for id = "+inTransaction.getTransactionId());
			}
			logger.debug("Request goes out from the update method of the class InWriteDAO, the request  return type is InTransactions "+inTransaction.getFkRequestId() + " " + inTransaction.getRequestProcessTime());
			return inTransaction;			
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public InRechargeRetryMap insertInRechargeRetry(InRechargeRetryMap inRechargeRetryMap) {
	    try {
            SqlParameterSource parameters = new BeanPropertySqlParameterSource(inRechargeRetryMap);
            Number number = this.insertInRechargeRetry.executeAndReturnKey(parameters);
            inRechargeRetryMap.setId(number.intValue());
            return inRechargeRetryMap;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
	}

    public void updateInRechargeRetry(InRechargeRetryMap inRechargeRetryMap) {
        String SQL = String.format("update in_recharge_retry set aggr_name = :aggr_name, "
                + "fk_operator_master_id=:fk_operator_master_id, aggr_resp_code=:aggr_resp_code, "
                + "is_retryable=:is_retryable, fk_retry_operator_master_id=:fk_retry_operator_master_id, "
                + "retry_priority=:retry_priority  where id=%s", inRechargeRetryMap.getId()) ;
        Map<String, Object> params = new HashMap<>();
        params.put("aggr_name", inRechargeRetryMap.getAggrName());
        params.put("fk_operator_master_id", inRechargeRetryMap.getFkOperatorMasterId());
        params.put("aggr_resp_code", inRechargeRetryMap.getAggrRespCode());
        params.put("is_retryable", inRechargeRetryMap.getIsRetryable());
        params.put("fk_retry_operator_master_id", inRechargeRetryMap.getFkRetryOperatorMasterId());
        params.put("retry_priority", inRechargeRetryMap.getRetryPriority());
        this.jdbcTemplate.update(SQL, params);
    }
       public Integer updateAgPreference(Map<String, Object> map) {
              Integer count = 0;
              String operatorId = (String) map.get(RechargeConstants.OPERATOR_ID);
              String agTxnRatioValue = (String) map.get(RechargeConstants.AG_TXN_RATIO);
              StringBuffer buffer = new StringBuffer("Update aggregator_preference Set ");
              Map<String, String> params = new HashMap<String, String>();
              for (Map.Entry<String, Object> entry : map.entrySet()) {
                     if (entry.getKey().contains("ag_") || entry.getKey().contains("is_")) {
                            if (count > 0)
                                   buffer.append(", ");
                            buffer.append(entry.getKey() + " = :"
                                          + entry.getKey().substring(entry.getKey().indexOf("_") + 1));
                            count++;

                            params.put(entry.getKey().substring(entry.getKey().indexOf("_") + 1), entry.getValue()
                                          .toString());
                     }
              }
              String query = buffer + ", " + RechargeConstants.AG_TXN_RATIO + "= '" + agTxnRatioValue + "'" +" where operator_id = " + operatorId;

              int status = this.jdbcTemplate.update(query, params);
              return status;
       }

    public void deleteOperatorRetryConfig(Integer id) {
        String SQL = String.format("DELETE from in_recharge_retry where id=" + id) ;
        Map<String, Object> params = new HashMap<>();
        this.jdbcTemplate.update(SQL, params);
    }

	public int updateStatusChange(InResponse inResponse) {
		try {
			logger.debug(
					"Request comes into the update method of the class InWriteDAO, the request parameter type is  InResponse "
							+ inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inResponse);
			String sql = "update in_response set request_id = :requestId, raw_request = :rawRequest, raw_response = :rawResponse, aggr_trans_id = :aggrTransId, opr_trans_id = :oprTransId, "
					+ "aggr_name = :aggrName, aggr_resp_code = :aggrRespCode, aggr_resp_msg = :aggrRespMsg, in_resp_code = :inRespCode, in_resp_msg = :inRespMsg, retry_number = :retryNumber, "
					+ "created_time = :createdTime, updated_time = :updatedTime where response_id= :responseId "
					+ "and (in_resp_code is NULL or in_resp_code = '08')";

			return this.jdbcTemplate.update(sql, parameters);

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public int updateStartupImpactedTransactions(InResponse inResponse) {
		try {
			logger.debug(
					"Request comes into the update method of the class InWriteDAO, the request parameter type is  InResponse "
							+ inResponse.getRequestId() + " " + inResponse.getRetryNumber());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(inResponse);
			String sql = "update in_response set request_id = :requestId, raw_request = :rawRequest, raw_response = :rawResponse, aggr_trans_id = :aggrTransId, opr_trans_id = :oprTransId, "
					+ "aggr_name = :aggrName, aggr_resp_code = :aggrRespCode, aggr_resp_msg = :aggrRespMsg, in_resp_code = :inRespCode, in_resp_msg = :inRespMsg, retry_number = :retryNumber, "
					+ "created_time = :createdTime, updated_time = :updatedTime where response_id= :responseId "
					+ "and (in_resp_code is NULL or in_resp_code = '08'or in_resp_code = '-1')";

			return this.jdbcTemplate.update(sql, parameters);

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
}
