package com.freecharge.app.domain.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.businessdo.ChangePasswordBusinessDO;
import com.freecharge.common.businessdo.CheckUserBusinessDO;
import com.freecharge.common.businessdo.MyContactsBussinessDO;
import com.freecharge.common.businessdo.MyRechargeContactProfileBusinessDO;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.web.util.WebConstants;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: Apr 30, 2012 Time: 10:36:13 AM To change this template use File | Settings | File Templates.
 */

public class HomeBusinessDao extends BaseDao implements IHomeBusinessDao {

    private static Logger logger = LoggingFactory.getLogger(HomeBusinessDao.class);

    @Autowired
    private IRegistrationDAO registrationDAO;

    @Autowired
    private IUsersDAO usersDao;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService    billPayServiceRemote;
    
    /** THIS METHOD SHOULDN'T CALLED FROM THIS CLASS. USED HomeBusinessReadDao.getAuthenticateUser INSTEAD
     * User Authentication using email and password.
     * 
     * @param email    email property to query
     * @param password password property to query
     * @return Users found by query
     */
    @SuppressWarnings("unchecked")
    public Users getAuthenticateUser(String email, String password) {
       
        /** THIS METHOD SHOULDN'T CALLED FROM THIS CLASS. USED HomeBusinessReadDao.getAuthenticateUser INSTEAD
         *  Throwing Run time exception 
         */
        throw new RuntimeException();
    }


    /**
     * Getting ProfileInfo
     *
     * @return List
     */
    public List getProfileInfo(BaseBusinessDO baseBusinessDO) {
        List<List<Object>> ObjectList = null;
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        Session session = getSession();

        String querystr = "from Users users, UserProfile userprofile where users.email=:email and users.userId=userprofile.fkUserId and userprofile.isDefault = true and userprofile.isActive=true";

        Query query = session.createQuery(querystr).setParameter("email", profilebusinessdo.getEmail());
        ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
        return ObjectList;
    }
    
    public List getProfileInfoByUserId(Integer userId) {
        List<List<Object>> ObjectList = null;
        Session session = getSession();
        String querystr = "from Users users, UserProfile userprofile where users.userId=:userId and users.userId=userprofile.fkUserId and userprofile.isDefault = true and userprofile.isActive=true";
        Query query = session.createQuery(querystr).setParameter("userId", userId);
        ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
        return ObjectList;
    }
    
    public UserContactDetail getLastestUserContactProfile(BaseBusinessDO baseBusinessDO) {
        MyRechargeContactProfileBusinessDO myRchgContactProfileBusinessDO = (MyRechargeContactProfileBusinessDO) baseBusinessDO;

        UserContactDetail userContactDetail = null;
        String email = null;
        com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getUserByUserId(myRchgContactProfileBusinessDO.getUserId());
        if (user == null) {
            return null;
        }else{
            email = user.getEmail();
        }
        List<UserContactDetail> list = registrationDAO.getUserContactDetails(email, myRchgContactProfileBusinessDO.getRechargeContactNo());

        if (list == null || list.isEmpty()) { // Return the default profile
            list = registrationDAO.getUserContactDefaultDetails(email);
        }

        if (list == null || list.isEmpty())
            return null;
        userContactDetail = list.get(0); // List is sorted in descending order of last_update and we want the latest recharge contact details
        return userContactDetail;
    }

    /**
     * Getting UserProfileInfo
     *
     * @return List
     */
    public List<UserProfile> getUserProfileInfo(BaseBusinessDO baseBusinessDO) {
        List<UserProfile> userProfileList = null;
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));

        Session session = getSession();

        Integer userId = profilebusinessdo.getUserid();
        Integer userProfileId = profilebusinessdo.getUserProfileId();
        boolean isDefaultProfile = profilebusinessdo.isIsDefaultProfile();
        Criteria criteria = session.createCriteria(UserProfile.class);

        criteria.add(Restrictions.eq("fkUserId", userId));
        criteria.add(Restrictions.eq("isActive", true));
        if (isDefaultProfile)
            criteria.add(Restrictions.eq("isDefault", true));
        if (userProfileId != null && userProfileId != 0) {
            criteria.add(Restrictions.eq("userProfileId", userProfileId));
        }

        userProfileList = criteria.list();

        return userProfileList;
    }

    /**
     * Updating ProfileInfo
     */
    public void updateProfile(List userAndUserProfile, boolean isupdatepass) {
        Users user = (Users) userAndUserProfile.get(0);
        Session session = getSession();
        Integer id;
        boolean profileupdatestatus = false;
        Transaction tx = getSession().beginTransaction();
        UserProfile profile = (UserProfile) userAndUserProfile.get(1);
        String userprofiletablquery = "update UserProfile userprofile set userprofile.firstName=:firstName, userprofile.title=:title where userprofile.fkUserId=" + user.getUserId() + " and userprofile.isActive=true and userprofile.isDefault=true";
        String userEditQuery = "update Users users set users.morf=:morf, users.dob=:dob,users.mobileNo=:mobileno where users.email=:email and users.userId=:userId ";

        if (isupdatepass) {
            String userstablequery = "update Users users set users.password=:password where users.email=:email";
            Query query = session.createQuery(userstablequery);
            query.setParameter("password", user.getPassword());
            query.setParameter("email", user.getEmail());
            query.executeUpdate();
        }
        Query profileuserquery = session.createQuery(userprofiletablquery);
        profileuserquery.setParameter("firstName", profile.getFirstName());
        profileuserquery.setParameter("title", profile.getTitle());
        profileuserquery.executeUpdate();

        Query userQuery = session.createQuery(userEditQuery);
        userQuery.setParameter("morf", user.getMorf());
        userQuery.setParameter("dob", user.getDob());
        userQuery.setParameter("email", user.getEmail());
        userQuery.setParameter("userId", user.getUserId());
        userQuery.setParameter("mobileno", user.getMobileNo());
        userQuery.executeUpdate();

        tx.commit();
    }

    /**
     * Updating Default ProfileInfo
     */
    public boolean updateDefaultProfile(List userAndUserProfile, boolean isupdatepass) {
        boolean updateStatus = false;
        Session session = getSession();

        Transaction tx = getSession().beginTransaction();
        Users user = (Users) userAndUserProfile.get(0);
        UserProfile profile = (UserProfile) userAndUserProfile.get(1);

        String userEditQuery = "update Users users set users.morf=:morf, users.dob=:dob, users.mobileNo=:mobileNo, users.lastUpdate=:lastUpdate where users.email=:email and users.userId=:userId ";

        Query userQuery = session.createQuery(userEditQuery);
        userQuery.setParameter("morf", user.getMorf());
        userQuery.setParameter("dob", user.getDob());
        userQuery.setParameter("email", user.getEmail());
        userQuery.setParameter("userId", user.getUserId());
        userQuery.setParameter("mobileNo", user.getMobileNo());
        userQuery.setParameter("lastUpdate", new Timestamp((new Date()).getTime()));
        int userQueryResult = userQuery.executeUpdate();

        if (userQueryResult > 0) { // If UserId + Email exists, then only update the userProfile
            if (isupdatepass) {
                String userstablequery = "update Users users set users.password=:password , users.lastUpdate=:lastUpdate where users.email=:email and users.userId=:userId ";
                Query query = session.createQuery(userstablequery);
                query.setParameter("password", user.getPassword());
                query.setParameter("email", user.getEmail());
                query.setParameter("userId", user.getUserId());
                query.setParameter("lastUpdate", new Timestamp((new Date()).getTime()));
                query.executeUpdate();
            }

            StringBuilder userprofiletablquery = new StringBuilder();
            userprofiletablquery.append("update UserProfile profile set profile.firstName=:firstName, profile.title=:title, ");
            userprofiletablquery.append("profile.address1=:address , profile.landmark=:landmark , profile.fkCityMasterId=:city , profile.postalCode=:postalcode , profile.fkStateMasterId=:state , profile.area=:area, ");
            userprofiletablquery.append("profile.lastUpdate=:lastUpdate");
            userprofiletablquery.append(" where profile.fkUserId=:fkUserId");
            userprofiletablquery.append(" and profile.isActive=true and profile.isDefault=true");

            Query profileuserquery = session.createQuery(userprofiletablquery.toString());
            profileuserquery.setParameter("firstName", profile.getFirstName());
            profileuserquery.setParameter("title", profile.getTitle());
            profileuserquery.setParameter("address", profile.getAddress1());
            profileuserquery.setParameter("landmark", profile.getLandmark());
            profileuserquery.setParameter("city", profile.getFkCityMasterId());
            profileuserquery.setParameter("postalcode", profile.getPostalCode());
            profileuserquery.setParameter("state", profile.getFkStateMasterId());
            profileuserquery.setParameter("area", profile.getArea());
            profileuserquery.setParameter("lastUpdate", new Timestamp((new Date()).getTime()));
            profileuserquery.setParameter("fkUserId", user.getUserId());
            profileuserquery.executeUpdate();

            updateStatus = true;
        }

        tx.commit();
        return updateStatus;
    }

    public boolean checkUserPassword(BaseBusinessDO businessdo) {
        boolean pswdcheck = false;
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (businessdo));
        String pwdcksql = "from Users user where user.email=:email and password=:password";
        Query query = getSession().createQuery(pwdcksql);
        query.setParameter("email", profilebusinessdo.getEmail());
        query.setParameter("password", profilebusinessdo.getOldpassword());
        List list = query.list();
        if (list != null && list.size() == 1)
            pswdcheck = true;
        return pswdcheck;
    }

    /*
    * It checks whether the given existing password matches with the given password for a userId+email combination.
    */
    public boolean checkUserPasswordWithEmailUserId(BaseBusinessDO businessdo) {
        boolean pswdcheck = false;
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (businessdo));
        String pwdcksql = "from Users user where user.email=:email and password=:password and user_id=:userId";
        Query query = getSession().createQuery(pwdcksql);
        query.setParameter("email", profilebusinessdo.getEmail());
        query.setParameter("password", profilebusinessdo.getOldpassword());
        query.setParameter("userId", profilebusinessdo.getUserid());
        List list = query.list();
        if (list != null && list.size() == 1)
            pswdcheck = true;
        return pswdcheck;
    }

    public void updateAddress(List userandprofile) {
        Users user = (Users) userandprofile.get(0);
        UserProfile profile = (UserProfile) userandprofile.get(1);
        String addupdatequery = "update UserProfile profile set profile.address1=:address , profile.landmark=:landmark , profile.fkCityMasterId=:city , profile.postalCode=:postalcode , profile.fkStateMasterId=:state , profile.area=:area where profile.fkUserId=" + user.getUserId() + " and profile.isDefault=true and profile.isActive=true";
        Transaction tx = getSession().beginTransaction();
        Query updatequery = getSession().createQuery(addupdatequery);
        updatequery.setParameter("address", profile.getAddress1());
        updatequery.setParameter("landmark", profile.getLandmark());
        updatequery.setParameter("city", profile.getFkCityMasterId());
        updatequery.setParameter("postalcode", profile.getPostalCode());
        updatequery.setParameter("state", profile.getFkStateMasterId());
        updatequery.setParameter("area", profile.getArea());
        int status = updatequery.executeUpdate();
        tx.commit();
    }

    public List<List<Object>> getMycontacts(Integer userid) {
        List<List<Object>> objectList = null;
        String contactsquerystr = "SELECT  service_number,first_name,user_recharge_contact_id,fk_product_master_id,fk_operator_master_id,fk_circle_master_id from user_recharge_contact contact Left OUTER  JOIN user_profile profile on contact.user_recharge_contact_id=profile.fk_user_recharge_contact_id where contact.fk_users_id=" + userid + "  and contact.service_number IS NOT NULL group by contact.service_number ORDER BY profile.first_name ASC";
        Query contactsquery = getSession().createSQLQuery(contactsquerystr);
        objectList = contactsquery.setResultTransformer(Transformers.TO_LIST).list();
        return objectList;
    }

    public List getAllProducts() {
        ProductMaster master;
        String productquery = "from ProductMaster";
        Query query = getSession().createQuery(productquery);
        List<ProductMaster> prodlist = query.list();
        return prodlist;
    }

    public List<UserProfile> profileNotexitIncontacts(BaseBusinessDO baseBusinessDO) {
        MyContactsBussinessDO mycontactsbusinessdo = ((MyContactsBussinessDO) (baseBusinessDO));
        int status = 0;
        String contactprofilechck = "from UserProfile profile where profile.fkUserId=" + mycontactsbusinessdo.getUserid();
        Query query = getSession().createQuery(contactprofilechck);
        List<UserProfile> profilelist = query.list();
        return profilelist;
    }

    public Integer saveProfile(UserProfile profile) {
        Integer profileid = (Integer) getSession().save(profile);
        return profileid;
    }

    public int updaeContact(BaseBusinessDO baseBusinessDO) {
        MyContactsBussinessDO mycontactsbusinessdo = ((MyContactsBussinessDO) (baseBusinessDO));
        int status = 0;
        Transaction tx = getSession().beginTransaction();
        String updatecont = "update user_recharge_contact contact,user_profile profile set contact.service_number=" + mycontactsbusinessdo.getServiceno() + ",contact.fk_operator_master_id=" + mycontactsbusinessdo.getOperator() + ",profile.first_name='" + mycontactsbusinessdo.getName() + "' where contact.user_recharge_contact_id=" + mycontactsbusinessdo.getContactid() + " and contact.fk_users_id='" + mycontactsbusinessdo.getUserid()
                + "' and contact.user_recharge_contact_id=profile.fk_user_recharge_contact_id and contact.fk_users_id=profile.fk_user_id and profile.is_default=0";
        Query updatecontquery = getSession().createSQLQuery(updatecont);
        status = updatecontquery.executeUpdate();
        tx.commit();
        return status;
    }


    public Integer changePassword(BaseBusinessDO baseBusinessDO) {
        Session session = getSession();
        String hqlQuery = "update Users set password=:password where email=:email";
        Query query = session.createQuery(hqlQuery).setParameter("email", ((ChangePasswordBusinessDO) ((baseBusinessDO))).getEmail()).setParameter("password", ((ChangePasswordBusinessDO) ((baseBusinessDO))).getNewPassword());
        Integer value = query.executeUpdate();
        return value;
    }

    public List<Users> validateUser(BaseBusinessDO baseBusinessDO) {
        List<Users> users = null;
        String hql = "from Users where email=:email";
        Session session = getSession();
        Query query = session.createQuery(hql);
        query.setParameter("email", ((CheckUserBusinessDO) (baseBusinessDO)).getEmail());
        users = query.list();
        return users;
    }

    public List<Users> validateUserIdAndEmail(Integer userId, String email) {
        logger.debug("Request comes into the validateUserIdAndEmail method of the class HomeBusinessDao, the request input parameters are Integer userId : " + userId + " and String email : " + email);
        List<Users> users = null;
        String hql = "from Users where userId=:userId and email=:email";
        Session session = getSession();
        Query query = session.createQuery(hql);
        query.setParameter("userId", userId);
        query.setParameter("email", email);
        users = query.list();
        return users;
    }

    
    public Map<String, Object> getUserDetailsFromOrderId(String orderId) {
        Map<String, Object> userDetails = new HashMap<String, Object>();
        String hql = "select txnfulfilment.email as email,txnhomepage.serviceNumber as mobileno," + "txnhomepage.amount as amount ," +
                "  txnhomepage.operatorName as oprtaorName,  " + "txnhomepage.circleName as circleName, orderid.lookupId as lookupId, " +
                "txnhomepage.productType as productType, " + "txnfulfilment.deliveryName as deliveryName, txnfulfilment.deliveryAdd as deliveryAdd, " +
                "cityMaster.cityName as deliveryCity, txnfulfilment.deliveryFkStateMasterId as stateId, " +
                "txnfulfilment.deliveryFkCountryMasterId as deliveryCountry, txnfulfilment.deliveryPincode as deliveryPinCode, txnfulfilment.id as txnfulfilmentid," +
                "txnfulfilment.createdAt as createdAt, txnfulfilment.deliveryStreet as deliveryStreet, txnfulfilment.deliveryArea as deliveryArea, " +
                "txnfulfilment.deliveryLandmark as deliveryLandmark, "
                + "stateMaster.stateName as stateName,txnfulfilment.deliveryAdd as address, " +
                "txnhomepage.billTxnHomePage.id as fkBillTxnHomePageId, txnfulfilment.transactionStatus as transactionStatus " +
                "from TxnFulfilment as txnfulfilment, " +
                "TxnHomePage as txnhomepage , OrderId as orderid, StateMaster as stateMaster, CityMaster as cityMaster "
                + "where orderid.orderId ='" + orderId + "' and txnfulfilment.txnHomePageId = txnhomepage.id and orderid.lookupId =  txnhomepage.lookupId " + " " +
                "and stateMaster.stateMasterId = txnfulfilment.deliveryFkStateMasterId and cityMaster.cityMasterId = txnfulfilment.deliveryFkCityMasterId";

        org.hibernate.Query query = getSession().createQuery(hql);
        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();

        for (List<Object> list : ObjectList) {

            userDetails.put("email", list.get(0));
            userDetails.put("mobileno", list.get(1));
            if (FCUtil.isUtilityPaymentType((String) list.get(7))) {
                String billId = (String) userDetails.get("mobileno");
                // Update the user details for bill pay here.
                BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
                userDetails.put("mobileno", billDetails.getAdditionalInfo1());
                userDetails.put("billId", billId);
            }
            userDetails.put("amount", list.get(2));
            userDetails.put("operatorName", list.get(3));
            userDetails.put("circleName", list.get(4));
            userDetails.put("lookupId", list.get(5));
            userDetails.put("productType", list.get(6));
            userDetails.put("deliveryNmae", list.get(7));
            userDetails.put("deliveryAdd", list.get(8));
            userDetails.put("deliveryCity", list.get(9));
            userDetails.put("deliverySatateId", list.get(10));
            userDetails.put("deliveryCountry", list.get(11));
            userDetails.put("deliveryPinCode", list.get(12));
            userDetails.put("txnfulfilmentid", list.get(13));
            userDetails.put("createdAt", list.get(14));
            if (list.get(16) != null)
                userDetails.put("deliveryStreet", list.get(15));
            userDetails.put("deliveryArea", list.get(16));
            if (list.get(18) != null)
                userDetails.put("deliveryLandmark", list.get(17));
            userDetails.put("stateName", list.get(18));
            userDetails.put("address", list.get(19));
            userDetails.put("fkBillTxnHomePageId", list.get(20));
            userDetails.put("transactionStatus", list.get(21));
        }

        return userDetails;
    }

    /*
     * This function is same as getUserDetailsFromOrderId() except that, it returns NULL for state and city of the user instead of query failure.
     * i.e., do the left outer join for the state_master and city_master tables.
     *
     */

    public Map<String, Object> getUserDetailsFromOrderIdWithAddressIfAvailable(String orderId) {
        Map<String, Object> userDetails = new HashMap<String, Object>();
        String sql = "select txnfulfilment.email as email,txnhomepage.service_number as mobileno, txnhomepage.amount as amount , "
               + "txnhomepage.operator_name as oprtaor_name,    txnhomepage.circle_name as circleName, orderid.lookup_id as lookupId,  "
             + "txnhomepage.product_type as productType,   txnfulfilment.delivery_name as deliveryName, txnfulfilment.delivery_add as deliveryAdd,  "
             + "cityMaster.city_name as deliveryCity, txnfulfilment.delivery_fk_state_master_id as stateId,  "
             + "txnfulfilment.delivery_fk_country_master_id as deliveryCountry, txnfulfilment.delivery_pincode as deliveryPinCode, txnfulfilment.id as txnfulfilmentid, "
             + "txnfulfilment.created_at as createdAt, txnfulfilment.delivery_street as deliveryStreet, txnfulfilment.delivery_area as deliveryArea,  "
             + "txnfulfilment.delivery_landmark as deliveryLandmark, "
              + "stateMaster.state_name as stateName,txnfulfilment.delivery_add as address,  "
             + "txnfulfilment.transaction_status as transactionStatus  "
              + "from (txn_fulfilment as txnfulfilment,  "
             + "txn_home_page as txnhomepage , order_id as orderid ) "
              + "LEFT JOIN state_master as stateMaster ON stateMaster.state_master_id = txnfulfilment.delivery_fk_state_master_id "
              + "LEFT JOIN city_master as cityMaster ON cityMaster.city_master_id = txnfulfilment.delivery_city "
              + "where orderid.order_id ='" + orderId + "' and txnfulfilment.txn_home_page_id = txnhomepage.id and orderid.lookup_id =  txnhomepage.lookup_id";

        org.hibernate.Query query = getSession().createSQLQuery(sql);
        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();

        for (List<Object> list : ObjectList) {

            userDetails.put("email", list.get(0));
            userDetails.put("mobileno", list.get(1));
            if (FCUtil.isUtilityPaymentType((String) list.get(6))) {
                String billId = (String) userDetails.get("mobileno");
                // Update the user details for bill pay here.
                BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
                userDetails.put("mobileno", billDetails.getAdditionalInfo1());
                userDetails.put("billId", billId);
            }
            //userDetails.put("firstname", list.get(2));
            userDetails.put("amount", list.get(2));
            userDetails.put("operatorName", list.get(3));
            userDetails.put("circleName", list.get(4));
            userDetails.put("lookupId", list.get(5));
            userDetails.put("productType", list.get(6));
            userDetails.put("deliveryNmae", list.get(7));
            userDetails.put("deliveryAdd", list.get(8));
            userDetails.put("deliveryCity", list.get(9));
            userDetails.put("deliverySatateId", list.get(10));
            userDetails.put("deliveryCountry", list.get(11));
            userDetails.put("deliveryPinCode", list.get(12));
            userDetails.put("txnfulfilmentid", list.get(13));
            userDetails.put("createdAt", list.get(14));
            if (list.get(15) != null)
                userDetails.put("deliveryStreet", list.get(15));
            userDetails.put("deliveryArea", list.get(16));
            if (list.get(17) != null)
                userDetails.put("deliveryLandmark", list.get(17));
            userDetails.put("stateName", list.get(18));
            userDetails.put("address", list.get(19));
            //userDetails.put("userId", list.get(21));
            userDetails.put("transactionStatus", list.get(20));
        }

        return userDetails;
    }
    
    private static final String SQL_FIND_INVOICE_INFO_FOR_RECHARGE = "" +
            "select transactionHistory.order_id orderId, transactionHistory.created_on createdOn," +
            " transactionHistory.subscriber_identification_number serviceNumber," +
            " transactionHistory.transaction_amount txnAmount" +
            " FROM" +
            " user_transaction_history transactionHistory where transactionHistory.order_id=:order_id and" +
            " transactionHistory.fk_user_id=:user_id and transactionHistory.transaction_status = '00' ";
    
    public List<List<Object>> getInvoiceInfoObjects(String orderId) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Integer userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);

        // TODO FIXME This works for only Postpaid now. Make it generic for all
        // bill payment types.
        String newSql = SQL_FIND_INVOICE_INFO_FOR_RECHARGE;
        
        Session session = getSession();
        Query query = session.createSQLQuery(newSql).setParameter("order_id", orderId)
                .setParameter("user_id", userId);
        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
        if (ObjectList.size() == 0) {
            logger.debug("invalid order id for which exception is throw " + orderId);
            throw new FCRuntimeException("Invalid OrderId");
        }
        return ObjectList;
    }
    
    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(HomeBusinessDao.class.getSimpleName());
    }
   
    
    public Map<String, Object> getUserRechargeDetailsFromOrderId(String orderId) {
        Map<String, Object> userDetails = new HashMap<String, Object>();
        String hql = "select txnfulfilment.email as email,"
                + "txnhomepage.serviceNumber as mobileno, txnhomepage.amount as amount ,"
                + "txnhomepage.operatorName as oprtaorName,"
                + "txnhomepage.circleName as circleName, orderid.lookupId as lookupId, "
                + "txnhomepage.productType as productType, txnfulfilment.id as txnfulfilmentid,"
                + "txnfulfilment.createdAt as createdAt, "
                + "txnhomepage.billTxnHomePage.id as fkBillTxnHomePageId, orderid.channelId as channelId "
                + "from TxnFulfilment as txnfulfilment,  "
                + "TxnHomePage as txnhomepage , OrderId as orderid "
                + "where orderid.orderId ='" + orderId + "'" + " and txnfulfilment.txnHomePageId = txnhomepage.id and "
                + "orderid.lookupId =  txnhomepage.lookupId";

        org.hibernate.Query query = getSession().createQuery(hql);
        List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();

        for (List<Object> list : ObjectList) {
            userDetails.put("email", list.get(0));
            com.freecharge.app.domain.entity.jdbc.Users users = userServiceProxy.getUserByEmailId((String)list.get(0));
            userDetails.put("userId", users.getUserId());
            userDetails.put("firstname", userServiceProxy.getUserProfile(users.getUserId()).getFirstName());
            userDetails.put("mobileno", list.get(1));
            if (FCUtil.isUtilityPaymentType((String) list.get(6))) {
                String billId = (String) userDetails.get("mobileno");
                // Update the user details for bill pay here.
                BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
                userDetails.put("mobileno", billDetails.getAdditionalInfo1());
                userDetails.put("billId", billId);
            }
            
            userDetails.put("amount", list.get(2));
            userDetails.put("operatorName", list.get(3));
            userDetails.put("circleName", list.get(4));
            userDetails.put("lookupId", list.get(5));
            userDetails.put("productType", list.get(6));
            userDetails.put("txnfulfilmentid", list.get(7));
            userDetails.put("createdAt", list.get(8));
            userDetails.put("fkBillTxnHomePageId", list.get(9));
            userDetails.put("channelId", list.get(10));
        }
        return userDetails;
    }
    
}
