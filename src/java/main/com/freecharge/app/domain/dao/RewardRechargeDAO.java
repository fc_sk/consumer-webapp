package com.freecharge.app.domain.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.RewardRecharge;

@Repository
public class RewardRechargeDAO extends CommonDao<RewardRecharge> {

    @Override
    protected Class<RewardRecharge> getDomainClass() {
        return RewardRecharge.class;
    }

    public RewardRecharge getRewardRecharge(Long rewardRechargeId) {
        return read(rewardRechargeId);
    }

    public int createRewardRecharge(RewardRecharge rewardRecharge) {
        rewardRecharge.setCreatedOn(new Timestamp(new Date().getTime()));
        return (int) this.getSession().save(rewardRecharge);
    }

    public RewardRecharge getRewardRecharge(String orderId) {
        Criteria criteria = getSession().createCriteria(RewardRecharge.class);
        criteria = criteria.add(Restrictions.eq("orderId", orderId));
        @SuppressWarnings("unchecked")
        List<RewardRecharge> rewardRecharges = criteria.list();
        return rewardRecharges != null && rewardRecharges.size() == 1 ? rewardRecharges.get(0) : null;
    }

}