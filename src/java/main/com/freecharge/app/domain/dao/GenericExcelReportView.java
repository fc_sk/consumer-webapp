package com.freecharge.app.domain.dao;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.freecharge.app.service.GenericMisDisplayRecords;

public class GenericExcelReportView extends AbstractExcelView{
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
	// throws Exception {
	{
		String filename = "C:/Users/VIPIN/Desktop/InMisdata.xls";
		try {
			List<GenericMisDisplayRecords> excelData = (List<GenericMisDisplayRecords>) model.get("excelData");
			HSSFSheet sheet = workbook.createSheet("Revenue Report");

			HSSFRow header = sheet.createRow(0);
			header.createCell(0).setCellValue("Order ID");
			header.createCell(1).setCellValue("IN_RequestID");
			header.createCell(2).setCellValue("Mobile NO");
			header.createCell(3).setCellValue("Operator_Circle");
			header.createCell(5).setCellValue("Amount");
			header.createCell(4).setCellValue("IN_Aggregator");
			header.createCell(6).setCellValue("IN Resp Code");
			header.createCell(7).setCellValue("IN Resp message");
			header.createCell(8).setCellValue("PG_RequestID");
			header.createCell(9).setCellValue("PG_Gateway");
			header.createCell(10).setCellValue("PG_Type");
			header.createCell(11).setCellValue("PG_Option");
			header.createCell(12).setCellValue("PG_Status");
			header.createCell(13).setCellValue("PG_Message");
			header.createCell(14).setCellValue("User_Email");
			header.createCell(15).setCellValue("Txn Fulfilment Status");
			header.createCell(15).setCellValue("Fulfilment updated Time");
			
			int rowNum = 1;

			if (excelData != null) {
				for (int i = 0; i < excelData.size(); i++) {
					sheet.autoSizeColumn(rowNum);
					GenericMisDisplayRecords displayRecord = (GenericMisDisplayRecords) excelData.get(i);
					HSSFRow row = sheet.createRow(rowNum++);
					row.createCell(0).setCellValue(displayRecord.getOrderID());
					row.createCell(2).setCellValue(displayRecord.getInRequestID());
					row.createCell(3).setCellValue(displayRecord.getMobileNumber());
					row.createCell(4).setCellValue(displayRecord.getOperator()+displayRecord.getCircle());
					row.createCell(5).setCellValue(displayRecord.getAmount());
					row.createCell(6).setCellValue(displayRecord.getInAggregator());
					row.createCell(7).setCellValue(displayRecord.getInResponseCodeStatus());
					row.createCell(8).setCellValue(displayRecord.getInResponseMessage());
					row.createCell(9).setCellValue(displayRecord.getPgRequestID());
					row.createCell(10).setCellValue(displayRecord.getPaymentGateway());
					row.createCell(11).setCellValue(displayRecord.getPaymentType());
					row.createCell(12).setCellValue(displayRecord.getPaymentOption());
					row.createCell(13).setCellValue(displayRecord.getPgStatusCode());
					row.createCell(14).setCellValue(displayRecord.getPgMessage());
					row.createCell(15).setCellValue(displayRecord.getUserEmailD());
					row.createCell(15).setCellValue(displayRecord.getUserTxnStatus());
					row.createCell(15).setCellValue(displayRecord.getRequestDate());
				}
				
				/*FileOutputStream fileOut =  new FileOutputStream(filename);
				workbook.write(fileOut);
				fileOut.close();
				*/

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	

}
