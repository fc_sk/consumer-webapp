package com.freecharge.app.domain.dao.jdbc.migration;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.migration.SiteUsers;
import com.freecharge.common.framework.logging.LoggingFactory;

public class MigrationWriteDao {

	private static Logger logger = LoggingFactory.getLogger(MigrationWriteDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertSiteUsers;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertSiteUsers = new SimpleJdbcInsert(dataSource).withTableName("site_users").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	public SiteUsers insertSiteUser(SiteUsers users) {
		try {
			logger.debug("Request comes into the insertSiteUser method of the class UserWriteDao, the request input parameter type Users "+users.getEmail());
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(users);
			Number number = this.insertSiteUsers.executeAndReturnKey(parameters);
			users.setId(number.intValue());
			logger.debug("Request goes out from the insertUser method of the class UserWriteDao, the request  return type is Users "+users.getId() + ", email = "+users.getEmail());
			return users;
		} catch (RuntimeException re) {
			logger.error("insertUser failed", re);
			logger.error("insertUser failed for user id: " + users.getEmail());
			throw re;
		}
	}


}
