package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.common.framework.logging.LoggingFactory;


public class UserProfileDAO extends CommonDao<UserProfile> implements IUserProfileDAO {
	// property constants
    public static final String USER_PROFILE_ID = "userProfileId";
	public static final String FK_USER_ID = "fkUserId";
	public static final String FK_USER_RECHARGE_CONTACT_ID = "fkUserRechargeContactId";
	public static final String NICK_ALIAS = "nickAlias";
	public static final String ADDRESS1 = "address1";
	public static final String LANDMARK = "landmark";
	public static final String CITY = "city";
	public static final String POSTAL_CODE = "postalCode";
	public static final String FK_STATE_MASTER_ID = "fkStateMasterId";
	public static final String FK_COUNTRY_MASTER_ID = "fkCountryMasterId";
	public static final String IS_ACTIVE = "isActive";
	public static final String IS_DEFAULT = "isDefault";

	private static Logger logger = LoggingFactory.getLogger(UserProfileDAO.class);



    protected Class<UserProfile> getDomainClass() {
        return UserProfile.class;
    }

	/**
	 * Find all UserProfile entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the UserProfile property to query
	 * @param value
	 *            the property value to match
	 * @return List<UserProfile> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<UserProfile> findByProperty(String propertyName,final Object value) {
		logger.debug("Request comes into the findByProperty method of the class UserProfileDAO, the request parameters type is String "+propertyName+"Object "+value);
		logger.info("finding UserProfile instance with property: "+ propertyName + ", value: " + value,null);
		try {
			final String queryString = "select model from UserProfile model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<UserProfile> list=query.list();
			logger.debug("Request goes out from the updateUserRechargeHistoryRechargeStatus method of the class UserRechargeHistoryDAO, the request  return type is List<UserProfile> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

	public List<UserProfile> findByFkUserId(Object fkUserId) {
		return findByProperty(FK_USER_ID, fkUserId);
	}

	public List<UserProfile> findByFkUserRechargeContactId(
			Object fkUserRechargeContactId) {
		return findByProperty(FK_USER_RECHARGE_CONTACT_ID,
				fkUserRechargeContactId);
	}

	public List<UserProfile> findByNickAlias(Object nickAlias) {
		return findByProperty(NICK_ALIAS, nickAlias);
	}

	public List<UserProfile> findByAddress1(Object address1) {
		return findByProperty(ADDRESS1, address1);
	}

	public List<UserProfile> findByLandmark(Object landmark) {
		return findByProperty(LANDMARK, landmark);
	}

	public List<UserProfile> findByCity(Object city) {
		return findByProperty(CITY, city);
	}

	public List<UserProfile> findByPostalCode(Object postalCode) {
		return findByProperty(POSTAL_CODE, postalCode);
	}

	public List<UserProfile> findByFkStateMasterId(Object fkStateMasterId) {
		return findByProperty(FK_STATE_MASTER_ID, fkStateMasterId);
	}

	public List<UserProfile> findByFkCountryMasterId(Object fkCountryMasterId) {
		return findByProperty(FK_COUNTRY_MASTER_ID, fkCountryMasterId);
	}

	public List<UserProfile> findByIsActive(Object isActive) {
		return findByProperty(IS_ACTIVE, isActive);
	}

	public List<UserProfile> findByIsDefault(Object isDefault) {
		return findByProperty(IS_DEFAULT, isDefault);
	}
	
	public List<UserProfile> findByUserProfileId(Object isDefault) {
        return findByProperty(USER_PROFILE_ID, isDefault);
    }
	/**
	 * Find all UserProfile entities.
	 * 
	 * @return List<UserProfile> all UserProfile entities
	 */
	@SuppressWarnings("unchecked")
	public List<UserProfile> findAll() {
		logger.debug("Request comes into the findAll method of the class UserProfileDAO, the request has no input parameters ");
		logger.info("finding all UserProfile instances",null);
		try {
			final String queryString = "select model from UserProfile model";
			Query query = getSession().createQuery(queryString);
			List<UserProfile> list=query.list();
			logger.debug("Request goes out from the findAll method of the class UserRechargeHistoryDAO, the request  return type is List<UserProfile> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}
	}
	
	public List<UserProfile> findByUserIdAndIsDefalut(Integer userId,boolean isDefaultValue){
		logger.debug("Request comes into the findByProperty method of the class UserProfileDAO, the request parameters type is Integer "+userId+"boolean "+isDefaultValue);
		logger.info("finding UserProfile instance with property: userId :"+ userId + ", isDefalutvalue: " + isDefaultValue,null);
		try {
			final String queryString = "select model from UserProfile model where model.fkUserId= :propertyValue and model.isDefault ="+isDefaultValue;
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", userId);
			List<UserProfile> list=query.list();
			logger.debug("Request goes out from the findAll method of the class UserRechargeHistoryDAO, the request  return type is List<UserProfile> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}
	
	public List<UserProfile> findByUserId(Integer userId){
		logger.info("finding UserProfile instance with property: userId :"+ userId );
		try {
			final String queryString = "select model from UserProfile model where model.fkUserId= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", userId);
			List<UserProfile> list=query.list();
			logger.debug("Request goes out from the findAll method of the class UserRechargeHistoryDAO, the request  return type is List<UserProfile> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

}