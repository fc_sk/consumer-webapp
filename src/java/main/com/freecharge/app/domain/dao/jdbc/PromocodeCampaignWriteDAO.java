package com.freecharge.app.domain.dao.jdbc;

import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;
import com.freecharge.common.framework.logging.LoggingFactory;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 8/11/12 Time: 8:14 PM To change this template use File |
 * Settings | File Templates.
 */
public class PromocodeCampaignWriteDAO {

	private static Logger logger = LoggingFactory.getLogger(PromocodeCampaignWriteDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	private SimpleJdbcInsert insertPromocodeCampaign;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocodeCampaign = new SimpleJdbcInsert(dataSource).withTableName("promocode_campaign").usingGeneratedKeyColumns("promocode_campaign_id", "n_last_updated", "n_created");
	}

	public Integer insertPromocodeCampaign(PromocodeCampaign promocodeCampaign) {
		SqlParameterSource parameters = promocodeCampaign.getMapSqlParameterSource();
		Number returnParam = this.insertPromocodeCampaign.executeAndReturnKey(parameters);
		return returnParam.intValue();
	}
	
	public boolean blockCode(Long couponId, Long userId, String orderId) {
        
        logger.info("blocking for couponId : " + couponId + ", userId: " + userId + " orderId: " + orderId);
        
        String sql = "update promocode_campaign set status = 'BLOCKED' where orderid =:orderid ";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderid", orderId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        if (updateSuccessful) {
            logger.info("Successfully blocked couponCode for id  " +  couponId+ " orderId: " + orderId);
        } else {
            logger.info("Failed to block  couponCode for id  " +  couponId + " number of rows updated " + updateCount+ " orderId: " + orderId);
        }
        return updateSuccessful;
    }
    	
    public boolean unblockCode(Long couponCodeId, Long userId, String orderId) {
        logger.info("releaseCouponCode for couponCodeId: " + couponCodeId + ", userId: " + userId + " orderId: " + orderId);
        
        String sql = "update promocode_campaign set status=null "+
                " where orderid=:orderid and status != 'REDEEMED'";
                
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderid", orderId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        
        if (updateSuccessful) {
            logger.info("Successfully released couponCode for id  " +  couponCodeId + " orderId: " + orderId);
        } else {
            logger.info("Failed to release  couponCode for id  " +  couponCodeId + " number of rows updated " + updateCount + " orderId: "+ orderId);
        }

        return updateSuccessful;
    }
    
    public boolean redeemCode(Long couponId, Long userId, String orderId) {
        logger.info("Redeeming for couponId : " + couponId + ", userId: " + userId + " orderId: " + orderId);
        
        String sql = "update promocode_campaign set status = 'REDEEMED' "+
                " where orderid=:orderid and status = 'BLOCKED' ";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderid", orderId);
        
        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        if (updateSuccessful) {
            logger.info("Successfully redeemed couponCode for id  " +  couponId + " orderId: " + orderId);
        } else {
            logger.info("Failed to redeem  couponCode for id  " +  couponId + " number of rows updated " + updateCount + " orderId: " + orderId);
        }
        return updateSuccessful;
    }

    public boolean updateOrderId(String lookupId, String orderId) {
        String sql = "update promocode_campaign set orderid = :orderId "+
                " where orderid=:lookupId and status = 'BLOCKED' ";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("lookupId", lookupId);
        paramMap.put("orderId", orderId);

        int updateCount = this.jdbcTemplate.update(sql, paramMap);
        final boolean updateSuccessful = updateCount != 0;
        return updateSuccessful;
    }
}
