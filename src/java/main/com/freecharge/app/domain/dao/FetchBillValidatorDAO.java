package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.mappers.FetchBillValidatorMapper;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;

@Component
public class FetchBillValidatorDAO {

	private NamedParameterJdbcTemplate jt;
	private final Logger logger = Logger.getLogger(FetchBillValidatorDAO.class);

	private final String SELECT_VALIDATOR_QUERY = "select * from fetchbill_validator where fk_operator_id =:fk_operator_id";

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jt = new NamedParameterJdbcTemplate(dataSource);
	}

	public BillPayValidator getValidator(String operatorId) {
		// TODO Auto-generated method stub
		Map<String, String> insertParams = new HashMap<>();
		try {
			insertParams.put("fk_operator_id", operatorId);
			List<BillPayValidator> validators = jt.query(SELECT_VALIDATOR_QUERY, insertParams,
					new FetchBillValidatorMapper());
			return validators.get(0);
		} catch (Exception e) {
			logger.error("Error caught while fetching validator for operatorId:" + operatorId);
		}
		return null;
	}

}
