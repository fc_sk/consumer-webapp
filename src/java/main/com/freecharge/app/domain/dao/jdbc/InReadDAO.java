package com.freecharge.app.domain.dao.jdbc;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.domain.entity.jdbc.mappers.InErrorcodeMapMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InRechargeRetryMapMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InRequestMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InResponseMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InTransactionsMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.RechargeAttemptMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.RechargeRetryDataMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.timeline.event.RechargeAttempt;

public class InReadDAO {
    private static Logger logger = LoggingFactory.getLogger(InReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate slaveJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    @Autowired
    public void setSlaveDataSource(DataSource dataSource) {
        this.slaveJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public InRequest getInRequest(Long requestId) {
        try {
            logger.debug("Request comes into the getInRequest method of the class InReadDAO, the request parameter types are  Long " + requestId);
            String queryString = "select * from in_request where request_id = :requestId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("requestId", requestId);

            List<InRequest> requests = this.jdbcTemplate.query(queryString, paramMap, new InRequestMapper());
            logger.debug("Request goes out from the getInRequest method of the class InReadDAO, the request  return type is InRequest " + requests);
            if (requests != null && requests.size() > 0)
                return requests.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public InRequest getInRequest(String affiliateTransId) {
        logger.debug("Request comes into the getInRequest method of the class InReadDAO, the request parameter types are  String " + affiliateTransId);
        String queryString = "select * from in_request where affiliate_trans_id = :affiliateTransId order by request_id desc";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("affiliateTransId", affiliateTransId);

        List<InRequest> requests = this.jdbcTemplate.query(queryString, paramMap, new InRequestMapper());
        logger.debug("Request goes out from the getInRequest method of the class InReadDAO, the request  return type is InRequest " + requests);
        if (requests != null && requests.size() > 0) {
            return requests.get(0);
        } else {
            return null;
        }
    }
    
    public List<InRequest> getAllInRequests(String orderId) {
        String queryString = "select * from in_request where affiliate_trans_id = :affiliateTransId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("affiliateTransId", orderId);

        return this.jdbcTemplate.query(queryString, paramMap, new InRequestMapper());
    }

    public List<RechargeAttempt> getAllRechargesForEmail(String email) {
        String queryString = "select ir.request_id, ir.affiliate_id, ir.affiliate_trans_id, ir.product_type, ir.subscriber_number, ir.operator, ir.circle, ir.amount, ir.created_time, ir.updated_time, ir.raw_request, ir.recharge_plan, irs.aggr_name, irs.in_resp_code, irs.in_resp_msg " 
        	+ " from txn_fulfilment tf"
        	+ " inner join txn_home_page thp on tf.txn_home_page_id = thp.id"
        	+ " inner join order_id oi on oi.lookup_id = thp.lookup_id"
        	+ " inner join in_request ir on oi.order_id = ir.affiliate_trans_id"
        	+ " inner join in_response irs on ir.request_id = irs.request_id"
        	+ " where tf.email=:email"
        	+ " order by ir.created_time desc";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("email", email);

        return this.slaveJdbcTemplate.query(queryString, paramMap, new RechargeAttemptMapper());
    }
    
    public List<RechargeAttempt> getAllRechargesForEmailFromDate(String email, Timestamp fromDate, Timestamp toDate) {
    	 String queryString = "select ir.request_id, ir.affiliate_id, ir.affiliate_trans_id, ir.product_type, ir.subscriber_number, ir.operator, ir.circle, ir.amount, ir.created_time, ir.updated_time, ir.raw_request, ir.recharge_plan, irs.aggr_name, irs.in_resp_code, irs.in_resp_msg " 
    	        	+ " from txn_fulfilment tf"
    	        	+ " inner join txn_home_page thp on tf.txn_home_page_id = thp.id"
    	        	+ " inner join order_id oi on oi.lookup_id = thp.lookup_id"
    	        	+ " inner join in_request ir on oi.order_id = ir.affiliate_trans_id"
    	        	+ " inner join in_response irs on ir.request_id = irs.request_id"
            	+ " where tf.email=:email and tf.created_at>=:fromDate and tf.created_at<=:toDate"
            	+ " order by ir.created_time desc";
        
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("email", email);
        paramMap.put("fromDate", fromDate);
        paramMap.put("toDate", toDate);

        return this.slaveJdbcTemplate.query(queryString, paramMap, new RechargeAttemptMapper());
    }
    
    public InResponse getInResponseByRequestId(Long requestId) {
        try {
            logger.debug("Request comes into the getInResponseByRequestId method of the class InReadDAO, the request parameter types are  Long " + requestId);
            String queryString = "select * from in_response where request_id = :requestId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("requestId", requestId);

            List<InResponse> responses = this.jdbcTemplate.query(queryString, paramMap, new InResponseMapper());
            logger.debug("Request goes out from the getInResponseByRequestId method of the class InReadDAO, the request  return type is InResponse " + responses);
            if (responses != null && responses.size() > 0)
                return responses.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public InResponse getInResponse(Long responseId) {
        try {
            logger.debug("Request comes into the getInResponse method of the class InReadDAO, the request parameter types are  Long " + responseId);
            String queryString = "select * from in_response where response_id = :responseId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("responseId", responseId);

            List<InResponse> responses = this.jdbcTemplate.query(queryString, paramMap, new InResponseMapper());
            logger.debug("Request goes out from the getInResponse method of the class InReadDAO, the request  return type is InResponse " + responses);
            if (responses != null && responses.size() > 0)
                return responses.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public InResponse getInResponseStatus(Long requestId) {
        try {
            logger.debug("Request comes into the getInResponseStatus method of the class InReadDAO, the request parameter types are  Long " + requestId);
            String queryString = "select * from in_response where request_id = :requestID";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("requestID", requestId);

            List<InResponse> responses = this.jdbcTemplate.query(queryString, paramMap, new InResponseMapper());
            logger.debug("Request goes out from the getInResponse method of the class InReadDAO, the request  return type is InResponse " + responses);
            if (responses != null && responses.size() > 0)
                return responses.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public static final String GET_RESPONSE_BY_ORDER_ID_SQL = "" + "SELECT p.* FROM in_response p, in_request q" + " WHERE p.request_id = q.request_id AND q.affiliate_trans_id = :order_id";

    private final String method_getInResponseByOrderID = "" + getClass().getName() + ".getInResponseByOrderID";

    public InResponse getInResponseByOrderID(String orderID) {
        logger.debug("Entered " + method_getInResponseByOrderID + ", orderID=" + orderID);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("order_id", orderID);
        List<InResponse> responses = this.jdbcTemplate.query(GET_RESPONSE_BY_ORDER_ID_SQL, params, new InResponseMapper());
        logger.debug("Exiting " + method_getInResponseByOrderID + ", return type is InResponse: " + responses);
        if (responses != null && !responses.isEmpty()) {
            return FCUtil.getLatestResponse(responses);
        } else {
            return null;
        }
    }
    
    public List<InResponse> getAllInResponsesForOrderID(String orderID) {
        logger.debug("Entered " + method_getInResponseByOrderID + ", orderID=" + orderID);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("order_id", orderID);
        List<InResponse> responses = this.jdbcTemplate.query(GET_RESPONSE_BY_ORDER_ID_SQL, params, new InResponseMapper());
        logger.debug("Exiting " + method_getInResponseByOrderID + ", return type is InResponse: " + responses);
        return responses;
    }
    
    
    public List<InTransactions> getInTransactionsByRequestId(Long requestId) {
        logger.debug("Request comes into the getInTransactionsByRequestId method of the class InReadDAO, the request parameter types are  Long " + requestId);
        String queryString = "select * from in_transactions where fk_request_id = :requestId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("requestId", requestId);

        List<InTransactions> transactions = this.jdbcTemplate.query(queryString, paramMap, new InTransactionsMapper());
        logger.debug("Request goes out from the getInTransactionsByRequestId method of the class InReadDAO, the request  return type is List<InTransactions> " + transactions);
        return transactions;
    }
    
        
    public List<InTransactions> getSortedInTransactionsByRequestId(Long requestId) {
        String queryString = "select * from in_transactions where fk_request_id = :requestId order by request_time desc";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("requestId", requestId);

        List<InTransactions> transactions = this.jdbcTemplate.query(queryString, paramMap, new InTransactionsMapper());
        return transactions;
    }

    public InTransactions getInTransactions(Long transactionId) {
        try {
            logger.debug("Request comes into the getInTransactions method of the class InReadDAO, the request parameter types are  Long " + transactionId);
            String queryString = "select * from in_response where transaction_id = :transactionId";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("transactionId", transactionId);

            List<InTransactions> transactions = this.jdbcTemplate.query(queryString, paramMap, new InTransactionsMapper());
            logger.debug("Request goes out from the getInTransactions method of the class InReadDAO, the request  return type is InTransactions " + transactions);
            if (transactions != null && transactions.size() > 0)
                return transactions.get(0);
            else
                return null;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public InTransactionData getInTransactionData(Long requestId) {
        InTransactionData data = new InTransactionData();

        data.setInRequest(getInRequest(requestId));
        data.setInResponse(getInResponseByRequestId(requestId));
        data.setTransactions(getInTransactionsByRequestId(requestId));

        return data;
    }

    public InTransactionData getInTransactionData(String affiliateTransId) {
        InTransactionData data = new InTransactionData();

        InRequest inRequest = getInRequest(affiliateTransId);

        if (inRequest == null) {
            return null;
        }

        data.setInRequest(inRequest);
        data.setInResponse(getInResponseByRequestId(data.getInRequest().getRequestId()));
        data.setTransactions(getInTransactionsByRequestId(data.getInRequest().getRequestId()));

        return data;
    }

    public List<InErrorcodeMap> getErrorCodeMap(String aggrRespCode, String aggrName) {
        logger.debug("Request comes into the getErrorCodeMap method of the class InReadDAO, the request parameter types are  String " + aggrRespCode + " String " + aggrName);
        logger.info("finding all InErrorcodeMap instances(FindByAggrRespCodeAndAggrName)", null);
        try {
            String queryString = "select * from in_errorcode_map where aggr_resp_code = :aggrRespCode and aggr_name = :aggrName";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("aggrRespCode", aggrRespCode);
            paramMap.put("aggrName", aggrName);

            List<InErrorcodeMap> errorCodeMapList = this.jdbcTemplate.query(queryString, paramMap, new InErrorcodeMapMapper());
            logger.debug("Request goes out from the getErrorCodeMap method of the class InReadDAO, the request  return type is List<InErrorcodeMap> " + errorCodeMapList);
            return errorCodeMapList;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public List<InErrorcodeMap> getErrorCodeMap() {
        logger.debug("Request comes into the getErrorCodeMap method of the class InReadDAO, the request has no parameter ");
        logger.info("finding all InErrorcodeMap instances(FindByAggrRespCodeAndAggrName)", null);
        try {
            String queryString = "select * from in_errorcode_map ";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            List<InErrorcodeMap> errorCodeMapList = this.jdbcTemplate.query(queryString, paramMap, new InErrorcodeMapMapper());
            logger.debug("Request goes out from the getErrorCodeMap method of the class InReadDAO, the request  return type is List<InErrorcodeMap> " + errorCodeMapList);
            return errorCodeMapList;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
    
    /*Adding method to read aggr & operator retry details if failed due to invalid mobile number*/
    
    public List<InRechargeRetryMap> getOperatorRetryConfig(String aggrRespCode, String aggrName,String operatorId, boolean isOperatorRetryForAllError) {
        logger.debug("Request comes into the getInRechargeRetryDetails method of the class InReadDAO, the request parameter types are  String " + aggrRespCode + " String " + aggrName+ " String " + operatorId);
        try { 
            String operatorRetryForAllError = isOperatorRetryForAllError ? ", '*'" : "";
            String queryString =   "  select id,aggr_name,aggr_resp_code,fk_operator_master_id,is_retryable,fk_retry_operator_master_Id,"
 		                         + " om.operator_code as retry_operator_Name,fk_product_master_id as retry_product_Id,retry_priority from in_recharge_retry as irr "
 		                         + ",operator_master as om where irr.fk_retry_operator_master_id=om.operator_master_id "
 		                         + " and aggr_resp_code in (:aggrRespCode " + operatorRetryForAllError + " ) and aggr_name = :aggrName and fk_operator_master_id = :operatorId order by irr.retry_priority";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("aggrRespCode", aggrRespCode);
            paramMap.put("aggrName", aggrName);
            paramMap.put("operatorId", operatorId);

            List<InRechargeRetryMap> inRechargeRetryMapList = this.jdbcTemplate.query(queryString, paramMap, new InRechargeRetryMapMapper());
            logger.debug("Request goes out from the getInRechargeRetryDetails method of the class InReadDAO, the request  return type is List<InRechargeRetryMap> " + inRechargeRetryMapList);
            return inRechargeRetryMapList;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
    
    public List<RechargeRetryData> getRechargeRetryDataForOrder(String orderId) {
        try { 
            String queryString =   "select retry_id, order_id, product_type, retry_Operator_Name, final_retry, "
                    + "created_at, updated_at, aggr_trans_id, opr_trans_id, aggr_name, aggr_resp_code, aggr_resp_msg, "
                    + "in_resp_code, in_resp_msg from recharge_retry_data where order_id = :order_id";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("order_id", orderId);

            List<RechargeRetryData> rechargeRetryDatas = this.jdbcTemplate.query(queryString, paramMap, new RechargeRetryDataMapper());
            
            return rechargeRetryDatas;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
    
    public List<InRechargeRetryMap> getAllOperatorRetryConfig() {
        logger.debug("Request comes into the getInRechargeRetryDetails method of the class InReadDAO, the request has no parameter ");
        try {
            String queryString = "select id,aggr_name,aggr_resp_code,fk_operator_master_id,is_retryable,fk_retry_operator_master_Id,"
            		           + " om.operator_code as retry_operator_Name,fk_product_master_id as retry_product_Id,retry_priority from in_recharge_retry as irr "
            		           + ",operator_master as om where irr.fk_retry_operator_master_id=om.operator_master_id order by irr.retry_priority ";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            List<InRechargeRetryMap> inRechargeRetryMapList = this.jdbcTemplate.query(queryString, paramMap, new InRechargeRetryMapMapper());
            logger.debug("Request goes out from the getInRechargeRetryDetails method of the class InReadDAO, the request  return type is List<InRechargeRetryMap> " + inRechargeRetryMapList);
            return inRechargeRetryMapList;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    public List<InResponse> getInResponsesByRequestId(Long requestID) {
        logger.debug("Request comes into the getInResponsesWithTimeRange method of the class InReadDAO, the request has no parameter ");
        try {
            final String queryString = "select * from in_response where request_id = :requestId";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("requestId", requestID);

            List<InResponse> inResponses = this.jdbcTemplate.query(queryString, paramMap, new InResponseMapper());
            logger.debug("Request goes out from the getInResponsesWithTimeRange method of the class InReadDAO, the request  return type is List<InResponse> " + inResponses.size());
            return inResponses;

        } catch (RuntimeException re) {
            logger.error("Exception occured during retry schedular", re);
            throw re;
        }
    }

    public InTransactions getLatestInTransactionsByRequestIdAndAggregaor(Long requestId, String aggrName) {

        logger.debug("Request comes into the getInTransactionsByRequestId method of the class InReadDAO, the request parameter types are  Long " + requestId);
        String queryString = "select * from in_transactions where fk_request_id = :requestId and aggr_name = :aggrName order by request_time desc limit 1";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("requestId", requestId);
        paramMap.put("aggrName", aggrName);

        List<InTransactions> transactions = this.jdbcTemplate.query(queryString, paramMap, new InTransactionsMapper());
        logger.debug("Request goes out from the getInTransactionsByRequestIdAndAggregator method of the class InReadDAO, the request  return type is List<InTransactions> " + transactions);
        if (transactions != null  && transactions.size() > 0) {
            return transactions.get(0);
        }

        return null;
    }
    

    public Object fetchAgPreferenceByOperatorId(Integer operatorId) {
           SqlRowSet srs = null;
           try {
           String sqlQuery = "select * from aggregator_preference where operator_id = :operatorId";
           Map<String, Object> paramMap = new HashMap<String, Object>();
           paramMap.put("operatorId", operatorId);
           srs =  this.jdbcTemplate.queryForRowSet(sqlQuery, paramMap);
           } catch(Exception exception) {
              logger.error("Exception while fetching aggregatorPreference operatorId "+operatorId+"" , exception);
           }
           return srs;
    }
    
    public  List<Map<String,Object>> fetchAgPreferenceObj() {
           List<Map<String,Object>> maps = null;
           try {
           String sqlQuery = "select * from aggregator_preference";
           Map<String, Object> paramMap = new HashMap<String, Object>();
           maps =  this.jdbcTemplate.queryForList(sqlQuery, paramMap);
           } catch(Exception exception) {
              logger.error("Exception while fetching aggregatorPreference Obj" , exception);
           }
           return maps;
    }

}
