package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.winpin.CouponPin;

/**
 * Interface for CouponPinDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ICouponPinDAO {
	
	/**
	 * Find all CouponPin entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CouponPin property to query
	 * @param value
	 *            the property value to match
	 * @return List<CouponPin> found by query
	 */
	public List<CouponPin> findByProperty(String propertyName, Object value);

	public List<CouponPin> findByFkCoupanTypeId(Object fkCoupanTypeId);

	public List<CouponPin> findByCfActionStatus(Object cfActionStatus);

	public List<CouponPin> findByCfActiveFlag(Object cfActiveFlag);

	public List<CouponPin> findByCfArchiveStatus(Object cfArchiveStatus);

	public List<CouponPin> findByCfCreatedById(Object cfCreatedById);

	public List<CouponPin> findByCfLastUpdatedById(Object cfLastUpdatedById);

	public List<CouponPin> findByCpnActive(Object cpnActive);

	public List<CouponPin> findByCpnBlock(Object cpnBlock);

	public List<CouponPin> findByCpnUsed(Object cpnUsed);

	public List<CouponPin> findByCpnInRecharge(Object cpnInRecharge);

	public List<CouponPin> findByCpnIsRedeemed(Object cpnIsRedeemed);

	public List<CouponPin> findByCpnLockStatus(Object cpnLockStatus);

	public List<CouponPin> findByCpnPin(Object cpnPin);

	public List<CouponPin> findByCpnSrNo(Object cpnSrNo);

	public List<CouponPin> findByCpnUsable(Object cpnUsable);

	public List<CouponPin> findByOrderId(Object orderId);

	public List<CouponPin> findByPgStatus(Object pgStatus);

	public List<CouponPin> findByRechargeStatus(Object rechargeStatus);

	public List<CouponPin> findByUniqueId(Object uniqueId);

    public CouponPin findOneCpnPin(String cpnPin);

	/**
	 * Find all CouponPin entities.
	 *
	 * @return List<CouponPin> all CouponPin entities
	 */
}