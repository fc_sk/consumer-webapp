package com.freecharge.app.domain.dao;


import java.util.List;

import com.freecharge.app.domain.entity.winpin.CouponType;

/**
 * Interface for CouponTypeDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface ICouponTypeDAO {
	
	/**
	 * Find all CouponType entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the CouponType property to query
	 * @param value
	 *            the property value to match
	 * @return List<CouponType> found by query
	 */
	public List<CouponType> findByProperty(String propertyName, Object value);

	public List<CouponType> findByType(Object type);

	public List<CouponType> findByName(Object name);

	/**
	 * Find all CouponType entities.
	 *
	 * @return List<CouponType> all CouponType entities
	 */
	public List<CouponType> findAll();
}