package com.freecharge.app.domain.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

import com.freecharge.app.domain.entity.TxnFulfilment;

public class TxnFulfilmentReadDAO extends CommonDao<TxnFulfilment> {

	protected Class<TxnFulfilment> getDomainClass() {
		return TxnFulfilment.class;
	}

	/* For fetching txn_fulfilmeent by email and from date */
	public List<String> getTxnHomePageIdByEmailFromDate(String email, Timestamp fromDateTime, Timestamp toDateTime) {
		String sql = "select txf.txnHomePageId from TxnFulfilment txf where txf.email=:email and txf.createdAt>=:fromDate and txf.createdAt<= :toDate";
		Query query = getSession().createQuery(sql);
		query.setParameter("email", email);
		query.setParameter("fromDate", fromDateTime);
		query.setParameter("toDate", toDateTime);
		List<Integer> list = query.list();
		List<String> txnHomePageIdList = new ArrayList<String>();
		for (Integer txnId : list) {
			if (txnId != null && !(txnId.toString().isEmpty())) {
				txnHomePageIdList.add(txnId.toString());
			}
		}
		return txnHomePageIdList;
	}

	/* For fetching txn_fulfilmeent by email */
	public List<String> getTxnHomePageIdByEmail(String email) {
		String sql = "select txf.txnHomePageId from TxnFulfilment txf where txf.email=:email";
		Query query = getSession().createQuery(sql);
		query.setParameter("email", email);
		List<Integer> list = query.list();

		List<String> txnHomePageIdList = new ArrayList<String>();	 
		for (Integer txnId : list) {
			if (txnId != null && !(txnId.toString().isEmpty())) {
				txnHomePageIdList.add(txnId.toString());
			}
		}
		return txnHomePageIdList;
	}
}