package com.freecharge.app.domain.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.logging.LoggingFactory;

public class StateMasterDAO extends CommonDao<StateMaster> implements IStateMasterDAO {

	private static Logger logger = LoggingFactory.getLogger(StateMasterDAO.class);
	
	
	@Override
	public List<StateMaster> findByProperty(String propertyName, Object value) {

		logger.debug("Request comes into the findByProperty method of the class StateMasterDAO, the request input parameter is "+ propertyName + ", value: " + value, null);
		try {
			final String queryString = "select model from StateMaster model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<StateMaster> list=query.list();
			logger.debug("Request goes out from the findByProperty method of the class StateMasterDAO, the request  return type is List<StateMaster> "+list);
			return list;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}

	}
	
	@Transactional
	public List<StateMaster> findByState(String stateName) {
	    return findByProperty("stateName", stateName);
	}

	@Override
	protected Class<StateMaster> getDomainClass() {
		return StateMaster.class;
	}

}
