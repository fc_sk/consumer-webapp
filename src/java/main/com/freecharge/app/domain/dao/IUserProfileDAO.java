package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.UserProfile;

/**
 * Interface for UserProfileDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IUserProfileDAO {
	

	/**
	 * Find all UserProfile entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the UserProfile property to query
	 * @param value
	 *            the property value to match
	 * @return List<UserProfile> found by query
	 */
	public List<UserProfile> findByProperty(String propertyName, Object value);

	public List<UserProfile> findByFkUserId(Object fkUserId);

	public List<UserProfile> findByFkUserRechargeContactId(
			Object fkUserRechargeContactId);

	public List<UserProfile> findByNickAlias(Object nickAlias);

	public List<UserProfile> findByAddress1(Object address1);

	public List<UserProfile> findByLandmark(Object landmark);

	public List<UserProfile> findByCity(Object city);

	public List<UserProfile> findByPostalCode(Object postalCode);

	public List<UserProfile> findByFkStateMasterId(Object fkStateMasterId);

	public List<UserProfile> findByFkCountryMasterId(Object fkCountryMasterId);

	public List<UserProfile> findByIsActive(Object isActive);

	public List<UserProfile> findByIsDefault(Object isDefault);

	/**
	 * Find all UserProfile entities.
	 * 
	 * @return List<UserProfile> all UserProfile entities
	 */
	public List<UserProfile> findAll();
	
	public List<UserProfile> findByUserIdAndIsDefalut(Integer userId,boolean isDefaultValue);
}