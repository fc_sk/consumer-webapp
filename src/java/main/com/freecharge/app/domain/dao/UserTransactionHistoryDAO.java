package com.freecharge.app.domain.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.mappers.UserTransactionHistoryMapper;
import com.freecharge.batch.job.recon.StatusChangeData;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.google.gson.Gson;

public class UserTransactionHistoryDAO {

	private NamedParameterJdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate slaveJdbcTemplate;
	private SimpleJdbcInsert insertIntoUserTransactionHistory;

	protected final Logger logger = LoggingFactory.getLogger(getClass()
			.getName());

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		insertIntoUserTransactionHistory = new SimpleJdbcInsert(dataSource)
				.withTableName("user_transaction_history")
				.usingGeneratedKeyColumns("transaction_id", "n_last_updated",
						"n_created");
	}

	@Autowired
    public void setSlaveDataSource(DataSource dataSource) {
        this.slaveJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
	
	public long insertUserTransactionHistory(UserTransactionHistory userHistory) {
		Map<String, Object> args = FCUtil.createMap("order_id",
				(Object) userHistory.getOrderId(), "fk_user_id",
				userHistory.getFkUserId(), "transaction_amount",
				userHistory.getTransactionAmount(),
				"subscriber_identification_number",
				userHistory.getSubscriberIdentificationNumber(),
				"product_type", userHistory.getProductType(),
				"service_provider", userHistory.getServiceProvider(),
				"service_region", userHistory.getServiceRegion(),
				"transaction_status", userHistory.getTransactionStatus(),
				"created_on", new Timestamp(new Date().getTime()),
				"last_updated", new Timestamp(new Date().getTime()),
				"metadata", userHistory.getMetadata());
		return (Long) insertIntoUserTransactionHistory
				.executeAndReturnKey(args);
	}
	
	public UserTransactionHistory createUserTransactionHistory(
			Map<String, Object> userDetail, String orderId,
			String inResponseCode) {
		try {
			
//			Gson serializer = new Gson();
//	        Map<String, Object> metaData = new HashMap();
//	        
//	        String BBPSReferenceNumber = userDetail.get("BBPSReferenceNumber") == null ? "" : (String) userDetail.get("BBPSReferenceNumber");
//	        metaData.put("BBPSReferenceNumber", BBPSReferenceNumber);
			UserTransactionHistory userTransactionHistoryObj = new UserTransactionHistory();
			userTransactionHistoryObj.setOrderId(orderId);
			userTransactionHistoryObj.setTransactionAmount(((Float) (userDetail
					.get("amount"))).doubleValue());
			userTransactionHistoryObj
					.setSubscriberIdentificationNumber((String) userDetail
							.get("mobileno"));
			userTransactionHistoryObj.setProductType((String) userDetail
					.get("productType"));
			userTransactionHistoryObj.setServiceProvider(((userDetail
                    .get("operatorName") == null) ? "" : (String) userDetail
                    .get("operatorName")));
			userTransactionHistoryObj.setServiceRegion(((userDetail
					.get("circleName") == null) ? "" : (String) userDetail
					.get("circleName")));
//			userTransactionHistoryObj.setMetadata(serializer.toJson(metaData));
			userTransactionHistoryObj.setTransactionStatus(inResponseCode);
			userTransactionHistoryObj.setFkUserId((Integer) userDetail
					.get("userId"));
			return userTransactionHistoryObj;
		} catch (IllegalStateException exception) {
			logger.info("User ID is null during creation of userTransaction history "
					+ orderId);
		} catch (Exception exception) {
			logger.info("An exception occured during creation of userTransaction history "
					+ orderId);
		}
		return null;
	}

	public UserTransactionHistory findUserTransactionHistoryByOrderId(
			String orderID) {
		String queryString = "SELECT * FROM user_transaction_history WHERE order_id = :orderId";

		Map<String, String> params = Collections.singletonMap("orderId",
				orderID);
		List<UserTransactionHistory> rows = jdbcTemplate.query(queryString,
				params, new UserTransactionHistoryMapper());
		return rows.isEmpty() ? null : rows.get(0);
	}

	public void updateUserTransactionHistory(String orderId, String response,
			Map<String, Object> userDetail) {
		String updateString = "update user_transaction_history set product_type = :productType, service_provider = :operatorName,service_region = :circleName, transaction_status = :transactionStatus  WHERE order_id = :orderId";

		Map<String, Object> namedParameters = new HashMap<String, Object>();
//		if(userDetail.get("BBPSReferenceNumber") != null) {
//			Gson serializer = new Gson();
//	        Map<String, Object> metaData = new HashMap();
//	        metaData.put("BBPSReferenceNumber", userDetail.get("BBPSReferenceNumber"));
//	        namedParameters.put("metadata", serializer.toJson(metaData));
//		} else {
//			namedParameters.put("metadata", null);
//		}
		
		namedParameters.put("orderId", orderId);
		namedParameters.put("transactionStatus", response);
		namedParameters.put("circleName", userDetail.get("circleName") == null ? "" : userDetail.get("circleName"));
		namedParameters.put("operatorName", userDetail.get("operatorName") == null ? "" : userDetail.get("operatorName"));
		namedParameters.put("productType", userDetail.get("productType"));
		jdbcTemplate.update(updateString, namedParameters);
	}
	
    public void updateStatus(String orderId, String response) {
        String updateString = "update user_transaction_history set transaction_status = :transactionStatus WHERE order_id = :orderId";

        Map<String, Object> namedParameters = new HashMap<String, Object>();
        namedParameters.put("orderId", orderId);
        namedParameters.put("transactionStatus", response);
        jdbcTemplate.update(updateString, namedParameters);
    }

	public List<String> findOrderIdByUserId(Integer userId, int limit) {
        String queryString = "SELECT order_id FROM user_transaction_history uth "
                + "WHERE uth.fk_user_id = :userId "
                + "ORDER BY uth.last_updated DESC LIMIT " + limit;

        Map<String, Object> namedParameters = new HashMap<String, Object>();
        namedParameters.put("userId", userId);

        List<String> rows = slaveJdbcTemplate.queryForList(queryString,
                namedParameters, String.class);
        return rows.isEmpty() ? null : rows;
    }
	
	public int countOfTransactionByUserForOption(Integer userId, String orderId , String paymentOption) {
		
		
		Map<String, Object> namedParameters = new HashMap<String, Object>();
		namedParameters.put("userId", userId);
        namedParameters.put("paymentOption", paymentOption);
        String queryString = "";
		if(orderId == null || orderId.isEmpty())
		{
			queryString = "select count(1) from user_transaction_history uth inner join payment_txn ptxn on ptxn.order_id = uth.order_id where uth.fk_user_id = :userId and uth.transaction_status = '00'  "
	        		+ " and ptxn.payment_option =:paymentOption and ptxn.is_successful = 1";
		}
		else
		{
			queryString = "select count(1) from user_transaction_history uth inner join payment_txn ptxn on ptxn.order_id = uth.order_id where uth.fk_user_id = :userId and uth.transaction_status = '00'  "
        		+ " and ptxn.payment_option =:paymentOption and uth.order_id != :orderId and ptxn.is_successful = 1";
			namedParameters.put("orderId", orderId);
		}
        int count = slaveJdbcTemplate.queryForInt(queryString,
                namedParameters);
        
        logger.info("countOfTransactionByUserForOption " + namedParameters + "  :: Count Value : "+ count);
				
        return count;
    }

	public List<String> findSuccessfulOrderIdByUserEmail(Long userId,
			int limit) {
		String queryString = "SELECT order_id FROM user_transaction_history uth "
				+ "WHERE uth.fk_user_id = :userId AND transaction_status = '00' "
				+ "ORDER BY uth.last_updated DESC LIMIT " + limit;

		Map<String, Object> namedParameters = new HashMap<String, Object>();
		namedParameters.put("userId", userId);

		List<String> rows = slaveJdbcTemplate.queryForList(queryString,
				namedParameters, String.class);
		return rows.isEmpty() ? null : rows;
	}

	public List<UserTransactionHistory> findUserTransactionHistoryByUserId(
			Integer userId, Timestamp fromDateTime) {
		String queryString = "SELECT * FROM user_transaction_history WHERE fk_user_id = :userID AND created_on > :fromDate";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userId);
		params.put("fromDate", fromDateTime);

		List<UserTransactionHistory> rows = jdbcTemplate.query(queryString,
				params, new UserTransactionHistoryMapper());
		return rows;
	}
	
	public List<UserTransactionHistory> findSuccessfulUTHByUserIdAndProductType(Long userId, String productType,
			Integer limit) {
		String queryString = "SELECT * FROM user_transaction_history WHERE fk_user_id = :userID AND product_type= :productType AND transaction_status = '00' ORDER BY last_updated DESC LIMIT "
				+ limit;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userId);
		params.put("productType", productType);

		List<UserTransactionHistory> rows = slaveJdbcTemplate.query(queryString, params,
				new UserTransactionHistoryMapper());
		return rows;
	}

	public List<UserTransactionHistory> findAllUserTransactionHistoryByUserId(
			Integer userId) {
		String queryString = "SELECT * FROM user_transaction_history WHERE fk_user_id = :userID";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userId);
		List<UserTransactionHistory> rows = jdbcTemplate.query(queryString,
				params, new UserTransactionHistoryMapper());
		return rows;
	}

	public int getTransactionCountSince(Integer userId, DateTime since) {
		String queryString = "SELECT count(1) FROM user_transaction_history WHERE fk_user_id = :userID AND created_on > :fromDate";

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userId);
		params.put("fromDate", new Timestamp(since.getMillis()));

		return jdbcTemplate.queryForInt(queryString, params);
	}

	public void markPendingTransactionAsFailure(String orderId) {
		String updateString = "update user_transaction_history set transaction_status = '43' WHERE order_id = :orderId and transaction_status = '08'";

		Map<String, Object> params = new HashMap<>();
		params.put("orderId", orderId);

		int rowsUpdated = jdbcTemplate.update(updateString, params);

		if (rowsUpdated != 1) {
			throw new IllegalStateException(
					"Failed to update user_transaction_history for orderID: "
							+ orderId);
		}
	}

	public List<String> getDistinctOrderIdsByUserId(int userId, int limitCount) {
		logger.debug("Getting order ids for user id: " + userId);

		String sqlquery = "select order_id from user_transaction_history where fk_user_id = :userId limit "
				+ limitCount;
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);

		List<String> ObjectList = null;
		try {
			ObjectList = slaveJdbcTemplate.queryForList(sqlquery, params,
					String.class);
		} catch (Exception e) {
			logger.error("Error getting order ids for user id: " + userId, e);
		}

		logger.debug("Request goes out from the getAllOrderIdsByUserId method of the class UserRechargeHistoryReadDAO, the request");
		return ObjectList;
	}

	public List<String> getRechargeSuccessDistinctOrderIdsByUserId(int userId,
			int limitCount) {
		logger.debug("Getting order ids for user id: " + userId);

		String sqlquery = "select order_id from user_transaction_history where transaction_status = '00' and fk_user_id = :userId limit "
				+ limitCount;
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);

		List<String> ObjectList = null;
		try {
			ObjectList = slaveJdbcTemplate.queryForList(sqlquery, params,
					String.class);
		} catch (Exception e) {
			logger.error("Error getting order ids for user id: " + userId, e);
		}

		logger.debug("Request goes out from the getAllOrderIdsByUserId method of the class UserRechargeHistoryReadDAO, the request");
		return ObjectList;
	}

	public List<String> getRechargeSuccessDistinctOrderIdsByUserId(int userId, Timestamp fromDateTime,
			int limitCount) {
		logger.debug("Getting order ids for user id: " + userId);
		String sqlquery = "select order_id from user_transaction_history where transaction_status = '00' and fk_user_id = :userId and created_on > :fromDate limit "
				+ limitCount;
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("fromDate", fromDateTime);

		List<String> ObjectList = null;
		try {
			ObjectList = slaveJdbcTemplate.queryForList(sqlquery, params,
					String.class);
		} catch (Exception e) {
			logger.error("Error getting order ids for user id: " + userId, e);
		}
		logger.debug("Request goes out from the getAllOrderIdsByUserId method of the class UserRechargeHistoryReadDAO, the request");
		return ObjectList;
	}

	public List<UserTransactionHistory> getUserTransactionHistory(int userId,
			int rechargeHistoryLimit) {

		try {
			String sqlquery = "select transaction_id, order_id, fk_user_id, transaction_amount, "
					+ "subscriber_identification_number, product_type, service_provider, service_region, "
					+ "transaction_status, created_on, last_updated, metadata from user_transaction_history "
					+ " where fk_user_id = :userId group by order_id order by created_on desc limit "
					+ rechargeHistoryLimit;
			Map<String, Object> params = new HashMap<>();
			params.put("userId", userId);

			List<UserTransactionHistory> userTransactionHistoryList = slaveJdbcTemplate
					.query(sqlquery, params, new UserTransactionHistoryMapper());
			if (userTransactionHistoryList != null
					&& userTransactionHistoryList.size() > 0)
				return userTransactionHistoryList;
			else
				return new ArrayList<UserTransactionHistory>();
		} catch (Exception exception) {
			logger.error("find getUserTransactionHistory failed for order id ",
					exception);
			throw exception;
		}
	}

	public List<UserTransactionHistory> getTransactionHistoryForCombination(
			Integer userId, String serviceNumber, int noOfMonths,
			Double minAmount, Double maxAmount) {
		String querySQL = "SELECT * FROM user_transaction_history WHERE fk_user_id=:userId AND subscriber_identification_number=:serviceNumber and created_on >:startTime";
		Map<String, Object> params = new HashMap<>();
		if (minAmount != null) {
			querySQL += " and transaction_amount >=:minAmount AND transaction_amount<=:maxAmount";
			params.put("minAmount", minAmount);
			params.put("maxAmount", maxAmount);
		}
		querySQL += " ORDER BY created_on";
		params.put("userId", userId);
		params.put("serviceNumber", serviceNumber);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - noOfMonths);
		params.put("startTime", calendar.getTime());
		return jdbcTemplate.query(querySQL, params,
				new UserTransactionHistoryMapper());
	}

    public List<UserTransactionHistory> getTransactionHistory(Integer userId, String rechargeNo,
            Date insertedOn, Double txnAmount) {
        String querySQL = "SELECT * FROM user_transaction_history WHERE fk_user_id=:userId AND subscriber_identification_number=:serviceNumber and created_on >:startTime and transaction_amount =:amount ORDER BY created_on";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("serviceNumber", rechargeNo);
        params.put("startTime", insertedOn);
        params.put("amount", txnAmount);
        return slaveJdbcTemplate.query(querySQL, params,
                new UserTransactionHistoryMapper());
    }
    
    public List<UserTransactionHistory> getSuccessfulTransactionHistoryByCombination(Integer userId, String rechargeNo,
            Date from, Date to, Float txnAmount) {
        String querySQL = "SELECT * FROM user_transaction_history WHERE fk_user_id=:userId AND subscriber_identification_number=:serviceNumber and transaction_amount =:amount and transaction_status = '00' and created_on >=:startTime and created_on <=:endTime  ORDER BY created_on";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("serviceNumber", rechargeNo);
        params.put("startTime", from);
        params.put("endTime", to);
        params.put("amount", txnAmount);
        return slaveJdbcTemplate.query(querySQL, params,
                new UserTransactionHistoryMapper());
    }

    public List<UserTransactionHistory> getBillPayUPTransactionHistory(Date from, Date to) {
        String billpayProductType = FCUtil.getUtilityProductTypes();
        String querySQL = "SELECT * FROM user_transaction_history WHERE  transaction_status = '08' "
                + "AND product_type in (" + billpayProductType + ") AND created_on >:startTime and created_on <:endTime";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", from);
        params.put("endTime", to);
        return jdbcTemplate.query(querySQL, params, new UserTransactionHistoryMapper());
    }

    public List<String> findFailedTransactionsBetween(Date from, Date to) {
        String status = FCUtil.getSuccessOrPendingOrFinalTransactionStatus();
        String querySQL = "SELECT distinct oid.order_id FROM user_transaction_history uth "
                + "JOIN order_id oid on uth.order_id=oid.order_id "
                + "WHERE uth.transaction_status not in (" + status + ") "
                + "AND uth.product_type not in ('T') AND oid.created_on >:startTime and oid.created_on <:endTime";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", from);
        params.put("endTime", to);
        return slaveJdbcTemplate.queryForList(querySQL, params, String.class);
    }

    public List<String> findPendingTransactionsBetween(Date from, Date to) {
        String querySQL = "SELECT distinct oid.order_id FROM user_transaction_history uth "
                + "JOIN order_id oid on uth.order_id=oid.order_id "
                + "WHERE  uth.transaction_status in ('08','-1') AND oid.created_on >:startTime and oid.created_on <:endTime";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", from);
        params.put("endTime", to);
        return slaveJdbcTemplate.queryForList(querySQL, params, String.class);
    }
    
    public List<StatusChangeData> findStatusChangeOrders(Date updatedDate) {
        // created_on and last_updated are stored in reverse and hence this
        // query looks misleading
        String querySQL = "SELECT order_id,uth.last_updated FROM user_transaction_history uth "
                + "WHERE  uth.created_on >:startTime and uth.created_on <:endTime and uth.last_updated < :orderCreationCutoffTime";
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", new DateTime(updatedDate).withTime(0, 0, 0, 0).toDate());
        params.put("endTime", new DateTime(updatedDate).plusDays(1).withTime(0, 0, 0, 0).toDate());
        params.put("orderCreationCutoffTime", new DateTime(updatedDate).withTime(0, 0, 0, 0).toDate());
        return slaveJdbcTemplate.query(querySQL, params, new RowMapper<StatusChangeData>() {
            @Override
            public StatusChangeData mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                StatusChangeData statusChangeData = new StatusChangeData();
                statusChangeData.setOrderId(resultSet.getString(1));
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                statusChangeData.setOrderCreationDate(sdf.format(resultSet.getDate(2)));
                return statusChangeData;
            }
        });
    }

	public List<UserTransactionHistory> findSuccessfulUTHByUserId(Long userId, int limit) {
		String queryString = "SELECT * FROM user_transaction_history WHERE fk_user_id = :userID AND transaction_status = '00' ORDER BY last_updated DESC LIMIT "
				+ limit;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userId);		

		List<UserTransactionHistory> rows = slaveJdbcTemplate.query(queryString, params,
				new UserTransactionHistoryMapper());
		return rows;
	}
}
