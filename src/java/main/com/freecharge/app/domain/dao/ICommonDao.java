package com.freecharge.app.domain.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: April 21, 2012
 * Time: 12:21:04 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ICommonDao<T extends Serializable> {
    T create(T t);
    T read(Long id);
    T update(T t);
    void delete(T t);
    List<T> findAll();
    List<T> findByHQL(String query,List paramList);
}

