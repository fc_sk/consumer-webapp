package com.freecharge.app.domain.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.UtmShortCode;
import com.freecharge.app.domain.entity.jdbc.mappers.UtmShortCodeRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmShortCodeReadDao {
    private static Logger logger = LoggingFactory.getLogger(UtmTrackerReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    
    public List<UtmShortCode> getShortCodeDetails(String shortCode) {
        List<UtmShortCode> utmShortCodeList = new ArrayList<>();
        try{
            String queryString = "select * from utm_shortcode_details where utm_short_code=:shortCode";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("shortCode", shortCode);

            utmShortCodeList = this.jdbcTemplate.query(queryString, paramMap, new UtmShortCodeRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch utm short code details for short code " + shortCode, e);
        }
        return utmShortCodeList;
    }

    public List<UtmShortCode> getAllShortCodeDetails() {
        List<UtmShortCode> utmShortCodeList = new ArrayList<>();
        try{
            String queryString = "select * from utm_shortcode_details";

            Map<String, Object> paramMap = new HashMap<>();

            utmShortCodeList = this.jdbcTemplate.query(queryString, paramMap, new UtmShortCodeRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch utm short code details for all campaigns", e);
        }
        return utmShortCodeList;
    }
}
