package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.CrossSellLeadData;
import com.freecharge.app.domain.entity.jdbc.mappers.CrossSellLeadDataRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class CrossSellLeadDataReadDAO {
	private static Logger logger = LoggingFactory.getLogger(CrossSellLeadDataReadDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	
	public List<CrossSellLeadData> getCrossSellLeadData(Integer userId, Integer crossSellId) {
		try {

			logger.debug("Request comes into the getCrossSellLeadData method of the class CrossSellLeadDataReadDAO, the request parameter type is user id =  " +userId + " cross sell id = "+crossSellId );
			String queryString = "select * from cross_sell_lead_data where fk_user_id = :userId and fk_cross_sell_id = :crossSellId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", userId);
			paramMap.put("crossSellId", crossSellId);

			List<CrossSellLeadData> crossSellLeadData = this.jdbcTemplate.query(queryString, paramMap, new CrossSellLeadDataRowMapper());
			
			logger.debug("Request goes out from the getCrossSellLeadData method of the class CrossSellLeadDataReadDAO ");

			return crossSellLeadData;
		} catch (RuntimeException re) {
			logger.error("getCrossSellLeadData failed", re);
			throw re;
		}
	}

	
	public List<CrossSellLeadData> getCrossSellLeadDataFromUserId(Integer userId) {
		try {

			logger.debug("Request comes into the getCrossSellLeadDataFromUserId method of the class CrossSellLeadDataReadDAO, the request parameter type is user id =  " +userId);
			String queryString = "select * from cross_sell_lead_data where fk_user_id = :userId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", userId);

			List<CrossSellLeadData> crossSellLeadData = this.jdbcTemplate.query(queryString, paramMap, new CrossSellLeadDataRowMapper());
			
			logger.debug("Request goes out from the getCrossSellLeadDataFromUserId method of the class CrossSellLeadDataReadDAO ");

			return crossSellLeadData;
		} catch (RuntimeException re) {
			logger.error("getCrossSellLeadDataFromUserId failed", re);
			throw re;
		}
	}

	public List<CrossSellLeadData> getCrossSellLeadDataFromCrossSellId(Integer crossSellId) {
		try {

			logger.debug("Request comes into the getCrossSellLeadDataFromCrossSellId method of the class CrossSellLeadDataReadDAO, the request parameter type is cross sell id = "+crossSellId );
			String queryString = "select * from cross_sell_lead_data where fk_cross_sell_id = :crossSellId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("crossSellId", crossSellId);

			List<CrossSellLeadData> crossSellLeadData = this.jdbcTemplate.query(queryString, paramMap, new CrossSellLeadDataRowMapper());
			
			logger.debug("Request goes out from the getCrossSellLeadDataFromCrossSellId method of the class CrossSellLeadDataReadDAO ");

			return crossSellLeadData;
		} catch (RuntimeException re) {
			logger.error("getCrossSellLeadDataFromCrossSellId failed", re);
			throw re;
		}
	}

}