package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;

public interface IStateMasterDAO {
	
	public List<StateMaster> findByProperty(String propertyName, Object value);
	public List<StateMaster> findByState(String stateName);
}
