package com.freecharge.app.domain.dao;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.mappers.GenericMisDisplayRecordRowMapper;
import com.freecharge.app.service.GenericMisDisplayRecords;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.businessdao.GenericMisData;
import com.freecharge.recharge.businessdao.INAuthenticationFetchDAOImpl;

public class GenericMisDataFetchDAOImpl implements GenericMisDataFetchDAO{

private static Logger logger = LoggingFactory.getLogger(INAuthenticationFetchDAOImpl.class);
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
	
	@Override
	public List<GenericMisDisplayRecords> getMisData(GenericMisData misRequestData, int pageCount) {
		List<GenericMisDisplayRecords> records = getMisRecords(misRequestData,pageCount);
		return records;
	}

	@Override
	public Integer getMisDataCount(GenericMisData misRequestData) {
		return  getMisRecordCounts(misRequestData);
	}
	
	public Integer getMisRecordCounts(final GenericMisData misRequestData) {
		logger.debug("Request comes into the getMisRecordCounts method of the class GenericMisDataFetchDAOImpl, the request input parameters are GenericMisData misRequestData : "+misRequestData);
		Calendar calender = Calendar.getInstance();
		calender.setTime(misRequestData.getStartDate());
		
		int year = calender.get(Calendar.YEAR);
		int month = calender.get(Calendar.MONTH);
		int date = calender.get(Calendar.DAY_OF_MONTH);
		calender.set(year, month, date, 0, 0, 0);
		misRequestData.setStartDate(calender.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 59, 59);
		misRequestData.setEndDate(endCalendar.getTime());
		
		String queryString = this.prepareMisQueryForCount(misRequestData);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Integer adminUserList =  this.jdbcTemplate.queryForInt(queryString, paramMap);
		logger.debug("Request goes out from the getMisRecordCounts method of the class GenericMisDataFetchDAOImpl, the request return type is Integer adminUserList :"+adminUserList);
		return adminUserList;
	}
	
	public String prepareMisQueryForCount(GenericMisData misRequestData)
	{
		logger.debug("Request comes into the prepareMisQueryForCount method of the class GenericMisDataFetchDAOImpl, the request input parameters are GenericMisData misRequestData : "+misRequestData);
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("SELECT count(*)  FROM in_request inr JOIN in_response inrs ON inr.request_id = inrs.request_id " +
				" JOIN payment_rq pgr ON inr.affiliate_trans_id = pgr.order_id JOIN payment_txn pgt ON pgr.payment_rq_id=pgt.fk_payment_rq_id JOIN txn_fulfilment tf ON pgt.order_id = tf.order_id " );
		
		if(misRequestData.getStartDate() != null) {
			queryString.append(firstClause ? " and " : " and ");
			
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getStartDate());
			String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		//	date = "2012-1-16 0:0:0";
			queryString.append("tf.updated_at >= '"+date+"'");
	        parameters.put("startDate",date);
	        firstClause = false;
		}
		if (misRequestData.getEndDate() != null) {
			queryString.append(" and ");
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getEndDate());
			String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
			queryString.append("tf.updated_at <= '"+date+"'");
	    }
		if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) {
			queryString.append(" and ");
			queryString.append("inr.subscriber_number = " + misRequestData.getMobileNo());
		}
		if (misRequestData.getOrderID() != null && !misRequestData.getOrderID().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inr.affiliate_trans_id = '" + misRequestData.getAppTxnID() + "'");
		}
		if (misRequestData.getInRequestID() != null) {
			queryString.append(" and ");
			queryString.append("inr.request_id = " + misRequestData.getInRequestID());
		}
		if (misRequestData.getPgRequestID() != null) {
			queryString.append(" and ");
			queryString.append("pgr.payment_rq_id = " + misRequestData.getPgRequestID());
		}
		if (misRequestData.getInAggregator() != null && !misRequestData.getInAggregator().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inrs.aggr_name = '" + misRequestData.getInAggregator()+"'");
		}
		if (misRequestData.getPaymentGateway() != null && !misRequestData.getPaymentGateway().isEmpty()) {
			queryString.append(" and ");
			queryString.append("pgr.payment_gateway = '" + misRequestData.getPaymentGateway() + "'");
		}
		if (misRequestData.getUserTxnStatus() != null) {
			queryString.append(" and ");
			queryString.append("tf.transaction_status = '" + misRequestData.getUserTxnStatus()+"'");
		}
		if(misRequestData.getPageSize() != null ) {
			queryString.append(" ORDER BY inr.request_id DESC");
		}
		logger.debug("Request goes out from the prepareMisQueryForCount method of the class GenericMisDataFetchDAOImpl, the request return type is String query :"+queryString.toString());
		  return queryString.toString();
	}

	public List<GenericMisDisplayRecords> getMisRecords(final GenericMisData misRequestData, int pageCount) {
		logger.debug("Request comes into the getMisRecords method of the class GenericMisDataFetchDAOImpl, the request input parameters are GenericMisData misRequestData : "+misRequestData+" and int pageCount:"+pageCount);
		Calendar calender = Calendar.getInstance();
		calender.setTime(misRequestData.getStartDate());
		int year = calender.get(Calendar.YEAR);
		int month = calender.get(Calendar.MONTH);
		int date = calender.get(Calendar.DAY_OF_MONTH);
		calender.set(year, month, date, 0, 0, 0);
		misRequestData.setStartDate(calender.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 59, 59);
		misRequestData.setEndDate(endCalendar.getTime());
		
		String queryString = this.prepareMisQuery(misRequestData,pageCount);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<GenericMisDisplayRecords> adminUserList =  this.jdbcTemplate.query(queryString, paramMap, new GenericMisDisplayRecordRowMapper());
		logger.debug("Request goes out from the getMisRecords method of the class GenericMisDataFetchDAOImpl, the request return type is List<GenericMisDisplayRecords> adminUserList :"+adminUserList);
		return adminUserList;
	}
	
	public String prepareMisQuery(GenericMisData misRequestData, int pageCount)
	{
		logger.debug("Request comes into the prepareMisQuery method of the class GenericMisDataFetchDAOImpl, the request input parameters are GenericMisData misRequestData : "+misRequestData+" and int pageCount:"+pageCount);
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("SELECT inr.request_id,pgr.payment_rq_id,inr.subscriber_number,inr.operator,inr.circle,inr.amount,inr.affiliate_trans_id,inrs.in_resp_code,inrs.in_resp_msg,inrs.aggr_name,"+ 
        " pgr.payment_gateway,pgr.payment_type,pgt.payment_option,pgt.is_successful,pgt.pg_message,tf.email,tf.transaction_status,tf.updated_at FROM in_request inr JOIN in_response inrs ON inr.request_id = inrs.request_id " +
		" JOIN payment_rq pgr ON inr.affiliate_trans_id = pgr.order_id JOIN payment_txn pgt ON pgr.payment_rq_id=pgt.fk_payment_rq_id JOIN txn_fulfilment tf ON pgt.order_id = tf.order_id " );
		
	if(misRequestData.getStartDate() != null) {
		queryString.append(firstClause ? " and " : " and ");
		
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getStartDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		date = "2012-1-16 0:0:0";
		queryString.append("tf.updated_at >= '"+date+"'");
        parameters.put("startDate",date);
        firstClause = false;
	}
	if (misRequestData.getEndDate() != null) {
		queryString.append(" and ");
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getEndDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		queryString.append("tf.updated_at <= '"+date+"'");
    }
	if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) {
		queryString.append(" and ");
		queryString.append("inr.subscriber_number = " + misRequestData.getMobileNo());
	}
	if (misRequestData.getOrderID() != null && !misRequestData.getOrderID().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inr.affiliate_trans_id = '" + misRequestData.getAppTxnID() + "'");
	}
	if (misRequestData.getInRequestID() != null) {
		queryString.append(" and ");
		queryString.append("inr.request_id = " + misRequestData.getInRequestID());
	}
	if (misRequestData.getPgRequestID() != null) {
		queryString.append(" and ");
		queryString.append("pgr.payment_rq_id = " + misRequestData.getPgRequestID());
	}
	if (misRequestData.getInAggregator() != null && !misRequestData.getInAggregator().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inrs.aggr_name = '" + misRequestData.getInAggregator()+"'");
		parameters.put("gateway", misRequestData.getInAggregator());
	}
	if (misRequestData.getPaymentGateway() != null && !misRequestData.getPaymentGateway().isEmpty()) {
		queryString.append(" and ");
		queryString.append("pgr.payment_gateway = '" + misRequestData.getPaymentGateway() + "'");
	}
	if (misRequestData.getUserTxnStatus() != null) {
		queryString.append(" and ");
		queryString.append("tf.transaction_status = '" + misRequestData.getUserTxnStatus()+"'");
		parameters.put("status", misRequestData.getUserTxnStatus());
	}
	if(misRequestData.getPageSize() != null ) {
		queryString.append(" ORDER BY inr.request_id DESC");
	}
		
	  if(pageCount!=500) {
	  queryString.append(" limit "+(pageCount-1)*80+", 80"); }
	  parameters.put("query", queryString.toString());
	  logger.debug("Request goes out from the getMisRecords method of the class GenericMisDataFetchDAOImpl, the request return type is String query :"+queryString.toString());
	  return queryString.toString();
	}
	
    public long getTotalTransactionByStatus(int transactionStatus) {
    	final String queryString = "select count(distinct order_id) from txn_fulfilment where transaction_status =:txnStatus";
    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	paramMap.put("txnStatus", transactionStatus);
    	long totalTxns = this.jdbcTemplate.queryForLong(queryString, paramMap);
    	return totalTxns;
    }

    public long getTotalTxnsByRechargeStatus(String aggrRespCode) {
    	final String queryString = "select count(request_id) from in_response where aggr_resp_code =:aggrRespCode";
    	Map<String, Object> paramMap = new HashMap<String, Object>();
    	paramMap.put("aggrRespCode", aggrRespCode);
    	long totalTxns = this.jdbcTemplate.queryForLong(queryString, paramMap);
    	return totalTxns;
    }
}
