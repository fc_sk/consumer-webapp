package com.freecharge.app.domain.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.mappers.CircleMasterRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.CircleOperatorMappingRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InAggrOprCircleMapMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InOprAggrPriorityMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.OperatorMasterRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.ProductMasterRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.rest.rechargeplans.plansdenomination.OperatorCircleBlock;
@Component("operatorCircleReadDAO")
public class OperatorCircleReadDAO {
	private static Logger logger = LoggingFactory.getLogger(OperatorCircleReadDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public List<CircleMaster> getCircles() {
		try {
			logger.debug("Request comes into the getCircles method of the class OperatorCircleReadDAO, the request parameter has no input parameters ");
			String queryString = "select * from circle_master where is_active = 1";

			List<CircleMaster> circles = this.jdbcTemplate.query(queryString, new HashMap<String, Object>(), new CircleMasterRowMapper());
			int size = 0;
			if (circles != null){
				size = circles.size();
			}
			logger.debug("Request goes out from the getCircles method of the class OperatorCircleReadDAO, the request  return type is List<CircleMaster>, size:"+size);
			return circles;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public CircleMaster getCircle(Integer circleId) {
		try {
			logger.debug("Request comes into the getCircles method of the class OperatorCircleReadDAO, the request  input parameters is Integer "+circleId);
			String queryString = "select * from circle_master where circle_master_id = :circleId and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("circleId", circleId);

			List<CircleMaster> circles = this.jdbcTemplate.query(queryString, paramMap, new CircleMasterRowMapper());
			int size = 0;
			if (circles != null){
				size = circles.size();
			}
			logger.debug("Request goes out from the getCircle method of the class OperatorCircleReadDAO, the request  return type is CircleMaster, size: "+size);
			if (circles != null && circles.size() > 0)
				return circles.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public CircleMaster getCircle(String circleName) {
		try {
			logger.debug("Request comes into the getCircles method of the class OperatorCircleReadDAO, the request  input parameters is String "+circleName);
			String queryString = "select * from circle_master where name = :circleName and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("circleName", circleName);

			List<CircleMaster> circles = this.jdbcTemplate.query(queryString, paramMap, new CircleMasterRowMapper());
			int size = 0;
			if (circles != null){
				size = circles.size();
			}
			logger.debug("Request goes out from the getCircle method of the class OperatorCircleReadDAO, the request  return type is CircleMaster, size:"+size);
			if (circles != null && circles.size() > 0)
				return circles.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<OperatorMaster> getOperators() {
		try {
			logger.debug("Request comes into the getOperators method of the class OperatorCircleReadDAO, the request  input has no parameters ");
			String queryString = "select * from operator_master where is_active = 1";

			List<OperatorMaster> operators = this.jdbcTemplate.query(queryString, new HashMap<String, Object>(), new OperatorMasterRowMapper());
			int size = 0;
			if (operators != null){
				size = operators.size();
			}
			logger.debug("Request goes out from the getOperators method of the class OperatorCircleReadDAO, the request  return type is List<OperatorMaster>, size:"+size);
			return operators;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public OperatorMaster getOperator(Integer operatorId) {
		try {
			logger.debug("Request comes into the getOperator method of the class OperatorCircleReadDAO, the request  input parameters is Integer "+operatorId);
			String queryString = "select * from operator_master where operator_master_id = :operatorId and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("operatorId", operatorId);

			List<OperatorMaster> operators = this.jdbcTemplate.query(queryString, paramMap, new OperatorMasterRowMapper());
			int size = 0;
			if (operators != null){
				size = operators.size();
			}
			logger.debug("Request goes out from the getOperator method of the class OperatorCircleReadDAO, the request  return type is OperatorMaster, size:"+size);
			if (operators != null && operators.size() > 0)
				return operators.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<OperatorMaster> getOperatorsByProductId(Integer productId) {
		try {
			logger.debug("Request comes into the getOperatorsByProductId method of the class OperatorCircleReadDAO, the request  input parameters is Integer "+productId);
			String queryString = "select * from operator_master where fk_product_master_id = :productId and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("productId", productId);

			List<OperatorMaster> operators = this.jdbcTemplate.query(queryString, paramMap, new OperatorMasterRowMapper());
			int size = 0;
			if (operators != null){
				size = operators.size();
			}
			logger.debug("Request goes out from the getOperatorsByProductId method of the class OperatorCircleReadDAO, the request  return type is List<OperatorMaster>, size: "+size);
			return operators;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public OperatorMaster getOperator(String operator) {
		try {
			logger.debug("Request comes into the getOperator method of the class OperatorCircleReadDAO, the request  input parameters is String "+operator);
			String queryString = "select * from operator_master where operator_code = :operator and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("operator", operator);

			List<OperatorMaster> operators = this.jdbcTemplate.query(queryString, paramMap, new OperatorMasterRowMapper());
			int size = 0;
			if (operators != null){
				size = operators.size();
			}
			logger.debug("Request goes out from the getOperator method of the class OperatorCircleReadDAO, the request  return type is OperatorMaster, size: "+size);
			if (operators != null && operators.size() > 0)
				return operators.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<Map<String, Object>> getOperatorCircleByPrefix(String prefix, String proMasterId) {
		logger.debug("Request comes into the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  input parameters is String "+prefix+" String "+proMasterId);
		logger.info("getting All Prefix All Operator All Circle Instances ", null);
		try {

			String queryString = "select sap.prefix as prefixValue, sap.fk_product_master_id as productMasterId, com.fk_circle_master_id as circleMasterId, " + "com.fk_operator_master_id as operatorMasterId, cm.name as circleName " + "from site_autosuggest_prefix sap join circle_operator_mapping com on com.circle_operator_mapping_id = sap.fk_circle_operator_mapping_id " + "join product_master pm on sap.fk_product_master_id=pm.product_master_id "
					+ "join circle_master cm on cm.circle_master_id=com.fk_circle_master_id where  " + "sap.prefix like :prefix and sap.fk_product_master_id=:proMasterId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("prefix", prefix + "%");
			paramMap.put("proMasterId", proMasterId);

			List<Map<String, Object>> result = this.jdbcTemplate.queryForList(queryString, paramMap);
			int size = 0;
			if (result != null){
				size = result.size();
			}
			logger.debug("Request goes out from the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return result;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}

    public Map<String, Object> getOperatorStateByPrefixAndProduct(String prefix, Integer productId){
        logger.debug("Getting Operator state for Prefix: "+prefix+" and Product: "+productId);

        String sql = "select c.name from site_autosuggest_prefix sap join circle_operator_mapping cm on " +
                "(sap.fk_circle_operator_mapping_id = cm.circle_operator_mapping_id) join circle_master c on " +
                "(cm.fk_circle_master_id = c.circle_master_id) where sap.prefix = :prefix and " +
                "fk_product_master_id = :productId";
        Map<String, Object> params = new HashMap<>();
        params.put("prefix", prefix);
        params.put("productId", productId);
        List<Map<String, Object>> result = this.jdbcTemplate.queryForList(sql, params);
        if(result==null){
            logger.debug("circle_result_for_operator_state: null Prefix: "+prefix+" Product: "+productId);
            return null;
        }
        logger.debug("circle_result_for_operator_state: "+result.size()+" Prefix: "+prefix+" Product: "+productId);

        if(result.size()>0){
            Map<String, Object> stateParams = result.get(0);
            String stateSql = "select state_master_id as state_id, state_name as name from state_master where state_name = :name";
            List<Map<String, Object>> states = this.jdbcTemplate.queryForList(stateSql, stateParams);
            if(states==null){
                logger.info("states_result_for_operator_state: null Prefix: "+prefix+" Product: "+productId);
                return null;
            }
            logger.info("states_result_for_operator_state: "+states.size()+" Prefix: "+prefix+" Product: "+productId);

            if(states.size()>0){
                return states.get(0);
            }
        }
        return null;
    }

	public List<Map<String, Object>> getOperatorCircleByPrefix() {
		logger.debug("Request comes into the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  input has no parameters");
		logger.info("getting All Prefix All Operator All Circle Instances ", null);
		try {

			String queryString = "select sap.prefix as prefixValue, sap.fk_product_master_id as productMasterId, com.fk_circle_master_id as circleMasterId, "
					+ "com.fk_operator_master_id as operatorMasterId, cm.name as circleName "
					+ "from site_autosuggest_prefix sap join circle_operator_mapping com on com.circle_operator_mapping_id = sap.fk_circle_operator_mapping_id "
					+ "join product_master pm on sap.fk_product_master_id=pm.product_master_id "
					+ "join circle_master cm on cm.circle_master_id=com.fk_circle_master_id";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<Map<String, Object>> result = this.jdbcTemplate.queryForList(queryString, paramMap);
			int size = 0;
			if (result != null){
				size = result.size();
			}
			logger.debug("Request goes out from the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return result;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}

	public CircleOperatorMapping getCircleOperatorMapping(Integer circleOperatorMappingId) {

		try {
			logger.debug("Request comes into the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request  input parameters is Integer "+circleOperatorMappingId);
			String queryString = "select * from circle_operator_mapping where circle_operator_mapping_id = :circleOperatorMappingId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("circleOperatorMappingId", circleOperatorMappingId);

			List<CircleOperatorMapping> circleOperatorMappings = this.jdbcTemplate.query(queryString, paramMap, new CircleOperatorMappingRowMapper());
			int size = 0;
			if (circleOperatorMappings != null){
				size = circleOperatorMappings.size();
			}
			logger.debug("Request goes out from the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the return size is: "+size);
			if (circleOperatorMappings != null && circleOperatorMappings.size() > 0)
				return circleOperatorMappings.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("cleanCartItems failed", re);
			throw re;
		}
	}
	
	public List<CircleOperatorMapping> getCircleOperatorMapping() {
		logger.debug("Request comes into the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request has no input parameters");
		try {
			logger.debug("Request comes into the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request  has no input parameters");
			String queryString = "select * from circle_operator_mapping";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<CircleOperatorMapping> circleOperatorMappings = this.jdbcTemplate.query(queryString, paramMap, new CircleOperatorMappingRowMapper());
			int size = 0;
			if (circleOperatorMappings != null){
				size = circleOperatorMappings.size();
			}
			logger.debug("Request goes out from the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request  return type is List<CircleOperatorMapping>, size: "+size);
			if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
				return circleOperatorMappings;				 
				}
		  } catch (RuntimeException re) {
			logger.error("cleanCartItems failed", re);
			throw re;
		}
		return null;
	}
	
	public Map<Integer, CircleOperatorMapping> getCircleOperatorMap() {
	    Map<Integer, CircleOperatorMapping> circleOperatorMap = new HashMap<Integer, CircleOperatorMapping>();
	    List<CircleOperatorMapping>  circleOperatorMappings = getCircleOperatorMapping();
	    if(circleOperatorMappings != null) {
	        for(CircleOperatorMapping circleOperatorMapping : circleOperatorMappings) {
	            circleOperatorMap.put(circleOperatorMapping.getCircleOperatorMappingId(), circleOperatorMapping);
	        }
	    }
	    return circleOperatorMap;
	}
	
	public List<CircleOperatorMapping> getCircleOperatorMapping(String aggregator) {
        logger.debug("Request comes into the getCircleOperatorMapping method of the class OperatorCircleReadDAO, for " + aggregator);
        try {
            String queryString = "select * from circle_operator_mapping where aggr_priority like '%"+aggregator+"%'";

            Map<String, Object> paramMap = new HashMap<String, Object>();
            
            List<CircleOperatorMapping> circleOperatorMappings = this.jdbcTemplate.query(queryString, paramMap, new CircleOperatorMappingRowMapper());
            if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
                return circleOperatorMappings;               
            }
          } catch (RuntimeException re) {
            logger.error("getCircleOperatorMapping failed ", re);
            throw re;
        }
        return null;
    }

	public CircleOperatorMapping getCircleOperatorMapping(Integer operatorId, Integer circleId) {
		try {
			logger.debug("Request comes into the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request input parameters is Integer "+operatorId+" Integer"+circleId);
			String queryString = "select * from circle_operator_mapping where fk_operator_master_id = :operatorId and fk_circle_master_id = :circleId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("operatorId", operatorId);
			paramMap.put("circleId", circleId);

			List<CircleOperatorMapping> circleOperatorMappings = this.jdbcTemplate.query(queryString, paramMap, new CircleOperatorMappingRowMapper());
			int size = 0;
			if (circleOperatorMappings != null){
				size = circleOperatorMappings.size();
			}
			logger.debug("Request goes out from the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the return size is: "+size);
			if (circleOperatorMappings != null && circleOperatorMappings.size() > 0)
				return circleOperatorMappings.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("getCircleOperatorMapping failed", re);
			throw re;
		}
	}

	public InOprAggrPriority getInOprAggrPriority(Integer circleOperatorMappingId) {
		try {
			logger.debug("Request comes into the getInOprAggrPriority method of the class OperatorCircleReadDAO, the request input parameters is Integer "+circleOperatorMappingId);
			String queryString = "select * from in_oppr_aggr_priority where fk_circle_operator_mapping_id = :circleOperatorMappingId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("circleOperatorMappingId", circleOperatorMappingId);

			List<InOprAggrPriority> inOprAggrPriorities = this.jdbcTemplate.query(queryString, paramMap, new InOprAggrPriorityMapper());
			int size = 0;
			if (inOprAggrPriorities != null){
				size = inOprAggrPriorities.size();
			}
			logger.debug("Request goes out from the getCircleOperatorMapping method of the class OperatorCircleReadDAO, the request  return type is InOprAggrPriority, size "+size);
			if (inOprAggrPriorities != null && inOprAggrPriorities.size() > 0)
				return inOprAggrPriorities.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("getInOprAggrPriority failed", re);
			throw re;
		}
	}

	public List<InOprAggrPriority> getInOprAggrPriorities() {
		try {
			logger.debug("Request comes into the getInOprAggrPriority method of the class OperatorCircleReadDAO, the request has no input parameters");
			String queryString = "select * from in_oppr_aggr_priority ";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<InOprAggrPriority> inOprAggrPriorities = this.jdbcTemplate.query(queryString, paramMap, new InOprAggrPriorityMapper());
			int size = 0;
			if (inOprAggrPriorities != null){
				size = inOprAggrPriorities.size();
			}
			logger.debug("Request goes out from the getInOprAggrPriorities method of the class OperatorCircleReadDAO, the request  return type is List<InOprAggrPriority>, size: "+size);
			if (inOprAggrPriorities != null && inOprAggrPriorities.size() > 0)
				return inOprAggrPriorities;
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("getInOprAggrPriority failed", re);
			throw re;
		}
	}

	public List<Map<String, Object>> getAllPrefixAllOperatorAllCircle() {
		logger.debug("Request comes into the getAllPrefixAllOperatorAllCircle method of the class OperatorCircleReadDAO, the request has no input parameters");
		logger.info("getting All Prefix All Operator All Circle Instances ", null);
		try {

			String queryString = "select sasp.site_autosuggest_prefix_id as prefixid, sasp.prefix as prefixvalue, om.operator_master_id as operatorid, om.operator_code as operatorccode, om.name as operatorname, "
					+ "cm.circle_master_id as circleid, cm.name as circlename, pm.product_name as productName "
					+ "from site_autosuggest_prefix as sasp join circle_operator_mapping as com on sasp.fk_circle_operator_mapping_id = com.circle_operator_mapping_id "
					+ "join circle_master as cm on com.fk_circle_master_id = cm.circle_master_id "
					+ "join operator_master as om on com.fk_operator_master_id = om.operator_master_id "
					+ "join product_master as pm on sasp.fk_product_master_id = pm.product_master_id ";

			List<Map<String, Object>> result = this.jdbcTemplate.queryForList(queryString, new HashMap<String, Object>());
			int size = 0;
			if (result != null){
				size = result.size();
			}
			logger.debug("Request goes out from the getAllPrefixAllOperatorAllCircle method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return result;

		} catch (Exception e) {
			logger.error("getAllPrefixAllOperatorAllCircle failed", e);
			return null;
		}

	}

	public List<Map<String, Object>> getAllOperators() {
		logger.debug("Request comes into the getAllOperators method of the class OperatorCircleReadDAO, the request has no input parameters");
		logger.info("getting All Operator For All Prefix Instances ", null);
		try {

			String queryString = "select om.operator_master_id as operatorMasterId, om.operator_code as operatorCode, om.name as operatorName, pm.product_master_id as productMasterId, pm.product_name as productName "
					+ "from operator_master om join product_master pm on om.fk_product_master_id = pm.product_master_id "
					+ "where om.is_active = true and pm.is_active = true";

			List<Map<String, Object>> result = this.jdbcTemplate.queryForList(queryString, new HashMap<String, Object>());
			int size = 0;
			if (result != null){
				size = result.size();
			}
			logger.debug("Request goes out from the getAllPrefixAllOperatorAllCircle method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return result;

		} catch (Exception e) {
			logger.error("getAllOperator failed", e);
			return null;
		}
	}

	public List<InAggrOprCircleMap> getInAggrOprCircleMap(Integer operatorId, Integer circleId) {
		logger.debug("Request comes into the getInAggrOprCircleMap method of the class OperatorCircleReadDAO, the request input parameters is Integer "+operatorId+" Integer "+circleId);
		logger.info("finding all InAggrOprCircleMap instances", null);
		try {
			String queryString = "select * from in_aggr_opr_circle_map where fk_operator_id = :operatorId and fk_circle_id = :circleId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("operatorId", operatorId);
			paramMap.put("circleId", circleId);
			List<InAggrOprCircleMap> inAggrOprCircleMaps = this.jdbcTemplate.query(queryString, paramMap, new InAggrOprCircleMapMapper());
			int size = 0;
			if (inAggrOprCircleMaps != null){
				size = inAggrOprCircleMaps.size();
			}
			logger.debug("Request goes out from the getInAggrOprCircleMap method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return inAggrOprCircleMaps;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List<InAggrOprCircleMap> getInAggrOprCircleMap() {
		logger.debug("Request comes into the getInAggrOprCircleMap method of the class OperatorCircleReadDAO, the request has no input parameters");
		logger.info("finding all InAggrOprCircleMap instances", null);
		try {
			String queryString = "select * from in_aggr_opr_circle_map";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<InAggrOprCircleMap> inAggrOprCircleMaps = this.jdbcTemplate.query(queryString, paramMap, new InAggrOprCircleMapMapper());
			int size = 0;
			if (inAggrOprCircleMaps != null){
				size = inAggrOprCircleMaps.size();
			}
			logger.debug("Request goes out from the getInAggrOprCircleMap method of the class OperatorCircleReadDAO, the request  return type is List<InAggrOprCircleMap>, size: "+size);
			return inAggrOprCircleMaps;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
	
	public List<Map<String, Object>> getOperatorCircleByPrefixId(String prefix) {
		logger.debug("Request comes into the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  input parameters is String " + prefix);
		logger.info("getting All Prefix All Operator All Circle Instances ", null);
		try {

			String queryString = "select sap.prefix as prefixValue, sap.fk_product_master_id as productMasterId, com.fk_circle_master_id as circleMasterId, " + "com.fk_operator_master_id as operatorMasterId, cm.name as circleName " + "from site_autosuggest_prefix sap join circle_operator_mapping com on com.circle_operator_mapping_id = sap.fk_circle_operator_mapping_id " + "join product_master pm on sap.fk_product_master_id=pm.product_master_id "
					+ "join circle_master cm on cm.circle_master_id=com.fk_circle_master_id where  " + "sap.prefix like :prefix ";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("prefix", prefix + "%");

			List<Map<String, Object>> result = this.jdbcTemplate.queryForList(queryString, paramMap);
			int size = 0;
			if (result != null){
				size = result.size();
			}
			logger.debug("Request goes out from the getOperatorCircleByPrefix method of the class OperatorCircleReadDAO, the request  return type is List<Map<String, Object>>, size: "+size);
			return result;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}
	
	public List<ProductMaster> getProducts(){
        try {
            logger.debug("Request comes into the getAllProducts method of the class OperatorCircleReadDAO, the request  input has no parameters ");
            String queryString = "select * from product_master where is_active = 1";

            List<ProductMaster> products = this.jdbcTemplate.query(queryString, new HashMap<String, Object>(), new ProductMasterRowMapper());
            int size = 0;
			if (products != null){
				size = products.size();
			}
            logger.debug("Request goes out from the getAllProducts method of the class OperatorCircleReadDAO, the request  return type is List<ProductMaster>, size:"+size);
            return products;

        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }
	
	public OperatorCircleBlock getOperatorCircleBlock(int operatorId, int circleId){
        	String SQL_GET_OPERATOR_CIRCLE_BLOCK = "select * from operator_circle_block where operator_id = :operatorId and circle_id = :circleId";
    		Map<String, Object> params = new HashMap<>();
    		params.put("operatorId", operatorId);
    		params.put("circleId", circleId);
    		List<OperatorCircleBlock> rows =  jdbcTemplate.query(SQL_GET_OPERATOR_CIRCLE_BLOCK, params, new OperatorCircleBlockMapper());
    		return rows.isEmpty() ? null : rows.get(0);
    	}
	
	 public static class OperatorCircleBlockMapper implements RowMapper<OperatorCircleBlock> {
	        @Override
	        public OperatorCircleBlock mapRow(ResultSet rs, int row) throws SQLException {
	        	OperatorCircleBlock operatorCircleBlock = new OperatorCircleBlock();
	        	operatorCircleBlock.setId(rs.getInt("id"));
	        	operatorCircleBlock.setOperatorId(rs.getInt("operator_id"));
	        	operatorCircleBlock.setCircleId(rs.getInt("circle_id"));
	        	operatorCircleBlock.setIsBlocked(rs.getBoolean("is_blocked"));
	        	operatorCircleBlock.setAdminUserEmail(rs.getString("user_email"));
	            return operatorCircleBlock;
	        }
	    }
    
   
}
