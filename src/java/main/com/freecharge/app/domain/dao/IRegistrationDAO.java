package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created with IntelliJ IDEA.
 * User: abc
 * Date: 5/3/12
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IRegistrationDAO {

    public List getUserContactDetails(BaseBusinessDO baseBusinessDO);

    public List<UserContactDetail> getUserContactDetails(String emailId,String serviceNumber);

    public List<UserContactDetail> getUserContactDefaultDetails(String emailId);
    
    public List<Users> getUserInfoByEmailid(BaseBusinessDO baseBusinessDO);

    public List<StateMaster> getStateMasterList();

    public List<StateMaster> getStateNameByStateId(Integer stateId);

    public Integer saveTransactionDetails(BaseBusinessDO baseBusinessDO);

    public List<CountryMaster> getCountryMasterList();

    public List<CountryMaster> getCountryNameByCountryId(Integer countryId);

    public Integer saveUserRechargeContact(UserRechargeContact userRechargeContact);
    
    public List<UserRechargeContact> getUserRechargeContact(String email,String serviceNumber);
}
