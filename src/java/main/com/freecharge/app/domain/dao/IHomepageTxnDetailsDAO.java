package com.freecharge.app.domain.dao;

import java.util.List;

import com.freecharge.app.domain.entity.HomepageTxnDetails;

/**
 * Interface for HomepageTxnDetailsDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IHomepageTxnDetailsDAO {
	
	/**
	 * Find all HomepageTxnDetails entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the HomepageTxnDetails property to query
	 * @param value
	 *            the property value to match
	 * @return List<HomepageTxnDetails> found by query
	 */
	public List<HomepageTxnDetails> findByProperty(String propertyName,
			Object value);

	public List<HomepageTxnDetails> findBySessionUniqueId(Object sessionUniqueId);

	public List<HomepageTxnDetails> findByLookupId(Object lookupId);

	public List<HomepageTxnDetails> findByProductServiceNumber(
			Object productServiceNumber);

	public List<HomepageTxnDetails> findByEmail(Object email);

	public List<HomepageTxnDetails> findByCoupon(Object coupon);

	public List<HomepageTxnDetails> findByIsLoggedin(Object isLoggedin);

	public List<HomepageTxnDetails> findByMyaccountAccessed(
			Object myaccountAccessed);

	public List<HomepageTxnDetails> findByAliasUsed(Object aliasUsed);

	public List<HomepageTxnDetails> findByAmount(Object amount);

	public List<HomepageTxnDetails> findByProductSelected(Object productSelected);

	/**
	 * Find all HomepageTxnDetails entities.
	 * 
	 * @return List<HomepageTxnDetails> all HomepageTxnDetails entities
	 */
	public List<HomepageTxnDetails> findAll();
}