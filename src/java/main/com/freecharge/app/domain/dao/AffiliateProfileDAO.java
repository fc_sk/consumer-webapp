package com.freecharge.app.domain.dao;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.AffiliateProfile;
import com.freecharge.common.framework.logging.LoggingFactory;


/**
 * A data access object (DAO) providing persistence and search support for
 * AffiliateProfile entities. Transaction control of the save(), update() and
 * delete() operations must be handled externally by senders of these methods or
 * must be manually added to each of these methods for data to be persisted to
 * the JPA datastore.
 * 
 * @see com.freecharge.app.domain.dao.AffiliateProfile
 * @author Rananjay
 */

public class AffiliateProfileDAO  extends CommonDao<AffiliateProfile> implements IAffiliateProfileDAO {
	// property constants
	public static final String AFFLIATE_UNIQUE_ID = "affliateUniqueId";
	public static final String COMPANY = "company";
	public static final String CONTACT_NAME = "contactName";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String EXT = "ext";
	public static final String FAX = "fax";
	public static final String ADDRESS1 = "address1";
	public static final String ADDRESS2 = "address2";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String POSTAL_CODE = "postalCode";
	public static final String SITE_NAME = "siteName";
	public static final String ID = "id";
	public static final String USER_ID = "userId";

    private static Logger logger = LoggingFactory.getLogger(AffiliateProfileDAO.class);
    
	private SimpleJdbcInsert insertAffiliateProfile;

    @Autowired
	public void setDataSource(DataSource dataSource) {
        this.insertAffiliateProfile = new SimpleJdbcInsert(dataSource).withTableName("affiliate_profile").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}



       protected Class<AffiliateProfile> getDomainClass() {
           return AffiliateProfile.class;
       }


	
	/**
	 * Find all AffiliateProfile entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the AffiliateProfile property to query
	 * @param value
	 *            the property value to match
	 * @return List<AffiliateProfile> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<AffiliateProfile> findByProperty(String propertyName,
			final Object value) {
		logger.debug("Request comes into the findByProperty method of the class AffiliateProfileDAO, the request input parameters are String propertyName:"+propertyName+" and Object value:"+value);
		logger.info("finding AffiliateProfile instance with property: "
						+ propertyName + ", value: " + value,null);
		try {
			final String queryString = "select model from AffiliateProfile model where model."
					+ propertyName + "= :propertyValue";
			Query query = getSession().createQuery(queryString);
			query.setParameter("propertyValue", value);
			List<AffiliateProfile> affiliateProfiles = query.list();
			logger.debug("Request goes out from the findByProperty method of the class AffiliateProfileDAO, the request return type is List<AffiliateProfile> :"+affiliateProfiles);
			return affiliateProfiles;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

	public List<AffiliateProfile> findByAffliateUniqueId(Object affliateUniqueId) {
		return findByProperty(AFFLIATE_UNIQUE_ID, affliateUniqueId);
	}

	public List<AffiliateProfile> findByCompany(Object company) {
		return findByProperty(COMPANY, company);
	}

	public List<AffiliateProfile> findByContactName(Object contactName) {
		return findByProperty(CONTACT_NAME, contactName);
	}

	public List<AffiliateProfile> findByEmail(Object email) {
		return findByProperty(EMAIL, email);
	}

	public List<AffiliateProfile> findByPhone(Object phone) {
		return findByProperty(PHONE, phone);
	}

	public List<AffiliateProfile> findByExt(Object ext) {
		return findByProperty(EXT, ext);
	}

	public List<AffiliateProfile> findByFax(Object fax) {
		return findByProperty(FAX, fax);
	}

	public List<AffiliateProfile> findByAddress1(Object address1) {
		return findByProperty(ADDRESS1, address1);
	}

	public List<AffiliateProfile> findByAddress2(Object address2) {
		return findByProperty(ADDRESS2, address2);
	}

	public List<AffiliateProfile> findByCity(Object city) {
		return findByProperty(CITY, city);
	}

	public List<AffiliateProfile> findByState(Object state) {
		return findByProperty(STATE, state);
	}

	public List<AffiliateProfile> findByPostalCode(Object postalCode) {
		return findByProperty(POSTAL_CODE, postalCode);
	}

	public List<AffiliateProfile> findBySiteName(Object siteName) {
		return findByProperty(SITE_NAME, siteName);
	}
	
    public List<AffiliateProfile> findById(Object siteName) {
        return findByProperty(ID, siteName);
    }
    
    public List<AffiliateProfile> findByUserId(Object userId) {
        return findByProperty(USER_ID, userId);
    }
    
	/**
	 * Find all AffiliateProfile entities.
	 * 
	 * @return List<AffiliateProfile> all AffiliateProfile entities
	 */
	@SuppressWarnings("unchecked")
	public List<AffiliateProfile> findAll() {
		logger.debug("Request comes into the findAll method of the class AffiliateProfileDAO ");
		logger.info("finding all AffiliateProfile instances",null);
		try {
			final String queryString = "select model from AffiliateProfile model";
			Query query = getSession().createQuery(queryString);
			List<AffiliateProfile> affiliateProfiles = query.list();
			logger.debug("Request goes out from the findAll method of the class AffiliateProfileDAO, the request return type is List<AffiliateProfile> "+affiliateProfiles);
			return affiliateProfiles;
		} catch (RuntimeException re) {
			logger.error("find all failed",re);
			throw re;
		}
	}
	
	public int insertAffiliateProfile(AffiliateProfile affiliateProfile) {
		try{SqlParameterSource parameters = new BeanPropertySqlParameterSource(affiliateProfile);
        Number id = this.insertAffiliateProfile.executeAndReturnKey(parameters);
        return id.intValue();
		}catch(Exception e){
			logger.error("createAffiliateProfile failed for affiliateunique id :"+ 
					affiliateProfile.getAffliateUniqueId()+ "and email: " + affiliateProfile.getEmail()
					, e);
			throw e;
		}
		
	}
	
	public Integer updateAffiliateProfile(Integer userId,Integer id) {
		try {
			logger.debug("Request comes into the updateAffiliateProfile method of the class AffiliateProfileDAO, the request input parameter type userId "+userId);
			String userstablequery = "update AffiliateProfile model set model.userId=:userId where users.id=:id";
			Query query =  getSession().createQuery(userstablequery);
            query.setParameter("userId",userId);
            query.setParameter("id", id);
            Integer update=query.executeUpdate();
            logger.debug("Request goes out the updateAffiliateProfile method of the class AffiliateProfileDAO, the request input parameter type userId "+userId);
			return update;
		} catch (RuntimeException re) {
			logger.error("updateAffiliateProfile failed for user id: " + userId);
			throw re;
		}
	}
	
	public List<AffiliateProfile> findByUserIdAndAffliateUniqueId(Integer userId,String affliateUniqueId) {
		try {
			final String queryString = "select model from AffiliateProfile model where model."
					+ "userId = :userId and model.affliateUniqueId = :affliateUniqueId" ;
			Query query = getSession().createQuery(queryString);
			query.setParameter("userId", userId);
			query.setParameter("affliateUniqueId", affliateUniqueId);
			List<AffiliateProfile> affiliateProfiles = query.list();
			logger.debug("Request goes out from the findByProperty method of the class AffiliateProfileDAO, the request return type is List<AffiliateProfile> :"+affiliateProfiles);
			return affiliateProfiles;
		} catch (RuntimeException re) {
			logger.error("find by property name failed",re);
			throw re;
		}
	}

}