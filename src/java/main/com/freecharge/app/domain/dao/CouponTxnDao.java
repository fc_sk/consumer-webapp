package com.freecharge.app.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;

@Repository
public class CouponTxnDao {
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	private static Logger logger = LoggingFactory.getLogger(CouponTxnDao.class);
	@Autowired
	@Qualifier("jdbcReadDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Map<String, Object>> getCouponIdsBylookupId(String lookupId) {
		logger.debug("Request comes into the getCouponDetailsBylookupId method of the class CouponReadDAO, the request  parameter type is string " + lookupId);
		List<Map<String, Object>> couponList = null;
		try {
			String queryWithCouponCodes = "SELECT tcs.coupon_id as couponStoreId,tcs.quantity as quantity FROM txn_home_page thp LEFT JOIN txn_cross_sell tcs on tcs.txn_home_page_id = thp.id " + 
									"WHERE thp.lookup_id = :lookupId and tcs.coupon_id <> 0 and tcs.is_deleted = 0 ";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("lookupId", lookupId);
			couponList = this.jdbcTemplate.queryForList(queryWithCouponCodes, paramMap);
		} catch (Exception e) {
			logger.error("getCouponIdsBylookupId failed", e);
		}
		logger.debug("Request goes out from the getCouponIdsBylookupId method of the class CouponTxnDao, the request  return type is List<Integer> ");
		return couponList;
	}
	

}
