package com.freecharge.app.domain.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.TxnCrosssellItemsVO;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.util.MapperUtil;

public class CartDAO extends CommonDao<Cart> implements ICartDAO {
	// property constants
	public static final String ORDER_ID = "orderId";
	public static final String FK_USER_ID = "fkUserId";
	public static final String LOOKUP_ID = "lookupId";
	public static final String IS_OPEN = "isOpen";
	public static final String FINAL_STATUS = "finalStatus";
	private static Logger logger = LoggingFactory.getLogger(CartDAO.class);
	
	@Autowired
	OrderIdWriteDAO orderIdDAO;	
	
	@Autowired
	FreefundService freefundService;
	
	@Autowired
	private OldCartItemsDao cartItemsDao;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	public Cart findByCartId(final Integer cartId) {
		try {
			return this.read(cartId);
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public Cart findByLookupId(final String lookupId) {
		logger.debug("Request comes into the findByLookupId method of the class CartDAO, the request input parameters are String lookupid:"+lookupId);
		try {
			final String queryString = "select model from Cart model where model.lookupId = :lookupId and model.expiresOn >= :expieryDate";
			org.hibernate.Query query = getSession().createQuery(queryString);
			query.setParameter("lookupId", lookupId);
			query.setParameter("expieryDate", new Timestamp(new Date().getTime()));

			List<Cart> cartList = query.list();
			logger.debug("Request goes out from the findByLookupId method of the class CartDAO, the request return type is Cart from the List<Cart> "+cartList);
			if (cartList != null && cartList.size() > 0)
				return (Cart) query.list().get(0);
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}

    public Cart findByLookupIdWithNoExpiryCheck(final String lookupId) {
        final String queryString = "select model from Cart model where model.lookupId = :lookupId";
        org.hibernate.Query query = getSession().createQuery(queryString);
        query.setParameter("lookupId", lookupId);

        List<Cart> cartList = query.list();
        if (cartList != null && cartList.size() > 0) {
            Cart cart = (Cart) query.list().get(0);
            if(cart!=null && (cart.getCartItemsList()==null || cart.getCartItemsList().size()==0)) {
            	List<CartItems> cartItemsOlds =  cartItemsDao.getCartItemsOld(cart.getCartId());
            	if(cartItemsOlds!=null && cartItemsOlds.size()!=0) {
	            	cart = MapperUtil.map(cart,Cart.class);
	            	cart.setCartItemsList(cartItemsOlds);
            	}
            }
            return cart;
        } else {
            return null;
        }
    }

	protected Class<Cart> getDomainClass() {
		return Cart.class; // To change body of implemented methods use File |
		// Settings | File Templates.
	}

	public Integer saveCart(Cart cart) {
		logger.debug("Request comes into the saveCart method of the class CartDAO, the request input parameters are Cart cart:"+cart);
		try {
			Session session = getSession();

			Integer cart_id = (Integer) session.save(cart);
			session.flush();
			logger.debug("Request goes out from the saveCart method of the class CartDAO, the request return type cart_id :"+cart_id);
			return cart_id;
		} catch (Exception exp) {
			logger.error("Exception raised while saving the cart , details are " + exp);
			return 0;
		}

	}

	public void saveCartItems(List<CartItems> cartItemsList,Integer cart_Id) {
		logger.debug("Request comes into the saveCartItems method of the class CartDAO, the request input parameters are List<CartItems> :"+cartItemsList+" and Integer cartId:"+cart_Id);
		try {
			Session session = getSession();
			Iterator<CartItems> ite= cartItemsList.iterator();
			while(ite.hasNext()){
				CartItems cartItems=	ite.next();
				Cart cart=new Cart();
				cart.setCartId(cart_Id);
				cartItems.setCart(cart);
				Integer cart_id = (Integer) session.save(cartItems);
				session.flush();
			}
			logger.debug("Request goes out from the saveCartItems method of the class CartDAO, the request return type void");			
		} catch (Exception exp) {
			logger.error("Exception raised while saving the cart , details are " + exp);
		}
	}

	public void deleteCartItems(Integer cartId) {
		logger.debug("Request comes into the deleteCartItems method of the class CartDAO, the request input parameters are Integer cartId:"+cartId);
		try {
			logger.info("entered into the deleteCartItems method of the cartDao class ..");
			logger.info("deleting from the deleteCartItems with id as " + cartId);
			String cartitemssql = "delete from cart_items where fk_cart_id=:cartItemsId " +
                    "and (entity_id not in ('" + FCConstants.ENTITY_NAME_FOR_CROSSSELL+ "', '" + FCConstants.ENTITY_NAME_FOR_FREEFUND + "') or entity_id is null)";
			int noOfRecords = getSession().createSQLQuery(cartitemssql).setParameter("cartItemsId", cartId).executeUpdate();
			logger.info("no of cartitems deleted is " + noOfRecords);
			logger.info("leaving from the deleteCartItems method of the cartDao class ..");
		} catch (Exception e) {
			logger.error("Exception Raised while removing cartItems , exp details are " + e);
		}
		logger.debug("Request goes out from the deleteCartItems method of the class CartDAO, the request return type void");
	}
	
	public List<com.freecharge.web.webdo.CartAndCartItemsVO> getCartByLookUpId(String lookupId) {
		logger.debug("Request comes into the getCartByLookUpId method of the class CartDAO, the request input parameters are String lookupId:"+lookupId);
		logger.info("Entered Into the getCartByLookUpId() of cartDao");
		String sql = "select cart.cart_id,cartItems.cart_items_id, cart.lookup_id  from cart as cart  , cart_items as cartItems  where cart.cart_id= cartItems.fk_cart_id   and cart.lookup_id=:lookupId ";

		List<com.freecharge.web.webdo.CartAndCartItemsVO> cartItemsList = new ArrayList<com.freecharge.web.webdo.CartAndCartItemsVO>();
		try {
			com.freecharge.web.webdo.CartAndCartItemsVO cartAndCartItemsVO =new com.freecharge.web.webdo.CartAndCartItemsVO();
			Query query = getSession().createSQLQuery(sql);
			query.setParameter("lookupId", lookupId);

			List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
			
			for (List<Object> list : ObjectList) {
				cartAndCartItemsVO.setCartId((Integer.parseInt(list.get(0).toString())));
				cartAndCartItemsVO.setCartItemsId(Integer.parseInt(list.get(1).toString()));
				cartItemsList.add(cartAndCartItemsVO);
			}
			logger.debug("Request goes out from the getCartByLookUpId method of the class CartDAO, the request return type List<com.freecharge.web.webdo.CartAndCartItemsVO>:"+cartItemsList);
			return cartItemsList;
		} catch (Exception e) {
			logger.error("Exception Raised while fetching data cart , exp details are " + e);
			return null;
		}
	}

	
	public List<TxnCrosssellItemsVO> getCrossSellID(Integer homePageID){
		logger.debug("Request comes into the getCrossSellID method of the class CartDAO, the request input parameters are Integer homePageId:"+homePageID);
		logger.info("Entered Into the getCrossSellID() of cartDao");
		String sql = "select crossSell.id,crossSell.corssSellId from TxnCrossSell crossSell where crossSell.txnHomePageId=:txnHomePageId and crossSell.isDeleted="+false;
		List<TxnCrosssellItemsVO> crossSellIDs=new ArrayList<TxnCrosssellItemsVO>();	
		try {
			
			Query query = getSession().createQuery(sql);
			query.setParameter("txnHomePageId",homePageID);
			List<List<Object>> ObjectList = query.setResultTransformer(Transformers.TO_LIST).list();
			for (List<Object> list : ObjectList) {
			TxnCrosssellItemsVO txnCrosssellItemsVO = new TxnCrosssellItemsVO();
			txnCrosssellItemsVO.setId((Integer)list.get(0));
			//id=	(Integer.parseInt(list.get(0).toString()));
	        txnCrosssellItemsVO.setCorssSellId((Integer)list.get(1));
	        crossSellIDs.add(txnCrosssellItemsVO);
			}
			
			logger.debug("Request goes out from the getCrossSellID method of the class CartDAO, the request return type List<TxnCrosssellItemsVO>:"+crossSellIDs);
			return crossSellIDs;
			
			
		} catch (Exception e) {
			logger.error("Exception , exp details are " + e);
			return crossSellIDs;
			
		}
	}
}