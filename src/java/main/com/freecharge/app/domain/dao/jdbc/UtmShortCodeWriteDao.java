package com.freecharge.app.domain.dao.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.UtmShortCode;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class UtmShortCodeWriteDao {
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertutmShortCode;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertutmShortCode = new SimpleJdbcInsert(dataSource).withTableName("utm_shortcode_details").usingGeneratedKeyColumns("utm_shortcode_id", "n_last_updated", "n_created");
    }

    public int insert(UtmShortCode utmShortCode) {
        SqlParameterSource parameters = utmShortCode.getMapSqlParameterSource();
        Number returnParam = this.insertutmShortCode.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public void editShortCode(UtmShortCode utmShortCode) {
        String query = "update utm_shortcode_details set utm_short_code=:shortCode, utm_campaign_id=:campaignId, " +
                "start_date=:startDate, end_date=:endDate, web_url=:webUrl, mobweb_url=:mobWebUrl," +
                "android_app_url=:androidAppUrl, ios_app_url=:iosAppUrl, windows_app_url=:windowsAppUrl, " +
                "updated_by=:updatedBy, updated_At=now() where utm_shortcode_id=:shortCodeId";
        Map<String, Object> params = new HashMap<>();
        params.put("shortCode", utmShortCode.getShortCode());
        params.put("campaignId", utmShortCode.getCampaignId());
        params.put("startDate", utmShortCode.getStartDate());
        params.put("endDate", utmShortCode.getEndDate());
        params.put("webUrl", utmShortCode.getWebUrl());
        params.put("mobWebUrl", utmShortCode.getMobileWebUrl());
        params.put("androidAppUrl", utmShortCode.getAndroidAppUrl());
        params.put("iosAppUrl", utmShortCode.getIosAppUrl());
        params.put("windowsAppUrl", utmShortCode.getWindowsAppUrl());
        params.put("updatedBy", utmShortCode.getUpdatedBy());
        params.put("shortCodeId", utmShortCode.getShortCodeId());

        this.jdbcTemplate.update(query, params);
    }
}
