package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.Crosssell;

public class CrosssellRowMapper implements RowMapper<Crosssell> {
	@Override
	public Crosssell mapRow(ResultSet rs, int row) throws SQLException {
		Crosssell crosssell = new Crosssell();
		
		crosssell.setId(rs.getInt("id"));
		crosssell.setXml(rs.getString("xml"));
		crosssell.setLabel(rs.getString("label"));
		crosssell.setCategory(rs.getString("category"));
		crosssell.setCreatedDate(rs.getTimestamp("created_date"));
		crosssell.setCreatedBy(rs.getString("created_by"));
		crosssell.setModifiedOn(rs.getTimestamp("modified_on"));
		crosssell.setModifiedBy(rs.getString("modified_by"));
		crosssell.setEffectiveFrom(rs.getDate("effective_from"));
		crosssell.setValidUpto(rs.getDate("valid_upto"));
		crosssell.setCountUses(rs.getInt("count_uses"));
		crosssell.setPrice(rs.getInt("price"));
		crosssell.setIsActive(rs.getBoolean("is_active"));
		
		return crosssell;
	}

}
