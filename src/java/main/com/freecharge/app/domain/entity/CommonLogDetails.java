package com.freecharge.app.domain.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * This class holds the values for plan level retry to be pushed into s3 for the purpose of analytics
 * @author abhishekanand
 *
 */
public class CommonLogDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String            database;
    private String            table;
    private String            type;
    private Long            ts;

    Map<String, Object> data = new HashMap<String, Object>();
    
    public String getDatabase() {
        return database;
    }
    
    public String getTable() {
        return table;
    }
    
    public String getType() {
        return type;
    }
    
    public Long getTimestamp() {
        return ts;
    }
    
    public void setDatabase(String db) {
        this.database = db;
    }
    
    public void setTable(String table) {
        this.table = table;
    }
    
    public void setTimestamp(Long TS) {
        this.ts = TS;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
	public void setData(Map<String,Object> data) {	
		this.data =data;
	}
	

}
