package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.api.coupon.service.model.CityMaster;
import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;
import com.freecharge.common.easydb.UpdateQuery;
import com.freecharge.common.util.FCStringUtils;

public class UserProfile implements Serializable, DBEntity {

	private static final long serialVersionUID = 1L;

    @ColumnMapping(name="user_profile_id")
	private Integer userProfileId;
    @ColumnMapping(name="fk_user_id")
	private Integer fkUserId;
    @ColumnMapping(name="fk_user_recharge_contact_id")
	private Integer fkUserRechargeContactId;
    @ColumnMapping(name="nick_alias")
	private String nickAlias;
    @ColumnMapping(name="address1")
	private String address1;
    @ColumnMapping(name="landmark")
	private String landmark;
    @ColumnMapping(name="city")
	private String city;
    @ColumnMapping(name="postal_code")
	private String postalCode;
    @ColumnMapping(name="fk_state_master_id")
	private Integer fkStateMasterId;
    @ColumnMapping(name="fk_country_master_id")
	private Integer fkCountryMasterId;
    @ColumnMapping(name="is_active")
	private Boolean isActive;
    @ColumnMapping(name="is_default")
	private Boolean isDefault;
    @ColumnMapping(name="created_on")
    private Timestamp createdOn;
    @ColumnMapping(name="last_update")
	private Timestamp lastUpdate;
    @ColumnMapping(name="first_name")
    private String firstName;
    @ColumnMapping(name="last_name")
    private String lastName;
    @ColumnMapping(name="title")
    private String title;
    @ColumnMapping(name="area")
    private String area;
    @ColumnMapping(name="street")
    private String street;

    private List<String> whereFields = new LinkedList<String>();

    private CityMaster cityMaster;

    public CityMaster getCityMaster() {
        return cityMaster;
    }

    public void setCityMaster(CityMaster cityMaster) {
        this.cityMaster = cityMaster;
    }

    public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Integer getUserProfileId() {
		return this.userProfileId;
	}

	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}

	public Integer getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	public Integer getFkUserRechargeContactId() {
		return this.fkUserRechargeContactId;
	}

	public void setFkUserRechargeContactId(Integer fkUserRechargeContactId) {
		this.fkUserRechargeContactId = fkUserRechargeContactId;
	}

	public String getNickAlias() {
		return this.nickAlias;
	}

	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getFkStateMasterId() {
		return this.fkStateMasterId;
	}

	public void setFkStateMasterId(Integer fkStateMasterId) {
		this.fkStateMasterId = fkStateMasterId;
	}

	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


    public String getTitle() {
         return title;
     }

     public void setTitle(String title) {
         this.title = title;
     }
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getOnlyFirstName(){
        return (firstName != null) ? firstName.split(" ")[0] :  "";
    }

    public String getOnlyLastName(){
        String [] name = (firstName != null) ? firstName.split(" ") : null;
        return ( name != null && name.length > 1) ? name[1] : "";
    }

    public void addToWhereFields(String columnName) {
        this.whereFields.add(columnName);
    }

    @Override
    public String getTableName() {
        return "user_profile";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        UpdateQuery updateQuery = new UpdateQuery(this, this.whereFields);
        return updateQuery.getUpdateDBVO();
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "userProfileId=" + userProfileId +
                ", fkUserId=" + fkUserId +
                ", fkUserRechargeContactId=" + fkUserRechargeContactId +
                ", nickAlias='" + nickAlias + '\'' +
                ", address1='" + address1 + '\'' +
                ", landmark='" + landmark + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", fkStateMasterId=" + fkStateMasterId +
                ", fkCountryMasterId=" + fkCountryMasterId +
                ", isActive=" + isActive +
                ", isDefault=" + isDefault +
                ", createdOn=" + createdOn +
                ", lastUpdate=" + lastUpdate +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", area='" + area + '\'' +
                ", street='" + street + '\'' +
                '}';
    }

    public String getGender(){
        if(FCStringUtils.isBlank(this.title)){
            return null;
        }
        return this.title.equalsIgnoreCase("mr")?"Male":"Female";
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));

        UserProfile userProfile = new UserProfile();
        userProfile.setUserProfileId(1480844);

        List<UserProfile> userProfiles = easyDBWriteDAO.get(userProfile);
        System.out.println(userProfiles);

        ctx.close();
    }
}