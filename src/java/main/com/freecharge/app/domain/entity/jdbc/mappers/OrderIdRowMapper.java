package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.OrderId;

public class OrderIdRowMapper implements RowMapper<OrderId> {
	@Override
	public OrderId mapRow(ResultSet rs, int row) throws SQLException {
		OrderId orderId = new OrderId();
		
		orderId.setPkOrderId(rs.getInt("pk_order_id"));
		orderId.setLookupId(rs.getString("lookup_id"));
		orderId.setSessionId(rs.getString("session_id"));
		orderId.setOrderId(rs.getString("order_id"));
		orderId.setOrderIdInc(rs.getInt("order_id_inc"));
		orderId.setFkProductMasterId(rs.getInt("fk_product_master_id"));
		orderId.setFkAffiliateProfileId(rs.getInt("fk_affiliate_profile_id"));
		orderId.setChannelId(rs.getInt("channel_id"));
		orderId.setCreatedOn(rs.getTimestamp("created_on"));
		
		return orderId;
	}

}
