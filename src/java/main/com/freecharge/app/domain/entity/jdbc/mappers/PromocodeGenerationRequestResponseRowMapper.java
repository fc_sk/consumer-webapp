package com.freecharge.app.domain.entity.jdbc.mappers;

import com.freecharge.promo.entity.PromocodeGenerationRequestResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PromocodeGenerationRequestResponseRowMapper implements RowMapper {

    @Override
    public PromocodeGenerationRequestResponse mapRow(ResultSet resultSet, int i) throws SQLException {
        PromocodeGenerationRequestResponse promocodeGenerationRequestResponse = new PromocodeGenerationRequestResponse();
        promocodeGenerationRequestResponse.setPromocodeId(resultSet.getLong("promocode_id"));
        promocodeGenerationRequestResponse.setRequestId(resultSet.getString("request_id"));
        promocodeGenerationRequestResponse.setRequestData(resultSet.getString("request_data"));
        promocodeGenerationRequestResponse.setResponseId(resultSet.getString("response_id"));
        promocodeGenerationRequestResponse.setResponseData(resultSet.getString("response_data"));
        promocodeGenerationRequestResponse.setCreatedAt(resultSet.getDate("created_at"));
        return promocodeGenerationRequestResponse;
    }
}
