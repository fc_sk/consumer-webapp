package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UserRechargeHistory entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user_recharge_history")
public class UserRechargeHistory implements java.io.Serializable {

	// Fields

	private Integer userRechargeHistoryId;
	private Integer fkUsersId;
	private Integer fkUserRechargeContact;
	private String  orderId;
	private Double txnAmount;
	private String paymentStatus;
	private String rechargeStatus;
	private Integer fkInMasterId;
	private Integer fkPaymentGatewayId;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Integer fkPaymentTxnId;
	private Integer fkRechargeTxnId;

	// Constructors

	/** default constructor */
	public UserRechargeHistory() {
	}

	/** minimal constructor */
	public UserRechargeHistory(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/** full constructor */
	public UserRechargeHistory(Integer fkUsersId,
			Integer fkUserRechargeContact, String orderId, Double txnAmount,
			String paymentStatus, String rechargeStatus, Integer fkInMasterId,
			Integer fkPaymentGatewayId, Timestamp createdOn,
			Timestamp updatedOn, Integer fkPaymentTxnId, Integer fkRechargeTxnId) {
		this.fkUsersId = fkUsersId;
		this.fkUserRechargeContact = fkUserRechargeContact;
		this.orderId = orderId;
		this.txnAmount = txnAmount;
		this.paymentStatus = paymentStatus;
		this.rechargeStatus = rechargeStatus;
		this.fkInMasterId = fkInMasterId;
		this.fkPaymentGatewayId = fkPaymentGatewayId;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.fkPaymentTxnId = fkPaymentTxnId;
		this.fkRechargeTxnId = fkRechargeTxnId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_recharge_history_id", unique = true, nullable = false)
	public Integer getUserRechargeHistoryId() {
		return this.userRechargeHistoryId;
	}

	public void setUserRechargeHistoryId(Integer userRechargeHistoryId) {
		this.userRechargeHistoryId = userRechargeHistoryId;
	}

	@Column(name = "fk_users_id")
	public Integer getFkUsersId() {
		return this.fkUsersId;
	}

	public void setFkUsersId(Integer fkUsersId) {
		this.fkUsersId = fkUsersId;
	}

	@Column(name = "fk_user_recharge_contact")
	public Integer getFkUserRechargeContact() {
		return this.fkUserRechargeContact;
	}

	public void setFkUserRechargeContact(Integer fkUserRechargeContact) {
		this.fkUserRechargeContact = fkUserRechargeContact;
	}

	@Column(name = "order_id")
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "txn_amount", precision = 10)
	public Double getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(Double txnAmount) {
		this.txnAmount = txnAmount;
	}

	@Column(name = "payment_status", length = 2)
	public String getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Column(name = "recharge_status", length = 2)
	public String getRechargeStatus() {
		return this.rechargeStatus;
	}

	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}

	@Column(name = "fk_in_master_id")
	public Integer getFkInMasterId() {
		return this.fkInMasterId;
	}

	public void setFkInMasterId(Integer fkInMasterId) {
		this.fkInMasterId = fkInMasterId;
	}

	@Column(name = "fk_payment_gateway_id")
	public Integer getFkPaymentGatewayId() {
		return this.fkPaymentGatewayId;
	}

	public void setFkPaymentGatewayId(Integer fkPaymentGatewayId) {
		this.fkPaymentGatewayId = fkPaymentGatewayId;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "updated_on", nullable = false, length = 0)
	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(name = "fk_payment_txn_id")
	public Integer getFkPaymentTxnId() {
		return this.fkPaymentTxnId;
	}

	public void setFkPaymentTxnId(Integer fkPaymentTxnId) {
		this.fkPaymentTxnId = fkPaymentTxnId;
	}

	@Column(name = "fk_recharge_txn_id")
	public Integer getFkRechargeTxnId() {
		return this.fkRechargeTxnId;
	}

	public void setFkRechargeTxnId(Integer fkRechargeTxnId) {
		this.fkRechargeTxnId = fkRechargeTxnId;
	}

}