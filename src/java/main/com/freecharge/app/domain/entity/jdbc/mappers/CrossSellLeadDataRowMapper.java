package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.CrossSellLeadData;

public class CrossSellLeadDataRowMapper implements RowMapper<CrossSellLeadData> {
	@Override
	public CrossSellLeadData mapRow(ResultSet rs, int row) throws SQLException {
		CrossSellLeadData crossSellLeadData = new CrossSellLeadData();
		
		crossSellLeadData.setId(rs.getInt("id"));
		crossSellLeadData.setFkUserId(rs.getInt("fk_user_id"));
		crossSellLeadData.setFkCrossSellId(rs.getInt("fk_cross_sell_id"));
		crossSellLeadData.setCrossSellData(rs.getString("cross_sell_data"));
		crossSellLeadData.setCreatedOn(rs.getTimestamp("created_on"));
		
		return crossSellLeadData;
	}

}
