package com.freecharge.app.domain.entity.jdbc;


public class EuronetCallbackResponse {
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "[result=" + result + "]";
    }
}
