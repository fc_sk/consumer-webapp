package com.freecharge.app.domain.entity.winpin;



import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CpnJobs entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cpn_jobs")
public class CpnJobs implements java.io.Serializable {

	// Fields

	private Long cjId;
	private CpnUser cpnUser;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private BigDecimal cjDenomination;
	private String cjEndSrNo;
	private Boolean cjIsActivated;
	private BigDecimal cjNoOfCpn;
	private BigDecimal cjPinLength;
	private Boolean cjSendForActivation;
	private Timestamp cjSendForActivtionDt;
	private String cjStartSrNo;
	private Set<CouponPin> couponPins = new HashSet<CouponPin>(0);
	private Set<CpnPinHolder> cpnPinHolders = new HashSet<CpnPinHolder>(0);

	// Constructors

	/** default constructor */
	public CpnJobs() {
	}

	/** full constructor */
	public CpnJobs(CpnUser cpnUser, String cfActionStatus, Date cfActiveDate,
			Boolean cfActiveFlag, Boolean cfArchiveStatus,
			Timestamp cfDateCreatedOn, Timestamp cfDateLastUpdatedOn,
			Long cfLastUpdatedById, BigDecimal cjDenomination,
			String cjEndSrNo, Boolean cjIsActivated, BigDecimal cjNoOfCpn,
			BigDecimal cjPinLength, Boolean cjSendForActivation,
			Timestamp cjSendForActivtionDt, String cjStartSrNo,
			Set<CouponPin> couponPins, Set<CpnPinHolder> cpnPinHolders) {
		this.cpnUser = cpnUser;
		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.cjDenomination = cjDenomination;
		this.cjEndSrNo = cjEndSrNo;
		this.cjIsActivated = cjIsActivated;
		this.cjNoOfCpn = cjNoOfCpn;
		this.cjPinLength = cjPinLength;
		this.cjSendForActivation = cjSendForActivation;
		this.cjSendForActivtionDt = cjSendForActivtionDt;
		this.cjStartSrNo = cjStartSrNo;
		this.couponPins = couponPins;
		this.cpnPinHolders = cpnPinHolders;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CJ_ID", unique = true, nullable = false)
	public Long getCjId() {
		return this.cjId;
	}

	public void setCjId(Long cjId) {
		this.cjId = cjId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CF_CREATED_BY_ID")
	public CpnUser getCpnUser() {
		return this.cpnUser;
	}

	public void setCpnUser(CpnUser cpnUser) {
		this.cpnUser = cpnUser;
	}

	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "CJ_DENOMINATION", precision = 22, scale = 0)
	public BigDecimal getCjDenomination() {
		return this.cjDenomination;
	}

	public void setCjDenomination(BigDecimal cjDenomination) {
		this.cjDenomination = cjDenomination;
	}

	@Column(name = "CJ_END_SR_NO", length = 50)
	public String getCjEndSrNo() {
		return this.cjEndSrNo;
	}

	public void setCjEndSrNo(String cjEndSrNo) {
		this.cjEndSrNo = cjEndSrNo;
	}

	@Column(name = "CJ_IS_ACTIVATED")
	public Boolean getCjIsActivated() {
		return this.cjIsActivated;
	}

	public void setCjIsActivated(Boolean cjIsActivated) {
		this.cjIsActivated = cjIsActivated;
	}

	@Column(name = "CJ_NO_OF_CPN", precision = 22, scale = 0)
	public BigDecimal getCjNoOfCpn() {
		return this.cjNoOfCpn;
	}

	public void setCjNoOfCpn(BigDecimal cjNoOfCpn) {
		this.cjNoOfCpn = cjNoOfCpn;
	}

	@Column(name = "CJ_PIN_LENGTH", precision = 22, scale = 0)
	public BigDecimal getCjPinLength() {
		return this.cjPinLength;
	}

	public void setCjPinLength(BigDecimal cjPinLength) {
		this.cjPinLength = cjPinLength;
	}

	@Column(name = "CJ_SEND_FOR_ACTIVATION")
	public Boolean getCjSendForActivation() {
		return this.cjSendForActivation;
	}

	public void setCjSendForActivation(Boolean cjSendForActivation) {
		this.cjSendForActivation = cjSendForActivation;
	}

	@Column(name = "CJ_SEND_FOR_ACTIVTION_DT", length = 0)
	public Timestamp getCjSendForActivtionDt() {
		return this.cjSendForActivtionDt;
	}

	public void setCjSendForActivtionDt(Timestamp cjSendForActivtionDt) {
		this.cjSendForActivtionDt = cjSendForActivtionDt;
	}

	@Column(name = "CJ_START_SR_NO", length = 50)
	public String getCjStartSrNo() {
		return this.cjStartSrNo;
	}

	public void setCjStartSrNo(String cjStartSrNo) {
		this.cjStartSrNo = cjStartSrNo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cpnJobs")
	public Set<CouponPin> getCouponPins() {
		return this.couponPins;
	}

	public void setCouponPins(Set<CouponPin> couponPins) {
		this.couponPins = couponPins;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cpnJobs")
	public Set<CpnPinHolder> getCpnPinHolders() {
		return this.cpnPinHolders;
	}

	public void setCpnPinHolders(Set<CpnPinHolder> cpnPinHolders) {
		this.cpnPinHolders = cpnPinHolders;
	}

}