package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;

public class InOprAggrPriorityMapper implements RowMapper<InOprAggrPriority> {

	@Override
	public InOprAggrPriority mapRow(ResultSet rs, int row) throws SQLException {
		InOprAggrPriority inOpprAggrPriority = new InOprAggrPriority();
		
		inOpprAggrPriority.setId(rs.getInt("id"));
		inOpprAggrPriority.setAggrPriority(rs.getString("aggr_priority"));
		inOpprAggrPriority.setUpdateTime(rs.getTimestamp("update_time"));
		inOpprAggrPriority.setUpdatedBy(rs.getString("updated_by"));
		inOpprAggrPriority.setFkcircleOperatorMappingId(rs.getInt("fk_circle_operator_mapping_id"));
		
		return inOpprAggrPriority;
	}
}
