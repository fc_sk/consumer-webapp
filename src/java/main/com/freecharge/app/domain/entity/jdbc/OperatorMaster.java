package com.freecharge.app.domain.entity.jdbc;

import com.freecharge.common.framework.basedo.AbstractDO;


public class OperatorMaster extends AbstractDO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer operatorMasterId;
	private String operatorCode;
	private String name;
	private Boolean isActive;
	private Integer fkCountryMasterId;
	private Integer fkProductMasterId;
	private String imgUrl;
	private Boolean isBbpsEnabled;

	public Boolean getIsBbpsEnabled() {
		return isBbpsEnabled;
	}

	public void setIsBbpsEnabled(Boolean isBBPSEnabled) {
		this.isBbpsEnabled = isBBPSEnabled;
	}

	public Integer getOperatorMasterId() {
		return this.operatorMasterId;
	}

	public void setOperatorMasterId(Integer operatorMasterId) {
		this.operatorMasterId = operatorMasterId;
	}

	public String getOperatorCode() {
		return this.operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	
	@Override
	public boolean equals(Object obj) {		
		return this.operatorMasterId.equals(((OperatorMaster)obj).getOperatorMasterId());
	}
	
	@Override
	public int hashCode() {		
		return this.getOperatorMasterId().hashCode();
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

}