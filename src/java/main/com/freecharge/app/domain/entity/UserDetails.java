package com.freecharge.app.domain.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class UserDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private int userId;
	private String email;
	private String mobileNo;
	private String name;
	private Boolean isActive;
	private String password;
	private String forgotPasswordKey;
	private Date forgotPasswordExpiry;
	private Timestamp lastUpdate;
	private Timestamp dateAdded;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getForgotPasswordKey() {
		return forgotPasswordKey;
	}
	public void setForgotPasswordKey(String forgotPasswordKey) {
		this.forgotPasswordKey = forgotPasswordKey;
	}
	public Date getForgotPasswordExpiry() {
		return forgotPasswordExpiry;
	}
	public void setForgotPasswordExpiry(Date forgotPasswordExpiry) {
		this.forgotPasswordExpiry = forgotPasswordExpiry;
	}
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Timestamp getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}
	@Override
	public String toString() {
		return "UserDetails [userId=" + userId + ", email=" + email + ", mobileNo=" + mobileNo + ", name=" + name
				+ ", isActive=" + isActive + ", password=" + password + ", forgotPasswordKey=" + forgotPasswordKey
				+ ", forgotPasswordExpiry=" + forgotPasswordExpiry + ", lastUpdate=" + lastUpdate + ", dateAdded="
				+ dateAdded + "]";
	}
}
