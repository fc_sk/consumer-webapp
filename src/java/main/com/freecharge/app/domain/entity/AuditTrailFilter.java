package com.freecharge.app.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 18, 2012
 * Time: 4:02:35 PM
 * To change this template use File | Settings | File Templates.
 */


@Entity
@Table(name = "audit_trail_filter")
public final class AuditTrailFilter implements Serializable {

   private int auditTrailMasterId;
    private String url;
    private String methodName;
    private boolean isActive;
    private int callingOrder;

    public AuditTrailFilter()
    {

    }

     @Id
     @GeneratedValue
     @Column(name = "audit_trail_master_id")
     public int getAuditTrailMasterId() {
        return auditTrailMasterId;
    }

    public void setAuditTrailMasterId(int auditTrailMasterId) {
        this.auditTrailMasterId = auditTrailMasterId;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "method_name")
    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Column(name = "is_active")
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Column(name = "calling_order")
    public int getCallingOrder() {
        return callingOrder;
    }

    public void setCallingOrder(int callingOrder) {
        this.callingOrder = callingOrder;
    }


}

