package com.freecharge.app.domain.entity.jdbc;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.freecharge.payment.services.PaymentGatewayHandler.Constants;

public class BinOffer implements java.io.Serializable {
    public static enum OfferType {
        INSTANT, CASHBACK, REWARD;
        
        public static OfferType fromPaymentResponse(String offerType) {
            if (Constants.OFFER_TYPE_INSTANT.equals(offerType)) {
                return INSTANT;
            } else if (Constants.OFFER_TYPE_CASHBACK.equals(offerType)) {
                return CASHBACK;
            } else {
                throw new IllegalArgumentException("Unknown offer type: [" + offerType + "]");
            }
        }
    }

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String bank;

	private String name;

	private String imageUrl;

	private String bin;

	private String offerKey;
	
	private OfferType offerType;
	
	private String discountType;

	private Double discount;

	private Double minRechargeAmount;
	
	private Integer couponFactor;
	
	private String description;
	
	private String shortDescription;
	
	private String datamap;
	
	private String conditions;

	private int isActive;

	private Calendar validFrom;

	private Calendar validUpto;

	private Calendar createdAt;

	private Calendar updatedAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getOfferKey() {
		return offerKey;
	}

	public void setOfferKey(String offerKey) {
		this.offerKey = offerKey;
	}

	public String getOfferType() {
		return offerType.toString();
	}

	public void setOfferType(String offerType) {
		this.offerType = OfferType.valueOf(offerType);
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getMinRechargeAmount() {
		return minRechargeAmount;
	}

	public void setMinRechargeAmount(Double minRechargeAmount) {
		this.minRechargeAmount = minRechargeAmount;
	}

	public Integer getCouponFactor() {
	    return couponFactor;
	}

	public void setCouponFactor(Integer couponFactor) {
		this.couponFactor = couponFactor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDatamap() {
	    return datamap;
	}

	public void setDatamap(String datamap) {
	    this.datamap = datamap;
	}

	public String getConditions() {
	    return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Calendar getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Calendar validFrom) {
		this.validFrom = validFrom;
	}

	public Calendar getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(Calendar validUpto) {
		this.validUpto = validUpto;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}

	public Calendar getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Calendar updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getBannerImage() {
	    Map<String, String> propertyMap = new HashMap<String, String>();
	    propertyMap = getStringMap(this.getDatamap(), "&"); 

	    if(propertyMap != null) {
	        if(propertyMap.containsKey("bannerImage")){
	            return propertyMap.get("bannerImage");                        
	        }                       
	    }   
	    return null;
	}

	public static Map<String, String> getStringMap(String propertyValue, String delimiter) {
	    StringTokenizer stringTokens = new StringTokenizer(propertyValue, delimiter);
	    Map<String, String> stringMap = new HashMap<String, String>();

	    while(stringTokens.hasMoreTokens()) {
	        String actualElement = stringTokens.nextToken();
	        StringTokenizer stringTokensEq = new StringTokenizer(actualElement, "=");
	        if ( stringTokensEq.countTokens() != 2 ) {
	            throw new RuntimeException("Paramters provided are in unexpeced format");
	        }
	        String key = stringTokensEq.nextToken();
	        String value = stringTokensEq.nextToken();
	        stringMap.put(key, value);        
	    }
	    return stringMap;
	}

}