package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.freecharge.freebill.domain.BillTxnHomePage;

/**
 * TxnHomePage entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "txn_home_page")
public class TxnHomePage implements java.io.Serializable {

	// Fields

	private Integer id;
	private String serviceNumber;
	private String operatorName;
	private String circleName;
	private Float amount;
	private String productType;
	private String sessionId;
	private String lookupId;
	private Timestamp creatredOn;
	private Integer fkOperatorId;
	private Integer fkCircleId;
	private Integer fkProductId;

	private BillTxnHomePage billTxnHomePage;

	// Constructors

	/** default constructor */
	public TxnHomePage() {
	}

	/** minimal constructor */
	public TxnHomePage(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public TxnHomePage(Integer id, String serviceNumber, String operatorName, String circleName, Float amount, String productType, String sessionId, String lookupId, Timestamp creatredOn) {
		this.id = id;
		this.serviceNumber = serviceNumber;
		this.operatorName = operatorName;
		this.circleName = circleName;
		this.amount = amount;
		this.productType = productType;
		this.sessionId = sessionId;
		this.lookupId = lookupId;
		this.creatredOn = creatredOn;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "service_number", length = 45)
	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	@Column(name = "operator_name", length = 45)
	public String getOperatorName() {
		return this.operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	@Column(name = "circle_name")
	public String getCircleName() {
		return this.circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	@Column(name = "amount", precision = 12, scale = 0)
	public Float getAmount() {
		return this.amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	@Column(name = "product_type", length = 45)
	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "session_id", length = 45)
	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name = "lookup_id", length = 45)
	public String getLookupId() {
		return this.lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	@Column(name = "creatred_on", length = 19)
	public Timestamp getCreatredOn() {
		return this.creatredOn;
	}

	public void setCreatredOn(Timestamp creatredOn) {
		this.creatredOn = creatredOn;
	}

	@Column(name = "fk_operator_master_id")
	public Integer getFkOperatorId() {
		return fkOperatorId;
	}

	public void setFkOperatorId(Integer fkOperatorId) {
		this.fkOperatorId = fkOperatorId;
	}

	@Column(name = "fk_circle_master_id")
	public Integer getFkCircleId() {
		return fkCircleId;
	}

	public void setFkCircleId(Integer fkCircleId) {
		this.fkCircleId = fkCircleId;
	}

	@Column(name = "fk_product_master_id")
	public Integer getFkProductId() {
		return fkProductId;
	}

	public void setFkProductId(Integer fkProductId) {
		this.fkProductId = fkProductId;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="fk_bill_txn_home_page_id")
	public BillTxnHomePage getBillTxnHomePage() {
		return billTxnHomePage;
	}

	public void setBillTxnHomePage(BillTxnHomePage billTxnHomePage) {
		this.billTxnHomePage = billTxnHomePage;
	}

	@Override
	public String toString() {
		return "TxnHomePage{" +
				"id=" + id +
				", serviceNumber='" + serviceNumber + '\'' +
				", operatorName='" + operatorName + '\'' +
				", circleName='" + circleName + '\'' +
				", amount=" + amount +
				", productType='" + productType + '\'' +
				", sessionId='" + sessionId + '\'' +
				", lookupId='" + lookupId + '\'' +
				", creatredOn=" + creatredOn +
				", fkOperatorId=" + fkOperatorId +
				", fkCircleId=" + fkCircleId +
				", fkProductId=" + fkProductId +
				", billTxnHomePage=" + billTxnHomePage +
				'}';
	}
}