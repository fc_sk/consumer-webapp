package com.freecharge.app.domain.entity.jdbc;


/**
 * CircleMaster entity. @author MyEclipse Persistence Tools
 */
public class CircleMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer circleMasterId;
	private String name;
	private Boolean isActive;
	private Integer fkCountryMasterId;

	public Integer getCircleMasterId() {
		return this.circleMasterId;
	}

	public void setCircleMasterId(Integer circleMasterId) {
		this.circleMasterId = circleMasterId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}


    @Override
	public boolean equals(Object obj) {
		return this.circleMasterId.equals(((CircleMaster)obj).getCircleMasterId());
	}
    
    @Override
    public int hashCode() {    	
    	return this.getCircleMasterId().hashCode();
    }
    @Override
    public String toString() {
		return "CircleMasterId : "+ this.circleMasterId + " Name : " + this.name + " is Active : " +this.isActive + " fkCountryMasterId : " +this.fkCountryMasterId;
    	
    }
}