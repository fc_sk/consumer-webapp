package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dao.PgMisDisplayRecord;
import com.freecharge.payment.services.PaymentType.PaymentTypeName;

public class PgMisDisplayRecordRowMapper implements RowMapper<PgMisDisplayRecord> {

	@Override
	public PgMisDisplayRecord mapRow(ResultSet rs, int row) throws SQLException {

		PgMisDisplayRecord adminUsers = new PgMisDisplayRecord();
		adminUsers.setTxnId(rs.getInt("payment_rq_id"));
		adminUsers.setAppTxnId(rs.getString("order_id"));
		adminUsers.setAmount(rs.getString("amount"));
		adminUsers.setPaymentMode(rs.getString("payment_mode"));
		adminUsers.setPaymentGateway(rs.getString("payment_gateway"));
		
		Integer paymentTypeCode = rs.getInt("payment_type");
		
		String paymentTypeCodeString = PaymentTypeName.fromPaymentTypeCode(paymentTypeCode).toString();
		
        adminUsers.setPaymentType(paymentTypeCodeString);
		adminUsers.setGatewayTransactionID(rs.getString("pg_transaction_id"));
		adminUsers.setPaymentOption(rs.getString("payment_option"));
		adminUsers.setPaymentMessage(rs.getString("pg_message"));
		adminUsers.setSendToPgDate(rs.getString("sent_to_pg"));
		adminUsers.setReceivedFromPgdate(rs.getTimestamp("recieved_from_pg"));
		adminUsers.setRequestCreatedOn(rs.getDate("created_on"));
		adminUsers.setStatus(rs.getString("is_successful"));
		adminUsers.setMtxnId(rs.getString("mtxn_id"));
		return adminUsers;
	}

}
