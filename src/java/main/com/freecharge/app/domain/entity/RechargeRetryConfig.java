package com.freecharge.app.domain.entity;

public class RechargeRetryConfig implements java.io.Serializable {
    
    private static final long serialVersionUID = 1L;
    
	private long id;
	private String errorCode;
	private String errorMsg;
	private int maxRetryCount;
	private long retryInterval;
	private long maxRetryDuration;
	private boolean isActive;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public int getMaxRetryCount() {
		return maxRetryCount;
	}
	public void setMaxRetryCount(int maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}
	public long getRetryInterval() {
		return retryInterval;
	}
	public void setRetryInterval(long retryInterval) {
		this.retryInterval = retryInterval;
	}
	public long getMaxRetryDuration() {
		return maxRetryDuration;
	}
	public void setMaxRetryDuration(long maxRetryDuration) {
		this.maxRetryDuration = maxRetryDuration;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
    public String getErrorMsg() {
        return errorMsg;
    }
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
