package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.wallet.service.WalletTransaction;

public class WalletTransactionRowMapper implements RowMapper<WalletTransaction> {

    @Override
    public WalletTransaction mapRow(ResultSet rs, int row) throws SQLException {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.setWalletTransactionId(rs.getInt("WALLET_TRANSACTION_ID"));
        walletTransaction.setFkWalletId(rs.getInt("fk_wallet_id"));
        walletTransaction.setTransactionDate(rs.getDate("transaction_date"));
        walletTransaction.setCallerRefrence(rs.getString("caller_refrence"));
        walletTransaction.setTransactionAmount(rs.getDouble("TXN_AMOUNT"));
        walletTransaction.setTransactionType(rs.getString("transaction_type"));
        walletTransaction.setFundSource(rs.getString("fund_source"));
        walletTransaction.setFundDestination(rs.getString("fund_destination"));
        walletTransaction.setMetadata(rs.getString("metadata"));
        walletTransaction.setOrderId(rs.getString("order_id"));
        walletTransaction.setRunningBalance(rs.getDouble("NEW_BALANCE"));
        walletTransaction.setFkUserId(rs.getInt("FK_USER_ID"));
        return walletTransaction;
    }
}
