package com.freecharge.app.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TESTTABLE")
public final class TestTable
{

	private int Id;
	private String name;

	public TestTable()
	{

	}

	@Id
	@GeneratedValue
	@Column(name = "ID")
	public int getId()
	{
		return Id;
	}

	@Column(name = "NAME")
	public String getName()
	{
		return name;
	}

	public void setId(int Id)
	{
		this.Id = Id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}
