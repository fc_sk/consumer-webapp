package com.freecharge.app.domain.entity;

import java.io.Serializable;
import java.util.Date;

public class PgAlert implements Serializable {
    
    private Long pgId;
    private Date startDateTime;
    private Date endDateTime;
    private String alertMessage;

    public PgAlert() {

    }

    public PgAlert(Long pgId, Date startDateTime, Date endDateTime, String alertMessage) {
        super();
        this.pgId = pgId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.alertMessage = alertMessage;
    }
    
    public Long getPgId() {
        return pgId;
    }

    public void setpgId(Long pgId) {
        this.pgId = pgId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    /**
     * For easier logging
     */
    public String toString() {
        return String.format("PgAlert[pgId=%d, start=%s, end=%s, msg=%s]",
                              pgId, startDateTime, endDateTime, alertMessage);
    }

}
