package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InResponse;

public class InResponseMapper implements RowMapper<InResponse> {

	@Override
	public InResponse mapRow(ResultSet rs, int row) throws SQLException {
		InResponse inResponse = new InResponse();
		
		inResponse.setResponseId(rs.getLong("response_id"));
		inResponse.setRequestId(rs.getLong("request_id"));
		inResponse.setRawRequest(rs.getString("raw_request"));
		inResponse.setRawResponse(rs.getString("raw_response"));
		inResponse.setAggrTransId(rs.getString("aggr_trans_id"));
		inResponse.setOprTransId(rs.getString("opr_trans_id"));
		inResponse.setAggrName(rs.getString("aggr_name"));
		inResponse.setAggrRespCode(rs.getString("aggr_resp_code"));
		inResponse.setAggrRespMsg(rs.getString("aggr_resp_msg"));
		inResponse.setInRespCode(rs.getString("in_resp_code"));
		inResponse.setInRespMsg(rs.getString("in_resp_msg"));
		inResponse.setRetryNumber(rs.getInt("retry_number"));
		inResponse.setCreatedTime(rs.getTimestamp("created_time"));
		inResponse.setUpdatedTime(rs.getTimestamp("updated_time"));
		
		return inResponse;
	}
}
