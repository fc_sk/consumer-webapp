package com.freecharge.app.domain.entity.jdbc.mappers;

import com.freecharge.admin.helper.AdditionalRewardAdminHelper;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdditionalRewardRowMapper implements RowMapper {
    @Override
    public AdditionalReward mapRow(ResultSet resultSet, int i) throws SQLException {
        AdditionalReward additionalReward = new AdditionalReward();
        additionalReward.setRewardName(resultSet.getString(AdditionalRewardAdminHelper.REWARD_NAME));
        additionalReward.setId(resultSet.getInt("id"));
        additionalReward.setRewardValue(resultSet.getFloat(AdditionalRewardAdminHelper.REWARD_VALUE));
        additionalReward.setRewardDiscountType(resultSet.getString(AdditionalRewardAdminHelper.REWARD_DISCOUNT_TYPE));
        additionalReward.setRewardMaxDiscount(resultSet.getFloat(AdditionalRewardAdminHelper.REWARD_MAX_DISCOUNT));
        additionalReward.setRewardConditionId(resultSet.getInt(AdditionalRewardAdminHelper.REWARD_CONDITION_ID));
        additionalReward.setEnabled(resultSet.getBoolean(AdditionalRewardAdminHelper.IS_ENABLED));
        
        return additionalReward;
    }
}
