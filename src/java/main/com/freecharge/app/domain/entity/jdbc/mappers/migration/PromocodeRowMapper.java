package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.Promocode;

public class PromocodeRowMapper implements RowMapper<Promocode> {
	@Override
	public Promocode mapRow(ResultSet rs, int row) throws SQLException {
		Promocode promocode = new Promocode();
		
		promocode.setPromocodeId(rs.getInt("promocode_id"));
		promocode.setPromoTitle(rs.getString("promo_title"));
		promocode.setPromoCode(rs.getString("promo_code"));
		promocode.setSuccessMsg(rs.getString("success_msg"));
		promocode.setFailureMsg(rs.getString("failure_msg"));
		promocode.setValidFrom(rs.getTimestamp("valid_from"));
		promocode.setValidUpto(rs.getTimestamp("valid_upto"));
		
		return promocode;
	}

}
