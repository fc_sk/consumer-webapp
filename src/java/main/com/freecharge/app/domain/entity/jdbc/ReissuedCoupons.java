package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 22/10/12 Time: 10:15 AM To change this template use File
 * | Settings | File Templates.
 */
public class ReissuedCoupons implements DBEntity {

	@ColumnMapping(name = "id")
	private Integer id;

	@ColumnMapping(name = "order_id")
	private String orderId;

	@ColumnMapping(name = "reissued_coupon_id")
	private Integer reissuedCouponId;

	@ColumnMapping(name = "reissued_coupon_quantity")
	private Integer reissuedCouponQuantity;

	@ColumnMapping(name = "fc_ticket_id")
	private Integer fcTicketId;

	@ColumnMapping(name = "remarks")
	private String remarks;

	@ColumnMapping(name = "is_deleted")
	private Boolean isDeleted;

	@ColumnMapping(name = "created_on")
	private Date createdOn;

	@ColumnMapping(name = "updated_on")
	private Date updatedOn;

	@ColumnMapping(name = "created_by")
	private String createdBy;

	@ColumnMapping(name = "deleted_by")
	private String deletedBy;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getReissuedCouponId() {
		return reissuedCouponId;
	}

	public void setReissuedCouponId(Integer reissuedCouponId) {
		this.reissuedCouponId = reissuedCouponId;
	}

	public Integer getReissuedCouponQuantity() {
		return reissuedCouponQuantity;
	}

	public void setReissuedCouponQuantity(Integer reissuedCouponQuantity) {
		this.reissuedCouponQuantity = reissuedCouponQuantity;
	}

	public Integer getFcTicketId() {
		return fcTicketId;
	}

	public void setFcTicketId(Integer fcTicketId) {
		this.fcTicketId = fcTicketId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Override
	public String toString() {
		return "OrderFullfillmentDetails{" + "id="
				+ id
					+ ", orderId='"
					+ orderId
					+ '\''
					+ ", reissuedCouponId='"
					+ reissuedCouponId
					+ '\''
					+ ", reissuedCouponQuantity='"
					+ reissuedCouponQuantity
					+ '\''
					+ ", fcTicketId="
					+ fcTicketId
					+ ", remarks='"
					+ remarks
					+ '\''
					+ ", isDeleted='"
					+ isDeleted
					+ '\''
					+ ", createdOn='"
					+ createdOn
					+ '\''
					+ ", updatedOn="
					+ updatedOn
					+ ", createdBy="
					+ createdBy
					+ ", deletedBy="
					+ deletedBy
					+  '}';
	}

	@Override
	public String getTableName() {
		return "reissued_coupons";
	}

	@Override
	public UpdateDBVO getUpdateDBVO() {
		return null; // To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public SelectDBVO getSelectDBVO() {
		SelectQuery selectQuery = new SelectQuery(this);
		return selectQuery.getSelectDBVO();
	}

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml", "web-delegates.xml");
		EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));
		ReissuedCoupons reissuedCoupons = new ReissuedCoupons();
		reissuedCoupons.setOrderId("FCVW1209060000029");
		List<ReissuedCoupons> transactionHomePages = easyDBWriteDAO.get(reissuedCoupons);
		System.out.println(transactionHomePages);
		ctx.close();
	}

}
