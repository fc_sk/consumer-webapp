package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Users entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "users")
public class Users implements java.io.Serializable {

	// Fields

	private Integer userId;
	private Integer fkAffiliateProfileId;
	private Integer type;
	private String email;
	private String password;
	private String mobileNo;
	private Date dob;
	private Boolean isActive;
	private Timestamp dateAdded;
	private Timestamp lastLoggedin;
	private Boolean isLoggedin;
	private String morf;
	private Timestamp lastUpdate;
	private String forgotPasswordKey;
	private Date forgotPasswordExpiry;
	private Integer fkRefferredByUserId;
	
	private List<UserProfile> userProfiles;

	// Constructors

	/** default constructor */
	public Users() {
	}

	/** minimal constructor */
	public Users(String email, String password, String mobileNo,
			Timestamp lastUpdate) {
		this.email = email;
		this.password = password;
		this.mobileNo = mobileNo;
		this.lastUpdate = lastUpdate;
	}

	/** full constructor */
	public Users(Integer fkAffiliateProfileId, Integer type, String email,
			String password, String mobileNo, Date dob, Boolean isActive,
			Timestamp dateAdded, Timestamp lastLoggedin, Boolean isLoggedin,
			String morf, Timestamp lastUpdate, String forgotPasswordKey,
			Date forgotPasswordExpiry, Integer fkRefferredByUserId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
		this.type = type;
		this.email = email;
		this.password = password;
		this.mobileNo = mobileNo;
		this.dob = dob;
		this.isActive = isActive;
		this.dateAdded = dateAdded;
		this.lastLoggedin = lastLoggedin;
		this.isLoggedin = isLoggedin;
		this.morf = morf;
		this.lastUpdate = lastUpdate;
		this.forgotPasswordKey = forgotPasswordKey;
		this.forgotPasswordExpiry = forgotPasswordExpiry;
		this.fkRefferredByUserId = fkRefferredByUserId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "fk_affiliate_profile_id")
	public Integer getFkAffiliateProfileId() {
		return this.fkAffiliateProfileId;
	}

	public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "email", nullable = false, length = 150)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "password", nullable = false, length = 50)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "mobile_no", nullable = false, length = 20)
	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dob", length = 0)
	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "date_added", length = 0)
	public Timestamp getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	@Column(name = "last_loggedin", length = 0)
	public Timestamp getLastLoggedin() {
		return this.lastLoggedin;
	}

	public void setLastLoggedin(Timestamp lastLoggedin) {
		this.lastLoggedin = lastLoggedin;
	}

	@Column(name = "is_loggedin")
	public Boolean getIsLoggedin() {
		return this.isLoggedin;
	}

	public void setIsLoggedin(Boolean isLoggedin) {
		this.isLoggedin = isLoggedin;
	}

	@Column(name = "morf", length = 5)
	public String getMorf() {
		return this.morf;
	}

	public void setMorf(String morf) {
		this.morf = morf;
	}

	@Column(name = "last_update", nullable = false, length = 0)
	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name = "forgot_password_key", length = 128)
	public String getForgotPasswordKey() {
		return this.forgotPasswordKey;
	}

	public void setForgotPasswordKey(String forgotPasswordKey) {
		this.forgotPasswordKey = forgotPasswordKey;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "forgot_password_expiry", length = 0)
	public Date getForgotPasswordExpiry() {
		return this.forgotPasswordExpiry;
	}

	public void setForgotPasswordExpiry(Date forgotPasswordExpiry) {
		this.forgotPasswordExpiry = forgotPasswordExpiry;
	}

	@Column(name = "fk_refferred_by_user_id")
	public Integer getFkRefferredByUserId() {
		return this.fkRefferredByUserId;
	}

	public void setFkRefferredByUserId(Integer fkRefferredByUserId) {
		this.fkRefferredByUserId = fkRefferredByUserId;
	}

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_id")
	public List<UserProfile> getUserProfile() {
		return userProfiles;
	}

	public void setUserProfile(List<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", fkAffiliateProfileId=" + fkAffiliateProfileId +
                ", type=" + type +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", dob=" + dob +
                ", isActive=" + isActive +
                ", dateAdded=" + dateAdded +
                ", lastLoggedin=" + lastLoggedin +
                ", isLoggedin=" + isLoggedin +
                ", morf='" + morf + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", forgotPasswordKey='" + forgotPasswordKey + '\'' +
                ", forgotPasswordExpiry=" + forgotPasswordExpiry +
                ", fkRefferredByUserId=" + fkRefferredByUserId +
                '}';
    }
}