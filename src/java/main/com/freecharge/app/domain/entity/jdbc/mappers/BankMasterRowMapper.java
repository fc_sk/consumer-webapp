package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.BankMaster;

public class BankMasterRowMapper implements RowMapper<BankMaster> {

	@Override
	public BankMaster mapRow(ResultSet rs, int row) throws SQLException {
		BankMaster bankMaster = new BankMaster();
		bankMaster.setActive(rs.getBoolean("is_active"));
		bankMaster.setBankCode(rs.getString("bank_code"));
		bankMaster.setCodeUI(rs.getString("code_ui"));
		bankMaster.setDisplayName(rs.getString("display_name"));
		bankMaster.setName(rs.getString("name"));
		bankMaster.setPaymentOption(rs.getInt("payment_option"));
		bankMaster.setId(rs.getInt("banks_grp_master_id"));
		return bankMaster;
	}

}
