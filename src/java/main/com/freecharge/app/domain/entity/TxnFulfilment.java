package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * TxnFulfilment entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="txn_fulfilment")

public class TxnFulfilment  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer txnHomePageId;
     private String email;
     private String addressMobile;
     private Boolean isGuest;
     /*private String billingAdd;
     private String billingCity;
     private Integer billingFkStateMasterId;
     private Integer billingFkCountryMasterId;
     private String billingCountry;
     private String billingPincode;*/
     private String deliveryAdd;
    private Integer deliveryFkCityMasterId;
     private Integer deliveryFkStateMasterId;
     private Integer deliveryFkCountryMasterId;    
     private String deliveryCountry;
     private String deliveryPincode;
     private String deliveryName;
     private Timestamp smsSent;
     private Timestamp emailSent;
     private Integer transactionStatus;
     private String orderId;
     private String firstName;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String deliveryStreet;
    private String deliveryArea;
    private String deliveryLandmark;
    private Integer fkUserProfileId;


// Constructors

    /** default constructor */
    public TxnFulfilment() {
    }

	/** minimal constructor */
    public TxnFulfilment(Integer id) {
        this.id = id;
    }
    
    /** full constructor */
    public TxnFulfilment(Integer id, Integer txnHomePageId, String email, String addressMobile, Boolean isGuest, String deliveryAdd, String deliveryCountry, String deliveryPincode, String deliveryName, Timestamp smsSent, Timestamp emailSent, Integer transactionStatus, String orderId) {
        this.id = id;
        this.txnHomePageId = txnHomePageId;
        this.email = email;
        this.addressMobile = addressMobile;
        this.isGuest = isGuest;
        this.deliveryAdd = deliveryAdd;
        this.deliveryCountry = deliveryCountry;
        this.deliveryPincode = deliveryPincode;
        this.deliveryName = deliveryName;
        this.smsSent = smsSent;
        this.emailSent = emailSent;
        this.transactionStatus = transactionStatus;
        this.orderId = orderId;
    }

   
    // Property accessors
    @Id
    @GeneratedValue(strategy = IDENTITY)        
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="txn_home_page_id")

    public Integer getTxnHomePageId() {
        return this.txnHomePageId;
    }
    
    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }
    
    @Column(name="email")

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name="address_mobile", length=12)

    public String getAddressMobile() {
        return this.addressMobile;
    }
    
    public void setAddressMobile(String addressMobile) {
        this.addressMobile = addressMobile;
    }
    
    @Column(name="is_guest")

    public Boolean getIsGuest() {
        return this.isGuest == null ? Boolean.FALSE : this.isGuest;
    }
    
    public void setIsGuest(Boolean isGuest) {
        if (isGuest == null) {
            this.isGuest = false;
        } else {
            this.isGuest = isGuest;
        }
    }
    
    /*
    @Column(name="billing_add")

    public String getBillingAdd() {
        return this.billingAdd;
    }
    
    public void setBillingAdd(String billingAdd) {
        this.billingAdd = billingAdd;
    }
    
    @Column(name="billing_city")

    public String getBillingCity() {
        return this.billingCity;
    }
    
    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }
    
    @Column(name="billing_country")

    public String getBillingCountry() {
        return this.billingCountry;
    }
    
    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }
    
    @Column(name="billing_pincode")

    public String getBillingPincode() {
        return this.billingPincode;
    }
    
    public void setBillingPincode(String billingPincode) {
        this.billingPincode = billingPincode;
    }
    */
    @Column(name="delivery_add")

    public String getDeliveryAdd() {
        return this.deliveryAdd;
    }
    
    public void setDeliveryAdd(String deliveryAdd) {
        this.deliveryAdd = deliveryAdd;
    }
    

    @Column(name="delivery_country")

    public String getDeliveryCountry() {
        return this.deliveryCountry;
    }
    
    public void setDeliveryCountry(String deliveryCountry) {
        this.deliveryCountry = deliveryCountry;
    }
    
    @Column(name="delivery_pincode")

    public String getDeliveryPincode() {
        return this.deliveryPincode;
    }
    
    public void setDeliveryPincode(String deliveryPincode) {
        this.deliveryPincode = deliveryPincode;
    }
    
    @Column(name="delivery_name")

    public String getDeliveryName() {
        return this.deliveryName;
    }
    
    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }
    
    @Column(name="sms_sent", length=19)

    public Timestamp getSmsSent() {
        return this.smsSent;
    }
    
    public void setSmsSent(Timestamp smsSent) {
        this.smsSent = smsSent;
    }
    
    @Column(name="email_sent", length=19)

    public Timestamp getEmailSent() {
        return this.emailSent;
    }
    
    public void setEmailSent(Timestamp emailSent) {
        this.emailSent = emailSent;
    }
    
    @Column(name="transaction_status")

    public Integer getTransactionStatus() {
        return this.transactionStatus;
    }
    
    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
    
    @Column(name="order_id", length=45)

    public String getOrderId() {
        return this.orderId;
    }
    
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
	/*
    @Column(name = "billing_fk_state_master_id")
    public Integer getBillingFkStateMasterId() {
        return billingFkStateMasterId;
    }

    public void setBillingFkStateMasterId(Integer billingFkStateMasterId) {
        this.billingFkStateMasterId = billingFkStateMasterId;
    }
    */
    @Column(name = "delivery_fk_country_master_id")
    public Integer getDeliveryFkCountryMasterId() {
        return deliveryFkCountryMasterId;
    }

    public void setDeliveryFkCountryMasterId(Integer deliveryFkCountryMasterId) {
        this.deliveryFkCountryMasterId = deliveryFkCountryMasterId;
    } 
    /*
    @Column(name = "billing_fk_country_master_id")
    public Integer getBillingFkCountryMasterId() {
        return billingFkCountryMasterId;
    }

    public void setBillingFkCountryMasterId(Integer billingFkCountryMasterId) {
        this.billingFkCountryMasterId = billingFkCountryMasterId;
    }
    */
    @Column(name = "delivery_fk_state_master_id")
    public Integer getDeliveryFkStateMasterId() {
        return deliveryFkStateMasterId;
    }

    public void setDeliveryFkStateMasterId(Integer deliveryFkStateMasterId) {
        this.deliveryFkStateMasterId = deliveryFkStateMasterId;
    }
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Column(name = "delivery_street")
    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public void setDeliveryStreet(String deliveryStreet) {
        this.deliveryStreet = deliveryStreet;
    }
    @Column(name = "delivery_area")
    public String getDeliveryArea() {
        return deliveryArea;
    }

    public void setDeliveryArea(String deliveryArea) {
        this.deliveryArea = deliveryArea;
    }
    @Column(name = "delivery_landmark")
    public String getDeliveryLandmark() {
        return deliveryLandmark;
    }

    public void setDeliveryLandmark(String deliveryLandmark) {
        this.deliveryLandmark = deliveryLandmark;
    }

    @Column(name="delivery_city")
    public Integer getDeliveryFkCityMasterId() {
        return deliveryFkCityMasterId;
    }

    public void setDeliveryFkCityMasterId(Integer deliveryFkCityMasterId) {
        this.deliveryFkCityMasterId = deliveryFkCityMasterId;
    }
    @Column(name="fk_user_profile_id")
    public Integer getFkUserProfileId() {
  		return fkUserProfileId;
  	}

  	public void setFkUserProfileId(Integer fkUserProfileId) {
  		this.fkUserProfileId = fkUserProfileId;
  	}
}