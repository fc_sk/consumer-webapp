package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderIDMapper implements RowMapper<String> {

    @Override
    public String mapRow(ResultSet rs, int row) throws SQLException {
        return rs.getString("order_id");
    }
}
