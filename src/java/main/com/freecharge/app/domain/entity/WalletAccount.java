package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * WalletAccount entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "wallet_account")
public class WalletAccount implements java.io.Serializable {

	// Fields

	private Integer walletAccountId;
	private Integer fkUsersId;
	private Integer fkAffiliateProfileId;
	private String name;
	private Double currentBalance;
	private Boolean isActive;
	private Timestamp lastUsed;
	private Timestamp createdOn;

	// Constructors

	/** default constructor */
	public WalletAccount() {
	}

	/** full constructor */
	public WalletAccount(Integer fkUsersId, Integer fkAffiliateProfileId,
			String name, Double currentBalance, Boolean isActive,
			Timestamp lastUsed, Timestamp createdOn) {
		this.fkUsersId = fkUsersId;
		this.fkAffiliateProfileId = fkAffiliateProfileId;
		this.name = name;
		this.currentBalance = currentBalance;
		this.isActive = isActive;
		this.lastUsed = lastUsed;
		this.createdOn = createdOn;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "wallet_account_id", unique = true, nullable = false)
	public Integer getWalletAccountId() {
		return this.walletAccountId;
	}

	public void setWalletAccountId(Integer walletAccountId) {
		this.walletAccountId = walletAccountId;
	}

	@Column(name = "fk_users_id")
	public Integer getFkUsersId() {
		return this.fkUsersId;
	}

	public void setFkUsersId(Integer fkUsersId) {
		this.fkUsersId = fkUsersId;
	}

	@Column(name = "fk_affiliate_profile_id")
	public Integer getFkAffiliateProfileId() {
		return this.fkAffiliateProfileId;
	}

	public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
	}

	@Column(name = "name", length = 128)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "current_balance", precision = 10)
	public Double getCurrentBalance() {
		return this.currentBalance;
	}

	public void setCurrentBalance(Double currentBalance) {
		this.currentBalance = currentBalance;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "last_used", length = 0)
	public Timestamp getLastUsed() {
		return this.lastUsed;
	}

	public void setLastUsed(Timestamp lastUsed) {
		this.lastUsed = lastUsed;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

}