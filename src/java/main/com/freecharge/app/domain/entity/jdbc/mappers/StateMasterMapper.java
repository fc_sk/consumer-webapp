package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.StateMaster;

public class StateMasterMapper implements RowMapper<StateMaster> {
	@Override
	public StateMaster mapRow(ResultSet rs, int row) throws SQLException {
		StateMaster stateMaster = new StateMaster();
		
		stateMaster.setStateMasterId(rs.getInt("state_master_id"));
		stateMaster.setStateName(rs.getString("state_name"));
		stateMaster.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		stateMaster.setFkZoneMasterId(rs.getInt("fk_zone_master_id"));
		stateMaster.setIsActive(rs.getBoolean("is_active"));
		
		return stateMaster;
	}

}
