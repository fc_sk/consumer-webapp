package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.ZoneMaster;

public class ZoneMasterRowMapper implements RowMapper<ZoneMaster> {
	@Override
	public ZoneMaster mapRow(ResultSet rs, int row) throws SQLException {
		ZoneMaster zone = new ZoneMaster();
		zone.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		zone.setZoneMasterId(rs.getInt("zone_master_id"));
		zone.setZoneName(rs.getString("zone_name"));
		return zone;
	}

}