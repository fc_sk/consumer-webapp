package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.common.businessdo.CartItemsBusinessDO;

public class CartItemsRowMapper implements RowMapper<CartItemsBusinessDO> {

    @Override
    public CartItemsBusinessDO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CartItemsBusinessDO ci = new CartItemsBusinessDO();
        
        ci.setAddedOn(rs.getDate("added_on"));
        final Boolean isDeleted = rs.getBoolean("is_deleted");
        
        if (isDeleted == null) {
            ci.setDeleted(false);
        } else {
            ci.setDeleted(isDeleted);
        }
        
        ci.setDeletedOn(rs.getDate("deleted_on"));
        ci.setDisplayLabel(rs.getString("display_label"));
        ci.setEntityId(rs.getString("entity_id"));
        ci.setId(rs.getLong("cart_items_id"));
        ci.setItemId(rs.getInt("fk_item_id"));
        ci.setPrice(rs.getDouble("price"));
        ci.setProductMasterId(rs.getInt("fl_product_master_id"));
        ci.setQuantity(rs.getInt("quantity"));
        
        
        return ci;
    }

}
