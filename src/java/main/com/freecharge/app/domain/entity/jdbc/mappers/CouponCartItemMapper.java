package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.api.coupon.common.CouponConstants;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;

public class CouponCartItemMapper implements RowMapper<CouponCartItem> {
	@Override
	public CouponCartItem mapRow(ResultSet rs, int row) throws SQLException {
		CouponCartItem cartItem = new CouponCartItem();
		cartItem.setCouponId(rs.getInt("fk_item_id"));
		cartItem.setQuantity(rs.getInt("quantity"));
		cartItem.setPrice(rs.getInt("price"));
		int productMasterId = rs.getInt("fl_product_master_id");
		CouponOptinType type = CouponOptinType.PRICED;
		if (productMasterId == CouponConstants.PRODUCT_MASTER_ID_PRICED_COUPONS){
			type = CouponOptinType.OPTIN;
		} else if(productMasterId == CouponConstants.PRODUCT_MASTER_ID_HCOUPONS) {
			type = CouponOptinType.HCOUPON;
		}
		cartItem.setType(type);
		return cartItem;
	}

}
