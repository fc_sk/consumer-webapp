package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * OrderId entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "order_id")
public class OrderId implements java.io.Serializable {

	// Fields

	private Integer pkOrderId;
	private String lookupId;
	private String sessionId;
	private String orderId;
	private Integer fkProductMasterId;
	private Integer fkAffliateProfileId;
	private Timestamp createdOn;
    private Integer channelId;
     private Integer orderIdInc;









	// Constructors

	/** default constructor */
	public OrderId() {
	}

	/** minimal constructor */
	public OrderId(String lookupId, String sessionId, String orderId,
			Integer fkProductMasterId, Timestamp createdOn) {
		this.lookupId = lookupId;
		this.sessionId = sessionId;
		this.orderId = orderId;
		this.fkProductMasterId = fkProductMasterId;
		this.createdOn = createdOn;
	}

	/** full constructor */
	public OrderId(String lookupId, String sessionId, String orderId,
			Integer fkProductMasterId, int fkAffliateProfileId, int channelId,
			Timestamp createdOn) {
		this.lookupId = lookupId;
		this.sessionId = sessionId;
		this.orderId = orderId;
		this.fkProductMasterId = fkProductMasterId;
		this.fkAffliateProfileId = fkAffliateProfileId;
        this.channelId=channelId;
		this.createdOn = createdOn;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pk_order_id", unique = true, nullable = false)
	public Integer getPkOrderId() {
		return this.pkOrderId;
	}

	public void setPkOrderId(Integer pkOrderId) {
		this.pkOrderId = pkOrderId;
	}

	@Column(name = "lookup_id", nullable = false, length = 36)
	public String getLookupId() {
		return this.lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	@Column(name = "session_id", nullable = false, length = 36)
	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name = "order_id", nullable = false, length = 64)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "fk_product_master_id", nullable = false)
	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	@Column(name = "fk_affliate_profile_id", length = 36)
	public Integer getFkAffliateProfileId() {
		return this.fkAffliateProfileId;
	}

	public void setFkAffliateProfileId(Integer fkAffliateProfileId) {
		this.fkAffliateProfileId = fkAffliateProfileId;
	}

	@Column(name = "created_on", nullable = false, length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

     @Column(name = "channel_id", nullable = false)
     public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }
     @Column(name = "order_id_inc", nullable = false)
     public Integer getOrderIdInc() {
        return orderIdInc;
    }

    public void setOrderIdInc(Integer orderIdInc) {
        this.orderIdInc = orderIdInc;
    }

}