package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.post.fulfillment.state.PostFulfillmentTaskState;
import com.freecharge.web.webdo.PostFFTaskModel;

public class PostFFTaskRowMapper implements RowMapper<PostFFTaskModel> {
	

	@Override
	public PostFFTaskModel mapRow(ResultSet rs, int row) throws SQLException {
		PostFFTaskModel model = new PostFFTaskModel();
		
		model.setCreated(rs.getDate("created"));
		model.setUpdated(rs.getDate("updated"));
		model.setId(rs.getLong("id"));
		model.setOrderId(rs.getString("order_id"));
		model.setRawRequest(rs.getString("raw_request"));
		model.setReason(rs.getString("reason"));
		model.setRetryCount(rs.getInt("retry_count"));
		model.setState(PostFulfillmentTaskState.valueOf(rs.getString("state")));
		return model;
	}
}
