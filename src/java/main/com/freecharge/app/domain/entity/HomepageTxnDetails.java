package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * HomepageTxnDetails entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "homepage_txn_details")
public class HomepageTxnDetails implements java.io.Serializable {

	// Fields

	private Integer homepageTxnDetailsId;
	private String sessionUniqueId;
	private String lookupId;
	private String productServiceNumber;
	private String email;
	private String coupon;
	private Timestamp createdOn;
	//private Timestamp modifiedOn;
	private Boolean isLoggedin;
	private Boolean myaccountAccessed;
	private String aliasUsed;
	private double amount;
	private Integer productSelected;
    private String operator;
    private String circle;

	// Constructors

	/** default constructor */
	public HomepageTxnDetails() {
	}

	/** minimal constructor */
	/*public HomepageTxnDetails(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}*/

	/** full constructor */
	public HomepageTxnDetails(String sessionUniqueId, String lookupId,
			String productServiceNumber, String email, String coupon,
			Timestamp createdOn, Timestamp modifiedOn, Boolean isLoggedin,
			Boolean myaccountAccessed, String aliasUsed, double amount,
			Integer productSelected, String operator, String circle) {
		this.sessionUniqueId = sessionUniqueId;
		this.lookupId = lookupId;
		this.productServiceNumber = productServiceNumber;
		this.email = email;
		this.coupon = coupon;
		this.createdOn = createdOn;
		//this.modifiedOn = modifiedOn;
		this.isLoggedin = isLoggedin;
		this.myaccountAccessed = myaccountAccessed;
		this.aliasUsed = aliasUsed;
		this.amount = amount;
		this.productSelected = productSelected;
        this.operator = operator;
        this.circle = circle;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "homepage_txn_details_id", unique = true, nullable = false)
	public Integer getHomepageTxnDetailsId() {
		return this.homepageTxnDetailsId;
	}

	public void setHomepageTxnDetailsId(Integer homepageTxnDetailsId) {
		this.homepageTxnDetailsId = homepageTxnDetailsId;
	}

	@Column(name = "session_unique_id", length = 36)
	public String getSessionUniqueId() {
		return this.sessionUniqueId;
	}

	public void setSessionUniqueId(String sessionUniqueId) {
		this.sessionUniqueId = sessionUniqueId;
	}

	@Column(name = "lookup_id")
	public String getLookupId() {
		return this.lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	@Column(name = "product_service_number", length = 64)
	public String getProductServiceNumber() {
		return this.productServiceNumber;
	}

	public void setProductServiceNumber(String productServiceNumber) {
		this.productServiceNumber = productServiceNumber;
	}

	@Column(name = "email", length = 128)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "coupon", length = 128)
	public String getCoupon() {
		return this.coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	//@Column(name = "modified_on", nullable = false, length = 0)
	/*public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
*/
	@Column(name = "is_loggedin")
	public Boolean getIsLoggedin() {
		return this.isLoggedin;
	}

	public void setIsLoggedin(Boolean isLoggedin) {
		this.isLoggedin = isLoggedin;
	}

	@Column(name = "myaccount_accessed")
	public Boolean getMyaccountAccessed() {
		return this.myaccountAccessed;
	}

	public void setMyaccountAccessed(Boolean myaccountAccessed) {
		this.myaccountAccessed = myaccountAccessed;
	}

	@Column(name = "alias_used", length = 128)
	public String getAliasUsed() {
		return this.aliasUsed;
	}

	public void setAliasUsed(String aliasUsed) {
		this.aliasUsed = aliasUsed;
	}

	@Column(name = "amount", precision = 10)
	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "product_selected")
	public Integer getProductSelected() {
		return this.productSelected;
	}

	public void setProductSelected(Integer productSelected) {
		this.productSelected = productSelected;
	}

    @Column(name = "operator")
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Column(name = "circle")
    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

}