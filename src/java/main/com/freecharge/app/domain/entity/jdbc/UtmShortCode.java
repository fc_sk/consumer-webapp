package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.util.Date;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class UtmShortCode implements Serializable {
	private static final long serialVersionUID = 1L;
	private int shortCodeId;
    private String shortCode;
    private int campaignId;
    private Date startDate;
    private Date endDate;
    private String freefundClassId;
    private String arDealId;
    private String webUrl;
    private String mobileWebUrl;
    private String androidAppUrl;
    private String iosAppUrl;
    private String windowsAppUrl;
    private String createdBy;
    private String createdAt;
    private String updatedBy;
    private String updatedAt;

    public int getShortCodeId() {
        return shortCodeId;
    }

    public void setShortCodeId(int shortCodeId) {
        this.shortCodeId = shortCodeId;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFreefundClassId() {
        return freefundClassId;
    }

    public void setFreefundClassId(String freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public String getArDealId() {
        return arDealId;
    }

    public void setArDealId(String arDealId) {
        this.arDealId = arDealId;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getMobileWebUrl() {
        return mobileWebUrl;
    }

    public void setMobileWebUrl(String mobileWebUrl) {
        this.mobileWebUrl = mobileWebUrl;
    }

    public String getAndroidAppUrl() {
        return androidAppUrl;
    }

    public void setAndroidAppUrl(String androidAppUrl) {
        this.androidAppUrl = androidAppUrl;
    }

    public String getIosAppUrl() {
        return iosAppUrl;
    }

    public void setIosAppUrl(String iosAppUrl) {
        this.iosAppUrl = iosAppUrl;
    }

    public String getWindowsAppUrl() {
        return windowsAppUrl;
    }

    public void setWindowsAppUrl(String windowsAppUrl) {
        this.windowsAppUrl = windowsAppUrl;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("utm_short_code", this.shortCode);
        mapSqlParameterSource.addValue("start_date", this.startDate);
        mapSqlParameterSource.addValue("end_date", this.endDate);
        mapSqlParameterSource.addValue("utm_campaign_id", this.campaignId);
        mapSqlParameterSource.addValue("web_url", this.webUrl);
        mapSqlParameterSource.addValue("mobWeb_url", this.mobileWebUrl);
        mapSqlParameterSource.addValue("android_app_url", this.androidAppUrl);
        mapSqlParameterSource.addValue("ios_app_url", this.iosAppUrl);
        mapSqlParameterSource.addValue("windows_app_url", this.windowsAppUrl);
        mapSqlParameterSource.addValue("created_at", this.createdAt);
        mapSqlParameterSource.addValue("created_by", this.createdBy);
        mapSqlParameterSource.addValue("updated_at", this.updatedAt);
        mapSqlParameterSource.addValue("updated_by", this.updatedBy);
        return mapSqlParameterSource;
    }
}
