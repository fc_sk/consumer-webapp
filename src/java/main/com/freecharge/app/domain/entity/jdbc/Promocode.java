package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;

public class Promocode implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer promocodeId;

	private String promoTitle;

	private String promoCode;

	private String successMsg;

	private String failureMsg;

	private Date validFrom;

	private Date validUpto;

	public Integer getPromocodeId() {
		return promocodeId;
	}

	public void setPromocodeId(Integer promocodeId) {
		this.promocodeId = promocodeId;
	}

	public String getPromoTitle() {
		return promoTitle;
	}

	public void setPromoTitle(String promoTitle) {
		this.promoTitle = promoTitle;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getSuccessMsg() {
		return successMsg;
	}

	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}

	public String getFailureMsg() {
		return failureMsg;
	}

	public void setFailureMsg(String failureMsg) {
		this.failureMsg = failureMsg;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(Date validUpto) {
		this.validUpto = validUpto;
	}

}