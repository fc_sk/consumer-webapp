package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "coupon_transactions")
public class CouponTransactions implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long couponTransactionId;
	private Integer fkCouponCodeId;
	private Integer fkCouponsStoreId;
	private Timestamp sentAt;
	private Integer fkUserId;
	private String orderId;

	public CouponTransactions(Integer fkCouponCodeId, Integer fkCouponsStoreId, Timestamp sentAt, Integer fkUserId, String orderId) {
		this.fkCouponCodeId = fkCouponCodeId;
		this.sentAt = sentAt;
		this.fkCouponsStoreId = fkCouponsStoreId;
		this.fkUserId = fkUserId;
		this.orderId = orderId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "coupon_transaction_id", unique = true, nullable = false)
	public Long getCouponTransactionId() {
		return this.couponTransactionId;
	}

	public void setCouponTransactionId(Long couponTransactionId) {
		this.couponTransactionId = couponTransactionId;
	}

	@Column(name = "fk_coupon_code_id", nullable = false)
	public Integer getFkCouponCodeId() {
		return fkCouponCodeId;
	}

	public void setFkCouponCodeId(Integer fkCouponCodeId) {
		this.fkCouponCodeId = fkCouponCodeId;
	}

	@Column(name = "sent_at", nullable = false, length = 0)
	public Timestamp getSentAt() {
		return sentAt;
	}

	public void setSentAt(Timestamp sentAt) {
		this.sentAt = sentAt;
	}

	@Column(name = "fk_user_id", nullable = false, length = 45)
	public Integer getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Column(name = "order_id", nullable = false, length = 50)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "fk_coupon_store_id", nullable = false)
	public Integer getFkCouponsStoreId() {
		return this.fkCouponsStoreId;
	}

	public void setFkCouponsStoreId(Integer fkCouponsStoreId) {
		this.fkCouponsStoreId = fkCouponsStoreId;
	}

}