package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.PaymentTypeMaster;

public class PaymentTypeMasterRowMapper implements RowMapper<PaymentTypeMaster> {
@Override
public PaymentTypeMaster mapRow(ResultSet rs, int row) throws SQLException {
	PaymentTypeMaster paymentTypeMaster = new PaymentTypeMaster();
	paymentTypeMaster.setCode(rs.getString("code"));
	paymentTypeMaster.setDisplayData(rs.getString("display_data"));
	paymentTypeMaster.setName(rs.getString("name"));
	paymentTypeMaster.setPaymentTypeMasterId(rs.getInt("payment_type_master_id"));
	return paymentTypeMaster;
}
	
}
