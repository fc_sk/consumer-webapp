package com.freecharge.app.domain.entity.winpin;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CpnUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cpn_user")
public class CpnUser implements java.io.Serializable {

	// Fields

	private Long cuId;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Long cfCreatedById;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private String cuEmail;
	private Boolean cuEnable;
	private String cuFirstname;
	private String cuMobile;
	private String cuPassword;
	private String cuSurname;
	private Set<CpnJobs> cpnJobses = new HashSet<CpnJobs>(0);
	private Set<CouponBatch> couponBatchs = new HashSet<CouponBatch>(0);

	// Constructors

	/** default constructor */
	public CpnUser() {
	}

	/** full constructor */
	public CpnUser( String cfActionStatus, Date cfActiveDate,
			Boolean cfActiveFlag, Boolean cfArchiveStatus, Long cfCreatedById,
			Timestamp cfDateCreatedOn, Timestamp cfDateLastUpdatedOn,
			Long cfLastUpdatedById, String cuEmail, Boolean cuEnable,
			String cuFirstname, String cuMobile, String cuPassword,
			String cuSurname, Set<CpnJobs> cpnJobses,
			Set<CouponBatch> couponBatchs) {

		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfCreatedById = cfCreatedById;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.cuEmail = cuEmail;
		this.cuEnable = cuEnable;
		this.cuFirstname = cuFirstname;
		this.cuMobile = cuMobile;
		this.cuPassword = cuPassword;
		this.cuSurname = cuSurname;
		this.cpnJobses = cpnJobses;
		this.couponBatchs = couponBatchs;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CU_ID", unique = true, nullable = false)
	public Long getCuId() {
		return this.cuId;
	}

	public void setCuId(Long cuId) {
		this.cuId = cuId;
	}


	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_CREATED_BY_ID")
	public Long getCfCreatedById() {
		return this.cfCreatedById;
	}

	public void setCfCreatedById(Long cfCreatedById) {
		this.cfCreatedById = cfCreatedById;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "CU_EMAIL", length = 100)
	public String getCuEmail() {
		return this.cuEmail;
	}

	public void setCuEmail(String cuEmail) {
		this.cuEmail = cuEmail;
	}

	@Column(name = "CU_ENABLE")
	public Boolean getCuEnable() {
		return this.cuEnable;
	}

	public void setCuEnable(Boolean cuEnable) {
		this.cuEnable = cuEnable;
	}

	@Column(name = "CU_FIRSTNAME", length = 50)
	public String getCuFirstname() {
		return this.cuFirstname;
	}

	public void setCuFirstname(String cuFirstname) {
		this.cuFirstname = cuFirstname;
	}

	@Column(name = "CU_MOBILE", length = 50)
	public String getCuMobile() {
		return this.cuMobile;
	}

	public void setCuMobile(String cuMobile) {
		this.cuMobile = cuMobile;
	}

	@Column(name = "CU_PASSWORD", length = 100)
	public String getCuPassword() {
		return this.cuPassword;
	}

	public void setCuPassword(String cuPassword) {
		this.cuPassword = cuPassword;
	}

	@Column(name = "CU_SURNAME", length = 50)
	public String getCuSurname() {
		return this.cuSurname;
	}

	public void setCuSurname(String cuSurname) {
		this.cuSurname = cuSurname;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cpnUser")
	public Set<CpnJobs> getCpnJobses() {
		return this.cpnJobses;
	}

	public void setCpnJobses(Set<CpnJobs> cpnJobses) {
		this.cpnJobses = cpnJobses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cpnUser")
	public Set<CouponBatch> getCouponBatchs() {
		return this.couponBatchs;
	}

	public void setCouponBatchs(Set<CouponBatch> couponBatchs) {
		this.couponBatchs = couponBatchs;
	}

}