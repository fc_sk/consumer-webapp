package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;
import com.freecharge.common.easydb.UpdateQuery;

public class Users implements Serializable, DBEntity {
	private static final long serialVersionUID = 1L;
    @ColumnMapping(name="user_id")
	private Integer userId;
    @ColumnMapping(name="fk_affiliate_profile_id")
	private Integer fkAffiliateProfileId;
    @ColumnMapping(name="type")
	private Integer type;
    @ColumnMapping(name="email")
	private String email;
    @ColumnMapping(name="password")
	private String password;
    @ColumnMapping(name="mobile_no")
	private String mobileNo;
    @ColumnMapping(name="dob")
	private Date dob;
    @ColumnMapping(name="is_active")
	private Boolean isActive;
    @ColumnMapping(name="date_added")
	private Timestamp dateAdded;
    @ColumnMapping(name="last_loggedin")
	private Timestamp lastLoggedin;
    @ColumnMapping(name="is_loggedin")
	private Boolean isLoggedin;
    @ColumnMapping(name="morf")
	private String morf;
    @ColumnMapping(name="last_update")
	private Timestamp lastUpdate;
    @ColumnMapping(name="forgot_password_key")
	private String forgotPasswordKey;
    @ColumnMapping(name="forgot_password_expiry")
	private Date forgotPasswordExpiry;
    @ColumnMapping(name="fk_refferred_by_user_id")
	private Integer fkRefferredByUserId;
    
    private List<String> whereFields = new LinkedList<String>();
    
    public static final String USER_ID = "user_id";
    public static final String EMAIl = "email";

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getFkAffiliateProfileId() {
		return this.fkAffiliateProfileId;
	}

	public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Timestamp getDateAdded() {
		return this.dateAdded;
	}

	public void setDateAdded(Timestamp dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Timestamp getLastLoggedin() {
		return this.lastLoggedin;
	}

	public void setLastLoggedin(Timestamp lastLoggedin) {
		this.lastLoggedin = lastLoggedin;
	}

	public Boolean getIsLoggedin() {
		return this.isLoggedin;
	}

	public void setIsLoggedin(Boolean isLoggedin) {
		this.isLoggedin = isLoggedin;
	}

	public String getMorf() {
		return this.morf;
	}

	public void setMorf(String morf) {
		this.morf = morf;
	}

	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getForgotPasswordKey() {
		return this.forgotPasswordKey;
	}

	public void setForgotPasswordKey(String forgotPasswordKey) {
		this.forgotPasswordKey = forgotPasswordKey;
	}

	@Temporal(TemporalType.DATE)
	public Date getForgotPasswordExpiry() {
		return this.forgotPasswordExpiry;
	}

	public void setForgotPasswordExpiry(Date forgotPasswordExpiry) {
		this.forgotPasswordExpiry = forgotPasswordExpiry;
	}

	public Integer getFkRefferredByUserId() {
		return this.fkRefferredByUserId;
	}

	public void setFkRefferredByUserId(Integer fkRefferredByUserId) {
		this.fkRefferredByUserId = fkRefferredByUserId;
	}

    public void addToWhereFields(String columnName) {
        this.whereFields.add(columnName);
    }


    @Override
    public String getTableName() {
        return "users";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        UpdateQuery updateQuery = new UpdateQuery(this, this.whereFields);
        return updateQuery.getUpdateDBVO();
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", fkAffiliateProfileId=" + fkAffiliateProfileId +
                ", type=" + type +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", dob=" + dob +
                ", isActive=" + isActive +
                ", dateAdded=" + dateAdded +
                ", lastLoggedin=" + lastLoggedin +
                ", isLoggedin=" + isLoggedin +
                ", morf='" + morf + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", forgotPasswordKey='" + forgotPasswordKey + '\'' +
                ", forgotPasswordExpiry=" + forgotPasswordExpiry +
                ", fkRefferredByUserId=" + fkRefferredByUserId +
                '}';
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));

        Users user = new Users();
        user.setUserId(1481379);

        List<Users> users = easyDBWriteDAO.get(user);
        System.out.println(users);

        ctx.close();
    }

}