package com.freecharge.app.domain.entity;

import java.sql.Timestamp;

public class MetroReportDataEntity {
	private Integer userId;
	private String afcCardNumber;
	private String orderId;
	private Timestamp paymentTime;
	private Timestamp rechargeTime;
	private String rechargeStatus;
	private String paymentStatus;
	private Integer paymentType;
	private Double amount;
	public String getAfcCardNumber() {
		return afcCardNumber;
	}
	public void setAfcCardNumber(String afcCardNumber) {
		this.afcCardNumber = afcCardNumber;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Timestamp getPaymentTime() {
		return paymentTime;
	}
	public void setPaymentTime(Timestamp paymentTime) {
		this.paymentTime = paymentTime;
	}
	public Timestamp getRechargeTime() {
		return rechargeTime;
	}
	public void setRechargeTime(Timestamp rechargeTime) {
		this.rechargeTime = rechargeTime;
	}
	public String getRechargeStatus() {
		return rechargeStatus;
	}
	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Integer getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
