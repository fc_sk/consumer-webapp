package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UserRechargeContact entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user_recharge_contact")
public class UserRechargeContact implements java.io.Serializable {

	// Fields

	private Integer userRechargeContactId;
	private Integer fkUsersId;
	private String serviceNumber;
	private Integer fkProductMasterId;
	private Integer fkCircleMasterId;
	private Integer fkOperatorMasterId;

	private String nickAlias;

	private Double lastRechargeAmount;
	private Boolean isActive;
	private Timestamp createdOn;
	private Timestamp updatedOn;

	// Constructors

	/** default constructor */
	public UserRechargeContact() {
	}

	/** minimal constructor */
	public UserRechargeContact(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/** full constructor */
	public UserRechargeContact(Integer fkUsersId, String serviceNumber,
			Integer fkProductMasterId, Integer fkCircleMasterId,
			Integer fkOperatorMasterId, String firstName, String nickAlias,
			String lastName, Double lastRechargeAmount, Boolean isActive,
			Timestamp createdOn, Timestamp updatedOn) {
		this.fkUsersId = fkUsersId;
		this.serviceNumber = serviceNumber;
		this.fkProductMasterId = fkProductMasterId;
		this.fkCircleMasterId = fkCircleMasterId;
		this.fkOperatorMasterId = fkOperatorMasterId;

		this.nickAlias = nickAlias;

		this.lastRechargeAmount = lastRechargeAmount;
		this.isActive = isActive;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_recharge_contact_id", unique = true, nullable = false)
	public Integer getUserRechargeContactId() {
		return this.userRechargeContactId;
	}

	public void setUserRechargeContactId(Integer userRechargeContactId) {
		this.userRechargeContactId = userRechargeContactId;
	}

	@Column(name = "fk_users_id")
	public Integer getFkUsersId() {
		return this.fkUsersId;
	}

	public void setFkUsersId(Integer fkUsersId) {
		this.fkUsersId = fkUsersId;
	}

	@Column(name = "service_number", length = 128)
	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	@Column(name = "fk_product_master_id")
	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	@Column(name = "fk_circle_master_id")
	public Integer getFkCircleMasterId() {
		return this.fkCircleMasterId;
	}

	public void setFkCircleMasterId(Integer fkCircleMasterId) {
		this.fkCircleMasterId = fkCircleMasterId;
	}

	@Column(name = "fk_operator_master_id")
	public Integer getFkOperatorMasterId() {
		return this.fkOperatorMasterId;
	}

	public void setFkOperatorMasterId(Integer fkOperatorMasterId) {
		this.fkOperatorMasterId = fkOperatorMasterId;
	}



	@Column(name = "nick_alias")
	public String getNickAlias() {
		return this.nickAlias;
	}

	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}


	@Column(name = "last_recharge_amount", precision = 10)
	public Double getLastRechargeAmount() {
		return this.lastRechargeAmount;
	}

	public void setLastRechargeAmount(Double lastRechargeAmount) {
		this.lastRechargeAmount = lastRechargeAmount;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "updated_on", nullable = false, length = 0)
	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}