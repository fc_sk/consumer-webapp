package com.freecharge.app.domain.entity.jdbc;

public class CartItemsMetadata {
    private Integer planId;
    private String planCategory;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanCategory() {
        return planCategory;
    }

    public void setPlanCategory(String planCategory) {
        this.planCategory = planCategory;
    }

    @Override
    public String toString() {
        return "CartItemsMetadata{" +
                "planId=" + planId +
                ", planCategory='" + planCategory + '\'' +
                '}';
    }
}
