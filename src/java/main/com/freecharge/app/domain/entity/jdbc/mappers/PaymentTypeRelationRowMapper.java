package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.PaymentTypeRelation;

public class PaymentTypeRelationRowMapper implements RowMapper<PaymentTypeRelation> {
@Override
public PaymentTypeRelation mapRow(ResultSet rs, int row) throws SQLException {
	PaymentTypeRelation paymentTypeRelation = new PaymentTypeRelation();
	paymentTypeRelation.setFkAffliateUniqueId(rs.getString("fk_affliate_unique_id"));
	paymentTypeRelation.setFkPaymentTypeId(rs.getInt("fk_payment_type_id"));
	paymentTypeRelation.setFkProductMasterId(rs.getInt("fk_product_master_id"));
	paymentTypeRelation.setIsActive(rs.getBoolean("is_active"));
	paymentTypeRelation.setOrderBy(rs.getInt("order_by"));
	paymentTypeRelation.setPaymentTypeRelationId(rs.getInt("payment_type_relation_id"));
	
	return paymentTypeRelation;
}
}
