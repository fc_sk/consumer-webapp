package com.freecharge.app.domain.entity.jdbc;

public class ZoneMaster implements java.io.Serializable {
	
	private Integer zoneMasterId;
	private String zoneName;
	private Integer fkCountryMasterId;
	
	public Integer getZoneMasterId() {
		return zoneMasterId;
	}
	public void setZoneMasterId(Integer zoneMasterId) {
		this.zoneMasterId = zoneMasterId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public Integer getFkCountryMasterId() {
		return fkCountryMasterId;
	}
	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}
	@Override
	public String toString() {
		return "ZoneMaster [zoneMasterId=" + zoneMasterId + ", zoneName="
				+ zoneName + ", fkCountryMasterId=" + fkCountryMasterId + "]";
	}
	
	
}
