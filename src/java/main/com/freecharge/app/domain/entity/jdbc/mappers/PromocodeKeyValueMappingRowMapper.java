package com.freecharge.app.domain.entity.jdbc.mappers;

import com.freecharge.promo.entity.PromocodeKeyValueMapping;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PromocodeKeyValueMappingRowMapper implements RowMapper {
    @Override
    public PromocodeKeyValueMapping mapRow(ResultSet resultSet, int i) throws SQLException {
        PromocodeKeyValueMapping promocodeKeyValueMapping = new PromocodeKeyValueMapping();
        promocodeKeyValueMapping.setPromocodeId(resultSet.getLong("id"));
        promocodeKeyValueMapping.setPromocodeId(resultSet.getLong("promocode_id"));
        promocodeKeyValueMapping.setKeyValue(resultSet.getString("key_value"));
        promocodeKeyValueMapping.setMappingKey(resultSet.getString("mapping_key"));
        return promocodeKeyValueMapping;
    }
}
