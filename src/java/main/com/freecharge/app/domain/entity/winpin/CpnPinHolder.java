package com.freecharge.app.domain.entity.winpin;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * CpnPinHolder entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cpn_pin_holder", uniqueConstraints = @UniqueConstraint(columnNames = "CPN_PIN"))
public class CpnPinHolder implements java.io.Serializable {

	// Fields

	private Long cphId;
	private CpnJobs cpnJobs;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Long cfCreatedById;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private String cphCpnSrNo;
	private String cpnPin;

	// Constructors

	/** default constructor */
	public CpnPinHolder() {
	}

	/** full constructor */
	public CpnPinHolder(CpnJobs cpnJobs, String cfActionStatus,
			Date cfActiveDate, Boolean cfActiveFlag, Boolean cfArchiveStatus,
			Long cfCreatedById, Timestamp cfDateCreatedOn,
			Timestamp cfDateLastUpdatedOn, Long cfLastUpdatedById,
			String cphCpnSrNo, String cpnPin) {
		this.cpnJobs = cpnJobs;
		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfCreatedById = cfCreatedById;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.cphCpnSrNo = cphCpnSrNo;
		this.cpnPin = cpnPin;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CPH_ID", unique = true, nullable = false)
	public Long getCphId() {
		return this.cphId;
	}

	public void setCphId(Long cphId) {
		this.cphId = cphId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CJ_ID")
	public CpnJobs getCpnJobs() {
		return this.cpnJobs;
	}

	public void setCpnJobs(CpnJobs cpnJobs) {
		this.cpnJobs = cpnJobs;
	}

	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_CREATED_BY_ID")
	public Long getCfCreatedById() {
		return this.cfCreatedById;
	}

	public void setCfCreatedById(Long cfCreatedById) {
		this.cfCreatedById = cfCreatedById;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "CPH_CPN_SR_NO", length = 20)
	public String getCphCpnSrNo() {
		return this.cphCpnSrNo;
	}

	public void setCphCpnSrNo(String cphCpnSrNo) {
		this.cphCpnSrNo = cphCpnSrNo;
	}

	@Column(name = "CPN_PIN", unique = true, length = 50)
	public String getCpnPin() {
		return this.cpnPin;
	}

	public void setCpnPin(String cpnPin) {
		this.cpnPin = cpnPin;
	}

}