package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.migration.SiteUsers;

public class SiteUsersMapper implements RowMapper<SiteUsers>{
	@Override
	public SiteUsers mapRow(ResultSet rs, int row) throws SQLException {
		SiteUsers siteUsers = new SiteUsers();
		
		siteUsers.setId(rs.getInt("id"));
		siteUsers.setUserName(rs.getString("user_name"));
		siteUsers.setPassword(rs.getString("password"));
		siteUsers.setEmail(rs.getString("email"));
		siteUsers.setCountryId(rs.getInt("country_id"));
		siteUsers.setCity(rs.getString("city"));
		siteUsers.setAddress1(rs.getString("address1"));
		siteUsers.setLandmark(rs.getString("landmark"));
		siteUsers.setPostalCode(rs.getString("postal_code"));
		siteUsers.setPhoneNo(rs.getString("phone_no"));
		siteUsers.setStatus(rs.getString("status"));
		siteUsers.setDateAdded(rs.getTimestamp("date_added"));
		siteUsers.setLoginStatus(rs.getString("login_status"));
		siteUsers.setMorf(rs.getString("morf"));
		siteUsers.setDob(rs.getString("dob"));
		siteUsers.setLastUpdate(rs.getTimestamp("last_update"));
		siteUsers.setEntity(rs.getString("entity"));
		siteUsers.setStreet1(rs.getString("street1"));
		siteUsers.setStreet2(rs.getString("street2"));
		
		return siteUsers;
	}
}
