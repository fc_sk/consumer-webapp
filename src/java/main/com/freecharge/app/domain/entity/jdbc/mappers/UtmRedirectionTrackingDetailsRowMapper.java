package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UtmRedirectionTrackingDetails;

public class UtmRedirectionTrackingDetailsRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        UtmRedirectionTrackingDetails utmRedirectionTrackingDetails = new UtmRedirectionTrackingDetails();
        utmRedirectionTrackingDetails.setUserId(resultSet.getInt("user_id"));
        utmRedirectionTrackingDetails.setShortCode(resultSet.getString("utm_short_code"));
        utmRedirectionTrackingDetails.setChannelId(resultSet.getInt("channel_id"));
        utmRedirectionTrackingDetails.setRedirectedUrl(resultSet.getString("redirected_url"));
        utmRedirectionTrackingDetails.setCreatedAt(resultSet.getString("created_at"));
        return utmRedirectionTrackingDetails;
    }
}
