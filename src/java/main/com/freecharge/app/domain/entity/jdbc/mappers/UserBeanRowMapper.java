package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.UserBean;

public class UserBeanRowMapper implements RowMapper<UserBean> {

    @Override
    public UserBean mapRow(ResultSet rs, int row) throws SQLException {
        UserBean userBean = new UserBean();
        
        userBean.setUserId(rs.getInt("user_id") == 0? null : rs.getInt("user_id"));
        userBean.setMobileNumber(rs.getString("mobile_no"));
        userBean.setEmail(rs.getString("email"));
        
        return userBean;
    }

}
