package com.freecharge.app.domain.entity.jdbc;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class UtmTracker {

    private int campaignId;
    private String fcMaster;
    private String campaignName;
    private String medium;
    private String source;
    private String keyword;
    private String url;
    private String content;
    private String forwardUrl;
    private String created_by;
    private String updated_by;

    public String getFcMaster() {
        return fcMaster;
    }

    public void setFcMaster(String fcMaster) {
        this.fcMaster = fcMaster;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("fc_master", this.fcMaster);
        mapSqlParameterSource.addValue("utm_campaign_name", this.campaignName);
        mapSqlParameterSource.addValue("utm_medium", this.medium);
        mapSqlParameterSource.addValue("utm_source", this.source);
        mapSqlParameterSource.addValue("utm_keyword", this.keyword);
        mapSqlParameterSource.addValue("utm_url", this.url);
        mapSqlParameterSource.addValue("utm_content", this.content);
        mapSqlParameterSource.addValue("forward_url", this.forwardUrl);
        mapSqlParameterSource.addValue("created_by", this.created_by);
        mapSqlParameterSource.addValue("updated_by", this.updated_by);
        return mapSqlParameterSource;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }
}
