package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PaymentRequest;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 14/9/12
 * Time: 1:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentRequestMapper implements RowMapper<PaymentRequest> {

    @Override
    public PaymentRequest mapRow(ResultSet resultSet, int i) throws SQLException {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setPaymentRqId(resultSet.getInt("payment_rq_id"));
        paymentRequest.setOrderId(resultSet.getString("order_id"));
        paymentRequest.setRequestToPp(resultSet.getString("request_to_pp"));
        paymentRequest.setAmount(resultSet.getDouble("amount"));
        paymentRequest.setPaymentMode(resultSet.getString("payment_mode"));
        paymentRequest.setPaymentGateway(resultSet.getString("payment_gateway"));
        paymentRequest.setPaymentType(resultSet.getString("payment_type"));
        paymentRequest.setMtxnId(resultSet.getString("mtxn_id"));
        paymentRequest.setRequestToGateway(resultSet.getString("request_to_gateway"));
        paymentRequest.setAffiliateCode(resultSet.getString("affiliate_code"));
        paymentRequest.setCreatedOn(resultSet.getTimestamp("created_on"));
        paymentRequest.setFkBbanksGrpMasterId(resultSet.getString("fk_banks_grp_master_id"));
        return paymentRequest;
    }
}
