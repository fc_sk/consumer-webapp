package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * StateMaster entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "state_master")
public class StateMaster implements java.io.Serializable {

	// Fields

	private Integer stateMasterId;
	private String stateName;
	private Boolean isActive;
	private Integer fkZoneMasterId;
	private Integer fkCountryMasterId;

	// Constructors

	/** default constructor */
	public StateMaster() {
	}

	/** full constructor */
	public StateMaster(String stateName, Boolean isActive,
			Integer fkZoneMasterId, Integer fkCountryMasterId) {
		this.stateName = stateName;
		this.isActive = isActive;
		this.fkZoneMasterId = fkZoneMasterId;
		this.fkCountryMasterId = fkCountryMasterId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "state_master_id", unique = true, nullable = false)
	public Integer getStateMasterId() {
		return this.stateMasterId;
	}

	public void setStateMasterId(Integer stateMasterId) {
		this.stateMasterId = stateMasterId;
	}

	@Column(name = "state_name", nullable = false, length = 100)
	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "fk_zone_master_id", nullable = false)
	public Integer getFkZoneMasterId() {
		return this.fkZoneMasterId;
	}

	public void setFkZoneMasterId(Integer fkZoneMasterId) {
		this.fkZoneMasterId = fkZoneMasterId;
	}

	@Column(name = "fk_country_master_id", nullable = false)
	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

}