package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.Users;

public class UsersMapper implements RowMapper<Users> {
	
	@Override
	public Users mapRow(ResultSet rs, int row) throws SQLException {
		Users users = new Users();
		
		users.setUserId(rs.getInt("user_id"));
		users.setDateAdded(rs.getTimestamp("date_added"));
		users.setDob(rs.getDate("dob"));
		users.setEmail(rs.getString("email"));
		users.setFkAffiliateProfileId(rs.getInt("fk_affiliate_profile_id"));
		users.setFkRefferredByUserId(rs.getInt("fk_refferred_by_user_id"));
		users.setForgotPasswordExpiry(rs.getDate("forgot_password_expiry"));
		users.setForgotPasswordKey(rs.getString("forgot_password_key"));
		users.setIsActive(rs.getBoolean("is_active"));
		users.setIsLoggedin(rs.getBoolean("is_loggedin"));
		users.setLastUpdate(rs.getTimestamp("last_update"));
		users.setLastLoggedin(rs.getTimestamp("last_loggedin"));
		users.setMobileNo(rs.getString("mobile_no"));
		users.setMorf(rs.getString("morf"));
		users.setPassword(rs.getString("password"));
		users.setType(rs.getInt("type"));
		
		return users;
	}

}
