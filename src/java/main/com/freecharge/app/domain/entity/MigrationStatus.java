package com.freecharge.app.domain.entity;

public class MigrationStatus {
	private String migrationStatus;
    private String fcWalletId;

    public String getMigrationStatus() {
        return migrationStatus;
    }

    public void setMigrationStatus(String migrationStatus) {
        this.migrationStatus = migrationStatus;
    }

    public String getFcWalletId() {
        return fcWalletId;
    }

    public void setFcWalletId(String fcWalletId) {
        this.fcWalletId = fcWalletId;
    }

    @Override
    public String toString() {
        return "MigrationStatus [migrationStatus=" + migrationStatus + ", fcWalletId=" + fcWalletId + "]";
    }
}
