package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reward_recharge")
public class RewardRecharge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "reward_recharge_id")
    private int               rechargeRewardId;
    @Column(name = "service_number")
    private String            serviceNumber;
    @Column(name = "fk_operator_id")
    private int               operatorId;
    @Column(name = "fk_circle_id")
    private int               circleId;
    @Column(name = "amount")
    private double            amount;
    @Column(name = "source_order_id")
    private String            sourceOrderId;
    @Column(name = "order_id")
    private String            orderId;
    @Column(name = "user_id")
    private int               userId;
    @Column(name = "created_on")
    private Timestamp         createdOn;
    @Column(name = "modified_on")
    private Timestamp         modifiedOn;

    public int getRechargeRewardId() {
        return rechargeRewardId;
    }

    public void setRechargeRewardId(int rechargeRewardId) {
        this.rechargeRewardId = rechargeRewardId;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getSourceOrderId() {
        return sourceOrderId;
    }

    public void setSourceOrderId(String sourceOrderId) {
        this.sourceOrderId = sourceOrderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Timestamp modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

}
