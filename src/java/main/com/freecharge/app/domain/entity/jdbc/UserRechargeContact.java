package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;

public class UserRechargeContact implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer userRechargeContactId;
	private Integer fkUsersId;
	private String serviceNumber;
	private Integer fkProductMasterId;
	private Integer fkCircleMasterId;
	private Integer fkOperatorMasterId;
	private String nickAlias;
	private Double lastRechargeAmount;
	private Boolean isActive;
	private Timestamp createdOn;
	private Timestamp updatedOn;

	public Integer getUserRechargeContactId() {
		return this.userRechargeContactId;
	}

	public void setUserRechargeContactId(Integer userRechargeContactId) {
		this.userRechargeContactId = userRechargeContactId;
	}

	public Integer getFkUsersId() {
		return this.fkUsersId;
	}

	public void setFkUsersId(Integer fkUsersId) {
		this.fkUsersId = fkUsersId;
	}

	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	public Integer getFkCircleMasterId() {
		return this.fkCircleMasterId;
	}

	public void setFkCircleMasterId(Integer fkCircleMasterId) {
		this.fkCircleMasterId = fkCircleMasterId;
	}

	public Integer getFkOperatorMasterId() {
		return this.fkOperatorMasterId;
	}

	public void setFkOperatorMasterId(Integer fkOperatorMasterId) {
		this.fkOperatorMasterId = fkOperatorMasterId;
	}

	public String getNickAlias() {
		return this.nickAlias;
	}

	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}

	public Double getLastRechargeAmount() {
		return this.lastRechargeAmount;
	}

	public void setLastRechargeAmount(Double lastRechargeAmount) {
		this.lastRechargeAmount = lastRechargeAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Override
	public boolean equals(Object obj) {		
		return this.getUserRechargeContactId().equals(((UserRechargeContact)obj).getUserRechargeContactId());
	}
	
	@Override
	public int hashCode() {
		return this.getUserRechargeContactId().hashCode();
	}

}