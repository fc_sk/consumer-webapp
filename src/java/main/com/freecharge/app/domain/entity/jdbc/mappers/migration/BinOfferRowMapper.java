package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.BinOffer;

public class BinOfferRowMapper implements RowMapper<BinOffer> {
	@Override
	public BinOffer mapRow(ResultSet rs, int row) throws SQLException {
		
		BinOffer binOffer = new BinOffer();
		binOffer.setBin(rs.getString("bin"));
		binOffer.setDescription(rs.getString("description"));
		binOffer.setShortDescription(rs.getString("short_description"));
		binOffer.setConditions(rs.getString("conditions"));
		binOffer.setDiscount(rs.getDouble("discount"));
		binOffer.setBank(rs.getString("bank"));
		binOffer.setId(rs.getInt("id"));
		binOffer.setImageUrl(rs.getString("image_url"));
		binOffer.setIsActive(rs.getInt("is_active"));
		binOffer.setMinRechargeAmount(rs.getDouble("min_recharge_amount"));
		binOffer.setDatamap(rs.getString("datamap"));
		binOffer.setCouponFactor(rs.getInt("coupon_factor"));
		binOffer.setName(rs.getString("name"));
		binOffer.setOfferKey(rs.getString("offer_key"));
		binOffer.setOfferType(rs.getString("offer_type"));
		binOffer.setDiscountType(rs.getString("discount_type"));
		
		Calendar cal1 = Calendar.getInstance();
		cal1.setTimeInMillis(rs.getTimestamp("created_at").getTime());
		binOffer.setCreatedAt(cal1);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTimeInMillis(rs.getTimestamp("updated_at").getTime());
		binOffer.setUpdatedAt(cal2);
		
		Calendar cal3 = Calendar.getInstance();
		cal3.setTimeInMillis(rs.getTimestamp("valid_from").getTime());
		binOffer.setValidFrom(cal3);
		
		Calendar cal4 = Calendar.getInstance();
		cal4.setTimeInMillis(rs.getTimestamp("valid_upto").getTime());
		binOffer.setValidUpto(cal4);
				
		return binOffer;
	}

}
