package com.freecharge.app.domain.entity.jdbc;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CityMaster entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "city_master")
public class CityMaster implements java.io.Serializable {

	// Fields

	private Integer cityMasterId;
	private String cityName;
	private Boolean status;
	private Integer fkStateMasterId;
	private Integer fkCountryMasterId;
	private Integer fkZoneMasterId;

	// Constructors

	/** default constructor */
	public CityMaster() {
	}

    public CityMaster(Integer cityMasterId) {
        this.cityMasterId = cityMasterId;
    }

	/** full constructor */
	public CityMaster(String cityName, Boolean status, Integer fkStateMasterId,
			Integer fkCountryMasterId, Integer fkZoneMasterId) {
		this.cityName = cityName;
		this.status = status;
		this.fkStateMasterId = fkStateMasterId;
		this.fkCountryMasterId = fkCountryMasterId;
		this.fkZoneMasterId = fkZoneMasterId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "city_master_id", unique = true, nullable = false)
	public Integer getCityMasterId() {
		return this.cityMasterId;
	}

	public void setCityMasterId(Integer cityMasterId) {
		this.cityMasterId = cityMasterId;
	}

	@Column(name = "city_name", nullable = false, length = 100)
	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Column(name = "status", nullable = false)
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name = "fk_state_master_id", nullable = false)
	public Integer getFkStateMasterId() {
		return this.fkStateMasterId;
	}

	public void setFkStateMasterId(Integer fkStateMasterId) {
		this.fkStateMasterId = fkStateMasterId;
	}

	@Column(name = "fk_country_master_id", nullable = false)
	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

	@Column(name = "fk_zone_master_id", nullable = false)
	public Integer getFkZoneMasterId() {
		return this.fkZoneMasterId;
	}

    public void setFkZoneMasterId(Integer fkZoneMasterId) {
		this.fkZoneMasterId = fkZoneMasterId;
	}

    
    @Override
    public boolean equals(Object o) {
        if (cityMasterId.equals(((CityMaster)o).cityMasterId)) return true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cityMasterId != null ? cityMasterId.hashCode() : 0;
        return result;
    }

	@Override
	public String toString() {
		return "CityMaster [cityMasterId=" + cityMasterId + ", cityName=" + cityName + ", status=" + status
				+ ", fkStateMasterId=" + fkStateMasterId + ", fkCountryMasterId=" + fkCountryMasterId
				+ ", fkZoneMasterId=" + fkZoneMasterId + "]";
	}




}