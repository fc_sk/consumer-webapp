package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;

public class PromocodeCampaignRowMapper implements RowMapper<PromocodeCampaign> {
	@Override
	public PromocodeCampaign mapRow(ResultSet rs, int row) throws SQLException {
	    PromocodeCampaign promocode = new PromocodeCampaign();
		
		promocode.setCampaignId(rs.getInt("promocode_campaign_id"));
		promocode.setFkPromocodeId(rs.getInt("fk_promocode_id"));
		promocode.setPhoneNo(rs.getString("phone_no"));
		promocode.setOrderId(rs.getString("orderid"));
		promocode.setCreatedDate(rs.getTimestamp("created_date"));
		promocode.setStatus(rs.getString("status"));
		promocode.setUserId(rs.getInt("fk_user_id"));
		
		return promocode;
	}

}
