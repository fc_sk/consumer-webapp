package com.freecharge.app.domain.entity.jdbc.mappers;

import com.freecharge.promo.entity.Promocode;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PromocodeRowMapper implements RowMapper {

    @Override
    public Promocode mapRow(ResultSet resultSet, int row) throws SQLException {
        Promocode promocode = new Promocode();
        promocode.setPromocodeId(resultSet.getInt("promocode_id"));
        promocode.setPromocode(resultSet.getString("promocode"));
        promocode.setPromoType(resultSet.getString("promo_type"));
        promocode.setPromoSponsorship(resultSet.getString("promo_sponsorship"));
        promocode.setCampaignId(resultSet.getLong("campaign_id"));
        promocode.setCurrentStatus(resultSet.getString("current_status"));
        promocode.setPrefix(resultSet.getString("prefix"));
        promocode.setCodeLength(resultSet.getInt("code_length"));
        promocode.setDiscountType(resultSet.getString("discount_type"));
        promocode.setDiscountValue(resultSet.getFloat("discount_value"));
        promocode.setMaxDiscountValue(resultSet.getFloat("max_discount_value"));
        promocode.setMinRechargeValue(resultSet.getFloat("min_recharge_value"));
        promocode.setApplyConditionId(resultSet.getLong("apply_condition_id"));
        promocode.setPaymentConditionId(resultSet.getLong("payment_condition_id"));
        promocode.setRedeemConditionId(resultSet.getLong("redeem_condition_id"));
        promocode.setSuccessMessageApply(resultSet.getString("success_message_apply"));
        promocode.setSuccessMessageRecharge(resultSet.getString("success_message_recharge"));
        promocode.setDescription(resultSet.getString("description"));
        promocode.setHasUniqueMapping(resultSet.getBoolean("has_unique_mapping"));
        promocode.setValidFrom(resultSet.getTimestamp("valid_from"));
        promocode.setValidUpto(resultSet.getTimestamp("valid_upto"));
        promocode.setCreatedAt(resultSet.getTimestamp("created_at"));
        promocode.setCreatedBy(resultSet.getString("created_by"));
        promocode.setUpdatedAt(resultSet.getTimestamp("updated_at"));
        promocode.setUpdatedBy(resultSet.getString("created_by"));
        promocode.setMaxRedemptions(resultSet.getInt("max_redemptions"));
        return promocode;
    }
}
