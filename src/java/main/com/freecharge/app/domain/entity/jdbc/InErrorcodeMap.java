package com.freecharge.app.domain.entity.jdbc;



public class InErrorcodeMap implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String aggrName;
	private String aggrRespCode;
	private String inRespCode;
	private String inErrorMsg;
	private Boolean isRetryable;
	private Boolean isDisplayEnabled;

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAggrName() {
		return this.aggrName;
	}

	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getAggrRespCode() {
		return this.aggrRespCode;
	}

	public void setAggrRespCode(String aggrRespCode) {
		this.aggrRespCode = aggrRespCode;
	}

	public String getInRespCode() {
		return this.inRespCode;
	}

	public void setInRespCode(String inRespCode) {
		this.inRespCode = inRespCode;
	}

	public String getInErrorMsg() {
		return this.inErrorMsg;
	}

	public void setInErrorMsg(String inErrorMsg) {
		this.inErrorMsg = inErrorMsg;
	}

	public Boolean getIsRetryable() {
		return this.isRetryable;
	}

	public void setIsRetryable(Boolean isRetryable) {
		this.isRetryable = isRetryable;
	}

	@Override
	public boolean equals(Object obj) {
		return this.getId().equals(((InErrorcodeMap)obj).getId());
	}
	
	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}

    public Boolean getIsDisplayEnabled() {
        return isDisplayEnabled;
    }

    public void setIsDisplayEnabled(Boolean isDisplayEnabled) {
        this.isDisplayEnabled = isDisplayEnabled;
    }
}