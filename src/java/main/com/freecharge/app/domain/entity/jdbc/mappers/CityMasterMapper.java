package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.CityMaster;


public class CityMasterMapper implements RowMapper<CityMaster> {
	@Override
	public CityMaster mapRow(ResultSet rs, int row) throws SQLException {
		CityMaster cityMaster = new CityMaster();
		
		cityMaster.setCityMasterId(rs.getInt("city_master_id"));
		cityMaster.setCityName(rs.getString("city_name"));
		cityMaster.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		cityMaster.setFkStateMasterId(rs.getInt("fk_state_master_id"));
		cityMaster.setFkZoneMasterId(rs.getInt("fk_zone_master_id"));
		cityMaster.setStatus(rs.getBoolean("status"));
		
		return cityMaster;
	}

}
