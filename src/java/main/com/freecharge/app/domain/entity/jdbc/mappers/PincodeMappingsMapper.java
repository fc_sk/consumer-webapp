package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.PincodeMapping;

public class PincodeMappingsMapper implements RowMapper<PincodeMapping> {
	
	@Override
	public PincodeMapping mapRow(ResultSet rs, int row) throws SQLException {
		PincodeMapping pincodeMapper = new PincodeMapping();
		pincodeMapper.setCity(rs.getString("city"));
		pincodeMapper.setState(rs.getString("state"));
		pincodeMapper.setPincode(rs.getInt("pincode"));
		return pincodeMapper;
	}

}
