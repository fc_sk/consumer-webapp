package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PaymentResponse;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 14/9/12
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentResponseRowMapper implements RowMapper<PaymentResponse> {
    @Override
    public PaymentResponse mapRow(ResultSet resultSet, int i) throws SQLException {
        PaymentResponse paymentResponse = new PaymentResponse();

        paymentResponse.setPaymentRsId(resultSet.getInt("payment_rs_id"));
        paymentResponse.setIsSuccessful(resultSet.getBoolean("is_successful"));
        paymentResponse.setPgTransactionId(resultSet.getString("pg_transaction_id"));
        paymentResponse.setOrderId(resultSet.getString("order_id"));
        paymentResponse.setMtxnId(resultSet.getString("mtxn_id"));
        paymentResponse.setResponseData(resultSet.getString("response_data"));
        paymentResponse.setAmount(resultSet.getDouble("amount"));
        paymentResponse.setPaymentGateway(resultSet.getString("payment_gateway"));
        paymentResponse.setPgMessage(resultSet.getString("pg_message"));
        paymentResponse.setRrnNo(resultSet.getString("rrn_no"));
        paymentResponse.setAuthCodeNo(resultSet.getString("auth_code_no"));
        paymentResponse.setAvsCode(resultSet.getString("avs_code"));
        paymentResponse.setEci(resultSet.getString("eci"));
        paymentResponse.setCscResponseCode(resultSet.getString("csc_response_code"));
        paymentResponse.setResponseFromPp(resultSet.getString("response_from_pp"));
        paymentResponse.setCreatedOn(resultSet.getTimestamp("created_on"));
        return paymentResponse;
    }
}
