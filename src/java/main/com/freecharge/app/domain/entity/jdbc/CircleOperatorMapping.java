package com.freecharge.app.domain.entity.jdbc;

import com.freecharge.common.framework.basedo.AbstractDO;

public class CircleOperatorMapping extends AbstractDO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer circleOperatorMappingId;
	private Integer fkOperatorMasterId;
	private Integer fkCircleMasterId;
	private String aggrPriority;

	public CircleOperatorMapping() {
		// TODO Auto-generated constructor stub
	}

	public CircleOperatorMapping(Integer operatorMasterId, Integer circleMasterId) {
		this.fkOperatorMasterId = operatorMasterId;
		this.fkCircleMasterId = circleMasterId;
	}

	public Integer getCircleOperatorMappingId() {
		return this.circleOperatorMappingId;
	}

	public void setCircleOperatorMappingId(Integer circleOperatorMappingId) {
		this.circleOperatorMappingId = circleOperatorMappingId;
	}

	public Integer getFkOperatorMasterId() {
		return fkOperatorMasterId;
	}

	public void setFkOperatorMasterId(Integer fkOperatorMasterId) {
		this.fkOperatorMasterId = fkOperatorMasterId;
	}

	public Integer getFkCircleMasterId() {
		return fkCircleMasterId;
	}

	public void setFkCircleMasterId(Integer fkCircleMasterId) {
		this.fkCircleMasterId = fkCircleMasterId;
	}

	public String getAggrPriority() {
        return aggrPriority;
    }

    public void setAggrPriority(String aggrPriority) {
        this.aggrPriority = aggrPriority;
    }

    @Override
	public boolean equals(Object obj) {
		return this.getFkOperatorMasterId().equals(((CircleOperatorMapping) obj).getFkOperatorMasterId()) && this.getFkCircleMasterId().equals(((CircleOperatorMapping) obj).getFkCircleMasterId());
	}

	@Override
	public int hashCode() {
		return this.getCircleOperatorMappingId().hashCode();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CircleOperatorMapping [circleOperatorMappingId=").append(circleOperatorMappingId)
				.append(", fkOperatorMasterId=").append(fkOperatorMasterId).append(", fkCircleMasterId=")
				.append(fkCircleMasterId).append(", aggrPriority=").append(aggrPriority).append("]");
		return builder.toString();
	}
	
	
}