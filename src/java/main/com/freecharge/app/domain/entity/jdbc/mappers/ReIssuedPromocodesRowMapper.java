package com.freecharge.app.domain.entity.jdbc.mappers;

import com.freecharge.freefund.dos.entity.ReIssuedPromocode;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


public class ReIssuedPromocodesRowMapper implements RowMapper {

    @Override
    public ReIssuedPromocode mapRow(ResultSet resultSet, int i) throws SQLException {
        ReIssuedPromocode reIssuedPromocode = new ReIssuedPromocode();
        reIssuedPromocode.setOrderId(resultSet.getString("order_id"));
        reIssuedPromocode.setId(resultSet.getInt("id"));
        reIssuedPromocode.setParentFreefundClassId(resultSet.getInt("parent_freefund_class_id"));
        reIssuedPromocode.setParentPromocodeId(resultSet.getLong("parent_promocode_id"));
        reIssuedPromocode.setReIssuedPromocodeId(resultSet.getLong("reissued_promocode_id"));
        return reIssuedPromocode;
    }
}
