package com.freecharge.app.domain.entity.jdbc;



public class StateMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer stateMasterId;
	private String stateName;
	private Boolean isActive;
	private Integer fkZoneMasterId;
	private Integer fkCountryMasterId;

	public Integer getStateMasterId() {
		return this.stateMasterId;
	}

	public void setStateMasterId(Integer stateMasterId) {
		this.stateMasterId = stateMasterId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getFkZoneMasterId() {
		return this.fkZoneMasterId;
	}

	public void setFkZoneMasterId(Integer fkZoneMasterId) {
		this.fkZoneMasterId = fkZoneMasterId;
	}

	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

}