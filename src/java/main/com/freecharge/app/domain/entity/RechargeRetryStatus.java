package com.freecharge.app.domain.entity;

import java.sql.Timestamp;

public class RechargeRetryStatus {
	private long id;
	private String orderId;
	private Integer retryCountRemaining;
	private boolean isRetryComplete;
	private Timestamp createdOn;
	private Timestamp updatedOn;
    private String errorCode;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Integer getRetryCountRemaining() {
		return retryCountRemaining;
	}
	public void setRetryCountRemaining(Integer retryCountRemaining) {
		this.retryCountRemaining = retryCountRemaining;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public boolean isRetryComplete() {
		return isRetryComplete;
	}
	public void setRetryComplete(boolean isRetryComplete) {
		this.isRetryComplete = isRetryComplete;
	}
	
	public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
