package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * WalletTransaction entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "wallet_transaction")
public class WalletTransaction implements java.io.Serializable {

	// Fields

	private Integer walletTransactionId;
	private Integer fkWalletAccountId;
	private String transactionId;
	private String orderId;
	private String sessionId;
	private String transactionType;
	private Double amount;
	private Double currentBalance;
	private String amountSource;
	private Boolean status;
	private Integer fkPaymentTransacitonId;
	private Integer fkParentPaymentTransactionId;
	private Timestamp createdOn;

	// Constructors

	/** default constructor */
	public WalletTransaction() {
	}

	/** full constructor */
	public WalletTransaction(Integer fkWalletAccountId, String transactionId,
			String orderId, String sessionId, String transactionType,
			Double amount, Double currentBalance, String amountSource,
			Boolean status, Integer fkPaymentTransacitonId,
			Integer fkParentPaymentTransactionId, Timestamp createdOn) {
		this.fkWalletAccountId = fkWalletAccountId;
		this.transactionId = transactionId;
		this.orderId = orderId;
		this.sessionId = sessionId;
		this.transactionType = transactionType;
		this.amount = amount;
		this.currentBalance = currentBalance;
		this.amountSource = amountSource;
		this.status = status;
		this.fkPaymentTransacitonId = fkPaymentTransacitonId;
		this.fkParentPaymentTransactionId = fkParentPaymentTransactionId;
		this.createdOn = createdOn;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "wallet_transaction_id", unique = true, nullable = false)
	public Integer getWalletTransactionId() {
		return this.walletTransactionId;
	}

	public void setWalletTransactionId(Integer walletTransactionId) {
		this.walletTransactionId = walletTransactionId;
	}

	@Column(name = "fk_wallet_account_id")
	public Integer getFkWalletAccountId() {
		return this.fkWalletAccountId;
	}

	public void setFkWalletAccountId(Integer fkWalletAccountId) {
		this.fkWalletAccountId = fkWalletAccountId;
	}

	@Column(name = "transaction_id", length = 64)
	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name = "order_id", length = 64)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "session_id", length = 64)
	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name = "transaction_type", length = 2)
	public String getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name = "amount", precision = 10)
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "current_balance", precision = 10)
	public Double getCurrentBalance() {
		return this.currentBalance;
	}

	public void setCurrentBalance(Double currentBalance) {
		this.currentBalance = currentBalance;
	}

	@Column(name = "amount_source", length = 2)
	public String getAmountSource() {
		return this.amountSource;
	}

	public void setAmountSource(String amountSource) {
		this.amountSource = amountSource;
	}

	@Column(name = "status")
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Column(name = "fk_payment_transaciton_id")
	public Integer getFkPaymentTransacitonId() {
		return this.fkPaymentTransacitonId;
	}

	public void setFkPaymentTransacitonId(Integer fkPaymentTransacitonId) {
		this.fkPaymentTransacitonId = fkPaymentTransacitonId;
	}

	@Column(name = "fk_parent_payment_transaction_id")
	public Integer getFkParentPaymentTransactionId() {
		return this.fkParentPaymentTransactionId;
	}

	public void setFkParentPaymentTransactionId(
			Integer fkParentPaymentTransactionId) {
		this.fkParentPaymentTransactionId = fkParentPaymentTransactionId;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

}