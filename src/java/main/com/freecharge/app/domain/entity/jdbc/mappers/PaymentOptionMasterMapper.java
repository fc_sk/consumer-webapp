package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PaymentOptionMaster;

public class PaymentOptionMasterMapper implements RowMapper<PaymentOptionMaster> {

    @Override
    public PaymentOptionMaster mapRow(ResultSet rs, int rowNum) throws SQLException {
        PaymentOptionMaster paymentOption = new PaymentOptionMaster();
        paymentOption.setId(rs.getInt("id"));
        paymentOption.setPaymentType(rs.getString("payment_type"));
        paymentOption.setPaymentOptionDisplayName(rs.getString("payment_option_display_name"));
        paymentOption.setPaymentOptionCode(rs.getString("payment_option_code"));
        paymentOption.setImageUrl(rs.getString("image_url"));
        return paymentOption;
    }

}
