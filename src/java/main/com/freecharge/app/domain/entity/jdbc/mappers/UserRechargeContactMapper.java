package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UserRechargeContact;

public class UserRechargeContactMapper implements RowMapper<UserRechargeContact> {
	
	@Override
	public UserRechargeContact mapRow(ResultSet rs, int row) throws SQLException {
		UserRechargeContact userRechargeContact = new UserRechargeContact();
		
		userRechargeContact.setUserRechargeContactId(rs.getInt("user_recharge_contact_id"));
		userRechargeContact.setFkUsersId(rs.getInt("fk_users_id"));
		userRechargeContact.setServiceNumber(rs.getString("service_number"));
		userRechargeContact.setFkProductMasterId(rs.getInt("fk_product_master_id"));
		userRechargeContact.setFkCircleMasterId(rs.getInt("fk_circle_master_id"));
		userRechargeContact.setFkOperatorMasterId(rs.getInt("fk_operator_master_id"));
		userRechargeContact.setNickAlias(rs.getString("nick_alias"));
		userRechargeContact.setLastRechargeAmount(rs.getDouble("last_recharge_amount"));
		userRechargeContact.setIsActive(rs.getBoolean("is_active"));
		userRechargeContact.setCreatedOn(rs.getTimestamp("created_on"));
		userRechargeContact.setUpdatedOn(rs.getTimestamp("updated_on"));
		
		return userRechargeContact;
	}
}
