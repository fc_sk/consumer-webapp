package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderIdLidMapper implements RowMapper<String> {

	@Override
	public String mapRow(ResultSet rs, int rownum) throws SQLException {
		return rs.getString("lookup_id");
	}
}
