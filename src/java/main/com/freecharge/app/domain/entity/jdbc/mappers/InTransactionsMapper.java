package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InTransactions;

public class InTransactionsMapper implements RowMapper<InTransactions> {
	@Override
	public InTransactions mapRow(ResultSet rs, int row) throws SQLException {
		InTransactions inTransactions = new InTransactions();
		
		inTransactions.setTransactionId(rs.getLong("transaction_id"));
		inTransactions.setFkRequestId(rs.getLong("fk_request_id"));
		inTransactions.setAggrName(rs.getString("aggr_name"));
		inTransactions.setAggrResponseCode(rs.getString("aggr_response_code"));
		inTransactions.setAggrResponseMessage(rs.getString("aggr_response_message"));
		inTransactions.setAggrReferenceNo(rs.getString("aggr_reference_no"));
		inTransactions.setAggrOprRefernceNo(rs.getString("aggr_opr_refernce_no"));
		inTransactions.setRawRequest(rs.getString("raw_request"));
		inTransactions.setRawResponse(rs.getString("raw_response"));
		inTransactions.setRequestTime(rs.getTimestamp("request_time"));
		inTransactions.setResponseTime(rs.getTimestamp("response_time"));
		inTransactions.setTransactionStatus(rs.getString("transaction_status"));
		
		
		return inTransactions;
	}

}
