package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UtmTracker;

public class UtmTrackerRowMapper implements RowMapper{
        @Override
        public UtmTracker mapRow(ResultSet rs, int row) throws SQLException {
            UtmTracker utmTracker = new UtmTracker();

            utmTracker.setCampaignId(rs.getInt("utm_campaign_id"));
            utmTracker.setFcMaster(rs.getString("fc_master"));
            utmTracker.setCampaignName(rs.getString("utm_campaign_name"));
            utmTracker.setMedium(rs.getString("utm_medium"));
            utmTracker.setSource(rs.getString("utm_source"));
            utmTracker.setKeyword(rs.getString("utm_keyword"));
            utmTracker.setUrl(rs.getString("utm_url"));
            utmTracker.setContent(rs.getString("utm_content"));
            utmTracker.setForwardUrl(rs.getString("forward_url"));
            utmTracker.setCreated_by(rs.getString("created_by"));
            utmTracker.setUpdated_by(rs.getString("updated_by"));
            return utmTracker;
        }
}
