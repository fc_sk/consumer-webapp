package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.CountryMaster;

public class CountryMasterMapper implements RowMapper<CountryMaster> {
	@Override
	public CountryMaster mapRow(ResultSet rs, int row) throws SQLException {
		CountryMaster countryMaster = new CountryMaster();
		
		countryMaster.setCountryMasterId(rs.getInt("country_master_id"));
		countryMaster.setCountryName(rs.getString("country_name"));
		countryMaster.setCountryCode(rs.getString("country_code"));
		countryMaster.setIsActive(rs.getBoolean("is_active"));
		countryMaster.setIso3(rs.getString("iso3"));
		countryMaster.setNumcode(rs.getString("numcode"));
		countryMaster.setPrintName(rs.getString("print_name"));
		
		return countryMaster;
	}

}
