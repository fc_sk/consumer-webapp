package com.freecharge.app.domain.entity;

public class AccessDetails {

    private int id;
    private String accessKey;
    private String api;
    private boolean isActive;

    public int getId() {
        return id;
    }
    
    public String getAccessKey() {
        return accessKey;
    }
    
    public String getApi() {
        return api;
    }
    
    public boolean isActive() {
        return isActive;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }
    
}
