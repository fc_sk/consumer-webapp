package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;

public class InRechargeRetryMapMapper implements RowMapper<InRechargeRetryMap> {
	@Override
	public InRechargeRetryMap mapRow(ResultSet rs, int row) throws SQLException {
		InRechargeRetryMap rechargeRetryMap = new InRechargeRetryMap();
		
		rechargeRetryMap.setId(rs.getInt("id"));
		rechargeRetryMap.setAggrName(rs.getString("aggr_name"));
		rechargeRetryMap.setAggrRespCode(rs.getString("aggr_resp_code"));
		rechargeRetryMap.setFkOperatorMasterId(rs.getString("fk_operator_master_id"));
		rechargeRetryMap.setIsRetryable(rs.getBoolean("is_retryable"));
		rechargeRetryMap.setFkRetryOperatorMasterId(rs.getString("fk_retry_operator_master_Id"));
		rechargeRetryMap.setRetryOperatorName(rs.getString("retry_operator_Name"));
		rechargeRetryMap.setRetryProductId(rs.getString("retry_product_Id"));
		rechargeRetryMap.setRetryPriority(rs.getString("retry_priority"));
		 
		return rechargeRetryMap;
	}

}
