package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PgRelation;

public class PgRelationRowMapper  implements RowMapper<PgRelation>{
@Override
public PgRelation mapRow(ResultSet rs, int row) throws SQLException {
	PgRelation pgRelation = new PgRelation();
	pgRelation.setActive(rs.getBoolean("is_active"));
	pgRelation.setAffiliateId(rs.getInt("fk_affiliate_profile_id"));
	pgRelation.setBankMasterId(rs.getInt("fk_banks_grp_master_id"));
	pgRelation.setCircleId(rs.getInt("fk_circle_master_id"));
	pgRelation.setId(rs.getInt("pg_relation_id"));
	pgRelation.setInId(rs.getInt("fk_in_master"));
	pgRelation.setPrimaryGateway(rs.getInt("primary_gateway"));
	pgRelation.setProductId(rs.getInt("fk_product_master_id"));
	pgRelation.setSecondaryGateway(rs.getInt("secondary_gateway"));
	return pgRelation;
}
}
