package com.freecharge.app.domain.entity.jdbc;


public class EuronetCallbackRequest {
    private String merchantrefno;
    private String enrefno;
    private String sprefno;
    private String responsecode;
    private String responsemessage;
    private String responseaction;

    public String getMerchantrefno() {
        return merchantrefno;
    }

    public void setMerchantrefno(String merchantrefno) {
        this.merchantrefno = merchantrefno;
    }

    public String getEnrefno() {
        return enrefno;
    }

    public void setEnrefno(String enrefno) {
        this.enrefno = enrefno;
    }

    public String getSprefno() {
        return sprefno;
    }

    public void setSprefno(String sprefno) {
        this.sprefno = sprefno;
    }

    public String getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public String getResponseaction() {
        return responseaction;
    }

    public void setResponseaction(String responseaction) {
        this.responseaction = responseaction;
    }

    @Override
    public String toString() {
        return "[merchantrefno=" + merchantrefno + ", enrefno=" + enrefno + ", sprefno="
                + sprefno + ", responsecode=" + responsecode + ", responsemessage=" + responsemessage
                + ", responseaction=" + responseaction + "]";
    }
    
    
}
