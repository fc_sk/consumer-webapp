package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.util.BillPayUtil;

public class FetchBillValidatorMapper implements RowMapper<BillPayValidator> {

	@Override
	public BillPayValidator mapRow(ResultSet rs, int row) throws SQLException {
		BillPayValidator billValidator = new BillPayValidator();
		billValidator.setId(rs.getInt("id"));
		billValidator.setFkOperatorId(rs.getInt("fk_operator_id"));
		billValidator.setFkCircleId(rs.getInt("fk_circle_id"));
		billValidator.setBillerType(rs.getInt("biller_type"));
		billValidator.setPartialPay(rs.getBoolean("is_partial_pay"));

		Integer requiredValidators = rs.getInt("required_validators");

		List<HashMap<String, String>> validator = new ArrayList<HashMap<String, String>>();

		for (int i = 1; i <= requiredValidators; i++) {
			String name = rs.getString("info" + i + "_name");
			if (!BillPayUtil.isEmpty(name)) {
				String regex = rs.getString("info" + i + "_regex");
				String msg = rs.getString("info" + i + "_msg");
				HashMap<String, String> info = new HashMap<>();
				info.put("name", name);
				info.put("regex", regex);
				info.put("msg", msg);
				validator.add(i - 1, info);
			} else {
				break;
			}
		}
		billValidator.setValidators(validator);
		return billValidator;
	}

}
