package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.sql.Timestamp;

public class ReplacedCoupons implements Serializable {

		private static final long serialVersionUID = 1L;
		private Integer id;
		private String orderId;
		private Integer replacedCouponId;
		private Integer replacedCouponQuantity;
		private Integer fcTicketId;
		private Timestamp createdOn;
		private Timestamp updatedOn;
		private String createdBy;
		private String updatedBy;
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public Integer getReplacedCouponId() {
			return replacedCouponId;
		}
		public void setReplacedCouponId(Integer replacedCouponId) {
			this.replacedCouponId = replacedCouponId;
		}
		public Integer getReplacedCouponQuantity() {
			return replacedCouponQuantity;
		}
		public void setReplacedCouponQuantity(Integer replacedCouponQuantity) {
			this.replacedCouponQuantity = replacedCouponQuantity;
		}
		public Integer getFcTicketId() {
			return fcTicketId;
		}
		public void setFcTicketId(Integer fcTicketId) {
			this.fcTicketId = fcTicketId;
		}
		public Timestamp getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Timestamp createdOn) {
			this.createdOn = createdOn;
		}
		public Timestamp getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Timestamp updatedOn) {
			this.updatedOn = updatedOn;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		
}
