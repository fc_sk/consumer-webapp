package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UserProfile;

public class UserProfileMapper implements RowMapper<UserProfile> {
	@Override
	public UserProfile mapRow(ResultSet rs, int row) throws SQLException {
		UserProfile profile = new UserProfile();

		profile.setUserProfileId(rs.getInt("user_profile_id"));
		profile.setFkUserId(rs.getInt("fk_user_id"));
		profile.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		profile.setFkStateMasterId(rs.getInt("fk_state_master_id"));
		profile.setFkUserRechargeContactId(rs.getInt("fk_user_recharge_contact_id"));
		profile.setAddress1(rs.getString("address1"));
		profile.setArea(rs.getString("area"));
		profile.setCity(rs.getString("city"));
		profile.setCreatedOn(rs.getTimestamp("created_on"));
		profile.setFirstName(rs.getString("first_name"));
		profile.setIsActive(rs.getBoolean("is_active"));
		profile.setIsDefault(rs.getBoolean("is_default"));
		profile.setLandmark(rs.getString("landmark"));
		profile.setLastUpdate(rs.getTimestamp("last_update"));
		profile.setLastName(rs.getString("last_name"));
		profile.setNickAlias(rs.getString("nick_alias"));
		profile.setPostalCode(rs.getString("postal_code"));
		profile.setStreet(rs.getString("street"));
		profile.setTitle(rs.getString("title"));
		
		return profile;
	}

}
