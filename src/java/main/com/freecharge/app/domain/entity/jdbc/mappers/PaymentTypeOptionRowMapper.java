package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.PaymentTypeOptions;


public class PaymentTypeOptionRowMapper  implements RowMapper<PaymentTypeOptions> {

	@Override
	public PaymentTypeOptions mapRow(ResultSet rs, int row) throws SQLException {
		PaymentTypeOptions adminUsers = new PaymentTypeOptions();
		
		adminUsers.setCode(rs.getString("code"));
		adminUsers.setPaymentTypeOptionsId(rs.getInt("payment_type_options_id"));
		adminUsers.setFkPaymentTypeRelationId(rs.getInt("fk_payment_type_relation_id"));
		adminUsers.setDisplayName(rs.getString("display_name"));
		adminUsers.setDisplayImg(rs.getString("display_img"));
		adminUsers.setImgUrl(rs.getString("img_url"));
		adminUsers.setIsActive(rs.getBoolean("is_active"));
		adminUsers.setOrderBy(rs.getInt("order_by"));
		return adminUsers;
	}

}
