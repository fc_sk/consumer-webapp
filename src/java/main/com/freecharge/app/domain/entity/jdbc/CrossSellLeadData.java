package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.sql.Timestamp;

public class CrossSellLeadData implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer fkUserId;
	private Integer fkCrossSellId;
	private String crossSellData;
	private Timestamp createdOn;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFkUserId() {
		return fkUserId;
	}
	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}
	public Integer getFkCrossSellId() {
		return fkCrossSellId;
	}
	public void setFkCrossSellId(Integer fkCrossSellId) {
		this.fkCrossSellId = fkCrossSellId;
	}
	public String getCrossSellData() {
		return crossSellData;
	}
	public void setCrossSellData(String crossSellData) {
		this.crossSellData = crossSellData;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

}
