package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *Manoj kumar
 */
@Entity
@Table(name = "airtel_recharge")
public class AirtelRecharge implements java.io.Serializable {

	// Fields

	private Integer airtelRechargeId;
	private String  airtelRechargeTitle;
	private String  airtelRechargeDesc;
	private Integer airtelRechargeAmount;
	private Timestamp addedOn;

    @Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "airtel_recharge_id", unique = true, nullable = false)
    public Integer getAirtelRechargeId() {
        return airtelRechargeId;
    }// Property accessors

    public void setAirtelRechargeId(Integer airtelRechargeId) {
        this.airtelRechargeId = airtelRechargeId;
    }
    @Column(name = "added_on", nullable = false)
    public Timestamp getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Timestamp addedOn) {
        this.addedOn = addedOn;
    }

    @Column(name = "airtel_recharge_amount", nullable = false)
    public Integer getAirtelRechargeAmount() {
        return airtelRechargeAmount;
    }

    public void setAirtelRechargeAmount(Integer airtelRechargeAmount) {
        this.airtelRechargeAmount = airtelRechargeAmount;
    }
    @Column(name = "airtel_recharge_desc")
    public String getAirtelRechargeDesc() {
        return airtelRechargeDesc;
    }

    public void setAirtelRechargeDesc(String airtelRechargeDesc) {
        this.airtelRechargeDesc = airtelRechargeDesc;
    }
    @Column(name = "airtel_recharge_title")
    public String getAirtelRechargeTitle() {
        return airtelRechargeTitle;
    }

    public void setAirtelRechargeTitle(String airtelRechargeTitle) {
        this.airtelRechargeTitle = airtelRechargeTitle;
    }

   }