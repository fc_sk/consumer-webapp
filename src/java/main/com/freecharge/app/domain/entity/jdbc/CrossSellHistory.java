package com.freecharge.app.domain.entity.jdbc;

/**
 * This class is used to capture the snapshot of cross sell details which exist when the order was placed.
 * This is done to keep a record, since cross sell stuff like tnc, validity date etc change over a period of time.
 * The snapshot is also used to render a cross sell list view.
 * @author shirish
 *
 */
public class CrossSellHistory {
	private String campaignName;
	private String crossSellData;
	private String crossSellLabel;
	private int crossSellPrice;
	private String crossSellValidFrom;
	private String crossSellValidUpto;
	private boolean crossSellIsActive;
	private String cityNameList;
	private String couponCode;
	private String blockTime;
	private Boolean isComplimentary;
	private Boolean isUnique;
	private Boolean isMCoupon;
	private String couponStoreValidityDate;
	private String orderId;
	private String createdOn;
	private Integer userId;
	private String couponType;
	public String getCrossSellData() {
		return crossSellData;
	}
	public void setCrossSellData(String crossSellData) {
		this.crossSellData = crossSellData;
	}
	public String getCrossSellLabel() {
		return crossSellLabel;
	}
	public void setCrossSellLabel(String crossSellLabel) {
		this.crossSellLabel = crossSellLabel;
	}
	public int getCrossSellPrice() {
		return crossSellPrice;
	}
	public void setCrossSellPrice(int crossSellPrice) {
		this.crossSellPrice = crossSellPrice;
	}
	public String getCrossSellValidFrom() {
		return crossSellValidFrom;
	}
	public void setCrossSellValidFrom(String crossSellValidFrom) {
		this.crossSellValidFrom = crossSellValidFrom;
	}
	public boolean isCrossSellIsActive() {
		return crossSellIsActive;
	}
	public void setCrossSellIsActive(boolean crossSellIsActive) {
		this.crossSellIsActive = crossSellIsActive;
	}
	public String getCityNameList() {
		return cityNameList;
	}
	public void setCityNameList(String cityNameList) {
		this.cityNameList = cityNameList;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getBlockTime() {
		return blockTime;
	}
	public void setBlockTime(String blockTime) {
		this.blockTime = blockTime;
	}
	public Boolean getIsComplimentary() {
		return isComplimentary;
	}
	public void setIsComplimentary(Boolean isComplimentary) {
		this.isComplimentary = isComplimentary;
	}
	public Boolean getIsUnique() {
		return isUnique;
	}
	public void setIsUnique(Boolean isUnique) {
		this.isUnique = isUnique;
	}
	public Boolean getIsMCoupon() {
		return isMCoupon;
	}
	public void setIsMCoupon(Boolean isMCoupon) {
		this.isMCoupon = isMCoupon;
	}
	public String getCouponStoreValidityDate() {
		return couponStoreValidityDate;
	}
	public void setCouponStoreValidityDate(String couponStoreValidityDate) {
		this.couponStoreValidityDate = couponStoreValidityDate;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getCrossSellValidUpto() {
		return crossSellValidUpto;
	}
	public void setCrossSellValidUpto(String crossSellValidUpto) {
		this.crossSellValidUpto = crossSellValidUpto;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
}
