package com.freecharge.app.domain.entity.winpin;



import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CouponBatch entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "coupon_batch")
public class CouponBatch implements java.io.Serializable {

	// Fields

	private Long cpnbId;
	private CpnUser cpnUser;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private Boolean cpnbBlock;
	private Date cpnbValidity;
	private Set<CouponPin> couponPins = new HashSet<CouponPin>(0);

	// Constructors

	/** default constructor */
	public CouponBatch() {
	}

	/** full constructor */
	public CouponBatch( CpnUser cpnUser,
			String cfActionStatus, Date cfActiveDate, Boolean cfActiveFlag,
			Boolean cfArchiveStatus, Timestamp cfDateCreatedOn,
			Timestamp cfDateLastUpdatedOn, Long cfLastUpdatedById,
			Boolean cpnbBlock, Date cpnbValidity, Set<CouponPin> couponPins) {

		this.cpnUser = cpnUser;
		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.cpnbBlock = cpnbBlock;
		this.cpnbValidity = cpnbValidity;
		this.couponPins = couponPins;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CPNB_ID", unique = true, nullable = false)
	public Long getCpnbId() {
		return this.cpnbId;
	}

	public void setCpnbId(Long cpnbId) {
		this.cpnbId = cpnbId;
	}



	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CF_CREATED_BY_ID")
	public CpnUser getCpnUser() {
		return this.cpnUser;
	}

	public void setCpnUser(CpnUser cpnUser) {
		this.cpnUser = cpnUser;
	}

	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "CPNB_BLOCK")
	public Boolean getCpnbBlock() {
		return this.cpnbBlock;
	}

	public void setCpnbBlock(Boolean cpnbBlock) {
		this.cpnbBlock = cpnbBlock;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CPNB_VALIDITY", length = 0)
	public Date getCpnbValidity() {
		return this.cpnbValidity;
	}

	public void setCpnbValidity(Date cpnbValidity) {
		this.cpnbValidity = cpnbValidity;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "couponBatch")
	public Set<CouponPin> getCouponPins() {
		return this.couponPins;
	}

	public void setCouponPins(Set<CouponPin> couponPins) {
		this.couponPins = couponPins;
	}

}