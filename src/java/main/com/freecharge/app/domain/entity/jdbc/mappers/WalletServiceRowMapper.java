package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.wallet.service.Wallet;

public class WalletServiceRowMapper implements RowMapper<Wallet> {

    @Override
    public Wallet mapRow(ResultSet rs, int row) throws SQLException {

        Wallet walletService = new Wallet();

        walletService.setWalletStatusId(rs.getInt("wallet_status_id"));
        walletService.setFkUserId(rs.getInt("fk_user_id"));
        walletService.setCurrentBalance(rs.getDouble("balance"));
        walletService.setCreatedOn(rs.getDate("created_ts"));
        walletService.setUpdatedOn(rs.getDate("updated_ts"));
        return walletService;
    }

}
