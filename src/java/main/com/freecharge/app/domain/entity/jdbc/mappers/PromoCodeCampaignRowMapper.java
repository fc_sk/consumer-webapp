package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;

public class PromoCodeCampaignRowMapper implements RowMapper<PromocodeCampaign> {

    @Override
    public PromocodeCampaign mapRow(ResultSet rs, int row) throws SQLException {
        PromocodeCampaign campaign = new PromocodeCampaign();
        campaign.setCampaignId(rs.getInt("promocode_campaign_id"));
        campaign.setFkPromocodeId(rs.getInt("fk_promocode_id"));
        campaign.setPhoneNo(rs.getString("phone_no"));
        campaign.setUserId(rs.getInt("fk_user_id"));
        campaign.setCreatedDate(rs.getDate("created_date"));
        campaign.setStatus(rs.getString("status"));
        return campaign;
    }

}
