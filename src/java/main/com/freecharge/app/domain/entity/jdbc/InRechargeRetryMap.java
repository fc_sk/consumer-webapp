package com.freecharge.app.domain.entity.jdbc;

import java.util.List;

public class InRechargeRetryMap implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer           id;
    private String            aggrName;
    private String            aggrRespCode;
    private String            fkOperatorMasterId;
    private String            fkRetryOperatorMasterId;
    private String            retryOperatorName;
    private String            retryProductId;
    private Boolean           isRetryable;
    private String            retryPriority;
    private String 			  fkCircleMasterId;
    private String 			  retryCircleName;
    private String 			  fkRetryCirlcleMasterId;
    private boolean 		  isProductTypeRetry;
    private boolean           aggregatorRetry;
    private List<String>      aggregatorsToSkip;
 
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAggrName() {
        return this.aggrName;
    }

    public void setAggrName(String aggrName) {
        this.aggrName = aggrName;
    }

    public String getAggrRespCode() {
        return this.aggrRespCode;
    }

    public void setAggrRespCode(String aggrRespCode) {
        this.aggrRespCode = aggrRespCode;
    }

    public String getFkOperatorMasterId() {
        return fkOperatorMasterId;
    }

    public void setFkOperatorMasterId(String fkOperatorMasterId) {
        this.fkOperatorMasterId = fkOperatorMasterId;
    }

    public String getFkRetryOperatorMasterId() {
        return fkRetryOperatorMasterId;
    }

    public void setFkRetryOperatorMasterId(String fkRetryOperatorMasterId) {
        this.fkRetryOperatorMasterId = fkRetryOperatorMasterId;
    }

    public Boolean getIsRetryable() {
        return this.isRetryable;
    }

    public void setIsRetryable(Boolean isRetryable) {
        this.isRetryable = isRetryable;
    }

    public String getRetryOperatorName() {
        return retryOperatorName;
    }

    public void setRetryOperatorName(String retryOperatorName) {
        this.retryOperatorName = retryOperatorName;
    }

    public String getRetryProductId() {
        return retryProductId;
    }

    public void setRetryProductId(String retryProductId) {
        this.retryProductId = retryProductId;
    }

    public String getRetryPriority() {
        return retryPriority;
    }

    public void setRetryPriority(String retryPriority) {
        this.retryPriority = retryPriority;
    }

    public String getFkCircleMasterId() {
		return fkCircleMasterId;
	}

	public void setFkCircleMasterId(String fkCircleMasterId) {
		this.fkCircleMasterId = fkCircleMasterId;
	}

	public String getRetryCircleName() {
		return retryCircleName;
	}

	public void setRetryCircleName(String retryCircleName) {
		this.retryCircleName = retryCircleName;
	}

	public String getFkRetryCirlcleMasterId() {
		return fkRetryCirlcleMasterId;
	}

	public void setFkRetryCirlcleMasterId(String fkRetryCirlcleMasterId) {
		this.fkRetryCirlcleMasterId = fkRetryCirlcleMasterId;
	}

	public boolean getIsProductTypeRetry() {
		return isProductTypeRetry;
	}

	public void setIsProductTypeRetry(boolean isProductTypeRetry) {
		this.isProductTypeRetry = isProductTypeRetry;
	}

	@Override
    public boolean equals(Object obj) {
        return this.getId().equals(((InErrorcodeMap) obj).getId());
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    public boolean isAggregatorRetry() {
        return aggregatorRetry;
    }

    public void setAggregatorRetry(boolean aggregatorRetry) {
        this.aggregatorRetry = aggregatorRetry;
    }

    public List<String> getAggregatorsToSkip() {
        return aggregatorsToSkip;
    }

    public void setAggregatorsToSkip(List<String> aggregatorsToSkip) {
        this.aggregatorsToSkip = aggregatorsToSkip;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InRechargeRetryMap [id=").append(id).append(", aggrName=").append(aggrName)
				.append(", aggrRespCode=").append(aggrRespCode).append(", fkOperatorMasterId=")
				.append(fkOperatorMasterId).append(", fkRetryOperatorMasterId=").append(fkRetryOperatorMasterId)
				.append(", retryOperatorName=").append(retryOperatorName).append(", retryProductId=")
				.append(retryProductId).append(", isRetryable=").append(isRetryable).append(", retryPriority=")
				.append(retryPriority).append(", fkCircleMasterId=").append(fkCircleMasterId)
				.append(", retryCircleName=").append(retryCircleName).append(", fkRetryCirlcleMasterId=")
				.append(fkRetryCirlcleMasterId).append(", isProductTypeRetry=").append(isProductTypeRetry)
				.append(", aggregatorRetry=").append(aggregatorRetry).append(", aggregatorsToSkip=")
				.append(aggregatorsToSkip).append("]");
		return builder.toString();
	}    
    
}