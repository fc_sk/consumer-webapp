package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * PaymentTypeRelation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "payment_type_relation")
public class PaymentTypeRelation implements java.io.Serializable {

	// Fields

	private Integer paymentTypeRelationId;
	private Integer fkProductMasterId;
	private String fkAffliateUniqueId;
	private Integer fkPaymentTypeId;
	private Boolean isActive;
    private Integer orderBy;

    // Constructors

	/** default constructor */
	public PaymentTypeRelation() {
	}

	/** minimal constructor */
	public PaymentTypeRelation(Boolean isActive) {
		this.isActive = isActive;
	}

	/** full constructor */
	public PaymentTypeRelation(Integer fkProductMasterId,
			String fkAffliateUniqueId, Integer fkPaymentTypeId, Boolean isActive) {
		this.fkProductMasterId = fkProductMasterId;
		this.fkAffliateUniqueId = fkAffliateUniqueId;
		this.fkPaymentTypeId = fkPaymentTypeId;
		this.isActive = isActive;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "payment_type_relation_id", unique = true, nullable = false)
	public Integer getPaymentTypeRelationId() {
		return this.paymentTypeRelationId;
	}

	public void setPaymentTypeRelationId(Integer paymentTypeRelationId) {
		this.paymentTypeRelationId = paymentTypeRelationId;
	}

	@Column(name = "fk_product_master_id")
	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	@Column(name = "fk_affliate_unique_id", length = 36)
	public String getFkAffliateUniqueId() {
		return this.fkAffliateUniqueId;
	}

	public void setFkAffliateUniqueId(String fkAffliateUniqueId) {
		this.fkAffliateUniqueId = fkAffliateUniqueId;
	}

	@Column(name = "fk_payment_type_id")
	public Integer getFkPaymentTypeId() {
		return this.fkPaymentTypeId;
	}

	public void setFkPaymentTypeId(Integer fkPaymentTypeId) {
		this.fkPaymentTypeId = fkPaymentTypeId;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

    @Column(name = "order_by", nullable = false)
    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }
}