package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SiteAutosuggest entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "site_autosuggest")
public class SiteAutosuggest implements java.io.Serializable {

	// Fields

	private Integer siteAutosuggestId;
	private Integer fkOperatorMasterId;
	private Integer fkCircleMasterId;
	private Integer fkProductMasterId;
	private String prefix;
	private Boolean isActive;

	// Constructors

	/** default constructor */
	public SiteAutosuggest() {
	}

	/** minimal constructor */
	public SiteAutosuggest(Boolean isActive) {
		this.isActive = isActive;
	}

	/** full constructor */
	public SiteAutosuggest(Integer fkOperatorMasterId,
			Integer fkCircleMasterId, Integer fkProductMasterId, String prefix,
			Boolean isActive) {
		this.fkOperatorMasterId = fkOperatorMasterId;
		this.fkCircleMasterId = fkCircleMasterId;
		this.fkProductMasterId = fkProductMasterId;
		this.prefix = prefix;
		this.isActive = isActive;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "site_autosuggest_id", unique = true, nullable = false)
	public Integer getSiteAutosuggestId() {
		return this.siteAutosuggestId;
	}

	public void setSiteAutosuggestId(Integer siteAutosuggestId) {
		this.siteAutosuggestId = siteAutosuggestId;
	}

	@Column(name = "fk_operator_master_id")
	public Integer getFkOperatorMasterId() {
		return this.fkOperatorMasterId;
	}

	public void setFkOperatorMasterId(Integer fkOperatorMasterId) {
		this.fkOperatorMasterId = fkOperatorMasterId;
	}

	@Column(name = "fk_circle_master_id")
	public Integer getFkCircleMasterId() {
		return this.fkCircleMasterId;
	}

	public void setFkCircleMasterId(Integer fkCircleMasterId) {
		this.fkCircleMasterId = fkCircleMasterId;
	}

	@Column(name = "fk_product_master_id")
	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	@Column(name = "prefix", length = 25)
	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}