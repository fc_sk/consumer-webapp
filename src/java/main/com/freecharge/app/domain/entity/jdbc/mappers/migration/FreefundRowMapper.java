package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.freefund.dos.entity.FreefundCoupon;

public class FreefundRowMapper implements RowMapper<FreefundCoupon> {
	@Override
	public FreefundCoupon mapRow(ResultSet rs, int row) throws SQLException {
		FreefundCoupon freefundCoupon = new FreefundCoupon();
		
		freefundCoupon.setId(rs.getLong("freefund_coupon_id"));
		freefundCoupon.setFreefundClassId(rs.getLong("freefund_class_id"));
		freefundCoupon.setFreefundCode(rs.getString("freefund_code"));
		
		freefundCoupon.setGeneratedDate(rs.getTimestamp("generated_date"));
		
		freefundCoupon.setUsedDate(rs.getTimestamp("used_date"));
		freefundCoupon.setUsedEmailId(rs.getString("used_email_id"));
		freefundCoupon.setUsedServiceNumber(rs.getString("used_service_number"));
		
		freefundCoupon.setFreefundValue(rs.getFloat("freefund_value"));
		freefundCoupon.setMinRechargeValue(rs.getFloat("min_recharge_value"));
		freefundCoupon.setPromoEntity(rs.getString("promo_entity"));
		freefundCoupon.setStatus(rs.getString("status"));
		
		return freefundCoupon;
	}

}
