package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 8/11/12 Time: 8:05 PM To change this template use File |
 * Settings | File Templates.
 */
public class PromocodeCampaign {
	private int campaignId;

	private int fkPromocodeId;

	private String phoneNo;

	private int userId;
	
	private String orderId;
	
    private Date createdDate;

    private String status;

	public int getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}

	public int getFkPromocodeId() {
		return fkPromocodeId;
	}

	public void setFkPromocodeId(int fkPromocodeId) {
		this.fkPromocodeId = fkPromocodeId;
	}

	public MapSqlParameterSource getMapSqlParameterSource() {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("fk_promocode_id", this.getFkPromocodeId());
		mapSqlParameterSource.addValue("phone_no", this.getPhoneNo());
		mapSqlParameterSource.addValue("fk_user_id", this.getUserId());
		mapSqlParameterSource.addValue("orderid", this.getOrderId());
		mapSqlParameterSource.addValue("status", this.getStatus());
        mapSqlParameterSource.addValue("created_date", this.getCreatedDate());
		return mapSqlParameterSource;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static PromocodeCampaign createDummyBean() {
		PromocodeCampaign campaign = new PromocodeCampaign();
		campaign.setPhoneNo("9886340067");
		campaign.setUserId(1);
		return campaign;
	}
}
