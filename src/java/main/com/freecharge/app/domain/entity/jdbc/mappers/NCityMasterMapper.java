package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.api.coupon.service.model.CityMaster;


public class NCityMasterMapper implements RowMapper<CityMaster> {
    private Map<String, String> prefixMap;

    public NCityMasterMapper(Map<String, String> prefixMap) {
        this.prefixMap = prefixMap;
    }

    @Override
    public CityMaster mapRow(ResultSet rs, int rowNum) throws SQLException {
        CityMaster cityMaster = new CityMaster();
        for (Map.Entry<String, String> entry: this.prefixMap.entrySet()) {
            if(entry.getKey().equals("city_master")) {
                cityMaster.setCityMasterId(rs.getInt("city_master_id"));
                cityMaster.setCityName(rs.getString("city_name"));
                cityMaster.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
                cityMaster.setFkStateMasterId(rs.getInt("fk_state_master_id"));
                cityMaster.setFkZoneMasterId(rs.getInt("fk_zone_master_id"));
                cityMaster.setStatus(rs.getBoolean("status"));
            }
            //Add others here like state, zone, country
        }
        return cityMaster;
    }
}
