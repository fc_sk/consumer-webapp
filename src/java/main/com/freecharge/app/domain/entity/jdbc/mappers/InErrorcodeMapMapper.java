package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;

public class InErrorcodeMapMapper implements RowMapper<InErrorcodeMap> {
	@Override
	public InErrorcodeMap mapRow(ResultSet rs, int row) throws SQLException {
		InErrorcodeMap errorcodeMap = new InErrorcodeMap();
		
		errorcodeMap.setId(rs.getInt("id"));
		errorcodeMap.setAggrName(rs.getString("aggr_name"));
		errorcodeMap.setAggrRespCode(rs.getString("aggr_resp_code"));
		errorcodeMap.setInErrorMsg(rs.getString("in_error_msg"));
		errorcodeMap.setInRespCode(rs.getString("in_resp_code"));
		errorcodeMap.setIsRetryable(rs.getBoolean("is_retryable"));
		errorcodeMap.setIsDisplayEnabled(rs.getBoolean("is_display_enabled"));
		
		return errorcodeMap;
	}

}
