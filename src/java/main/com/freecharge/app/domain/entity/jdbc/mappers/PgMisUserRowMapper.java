package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PgMisUser;

public class PgMisUserRowMapper implements RowMapper<PgMisUser> {

	@Override
	public PgMisUser mapRow(ResultSet rs, int row) throws SQLException {
		PgMisUser adminUsers = new PgMisUser();
		
		adminUsers.setPgUserID(rs.getInt("pg_user_id"));
		adminUsers.setPgUserName(rs.getString("pg_user_name"));
		adminUsers.setPgUserActiveStatus(rs.getBoolean("pg_user_active_status"));		
		adminUsers.setPgUserPassword(rs.getString("pg_user_password"));
		
		return adminUsers;
	}
}
