package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.FailedLogin;

public class FailedLoginMapper implements RowMapper<FailedLogin> {
    public static final FailedLoginMapper FAILED_LOGIN_MAPPER = new FailedLoginMapper();

    @Override
    public FailedLogin mapRow(final ResultSet resultSet, final int i) throws SQLException {
        FailedLogin failedLogin = new FailedLogin();

        int failedLoginId = resultSet.getInt(FailedLogin.FAILED_LOGIN_ID);
        failedLogin.setFailedLoginId(failedLoginId);

        Timestamp attempted = resultSet.getTimestamp(FailedLogin.FAILED_LOGIN_ATTEMPTED_TIME);
        failedLogin.setAttemptedTime(attempted);

        String userEmail = resultSet.getString(FailedLogin.FAILED_LOGIN_USER_EMAIL);
        failedLogin.setUserEmail(userEmail);

        String ipAddress = resultSet.getString(FailedLogin.FAILED_LOGIN_IP_ADDRESS);
        failedLogin.setIpAddress(ipAddress);

        return failedLogin;
    }
}
