package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UserProfile;

public class NUserProfileMapper implements RowMapper<UserProfile> {
    private Map<String, String> prefixMap;

    public NUserProfileMapper(Map<String, String> prefixMap) {
        this.prefixMap = prefixMap;
    }

    @Override
    public UserProfile mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserProfile profile = new UserProfile();
        for(Map.Entry<String, String> entry : prefixMap.entrySet()) {
            if(entry.getKey().equals("user_profile")){
                profile.setUserProfileId(rs.getInt(entry.getValue()+"user_profile_id"));
                profile.setFkUserId(rs.getInt(entry.getValue()+"fk_user_id"));
                profile.setFkCountryMasterId(rs.getInt(entry.getValue()+"fk_country_master_id"));
                profile.setFkStateMasterId(rs.getInt(entry.getValue()+"fk_state_master_id"));
                profile.setFkUserRechargeContactId(rs.getInt(entry.getValue()+"fk_user_recharge_contact_id"));
                profile.setAddress1(rs.getString(entry.getValue()+"address1"));
                profile.setArea(rs.getString(entry.getValue()+"area"));
                profile.setCity(rs.getString(entry.getValue()+"city"));
                profile.setCreatedOn(rs.getTimestamp(entry.getValue()+"created_on"));
                profile.setFirstName(rs.getString(entry.getValue()+"first_name"));
                profile.setIsActive(rs.getBoolean(entry.getValue()+"is_active"));
                profile.setIsDefault(rs.getBoolean(entry.getValue()+"is_default"));
                profile.setLandmark(rs.getString(entry.getValue()+"landmark"));
                profile.setLastUpdate(rs.getTimestamp(entry.getValue()+"last_update"));
                profile.setLastName(rs.getString(entry.getValue()+"last_name"));
                profile.setNickAlias(rs.getString(entry.getValue()+"nick_alias"));
                profile.setPostalCode(rs.getString(entry.getValue()+"postal_code"));
                profile.setStreet(rs.getString(entry.getValue()+"street"));
                profile.setTitle(rs.getString(entry.getValue()+"title"));
            }else if(entry.getKey().equals("city_master")){
                Map<String, String> cityPrefixMap = new HashMap<String, String>();
                cityPrefixMap.put("city_master", entry.getValue());
                if(prefixMap.get("country_master")!=null) {
                    cityPrefixMap.put("country_master", prefixMap.get("country_master"));
                }
                if(prefixMap.get("state_master")!=null) {
                    cityPrefixMap.put("state_master", prefixMap.get("state_master"));
                }
                if(prefixMap.get("zone_master")!=null) {
                    cityPrefixMap.put("zone_master", prefixMap.get("zone_master"));
                }
                profile.setCityMaster(new NCityMasterMapper(cityPrefixMap).mapRow(rs, rowNum));
            }
        }
        return profile;
    }
}
