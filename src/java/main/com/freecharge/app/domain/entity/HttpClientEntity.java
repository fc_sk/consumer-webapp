package com.freecharge.app.domain.entity;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public class HttpClientEntity {

	private HttpClient httpClient;
	private PoolingHttpClientConnectionManager connManager;
	
	public HttpClient getHttpClient() {
		return httpClient;
	}
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	public PoolingHttpClientConnectionManager getConnManager() {
		return connManager;
	}
	public void setConnManager(PoolingHttpClientConnectionManager connManager) {
		this.connManager = connManager;
	}
	
}
