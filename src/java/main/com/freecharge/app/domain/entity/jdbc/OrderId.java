package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

public class OrderId implements Serializable, DBEntity {

    @ColumnMapping(name="pk_order_id")
	private Integer pkOrderId;
    @ColumnMapping(name="lookup_id")
	private String lookupId;
    @ColumnMapping(name="session_id")
	private String sessionId;
    @ColumnMapping(name="order_id")
	private String orderId;
    @ColumnMapping(name="fk_product_master_id")
	private Integer fkProductMasterId;
    @ColumnMapping(name="fk_affiliate_profile_id")
	private Integer fkAffiliateProfileId;
    @ColumnMapping(name="created_on")
	private Timestamp createdOn;
    @ColumnMapping(name="channel_id")
	private Integer channelId;
    @ColumnMapping(name="order_id_inc")
	private Integer orderIdInc;

	/** default constructor */
	public OrderId() {
	}

	/** minimal constructor */
	public OrderId(String lookupId, String sessionId, String orderId, Integer fkProductMasterId, Timestamp createdOn) {
		this.lookupId = lookupId;
		this.sessionId = sessionId;
		this.orderId = orderId;
		this.fkProductMasterId = fkProductMasterId;
		this.createdOn = createdOn;
	}

	/** full constructor */
	public OrderId(String lookupId, String sessionId, String orderId, Integer fkProductMasterId, int fkAffiliateProfileId, int channelId, Timestamp createdOn) {
		this.lookupId = lookupId;
		this.sessionId = sessionId;
		this.orderId = orderId;
		this.fkProductMasterId = fkProductMasterId;
		this.fkAffiliateProfileId = fkAffiliateProfileId;
		this.channelId = channelId;
		this.createdOn = createdOn;
	}

	public Integer getPkOrderId() {
		return this.pkOrderId;
	}

	public void setPkOrderId(Integer pkOrderId) {
		this.pkOrderId = pkOrderId;
	}

	public String getLookupId() {
		return this.lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	public Integer getFkAffiliateProfileId() {
		return this.fkAffiliateProfileId;
	}

	public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getOrderIdInc() {
		return orderIdInc;
	}

	public void setOrderIdInc(Integer orderIdInc) {
		this.orderIdInc = orderIdInc;
	}

    @Override
    public String getTableName() {
        return "order_id";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        return null;
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }
    
    public boolean isMoreThanThreeHoursOld() {
        return this.createdOn != null
                && ( (new Date()).getTime() - this.createdOn.getTime()  > 3 * 60 * 60 * 1000);
    }

    @Override
    public String toString() {
        return "OrderId{" +
                "pkOrderId=" + pkOrderId +
                ", lookupId='" + lookupId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", fkProductMasterId=" + fkProductMasterId +
                ", fkAffiliateProfileId=" + fkAffiliateProfileId +
                ", createdOn=" + createdOn +
                ", channelId=" + channelId +
                ", orderIdInc=" + orderIdInc +
                '}';
    }
}