package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.common.businessdo.TxnHomePageBusinessDO;

public class TxnHomePageMapper implements RowMapper<TxnHomePageBusinessDO> {

    @Override
    public TxnHomePageBusinessDO mapRow(ResultSet rs, int row) throws SQLException {
        TxnHomePageBusinessDO bizDO = new TxnHomePageBusinessDO();
        
        bizDO.setProductType(rs.getString("product_type"));
        bizDO.setProductId(Integer.toString(rs.getInt("fk_product_master_id")));
        bizDO.setServiceNumber(rs.getString("service_number"));
        bizDO.setAmount(rs.getFloat("amount"));
        bizDO.setOperatorName(rs.getString("fk_operator_master_id"));
        bizDO.setCircleName(rs.getString("fk_circle_master_id"));
        bizDO.setOpName(rs.getString("operator_name"));
        bizDO.setCirName(rs.getString("circle_name"));
        
        return bizDO;
    }

}
