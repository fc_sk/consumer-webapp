package com.freecharge.app.domain.entity.winpin;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * CouponPin entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "coupon_pin",uniqueConstraints = @UniqueConstraint(columnNames = "CPN_PIN"))
public class CouponPin implements java.io.Serializable {

	// Fields

	private Long cpnId;
	private CouponBatch couponBatch;
	private CpnJobs cpnJobs;
	private Integer fkCoupanTypeId;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Long cfCreatedById;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private Boolean cpnActive;
	private Boolean cpnBlock;
	private Boolean cpnUsed;
	private BigDecimal cpnDenomination;
	private Boolean cpnInRecharge;
	private Boolean cpnIsRedeemed;
	private Timestamp cpnLockDatetime;
	private Boolean cpnLockStatus;
	private String cpnPin;
	private Timestamp cpnRedemptionDatetime;
	private String cpnSrNo;
	private Boolean cpnUsable;
	private String orderId;
	private String pgStatus;
	private String rechargeStatus;
	private Timestamp responseDatetime;
	private String uniqueId;
	private Set<CpnTransLogger> cpnTransLoggers = new HashSet<CpnTransLogger>(0);

	// Constructors

	/** default constructor */
	public CouponPin() {
	}

	/** minimal constructor */
	public CouponPin(Integer fkCoupanTypeId) {
		this.fkCoupanTypeId = fkCoupanTypeId;
	}

	/** full constructor */
	public CouponPin(CouponBatch couponBatch, CpnJobs cpnJobs,
			Integer fkCoupanTypeId, String cfActionStatus, Date cfActiveDate,
			Boolean cfActiveFlag, Boolean cfArchiveStatus, Long cfCreatedById,
			Timestamp cfDateCreatedOn, Timestamp cfDateLastUpdatedOn,
			Long cfLastUpdatedById, Boolean cpnActive, Boolean cpnBlock,
			Boolean cpnUsed, BigDecimal cpnDenomination, Boolean cpnInRecharge,
			Boolean cpnIsRedeemed, Timestamp cpnLockDatetime,
			Boolean cpnLockStatus, String cpnPin,
			Timestamp cpnRedemptionDatetime, String cpnSrNo, Boolean cpnUsable,
			String orderId, String pgStatus, String rechargeStatus,
			Timestamp responseDatetime, String uniqueId,
			Set<CpnTransLogger> cpnTransLoggers) {
		this.couponBatch = couponBatch;
		this.cpnJobs = cpnJobs;
		this.fkCoupanTypeId = fkCoupanTypeId;
		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfCreatedById = cfCreatedById;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.cpnActive = cpnActive;
		this.cpnBlock = cpnBlock;
		this.cpnUsed = cpnUsed;
		this.cpnDenomination = cpnDenomination;
		this.cpnInRecharge = cpnInRecharge;
		this.cpnIsRedeemed = cpnIsRedeemed;
		this.cpnLockDatetime = cpnLockDatetime;
		this.cpnLockStatus = cpnLockStatus;
		this.cpnPin = cpnPin;
		this.cpnRedemptionDatetime = cpnRedemptionDatetime;
		this.cpnSrNo = cpnSrNo;
		this.cpnUsable = cpnUsable;
		this.orderId = orderId;
		this.pgStatus = pgStatus;
		this.rechargeStatus = rechargeStatus;
		this.responseDatetime = responseDatetime;
		this.uniqueId = uniqueId;
		this.cpnTransLoggers = cpnTransLoggers;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CPN_ID", unique = true, nullable = false)
	public Long getCpnId() {
		return this.cpnId;
	}

	public void setCpnId(Long cpnId) {
		this.cpnId = cpnId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CPNB_ID")
	public CouponBatch getCouponBatch() {
		return this.couponBatch;
	}

	public void setCouponBatch(CouponBatch couponBatch) {
		this.couponBatch = couponBatch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CJ_ID")
	public CpnJobs getCpnJobs() {
		return this.cpnJobs;
	}

	public void setCpnJobs(CpnJobs cpnJobs) {
		this.cpnJobs = cpnJobs;
	}

	@Column(name = "FK_COUPAN_TYPE_ID", nullable = false)
	public Integer getFkCoupanTypeId() {
		return this.fkCoupanTypeId;
	}

	public void setFkCoupanTypeId(Integer fkCoupanTypeId) {
		this.fkCoupanTypeId = fkCoupanTypeId;
	}

	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_CREATED_BY_ID")
	public Long getCfCreatedById() {
		return this.cfCreatedById;
	}

	public void setCfCreatedById(Long cfCreatedById) {
		this.cfCreatedById = cfCreatedById;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "CPN_ACTIVE")
	public Boolean getCpnActive() {
		return this.cpnActive;
	}

	public void setCpnActive(Boolean cpnActive) {
		this.cpnActive = cpnActive;
	}

	@Column(name = "CPN_BLOCK")
	public Boolean getCpnBlock() {
		return this.cpnBlock;
	}

	public void setCpnBlock(Boolean cpnBlock) {
		this.cpnBlock = cpnBlock;
	}

	@Column(name = "CPN_USED")
	public Boolean getCpnUsed() {
		return this.cpnUsed;
	}

	public void setCpnUsed(Boolean cpnUsed) {
		this.cpnUsed = cpnUsed;
	}

	@Column(name = "CPN_DENOMINATION", precision = 22, scale = 0)
	public BigDecimal getCpnDenomination() {
		return this.cpnDenomination;
	}

	public void setCpnDenomination(BigDecimal cpnDenomination) {
		this.cpnDenomination = cpnDenomination;
	}

	@Column(name = "CPN_IN_RECHARGE")
	public Boolean getCpnInRecharge() {
		return this.cpnInRecharge;
	}

	public void setCpnInRecharge(Boolean cpnInRecharge) {
		this.cpnInRecharge = cpnInRecharge;
	}

	@Column(name = "CPN_IS_REDEEMED")
	public Boolean getCpnIsRedeemed() {
		return this.cpnIsRedeemed;
	}

	public void setCpnIsRedeemed(Boolean cpnIsRedeemed) {
		this.cpnIsRedeemed = cpnIsRedeemed;
	}

	@Column(name = "CPN_LOCK_DATETIME", length = 0)
	public Timestamp getCpnLockDatetime() {
		return this.cpnLockDatetime;
	}

	public void setCpnLockDatetime(Timestamp cpnLockDatetime) {
		this.cpnLockDatetime = cpnLockDatetime;
	}

	@Column(name = "CPN_LOCK_STATUS")
	public Boolean getCpnLockStatus() {
		return this.cpnLockStatus;
	}

	public void setCpnLockStatus(Boolean cpnLockStatus) {
		this.cpnLockStatus = cpnLockStatus;
	}

	@Column(name = "CPN_PIN", unique = true, length = 50)
	public String getCpnPin() {
		return this.cpnPin;
	}

	public void setCpnPin(String cpnPin) {
		this.cpnPin = cpnPin;
	}

	@Column(name = "CPN_REDEMPTION_DATETIME", length = 0)
	public Timestamp getCpnRedemptionDatetime() {
		return this.cpnRedemptionDatetime;
	}

	public void setCpnRedemptionDatetime(Timestamp cpnRedemptionDatetime) {
		this.cpnRedemptionDatetime = cpnRedemptionDatetime;
	}

	@Column(name = "CPN_SR_NO", length = 50)
	public String getCpnSrNo() {
		return this.cpnSrNo;
	}

	public void setCpnSrNo(String cpnSrNo) {
		this.cpnSrNo = cpnSrNo;
	}

	@Column(name = "CPN_USABLE")
	public Boolean getCpnUsable() {
		return this.cpnUsable;
	}

	public void setCpnUsable(Boolean cpnUsable) {
		this.cpnUsable = cpnUsable;
	}

	@Column(name = "ORDER_ID", length = 100)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "PG_STATUS", length = 50)
	public String getPgStatus() {
		return this.pgStatus;
	}

	public void setPgStatus(String pgStatus) {
		this.pgStatus = pgStatus;
	}

	@Column(name = "RECHARGE_STATUS", length = 50)
	public String getRechargeStatus() {
		return this.rechargeStatus;
	}

	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}

	@Column(name = "RESPONSE_DATETIME", length = 0)
	public Timestamp getResponseDatetime() {
		return this.responseDatetime;
	}

	public void setResponseDatetime(Timestamp responseDatetime) {
		this.responseDatetime = responseDatetime;
	}

	@Column(name = "UNIQUE_ID", length = 100)
	public String getUniqueId() {
		return this.uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "couponPin")
	public Set<CpnTransLogger> getCpnTransLoggers() {
		return this.cpnTransLoggers;
	}

	public void setCpnTransLoggers(Set<CpnTransLogger> cpnTransLoggers) {
		this.cpnTransLoggers = cpnTransLoggers;
	}

}