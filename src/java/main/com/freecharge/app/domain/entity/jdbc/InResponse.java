package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;
import java.util.Date;

import com.freecharge.common.util.FCUtil;


public class InResponse implements java.io.Serializable {

	
    private static final long serialVersionUID = 1L;
	private Long responseId;
	private String rawRequest;
	private String rawResponse;
	private String aggrTransId;
	private String oprTransId;
	private String aggrName;
	private String aggrRespCode;
	private String aggrRespMsg;
	private String inRespCode;
	private String inRespMsg;
	private Integer retryNumber;
	private Timestamp createdTime;
	private Timestamp updatedTime;
	private Long requestId;


	public Long getResponseId() {
		return this.responseId;
	}

	public void setResponseId(Long responseId) {
		this.responseId = responseId;
	}

	public String getRawRequest() {
		return this.rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public String getRawResponse() {
		return this.rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public String getAggrTransId() {
		return this.aggrTransId;
	}

	public void setAggrTransId(String aggrTransId) {
		this.aggrTransId = aggrTransId;
	}

	public String getOprTransId() {
		return this.oprTransId;
	}

	public void setOprTransId(String oprTransId) {
		this.oprTransId = oprTransId;
	}

	public String getAggrName() {
		return this.aggrName;
	}

	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getAggrRespCode() {
		return this.aggrRespCode;
	}

	public void setAggrRespCode(String aggrRespCode) {
		this.aggrRespCode = aggrRespCode;
	}

	public String getAggrRespMsg() {
		return this.aggrRespMsg;
	}

	public void setAggrRespMsg(String aggrRespMsg) {
		this.aggrRespMsg = aggrRespMsg;
	}

	public String getInRespCode() {
		return this.inRespCode;
	}

	public void setInRespCode(String inRespCode) {
		this.inRespCode = inRespCode;
	}

	public String getInRespMsg() {
		return this.inRespMsg;
	}

	public void setInRespMsg(String inRespMsg) {
		this.inRespMsg = inRespMsg;
	}

	public Integer getRetryNumber() {
		return this.retryNumber;
	}

	public void setRetryNumber(Integer retryNumber) {
		this.retryNumber = retryNumber;
	}

	public Timestamp getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	@Override
	public boolean equals(Object arg0) {		
		return this.getResponseId().equals(((InResponse)arg0).getResponseId());
	}
	
	@Override
	public int hashCode() {		
		return this.getResponseId().hashCode();
	}
	
	@Override
	public InResponse clone() {
	    InResponse toReturn = new InResponse();
	    toReturn.setAggrName(this.getAggrName());
	    toReturn.setAggrRespCode(this.getAggrRespCode());
	    toReturn.setAggrRespMsg(this.getAggrRespMsg());
	    toReturn.setAggrTransId(this.getAggrTransId());
	    toReturn.setInRespCode(this.getInRespCode());
	    toReturn.setInRespMsg(this.getInRespMsg());
	    toReturn.setOprTransId(this.getOprTransId());
	    toReturn.setRawRequest(this.getRawRequest());
	    toReturn.setRawResponse(this.getRawResponse());
	    toReturn.setRequestId(this.getRequestId());
	    toReturn.setResponseId(this.getResponseId());
	    toReturn.setRetryNumber(this.getRetryNumber());
        toReturn.setCreatedTime(new Timestamp(this.getCreatedTime().getTime()));
        toReturn.setUpdatedTime(new Timestamp(this.getUpdatedTime().getTime()));
	    
	    return toReturn;
	}
	
	public boolean isSuccessful(){
	    return  ("0".equals(this.getInRespCode()) || "00".equals(this.getInRespCode()));
	}
	
	public String getOperatorCode(){
	    return FCUtil.getOperatorCode(this.rawRequest,this.aggrName);
	}
	
	public boolean isMoreThanThreeHoursOld() {
        return this.createdTime != null
                && ( (new Date()).getTime() - this.createdTime.getTime()  > 3 * 60 * 60 * 1000);
    }

    @Override
    public String toString() {
        return "InResponse{" +
                "responseId=" + responseId +
                ", rawRequest='" + rawRequest + '\'' +
                ", rawResponse='" + rawResponse + '\'' +
                ", aggrTransId='" + aggrTransId + '\'' +
                ", oprTransId='" + oprTransId + '\'' +
                ", aggrName='" + aggrName + '\'' +
                ", aggrRespCode='" + aggrRespCode + '\'' +
                ", aggrRespMsg='" + aggrRespMsg + '\'' +
                ", inRespCode='" + inRespCode + '\'' +
                ", inRespMsg='" + inRespMsg + '\'' +
                ", retryNumber=" + retryNumber +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", requestId=" + requestId +
                '}';
    }
}
