package com.freecharge.app.domain.entity.jdbc;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;

public class FreefundClass extends AbstractDO implements java.io.Serializable {

    private static Logger logger = LoggingFactory.getLogger(FreefundClass.class);
    public static final String PROMO_ENTITY_ALL = "all-payment-options";
    public static final String PROMO_ENTITY_ICICINET = "icici-netbanking";
    public static final String PROMO_ENTITY_HDFCD = "hdfc-debitcard";
    public static final String PROMO_ENTITY_CASHBACK = "cashback-promocode";
    public static final String PROMO_ENTITY_DISCOUNT = "all-payment-options";
    public static final String PROMO_ENTITY_GENERIC_CASHBACK = "generic-cashback-promocode";
    public static final String PROMO_ENTITY_GENERIC_DISCOUNT = "generic-discount-promocode";
    public static final List FREEFUND_UNIQUE_CODE_TYPE_LIST = Arrays.asList(PROMO_ENTITY_CASHBACK, PROMO_ENTITY_DISCOUNT);

    private static final long serialVersionUID = 1L;

    public FreefundClass() {
    }

    public FreefundClass(FreefundClass freefundClass) {
        this.id = freefundClass.getId();
        this.name = freefundClass.getName();
        this.description = freefundClass.getDescription();
        this.isActive = freefundClass.getIsActive();
        this.maxRedemptions = freefundClass.getMaxRedemptions();
        this.minRechargeValue = freefundClass.getMinRechargeValue();
        this.freefundValue = freefundClass.getFreefundValue();
        this.promoEntity = freefundClass.getPromoEntity();
        this.validFrom = freefundClass.getValidFrom();
        this.validUpto = freefundClass.getValidUpto();
        this.createdAt = freefundClass.getCreatedAt();
        this.updatedAt = freefundClass.getUpdatedAt();
        this.discountType = freefundClass.getDiscountType();
        this.maxDiscount = freefundClass.getMaxDiscount();
        this.applyConditionId = freefundClass.getApplyConditionId();
        this.paymentConditionId = freefundClass.getPaymentConditionId();
        this.datamap = freefundClass.getDatamap();
        this.prefix = freefundClass.getPrefix();
        this.numCodes = freefundClass.getNumCodes();
        this.codeLength = freefundClass.getCodeLength();
        this.redeemConditionId = freefundClass.getRedeemConditionId();
        this.successMessage = freefundClass.getSuccessMessage();
        this.rechargeSuccessMessage = freefundClass.getRechargeSuccessMessage();
        this.rewardId = freefundClass.getRewardId();
        this.arRewardId = freefundClass.getArRewardId();
        this.campaignSponsorship = freefundClass.getCampaignSponsorship();
        this.campaignDescription = freefundClass.getCampaignDescription();
        this.affiliateId = freefundClass.getAffiliateId();
        this.creationLimit = freefundClass.getCreationLimit();
        this.redemptionLimit = freefundClass.getRedemptionLimit();
        this.isNewCampaign = freefundClass.getIsNewCampaign();
    }

    private Integer id;

	private String name;

	private String description;

	private int isActive;
	
	private Integer maxRedemptions;
	
	private Float minRechargeValue;
	
	private Float freefundValue;

	private String promoEntity;

	private Calendar validFrom;

	private Calendar validUpto;

	private Calendar createdAt;

	private Calendar updatedAt;
	
	private String discountType;
	
	private Float maxDiscount;

	private Long applyConditionId;
	
	private Long paymentConditionId;

    private String datamap;

    private String prefix;

    private String numCodes;

    private String codeLength;

    private String redeemConditionId;

    private String successMessage;

    private String rechargeSuccessMessage;

    private String rewardId;
    
    private String arRewardId;
    
    private String campaignSponsorship;
    
    private String campaignDescription;

    private String affiliateId;

    private Integer creationLimit;

    private Integer redemptionLimit;

    private String IVRSClient;

    private String ivrId;

    private String mappingKeys;

    private boolean isNewCampaign;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Integer getMaxRedemptions() {
		return maxRedemptions;
	}

	public void setMaxRedemptions(Integer maxRedemptions) {
		this.maxRedemptions = maxRedemptions;
	}

	public Float getMinRechargeValue() {
        return minRechargeValue;
    }

    public void setMinRechargeValue(Float minRechargeValue) {
        this.minRechargeValue = minRechargeValue;
    }

    public Float getFreefundValue() {
        return freefundValue;
    }

    public void setFreefundValue(Float freefundValue) {
        this.freefundValue = freefundValue;
    }

    public String getPromoEntity() {
        return promoEntity;
    }

    public void setPromoEntity(String promoEntity) {
        this.promoEntity = promoEntity;
    }

    public Calendar getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Calendar validFrom) {
		this.validFrom = validFrom;
	}

	public Calendar getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(Calendar validUpto) {
		this.validUpto = validUpto;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}

	public Calendar getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Calendar updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Float getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Float maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public Long getApplyConditionId() {
		return applyConditionId;
	}

	public void setApplyConditionId(Long applyConditionId) {
		this.applyConditionId = applyConditionId;
	}

	public Long getPaymentConditionId() {
		return paymentConditionId;
	}

	public void setPaymentConditionId(Long paymentConditionId) {
		this.paymentConditionId = paymentConditionId;
	}

    public String getDatamap() {
        return datamap;
    }

    public void setDatamap(String datamap) {
        this.datamap = datamap;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getNumCodes() {
        return numCodes;
    }

    public void setNumCodes(String numCodes) {
        this.numCodes = numCodes;
    }

    public String getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(String codeLength) {
        this.codeLength = codeLength;
    }

    public String getRedeemConditionId() {
        return redeemConditionId;
    }

    public void setRedeemConditionId(String redeemConditionId) {
        this.redeemConditionId = redeemConditionId;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getRechargeSuccessMessage() {
        return rechargeSuccessMessage;
    }

    public void setRechargeSuccessMessage(String rechargeSuccessMessage) {
        this.rechargeSuccessMessage = rechargeSuccessMessage;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getArRewardId() {
        return arRewardId;
    }

    public void setArRewardId(String arRewardId) {
        this.arRewardId = arRewardId;
    }

    public String getCampaignSponsorship() {
        return campaignSponsorship;
    }

    public void setCampaignSponsorship(String campaignSponsorship) {
        this.campaignSponsorship = campaignSponsorship;
    }

    public String getCampaignDescription() {
        return campaignDescription;
    }

    public void setCampaignDescription(String campaignDescription) {
        this.campaignDescription = campaignDescription;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public Integer getCreationLimit() {
        return creationLimit;
    }

    public void setCreationLimit(Integer creationLimit) {
        this.creationLimit = creationLimit;
    }

    public Integer getRedemptionLimit() {
        return redemptionLimit;
    }

    public void setRedemptionLimit(Integer redemptionLimit) {
        this.redemptionLimit = redemptionLimit;
    }

    public String getMappingKeys() {
        return mappingKeys;
    }

    public void setMappingKeys(String mappingKeys) {
        this.mappingKeys = mappingKeys;
    }

    public void getDatamapComponents() {
        String dataMapString = this.getDatamap();
        if (dataMapString != null) {
            Map dataMap = FCStringUtils.jsonToMap(this.getDatamap());
            this.setNumCodes((String) dataMap.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME));
            this.setCodeLength((String) dataMap.get(FCConstants.LENGTH_PARAM_NAME));
            this.setPrefix((String) dataMap.get(FCConstants.PREFIX_PARAM_NAME));
            this.setRedeemConditionId((String) dataMap.get("redeemConditionId"));
            this.setSuccessMessage((String) dataMap.get(FCConstants.SUCCESS_MESSAGE_PARAM_NAME));
            this.setRechargeSuccessMessage((String) dataMap.get(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME));
            this.setRewardId((String) dataMap.get(FCConstants.REWARD_ID));
            this.setArRewardId((String) dataMap.get(FCConstants.AR_REWARD_ID));
            this.setIvrId((String) dataMap.get(FCConstants.IVR_ID));
            this.setIVRSClient((String) dataMap.get(FCConstants.IVRSClient));
            this.setMappingKeys((String) dataMap.get(FCConstants.MAPPING_KEYS));
        }
    }

    public void setIvrId(String ivrId) {
        this.ivrId = ivrId;
    }

    public String getIvrId() {
        return ivrId;
    }

    public String getIVRSClient() {
        return IVRSClient;
    }

    public void setIVRSClient(String IVRSClient) {
        this.IVRSClient = IVRSClient;
    }

    @Override
    public Map<String, Object> toMap() {
        Field[] fields = getClass().getDeclaredFields();
        Map<String, Object> result = new LinkedHashMap<>();
        try {
            for (Field each: fields) {
                each.setAccessible(true);
                Object val = each.get(this);
                result.put(each.getName(), val);
            }
            return result;
        } catch (IllegalAccessException e) {
            logger.error("Error while converting freefund_class object to map. ", e);
            throw new RuntimeException(e);
        }
    }

    public boolean getIsNewCampaign() {
        return isNewCampaign;
    }

    public void setIsNewCampaign(boolean isNewCampaign) {
        this.isNewCampaign = isNewCampaign;
    }
}