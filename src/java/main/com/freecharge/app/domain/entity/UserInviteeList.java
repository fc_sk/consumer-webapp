package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * UserInviteeList entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user_invitee_list")
public class UserInviteeList implements java.io.Serializable {

	// Fields

	private Integer userRechargeeListId;
	private Integer fkUserId;
	private String firstName;
	private String lastName;
	private String nickName;
	private Timestamp createdDate;
	private Boolean isInvited;
	private Timestamp invitationAccepted;
	private Timestamp inviationDate;
	private String invitationSource;
	private String invitationCode;

	// Constructors

	/** default constructor */
	public UserInviteeList() {
	}

	/** minimal constructor */
	public UserInviteeList(Integer fkUserId, Timestamp createdDate) {
		this.fkUserId = fkUserId;
		this.createdDate = createdDate;
	}

	/** full constructor */
	public UserInviteeList(Integer fkUserId, String firstName, String lastName,
			String nickName, Timestamp createdDate, Boolean isInvited,
			Timestamp invitationAccepted, Timestamp inviationDate,
			String invitationSource, String invitationCode) {
		this.fkUserId = fkUserId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
		this.createdDate = createdDate;
		this.isInvited = isInvited;
		this.invitationAccepted = invitationAccepted;
		this.inviationDate = inviationDate;
		this.invitationSource = invitationSource;
		this.invitationCode = invitationCode;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_rechargee_list_id", unique = true, nullable = false)
	public Integer getUserRechargeeListId() {
		return this.userRechargeeListId;
	}

	public void setUserRechargeeListId(Integer userRechargeeListId) {
		this.userRechargeeListId = userRechargeeListId;
	}

	@Column(name = "fk_user_id", nullable = false)
	public Integer getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Column(name = "first_name", length = 64)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", length = 64)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "nick_name", length = 64)
	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "created_date", nullable = false, length = 0)
	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "is_invited")
	public Boolean getIsInvited() {
		return this.isInvited;
	}

	public void setIsInvited(Boolean isInvited) {
		this.isInvited = isInvited;
	}

	@Column(name = "invitation_accepted", length = 0)
	public Timestamp getInvitationAccepted() {
		return this.invitationAccepted;
	}

	public void setInvitationAccepted(Timestamp invitationAccepted) {
		this.invitationAccepted = invitationAccepted;
	}

	@Column(name = "inviation_date", length = 0)
	public Timestamp getInviationDate() {
		return this.inviationDate;
	}

	public void setInviationDate(Timestamp inviationDate) {
		this.inviationDate = inviationDate;
	}

	@Column(name = "invitation_source")
	public String getInvitationSource() {
		return this.invitationSource;
	}

	public void setInvitationSource(String invitationSource) {
		this.invitationSource = invitationSource;
	}

	@Column(name = "invitation_code", length = 64)
	public String getInvitationCode() {
		return this.invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

}