package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.common.util.InTransactionsForStatistics;


public class InRequestRowMapper implements RowMapper<InTransactionsForStatistics>{

	@Override
	public InTransactionsForStatistics mapRow(ResultSet rs, int arg1)	throws SQLException {
		InTransactionsForStatistics inTransactionsForStatistics = new InTransactionsForStatistics();
		inTransactionsForStatistics.setAmount(new Double(rs.getFloat("amount")));
		inTransactionsForStatistics.setProductType(rs.getString("product_type"));
		inTransactionsForStatistics.setServiceNumber(rs.getString("subscriber_number"));
		inTransactionsForStatistics.setInResponseCode(rs.getString("in_resp_code"));
		return inTransactionsForStatistics; 
	}

}
