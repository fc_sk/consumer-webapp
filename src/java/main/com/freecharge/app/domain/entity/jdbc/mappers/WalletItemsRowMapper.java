package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.wallet.service.WalletItems;

public class WalletItemsRowMapper implements RowMapper<WalletItems> {
	 @Override
	    public WalletItems mapRow(ResultSet rs, int row) throws SQLException {
	    WalletItems walletItems = new WalletItems();
	    walletItems.setWalletItemsId(rs.getInt("wallet_items_id"));
	    walletItems.setfkwalletTransactionId(rs.getInt("fk_wallet_transaction_id"));
	    walletItems.setfkItemId(rs.getInt("fk_item_id"));
	    walletItems.setentityId(rs.getString("entity_id"));
	    walletItems.setAddedOn(rs.getDate("added_on"));
	    return walletItems;
   }
}