package com.freecharge.app.domain.entity.jdbc;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class UtmRedirectionTrackingDetails {

    private String shortCode;
    private int userId;
    private int channelId;
    private String redirectedUrl;
    private String createdAt;

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("utm_short_code", this.shortCode);
        mapSqlParameterSource.addValue("user_id", this.userId);
        mapSqlParameterSource.addValue("channel_id", this.channelId);
        mapSqlParameterSource.addValue("redirected_url", this.redirectedUrl);
        mapSqlParameterSource.addValue("created_at", this.createdAt);
        return mapSqlParameterSource;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getRedirectedUrl() {
        return redirectedUrl;
    }

    public void setRedirectedUrl(String redirectedUrl) {
        this.redirectedUrl = redirectedUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
