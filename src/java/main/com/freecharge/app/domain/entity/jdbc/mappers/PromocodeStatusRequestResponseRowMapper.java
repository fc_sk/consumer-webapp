package com.freecharge.app.domain.entity.jdbc.mappers;


import com.freecharge.promo.entity.PromocodeStatusRequestResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PromocodeStatusRequestResponseRowMapper implements RowMapper{
    @Override
    public PromocodeStatusRequestResponse mapRow(ResultSet resultSet, int i) throws SQLException {
        PromocodeStatusRequestResponse promocodeStatusRequestResponse = new PromocodeStatusRequestResponse();
        promocodeStatusRequestResponse.setRequestId(resultSet.getString("request_id"));
        promocodeStatusRequestResponse.setRequest(resultSet.getString("request"));
        promocodeStatusRequestResponse.setResponse(resultSet.getString("response"));
        return promocodeStatusRequestResponse;
    }
}
