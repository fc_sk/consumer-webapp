package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * CartItems entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cart_items")

public class CartItems  implements java.io.Serializable {
    // Fields    

     private Integer cartItemsId;
     private Integer fkItemId;
     private String entityId;
     private Integer flProductMasterId;
     private String displayLabel;
     private Double price;
     private Timestamp addedOn;
     private Timestamp deletedOn;
     private Boolean isDeleted;
     private Cart cart;
     private Integer quantity;
     private String metadata;


    // Constructors

    

	/** default constructor */
    public CartItems() {
    }


    /** full constructor */
    public CartItems(Integer cartItemsId, Integer fkItemId, String entityId, Integer flProductMasterId, String displayLabel, Double price, Timestamp addedOn, Timestamp deletedOn, Boolean isDeleted ,Integer quantity, String metadata) {
        this.cartItemsId = cartItemsId;
        this.fkItemId = fkItemId;
        this.entityId = entityId;
        this.flProductMasterId = flProductMasterId;
        this.displayLabel = displayLabel;
        this.price = price;
        this.addedOn = addedOn;
        this.deletedOn = deletedOn;
        this.isDeleted = isDeleted;
        this.quantity=quantity;
        this.metadata = metadata;
    }


    // Property accessors
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "cart_items_id", unique = true, nullable = false)
    public Integer getCartItemsId() {
        return this.cartItemsId;
    }

    public void setCartItemsId(Integer cartItemsId) {
        this.cartItemsId = cartItemsId;
    }

    @Column(name="fk_item_id")

    public Integer getFkItemId() {
        return this.fkItemId;
    }

    public void setFkItemId(Integer fkItemId) {
        this.fkItemId = fkItemId;
    }

    @Column(name="entity_id")

    public String getEntityId() {
        return this.entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @Column(name="fl_product_master_id")

    public Integer getFlProductMasterId() {
        return this.flProductMasterId;
    }

    public void setFlProductMasterId(Integer flProductMasterId) {
        this.flProductMasterId = flProductMasterId;
    }

    @Column(name="display_label")

    public String getDisplayLabel() {
        return this.displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }

    @Column(name="price", precision=10)

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name="added_on", length=19)

    public Timestamp getAddedOn() {
        return this.addedOn;
    }

    public void setAddedOn(Timestamp addedOn) {
        this.addedOn = addedOn;
    }

    @Column(name="deleted_on", length=19)

    public Timestamp getDeletedOn() {
        return this.deletedOn;
    }

    public void setDeletedOn(Timestamp deletedOn) {
        this.deletedOn = deletedOn;
    }

    @Column(name="is_deleted")

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }


    @JoinColumn(name = "fk_cart_id", referencedColumnName = "cart_id")
	@ManyToOne(optional = false)
	public Cart getCart() {
		return this.cart;
	}


	public void setCart(Cart cart) {
		this.cart = cart;
	}
    
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "metadata")
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
}