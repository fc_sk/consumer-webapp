package com.freecharge.app.domain.entity.jdbc.mappers;


import com.freecharge.freefund.dos.entity.PromocodeRedeemedDetails;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PromocodeRedeemDetailsRowMapper implements RowMapper{
    @Override
    public PromocodeRedeemedDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        PromocodeRedeemedDetails promocodeRedeemedDetails = new PromocodeRedeemedDetails();
        promocodeRedeemedDetails.setOrderId(resultSet.getString("order_id"));
        promocodeRedeemedDetails.setFreefundClassId(resultSet.getLong("campaign_id"));
        promocodeRedeemedDetails.setImeiNumber(resultSet.getString("imei_number"));
        promocodeRedeemedDetails.setAdvertisementId(resultSet.getString("advertisement_id"));
        promocodeRedeemedDetails.setDeviceUniqueId(resultSet.getString("windows_device_unique_id"));
        promocodeRedeemedDetails.setFreefundCode(resultSet.getString("freefund_code"));
        promocodeRedeemedDetails.setFreefundValue(resultSet.getFloat("freefund_value"));
        promocodeRedeemedDetails.setCouponOpted(resultSet.getInt("is_coupon_opted"));
        promocodeRedeemedDetails.setChannelId(resultSet.getInt("channel_id"));
        promocodeRedeemedDetails.setCircleName(resultSet.getString("circle_name"));
        promocodeRedeemedDetails.setName(resultSet.getString("campaign_name"));
        promocodeRedeemedDetails.setUserId(resultSet.getInt("user_id"));
        promocodeRedeemedDetails.setServiceNumber(resultSet.getString("service_number"));
        promocodeRedeemedDetails.setProductName(resultSet.getString("product_type"));
        promocodeRedeemedDetails.setPromoEntity(resultSet.getString("promo_entity"));
        promocodeRedeemedDetails.setOperatorName(resultSet.getString("operator_name"));
        promocodeRedeemedDetails.setDiscountType(resultSet.getString("discount_type"));
        promocodeRedeemedDetails.setDiscountValue(resultSet.getDouble("discount_value"));
        return promocodeRedeemedDetails;
    }
}
