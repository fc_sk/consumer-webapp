package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 22/10/12 Time: 10:15 AM To change this template use File
 * | Settings | File Templates.
 */
public class OrderFulfillmentDetails implements DBEntity {

	@ColumnMapping(name = "id")
	private Integer id;

	@ColumnMapping(name = "fk_txn_fulfillment_id")
	private String fkTxnFulfillmentId;

	@ColumnMapping(name = "order_id")
	private String orderId;

	@ColumnMapping(name = "daily_seq_id")
	private String dailySeqId;

	@ColumnMapping(name = "courier_master_id")
	private Float courierMasterId;

	@ColumnMapping(name = "awb_num")
	private String awbNum;

	@ColumnMapping(name = "fulfillment_status")
	private String fulfillmentStatus;

	@ColumnMapping(name = "courier_remarks")
	private String courierRemarks;

	@ColumnMapping(name = "date_added")
	private String dateAdded;

	@ColumnMapping(name = "date_printed")
	private Date datePrinted;

	@ColumnMapping(name = "date_sent")
	private Integer dateSent;

	@ColumnMapping(name = "date_delivered")
	private Integer dateDelivered;

	@ColumnMapping(name = "date_updated")
	private Integer dateUpdated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public String getFkTxnFulfillmentId() {
		return fkTxnFulfillmentId;
	}

	public void setFkTxnFulfillmentId(String fkTxnFulfillmentId) {
		this.fkTxnFulfillmentId = fkTxnFulfillmentId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDailySeqId() {
		return dailySeqId;
	}

	public void setDailySeqId(String dailySeqId) {
		this.dailySeqId = dailySeqId;
	}

	public Float getCourierMasterId() {
		return courierMasterId;
	}

	public void setCourierMasterId(Float courierMasterId) {
		this.courierMasterId = courierMasterId;
	}

	public String getAwbNum() {
		return awbNum;
	}

	public void setAwbNum(String awbNum) {
		this.awbNum = awbNum;
	}

	public String getFulfillmentStatus() {
		return fulfillmentStatus;
	}

	public void setFulfillmentStatus(String fulfillmentStatus) {
		this.fulfillmentStatus = fulfillmentStatus;
	}

	public String getCourierRemarks() {
		return courierRemarks;
	}

	public void setCourierRemarks(String courierRemarks) {
		this.courierRemarks = courierRemarks;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getDatePrinted() {
		return datePrinted;
	}

	public void setDatePrinted(Date datePrinted) {
		this.datePrinted = datePrinted;
	}

	public Integer getDateSent() {
		return dateSent;
	}

	public void setDateSent(Integer dateSent) {
		this.dateSent = dateSent;
	}

	public Integer getDateDelivered() {
		return dateDelivered;
	}

	public void setDateDelivered(Integer dateDelivered) {
		this.dateDelivered = dateDelivered;
	}

	public Integer getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Integer dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	@Override
	public String toString() {
		return "OrderFullfillmentDetails{" + "id="
				+ id
					+ ", orderId='"
					+ orderId
					+ '\''
					+ ", dailySeqId='"
					+ dailySeqId
					+ '\''
					+ ", courierMasterId='"
					+ courierMasterId
					+ '\''
					+ ", awbNum="
					+ awbNum
					+ ", fulfillmentStatus='"
					+ fulfillmentStatus
					+ '\''
					+ ", courierRemarks='"
					+ courierRemarks
					+ '\''
					+ ", dateAdded='"
					+ dateAdded
					+ '\''
					+ ", dateDelivered="
					+ dateDelivered
					+ ", dateSent="
					+ dateSent
					+ ", dateUpdated="
					+ dateUpdated
					+ ", datePrinted="
					+ datePrinted
					+ '}';
	}

	@Override
	public String getTableName() {
		return "order_fulfillment_details";
	}

	@Override
	public UpdateDBVO getUpdateDBVO() {
		return null; // To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public SelectDBVO getSelectDBVO() {
		SelectQuery selectQuery = new SelectQuery(this);
		return selectQuery.getSelectDBVO();
	}

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
				"applicationContext.xml", "web-delegates.xml");
		EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));
		OrderFulfillmentDetails orderFulfillmentDetails = new OrderFulfillmentDetails();
		orderFulfillmentDetails.setOrderId("FCVW1209060000029");
		List<OrderFulfillmentDetails> transactionHomePages = easyDBWriteDAO.get(orderFulfillmentDetails);
		System.out.println(transactionHomePages);
		ctx.close();
	}

}
