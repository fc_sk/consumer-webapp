package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class InRequest implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long requestId;
	private Integer affiliateId;
	private String affiliateTransId;
	private String productType;
	private String subscriberNumber;
	private String operator;
	private String circle;
	private Float amount;
	private Timestamp createdTime;
	private Timestamp updatedTime;
	private String rawRequest;
	private String rechargePlan;

	public Long getRequestId() {
		return this.requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Integer getAffiliateId() {
		return this.affiliateId;
	}

	public void setAffiliateId(Integer affiliateId) {
		this.affiliateId = affiliateId;
	}

	public String getAffiliateTransId() {
		return this.affiliateTransId;
	}

	public void setAffiliateTransId(String affiliateTransId) {
		this.affiliateTransId = affiliateTransId;
	}

	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSubscriberNumber() {
		return this.subscriberNumber;
	}

	public void setSubscriberNumber(String subscriberNumber) {
		this.subscriberNumber = subscriberNumber;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCircle() {
		return this.circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public Float getAmount() {
		return this.amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public Timestamp getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getRawRequest() {
		return this.rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	@Override
	public boolean equals(Object obj) {		
		return this.getRequestId().equals(((InRequest)obj).getRequestId());
	}
	
	@Override
	public int hashCode() {		
		return this.getRequestId().hashCode();
	}
	
	public String getRechargePlan() {
		return rechargePlan;
	}

	public void setRechargePlan(String rechargePlan) {
		this.rechargePlan = rechargePlan;
	}

    public static Set<String> extractOrderIds(List<InRequest> inRequests) {
        Set<String> orderIds = new HashSet<String>();

        for (InRequest inRequest : inRequests) {
            orderIds.add(inRequest.getAffiliateTransId());
        }

        return orderIds;
    }

    @Override
    public String toString() {
        return "InRequest{" +
                "requestId=" + requestId +
                ", affiliateId=" + affiliateId +
                ", affiliateTransId='" + affiliateTransId + '\'' +
                ", productType='" + productType + '\'' +
                ", subscriberNumber='" + subscriberNumber + '\'' +
                ", operator='" + operator + '\'' +
                ", circle='" + circle + '\'' +
                ", amount=" + amount +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", rawRequest='" + rawRequest + '\'' +
                '}';
    }
}