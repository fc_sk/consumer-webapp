package com.freecharge.app.domain.entity.winpin;



import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CpnTransLogger entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cpn_trans_logger")
public class CpnTransLogger implements java.io.Serializable {

	// Fields

	private Long ctlId;
	private CouponPin couponPin;
	private String cfActionStatus;
	private Date cfActiveDate;
	private Boolean cfActiveFlag;
	private Boolean cfArchiveStatus;
	private Long cfCreatedById;
	private Timestamp cfDateCreatedOn;
	private Timestamp cfDateLastUpdatedOn;
	private Long cfLastUpdatedById;
	private String orderId;
	private String pgStatus;
	private String rechargeStatus;

	// Constructors

	/** default constructor */
	public CpnTransLogger() {
	}

	/** full constructor */
	public CpnTransLogger(CouponPin couponPin, String cfActionStatus,
			Date cfActiveDate, Boolean cfActiveFlag, Boolean cfArchiveStatus,
			Long cfCreatedById, Timestamp cfDateCreatedOn,
			Timestamp cfDateLastUpdatedOn, Long cfLastUpdatedById,
			String orderId, String pgStatus, String rechargeStatus) {
		this.couponPin = couponPin;
		this.cfActionStatus = cfActionStatus;
		this.cfActiveDate = cfActiveDate;
		this.cfActiveFlag = cfActiveFlag;
		this.cfArchiveStatus = cfArchiveStatus;
		this.cfCreatedById = cfCreatedById;
		this.cfDateCreatedOn = cfDateCreatedOn;
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
		this.cfLastUpdatedById = cfLastUpdatedById;
		this.orderId = orderId;
		this.pgStatus = pgStatus;
		this.rechargeStatus = rechargeStatus;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CTL_ID", unique = true, nullable = false)
	public Long getCtlId() {
		return this.ctlId;
	}

	public void setCtlId(Long ctlId) {
		this.ctlId = ctlId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CPN_ID")
	public CouponPin getCouponPin() {
		return this.couponPin;
	}

	public void setCouponPin(CouponPin couponPin) {
		this.couponPin = couponPin;
	}

	@Column(name = "CF_ACTION_STATUS", length = 100)
	public String getCfActionStatus() {
		return this.cfActionStatus;
	}

	public void setCfActionStatus(String cfActionStatus) {
		this.cfActionStatus = cfActionStatus;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CF_ACTIVE_DATE", length = 0)
	public Date getCfActiveDate() {
		return this.cfActiveDate;
	}

	public void setCfActiveDate(Date cfActiveDate) {
		this.cfActiveDate = cfActiveDate;
	}

	@Column(name = "CF_ACTIVE_FLAG")
	public Boolean getCfActiveFlag() {
		return this.cfActiveFlag;
	}

	public void setCfActiveFlag(Boolean cfActiveFlag) {
		this.cfActiveFlag = cfActiveFlag;
	}

	@Column(name = "CF_ARCHIVE_STATUS")
	public Boolean getCfArchiveStatus() {
		return this.cfArchiveStatus;
	}

	public void setCfArchiveStatus(Boolean cfArchiveStatus) {
		this.cfArchiveStatus = cfArchiveStatus;
	}

	@Column(name = "CF_CREATED_BY_ID")
	public Long getCfCreatedById() {
		return this.cfCreatedById;
	}

	public void setCfCreatedById(Long cfCreatedById) {
		this.cfCreatedById = cfCreatedById;
	}

	@Column(name = "CF_DATE_CREATED_ON", length = 0)
	public Timestamp getCfDateCreatedOn() {
		return this.cfDateCreatedOn;
	}

	public void setCfDateCreatedOn(Timestamp cfDateCreatedOn) {
		this.cfDateCreatedOn = cfDateCreatedOn;
	}

	@Column(name = "CF_DATE_LAST_UPDATED_ON", length = 0)
	public Timestamp getCfDateLastUpdatedOn() {
		return this.cfDateLastUpdatedOn;
	}

	public void setCfDateLastUpdatedOn(Timestamp cfDateLastUpdatedOn) {
		this.cfDateLastUpdatedOn = cfDateLastUpdatedOn;
	}

	@Column(name = "CF_LAST_UPDATED_BY_ID")
	public Long getCfLastUpdatedById() {
		return this.cfLastUpdatedById;
	}

	public void setCfLastUpdatedById(Long cfLastUpdatedById) {
		this.cfLastUpdatedById = cfLastUpdatedById;
	}

	@Column(name = "ORDER_ID", length = 100)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "PG_STATUS", length = 20)
	public String getPgStatus() {
		return this.pgStatus;
	}

	public void setPgStatus(String pgStatus) {
		this.pgStatus = pgStatus;
	}

	@Column(name = "RECHARGE_STATUS", length = 20)
	public String getRechargeStatus() {
		return this.rechargeStatus;
	}

	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}

}