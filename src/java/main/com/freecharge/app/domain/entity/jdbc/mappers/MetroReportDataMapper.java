package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.MetroReportDataEntity;

public class MetroReportDataMapper implements RowMapper<MetroReportDataEntity> {

    @Override
    public MetroReportDataEntity mapRow(ResultSet rs, int row) throws SQLException {
    	MetroReportDataEntity metroReportData = new MetroReportDataEntity();
    	metroReportData.setAfcCardNumber(rs.getString("rechargeNo"));
    	metroReportData.setOrderId(rs.getString("orderId"));
    	metroReportData.setPaymentTime(rs.getTimestamp("paymentDate"));
    	metroReportData.setRechargeTime(rs.getTimestamp("rechargeDate"));
    	metroReportData.setRechargeStatus(rs.getString("rechargeStatus"));
    	metroReportData.setPaymentStatus(rs.getString("paymentStatus"));
    	metroReportData.setPaymentType(rs.getInt("paymentType"));
    	metroReportData.setUserId(rs.getInt("userId"));
    	metroReportData.setAmount(rs.getDouble("amount"));
        return metroReportData;
    }
}
