package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.collect.ImmutableList;

/**
 * ProductMaster entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "product_master")
public class ProductMaster implements java.io.Serializable {

    public static enum ProductName {
        Mobile(1, "V"), DTH(2, "D"), DataCard(3, "C"), Coupons(4, "U"), 
        Crosssell(5, "U"), Charges(6, "U"), MasterCard(7, "U"), FreeFund(8, "U"), 
        WalletCash(10, "T"), BinOffer(11, "U"), GoogleCredits(21,"X"),
        // 201-299 is for bill payments
        MobilePostpaid(201, "M"), 
        Bill(311, "B"), Landline(312, "L"), Electricity(313, "E"), Insurance(314, "I"), Gas(315, "G"), Metro(316, "Z"), Broadband(317, "Y"),
        PriceCoupons(15, "P"),HCoupons(16,"H"),RechargeReward(17,"R"), PostPaidDataCard(12, "F"), Water(319, "W"),ConvenienceFee(420, "A");
        ; 
        
        private final int productId;
        private final String productType;


        private ProductName(int productId, String productType) {
            this.productId = productId;
            this.productType = productType;
        }
        
        public static ProductName fromProductId(int productId) {
            switch (productId) {
                case 1: return Mobile;
                case 2: return DTH;
                case 3: return DataCard;
                case 12: return PostPaidDataCard;
                case 4: return Coupons;
                case 5: return Crosssell;
                case 6: return Charges;
                case 7: return MasterCard;
                case 8: return FreeFund;
                case 81: return FreeFund;
                case 9: return FreeFund;
                case 10: return WalletCash;
                case 11: return BinOffer;
                case 15: return PriceCoupons;
                case 17 : return RechargeReward;
                case 201: return MobilePostpaid;
                case 16 : return HCoupons;
                case 311: return Bill;
                case 312: return Landline;
                case 313: return Electricity;
                case 314: return Insurance;
                case 315: return Gas;
                case 316: return Metro;
                case 317: return Broadband;
                case 319: return Water;
                case 21: return GoogleCredits;
                case 420: return ConvenienceFee;
                default: throw new IllegalArgumentException("Unknown product ID : " + productId);
            }
        }
        
        public static ProductName fromPrimaryProductType(String productType) {
            switch (productType) {
                case "V": return Mobile;
                case "D": return DTH;
                case "C": return DataCard;
                case "F": return PostPaidDataCard;
                case "T": return WalletCash;
                case "M": return MobilePostpaid;
                case "B": return Bill;
                case "L": return Landline;
                case "E": return Electricity;
                case "I": return Insurance;
                case "G": return Gas;
                case "Z": return Metro;
                case "P": return PriceCoupons;
                case "H": return HCoupons;
                case "R": return RechargeReward;
                case "Y": return Broadband;
                case "W": return Water;
                case "X": return GoogleCredits;
                default: throw new IllegalArgumentException("Unknown primary product : " + productType);
            }
        }
        
        public int getProductId() {
            return productId;
        }
        
        public String getLabel() {

            switch (this) {
                case Mobile:
                    return "prepaid mobile";

                case DTH:
                    return "DTH";

                case DataCard:
                case PostPaidDataCard:
                    return "datacard";

                case MobilePostpaid:
                    return "postpaid mobile";

                case WalletCash:
                    return "wallet cash";

                case Bill:
                case Landline:
                case Electricity:
                case Insurance:
                case Gas:
                case Metro:
                case Broadband:
                case Water:
                    return "bill payment";
                
                case PriceCoupons:
                case HCoupons:
                    return "Offer";
                
                case RechargeReward:
                    return "Reward";
                    
                case GoogleCredits:
                	return "Google Credits";

                default:
                    throw new IllegalStateException("Unexpected product type found: " + productType);
            }
        }

        public boolean isPrimaryProduct() {
            switch (this) {
                case Mobile:
                case DTH:
                case DataCard:
                case PostPaidDataCard:
                case WalletCash:
                case Bill:
                case Landline:
                case Electricity:
                case Insurance:
                case Gas:
                case Metro:
                case Broadband:
                case Water:
                case MobilePostpaid: 
                case HCoupons:	
                case RechargeReward : 
                case GoogleCredits : return true;
                default: return false;
            }
        }
        
        public String getProductType() {
            /*
             * For now return product type
             * only for primary products.
             */
            
            if (this.isPrimaryProduct()) {
                return this.productType;
            }
            
            throw new IllegalArgumentException("Product type undefined for " + this.toString());
        }

        public boolean isCashBackCampaignOfferApplicable() {
            final List<ProductName> cashBackOfferParticipants = ImmutableList.of(
                    Mobile, DTH, DataCard, PostPaidDataCard
            );

            return cashBackOfferParticipants.contains(this);
        }

        @SuppressWarnings("serial")
        private static final Set<ProductName> BILL_PAYMENT_PRODUCTS = new HashSet<ProductName>() {
            public HashSet<ProductName> init() {
                add(MobilePostpaid);
                return this;
            }
        }.init();

        public boolean isBillPayment() {
            return BILL_PAYMENT_PRODUCTS.contains(this);
        }
        
        public boolean isPrepaid() {
            return this == ProductName.Mobile;
        }
        
        @SuppressWarnings("serial")
        private static final Set<ProductName> UTILITY_PAYMENT_PRODUCTS = new HashSet<ProductName>() {
            public HashSet<ProductName> init() {
                add(Bill);
                add(Landline);
                add(Electricity);
                add(Insurance);
                add(Gas);
                add(Metro);
                add(Broadband);
                add(Water);
                return this;
            }
        }.init();
        
        public boolean isUtilityPaymentProduct() {
            return UTILITY_PAYMENT_PRODUCTS.contains(this);
        }
        
        public static boolean isValidProductName(String prodName) {
        	for (ProductName productName : ProductName.values()) {
        		if ((productName.name()).equals(prodName)) {
    				return true;
    			}
    		}
        	return false;
        }   
        
        public boolean isNumberedProduct(){
        	switch(this) {
	            case Mobile:
	            case DTH:
	            case DataCard: 
	            case PostPaidDataCard:
	            case MobilePostpaid:
	            case GoogleCredits:
	            return true;
	            default: return false;
        	}
        }
        
        public static ProductName fromCode(String productCode) {
            switch(productCode) {
                case "V": return Mobile;
                case "D": return DTH;
                case "C": return DataCard;
                case "F": return PostPaidDataCard;
                case "T": return WalletCash;
                case "M": return MobilePostpaid;
                case "B": return Bill;
                case "L": return Landline;
                case "E": return Electricity;
                case "I": return Insurance;
                case "G": return Gas;
                case "Z": return Metro;
                case "H": return HCoupons;
                case "R": return RechargeReward;
                case "Y": return Broadband;
                case "W": return Water;
                case "X": return GoogleCredits;
                default: throw new IllegalArgumentException("Invalid productCode: [" + String.valueOf(productCode) + "]");
            }
        }
        
        public static final Set<ProductName> getUtilityProducts() {
            return UTILITY_PAYMENT_PRODUCTS;
        }
    }
    
	// Fields

	private Integer productMasterId;
	private String productName;
	private String imgUrl;
	private Integer fkAffiliateProfileId;
	private Boolean isActive;
    private Integer productType;
    private Boolean isRefundable;

	// Constructors

	/** default constructor */
	public ProductMaster() {
	}

    public ProductMaster(Integer productMasterId, String productName, String imgUrl, Integer fkAffiliateProfileId, Boolean isActive, Integer productType, Boolean isRefundable) {
        this.productMasterId = productMasterId;
        this.productName = productName;
        this.imgUrl = imgUrl;
        this.fkAffiliateProfileId = fkAffiliateProfileId;
        this.isActive = isActive;
        this.productType = productType;
        this.isRefundable = isRefundable;
    }

    /** full constructor */


	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "product_master_id", unique = true, nullable = false)
	public Integer getProductMasterId() {
		return this.productMasterId;
	}

	public void setProductMasterId(Integer productMasterId) {
		this.productMasterId = productMasterId;
	}

	@Column(name = "product_name")
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "img_url")
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "fk_affiliate_profile_id")
	public Integer getFkAffiliateProfileId() {
		return this.fkAffiliateProfileId;
	}

	public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
		this.fkAffiliateProfileId = fkAffiliateProfileId;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
    @Column(name = "product_type")
    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
   @Column(name = "is_refundable")
    public Boolean getIsRefundable() {
        return isRefundable;
    }

    public void setIsRefundable(Boolean isRefundable) {
        this.isRefundable = isRefundable;
    }
}