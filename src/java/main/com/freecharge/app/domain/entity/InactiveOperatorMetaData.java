package com.freecharge.app.domain.entity;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.recharge.businessDo.OperatorUpNotificationDo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yashveer on 30/5/16.
 */
public class InactiveOperatorMetaData extends AbstractDO implements Serializable {
    private static final long serialVersionUID = 1L;

    private OperatorAlert operatorAlert;
    private Integer operatorId;
    private Integer circleId;
    private List<OperatorUpNotificationDo> userNotificationList = new ArrayList<>();

    public InactiveOperatorMetaData() {
    }

    public OperatorAlert getOperatorAlert() {
        return operatorAlert;
    }

    public void setOperatorAlert(OperatorAlert operatorAlert) {
        this.operatorAlert = operatorAlert;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public List<OperatorUpNotificationDo> getUserNotificationList() {
        return userNotificationList;
    }

    public void setUserNotificationList(List<OperatorUpNotificationDo> userNotificationList) {
        this.userNotificationList = userNotificationList;
    }
}
