package com.freecharge.app.domain.entity;

import java.io.Serializable;
import java.util.Date;

import com.freecharge.common.framework.basedo.AbstractDO;

public class OperatorAlert extends AbstractDO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer    operatorId;
    private Integer    circleId;
    private String    operatorName;
    private String    circleName;
    private Date    startDateTime;
    private Date    endDateTime;
    private String  alertMessage;
    private boolean isBlocking;
    private Date expiryTime;
    private String operatorCode;

    public OperatorAlert() {

    }

    public OperatorAlert(final Integer operatorId, final Date startDateTime, final Date endDateTime,
            final String alertMessage) {
        super();
        this.operatorId = operatorId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.alertMessage = alertMessage;
        this.isBlocking = false;
    }
    
    public OperatorAlert(final Integer operatorId, final Integer circleId, final Date startDateTime, final Date endDateTime,
            final String alertMessage) {
        super();
        this.operatorId = operatorId;
        this.circleId = circleId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.alertMessage = alertMessage;
        this.isBlocking = false;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(final Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(final Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(final Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(final String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public boolean getIsBlocking() {
        return isBlocking;
    }

    public void setIsBlocking(final boolean isBlocking) {
        this.isBlocking = isBlocking;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
    
    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }
    @Override
    public String toString() {
		return " Operator ID : " + this.operatorId + " Circle ID : " + this.circleId + " StartDateTime : "
				+ this.startDateTime + " EndDateTime : " + this.endDateTime + " AlertMessage : " + this.alertMessage
				+ " isBlocking : " + this.isBlocking;
    }
}
