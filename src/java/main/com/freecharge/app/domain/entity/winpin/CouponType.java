package  com.freecharge.app.domain.entity.winpin;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CouponType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "coupon_type")
public class CouponType implements java.io.Serializable {

	// Fields

	private Integer id;
	private String type;
	private String name;
	private Timestamp addDate;

	// Constructors

	/** default constructor */
	public CouponType() {
	}

	/** minimal constructor */
	public CouponType(Timestamp addDate) {
		this.addDate = addDate;
	}

	/** full constructor */
	public CouponType(String type, String name, Timestamp addDate) {
		this.type = type;
		this.name = name;
		this.addDate = addDate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type", length = 50)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "add_date", nullable = false, length = 0)
	public Timestamp getAddDate() {
		return this.addDate;
	}

	public void setAddDate(Timestamp addDate) {
		this.addDate = addDate;
	}

}