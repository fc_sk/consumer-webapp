package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;

public class RechargeRetryDataMapper implements RowMapper<RechargeRetryData> {

    @Override
    public RechargeRetryData mapRow(ResultSet rs, int row) throws SQLException {
        RechargeRetryData rechargeRetryData = new RechargeRetryData();
        
        rechargeRetryData.setRetryId(rs.getInt("retry_id"));
        rechargeRetryData.setOrderId(rs.getString("order_id"));
        rechargeRetryData.setProductType(rs.getString("product_type"));
        rechargeRetryData.setRetryOperatorName(rs.getString("retry_Operator_Name"));
        rechargeRetryData.setFinalRetry(rs.getBoolean("final_retry"));
        rechargeRetryData.setCreatedTime(rs.getTimestamp("created_at"));
        rechargeRetryData.setUpdatedTime(rs.getTimestamp("updated_at"));
        rechargeRetryData.setAggrTransId(rs.getString("aggr_trans_id"));
        rechargeRetryData.setOprTransId(rs.getString("opr_trans_id"));
        rechargeRetryData.setAggrName(rs.getString("aggr_name"));
        rechargeRetryData.setAggrRespCode(rs.getString("aggr_resp_code"));
        rechargeRetryData.setAggrRespMsg(rs.getString("aggr_resp_msg"));
        rechargeRetryData.setInRespCode(rs.getString("in_resp_code"));
        rechargeRetryData.setInRespMsg(rs.getString("in_resp_msg"));
        return rechargeRetryData;
    }

}
