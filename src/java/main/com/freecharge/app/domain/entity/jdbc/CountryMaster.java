package com.freecharge.app.domain.entity.jdbc;



public class CountryMaster implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer countryMasterId;
	private String countryCode;
	private String printName;
	private String countryName;
	private String iso3;
	private String numcode;
	private Boolean isActive;

	public Integer getCountryMasterId() {
		return this.countryMasterId;
	}

	public void setCountryMasterId(Integer countryMasterId) {
		this.countryMasterId = countryMasterId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPrintName() {
		return this.printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getIso3() {
		return this.iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	public String getNumcode() {
		return this.numcode;
	}

	public void setNumcode(String numcode) {
		this.numcode = numcode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}