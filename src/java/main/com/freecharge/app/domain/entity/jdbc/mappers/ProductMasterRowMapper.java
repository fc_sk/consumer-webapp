package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.ProductMaster;

public class ProductMasterRowMapper implements RowMapper<ProductMaster> {
	@Override
	public ProductMaster mapRow(ResultSet rs, int row) throws SQLException {
		ProductMaster productMaster = new ProductMaster();
		
		productMaster.setProductMasterId(rs.getInt("product_master_id"));
		productMaster.setFkAffiliateProfileId(rs.getInt("fk_affiliate_profile_id"));
		productMaster.setIsActive(rs.getBoolean("is_active"));
		productMaster.setIsRefundable(rs.getBoolean("is_refundable"));
		productMaster.setImgUrl(rs.getString("img_url"));
		productMaster.setProductName(rs.getString("product_name"));
		productMaster.setProductType(rs.getInt("product_type"));
		return productMaster;
	}

}
