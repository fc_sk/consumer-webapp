package com.freecharge.app.domain.entity;

public class AccessInfo {

    private int id;
    private String accessKey;
    private String secretKey;

    public int getId() {
        return id;
    }
    
    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    
}
