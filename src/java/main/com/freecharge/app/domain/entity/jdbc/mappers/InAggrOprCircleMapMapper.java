package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;

public class InAggrOprCircleMapMapper implements RowMapper<InAggrOprCircleMap> {
	@Override
	public InAggrOprCircleMap mapRow(ResultSet rs, int row) throws SQLException {
		InAggrOprCircleMap aggrOprCircleMap = new InAggrOprCircleMap();
		
		aggrOprCircleMap.setId(rs.getInt("id"));
		aggrOprCircleMap.setFkCircleId(rs.getInt("fk_circle_id"));
		aggrOprCircleMap.setFkOperatorId(rs.getInt("fk_operator_id"));
		aggrOprCircleMap.setAggrName(rs.getString("aggr_name"));
		aggrOprCircleMap.setAggrOperatorCode(rs.getString("aggr_operator_code"));
		aggrOprCircleMap.setAggrCircleCode(rs.getString("aggr_circle_code"));
		aggrOprCircleMap.setRechargeplanType(rs.getString("recharge_plan_type"));
		aggrOprCircleMap.setShouldTimeOut(rs.getBoolean("should_timeout"));
		aggrOprCircleMap.setTimeOutInMillis(rs.getInt("time_out_millis"));
		return aggrOprCircleMap;
	}

}
