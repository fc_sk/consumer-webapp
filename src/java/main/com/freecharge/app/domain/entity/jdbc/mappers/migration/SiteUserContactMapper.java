package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.migration.SiteUserContact;

public class SiteUserContactMapper implements RowMapper<SiteUserContact>{

	@Override
	public SiteUserContact mapRow(ResultSet rs, int row) throws SQLException {
		SiteUserContact siteUserContact = new SiteUserContact();
		
		siteUserContact.setId(rs.getInt("id"));
		siteUserContact.setUserId(rs.getInt("user_id"));
		siteUserContact.setName(rs.getString("name"));
		siteUserContact.setMobileNo(rs.getString("mobile_no"));
		siteUserContact.setServiceProvider(rs.getString("service_provider"));
		siteUserContact.setDateAdded(rs.getTimestamp("date_added"));
		siteUserContact.setStatus(rs.getBoolean("status"));
		
		return siteUserContact;
	}
}
