package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.InRequest;

public class InRequestMapper implements RowMapper<InRequest>{

	@Override
	public InRequest mapRow(ResultSet rs, int row) throws SQLException {
		InRequest inRequest = new InRequest();
		
		inRequest.setRequestId(rs.getLong("request_id"));
		inRequest.setAffiliateId(rs.getInt("affiliate_id"));
		inRequest.setAffiliateTransId(rs.getString("affiliate_trans_id"));
		inRequest.setProductType(rs.getString("product_type"));
		inRequest.setSubscriberNumber(rs.getString("subscriber_number"));
		inRequest.setOperator(rs.getString("operator"));
		inRequest.setCircle(rs.getString("circle"));
		inRequest.setAmount(rs.getFloat("amount"));
		inRequest.setCreatedTime(rs.getTimestamp("created_time"));
		inRequest.setUpdatedTime(rs.getTimestamp("updated_time"));
		inRequest.setRawRequest(rs.getString("raw_request"));
		inRequest.setRechargePlan(rs.getString("recharge_plan"));
		
		return inRequest;
	}
}
