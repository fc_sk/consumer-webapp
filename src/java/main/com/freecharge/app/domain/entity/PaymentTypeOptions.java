package com.freecharge.app.domain.entity;

import java.io.Serializable;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * PaymentTypeOptions entity. @author MyEclipse Persistence Tools
 */

public class PaymentTypeOptions implements Serializable, DBEntity {
    // Fields

    @ColumnMapping(name = "payment_type_options_id")
    private Integer paymentTypeOptionsId;
    @ColumnMapping(name = "fk_payment_type_relation_id")
    private Integer fkPaymentTypeRelationId;
    @ColumnMapping(name = "display_name")
    private String displayName;
    @ColumnMapping(name = "display_img")
    private String displayImg;
    @ColumnMapping(name = "img_url")
    private String imgUrl;
    @ColumnMapping(name = "code")
    private String code;
    @ColumnMapping(name = "is_active")
    private Boolean isActive;
    @ColumnMapping(name = "order_by")
    private Integer orderBy;


    // Constructors

    /**
     * default constructor
     */
    public PaymentTypeOptions() {
    }

    /**
     * minimal constructor
     */
    public PaymentTypeOptions(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * full constructor
     */
    public PaymentTypeOptions(Integer fkPaymentTypeRelationId,
                              String displayName, String displayImg, String imgUrl, String code,
                              Boolean isActive) {
        this.fkPaymentTypeRelationId = fkPaymentTypeRelationId;
        this.displayName = displayName;
        this.displayImg = displayImg;
        this.imgUrl = imgUrl;
        this.code = code;
        this.isActive = isActive;
    }

    // Property accessors

    public Integer getPaymentTypeOptionsId() {
        return this.paymentTypeOptionsId;
    }

    public void setPaymentTypeOptionsId(Integer paymentTypeOptionsId) {
        this.paymentTypeOptionsId = paymentTypeOptionsId;
    }


    public Integer getFkPaymentTypeRelationId() {
        return this.fkPaymentTypeRelationId;
    }

    public void setFkPaymentTypeRelationId(Integer fkPaymentTypeRelationId) {
        this.fkPaymentTypeRelationId = fkPaymentTypeRelationId;
    }


    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public String getDisplayImg() {
        return this.displayImg;
    }

    public void setDisplayImg(String displayImg) {
        this.displayImg = displayImg;
    }


    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }


    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public String getTableName() {
        return "payment_type_options";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        return null;
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    @Override
    public String toString() {
        return "PaymentTypeOptions{" +
                "paymentTypeOptionsId=" + paymentTypeOptionsId +
                ", fkPaymentTypeRelationId=" + fkPaymentTypeRelationId +
                ", displayName='" + displayName + '\'' +
                ", displayImg='" + displayImg + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", code='" + code + '\'' +
                ", isActive=" + isActive +
                ", orderBy=" + orderBy +
                '}';
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));

        PaymentTypeOptions paymentTypeOptions = new PaymentTypeOptions();
        paymentTypeOptions.setCode("MC");

        System.out.println(easyDBWriteDAO.get(paymentTypeOptions));
    }
}