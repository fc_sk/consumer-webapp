package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * AffiliateProfile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "affiliate_profile")
public class AffiliateProfile implements java.io.Serializable {

	// Fields

	private Integer id;
	private String affliateUniqueId;
	private String company;                
	private String contactName;
	private String email;
	private String phone;
	private String ext;
	private String fax;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String siteName;
	private Timestamp createdDate;
	private Integer userId;

	// Constructors

	/** default constructor */
	public AffiliateProfile() {
		
	}

	/** minimal constructor */
	public AffiliateProfile(String affliateUniqueId, String company,
			String contactName, String email, String phone, String fax,
			String address1, String city, String state, String postalCode,
			String siteName, Timestamp createdDate) {
		this.affliateUniqueId = affliateUniqueId;
		this.company = company;
		this.contactName = contactName;
		this.email = email;
		this.phone = phone;
		this.fax = fax;
		this.address1 = address1;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.siteName = siteName;
		this.createdDate = createdDate;
	}

	/** full constructor */
	public AffiliateProfile(String affliateUniqueId, String company,
			String contactName, String email, String phone, String ext,
			String fax, String address1, String address2, String city,
			String state, String postalCode, String siteName,
			Timestamp createdDate) {
		this.affliateUniqueId = affliateUniqueId;
		this.company = company;
		this.contactName = contactName;
		this.email = email;
		this.phone = phone;
		this.ext = ext;
		this.fax = fax;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.siteName = siteName;
		this.createdDate = createdDate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "affliate_unique_id", nullable = false, length = 36)
	public String getAffliateUniqueId() {
		return this.affliateUniqueId;
	}

	public void setAffliateUniqueId(String affliateUniqueId) {
		this.affliateUniqueId = affliateUniqueId;
	}

	@Column(name = "company", nullable = false)
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "contact_name", nullable = false)
	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	@Column(name = "email", nullable = false)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone", nullable = false, length = 24)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ext", length = 8)
	public String getExt() {
		return this.ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	@Column(name = "fax", nullable = false, length = 24)
	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "address1", nullable = false)
	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@Column(name = "address2")
	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@Column(name = "city", nullable = false, length = 127)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state", nullable = false, length = 127)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "postal_code", nullable = false, length = 16)
	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Column(name = "site_name", nullable = false, length = 127)
	public String getSiteName() {
		return this.siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@Column(name = "created_date", nullable = false, length = 0)
	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "user_id", nullable = false)
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}