package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction.PaymentTransactionSentToPGComparator;

public class RechargeRetryData implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer retryId;
	private String orderId;
	private String productType;
	private String retryOperatorName;
	private String aggrTransId;
	private String oprTransId;
	private String aggrName;
	private String aggrRespCode;
	private String aggrRespMsg;
	private String inRespCode;
	private String inRespMsg;
	private boolean finalRetry;
	private Timestamp createdTime;
	private Timestamp updatedTime;


	public Integer getRetryId() {
		return retryId;
	}

	public void setRetryId(Integer retryId) {
		this.retryId = retryId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getRetryOperatorName() {
		return retryOperatorName;
	}

	public void setRetryOperatorName(String retryOperatorName) {
		this.retryOperatorName = retryOperatorName;
	}
	
	public String getAggrTransId() {
		return aggrTransId;
	}

	public void setAggrTransId(String aggrTransId) {
		this.aggrTransId = aggrTransId;
	}

	public String getOprTransId() {
		return oprTransId;
	}

	public void setOprTransId(String oprTransId) {
		this.oprTransId = oprTransId;
	}

	public String getAggrName() {
		return aggrName;
	}

	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getAggrRespCode() {
		return aggrRespCode;
	}

	public void setAggrRespCode(String aggrRespCode) {
		this.aggrRespCode = aggrRespCode;
	}

	public String getAggrRespMsg() {
		return aggrRespMsg;
	}

	public void setAggrRespMsg(String aggrRespMsg) {
		this.aggrRespMsg = aggrRespMsg;
	}

	public String getInRespCode() {
		return inRespCode;
	}

	public void setInRespCode(String inRespCode) {
		this.inRespCode = inRespCode;
	}

	public String getInRespMsg() {
		return inRespMsg;
	}

	public void setInRespMsg(String inRespMsg) {
		this.inRespMsg = inRespMsg;
	}


	public boolean isFinalRetry() {
		return finalRetry;
	}

	public void setFinalRetry(boolean finalRetry) {
		this.finalRetry = finalRetry;
	}

	public Timestamp getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public Timestamp getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	public static class RechargeRetryCreatedTimeComparator implements Comparator<RechargeRetryData> {
        @Override
        public int compare(RechargeRetryData rechargeRetry0, RechargeRetryData rechargeRetry1) {
            return rechargeRetry1.getCreatedTime().compareTo(rechargeRetry0.getCreatedTime());
        }

    }
	
	@Override
	public boolean equals(Object obj) {
		return this.getRetryId().equals(((RechargeRetryData)obj).getRetryId());
	}
	
	@Override
	public int hashCode() {
		return this.getRetryId().hashCode();
	}

   
}