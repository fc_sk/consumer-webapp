package com.freecharge.app.domain.entity.jdbc;

import java.sql.Date;

import com.freecharge.common.framework.basedo.AbstractDO;

public class RechargeDetails extends AbstractDO {

	private String productName;
	private String operatorName;
	private String operatorImageURL;
	private String circleName;
	private String circleId;
	private String amount;
	private String serviceNumber;
	private Date rechargedOn;
	private String operatorMasterId;
	private String rechargeType;
	private String rechargeStatus;
	private String refundStatus;
	private boolean isRefundable;
	private Double refundAmount;
	private String billingCycle;
	private String accountNumber;
	private String transactionStatusCode;
	private String userMobileNumber;
	private String userEmailId;
	private String userName;
	private boolean isInvoice = false;
	private String txnType;

	public String getOperatorMasterId() {
		return operatorMasterId;
	}

	public void setOperatorMasterId(String operatorMasterId) {
		this.operatorMasterId = operatorMasterId;
	}

	private String orderId;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperatorImageURL() {
		return operatorImageURL;
	}

	public void setOperatorImageURL(String operatorImageURL) {
		this.operatorImageURL = operatorImageURL;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getCircleId() {
		return circleId;
	}

	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public Date getRechargedOn() {
		return rechargedOn;
	}

	public void setRechargedOn(Date rechargedOn) {
		this.rechargedOn = rechargedOn;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeStatus() {
		return rechargeStatus;
	}

	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

  public boolean isRefundable() {
      return isRefundable;
  }

  public void setRefundable(boolean isRefundable) {
      this.isRefundable = isRefundable;
  }

  public String getBillingCycle() {
      return billingCycle;
  }

  public void setBillingCycle(String billingCycle) {
      this.billingCycle = billingCycle;
  }

  public String getAccountNumber() {
      return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
      this.accountNumber = accountNumber;
  }

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    public void setTransactionStatusCode(String transactionStatusCode) {
        this.transactionStatusCode = transactionStatusCode;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

	public boolean isInvoice() {
		return isInvoice;
	}

	public void setInvoice(boolean isInvoice) {
		this.isInvoice = isInvoice;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
}
