package com.freecharge.app.domain.entity;

import java.util.Date;

public class CustomRechargePlanTxnTracker {
	private long id;
	private String lookupId;
	private String fulfillmentIdf;
	private double amount;
	private Date createdOn;
	private Date updatedOn;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public String getFulfillmentIdf() {
		return fulfillmentIdf;
	}
	public void setFulfillmentIdf(String fulfillmentIdf) {
		this.fulfillmentIdf = fulfillmentIdf;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
