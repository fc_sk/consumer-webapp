package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CountryMaster entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "country_master")
public class CountryMaster implements java.io.Serializable {

	// Fields

	private Integer countryMasterId;
	private String countryCode;
	private String printName;
	private String countryName;
	private String iso3;
	private String numcode;
	private Boolean isActive;

	// Constructors

	/** default constructor */
	public CountryMaster() {
	}

	/** minimal constructor */
	public CountryMaster(String countryName, Boolean isActive) {
		this.countryName = countryName;
		this.isActive = isActive;
	}

	/** full constructor */
	public CountryMaster(String countryCode, String printName,
			String countryName, String iso3, String numcode, Boolean isActive) {
		this.countryCode = countryCode;
		this.printName = printName;
		this.countryName = countryName;
		this.iso3 = iso3;
		this.numcode = numcode;
		this.isActive = isActive;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "country_master_id", unique = true, nullable = false)
	public Integer getCountryMasterId() {
		return this.countryMasterId;
	}

	public void setCountryMasterId(Integer countryMasterId) {
		this.countryMasterId = countryMasterId;
	}

	@Column(name = "country_code", length = 5)
	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Column(name = "print_name", length = 25)
	public String getPrintName() {
		return this.printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	@Column(name = "country_name", nullable = false, length = 100)
	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Column(name = "iso3", length = 5)
	public String getIso3() {
		return this.iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	@Column(name = "numcode", length = 5)
	public String getNumcode() {
		return this.numcode;
	}

	public void setNumcode(String numcode) {
		this.numcode = numcode;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}