package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * UserProfile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user_profile")
public class UserProfile implements java.io.Serializable {

	// Fields

	private Integer userProfileId;
	private Integer fkUserId;
	private Integer fkUserRechargeContactId;
	private String nickAlias;
	private String address1;
	private String landmark;
	private Integer fkCityMasterId;
	private String postalCode;
	private Integer fkStateMasterId;
	private Integer fkCountryMasterId;
	private Boolean isActive;
	private Boolean isDefault;
	private Timestamp createdOn;
	private Timestamp lastUpdate;
    private String firstName;
    private String lastName;
    private String title;
    private String area;
    private String street;

    private Users users;


    public UserProfile() {
	}

	/** minimal constructor */
	public UserProfile(Integer cityId, Timestamp lastUpdate) {
        this.fkCityMasterId = cityId;
		this.lastUpdate = lastUpdate;
	}

	/** full constructor */
	public UserProfile(Integer fkUserId, Integer fkUserRechargeContactId,
			String nickAlias, String address1, String landmark, Integer fkCityMasterId,
			String postalCode, Integer fkStateMasterId,
			Integer fkCountryMasterId, Boolean isActive, Boolean isDefault,
			Timestamp createdOn, Timestamp lastUpdate,String title) {
		this.fkUserId = fkUserId;
		this.fkUserRechargeContactId = fkUserRechargeContactId;
		this.nickAlias = nickAlias;
		this.address1 = address1;
		this.landmark = landmark;
		this.fkCityMasterId = fkCityMasterId;
		this.postalCode = postalCode;
		this.fkStateMasterId = fkStateMasterId;
		this.fkCountryMasterId = fkCountryMasterId;
		this.isActive = isActive;
		this.isDefault = isDefault;
		this.createdOn = createdOn;
		this.lastUpdate = lastUpdate;
        this.title=title;
	}

	// Property accessors
    @Column(name = "first_name", length = 128)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


@Column(name = "last_name", length = 128)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_profile_id", unique = true, nullable = false)
	public Integer getUserProfileId() {
		return this.userProfileId;
	}

	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}

	@Column(name = "fk_user_id")
	public Integer getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Column(name = "fk_user_recharge_contact_id")
	public Integer getFkUserRechargeContactId() {
		return this.fkUserRechargeContactId;
	}

	public void setFkUserRechargeContactId(Integer fkUserRechargeContactId) {
		this.fkUserRechargeContactId = fkUserRechargeContactId;
	}

	@Column(name = "nick_alias", length = 64)
	public String getNickAlias() {
		return this.nickAlias;
	}

	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}

	@Column(name = "address1")
	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@Column(name = "landmark")
	public String getLandmark() {
		return this.landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

    @Column(name = "city", nullable = false, length = 100)
    public Integer getFkCityMasterId() {
        return this.fkCityMasterId;
    }

    public void setFkCityMasterId(Integer fkCityMasterId) {
        this.fkCityMasterId = fkCityMasterId;
    }

    @Column(name = "postal_code", length = 50)
	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Column(name = "fk_state_master_id")
	public Integer getFkStateMasterId() {
		return this.fkStateMasterId;
	}

	public void setFkStateMasterId(Integer fkStateMasterId) {
		this.fkStateMasterId = fkStateMasterId;
	}

	@Column(name = "fk_country_master_id")
	public Integer getFkCountryMasterId() {
		return this.fkCountryMasterId;
	}

	public void setFkCountryMasterId(Integer fkCountryMasterId) {
		this.fkCountryMasterId = fkCountryMasterId;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "is_default")
	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	@Column(name = "created_on", length = 0)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "last_update", nullable = false, length = 0)
	public Timestamp getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


    @Column(name = "title")
    public String getTitle() {
         return title;
     }

     public void setTitle(String title) {
         this.title = title;
     }
    @Column(name = "area")  
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
   @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @JoinColumn(name = "fk_user_id", referencedColumnName = "user_id" , insertable = false, updatable = false )
	@ManyToOne(optional = false)
	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

    @Override
    public String toString() {
        return "UserProfile{" +
                "userProfileId=" + userProfileId +
                ", fkUserId=" + fkUserId +
                ", fkUserRechargeContactId=" + fkUserRechargeContactId +
                ", nickAlias='" + nickAlias + '\'' +
                ", address1='" + address1 + '\'' +
                ", landmark='" + landmark + '\'' +
                ", fkCityMasterId=" + fkCityMasterId +
                ", postalCode='" + postalCode + '\'' +
                ", fkStateMasterId=" + fkStateMasterId +
                ", fkCountryMasterId=" + fkCountryMasterId +
                ", isActive=" + isActive +
                ", isDefault=" + isDefault +
                ", createdOn=" + createdOn +
                ", lastUpdate=" + lastUpdate +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", area='" + area + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}