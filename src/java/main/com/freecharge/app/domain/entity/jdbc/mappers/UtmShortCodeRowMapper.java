package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.UtmShortCode;

public class UtmShortCodeRowMapper implements RowMapper{
    @Override
    public UtmShortCode mapRow(ResultSet rs, int row) throws SQLException {
        UtmShortCode utmShortCode = new UtmShortCode();
        utmShortCode.setShortCodeId(rs.getInt("utm_shortcode_id"));
        utmShortCode.setShortCode(rs.getString("utm_short_code"));
        utmShortCode.setCampaignId(rs.getInt("utm_campaign_id"));
        utmShortCode.setStartDate(rs.getDate("start_date"));
        utmShortCode.setEndDate(rs.getDate("end_date"));
        utmShortCode.setWebUrl(rs.getString("web_url"));
        utmShortCode.setMobileWebUrl(rs.getString("mobweb_url"));
        utmShortCode.setAndroidAppUrl(rs.getString("android_app_url"));
        utmShortCode.setIosAppUrl(rs.getString("ios_app_url"));
        utmShortCode.setWindowsAppUrl(rs.getString("windows_app_url"));
        utmShortCode.setCreatedAt(rs.getString("created_at"));
        utmShortCode.setCreatedBy(rs.getString("created_by"));
        utmShortCode.setUpdatedBy(rs.getString("updated_by"));
        utmShortCode.setUpdatedAt(rs.getString("updated_at"));
        return utmShortCode;
    }
}

