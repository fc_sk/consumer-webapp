package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.CircleMaster;


public class CircleMasterRowMapper implements RowMapper<CircleMaster>{
	
	@Override
	public CircleMaster mapRow(ResultSet rs, int row) throws SQLException {
		CircleMaster circleMaster = new CircleMaster();
		
		circleMaster.setCircleMasterId(rs.getInt("circle_master_id"));
		circleMaster.setName(rs.getString("name"));
		circleMaster.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		circleMaster.setIsActive(rs.getBoolean("is_active"));
		
		
		return circleMaster;
	}
	
}
