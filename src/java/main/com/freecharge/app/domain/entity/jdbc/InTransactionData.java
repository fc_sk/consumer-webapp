package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.service.InService;
import com.freecharge.recharge.businessdao.AggregatorInterface;

public class InTransactionData implements Serializable {

	private static final long serialVersionUID = 1L;
	private InRequest inRequest;
	private InResponse inResponse;
	private List<InTransactions> transactions;

	public InRequest getInRequest() {
		return inRequest;
	}

	public void setInRequest(InRequest inRequest) {
		this.inRequest = inRequest;
	}

	public InResponse getInResponse() {
		return inResponse;
	}

	public void setInResponse(InResponse inResponse) {
		this.inResponse = inResponse;
	}

	public List<InTransactions> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<InTransactions> transactions) {
		this.transactions = transactions;
	}

    public static Set<String> getOrderIds(Collection<InTransactionData> inTransactionDatum) {
        Set<String> orderIds = new HashSet<String>(inTransactionDatum.size());
        for (InTransactionData inTransactionData : inTransactionDatum) {
            orderIds.add(inTransactionData.getInRequest().getAffiliateTransId());
        }

        return orderIds;
    }

    public boolean isTransactionSuccessful() {
        if (inResponse == null) {
            return false;
        }

        return AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(inResponse.getInRespCode());
    }
    
    public boolean isTransactionSuccessOrPending() {
        if (inResponse == null) {
            return false;
        }

        return AggregatorInterface.SUCCESS_OR_PENDING_RESPONSES.contains(inResponse.getInRespCode());
    }
    

    public static Collection<InTransactionData> filterOutSuccessfulTransactions(Collection<InTransactionData> inTransactionDatum) {
        List<InTransactionData> failedInTransactionDatum = new LinkedList<InTransactionData>();
        for (InTransactionData inTransactionData : inTransactionDatum) {
            if (inTransactionData.isTransactionSuccessful()) {
                continue ;
            }
            failedInTransactionDatum.add(inTransactionData);
        }
        return  failedInTransactionDatum;
    }

    @Override
    public String toString() {
        return "InTransactionData{" +
                "inRequest=" + inRequest +
                ", inResponse=" + inResponse +
                ", transactions=" + transactions +
                '}';
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        InService inService = ((InService) ctx.getBean("inService"));

        InTransactionData inTransactionData = inService.getInTransactionData("FCVW1209050000015");

        System.out.println(inTransactionData.isTransactionSuccessful());

        ctx.close();

    }
}
