package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.AccessInfo;

public class AccessInfoMapper implements RowMapper<AccessInfo> {

    @Override
    public AccessInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        AccessInfo accessInfo = new AccessInfo();
        accessInfo.setId(rs.getInt("id"));
        accessInfo.setAccessKey(rs.getString("access_key"));
        accessInfo.setSecretKey(rs.getString("secret_key"));
        return accessInfo;
    }

}
