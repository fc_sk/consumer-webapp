package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;

import com.freecharge.common.util.FCUtil;


public class InTransactions implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long transactionId;
	private Long fkRequestId;
	private String aggrName;
	private String aggrResponseCode;
	private String aggrResponseMessage;
	private String aggrReferenceNo;
	private String aggrOprRefernceNo;
	private String rawRequest;
	private String rawResponse;
	private Timestamp requestTime;
	private Timestamp responseTime;
	private String transactionStatus;
	private Long requestProcessTime;
	
	public Long getRequestProcessTime() {
		return requestProcessTime;
	}

	public void setRequestProcessTime(Long requestProcessTime) {
		this.requestProcessTime = requestProcessTime;
	}

	public Long getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getFkRequestId() {
		return this.fkRequestId;
	}

	public void setFkRequestId(Long fkRequestId) {
		this.fkRequestId = fkRequestId;
	}

	public String getAggrName() {
		return this.aggrName;
	}

	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getAggrResponseCode() {
		return this.aggrResponseCode;
	}

	public void setAggrResponseCode(String aggrResponseCode) {
		this.aggrResponseCode = aggrResponseCode;
	}

	public String getAggrResponseMessage() {
		return this.aggrResponseMessage;
	}

	public void setAggrResponseMessage(String aggrResponseMessage) {
		this.aggrResponseMessage = aggrResponseMessage;
	}

	public String getAggrReferenceNo() {
		return this.aggrReferenceNo;
	}

	public void setAggrReferenceNo(String aggrReferenceNo) {
		this.aggrReferenceNo = aggrReferenceNo;
	}

	public String getAggrOprRefernceNo() {
		return this.aggrOprRefernceNo;
	}

	public void setAggrOprRefernceNo(String aggrOprRefernceNo) {
		this.aggrOprRefernceNo = aggrOprRefernceNo;
	}

	public String getRawRequest() {
		return this.rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public String getRawResponse() {
		return this.rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public Timestamp getRequestTime() {
		return this.requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public Timestamp getResponseTime() {
		return this.responseTime;
	}

	public void setResponseTime(Timestamp responseTime) {
		this.responseTime = responseTime;
	}

	public String getTransactionStatus() {
		return this.transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	public String getOperatorCode(){
        return FCUtil.getOperatorCode(this.rawRequest,this.aggrName);
    }
	
	@Override
	public boolean equals(Object obj) {
		return this.getTransactionId().equals(((InTransactions)obj).getTransactionId());
	}
	
	@Override
	public int hashCode() {		
		return this.getTransactionId().hashCode();
	}

    @Override
    public String toString() {
        return "InTransactions{" +
                "transactionId=" + transactionId +
                ", fkRequestId=" + fkRequestId +
                ", aggrName='" + aggrName + '\'' +
                ", aggrResponseCode='" + aggrResponseCode + '\'' +
                ", aggrResponseMessage='" + aggrResponseMessage + '\'' +
                ", aggrReferenceNo='" + aggrReferenceNo + '\'' +
                ", aggrOprRefernceNo='" + aggrOprRefernceNo + '\'' +
                ", rawRequest='" + rawRequest + '\'' +
                ", rawResponse='" + rawResponse + '\'' +
                ", requestTime=" + requestTime +
                ", responseTime=" + responseTime +
                ", transactionStatus='" + transactionStatus + '\'' +
                ", requestProcessTime=" + requestProcessTime +
                '}';
    }
}