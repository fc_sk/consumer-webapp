package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.recharge.businessdao.MISDisplayRecord;

public class MISDisplayRecordRowMapper implements RowMapper<MISDisplayRecord> {

	@Override
	public MISDisplayRecord mapRow(ResultSet rs, int row) throws SQLException {

		MISDisplayRecord adminUsers = new MISDisplayRecord();
		adminUsers.setTxnId(rs.getLong("request_id"));
		adminUsers.setMobileNo(rs.getString("subscriber_number"));
		adminUsers.setProductType(rs.getString("product_type"));
		adminUsers.setAmount(rs.getString("amount"));
		adminUsers.setCircle(rs.getString("circle"));
		adminUsers.setOperator(rs.getString("operator"));
		adminUsers.setAppTxnId(rs.getString("affiliate_trans_id"));
		adminUsers.setGatewayRespCode(rs.getString("aggr_response_code"));
		adminUsers.setGateway(rs.getString("aggr_name"));
		adminUsers.setRespMessage(rs.getString("aggr_response_message"));
		adminUsers.setGatewayRefNo(rs.getString("aggr_reference_no"));
		adminUsers.setOperatorRefNo(rs.getString("aggr_opr_refernce_no"));
		adminUsers.setDate(rs.getTimestamp("request_time"));
		adminUsers.setStatus(rs.getString("transaction_status"));
		adminUsers.setRespCode(rs.getString("in_resp_code"));
		return adminUsers;
	}
}
