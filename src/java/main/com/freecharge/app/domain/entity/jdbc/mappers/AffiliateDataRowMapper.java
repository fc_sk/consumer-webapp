package com.freecharge.app.domain.entity.jdbc.mappers;


import com.freecharge.promo.entity.PromocodeAffiliates;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public  class AffiliateDataRowMapper implements RowMapper {
    @Override
    public PromocodeAffiliates mapRow(ResultSet resultSet, int row) throws SQLException {
        PromocodeAffiliates promocodeAffiliates = new PromocodeAffiliates();
        promocodeAffiliates.setAffiliateId(resultSet.getString("affiliate_id"));
        promocodeAffiliates.setAffiliateName(resultSet.getString("affiliate_name"));
        promocodeAffiliates.setAffiliateType(resultSet.getString("affiliate_type"));
        return promocodeAffiliates;
    }
}
