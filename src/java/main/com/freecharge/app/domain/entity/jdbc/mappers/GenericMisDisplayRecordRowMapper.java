package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.service.GenericMisDisplayRecords;

public class GenericMisDisplayRecordRowMapper implements RowMapper<GenericMisDisplayRecords> {

	@Override
	public GenericMisDisplayRecords mapRow(ResultSet rs, int row) throws SQLException {

		GenericMisDisplayRecords adminUsers = new GenericMisDisplayRecords();
		adminUsers.setInRequestID(rs.getLong("request_id"));
		adminUsers.setPgRequestID(rs.getInt("payment_rq_id"));
		adminUsers.setAmount(rs.getString("amount"));
		adminUsers.setMobileNumber(rs.getString("subscriber_number"));
		adminUsers.setOperator(rs.getString("operator"));
		adminUsers.setCircle(rs.getString("circle"));
		adminUsers.setOperator(rs.getString("operator"));
		adminUsers.setOrderID(rs.getString("affiliate_trans_id"));
		adminUsers.setInAggregator(rs.getString("aggr_name"));
		adminUsers.setInResponseCodeStatus(rs.getString("in_resp_code"));
		adminUsers.setInResponseMessage(rs.getString("in_resp_msg"));
		adminUsers.setPaymentGateway(rs.getString("payment_gateway"));
		adminUsers.setPaymentType(rs.getString("payment_type"));
		adminUsers.setPaymentOption(rs.getString("payment_option"));
		adminUsers.setPgStatusCode(rs.getBoolean("is_successful"));
		adminUsers.setPgMessage(rs.getString("pg_message"));
		adminUsers.setUserEmailD(rs.getString("email"));
		adminUsers.setUserTxnStatus(rs.getBoolean("transaction_status"));
		adminUsers.setRequestDate(rs.getDate("updated_at"));
		return adminUsers;
	}
}
