package com.freecharge.app.domain.entity.jdbc;

import java.sql.Timestamp;


public class InOprAggrPriority implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String aggrPriority;
	private Timestamp updateTime;
	private String updatedBy;
	private Integer fkcircleOperatorMappingId;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAggrPriority() {
		return this.aggrPriority;
	}

	public void setAggrPriority(String aggrPriority) {
		this.aggrPriority = aggrPriority;
	}

	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getFkcircleOperatorMappingId() {
		return fkcircleOperatorMappingId;
	}

	public void setFkcircleOperatorMappingId(Integer fkcircleOperatorMappingId) {
		this.fkcircleOperatorMappingId = fkcircleOperatorMappingId;
	}

	@Override
	public boolean equals(Object obj) {
		return this.getId().equals(((InOprAggrPriority)obj).getId());
	}

	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
}