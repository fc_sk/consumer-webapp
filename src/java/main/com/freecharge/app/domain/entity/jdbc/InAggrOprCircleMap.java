package com.freecharge.app.domain.entity.jdbc;



public class InAggrOprCircleMap implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer fkCircleId;
	private Integer fkOperatorId;
	private String aggrName;
	private String aggrOperatorCode;
	private String aggrCircleCode;
	private String rechargeplanType;
	private Boolean shouldTimeOut;
	private Integer timeOutInMillis;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFkCircleId() {
		return fkCircleId;
	}

	public void setFkCircleId(Integer fkCircleId) {
		this.fkCircleId = fkCircleId;
	}

	public Integer getFkOperatorId() {
		return fkOperatorId;
	}

	public void setFkOperatorId(Integer fkOperatorId) {
		this.fkOperatorId = fkOperatorId;
	}

	public String getAggrName() {
		return this.aggrName;
	}

	public void setAggrName(String aggrName) {
		this.aggrName = aggrName;
	}

	public String getAggrOperatorCode() {
		return this.aggrOperatorCode;
	}

	public void setAggrOperatorCode(String aggrOperatorCode) {
		this.aggrOperatorCode = aggrOperatorCode;
	}

	public String getAggrCircleCode() {
		return this.aggrCircleCode;
	}

	public void setAggrCircleCode(String aggrCircleCode) {
		this.aggrCircleCode = aggrCircleCode;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return this.getId().equals(((InAggrOprCircleMap)arg0).getId());
	}
	
	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
	
	public String getRechargeplanType() {
		return rechargeplanType;
	}

	public void setRechargeplanType(String rechargeplanType) {
		this.rechargeplanType = rechargeplanType;
	}

    public Boolean getShouldTimeOut() {
        return shouldTimeOut;
    }

    public void setShouldTimeOut(Boolean shouldTimeOut) {
        this.shouldTimeOut = shouldTimeOut;
    }

    public Integer getTimeOutInMillis() {
        return timeOutInMillis;
    }

    public void setTimeOutInMillis(Integer timeOutInMillis) {
        this.timeOutInMillis = timeOutInMillis;
    }


}