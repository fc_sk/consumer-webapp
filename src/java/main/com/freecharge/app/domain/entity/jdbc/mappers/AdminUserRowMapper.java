package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.AdminUsers;

public class AdminUserRowMapper implements RowMapper<AdminUsers> {
	

	@Override
	public AdminUsers mapRow(ResultSet rs, int row) throws SQLException {
		AdminUsers adminUsers = new AdminUsers();
		
		adminUsers.setAdminUserID(rs.getInt("admin_user_id"));
		adminUsers.setAdminUserName(rs.getString("admin_user_name"));
		adminUsers.setAdminUserPassword(rs.getString("admin_user_password"));
		adminUsers.setAdminUserActiveStatus(rs.getBoolean("admin_user_active_status"));
		return adminUsers;
	}
}
