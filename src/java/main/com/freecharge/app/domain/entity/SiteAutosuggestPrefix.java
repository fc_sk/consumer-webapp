package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SiteAutosuggestPrefix entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "site_autosuggest_prefix")
public class SiteAutosuggestPrefix implements java.io.Serializable {

	// Fields

	private Integer siteAutosuggestPrefixId;
	private String prefix;
	private Integer fkCircleOperatorMappingId;
	private Integer fkProductMasterId;
	private Boolean isActive;

	// Constructors

	/** default constructor */
	public SiteAutosuggestPrefix() {
	}

	/** full constructor */
	public SiteAutosuggestPrefix(String prefix,
			Integer fkCircleOperatorMappingId, Integer fkProductMasterId,
			Boolean isActive) {
		this.prefix = prefix;
		this.fkCircleOperatorMappingId = fkCircleOperatorMappingId;
		this.fkProductMasterId = fkProductMasterId;
		this.isActive = isActive;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "site_autosuggest_prefix_id", unique = true, nullable = false)
	public Integer getSiteAutosuggestPrefixId() {
		return this.siteAutosuggestPrefixId;
	}

	public void setSiteAutosuggestPrefixId(Integer siteAutosuggestPrefixId) {
		this.siteAutosuggestPrefixId = siteAutosuggestPrefixId;
	}

	@Column(name = "prefix", length = 64)
	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Column(name = "fk_circle_operator_mapping_id")
	public Integer getFkCircleOperatorMappingId() {
		return this.fkCircleOperatorMappingId;
	}

	public void setFkCircleOperatorMappingId(Integer fkCircleOperatorMappingId) {
		this.fkCircleOperatorMappingId = fkCircleOperatorMappingId;
	}

	@Column(name = "fk_product_master_id")
	public Integer getFkProductMasterId() {
		return this.fkProductMasterId;
	}

	public void setFkProductMasterId(Integer fkProductMasterId) {
		this.fkProductMasterId = fkProductMasterId;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}