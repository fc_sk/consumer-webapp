package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.UserTransactionHistory;

public class UserTransactionHistoryMapper implements RowMapper<UserTransactionHistory> {

    @Override
    public UserTransactionHistory mapRow(ResultSet rs, int row) throws SQLException {
        UserTransactionHistory userTransactionHistory = new UserTransactionHistory();
        userTransactionHistory.setTransactionId(rs.getInt("transaction_id"));
        userTransactionHistory.setOrderId(rs.getString("order_id"));
        userTransactionHistory.setFkUserId(rs.getInt("fk_user_id"));
        userTransactionHistory.setTransactionAmount(rs.getDouble("transaction_amount"));
        userTransactionHistory.setSubscriberIdentificationNumber(rs.getString("subscriber_identification_number"));
        userTransactionHistory.setProductType(rs.getString("product_type"));
        userTransactionHistory.setServiceProvider(rs.getString("service_provider"));
        userTransactionHistory.setServiceRegion(rs.getString("service_region"));
        userTransactionHistory.setTransactionStatus(rs.getString("transaction_status"));
        userTransactionHistory.setCreatedOn(rs.getTimestamp("created_on"));
        userTransactionHistory.setLastUpdated(rs.getTimestamp("last_updated"));
        userTransactionHistory.setMetadata(rs.getString("metadata"));
        return userTransactionHistory;
    }
}
