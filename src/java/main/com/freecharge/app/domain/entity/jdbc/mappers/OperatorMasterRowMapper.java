package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.OperatorMaster;

public class OperatorMasterRowMapper implements RowMapper<OperatorMaster> {
	@Override
	public OperatorMaster mapRow(ResultSet rs, int row) throws SQLException {
		OperatorMaster operatorMaster = new OperatorMaster();
		
		operatorMaster.setOperatorMasterId(rs.getInt("operator_master_id"));
		operatorMaster.setFkProductMasterId(rs.getInt("fk_product_master_id"));
		operatorMaster.setFkCountryMasterId(rs.getInt("fk_country_master_id"));
		operatorMaster.setName(rs.getString("name"));
		operatorMaster.setIsActive(rs.getBoolean("is_active"));
		operatorMaster.setOperatorCode(rs.getString("operator_code"));
		operatorMaster.setImgUrl(rs.getString("img_url"));
		operatorMaster.setIsBbpsEnabled(rs.getBoolean("is_bbps_enabled"));
		
		return operatorMaster;
	}

}
