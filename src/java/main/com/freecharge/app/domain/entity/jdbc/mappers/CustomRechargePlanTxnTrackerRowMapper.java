package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;

public class CustomRechargePlanTxnTrackerRowMapper implements RowMapper<CustomRechargePlanTxnTracker>{
	
	public static enum ColumnName{
		ID("id"),LOOKUP_ID("lookup_id"),FULFILLMENT_IDF("fulfillment_idf"),AMOUNT("amount"),
		N_CREATED("n_created"),N_LAST_UPDATED("n_last_updated");
		
		private String value;
		
		private ColumnName(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}

	@Override
	public CustomRechargePlanTxnTracker mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		CustomRechargePlanTxnTracker customRechargePlanTxnTracker = new CustomRechargePlanTxnTracker();
		customRechargePlanTxnTracker.setId(resultSet.getLong(ColumnName.ID.getValue()));
		customRechargePlanTxnTracker.setLookupId(resultSet.getString(ColumnName.LOOKUP_ID.getValue()));
		customRechargePlanTxnTracker.setFulfillmentIdf(resultSet.getString(ColumnName.FULFILLMENT_IDF.getValue()));
		customRechargePlanTxnTracker.setAmount(resultSet.getDouble(ColumnName.AMOUNT.getValue()));
		customRechargePlanTxnTracker.setCreatedOn(resultSet.getDate(ColumnName.N_CREATED.getValue()));
		customRechargePlanTxnTracker.setUpdatedOn(resultSet.getDate(ColumnName.N_LAST_UPDATED.getValue()));
		return customRechargePlanTxnTracker;
	}

}
