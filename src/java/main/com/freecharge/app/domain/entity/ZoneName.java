package com.freecharge.app.domain.entity;

public enum ZoneName {
    All(1), East(2), West(3), North(4), South(5);
    
    private final int zonerMasterId;
    
    private ZoneName(int zoneMasterId) {
        this.zonerMasterId = zoneMasterId;
    }
    
    public static ZoneName fromZoneMasterId(Integer zoneMasterId) {
        switch (zoneMasterId) {
            case 1: return All;
            case 2: return East;
            case 3: return West;
            case 4: return North;
            case 5: return South;
            default: throw new IllegalArgumentException("Unknown zone masterId: " + String.valueOf(zoneMasterId));
        }
    }
}
