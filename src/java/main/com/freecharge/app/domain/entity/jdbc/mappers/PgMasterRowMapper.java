package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PaymentGatewayMaster;

public class PgMasterRowMapper implements RowMapper<PaymentGatewayMaster> {

	@Override
	public PaymentGatewayMaster mapRow(ResultSet rs, int row) throws SQLException {
	
		PaymentGatewayMaster paymentGatewayMaster = new PaymentGatewayMaster();
		paymentGatewayMaster.setActive(rs.getBoolean("is_active"));
		paymentGatewayMaster.setCode(rs.getString("code"));
		paymentGatewayMaster.setCustomProperty1(rs.getString("custom_property_1"));
		paymentGatewayMaster.setCustomProperty2(rs.getString("custom_property_2"));
		paymentGatewayMaster.setCustomProperty3(rs.getString("custom_property_3"));
		paymentGatewayMaster.setName(rs.getString("name"));
		paymentGatewayMaster.setId(rs.getInt("payment_gateway_master_id"));
		return paymentGatewayMaster;
}
}
