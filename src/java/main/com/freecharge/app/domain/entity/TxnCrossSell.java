package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * TxnCrossSell entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="txn_cross_sell")

public class TxnCrossSell  implements java.io.Serializable {


    // Fields    

     private Integer id;
     private Integer txnHomePageId;
     private Integer couponId;
     private Integer corssSellId;
     private Boolean isDeleted;
     private Timestamp createdOn;
     private Integer quantity;

    // Constructors

    /** default constructor */
    public TxnCrossSell() {
    }

	/** minimal constructor */
    public TxnCrossSell(Integer id) {
        this.id = id;
    }



    /** full constructor */
    public TxnCrossSell(Integer id, Integer txnHomePageId, Integer couponId, Integer corssSellId, Boolean isDeleted, Timestamp createdOn , Integer quantity) {
        this.id = id;
        this.txnHomePageId = txnHomePageId;
        this.couponId = couponId;
        this.corssSellId = corssSellId;
        this.isDeleted = isDeleted;
        this.createdOn = createdOn;
        this.quantity=quantity;

    }

   
    // Property accessors
    @Id 
    @GeneratedValue(strategy=IDENTITY)
    @Column(name="id", unique=true, nullable=false)

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(name="txn_home_page_id")

    public Integer getTxnHomePageId() {
        return this.txnHomePageId;
    }
    
    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }
    
    @Column(name="coupon_id")

    public Integer getCouponId() {
        return this.couponId;
    }
    
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }
    
    @Column(name="corss_sell_id")

    public Integer getCorssSellId() {
        return this.corssSellId;
    }
    
    public void setCorssSellId(Integer corssSellId) {
        this.corssSellId = corssSellId;
    }
    
    @Column(name="is_deleted")

    public Boolean getIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    @Column(name="created_on", length=19)

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }
    
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }
     @Column(name="quantity")
     public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }







}