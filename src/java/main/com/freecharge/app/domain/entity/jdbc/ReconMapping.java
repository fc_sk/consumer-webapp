package com.freecharge.app.domain.entity.jdbc;

import java.io.Serializable;
import java.util.Date;

public class ReconMapping implements Serializable{

	private int id;
	private String reconId;
	private String orderId;
	private Date createdOn;
	private Date updatedOn;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReconId() {
		return reconId;
	}
	public void setReconId(String reconId) {
		this.reconId = reconId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
}
