package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.timeline.event.RechargeAttempt;

public class RechargeAttemptMapper implements RowMapper<RechargeAttempt> {

	@Override
	public RechargeAttempt mapRow(ResultSet rs, int row) throws SQLException {
		RechargeAttempt rechargeAttempt = new RechargeAttempt();
		
		rechargeAttempt.setRequestId(rs.getLong("request_id"));
		rechargeAttempt.setAffiliateId(rs.getInt("affiliate_id"));
		rechargeAttempt.setAffiliateTransId(rs.getString("affiliate_trans_id"));
		rechargeAttempt.setProductType(rs.getString("product_type"));
		rechargeAttempt.setSubscriberNumber(rs.getString("subscriber_number"));
		rechargeAttempt.setOperator(rs.getString("operator"));
		rechargeAttempt.setCircle(rs.getString("circle"));
		rechargeAttempt.setAmount(rs.getFloat("amount"));
		rechargeAttempt.setCreatedTime(rs.getTimestamp("created_time"));
		rechargeAttempt.setUpdatedTime(rs.getTimestamp("updated_time"));
		rechargeAttempt.setRawRequest(rs.getString("raw_request"));
		rechargeAttempt.setRechargePlan(rs.getString("recharge_plan"));
		rechargeAttempt.setAggrName(rs.getString("aggr_name"));
		rechargeAttempt.setResponseCode(rs.getString("in_resp_code"));
		rechargeAttempt.setResponseMessage(rs.getString("in_resp_msg"));

		return rechargeAttempt;
	}
}
