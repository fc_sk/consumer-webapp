package com.freecharge.app.domain.entity;

import java.sql.Timestamp;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.framework.basedo.AbstractDO;

public class FailedLogin extends AbstractDO {
    public static final String FAILED_LOGIN_ID             = "id";
    public static final String FAILED_LOGIN_USER_EMAIL     = "username";
    public static final String FAILED_LOGIN_IP_ADDRESS     = "ip_address";
    public static final String FAILED_LOGIN_ATTEMPTED_TIME = "attempted";

    private int                failedLoginId;
    private String             userEmail;
    private String             ipAddress;
    private Timestamp          attemptedTime;

    public void setFailedLoginId(final int failedLoginId) {
        this.failedLoginId = failedLoginId;
    }

    public int getFailedLoginId() {
        return this.failedLoginId;
    }

    public void setUserEmail(final String email) {
        this.userEmail = email;
    }

    public String getUserEmail() {
        return this.userEmail;
    }

    public void setIpAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setAttemptedTime(final Timestamp attemptedTime) {
        this.attemptedTime = attemptedTime;
    }

    public Timestamp getAttemptedTime() {
        return this.attemptedTime;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(FAILED_LOGIN_USER_EMAIL, userEmail);
        mapSqlParameterSource.addValue(FAILED_LOGIN_IP_ADDRESS, ipAddress);
        mapSqlParameterSource.addValue(FAILED_LOGIN_ATTEMPTED_TIME, attemptedTime);
        return mapSqlParameterSource;
    }

    public MapSqlParameterSource getMapSqlParameterSourceForUpdate() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(FAILED_LOGIN_ID, failedLoginId);
        mapSqlParameterSource.addValue(FAILED_LOGIN_USER_EMAIL, userEmail);
        mapSqlParameterSource.addValue(FAILED_LOGIN_IP_ADDRESS, ipAddress);
        mapSqlParameterSource.addValue(FAILED_LOGIN_ATTEMPTED_TIME, attemptedTime);
        return mapSqlParameterSource;
    }
}
