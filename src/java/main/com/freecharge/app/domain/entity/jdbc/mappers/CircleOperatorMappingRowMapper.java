package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;

public class CircleOperatorMappingRowMapper implements RowMapper<CircleOperatorMapping> {
	
	@Override
	public CircleOperatorMapping mapRow(ResultSet rs, int row) throws SQLException {
		CircleOperatorMapping circleOperatorMapping = new CircleOperatorMapping();
		
		circleOperatorMapping.setCircleOperatorMappingId(rs.getInt("circle_operator_mapping_id"));
		circleOperatorMapping.setFkOperatorMasterId(rs.getInt("fk_operator_master_id"));
		circleOperatorMapping.setFkCircleMasterId(rs.getInt("fk_circle_master_id"));
		circleOperatorMapping.setAggrPriority(rs.getString("aggr_priority"));
		
		return circleOperatorMapping;
	}

}
