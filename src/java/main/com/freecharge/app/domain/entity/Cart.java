package com.freecharge.app.domain.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Cart entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "cart")
public class Cart implements java.io.Serializable {

	// Fields

	private Integer cartId;
	private Integer fkOrderId;
	private String lookupId;
	private Integer fkUserId;
	private Integer isOpen;
	private Timestamp createdOn;
	private Timestamp expiresOn;
	private Integer finalStatus;
	private List<CartItems> cartItemsList;

	// Constructors

	/** default constructor */
	public Cart() {
	}

	/** full constructor */
	public Cart(Integer fkOrderId, Integer fkUserId, String lookupId, Integer isOpen, Timestamp createdOn, Timestamp expiresOn, Integer finalStatus) {
		this.fkOrderId = fkOrderId;
		this.fkUserId = fkUserId;
		this.lookupId = lookupId;
		this.isOpen = isOpen;
		this.createdOn = createdOn;
		this.expiresOn = expiresOn;
		this.finalStatus = finalStatus;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "cart_id", unique = true, nullable = false)
	public Integer getCartId() {
		return this.cartId;
	}

	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

	@Column(name = "fk_order_id")
	public Integer getFkOrderId() {
		return this.fkOrderId;
	}

	public void setFkOrderId(Integer fkOrderId) {
		this.fkOrderId = fkOrderId;
	}

	@Column(name = "lookup_id")
	public String getLookupId() {
		return lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	@Column(name = "fk_user_id")
	public Integer getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(Integer fkUserId) {
		this.fkUserId = fkUserId;
	}

	@Column(name = "is_open")
	public Integer getIsOpen() {
		return this.isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	@Column(name = "created_on", length = 19)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "expires_on", length = 19)
	public Timestamp getExpiresOn() {
		return this.expiresOn;
	}

	public void setExpiresOn(Timestamp expiresOn) {
		this.expiresOn = expiresOn;
	}

	@Column(name = "final_status")
	public Integer getFinalStatus() {
		return this.finalStatus;
	}

	public void setFinalStatus(Integer finalStatus) {
		this.finalStatus = finalStatus;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "fk_cart_id")
	public List<CartItems> getCartItemsList() {
		return cartItemsList;
	}

	public void setCartItemsList(List<CartItems> cartItemsList) {
		this.cartItemsList = cartItemsList;
	}
	
	

}