package com.freecharge.app.domain.entity;


import java.io.Serializable;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * PaymentTypeMaster entity. @author MyEclipse Persistence Tools
 */
public class PaymentTypeMaster implements Serializable, DBEntity {
	// Fields

    @ColumnMapping(name = "payment_type_master_id")
	private Integer paymentTypeMasterId;
    @ColumnMapping(name = "name")
	private String name;
    @ColumnMapping(name = "code")
	private String code;
    @ColumnMapping(name = "display_data")
	private String displayData;
	private Integer relationId;

	// Constructors

	/** default constructor */
	public PaymentTypeMaster() {
	}

	/** minimal constructor */
	public PaymentTypeMaster(String name) {
		this.name = name;
	}

	/** full constructor */
	public PaymentTypeMaster(String name, String code, String displayData) {
		this.name = name;
		this.code = code;
		this.displayData = displayData;
	}

	// Property accessors
	public Integer getPaymentTypeMasterId() {
		return this.paymentTypeMasterId;
	}

	public void setPaymentTypeMasterId(Integer paymentTypeMasterId) {
		this.paymentTypeMasterId = paymentTypeMasterId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplayData() {
		return this.displayData;
	}

	public void setDisplayData(String displayData) {
		this.displayData = displayData;
	}

	public Integer getRelationId() {
		return relationId;
	}

	public void setRelationId(Integer relationId) {
		this.relationId = relationId;
	}

    @Override
    public String getTableName() {
        return "payment_type_master";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        return null;
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    @Override
    public String toString() {
        return "PaymentTypeMaster{" +
                "paymentTypeMasterId=" + paymentTypeMasterId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", displayData='" + displayData + '\'' +
                ", relationId=" + relationId +
                '}';
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));
        PaymentTypeMaster paymentTypeMaster = new PaymentTypeMaster();
        paymentTypeMaster.setPaymentTypeMasterId(1);
        System.out.println(easyDBWriteDAO.get(paymentTypeMaster));
        ctx.close();
    }
}