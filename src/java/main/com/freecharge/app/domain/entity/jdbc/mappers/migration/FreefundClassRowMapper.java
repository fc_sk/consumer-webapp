package com.freecharge.app.domain.entity.jdbc.mappers.migration;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Map;

public class FreefundClassRowMapper implements RowMapper<FreefundClass> {
	@Override
	public FreefundClass mapRow(ResultSet rs, int row) throws SQLException {
		FreefundClass freefundClass = new FreefundClass();
		
		freefundClass.setId(rs.getInt("id"));
		freefundClass.setName(rs.getString("name"));
		freefundClass.setDescription(rs.getString("description"));
		freefundClass.setIsActive(rs.getInt("is_active"));
		freefundClass.setMaxRedemptions((Integer) rs.getObject("max_redemptions"));
		freefundClass.setMinRechargeValue(rs.getFloat("min_recharge_value"));
		freefundClass.setFreefundValue(rs.getFloat("freefund_value"));
		freefundClass.setPromoEntity(rs.getString("promo_entity"));
		freefundClass.setDiscountType(rs.getString("discount_type"));
		freefundClass.setMaxDiscount(rs.getFloat("max_discount"));
		freefundClass.setApplyConditionId(rs.getLong("apply_condition_id"));
		freefundClass.setPaymentConditionId(rs.getLong("payment_condition_id"));
		freefundClass.setDatamap(rs.getString("datamap"));
		freefundClass.setCampaignSponsorship(rs.getString("campaign_sponsorship"));
		freefundClass.setCampaignDescription(rs.getString("campaign_description"));
		freefundClass.setAffiliateId(rs.getString("affiliate_id"));
		freefundClass.setCreationLimit((Integer) rs.getObject("creation_limit"));
		freefundClass.setRedemptionLimit((Integer) rs.getObject("redemption_limit"));
        freefundClass.setIsNewCampaign(rs.getBoolean("isNewCampaign"));
		Calendar cal1 = Calendar.getInstance();
		cal1.setTimeInMillis(rs.getTimestamp("created_at").getTime());
		freefundClass.setCreatedAt(cal1);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTimeInMillis(rs.getTimestamp("updated_at").getTime());
		freefundClass.setUpdatedAt(cal2);
		
		Calendar cal3 = Calendar.getInstance();
		cal3.setTimeInMillis(rs.getTimestamp("valid_from").getTime());
		freefundClass.setValidFrom(cal3);
		
		Calendar cal4 = Calendar.getInstance();
		cal4.setTimeInMillis(rs.getTimestamp("valid_upto").getTime());
		freefundClass.setValidUpto(cal4);

		String dataMapString = freefundClass.getDatamap();
		if (dataMapString != null) {
			Map dataMap = FCStringUtils.jsonToMap(dataMapString);
			freefundClass.setNumCodes((String) dataMap.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME));
			freefundClass.setCodeLength((String) dataMap.get(FCConstants.LENGTH_PARAM_NAME));
			freefundClass.setPrefix((String) dataMap.get(FCConstants.PREFIX_PARAM_NAME));
			freefundClass.setRedeemConditionId((String) dataMap.get("redeemConditionId"));
			freefundClass.setSuccessMessage((String) dataMap.get(FCConstants.SUCCESS_MESSAGE_PARAM_NAME));
			freefundClass.setRechargeSuccessMessage((String) dataMap.get(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME));
			freefundClass.setRewardId((String) dataMap.get(FCConstants.REWARD_ID));
			freefundClass.setArRewardId((String) dataMap.get(FCConstants.AR_REWARD_ID));
			freefundClass.setIvrId((String) dataMap.get(FCConstants.IVR_ID));
			freefundClass.setIVRSClient((String) dataMap.get(FCConstants.IVRSClient));
			freefundClass.setMappingKeys((String) dataMap.get(FCConstants.MAPPING_KEYS));
		}

		return freefundClass;
	}

}
