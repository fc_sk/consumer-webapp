package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 22/10/12
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionHomePage implements DBEntity {
    @ColumnMapping(name="id")
    private Integer id;
    @ColumnMapping(name="service_number")
    private String serviceNumber;
    @ColumnMapping(name="operator_name")
    private String operatorName;
    @ColumnMapping(name="circle_name")
    private String circleName;
    @ColumnMapping(name="amount")
    private Float amount;
    @ColumnMapping(name="product_type")
    private String productType;
    @ColumnMapping(name="session_id")
    private String sessionId;
    @ColumnMapping(name="lookup_id")
    private String lookupId;
    @ColumnMapping(name="creatred_on")
    private Date creatredOn;
    @ColumnMapping(name="fk_operator_master_id")
    private Integer fkOperatorId;
    @ColumnMapping(name="fk_circle_master_id")
    private Integer fkCircleId;
    @ColumnMapping(name="fk_product_master_id")
    private Integer fkProductId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public Date getCreatredOn() {
        return creatredOn;
    }

    public void setCreatredOn(Date creatredOn) {
        this.creatredOn = creatredOn;
    }

    public Integer getFkOperatorId() {
        return fkOperatorId;
    }

    public void setFkOperatorId(Integer fkOperatorId) {
        this.fkOperatorId = fkOperatorId;
    }

    public Integer getFkCircleId() {
        return fkCircleId;
    }

    public void setFkCircleId(Integer fkCircleId) {
        this.fkCircleId = fkCircleId;
    }

    public Integer getFkProductId() {
        return fkProductId;
    }

    public void setFkProductId(Integer fkProductId) {
        this.fkProductId = fkProductId;
    }

    @Override
    public String toString() {
        return "TransactionHomePage{" +
                "id=" + id +
                ", serviceNumber='" + serviceNumber + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", circleName='" + circleName + '\'' +
                ", amount=" + amount +
                ", productType='" + productType + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", lookupId='" + lookupId + '\'' +
                ", creatredOn=" + creatredOn +
                ", fkOperatorId=" + fkOperatorId +
                ", fkCircleId=" + fkCircleId +
                ", fkProductId=" + fkProductId +
                '}';
    }

    @Override
    public String getTableName() {
        return "txn_home_page";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));
        TransactionHomePage transactionHomePage = new TransactionHomePage();
        transactionHomePage.setLookupId("f3d2aff6-1d6f-42a0-a368-b48fe5dc2864");
        List<TransactionHomePage> transactionHomePages = easyDBWriteDAO.get(transactionHomePage);
        System.out.println(transactionHomePages);
        ctx.close();
    }

}
