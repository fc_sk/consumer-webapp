package com.freecharge.app.domain.entity.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.dos.entity.PaymentTransaction;

public class PaymentTransactionMapper implements RowMapper<PaymentTransaction>{
	
@Override
public PaymentTransaction mapRow(ResultSet rs, int row) throws SQLException {
	PaymentTransaction paymentTransaction = new PaymentTransaction();
	paymentTransaction.setAmount(rs.getDouble("amount"));
	paymentTransaction.setPaymentTxnId(rs.getInt("payment_txn_id"));
	paymentTransaction.setMerchantTxnId(rs.getString("mtxn_id"));
	paymentTransaction.setOrderId(rs.getString("order_id"));
	paymentTransaction.setPaymentGateway(rs.getString("payment_gateway"));
	paymentTransaction.setPaymentMode(rs.getString("payment_mode"));
	paymentTransaction.setPaymentOption(rs.getString("payment_option"));
	paymentTransaction.setPaymentRequestId(rs.getInt("fk_payment_rq_id"));
	paymentTransaction.setPaymentResponseId(rs.getInt("fk_payment_rs_id"));
	paymentTransaction.setPaymentType(rs.getInt("payment_type"));
	paymentTransaction.setPgMessage(rs.getString("pg_message"));
	paymentTransaction.setRecievedToPg(rs.getTimestamp("recieved_from_pg"));
	paymentTransaction.setSetToPg(rs.getTimestamp("sent_to_pg"));
	paymentTransaction.setSuccessful(rs.getBoolean("is_successful"));
	paymentTransaction.setTransactionId(rs.getString("pg_transaction_id"));
	paymentTransaction.setPgTransactionTime(rs.getTimestamp("pg_transaction_time"));
	return paymentTransaction;
}
}
