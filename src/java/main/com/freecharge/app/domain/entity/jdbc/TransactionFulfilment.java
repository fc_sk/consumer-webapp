package com.freecharge.app.domain.entity.jdbc;

import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 22/10/12
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionFulfilment implements DBEntity {
/*    id	int(11) PK AI
    txn_home_page_id	int(11)
    email	varchar(255)
    address_mobile	varchar(12)
    first_name	varchar(255)
    is_guest	tinyint(1)
    delivery_street	varchar(255)
    delivery_area	varchar(255)
    delivery_landmark	varchar(255)
    delivery_add	varchar(255)
    delivery_city	varchar(255)
    delivery_fk_state_master_id	int(11)
    delivery_fk_country_master_id	int(11)
    delivery_country	varchar(255)
    delivery_pincode	varchar(255)
    delivery_name	varchar(255)
    sms_sent	datetime
    email_sent	datetime
    transaction_status	tinyint(4)
    order_id	varchar(45)
    fk_user_profile_id	int(11)
    created_at	datetime
    updated_at	timestamp*/

    @ColumnMapping(name="id")
    private Integer id;
    @ColumnMapping(name="txn_home_page_id")
    private Integer txnHomePageId;
    @ColumnMapping(name="email")
    private String email;
    @ColumnMapping(name="address_mobile")
    private String addressMobile;
    //todo - check why this is not working
/*    @ColumnMapping(name="is_guest")
    private boolean isGuest;*/
    @ColumnMapping(name="delivery_add")
    private String deliveryAdd;
    @ColumnMapping(name="delivery_city")
    private Integer deliveryFkCityMasterId;
    @ColumnMapping(name="delivery_fk_state_master_id")
    private Integer deliveryFkStateMasterId;
    @ColumnMapping(name="delivery_fk_country_master_id")
    private Integer deliveryFkCountryMasterId;
    @ColumnMapping(name="delivery_country")
    private String deliveryCountry;
    @ColumnMapping(name="delivery_pincode")
    private String deliveryPincode;
    @ColumnMapping(name="delivery_name")
    private String deliveryName;
    @ColumnMapping(name="sms_sent")
    private Date smsSent;
    @ColumnMapping(name="email_sent")
    private Date emailSent;
    @ColumnMapping(name="transaction_status")
    private Integer transactionStatus;
    @ColumnMapping(name="order_id")
    private String orderId;
    @ColumnMapping(name="first_name")
    private String firstName;
    @ColumnMapping(name="created_at")
    private Date createdAt;
    @ColumnMapping(name="updated_at")
    private Date updatedAt;
    @ColumnMapping(name="delivery_street")
    private String deliveryStreet;
    @ColumnMapping(name="delivery_area")
    private String deliveryArea;
    @ColumnMapping(name="delivery_landmark")
    private String deliveryLandmark;
    @ColumnMapping(name="fk_user_profile_id")
    private Integer fkUserProfileId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddressMobile() {
        return addressMobile;
    }

    public void setAddressMobile(String addressMobile) {
        this.addressMobile = addressMobile;
    }

/*    public boolean getGuest() {
        return isGuest;
    }

    public void setGuest(boolean guest) {
        isGuest = guest;
    }*/

    public String getDeliveryAdd() {
        return deliveryAdd;
    }

    public void setDeliveryAdd(String deliveryAdd) {
        this.deliveryAdd = deliveryAdd;
    }

    public Integer getDeliveryFkCityMasterId() {
        return deliveryFkCityMasterId;
    }

    public void setDeliveryFkCityMasterId(Integer deliveryFkCityMasterId) {
        this.deliveryFkCityMasterId = deliveryFkCityMasterId;
    }

    public Integer getDeliveryFkStateMasterId() {
        return deliveryFkStateMasterId;
    }

    public void setDeliveryFkStateMasterId(Integer deliveryFkStateMasterId) {
        this.deliveryFkStateMasterId = deliveryFkStateMasterId;
    }

    public Integer getDeliveryFkCountryMasterId() {
        return deliveryFkCountryMasterId;
    }

    public void setDeliveryFkCountryMasterId(Integer deliveryFkCountryMasterId) {
        this.deliveryFkCountryMasterId = deliveryFkCountryMasterId;
    }

    public String getDeliveryCountry() {
        return deliveryCountry;
    }

    public void setDeliveryCountry(String deliveryCountry) {
        this.deliveryCountry = deliveryCountry;
    }

    public String getDeliveryPincode() {
        return deliveryPincode;
    }

    public void setDeliveryPincode(String deliveryPincode) {
        this.deliveryPincode = deliveryPincode;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public Date getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(Date smsSent) {
        this.smsSent = smsSent;
    }

    public Date getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(Date emailSent) {
        this.emailSent = emailSent;
    }

    public Integer getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Integer transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public void setDeliveryStreet(String deliveryStreet) {
        this.deliveryStreet = deliveryStreet;
    }

    public String getDeliveryArea() {
        return deliveryArea;
    }

    public void setDeliveryArea(String deliveryArea) {
        this.deliveryArea = deliveryArea;
    }

    public String getDeliveryLandmark() {
        return deliveryLandmark;
    }

    public void setDeliveryLandmark(String deliveryLandmark) {
        this.deliveryLandmark = deliveryLandmark;
    }

    public Integer getFkUserProfileId() {
        return fkUserProfileId;
    }

    public void setFkUserProfileId(Integer fkUserProfileId) {
        this.fkUserProfileId = fkUserProfileId;
    }

    @Override
    public String getTableName() {
        return "txn_fulfilment";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        return null;
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));

        TransactionFulfilment transactionFulfilment = new TransactionFulfilment();
        transactionFulfilment.setTxnHomePageId(481626);
        List<TransactionFulfilment> transactionFulfilments = easyDBWriteDAO.get(transactionFulfilment);

        System.out.println(transactionFulfilments);
    }

    @Override
    public String toString() {
        return "TransactionFulfilment{" +
                "id=" + id +
                ", txnHomePageId=" + txnHomePageId +
                ", email='" + email + '\'' +
                ", addressMobile='" + addressMobile + '\'' +
                ", deliveryAdd='" + deliveryAdd + '\'' +
                ", deliveryFkCityMasterId=" + deliveryFkCityMasterId +
                ", deliveryFkStateMasterId=" + deliveryFkStateMasterId +
                ", deliveryFkCountryMasterId=" + deliveryFkCountryMasterId +
                ", deliveryCountry='" + deliveryCountry + '\'' +
                ", deliveryPincode='" + deliveryPincode + '\'' +
                ", deliveryName='" + deliveryName + '\'' +
                ", smsSent=" + smsSent +
                ", emailSent=" + emailSent +
                ", transactionStatus=" + transactionStatus +
                ", orderId='" + orderId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deliveryStreet='" + deliveryStreet + '\'' +
                ", deliveryArea='" + deliveryArea + '\'' +
                ", deliveryLandmark='" + deliveryLandmark + '\'' +
                ", fkUserProfileId=" + fkUserProfileId +
                '}';
    }
}
