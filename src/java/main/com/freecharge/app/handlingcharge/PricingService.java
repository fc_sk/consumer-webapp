package com.freecharge.app.handlingcharge;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.ProductMasterDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;

@Service
public class PricingService {
	@Autowired 
	private FCProperties fcProperties;
	
	@Autowired
	private OrderIdReadDAO orderIdDAO;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private ProductMasterDAO productMasterDAO;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	public int getHandlingCharge(int productMasterId){
		return getHandlingCharge(FCConstants.productMasterMap.get(productMasterId));
	}
	
	public Double getCouponCharges(String orderId){
		List<CartItemsBusinessDO> ctbs = orderIdDAO.getCartItemsByOrderId(orderId);
		Double totalCouponsPrice = 0d;
	        if (ctbs != null && ctbs.size() != 0){
	        	for (CartItemsBusinessDO ctb : ctbs) {
	                if (!ctb.getDeleted()) {
	                    ProductName productName = ProductMaster.ProductName.fromProductId(ctb.getProductMasterId());

	                    switch (productName) {
		                    case PriceCoupons:
		                    	totalCouponsPrice += (ctb.getPrice() * ctb.getQuantity());
		                        break;
	                    }
	                }
	            }
		
	        }
	        double roundOff = Math.round(totalCouponsPrice * 100.0) / 100.0;
	        return roundOff;
	}

	public int getHandlingCharge(String productType){
	    switch (productType) {
            case FCConstants.PRODUCT_TYPE_MOBILE:
                return getMobileHandlingCharge();
                
            case FCConstants.PRODUCT_TYPE_DTH:
                return getDTHHandlingCharge();
                
            case FCConstants.PRODUCT_TYPE_DATACARD:
            case FCConstants.PRODUCT_TYPE_POSTPAID_DATACARD:
                return getDatacardHandlingCharge();
                
            case FCConstants.PRODUCT_TYPE_POSTPAID:
                return getDatacardHandlingCharge();
            
            case FCConstants.PRODUCT_TYPE_BILL:
                return getBillHandlingCharge();
                
            default:
                return 0;
        }
	}
	
	
    public Double getPayableAmountByOrderId(String orderId) {
        List<CartItemsBusinessDO> cartItemsList = orderIdDAO.getCartItemsByOrderId(orderId);
        return getPayableAmount(cartItemsList);
    }


    private Double getPayableAmount(List<CartItemsBusinessDO> ctbs) {
        Double totalPrice = 0d;
        if (ctbs != null && ctbs.size() != 0){
        	for (CartItemsBusinessDO ctb : ctbs) {
                if (!ctb.getDeleted()) {
                    ProductName productName = ProductMaster.ProductName.fromProductId(ctb.getProductMasterId());

                    switch (productName) {
                    case Coupons:
                    case BinOffer:
                        break;
                    case FreeFund:
                        if(FCConstants.PRODUCT_ID_FREEFUND_CASHBACK != ctb.getProductMasterId()) {
                            totalPrice -= ctb.getPrice();
                        } 
                        break;
                    case PriceCoupons:
                    case HCoupons:
                    	totalPrice += (ctb.getPrice() * ctb.getQuantity());
                        break;
                    default:
                        totalPrice += ctb.getPrice();
                        break;
                    }
                }
            } // end for
	
        	 if (totalPrice < 0.0) {
                 if (cartService.containsFreeFund(ctbs)) {
                     logger.info("Negative payable amount for lookupId. Making it zero");
                     return new Double("0.0");
                 } else {
                     throw new IllegalStateException(
                             "Negative payable amount found without any freefund item for lookupId.");
                 }
             }
        }// end if
        double roundOff = Math.round(totalPrice * 100.0) / 100.0; 
        return roundOff;
    }
    
    public Double getCouponPayableAmount(List<CartItemsBusinessDO> ctbs) {
        Double totalPrice = 0d;
        if (fcProperties.isCouponPricingEnabled()){
	        for (CartItemsBusinessDO ctb : ctbs) {
	            if (!ctb.getDeleted()) {
	                ProductName productName = ProductMaster.ProductName.fromProductId(ctb.getProductMasterId());
	
	                switch (productName) {
	                case PriceCoupons:
	                	totalPrice += (ctb.getPrice() * ctb.getQuantity());
	                    break;
	                default:
	                    break;
	                }
	            }
	        }
        }
        double roundOff = Math.round(totalPrice * 100.0) / 100.0;
        return roundOff;
    }
    
    private Double getCouponWorthAmount(List<CartItemsBusinessDO> ctbs) {
        Double worthAmount = 0d;
        for (CartItemsBusinessDO ctb : ctbs) {
            if (!ctb.getDeleted()) {
                ProductName productName = ProductMaster.ProductName.fromProductId(ctb.getProductMasterId());

                switch (productName) {
                case PriceCoupons:
                	Integer couponValue = getCouponValue(ctb.getItemId());
                	worthAmount += (couponValue * ctb.getQuantity());
                    break;
                default:
                    break;
                }
            }
        }
        double roundOff = Math.round(worthAmount * 100.0) / 100.0; 
        return roundOff;
    }
    
    private Integer getCouponValue(Integer couponId) {
    	if (couponId == null){
    		return 0;
    	}
    	Integer couponFaceValue = 0;
    	CouponStore coupon = couponServiceProxy.getCouponStore(couponId);
    	if (coupon != null){
    		couponFaceValue = coupon.getCouponValue();
    	}
		return couponFaceValue;
	}

	public boolean hasPaidCoupons(String orderId) {
    	boolean hasPaidCoupons = false;
    	List<CartItemsBusinessDO> ctbs = orderIdDAO.getCartItemsByOrderId(orderId);
        if (fcProperties.isCouponPricingEnabled()){
	        for (CartItemsBusinessDO ctb : ctbs) {
	            if (!ctb.getDeleted()) {
	                ProductName productName = ProductMaster.ProductName.fromProductId(ctb.getProductMasterId());
	                switch (productName) {
	                case PriceCoupons:
	                	hasPaidCoupons = true;
	                    break;
	                default:
	                    break;
	                }
	            }
	        }
        }
        return hasPaidCoupons;
    }
    
    /**
     * This is essentially difference between what the customer paid and what
     * would be refunded to him in case of recharge failure. One example of
     * non-refundable amount is cross-sell amount. For a given order ID,
     * following invariant should hold true:
     * getNonRefundableAmountForOrderId(orderId) = getPayableAmount(orderId) -
     * getRefundAmountForOrderId(orderId)
     * 
     * @param orderId
     *            for which non-refundable amount is to be calculated.
     * @return Non-refundable amount. Zero if orderId does not have any cart.
     */
    public Double getNonRefundableAmountForOrderId(String orderId) {
        CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId);

        Double nonRefundableAmount = new Double("0");

        if (cartToInspect != null) {
            nonRefundableAmount = getPayableAmount(cartToInspect).doubleValue()
                    - getRefundAmountForOrderId(orderId).doubleValue();
        } else {
            logger.info("No cart found for Order ID [" + orderId + "]. Returning zero as nonRefundableAmount");
        }
        
        double roundOff = Math.round(nonRefundableAmount * 100.0) / 100.0; 
        return roundOff;
    }
    
    public Double getPayableAmount(CartBusinessDo cartBusinessDo) {
        List<CartItemsBusinessDO> ctbs = cartBusinessDo.getCartItems();
        Double totalPrice = getPayableAmount(ctbs);
        return totalPrice;
    }
    
    
    /**
     * Calculate amount to refund for a given order ID. This takes freefund and
     * cross sell into consideration.
     * 
     * @param orderId
     * @return
     */
    @Transactional(readOnly = true)
    public Double getRefundAmountForOrderId(String orderId) {
    	logger.info("Got a call to get refund amount for order id: " + orderId);
        CartBusinessDo cartTobeRefunded = cartService.getCartByOrderId(orderId);
        logger.info("Just got out of get cart by order id for order id: " + orderId + " and cart to refund is " + cartTobeRefunded);

        Double refundAmount = new Double("0");

        if (cartTobeRefunded != null) {
        	logger.info("Got cart to be refunded for order id: " + orderId + " as " + cartTobeRefunded.toString());
            for (CartItemsBusinessDO cartItem : cartTobeRefunded.getCartItems()) {
                if (!cartItem.getDeleted()) {
                	logger.info("Cart was not deleted for order id: " + orderId);
                    ProductMaster productInCart = productMasterDAO.findByProductMasterId(cartItem.getProductMasterId());

                    if (productInCart.getIsRefundable()) {
                    	logger.info("Cart is refundable for order id: " + orderId);
                    	if (productInCart!= null && productInCart.getProductMasterId() != null && 
                    			productInCart.getProductMasterId().equals(FCConstants.PRODUCT_ID_PAID_COUPON)){
                    		refundAmount += (cartItem.getPrice() * cartItem.getQuantity());	
                    	} else{
                    		refundAmount += cartItem.getPrice();
                    	}
                    	logger.info("Cart is refundable for order id: " + orderId + " and refund amount is: " + refundAmount);
                    }

                    // TODO: We need a "isChargeable" attribute.
                    if ("FreeFund".equalsIgnoreCase(productInCart.getProductName()) && cartItem.getProductMasterId() != FCConstants.PRODUCT_ID_FREEFUND_CASHBACK) {
                    	refundAmount -= cartItem.getPrice();
                    	logger.info("Removed freefund cash for order id: " + orderId + " and new refund amount is: " + refundAmount);
                    }
                }
            }
        } else {
            logger.info("Oops! No cart found for Order ID [" + orderId + "]. Nothing to refund");
        }

        if (refundAmount < 0.0) {
            if (cartService.containsFreeFund(cartTobeRefunded.getCartItems())) {
                logger.info("Negative refund amount for lookupId:[" + cartTobeRefunded.getLookupID()
                        + "]. Making it zero");
                return new Double(0);
            } else {
            	logger.info("Negative refund amount found for order id: " + orderId + " and refund amount is: " + refundAmount);
                throw new IllegalStateException(
                        "Negative refund amount found without any freefund item for lookupId: ["
                                + cartTobeRefunded.getLookupID() + "]");
            }
        }
        double roundOff = Math.round(refundAmount * 100.0) / 100.0; 
        return roundOff;
    }
    
    public boolean isTotalPayableamountIsZero(CartBusinessDo cartBusinessDo) {
        if (getPayableAmount(cartBusinessDo) == 0.0) {
            return true;
        }
        return false;
    }
    
    public Double getCouponPayableAmountByOrderId(String orderId) {
    	List<CartItemsBusinessDO> cartItemsList = orderIdDAO.getCartItemsByOrderId(orderId);
    	return getCouponPayableAmount(cartItemsList);
    }
    
    public Double getCouponWorthAmountByOrderId(String orderId) {
    	List<CartItemsBusinessDO> cartItemsList = orderIdDAO.getCartItemsByOrderId(orderId);
    	return getCouponWorthAmount(cartItemsList);
    }
    
	private int getDatacardHandlingCharge() {
		return fcProperties.getIntProperty(FCConstants.HANDLING_CHARGE_DATACARD);
	}

	private int getDTHHandlingCharge() {
		return fcProperties.getIntProperty(FCConstants.HANDLING_CHARGE_DTH);
	}

	private int getMobileHandlingCharge() {
		return fcProperties.getIntProperty(FCConstants.HANDLING_CHARGE_MOBILE);
	}

	private int getBillHandlingCharge() {
        return fcProperties.getIntProperty(FCConstants.HANDLING_CHARGE_BILL);
    }
}
