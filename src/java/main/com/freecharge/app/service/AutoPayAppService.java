package com.freecharge.app.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.freecharge.app.domain.dao.jdbc.RequestLookupMapDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.BillerType;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.service.CommonHttpService;
import com.freecharge.web.service.CommonHttpService.HttpResponse;
import com.freecharge.web.webdo.RechargeInitiateWebDo;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.snapdeal.payments.sdmoney.service.model.Balance;

@Service
public class AutoPayAppService {
	Logger logger = LoggingFactory.getLogger(AutoPayAppService.class);

	@Autowired
	RequestLookupMapDAO requestLookupMapDAO;

	@Autowired
	private OneCheckWalletService ocWalletService;

	@Autowired
	private UserService userService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private CommonHttpService commonHttpService;

	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;

	public static final String LOW_BALANCE_ERROR = "AP-1001";
	public final static String SUCCESSFUL_REFILL_PRESENT = "AP-2001";

	private static final String PLAN_URL_PROD = "https://api-internal.freecharge.in/rds/plan/info/{operatorId}/{circleId}/{amount}";
	private static final String PLAN_URL_QA = "https://freecharge.in/rds/plan/info/{operatorId}/{circleId}/{amount}";

	private static final String AUTO_PAY_ENABLED_WITH_AMOUNT = "/isAutopayEnabledWithAmount";
	private static final String AUTO_PAY_ENABLED_WITHOUT_AMOUNT = "/isAutopayEnabled";
	
	public enum AutopayAPIVersion {
		VERSION_1(1), VERSION_2(2);

		private int versionId;

		private AutopayAPIVersion(int versionId) {
			this.versionId = versionId;
		}

		public int getVersionId() {
			return versionId;
		}

	}

	public void insertRequestLookupMap(String requestId, String lookupId) {
		if (!FCUtil.isEmpty(requestId)) {
			requestLookupMapDAO.insert(RechargeConstants.AUTO_PAY_MERCHANT_ID, requestId, lookupId);
		} else {
			logger.error("Empty requestId");
			throw new RuntimeException("Empty request Id");
		}
	}

	public String getLookupIdFromRequestId(String requestId) {
		return requestLookupMapDAO.getLookupIdForRequest(RechargeConstants.AUTO_PAY_MERCHANT_ID, requestId);
	}

	public Balance getBalance(int userId) {
		String fcWalletId = ocWalletService.getOneCheckIdIfOneCheckWalletEnabled(userService.getEmail(userId));
		Balance balance = null;
		if (fcWalletId != null) {
			balance = ocWalletService.getFCWalletAccountBalance(fcWalletId);
		}
		return balance;
	}

	public String validateBalance(Double amount, int userId) {
		Balance balance = getBalance(userId);
		if (balance == null) {
			return null;
		}
		if (balance.getTotalBalance().compareTo(new BigDecimal(amount)) < 0) {
			return LOW_BALANCE_ERROR;
		}
		return null;
	}

	public String validateBalance(Float amount, int userId) {
		Balance balance = getBalance(userId);
		if (balance == null) {
			return null;
		}
		if (balance.getTotalBalance().compareTo(new BigDecimal(amount)) < 0) {
			return LOW_BALANCE_ERROR;
		}
		return null;
	}

	public Map<String, Object> getAutopayMetadata(String orderId, RechargeDetails rechargeDetails,
			RechargeInitiateWebDo rechargeInitiateWebDo, Integer loggedInUserId, AutopayAPIVersion version) {
		Map<String, Object> attributeMap = new HashMap<>();
		if (!appConfigService.isAutoPayEnabled()) {
			return attributeMap;
		}
		try {
			if (StringUtils.isEmpty(rechargeInitiateWebDo.getRechargeResponse())
					|| !RechargeConstants.SUCCESS_RESPONSE_CODE.equals(rechargeInitiateWebDo.getRechargeResponse())) {
				return attributeMap;
			}
			
			ProductName productType = ProductName.fromPrimaryProductType(rechargeInitiateWebDo.getProductType());
			if (!productType.isUtilityPaymentProduct() && !productType.isPrepaid()) {
				return attributeMap;
			}
			
			if (productType.isUtilityPaymentProduct() && !appConfigService.isAutoPayEnabledForUtility()) {
				return attributeMap;
			}

			int validity = 0;
			if (productType.isPrepaid()) {
				if (isAutoPayAlreadyEnabled(rechargeDetails.getServiceNumber(), loggedInUserId,
						rechargeDetails.getAmount(), AUTO_PAY_ENABLED_WITH_AMOUNT)) {
					logger.info(
							"Recharge Autopay schedlue already exists for num : " + rechargeDetails.getServiceNumber()
									+ " user : " + loggedInUserId + " amount : " + rechargeDetails.getAmount());
					return attributeMap;
				}
				UriComponentsBuilder builder = null;
				if (fcProperties.isProdMode() || fcProperties.isPreProdMode()) {
					builder = UriComponentsBuilder.fromHttpUrl(PLAN_URL_PROD);
				} else {
					builder = UriComponentsBuilder.fromHttpUrl(PLAN_URL_QA);
				}
				
				Map<String, Object> pathParams = new HashMap<>();
				pathParams.put("operatorId", rechargeDetails.getOperatorMasterId());
				pathParams.put("circleId", rechargeDetails.getCircleId());
				pathParams.put("amount", rechargeDetails.getAmount());
				URI uri = builder.buildAndExpand(pathParams).toUri();
				HttpResponse validityResponse = commonHttpService.fireGetRequestWithTimeOut(uri.toURL().toString(),
						new HashMap<String, String>(), 500);
				validity = getValidity(validityResponse.getResponseBody());
			} else if (productType.isUtilityPaymentProduct()
					&& isFetchAndPayOperator(rechargeDetails.getOperatorMasterId(), rechargeDetails.getCircleId())) {
				if (isAutoPayAlreadyEnabled(rechargeDetails.getServiceNumber(), loggedInUserId,
						rechargeDetails.getAmount(), AUTO_PAY_ENABLED_WITHOUT_AMOUNT)) {
					logger.info("Billpay Autopay schedlue already exists for num : "
							+ rechargeDetails.getServiceNumber() + " user : " + loggedInUserId);
					return attributeMap;
				}
				validity = 30;
			}
			if (validity == 0) {
				attributeMap.clear();
				return attributeMap;
			}
			if(AutopayAPIVersion.VERSION_2 == version){
				formAttributeMapV2(attributeMap,productType,validity,rechargeDetails);
			}else{
				formAttributeMap(attributeMap,productType,validity,rechargeDetails);
			}			
	
			return attributeMap;
		} catch (Exception ex) {
			logger.error("Error generating autopay metadata for orderId: " + orderId, ex);
			attributeMap.clear();
		}
		return attributeMap;
	}

	private void formAttributeMapV2(Map<String, Object> attributeMap, ProductName productType, int validity,
			RechargeDetails rechargeDetails) {		
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MMM dd");		
		if (productType.isPrepaid()) {
			attributeMap.put("autopayHeader", "<b><font color=#000000>RECHARGE AUTOMATICALLY</font></b>");
			attributeMap.put("autopaySubHeader", String.format("Repeat this recharge after <b><font color=#E3714D>every %s days</font></b> using Freecharge wallet balance.",String.valueOf(validity)));
			
		} else {
			attributeMap.put("autopayHeader", "<b><font color=#000000>AUTOMATIC BILL PAYMENT</font></b>");
			attributeMap.put("autopaySubHeader", "From next month, pay "+"<b><font color=#E3714D>"+rechargeDetails.getOperatorName()+"</font></b>"+" bill automatically using Freecharge wallet balance");
		}
		attributeMap.put("bannerUrl", "");
		attributeMap.put("iconUrl", "images/autopay/autopay.png");
		List<String> faq = new ArrayList<>();
		faq.add("Q.What if wallet balance is low?");
		faq.add("A.We'll remind you to add money 1 day before and on the day of transaction.");
		faq.add("Q.How do i Opt-Out in future?");
		faq.add("A.Go to profile - My account details - AutoPay & Notifications - and switch off Auto Pay.");
		attributeMap.put("faq", faq);
	}

	private void formAttributeMap(Map<String, Object> attributeMap, ProductName productType, int validity, RechargeDetails rechargeDetails) {
		List<String> timeLine = new ArrayList<>();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MMM dd");
		attributeMap.put("autopayHeader", "Activate Auto Pay");
		if (productType.isPrepaid()) {
			timeLine.add(new LocalDate().toString(dtf) + ",Today's Recharge");
			timeLine.add(new LocalDate().plusDays(validity).toString(dtf) + ",Due Date");
			timeLine.add(new LocalDate().plusDays(validity * 2).toString(dtf) + ",Next Due Date");
			attributeMap.put("timeLine", timeLine);
			attributeMap.put("timeLineTxt", "Vaid till " + new LocalDate().plusDays(validity).toString(dtf) + " midnight");
			attributeMap.put("timeLineHeader", "RECHARGE TIMELINE");
			attributeMap.put("iconText", "Opt-In for autopay and we will automatically repeat "
					+ "this recharge using wallet money.We recharge after 12PM, so squeeze every second of validity.");
		} else {
			timeLine.add(new LocalDate().toString(dtf) + ",Today's payment");
			timeLine.add(new LocalDate().plusDays(validity).toString(dtf) + ",Next payment");
			timeLine.add(new LocalDate().plusDays(validity * 2).toString(dtf) + ",Payment after that");
			attributeMap.put("timeLine", timeLine);
			
			attributeMap.put("timeLineTxt", "Next bill payment on " + new LocalDate().plusDays(validity).toString(dtf));
			attributeMap.put("timeLineHeader", "BILLPAY TIMELINE");
			attributeMap.put("iconText", "Opt-In for autopay and we will automatically do your " + rechargeDetails.getOperatorName()
					+ " bill payment using wallet money from next time onward 5 days before due date.");
		}
		attributeMap.put("iconUrl", "images/autopay/autopay.png");
		List<String> faq = new ArrayList<>();
		faq.add("Q.What if wallet balance is low?");
		faq.add("A.We'll remind you to add money 1 day before and on the day of transaction.");
		faq.add("Q.How do i Opt-Out in future?");
		faq.add("A.Go to profile - My account details - AutoPay & Notifications - and switch off Auto Pay.");
		attributeMap.put("faq", faq);
		
	}

	private boolean isFetchAndPayOperator(String operatorMasterId, String circleId) {
		BillPayValidator validator = billPayServiceRemote.getBillPayValidatorForOperator(
				operatorMasterId == null ? null : Integer.parseInt(operatorMasterId),
				circleId == null ? null : Integer.parseInt(circleId));
		return BillerType.FETCH_AND_PAY.getBillerType().equals(validator.getBillerType());
	}

	private Boolean isAutoPayAlreadyEnabled(String serviceNumber, Integer userId, String amount, String api) {
		Map<String, Object> pathParams = new HashMap<>();
		pathParams.put("amount", Double.parseDouble(amount));
		pathParams.put("serviceNumber", serviceNumber);
		pathParams.put("userId", userId.intValue());
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		try {
			HttpResponse response = commonHttpService.firePostRequestWithCustomHeader(
					fcProperties.getAutoPayBaseUrl() + api, new Gson().toJson(pathParams), Boolean.FALSE,
					Integer.valueOf(500), headers);
			Type type = new TypeToken<Map<String, Boolean>>() {
			}.getType();
			Map<String, Boolean> a = new Gson().fromJson(response.getResponseBody(), type);
			Boolean isSchedulePresent = a.get("autoPayEnabled");
			return isSchedulePresent == null ? false : isSchedulePresent;
		} catch (HTTPException e) {
			logger.error("Caught HTTPException on getting schedule for user " + userId, e);
		} catch (IOException e) {
			logger.error("Caught IOException on getting schedule for user " + userId, e);
		}
		return true;
	}

	private int getValidity(String planJson) {
		logger.info("Plan details : " + planJson);
		int validity = 0;
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(planJson);
		JsonObject obj = element.getAsJsonObject();
		Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();
		JsonElement temp = null;
		for (Map.Entry<String, JsonElement> entry : entries) {
			temp = entry.getValue().getAsJsonObject().get("validity");
			if (temp != null && temp.getAsString() != null && !temp.getAsString().equals("NA")
					&& !temp.getAsString().equals("null")) {
				validity = getValidityFrmStr(temp.getAsString());
				break;
			}
		}
		return validity;
	}

	private static int getValidityFrmStr(String validity) {
		int validityDays = 0;
		Pattern dayPattern = Pattern.compile("(.*?)(\\d{1,})(.*[[Ww]eek?|[Dd]ay?|[Mm]onth?].*)");
		if (validity == null || dayPattern == null) {
			return validityDays;
		}
		Matcher matcher = dayPattern.matcher(validity);
		if (matcher.matches()) {
			String validityIn = matcher.group(3).trim();
			if (validityIn.contains("week") || validityIn.contains("Week")) {
				validityDays = Integer.parseInt(matcher.group(2)) * 7;
			} else if (validityIn.contains("Day") || validityIn.contains("day")) {
				validityDays = Integer.parseInt(matcher.group(2));
			} else if (validityIn.contains("Month") || validityIn.contains("month")) {
				validityDays = Integer.parseInt(matcher.group(2)) * 30;
			}
		}
		return validityDays;
	}
}
