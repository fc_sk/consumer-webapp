package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.AuditTrailFilterDao;
import com.freecharge.app.domain.dao.HomepageTxnDetailsDao;
import com.freecharge.app.domain.entity.AuditTrailFilter;
import com.freecharge.app.domain.entity.HomepageTxnDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 19, 2012
 * Time: 10:03:56 AM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class AuditTrailService {

    private static Logger logger = LoggingFactory.getLogger(AuditTrailService.class);
    private AuditTrailFilterDao auditTrailFilterDao;
    private HomepageTxnDetailsDao homepageTxnDetailsDAO;

    @Autowired
    public void setAuditTrailFilterDao(AuditTrailFilterDao auditTrailFilterDao) {
        this.auditTrailFilterDao = auditTrailFilterDao;
    }

    @Autowired
    public void setHomepageTxnDetailsDao(HomepageTxnDetailsDao homepageTxnDetailsDAO) {
        this.homepageTxnDetailsDAO = homepageTxnDetailsDAO;
    }

    @Transactional(readOnly = true)
    public String doAuditTrailService(String url) {

        AuditTrailFilter auditTrailFilter = auditTrailFilterDao.findByUrl(url);
        String methodName = null;
        if (auditTrailFilter != null) {
            methodName = auditTrailFilter.getMethodName();
        }

        return methodName;
    }


    public void insertSession(String sessionId, HttpServletRequest httpServletRequest) {

        HomepageTxnDetails homepageTxnDetails = new HomepageTxnDetails();
        homepageTxnDetails.setSessionUniqueId(sessionId);

        try {

        String hql= "from  HomepageTxnDetails where sessionUniqueId=?";
        List paramList=new ArrayList();
        paramList.add(sessionId) ;
        List<HomepageTxnDetails> homepageTxnDetailsList= homepageTxnDetailsDAO.findByHQL(hql, paramList);
        if(homepageTxnDetailsList!=null && homepageTxnDetailsList.size()!=0 && !homepageTxnDetailsList.isEmpty()){
        logger.info("Session Id Value is " + sessionId);
              }
            else{
            homepageTxnDetailsDAO.create(homepageTxnDetails);
        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void insertRechargeDetails(String sessionId , HttpServletRequest httpServletRequest){
        try{
        logger.info("Entered into the insertRechargeDetails() of the AuditTrailService  ");
        logger.info("Session id in insertRechargeDetails is "+sessionId);
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String mobileNumber = httpServletRequest.getParameter("mobileNumber");
        String amount = httpServletRequest.getParameter("amount");
        String operator = httpServletRequest.getParameter("operator");
        String circle = httpServletRequest.getParameter("circle");
        //httpServletRequest.getParameter("1");
        logger.info("Service Number is " + mobileNumber);
        HomepageTxnDetails homepageTxnDetails = new HomepageTxnDetails();
        homepageTxnDetails.setSessionUniqueId(sessionId);

        if(amount!=null)
        homepageTxnDetails.setAmount(Double.valueOf(amount.trim()).doubleValue());
        else
        homepageTxnDetails.setAmount(Double.valueOf(100).doubleValue());

        homepageTxnDetails.setProductServiceNumber(mobileNumber);
        //homepageTxnDetails.setProductSelected();
        homepageTxnDetails.setOperator(operator);
        homepageTxnDetails.setCircle(circle);

        if(fs.isLogin()) {
        homepageTxnDetails.setIsLoggedin(true);
        Map sessionData=fs.getSessionData();
        homepageTxnDetails.setEmail((String)sessionData.get("email"));    
        }

    

        String hql= "from  HomepageTxnDetails where sessionUniqueId=?";
        List paramList=new ArrayList();
        paramList.add(sessionId) ;
        List<HomepageTxnDetails> homepageTxnDetailsList= homepageTxnDetailsDAO.findByHQL(hql, paramList);
        if(homepageTxnDetailsList!=null){
        logger.info("Session Id Value is " + sessionId);
        homepageTxnDetails.setHomepageTxnDetailsId(homepageTxnDetailsList.get(0).getHomepageTxnDetailsId());
        }
        if(mobileNumber!= null){
      homepageTxnDetailsDAO.updateHomepage(homepageTxnDetails);
        }
        }
        catch(Exception exp){
            logger.error("exception raised while auditing , exp details are " +exp);
        }
       }

    public void accountAccessed(String sessionId , HttpServletRequest httpServletRequest){
           try{
        String hql= "from  HomepageTxnDetails where sessionUniqueId=?";
        HomepageTxnDetails homepageTxnDetails=null;
        List paramList=new ArrayList();
        paramList.add(sessionId) ;
        List<HomepageTxnDetails> homepageTxnDetailsList= homepageTxnDetailsDAO.findByHQL(hql, paramList);
        if(homepageTxnDetailsList!=null){
        logger.info("Session Id Value is " + sessionId);
        homepageTxnDetails=       homepageTxnDetailsList.get(0);
            homepageTxnDetails.setHomepageTxnDetailsId(homepageTxnDetailsList.get(0).getHomepageTxnDetailsId());
            homepageTxnDetails.setMyaccountAccessed(true);

            homepageTxnDetailsDAO.updateHomepage(homepageTxnDetails); 
            }
           } catch(Exception e){
              logger.error("exception raised while updating the Hometrxdetails , exp details are " + e);

            }
    }




  }
