package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.jdbc.BinOfferDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.BinOffer.OfferType;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.web.util.WebConstants;
import com.google.common.collect.ImmutableMap;

@Service
public class BinOfferService {
	private static Logger logger = LoggingFactory.getLogger(BinOfferService.class);

	public static final Map<String, String> banks = ImmutableMap.of(
			"AXIS",
				"Axis Bank",
				"ICICI",
				"ICICI Bank",
				"YESBANK",
				"Yes Bank",
				"HDFC",
                "HDFC Bank",
                "KOTAK",
                "Kotak Bank");
	
	public static final Map<String, String> offerTypes = ImmutableMap.of(
			"CASHBACK",
				"Cash Back",
				"INSTANT",
				"Instant",
				"REWARD",
				"Reward");
	
	public static final Map<String, String> discountTypes = ImmutableMap.of(
			"PERCENT",
				"Percentage",
				"FLAT",
				"Flat");
	
	public static final String CONDITION_STRING_NEW_USER = "new_user";
	public static final String INACTIVE_USER_STRING_NEW_USER = "inactive_user";
	public static final String OFFER_TYPE_CASHBACK = "CASHBACK";
	public static final String OFFER_TYPE_INSTANT = "INSTANT";
	
	public static final String DISCOUNT_TYPE_PERCENT = "PERCENT";
	public static final String DISCOUNT_TYPE_FLAT = "FLAT";
	
	@Autowired
	private CartItemsDAO cartItemsDAO;
	
	@Autowired
	private BinOfferDAO binOfferDAO;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private PricingService pricingService;
	
	@Autowired
	private MyAccountService myAccountService;
	
	@Autowired
	private FreefundService freefundService;
	
	@Autowired
	private UserTransactionHistoryService historyService;

    public double calculateDiscount(BinOffer binOffer, CartBusinessDo cartBusinessDo) {
		
		Double totalPayableAmount = pricingService.getPayableAmount(cartBusinessDo);
		if (BinOfferService.DISCOUNT_TYPE_PERCENT.equals(binOffer.getDiscountType())) {
			return totalPayableAmount * (binOffer.getDiscount() / 100);
		}
		else {
			return binOffer.getDiscount();
		}
	}
	
	public BinOffer create(BinOffer binOffer) {
		return binOfferDAO.create(binOffer);
	}
	
	public boolean update(BinOffer binOffer) {
		return binOfferDAO.update(binOffer);
	}


	public List<BinOffer> getAll() {
		return binOfferDAO.getAll();
	}

	public List<BinOffer> getAllActive(Integer id) {
		return binOfferDAO.getAllActive(id);
	}

	public List<BinOffer> getAllActive(String bank) {
		return binOfferDAO.getAllActive(bank);
	}

	public BinOffer getValidBinOffer(List<BinOffer> binOffers, String cardNumber) {
		if (binOffers == null || binOffers.isEmpty()) {
			return null;
		}

		for (BinOffer binOffer : binOffers) {
			boolean isBinValid = checkBin(binOffer.getBin(), cardNumber);
			if (isBinValid) {
				return binOffer;
			}
		}
		return null;
	}

	public boolean checkBin(String binCode, String cardNo) {
		// 512345********** 532702 ***** *****
		// **********123456 532702 02143 34024
		binCode = binCode.trim();
		String bin = binCode;
		int binStart = bin.indexOf('*');
		int binEnd = bin.lastIndexOf('*');

		bin = bin.replace("*", "");

		String binPart = "";
		if(binStart == -1 && binEnd == -1) { //bins got uploaded without star, then this case will arrive
			if (cardNo.length() < bin.length()) {
                //bin=12345678, cardNo=123456
                binPart = cardNo;
            } else {
                binPart = cardNo.substring(0,bin.length());
            }
		} else {
			if (binStart == 0) {
				if (cardNo.length() < binEnd + 1) {
					return false;
				}
				binPart = cardNo.substring(binEnd + 1);
			}
			else {
				if (cardNo.length() < binStart) {
					return false;
				}
                if (binStart <= cardNo.length()) {
				    binPart = cardNo.substring(0, binStart);
                } else {
                    binPart = cardNo;
                    bin = bin.substring(0, cardNo.length());
                }
            }
		}
		
		if (bin.equals(binPart)) {
			return true;
		}
		return false;
	}

	public Map<String, BinOffer> getActiveOffers(int userId) {
		Map<String, BinOffer> allOffers = binOfferDAO.getActiveOffers();
		Map<String, BinOffer> validForUserOffer = new HashMap<String, BinOffer>();
		for(String key : allOffers.keySet()) {
			BinOffer binOffer = allOffers.get(key);
			if(validateForUser(binOffer, userId)) {
				validForUserOffer.put(key, binOffer);
			}
		}
		return validForUserOffer;
	}

	public boolean isValidAmount(BinOffer binOffer, String lid) {
		CartBusinessDo cartBusinessDo = cartService.getCart(lid);
        Double rechargeAmount = cartService.getRechargeAmount(cartBusinessDo);
        if(rechargeAmount >= binOffer.getMinRechargeAmount()) {
        	return true;
        }
		return false;
	}

	public List<BinOffer> getAllActive() {
		return binOfferDAO.getAllActive();
	}
	
	@Transactional
	public void saveCartWithBinOffer(BinOffer binOffer, CartBusinessDo cartBusinessDo, double amount) {
		CartItems cartItems = new CartItems();
		Cart cart = new Cart();
		cart.setCartId(cartBusinessDo.getId().intValue());

		cartItems.setCart(cart);
		cartItems.setFkItemId(binOffer.getId().intValue());
        cartItems.setPrice(amount);
        cartItems.setQuantity(1);
        cartItems.setDisplayLabel(binOffer.getBin() +" ("+binOffer.getBank()+")");
        cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
        cartItems.setIsDeleted(false); //save binOffer in deleted state, on offer success in PayU gteway handler, then change status to false

		switch (OfferType.valueOf(binOffer.getOfferType())) {
            case INSTANT:
                cartItems.setEntityId(ProductName.FreeFund.toString());
                cartItems.setFlProductMasterId(9);
                if (!cartService.isFreeFundPresent(cartItems)) {
                    cartService.saveCartItems(cartItems);
                }
                break;

            case CASHBACK:
                cartItems.setEntityId(ProductName.BinOffer.toString());
                cartItems.setFlProductMasterId(ProductName.BinOffer.getProductId());
                cartService.saveCartItems(cartItems);
                break;
        }
	}
	
	public BinOffer getBinOfferById(Integer id) {
		return binOfferDAO.getBinOfferById(id);
	}
	
	public BinOffer getBinOffer(Integer id) {
		return binOfferDAO.getBinOffer(id);
	}

	public List<BinOffer> getAllWithKey(String offerKey) {
		return binOfferDAO.getAllWithKey(offerKey);
	}

	public void deleteOffersOfkey(String offerKey) {
		binOfferDAO.deleteOffersOfkey(offerKey);
		
	}

	public void updateStatus(String offerKey, int status) {
		binOfferDAO.updateStatus(offerKey, status);
	}
	
	public boolean isNewUser(int userId) {
		return !historyService.hasTransacted(userId);
	}
	
	public boolean isInActiveUser(int userId, int inactiveDays) {
	    DateTime activeDateThreshold = DateTime.now().minusDays(inactiveDays);
	    
	    return !historyService.hasTransactedSince(userId, activeDateThreshold);
	}
	
	public boolean validateForUser(BinOffer binOffer, Integer userId) {
		boolean valid = false;
		Map<String, Object> conditionsMap = getConditionsAsMap(binOffer.getConditions());
		if (!conditionsMap.isEmpty()) {
			for (String conditionKey : conditionsMap.keySet()) {
				String conditionValue = (String) conditionsMap.get(conditionKey);
				if (isConditionValid(conditionKey, conditionValue, userId)) {
					valid = true;
				}
			}
		} else {
			valid = true;
		}
		return valid;
	}
	
	public List<BinOffer> validateForUser(List<BinOffer> binOffers, Integer userId) {
		List<BinOffer> validBinOffers = new ArrayList<BinOffer>();
		for(BinOffer binOffer : binOffers) {
			Map<String, Object> conditionsMap = getConditionsAsMap(binOffer.getConditions());
			if(!conditionsMap.isEmpty()) {
				for(String conditionKey : conditionsMap.keySet()) {
					String conditionValue = (String)conditionsMap.get(conditionKey);
					if(isConditionValid(conditionKey, conditionValue, userId)) {
						validBinOffers.add(binOffer);
					}
				}
			} else {
				validBinOffers.add(binOffer);
			}
		}
		return validBinOffers;
	}

	private boolean isConditionValid(String conditionKey, String conditionValue, Integer userId) {

		if(conditionKey.equals(CONDITION_STRING_NEW_USER)) {
			if(isNewUser(userId)) {
				return true;
			}
		} else if(conditionKey.equals(INACTIVE_USER_STRING_NEW_USER)) {
			Integer inactiveDays = 0;
			try {
				inactiveDays = Integer.parseInt(conditionValue);
			}
			catch (Exception e) {
				logger.error("Error parsing value " + conditionValue );
			}
			if(inactiveDays != 0 && isInActiveUser(userId, inactiveDays)) {
				return true;
			}
		} 
		return false;
	}

	private Map<String, Object> getConditionsAsMap(String conditions) {
		Map<String, Object> map = new HashMap<String, Object>();
		if(StringUtils.isEmpty(conditions)) return map;
		String[] splitArray = conditions.split("&");
        for (int i=0; i < splitArray.length; i++) {
            String[] splitEqual = splitArray[i].split("=");
            String part0 = splitEqual[0].trim();
            String part1 = "";
            
            if (splitEqual.length == 2) {
                part1 = splitEqual[1];
            }
            
            map.put(part0, part1);
        }
        return map;
	}
	
	public Integer getEligibleOffer(String cardNumber, String lid) {
        List<BinOffer> binOffers = getAllActive();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Integer userId = ((Integer) fs.getSessionData().get(WebConstants.SESSION_USER_USERID));
        /*
         * Heavy querying. Fix it
         */
        binOffers = validateForUser(binOffers, userId);
        BinOffer binOffer = getValidBinOffer(binOffers, cardNumber);
        if (binOffer != null) {
            boolean isValidAmount = isValidAmount(binOffer, lid);
            if(!isValidAmount) {
                return null;
            }
            CartBusinessDo cartBusinessDo = cartService.getCart(lid);
            boolean isCartHasFreefund =  freefundService.containsFreeFund(cartBusinessDo);
            if(isCartHasFreefund) {
                return null;
            }
            return binOffer.getId();
        }
        return null;
    }
}
