package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import bsh.StringUtil;

import com.freecharge.cartcheckout.CartCheckoutClientService;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.order.checkout.client.impl.OrderCheckoutServiceClientImpl;
import com.freecharge.order.checkout.enums.ClientPlatform;
import com.freecharge.order.checkout.enums.PaymentMethods;
import com.freecharge.order.checkout.exception.ServiceException;
import com.freecharge.order.checkout.model.PaymentAmount;
import com.freecharge.order.checkout.request.OndeckProcessOrderRequest;
import com.freecharge.order.checkout.service.IOrderCheckoutService;
import com.freecharge.order.checkout.utils.ClientDetails;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.ProcessPayTaskRequest;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.wallet.OneCheckWalletService;
import com.snapdeal.payments.ts.TaskScheduler;
import com.snapdeal.payments.ts.dto.TaskDTO;
import com.snapdeal.payments.ts.response.ExecutorResponse;

@Component
public class OMSClientService {

	@Value("${oms.client.IP}")
	private String ip;

	@Value("${oms.client.port}")
	private String port;

	@Value("${oms.client.clientKey}")
	private String clientKey;

	@Value("${oms.client.clientId}")
	private String clientId;

	// @Value("${oms.client.apiTimeout}")
	private int apiTimeout = 6000;

	private Logger logger = LoggingFactory.getLogger(OMSClientService.class);

	@Autowired
	private TaskScheduler taskScheduler;

	private IOrderCheckoutService orderCheckoutService;

	@Autowired
	private PaymentsCache paymentsCache;

	@Autowired
	private CartCheckoutClientService service;

	@Autowired
	private PaymentPlanService paymentPlanService;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private AppConfigService appConfig;

	@PostConstruct
	private void init() {
		try {
			ClientDetails.init(ip, port, clientKey, clientId, apiTimeout);
			orderCheckoutService = new OrderCheckoutServiceClientImpl();
		} catch (Exception e) {
			logger.error("oms client initialization exception", e);
		}
	}

	public void processOndeckOrder(String orderId, int channelId,
			String lookupId, boolean isRetry, PaymentPlan palymentPlan,
			ProductPaymentBusinessDO productPaymentBusinessDO) {
		OndeckProcessOrderRequest processOrderRequest = new OndeckProcessOrderRequest();
		String mtxnId = productPaymentBusinessDO.getMtxnId();
		String mobileNumber = productPaymentBusinessDO.getUser().getMobileNo();
		String email = productPaymentBusinessDO.getUser().getEmail();
		List<PaymentAmount> listOfpaymentPlan = new ArrayList<PaymentAmount>();
		String channelName = (String) FCConstants.channelNameMasterMap
				.get(FCConstants.channelMasterMap.get(channelId));
		try {
			String imsId = oneCheckWalletService
					.getOneCheckIdIfOneCheckWalletEnabled(email);
			if (null != palymentPlan
					&& palymentPlan.getPgAmount().getAmount().doubleValue() > 0.0
					&& palymentPlan.getWalletAmount().getAmount().doubleValue() > 0.0) {
				listOfpaymentPlan.add(new PaymentAmount(palymentPlan
						.getPgAmount().getAmount(), PaymentMethods
						.valueOf("PG")));
				listOfpaymentPlan.add(new PaymentAmount(palymentPlan
						.getWalletAmount().getAmount(), PaymentMethods
						.forMethod("WALLET")));
			} else {
				if (productPaymentBusinessDO.getPaymenttype().equalsIgnoreCase(
						PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET)
						|| productPaymentBusinessDO.getPaymenttype()
								.equalsIgnoreCase(
										PaymentConstants.PAYMENT_TYPE_WALLET)) {
					listOfpaymentPlan.add(new PaymentAmount(palymentPlan
							.getPgAmount().getAmount(), PaymentMethods
							.forMethod("WALLET")));
				} else {
					listOfpaymentPlan.add(new PaymentAmount(palymentPlan
							.getPgAmount().getAmount(), PaymentMethods
							.forMethod("PG")));
				}
			}
			processOrderRequest.setOrderId(orderId);
			processOrderRequest.setMtxnId(mtxnId);
			processOrderRequest.setRetry(isRetry);
			processOrderRequest.setUserId(imsId);
			if(channelName.equalsIgnoreCase("MOBLIE")) {
				processOrderRequest.setChannel(ClientPlatform.valueOf("WAP"));
			} else {
				processOrderRequest.setChannel(ClientPlatform.valueOf(channelName
						.toUpperCase()));
			}
			processOrderRequest.setMobileNumber(mobileNumber);
			if(StringUtils.isEmpty(mobileNumber)) {
				processOrderRequest.setMobileNumber("9999xxxx99");
			}
			processOrderRequest.setPaymentAmounts(listOfpaymentPlan);
			processOrderRequest.setCheckoutId(service
					.getCheckoutIdFromLookupId(lookupId));
			orderCheckoutService.processOnDeckOrder(processOrderRequest);
			logger.info("intent created successfully into tsm for processpay api for mtxnid "+ mtxnId);
		} catch (ServiceException e) {
			logger.error(
					"processOnDeckOrder throwing service exception for mtxnId"
							+ processOrderRequest.toString(),  e);
			logger.info("error code for processOnDeckOrder " + e.getErrCode() + " error message " + e.getErrMsg() +  " for mtxn id " + mtxnId);
			if (!StringUtils.isEmpty(e.getErrCode())
					&& e.getErrCode().equalsIgnoreCase(
							PaymentConstants.TASK_ALREADY_CREATED)) {
				logger.info("task already created for mtxn id "
						+ processOrderRequest.getMtxnId());
			} else {
				createTask(processOrderRequest, lookupId, mtxnId);
			}
		} catch (Exception e) {
			logger.error(
					"processOnDeckOrder api throwing exception for mtxnId "
							+ mtxnId, e);
			createTask(processOrderRequest, lookupId, mtxnId);
		}
	}

	private String generateTaskId() {
		return UUID.randomUUID().toString()
				+ new Timestamp(System.currentTimeMillis());
	}

	private void createTask(OndeckProcessOrderRequest processOrderRequest,
			String lookupId, String mtxnId) {
		ProcessPayTaskRequest paymentProcessTaskRequest = new ProcessPayTaskRequest();
		paymentProcessTaskRequest
				.setOndeckProcessOrderRequest(processOrderRequest);
		logger.info("paymentProcessTaskRequest metadata " + lookupId
				+ " generateTaskId() " + generateTaskId());
		paymentProcessTaskRequest.setLookupId(lookupId);
		paymentProcessTaskRequest.setTaskId(generateTaskId());
		TaskDTO taskDTO = new TaskDTO();
		taskDTO.setCurrentScheduleTime(new Date());
		taskDTO.setRequest(paymentProcessTaskRequest);
		taskDTO.setTaskType(PaymentConstants.PROCESS_PAY_TASK_TYPE);
		try {
			logger.info("going to sumbit paymentprocesstask for mtxnId "
					+ mtxnId);
			taskScheduler.submitTask(taskDTO);
			logger.info("paymentprocesstask scheduled successfully for mtxnId "
					+ mtxnId);
		} catch (Exception ex) {
			logger.error(
					"Exception while creating paymentprocesstask for mtxnId "
							+ mtxnId, ex);
		}
	}
}
