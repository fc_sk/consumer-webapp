package com.freecharge.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.PincodeMappingReadDao;
import com.freecharge.app.domain.entity.PincodeMapping;

@Service
public class PincodeMappingService {
	
	@Autowired
	private PincodeMappingReadDao pincodeMappingReadDao;
	
	public List<PincodeMapping> fetchMappingForPincode(String pincode) {
		return pincodeMappingReadDao.getPincodeMappingByPincode(pincode);
	}
	
}