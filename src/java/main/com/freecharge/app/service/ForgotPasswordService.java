package com.freecharge.app.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.IllegalBlockSizeException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.IForgotPasswordDAO;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.businessdo.ForgotPasswordBusinessDO;
import com.freecharge.common.businessdo.PasswordRecoveryBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.growthevent.service.UserServiceProxy;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/2/12
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ForgotPasswordService {
     private Logger logger = LoggingFactory.getLogger(getClass());
    private IForgotPasswordDAO forgotPasswordDAO;
    private EmailService emailService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;

    @Autowired
    EPinEncrypt ePinEncrypt;
    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
    @Autowired
    public void setForgotPasswordDAO(IForgotPasswordDAO forgotPasswordDAO) {
        this.forgotPasswordDAO = forgotPasswordDAO;
    }

    @Transactional
    public  void getPassword(BaseBusinessDO businessDO) throws Exception{
       Users users=null;
       UserProfile userProfile= null;
       ForgotPasswordBusinessDO forgotPasswordBusinessDO= (ForgotPasswordBusinessDO)businessDO;
       List<UserProfile> list=   forgotPasswordDAO.getPassword(businessDO);
       if(list!=null && list.size()==1){
           userProfile=(UserProfile)list.get(0);
           users = userProfile.getUsers();
           Boolean activeFlag = users.getIsActive();
           if(activeFlag){
        	   String firstName = userProfile.getFirstName();
		        Calendar c = Calendar.getInstance();
		        Date date = c.getTime();
		        String encryptedValue = ePinEncrypt.getEncryptedPin(users.getUserId()+"~"+new Timestamp(date.getTime()));
		        users.setForgotPasswordKey(encryptedValue);
			    c.add(Calendar.DATE, 1);
			    date.setTime(c.getTimeInMillis());
			    /******* now date obj modified to one day ahead **********/
		        users.setForgotPasswordExpiry(new Timestamp(date.getTime()));
		        forgotPasswordDAO.updateUser(users);
		        forgotPasswordBusinessDO.setUserValid(true);
		        EmailBusinessDO emailBusinessDO=new EmailBusinessDO();
		        Map<String,String>  variableValues = new HashMap<String,String>();
		        variableValues.put("firstName", firstName);
		        variableValues.put("encryptedValue",encryptedValue);
		        
		        String imgPrefix=fcPropertyPlaceholderConfigurer.getStringValueStatic("imgprefix1");
		
		        String hostprefix=fcPropertyPlaceholderConfigurer.getStringValueStatic("hostprefix");
		        variableValues.put("hostprefix",hostprefix);
		        variableValues.put("imgPrefix",imgPrefix);
		        logger.info("setting Password Recovery email details");
		        forgotPasswordBusinessDO.setStatus(fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.success.msg"));
		        String from=fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.from.emailid");
		        String subject=fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.sub");
		        String replyTo=fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.replyto");
		        emailBusinessDO.setVariableValues(variableValues);
		        emailBusinessDO.setFrom(from);
		        emailBusinessDO.setTo(forgotPasswordBusinessDO.getEmail());
		        String passwordRecoveryVm = fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.email.vm").trim();
		        emailBusinessDO.setTemplateName(passwordRecoveryVm);
		        emailBusinessDO.setSubject(subject);
		        emailBusinessDO.setReplyTo(replyTo);
		        emailService.sendEmail(emailBusinessDO);
	        }else{
	        	forgotPasswordBusinessDO.setStatus(fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.user.inactive.msg"));
	        	return;
	        }
       }else{
             forgotPasswordBusinessDO.setUserValid(false);
             forgotPasswordBusinessDO.setStatus(fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.failuere.msg"));
             return;
       }
    }

    public void doesUserExist(BaseBusinessDO baseBusinessDO){
        PasswordRecoveryBusinessDO passwordRecoveryBusinessDO=(PasswordRecoveryBusinessDO)baseBusinessDO;
       	String encValue = passwordRecoveryBusinessDO.getEncryptValue();
       	String decryptedValue ="";
       	try{
       		decryptedValue = EPinEncrypt.getDecryptedPin(encValue);
       	}catch(IllegalBlockSizeException exp){
       		logger.error("Error occured while decrypting encrypted value of password recovery string in ForgotPasswordService ");
       	} catch (ParseException e) {
       		logger.error("Error occured while decrypting encrypted value of password recovery string in ForgotPasswordService ");
		} catch (Exception e) {
			logger.error("Error occured while decrypting encrypted value of password recovery string in ForgotPasswordService ");
		}
       	String splittedStr[] = decryptedValue.split("~");
       	if(splittedStr.length == 2){
        	String userid = splittedStr[0];
        	String date = splittedStr[1];
        	List<Users> userList = forgotPasswordDAO.getUser(new Integer(userid),encValue);
        	if(userList == null || userList.size() == 0){
        		((PasswordRecoveryBusinessDO)baseBusinessDO).setUserExist(true);
        		((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("ExpairedLink");
        	}else{
        		Users user = userList.get(0);
        		Integer userId = user.getUserId();
        		DateFormat formater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        		Long currentMillis = new Date().getTime();
        		try{
        			Date initialDate = formater.parse(date);
        			Long initialdateMillis=initialDate.getTime();
        			Long diff=(currentMillis-initialdateMillis)/(1000*60*60*24);
        			
	        		if(diff < 1){
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setUserExist(true);
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("ValidLink");
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setEmail(user.getEmail());
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setUserId(userId.toString());
	        		}else{
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setUserExist(true);
	        			((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("ExpairedLink");
	        		}
        		}catch (ParseException pe) {
					logger.error("Error occured while parsing string into Date in ForgotpasswordService for the string "+date);
					((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("InvalidLink");
				}
        	}
       	}else{
       		((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("InvalidLink");
       	}
       return ;
    }


    public void changePassword(BaseBusinessDO baseBusinessDO){
        String newPassword=((PasswordRecoveryBusinessDO)baseBusinessDO).getNewPassword();
        String confirmPassword=((PasswordRecoveryBusinessDO)baseBusinessDO).getConfirmPassword();
        String emailId=((PasswordRecoveryBusinessDO)baseBusinessDO).getEmail();
        String userId=((PasswordRecoveryBusinessDO)baseBusinessDO).getUserId();
        if(newPassword.equals(confirmPassword)) {
            //EPinEncrypt ePinEncrypt=new EPinEncrypt();
          try{
          ((PasswordRecoveryBusinessDO)baseBusinessDO).setEmail(ePinEncrypt.getDecryptedPin(emailId));
          ((PasswordRecoveryBusinessDO)baseBusinessDO).setUserId(ePinEncrypt.getDecryptedPin(userId));
          ((PasswordRecoveryBusinessDO)baseBusinessDO).setNewPassword(ePinEncrypt.getEncryptedPin(newPassword));
              Boolean passwordChangeStatus = forgotPasswordDAO.changePassword(baseBusinessDO);
              if(passwordChangeStatus){
                  ((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("Your Password Changed Successfully");
              }else{
                 ((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("expairedLink");
              }
          }catch (Exception e){
            ((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("Your password not changed due to technical problem");  
          }
        } else {
             ((PasswordRecoveryBusinessDO)baseBusinessDO).setStatus("Password and confirm password not matched");
        }


    }

	public boolean expirePasswordLinkOnSuccessfulLogin(String emailId) {
		com.freecharge.app.domain.entity.jdbc.Users users = userServiceProxy
				.getUserByEmailId(emailId);
		if (users != null) {
			if (users.getForgotPasswordKey() != null) {
				users.setForgotPasswordKey(null);
				userService.updateUser(users);
				return true;
			}
		}
		return false;
	}

}
