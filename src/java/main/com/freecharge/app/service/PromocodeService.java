package com.freecharge.app.service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeReadDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeWriteDAO;
import com.freecharge.app.domain.entity.jdbc.Promocode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.SimpleReturn;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.rest.onecheck.model.PaymentType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA. User: Manoj Kumar Date: May 11, 2012 Time: 11:52:22 AM To change this
 * template use File | Settings | File Templates.
 */
@Service
public class PromocodeService {
	private static Logger logger = LoggingFactory.getLogger(PromocodeService.class);

	private final static String PROMOCODE_CREATE = "create";

	private final static String PROMOCODE_SAVE = "save";

	@Autowired
	private PromocodeReadDAO promocodeReadDAO;

	@Autowired
	private PromocodeWriteDAO promocodeWriteDAO;
	
	@Autowired
    private KestrelWrapper	kestrelWrapper;

	@Autowired
	private MetricsClient	metricsClient;
	
	@Autowired
	private OrderIdWriteDAO              orderIdWriteDAO;

	@Autowired
	private FreefundService              freefundService;
	
	@Autowired
    private UserServiceProxy              userServiceProxy;
	
	@Autowired
    private RechargeStatusIdempotencyCache rechargeStatusIdempotencyCache;
	
	@Autowired
    private AppConfigService             appConfigService;
	
	@Autowired
    private CartService                  cartService;

    @Autowired
    private CampaignServiceClient campaignServiceClient;


	public Promocode getPromocode(Integer id) {
		return promocodeReadDAO.getPromocode(id);
	}

	public List<Promocode> getAll() {
		return promocodeReadDAO.getAll();
	}

	public boolean createOrUpdate(Promocode promocode, String createOrSave) {
		if (PROMOCODE_CREATE.equals(createOrSave)) {
			return promocodeWriteDAO.create(promocode);
		} else
			if (PROMOCODE_SAVE.equals(createOrSave)) {
				return promocodeWriteDAO.update(promocode);
			}

		return false;
	}

	public Promocode getPromocode(String promoCode) {
		return promocodeReadDAO.getPromocode(promoCode);
	}

	public Promocode getFirstActivePromocode() {
		return promocodeReadDAO.getFirstActivePromocode();
	}

	public List<Promocode> getConflictingPromos(Promocode promocode) {
		return promocodeReadDAO.getConflictingPromos(promocode);
	}
	
	public void doPromocodeFullfilment(String orderId, PaymentTransaction walletTransaction, PaymentPlan paymentPlan, PaymentTransaction paymentTransaction) {
        logger.info("doing promocode fullfilment for " + orderId);
        String lookupId = orderIdWriteDAO.getLookupIdForOrderId(orderId);

        //Saving payment related data to intxndata store
        HashMap<String, Object> paymentInfo = new HashMap<String, Object>();
        paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, paymentTransaction.getPaymentType());
        String paymentOptionStr = (paymentTransaction.getPaymentOption() == null)? "" : paymentTransaction.getPaymentOption(); 
        if("UPI".equalsIgnoreCase(paymentOptionStr))
        {
        	paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, com.freecharge.payment.services.PaymentType.PaymentTypeName.UPI.getPaymentTypeCode());
        }
        if (paymentPlan!=null && paymentTransaction.getPaymentType() != 9){
            paymentInfo.put(InTxnDataConstants.PG_AMOUNT, paymentPlan.getPgAmount().getAmount().doubleValue());
            paymentInfo.put(InTxnDataConstants.WALLET_AMOUNT, paymentPlan.getWalletAmount().getAmount().doubleValue());
        }
        try {
            logger.info("Populating payment data to intxndata " + paymentInfo + " for lookupid " + lookupId);
            campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, paymentInfo);
        }catch (Exception e){
            logger.error("Error saving intxndata in payment gateway handler: ", e);
        }
        Long freefundCouponId = freefundService.getFreefundCouponIdForLookupId(lookupId);
        if (freefundCouponId != null) {
            boolean fullfilmentFailed = false;
            FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(freefundCouponId);
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + "  doing promocode fullfilment for " + orderId);
            
            if (rechargeStatusIdempotencyCache.isDuplicateRequest(orderId, freefundCoupon.getFreefundCode())) {
                logger.info("Duplicate Request : " + freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] : " + orderId);
                return;
            }
            
            Integer userId = orderIdWriteDAO.getUserIdFromOrderId(orderId);

            Users user = userServiceProxy.getUserByUserId(userId);
            CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
            boolean isValidClass = false;
            if (!freefundService.getFreefundClass(freefundCoupon.getFreefundClassId()).getIsNewCampaign()) {
                SimpleReturn simpleReturn = freefundService.validateClass(user, cartBusinessDo, freefundCoupon, null);
                isValidClass = simpleReturn.isValid();
                if (!isValidClass) {
                    if (FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(freefundCoupon.getPromoEntity())) {
                        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND,
                                FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-" + freefundCoupon.getFreefundClassId()
                                + "." + simpleReturn.getErrorCode().toString());
                    } else {
                        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                                FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-" + freefundCoupon.getFreefundClassId()
                                + "." + simpleReturn.getErrorCode().toString());
                    }
                }
            } else {
                try {
                    CampaignResult campaignResult = null;
                    JSONObject hashCheckObj = appConfigService.getCustomProp(FCConstants.PROMOCODE_PAYMENT_INITIATE_CHECK);
                    Object isCheckEnabled = hashCheckObj.get(FCConstants.IS_CHECK_ENABLED_FOR_BLOCK);
                    if (isCheckEnabled != null && !FCUtil.isEmpty(String.valueOf(isCheckEnabled))
                            && "true".equalsIgnoreCase(String.valueOf(isCheckEnabled))) {

                        campaignResult = campaignServiceClient.checkAndProcessCampaign1(lookupId,
                                freefundCoupon.getFreefundClassId().intValue(),
                                "BLOCK_PROMOCODE", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
                    } else {
                        campaignResult = new CampaignResult();
                        campaignResult.setStatus(true);
                    }
                    if (!campaignResult.getStatus()) {
                        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId()
                                + "] " + campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                        isValidClass = false;
                    }
                    isValidClass = true;
                } catch (Exception e) {
                    logger.error("Error while incrementing Block Promocode count for orderId " + orderId + ", lookupId " + lookupId);
                }
            }
            boolean cartEligibleForOffer = cartService.isEligibleForOffers(cartBusinessDo);

            if (appConfigService.isPromocodeEnabled() && cartEligibleForOffer && isValidClass) {

                FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
                freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
                freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
                freeFundRequestBusinessDO.setEmail(user.getEmail());

                freeFundRequestBusinessDO.setCouponCode(freefundCoupon.getFreefundCode());
                freeFundRequestBusinessDO.setLookupID(lookupId);

                freeFundRequestBusinessDO.setServiceNumber(cartService.getRechargedNumber(lookupId));

                boolean isSuccess = freefundService.blockCouponCode(freeFundRequestBusinessDO);
                if (isSuccess) {
                    rechargeStatusIdempotencyCache.markPromocodeFulfillmentSuccess(orderId, freefundCoupon.getFreefundCode());
                    logger.info("Promocode fullfilment success for " + freefundCoupon.getFreefundCode() + " ["
                            + freefundCoupon.getFreefundClassId() + "] " + "  for " + orderId);
                } else {
                    fullfilmentFailed = true ;
                }
            } else {
                fullfilmentFailed = true ;
            }

            if (!appConfigService.isPromocodeEnabled()) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-" + freefundCoupon.getFreefundClassId() +
                        "." + ErrorCode.PROMOCODE_FEATURE_DISABLED.toString());
            } else if (!cartEligibleForOffer) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-" + freefundCoupon.getFreefundClassId() +
                        "." + ErrorCode.CART_NOT_ELIGIBLE_FOR_OFFER.toString());
            }

            if(fullfilmentFailed) {
                String errorMessage = freefundCoupon.getFreefundCode() + " used in order ID: " + orderId
                        + " is failed to redeem. cartEligibleForOffer : " + cartEligibleForOffer + " isValidClass : "
                        + isValidClass;
                logger.error(errorMessage);
                kestrelWrapper.enqueueUPUpdate(orderId);
                /* We are already recording events in blockcouponcode.
                metricsClient.recordEvent("Promocode", "Fulfillment", "Failure"); */
                throw new IllegalStateException(errorMessage);
            }
        }
    }
}
