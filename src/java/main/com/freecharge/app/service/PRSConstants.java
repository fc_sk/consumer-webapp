package com.freecharge.app.service;

public class PRSConstants {

	public static final String TITLE = "title";
	public static final String REASON_MSG = "reasonMsg";
	public static final String NEXT_MSG = "nextMsg";
	public static final String CARE_MSG = "careMsg";
	public static final String IS_ACTION_REQUIRED = "isActionRequired";
	public static final String ACTION_BUCKET = "actionBucket";
	public static final String ACTION_BUCKET_DEEPLINK = "actionBucketDeepLink";
	public static final String ACTION_BUCKET_CTA = "actionBucketCTA";
}
