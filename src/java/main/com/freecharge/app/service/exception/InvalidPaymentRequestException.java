package com.freecharge.app.service.exception;

import java.util.List;

public class InvalidPaymentRequestException extends Exception{
	private List<String> errorMessages;

	public InvalidPaymentRequestException(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
}
