package com.freecharge.app.service.exception;


public class CampaignConditionException extends Exception {
    private static final long serialVersionUID = 1L;

    private String            customMsg;

    public CampaignConditionException(String customMsg) {
        this.customMsg = customMsg;
    }

    public CampaignConditionException() {
        this.customMsg = null;
    }

    public String getCustomMsg() {
        return customMsg;
    }

    public void setCustomMsg(String customMsg) {
        this.customMsg = customMsg;
    }
}
