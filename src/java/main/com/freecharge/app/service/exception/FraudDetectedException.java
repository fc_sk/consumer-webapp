package com.freecharge.app.service.exception;

public class FraudDetectedException extends Exception{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String customMsg;
    
    public String getCustomMsg() {
        return customMsg;
    }
    
    public void setCustomMsg(String customMsg) {
        this.customMsg = customMsg;
    }

    public FraudDetectedException(String customMsg) {
        this.customMsg = customMsg;
    }
}
