package com.freecharge.app.service.exception;

public class CardValidationException extends Exception {

    private static final long serialVersionUID = 1L;

    private String            customMsg;

    public CardValidationException(String customMsg) {
        this.customMsg = customMsg;
    }

    public String getCustomMsg() {
        return customMsg;
    }

    public void setCustomMsg(String customMsg) {
        this.customMsg = customMsg;
    }
}
