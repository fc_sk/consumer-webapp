package com.freecharge.app.service;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: Apr 21, 2012
 * Time: 12:50:31 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ICouponRedeemptionService {
    public Map redeemCouponAction(String couponNo) throws Exception;
    public void afterRedeemCouponAction(String couponNo,boolean rechargeStatus) throws Exception;
}
