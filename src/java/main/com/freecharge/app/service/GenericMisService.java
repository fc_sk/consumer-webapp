package com.freecharge.app.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.GenericMisDataFetchDAO;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.recharge.util.RechargeConstants;

public class GenericMisService {

	@Autowired
	private GenericMisDataFetchDAO genericMisDataFetchDAO;
	
	@Autowired
	private FCProperties fcProperties;
	
	public long getTotalSuccessfulTransactions() {
		long totalSuccessfulTxns = 0;
		
		totalSuccessfulTxns = fcProperties.getIntProperty(FCConstants.OLD_DB_SUCCESSFUL_TRANSACTIONS) + fcProperties.getIntProperty(FCConstants.NEW_DB_SUCCESSFUL_RECHARGES_RESP_CODE_0) + genericMisDataFetchDAO.getTotalTxnsByRechargeStatus(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE);
		return totalSuccessfulTxns;
	}
}
