package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.FailedLoginReadDAO;
import com.freecharge.app.domain.dao.jdbc.FailedLoginWriteDAO;
import com.freecharge.app.domain.entity.FailedLogin;

@Service
public class FailedLoginService {

    @Autowired
    private FailedLoginReadDAO  failedLoginReadDAO;

    @Autowired
    private FailedLoginWriteDAO failedLoginWriteDAO;
    
    private static final long ONE_DAY_MILLISCONDS = 24 * 60 * 60 * 1000;
    private static final long ONE_WEEK_DAYS = 7;

    public FailedLogin createFailedLogin(final String userEmail, String userIp, final Timestamp failedTimestamp) {
        FailedLogin failedLogin = new FailedLogin();
        failedLogin.setUserEmail(userEmail);
        if (userIp == null){
        	userIp = "";
        }
        failedLogin.setIpAddress(userIp);
        failedLogin.setAttemptedTime(failedTimestamp);
        return failedLogin;
    }

    public FailedLogin insertFailedLogin(final FailedLogin failedLogin) {
        return failedLoginWriteDAO.insertFailedLogin(failedLogin);
    }
    
    public void removeDataOlderThanAWeek() {
        long beforeOneWeek = new Date().getTime() - (ONE_WEEK_DAYS * ONE_DAY_MILLISCONDS);
        failedLoginWriteDAO.deleteOlderFailedLogins(new Timestamp(beforeOneWeek));
    }
    
    public Boolean isFailedLoginThreshold(String userEmail, int thresholdCount, Timestamp start) {
        Timestamp end = new Timestamp(new Date().getTime());
        List<FailedLogin> failedLogins = failedLoginReadDAO.getAllFailedLoginsForUserEmail(userEmail, start, end);
        return failedLogins.size() > thresholdCount;
    }
}
