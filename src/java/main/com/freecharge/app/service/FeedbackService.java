package com.freecharge.app.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.businessdo.FeedbackBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;

@Service("feedbackService")
public class FeedbackService {
	@Autowired
	private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;
	@Autowired
	private EmailService emailService;
	private Logger logger = LoggingFactory.getLogger(getClass());
	public String sendFeedBack(BaseBusinessDO baseBusinessDO){
		try{
			//FeedbackBusinessDO businessDO = (FeedbackBusinessDO)baseBusinessDO;
			this.sendFeedBackDetails(baseBusinessDO);
			
			return "success";
		}catch(Exception exception){
			logger.error("Exception occured while sending feedback",exception);
			return "fail";
		}
	}
	private void sendFeedBackDetails(BaseBusinessDO baseBusinessDO){
		FeedbackBusinessDO businessDO = (FeedbackBusinessDO)baseBusinessDO;
		String email = businessDO.getEmail();
		String topicArea = businessDO.getTopicArea();
		String subject = businessDO.getSubject();
		String message = businessDO.getMessage();
		
		Map<String, String> variableValues = new HashMap<String, String>();
		variableValues.put("email", email);
		variableValues.put("topicArea", topicArea);
		variableValues.put("subject", subject);
		variableValues.put("message", message);
		String imgPrefix = fcPropertyPlaceholderConfigurer.getStringValueStatic("imgprefix1");
		variableValues.put("imgPrefix", imgPrefix);
		String from = fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.feedback.from.emailid");
		String to = fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.feedback.to.emailid");
		//String to = "gvs.chinny@gmail.com";
		String sub = fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.feedback.subject");
		String replyTo = fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.feedback.replyto");
		String bcc = fcPropertyPlaceholderConfigurer.getStringValueStatic("fc.feedback.bcc");
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setTo(to);
		emailBusinessDO.setFrom(from);
		emailBusinessDO.setBcc(bcc);
		emailBusinessDO.setReplyTo(replyTo);
		emailBusinessDO.setSubject(sub);
		emailBusinessDO.setVariableValues(variableValues);
		emailBusinessDO.setTemplateName("FeedbackEmail.vm");
		
		emailService.sendEmail(emailBusinessDO);
		
	}
}
