package com.freecharge.app.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.HomeBusinessDao;

/**
 * Created by IntelliJ IDEA. User: Jitender Date: Apr 17, 2012 Time: 11:06:54 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class HomeService {
    @Autowired
    private HomeBusinessDao homeBusinessDao;

    @Transactional
    public Map<String, Object> getUserRechargeDetailsFromOrderId(String orderId) {
        return homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
    }

    @Transactional
    public Map<String, Object> getUserDetailsFromOrderIdWithAddressIfAvailable(String orderID) {
        return homeBusinessDao.getUserDetailsFromOrderIdWithAddressIfAvailable(orderID);
    }
}
