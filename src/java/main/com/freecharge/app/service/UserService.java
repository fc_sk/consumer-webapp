package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.IUsersDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.UserSlaveDao;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.dao.jdbc.UsersWhitelistDAO;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.easydb.EasyDBReadDAO;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.session.service.ISessionService;
import com.freecharge.common.framework.session.service.SessionService;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.recharge.services.CommonRechargeService;
import com.freecharge.rest.user.UserProfileCache;
import com.freecharge.rest.user.bean.FailureRechargeInfo;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.web.util.WebConstants;

@Component
public class UserService {
    @Autowired
    private EasyDBReadDAO easyDBReadDAO;
    @Autowired
    private EasyDBWriteDAO easyDBWriteDAO;
    @Autowired
    private TxnFulfilmentDAO txnFulfilmentDAO;
    @Autowired
    private TxnHomePageDAO txnHomePageDAO;
    @Autowired
    private ISessionService sessionService;
    
    @Autowired
    private UserProfileCache profileCache;
    
    @Autowired
    private UserSlaveDao userSlaveDao;
    
    @Autowired
    private UserWriteDao userWriteDao;
    
    @Autowired
    AppConfigService appConfig;
    
    @Autowired
    private IUsersDAO usersDao;
    
    @Autowired
    private UsersWhitelistDAO usersWhitelistDAO;
    
    @Autowired
    OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private CommonRechargeService rechargeService;
    
    @Autowired
    HomeBusinessDao                homeBusinessDao;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(UserService.class.getSimpleName());
    }
    
    public SuccessfulRechargeInfo getLastSuccessfulRechargeByNumber(String serviceNumber) {
        if (appConfig.isRechargeRetryBlockEnabled()) {
            final SuccessfulRechargeInfo lastRecharge = getLastSuccessfulRechargeNoConfigCheck(serviceNumber);
            
            if (lastRecharge != null) {
                if (rechargeService.isRetryAttemptBlocked(lastRecharge.getOperatorCode())) {
                    return lastRecharge;
                }
            }
            
        }
        
        return null;
    }
    
    public FailureRechargeInfo getLastFailureRechargeByNumber(String serviceNumber) {
           if (appConfig.isRechargeRetryBlockEnabled()) {
               return getLastFailureRechargeNoConfigCheck(serviceNumber);
           }
           
           return null;
    }
    
    public SuccessfulRechargeInfo getLastSuccessfulRechargeNoConfigCheck(String serviceNumber) {
        try {
            return profileCache.getLastSuccessfulRechargeByNumber(serviceNumber);
        }catch (Exception e){
            logger.error("error getting last successful recharge: ServiceNumber: "+serviceNumber, e);
        }
        return null;
    }
    
    public FailureRechargeInfo getLastFailureRechargeNoConfigCheck(String serviceNumber) {
           try {
               return profileCache.getLastFailureRechargeByNumber(serviceNumber);
           }catch (Exception e){
               logger.error("error getting last failure recharge: ServiceNumber: "+serviceNumber, e);
           }
           return null;
    }
    
    public void setLatestSuccessfulRecharge(String emailId, SuccessfulRechargeInfo rechargeInfo) {
        try {
            profileCache.saveLastSuccessfulRecharge(emailId, rechargeInfo);
        }catch (Exception e){
            logger.error("error setting last successful recharge: email: "+emailId, e);
        }
    }
    
    public void setLatestFailureRecharge(String emailId, FailureRechargeInfo rechargeInfo) {
           try {
               profileCache.saveLastFailureRecharge(emailId, rechargeInfo);
           }catch (Exception e){
               logger.error("error setting last failure recharge: email: "+emailId, e);
           }
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        EasyDBReadDAO easyDBReadDAO = ((EasyDBReadDAO) ctx.getBean(EasyDBReadDAO.getBeanName()));
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean(EasyDBWriteDAO.getBeanName()));
        TxnFulfilmentDAO txnFulfilmentDAO = ((TxnFulfilmentDAO) ctx.getBean("txnFulfilmentDAO"));
        TxnHomePageDAO txnHomePageDAO = ((TxnHomePageDAO) ctx.getBean(TxnHomePageDAO.getBeanName()));
        SessionService sessionService = ((SessionService) ctx.getBean(SessionService.getBeanName()));

        Users users = new Users();

        String email = "abhirama@freecsdfdharge.com";
        users.setEmail(email);

        List<Users> userList = easyDBReadDAO.get(users);

        users = FCUtil.getFirstElement(userList);
        users.setIsActive(false);
        users.addToWhereFields("email");
        easyDBWriteDAO.update(users);
        System.out.println(easyDBReadDAO.get(users));

        UserProfile userProfile = new UserProfile();
        userProfile.setFkUserId(users.getUserId());

        List<UserProfile> userProfiles = easyDBReadDAO.get(userProfile);
        userProfile = FCUtil.getFirstElement(userProfiles);
        userProfile.setIsActive(false);
        userProfile.addToWhereFields("userProfileId");
        easyDBWriteDAO.update(userProfile);
        System.out.println(easyDBReadDAO.get(userProfile));

        List<TxnFulfilment> txnFulfilments = txnFulfilmentDAO.findByEmail(email);

        Set<String> sessionIds = new HashSet<String>();
        for (TxnFulfilment txnFulfilment : txnFulfilments) {
            Integer homePageId = txnFulfilment.getTxnHomePageId();
            List<TxnHomePage> txnHomePages = txnHomePageDAO.findByProperty("id", homePageId);
            for (TxnHomePage txnHomePage : txnHomePages) {
                String sessionId = txnHomePage.getSessionId();
                System.out.println(sessionId);
                sessionIds.add(sessionId);
            }
        }

        for (String sessionId : sessionIds) {
            sessionService.deleteSession(sessionId);
        }

        ctx.close();
    }
    
    public Users getUser(String emailId) {
        return  userServiceProxy.getUserByEmailId(emailId);
    }
    
    public Integer whitelistUserInDatabase(Integer userId) {
        return usersWhitelistDAO.updateWhitelistedForUser(userId, true);
    }
    
    public Integer blacklistUserInDatabase(Integer userId) {
        return usersWhitelistDAO.updateWhitelistedForUser(userId, false);
    }
    
    public Boolean isWhitelistedInDatabase(Integer userId) {
        return usersWhitelistDAO.isWhitelisted(userId);
    }
    
    public Users updateUser(Users users) {
    	return userWriteDao.updateUser(users);
    }
    
    /**
     * Validates if a given order Id is owned by
     * the logged in user.
     * @param orderId
     * @return <code>true</code> if valid.
     */
    public boolean validateOrderOwnership(String orderId) {
        Integer loggedinUserId = this.getLoggedInUser().getUserId();
        Users orderIdOwner = this.getUserFromOrderId(orderId);
        
        if (orderIdOwner == null || (orderIdOwner.getUserId().intValue() != loggedinUserId.intValue())) {
            return false;
        }
        
        return true;
    }
    
    public int updateUserEmailId(Integer userId, String emailId) {
        return userWriteDao.updateUserEmailId(userId, emailId);
    }
    
    public List<Users> getUserByPartialEmailId(String partialEmailId) {
        return userSlaveDao.getUserByPartialEmailId(partialEmailId);
    }
    
    public Users getLoggedInUser() {
        if(FreechargeContextDirectory.get() != null && FreechargeContextDirectory.get().getFreechargeSession() != null){
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            Map<String, Object> map = fs.getSessionData();
            Boolean isloggedin = false;
            if (map != null) {
                String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
                if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                    isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                    if (isloggedin != null && !isloggedin) {
                        logger.debug("No logged in user found for " + email);
                        return null;
                    } else {
                        Users user = getUser(email);
                        if (user == null) {
                            logger.debug("No user found in db for " + email);
                        }
                        return user;
                    }
                } else {
                    logger.debug("isLogin boolean not found in session data for " + email);
                    return null;
                }
            } else {
                logger.debug("No session data found for user");
            }
        } else {
            logger.debug("No context found in freecharge context directory");
        }
        return null;
    }
    
    public Users getUserById(Integer userId) {
        return userServiceProxy.getUserByUserId(userId);
    }
    
    public Users getUserFromOrderId(String orderId) {
        Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
        if (userId != null) {
            return getUserById(userId);
        }
        return null;
    }
    
    public UserProfile getUserProfile(Integer userId){
        return this.userWriteDao.getUserProfile(userId);
    }

    public UserProfile getDefaultUserProfile(Integer userId){
        return this.userWriteDao.getDefaultUserProfile(userId);
    }

  //Sms alert:Updating profile No with user entered alert No
    public void updateUserByProfileNumber(Integer userId, String alertMobileNo) {
        userWriteDao.updateUserByProfileNumber(userId, alertMobileNo);
    }

    private static Logger logger = LoggingFactory.getLogger(UserService.class);
    
    public List<com.freecharge.app.domain.entity.Users> findByMobileNo(final String profileNo) {
        return usersDao.findByMobileNo(profileNo);
    }

    public boolean userConnectedViaProvider(SessionConstants.LoginSource currentProvider, String provider) {
        return currentProvider!=null && currentProvider.toString().equalsIgnoreCase(provider);
     }
    
	public long getUserId(String email) {
		try {
			return userServiceProxy.getUserIdByEmailId(email);
		} catch (RuntimeException ex) {
			return -1;
		}
	}
	
	public String getEmail(Integer userId) {
		try {
			Users users = this.getUserById(userId);
		    if (users != null) {
		    	return users.getEmail();
		    }
		} catch(Exception ex) {
			return null;
		}
		return null;
	}

	public boolean deActivateUser(String email) {
		Users users = new Users();
        users.setEmail(email);
        List<Users> userList = easyDBReadDAO.get(users);
        users = FCUtil.getFirstElement(userList);
        if (users != null) {
            boolean banned = users.getIsActive();
            if (banned == false) {
                return false;
            } else {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                users.setIsActive(false);
                users.addToWhereFields("email");
                users.setLastUpdate(timestamp);
                easyDBWriteDAO.update(users);

                UserProfile userProfile = new UserProfile();
                userProfile.setFkUserId(users.getUserId());
                List<UserProfile> userProfiles = easyDBReadDAO.get(userProfile);
                for (UserProfile profile : userProfiles) {
                    profile.setIsActive(false);
                    profile.addToWhereFields("userProfileId");
                    profile.setLastUpdate(timestamp);
                    easyDBWriteDAO.update(profile);
                }
            }
        }
        return true;
	}
	
	public void deleteSessionOfUserTransaction(String email) {
		List<TxnFulfilment> txnFulfilments = txnFulfilmentDAO.findByEmail(email);
        Set<String> sessionIds = new HashSet<String>();
        for (TxnFulfilment txnFulfilment : txnFulfilments) {
            Integer homePageId = txnFulfilment.getTxnHomePageId();
            List<TxnHomePage> txnHomePages = txnHomePageDAO.findByProperty("id", homePageId);
            for (TxnHomePage txnHomePage : txnHomePages) {
                String sessionId = txnHomePage.getSessionId();
                sessionIds.add(sessionId);
            }
        }

        for (String sessionId : sessionIds) {
            sessionService.deleteSession(sessionId);
        }
	}
	
	public boolean activateUser(String email) {
		Users users = new Users();
        users.setEmail(email);
        List<Users> userList = easyDBReadDAO.get(users);
        users = FCUtil.getFirstElement(userList);
        boolean unblock = users.getIsActive();
        if (unblock == true) {
            return true;
        } else {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            users.setIsActive(true);
            users.addToWhereFields("email");
            users.setLastUpdate(timestamp);
            easyDBWriteDAO.update(users);

            UserProfile userProfile = new UserProfile();
            userProfile.setFkUserId(users.getUserId());

            List<UserProfile> userProfiles = easyDBReadDAO.get(userProfile);

            for (UserProfile profile : userProfiles) {
                profile.setIsActive(true);
                profile.addToWhereFields("userProfileId");
                profile.setLastUpdate(timestamp);
                easyDBWriteDAO.update(profile);
            }
	   }
       return false; 
	}
}