package com.freecharge.app.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.app.domain.dao.jdbc.CrossSellHistoryReadDAO;
import com.freecharge.app.domain.dao.jdbc.CrossSellHistoryWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CrossSellHistory;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.XMLConverter;
import com.freecharge.platform.metrics.MetricsClient;
@Component
public class CrossSellHistoryService {
	@Autowired
	private CrossSellHistoryWriteDAO crossSellHistoryWriteDao;
	@Autowired
	private CrossSellHistoryReadDAO crossSellHistoryReadDao;
	@Autowired
	private CommonService commonService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private FCProperties fcProperties;
	@Autowired
    private MetricsClient metricsClient;
	
	@Resource(name="XMLConverter")
	private XMLConverter xmlConverter;
	
	private static Logger logger = LoggingFactory.getLogger(CrossSellHistoryService.class);
	
	
	public void saveCrossSellHistory(List<Map<String, Object>> couponsData, String orderId){
		long startTime = System.currentTimeMillis();
		logger.info("Saving cross sell history for orderId: " + orderId);
		if (couponsData != null && couponsData.size() > 0){
			List<CrossSellHistory> crossSellHistoryList = new ArrayList<CrossSellHistory>();
			for (Map<String, Object> couponData : couponsData){
				if (FCConstants.COUPON_TYPE_C.equals(couponData.get("couponType"))){
					addCrossSellHistory(orderId, crossSellHistoryList, couponData);
				}
			} // end for
			createCrossSellHistoryList(crossSellHistoryList);
		} // end if
		logger.info("Finished saving coupon history for orderId: " + orderId);
		long endTime = System.currentTimeMillis();
		metricsClient.recordLatency("CrossSellHistoryService", "saveCrossSellHistory", endTime - startTime);
	}
	
	private void createCrossSellHistoryList(
			List<CrossSellHistory> crossSellHistoryList) {
		if (crossSellHistoryList != null && crossSellHistoryList.size() > 0){
			for (CrossSellHistory crossSellHistory : crossSellHistoryList){
				crossSellHistoryWriteDao.create(crossSellHistory);
			} // end for
		} // end if
	}

	private void addCrossSellHistory(String orderId,
			List<CrossSellHistory> crossSellHistoryList,
			Map<String, Object> couponData) {
		if (couponData != null){
			CrossSellHistory crossSellHistory = getCrossSellHistory(orderId, couponData);
			String countStr = FCUtil.getStringValue(couponData.get("count"));
			if (FCUtil.isInteger(countStr)){
				int count = Integer.parseInt(countStr);
				for (int i=0; i<count; ++i){
					crossSellHistoryList.add(crossSellHistory);
				}
			}
		} else{
			logger.error("Coupon data null while saving coupon history for order id: " + orderId);
		} // end else
	}

	private CrossSellHistory getCrossSellHistory(String orderId,
			Map<String, Object> couponData) {
		String campaignName = FCUtil.getStringValue(couponData.get("couponName"));
		String crossSellLabel = FCUtil.getStringValue(couponData.get("crossSellLabel"));
		String crossSellAmount = FCUtil.getStringValue(couponData.get("crosssellamount"));
		String crossSellValidFrom = FCUtil.getStringValue(couponData.get("crossSellValidFrom"));
		String crossSellValidTo = FCUtil.getStringValue(couponData.get("crossSellValidTo"));
		String crossSellIsActive = FCUtil.getStringValue(couponData.get("crossSellIsActive"));
		String cityIdList = FCUtil.getStringValue(couponData.get("cityIdList"));
		String blockTime = FCUtil.getStringValue(couponData.get("blockTime"));
		String couponCode = FCUtil.getStringValue(couponData.get("couponCode"));
		String couponType = FCUtil.getStringValue(couponData.get("couponType"));
		String isComplimentary = FCUtil.getStringValue(couponData.get("isComplimentary"));
		String isUnique = FCUtil.getStringValue(couponData.get("isUnique"));
		String isMCoupon = FCUtil.getStringValue(couponData.get("isMCoupon"));
		String couponStoreValidityDate = FCUtil.getStringValue(couponData.get("expireDate"));
		
		CrossSellHistory crossSellHistory = new CrossSellHistory();
		crossSellHistory.setCampaignName(campaignName);
		crossSellHistory.setCrossSellData(String.valueOf(couponData.get("crosssellxml")));
		crossSellHistory.setCrossSellLabel(crossSellLabel);
		populateCrossSellPrice(crossSellAmount, crossSellHistory);
		crossSellHistory.setCrossSellValidFrom(crossSellValidFrom);
		crossSellHistory.setCrossSellValidUpto(crossSellValidTo);
		populateCrossSellIsActive(crossSellIsActive, crossSellHistory);
		crossSellHistory.setCityNameList(commonService.getCityNameList(cityIdList));
		crossSellHistory.setCouponCode(couponCode);
		crossSellHistory.setBlockTime(blockTime);
		populateIsComplimentary(isComplimentary, crossSellHistory);
		populateIsUnique(isUnique, crossSellHistory);
		populateIsMCoupon(isMCoupon, crossSellHistory);
		crossSellHistory.setCouponStoreValidityDate(couponStoreValidityDate);
		crossSellHistory.setOrderId(orderId);
		crossSellHistory.setCouponType(couponType);
		populateUserId(crossSellHistory, orderId);
		return crossSellHistory;
	}

	private void populateIsMCoupon(String isMCoupon,
			CrossSellHistory crossSellHistory) {
		if (FCUtil.isBoolean(isMCoupon)){
			crossSellHistory.setIsMCoupon(Boolean.parseBoolean(isMCoupon));
		}
	}

	private void populateIsUnique(String isUnique,
			CrossSellHistory crossSellHistory) {
		if (FCUtil.isBoolean(isUnique)){
			crossSellHistory.setIsUnique(Boolean.parseBoolean(isUnique));
		}
	}

	private void populateIsComplimentary(String isComplimentary,
			CrossSellHistory crossSellHistory) {
		if (FCUtil.isBoolean(isComplimentary)){
			crossSellHistory.setIsComplimentary(Boolean.parseBoolean(isComplimentary));
		}
	}

	private void populateCrossSellIsActive(String crossSellIsActive,
			CrossSellHistory crossSellHistory) {
		if (FCUtil.isBoolean(crossSellIsActive)){
			crossSellHistory.setCrossSellIsActive(Boolean.parseBoolean(crossSellIsActive));
		}
	}

	private void populateCrossSellPrice(String crossSellAmount,
			CrossSellHistory crossSellHistory) {
		if (FCUtil.isInteger(crossSellAmount)){
			crossSellHistory.setCrossSellPrice(Integer.parseInt(crossSellAmount));
		}
	}

	private void populateUserId(CrossSellHistory crossSellHistory, String orderId) {
		Integer userId = orderService.getUserIdForOrder(orderId);
		if (userId != null ){
			crossSellHistory.setUserId(userId);
		} else{
			logger.error("Null user id while saving cross sell history for order id: " + orderId);
		}
	}
	
	public List<CrossSellHistory> getCrossSells(List<String> orderIds) {
		return crossSellHistoryReadDao.getCrossSellsByOrderIds(orderIds);
	}

	public List<CouponHistory> getCrossSellsAsCoupons(List<String> orderIds) {
		List<CrossSellHistory> crossSells = getCrossSells(orderIds);
		List<CouponHistory> coupons = new ArrayList<CouponHistory>();
		if (crossSells != null && crossSells.size() > 0){
			for (CrossSellHistory crossSell : crossSells){
				CouponHistory coupon = getCoupon(crossSell);
				if (coupon != null){
					coupons.add(coupon);
				} else{
					logger.error("Error converting cross sell to coupon.");
				}
				
			}
		}
		return coupons;
	}

	private CouponHistory getCoupon(CrossSellHistory crossSell) {
		CouponHistory coupon = null;
		try {
			coupon = new CouponHistory();
			String crosssellXML = crossSell.getCrossSellData();
			coupon.setCouponType(FCConstants.COUPON_TYPE_C);
			CrosssellBusinessDo crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);
			coupon.setTermsAndConditions(crosssellBusinessDo.getMailTnc());
			String imgPrefix = fcProperties.getProperty("imgprefix1");
			coupon.setCouponImagePath(imgPrefix + "/" + crosssellBusinessDo.getMailImgUrl());
			coupon.setCouponCode(crossSell.getCouponCode());
			coupon.setCampaignName(crossSell.getCampaignName());
			coupon.setCouponNature("Offer Code");
			coupon.setCreatedOn(crossSell.getCreatedOn());
			coupon.setOrderId(crossSell.getOrderId());
			coupon.setValidityDate(crossSell.getCouponStoreValidityDate());
		} catch (IOException e) {
			logger.error("Error parsing cross sell xml." + e);
		}
		return coupon;
	}
	
    public List<CrossSellHistory> getCrossSellOptinHistory(String orderId, Integer couponStoreId) {
        return crossSellHistoryReadDao.getCrossSellOptinHistory(orderId, couponStoreId);
    }

    public List<String> getCrosssellCodeByOrderIdCouponId(String orderId, Integer couponStoreId) {
        return crossSellHistoryReadDao.getCrosssellCodeByOrderIdCouponId(orderId, couponStoreId);
    }
}
