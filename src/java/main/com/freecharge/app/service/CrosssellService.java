package com.freecharge.app.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.api.coupon.service.model.CouponInventory;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.crosssell.CrossSellInterface;
import com.freecharge.app.crosssell.LeadGenerationInterface;
import com.freecharge.app.crosssell.factory.CrosssellFactory;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.jdbc.CrosssellReadDAO;
import com.freecharge.app.domain.dao.jdbc.CrosssellWriteDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.CouponInventoryBusinessDO;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.XMLConverter;
import com.freecharge.web.webdo.CrosssellWebDo;

/**
 * Created by IntelliJ IDEA. User: Manoj Kumar Date: May 11, 2012 Time: 11:52:22 AM To change this template use File | Settings | File Templates.
 */
@Service
public class CrosssellService {
	private static Logger logger = LoggingFactory.getLogger(CrosssellService.class);

	@Autowired
	private CrosssellReadDAO crosssellReadDAO;

	@Autowired
	private CrosssellWriteDAO crosssellWriteDAO;

	@Autowired
	HomeBusinessDao homeBusinessDao;

	@Autowired
	private VoucherService voucherService;

	@Resource(name="XMLConverter")
	private XMLConverter xmlConverter;
	
	@Autowired
	private CrosssellFactory crosssellFactory;
	
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
	@Autowired
	private CartService cartService;
	
	@Autowired
    private IRegistrationDAO registrationDAO;
	
	@Autowired
    SMSService smsService;

	@Autowired
	private CrossSellHistoryService crossSellHistoryService;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	public void setXmlConverter(XMLConverter xmlConverter) {
		this.xmlConverter = xmlConverter;
	}

	public List<CrosssellBusinessDo> getAllCrosssell() {
		List<Crosssell> crosssells = crosssellReadDAO.getAllCrosssell();
		
		List<CrosssellBusinessDo> crosssellBusinessDos = new ArrayList<CrosssellBusinessDo>();
		
		Iterator<Crosssell> iterator = crosssells.iterator();
		
		while (iterator.hasNext()) {
			try {

				Crosssell crosssell = iterator.next();
				String crosssellXML = crosssell.getXml();
				logger.debug("Processing crosssellXML" + crosssellXML);
				CrosssellBusinessDo crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);
				crosssellBusinessDo.setId(crosssell.getId().longValue());
				crosssellBusinessDo.setCrosssell(crosssell);
				crosssellBusinessDos.add(crosssellBusinessDo);
			} catch (IOException e) {
				logger.error("error in getAllCrosssell " + e);
			}
		}
		return crosssellBusinessDos;
	}
	
	@Transactional
	public List<CrosssellBusinessDo> getAllCrosssellCoupon(Float amt, String productType) {

		List<Crosssell> crosssells = null;
		List<CrosssellBusinessDo> crosssellBusinessDos = null;
			FCConstants.crosssellCoupons = new ArrayList<CrosssellBusinessDo>();
			crosssellBusinessDos = new ArrayList<CrosssellBusinessDo>();
			crosssells = crosssellReadDAO.findAll();

			Iterator<Crosssell> iterator = crosssells.iterator();
			while (iterator.hasNext()) {
				try {

					Crosssell crosssell = iterator.next();
					String crosssellXML = crosssell.getXml();
					logger.debug("Processing crosssellXML" + crosssellXML);
					CrosssellBusinessDo crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);
					crosssellBusinessDo.setId(crosssell.getId().longValue());
					if (crosssell.getCountUses() <= crosssellBusinessDo.getMaxuses()) {
						String[] productArray = productType.split(FCConstants.CROSSSELL_PRODUCT_DELIMITER);

						if (amt >= crosssellBusinessDo.getPrimaryproductamount() && productType.matches(crosssellBusinessDo.getPrimaryproduct())) {
							crosssellBusinessDos.add(crosssellBusinessDo);
							
						}						
					}
				} catch (IOException e) {
					logger.error("error in getAllCrosssellCoupon " + e);
				}
			}
		if(crosssellBusinessDos != null)	
			logger.debug("getAllCrosssellCoupon " + crosssellBusinessDos.size());	
		return crosssellBusinessDos;
	}

	@Transactional
	public List<CrosssellWebDo> getCrosssellCouponByProperty(Integer amount, String category) {
		List<CrosssellWebDo> crosssellCoupons = new ArrayList<CrosssellWebDo>();
		List<Crosssell> crosssells = crosssellReadDAO.getCrosssellCouponByProperty(amount, category);
		Iterator<Crosssell> iterator = crosssells.iterator();
		
		while (iterator.hasNext()) {
			try {
				Crosssell crosssell = (Crosssell) iterator.next();

				CrosssellWebDo crosssellWebDo = new CrosssellWebDo();

				String crosssellXML = crosssell.getXml();

				CrosssellBusinessDo crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);

				crosssellWebDo.setId(crosssell.getId().longValue());
				crosssellWebDo.setCategory(crosssell.getCategory());

				crosssellCoupons.add(crosssellWebDo);
			} catch (IOException e) {
				logger.error("error in getAllCrosssellCoupon " + e);
				e.printStackTrace();
			}
		}
		return crosssellCoupons;
	}

	@Transactional
	public boolean update(Crosssell crosssell) {
		crosssellWriteDAO.update(crosssell);
		return true;
	}
	
	@Transactional
	public List<Map<String, Object>> getCrossSellDetailsBylookupId(String lookupId) {
		logger.debug("Request comes into the getCrossSellDetailsBylookupId method of the class Crosssellservice" + lookupId);
		List<Map<String, Object>> crosssellList = null;
		List<Map<String, Object>> objectMap = new ArrayList<Map<String, Object>>();
		try {

			crosssellList = crosssellReadDAO.getCrossSellDetailsBylookupId(lookupId);

			if (crosssellList != null && crosssellList.size() > 0) {
				for (Map<String, Object> crosssell : crosssellList) {
					String crosssellXML = crosssell.get("crosssellxml").toString();
					CrosssellBusinessDo crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);
					
					crosssell.put("crosssellid",crosssellBusinessDo.getId());
					crosssell.put("crosssellname",crosssellBusinessDo.getTitle());
					crosssell.put("crosssellamount",crosssellBusinessDo.getPrice());
					crosssell.put("crosssellcategory",crosssellBusinessDo.getCategory());
					crosssell.put("crosssellorder",crosssellBusinessDo.getOrder());
					crosssell.put("crosssellrefundable",crosssellBusinessDo.isRefundable());
					crosssell.put("couponImagePath",crosssellBusinessDo.getImgurl());
					crosssell.put("crosssellheadDescription",crosssellBusinessDo.getHeadDescription());
					crosssell.put("crossselldescription",crosssellBusinessDo.getDescription());
					crosssell.put("crosssellprimaryproduct",crosssellBusinessDo.getPrimaryproduct());
					crosssell.put("crosssellprimaryproductamount",crosssellBusinessDo.getPrimaryproductamount());
					crosssell.put("crosssellothercondition",crosssellBusinessDo.getOthercondition());
					crosssell.put("crosssellmaxuses",crosssellBusinessDo.getMaxuses());
					crosssell.put("crosssellstoreId",crosssellBusinessDo.getStoreId());
					crosssell.put("termsConditions", crosssellBusinessDo.getTnc());
					crosssell.put("crosssellClassName", crosssellBusinessDo.getClassName());
					crosssell.put("crosssellTemplate", crosssellBusinessDo.getTemplate());
					crosssell.put("crosssellMailTnc", crosssellBusinessDo.getMailTnc());
					crosssell.put("crosssellMailSubject", crosssellBusinessDo.getMailSubject());
					crosssell.put("crosssellMailImgUrl", crosssellBusinessDo.getMailImgUrl());
					crosssell.put("couponType", "C");
					crosssell.put("couponStoreId", crosssell.get("couponStoreId"));
					crosssell.put("count", crosssell.get("count"));
					crosssell.put("isUnique", crosssell.get("isUnique"));
					
					objectMap.add(crosssell);
				}
			}
		} catch (Exception e) {
			logger.error("getCouponDetailsForMail failed", e);
		}
		logger.debug("Request goes out from the getCrossSellDetailsBylookupId method of the class Crosssellservice" + objectMap);
		return objectMap;
	}
	
	@Transactional
	public CrosssellBusinessDo getCrosssell(Integer id) {
		Crosssell crosssell = crosssellReadDAO.getCrosssell(id); 
		CrosssellBusinessDo crosssellBusinessDo = null;

		try {
		
			String crosssellXML = crosssell.getXml();
			crosssellBusinessDo = (CrosssellBusinessDo) xmlConverter.convertFromXMLStringToObject(crosssellXML);
			crosssellBusinessDo.setId(crosssell.getId().longValue());
			crosssellBusinessDo.setCrosssell(crosssell);
			
		} catch (IOException e) {
			logger.error("error in getAllCrosssellCoupon " + e);
		}

		return crosssellBusinessDo;
	}
	
	public boolean processCrossSells (List<Map<String, Object>> crossSellList, UserContactDetail contactDetail, String orderId) {
		for(Map<String, Object> map: crossSellList) {
	        if(map.get("crosssellcategory") !=null && map.get("crosssellcategory").equals(FCConstants.CROSS_SELL_TYPE_LEAD_GEN)) {
	        	LeadGenerationInterface leadGenerationInterface = (LeadGenerationInterface)crosssellFactory.getCrossSellBean((String)map.get("crosssellClassName")); 
	        	leadGenerationInterface.processCrossSell(contactDetail, Integer.parseInt(map.get("crosssellid").toString()), (String)map.get("crosssellTemplate"));
	        }else if(map.get("crosssellcategory") !=null && map.get("crosssellcategory").equals(FCConstants.CROSS_SELL_TYPE_CROSS_SELL)){
	        	logger.info("OrderId : " + orderId +" cross sell bean name: "+map.get("crosssellClassName") +" and type = "+map.get("couponType")+ " and store id is: "+map.get("couponStoreId"));
	        	List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<Map<String, Object>>();
                modifiedcouponListForEmail = addCrossSellCodesinList(map, orderId);
	        	CrossSellInterface crossSellInterface = (CrossSellInterface)crosssellFactory.getCrossSellBean((String)map.get("crosssellClassName")); 
	        	crossSellInterface.processCrossSell(contactDetail, modifiedcouponListForEmail, (String)map.get("crosssellTemplate"), orderId);
	        	crossSellHistoryService.saveCrossSellHistory(modifiedcouponListForEmail, orderId);
	        }
		}
		
		return true;
	}
	
	public List<Map<String, Object>> addCrossSellCodesinList(Map<String, Object> couponList, String orderId) {
		List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<Map<String, Object>>();
				String lookupId = orderIdSlaveDAO.getLookupIdForOrderId(orderId);
				List<Map<String, Object>> couponListMap = voucherService.getCouponDetailsBylookupId(lookupId);
				CouponInventoryBusinessDO couponInventoryBusinessDO = voucherService.getCouponInventoryBusinessDO(couponListMap, orderId, lookupId);
				couponInventoryBusinessDO = voucherService.CouponCodeMap(couponInventoryBusinessDO);
		
			if (couponInventoryBusinessDO != null) {
				Map<String, List<String>> couponCodes = couponInventoryBusinessDO.getCouponCodes();
				if(couponCodes != null) {
					logger.debug("couponList = "+couponList);
						if(couponList.get("couponType") != null && couponList.get("couponType").equals("C")) {
							logger.debug("couponType = "+couponList.get("couponType"));
							if (couponList.get("count") != null && couponList.get("couponStoreId") != null) {
								logger.debug("couponCount = "+couponList.get("count"));
								logger.debug("couponCount = "+couponList.get("couponStoreId"));
								
								Integer couponCount = Integer.parseInt(couponList.get("count").toString());
								logger.debug("couponCount = "+couponCount);
								
								String couponStoreId = couponList.get("couponStoreId").toString();
								
								for(int j = 0; j < couponCount; j ++) {
									Map<String, Object> map = new HashMap<String, Object>();
									map.putAll(couponList);
									logger.debug("all coupon codes = "+ couponCodes);
									Boolean isUnique = new Boolean(map.get("isUnique").toString());
									if(couponCodes.get(couponStoreId) != null && couponCodes.get(couponStoreId).size() == couponCount){
										logger.debug("couponcode for mail = "+ couponCodes.get(couponStoreId).get(j));
										map.put("couponCode", couponCodes.get(couponStoreId).get(j));
									}else if(!isUnique) {
										if(couponCodes.get(couponStoreId) != null) {
											logger.debug("couponcode for mail = "+ couponCodes.get(couponStoreId).get(0));
											map.put("couponCode", couponCodes.get(couponStoreId).get(0));
										}else {
											logger.debug("There is an error for a uniue coupon in inventory for which coupon codes are not received. coupon_store_id is = "+couponStoreId+" order_id is = "+orderId);
										}
									}
									
									map.put("count", "1");
									modifiedcouponListForEmail.add(map);																
								}

							}else {
								modifiedcouponListForEmail.add(couponList);
							}
						}else {
							modifiedcouponListForEmail.add(couponList);
						}
				} else {
					logger.error("couponCodes is null");
				}
			} else {
				logger.error("couponInventoryBusinessDO is null");
			}
		
		if(modifiedcouponListForEmail.isEmpty()) {
			logger.error("modifiedcouponListForEmail is empty");
		}
		return modifiedcouponListForEmail;
	}
	
	
    public HashMap<String, Object> getCrossSellCountAndAmountForInvoice(String orderId) {
    	String lookupId = orderIdSlaveDAO.getLookupIdForOrderId(orderId);
    	Float amount = new Float(0.0f);
    	Integer count = 0;
    	CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
    	List<CartItemsBusinessDO> ctbs = cartBusinessDo.getCartItems();
		for (CartItemsBusinessDO ctb : ctbs) {
			if (!ctb.getDeleted()) {
				if(FCConstants.ENTITY_NAME_FOR_CROSSSELL.equals(ctb.getEntityId())) {
					amount += ctb.getPrice().floatValue();
					count += 1;
				}
			}
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("amount", amount);
		
		return map;
    }
    
//    @Transactional
//    public Integer getPriceFromCrosssellbyCrosssellId(Integer id) {
//    	return crosssellReadDAO.getPriceFromCrosssellbyCrosssellId(id);
//    }
//    
    public boolean create(Crosssell crosssell) {
    	return crosssellWriteDAO.create(crosssell);
    }
    
    public boolean create(CouponStore couponStore) {
    	return couponServiceProxy.createCoupon(couponStore);
    }
    
    public boolean create(CouponInventory couponInventory) {
    	return couponServiceProxy.createCouponInventory(couponInventory);
    }
    
    public boolean deactivateCoupon(Integer couponStoreId) {
    	return couponServiceProxy.deactivateCoupon(couponStoreId);
    }
    
    public boolean activateCoupon(Integer couponStoreId) {
    	return couponServiceProxy.activateCoupon(couponStoreId);
    }
    
    @Transactional
	public boolean create(Crosssell crosssell, CouponStore couponStore,
			CrosssellBusinessDo crosssellBusinessDo, CouponInventory couponInventory) throws Exception {
    	boolean status = false;
    	status = create(crosssell);
    	if(!status) throw new Exception("Crosssell creattion failed.");
    	
    	couponStore.setFkCrosssellId(crosssell.getId());
    	status = create(couponStore);
    	if(!status) throw new Exception("CouponStore creattion failed.");
    	
    	couponInventory.setFkCouponStoreId(couponStore.getCouponStoreId());
    	status = create(couponInventory);
    	if(!status) throw new Exception("CouponInventory creattion failed.");
    	
    	crosssellBusinessDo.setId((long)crosssell.getId());
    	crosssellBusinessDo.setStoreId(couponStore.getCouponStoreId());
    	String xmlString = xmlConverter.convertFromObjectToXML(crosssellBusinessDo);
    	crosssell.setXml(xmlString);
		status = update(crosssell);
		if(!status) throw new Exception("Crosssell updation failed. " + xmlString);
		
		return status;
	}
    
	public List<CouponCode> getAllCouponCodes(Integer couponInventoryId) {
		return  couponServiceProxy.getAllCouponCodes(couponInventoryId);
	}
	public List<CouponCode> getUnusedCouponCodes(Integer couponInventoryId) {
		return  couponServiceProxy.getUnusedCouponCodes(couponInventoryId);
	}
    public CouponStore getCouponStore(Integer couponStoreId) {
    	return  couponServiceProxy.getCouponStore(couponStoreId);
    }
    
    public CouponInventory getCouponInventoryByCouponStoreId(Integer couponStoreId) {
    	return  couponServiceProxy.getCouponInventoryByCouponStoreId(couponStoreId);
    }
    
    public boolean updateInventory(CouponInventory couponInventory) {
    	return couponServiceProxy.updateInventory(couponInventory);
    }
    
    public boolean updateCoupon(CouponStore couponStore) {
    	return couponServiceProxy.updateCoupon(couponStore);
    }
    
    @Transactional
	public boolean uploadCodes(CouponStore couponStore, CouponInventory couponInventory,
			List<CouponCode> newCodes) {
    	couponServiceProxy.upload(newCodes);
    	int quantity = couponInventory.getCouponCount()+newCodes.size();
    	couponServiceProxy.updateCouponCount(couponInventory.getCouponInventoryId(), quantity);
		return true;
	}
    
    @Transactional
	public boolean blockCodes(CouponStore couponStore, CouponInventory couponInventory,
			List<CouponCode> codesToBlock, String reasonToBlock) {
    	couponServiceProxy.block(codesToBlock, reasonToBlock);
    	couponServiceProxy.updateCouponInventoryCount(couponInventory.getCouponInventoryId(), codesToBlock.size());
		return true;
	}
    
    @Transactional
	public boolean update(Crosssell crosssell, CouponStore couponStore,
			CrosssellBusinessDo crosssellBusinessDo, CouponInventory couponInventory) throws Exception {
		boolean status = false;
		
		String xmlString = xmlConverter.convertFromObjectToXML(crosssellBusinessDo);
    	crosssell.setXml(xmlString);
		status = update(crosssell);
		if(!status) throw new Exception("Crosssell updation failed. " + xmlString);
		
    	status = updateCoupon(couponStore);
    	if(!status) throw new Exception("CouponStore creattion failed.");
    	
    	status = updateInventory(couponInventory);
    	if(!status) throw new Exception("CouponInventory creattion failed.");
		
		return status;
	}
    

    public List<Map<String, Object>> addCouponCodesinList(List<Map<String, Object>> couponList, String orderId,
            FreechargeSession freechargeSession) {
        List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<>();
        return modifiedcouponListForEmail;
    }
	
//	public void crossSellHandling(UserDetails UserDetailsObj) {
//	    List<Map<String, Object>> couponCrossSellList = voucherService.getCouponDetailsBylookupId(UserDetailsObj.getLookupId());
//        List<Map<String, Object>> crossSellList = new ArrayList<Map<String, Object>>();
//
//        if (couponCrossSellList != null) {
//            for (Map<String, Object> coupons : couponCrossSellList) {
//                if (coupons.get("couponType").equals("C")) {
//                    crossSellList.add(coupons);
//                }
//            }
//        }
//
//        if (crossSellList != null) {
//            if (!crossSellList.isEmpty()) {
//                voucherService.blockCrossSell(UserDetailsObj.getOrderId());
//                List<UserContactDetail> userContactDetailList = registrationDAO
//                        .getUserContactDefaultDetailsWithoutAddressChecks(UserDetailsObj.getUserEmail());
//                if (userContactDetailList == null || userContactDetailList.isEmpty()) {
//                    throw new FCRuntimeException("UserContactDetails not found for user id: " + UserDetailsObj.getUserEmail()
//                            + " while processing crosssells.");
//                }
//                processCrossSells(crossSellList, userContactDetailList.get(0), UserDetailsObj.getOrderId());
//
//                // Sending sms for crossSellCoupon in case of
//                // recharge failed
//                smsService.sendCrossCellCouponSMS(couponCrossSellList, UserDetailsObj.getUserMobileNo(), UserDetailsObj.getUserEmail());
//            }
//        }
//	}
}
