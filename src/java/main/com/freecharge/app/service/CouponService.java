package com.freecharge.app.service;


import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.api.coupon.service.model.CouponOrderIdTracker;
import com.freecharge.api.coupon.service.web.model.*;
import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.dao.CouponCartDao;
import com.freecharge.app.domain.dao.CouponTxnDao;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.platform.metrics.MetricsClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

@Service
public class CouponService {

	@Autowired
	private CityMasterDAO cityMasterDAO;
	
	
	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@Autowired
	private CouponCartDao couponCartDao;
	
	@Autowired
	private CouponTxnDao couponTxnDao;
	
	@Autowired
	private CouponCartService couponCartService;
	
	@Autowired
    private OrderService orderService;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

    private MetricsClient metricsClient;

	private static Logger logger = LoggingFactory.getLogger(CouponService.class);
	
	public List<String> getCouponCodesForNonPhysicalCoupons(String orderId, String couponStoreId, Boolean isUnique, int count) {
		return null;
	}

	public List<CouponCode> getCouponCodesForNonPhysicalCoupons(
			String orderId, String couponStoreId, Boolean isUnique) {
		return null;
    }
    
    public CouponStore getCouponStoreByCrosssellId(Integer crosssellId) {
        return this.couponServiceProxy.getCouponStoreByCrosssellId(crosssellId);
    }

    public CouponStore getCouponStore(int id){
    	return couponServiceProxy.getCouponStore(id);
    }
    
    public List<CouponStore> getAllCouponStores(CartBusinessDo cartBusinessDo) {
        List<CouponStore> couponStores = new LinkedList<CouponStore>();

        for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo.getCartItems()) {
            switch (cartService.getProductName(cartItemsBusinessDO)) {
                case Coupons:
                	CouponStore couponStore = couponServiceProxy.getCouponStore(cartItemsBusinessDO.getItemId());
                	if(couponStore!=null) {
                		couponStores.add(couponStore);
                	}
                    continue;
                    
                case PriceCoupons:
                	CouponStore pCouponStore = couponServiceProxy.getCouponStore(cartItemsBusinessDO.getItemId());
                	if(pCouponStore!=null) {
                		couponStores.add(pCouponStore);
                	}
                    continue;
                    
                case Crosssell:
                	CouponStore cCouponStore =this.getCouponStoreByCrosssellId(cartItemsBusinessDO.getItemId());
                	if(cCouponStore!=null) {
                		couponStores.add(cCouponStore);
                	}
                    continue;
                case HCoupons:
                	CouponStore hCouponStore = couponServiceProxy.getCouponStore(cartItemsBusinessDO.getItemId());
                	if(hCouponStore!=null) {
                		couponStores.add(hCouponStore);
                	}
                    continue;   
                    
                default:
                    continue;
            }
        }

        return couponStores;
    }
    
    @Transactional
	public List<CouponCode> getCouponCodesFromCouponStoreAndOrderId(Integer couponStoreId, String orderId) {
		return couponServiceProxy.getCouponCodesFromCouponStoreAndOrderId(couponStoreId, orderId);
	}
    @Transactional
    public List<CouponCode>getCouponCodesFromCouponStoreIdForNonUniqueCoupons(Integer couponStoreId) {
    	return couponServiceProxy.getCouponCodesFromCouponStoreIdForNonUniqueCoupons(couponStoreId);
    }
    
    public CouponVO getCouponInfo(Integer couponStoreId) {
    	try {
    	List<VoucherCat> categories;
    	
    	try {
			categories = couponServiceProxy.getAllVoucherCategoriesOrdered();
			couponServiceProxy.setVoucherCategories(categories);
		} catch (Exception ex) {
			logger.error("error occur while calling  the getVoucherCategories() method :" + ex);
			ex.printStackTrace();
			categories = couponServiceProxy.getAllVoucherCategoriesOrdered();
		}
    	
    	return getCouponVO(couponStoreId);
    	} catch (Exception e) {
    		logger.error("Error while fetching coupon information for coupon : " + couponStoreId, e);
    	}
    	return null;
    }

	public CouponVO getCouponVO(Integer couponStoreId) {
		List<VoucherCat> categories = couponServiceProxy.getAllVoucherCategoriesOrdered();
		CouponStore couponStore = this.couponServiceProxy.getCouponStore(couponStoreId);

    	if (couponStore != null) {
    		
    		CouponVO voucherVO = new CouponVO();
    		voucherVO.setVoucherId(couponStore.getCouponStoreId());
    		voucherVO.setVoucherName(couponStore.getCampaignName());
    		voucherVO.setVoucherAmount(couponStore.getCouponValue());
    		voucherVO.setVoucherType(couponStore.getCouponType());
    		voucherVO.setVoucherImgUrl(fcProperties.getAbsoluteImgUrl(couponStore.getCouponImagePath()));
    		voucherVO.setTermsConditions(couponStore.getTermsConditions() != null ? (couponStore.getTermsConditions().replaceAll("(\\r)", "<br />")) : "");
    		voucherVO.setVoucherUrl(couponStore.getCampaignName());
    		voucherVO.setCouponRemainingCount(0);
    		voucherVO.setFaceValue(couponStore.getFaceValue());
    		voucherVO.setMinRedemptionAmount(couponStore.getMinRedemAmt());
    		voucherVO.setVoucherCat(categories.get(categories.indexOf(new VoucherCat(couponStore.getFkVoucherCatId()))).getCatName());
    		voucherVO.setCouponShortDesc(couponStore.getCouponShortDesc());
            voucherVO.setValidityDate(new Date(couponStore.getValidityDate().getTime()));

    		StringBuffer sb = new StringBuffer();
    		String[] cityIdArray = couponStore.getCityIdList().split(",");

    		for (int i = 0; i < cityIdArray.length; i++) {
    			String cityName=cityMasterDAO.getCityNameById(cityIdArray[i]);
    			sb.append(", " + cityName);
    		}

    		voucherVO.setCityNames(sb.replace(0, 1, "").toString());

    		return voucherVO;
    	}
    	return null;
	}

    public static void main(String[] args) {
        /*ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        CouponService couponService = ((CouponService) ctx.getBean("couponService"));
        CouponStore couponStore = couponReadDAO.getCouponStore(259);
        System.out.println(couponStore);
        ctx.close();*/
    }
    
    public List<Map<String, Object>> addCouponCodesinList(List<Map<String, Object>> couponList, String orderId,
            String lookupId) {
        return null;
    }
    
    public Integer getMissionIdForCoupon(int couponStoreId){
    	return couponServiceProxy.getMissionIdByCouponId(couponStoreId);
    }
    
    public List<Map<String, Object>> getCouponDetailsFromCartBylookupIdAndCouponType(String lookupId, String couponType){
		List<Map<String, Object>> objectMap = new ArrayList<Map<String, Object>>();
    	List<Integer> couponIds = couponCartDao.getCouponIdsFromCartBylookupId(lookupId);
    	if(couponIds!=null && couponIds.size()>0) {
    		objectMap=couponServiceProxy.getCouponDetailsFromCouponIdsAndCouponType(couponIds, couponType);
    	}
    	return objectMap;
    }
    
    public List<Map<String, Object>> getCouponDetailsBylookupId(String lookupId){
    	List<Map<String, Object>> objectMap = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> couponList = couponTxnDao.getCouponIdsBylookupId(lookupId);
		List<Integer> couponIds = new ArrayList<>();
		Map<String,Integer> quantityMap = new HashMap<>();
    	if(couponList!=null && couponList.size()>0) {
    		for(Map<String,Object> map:couponList) {
    			Integer quantity  = Integer.parseInt(map.get("quantity").toString());
    			String couponStoreId = map.get("couponStoreId").toString();
    			couponIds.add(Integer.parseInt(couponStoreId));
    			if(quantityMap.get(couponStoreId)!=null) {
    				quantity=quantity+quantityMap.get(couponStoreId);
    			}
    			quantityMap.put(couponStoreId, quantity);
    		}
    		objectMap=couponServiceProxy.getCouponDetailsFromCouponIds(couponIds);
    		if(objectMap!=null && objectMap.size()>0) {
    			for(Map<String,Object> map:objectMap) {
    				Integer quantity = quantityMap.get(map.get("couponStoreId").toString());
    				if(quantity!=null) {
    					map.put("quantity",quantity);
    				}
    			}
    		}
    	}
    	return objectMap;
    }
    
    public List<Map<String, Object>> getCouponDetailsBylookupIdAndCouponType(String lookupId,String couponType){
		List<Map<String, Object>> objectMap = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> couponList = couponTxnDao.getCouponIdsBylookupId(lookupId);
		List<Integer> couponIds = new ArrayList<>();
		Map<String,Integer> quantityMap = new HashMap<>();
    	if(couponList!=null && couponList.size()>0) {
    		for(Map<String,Object> map:couponList) {
    			Integer quantity  = Integer.parseInt(map.get("quantity").toString());
    			String couponStoreId = map.get("couponStoreId").toString();
    			couponIds.add(Integer.parseInt(couponStoreId));
    			if(quantityMap.get(couponStoreId)!=null) {
    				quantity=quantity+quantityMap.get(couponStoreId);
    			}
    			quantityMap.put(couponStoreId, quantity);
    		}
    		objectMap=couponServiceProxy.getCouponDetailsFromCouponIdsAndCouponType(couponIds, couponType);
    		if(objectMap!=null && objectMap.size()>0) {
    			for(Map<String,Object> map:objectMap) {
    				Integer quantity = quantityMap.get(map.get("couponStoreId").toString());
    				if(quantity!=null) {
    					map.put("quantity",quantity);
    				}
    			}
    		}
    	}
    	return objectMap;
    }
    
    public void blockAllCartCouponsForFreechargeOrder(String orderId) {
    	try {
	    	int couponScaleUpFactor = cartService.getCouponScaleUpFactorForOrder(orderId);
	    	List<CouponCartItem> couponCartItems = couponCartService.getCouponCartItemsForOrderId(orderId);
	    	List<BlockCouponRequest> requestList = new ArrayList<BlockCouponRequest>();
			if(couponCartItems == null || couponCartItems.size() == 0) {
				logger.error("No coupons in cart items for orderId: " + orderId);
				return;
			}
			Integer userId = orderService.getUserIdForOrder(orderId);
			int count=0;
			for(CouponCartItem couponCartItem:couponCartItems) {
				if(count==3) {
					couponServiceProxy.blockMultipleCouponsForOrder(requestList,userId);
					count = 0;
					requestList = new ArrayList<BlockCouponRequest>();
				}
				final Integer couponId = couponCartItem.getCouponId();	
				final Integer couponsQuantity = couponCartItem.getQuantity();
				int quantityToBlock = couponsQuantity * couponScaleUpFactor;
				BlockCouponRequest request = new BlockCouponRequest();
				request.setOrderId(orderId);
				request.setCouponStoreId(couponId);
				request.setQuantity(quantityToBlock);
				CouponOptinType optinType = couponCartItem.getType();
				request.setCouponOptinType(optinType);
				requestList.add(request);
				count++;
			}
			if(requestList!=null && requestList.size()>0) {
				couponServiceProxy.blockMultipleCouponsForOrder(requestList,userId);
			}
    	} catch(Exception e) {
    		logger.error("Error in blockAllCartCouponsForFreechargeOrder",e);
    	}
    }
    
    public void doCouponFulfillment(String orderId,Map<String, String> variableValues){
    	try {
	    	int couponScaleUpFactor = cartService.getCouponScaleUpFactorForOrder(orderId);
	    	List<CouponCartItem> couponCartItems = couponCartService.getCouponCartItemsForOrderId(orderId);
	    	List<BlockCouponRequest> requestList = new ArrayList<BlockCouponRequest>();
			if(couponCartItems == null || couponCartItems.size() == 0) {
				logger.error("No coupons in cart items for orderId: " + orderId);
				return;
			}
			Integer userId = orderService.getUserIdForOrder(orderId);
			for(CouponCartItem couponCartItem:couponCartItems) {
				final Integer couponId = couponCartItem.getCouponId();	
				final Integer couponsQuantity = couponCartItem.getQuantity();
				int quantityToBlock = couponsQuantity * couponScaleUpFactor;
				BlockCouponRequest request = new BlockCouponRequest();
				request.setOrderId(orderId);
				request.setCouponStoreId(couponId);
				request.setQuantity(quantityToBlock);
				CouponOptinType optinType = couponCartItem.getType();
				request.setCouponOptinType(optinType);
				requestList.add(request);
			}
			if(requestList!=null && requestList.size()>0) {
				couponServiceProxy.doCouponFulfillment(requestList,userId,variableValues);
			}
    	} catch(Exception e) {
    		logger.error("Error in blockAllCartCouponsForFreechargeOrder",e);
    	}
    }

    public BlockedCouponResponse doAllTypeCouponFulfilment(List<CouponCartItem> coupons, String orderId, Map<String, String> couponDataMap) throws Exception {
		boolean isRefunded = false;
		String reason = null;
		Integer couponId = null;
		boolean isSuccess = false;

		int couponScaleUpFactor = cartService.getCouponScaleUpFactorForOrder(orderId);
		
		couponDataMap.put("couponScaleUpFactor", String.valueOf(couponScaleUpFactor));
		couponDataMap.put("orderno", orderId);

		Integer userId = getUserIdForOrder(orderId);
		
		couponDataMap.put("userEmail", userServiceProxy.getEmailByUserId(userId));
		couponDataMap.put("maxQuantity", MAX_COUPONS_QUANTITY_ALLOWED);
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = couponServiceProxy.doAllTypeCouponFulfilment(coupons, orderId, userId, couponDataMap);
		} catch (Exception e) {
			logger.error("Unknown error in blocking coupons for orderId: " + orderId, e);
			reason = REASON_SOMETHING_WRONG;
			couponServiceProxy.saveOrderStatus(orderId, isSuccess, reason, couponId, isRefunded);
			throw e;
		}
    	return blockedCouponResponse;
	}

	public static final String REASON_SOMETHING_WRONG = "someThingWrong";
	public static final String REASON_COUPON_OF_STOCK = "couponOutOfStock";
	public static final String REASON_COUPON_EXPIRED = "couponExpired";
	
	public static final String MAX_COUPONS_QUANTITY_ALLOWED = "10";

	public Integer couponsRangeGotFromCouponsBucket(Integer transactionAmount) {		
		if (transactionAmount > 0 && transactionAmount <= 100) {
			return 100;
		} else if (transactionAmount > 100 && transactionAmount <= 200) {
			return 200;
		} else if (transactionAmount > 200 && transactionAmount <= 300) {
			return 300;
		} else if (transactionAmount > 300 && transactionAmount <= 400) {
			return 400;
		} else if (transactionAmount > 400 && transactionAmount <= 500) {
			return 500;
		} else if (transactionAmount > 500 && transactionAmount <= 600) {
			return 600;
		} else if (transactionAmount > 600 && transactionAmount <= 700) {
			return 700;
		} else if (transactionAmount > 700 && transactionAmount <= 800) {
			return 800;
		} else if (transactionAmount > 800 && transactionAmount <= 900) {
			return 900;
		} else if (transactionAmount > 900 && transactionAmount <= 1000) {
			return 1000;
		} else if (transactionAmount > 1000 && transactionAmount <= 1500) {
			return 1500;
		} else if (transactionAmount > 1500 && transactionAmount <= 2000) {
			return 2000;
		} else if (transactionAmount > 2000 && transactionAmount <= 2500) {
			return 2500;
		} else if (transactionAmount > 2500 && transactionAmount <= 3000) {
			return 3000;
		} else if (transactionAmount > 3000 && transactionAmount <= 3500) {
			return 3500;
		} else if  (transactionAmount > 3500 && transactionAmount <= 4000) {
			return 4000;
		} else if  (transactionAmount > 4000 && transactionAmount <= 4500) {
			return 4500;
		} else if  (transactionAmount > 4500 && transactionAmount <= 5000) {
			return 5000;
		} else if  (transactionAmount > 5000 && transactionAmount <= 5500) {
			return 5500;
		} else if  (transactionAmount > 5500 && transactionAmount <= 6000) {
			return 6000;
		} else if (transactionAmount > 6000) {
			return 6000;
		}
		return 0;
	}

	public Map<String, Object> getAllowedAmountRangeAndQuantityFromCouponTracker(String orderId, Integer transactionAmount) {
		Map<String, Object> allowedAmountQuantity = new HashMap<>();
		Integer allowedCouponAmountRange =  couponsRangeGotFromCouponsBucket(transactionAmount);
		Integer allowedCouponQuantity = 10;
		CouponOrderIdTracker couponOrderIdTracker = couponServiceProxy.getCouponOrderIdTracker(orderId);
		if (couponOrderIdTracker != null) {
			allowedCouponAmountRange = allowedCouponAmountRange - couponOrderIdTracker.getTotalCouponsAmount();
			allowedCouponQuantity = allowedCouponQuantity - couponOrderIdTracker.getTotalCouponsCount();
		}
		
		allowedAmountQuantity.put("allowedAmount", allowedCouponAmountRange);
		allowedAmountQuantity.put("allowedQuantity", allowedCouponQuantity);
		return allowedAmountQuantity;
	}


	public Integer getBucketLimitOfTransaction(String orderId, UserTransactionHistory userTransactionHistory) {
		Integer bucketLimit = 0;
		if (userTransactionHistory != null && userTransactionHistory.getTransactionStatus() != null) {
	    	String txnStatus = userTransactionHistory.getTransactionStatus();
	    	if (txnStatus.equalsIgnoreCase("00") || txnStatus.equalsIgnoreCase("0")) {
	    		if (userTransactionHistory.getTransactionAmount() != null) {
	    			return couponsRangeGotFromCouponsBucket(userTransactionHistory.getTransactionAmount().intValue());
	    		}
	    	}
	    }
		return bucketLimit;
	}

	public Map<String, Object> getAllowedAmountAndQuantityByCouponTracker(String orderId, Integer txnBucketLimit) {
		Map<String, Object> allowedAmountQuantity = new HashMap<>();
		Integer allowedCouponRange = txnBucketLimit;
		Integer allowedCouponQuantity = 10;
		CouponOrderIdTracker couponOrderIdTracker = couponServiceProxy.getCouponOrderIdTracker(orderId);
		if (couponOrderIdTracker != null) {
			allowedCouponRange = allowedCouponRange - couponOrderIdTracker.getTotalCouponsAmount();
			allowedCouponQuantity = allowedCouponQuantity - couponOrderIdTracker.getTotalCouponsCount();
		}
		allowedAmountQuantity.put("allowedAmountRange", allowedCouponRange);
		allowedAmountQuantity.put("allowedCouponQuantity", allowedCouponQuantity);
		return allowedAmountQuantity;
	}
	
	@Transactional
	public UserTransactionHistory findUserTransactionHistoryByOrderId(String orderId) {
		return userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
	}
	
	@Transactional
	public Integer getUserIdForOrder(String orderId) {
		return orderService.getUserIdForOrder(orderId);
	}

	public boolean isValidOrderId(UserTransactionHistory userTransactionHistory) {
		Timestamp txnCreatedOn = userTransactionHistory.getCreatedOn();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(txnCreatedOn.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 30);
		Timestamp txnDayWithExpiryDays = new Timestamp(cal.getTime().getTime());

		Timestamp currentTimestamp = new Timestamp(new Date().getTime());
		if (currentTimestamp.after(txnDayWithExpiryDays)) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isSuccessTransaction(UserTransactionHistory userTransactionHistory) {
		if (userTransactionHistory.getTransactionStatus() == null) {
			logger.error("Transaction startus is null for " + userTransactionHistory.getOrderId());
			return false;
		}
		String txnStatus = userTransactionHistory.getTransactionStatus();
		if (txnStatus.equalsIgnoreCase("00") || txnStatus.equalsIgnoreCase("0")) {
			return true;
		}
		return false;
	}
}
