package com.freecharge.app.service;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.freecharge.common.util.RechargeProductType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.DynamoConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.recharge.businessDo.SuccessfulBillRechargeDo;
import com.freecharge.recharge.businessDo.SuccessfulRechargeDetails;
import com.freecharge.recharge.businessDo.SuccessfulRechargeDo;
import com.freecharge.recharge.businessDo.SuccessfulRechargeWebDo;
import com.freecharge.recharge.services.OperatorAlertService;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Service
public class SuccessfulRechargeService {

    private static final String TABLE_NAME = "recharge_history";
    private static final String EMAIL_ID_FIELD = "emailId";
    private static final String FREQ_WINDOW_START_FIELD = "freqWindowStart";

    @Autowired
    private AmazonDynamoDBAsync dynamoClientMum;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private OperatorAlertService operatorAlertService;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;

    private Logger  logger = LoggingFactory.getLogger(getClass());

    private String getCurrAttrName(String productType) {

        if (FCUtil.isEmpty(productType)) {
            return "";
        }

        switch (productType) {
            case FCConstants.PRODUCT_TYPE_MOBILE:
                return DynamoConstants.MOBILE_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_POSTPAID:
                return DynamoConstants.POSTPAID_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_DTH:
                return DynamoConstants.DTH_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_DATACARD:
                return DynamoConstants.DATACARD_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_POSTPAID_DATACARD:
                return DynamoConstants.POSTAPID_DATACARD_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_ELECTRICITY:
                return DynamoConstants.ELECTRICITY_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_GAS:
                return DynamoConstants.GAS_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_LANDLINE:
                return DynamoConstants.LANDLINE_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_BROADBAND:
                return DynamoConstants.BROADBAND_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_METRO:
                return DynamoConstants.METRO_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_WATER:
            	return DynamoConstants.WATER_AMOUNT_FIELD;
            case FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS:
            	return DynamoConstants.GOOGLE_CREDITS_AMOUNT_FIELD;

            default:
                return "";
        }
    }

    // Return the index of the number, if it is already present else return -1.
    private int getCurrNumFromList(List<SuccessfulRechargeDo> currNumList, String serviceNumber) {
        for (int i = 0; i < currNumList.size(); i++) {
            if (currNumList.get(i).getServiceNumber().contains(serviceNumber)) {
                return i;
            }
        }
        return -1;
    }

    public List<SuccessfulRechargeDo> getRechargeObjFromJSON(String json) {
        Type listType = new TypeToken<List<SuccessfulRechargeDo>>() {}.getType();
        return new Gson().fromJson(json, listType);
    }


    private int getDenomFromList(List<Object> denomList, String amount) {
        Float amt = Float.parseFloat(amount);
        for (int i = 0; i < denomList.size(); i++) {
            @SuppressWarnings("unchecked")
            List<Object> denom = (List<Object>) denomList.get(i);
            String currAmount = (String) denom.get(0);
            Float currAmt;
            currAmt = Float.parseFloat(currAmount);
            if (currAmt.equals(amt))  {
                return i;
            }
        }
        return -1;
    }

    private boolean updateDenomList(List<Object> denomList, SuccessfulRechargeDetails suDetails) {
        int index = this.getDenomFromList(denomList, suDetails.getAmount());

        if (index != -1) {
            @SuppressWarnings("unchecked")
            List<Object> denom = (List<Object>) denomList.get(index);
            String currplanType = (String) denom.get(1);

            // If everything is up-to date , no need to write back. 
            if (index == 0 && FCUtil.isNotEmpty(currplanType) && suDetails.getRechargePlanType().equals(currplanType)) {
                return false;
            }
            denomList.remove(index);
            denom.set(1, suDetails.getRechargePlanType());

            // Add the denom in the front, which is index 0.
            denomList.add(0, denom);
        } else {
            Object currDenom = new Object[]{suDetails.getAmount(), suDetails.getRechargePlanType()};
            denomList.add(0, currDenom);
        }

        int maxDenomToStore = fcProperties.getMaxDenomRechargeHistory();
        // remove the older item from the list.
        if (denomList.size() > maxDenomToStore) {
            denomList.remove(denomList.size()-1);
        }
        return true;
    }

    private boolean noChangeRequiredInDB(SuccessfulRechargeDo suRechargeDo, SuccessfulRechargeDetails sucDetails) {
        int newOperatorId = sucDetails.getOperatorId();
        int newCircleId = sucDetails.getCircleId();
        if (newOperatorId != suRechargeDo.getOperatorId() || newCircleId != suRechargeDo.getCircleId()) {
            return false;
        }
        return true;
    }

    /*
     * 1.Find the service number in the successfulRechargeDos
     * 2. if found:
     *   a. Update the denomination in corresponding SuccessfulRechargeDo.denomList;
     *   b. Move the SuccessfulRechargeDo to the front of the List.
     * 3. Else:
     *  a. Create the SuccessfulRechargeDo new object and prepend it to the successfulRechargeDos
     * 4. Remove the older SuccessfulRechargeDo at the end of the list, if it exceeds the max storage count.
     * 5. Returns true, if something has changed in the successfulRechargeDos.
     */

    private boolean updateNumberInRechargeList(List<SuccessfulRechargeDo> successfulRechargeDos, SuccessfulRechargeDetails sucDetails) {
        int index = this.getCurrNumFromList(successfulRechargeDos, sucDetails.getServiceNumber());
        SuccessfulRechargeDo suRechargeDo = null;
        List<Object> denomList = null;
        long timestamp= System.currentTimeMillis();
        if (index != -1) {
            suRechargeDo = successfulRechargeDos.remove(index);
            denomList = suRechargeDo.getDenomList();
            updateDenomList(denomList, sucDetails);

        } else {
            // Number doesn't exist yet; add it to List.
            suRechargeDo = new SuccessfulRechargeDo();
            suRechargeDo.setServiceNumber(sucDetails.getServiceNumber());

            denomList = new LinkedList<Object>();
            denomList.add(new Object[]{sucDetails.getAmount(), sucDetails.getRechargePlanType()});
        }

        suRechargeDo.setDenomList(denomList);
        suRechargeDo.setCircleId(sucDetails.getCircleId());
        suRechargeDo.setOperatorId(sucDetails.getOperatorId());
        suRechargeDo.setTimestamp(timestamp);
        // increment frequency
        suRechargeDo.setFrequency(suRechargeDo.getFrequency()+1);
        logger.info("Frequency incremented to - " + suRechargeDo.getFrequency());
        // Add the recent transaction to the front of the List.
        successfulRechargeDos.add(0, suRechargeDo);

        int maxNumbersToStore = fcProperties.getMaxMobNumRechargeHistory();
        // Remove the last mobile number as it is older one.
        if (successfulRechargeDos.size() > maxNumbersToStore) {
            successfulRechargeDos.remove(successfulRechargeDos.size() - 1);
        }

        return true;
    }

    public void insertNewTransaction(SuccessfulRechargeDetails sucDetails) {
        logger.info("SuccessDetails to populate "+sucDetails );
        try {
            String attrName = this.getCurrAttrName(sucDetails.getProductType());
            if (FCUtil.isEmpty(attrName)) {
                logger.error("Invalid product type " + sucDetails.getProductType());
                return;
            }

            Map<String, AttributeValue> item = this.getSuccessfulRechargesForEmailId(sucDetails.getEmailId(), null);
            Map<String, AttributeValueUpdate> freqUpdatedAttrs = processWindowExpiry(item);
            Map<String, AttributeValueUpdate> circleChangeUpdateAttrs = handleOperatorCircleChange(sucDetails, item);
            List<SuccessfulRechargeDo> existingRecharges = null;
            if (item == null || item.get(attrName) == null) {;
                existingRecharges = new LinkedList<SuccessfulRechargeDo>();
            }
            else {
                String jsonValue = item.get(attrName).getS();
                existingRecharges = getRechargeObjFromJSON(jsonValue);
            }

            this.updateNumberInRechargeList(existingRecharges, sucDetails);
            String json = new Gson().toJson(existingRecharges);
            Map<String, AttributeValue> updateItemKey = new HashMap<String, AttributeValue>();
            updateItemKey.put(EMAIL_ID_FIELD, new AttributeValue(sucDetails.getEmailId().toLowerCase()));

            Map<String, AttributeValueUpdate> updateItemAttrs = new HashMap<String, AttributeValueUpdate>();
            updateItemAttrs.putAll(freqUpdatedAttrs);
            updateItemAttrs.putAll(circleChangeUpdateAttrs);
            updateItemAttrs.put(attrName, new AttributeValueUpdate().withValue(new AttributeValue().withS(json)));

            UpdateItemRequest updateItemRequest = new UpdateItemRequest(TABLE_NAME, updateItemKey, updateItemAttrs);
            dynamoClientMum.updateItem(updateItemRequest);
        } catch (Exception e) {
            logger.error("Error in writing data to table " + TABLE_NAME + "  for emailId " + sucDetails.getEmailId(), e);
        }
    }

    /**
     * Checks if the current window for frequency counters has expired or not
     * @param windowStartTime starting time for current window
     * @return
     */
    private boolean isValidWindow(Long windowStartTime){
        logger.debug("Checking window expiry for startTime - " + windowStartTime);
        int intervalSize = Integer.parseInt(fcProperties.getProperty(FCConstants.RECHARGE_COUNTER_WINDOW_INTERVAL_DAYS));

        long daysElapsed = TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - windowStartTime);
        return ( daysElapsed < intervalSize);
    }

    /**
     * 1. Set window start time if not present in the user recharge history details
     * 2. Check if the window has expired [ NOW - 90(configureable) days]
     * 3. If expired, decay all frequency values by a decay function (f(x) = x/2)
     *    and reset the window left boundary to current date
     *
     * @param item
     * @return map of fields/columns that need to be updated with their values
     */
    private Map<String, AttributeValueUpdate> processWindowExpiry(Map<String, AttributeValue> item){
        Map<String, AttributeValueUpdate> attrUpdateMap = new HashMap<String, AttributeValueUpdate>();
        // If this is a new item OR window has not been set before
        if((item == null) || (item.get(FREQ_WINDOW_START_FIELD) == null)){
            logger.debug("Window start not available. Initializing window");
            attrUpdateMap.put(FREQ_WINDOW_START_FIELD, new AttributeValueUpdate().withValue(new AttributeValue().withN(Long.toString(System.currentTimeMillis()))));
            return attrUpdateMap;
        }
        //decay frequency counters if window has changed
        if(!isValidWindow(Long.parseLong(item.get(FREQ_WINDOW_START_FIELD).getN()))){
            logger.info("Window Expired. Reassigning frequency values");
            for(RechargeProductType rechargeType : RechargeProductType.values()){
                if(item.get(rechargeType.getName())!=null){
                    String jsonValue = item.get(rechargeType.getName()).getS();
                    List<SuccessfulRechargeDo> existingRecharges = getRechargeObjFromJSON(jsonValue);
                    for(SuccessfulRechargeDo recharge : existingRecharges){
                        recharge.setFrequency(recharge.getFrequency()/2);
                    }
                    String json = new Gson().toJson(existingRecharges);
                    AttributeValue rechargeListVal = new AttributeValue().withS(json);
                    item.put(rechargeType.getName(), rechargeListVal);
                    attrUpdateMap.put(rechargeType.getName(), new AttributeValueUpdate().withValue(rechargeListVal));
                }
            }
            // reset the window left boundary
            attrUpdateMap.put(FREQ_WINDOW_START_FIELD, new AttributeValueUpdate().withValue(new AttributeValue().withN(Long.toString(System.currentTimeMillis()))));
        }

        return attrUpdateMap;
    }

    /**
     * Detects a change in operator circle while saving the recharge.
     * Checks if an entry for the same service number exists in the POSTPAD/MOBILE category if current recharge is a MOBILE/POSTPAID type.
     * Deletes the previous entry to prevent multilpe records.
     * @param sucDetails
     * @param item
     * @return
     */
    private Map<String, AttributeValueUpdate> handleOperatorCircleChange(SuccessfulRechargeDetails sucDetails, Map<String, AttributeValue> item){
        Map<String, AttributeValueUpdate> attrUpdateMap = new HashMap<String, AttributeValueUpdate>();
        if(item == null){
            // we do not have a case here
            return attrUpdateMap;
        }
        String productType = sucDetails.getProductType();
        if(FCConstants.PRODUCT_TYPE_MOBILE.equals(productType) || FCConstants.PRODUCT_TYPE_POSTPAID.equals(productType)){
            String juxtaposingProductType = productType.equals(FCConstants.PRODUCT_TYPE_MOBILE)?FCConstants.PRODUCT_TYPE_POSTPAID:FCConstants.PRODUCT_TYPE_MOBILE;
            if(item.get(juxtaposingProductType)!=null) {
                String jsonValue = item.get(juxtaposingProductType).getS();
                List<SuccessfulRechargeDo> complementRecharges = getRechargeObjFromJSON(jsonValue);
                int index = getCurrNumFromList(complementRecharges, sucDetails.getServiceNumber());
                if (index > -1) {
                    // delete the older entry
                    logger.info("Operator circle changed for service number - " + sucDetails.getServiceNumber() + ". Removing previous entry");
                    complementRecharges.remove(index);
                    String json = new Gson().toJson(complementRecharges);
                    AttributeValue rechargeListVal = new AttributeValue().withS(json);
                    attrUpdateMap.put(juxtaposingProductType, new AttributeValueUpdate().withValue(rechargeListVal));
                }
            }
        }
        return attrUpdateMap;
    }


    public Map<String, AttributeValue> getSuccessfulRechargesForEmailId(String emailId, String attrName) {
        if (emailId != null) {

            try {
                GetItemRequest getItemRequest = new GetItemRequest().withTableName(TABLE_NAME)
                        .withKey(ImmutableMap.of(EMAIL_ID_FIELD, new AttributeValue(emailId.toLowerCase())));

                // Get all the columns, if the attribute name is empty
                if (!FCUtil.isEmpty(attrName)) {
                    List<String> attrList = new LinkedList<String>();
                    attrList.add(attrName);
                    getItemRequest.setAttributesToGet(attrList);
                }

                GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);
                Map<String, AttributeValue> item = getItemResult.getItem();
                return item;
            } catch (Exception e) {
                logger.error("Error in reading data from table " + TABLE_NAME + "  for emailId " + emailId, e);
            }
        }
        logger.info("EmailId received is null");
        return null;
    }

    public Map<String, Object>  getSuccessfulRechargesForProduct(String emailId, String productType) {
        List<SuccessfulRechargeDo> rechargeList = null;
        String attrName = this.getCurrAttrName(productType);
        Map<String, Object> returnData = new HashMap<String, Object>();

        if (FCUtil.isEmpty(attrName)) {
            logger.error("Invalid product type for email Id" + emailId );
            returnData.put("error", "Invalid product type");
            return returnData;
        }

        Map<String, AttributeValue> item = this.getSuccessfulRechargesForEmailId(emailId, attrName);
        if (item == null || item.get(attrName) == null) {
            returnData.put(productType, "");
            return returnData;
        }

        String jsonNumList = item.get(attrName).getS();
        if (FCUtil.isEmpty(jsonNumList)) {
            returnData.put(productType, "");
            return returnData;
        }

        rechargeList = getRechargeObjFromJSON(jsonNumList);

        // Prepare the webDo to send. Also find the operator status for the given operatorId and circleId;
        List<SuccessfulRechargeWebDo> suWebDos = new LinkedList<SuccessfulRechargeWebDo>();
        Map<String, Boolean> statusMap =  new HashMap<String, Boolean>();

        for (SuccessfulRechargeDo suDo : rechargeList ) {
            SuccessfulRechargeWebDo newWebDo =  new SuccessfulRechargeWebDo();
            newWebDo.setRechargeDetails(suDo);

            String operatorId = Integer.toString(suDo.getOperatorId());
            String circleId = Integer.toString(suDo.getCircleId());

            Boolean status = statusMap.get(operatorId + "_" + circleId);
            if (status == null) {
                status = operatorAlertService.isOperatorInAlert(suDo.getOperatorId(), suDo.getCircleId());
                statusMap.put(operatorId + "_" + circleId, status);
            }

            newWebDo.setStatus(status);
            suWebDos.add(newWebDo);
        }

        returnData.put(productType, suWebDos);
        return returnData;
    }

    public void insertNewBillPayTransaction(SuccessfulRechargeDetails sucDetails, String billId , String orderId) {
        try {
            String attrName = this.getCurrAttrName(sucDetails.getProductType());

            if (FCUtil.isEmpty(attrName)) {
                logger.error("Invalid product type " + sucDetails.getProductType());
                return;
            }

            Map<String, AttributeValue> item = this.getSuccessfulRechargesForEmailId(sucDetails.getEmailId(), attrName);
            Map<String, AttributeValueUpdate> freqUpdatedAttrs = processWindowExpiry(item);
            List<SuccessfulBillRechargeDo> existingRecharges = null;
            if (item == null || item.get(attrName) == null) {
                existingRecharges = new LinkedList<SuccessfulBillRechargeDo>();
            } else {
                String jsonValue = item.get(attrName).getS();
                existingRecharges = getBillRechargeObjFromJSON(jsonValue);
            }

            this.updateNumberInBillRechargeList(existingRecharges, sucDetails, billId, orderId);

            String json = new Gson().toJson(existingRecharges);
            Map<String, AttributeValue> updateItemKey = new HashMap<String, AttributeValue>();
            updateItemKey.put(EMAIL_ID_FIELD, new AttributeValue(sucDetails.getEmailId().toLowerCase()));

            Map<String, AttributeValueUpdate> updateItemAttrs = new HashMap<String, AttributeValueUpdate>();
            updateItemAttrs.putAll(freqUpdatedAttrs);
            updateItemAttrs.put(attrName, new AttributeValueUpdate().withValue(new AttributeValue().withS(json)));

            UpdateItemRequest updateItemRequest = new UpdateItemRequest(TABLE_NAME, updateItemKey, updateItemAttrs);
            dynamoClientMum.updateItem(updateItemRequest);
            logger.info("Item successfuly added to dynamoDB");
        } catch (Exception e) {
            logger.error("Error in writing data to table " + TABLE_NAME + "  for emailId " + sucDetails.getEmailId(),
                    e);
        }

    }

    private void updateNumberInBillRechargeList(List<SuccessfulBillRechargeDo> successfulBillRechargeDos,
            SuccessfulRechargeDetails sucDetails, String billId, String orderId) {
        int index = this.getCurrNumFromBillList(successfulBillRechargeDos, sucDetails.getServiceNumber());
        SuccessfulBillRechargeDo suRechargeDo = null;
        BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
        BillPayValidator validator = billPayServiceRemote.getBillPayValidatorForOperator(sucDetails.getOperatorId(),
                sucDetails.getCircleId());
        if (index != -1) {
            suRechargeDo = successfulBillRechargeDos.remove(index);

        } else {
            // Number doesn't exist yet; add it to List.
            suRechargeDo = new SuccessfulBillRechargeDo();
            suRechargeDo.setServiceNumber(billDetails.getAdditionalInfo1());

        }
        long timestamp = System.currentTimeMillis();
        suRechargeDo.setCircleId(sucDetails.getCircleId());
        suRechargeDo.setOperatorId(sucDetails.getOperatorId());
        suRechargeDo.setAmount(sucDetails.getAmount());
        if (validator != null) {
            suRechargeDo.setBillerType(validator.getBillerType());
        }
        suRechargeDo.setBillDate(billDetails.getBillDate());
        suRechargeDo.setAdditionalInfo1(billDetails.getAdditionalInfo1());
        suRechargeDo.setAdditionalInfo2(billDetails.getAdditionalInfo2());
        suRechargeDo.setAdditionalInfo3(billDetails.getAdditionalInfo3());
        suRechargeDo.setAdditionalInfo4(billDetails.getAdditionalInfo4());
        suRechargeDo.setAdditionalInfo5(billDetails.getAdditionalInfo5());
        suRechargeDo.setOrderId(orderId);
        suRechargeDo.setDueDate(billDetails.getDueDate());
        suRechargeDo.setTimestamp(timestamp);
        // Add the recent transaction to the front of the List.
        successfulBillRechargeDos.add(0, suRechargeDo);

        int maxNumbersToStore = fcProperties.getMaxMobNumRechargeHistory();
        // Remove the last mobile number as it is older one.
        if (successfulBillRechargeDos.size() > maxNumbersToStore) {
            successfulBillRechargeDos.remove(successfulBillRechargeDos.size() - 1);
        }

    }

    private int getCurrNumFromBillList(List<SuccessfulBillRechargeDo> currNumList, String serviceNumber) {
        for (int i = 0; i < currNumList.size(); i++) {
            if (currNumList.get(i).getServiceNumber().contains(serviceNumber)) {
                return i;
            }
        }
        return -1;
    }

    public List<SuccessfulBillRechargeDo> getBillRechargeObjFromJSON(String json) {
        Type listType = new TypeToken<List<SuccessfulBillRechargeDo>>() {
        }.getType();
        return new Gson().fromJson(json, listType);
    }
}