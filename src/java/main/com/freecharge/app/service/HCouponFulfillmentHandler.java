package com.freecharge.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.exception.CouponExpireException;
import com.freecharge.api.coupon.service.exception.CouponOutOfStockException;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.coupon.model.SaveCartItemsRequestData;
import com.freecharge.rest.coupon.model.SaveCartItemsResponseData;

@Service
@EnableAsync
public class HCouponFulfillmentHandler {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    @Qualifier("couponServiceProxy")
    private ICouponService couponServiceProxy;
    
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private CouponCartService couponCartService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    public static final String REASON_SOMETHING_WRONG = "someThingWrong";
    public static final String REASON_COUPON_OF_STOCK = "couponOutOfStock";
    public static final String REASON_COUPON_EXPIRED = "couponExpired";
    
    /*We are not going to use this method because of some reason*/
    @Async
    public void doHCouponFulfilment(List<SaveCartItemsRequestData> saveCartItemsRequestDataList) {
        Map<Integer, BlockedCoupon> hcouponFulfilmentMap = null;
        boolean isRefunded = false;
        String reason = null;
        Integer couponId = null;
        boolean isSuccess = false;
        
        for (SaveCartItemsRequestData saveCartItemsRequestData : saveCartItemsRequestDataList) {
            String orderId = saveCartItemsRequestData.getOrderId();
            try {
                Integer userId = orderService.getUserIdForOrder(orderId);
                if (userId == null) {
                    logger.error("UserId not found, fulfilment failed for orderId: " + orderId);
                    return;
                }
                
                List<CouponCartItem> couponCartItems = couponCartService.getCouponCartItemsForOrderId(orderId);
                if (couponCartService == null || couponCartItems.isEmpty()) {
                    logger.error("Cart items not found, fulfilment failed for orderId: " + orderId);
                    return;
                }
                logger.info("printing couponCartItems...");
                for(CouponCartItem ccItem: couponCartItems){
                	logger.info("ccItem= couponId="+ccItem.getCouponId()+" quantity="+ccItem.getQuantity());
                	
                }
                logger.info("starting doHCouponFulfilment in HCouponFulfillmentHandler");
                hcouponFulfilmentMap = couponServiceProxy.doHCouponFulfilment(couponCartItems,orderId,userId);
                
                logger.info("hcouponFulfilmentMap= "+hcouponFulfilmentMap); 
//                for (Map<Integer,BlockedCoupon> hcouponMap : hcouponFulfilmentMap.entrySet()) {
//            	    logger.info("key= " + hcouponMap.getKey() + ", BlockedCouponValues= " + hcouponMap.getValue());
//            	}
                if (hcouponFulfilmentMap == null) {
                    logger.error("Fulfilment failed for orderId: " + orderId);
                    return;
                }
                
                updateUsertTransactionHistory(orderId, RechargeConstants.SUCCESS_RESPONSE_CODE);
                isSuccess = true;
                logger.info("Order success orderId: " + orderId);
            } catch (InvalidParameterException e) {
                logger.error("InvalidParameterException Parameter name: " + e.getParameterName() + " Value: " + e.getReceivedValue()
                        + "  Message: " + e.getMessage());
                reason = REASON_SOMETHING_WRONG;
                updateUsertTransactionHistory(orderId, RechargeConstants.HCOUPON_FAILURE_PROCESS_CODE);
            } catch (CouponOutOfStockException e) {
                couponId = e.getCouponId();
                reason = REASON_COUPON_OF_STOCK;
                updateUsertTransactionHistory(orderId, RechargeConstants.HCOUPON_FAILURE_PROCESS_CODE);
            } catch (CouponExpireException e) {
                couponId = e.getCouponId();
                reason = REASON_COUPON_EXPIRED;
                updateUsertTransactionHistory(orderId, RechargeConstants.HCOUPON_FAILURE_PROCESS_CODE);
            } catch (Exception e) {
                logger.error("Unknown error in blocking coupons for orderId: " + orderId, e);
                reason = REASON_SOMETHING_WRONG;
                updateUsertTransactionHistory(orderId, RechargeConstants.HCOUPON_FAILURE_PROCESS_CODE);
            }
            couponServiceProxy.saveOrderStatus(orderId, isSuccess, reason, couponId, isRefunded);
            logger.info("Saved coupon order status successfully for orderId: " + orderId);
        }
    }
    
    private void updateUsertTransactionHistory(String orderId, String status) {
        Integer userId = orderService.getUserIdForOrder(orderId);
        Map<String, Object> userDetail = new HashMap<>();
        userDetail.put("amount", 0f);
        userDetail.put("mobileno", "");
        userDetail.put("productType", FCConstants.PRODUCT_TYPE_HCOUPON);
        userDetail.put("operatorName", "");
        userDetail.put("circleName", "");
        userDetail.put("userId", userId);
        userTransactionHistoryService.createOrUpdateRechargeHistory(orderId, userDetail, status);
    }
}
