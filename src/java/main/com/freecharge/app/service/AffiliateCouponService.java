package com.freecharge.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.affiliate.order.AffiliateOrder;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.OrderIdUtil;
import com.freecharge.mongo.repos.AffiliateOrderRepository;
import com.freecharge.rest.model.AffiliateCouponsRequest;
import com.mongodb.BasicDBObject;

@Service
public class AffiliateCouponService {
	@Autowired
	private AffiliateOrderRepository repository;
	
	@Transactional
	public AffiliateOrder saveAffiliateCouponsOrder(AffiliateCouponsRequest request){
		String orderIdPrefix = OrderIdUtil.getOrderIdPrefix(request.getCouponAffiliateId(), FCConstants.PRODUCT_ID_COUPONS, FCConstants.CHANNEL_ID_SERVICE);
		String nextSequenceId = repository.getNextAffiliateOrderSequence();
		String orderId = OrderIdUtil.getFullOrderId(orderIdPrefix, nextSequenceId);
		
		AffiliateOrder affiliateOrder = new AffiliateOrder();
		affiliateOrder.setOrderId(orderId);
		affiliateOrder.setAffiliateId(String.valueOf(request.getCouponAffiliateId()));
		affiliateOrder.setAffiliateOrderId(request.getAffiliateOrderId());
		
		if (request.getAffiliateRequestData() != null){
			BasicDBObject affiliateRequestData = new BasicDBObject(request.getAffiliateRequestData());	
			affiliateOrder.setOrderData(affiliateRequestData);
			
		}
		repository.updateOrCreateAffiliateOrder(orderId, affiliateOrder);
		return affiliateOrder;
	}
}
