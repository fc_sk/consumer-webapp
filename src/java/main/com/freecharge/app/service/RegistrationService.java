package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.IStateMasterDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserProfileDAO;
import com.freecharge.app.domain.dao.UserRechargeContactDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.TimeBarrierService;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.sns.UserDataPointSNSService;
import com.freecharge.tracker.Tracker;
import com.freecharge.uuip.UserDataPointType;
import com.freecharge.web.util.WebConstants;

/**
 * Created with IntelliJ IDEA. User: abc Date: 5/3/12 Time: 11:30 AM To change
 * this template use File | Settings | File Templates.
 */

@Service
public class RegistrationService {
    static final String                    SUCCESS = "Success";

    private Logger                         logger  = LoggingFactory.getLogger(getClass());
    @Autowired
    private FCProperties                   fcProperties;
    @Autowired
    private EmailService                   emailService;
    @Autowired
    private IRegistrationDAO               registrationDAO;
    @Autowired
    private TxnHomePageDAO                 txnHomePageDAO;
    @Autowired
    private TxnFulfilmentDAO               txnFulfilmentDAO;
    @Autowired
    private UserProfileDAO                 userProfileDAO;
    @Autowired
    private UserRechargeContactDAO         userRechargeContactDAO;
    @Autowired
    private CommonService                  commonService;
    @Autowired
    private IStateMasterDAO                stateMasterDAO;
    @Autowired
    private UserWriteDao                   userWriteDao;
    @Autowired
    private EPinEncrypt                    ePinEncrypt;
    @Autowired
    private TimeBarrierService             timeBarrierService;

    @Autowired
    private UserDataPointSNSService        userDataPointSNSService;

    @Autowired
    private AppConfigService               appConfigService;

    @Autowired
    private Tracker                        tracker;

    /**
     * This is done because we want to send a param to affiliates saying whether
     * this user has ever recharged or not. Once the user does a recharge, this
     * is knocked off from the session.
     */
    private void setAffiliateCampaignIdentifier() {
        // Just the key is used, the value does not matter
        FreechargeContextDirectory.get().getFreechargeSession().getSessionData()
                .put(FreechargeSession.IS_FIRST_RECHARGE, true);
    }

    public void sendSuccessfulRegistrationMail(final BaseBusinessDO baseBusinessDO) {
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO) baseBusinessDO;
        String customerName = null;
        String firstName = registerBusinessDO.getFirstName();
        Map<String, String> variableValues = new HashMap<String, String>();
        variableValues.put("email", registerBusinessDO.getEmail());
        variableValues.put("password", registerBusinessDO.getPassword());
        if (FCUtil.isEmpty(firstName) || firstName.contains("@")) {
            customerName = "Customer";
        } else {
            customerName = firstName;
        }
        variableValues.put("nickName", customerName);
        String imgPrefix = fcProperties.getProperty("imgprefix1");
        variableValues.put("imgPrefix", imgPrefix);
        String from = fcProperties.getProperty(FCProperties.REGISTRATION_FROM_EMAILID);
        String subject = fcProperties.getProperty(FCProperties.REGISTRATION_SUBJECT);
        String replyTo = fcProperties.getProperty(FCProperties.REGISTRATION_REPLYTO);
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setTo(registerBusinessDO.getEmail());
        emailBusinessDO.setFrom(from);
        emailBusinessDO.setReplyTo(replyTo);
        emailBusinessDO.setSubject(subject);
        emailBusinessDO.setVariableValues(variableValues);
        emailBusinessDO.setTemplateName("RegistrationEmail.vm");
        logger.info("Registration Successful.Sending mail to : " + registerBusinessDO.getEmail());
        emailService.sendEmail(emailBusinessDO);
    }

    @Transactional
    public boolean registerOnBothDB(final BaseBusinessDO baseBusinessDO) {

        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO) baseBusinessDO;
        // Do not allow any other thread trying to create same user for 5
        // seconds
        final int timeBarrierDuration = 5000;
        if (!timeBarrierService.putTimeBarrier(registerBusinessDO.getEmail(), timeBarrierDuration)) {
            logger.warn("Failed to acquire time barrier on " + registerBusinessDO.getEmail());
            registerBusinessDO.setRegistrationStatus(WebConstants.EMAIL_ID_EXISTS);
            return false;
        }
        
        // Check if the user is already registered with this emailId
        com.freecharge.app.domain.entity.jdbc.Users users = userWriteDao
                .getUserForUpdate(registerBusinessDO.getEmail());
        if (users != null && users.getUserId() > 0) {
            registerBusinessDO.setRegistrationStatus(WebConstants.EMAIL_ID_EXISTS);
            return false;
        }
        users = new com.freecharge.app.domain.entity.jdbc.Users();
        users.setEmail(registerBusinessDO.getEmail().trim());

        String password = registerBusinessDO.getPassword();
        try {
            users.setPassword(ePinEncrypt.getEncryptedPin(password));
        } catch (Exception e) {
            logger.error("RegisterWithoutSession : Error while encrypting the password : ", e);
        }
        users.setType(registerBusinessDO.getType());
        users.setMobileNo(registerBusinessDO.getMobileNo());
        users.setIsActive(true);
        Date date = new java.util.Date();
        users.setDateAdded(new Timestamp(date.getTime()));
        users.setFkAffiliateProfileId(registerBusinessDO.getFkAffiliateProfileId());
        com.freecharge.app.domain.entity.jdbc.UserProfile userProfile =
                new com.freecharge.app.domain.entity.jdbc.UserProfile();
        String city = null;
        if (registerBusinessDO.getCity() != null) {
            city = registerBusinessDO.getCity();
        }
        /*
         * else city = registerBusinessDO.getCityId().toString();
         */
        userProfile.setNickAlias(registerBusinessDO.getNickAlias());
        userProfile.setFkStateMasterId(registerBusinessDO.getStateId());
        userProfile.setAddress1(registerBusinessDO.getAddress1());
        userProfile.setLandmark(registerBusinessDO.getLandmark());
        userProfile.setCity(city);
        userProfile.setPostalCode(registerBusinessDO.getPostalCode());
        userProfile.setFirstName(registerBusinessDO.getFirstName());
        userProfile.setFkCountryMasterId(registerBusinessDO.getCountryId());
        userProfile.setStreet(registerBusinessDO.getStreet());
        userProfile.setArea(registerBusinessDO.getArea());
        userProfile.setIsActive(true);
        userProfile.setIsDefault(true);
        userProfile.setTitle(registerBusinessDO.getTitle());
        userProfile.setCreatedOn(new Timestamp(date.getTime()));

        try {

            com.freecharge.app.domain.entity.jdbc.Users newRegisteredUser = userWriteDao.insertUser(users);
            final Integer userId = newRegisteredUser.getUserId();
            userProfile.setFkUserId(userId);
            com.freecharge.app.domain.entity.jdbc.UserProfile newRegisteredUserProfile = userWriteDao
                    .insertUserProfile(userProfile);

            setAffiliateCampaignIdentifier();

            Integer value = newRegisteredUserProfile.getUserProfileId();

            if (value.intValue() > 0) {
                // send mail with credentials

                // registerBusinessDO.setIsLoggedin(true);
                registerBusinessDO.setUserProfileId(value);
                registerBusinessDO.setUserId(userProfile.getFkUserId());

                FreechargeSession.setUserRelatedSessionData(users.getEmail(), registerBusinessDO.getFirstName(), true,
                        userProfile.getFkUserId(), registerBusinessDO.getLoginSource(), registerBusinessDO.getImei(), 
                        registerBusinessDO.getUniqueDeviceId());

                registerBusinessDO.setRegistrationStatus(fcProperties.getProperty(FCProperties.REGISTRATION_SUCCESS_MSG));

                tracker.trackUser(userId);

                // Publishing user data points for Mystique (Unique User Identification Service)
                userDataPointSNSService
                    .publishUserDataPoint(userId, users.getEmail(), UserDataPointType.EMAIL_ID, date);
                userDataPointSNSService
                    .publishUserDataPoint(userId, users.getMobileNo(), UserDataPointType.PROFILE_NO, date);
                userDataPointSNSService
                    .publishUserDataPoint(userId, registerBusinessDO.getImei(), UserDataPointType.IMEI, date);
                userDataPointSNSService.publishUserDataPoint(userId, registerBusinessDO.getUniqueDeviceId(),
                    UserDataPointType.WINDOWS_UNIQUE_DEVICE_ID, date);
                return true;
            } else {
                // provide message with registration failed
                registerBusinessDO.setRegistrationStatus(fcProperties.getProperty(FCProperties.REGISTRATION_FAILED_MSG));
                registerBusinessDO.setIsLoggedin(false);
                return false;
            }
        } catch (Exception exception) {

            logger.error("Error occured while user registration information transaction has been rollbacked ",
                    exception);
            return false;
        }
    }

    public String registerUser(final ResponseStatus responseStatus, final RegisterBusinessDO registrationbdo) {
        logger.debug("registering user with emailId: " + registrationbdo.getEmail());
        boolean registrationStatus = registerOnBothDB(registrationbdo);

        if (registrationStatus) {
            sendSuccessfulRegistrationMail(registrationbdo);
            logger.info("user registered successfully with emailId: " + registrationbdo.getEmail());
            responseStatus.setStatus("success");
            responseStatus.setResult("user registered Successfully with Email Id " + registrationbdo.getEmail());
            return SUCCESS;
        }

        return registrationbdo.getRegistrationStatus();
    }

    @Transactional
    public String paymentTimeSignup(final BaseBusinessDO baseBusinessDO) {
        String lookupID = ((RegisterBusinessDO) baseBusinessDO).getLookupID();
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO) baseBusinessDO;

        if (!FCUtil.isEmpty(lookupID)) {
            String password = ((RegisterBusinessDO) baseBusinessDO).getPassword();
            String cPassword = ((RegisterBusinessDO) baseBusinessDO).getConfirmPassword();
            if (password.equals(cPassword)) {
                List<Users> userList = registrationDAO.getUserInfoByEmailid(baseBusinessDO);
                if (userList != null) {
                    if (userList.size() > 0) {
                        // send back this maild is existed
                        return "emailExisted";
                    } else {
                        // saving info
                        List<TxnHomePage> txnHomePageList = txnHomePageDAO.findByLookupId(lookupID);
                        if (!FCUtil.isEmpty(txnHomePageList)) {
                            TxnHomePage txnHomePage = txnHomePageList.get(0);
                            if (txnHomePage != null) {
                                String serviceNumber = txnHomePage.getServiceNumber();
                                if (!FCUtil.isEmpty(serviceNumber)) {
                                    ((RegisterBusinessDO) baseBusinessDO).setTxnHomePageId(txnHomePage.getId());

                                    com.freecharge.app.domain.entity.jdbc.Users users =
                                            new com.freecharge.app.domain.entity.jdbc.Users();
                                    try {
                                        users.setPassword(ePinEncrypt.getEncryptedPin(password));
                                    } catch (Exception e) {
                                        logger.error("Error occured while login as guest when random password"
                                                + " encrypting..", e);
                                    }
                                    users.setEmail(registerBusinessDO.getEmail().trim());
                                    users.setMobileNo(((RegisterBusinessDO) baseBusinessDO).getMobileNo());
                                    users.setIsActive(true);
                                    users.setDateAdded(new Timestamp((new Date()).getTime()));

                                    users.setMobileNo(registerBusinessDO.getMobileNo());
                                    users.setMorf(registerBusinessDO.getMorf());
                                    users.setIsActive(true);
                                    Date date = new java.util.Date();
                                    users.setDateAdded(new Timestamp(date.getTime()));
                                    users.setFkAffiliateProfileId(registerBusinessDO.getFkAffiliateProfileId());

                                    com.freecharge.app.domain.entity.jdbc.UserProfile userProfile =
                                            new com.freecharge.app.domain.entity.jdbc.UserProfile();
                                    userProfile.setTitle(registerBusinessDO.getTitle());
                                    String firstName = (registerBusinessDO.getFirstName());
                                    userProfile.setFirstName(firstName);
                                    userProfile.setAddress1(registerBusinessDO.getAddress1());
                                    userProfile.setArea(registerBusinessDO.getArea());
                                    userProfile.setLandmark(registerBusinessDO.getLandmark());
                                    userProfile.setCity(registerBusinessDO.getCityId().toString());
                                    userProfile.setFkStateMasterId(registerBusinessDO.getStateId());
                                    userProfile.setPostalCode(registerBusinessDO.getPostalCode());
                                    userProfile.setFkCountryMasterId(registerBusinessDO.getCountryId());
                                    userProfile.setIsActive(true);
                                    userProfile.setIsDefault(true);
                                    userProfile.setCreatedOn(new Timestamp((new Date()).getTime()));

                                    com.freecharge.app.domain.entity.jdbc.Users newRegisteredUser = userWriteDao
                                            .insertUser(users);

                                    Integer userId = newRegisteredUser.getUserId();
                                    userProfile.setFkUserId(userId);
                                    com.freecharge.app.domain.entity.jdbc.UserProfile newRegisteredUserProfile =
                                            userWriteDao.insertUserProfile(userProfile);

                                    setAffiliateCampaignIdentifier();

                                    Integer value = newRegisteredUserProfile.getUserProfileId();

                                    if (value > 0) {
                                        ((RegisterBusinessDO) baseBusinessDO).setDefaultProfile("yes");
                                        ((RegisterBusinessDO) baseBusinessDO).setUserProfileId(value);
                                        Integer operatorId = txnHomePage.getFkOperatorId();
                                        Integer circleId = txnHomePage.getFkCircleId();
                                        Integer productTypeId = txnHomePage.getFkProductId();
                                        Float amount = txnHomePage.getAmount();
                                        UserRechargeContact userRechargeContact = new UserRechargeContact();
                                        userRechargeContact.setServiceNumber(serviceNumber);

                                        userRechargeContact.setFkUsersId(userId);
                                        userRechargeContact.setFkOperatorMasterId(operatorId);
                                        userRechargeContact.setFkCircleMasterId(circleId);
                                        userRechargeContact.setFkProductMasterId(productTypeId);
                                        userRechargeContact.setIsActive(true);
                                        if (amount != null) {
                                            try {
                                                userRechargeContact.setLastRechargeAmount(new Double(amount));
                                            } catch (NumberFormatException nfe) {
                                                logger.error("Exception occured while parsing float amount to double.",
                                                        nfe);
                                            }
                                        }
                                        userRechargeContact.setCreatedOn(new Timestamp((new Date()).getTime()));
                                        Integer value1 = registrationDAO.saveUserRechargeContact(userRechargeContact);

                                        if (value1 > 0) {
                                            ((RegisterBusinessDO) baseBusinessDO).setUserRechargeContactId(value1);
                                            sendSuccessfulRegistrationMail(baseBusinessDO);

                                            Integer value2 = registrationDAO.saveTransactionDetails(baseBusinessDO);
                                            if (value2 > 0) {
                                                Boolean updateStatus = commonService.updateCartWithUserId(userId,
                                                        ((RegisterBusinessDO) baseBusinessDO).getLookupID());
                                                if (updateStatus) {
                                                    commonService.getState(baseBusinessDO);
                                                    commonService.getCountry(baseBusinessDO);
                                                    commonService.setCity(baseBusinessDO);
                                                    ((RegisterBusinessDO) baseBusinessDO).setTxnfulfilmentId(value2);

                                                    FreechargeSession.setUserRelatedSessionData(
                                                            registerBusinessDO.getEmail(), firstName, true, userId,
                                                            registerBusinessDO.getLoginSource(), 
                                                            registerBusinessDO.getImei(), 
                                                            registerBusinessDO.getUniqueDeviceId());

                                                    this.convertBusinessDoToUserContactDetails(baseBusinessDO);
                                                    tracker.trackUser(userId);
                                                    return "success";
                                                } else {
                                                    return "fail";
                                                }
                                            } else {
                                                return "fail";
                                            }
                                        } else {
                                            return "fail";
                                        }
                                    } else {
                                        commonService.getState(baseBusinessDO);
                                        commonService.setCity(baseBusinessDO);
                                        return "fail";
                                    }

                                } else {
                                    return "fail";
                                }
                            } else {
                                return "fail";
                            }
                        } else {
                            return "fail";
                        }
                    }
                } else {
                    // send back to Exception to connect db
                    return "fail";
                }

            } else {
                // send passwords not mach error
                return "passwordNotMach";
            }
        } else {
            return "fail";
        }

    }

    public void convertBusinessDoToUserContactDetails(final BaseBusinessDO baseBusinessDO) {
        RegisterBusinessDO registerBusinessDO = (RegisterBusinessDO) baseBusinessDO;
        UserContactDetail userContactDetail = new UserContactDetail();

        userContactDetail.setUserId(registerBusinessDO.getUserId());
        userContactDetail.setUserProfileId(registerBusinessDO.getUserProfileId());
        userContactDetail.setUserRechargeContactId(registerBusinessDO.getUserRechargeContactId());
        userContactDetail.setEmail(registerBusinessDO.getEmail());
        userContactDetail.setTxnHomePageId(registerBusinessDO.getTxnHomePageId());
        userContactDetail.setTxnFulfilmentId(registerBusinessDO.getTxnfulfilmentId());
        userContactDetail.setArea(registerBusinessDO.getArea());
        userContactDetail.setAddress1(registerBusinessDO.getAddress1());
        userContactDetail.setCityName(registerBusinessDO.getCity());
        userContactDetail.setFirstName(registerBusinessDO.getFirstName());
        userContactDetail.setLandmark(registerBusinessDO.getLandmark());
        userContactDetail.setPostalCode(registerBusinessDO.getPostalCode());
        userContactDetail.setServiceNumber(registerBusinessDO.getServiceNumber());
        userContactDetail.setStateId(registerBusinessDO.getStateId());
        userContactDetail.setCountryId(registerBusinessDO.getCountryId());
        userContactDetail.setMobile(registerBusinessDO.getMobileNo());
        userContactDetail.setStateName(registerBusinessDO.getState());
        userContactDetail.setCountryName(registerBusinessDO.getCountry());
        userContactDetail.setTitle(registerBusinessDO.getTitle());
        CityMasterBusinessDO cityMasterBusinessDO = new CityMasterBusinessDO();
        cityMasterBusinessDO.setStateId(registerBusinessDO.getStateId());
        commonService.getCityMasterList(cityMasterBusinessDO);
        registerBusinessDO.setCityMasterList(cityMasterBusinessDO.getCityMasterList());
        registerBusinessDO.setUserContactDetails(userContactDetail);
    }

    public Integer saveFirstName(final BaseBusinessDO baseBusinessDO) {
        return txnFulfilmentDAO.saveFirstName(baseBusinessDO);
    }

    private TxnFulfilment makeTxnFulfilment(final BaseBusinessDO baseBusinessDO, final List<UserContactDetail> list) {
        UserContactDetail ucd = list.get(0);
        Integer countryId = ucd.getCountryId();
        Integer ststeId = ucd.getStateId();
        String addr = ucd.getAddress1();
        String email = ucd.getEmail();
        String fname = ucd.getFirstName();
        String city = ucd.getCityName();
        String postalCode = ucd.getPostalCode();
        String mobile = ucd.getMobile();

        String area = ucd.getArea();
        String landmark = ucd.getLandmark();

        TxnFulfilment txnFulFilment = new TxnFulfilment();
        txnFulFilment.setTxnHomePageId(((RegisterBusinessDO) baseBusinessDO).getTxnHomePageId());
        txnFulFilment.setEmail(email);
        txnFulFilment.setFirstName(fname);
        txnFulFilment.setDeliveryName(fname);
        txnFulFilment.setAddressMobile(mobile);
        txnFulFilment.setDeliveryAdd(addr);
        // txnFulFilment.setDeliveryStreet(street);
        txnFulFilment.setDeliveryArea(area);
        txnFulFilment.setDeliveryLandmark(landmark);
        txnFulFilment.setDeliveryFkCityMasterId(ucd.getCityId());
        txnFulFilment.setDeliveryPincode(postalCode);
        txnFulFilment.setDeliveryFkStateMasterId(ststeId);
        txnFulFilment.setDeliveryFkCountryMasterId(countryId);
        txnFulFilment.setCreatedAt(new Timestamp((new Date()).getTime()));
        txnFulFilment.setFkUserProfileId(((RegisterBusinessDO) baseBusinessDO).getUserContactDetails()
                .getUserProfileId());
        // txnFulFilment.setIsGuest(false);
        return txnFulFilment;
    }

    @Transactional
    public void saveAddress(final BaseBusinessDO baseBusinessDO) {
        AddressBusinessDO addressBusinessDO = (AddressBusinessDO) baseBusinessDO;
        Integer userRechargeContactId = addressBusinessDO.getUserRechargeContactId();
        Integer userProfileId = addressBusinessDO.getUserProfileId();
        Integer txnfullfilmentId = addressBusinessDO.getTxnfulfilmentId();
        String defaultProfile = addressBusinessDO.getDefaultProfile();
        String actionParam = addressBusinessDO.getActionParam();
        String isEdit = addressBusinessDO.getIsEdit();
        if (FCUtil.isEmpty(defaultProfile)) {
            // set to yes if empty
            defaultProfile = "yes";
        }

        if (userRechargeContactId != null && userRechargeContactId != 0) {
            if (userProfileId != null && txnfullfilmentId != null) {
                FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
                String email = null;
                if (fs != null) {
                    Map<String, Object> sessionMap = fs.getSessionData();
                    if (sessionMap != null) {
                        email = (String) sessionMap.get(WebConstants.SESSION_USER_EMAIL_ID);
                    }
                }
                if (actionParam.equals("saveorupdate")) {
                    if (defaultProfile.equals("yes")) {
                        this.saveProfileAndUpdateTxnfulfilment(baseBusinessDO, userRechargeContactId, email,
                                txnfullfilmentId);

                    } else if (userProfileId != 0 && txnfullfilmentId != 0) {
                        if (isEdit.equals("yes")) {
                            this.saveOrUpdateProfile(baseBusinessDO, userRechargeContactId, email, userProfileId,
                                    txnfullfilmentId);
                        } else if (userProfileId != 0 && txnfullfilmentId != 0) {
                            this.updateProfileAndTxnfulfilment(baseBusinessDO, userProfileId, txnfullfilmentId);
                        }
                    }
                } else if (actionParam.equals("pay")) {
                    if (defaultProfile.equals("yes")) {
                        this.saveProfileAndUpdateTxnfulfilment(baseBusinessDO, userRechargeContactId, email,
                                txnfullfilmentId);
                    } else {
                        if (userProfileId != 0 && txnfullfilmentId != 0) {
                            this.updateProfileAndTxnfulfilment(baseBusinessDO, userProfileId, txnfullfilmentId);
                        } else if (userProfileId != 0 && txnfullfilmentId == 0) {

                            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");

                        } else {
                            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                        }

                    }
                } else {
                    ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                }
            } else {
                ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
            }
        } else {
            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
        }
        commonService.getCity(baseBusinessDO);
        commonService.getState(baseBusinessDO);
        commonService.getCountry(baseBusinessDO);
        commonService.setCity(baseBusinessDO);

    }

    public void saveProfileAndUpdateTxnfulfilment(final BaseBusinessDO baseBusinessDO,
            final Integer userRechargeContactId, final String email, final Integer txnfulfilmentId) {
        AddressBusinessDO addressBusinessDO = (AddressBusinessDO) baseBusinessDO;
        UserProfile userProfile = new UserProfile();
        userProfile.setTitle(addressBusinessDO.getTitle());
        userProfile.setFirstName(addressBusinessDO.getFirstName());
        userProfile.setAddress1(addressBusinessDO.getAddress());
        userProfile.setArea(addressBusinessDO.getArea());
        userProfile.setLandmark(addressBusinessDO.getLandmark());
        userProfile.setFkCityMasterId(addressBusinessDO.getCityId());
        userProfile.setFkStateMasterId(addressBusinessDO.getStateId());
        userProfile.setFkCountryMasterId(addressBusinessDO.getCountryId());
        userProfile.setPostalCode(addressBusinessDO.getPostalCode());
        userProfile.setFkUserRechargeContactId(userRechargeContactId);
        userProfile.setFkUserId(addressBusinessDO.getUserId());
        userProfile.setIsActive(true);
        userProfile.setIsDefault(false);
        userProfile.setCreatedOn(new Timestamp((new Date()).getTime()));
        try {
            UserProfile userProfileObj = userProfileDAO.create(userProfile);
            Integer userProfileId1 = userProfileObj.getUserProfileId();
            List<TxnFulfilment> txnFulfilmentList = txnFulfilmentDAO.findByProperty("id", txnfulfilmentId);
            if (!FCUtil.isEmpty(txnFulfilmentList)) {
                TxnFulfilment txnFulfilment = txnFulfilmentList.get(0);
                if (!addressBusinessDO.getFirstName().equals(txnFulfilment.getFirstName())
                        || !addressBusinessDO.getFirstName().equals(txnFulfilment.getDeliveryName())
                        || !addressBusinessDO.getAddress().equals(txnFulfilment.getDeliveryAdd())
                        || !addressBusinessDO.getArea().equals(txnFulfilment.getDeliveryArea())
                        || !addressBusinessDO.getLandmark().equals(txnFulfilment.getDeliveryLandmark())
                        || !addressBusinessDO.getCityId().equals(txnFulfilment.getDeliveryFkCityMasterId())
                        || !addressBusinessDO.getPostalCode().equals(txnFulfilment.getDeliveryPincode())
                        || addressBusinessDO.getStateId() != txnFulfilment.getDeliveryFkStateMasterId()) {
                    txnFulfilment.setDeliveryAdd(addressBusinessDO.getAddress());
                    txnFulfilment.setDeliveryArea(addressBusinessDO.getArea());
                    txnFulfilment.setDeliveryLandmark(addressBusinessDO.getLandmark());
                    txnFulfilment.setDeliveryFkCityMasterId(addressBusinessDO.getCityId());
                    txnFulfilment.setDeliveryPincode(addressBusinessDO.getPostalCode());
                    txnFulfilment.setDeliveryFkStateMasterId(addressBusinessDO.getStateId());
                    txnFulfilment.setFirstName(addressBusinessDO.getFirstName());
                    txnFulfilment.setDeliveryName(addressBusinessDO.getFirstName());
                    txnFulfilment.setUpdatedAt(new Timestamp((new Date()).getTime()));
                    txnFulfilment.setFkUserProfileId(userProfileId1);
                    TxnFulfilment txnFulfilment1 = txnFulfilmentDAO.update(txnFulfilment);
                    if (txnFulfilment1 != null) {

                        ((AddressBusinessDO) baseBusinessDO).setStatus("success");
                        ((AddressBusinessDO) baseBusinessDO).setUserProfileId(userProfileId1);
                        ((AddressBusinessDO) baseBusinessDO).setIsEdit("no");
                        ((AddressBusinessDO) baseBusinessDO).setDefaultProfile("no");
                    } else {
                        ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                    }
                } else {
                    ((AddressBusinessDO) baseBusinessDO).setStatus("success");
                    ((AddressBusinessDO) baseBusinessDO).setUserProfileId(userProfileId1);
                    ((AddressBusinessDO) baseBusinessDO).setIsEdit("no");
                    ((AddressBusinessDO) baseBusinessDO).setDefaultProfile("no");
                }

            } else {
                ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
            }
        } catch (Exception e) {
            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
            logger.error("Error while updating Txn fulfillment id: " + txnfulfilmentId, e);
        }

    }

    public void updateProfileAndTxnfulfilment(final BaseBusinessDO baseBusinessDO, final Integer userProfileId,
            final Integer txnfullfilmentId) {
        AddressBusinessDO addressBusinessDO = (AddressBusinessDO) baseBusinessDO;
        List<UserProfile> userProfilelist = userProfileDAO.findByProperty("userProfileId", userProfileId);
        if (userProfilelist != null && userProfilelist.size() == 1) {
            UserProfile userProfile = userProfilelist.get(0);
            String upTitle = userProfile.getTitle();
            String upFirstName = userProfile.getFirstName();
            String upAddress = userProfile.getAddress1();
            String upArea = userProfile.getArea();
            String upLandmark = userProfile.getLandmark();
            Integer upCityId = userProfile.getFkCityMasterId();
            Integer upStateId = userProfile.getFkStateMasterId();
            String upPincode = userProfile.getPostalCode();
            String title = addressBusinessDO.getTitle();
            String firstName = addressBusinessDO.getFirstName();
            String address = addressBusinessDO.getAddress();
            String area = addressBusinessDO.getArea();
            String landmark = addressBusinessDO.getLandmark();
            Integer cityId = addressBusinessDO.getCityId();
            Integer stateId = addressBusinessDO.getStateId();
            String pincode = addressBusinessDO.getPostalCode();
            if (!upTitle.equals(title) || !upFirstName.equals(firstName) || !upAddress.equals(address)
                    || !upArea.equals(area) || !upLandmark.equals(landmark) || !upCityId.equals(cityId)
                    || !upPincode.equals(pincode) || upStateId != stateId) {
                userProfile.setTitle(title);
                userProfile.setFirstName(firstName);
                // userProfile.setStreet(street);
                userProfile.setAddress1(address);
                userProfile.setArea(area);
                userProfile.setLandmark(landmark);
                userProfile.setFkCityMasterId(cityId);
                userProfile.setPostalCode(pincode);
                userProfile.setFkStateMasterId(stateId);
                userProfile.setLastUpdate(new Timestamp((new Date()).getTime()));
                UserProfile userProfile1 = userProfileDAO.update(userProfile);
                if (userProfile1 != null) {
                    List<TxnFulfilment> txnFulfilmentList = txnFulfilmentDAO.findByProperty("id", txnfullfilmentId);
                    if (txnFulfilmentList != null && txnFulfilmentList.size() == 1) {
                        TxnFulfilment txnFulfilment = txnFulfilmentList.get(0);
                        // txnFulfilment.setDeliveryStreet(address);
                        txnFulfilment.setDeliveryAdd(address);
                        txnFulfilment.setDeliveryArea(area);
                        txnFulfilment.setDeliveryLandmark(landmark);
                        txnFulfilment.setDeliveryFkCityMasterId(cityId);
                        txnFulfilment.setDeliveryPincode(pincode);
                        txnFulfilment.setDeliveryFkStateMasterId(stateId);
                        txnFulfilment.setUpdatedAt(new Timestamp((new Date()).getTime()));
                        txnFulfilment.setFkUserProfileId(userProfile.getUserProfileId());
                        TxnFulfilment txnFulfilment1 = txnFulfilmentDAO.update(txnFulfilment);
                        if (txnFulfilment1 != null) {
                            ((AddressBusinessDO) baseBusinessDO).setStatus("success");
                        } else {
                            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                        }
                    } else {
                        ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                    }
                } else {
                    ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                }
            } else {
                // no need to do any thing
                ((AddressBusinessDO) baseBusinessDO).setStatus("success");
            }

        } else {
            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
        }

    }

    public void saveOrUpdateProfile(final BaseBusinessDO baseBusinessDO, final Integer userRechargeContactId,
            final String email, final Integer userProfileId, final Integer txnfulfilmentId) {
        AddressBusinessDO addressBusinessDO = (AddressBusinessDO) baseBusinessDO;
        List<UserProfile> userProfilelist = userProfileDAO.findByProperty("userProfileId", userProfileId);
        if (!FCUtil.isEmpty(userProfilelist)) {
            UserProfile profile = userProfilelist.get(0);
            String profileTitle = profile.getTitle();
            String profileFirstName = profile.getFirstName();
            // String p_street = profile.getStreet();
            String profileAddress = profile.getAddress1();
            String profileArea = profile.getArea();
            String profileLandmark = profile.getLandmark();
            Integer profileStateId = profile.getFkStateMasterId();
            Integer profileCity = profile.getFkCityMasterId();
            String prrofilePincode = profile.getPostalCode();
            String title = addressBusinessDO.getTitle();
            String firstName = addressBusinessDO.getFirstName();
            // String street = addressBusinessDO.getStreet();
            String address = addressBusinessDO.getAddress();
            String area = addressBusinessDO.getArea();
            String landmark = addressBusinessDO.getLandmark();
            Integer stateId = addressBusinessDO.getStateId();
            Integer city = addressBusinessDO.getCityId();
            String pincode = addressBusinessDO.getPostalCode();
            if (!profileTitle.equals(title) || !profileFirstName.equals(firstName) || !profileAddress.equals(address)
                    || !profileArea.equals(area) || !profileLandmark.equals(landmark) || profileStateId != stateId
                    || !profileCity.equals(city) || !prrofilePincode.equals(pincode)) {
                UserProfile userProfile = new UserProfile();
                userProfile.setTitle(title);
                userProfile.setFirstName(firstName);
                // userProfile.setStreet(street);
                userProfile.setAddress1(address);
                userProfile.setArea(area);
                userProfile.setLandmark(landmark);
                userProfile.setFkCityMasterId(city);
                userProfile.setFkStateMasterId(stateId);
                userProfile.setFkCountryMasterId(addressBusinessDO.getCountryId());
                userProfile.setPostalCode(pincode);
                userProfile.setFkUserRechargeContactId(userRechargeContactId);
                userProfile.setFkUserId(addressBusinessDO.getUserId());
                userProfile.setIsActive(true);
                userProfile.setIsDefault(false);
                userProfile.setCreatedOn(new Timestamp((new Date()).getTime()));
                try {
                    UserProfile userProfileObj = userProfileDAO.create(userProfile);
                    Integer userProfileId1 = userProfileObj.getUserProfileId();
                    List<TxnFulfilment> txnFulfilmentList = txnFulfilmentDAO.findByProperty("id", txnfulfilmentId);
                    if (!FCUtil.isEmpty(txnFulfilmentList)) {
                        TxnFulfilment fulfilment = txnFulfilmentList.get(0);
                        if (fulfilment != null) {

                            fulfilment.setDeliveryAdd(addressBusinessDO.getAddress());
                            fulfilment.setDeliveryArea(addressBusinessDO.getArea());
                            fulfilment.setDeliveryLandmark(addressBusinessDO.getLandmark());
                            fulfilment.setDeliveryFkCityMasterId(addressBusinessDO.getCityId());
                            fulfilment.setDeliveryPincode(addressBusinessDO.getPostalCode());
                            fulfilment.setDeliveryFkStateMasterId(addressBusinessDO.getStateId());
                            fulfilment.setFirstName(addressBusinessDO.getFirstName());
                            fulfilment.setDeliveryName(addressBusinessDO.getFirstName());
                            fulfilment.setUpdatedAt(new Timestamp((new Date()).getTime()));
                            fulfilment.setFkUserProfileId(userProfileId1);
                            TxnFulfilment txnFulfilment1 = txnFulfilmentDAO.update(fulfilment);
                            if (txnFulfilment1 != null) {
                                ((AddressBusinessDO) baseBusinessDO).setUserProfileId(userProfileId1);
                                ((AddressBusinessDO) baseBusinessDO).setIsEdit("no");
                                ((AddressBusinessDO) baseBusinessDO).setDefaultProfile("no");
                                ((AddressBusinessDO) baseBusinessDO).setStatus("success");
                            } else {
                                ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                            }
                        } else {
                            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                        }

                    } else {
                        ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                    }

                } catch (Exception e) {
                    ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
                    logger.error("Error while saving/updating User profile: ", e);
                }

            } else {
                // nothing to do any thing
                ((AddressBusinessDO) baseBusinessDO).setStatus("success");
            }

        } else {
            ((AddressBusinessDO) baseBusinessDO).setStatus("fail");
        }

    }

}
