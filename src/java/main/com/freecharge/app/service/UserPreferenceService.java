package com.freecharge.app.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.UserPreferenceRepository;
import com.mongodb.DBObject;

@Service
public class UserPreferenceService {

    public static final String ID_KEY = "_id";
    public static final String PREF_ID = "prefId";
    public static final String USEREMAIL_KEY = "userEMail";
    public static final String TYPE_KEY = "type";
    public static final String RANK_KEY = "rank";
    public static final String LASTUSED_KEY = "lastUsed";
    public static final String USAGECOUNT_KEY = "usageCount";
    public static final String USERPREFMAP_KEY = "userPrefMap";
    public static final String CREATEDON_KEY = "createdOn";
    public static final String USER_PREF_ID_SEQUENCE_NAME = "user_pref_id_sequence";
    public static final String USER_RATING_ANDROID_APP = "userRating";

    private Logger logger = LoggingFactory.getLogger(UserPreferenceService.class);

    @Autowired
    private UserPreferenceRepository userPreferenceRepository;

    /*public void savePreference(final String userEmail, Map<String, Object> userPrefMap, final String type,
            final String userRating) {
        Map<String, Object> preferenceMap = new LinkedHashMap<>();
        preferenceMap.put(USEREMAIL_KEY, userEmail);
        preferenceMap.put(TYPE_KEY, type);
        if (userRating != null && !userRating.isEmpty()){
        	preferenceMap.put(USER_RATING_ANDROID_APP, userRating);
        }
        if (userPrefMap != null && !userPrefMap.isEmpty()){
        	preferenceMap.put(USERPREFMAP_KEY, userPrefMap);	
        }
        preferenceMap.put(CREATEDON_KEY, new Date());
        userPreferenceRepository.savePreference(preferenceMap);
    }*/

    public List<DBObject> getAllUserPreferences(String userEmail) {
        List<DBObject> pref = userPreferenceRepository.getAllUserPreferences(userEmail);
        for (DBObject obj : pref) {
            obj.removeField(ID_KEY);
        }
        return pref;
    }

    public List<DBObject> getUserPreferencesByUserIdandType(String userEmail, String type) {
        List<DBObject> pref = userPreferenceRepository.getUserPreferencesByUserandType(userEmail, type);
        for (DBObject obj : pref) {
            obj.removeField(ID_KEY);
        }
        return pref;
    }

    public void deletePreference(String userEmail, String preferenceId) {
        userPreferenceRepository.deletePreference(userEmail, preferenceId);
    }

    public void deleteAllUserPreferences(String userEmail) {
        userPreferenceRepository.deleleAllUserPreferences(userEmail);
    }

/*    public void editPreference(final String predId, final String userEmail, final String userRating,
            final Map<String, Object> userPrefMap) {
        Map<String, Object> requestMap = new LinkedHashMap<>();
        requestMap.put(USEREMAIL_KEY, userEmail);
        requestMap.put(PREF_ID, predId);
        if (userRating != null && !userRating.isEmpty()){
        	requestMap.put(USER_RATING_ANDROID_APP, userRating);	
        }
        if (userPrefMap != null && !userPrefMap.isEmpty()){
        	requestMap.put(USERPREFMAP_KEY, userPrefMap);	
        }
        
        userPreferenceRepository.editPreference(requestMap);
    }*/

    public String insertOrUpdatePreference(final String prefId, final String userEmailId, final Map<String, Object> userPrefMap,
            final String type, final String userRating) {
        Map<String, Object> preferenceMap = new LinkedHashMap<>();
        preferenceMap.put(USEREMAIL_KEY, userEmailId);
        preferenceMap.put(TYPE_KEY, type);
        if (prefId != null){
        	preferenceMap.put(PREF_ID, prefId);	
        } 
        if (userRating != null){
        	preferenceMap.put(USER_RATING_ANDROID_APP, userRating);	
        }
        if (userPrefMap != null && !userPrefMap.isEmpty()){
        	preferenceMap.put(USERPREFMAP_KEY, userPrefMap);	
        }
        userPreferenceRepository.upsertPreference(preferenceMap);
        return String.valueOf(preferenceMap.get(UserPreferenceService.PREF_ID));
        /*List<DBObject> allUserList = getAllUserPreferences(userEmailId);
        if (allUserList.size() != 0) {
            for (DBObject allUsers : allUserList) {
                if (allUsers.get(USEREMAIL_KEY).equals(userEmailId)) {
                    preferenceMap.put(PREF_ID, allUsers.get(PREF_ID));
                    userPreferenceRepository.editPreference(preferenceMap);
                } else {
                    userPreferenceRepository.savePreference(preferenceMap);
                }
            }
        } else {
            userPreferenceRepository.savePreference(preferenceMap);
        }*/
    }
}
