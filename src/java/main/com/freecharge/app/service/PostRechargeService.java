package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.service.RechargeInitiateService.ProductType;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.payment.services.PaymentRetryService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.web.webdo.PostRechargeDetailsWebDo;
import com.freecharge.web.webdo.RechargeInitiateWebDo;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Service
public class PostRechargeService {

	private static final String ERROR_BUCKET_MAP = "errorBucketMap";
	private static final String SUCCESS_ACTION = "success";
	private static final String FAILURE_ACTION = "failure";
	private static final String UP_ACTION = "UP";
	private static final String GENERIC_FAILURE_ACTION = "UnknownError";
	private static final String GENERIC_SUCCESS_ACTION = "Success";
	private static final String GENERIC_UP_ACTION = "RechargeUnderProcess";
	private static final String GENERIC_SUCCESS_WHY ="Generally operator sends success SMS to registered mobile no. If account balance doesn't update for long, please check with #operator# customer care";	

	@Autowired
	private InService inService;

	@Autowired
	private AppConfigService appConfig;

	@Autowired
	private BillPaymentService billPaymentService;

	@Autowired
	private OrderIdReadDAO orderIdReadDAO;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	PaymentRetryService paymentRetryService;

	@Autowired
	RechargeInitiateService rechargeInitiateService;

    @Autowired
    OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private SMSService smsService;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;
	
	@Autowired
	private AppConfigService appConfigService;

	private final Logger logger = LoggingFactory.getLogger(getClass());

	private final ArrayList<String> actionableBucket = new ArrayList<String>(Arrays.asList("InvalidDenomination",
			"InvalidNoDenomination", "InvalidOperatorCircle", "InvalidServiceClassDenomination","ConnectionTimeOut"));

	public PostRechargeDetailsWebDo getPostRechargeDetails(String orderId, RechargeInitiateWebDo rechargeInitiateWebDo,
			RechargeDetails rechargeDetails, Boolean deeplinkEnabled) {
		String agRespCode = null;
		String inResponseCode = null;
		PostRechargeDetailsWebDo postRechargeDetails = null;
		String operatorTxnId = null;
		ProductType productType = rechargeInitiateService.getFinalProductType(orderId,
				rechargeInitiateWebDo.getProductType());
		if (FCUtil.isUtilityPaymentType(rechargeInitiateWebDo.getProductType())) {
			String billId= orderIdSlaveDAO.getBillIdForOrderId(orderId, rechargeInitiateWebDo.getProductType());
			BillTransaction billTransaction = billPayServiceRemote.getBillTransaction(billId);
			if (inService.isSuccessfulStatusCode(billTransaction.getTransactionStatus())) {
				inResponseCode = AggregatorInterface.SUCCESS_IN_RESPONSE_CODE;
			} else if (inService.isPending(billTransaction.getTransactionStatus())) {
				inResponseCode = AggregatorInterface.PENDING_IN_RESPONSE_CODE;
			} else {
				inResponseCode = "29";
			}			
		}
		try {
			if (inService.isPending(rechargeInitiateWebDo.getRechargeResponse())) {
				inResponseCode = AggregatorInterface.PENDING_IN_RESPONSE_CODE;
			} else {
				if (productType == ProductType.RechargeType) {
					InResponse inResponse = inService.getInResponseByOrderID(orderId);
					agRespCode = inResponse.getAggrRespCode();
					inResponseCode = inResponse.getInRespCode();
					operatorTxnId = inResponse.getOprTransId();
				} else if (productType == ProductType.PostPaidType) {
					BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
					agRespCode = billPaymentStatus.getAuthStatus();
					if (billPaymentStatus.getSuccess()) {
						inResponseCode = AggregatorInterface.SUCCESS_IN_RESPONSE_CODE;
					} else {
						// Some dummy failure code
						inResponseCode = "29";
					}

				}
			}
			JSONObject configJson = appConfig.getCustomProp(appConfig.POSTRECHARGE_CUSTOM_CONFIG);
			String configString = configJson.toJSONString();
			Map<String, Object> configMap = getMapFromString(configString);
			logger.info("configString" + configString);
			logger.info("agRespCoe:" + agRespCode + " inResponseCode" + inResponseCode);
			if (inService.isSuccessfulStatusCode(inResponseCode)) {
				postRechargeDetails = getSuccessfulResponse(configMap, orderId, rechargeInitiateWebDo, rechargeDetails,operatorTxnId, deeplinkEnabled);
			} else if (inService.isFailure(inResponseCode)) {
				postRechargeDetails = getFailureResponse(configMap, agRespCode, orderId, rechargeInitiateWebDo,
						rechargeDetails ,operatorTxnId, deeplinkEnabled);
			} else if (inService.isPending(inResponseCode)) {
				postRechargeDetails = getUPResponse(configMap, orderId, rechargeInitiateWebDo, rechargeDetails ,operatorTxnId, deeplinkEnabled);
			}
		} catch (Exception e) {
			logger.error("Exception caught is ", e);
		}

		return postRechargeDetails;
	}
	
	


	private PostRechargeDetailsWebDo getUPResponse(Map<String, Object> configMap, String orderId,
			RechargeInitiateWebDo riwd, RechargeDetails rechargeDetails, String operatorTxnId, Boolean deeplinkEnabled) {
		String errorBucket = GENERIC_UP_ACTION;
		String errorBucketInfoString = (String) configMap.get(errorBucket);
		Map<String, Object> errorBucketInfo = getMapFromString(errorBucketInfoString);
		return formPostRechargeDetailsResponse(errorBucketInfo, errorBucket, UP_ACTION, orderId, riwd, rechargeDetails ,operatorTxnId, deeplinkEnabled);
	}
	

	private PostRechargeDetailsWebDo getFailureResponse(Map<String, Object> configMap, String agRespCode,
			String orderId, RechargeInitiateWebDo riwd, RechargeDetails rechargeDetails, String operatorTxnId, Boolean deeplinkEnabled) {
		PostRechargeDetailsWebDo postRechargeDetailsWebDo = null;
		JSONObject configJson = appConfig.getCustomProp(appConfig.POSTRECHARGE_CUSTOM_CONFIG);
		String configString = configJson.toJSONString();
		String errorBucketMapString = getValueFromMapString(configString, ERROR_BUCKET_MAP);
		String errorBucket = getValueFromMapString(errorBucketMapString, agRespCode);
		logger.info("Error Bucket is " + errorBucket);
		if (FCUtil.isEmpty(errorBucket)) {
			logger.info("its generic");
			errorBucket = GENERIC_FAILURE_ACTION;
		}
		String errorBucketInfoString = (String) configMap.get(errorBucket);
		Map<String, Object> errorBucketInfo = getMapFromString(errorBucketInfoString);
		postRechargeDetailsWebDo = formPostRechargeDetailsResponse(errorBucketInfo, errorBucket, FAILURE_ACTION,
				orderId, riwd, rechargeDetails ,operatorTxnId, deeplinkEnabled);

		return postRechargeDetailsWebDo;
	}
	

	@SuppressWarnings("serial")
	private String getValueFromMapString(String mapString, String key) {
		Map<String, Object> configMap = new Gson().fromJson(mapString, new TypeToken<HashMap<String, Object>>() {
		}.getType());
		String value = (String) configMap.get(key);
		return value;
	}

	@SuppressWarnings("serial")
	private Map<String, Object> getMapFromString(String mapString) {
		Map<String, Object> map = new Gson().fromJson(mapString, new TypeToken<HashMap<String, String>>() {
		}.getType());
		return map;
	}

	private PostRechargeDetailsWebDo formPostRechargeDetailsResponse(Map<String, Object> errorBucketInfo,
			String errorBucket, String action, String orderId, RechargeInitiateWebDo riwd,
			RechargeDetails rechargeDetails, String operatorTxnId, Boolean deeplinkEnabled) {
		PostRechargeDetailsWebDo postRechargeDetailsWebDo = new PostRechargeDetailsWebDo();
		String emailId = userServiceProxy.getEmailIdFromOrderId(orderId);
		String firstName = userServiceProxy.getUserProfile(emailId).getOnlyFirstName();
		postRechargeDetailsWebDo = setPostRechargeDetailsByAction(action, errorBucket, errorBucketInfo, rechargeDetails,
				firstName, riwd ,operatorTxnId, deeplinkEnabled);
		return postRechargeDetailsWebDo;
	}
	

	private PostRechargeDetailsWebDo setPostRechargeDetailsByAction(String action, String errorBucket,
			Map<String, Object> errorBucketInfo, RechargeDetails rechargeDetails, String firstName,
			RechargeInitiateWebDo riwd, String operatorTxnId, Boolean deeplinkEnabled) {
		PostRechargeDetailsWebDo postRechargeDetailsWebDo = new PostRechargeDetailsWebDo();

		if (!FCUtil.isEmpty(errorBucketInfo) || !FCUtil.isEmpty(errorBucket)) {
			postRechargeDetailsWebDo.setActionBucket(errorBucket);
			postRechargeDetailsWebDo.setTitle(formAPIStringFromTemplate(
					(String) errorBucketInfo.get(PRSConstants.TITLE), rechargeDetails, firstName, riwd, operatorTxnId));
			postRechargeDetailsWebDo
			.setCareMsg(formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.CARE_MSG),
					rechargeDetails, firstName, riwd, operatorTxnId));
			postRechargeDetailsWebDo.setActionBucket((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET));
			if (actionableBucket.contains(errorBucket)) {
				postRechargeDetailsWebDo.setActionBucketDeepLink(
						formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_DEEPLINK),
								rechargeDetails, firstName, riwd, operatorTxnId));
			}
			postRechargeDetailsWebDo.setActionBucketCTA(
					formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_CTA),
							rechargeDetails, firstName, riwd, operatorTxnId));
			if (action.equals(SUCCESS_ACTION)) {
				handlePostResponseForSuccess(postRechargeDetailsWebDo,
						(String) errorBucketInfo.get(PRSConstants.REASON_MSG),
						(String) errorBucketInfo.get(PRSConstants.NEXT_MSG), firstName, rechargeDetails, riwd,
						operatorTxnId);
			} else {
				if(action.equals(FAILURE_ACTION)) {
					if(errorBucketInfo.containsKey(PRSConstants.ACTION_BUCKET_CTA)) {
						logger.info("cta value: " + formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_CTA),
								rechargeDetails, firstName, riwd, operatorTxnId) );
					}
					if(errorBucketInfo.containsKey(PRSConstants.ACTION_BUCKET_DEEPLINK)) {
						logger.info("deeplink value: " + formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_DEEPLINK),
								rechargeDetails, firstName, riwd, operatorTxnId) );
					}
				}
				
				postRechargeDetailsWebDo
				.setReasonMsg(formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.REASON_MSG),
						rechargeDetails, firstName, riwd, operatorTxnId));
				postRechargeDetailsWebDo
				.setNextMsg(formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.NEXT_MSG),
						rechargeDetails, firstName, riwd, operatorTxnId));
				postRechargeDetailsWebDo.setIsActionRequired(true);
				if(deeplinkEnabled) {
					postRechargeDetailsWebDo.setActionBucketCTA(formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_CTA),
							rechargeDetails, firstName, riwd, operatorTxnId));
					postRechargeDetailsWebDo.setActionBucketDeepLink(formAPIStringFromTemplate((String) errorBucketInfo.get(PRSConstants.ACTION_BUCKET_DEEPLINK),
							rechargeDetails, firstName, riwd, operatorTxnId));
				}
			}

		}

		return postRechargeDetailsWebDo;
	}
	

	private void handlePostResponseForSuccess(PostRechargeDetailsWebDo postRechargeDetailsWebDo, String templateReason,
			String templateWhy, String firstName, RechargeDetails rechargeDetails, RechargeInitiateWebDo riwd,
			String operatorTxnId) {
		StringBuffer reasonMsgTemplate = new StringBuffer();
		String[] templateArray = templateReason.split(":");
		reasonMsgTemplate.append(templateArray[0]);
		if (!FCUtil.isEmpty(operatorTxnId) && templateArray.length > 1) {
			reasonMsgTemplate.append(templateArray[1]);
		} else {
			templateWhy = GENERIC_SUCCESS_WHY;
		}
		postRechargeDetailsWebDo.setReasonMsg(formAPIStringFromTemplate(reasonMsgTemplate.toString(), rechargeDetails,
				firstName, riwd, operatorTxnId));
		postRechargeDetailsWebDo
				.setNextMsg(formAPIStringFromTemplate(templateWhy, rechargeDetails, firstName, riwd, operatorTxnId));
	}

	private String formAPIStringFromTemplate(String template, RechargeDetails rechargeDetails, String firstName,
			RechargeInitiateWebDo riwd, String operatorTxnId) {
		Map<String, String> templateVariableMap = getTemplateMap(rechargeDetails, firstName, riwd, operatorTxnId);
		if (!FCUtil.isEmpty(template)) {
			for (String key : templateVariableMap.keySet()) {
				if (templateVariableMap.get(key) != null) {
					template = template.replace("#" + key + "#", templateVariableMap.get(key));
				} else {
					template = template.replace("#" + key + "#", "");
				}
			}
		}
		return template;
	}

	private Map<String, String> getTemplateMap(RechargeDetails rechargeDetails, String firstName, RechargeInitiateWebDo riwd, String operatorTxnId) {
		Map<String, String> templateVariables = new HashMap<String, String>();
		String type = getType(riwd.getProductType());
		String rechargeType = getRechargeType(riwd.getProductType());
		String ussd = smsService.getOperatorUssdCode(rechargeDetails.getOperatorName());
		String paymentSource = getPaymentSource(riwd.getRefundTo());
		templateVariables.put("name", firstName);
		templateVariables.put("amount", rechargeDetails.getAmount());
		templateVariables.put("operator", rechargeDetails.getOperatorName());
		templateVariables.put("circle", rechargeDetails.getCircleName());
		templateVariables.put("serviceNumber", rechargeDetails.getServiceNumber());
		templateVariables.put("productType", riwd.getProductType());
		templateVariables.put("type", type);
		templateVariables.put("operatorId", rechargeDetails.getOperatorMasterId());
		templateVariables.put("circleId", rechargeDetails.getCircleId());	
		if(!FCUtil.isEmpty(operatorTxnId)){
			templateVariables.put("operatorTxnId", operatorTxnId);
		}else{
			templateVariables.put("operatorTxnId", "");
		}
		templateVariables.put("ussd", ussd);
		templateVariables.put("rechargeType", rechargeType);
		templateVariables.put("paymentSource",paymentSource);
		return templateVariables;
	}

	private String getPaymentSource(String refundTo) {
		String paymentSource = "source";
		if (PaymentConstants.ONE_CHECK_WALLET_CODE.equals(refundTo)) {
			return "Freecharge Wallet";
		}
		return paymentSource;
	}

	private String getRechargeType(String productType) {
		if (FCUtil.isRechargeBillerProductType(productType)) {
			return "Recharge";
		}
		return "Bill Payment";
	}

	private String getType(String productType) {
		String type = "";
		switch (productType) {
		case "V":
			type = "prepaid";
			break;

		case "M":
			type = "postpaid";
			break;

		case "D":
			type = "dth";
			break;

		case "C":
		case "F":
			type = "datacard";
			break;
		}
		return type;
	}

	private PostRechargeDetailsWebDo getSuccessfulResponse(Map<String, Object> configMap, String orderId,
			RechargeInitiateWebDo riwd, RechargeDetails rechargeDetails, String operatorTxnId, Boolean deeplinkEnabled) {
		String errorBucket = GENERIC_SUCCESS_ACTION;
		String errorBucketInfoString = (String) configMap.get(errorBucket);
		Map<String, Object> errorBucketInfo = getMapFromString(errorBucketInfoString);
		return formPostRechargeDetailsResponse(errorBucketInfo, errorBucket, SUCCESS_ACTION, orderId, riwd,
				rechargeDetails , operatorTxnId, deeplinkEnabled);
	}

	
	public PostRechargeDetailsWebDo getGoogleCreditsPostRechargeDetails(String orderId, RechargeInitiateWebDo rechargeInitiateWebDo,
			RechargeDetails rechargeDetails){
		PostRechargeDetailsWebDo postRechargeDetailsWebDo = new PostRechargeDetailsWebDo();
		postRechargeDetailsWebDo.setActionBucket(GENERIC_UP_ACTION);
		postRechargeDetailsWebDo.setTitle(FCConstants.GOOGLE_CREDITS_SUCCESS_TITLE);
		postRechargeDetailsWebDo.setReasonMsg(FCConstants.GOOGLE_CREDITS_UNDER_PROCESS_REASON_MSG);
		postRechargeDetailsWebDo.setNextMsg(FCConstants.GOOGLE_CREDITS_UNDER_PROCESS_NEXT_MSG);
		String inResponseCode = null;
		
		
		JSONObject configJson = appConfigService.getCustomProp(appConfigService.GOOGLE_CREDITS_POST_RECHARGE_CUSTOM_CONFIG);
		String configString = configJson.toJSONString();
		Map<String, Object> configMap = getMapFromString(configString);
		
		try {
			InResponse inResponse = inService.getInResponseByOrderID(orderId);
			inResponseCode = inResponse.getInRespCode();
			if (inService.isPending(rechargeInitiateWebDo.getRechargeResponse())) {
				inResponseCode = AggregatorInterface.PENDING_IN_RESPONSE_CODE;
			} 
			if (inService.isSuccessfulStatusCode(inResponseCode)) {
				postRechargeDetailsWebDo.setActionBucket(SUCCESS_ACTION);
				postRechargeDetailsWebDo.setTitle(FCConstants.GOOGLE_CREDITS_SUCCESS_TITLE);
				postRechargeDetailsWebDo.setReasonMsg(FCConstants.GOOGLE_CREDITS_SUCCESS_REASON_MSG);
				postRechargeDetailsWebDo.setNextMsg(String.format((String) configMap.get(FCConstants.GOOGLE_CREDITS_SUCCESS_NEXT_MSG),rechargeInitiateWebDo.getEmail(), rechargeDetails.getServiceNumber()));
				postRechargeDetailsWebDo.setHowToUseAndroid((String) configMap.get("howToUseAndroid"));
				postRechargeDetailsWebDo.setHowToUseWeb((String) configMap.get("howToUseWeb"));
			} else if (inService.isFailure(inResponseCode)) {
				postRechargeDetailsWebDo.setActionBucket(FAILURE_ACTION);
				postRechargeDetailsWebDo.setTitle(FCConstants.GOOGLE_CREDITS_FAILURE_TITLE);
				postRechargeDetailsWebDo.setReasonMsg(FCConstants.GOOGLE_CREDITS_FAILURE_REASON_MSG);
				postRechargeDetailsWebDo.setNextMsg(FCConstants.GOOGLE_CREDITS_FAILURE_NEXT_MSG);
			} 
		} catch (Exception e) {
			logger.error("Exception caught is ", e);
		}
		return postRechargeDetailsWebDo;
	}
}
