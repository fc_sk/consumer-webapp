package com.freecharge.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.cache.CacheManager;
import com.freecharge.payment.dos.entity.BankMaster;
import com.freecharge.payment.dos.entity.PaymentGatewayMaster;
import com.freecharge.payment.dos.entity.PgMisUser;
import com.freecharge.payment.dos.entity.PgRelation;

@Service
public class PgMemcachedService extends CacheManager {

	private static String PAYMENT_GATEWAY_MASTER_CACHE = "paymentGatewayMasterCache";
	private static String PAYMENT_TYPE_MASTER_CACHE = "paymentTypeMasterCache";
	private static String PAYMENT_TYPE_OPTIONS_CACHE = "paymentTypeOptioncache";
	private static String PG_MIS_USER_CACHE = "pgMisUserCache";
	private static String PRODUCT_MASTER_CACHE = "productMasterCache";
	private static String PAYMENT_TYPE_RELATION_CACHE = "paymentTypeRelation";
	private static String PG_RELATION_CACHE = "PgRelationcache";
	private static String BANK_MASTER_CACHE = "bankMasterCache";
	private static String PAYMENT_OPTION_CACHE = "paymentOptionCache";

	// bank Master

	public BankMaster getBankMaster(String uiCode, Integer paymentOption) {
		Map<String, BankMaster> map = (HashMap<String, BankMaster>) get(BANK_MASTER_CACHE);
		if (map != null && map.size() > 0)
			return map.get(uiCode + paymentOption);
		else
			return null;
		// return (BankMaster) get(BANK_MASTER_CACHE);
	}

	public void setBankMaster(List<BankMaster> bankMasters) {
		Map<String, BankMaster> bankMap = new HashMap<String, BankMaster>();
		if (bankMasters != null && bankMasters.size() > 0) {

			for (BankMaster banMaster : bankMasters) {
				if (bankMap.get(banMaster.getCodeUI() + banMaster.getPaymentOption().toString()) == null) {
					bankMap.put(banMaster.getCodeUI() + banMaster.getPaymentOption().toString(), banMaster);
				}
			}
			set(BANK_MASTER_CACHE, bankMap);
		}
	}

	// Payment Gateway Master Cache

	public PaymentGatewayMaster getPaymentGatewayMaster(Integer gatewayId) {
		Map<Integer, PaymentGatewayMaster> payMap = (HashMap<Integer, PaymentGatewayMaster>) get(PAYMENT_GATEWAY_MASTER_CACHE);
		if (payMap != null && payMap.size() > 0) {
			return payMap.get(gatewayId);
		}
		return null;
	}

	public void setPaymentGatewayMaster(List<PaymentGatewayMaster> paymentGatewayMasters) {
		Map<Integer, PaymentGatewayMaster> paymentMap = new HashMap<Integer, PaymentGatewayMaster>();
		if (paymentGatewayMasters != null && paymentGatewayMasters.size() > 0) {

			for (PaymentGatewayMaster paymentGatewayMaster : paymentGatewayMasters) {

				if (paymentMap.get(paymentGatewayMaster.getId()) == null)
					paymentMap.put(paymentGatewayMaster.getId(), paymentGatewayMaster);
			}
			set(PAYMENT_GATEWAY_MASTER_CACHE, paymentMap);
		}
	}

	// Pg Relation cache

	public void setPgRelation(List<PgRelation> pgRelations) {
		if (pgRelations != null && pgRelations.size() > 0) {
			Map<String, PgRelation> map = new HashMap<String, PgRelation>();
			for (PgRelation pgRelation : pgRelations) {
				if (map.get(pgRelation.getProductId().toString() + pgRelation.getAffiliateId() + pgRelation.getBankMasterId()) == null) {
					map.put(pgRelation.getProductId().toString() + pgRelation.getAffiliateId() + pgRelation.getBankMasterId(), pgRelation);
				}
			}
			set(PG_RELATION_CACHE, map);
		}
	}

	public List<PaymentTypeOptions> getPaymentTypeOptions() {
		return (List<PaymentTypeOptions>) get(PAYMENT_TYPE_OPTIONS_CACHE);
	}

	public void setPaymentTypeOptions(List<PaymentTypeOptions> paymentTypeOptions) {
		if (paymentTypeOptions != null && paymentTypeOptions.size() > 0)
		    set(PAYMENT_TYPE_OPTIONS_CACHE, paymentTypeOptions);
	}

	public List<PgMisUser> getPgMisUser() {
		return (List<PgMisUser>) get(PG_MIS_USER_CACHE);
	}

	public void setPgMisUser(List<PgMisUser> pgMisUsers) {
		set(PG_MIS_USER_CACHE, pgMisUsers);
	}

	public List<ProductMaster> getProductMaster() {
		return (List<ProductMaster>) get(PRODUCT_MASTER_CACHE);
	}

	public void setProductMaster(List<ProductMaster> productMasters) {
	    set(PRODUCT_MASTER_CACHE, productMasters);
	}

	public HashMap<String, PgRelation> getPgRelation() {
		return (HashMap<String, PgRelation>) get(PG_RELATION_CACHE);
	}

	public List<PaymentTypeMaster> getPaymentOptions() {
		return (List<PaymentTypeMaster>) get(PAYMENT_TYPE_MASTER_CACHE);
	}

	public void setPaymentOptions(List<PaymentTypeMaster> paymentTypeMasters) {
		if (paymentTypeMasters != null && paymentTypeMasters.size() > 0)
		    set(PAYMENT_TYPE_MASTER_CACHE, paymentTypeMasters);
	}

}
