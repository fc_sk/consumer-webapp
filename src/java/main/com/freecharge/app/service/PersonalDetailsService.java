package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.admin.model.FulfilmentShippingAddress;
import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserProfileDAO;
import com.freecharge.app.domain.dao.UserRechargeContactDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.TransactionFulfilment;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.businessdo.PersonalDetailsBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.util.WebConstants;
//import com.freecharge.app.domain.entity.UserProfile;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 16, 2012
 * Time: 10:52:17 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PersonalDetailsService {
    @Autowired
    private IRegistrationDAO registrationDAO;
    @Autowired
    private TxnHomePageDAO txnHomePageDAO;
    @Autowired
    private TxnFulfilmentDAO txnFulfilmentDAO;
    @Autowired
    private UserRechargeContactDAO userRechargeContactDAO;
    @Autowired
    private CommonService commonService;
    private final Logger logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private VoucherService voucherService;
    
    @Autowired
    private UserWriteDao userWriteDao;
    
	@Autowired
	private UserProfileDAO userProfileDAO;
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
    
    private static final String FAIL = "fail";
    private static final String SUCCESS = "success";
    
   
    public void getOperatorImage(Integer operatorId, String operatorName, PersonalDetailsBusinessDO personalDetailsBusinessDO) {
    	
    	Map<Integer,OperatorMaster> operatorMasterMap = commonService.getOperatorMasters();
		if(operatorMasterMap != null){
			String operatorImgUrl = null;
			OperatorMaster operatorMaster = operatorMasterMap.get(operatorId);
			if(operatorMaster != null)
				operatorImgUrl = operatorMaster.getImgUrl();
			personalDetailsBusinessDO.setOperatorImgUrl(operatorImgUrl);
			personalDetailsBusinessDO.setOperatorName(operatorName);
		}
		
	}
    
	@Transactional
	public String getOrCreatePersonalDetails(BaseBusinessDO baseBusinessDO) {
		PersonalDetailsBusinessDO personalDetailsBusinessDO = (PersonalDetailsBusinessDO) baseBusinessDO;
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Boolean isloggedin = false;
		String email = "";
		Integer urcId = 0;
		Boolean contactDetailsReq = false;
		String operatorName = null;
		Integer operatorId = 0;
		String circleName = null;
		Integer circleId = 0;
		Integer productTypeId = 0;
		String productType = null;                
		Float amount = null;
		Integer txnHomePageId = 0;             
		String serviceNumber = null;
		Integer userId = 0;
		Integer txnFulfilmentId = 0;
		if(fs != null){
			Map map = fs.getSessionData();

			if (map != null) {
				email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
					userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);
				}
			}
		}

		String lookupId =  personalDetailsBusinessDO.getLookupID();


		if (!StringUtils.isEmpty(lookupId)) {

			List<TxnHomePage> txnHomePageList = txnHomePageDAO.findByLookupId(lookupId);
			if(txnHomePageList != null && txnHomePageList.size() > 0) {
				TxnHomePage txnHomePage = txnHomePageList.get(0);
				if(txnHomePage != null){

					operatorName = txnHomePage.getOperatorName();
					operatorId = txnHomePage.getFkOperatorId();
					circleName = txnHomePage.getCircleName();
					circleId = txnHomePage.getFkCircleId();
					productTypeId = txnHomePage.getFkProductId();
					productType = txnHomePage.getProductType();                
					amount = txnHomePage.getAmount();
					txnHomePageId = txnHomePage.getId();             
					serviceNumber = txnHomePage.getServiceNumber();

					// 1. Fetch recharge no. details

					personalDetailsBusinessDO.setTxnHomePageId(txnHomePageId);
					personalDetailsBusinessDO.setCircleName(circleName);
					personalDetailsBusinessDO.setProductType(productType);
					personalDetailsBusinessDO.setServiceNumber(serviceNumber);

					// 2. Fetch operator details
					getOperatorImage(operatorId,operatorName,personalDetailsBusinessDO);

					// 3. Fetch User Recharge Contact and related profile

					if(serviceNumber != null && serviceNumber != "" && FCUtil.isRechargeProductType(productType)) {                  
						List <UserRechargeContact> userRechargeContacts = registrationDAO.getUserRechargeContact(email,serviceNumber);
						if(userRechargeContacts != null && userRechargeContacts.size()>0){
							UserRechargeContact contact = userRechargeContacts.get(0);
							if(contact != null){
								urcId = contact.getUserRechargeContactId();


								// 4. Update user recharge contact details
								if((contact.getFkOperatorMasterId() != operatorId || contact.getFkCircleMasterId() != circleId)) {
									contact.setFkCircleMasterId(circleId);
									contact.setFkOperatorMasterId(operatorId);
									contact = userRechargeContactDAO.update(contact);  
								}

							}
						} else if (urcId == 0) { // 5. Contact doesn't exist , create a new contact
							Integer value = createUserRechargeContact(userId, serviceNumber, operatorId, circleId, productTypeId, amount);
							urcId = value;
							if (value < 0)
								return FAIL;
						}
					}

				}
			}

		}

		List<Map<String, Object>> physicalCoupons = voucherService.getCouponDetailsFromCartBylookupIdAndCouponType(lookupId,FCConstants.COUPON_TYPE_P);
		if (physicalCoupons != null && !physicalCoupons.isEmpty()) {
			contactDetailsReq = true;

		}
		personalDetailsBusinessDO.setContactDetailsReq(contactDetailsReq);

		/* Removing the check for now. As we want to persist all user profile data along with txn details */
		// If there are physical coupons, we need contact details
		//if (contactDetailsReq) {
			//6. Fetch corresponding profile

		if (!getUserProfileInfo(personalDetailsBusinessDO, urcId, userId, email))
				return FAIL;
		//}

		// 7. Save entries in txn_fulfilment table
		txnFulfilmentId = createTxnFulfilment(baseBusinessDO, email);
		if ( txnFulfilmentId < 0 ) 
			return FAIL;

		// 8. Update cart_items with userId
		if (!updateCartItems(baseBusinessDO, userId))
			return FAIL;

		/*
		 *  9. If there is no default mobile no. of the user, then save this no. as the mobile no.We are commenting now
		 */
		/*if (productType.compareToIgnoreCase(RechargeConstants.MOBILE_PRODUCT_TYPE) == 0) {
			if(!checkAndUpdateContactNo(userId, email, serviceNumber))
				return FAIL;
		}*/

		// Explicitly set all available details in UserRechargeContact Object
		if (personalDetailsBusinessDO.getUserContactDetail() == null)
			personalDetailsBusinessDO.setUserContactDetail(new UserContactDetail());
		personalDetailsBusinessDO.getUserContactDetail().setUserId(userId);
		personalDetailsBusinessDO.getUserContactDetail().setUserRechargeContactId(urcId);
		personalDetailsBusinessDO.getUserContactDetail().setTxnFulfilmentId(txnFulfilmentId);
		personalDetailsBusinessDO.getUserContactDetail().setTxnHomePageId(txnHomePageId);
		personalDetailsBusinessDO.getUserContactDetail().setEmail(email);

		return SUCCESS;
	}

	public boolean getUserProfileInfo(PersonalDetailsBusinessDO personalDetailsBusinessDO, Integer urcId, Integer userId, String email) {
		try {
			List<UserContactDetail> list = registrationDAO.getUserContactDetails(email,personalDetailsBusinessDO.getServiceNumber());
			if(list!= null && list.size()>0){
				personalDetailsBusinessDO.setUserContactDetail(list.get(0));
				personalDetailsBusinessDO.setDefaultProfile("no");

				return true;
			} else {
				List<UserContactDetail> defaultInfoList = registrationDAO.getUserContactDefaultDetails(email);
				if(defaultInfoList != null && defaultInfoList.size() > 0){
					personalDetailsBusinessDO.setUserContactDetail(defaultInfoList.get(0));
					personalDetailsBusinessDO.setDefaultProfile("yes");
					return true;
				}
			}
			personalDetailsBusinessDO.setDefaultProfile("notExist");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Integer createUserRechargeContact(Integer userId, String serviceNumber, Integer operatorId, Integer circleId, Integer productTypeId, Float amount) {

		UserRechargeContact userRechargeContact = new UserRechargeContact();
		userRechargeContact.setFkUsersId(userId);
		userRechargeContact.setServiceNumber(serviceNumber);
		userRechargeContact.setFkOperatorMasterId(operatorId);
		userRechargeContact.setFkCircleMasterId(circleId);
		userRechargeContact.setFkProductMasterId(productTypeId);
		userRechargeContact.setCreatedOn(new Timestamp((new Date()).getTime()));
		if(amount != null)
			userRechargeContact.setLastRechargeAmount(new Double(amount));
		userRechargeContact.setIsActive(true);
		Integer value = registrationDAO.saveUserRechargeContact(userRechargeContact);

		return value;
	}

	public Integer createTxnFulfilment(BaseBusinessDO baseBusinessDO, String email){
		UserContactDetail ucd = ((PersonalDetailsBusinessDO)baseBusinessDO).getUserContactDetail();
		TxnFulfilment txnFulFilment=new TxnFulfilment();

		if (ucd != null) {
			Integer countryId=ucd.getCountryId();
			Integer ststeId= ucd.getStateId();
			String addr= ucd.getAddress1();

			String fname= ucd.getFirstName();
			String city=ucd.getCityName();
			String postalCode=ucd.getPostalCode();
			String mobile=ucd.getMobile();
			String area = ucd.getArea();
			String landmark = ucd.getLandmark();

			txnFulFilment.setFirstName(fname);
			txnFulFilment.setDeliveryName(fname);
			txnFulFilment.setAddressMobile(mobile);
			txnFulFilment.setDeliveryAdd(addr);
			txnFulFilment.setDeliveryArea(area);
			txnFulFilment.setDeliveryLandmark(landmark);
			txnFulFilment.setDeliveryFkCityMasterId(ucd.getCityId());
			txnFulFilment.setDeliveryPincode(postalCode);
			txnFulFilment.setDeliveryFkStateMasterId(ststeId);
			txnFulFilment.setDeliveryFkCountryMasterId(countryId);
			txnFulFilment.setFkUserProfileId(((PersonalDetailsBusinessDO)baseBusinessDO).getUserContactDetail().getUserProfileId());
		}

		txnFulFilment.setTxnHomePageId(((PersonalDetailsBusinessDO)baseBusinessDO).getTxnHomePageId());
		txnFulFilment.setCreatedAt(new Timestamp((new Date()).getTime()));
		txnFulFilment.setEmail(email);

		Integer value = txnFulfilmentDAO.save(txnFulFilment);

		return value;
	}

	@Transactional
	public Boolean updateCartItems(BaseBusinessDO baseBusinessDO, Integer userId) {
		Boolean cartUpdateStatus = commonService.updateCartWithUserId(userId, ((PersonalDetailsBusinessDO)baseBusinessDO).getLookupID());

		return cartUpdateStatus;
	}

	/*Getting user profile specific to service number if not returns default profile*/

	public FulfilmentShippingAddress getFulfilmentShippingAddress(TransactionFulfilment transactionFulfilment) {
		FulfilmentShippingAddress fulfilmentShippingAddress = new FulfilmentShippingAddress();
		
		fulfilmentShippingAddress.setFirstName(transactionFulfilment.getFirstName());
		fulfilmentShippingAddress.setAddress(transactionFulfilment.getDeliveryAdd());;
		fulfilmentShippingAddress.setArea(transactionFulfilment.getDeliveryArea());
		fulfilmentShippingAddress.setLandmar(transactionFulfilment.getDeliveryLandmark());
		fulfilmentShippingAddress.setCity(transactionFulfilment.getDeliveryFkCityMasterId());
		fulfilmentShippingAddress.setState(transactionFulfilment.getDeliveryFkStateMasterId());
		fulfilmentShippingAddress.setPincode(transactionFulfilment.getDeliveryPincode());
		fulfilmentShippingAddress.setOrderId(transactionFulfilment.getOrderId());
		fulfilmentShippingAddress.setCountry(transactionFulfilment.getDeliveryFkCountryMasterId());
		fulfilmentShippingAddress.setFkUserProfileId(transactionFulfilment.getFkUserProfileId());
		fulfilmentShippingAddress.setFulfilmentId(transactionFulfilment.getId());
		fulfilmentShippingAddress.setEmail(transactionFulfilment.getEmail());
		
		Integer userstate = 1;
		List<StateMaster> userstatelist = registrationDAO.getStateNameByStateId(transactionFulfilment.getDeliveryFkStateMasterId());
		if (userstatelist != null && userstatelist.size() > 0)
			userstate = userstatelist.get(0).getStateMasterId();
	
		List<CityMaster> usercitylist = cityMasterDAO.findByFkStateMasterId(userstate);
		fulfilmentShippingAddress.setCitylist(usercitylist);
		
		List<StateMaster> stateMasterList = registrationDAO.getStateMasterList();
		fulfilmentShippingAddress.setStatelist(stateMasterList);
		
		
		return fulfilmentShippingAddress;
	}
}