package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.jdbc.LocationReadDao;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.businessdo.AddressBusinessDO;
import com.freecharge.common.businessdo.CityMasterBusinessDO;
import com.freecharge.common.businessdo.CommonBusinessDO;
import com.freecharge.common.businessdo.HomeBusinessDo;
import com.freecharge.common.businessdo.MyProfileBusinessDO;
import com.freecharge.common.businessdo.PersonalDetailsBusinessDO;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCategoryMaster;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayCircleMaster;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.BillerDetails;

@Service(value = "commonService")
public class CommonService {
    @Autowired
    private CartDAO               cartDAO;
    @Autowired
    private IRegistrationDAO      registrationDAO;
    @Autowired
    private CityMasterDAO         cityMasterDAO;

    @Autowired
    private HomeBusinessDao       homeBusinessDao;

    @Autowired
    private LocationReadDao       locationReadDao;

    private OperatorCircleReadDAO operatorCircleReadDAO;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService    billPayServiceRemote;

    @Autowired
    public final void setOperatorCircleReadDAO(final OperatorCircleReadDAO operatorCircleReadDao) {
        this.operatorCircleReadDAO = operatorCircleReadDao;
    }

    private Logger logger = LoggingFactory.getLogger(CommonService.class);

    public final Boolean updateCartWithUserId(final Integer userId, final String lookupID) {
        logger.info("Before updating the cart with user id......User Id :" + userId + "  and lookup Id : " + lookupID);
        Cart cart = cartDAO.findByLookupId(lookupID);
        if (cart != null) {
            cart.setFkUserId(userId);
            Cart cart1 = cartDAO.update(cart);
            if (cart1 != null) {
                logger.info("Upadated cart with User Id :" + userId + "  and lookup Id : " + lookupID);
                return true;
            } else {
                logger.error("Failed while Upadating the cart with User Id :" + userId + "  and lookup Id : "
                        + lookupID);
                return false;
            }
        } else {
            logger.error("lookup id related cart not exist in the database,requested lookup id is: " + lookupID);
            return false;
        }
    }

    public final void getStateMasterList(final BaseBusinessDO baseBusinessDO) {
        List<StateMaster> stateMasters = this.getStateMasterList();

        if (CollectionUtils.isEmpty(stateMasters)) {
            logger.error("No State list returned from database");
            return;
        }

        if (baseBusinessDO instanceof MyProfileBusinessDO) {
            ((MyProfileBusinessDO) baseBusinessDO).setStateMaster(stateMasters);
        } else if (baseBusinessDO instanceof RegisterBusinessDO) {
            ((RegisterBusinessDO) baseBusinessDO).setStateMaster(stateMasters);
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            ((AddressBusinessDO) baseBusinessDO).setStateMaster(stateMasters);
        } else if (baseBusinessDO instanceof PersonalDetailsBusinessDO) {
            ((PersonalDetailsBusinessDO) baseBusinessDO).setStateMaster(stateMasters);
        } else if (baseBusinessDO instanceof HomeBusinessDo) {
            ((HomeBusinessDo) baseBusinessDO).setStateMaster(stateMasters);
        } else if (baseBusinessDO instanceof CommonBusinessDO) {
            ((CommonBusinessDO) baseBusinessDO).setStateMaster(stateMasters);
        }
    }

    public final List<StateMaster> getStateMasterList() {
        if (FCConstants.stateMasterList == null || FCConstants.stateMasterList.size() < 1) {
            synchronized (this) {
                if (FCConstants.stateMasterList == null || FCConstants.stateMasterList.size() < 1) {
                    refreshStateMasterListAndMap();
                }
            }
        }

        return FCConstants.stateMasterList;
    }

    public final Map<Integer, StateMaster> getStateMasterMap() {
        if (FCConstants.stateMasterMap == null || FCConstants.stateMasterMap.size() < 1) {
            synchronized (this) {
                if (FCConstants.stateMasterMap == null || FCConstants.stateMasterMap.size() < 1) {
                    refreshStateMasterListAndMap();
                }
            }
        }
        return FCConstants.stateMasterMap;
    }

    public final synchronized void refreshStateMasterListAndMap() {
        FCConstants.stateMasterList = registrationDAO.getStateMasterList();
        FCConstants.stateMasterMap = new HashMap<Integer, StateMaster>();
        for (StateMaster stateMaster : FCConstants.stateMasterList) {
            FCConstants.stateMasterMap.put(stateMaster.getStateMasterId(), stateMaster);
        }
    }

    public final void getState(final BaseBusinessDO baseBusinessDO) {
        Integer stateId = null;
        if (baseBusinessDO instanceof RegisterBusinessDO) {
            stateId = ((RegisterBusinessDO) baseBusinessDO).getStateId();
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            stateId = ((AddressBusinessDO) baseBusinessDO).getStateId();
        }
        List<StateMaster> stateList = registrationDAO.getStateNameByStateId(stateId);
        if (stateList != null && stateList.size() != 0) {
            StateMaster stateMaster = stateList.get(0);
            if (baseBusinessDO instanceof RegisterBusinessDO) {
                ((RegisterBusinessDO) baseBusinessDO).setState(stateMaster.getStateName());
            } else if (baseBusinessDO instanceof AddressBusinessDO) {
                ((AddressBusinessDO) baseBusinessDO).setState(stateMaster.getStateName());
            }
        }
    }

    public final void setCity(final BaseBusinessDO baseBusinessDO) {
        Integer cityId = null;
        if (baseBusinessDO instanceof RegisterBusinessDO) {
            cityId = ((RegisterBusinessDO) baseBusinessDO).getCityId();
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            cityId = ((AddressBusinessDO) baseBusinessDO).getCityId();
        }

        CityMaster cityMaster = cityMasterDAO.read(cityId);

        if (baseBusinessDO instanceof RegisterBusinessDO) {
            ((RegisterBusinessDO) baseBusinessDO).setCity(cityMaster.getCityName());
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            ((AddressBusinessDO) baseBusinessDO).setCity(cityMaster.getCityName());
        }
    }

    public final void getCountryMasterList(final BaseBusinessDO baseBusinessDO) {
        List<CountryMaster> countryMasterList = registrationDAO.getCountryMasterList();
        if (baseBusinessDO instanceof MyProfileBusinessDO) {
            ((MyProfileBusinessDO) baseBusinessDO).setCountryMaster(countryMasterList);
        } else if (baseBusinessDO instanceof RegisterBusinessDO) {
            ((RegisterBusinessDO) baseBusinessDO).setCountryMaster(countryMasterList);
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            ((AddressBusinessDO) baseBusinessDO).setCountryMaster(countryMasterList);
        }

    }

    public final void getCountry(final BaseBusinessDO baseBusinessDO) {
        Integer countryId = null;
        if (baseBusinessDO instanceof RegisterBusinessDO) {
            countryId = ((RegisterBusinessDO) baseBusinessDO).getCountryId();
        } else if (baseBusinessDO instanceof AddressBusinessDO) {
            countryId = ((AddressBusinessDO) baseBusinessDO).getCountryId();
        }
        List<CountryMaster> countryList = registrationDAO.getCountryNameByCountryId(countryId);
        if (countryList != null && countryList.size() != 0) {
            CountryMaster countryMaster = countryList.get(0);
            if (baseBusinessDO instanceof RegisterBusinessDO) {
                ((RegisterBusinessDO) baseBusinessDO).setCountry(countryMaster.getCountryName());
            } else if (baseBusinessDO instanceof AddressBusinessDO) {
                ((AddressBusinessDO) baseBusinessDO).setCountry(countryMaster.getCountryName());
            }
        }
    }

    public final void getCityMasterList(final BaseBusinessDO baseBusinessDO) {
        Integer stateId = ((CityMasterBusinessDO) baseBusinessDO).getStateId();
        List<CityMaster> cityMasterList = cityMasterDAO.findByFkStateMasterId(stateId);
        ((CityMasterBusinessDO) baseBusinessDO).setCityMasterList(cityMasterList);
    }

    public final Map<Integer, CityMaster> getAllCitiesMasterMap() {
        if (FCConstants.allCitiesMasterMap == null || FCConstants.allCitiesMasterMap.size() < 1) {
            synchronized (this) {
                if (FCConstants.allCitiesMasterMap == null || FCConstants.allCitiesMasterMap.size() < 1) {
                    FCConstants.allCitiesMasterMap = cityMasterDAO.getAllCitiesMap();
                }
            }
        }

        return FCConstants.allCitiesMasterMap;
    }
    
	public String getCityNames(String cityIdList) {
		if (cityIdList == null) {
			return null;
		}
		StringBuffer sb = new StringBuffer();
		String[] cityIdArray = cityIdList.split(",");
		for (int i = 0; i < cityIdArray.length; i++) {
			if (FCUtil.isInteger(cityIdArray[i])){
				String cityName = getCityName(Integer.parseInt(cityIdArray[i]));
				if (cityName != null){
					sb.append(", " + cityName);					
				}
			}
		} // end for
		return sb.toString();
	}
	
	public List<String> getCityNamesList(String cityIdList) {
		if (cityIdList == null) {
			return null;
		}
		List<String> cityNamesList = new ArrayList<>();
		String[] cityIdArray = cityIdList.split(",");
		for (int i = 0; i < cityIdArray.length; i++) {
			if (FCUtil.isInteger(cityIdArray[i])){
				String cityName = getCityName(Integer.parseInt(cityIdArray[i]));
				if (cityName != null){
					cityNamesList.add(cityName);					
				}
			}
		} // end for
		return cityNamesList;
	}

    public String getCityName(Integer cityId){
    	if (cityId == null){
    		return null;
    	}
    	String cityName = null;
    	Map<Integer, CityMaster> cityMap = getAllCitiesMasterMap();
    	if (cityMap != null){
    		CityMaster city = cityMap.get(cityId);
    		if (city != null){
    			cityName = city.getCityName();
    		}
    	}
    	return cityName;
    }
    
    public final Map<String, String> getCityAndStateByCityId(final Integer cityId) {
        CityMaster cityMaster = getAllCitiesMasterMap().get(cityId);
        String cityName = cityMaster.getCityName();
        Integer stateId = cityMaster.getFkStateMasterId();
        String stateName = null;
        if (!FCUtil.isEmpty(FCConstants.stateMasterMap)) {
            stateName = FCConstants.stateMasterMap.get(stateId).getStateName();
        }
        Map<String, String> cityDetails = new HashMap<String, String>();
        cityDetails.put("cityName", cityName);
        cityDetails.put("stateName", stateName);
        return cityDetails;
    }

    public final Map<Integer, OperatorMaster> getOperatorMasters() {
        if (FCConstants.operatorMasterList == null || FCConstants.operatorMasterList.size() < 1) {
            synchronized (this) {
                refreshOperatorMaster();
            }
        }
        if (FCConstants.operatorMasterList == null || FCConstants.operatorMasterList.size() < 1) {
            logger.fatal("No operator list returned from database");
            return null;
        }

        return FCConstants.operatorMasterMap;
    }

    public final synchronized void refreshOperatorMaster() {
        FCConstants.operatorMasterList = operatorCircleReadDAO.getOperators();
        FCConstants.operatorMasterMap = new HashMap<Integer, OperatorMaster>();
        for (OperatorMaster operatorMaster : FCConstants.operatorMasterList) {
            FCConstants.operatorMasterMap.put(operatorMaster.getOperatorMasterId(), operatorMaster);
        }
    }

    public final OperatorMaster getOperatorMasterByOperatorCode(final String operatorCode) {
        if (FCConstants.operatorMasterList == null) {
            return null;
        }
        for (OperatorMaster operator : FCConstants.operatorMasterList) {
            if (operator != null && operator.getOperatorCode().equals(operatorCode)) {
                return operator;
            }
        }
        return null;
    }
    
    public BillerDetails getBillPayOperatorMasterByOperatorId(final Integer operatorId) {
        List<BillerDetails> operatorList = billPayServiceRemote.getBillPayOperatorMaster(null);
        if (operatorList != null && operatorId != null) {
            for (BillerDetails operatorMaster : operatorList) {
                if (operatorMaster.getOperatorMasterId().equals(operatorId)) {
                    return operatorMaster;
                }
            }
        }
        return null;
    }

    public final OperatorMaster getDthOperator(final String operatorMasterId) {
        List<OperatorMaster> dthOperatorMsterList = getDTHOperatorMasterList();
        if (dthOperatorMsterList == null) {
            return null;
        }
        for (OperatorMaster operator : dthOperatorMsterList) {
            if (operator != null && String.valueOf(operator.getOperatorMasterId()).equals(operatorMasterId)) {
                return operator;
            }
        }
        return null;
    }

    // getting specific operatotor lists:mobile-DTH-Datacard
    public final List<OperatorMaster> getMobileOperatorMasterList() {
        if (FCConstants.mobileOperatorMasterList == null || FCConstants.mobileOperatorMasterList.size() < 1) {
            synchronized (this) {
                List<OperatorMaster> mobileOperatorList = new ArrayList<OperatorMaster>();
                Map<Integer, OperatorMaster> operatorMasters = getOperatorMasters();
                Map<Integer, OperatorMaster> mobileOperators = new TreeMap<Integer, OperatorMaster>();
                Set<Integer> operatorIds = operatorMasters.keySet();
                for (Integer operatorId : operatorIds) {
                    OperatorMaster operator = operatorMasters.get(operatorId);
                    if (operator != null && operator.getFkProductMasterId() == FCConstants.MOBILE_PRODUCT_MASTER_ID) {
                        mobileOperators.put(operatorId, operator);
                    }
                }
                Set<Integer> mobileOperatorsKeySet = mobileOperators.keySet();
                for (Integer operatorId : mobileOperatorsKeySet) {
                    mobileOperatorList.add(mobileOperators.get(operatorId));
                }
                FCConstants.mobileOperatorMasterList = mobileOperatorList;
            }
        }
        return FCConstants.mobileOperatorMasterList;
    }

    public final List<OperatorMaster> getDTHOperatorMasterList() {
        if (FCConstants.dthOperatorMasterList == null || FCConstants.dthOperatorMasterList.size() < 1) {
            synchronized (this) {
                List<OperatorMaster> dthOperatorList = new ArrayList<OperatorMaster>();
                Map<Integer, OperatorMaster> operatorMasters = getOperatorMasters();
                Map<Integer, OperatorMaster> dthOperators = new TreeMap<Integer, OperatorMaster>();
                Set<Integer> operatorIds = operatorMasters.keySet();
                for (Integer operatorId : operatorIds) {
                    OperatorMaster operator = operatorMasters.get(operatorId);
                    if (operator != null && operator.getFkProductMasterId() == FCConstants.DTH_PRODUCT_MASTER_ID) {
                        dthOperators.put(operatorId, operator);
                    }
                }

                Set<Integer> dthOperatorsKeySet = dthOperators.keySet();
                for (Integer operatorId : dthOperatorsKeySet) {
                    dthOperatorList.add(dthOperators.get(operatorId));
                }
                FCConstants.dthOperatorMasterList = dthOperatorList;
            }
        }
        return FCConstants.dthOperatorMasterList;
    }

    public final List<OperatorMaster> getDatacardOperatorMasterList() {
        if (FCConstants.datacardOperatorMasterList == null || FCConstants.datacardOperatorMasterList.size() < 1) {
            synchronized (this) {
                List<OperatorMaster> datacardOperatorList = new ArrayList<OperatorMaster>();
                Map<Integer, OperatorMaster> operatorMasters = getOperatorMasters();
                Map<Integer, OperatorMaster> datacardOperators = new TreeMap<Integer, OperatorMaster>();
                Set<Integer> operatorIds = operatorMasters.keySet();
                for (Integer operatorId : operatorIds) {
                    OperatorMaster operator = operatorMasters.get(operatorId);
                    if (operator != null && operator.getFkProductMasterId() == FCConstants.DATACARD_PRODUCT_MASTER_ID) {
                        datacardOperators.put(operatorId, operator);
                    }
                }
                Set<Integer> datacardOperatorsKeySet = datacardOperators.keySet();
                for (Integer operatorId : datacardOperatorsKeySet) {
                    datacardOperatorList.add(datacardOperators.get(operatorId));
                }
                FCConstants.datacardOperatorMasterList = datacardOperatorList;
            }
        }
        return FCConstants.datacardOperatorMasterList;
    }
    
    public final List<OperatorMaster> getPostPaidDatacardOperatorMasterList() {
        if (FCConstants.postPaidDatacardOperatorMasterList == null || FCConstants.postPaidDatacardOperatorMasterList.size() < 1) {
            synchronized (this) {
                List<OperatorMaster> datacardOperatorList = new ArrayList<OperatorMaster>();
                Map<Integer, OperatorMaster> operatorMasters = getOperatorMasters();
                Map<Integer, OperatorMaster> datacardOperators = new TreeMap<Integer, OperatorMaster>();
                Set<Integer> operatorIds = operatorMasters.keySet();
                for (Integer operatorId : operatorIds) {
                    OperatorMaster operator = operatorMasters.get(operatorId);
                    if (operator != null && operator.getFkProductMasterId() == FCConstants.PRODUCT_ID_POSTPAID_DATACARD) {
                        datacardOperators.put(operatorId, operator);
                    }
                }
                Set<Integer> datacardOperatorsKeySet = datacardOperators.keySet();
                for (Integer operatorId : datacardOperatorsKeySet) {
                    datacardOperatorList.add(datacardOperators.get(operatorId));
                }
                FCConstants.postPaidDatacardOperatorMasterList = datacardOperatorList;
            }
        }
        return FCConstants.postPaidDatacardOperatorMasterList;
    }

    // ---- bill payment ----

    final  List<OperatorMaster> getOperatorsFor(final int fkProductMasterID) {
        List<OperatorMaster> operatorList = new ArrayList<OperatorMaster>();
        Map<Integer, OperatorMaster> operatorMasters = getOperatorMasters();
        Map<Integer, OperatorMaster> operators = new TreeMap<Integer, OperatorMaster>();
        Set<Integer> operatorIds = operatorMasters.keySet();
        for (Integer operatorId : operatorIds) {
            OperatorMaster operator = operatorMasters.get(operatorId);
            if (operator != null && operator.getFkProductMasterId() == fkProductMasterID) {
                operators.put(operatorId, operator);
            }
        }
        Set<Integer> datacardOperatorsKeySet = operators.keySet();
        for (Integer operatorId : datacardOperatorsKeySet) {
            operatorList.add(operators.get(operatorId));
        }
        return operatorList;
    }

    public final List<OperatorMaster> getPostpaidMobileOperatorMasterList() {
        if (FCConstants.postpaidMobileOperatorMasterList == null
                || FCConstants.postpaidMobileOperatorMasterList.size() < 1) {
            synchronized (this) {
                FCConstants.postpaidMobileOperatorMasterList = getOperatorsFor(FCConstants.PRODUCT_ID_BILL_MOBILE);
            }
        }
        return FCConstants.postpaidMobileOperatorMasterList;
    }
    
    public final List<BillerDetails> getBillPayOperatorMasterList(String locationId) {
        return billPayServiceRemote.getBillPayOperatorMaster(locationId);
    }
    
    public final List<BillerDetails> getBillPayOperatorMasterListWithChannelBlocking(String locationId, Integer channelId, Integer versionId) {
        return billPayServiceRemote.getBillPayOperatorMasterWithChannelBlocking(locationId, channelId, versionId);
    }
    
    public final List<BillPayCategoryMaster> getBillPayCategoryMasterList() {
         return billPayServiceRemote.getBillPayCategoryMaster();
    }
    
    public final Map<String, BillPayValidator> getBillPayValidatorList() {
        return billPayServiceRemote.getBillPayValidators();
    }
    
    public final BillPayValidator getBillPayValidatorForOperator(Integer operatorId, Integer circleId) {
        return billPayServiceRemote.getBillPayValidatorForOperator(operatorId, circleId);
   }

    // close : created operator master lists for: mobile-DTH-Datacard

    public final void getCity(final BaseBusinessDO baseBusinessDO) {
        AddressBusinessDO addressBusinessDO = (AddressBusinessDO) baseBusinessDO;
        Integer cityId = addressBusinessDO.getCityId();
        CityMaster cityMaster = cityMasterDAO.read(cityId);
        if (cityMaster != null) {
            addressBusinessDO.setCity(cityMaster.getCityName());
        }

    }

    public final List<CityMaster> getCitieNamesByCityIds(
            final String commaSeparatedCityIds) {
        List<CityMaster> cities = locationReadDao
                .getCitiesByCityIds(commaSeparatedCityIds);
        return cities;
    }

    public final List<CityMaster> getCities() {
        List<CityMaster> cities = locationReadDao.getCities();
        return cities;
    }

    public final Map<String, CityMaster> getCitiesAsMap() {
        List<CityMaster> cities = locationReadDao.getCities();
        Map<String, CityMaster> cityMap = new HashMap<String,
                CityMaster>();
        for (CityMaster cityMaster : cities) {
            cityMap.put(cityMaster.getCityMasterId() + "", cityMaster);
        }
        return cityMap;
    }

    public final String getCityNameList(final String cityIdList) {
        List<CityMaster> cityList = getCitieNamesByCityIds(cityIdList);
        StringBuilder cityNames = new StringBuilder();
        if (cityIdList != null) {
            for (int i = 0; i < cityList.size(); ++i) {
                if (cityList.get(i) != null) {
                    if (i != 0) {
                        cityNames.append(",");
                    }
                    cityNames.append(cityList.get(i).getCityName());
                } else {
                    logger.error("Null city name for city id: " + cityList.get(i).getCityMasterId());
                }
            }
        }
        return cityNames.toString();
    }

    public List<BillPayCircleMaster> getBillPayCircleMasterList() {
        return billPayServiceRemote.getBillPayCircleMasterList();
    }

    public List<BillPayOperatorMaster> getOperatorsForProductId(int productId) {
        return billPayServiceRemote.getOperatorMastersFromProductId(productId);
    }

}
