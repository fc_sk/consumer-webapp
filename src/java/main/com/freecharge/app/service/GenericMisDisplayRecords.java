package com.freecharge.app.service;

import java.io.Serializable;
import java.util.Date;

public class GenericMisDisplayRecords implements Serializable,Comparable<GenericMisDisplayRecords> {

	private static final long serialVersionUID = 1L;
	private Long inRequestID;
	private Integer pgRequestID;
	private String mobileNumber;
	private String operator;
	private String circle;
	private String amount;
	private String orderID;
	private String inResponseCodeStatus;
	private String inAggregator;
	private String paymentGateway;
	private String paymentOption;
	private String paymentType;
	private Boolean pgStatusCode;
	private String userEmailD;
	private String inResponseMessage;
    private Boolean userTxnStatus;
    private Date requestDate;
    private String pgMessage;

	public String getPgMessage() {
		return pgMessage;
	}


	public void setPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
	}


	public GenericMisDisplayRecords() {
		// TODO Auto-generated constructor stub
	}
		
	
	public Long getInRequestID() {
		return inRequestID;
	}

	public String getOperator() {
		return operator;
	}
	public String getInResponseMessage() {
		return inResponseMessage;
	}


	public void setInResponseMessage(String inResponseMessage) {
		this.inResponseMessage = inResponseMessage;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	public void setInRequestID(Long inRequestID) {
		this.inRequestID = inRequestID;
	}


	public Integer getPgRequestID() {
		return pgRequestID;
	}


	public void setPgRequestID(Integer pgRequestID) {
		this.pgRequestID = pgRequestID;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getAmount() {
		return amount;
	}

	public String getCircle() {
		return circle;
	}


	public void setCircle(String circle) {
		this.circle = circle;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}


	public String getOrderID() {
		return orderID;
	}


	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}


	public String getInResponseCodeStatus() {
		return inResponseCodeStatus;
	}


	public void setInResponseCodeStatus(String inResponseCodeStatus) {
		this.inResponseCodeStatus = inResponseCodeStatus;
	}


	public String getInAggregator() {
		return inAggregator;
	}


	public void setInAggregator(String inAggregator) {
		this.inAggregator = inAggregator;
	}


	public String getPaymentGateway() {
		return paymentGateway;
	}


	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getPaymentOption() {
		return paymentOption;
	}


	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}


	public String getPaymentType() {
		return paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


	public Boolean getPgStatusCode() {
		return pgStatusCode;
	}


	public void setPgStatusCode(Boolean pgStatusCode) {
		this.pgStatusCode = pgStatusCode;
	}


	public String getUserEmailD() {
		return userEmailD;
	}


	public void setUserEmailD(String userEmailD) {
		this.userEmailD = userEmailD;
	}

	public Boolean getUserTxnStatus() {
		return userTxnStatus;
	}


	public void setUserTxnStatus(Boolean userTxnStatus) {
		this.userTxnStatus = userTxnStatus;
	}


	public Date getRequestDate() {
		return requestDate;
	}


	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public int compareTo(GenericMisDisplayRecords o) {
		if(this.getRequestDate().getTime() > o.getRequestDate().getTime())
			return -1;
		else if(this.getRequestDate().getTime() < o.getRequestDate().getTime())
			return 1;
		else 
			return 0;
	}
}
