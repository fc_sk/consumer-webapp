package com.freecharge.app.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.crosssell.CrossSellInterface;
import com.freecharge.app.crosssell.LeadGenerationInterface;
import com.freecharge.app.crosssell.factory.CrosssellFactory;
import com.freecharge.app.domain.dao.ICartItemsDAO;
import com.freecharge.app.domain.dao.ITxnCrossSellDAO;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.common.businessdo.CartCrosssellBusinessDO;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 9, 2012
 * Time: 3:43:24 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("cartCrosssellService")
public class CartCrosssellService {
	
	@Autowired
    private ICartItemsDAO cartItemsDAO;
	
    @Autowired
    private ITxnCrossSellDAO txnCrossSellDAO;
	
    @Autowired
    private CrosssellService crosssellService;
    
    @Autowired
    private CrosssellFactory crosssellFactory;
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    public boolean getCrossSellAvailability(BaseBusinessDO baseBusinessDO) {
    	CartCrosssellBusinessDO cartCrosssellBusinessDO = (CartCrosssellBusinessDO)baseBusinessDO; 
    	Integer crossSellId = cartCrosssellBusinessDO.getCrosssellId();
    	FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    	if (fs != null) {
            Integer userid = (Integer) fs.getSessionData().get(cartCrosssellBusinessDO.getLookupID()+"_"+ FCConstants.SESSION_DATA_USERCONTACTDETAIL + "_userId");
            if (userid == null) {
                UserContactDetail userContactDetail = (UserContactDetail)fs.getSessionData().get(cartCrosssellBusinessDO.getLookupID()+"_"+ FCConstants.SESSION_DATA_USERCONTACTDETAIL);
                if (userContactDetail != null) {
                    userid = userContactDetail.getUserId();
                }
            }
            CrosssellBusinessDo crosssellBusinessDo = crosssellService.getCrosssell(crossSellId);
            logger.debug("Crosssell Category " + crosssellBusinessDo.getCategory());
            if(crosssellBusinessDo.getCategory().equals(FCConstants.CROSS_SELL_TYPE_LEAD_GEN)) {
            	LeadGenerationInterface leadGenerationInterface = (LeadGenerationInterface)crosssellFactory.getCrossSellBean(crosssellBusinessDo.getClassName()); 
            	return leadGenerationInterface.isValidCrossSellForUser(userid, crossSellId);
            }else if(crosssellBusinessDo.getCategory().equals(FCConstants.CROSS_SELL_TYPE_CROSS_SELL)){
            	CrossSellInterface crossSellInterface = (CrossSellInterface)crosssellFactory.getCrossSellBean(crosssellBusinessDo.getClassName()); 
            	return crossSellInterface.isValidCrossSellForUser(userid, crossSellId);
            }else return false;
    	}
    	return false;
    }
}
