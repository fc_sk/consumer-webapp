package com.freecharge.app.service.model;

import java.util.HashMap;
import java.util.Map;

public enum CouponOrderType {
	RANKS("rank");
	
	private String type;
	static final Map<String, CouponOrderType> couponOrderTypeMap;
	
	static {
		couponOrderTypeMap = new HashMap<String, CouponOrderType>();
        for (CouponOrderType couponOrderingType : CouponOrderType.values()) {
        	couponOrderTypeMap.put(couponOrderingType.getType(), couponOrderingType);
        }
    }
	
	private CouponOrderType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}	
	
	public static CouponOrderType fromString(String type) {
		return couponOrderTypeMap.get(type);
    }
}
