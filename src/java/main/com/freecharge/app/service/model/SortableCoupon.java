package com.freecharge.app.service.model;

public class SortableCoupon implements Comparable<SortableCoupon>{
	private Integer couponStoreId;
	private Integer couponPosition;
	private Integer couponInferredRank;
	
	public Integer getCouponStoreId() {
		return couponStoreId;
	}
	public void setCouponStoreId(Integer couponStoreId) {
		this.couponStoreId = couponStoreId;
	}
	public Integer getCouponPosition() {
		return couponPosition;
	}
	public void setCouponPosition(Integer couponPosition) {
		this.couponPosition = couponPosition;
	}
	public Integer getCouponInferredRank() {
		return couponInferredRank;
	}
	public void setCouponInferredRank(Integer couponInferredRank) {
		this.couponInferredRank = couponInferredRank;
	}
	@Override
	public int compareTo(SortableCoupon other) {
		if (couponInferredRank == other.couponInferredRank){
			return couponPosition.compareTo(other.couponPosition);
		} 
		return couponInferredRank.compareTo(other.couponInferredRank);
	}
	
}
