package com.freecharge.app.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.api.coupon.service.model.CouponImage;
import com.freecharge.api.coupon.service.model.CouponInventory;
import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnCrossSell;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.model.SortableCoupon;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.CouponInventoryBusinessDO;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.businessdo.ReissueCouponInventoryBusinessDO;
import com.freecharge.common.businessdo.VouchersBusinessDO;
import com.freecharge.common.businessdo.VouchersQuantityBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.properties.InMemoryAppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.CouponInventoryUtil;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.coupon.service.exclusive.ExclusiveCouponService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.platform.metrics.MetricsClient;

@Service
public class VoucherService {

        private static Logger logger = LoggingFactory.getLogger(VoucherService.class);
        
        @Autowired
        private ExclusiveCouponService exclusiveCouponService;
        
        
        @Autowired
        private CrosssellService crosssellService;

        @Autowired
        private InMemoryAppConfigService inMemoryAppConfigService;
        
      
        @Autowired
        private UserServiceProxy userServiceProxy;
        @Autowired
        private EmailService emailService;

        @Autowired
        private FCProperties fcProperties;
        
        @Autowired
        private CartService cartService;

        @Autowired
        private TxnHomePageDAO txnHomePageDAO;
        
        @Autowired
        private TxnCrossSellService txnCrossSellService;
        
        @Autowired
        private OrderIdWriteDAO orderIdDao;
        
        @Autowired
        private OrderIdSlaveDAO orderIdSlaveDAO;
        
        @Autowired
        private PaymentTransactionService paymentTransactionService;
        
        
        @Autowired
        private AppConfigService appConfigService;
        
    	@Autowired
        private MetricsClient metricsClient;
    	
    	@Autowired
    	@Qualifier("couponServiceProxy")
    	private ICouponService couponServiceProxy;
    	
    	@Autowired
    	private CouponService couponService;

    	
		public Map<String, List<CouponVO>> getAllActiveCouponsWithOrdering(int channelId, int appVersion) {
            long startTime =  System.currentTimeMillis();
			if (!couponServiceProxy.isValidChannel(channelId)){
				return null;
			}
			boolean cacheMiss = false;
            Map<String, List<CouponVO>> vouchersMap = null;
            try {
          		vouchersMap = couponServiceProxy.getAllActiveCouponsOrdered(channelId);
            }catch (Exception e) {
                logger.error("unable to get coupons from cache", e);
            }
            if (vouchersMap == null || vouchersMap.size() < 1) {
            	cacheMiss = true;
                vouchersMap = getVouchersMap(false, channelId); //fetch if not in cache
                if (vouchersMap != null && vouchersMap.size() > 0) {
                    try {
                    	couponServiceProxy.setAllActiveCouponsOrdered(vouchersMap, channelId);
                    }catch (Exception e){
                        logger.error("error setting voucherMap to cache:", e);
                    }
                }
            }
            if (appConfigService.isCouponFraudCheckEnabled()){
            	vouchersMap = filterOutFraudCoupons(vouchersMap,channelId,appVersion);            	
            }
            long endTime = System.currentTimeMillis();
    		long duration = endTime - startTime;
    		boolean isCouponServiceProfilingEnabled = fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED);
    		if(isCouponServiceProfilingEnabled){
    			metricsClient.recordLatency("CouponService", "getAllActiveCouponsWithOrdering", duration);
    			metricsClient.recordEvent("CouponService", "cacheMiss-getAllActiveCouponsWithOrdering", String.valueOf(cacheMiss));
    		}
            return vouchersMap;
		}

		private Map<String, List<CouponVO>> filterOutFraudCoupons(
				Map<String, List<CouponVO>> vouchersMap, int channelId, int appVersion) {
        	Map<String, List<CouponVO>> filteredOutVouchersMap = new HashMap<>();
        	if (vouchersMap == null || vouchersMap.isEmpty()){
        		return filteredOutVouchersMap;
        	}
        	
        	String cookie = FCSessionUtil.getPermanentCookie();
        	String ipAddress = FCSessionUtil.getIpAddress();
        	String email = FCSessionUtil.getLoggedInEmailId();
        	String profileNo = null;
        	String userId = null;
        	if (email != null){
        		Users user = userServiceProxy.getUserByEmailId(email);
        		if(user!=null) {
        			profileNo = user.getMobileNo();
        			userId = String.valueOf(user.getUserId());
        		}
        	}
        	
        	// We don't have service no here.. TODO: Think how service nos can be got from user id.
        	Set<Integer> invalidCoupons = couponServiceProxy.getInvalidCouponIds(cookie, ipAddress, email, profileNo, null,channelId, userId);
			Set<Integer> invalidCouponsForChannelVersion = couponServiceProxy.getInvalidCouponIds(channelId,appVersion);
        	invalidCoupons.addAll(invalidCouponsForChannelVersion);
        	Iterator<String> categoriesIterator  = vouchersMap.keySet().iterator(); 
        	while(categoriesIterator.hasNext()){
        		String categoryKey = categoriesIterator.next();
        		List<CouponVO> allCategoryCouponList = vouchersMap.get(categoryKey);
        		if (allCategoryCouponList == null || allCategoryCouponList.isEmpty()){
        			continue;
        		}
        		List<CouponVO> newCategoryCouponList = filteredOutVouchersMap.get(categoryKey);
        		if (newCategoryCouponList == null){
        			newCategoryCouponList = new ArrayList<>();
        		}
        		for (CouponVO coupon : allCategoryCouponList){
        			if (coupon != null && !invalidCoupons.contains(coupon.getVoucherId())){
        				newCategoryCouponList.add(coupon);
        			}
        		}
        		filteredOutVouchersMap.put(categoryKey, newCategoryCouponList);
        	}
        	return filteredOutVouchersMap;
		}

		/**
         * 
         * @param isRefresh  pass isRefresh as true if the data needs to read from database and updated in cache.
         * 					 pass isRefresh as false if the data needs to be read from cache.
		 * @param channelId 
         * @return			 The coupon list mapped to the coupon category.
         */
        @Transactional
        private Map<String, List<CouponVO>> getVouchersMap(boolean isRefresh, int channelId) {
                List<CouponStore> couponStoreList = null;
                List<VoucherCat> categories = null;
                Map<Integer, List<CouponImage>> couponImagesMap = null;

                try {
                		if (!isRefresh){
                			categories = couponServiceProxy.getVoucherCategories();	
               				couponStoreList = couponServiceProxy.getAvailableCouponsOrdered();
               				couponImagesMap = couponServiceProxy.getCouponImagesInMemAppConfigService();
                		}

                        if (categories == null || categories.size() == 0) {
                                categories = couponServiceProxy.getAllVoucherCategoriesOrdered();
                                couponServiceProxy.setVoucherCategories(categories); 
                        }
                        
                        if (couponStoreList == null || couponStoreList.size() == 0) {
                            couponStoreList = couponServiceProxy.getAvailableCouponsForChannel(channelId, " 'E' ");
                        	couponStoreList = reorderCouponsByRank(couponStoreList);
                        	couponServiceProxy.setAvailableCouponsOrdered(couponStoreList); 
                        }
                        
                        if(couponImagesMap == null || couponImagesMap.size() > 0){
            				couponImagesMap =  couponServiceProxy.getCouponImagesInMemAppConfigService();
                        }
                        
                } catch (Exception ex) {
                        logger.error("error occur while calling  the getVoucherCategories() method :" + ex, ex);
                        categories = couponServiceProxy.getAllVoucherCategoriesOrdered();
                        couponStoreList = couponServiceProxy.getAvailableCoupons();
                        couponImagesMap =  couponServiceProxy.getCouponImagesInMemAppConfigService();
                }

                List<CouponMerchant>  merchants = new ArrayList<CouponMerchant>();
                try{
                	merchants = inMemoryAppConfigService.getCouponMerchants();	
                }catch(Exception e){
                    logger.error("error occur while calling merchantsDAO.getAllMerchants() in the getVouchersMap() method :" + e, e);
                }
                
                Map<String, List<CouponVO>> vouchersMap = new HashMap<String, List<CouponVO>>();

                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
                if(couponStoreList==null){
                	return vouchersMap;
                }
                for (CouponStore couponStore : couponStoreList) {
                        CouponVO voucherVO = new CouponVO();

                        voucherVO.setVoucherId(couponStore.getCouponStoreId());
                        voucherVO.setVoucherName(couponStore.getCampaignName());
                        voucherVO.setVoucherAmount(couponStore.getCouponValue());
                        voucherVO.setVoucherType(couponStore.getCouponType());
                        //voucherVO.setVoucherImgUrl(fcProperties.getAbsoluteImgUrl(couponStore.getCouponImagePath()));
                        //voucherVO.setVoucherImgUrlSW(voucherVO.getVoucherImgUrl().replaceFirst("images/", "images/sw/"));
                        voucherVO.setTermsConditions(couponStore.getTermsConditions() != null ? (couponStore.getTermsConditions().replaceAll("(\\r)", "<br />")) : "");
                        voucherVO.setVoucherUrl(couponStore.getCampaignName());
                        voucherVO.setCouponRemainingCount(0);
                        voucherVO.setFaceValue(couponStore.getFaceValue());
                        voucherVO.setMinRedemptionAmount(couponStore.getMinRedemAmt());
                        voucherVO.setVoucherCat(categories.get(categories.indexOf(new VoucherCat(couponStore.getFkVoucherCatId()))).getCatName());
                        voucherVO.setCouponShortDesc(couponStore.getCouponShortDesc());
                        voucherVO.setPromotionLevel(couponStore.getPromotionLevel());
                        voucherVO.setPrice(couponStore.getPrice());
                        if (couponStore.getPrice() == 0){
                        	voucherVO.setPriceLabel("free");
                        } else if (couponStore.getPrice() < 0){
                        	voucherVO.setIsNegative(true);
                        	voucherVO.setPriceLabel("free");
                        } else{
                        	voucherVO.setPriceLabel("paid");
                        }
                        
						voucherVO.setValidityDate(couponStore.getValidityDate());
						// As a rule, the readability modifications etc should be done in the browser.
						// But in this case, we can do it on the server (here) because this function won't be called
						// frequently, given that we will be storing the CouponsData in cloudfront.	
						voucherVO.setReadableValidityDate(dateFormatter.format(couponStore.getValidityDate()));
			
                        voucherVO.setCityNames(couponServiceProxy.getCommaSeparatedCityNamesWithCouponStore(couponStore));
                        
                        if (vouchersMap.containsKey("ALL")) 
                                vouchersMap.get("ALL").add(voucherVO);
                        else {
                                List<CouponVO> voucherBusinessDoList = new ArrayList<CouponVO>();
                                voucherBusinessDoList.add(voucherVO);
                                vouchersMap.put("ALL", voucherBusinessDoList);
                        }
                        if (vouchersMap.containsKey(voucherVO.getVoucherCat())) {
                                vouchersMap.get(voucherVO.getVoucherCat()).add(voucherVO);
                        } else {
                                List<CouponVO> voucherBusinessDoList = new ArrayList<CouponVO>();
                                voucherBusinessDoList.add(voucherVO);
                                vouchersMap.put(voucherVO.getVoucherCat(), voucherBusinessDoList);
                        }
                        
                        voucherVO.setMerchantTitle(getMerchantTitle(merchants, couponStore.getMerchantId()));
                        
                        List<CouponImage> images = couponImagesMap.get(voucherVO.getVoucherId());
            			if(images != null){
            				for(CouponImage image : images){
            					if(image.getResolution().equals(fcProperties.getDesktopImageResolution())){
             						String couponImagePath = fcProperties.getAbsoluteImgUrl("content/images/coupon/web/" + image.getImageName());
             						voucherVO.setVoucherImgUrlSW(couponImagePath);
            					}else if(image.getResolution().equals(fcProperties.getMobileimageResolution()) && image.getChannel().equals("mobile")){
             						String couponImagePath = fcProperties.getAbsoluteImgUrl("content/images/coupon/mobile/" + image.getImageName());
             						voucherVO.setVoucherImgUrl(couponImagePath);
            					}
            				}
            			}
                }
                return vouchersMap;
        }
        
        private void updateCouponFlags(Map<String, List<CouponVO>> vouchersMap){ 
        	try{
//        		updateNewCoupons(vouchersMap);
//        		updatePopularCoupons(vouchersMap);
        	}catch(Exception e){
        		logger.error("error occured in  updateCouponFlags  :" + e, e);
        	}
        }
        
        /**
         * This method takes a coupon store list as input and returns a coupon store list sorted by below logic
         * 1. Reorder coupons with higher inferred ranks first.
         * 2. If there are two coupons have same inferred rank reorder based on coupon position of coupon store object.
         * 3. Reorder with manual rank configuration. If a manual rank is set for a coupon the coupon occupy that 
         * 	position irrespective of inferred rank. If coupon store list size is less than manual rank. The coupon will occupy last position.
         * 
         * 
         * @param couponStoreList	the coupon store list to be ordered.
         */
        private List<CouponStore> reorderCouponsByRank(List<CouponStore> couponStoreList) {
        	Map<Integer, CouponStore> couponStoreIdMap = new HashMap<Integer, CouponStore>(); 
        	Map<Integer, Integer> couponStoreIdInferredRankMap = couponServiceProxy.getCouponStoreInferredRankMap(); 
        	List<SortableCoupon> couponListForSort = new ArrayList<SortableCoupon>();
        	for (CouponStore couponStore : couponStoreList){
        		Integer couponStoreId = couponStore.getCouponStoreId();
        		couponStoreIdMap.put(couponStore.getCouponStoreId(), couponStore);
        		SortableCoupon scoupon = new SortableCoupon();
        		Integer inferredRank = couponStoreIdInferredRankMap.get(couponStoreId);
        		Integer couponPosition = couponStore.getCouponPosition();
        		if (inferredRank == null){
        			inferredRank = Integer.MAX_VALUE;
        		}
        		scoupon.setCouponInferredRank(inferredRank);
        		scoupon.setCouponPosition(couponPosition);
        		scoupon.setCouponStoreId(couponStoreId);
        		couponListForSort.add(scoupon);
        	}
        	
        	// Sort by inferred rank. Check SortableCoupon.compareTo() method.
        	Collections.sort(couponListForSort);
        	
        	List<CouponStore> sortedList = getInferredRankSortedList(
					couponStoreIdMap, couponListForSort);
        	
        	reorderByManualRanks(couponStoreIdMap, sortedList);
        	return sortedList;
    	}

        /**
         * Converts SortableCoupon list to CouponStore list retaining the order of SortableCoupon list.
         * @param couponStoreIdMap 
         * 								a map of coupon store id to coupon store object.
         * @param couponListForSort
         * 								SortableCoupon list.
         * @return
         */
		private List<CouponStore> getInferredRankSortedList(
				Map<Integer, CouponStore> couponStoreIdMap,
				List<SortableCoupon> couponListForSort) {
			List<CouponStore> sortedList = new ArrayList<CouponStore>();
        	for(SortableCoupon sortableCoupon : couponListForSort){
        		sortedList.add(couponStoreIdMap.get(sortableCoupon.getCouponStoreId()));
        	}
			return sortedList;
		}

		/**
		 * This method takes the coupon store list and reorders it by manual ranks.
		 * @param couponStoreIdMap 
		 * 								a map of coupon store id to coupon store object.
		 * @param couponStoreList  
		 * 								the list which is to be reordered.
		 */
		private void reorderByManualRanks(
				Map<Integer, CouponStore> couponStoreIdMap,
				List<CouponStore> couponStoreList) {
			Map<Integer, Integer> couponStoreIdManualRankMap = couponServiceProxy.getCouponStoreManualRankMap();
			if(couponStoreIdManualRankMap!=null) {
	        	Iterator<Integer> manualRankKeysIterator = couponStoreIdManualRankMap.keySet().iterator();
	        	while (manualRankKeysIterator.hasNext()){
	        		Integer couponStoreId = manualRankKeysIterator.next();
	        		Integer manualRank = couponStoreIdManualRankMap.get(couponStoreId);
	        		CouponStore couponStore = couponStoreIdMap.get(couponStoreId);
	        		if (couponStore != null){
	        			couponStoreList.remove(couponStore);
	        			if (manualRank != null){
	        				Integer newPosition = manualRank - 1;
	                		if (couponStoreList.size() >= newPosition){
	                			couponStoreList.add(newPosition, couponStore);
	                		} else{
	                			couponStoreList.add(couponStore);
	                		} // end else
	        			}
	        		} // end if
	        	} // end while
			}
		}

		private String getMerchantTitle(List<CouponMerchant> merchants,
				Integer merchantId) {
			for(CouponMerchant merchant : merchants){
				if(merchant.getId() == merchantId)
					return merchant.getMerchantTitle();
			}
			return null;
		}

		@Transactional(propagation = Propagation.REQUIRES_NEW)
        public void refreshAllActiveCoupons() {
			int oldMobileSiteChannel = FCConstants.CHANNEL_ID_ANDROID_APP;
			int mobileSiteChannel = FCConstants.CHANNEL_ID_MOBILE;
    		Map<String, List<CouponVO>> orderedVouchersMap = getVouchersMap(true, oldMobileSiteChannel);
    		Map<String, List<CouponVO>> orderedVoucherMapForMobileWeb = getVouchersMap(true, mobileSiteChannel);
    		if (orderedVouchersMap != null && orderedVouchersMap.size() > 0){
    			updateCouponFlags(orderedVouchersMap);
        		couponServiceProxy.setAllActiveCouponsOrdered(orderedVouchersMap, oldMobileSiteChannel);
        		couponServiceProxy.setAllActiveCouponsOrdered(orderedVoucherMapForMobileWeb, mobileSiteChannel);
    		}
    		
        	couponServiceProxy.updateCache();
        	exclusiveCouponService.initCoupon();
        }

        @Transactional
        public List<Integer> getAllActiveCouponsIds(BaseBusinessDO baseBusinessDO) {
                List<Integer> activeCouponsList = null;
                VouchersBusinessDO voucherBusinessDO = (VouchersBusinessDO) baseBusinessDO;

                try {
                        activeCouponsList = couponServiceProxy.getAvailableCouponIds();

                        if (activeCouponsList == null || activeCouponsList.size() == 0) {
                                activeCouponsList = couponServiceProxy.getAvailableCouponIds();
                                couponServiceProxy.setAvailableCouponIds((activeCouponsList)); 
                        }
                } catch (Exception ex) {
                        logger.error("error occur while calling  the getAllActiveCouponsIds(BaseBusinessDO baseBusinessDO) method :" + ex, ex);
                        ex.printStackTrace();
                        activeCouponsList = couponServiceProxy.getAvailableCouponIds();
                }

                voucherBusinessDO.setVoucherIdList(activeCouponsList);

                return activeCouponsList;
        }
        
        /**
         * Given an order ID this method blocks the required number of
         * coupons (physical/unique/non-unique). This should be called
         * *only* after payment has succeeded otherwise {@link IllegalStateException} 
         * will be thrown. This method is idempotent on orderId. 
         * 
         * {@link CartItems} is the source of truth of coupon information.
         * 
         * @param orderId
         * @throws IllegalStateException If payment has not succeeded.
         */
        public void blockCoupons(String orderId) throws IllegalStateException {
            logger.info("Entering into blockCoupons  for orderId " + orderId);
            if (!paymentTransactionService.hasPaymentSucceeded(orderId)) {
                throw new IllegalArgumentException("Trying to block coupons when payment has failed for orderID: [" + orderId + "]");
            }


            CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId);

            List<VouchersQuantityBusinessDO> voucherQuantityList = new ArrayList<VouchersQuantityBusinessDO>();

            final int couponScalingFactor = cartService.getCouponScaleUpFactor(cartToInspect);
            
            for (CartItemsBusinessDO cartItems : cartToInspect.getCartItems()) {
            if (!cartItems.getDeleted()) {
                if (cartService.getProductName(cartItems) == ProductName.Coupons || cartService.getProductName(cartItems) == ProductName.PriceCoupons) {
                    Integer couponId = cartItems.getItemId();
                    Integer couponsQuantity = cartItems.getQuantity();

                    if (couponsAlreadyBlocked(orderId, cartToInspect, couponId)) {
                        continue;
                    }
                    
                    CouponStore couponStore = couponServiceProxy.getCouponStore(couponId);
                    if(couponStore!=null) {
	                    VouchersQuantityBusinessDO voucherQuantityBizDo = new VouchersQuantityBusinessDO();
	                    voucherQuantityBizDo.setComplimentary(couponStore.getIsComplimentary());
	                    voucherQuantityBizDo.setCouponStoreId(couponStore.getCouponStoreId());
	                    voucherQuantityBizDo.setLookupID(cartToInspect.getLookupID());
	                    voucherQuantityBizDo.setQuantity(couponsQuantity * couponScalingFactor);
	                    voucherQuantityBizDo.setUnique(couponStore.getIsUnique());
	                    voucherQuantityBizDo.setIsMCoupon(couponStore.getIsMCoupon());
	                    voucherQuantityBizDo.setMerchantId(couponStore.getMerchantId());
	                    
	                    voucherQuantityList.add(voucherQuantityBizDo);
                    }
                }
            }
        }
	    
	    CouponInventoryBusinessDO couponInventorDO = new CouponInventoryBusinessDO();
	    couponInventorDO.setLookupID(cartToInspect.getLookupID());
	    couponInventorDO.setOrderId(orderId);
	    couponInventorDO.setCouponInventoryList(voucherQuantityList);
	    
	    
	    
	    if (!voucherQuantityList.isEmpty()) {
	        updateCouponInventoryNew(couponInventorDO);
	    }else{
	    	logger.info("voucherQuantityList is empty for orderId " + orderId);
	    }
	}

	   public void blockCrossSell(String orderId) throws IllegalStateException {
	        
	        if (!paymentTransactionService.hasPaymentSucceeded(orderId)) {
	            throw new IllegalArgumentException("Trying to block coupons when payment has failed for orderID: [" + orderId + "]");
	        }

	        CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId);

	        List<VouchersQuantityBusinessDO> voucherQuantityList = new ArrayList<VouchersQuantityBusinessDO>();

	        for (CartItemsBusinessDO cartItems : cartToInspect.getCartItems()) {
	            if (!cartItems.getDeleted()) {
	                if (cartService.getProductName(cartItems) == ProductName.Crosssell) {
	                    Integer crossSellId = cartItems.getItemId();
	                    Integer couponsQuantity = cartItems.getQuantity();

	                    CrosssellBusinessDo crossSell = crosssellService.getCrosssell(crossSellId);
	                    
	                    Integer couponStoreId = crossSell.getStoreId();
	                    
	                    CouponStore couponStore = couponServiceProxy.getCouponStore(couponStoreId);

	                    if (crossSellAlreadyBlocked(orderId, cartToInspect, couponStore.getCouponStoreId())) {
	                        continue;
	                    }

	                    VouchersQuantityBusinessDO voucherQuantityBizDo = new VouchersQuantityBusinessDO();
	                    voucherQuantityBizDo.setComplimentary(couponStore.getIsComplimentary());
	                    voucherQuantityBizDo.setCouponStoreId(couponStore.getCouponStoreId());
	                    voucherQuantityBizDo.setLookupID(cartToInspect.getLookupID());
	                    voucherQuantityBizDo.setQuantity(couponsQuantity);
	                    voucherQuantityBizDo.setUnique(couponStore.getIsUnique());
	                    voucherQuantityBizDo.setIsMCoupon(couponStore.getIsMCoupon());
	                    voucherQuantityBizDo.setMerchantId(couponStore.getMerchantId());
	                    
	                    voucherQuantityList.add(voucherQuantityBizDo);
	                }
	            }
	        }
	        
	        CouponInventoryBusinessDO couponInventorDO = new CouponInventoryBusinessDO();
	        couponInventorDO.setLookupID(cartToInspect.getLookupID());
	        couponInventorDO.setOrderId(orderId);
	        couponInventorDO.setCouponInventoryList(voucherQuantityList);
	        
	        if (!voucherQuantityList.isEmpty()) {
	            updateCouponInventoryNew(couponInventorDO);
	        }
	    }


    private boolean crossSellAlreadyBlocked(String orderId, CartBusinessDo cartToInspect, Integer couponId) {
        List<TxnHomePage> homePageList = txnHomePageDAO.findByLookupId(cartToInspect.getLookupID());
        Integer txnHomePageId = homePageList.get(0).getId();
        List<TxnCrossSell> crossSellList = txnCrossSellService.getActiveItemByTxnHomePageIdAndCouponId(txnHomePageId, couponId);
        
        if (crossSellList != null && !crossSellList.isEmpty()) {
            for (TxnCrossSell txnCrossSell : crossSellList) {
                if (!txnCrossSell.getIsDeleted()) {
                    logger.info("Coupons already blocked for orderID: [" + orderId + "] and coupon store ID: [" + couponId + "]");
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * Checks if coupons have already been blocked.
	 * @param orderId
	 * @param cartToInspect
	 * @param couponId
	 * @return
	 */
    private boolean couponsAlreadyBlocked(String orderId, CartBusinessDo cartToInspect, Integer couponId) {
        List<TxnHomePage> homePageList = txnHomePageDAO.findByLookupId(cartToInspect.getLookupID());
        Integer txnHomePageId = homePageList.get(0).getId();
        /*
         * Idempotency check.
         */
        List<TxnCrossSell> crossSellList = txnCrossSellService.getActiveItemByTxnHomePageIdAndCouponId(txnHomePageId, couponId);
        if (crossSellList != null && !crossSellList.isEmpty()) {
            for (TxnCrossSell txnCrossSell : crossSellList) {
                if (!txnCrossSell.getIsDeleted()) {
                    logger.info("Coupons already blocked for orderID: [" + orderId + "] and coupon store ID: [" + couponId + "]");
                    return true;
                }
            }
        }
        
        return false;
    }
        
        @Transactional
        public boolean updateCouponInventoryNew(BaseBusinessDO baseBusinessDO) {
                CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;
                logger.info("Entering into updateCouponInventoryNew, couponInventoryBusinessDO value is "+ couponInventoryBusinessDO);
                CartBusinessDo cartToInspect = cartService.getCartByOrderId(couponInventoryBusinessDO.getOrderId());
                List<CartItems> cartItems = cartService.getCartItemsByCartId(cartToInspect.getId().intValue());
                
                List<TxnHomePage> homePageList = txnHomePageDAO.findByLookupId(cartToInspect.getLookupID());
                
                Integer txnHomePageId = homePageList.get(0).getId();
                
                Map<String, List<String>> couponCodeMap = new HashMap<String, List<String>>();
                boolean updateStatus = false;
                try {
                        for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
                                List<Map<String, Object>> coupons = this.getCouponStoreAndInventoryDetails(voucherQtyBusinessDO.getCouponStoreId());
                                Map<String, Object> couponDetails = new HashMap<String, Object>();
                                
                                if(coupons != null && coupons.size() > 0)
                                        couponDetails = coupons.get(0);
                                
                                logger.info((Boolean)couponDetails.get("isUnique"));
                                if((Boolean)couponDetails.get("isUnique")) {
                                        if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {                                        
                                                
                                                int count = this.updateCouponInventoryForUniqueCoupons((Integer)couponDetails.get("couponInventoryId"), voucherQtyBusinessDO.getQuantity(), couponInventoryBusinessDO.getOrderId(), voucherQtyBusinessDO.isComplimentary());
                                                if (count != voucherQtyBusinessDO.getQuantity()){
                    								throw new IllegalStateException("Error updating coupon inventory count for couponInventoryId=" +couponDetails.get("couponInventoryId") + " count="+couponDetails.get("couponCount"));
                                                } else{
                                                	updateStatus = true;
                                                }
                                                logger.info("Coupon count update was success for coupon_inventory_id=" + couponDetails.get("couponInventoryId") + " and count=" + couponDetails.get("couponCount"));
                                                updateCouponInventory(cartItems, txnHomePageId, voucherQtyBusinessDO, couponDetails);
                                                        
                                                List<CouponCode> couponCodes = getCouponCodesFromCouponInventoryAndOrderId((Integer)couponDetails.get("couponInventoryId"), couponInventoryBusinessDO.getOrderId());
                                                
                                                List<String> couponCodeList = getCouponCodes(couponCodes);
                                                couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
                                                logger.info("Coupon quantity requested for orderid: "+ couponInventoryBusinessDO.getOrderId() +" and Coupon Store id: "+voucherQtyBusinessDO.getCouponStoreId() +" = "+voucherQtyBusinessDO.getQuantity()+" size of assigned coupon_code list = "+couponCodes.size());

                                                if(couponCodes !=null && couponCodes.size() < voucherQtyBusinessDO.getQuantity()) {
                                                        throw new IllegalStateException("Could not assign coupon codes for coupon quantity requested for orderid: "+ couponInventoryBusinessDO.getOrderId() +" and Coupon Store id: "+voucherQtyBusinessDO.getCouponStoreId() +" = "+voucherQtyBusinessDO.getQuantity()+" size of coupon list = "+couponCodes.size());
                                                }
                                                
                                                if (voucherQtyBusinessDO.getIsMCoupon()) {
                                                    for (String couponCodeForRedemption : couponCodeList) {
                                                        CouponMerchant couponMerchant = couponServiceProxy.getCouponMerchant(voucherQtyBusinessDO.getMerchantId());
                                                        if(couponMerchant!=null) {
                                                        	couponServiceProxy.createRedemption(couponCodeForRedemption, voucherQtyBusinessDO.getCouponStoreId(), couponMerchant.getMerchantReference());
                                                        }
                                                    }
                                                }
                                        }else {
                                                this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
                                                this.refreshAllActiveCoupons();
                                        }
                                }else {// non unique coupons
                                        if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {                                        
                                                
                                                boolean status = updateCouponInventory(cartItems, txnHomePageId, voucherQtyBusinessDO, couponDetails);
                                                updateStatus = status;
                                                logger.info("notUnique updateStatus" + updateStatus);
                                                
                                                if(status) {
                                                        List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryIdForNonUniqueCoupons((Integer)couponDetails.get("couponInventoryId"));
                                                        
                                                        if(couponCodes != null && couponCodes.size() > 0) {
                                                                List<String> couponCodeList = getCouponCodes(couponCodes);
                                                                couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
                                                        }else {
                                                                List<String> couponCodeList = new ArrayList<String>();
                                                                String couponCode = couponServiceProxy.getGenericCoupons();
                                                                if(couponCode!=null){
                                                                	couponCodeList.add(couponCode);
                                                                }
                                                                couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
                                                        }                       
                                                } else{
                                                	throw new IllegalStateException("Error updating coupon inventory count for couponInventoryId=" +couponDetails.get("couponInventoryId") + " count="+couponDetails.get("couponCount"));
                                                }
                                        }else {
                                                this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
                                                this.refreshAllActiveCoupons();
                                        }
                                }
                                
                                Timestamp couponValidityDate = (Timestamp)couponDetails.get("validityDate");
                                if (couponValidityDate.before(new Date())) {
                                        this.deactivateCoupon(couponDetails, FCConstants.COUPON_EXPIRE);
                                        this.refreshAllActiveCoupons();
                                }
                                if(updateStatus && (Integer)couponDetails.get("thresholdCount") >= ((Integer)couponDetails.get("couponCount") - (Integer)couponDetails.get("sentCouponCount") )) {
                                        this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
                                        this.refreshAllActiveCoupons();
                                }
                        }
                        
                        checkForOrderCouponCountThreshold(txnHomePageId, couponInventoryBusinessDO.getLookupID());

                        logger.info("Allocated CouponCodes : " + couponCodeMap + " for orderId : " + couponInventoryBusinessDO.getOrderId());
                        couponInventoryBusinessDO.setCouponCodes(couponCodeMap);
                } catch (Exception e) {
                        logger.error("Exception in updateCouponInventoryNew : " + e, e);
                }
                return updateStatus;
        }
        
        private void checkForOrderCouponCountThreshold(Integer txnHomePageId, String lookupId) {
            try {
                List<TxnCrossSell> crossSellList = txnCrossSellService.getActiveItemByTxnHomePageId(txnHomePageId);
                
                for (TxnCrossSell txnCrossSell : crossSellList) {
                    /*
                     * In case count of coupons in crossSellList for a given
                     * order ID exceeds a threshold send out e-mail. 
                     * It's an indicative of some problem.
                     */
                    if (txnCrossSell.getQuantity() > fcProperties.getCouponThresholdForOrder()) {
                        this.sendOrderCouponCountThresholdBreacnhamil(lookupId, txnCrossSell.getQuantity());
                    }
                }
	    } catch (Exception e) {
	        logger.error("Something went wrong in checkForOrderCouponCountThreshold for lookupId:[" + lookupId + "] txnHomePageId: " + String.valueOf(txnHomePageId), e);
            }
	}

    private boolean updateCouponInventory(List<CartItems> cartItems, Integer txnHomePageId, VouchersQuantityBusinessDO voucherQtyBusinessDO, Map<String, Object> couponDetails) {
        List<TxnCrossSell> crossSellList = txnCrossSellService.getActiveItemByTxnHomePageIdAndCouponId(txnHomePageId, voucherQtyBusinessDO.getCouponStoreId());

        if (crossSellList == null || crossSellList.isEmpty()) {
            TxnCrossSell txnCrossSell = new TxnCrossSell();
        
            txnCrossSell.setQuantity(voucherQtyBusinessDO.getQuantity());
            txnCrossSell.setCouponId(voucherQtyBusinessDO.getCouponStoreId());
            txnCrossSell.setTxnHomePageId(txnHomePageId);
            txnCrossSell.setCreatedOn((new Timestamp(new Date().getTime())));
            txnCrossSell.setIsDeleted(false);
            
            txnCrossSellService.saveTxnCrossSell(txnCrossSell);

        } else {
        for (TxnCrossSell txnCrossSell : crossSellList) {
            if (txnCrossSell.getCouponId().intValue() == voucherQtyBusinessDO.getCouponStoreId().intValue()) {
                txnCrossSell.setQuantity(voucherQtyBusinessDO.getQuantity());
                txnCrossSellService.updateTxnCrosssell(txnCrossSell);
            }
        }
        }
        
        return this.updateCouponInventoryCount((Integer)couponDetails.get("couponInventoryId"), voucherQtyBusinessDO.getQuantity());
    }
	
	public boolean updateCouponInventoryVeryNew(BaseBusinessDO baseBusinessDO) {
		/*
		 * a non transactional version of updateCouponInventoryNew function is added temparaly. 
		 */
		CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;
		
		CartBusinessDo cartToInspect = cartService.getCartByOrderId(couponInventoryBusinessDO.getOrderId());
		List<CartItems> cartItems = cartService.getCartItemsByCartId(cartToInspect.getId().intValue());
	        
		List<TxnHomePage> homePageList = txnHomePageDAO.findByLookupId(cartToInspect.getLookupID());
	        
		Integer txnHomePageId = homePageList.get(0).getId();
	        
		Map<String, List<String>> couponCodeMap = new HashMap<String, List<String>>();
		boolean updateStatus = false;
		try {
			for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
				List<Map<String, Object>> coupons = this.getCouponStoreAndInventoryDetails(voucherQtyBusinessDO.getCouponStoreId());
				Map<String, Object> couponDetails = new HashMap<String, Object>();
				
				if(coupons != null && coupons.size() > 0)
					couponDetails = coupons.get(0);
				
				if((Boolean)couponDetails.get("isUnique")) {
					if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {					
						
						int countUpdated = this.updateCouponInventoryForUniqueCoupons((Integer)couponDetails.get("couponInventoryId"), voucherQtyBusinessDO.getQuantity(), couponInventoryBusinessDO.getOrderId(), voucherQtyBusinessDO.isComplimentary());
						boolean countUpdateStatus = false;
						if (countUpdated == voucherQtyBusinessDO.getQuantity()){
							countUpdateStatus = true;
						}
						
						if(countUpdateStatus) {
						    updateCouponInventory(cartItems, txnHomePageId, voucherQtyBusinessDO, couponDetails);
							
							List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryAndOrderId((Integer)couponDetails.get("couponInventoryId"), couponInventoryBusinessDO.getOrderId());
							
							List<String> couponCodeList = getCouponCodes(couponCodes);
							List<String> blockTimeList = getBlockTimes(couponCodes);
							couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString() + "BlockTimeList", blockTimeList);
							couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
							logger.debug("Coupon quantity requested for orderid: "+ couponInventoryBusinessDO.getOrderId() +" and Coupon Store id: "+voucherQtyBusinessDO.getCouponStoreId() +" = "+voucherQtyBusinessDO.getQuantity()+" size of assigned coupon_code list = "+couponCodes.size());

							if(couponCodes !=null && couponCodes.size() < voucherQtyBusinessDO.getQuantity()) {
								String messageBody = "Coupon quantity requested for orderid: "+ couponInventoryBusinessDO.getOrderId() +" and Coupon Store id: "+voucherQtyBusinessDO.getCouponStoreId() +" = "+voucherQtyBusinessDO.getQuantity()+" size of coupon list = "+couponCodes.size();
								this.sendErrorEmail(couponInventoryBusinessDO.getOrderId(), messageBody);
                                                        }
                                                }
					}else {
						this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
						this.refreshAllActiveCoupons();
					}
				}else {// non unique coupons
					if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {					
						
						boolean status = updateCouponInventory(cartItems, txnHomePageId, voucherQtyBusinessDO, couponDetails);
						updateStatus = status;
						
						if(status) {
							List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryIdForNonUniqueCoupons((Integer)couponDetails.get("couponInventoryId"));
							
							if(couponCodes != null && couponCodes.size() > 0) {
								List<String> couponCodeList = getCouponCodes(couponCodes);
								couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
							}else {
								List<String> couponCodeList = new ArrayList<String>();
								 String couponCode = couponServiceProxy.getGenericCoupons();
                                 if(couponCode!=null){
                                 	couponCodeList.add(couponCode);
                                 }
								couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
							}			

						}
					}else {
						this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
						this.refreshAllActiveCoupons();
					}
				}
				
				Timestamp couponValidityDate = (Timestamp)couponDetails.get("validityDate");
				if (couponValidityDate.before(new Date())) {
					this.deactivateCoupon(couponDetails, FCConstants.COUPON_EXPIRE);
					this.refreshAllActiveCoupons();
				}
				if(updateStatus && (Integer)couponDetails.get("thresholdCount") >= ((Integer)couponDetails.get("couponCount") - (Integer)couponDetails.get("sentCouponCount") )) {
					this.deactivateCoupon(couponDetails, FCConstants.COUPON_INV_EXHAUST);
					this.refreshAllActiveCoupons();
				}
			}

			logger.debug("Allocated CouponCodes : " + couponCodeMap + " for orderId : " + couponInventoryBusinessDO.getOrderId());
			couponInventoryBusinessDO.setCouponCodes(couponCodeMap);
		} catch (Exception e) {
			logger.error("Exception in updateCouponInventoryNew : " + e, e);
		}
		return updateStatus;
	}
	
	@Transactional
	public boolean updateCouponInventoryCount(Integer couponInventoryId, Integer quantity) {
		boolean isSuccess = false;
		int updatedCount = couponServiceProxy.updateCouponInventoryCount(couponInventoryId, quantity);
		if (updatedCount == 1){
			isSuccess = true;
		}
		return isSuccess;
	}
	
	@Transactional
	public int updateCouponInventoryForUniqueCoupons(Integer couponInventoryId, Integer quantity, String orderId, boolean isComplimentary) {
		boolean isBlocked = couponServiceProxy.blockAndGetCouponCodesNEW(couponInventoryId, quantity, orderId);
		List<CouponCode> codes = couponServiceProxy.getCouponCodesFromCouponInventoryAndOrderId(couponInventoryId, orderId);
		int count = 0;
		if (codes != null){
			count = codes.size();
		}
		return count;
	}
	
	@Transactional
	public List<Map<String, Object>> getCouponStoreAndInventoryDetails(Integer couponStoreId) {
		return couponServiceProxy.getCouponStoreAndInventoryDetails(couponStoreId);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean deactivateCoupon(Map<String, Object> couponDetails , String statusUpdateReason ) {
		 if(couponServiceProxy.deactivateCoupon((Integer)couponDetails.get("couponStoreId"))) {
			 return sendCouponDeactivationNotification(couponDetails,statusUpdateReason);
		 }
		 return false;
	}
	
	private boolean sendCouponDeactivationNotification(Map<String, Object> couponDetails,String statusUpdateReason) {
	 	
			Map<String, Object> params = new HashMap<String, Object>();
			couponDetails.put("eventTime", new Date());
			couponDetails.put("updatedBy", fcProperties.getProperty("coupon.status.updated.by"));
			couponDetails.put("statusUpdateReason", statusUpdateReason);
			List<Map<String, Object>> couponDetailsList = new ArrayList<Map<String, Object>>();
			couponDetailsList.add(couponDetails);
			params.put("couponInventorylist", couponDetailsList);
			String emailIdFrom = fcProperties.getProperty("coupon.inventory.notification.from.emailid");
			String emailIdTo = fcProperties.getProperty("coupon.inventory.notification.to.emailid");
			String subject = fcProperties.getProperty("coupon.status.notification.subject") + " - " + (String)couponDetails.get("couponName") + " : Deactiavted";
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			list.add(params);
			emailService.sendCouponinventoryNotificationMail(emailIdTo, emailIdFrom, subject, FCConstants.COUPON_STATUS_NOTIFICATION_VM, list, true);
		return true;
	}
	
	private void sendOrderCouponCountThresholdBreacnhamil(String lookupId, Integer couponCount) {
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setSubject("Coupon count breached for Lookup ID: " + lookupId);
        emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_TO));
        emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_REPLY_TO));
        emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_FROM));
        emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
        emailBusinessDO.setContent("Coupon count breached for Lookup ID: [" + lookupId + "]. Number of coupons opted : [" + couponCount + "].");
        
        emailService.sendNonVelocityEmail(emailBusinessDO);
	}

	private void sendErrorEmail(String orderId, String messageBody) {
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_FROM));
        emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_TO));
        emailBusinessDO.setSubject("Coupon quantity not matching the requested amount for order id: " + orderId);
        emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.COUPON_THRESHOLD_BREACH_REPLY_TO));
        emailBusinessDO.setId(System.currentTimeMillis());
        emailBusinessDO.setContent(messageBody);
        
        try {
            emailService.sendNonVelocityEmail(emailBusinessDO);
        } catch (Exception e) {
            logger.error("Failed to send error mail for order ID : " + orderId, e);
        }
    }

	@Transactional
	public List<CouponCode> getCouponCodesFromCouponInventoryIdForNonUniqueCoupons(Integer couponInventoryId) {
		return couponServiceProxy.getCouponCodesFromCouponInventoryIdForNonUniqueCoupons(couponInventoryId);
	}
	
	@Transactional
	public boolean resetCouponInventory(BaseBusinessDO baseBusinessDO) {

		CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;

		boolean resetStatus = false;
		for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
			resetStatus = couponServiceProxy.resetCouponInventory(voucherQtyBusinessDO.getCouponStoreId(), voucherQtyBusinessDO.getQuantity(), couponInventoryBusinessDO.getOrderId(), voucherQtyBusinessDO.isComplimentary());

			if (resetStatus == false)
				break;
		}

		return resetStatus;
	}

	@Transactional
	public boolean resetCouponInventoryNew(BaseBusinessDO baseBusinessDO) {
	       CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;
	        logger.debug("ResetInventoryNew: Release Inv, For OrderId: " + couponInventoryBusinessDO.getOrderId());

	        boolean resetStatus = false;
	        
	        for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
	            List<CouponInventory> couponInventoryList = couponServiceProxy.getCouponInventoriesFromCouponStore(voucherQtyBusinessDO.getCouponStoreId());
	            logger.debug("ResetInventoryNew: OrderId: " + couponInventoryBusinessDO.getOrderId() + " Cpn Id: " + voucherQtyBusinessDO.getCouponStoreId());
	            
	            if (couponInventoryList != null && !couponInventoryList.isEmpty()) {
	                int couponQtyReleased = voucherQtyBusinessDO.getQuantity();
	                if (voucherQtyBusinessDO.isUnique()) {
	                    // Released quantity can differ from actual quantity against the Order
	                    int uniqueCouponReleased = couponServiceProxy.releaseCouponInventoryForUniqueCouponsAndReturnCount(couponInventoryList.get(0).getCouponInventoryId(),  couponInventoryBusinessDO.getOrderId());
	                    logger.debug("ResetInventoryNew: OrderId: " + couponInventoryBusinessDO.getOrderId() + "Cpn Inv ID: " + couponInventoryList.get(0).getCouponInventoryId() + " CouponQtyToBeReleased: " + couponQtyReleased + " ActualCountReleased: " + uniqueCouponReleased);
	                    
	                    couponQtyReleased = uniqueCouponReleased;
	                }

	                if ((couponInventoryList.get(0).getSentCouponCount() - couponQtyReleased ) < 0) // We don't want to update sent_coupon_count to negative values
	                    couponQtyReleased = couponInventoryList.get(0).getSentCouponCount(); 
	                
	                resetStatus = couponServiceProxy.resetCouponInventoryCount(couponInventoryList.get(0).getCouponInventoryId(), couponQtyReleased);
	                
	                if (resetStatus == false) {
	                    logger.error("ResetInventoryNew: Release Inv Failed For OrderId: " + couponInventoryBusinessDO.getOrderId());
	                    break;
	                }
	            }
	        }
	        
	        logger.debug("ResetInventoryNew: Release Inv, For OrderId: " + couponInventoryBusinessDO.getOrderId() + "Status: " + resetStatus);
	        return resetStatus;
	}

	public List<String> getCouponCodes(List<CouponCode> couponCodes) {
		List<String> couponCodeList = new ArrayList<String>();

		for (CouponCode couponCode : couponCodes) {
			couponCodeList.add(couponCode.getCouponCode());
		}

		return couponCodeList;
	}
	
	public List<String> getBlockTimes(List<CouponCode> couponCodes) {
		List<String> blockTimeList = new ArrayList<String>();

		for (CouponCode couponCode : couponCodes) {
			blockTimeList.add(String.valueOf(couponCode.getBlockTime()));
		}

		return blockTimeList;
	}

	public List<VoucherCat> getAllActiveVoucherCat() {
		long startTime = System.currentTimeMillis();
		boolean cacheMiss = false;
		List<VoucherCat> voucherCatList = null;
		try {
			voucherCatList = couponServiceProxy.getAllActiveVaucherCatCouponMemCache();
			if (voucherCatList == null || voucherCatList.size() == 0) {
				cacheMiss = true;
				voucherCatList = couponServiceProxy.getAllActiveVaucherCatDao();
				couponServiceProxy.setAllActiveVaucherCat(voucherCatList);
			}

		} catch (Exception ex) {
			logger.error("error occur while calling  the getAllActiveVaucherCat(BaseBusinessDO baseBusinessDO) method :" + ex, ex);
			voucherCatList = couponServiceProxy.getAllActiveVaucherCatDao();
			ex.printStackTrace();
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		boolean isCouponServiceProfilingEnabled = fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED);
		if(isCouponServiceProfilingEnabled){
			metricsClient.recordLatency("CouponService", "getAllActiveVoucherCat", duration);
			metricsClient.recordEvent("CouponService", "cacheMiss-getAllActiveVoucherCat", String.valueOf(cacheMiss));
		}
		return voucherCatList;
	}

	@Transactional
	public List<Map<String, Object>> getCouponDetailsBylookupId(String lookupid) {
		List<Map<String, Object>> map = null;
		try {
			if (lookupid != null) {
				map = couponService.getCouponDetailsBylookupId(lookupid);
				map.addAll(crosssellService.getCrossSellDetailsBylookupId(lookupid));
			}
		} catch (Exception e) {
			logger.error("error occur while getting the coupon details based on the lookup id ,lookup id is :" + lookupid, e);
		}
		return map;
	}
	
	public CouponInventoryBusinessDO getCouponInventoryBusinessDO(List<Map<String, Object>> couponDetailsListmap, String orderId, String lookupId) {
		CouponInventoryBusinessDO couponInventoryBusinessDO = new CouponInventoryBusinessDO();
		try {
			couponInventoryBusinessDO.setOrderId(orderId);
			couponInventoryBusinessDO.setLookupID(lookupId);
			List<VouchersQuantityBusinessDO> vaoucherQuantiyList = new ArrayList<VouchersQuantityBusinessDO>();
			for (Map<String, Object> couponDataMap : couponDetailsListmap) {
				if (couponDataMap != null && !couponDataMap.isEmpty()) {
					VouchersQuantityBusinessDO vouchersQuantityBusinessDO = new VouchersQuantityBusinessDO();
					vouchersQuantityBusinessDO.setCouponStoreId(Integer.parseInt(couponDataMap.get("couponStoreId").toString()));
					vouchersQuantityBusinessDO.setLookupID(lookupId);
					vouchersQuantityBusinessDO.setQuantity(Integer.parseInt(couponDataMap.get("count").toString()));
					vouchersQuantityBusinessDO.setComplimentary(false);
					vouchersQuantityBusinessDO.setUnique(new Boolean(couponDataMap.get("isUnique").toString()));
					vaoucherQuantiyList.add(vouchersQuantityBusinessDO);
				}
			}
			couponInventoryBusinessDO.setCouponInventoryList(vaoucherQuantiyList);
		} catch (Exception e) {
			logger.error("error occur while getCouponInventoryBusinessDO ,cause :" + e.getCause() + e.getMessage(), e);
		}
		return couponInventoryBusinessDO;
	}
	
	public CouponInventoryBusinessDO getFreeCouponInventoryForResetBusinessDO(List<Map<String, Object>> couponDetailsListmap, String orderId, String lookupId) {
		CouponInventoryBusinessDO couponInventoryBusinessDO = new CouponInventoryBusinessDO();
		try {
			couponInventoryBusinessDO.setOrderId(orderId);
			couponInventoryBusinessDO.setLookupID(lookupId);
			List<VouchersQuantityBusinessDO> vaoucherQuantiyList = new ArrayList<VouchersQuantityBusinessDO>();
			for (Map<String, Object> couponDataMap : couponDetailsListmap) {
				if (couponDataMap != null && !couponDataMap.isEmpty() && !FCConstants.COUPON_TYPE_C.equals(couponDataMap.get("couponType").toString())) {
					VouchersQuantityBusinessDO vouchersQuantityBusinessDO = new VouchersQuantityBusinessDO();
					vouchersQuantityBusinessDO.setCouponStoreId(Integer.parseInt(couponDataMap.get("couponStoreId").toString()));
					vouchersQuantityBusinessDO.setLookupID(lookupId);
					vouchersQuantityBusinessDO.setQuantity(Integer.parseInt(couponDataMap.get("count").toString()));
					vouchersQuantityBusinessDO.setComplimentary(false);
					vouchersQuantityBusinessDO.setUnique(new Boolean(couponDataMap.get("isUnique").toString()));
					vaoucherQuantiyList.add(vouchersQuantityBusinessDO);
				}
			}
			couponInventoryBusinessDO.setCouponInventoryList(vaoucherQuantiyList);
		} catch (Exception e) {
			logger.error("error occur while getCouponInventoryBusinessDO ,cause :" + e.getCause() + e.getMessage(), e);
		}
		return couponInventoryBusinessDO;
	}
	
	public void resetCouponInventoryDetails(BaseBusinessDO baseBusinessDO) {
        try {
            String orderid = ((CouponInventoryBusinessDO) baseBusinessDO).getOrderId();
            resetCouponInventoryNew(baseBusinessDO);
            // Reset the coupon inventory Key in session object
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            fs.getSessionData().put(orderid+"_couponInventory", null);
            
        } catch (Exception e) {
            logger.error("error occur while resetting coupondetails in inventory ,cause :" + e.getCause() + " Message:" + e.getMessage(), e);
        }
	}

	@Transactional
	public void getCouponInventoryAndSendMailIfReachThresholdValue() {
		try {
			int additionalThresholdvalue = Integer.parseInt(fcProperties.getProperty(FCConstants.COUPON_ADDITIONAL_THRESHOLD_COUNT));
			List<CouponInventory> couponinventoryList = couponServiceProxy.getCouponInventoryWhichReachedThresholdValue(additionalThresholdvalue);
			if (couponinventoryList != null && couponinventoryList.size() > 0) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("couponInventorylist", couponinventoryList);
				String emailIdFrom = fcProperties.getProperty("coupon.inventory.notification.from.emailid");
				String emailIdTo = fcProperties.getProperty("coupon.inventory.notification.to.emailid");
				String subject = fcProperties.getProperty("coupon.inventory.notification.subject");
				List<Map<String, Object>> map = new ArrayList<Map<String, Object>>();
				map.add(params);
				emailService.sendCouponinventoryNotificationMail(emailIdTo, emailIdFrom, subject, FCConstants.COUPON_INVENTORY_NOTIFICATION_WHEN_REACHED_THRESHOLD_VALUE_VM, map, true);
			}
		} catch (Exception e) {
			logger.error("exception occur while getCouponInventoryAndSendMailIfReachThresholdValue :" + e.toString() + "  cause :" + e.getCause() + ", Message " + e.getMessage(), e);
		}
	}
	
	@Transactional
	public List<CouponCode> getCouponCodesFromCouponInventoryAndOrderId(Integer couponInventoryId, String orderId) {
		return couponServiceProxy.getCouponCodesFromCouponInventoryAndOrderId(couponInventoryId, orderId);
	}
	
	@Transactional
	public CouponInventoryBusinessDO CouponCodeMap(BaseBusinessDO baseBusinessDO) {
		CouponInventoryBusinessDO couponInventoryBusinessDO = (CouponInventoryBusinessDO) baseBusinessDO;
		Map<String, List<String>> couponCodeMap = new HashMap<String, List<String>>();
		boolean updateStatus = false;
		try {
			for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
				List<Map<String, Object>> coupons = this.getCouponStoreAndInventoryDetails(voucherQtyBusinessDO.getCouponStoreId());
				Map<String, Object> couponDetails = new HashMap<String, Object>();
				
				if(coupons != null && coupons.size() > 0)
					couponDetails = coupons.get(0);
				
				if((Boolean)couponDetails.get("isUnique")) {
					if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {					
						
							List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryAndOrderId((Integer)couponDetails.get("couponInventoryId"), couponInventoryBusinessDO.getOrderId());
							
							List<String> couponCodeList = getCouponCodes(couponCodes);
							List<String> blockTimeList = getBlockTimes(couponCodes);
 							couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
 							couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString() + "BlockTimeList", blockTimeList);
					}
				}else {
					if(couponDetails != null && (Integer)couponDetails.get("couponCount") >= ((Integer)couponDetails.get("sentCouponCount") + voucherQtyBusinessDO.getQuantity())) {					
						
							List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryIdForNonUniqueCoupons((Integer)couponDetails.get("couponInventoryId"));
							
							if(couponCodes != null && couponCodes.size() > 0) {
								List<String> couponCodeList = getCouponCodes(couponCodes);
								couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
							}else {
								List<String> couponCodeList = new ArrayList<String>();
								 String couponCode = couponServiceProxy.getGenericCoupons();
                                 if(couponCode!=null){
                                 	couponCodeList.add(couponCode);
                                 }
								couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
							}			
					}
				}
			}

			logger.debug("Allocated CouponCodes : " + couponCodeMap + " for orderId : " + couponInventoryBusinessDO.getOrderId());
			couponInventoryBusinessDO.setCouponCodes(couponCodeMap);
		} catch (Exception e) {
			logger.error("Exception in updateCouponInventoryNew : " + e, e);
		}
		return couponInventoryBusinessDO;
	}
	
	public Map<String, List<String>> getAllCouponCodes(String orderId, CouponInventoryBusinessDO couponInventoryBusinessDO) {
		Map<String, List<String>> couponCodeMap = new HashMap<String, List<String>>();
		
		for (VouchersQuantityBusinessDO voucherQtyBusinessDO : couponInventoryBusinessDO.getCouponInventoryList()) {
			List<Map<String, Object>> coupons = this.getCouponStoreAndInventoryDetails(voucherQtyBusinessDO.getCouponStoreId());
			Map<String, Object> couponDetails = new HashMap<String, Object>();
			
			if(coupons != null && coupons.size() > 0)
				couponDetails = coupons.get(0);
			
			if((Boolean)couponDetails.get("isUnique")) {
				
				List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryAndOrderId((Integer)couponDetails.get("couponInventoryId"), couponInventoryBusinessDO.getOrderId());
				
				List<String> couponCodeList = getCouponCodes(couponCodes);
				couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
				logger.debug("Coupon quantity requested for orderid: "+ couponInventoryBusinessDO.getOrderId() +" and Coupon Store id: "+voucherQtyBusinessDO.getCouponStoreId() +" = "+voucherQtyBusinessDO.getQuantity()+" size of assigned coupon_code list = "+couponCodes.size());

			} else {
				List<CouponCode> couponCodes = this.getCouponCodesFromCouponInventoryIdForNonUniqueCoupons((Integer)couponDetails.get("couponInventoryId"));
				
				if(couponCodes != null && couponCodes.size() > 0) {
					List<String> couponCodeList = getCouponCodes(couponCodes);
					couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
				}else {
					List<String> couponCodeList = new ArrayList<String>();
					 String couponCode = couponServiceProxy.getGenericCoupons();
                     if(couponCode!=null){
                     	couponCodeList.add(couponCode);
                     }
					couponCodeMap.put(voucherQtyBusinessDO.getCouponStoreId().toString(), couponCodeList);
				}	
			}
		}
			
		return couponCodeMap;
	}
	
	public List<Map<String, Object>> getCouponDetailsBylookupIdAndCouponType(String lookupId, String couponType) {

		List<Map<String, Object>> map = null;
		try {
			if (lookupId != null) {
				map = couponService.getCouponDetailsBylookupIdAndCouponType(lookupId, couponType);
			}
		} catch (Exception e) {
			logger.error("error occur while getting coupon details for lookup id  :" + lookupId + " couponType:" + couponType, e);
		}

		return map;
	}

	public List<Map<String, Object>> getCouponDetailsFromCartBylookupIdAndCouponType(String lookupId, String couponType) {

		List<Map<String, Object>> map = null;
		try {
			if (lookupId != null) {
				map = couponService.getCouponDetailsFromCartBylookupIdAndCouponType(lookupId, couponType);
			}
		} catch (Exception e) {
			logger.error("error occur while getting coupon details from cart for lookup id  :" + lookupId + " couponType:" + couponType, e);
		}
		return map;
	}
	
	/*Getting reissued coupons details*/
	public List<Map<String, Object>> getReissuedCouponMaps(String orderId) {
		return couponServiceProxy.getReissuedCouponMaps(orderId);
	}
	
	@Transactional(readOnly=true)
	public List<CouponStore> getAllUniqueCampaigns() {
		List<CouponStore> coupons = couponServiceProxy.getAvailableUniqueCoupons();
		return coupons;
	}
	
	@Transactional
	public void setMCouponStatus(int couponStoreId, boolean isMCoupon){
		int updatedCount = couponServiceProxy.setmCoupon(couponStoreId, isMCoupon);
		if (updatedCount != 1){
			throw new IllegalStateException("Failed to update m_coupon status for couponStoreId=" + couponStoreId);
		}
	}
	
	@Transactional(readOnly=true)
	public CouponStore getActiveCouponByCouponStoreId(int couponStoreId){
		return couponServiceProxy.getActiveCouponByCouponStoreId(couponStoreId);
	}
	
}
