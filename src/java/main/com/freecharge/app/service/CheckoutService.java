package com.freecharge.app.service;

import com.freecharge.antifraud.AntiFraudDAO;
import com.freecharge.antifraud.AntiFraudUtil;
import com.freecharge.antifraud.FraudDetection;
import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.exception.CampaignConditionException;
import com.freecharge.app.service.exception.CardValidationException;
import com.freecharge.app.service.exception.FraudDetectedException;
import com.freecharge.app.service.exception.InvalidPaymentRequestException;
import com.freecharge.cardstorage.*;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.PersonalDetailsBusinessDO;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.exception.DataNotFoundInLocalCacheException;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.freefund.services.UtmTrackerService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.view.CheckoutData;
import com.freecharge.mobile.web.view.payment.CardStorage;
import com.freecharge.mobile.web.view.payment.CreditCard;
import com.freecharge.mobile.web.view.payment.DebitCard;
import com.freecharge.payment.dao.SavedCardTransactionsDAO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.*;
import com.freecharge.payment.services.PaymentGatewayHandler.RequestParameters;
import com.freecharge.payment.services.binrange.CardBinRangeDelegate;
import com.freecharge.payment.util.CardValidationUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.PaymentAttemptSNSService;
import com.freecharge.sns.bean.PaymentAttemptAlertBean;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.util.WebConstants;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CheckoutService {
    private final Logger                  logger      = LoggingFactory.getLogger(getClass());

    @Autowired
    private CartService                   cartService;

    @Autowired
    private CartCrosssellService          cartCrosssellService;

    @Autowired
    private FCProperties                  properties;

    @Autowired
    private UserAuthService               userAuthService;

    @Autowired
    private FreefundService               freefundService;

    @Autowired
    private WalletService                 walletService;

    @Autowired
    private PaymentPlanService            paymentPlanService;

    @Autowired
    @Qualifier("card-and-meta")
    private ICardStorageService           cardStorageService;

    @Autowired
    private CardStorageService            cardStorage;

    // TODO: This should not be required here.
    @Autowired
    private UsersDAO                      usersDao;

    @Autowired
    private CardStorageDAO                cardStorageDAO;

    @Autowired
    private AntiFraudDAO                  afDAO;

    @Autowired
    private AntiFraudUtil                 afu;

    @Autowired
    private FraudDetection                fd;

    @Autowired
    private FCProperties                  fcProperties;

    @Autowired
    UserServiceProxy                      userServiceProxy;

    @Autowired
    private CardStorageService            cardUserPreferenceStorageService;

    @Autowired
    private PaymentTypeFactory            paymentTypeFactory;

    @Autowired
    private AppConfigService              appConfigService;

    @Autowired
    private MetricsClient                 metricsClient;

    @Autowired
    private FraudWhitelistService         fraudWhitelistService;

    @Autowired
    private CardBinRangeDelegate          cardBinRangeDelegate;

    private static final String[]         extraFields = { "ccCardToken", "ccCardSaveBool", "ccCardSaveName",
            "dbCardToken", "dbCardSaveBool", "dbCardSaveName", "cardToken", "saveCard", "saveCardName" };

    @Autowired
    private PricingService                pricingService;

    @Autowired
    private DeviceFingerPrintManager      deviceFingerPrintManager;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private PaymentService                paymentService;

    @Autowired
    private PersonalDetailsService        personalDetailsService;

    @Autowired
    WalletWrapper                         walletWrapper;

    @Autowired
    OneCheckWalletService                 oneCheckWalletService;

    @Autowired
    private PaymentAttemptSNSService      paymentAttemptSNSService;

    @Autowired
    private PaymentOptionCache            paymentOptionCache;

    @Autowired
    SavedCardTransactionsDAO              savedCardTransactionsDAO;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    CardStatusHandler                     cardStatusHandler;

    @Autowired
    private CardStorageDelegate           cardStorageDelegate;
    
    @Autowired
    PaymentRetryService                   paymentRetryService;

    @Autowired
    private CampaignClientService campaignClientService;
    
    @Autowired
    private UtmTrackerService utmTrackerService;
    
    @Autowired
    private OrderIdWriteDAO                 orderIdWriteDAO;
    
    @Autowired
    private OMSClientService omsClient;
    
    @Autowired
    private PaymentsCache paymentsCache;
    
    private ExecutorService campaignExecutor = Executors.newCachedThreadPool();
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map doPayment(Map<String, String> requestMapData, ProductPaymentBusinessDO productPaymentBusinessDO,
            String lookupId, String ipAddress, String iciciSecureCookie, int channel)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        doProductPaymentRequestDelegate(productPaymentBusinessDO, channel);
        checkFraud(productPaymentBusinessDO, channel);
        Map<String, String> requestMap = doPaymentPortalHandleRequest(productPaymentBusinessDO, requestMapData,
                channel);
        // Async method to check if card bin is existing in database.
        cardBinRangeDelegate.checkCardBinRangeTable(productPaymentBusinessDO);
        Users loggedInUser = userServiceProxy.getLoggedInUser();
        productPaymentBusinessDO.setUser(loggedInUser);
        if (loggedInUser != null) {
            productPaymentBusinessDO.setUserId(loggedInUser.getUserId());
        }
        productPaymentBusinessDO.setMtxnId(getPaymentGatewayOrderId(productPaymentBusinessDO.getOrderId()));
        paymentService.updateTransactionHistoryForTransaction(productPaymentBusinessDO.getOrderId(),
                productPaymentBusinessDO.getUserId(), PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED);
        PaymentRequestBusinessDO prbdo = doPaymentRequestDelegate(productPaymentBusinessDO, requestMap, lookupId,
                ipAddress, iciciSecureCookie, channel);
        int channelId = FCConstants.CHANNEL_ID_WEB;
        Map<String, Object> resultMap = prbdo.getResultMap();
        String mtxnId = (String) resultMap.get(PaymentConstants.MERCHANT_ORDER_ID);
        String paymentGatewayName = (String) resultMap.get(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY);
        if (mtxnId != null && paymentGatewayName != null) {
            publishPaymentAttempt(productPaymentBusinessDO, mtxnId, channelId, paymentGatewayName);
        }
        return prbdo.getResultMap();
    }

    public void publishPaymentAttempt(ProductPaymentBusinessDO paymentBusinessDO, String mtxnId, int channelId,
            String paymentGateway) {

        logger.info("Publising payment attempt alert for mtxnid : " + mtxnId);
        try {
            PaymentAttemptAlertBean alertBean = new PaymentAttemptAlertBean();
            alertBean.setUserId((long) paymentBusinessDO.getUserId());
            alertBean.setOrderId(paymentBusinessDO.getOrderId());

            String channelType = FCUtil.getChannelFromOrderId(alertBean.getOrderId());
            alertBean.setChannel(channelType);
            String productLabel;
            try {
                FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
                String rechargeType = FCSessionUtil.getRechargeType(paymentBusinessDO.getLookupID(), fs);
                ProductName productName = ProductName.fromPrimaryProductType(rechargeType);
                productLabel = productName.getLabel();
            } catch (IllegalArgumentException e) {
                logger.error("Illegal product type found while notifying payment attempt alert", e);
                productLabel = "";
            }
            alertBean.setProduct(productLabel);

            alertBean.setMtxnId(mtxnId);
            if (StringUtils.isNotEmpty(paymentGateway)) {
                alertBean.setPaymentGateway(paymentGateway);
            }

            int paymentType = Integer.parseInt(paymentBusinessDO.getPaymenttype());
            String paymentTypeName;
            try {
                PaymentType.PaymentTypeName typeName = PaymentType.PaymentTypeName.fromPaymentTypeCode(paymentType);
                paymentTypeName = typeName.toString();
            } catch (IllegalArgumentException e) {
                logger.error("Illegal payment type found while notifying payment attempt alert", e);
                paymentTypeName = "";
            }
            alertBean.setPaymentType(paymentTypeName);

            PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlan(paymentBusinessDO.getOrderId());
            if (fcPaymentPlan != null) {
                Double creditsAmount = fcPaymentPlan.getWalletAmount().getAmount().doubleValue();
                Double pgAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                if ((paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)
                        || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType)
                        && creditsAmount == 0.0) {
                    creditsAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                    pgAmount = 0.0;
                }
                com.freecharge.sns.bean.PaymentPlan paymentPlan = new com.freecharge.sns.bean.PaymentPlan();
                paymentPlan.setPg(pgAmount);
                paymentPlan.setCredits(new Double(0.0));
                paymentPlan.setOcw(new Double(0.0));
                if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                    paymentPlan.setCredits(creditsAmount);
                } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType) {
                    paymentPlan.setOcw(creditsAmount);
                }
                alertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
                alertBean.setPaymentPlan(paymentPlan);
            }
            paymentAttemptSNSService.publish(paymentBusinessDO.getOrderId(),
                    "{\"message\" : " + PaymentAttemptAlertBean.getJsonString(alertBean) + "}");

        } catch (Exception e) {
            logger.error("Error while publishing the payment attempt method to SNS for order id "
                    + paymentBusinessDO.getOrderId(), e);
        }
    }

    // TODO: Get rid of requestMapData not very useful
    @SuppressWarnings("rawtypes")
    public Map doCompletePayment(Map<String, String> requestMapData, ProductPaymentBusinessDO productPaymentBusinessDO,
            String lookupId, String ipAddress, String iciciSecureCookie, int channel, boolean saveCard)
                    throws Exception {
        Map<String, String> storedCardDetails = getStoredCardDetails(userServiceProxy.getLoggedInUser().getUserId(),
                requestMapData.get(PaymentConstants.CARD_TOKEN));
        validateCardDetails(productPaymentBusinessDO, storedCardDetails, saveCard);

        JSONObject hashCheckObj = appConfigService.getCustomProp(FCConstants.PROMOCODE_PAYMENT_INITIATE_CHECK);
        Object isCheckEnabled = hashCheckObj.get(FCConstants.IS_CHECK_ENABLED);
        if (isCheckEnabled != null && !FCUtil.isEmpty(String.valueOf(isCheckEnabled))
                && "true".equalsIgnoreCase(String.valueOf(isCheckEnabled))) {
            String channels = String.valueOf(hashCheckObj.get(FCConstants.ENABLED_CHANNELS));
            if (!FCUtil.isEmpty(channels) && channels.contains(String.valueOf(channel))) {
                logger.info(productPaymentBusinessDO.getPaymentoption());
                evaluateCampaignOnPaymentInitiation(productPaymentBusinessDO, lookupId, storedCardDetails);
            }
        }
        doProductPaymentPreProcessing(productPaymentBusinessDO, channel);
        //storing Data to UTM
        utmTrackerService.storeUtmDetailsInDynamo(productPaymentBusinessDO.getUtmDetails(), String.valueOf(channel), productPaymentBusinessDO.getOrderId());
        checkFraud(productPaymentBusinessDO, channel);
        populateStoredCardDetails(userServiceProxy.getLoggedInUser().getUserId(), productPaymentBusinessDO,
                requestMapData, storedCardDetails);
        fd.checkProductLevelFraudLimits(userServiceProxy.getLoggedInUser(), productPaymentBusinessDO);
        // Async method to check if card bin is existing in database.
        cardBinRangeDelegate.checkCardBinRangeTable(productPaymentBusinessDO);
        paymentService.updateTransactionHistoryForTransaction(productPaymentBusinessDO.getOrderId(),
                productPaymentBusinessDO.getUserId(), PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED);
        if (requestMapData.containsKey("retry_order_id")
                && !StringUtils.isEmpty(requestMapData.get("retry_order_id"))) {
            logger.info("Updating retrying order id");
            paymentRetryService.updateOrderForRetry(requestMapData.get("retry_order_id"),
                    productPaymentBusinessDO.getOrderId());
        }
        
        logger.info("UPIDebug: Checkoutservice orderId: "+productPaymentBusinessDO.getOrderId());
        PaymentRequestBusinessDO prbdo = paymentExecution(productPaymentBusinessDO, requestMapData, lookupId, ipAddress,
                iciciSecureCookie, channel);
        return prbdo.getResultMap();
    }
    
    private void evaluateCampaignOnPaymentInitiation(final ProductPaymentBusinessDO productPaymentBusinessDO,
            final String lookupId, final Map<String, String> storedCardDetails) throws Exception {

        Callable<Void> task = new Callable<Void>() {
            public Void call() throws CampaignConditionException, InvalidPaymentRequestException {
                campaignClientService.evaluateCampaignOnPaymentInitiation(productPaymentBusinessDO, lookupId,
                        storedCardDetails);
                return null;
            }
        };
        Future<Void> future = campaignExecutor.submit(task);
        try {
            Void response = future.get(FCConstants.CAMPAIGN_SERVICE_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            logger.error("Timeout exception while calling campaign client for lookupId: " + lookupId);
        } catch (InterruptedException e) {
            logger.error("InterruptedException exception while calling campaign client for lookupId: " + lookupId);
        } catch (ExecutionException e) {
        	if(e.getCause() != null && e.getCause() instanceof CampaignConditionException){
        		throw (CampaignConditionException) e.getCause();
        	}
            logger.error("ExecutionException exception while calling campaign client for lookupId: " + lookupId);
        } finally {
            future.cancel(true); // may or may not desire this
        }

    }

    private void saveCard(ProductPaymentBusinessDO productPaymentBusinessDO) {
        PaymentRequestCardDetails paymentRequestCardDetails = productPaymentBusinessDO.getPaymentRequestCardDetails();
        if (FCUtil.isEmpty(paymentRequestCardDetails.getCardNumber())) {
            return;
        }
        Integer userId = FCSessionUtil.getLoggedInUserId();
        cardStorageDelegate.addCard(paymentRequestCardDetails, userId,
                FCConstants.channelMasterMap.get(productPaymentBusinessDO.getChannelId()),
                productPaymentBusinessDO.getPaymentoption());
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Map doCreditCardPayment(CheckoutData checkoutData, CardStorage cardStorage, String lookupId,
            String iciciSecureCookie, String remoteIp, int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        CreditCard creditCard = checkoutData.getPaymentOptions().getCreditCard();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();

        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_CREDITCARD);
        String cardType = PaymentUtil.getCreditCardType(creditCard.getCardNumber());
        ppbdo.setPaymentoption(cardType);
        ppbdo.setLookupID(lookupId);
        ppbdo.setCcNumber(creditCard.getCardNumber().trim());
        ppbdo.setAppVersion(appVersion);

        Map requestMap = new HashMap();
        requestMap.put(PaymentConstants.CARD_NUM_KEY, creditCard.getCardNumber());
        requestMap.put(PaymentConstants.CARD_EXPMON_KEY, creditCard.getExpiryMonth());
        requestMap.put(PaymentConstants.CARD_EXPYEAR_KEY, creditCard.getExpiryYear());
        requestMap.put(PaymentConstants.CARD_HOLDER_NAME_KEY, creditCard.getName().trim());
        requestMap.put(PaymentConstants.CARD_CVV_KEY, creditCard.getCvvNumber().trim());
        requestMap.put(PaymentConstants.CARD_SAVE_BOOL, String.valueOf(cardStorage.isSaveCardSelected()));
        requestMap.put(PaymentConstants.CARD_SAVE_NAME, cardStorage.getSaveCardName().trim());
        requestMap.put(PaymentConstants.IS_DEBIT_CARD, "false");

        ppbdo.setUsePartialWalletPayment(partialPayment);
        checkoutData.setOrderId(ppbdo.getOrderId());
        Map resultMap = doPayment(requestMap, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map doSaveCreditCardPayment(CheckoutData checkoutData, CardStorage cardStorage, String lookupId,
            String iciciSecureCookie, String remoteIp, int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        CreditCard creditCard = checkoutData.getPaymentOptions().getCreditCard();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();
        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_CREDITCARD);
        // String cardType = getCreditCardType(creditCard.getCardNumber());
        String cardType = PaymentConstants.VISA_CC;
        if (cardStorage.getCardReference() != null || cardStorage.getCardReference().equals("")) {
            cardType = getStoredCreditCardType(cardStorageDAO.findCardByReference(cardStorage.getCardReference()));
        }

        ppbdo.setPaymentoption(cardType);
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);

        Map requestMap = new HashMap();

        requestMap.put(PaymentConstants.CARD_CVV_KEY, creditCard.getCvvNumber().trim());
        requestMap.put(PaymentConstants.CARD_TOKEN, cardStorage.getCreditCardToken().trim());
        requestMap.put(PaymentConstants.IS_DEBIT_CARD, "false");
        // for partial payment
        ppbdo.setUsePartialWalletPayment(partialPayment);
        checkoutData.setOrderId(ppbdo.getOrderId());
        Map resultMap = doPayment(requestMap, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    private String getStoredCreditCardType(CardMetaData findCardByReference) {
        String cardType = PaymentConstants.VISA_CC;
        if (findCardByReference != null) {
            if (findCardByReference.getCardBrand().equalsIgnoreCase("MAESTRO")
                    || findCardByReference.getCardBrand().equalsIgnoreCase("MASTERCARD")) {
                return PaymentConstants.MASTER_CC;
            } else if (findCardByReference.getCardBrand().equalsIgnoreCase("VISA")) {
                return PaymentConstants.VISA_CC;
            }
        }
        return cardType;
    }

    public Map doDebitCardPayment(CheckoutData checkoutData, CardStorage cardStorage, String lookupId,
            String iciciSecureCookie, String remoteIp, int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        DebitCard debitCard = checkoutData.getPaymentOptions().getDebitCard();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();
        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_DEBITCARD);
        ppbdo.setPaymentoption(debitCard.getCardType());
        ppbdo.setLookupID(lookupId);
        ppbdo.setDcNumber(debitCard.getCardNumber().trim());
        ppbdo.setAppVersion(appVersion);
        Map requestMap = new HashMap();
        requestMap.put(PaymentConstants.CARD_NUM_KEY, debitCard.getCardNumber());
        requestMap.put(PaymentConstants.CARD_EXPMON_KEY, debitCard.getExpiryMonth());
        requestMap.put(PaymentConstants.CARD_EXPYEAR_KEY, debitCard.getExpiryYear());
        requestMap.put(PaymentConstants.CARD_HOLDER_NAME_KEY, debitCard.getName().trim());
        requestMap.put(PaymentConstants.CARD_CVV_KEY, debitCard.getCvvNumber().trim());
        requestMap.put(PaymentConstants.CARD_SAVE_BOOL, String.valueOf(cardStorage.isSaveCardSelected()));
        requestMap.put(PaymentConstants.CARD_SAVE_NAME, cardStorage.getSaveCardName().trim());
        requestMap.put(PaymentConstants.IS_DEBIT_CARD, "true");
        requestMap.put(RequestParameters.PYOP, debitCard.getCardType().trim());

        ppbdo.setUsePartialWalletPayment(partialPayment);
        checkoutData.setOrderId(ppbdo.getOrderId());
        Map resultMap = doPayment(requestMap, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map doSaveDebitCardPayment(CheckoutData checkoutData, CardStorage cardStorage, String lookupId,
            String iciciSecureCookie, String remoteIp, int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        DebitCard debitCard = checkoutData.getPaymentOptions().getDebitCard();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();
        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_DEBITCARD);
        ppbdo.setPaymentoption(cardStorage.getCardType());
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);
        Map requestMap = new HashMap();

        requestMap.put(PaymentConstants.CARD_CVV_KEY, debitCard.getCvvNumber().trim());

        requestMap.put(PaymentConstants.CARD_TOKEN, cardStorage.getDebitCardToken().trim());
        requestMap.put(PaymentConstants.IS_DEBIT_CARD, "true");
        ppbdo.setUsePartialWalletPayment(partialPayment);

        Map resultMap = doPayment(requestMap, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map doNetBankingPayment(CheckoutData checkoutData, String lookupId, String iciciSecureCookie,
            String remoteIp, int channel, Boolean partialPayment, Integer appVersion, String deviceId)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        String bankCode = checkoutData.getPaymentOptions().getNetBanking().getBankCode();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();

        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_NETBANKING);
        ppbdo.setPaymentoption(bankCode);
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);

        ppbdo.setUsePartialWalletPayment(partialPayment);
        ppbdo.setDeviceId(deviceId);
        Map resultMap = doPayment(null, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map dohdfcDebitPayment(CheckoutData checkoutData, String lookupId, String iciciSecureCookie, String remoteIp,
            int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();

        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD);
        ppbdo.setPaymentoption(PaymentConstants.HDFCDEBITCARD_PAYMENT_OPTION);
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);

        ppbdo.setUsePartialWalletPayment(partialPayment);
        Map resultMap = doPayment(null, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map doZeroPayment(CheckoutData checkoutData, String lookupId, String iciciSecureCookie, String remoteIp,
            int channel, Boolean partialPayment, Integer appVersion)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {

        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();

        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_ZEROPAY);
        ppbdo.setPaymentoption("");
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);

        ppbdo.setUsePartialWalletPayment(partialPayment);
        Map resultMap = doPayment(null, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        checkoutData.setOrderId(ppbdo.getOrderId());
        return resultMap;
    }

    public Map doWalletPayment(CheckoutData checkoutData, String iciciSecureCookie, String remoteIp, int channel,
            Integer appVersion) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        String lookupId = checkoutData.getLookupId();
        ProductPaymentBusinessDO ppbdo = new ProductPaymentBusinessDO();
        ppbdo.setPaymentoption("");
        ppbdo.setPaymenttype(PaymentConstants.PAYMENT_TYPE_WALLET);
        ppbdo.setLookupID(lookupId);
        ppbdo.setAppVersion(appVersion);
        Map resultMap = doPayment(null, ppbdo, lookupId, remoteIp, iciciSecureCookie, channel);
        setOrderIdFromMerchantOrderId(checkoutData, resultMap);
        return resultMap;
    }

    private void setOrderIdFromMerchantOrderId(CheckoutData checkoutData, Map resultMap) {
        if (resultMap != null && resultMap.get("resultMap") != null && resultMap.get("resultMap") instanceof Map) {
            Map map = (Map) resultMap.get("resultMap");
            checkoutData.setOrderId((String) map.get(PaymentConstants.MERCHANT_ORDER_ID));
        }
    }

    private void checkFraud(ProductPaymentBusinessDO productPaymentBusinessDO, int channel)
            throws FCPaymentPortalException, FCPaymentGatewayException, FraudDetectedException, Exception {
        // ***********************************************//
        // FRAUD DETECTION CODE //
        // ***********************************************//

        // check for potential fraud-attempt

        int userId = userServiceProxy.getLoggedInUser().getUserId();
        String totalAmount = productPaymentBusinessDO.getAmount().toString();
        String lookupId = productPaymentBusinessDO.getLookupID();

        long startTime = System.currentTimeMillis();
        try {
            fd.checkFraud(userId, lookupId, totalAmount, productPaymentBusinessDO.getPaymenttype(),
                    productPaymentBusinessDO.getDeviceId(), channel, productPaymentBusinessDO.getOrderId());
            metricsClient.recordLatency("Payment", "FraudCheck.Latency", System.currentTimeMillis() - startTime);
        } catch (FraudDetectedException e) {
            metricsClient.recordLatency("Payment", "FraudCheck.Latency", System.currentTimeMillis() - startTime);
            throw e;
        }
        // ********************* Rest of the payment flow *********************

        List<String> msgs = PaymentUtil.requestValidation(productPaymentBusinessDO.getPaymenttype());
        if (msgs.size() > 0) {
            throw new InvalidPaymentRequestException(msgs);
        }
    }

    private Map<String, String> doPaymentPortalHandleRequest(ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> requestMap, int channel) {

        int userId = userServiceProxy.getLoggedInUser().getUserId();
        String orderID = productPaymentBusinessDO.getOrderId();

        // ***********************************************//
        // CARD STORAGE CODE //
        // ***********************************************//

        if (requestMap == null) {
            requestMap = new HashMap<String, String>();
        }
        requestMap.put("orderid", productPaymentBusinessDO.getOrderId());
        requestMap.put("channel", FCConstants.channelMasterMap.get(channel));

        try {
            productPaymentBusinessDO
                    .setPaymentRequestCardDetails(PaymentUtil.convertRequestMapToPaymentRequestCardDetails(requestMap));
            processAndAddCard(userId, productPaymentBusinessDO, requestMap, channel);
            Map<String, String> storedCardDetails = getStoredCardDetails(userServiceProxy.getLoggedInUser().getUserId(),
                    requestMap.get(PaymentConstants.CARD_TOKEN));
            populateStoredCardDetails(userId, productPaymentBusinessDO, requestMap, storedCardDetails);
        } catch (RuntimeException e) {
            final String msg = String.format("Error processing card storage for userID=%d, orderID=%s, requestMap=%s",
                    userId, orderID, requestMap);
            logger.error(msg, e);
            throw e;
        }
        return requestMap;
    }

    private PaymentRequestBusinessDO paymentExecution(ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> requestMap, String lookupId, String ipAddress, String iciciSecureCookie, int channel)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        PaymentRequestBusinessDO paymentRequestBusinessDO = getPaymentRequestBusinessDO(productPaymentBusinessDO,
                lookupId, ipAddress, iciciSecureCookie, channel);
        paymentRequestBusinessDO.setRequestMap(requestMap);
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(paymentRequestBusinessDO.getOrderId());
        // Check if request is tampered
        Boolean valid = false;
        valid = validateHashKey(paymentRequestBusinessDO);
        if (valid) {
            Double pgAmountToSet = null;
            Double totalAmountToPay = pricingService.getPayableAmount(cartBusinessDo);

            try {
                PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(paymentRequestBusinessDO.getOrderId());
                if (totalAmountToPay.doubleValue() != paymentPlan.getTotalAmount().getAmount().doubleValue()) {
                    String errorString = "Total amount to pay does not match with payment plan. Expected amount: "
                            + totalAmountToPay.doubleValue() + ". Payment plan : " + paymentPlan;

                    throw new IllegalStateException(errorString);
                }

                pgAmountToSet = paymentPlan.getPgAmount().getAmount().doubleValue();
            } catch (PaymentPlanNotFoundException ppnfe) {
                logger.error("Payment plan not found for order ID: [" + paymentRequestBusinessDO.getOrderId() + "]");

                pgAmountToSet = totalAmountToPay;
            }

            logger.info("UPIDebug: paymentExecution Setting pg amount for order ID : [" + paymentRequestBusinessDO.getOrderId() + "] as "
                    + pgAmountToSet);
            paymentRequestBusinessDO.setAmount(Double.toString(pgAmountToSet));
            // Since we re-set amount (considering freefund removal), hence
            // re-calculating HashKey
            String f_string = paymentRequestBusinessDO.getAmount() + paymentRequestBusinessDO.getOrderId()
                    + "freecharge" + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);
            paymentRequestBusinessDO.setHashKey(PaymentUtil.getHashKey(f_string));

            IPaymentType paymentType = paymentTypeFactory.getPaymentType(paymentRequestBusinessDO.getPaymentType());

            if (paymentType != null) {
                valid = isZeroPayAllowed(paymentRequestBusinessDO, pgAmountToSet);
                if (!valid) {
                    paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
                } else {
                    if (paymentRequestBusinessDO.getRequestMap() != null) {
                        paymentRequestBusinessDO.getRequestMap().put(FCConstants.CHANNEL_KEY, channel);
                    } else {
                        Map prequestMap = new HashMap();
                        paymentRequestBusinessDO.setRequestMap(prequestMap);
                        paymentRequestBusinessDO.getRequestMap().put(FCConstants.CHANNEL_KEY, channel);

                    }
                    valid = paymentType.doPayment(paymentRequestBusinessDO);
                    // Log transactions done with a saved card
                    try {
                        if (paymentRequestBusinessDO.getRequestMap().containsKey("is_saved_card_txn")) {
                            if ("true".equals(paymentRequestBusinessDO.getRequestMap().get("is_saved_card_txn"))) {
                                savedCardTransactionsDAO.logTransactionWithSavedCard(
                                        paymentRequestBusinessDO.getOrderId(), paymentRequestBusinessDO.getMtxnId());
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Exception logging details for saved card Transaction", e);
                    }
                    if (paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_ANDROID_APP
                            && paymentRequestBusinessDO.getAppVersion() != null
                            && paymentRequestBusinessDO.getAppVersion() > 33) {
                        paymentRequestBusinessDO.getResultMap().put(PaymentConstants.ORDER_ID_KEY,
                                paymentRequestBusinessDO.getOrderId());
                    }

                }
            }

            if (!valid) {
                paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
            }
        } else {
            if (paymentRequestBusinessDO.getResultMap() == null) {
                paymentRequestBusinessDO.setResultMap(new HashMap());
            }
            paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
        }
        return paymentRequestBusinessDO;
    }

    private PaymentRequestBusinessDO doPaymentRequestDelegate(ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> requestMap, String lookupId, String ipAddress, String iciciSecureCookie, int channel)
                    throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        PaymentRequestBusinessDO paymentRequestBusinessDO = getPaymentRequestBusinessDO(productPaymentBusinessDO,
                lookupId, ipAddress, iciciSecureCookie, channel);
        paymentRequestBusinessDO.setRequestMap(requestMap);
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(paymentRequestBusinessDO.getOrderId());
        // Check if request is tampered
        Boolean valid = false;
        valid = validateHashKey(paymentRequestBusinessDO);
        if (valid) {
            Double pgAmountToSet = null;
            Double totalAmountToPay = pricingService.getPayableAmount(cartBusinessDo);

            try {
                PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(paymentRequestBusinessDO.getOrderId());
                if (totalAmountToPay.doubleValue() != paymentPlan.getTotalAmount().getAmount().doubleValue()) {
                    String errorString = "Total amount to pay does not match with payment plan. Expected amount: "
                            + totalAmountToPay + ". Payment plan : " + paymentPlan;

                    throw new IllegalStateException(errorString);
                }

                pgAmountToSet = paymentPlan.getPgAmount().getAmount().doubleValue();
            } catch (PaymentPlanNotFoundException ppnfe) {
                logger.error("Payment plan not found for order ID: [" + paymentRequestBusinessDO.getOrderId() + "]");

                pgAmountToSet = totalAmountToPay;
            }

            logger.info("Setting pg amount for order ID : [" + paymentRequestBusinessDO.getOrderId() + "] as "
                    + pgAmountToSet);
            paymentRequestBusinessDO.setAmount(Double.toString(pgAmountToSet));
            // Since we re-set amount (considering freefund removal), hence
            // re-calculating HashKey
            String f_string = paymentRequestBusinessDO.getAmount() + paymentRequestBusinessDO.getOrderId()
                    + "freecharge" + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);
            paymentRequestBusinessDO.setHashKey(PaymentUtil.getHashKey(f_string));

            IPaymentType paymentType = paymentTypeFactory.getPaymentType(paymentRequestBusinessDO.getPaymentType());

            if (paymentType != null) {
                valid = isZeroPayAllowed(paymentRequestBusinessDO, pgAmountToSet);
                if (!valid) {
                    paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
                } else {
                    if (paymentRequestBusinessDO.getRequestMap() != null) {
                        paymentRequestBusinessDO.getRequestMap().put(FCConstants.CHANNEL_KEY, channel);
                    } else {
                        Map prequestMap = new HashMap();
                        paymentRequestBusinessDO.setRequestMap(prequestMap);
                        paymentRequestBusinessDO.getRequestMap().put(FCConstants.CHANNEL_KEY, channel);

                    }
                    valid = paymentType.doPayment(paymentRequestBusinessDO);
                }
            }

            if (!valid) {
                paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
            }
        } else {
            if (paymentRequestBusinessDO.getResultMap() == null) {
                paymentRequestBusinessDO.setResultMap(new HashMap());
            }
            paymentRequestBusinessDO.getResultMap().put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
        }
        return paymentRequestBusinessDO;
    }

    private boolean validateHashKey(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        String hashKey = paymentRequestBusinessDO.getHashKey();

        // hashkey generation format is amount+orderid+"freecharge"+key
        String f_string = paymentRequestBusinessDO.getAmount() + paymentRequestBusinessDO.getOrderId() + "freecharge"
                + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);

        if (StringUtils.isNotBlank(hashKey)) {

            String outputString = PaymentUtil.getHashKey(f_string);

            if (!StringUtils.equalsIgnoreCase(hashKey, outputString)) {
                return false;
            }
        }
        return true;
    }

    private Boolean isZeroPayAllowed(PaymentRequestBusinessDO paymentRequestBusinessDO, Double amount) {
        if (paymentRequestBusinessDO.getResultMap() == null) {
            paymentRequestBusinessDO.setResultMap(new HashMap());
        }

        if (PaymentConstants.PAYMENT_TYPE_ZEROPAY.equals(paymentRequestBusinessDO.getPaymentType() + "")
                && amount != 0.0) {
            return false;
        }
        return true;
    }

    private void doProductPaymentPreProcessing(ProductPaymentBusinessDO productPaymentBusinessDO, int channelId)
            throws InvalidPaymentRequestException, FCPaymentPortalException, FCPaymentGatewayException,
            FraudDetectedException, Exception {
    	String orderId = null;
    	boolean isRetry = false;
    	PaymentPlan paymentPlan = null;
        metricInvalidPayTypeOption(productPaymentBusinessDO, channelId);
        String lookupId = productPaymentBusinessDO.getLookupID();
        /*
         * We auto-detect payment option for credit card cards.
         */
        if (isPaymentTypeCreditCard(productPaymentBusinessDO) && !productPaymentBusinessDO.getPaymentoption()
                .equalsIgnoreCase(PaymentConstants.AMEX_PAYMENT_OPTION)) {
            String cardType = PaymentUtil
                    .getCreditCardType(productPaymentBusinessDO.getPaymentRequestCardDetails().getCardNumber());
            productPaymentBusinessDO.setPaymentoption(cardType);
        }

        CartBusinessDo cartBusinessDo = cartProcessingForPayment(productPaymentBusinessDO, channelId);
        if (cartBusinessDo == null) {
            throw new InvalidPaymentRequestException(Arrays.asList(
                    "No cart present for payment processing of lookupid : " + lookupId));
        }

        // Verifying user id on cart object. If user id is not present, updating
        // cart with userid
        if (cartBusinessDo.getUserId() == null) {
            PersonalDetailsBusinessDO personalDetailsBusinessDO = new PersonalDetailsBusinessDO();
            personalDetailsBusinessDO.setLookupID(productPaymentBusinessDO.getLookupID());
            boolean isUserIdUpdated = personalDetailsService.updateCartItems(personalDetailsBusinessDO,
                    productPaymentBusinessDO.getUserId());
            if (!isUserIdUpdated) {
                throw new InvalidPaymentRequestException(
                        Arrays.asList("User id not present on cart : " + lookupId));
            }
        }
        
		FreechargeSession fs = FreechargeContextDirectory.get()
				.getFreechargeSession();
		String rechargeType = FCSessionUtil.getRechargeType(lookupId, fs);
		if (rechargeType == null) {
			throw new DataNotFoundInLocalCacheException(
					"Data not found in local cache exception");
		}
		Integer productId = (Integer)FCConstants.productMasterMapReverse.get(rechargeType);
		Integer affiliateId = getAffiliateId(productPaymentBusinessDO);
		orderId = getOrderIdIfAlreadyCreated(productPaymentBusinessDO,
				channelId, productId);
		if (orderId == null) {
			orderId = userAuthService.generateOrderId(fs.getUuid(),
					productPaymentBusinessDO, affiliateId, productId,
					channelId, productPaymentBusinessDO.getAppVersion());
		} else {
			isRetry = true;
		}
		productPaymentBusinessDO.setOrderId(orderId);
		productPaymentBusinessDO.setMtxnId(getPaymentGatewayOrderId(orderId));
		paymentPlan = validateAndSetPaymentPlan(cartBusinessDo, productPaymentBusinessDO);
		omsClient.processOndeckOrder(orderId, channelId, lookupId, isRetry, paymentPlan, productPaymentBusinessDO);
        // hashkey generation format is amount+orderid+"freecharge"+key
        String f_string = productPaymentBusinessDO.getAmount() + orderId + "freecharge"
                + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);
        String h_key = PaymentUtil.getHashKey(f_string);
        productPaymentBusinessDO.setBreaker(h_key);
        // If a user has opted out of card storage, then the user should not
        // have the card storage auto selected the next time he/she lands up on
        // the
        // payment page.
        updateUserCardStoragePreference(productPaymentBusinessDO);
    }

    private void metricInvalidPayTypeOption(ProductPaymentBusinessDO productPaymentBusinessDO, Integer channel) {
        if (!PaymentUtil.isValidPaymentTypeOptionMap(Integer.parseInt(productPaymentBusinessDO.getPaymenttype()),
                productPaymentBusinessDO.getPaymentoption())) {
            logger.error("Invalid Payment Type to Payment Option Map " + productPaymentBusinessDO.getPaymenttype() + ":"
                    + productPaymentBusinessDO.getPaymentoption());
            metricsClient.recordEvent("Payment",
                    "Attempt." + channel + "." + productPaymentBusinessDO.getAppVersion() + "."
                            + productPaymentBusinessDO.getPaymenttype() + "."
                            + productPaymentBusinessDO.getPaymentoption(),
                    "Invalid");
        }
    }

    private String getOrderIdIfAlreadyCreated(ProductPaymentBusinessDO productPaymentBusinessDO, int channelId, Integer productId) throws FraudDetectedException {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Integer affiliateId = getAffiliateId(productPaymentBusinessDO);
        String lookupId = productPaymentBusinessDO.getLookupID();
        // Added this check to double check if the 'type' from session is valid
        if (ProductName.fromProductId(productId) == ProductName.WalletCash) {
            metricsClient.recordEvent("Payment", "AddCash", "AddCashBlock");
            throw new FraudDetectedException("Add cash to old wallet is not supported.");
        }
        String orderId = orderIdWriteDAO.getOrderIdByAllParams(fs.getUuid(), lookupId, affiliateId, productId,
                channelId);
        productPaymentBusinessDO.setProductId(String.valueOf(productId));
        return orderId;
    }

	private Integer getAffiliateId(ProductPaymentBusinessDO productPaymentBusinessDO) {
		Integer affiliateId = FCConstants.DEFAULT_AFFILIATE_ID;
		logger.info("affiliateId received is" + productPaymentBusinessDO.getAffiliateid());
		try {
			if (productPaymentBusinessDO != null && productPaymentBusinessDO.getAffiliateid() != null
					&& FCConstants.afflicateMasterMap
							.containsKey(Integer.valueOf(productPaymentBusinessDO.getAffiliateid()))) {
				affiliateId = Integer.valueOf(productPaymentBusinessDO.getAffiliateid());
			}
		} catch (Exception e) {
			logger.info("Error getting affiliateId ");
		}
		return affiliateId;
	}

	private void validateAndBlockOneCheckAddCash() throws FraudDetectedException {
        String email = FCSessionUtil.getLoggedInEmailId();
        if (null != oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email)) {
            throw new FraudDetectedException(
                    "OneCheck user trying to do add cash via freecharge payment system - " + email);
        }
    }

    private CartBusinessDo cartProcessingForPayment(ProductPaymentBusinessDO productPaymentBusinessDO, int channel)
            throws InvalidPaymentRequestException {
        Cart cartObj = cartService.getCartBylookUp(productPaymentBusinessDO.getLookupID());
        if (cartObj == null) {
            logger.info("Found null cart for lookup id : " + productPaymentBusinessDO.getLookupID());
            metricsClient.recordEvent("Payment",
                    "Attempt." + productPaymentBusinessDO.getPaymenttype() + "."
                            + FCConstants.channelMasterMap.get(channel) + "."
                            + productPaymentBusinessDO.getAppVersion(),
                    "NullCart");
            throw new InvalidPaymentRequestException(Arrays.asList(
                    "No cart present for payment processing of lookupid : " + productPaymentBusinessDO.getLookupID()));
        }

        /* Modify the cart for MasterCard Item */
        if (!StringUtils.isEmpty(productPaymentBusinessDO.getPaymentoption())) {
            if (productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("MC")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DMC")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DCME")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DSME")) {
                CartItems cartItems = new CartItems();
                cartItems.setCart(cartObj);
                cartItems.setFkItemId(null);
                cartItems.setEntityId("");
                cartItems.setFlProductMasterId(7);
                cartItems.setPrice(0.0);
                cartItems.setDisplayLabel("master card");
                cartItems.setQuantity(1);
                Date currentDate = new Date();
                cartItems.setAddedOn(new Timestamp(currentDate.getTime()));
                cartItems.setIsDeleted(false);
                cartService.saveCartItems(cartItems);
            }
        } else {
            List<CartItems> cartItems = cartService.getCartItemsByCartId(cartObj.getCartId());
            if (cartItems != null && !cartItems.isEmpty()) {
                for (CartItems cartItem : cartItems) {
                    if (cartItem.getFlProductMasterId().intValue() == 7) {
                        cartService.removeCartItem(cartItem.getCartItemsId());
                    }
                }
            }
        }

        CartBusinessDo cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());
        return cartBusinessDo;
    }

    private void doProductPaymentRequestDelegate(ProductPaymentBusinessDO productPaymentBusinessDO, int channel)
            throws InvalidPaymentRequestException, FCPaymentPortalException, FCPaymentGatewayException,
            FraudDetectedException, Exception {
        logger.info(String.format("OrderID: %s, payment option: %s", productPaymentBusinessDO.getOrderId(),
                productPaymentBusinessDO.getPaymentoption()));
        metricInvalidPayTypeOption(productPaymentBusinessDO, channel);

        String lookupID = productPaymentBusinessDO.getLookupID();
        Cart cartObj = cartService.getCartBylookUp(lookupID);
        if (cartObj == null) {
            logger.info("Found null cart for lookup id : " + productPaymentBusinessDO.getLookupID());
            metricsClient.recordEvent("Payment",
                    "Attempt." + productPaymentBusinessDO.getPaymenttype() + "."
                            + FCConstants.channelMasterMap.get(channel) + "."
                            + productPaymentBusinessDO.getAppVersion(),
                    "NullCart");
            throw new InvalidPaymentRequestException(Arrays.asList(
                    "No cart present for payment processing of lookupid : " + productPaymentBusinessDO.getLookupID()));
        }

        // Verifying user id on cart. If not present, setting it
        if (cartObj.getFkUserId() == null) {
            PersonalDetailsBusinessDO personalDetailsBusinessDO = new PersonalDetailsBusinessDO();
            personalDetailsBusinessDO.setLookupID(lookupID);
            boolean isUserIdUpdated = personalDetailsService.updateCartItems(personalDetailsBusinessDO,
                    userServiceProxy.getLoggedInUser().getUserId());
            if (!isUserIdUpdated) {
                throw new InvalidPaymentRequestException(
                        Arrays.asList("No user id present on cart : " + productPaymentBusinessDO.getLookupID()));
            }
        }

        /*
         * We auto-detect payment option for credit card cards.
         */

        if (isPaymentTypeCreditCard(productPaymentBusinessDO) && !productPaymentBusinessDO.getPaymentoption()
                .equalsIgnoreCase(PaymentConstants.AMEX_PAYMENT_OPTION)) {
            String cardType = PaymentUtil.getCreditCardType(productPaymentBusinessDO.getCcNumber());
            productPaymentBusinessDO.setPaymentoption(cardType);
        }

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(productPaymentBusinessDO.getLookupID(), fs);
        fs.getSessionData().put(PaymentConstants.PAYMENT_TYPE, productPaymentBusinessDO.getPaymenttype());
        fs.getSessionData().put(PaymentConstants.PAYMENT_OPTION, productPaymentBusinessDO.getPaymentoption());
        Integer productId = (Integer)FCConstants.productMasterMapReverse.get(type);
        fs.getSessionData().put(PaymentConstants.NEW_PAYMENTS_FLOW, "false");
        fs.getSessionData().put(PaymentConstants.IS_MOBILE_WEB, Boolean.FALSE);
        CartBusinessDo cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());
        if (cartBusinessDo == null) {
            throw new InvalidPaymentRequestException(Arrays.asList(
                    "No cart present for payment processing of lookupid : " + productPaymentBusinessDO.getLookupID()));
        }
        // Added this check to double check if the 'type' from session is valid
        if (ProductName.fromProductId(productId) == ProductName.WalletCash) {
            metricsClient.recordEvent("Payment", "AddCash", "AddCashBlock");
            throw new FraudDetectedException("Add cash to old wallet is not supported.");
        }
        String orderId = userAuthService.generateOrderId(fs.getUuid(), productPaymentBusinessDO,
                FCConstants.DEFAULT_AFFILIATE_ID, productId, channel, productPaymentBusinessDO.getAppVersion());

        /* Modify the cart for MasterCard Item */
        if (!StringUtils.isEmpty(productPaymentBusinessDO.getPaymentoption())) {
            if (productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("MC")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DMC")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DCME")
                    || productPaymentBusinessDO.getPaymentoption().equalsIgnoreCase("DSME")) {
                CartItems cartItems = new CartItems();
                cartItems.setCart(cartObj);
                cartItems.setFkItemId(null);
                cartItems.setEntityId("");
                cartItems.setFlProductMasterId(7);
                cartItems.setPrice(0.0);
                cartItems.setDisplayLabel("master card");
                cartItems.setQuantity(1);
                Date currentDate = new Date();
                cartItems.setAddedOn(new Timestamp(currentDate.getTime()));
                cartItems.setIsDeleted(false);
                cartService.saveCartItems(cartItems);
            }
        } else {
            List<CartItems> cartItems = cartService.getCartItemsByCartId(cartObj.getCartId());
            if (cartItems != null && !cartItems.isEmpty()) {
                for (CartItems cartItem : cartItems) {
                    if (cartItem.getFlProductMasterId().intValue() == 7) {
                        cartService.removeCartItem(cartItem.getCartItemsId());
                    }
                }
            }
        }

        cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());

        productPaymentBusinessDO.setOrderId(orderId);
        productPaymentBusinessDO.setMtxnId((getPaymentGatewayOrderId(orderId)));
        validateAndSetPaymentPlan(cartBusinessDo, productPaymentBusinessDO);

        // hashkey generation format is amount+orderid+"freecharge"+key
        String f_string = productPaymentBusinessDO.getAmount() + orderId + "freecharge"
                + properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);
        String h_key = PaymentUtil.getHashKey(f_string);

        productPaymentBusinessDO.setBreaker(h_key);

        // If a user has opted out of card storage, then the user should not
        // have the card storage auto selected the next time he/she lands up on
        // the
        // payment page.
        updateUserCardStoragePreference(productPaymentBusinessDO);
    }

    private void processAndAddCard(int userId, ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> requestMap, int channel) {
        if (isNonCardPaymentType(productPaymentBusinessDO)) {
            return;
        }
        if (appConfigService.isCardStorageEnabled()) {
            cardStorage.addCard(productPaymentBusinessDO.getPaymentRequestCardDetails(), userId,
                    String.valueOf(channel), productPaymentBusinessDO.getPaymentoption());
        }
    }

    private void validateAddCashPaymentCart(CartBusinessDo cartBusinessDo) {
        List<CartItemsBusinessDO> cartItems = cartBusinessDo.getCartItems();
        boolean isAddCashInCart = false;
        for (CartItemsBusinessDO cartItemsBusinessDO : cartItems) {
            if (ProductName.fromProductId(cartItemsBusinessDO.getProductMasterId()) == ProductName.WalletCash
                    && !cartItemsBusinessDO.getDeleted()) {
                isAddCashInCart = true;
                break;
            }
        }
        if (!isAddCashInCart) {
            String errorString = "Add cash item not found in cart for an add cash product type. Cart : "
                    + cartBusinessDo;
            metricsClient.recordEvent("Payment", "Attempt.AddCash", "NoAddCashInCart");
            throw new RuntimeException(errorString);
        }
    }

    private void validateAddCashWalletPayment(String paymentType) {
        if (paymentType != null && paymentType.equals(PaymentConstants.PAYMENT_TYPE_WALLET)) {
            String errorString = "Tried AddCash with wallet payment.";
            metricsClient.recordEvent("Payment", "Attempt.AddCash", "WalletPay");
            throw new RuntimeException(errorString);
        }
    }

    private boolean isNonCardPaymentType(ProductPaymentBusinessDO productPaymentBusinessDO) {
        if (!(PaymentUtil.isCreditCard(productPaymentBusinessDO.getPaymenttype())
                || PaymentUtil.isDebitCard(productPaymentBusinessDO.getPaymenttype()))) {
            return true;
        }

        return false;
    }

    /**
     * Return month as two-digit numeric string padded with 0, or null if
     * invalid.
     * 
     * @param month
     * @return
     */
    private String fixMonth(String month) {
        if (month != null) {
            if (month.length() == 1 && Character.isDigit(month.charAt(0))) {
                return "0" + month;
            } else if (month.length() == 2 && Character.isDigit(month.charAt(0))
                    && Character.isDigit(month.charAt(1))) {
                return month;
            }
            return null;
        }
        return null;
    }

    private boolean isPaymentTypeCreditCard(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return PaymentConstants.PAYMENT_TYPE_CREDITCARD.equals(productPaymentBusinessDO.getPaymenttype());
    }

    /**
     * This method does data validation and creates payment plan for the order.
     * Validation is carried out to ensure that payable amount is equal to sum
     * of all amounts in the payment plan. Also validated is amount in wallet <
     * payable amount.
     * 
     * @param cartBusinessDo
     * @param paymentBusinessDO
     */
    private PaymentPlan validateAndSetPaymentPlan(CartBusinessDo cartBusinessDo, ProductPaymentBusinessDO paymentBusinessDO) {
        logger.info(
                "Cart items for lookup id : " + cartBusinessDo.getLookupID() + " is " + cartBusinessDo.getCartItems());

        if (paymentBusinessDO.getUsePartialWalletPayment() == null) {
            paymentBusinessDO.setUsePartialWalletPayment(Boolean.FALSE);
        }
        Double expectedPayAmount = pricingService.getPayableAmount(cartBusinessDo);
        PaymentPlan paymentPlan = new PaymentPlan();
        paymentPlan.setPlanReferenceId(paymentBusinessDO.getOrderId());
        paymentPlan.setMtxnId(paymentBusinessDO.getMtxnId());

        if (paymentBusinessDO.getUsePartialWalletPayment()) {
            logger.info("Partial payment is enabled for Order ID : [" + paymentBusinessDO.getOrderId() + "]");
            metricsClient.recordEvent("Payment", "Partialpayment", "Enabled");

            Integer userId = cartBusinessDo.getUserId();
            Amount walletBalance = walletWrapper.getDebitableBalance(userId);

            int walletBalancePayAmountCompare = walletBalance.getAmount().compareTo(new BigDecimal(expectedPayAmount));

            if (walletBalancePayAmountCompare > 0 || walletBalancePayAmountCompare == 0) {
                String errorString = "Wallet balance is greater than or equal to pay amount for partial payment. WalletBalance : ["
                        + walletBalance.getAmount() + "]. PayAmount : [" + expectedPayAmount + "]";
                logger.error(errorString);
                throw new RuntimeException(errorString);
            }

            Amount walletAmount = walletBalance;
            Amount pgAmount = new Amount((new BigDecimal(expectedPayAmount)).subtract(walletAmount.getAmount()));

            paymentPlan.setPgAmount(pgAmount);
            paymentPlan.setWalletAmount(walletAmount);
            logger.info("Wallet amount : "+walletAmount);
            logger.info("Partial payment is enabled for Order ID : [" + paymentBusinessDO.getOrderId() + "] Expected Amount : ["+expectedPayAmount+"] PaymentPlan : ["+ paymentPlan + "]");
        } else {
            logger.info("Partial payment is disabled for Order ID : [" + paymentBusinessDO.getOrderId() + "]");

            paymentPlan.setPgAmount(new Amount(expectedPayAmount));
            paymentPlan.setWalletAmount(Amount.ZERO);
        }

        logger.info("About to create payment plan : " + paymentPlan);

        paymentPlanService.createPaymentPlan(paymentPlan);

        if (paymentPlan.getPgAmount().getAmount().equals(BigDecimal.ZERO)) {
            logger.info("PG Amount is zero. Setting amount as wallet amount for order ID ["
                    + paymentBusinessDO.getOrderId() + "]");
            paymentBusinessDO.setAmount(paymentPlan.getWalletAmount().getAmount().doubleValue());
        } else {
            logger.info("PG Amount is non-zero. Setting amount as pg amount for order ID ["
                    + paymentBusinessDO.getOrderId() + "]");
            paymentBusinessDO.setAmount(paymentPlan.getPgAmount().getAmount().doubleValue());
        }
        // This is to validate invalid decimal amounts.
        // validateAmount(paymentBusinessDO.getAmount(),
        // paymentBusinessDO.getOrderId());
        return paymentPlan;
    }

    private void validateAmount(Double amount, String orderId) {
        if ((amount - amount.intValue()) != 0.0) {
            String errorString = "Invalid amount " + amount + " for order id: " + orderId;
            throw new RuntimeException(errorString);
        }
    }

    private boolean checkWhetherUserOptedOutOfCardStorage(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return (isPaymentTypeCreditCard(productPaymentBusinessDO))
                || (isPaymentTypeDebitCard(productPaymentBusinessDO));
    }

    private boolean isPaymentTypeDebitCard(ProductPaymentBusinessDO productPaymentBusinessDO) {
        return PaymentConstants.PAYMENT_TYPE_DEBITCARD.equals(productPaymentBusinessDO.getPaymenttype());
    }

    private void updateUserCardStoragePreference(ProductPaymentBusinessDO productPaymentBusinessDO) {
        Integer userId = (Integer) FreechargeContextDirectory.get().getFreechargeSession().getSessionData()
                .get(WebConstants.SESSION_USER_USERID);
        if (checkWhetherUserOptedOutOfCardStorage(productPaymentBusinessDO)
                && !cardUserPreferenceStorageService.isUserPreferencePresent(userId)) {
            if (productPaymentBusinessDO.getUnCheckedStoreCard() != null
                    && productPaymentBusinessDO.getUnCheckedStoreCard()) {
                UserCardStorageChoicePreference userCardStorageChoicePreference = new UserCardStorageChoicePreference();
                userCardStorageChoicePreference.setUserId(userId);
                userCardStorageChoicePreference.setPreference(UserCardStorageChoicePreference.Preference.DO_NOT_CHOOSE);
                cardStorageDAO.insertUserCardStorageChoicePreference(userCardStorageChoicePreference);
            }
        }
    }

    public PaymentRequestBusinessDO getPaymentRequestBusinessDO(ProductPaymentBusinessDO productPaymentBusinessDO,
            String lookupId, String ipAddress, String iciciSecureCookie, int channel) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(productPaymentBusinessDO.getLookupID(), fs);
        Integer productId = (Integer)FCConstants.productMasterMapReverse.get(type);
        CartBusinessDo cartBusinessDo = cartService.getCart(productPaymentBusinessDO.getLookupID());
        PaymentRequestBusinessDO prbdo = new PaymentRequestBusinessDO();
        prbdo.setAmount(String.valueOf(productPaymentBusinessDO.getAmount()));
        prbdo.setPaymentOption(productPaymentBusinessDO.getPaymentoption());
        prbdo.setUpiInfo(productPaymentBusinessDO.getUpiInfo());
        prbdo.setPaymentCurrency("INR");
        prbdo.setErrorUrl(properties.getProperty("application.domain")
                + properties.getProperty("application.payment.uri.handle.error") + "?lid="
                + productPaymentBusinessDO.getLookupID());
        prbdo.setHashKey(productPaymentBusinessDO.getBreaker());
        prbdo.setPaymentStatus("false");
        prbdo.setPaymentType(Integer.parseInt(productPaymentBusinessDO.getPaymenttype()));
        prbdo.setPaymentGatewayOverride("");
        ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDo);
        if (ProductName.WalletCash == primaryProduct) {
            prbdo.setSuccessUrl(properties.getProperty("application.domain")
                    + properties.getProperty("application.payment.uri.wallet.fulfillment"));
        } else {
            prbdo.setSuccessUrl(properties.getProperty("application.domain")
                    + properties.getProperty("application.payment.uri.handle.success"));
        }
        prbdo.setAppVersion(productPaymentBusinessDO.getAppVersion());
        prbdo.setOrderId(productPaymentBusinessDO.getOrderId());
        prbdo.setAffiliateId(FCConstants.DEFAULT_AFFILIATE_ID);
        prbdo.setProductId(productId);
        prbdo.setOperatorId("1");
        prbdo.setRemoteIp(ipAddress);
        prbdo.setLookupID(productPaymentBusinessDO.getLookupID());
        prbdo.setPaymentRequestCardDetails(productPaymentBusinessDO.getPaymentRequestCardDetails());
        prbdo.setChannel(channel);
        prbdo.setUser(productPaymentBusinessDO.getUser());
        prbdo.setTransactionTypeStatus(productPaymentBusinessDO.getTransactionTypeStatus());
        prbdo.setMtxnId(productPaymentBusinessDO.getMtxnId());
        return prbdo;
    }

    private String getCardType(PaymentRequestCardDetails cardDetails, String paymentOption) {
        String cardType = null;
        if (cardDetails.isDebitCard()) {
            if (paymentOption.toCharArray()[1] == 'V') {
                cardType = "V";
            } else {
                cardType = "M";
            }
        } else if (cardDetails.getCardNumber() != null && !cardDetails.getCardNumber().isEmpty()) {
            String ccCardType = PaymentUtil.getCreditCardType(cardDetails.getCardNumber());
            if (ccCardType.equals(PaymentConstants.VISA_CC)) {
                cardType = "V";
            } else {
                cardType = "M";
            }
        }
        return cardType;
    }

    private void populateStoredCardDetails(int userId, ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> requestMap, Map<String, String> storedCard) {
        if (isNonCardPaymentType(productPaymentBusinessDO)) {
            return;
        }

        final String orderID = productPaymentBusinessDO.getOrderId();
        // detect whether Credit card token exists
        final String cardToken = requestMap.get(PaymentConstants.CARD_TOKEN);
        boolean isDebitCard = false;
        final String UNDEFINED = "undefined";

        if (!(StringUtils.isEmpty(cardToken) || UNDEFINED.equals(cardToken))) {
            // detected "pay-using-saved-credit-card" condition, so we
            // populate required fields using Card storage get() call
            try {

                // TODO : Cache the card reference against order id in redis for
                // 20 mins
                paymentOptionCache.setCardReferenceByOrderId(orderID, storedCard.get(CardStorageKey.CARD_REFERENCE));
                requestMap.put(PaymentConstants.CARD_NUM_KEY, storedCard.get(CardStorageKey.CARD_NUMBER));
                requestMap.put(PaymentConstants.CARD_EXPYEAR_KEY, storedCard.get(CardStorageKey.CARD_EXPIRY_YEAR));
                requestMap.put(PaymentConstants.CARD_EXPMON_KEY,
                        PaymentUtil.getExpMonthInReqFormat(storedCard.get(CardStorageKey.CARD_EXPIRY_MONTH)));
                requestMap.put(PaymentConstants.CARD_HOLDER_NAME_KEY, storedCard.get(CardStorageKey.NAME_ON_CARD));

                if (PaymentUtil.isCreditCard(productPaymentBusinessDO.getPaymenttype())) {
                    productPaymentBusinessDO.setPaymentoption(
                            PaymentUtil.getCreditCardType(storedCard.get(CardStorageKey.CARD_NUMBER)));
                    isDebitCard = false;
                } else if (PaymentUtil.isDebitCard(productPaymentBusinessDO.getPaymenttype())) {
                    isDebitCard = true;
                    final String cardBrand = storedCard.get(CardStorageKey.CARD_BRAND);

                    if (StringUtils.isNotBlank(cardBrand)) {
                        switch (cardBrand) {
                            case "VISA":
                                productPaymentBusinessDO.setPaymentoption(PaymentConstants.VISA_DC);
                                break;

                            case "MASTERCARD":
                                productPaymentBusinessDO.setPaymentoption(PaymentConstants.MASTER_DC);
                                break;

                            case "RUPAY":
                                productPaymentBusinessDO.setPaymentoption(PaymentConstants.RUPAY_DC);
                                break;

                            case "MAESTRO":
                                final String cardIssuer = storedCard.get(CardStorageKey.CARD_ISSUER);

                                if (StringUtils.equals("SBI", cardIssuer)) {
                                    productPaymentBusinessDO.setPaymentoption(PaymentConstants.MAESTRO_SBI);
                                } else {
                                    productPaymentBusinessDO.setPaymentoption(PaymentConstants.MAESTRO_CITI);
                                }
                                break;
                            default:
                                logger.error(
                                        "UNKNOWN Card Brand. Unable to determine payment option from stored card for order id: "
                                                + productPaymentBusinessDO.getOrderId());
                                metricsClient.recordEvent("Payment",
                                        "Attempt.StoredCard." + productPaymentBusinessDO.getPaymenttype(),
                                        "invalidCardBrand");
                                break;
                        }
                    }
                    if (FCUtil.isEmpty(productPaymentBusinessDO.getPaymentoption())) {
                        metricsClient.recordEvent("Payment",
                                "Attempt.StoredCard." + productPaymentBusinessDO.getPaymenttype(),
                                "invalidDebitCardBrand");
                        throw new RuntimeException("Unable to determine payment option for stored card for order : "
                                + productPaymentBusinessDO.getOrderId());
                    }
                }
                productPaymentBusinessDO.getPaymentRequestCardDetails().setDebitCard(isDebitCard);
                productPaymentBusinessDO.getPaymentRequestCardDetails()
                        .setCardHolderName(storedCard.get(CardStorageKey.NAME_ON_CARD));
                productPaymentBusinessDO.getPaymentRequestCardDetails()
                        .setCardNumber(storedCard.get(CardStorageKey.CARD_NUMBER));
                productPaymentBusinessDO.getPaymentRequestCardDetails()
                        .setCardExpYear(storedCard.get(CardStorageKey.CARD_EXPIRY_YEAR));
                productPaymentBusinessDO.getPaymentRequestCardDetails().setCardExpMonth(
                        PaymentUtil.getExpMonthInReqFormat(storedCard.get(CardStorageKey.CARD_EXPIRY_MONTH)));
                productPaymentBusinessDO.getPaymentRequestCardDetails()
                        .setCardType(PaymentUtil.getCardTypeFromCard(storedCard.get(CardStorageKey.CARD_NUMBER),
                                isDebitCard, productPaymentBusinessDO.getPaymentoption()));
            } catch (RuntimeException e) {
                logger.error("Error fetching credit card details for orderID=" + orderID, e);
            }
        }

        /*
         * JavaScript variables, when coerced as string, have this nasty habit
         * of turning into "undefined". So, here we detect such occurrences and
         * remove the field as if it was never passed.
         */

        // replace undefined CVV with empty ones
        final String cardCvv = requestMap.get("cardCvv");
        if (UNDEFINED.equals(cardCvv) || "".equals(cardCvv)) {
            requestMap.remove("cardCvv");
        }
        // fix expiry month
        final String cardExpMonth = fixMonth(("" + requestMap.remove("cardExpMonth")).trim());
        if (cardExpMonth != null) {
            requestMap.put("cardExpMonth", cardExpMonth);
        }

        // remove extra fields -- see ProductPaymentWebDO.getParams()
        // because they interfere with PayU's payment flow
        for (String each : extraFields) {
            requestMap.remove(each);
        }
    }

    private Map<String, String> getStoredCardDetails(int userId, String cardToken) {
        final String UNDEFINED = "undefined";

        if (!(StringUtils.isEmpty(cardToken) || UNDEFINED.equals(cardToken))) {
            Map<String, String> storedCard = cardStorageService.get(userId, cardToken, null);
            return storedCard;
        }
        return null;
    }

    private void validateCardDetails(ProductPaymentBusinessDO productPaymentBusinessDO,
            Map<String, String> storedCardDetails, boolean saveCard) throws CardValidationException {
        String cardNumber = null;
        String cardExpiryMonth = null;
        String cardExpiryYear = null;
        String cardCVV = productPaymentBusinessDO.getPaymentRequestCardDetails().getCardCvv();

        if (storedCardDetails != null) {
            cardNumber = storedCardDetails.get(CardStorageKey.CARD_NUMBER);
            cardExpiryMonth = storedCardDetails.get(CardStorageKey.CARD_EXPIRY_MONTH);
            cardExpiryYear = storedCardDetails.get(CardStorageKey.CARD_EXPIRY_YEAR);
        } else {
            cardNumber = productPaymentBusinessDO.getPaymentRequestCardDetails().getCardNumber();
            cardExpiryMonth = productPaymentBusinessDO.getPaymentRequestCardDetails().getCardExpMonth();
            cardExpiryYear = productPaymentBusinessDO.getPaymentRequestCardDetails().getCardExpYear();
        }

        if (StringUtils.isEmpty(cardNumber)) {
            return;
        }

        CardValidationUtil.validateCardDetails(cardNumber, cardExpiryMonth, cardExpiryYear, cardCVV);

        if (saveCard && !StringUtils.isEmpty(productPaymentBusinessDO.getPaymentRequestCardDetails().getCardNumber())) {
            saveCard(productPaymentBusinessDO);
        }
        Map<String, Object> cardStatus = cardStatusHandler.isCardValid(cardNumber);
        if (cardStatus != null && cardStatus.containsKey("status") && ((String) cardStatus.get("status")).equals("0")) {
            String mssg = (String) cardStatus.get("mssg");
            if (StringUtils.isEmpty(mssg)) {
                return;
            }
            if (mssg.equals("Non-domestic card")) {
                throw new CardValidationException(PaymentConstants.CARD_VALIDATION_NON_DEOMESTIC_CARD);
            } else if (mssg.equals("Hotlisted card")) {
                throw new CardValidationException(PaymentConstants.CARD_VALIDATION_HOTLISTED_CARD);
            }
        }

    }

    private boolean luhnCheckValidated(String cardNumber) {
        if (StringUtils.isEmpty(cardNumber)) {
            return true;
        }
        int sumOfEvenDigits = 0, sumOfOddDigits = 0;
        String reverse = new StringBuffer(cardNumber).reverse().toString();
        for (int i = 0; i < reverse.length(); i++) {
            int digit = Character.digit(reverse.charAt(i), 10);
            if (i % 2 == 0) {
                sumOfEvenDigits += digit;
            } else {
                sumOfOddDigits += 2 * digit;
                if (digit >= 5) {
                    sumOfOddDigits -= 9;
                }
            }
        }
        boolean isValid = (sumOfEvenDigits + sumOfOddDigits) % 10 == 0;
        return isValid;
    }
    
    private String getPaymentGatewayOrderId(String orderId) {
        Long retryAttempt = paymentsCache.getRetryCount(orderId).longValue();
        return retryAttempt == 1 ? orderId  : orderId + retryAttempt;
    }
}
