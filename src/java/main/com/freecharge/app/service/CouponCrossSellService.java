package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
@Component
public class CouponCrossSellService {
	@Autowired
    private MetricsClient metricsClient;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	public List<CouponHistory> getCouponAndCrossSellHistory(List<String> orderIds){
		long startTime = System.currentTimeMillis();
		List<CouponHistory> coupons = couponServiceProxy.getCoupons(orderIds);
		List<CouponHistory> history = new ArrayList<CouponHistory>();
		if (coupons != null){
			for (CouponHistory coupon : coupons){
				if (!coupon.getCouponNature().equals(FCConstants.COUPON_NATURE_MISSION)){
					history.add(coupon);					
				}
			}
		}
		Collections.sort(history, Collections.reverseOrder());
		long endTime = System.currentTimeMillis();
		metricsClient.recordLatency("CouponCrossSellService", "getCouponAndCrossSellHistory", endTime - startTime);
		return history;
	}
}
