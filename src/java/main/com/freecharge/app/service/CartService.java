package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.CouponConstants;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.ProductMasterDAO;
import com.freecharge.app.domain.dao.jdbc.CrosssellReadDAO;
import com.freecharge.app.domain.dao.jdbc.CrosssellWriteDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.TxnCrosssellItemsVO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;
import com.freecharge.rest.coupon.model.CouponsCartRes;
import com.freecharge.rest.coupon.model.SaveCartItemsRequestData;
import com.freecharge.rest.coupon.model.SaveCouponReq;
import com.freecharge.web.webdo.CartAndCartItemsVO;
import com.freecharge.web.webdo.CartItemsWebDO;
import com.freecharge.web.webdo.CartWebDo;

/**
 * Created by IntelliJ IDEA. User: Jitender Date: May 11, 2012 Time: 10:22:56 AM
 * To change this template use File | Settings | File Templates.
 */
@Service("cartService")
// @EnableAsync
public class CartService {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private CartDAO cartDao;

	@Autowired
	private ProductMasterDAO productMasterDAO;
	@Autowired
	private CartItemsDAO cartItemsDAO;

	@Autowired
	private FreefundService freefundService;

	@Autowired
	private CrosssellReadDAO crosssellReadDAO;

	@Autowired
	private CrosssellWriteDAO crosssellWriteDAO;

	@Autowired
	private OrderIdWriteDAO orderIdDAO;

	@Autowired
	private OrderIdSlaveDAO orderIdSlaveDAO;

	@Autowired
	private BinOfferService binOfferService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private TxnHomePageService txnHomePageService;

	@Autowired
	private MobileCheckoutService mCheckoutService;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	@Autowired
	private OrderService orderService;

	@Autowired
	private CouponCartService couponCartService;

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;

	public static final String REASON_SOMETHING_WRONG = "someThingWrong";
	public static final String REASON_COUPON_OF_STOCK = "couponOutOfStock";
	public static final String REASON_COUPON_EXPIRED = "couponExpired";
	
	public static final String COUPON_TYPE_H_COUPON = "H";
	public static final String COUPON_TYPE_E_COUPON = "E";

	@Transactional
	public void removeCartItem(Integer cartItemId) {
		logger.info("Coming in CartService : removeCartItem " + cartItemId);

		CartItems cartItems = cartItemsDAO.read(cartItemId);

		cartItems.setIsDeleted(true);
		cartItems.setDeletedOn((new Timestamp(new Date().getTime())));

		cartItemsDAO.update(cartItems);

		if ("crosssell".equalsIgnoreCase(cartItems.getEntityId())) {
			Crosssell crosssell = crosssellReadDAO.getCrosssell(cartItems.getFkItemId());
			crosssell.setCountUses(crosssell.getCountUses() - 1);
			crosssellWriteDAO.update(crosssell);
		}

		logger.info("Data deleted in cart_items for forcart_items_id : " + cartItems.getCartItemsId());
	}

	@Transactional
	public CartBusinessDo getCartBusinessDoByLid(String lookupId) {
		return createCartBusinessDO(this.getCartBylookUp(lookupId));
	}

	@Transactional
	public CartBusinessDo getCart(String lookupId) {
		Cart cart = cartDao.findByLookupId(lookupId);
		CartBusinessDo cartBusinessDo = null;
		if (cart != null) {
			cartBusinessDo = createCartBusinessDO(cart);
		}
		return cartBusinessDo;
	}

	public Double getRechargeAmount(CartBusinessDo cartBusinessDo) {
		for (CartItemsBusinessDO cartItemsDO : cartBusinessDo.getCartItems()) {
			ProductName product = ProductName.fromProductId(cartItemsDO.getProductMasterId());
			if (product != null && product.isPrimaryProduct()) {
				return cartItemsDO.getPrice();
			}
		}

		return new Double("0");
	}

	@Transactional
	public CartBusinessDo getCartWithNoExpiryCheck(String lookupId) {
		Cart cart = cartDao.findByLookupIdWithNoExpiryCheck(lookupId);
		CartBusinessDo cartBusinessDo = createCartBusinessDO(cart);
		return cartBusinessDo;
	}

	@Transactional
	public Cart getUserIdFromCartWithNoExpiryCheck(String lookupId) {
		Cart cart = cartDao.findByLookupIdWithNoExpiryCheck(lookupId);
		return cart;
	}

	@Transactional
	public Cart getCartBylookUp(String lookupId) {
		Cart cart = cartDao.findByLookupId(lookupId);
		return cart;
	}

	@Transactional
	public CartBusinessDo getCartByOrderId(String orderId) {
		List<OrderId> orderIdList = orderIdDAO.getByOrderId(orderId);

		if (orderIdList == null || orderIdList.isEmpty()) {
			return null;
		}

		if (orderIdList.size() > 1) {
			logger.error("[getCartByOrderId] More than one OrderID row found for orderId : [" + orderId
					+ "] fetching the first one in the list and continuing");
		}

		return getCartWithNoExpiryCheck(orderIdList.get(0).getLookupId());
	}

	/**
	 * Return Mobile number if it was a mobile recharge.
	 * 
	 * @param orderId
	 * @return
	 */
	@Transactional(readOnly = true)
	public String getMobileNumber(String orderId) {
		CartBusinessDo cartBusinessDo = getCartByOrderId(orderId);
		if (cartBusinessDo != null) {
			return getMobileNumber(cartBusinessDo);
		} else {
			logger.info("Oops! No cart found for Order ID [" + orderId + "]. Nothing to refund");
		}
		return null;
	}

	public String getMobileNumber(CartBusinessDo cartBusinessDo) {
		if (cartBusinessDo == null)
			return null;
		for (CartItemsBusinessDO cartItem : cartBusinessDo.getCartItems()) {
			if (!cartItem.getDeleted()) {
				if (ProductMaster.ProductName
						.fromProductId(cartItem.getProductMasterId()) == ProductMaster.ProductName.Mobile
						|| ProductMaster.ProductName.fromProductId(
								cartItem.getProductMasterId()) == ProductMaster.ProductName.MobilePostpaid) {
					return cartItem.getDisplayLabel();
				}
			}
		}
		return null;
	}

	@Transactional(readOnly = true)
	public String getRechargedNumber(String lookupId) {
		if (lookupId == null) {
			return null;
		}

		List<CartItemsBusinessDO> cartItems = orderIdSlaveDAO.getCartItemsByLookupId(lookupId);

		if (cartItems == null) {
			return null;
		}

		for (CartItemsBusinessDO cartItem : cartItems) {
			if (!cartItem.getDeleted()) {
				ProductMaster.ProductName productName = ProductMaster.ProductName
						.fromProductId(cartItem.getProductMasterId());
				if (productName.isUtilityPaymentProduct()) {
					TxnHomePageBusinessDO txnHomePage = txnHomePageService.getTransactionDetails(lookupId);
					BillWithSubscriberNumber bill = billPayServiceRemote.getBillDetails(txnHomePage.getServiceNumber());
					if (bill != null) {
						return bill.getAdditionalInfo1();
					}
				}
				if (productName.isNumberedProduct()) {
					return cartItem.getDisplayLabel();
				}
			}
		}
		return null;
	}

	private CartBusinessDo createCartBusinessDO(Cart cart) {

		if (cart == null)
			return null;

		CartBusinessDo cartBusinessDo = new CartBusinessDo();
		cartBusinessDo.setCreatedOn(cart.getCreatedOn());
		cartBusinessDo.setExpiresOn(cart.getExpiresOn());
		cartBusinessDo.setFinalStatus(cart.getFinalStatus());
		cartBusinessDo.setId(cart.getCartId().longValue());
		cartBusinessDo.setLookupID(cart.getLookupId());
		cartBusinessDo.setOrderId(cart.getFkOrderId());
		cartBusinessDo.setUserId(cart.getFkUserId());

		List<CartItemsBusinessDO> itemsBusinessList = new ArrayList<CartItemsBusinessDO>();

		for (CartItems items : cart.getCartItemsList()) {
			CartItemsBusinessDO cartItemsBusinessDO = new CartItemsBusinessDO();
			cartItemsBusinessDO.setAddedOn(items.getAddedOn());
			cartItemsBusinessDO.setDeleted(items.getIsDeleted());
			cartItemsBusinessDO.setDeletedOn(items.getDeletedOn());
			cartItemsBusinessDO.setDisplayLabel(items.getDisplayLabel());
			cartItemsBusinessDO.setEntityId(items.getEntityId());
			cartItemsBusinessDO.setId(items.getCartItemsId().longValue());
			cartItemsBusinessDO.setItemId(items.getFkItemId());
			cartItemsBusinessDO.setPrice(items.getPrice());
			cartItemsBusinessDO.setProductMasterId(items.getFlProductMasterId());
			cartItemsBusinessDO.setQuantity(items.getQuantity());

			itemsBusinessList.add(cartItemsBusinessDO);
		}
		cartBusinessDo.setCartItems(itemsBusinessList);

		return cartBusinessDo;

	}

	@Transactional
	public List<TxnCrosssellItemsVO> getTxnCrossSellIdByHomePageID(Integer homePageId) {
		return cartDao.getCrossSellID(homePageId);
	}

	public List<CartItems> getCartItemsByCartId(Integer fkCartId) {
		return cartItemsDAO.findByFkCartId(fkCartId);
	}

	public CartItemsBusinessDO getBinOfferBusinessDO(CartBusinessDo cartBusinessDo) {
		List<CartItemsBusinessDO> ctbs = cartBusinessDo.getCartItems();
		for (CartItemsBusinessDO ctb : ctbs) {
			if (FCConstants.FFREEFUND_PRODUCT_TYPE.equals(ctb.getEntityId())) {
				return ctb;
			}
		}

		return null;
	}

	public boolean containsFreeFund(List<CartItemsBusinessDO> cartItems) {
		for (CartItemsBusinessDO cartItem : cartItems) {
			if (!cartItem.getDeleted()) {
				if (ProductName.fromProductId(cartItem.getProductMasterId()) == ProductName.FreeFund) {
					return true;
				}
			}
		}

		return false;
	}

	public FreefundCoupon getFreefundCoupon(CartWebDo cartWebDo) {
		if (cartWebDo == null) {
			return null;
		}
		List<CartItemsWebDO> itemsList = cartWebDo.getItemsList();
		for (CartItemsWebDO item : itemsList) {
			if (FCConstants.ENTITY_NAME_FOR_FREEFUND.equals(item.getItemTableName())) {
				return freefundService.getFreefundCoupon((long) item.getItemId());
			}
		}
		return null;
	}

	public FreefundCoupon getFreefundCoupon(CartBusinessDo cart) {
		List<CartItemsBusinessDO> cartItems = cart.getCartItems();

		for (CartItemsBusinessDO cartItem : cartItems) {
			if (!cartItem.getDeleted()) {
				if (ProductName.fromProductId(cartItem.getProductMasterId()) == ProductName.FreeFund) {
					return this.freefundService.getFreefundCoupon(cartItem.getItemId().longValue());
				}
			}
		}
		return null;
	}

	public ProductMaster.ProductName getPrimaryProduct(CartBusinessDo cartBusinessDo) {
		for (CartItemsBusinessDO cartItem : cartBusinessDo.getCartItems()) {
			if (!cartItem.getDeleted()) {
				ProductName productName = ProductMaster.ProductName.fromProductId(cartItem.getProductMasterId());
				if (productName.isPrimaryProduct()) {
					return productName;
				}
			}
		}

		throw new IllegalStateException("Trying to fetch primary product when none exists");
	}

	@Transactional(readOnly = true)
	public ProductName getPrimaryProduct(String orderId) {
		CartBusinessDo cartToInspect = this.getCartByOrderId(orderId);

		return this.getPrimaryProduct(cartToInspect);
	}

	public ProductName getProductName(CartItemsBusinessDO cartItems) {
		return ProductName.fromProductId(cartItems.getProductMasterId());
	}

	public ProductName getProductName(CartItems cartItems) {
		return ProductName.fromProductId(cartItems.getFlProductMasterId());
	}

	@Transactional
	public void saveCartForWalletFund(String lookupId, int userId, Amount amountToFund) {
		Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
		Timestamp cartExpiryTime = new Timestamp(DateUtils.addDays(currentTimestamp, 1).getTime());

		Cart cart = new Cart();
		cart.setLookupId(lookupId);
		cart.setCreatedOn(currentTimestamp);
		cart.setExpiresOn(cartExpiryTime);
		cart.setFkUserId(userId);

		Integer cartId = cartDao.saveCart(cart);

		CartItems cartItems = new CartItems();
		List<CartItems> cartItemsList = new ArrayList<CartItems>();
		cartItems.setCart(cart);
		cartItems.setQuantity(1);
		cartItems.setIsDeleted(false);
		cartItems.setPrice(amountToFund.getAmount().doubleValue());
		cartItems.setFlProductMasterId(ProductMaster.ProductName.WalletCash.getProductId());
		cartItems.setDisplayLabel("Add Cash");
		Calendar c = Calendar.getInstance();
		cartItems.setAddedOn((new Timestamp(c.getTime().getTime())));
		cartItemsList.add(cartItems);
		cartDao.saveCartItems(cartItemsList, cartId);
	}

	@Transactional
	public Cart getCartByLookUpNoExpiryCheck(String lookupId) {
		Cart cart = cartDao.findByLookupIdWithNoExpiryCheck(lookupId);
		return cart;
	}

	@Transactional
	public Integer saveCartItems(CartItems cartItems) {
		return cartItemsDAO.save(cartItems);
	}

    public boolean isFreeFundPresent(CartItems cartItems) {
    	return cartItemsDAO.isFreeFundPresent(cartItems);
    }
 
    @Transactional
	public CouponsCartRes saveCouponsInCart(SaveCartItemsRequestData saveCartItemsRequestData) {
		logger.info("Started Saving coupons in cart = " + saveCartItemsRequestData.getOrderId());
		//Get lookupId for orderId
		String lookupId = orderIdDAO.getLookupIdForOrderId(saveCartItemsRequestData.getOrderId());

		/*Get cart for lookupId without expirydate*/
		Cart cart = cartDao.findByLookupIdWithNoExpiryCheck(lookupId);

		if (cart == null) {
			logger.debug("Couldnt find cart for " + lookupId );
			logger.error("Coupold not find the cart for orderId = " + saveCartItemsRequestData.getOrderId());
			return null;
		}
		logger.info("CartId for " + saveCartItemsRequestData.getOrderId() + " is " + cart.getCartId());

		//Initialize result
		CouponsCartRes couponsCartRes = new CouponsCartRes();
		couponsCartRes.setOrderId(saveCartItemsRequestData.getOrderId());
		List<CouponCartItem> couponCartItems = new ArrayList<>();
		try {
			//Add coupons to this cart
			for (SaveCouponReq saveCouponReq : saveCartItemsRequestData.getSaveCouponReqs()){
				if (saveCouponReq.getCouponId() == null) {
					continue;
				}
				
//				if (isCouponIdAlreadyExist(saveCouponReq.getCouponId(), saveCartItemsRequestData.getOrderId())) {
//					logger.error(saveCouponReq.getCouponId() + "already present in cart for orderId " + saveCartItemsRequestData.getOrderId());
//					continue;
//				}
				
				if (alreadyOrderExistForCouponId(saveCartItemsRequestData.getOrderId(), saveCouponReq.getCouponId())) {
					logger.error(saveCouponReq.getCouponId() + " coupon already given orderId " + saveCartItemsRequestData.getOrderId());
					continue;
				}
				
				CartItems cartItems = new CartItems();
				cartItems.setCart(cart);
				cartItems.setEntityId("post_txn_coupon");
				cartItems.setFkItemId(saveCouponReq.getCouponId());
				cartItems.setIsDeleted(false);
				if (COUPON_TYPE_H_COUPON.equalsIgnoreCase(saveCouponReq.getCouponType())) {
					cartItems.setFlProductMasterId(CouponConstants.PRODUCT_MASTER_ID_HCOUPONS);
				} else if (COUPON_TYPE_E_COUPON.equalsIgnoreCase(saveCouponReq.getCouponType())) {
					cartItems.setFlProductMasterId(15);
				}
				cartItems.setPrice(0D);
				cartItems.setQuantity(1);
				cartItems.setAddedOn(new Timestamp((new Date()).getTime()));
				Integer cartItemId = saveCartItems(cartItems);
				
				logger.info("Cart Items added cartItemId = " + cartItemId);
				
				CouponCartItem couponCartItem = new CouponCartItem();
				couponCartItem.setCouponId(saveCouponReq.getCouponId());
				couponCartItem.setPrice(0);
				couponCartItem.setQuantity(1);
				if (COUPON_TYPE_H_COUPON.equalsIgnoreCase(saveCouponReq.getCouponType())) {
					couponCartItem.setType(CouponOptinType.HCOUPON);
				} else if (COUPON_TYPE_E_COUPON.equalsIgnoreCase(saveCouponReq.getCouponType())) {
					couponCartItem.setType(CouponOptinType.OPTIN);
				}
				couponCartItems.add(couponCartItem);
			}
			couponsCartRes.setStatus(true);
			couponsCartRes.setCouponCartItems(couponCartItems);
			logger.info("Coupons saved to cart for orderId: "+saveCartItemsRequestData.getOrderId());
			return couponsCartRes;
		}catch (Exception e){
			logger.error("Error while saving coupons to cartItems. Ignoring.", e);
			couponsCartRes.setOrderId(saveCartItemsRequestData.getOrderId());
			couponsCartRes.setStatus(false);
			return couponsCartRes;
		}
	}

	private boolean alreadyOrderExistForCouponId(String orderId, Integer couponId) {
		List<CouponHistory> couponHistoryList = couponServiceProxy.getCouponsOptinHistory(orderId, couponId);
		if (couponHistoryList != null && couponHistoryList.size() > 0) {
			return true;
		}
		return false;
	}

	private boolean isCouponIdAlreadyExist(Integer couponId, String orderId) {
		CartItemsBusinessDO cartItemsBusinessDO = getCouponCartItemByCouponId(couponId, orderId);
		if (cartItemsBusinessDO != null) {
			return true;
		}
		return false;
	}

	/**
	 * A global boolean to indicate if offers are enabled for a cart or not.
	 * Introduced since add-cash-to-wallet feature since while adding cash to
	 * wallet offers are disabled.
	 * 
	 * @param cartBusinessDo
	 * @return
	 */
	public boolean isEligibleForOffers(CartBusinessDo cartBusinessDo) {
		if (ProductName.WalletCash == this.getPrimaryProduct(cartBusinessDo)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * This method abstracts out the business logic of giving away extra factor
	 * (2x, 3x etc.,) of coupons for a given cart. Below is the business logic
	 * as of now.
	 * <ul>
	 * <li>FreeFund item that has product master id = 8 is eligible for double
	 * coupons.</li>
	 * <li>Master and Maestro cards (Debit or Credit) cards are eligible for
	 * double coupons.</li>
	 * <li>BinOffer cart item is eligible for n * x coupons where n is specified
	 * by bin offers
	 * <li>
	 * <li>Offers can't be combined i.e., if one pays using Master card that is
	 * eligible for 3x coupons he will only get 3x coupons instead of 6x</li>
	 * </ul>
	 * 
	 * @param cart
	 * @return
	 */
	public Integer getCouponScaleUpFactor(CartBusinessDo cart) {
		// default
		Integer scaleUpFactor = 1;

		/*
		 * Coupon factor as specified in bin offer takes precedence over master
		 * card and freefund since bin offers could give away more than 2x
		 * coupons (generally 3x). That's why we break out of for loop as soon
		 * as we encounter a bin offer and continue our search in case master
		 * card or freefund is found.
		 */
		for (CartItemsBusinessDO cartItems : cart.getCartItems()) {
			if (!cartItems.getDeleted()) {
				switch (this.getProductName(cartItems)) {
				case BinOffer:
					scaleUpFactor = getCouponFactorForBinOffer(cartItems.getItemId());
					break;

				case FreeFund:
					Integer productMasterId = cartItems.getProductMasterId();

					// productMasterId == 9 is bin-based freefund
					if (productMasterId == 9) {
						scaleUpFactor = getCouponFactorForBinOffer(cartItems.getItemId());
						break;
					} else if (productMasterId == 8) {
						scaleUpFactor = freefundService.getCouponFactor(cartItems.getItemId());
					}

					continue;

				case MasterCard:
					scaleUpFactor = 1;

					continue;

				default:
					continue;
				}
			}
		}

		return scaleUpFactor;
	}

	@Transactional
	public Integer getCouponScaleUpFactorForOrder(String orderId) {
		CartBusinessDo cartToInspect = getCartByOrderId(orderId);
		final int couponScalingFactor = getCouponScaleUpFactor(cartToInspect);
		return couponScalingFactor;
	}

	private Integer getCouponFactorForBinOffer(Integer binOfferId) {
		Integer scaleUpFactor;
		BinOffer binOffer = binOfferService.getBinOffer(binOfferId);

		scaleUpFactor = binOffer.getCouponFactor();
		return scaleUpFactor;
	}

	public List<CartItemsBusinessDO> getCouponsForOrder(String orderId) {
		CartBusinessDo cartToInspect = getCartByOrderId(orderId);
		List<CartItemsBusinessDO> coupons = new ArrayList<>();
		for (CartItemsBusinessDO cartItem : cartToInspect.getCartItems()) {
			if (!cartItem.getDeleted()) {
				ProductName productName = getProductName(cartItem);
				if (productName == ProductName.Coupons || productName == ProductName.PriceCoupons) {
					coupons.add(cartItem);
				} // end if
			} // end if
		} // end for
		return coupons;
	}

	/**
	 * Returns true if cart items corresponding to a given order ID contains
	 * coupons.
	 * 
	 * @param orderId
	 * @return
	 */
	public boolean hasCoupons(String orderId) {
		return orderIdSlaveDAO.hasCoupons(orderId);
	}

	public Integer getCouponQuantity(Integer item_id, Integer cartId) {
		return cartItemsDAO.getCouponQuantity(item_id, cartId);
	}

	public Cart buildCart(String lookupID) {
		Cart cart = new Cart();
		cart.setLookupId(lookupID);
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		cart.setCreatedOn((new Timestamp(date.getTime())));
		c.add(Calendar.DATE, 1);
		date.setTime(c.getTimeInMillis());
		cart.setExpiresOn(new Timestamp(date.getTime()));
		return cart;
	}

	@Transactional
	public String createCartForHCoupons(String itemIdArray, Integer userId, String lookUpId) {
		if (lookUpId == null) {
			lookUpId = FCUtil.getLookupID();
		}
		Cart cart = buildCart(lookUpId);
		cart.setFkUserId(userId);
		Integer cart_id = cartDao.saveCart(cart);
		createCartItemsForHCoupons(itemIdArray, cart_id, lookUpId);
		return lookUpId;
	}

	@Transactional
	private void createCartItemsForHCoupons(String itemIdArray, Integer cart_id, String lookupId) {
		if (itemIdArray.startsWith(",")) {
			itemIdArray = itemIdArray.replaceFirst(",", "");
		}
		String splictedCouponArr[] = itemIdArray.split(",");
		logger.info("Splicted  Coupon array is " + splictedCouponArr);
		List<String> splictedCouponList = Arrays.asList(splictedCouponArr);
		logger.info("Splicted  Coupon array is after Making it as List is " + splictedCouponList);
		Set<String> splictedCouponset = new HashSet<String>(splictedCouponList);
		Iterator<String> ite = splictedCouponset.iterator();
		List<CartItems> cartItemsList = new ArrayList<CartItems>();
		while (ite.hasNext()) {
			String id = (String) ite.next();
			Integer frequency = Collections.frequency(splictedCouponList, id);
			/*
			 * short eligiableCoupons =
			 * Short.parseShort(fcProperties.getProperty(String.valueOf(
			 * FCConstants.PRODUCT_ID_HCOUPONS))); if(frequency >
			 * eligiableCoupons ){
			 * logger.error("frequency > eligiableCoupons  : " + frequency
			 * +" > "+ eligiableCoupons ); throw new CouponTamperedException();
			 * }
			 */
			CouponStore couponStore = couponService.getCouponStore(Integer.parseInt(id));
			if (couponStore != null) {
				CartItems cartItems = new CartItems();
				Cart cartdata = new Cart();
				cartdata.setCartId(cart_id);
				cartItems.setFkItemId(Integer.parseInt(id));
				cartItems.setCart(cartdata);
				cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
				cartItems.setIsDeleted(false);
				cartItems.setQuantity(frequency);
				cartItems.setEntityId("site_merchant");
				cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_HCOUPONS);
				cartItems.setPrice(couponStore.getPrice().doubleValue());
				cartItemsList.add(cartItems);
			}
		}
		List<CartAndCartItemsVO> list = cartDao.getCartByLookUpId(lookupId);

		if (!FCUtil.isEmpty(list)) {
			cartDao.deleteCartItems(list.get(0).getCartId());
			cart_id = list.get(0).getCartId();
		}
		cartDao.saveCartItems(cartItemsList, cart_id);
	}

	public void createCartByLookupId(String lookupId) {
		TxnCrossSellBusinessDo bdo = new TxnCrossSellBusinessDo();
		TxnHomePageBusinessDO txnHomePageBusinessDO = txnHomePageService.getTransactionDetails(lookupId);
		logger.info("about to go inside method mCheckoutService.createCartAndInitiateCheckout()");
		mCheckoutService.createCartAndInitiateCheckout(txnHomePageBusinessDO, bdo);
	}

	public void createCartByLookupId(String lookupId, Integer planId, String planCategory){
		TxnCrossSellBusinessDo bdo = new TxnCrossSellBusinessDo();
		bdo.setPlanId(planId);
		bdo.setPlanCategory(planCategory);
		TxnHomePageBusinessDO txnHomePageBusinessDO = txnHomePageService.getTransactionDetails(lookupId);
		logger.info("about to go inside method mCheckoutService.createCartAndInitiateCheckout()");
		mCheckoutService.createCartAndInitiateCheckout(txnHomePageBusinessDO, bdo);
	}

	public CartItemsBusinessDO getCouponCartItemByCouponId(Integer couponId, String orderId) {
		CartBusinessDo cartBusinessDo = getCartByOrderId(orderId);
		if (cartBusinessDo != null) {
			List<CartItemsBusinessDO>  cartItemsBusinessDOList = cartBusinessDo.getCartItems();
			if (cartItemsBusinessDOList != null) {
				for (CartItemsBusinessDO cartItemsBusinessDO : cartItemsBusinessDOList) {
					if (cartItemsBusinessDO.getEntityId() != null && cartItemsBusinessDO.getItemId() != null) {
						if (cartItemsBusinessDO.getEntityId().equalsIgnoreCase("post_txn_coupon") &&  (couponId.intValue() == cartItemsBusinessDO.getItemId().intValue())) {
							return cartItemsBusinessDO;
						}
					}
				}
			}
		}
		return null;
	}
}