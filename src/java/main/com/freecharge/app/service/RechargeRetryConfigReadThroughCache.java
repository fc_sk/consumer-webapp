package com.freecharge.app.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.IRechargeRetryConfigDao;
import com.freecharge.app.domain.entity.PlanLevelRetryConfig;
import com.freecharge.app.domain.entity.RechargeRetryConfig;
import com.freecharge.common.cache.CacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.hlr.service.HLRResponse;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;

@Service
public class RechargeRetryConfigReadThroughCache extends CacheManager {

	private static final String HLR_RETRY_DATA_KEY = "hlrRetryData";

	public static String RECHARGE_RETRY__CONFIG_CACHE_KEY = "rechargeRetryConfigCache";

	public static String PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY = "planLevelRetryData";

	public static final String PLAN_LEVEL_RETRY_KEY = "planLevelRetryKey";

	public static final String LOB3_FLAG_KEY = "lobKey_";

	private static final String SCHEDULE_KEY = "rechargeSchedule";
	
	@Autowired
	private IRechargeRetryConfigDao rechargeRetryConfigDao;
	
	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
    MetricsClient metricsClient;

	private static Logger logger = LoggingFactory.getLogger(RechargeRetryConfigReadThroughCache.class);

	public List<RechargeRetryConfig> getRechargeRetryConfigs() {
		if(appConfig.isRechargeRetryCodesConfigEnabled()) {
			List<RechargeRetryConfig> rechargeRetryConfigs = appConfig.getRechargeRetryCodesConfigs();
			if(rechargeRetryConfigs == null) {
				try {
					metricsClient.recordEvent("Recharge", "RechargeRetryConfigReadFromAppConfig", "RetryConfigNull");
				}catch (Exception e) {
					logger.error("Exception while recording metric for RechargeRetryConfigReadFromAppConfig : " , e);
				}
			}
			return rechargeRetryConfigs;
        }
        
		List<RechargeRetryConfig> configs = getRechagerRetryConfigsFromCache();

		if (configs == null || configs.isEmpty()) {
			refreshCache();
			configs = getRechagerRetryConfigsFromCache();
		}
		return configs;
	}

	@SuppressWarnings("unchecked")
	private List<RechargeRetryConfig> getRechagerRetryConfigsFromCache() {
		return (List<RechargeRetryConfig>) get(RECHARGE_RETRY__CONFIG_CACHE_KEY);
	}

	@PostConstruct
	public void refreshCache() {
		List<RechargeRetryConfig> configs = rechargeRetryConfigDao.getAllRechargeRetryConfig();
		set(RECHARGE_RETRY__CONFIG_CACHE_KEY, configs);
	}

	public Boolean setIfAbsentRechargeRetryOrderId(String orderId, Object value) {
		return setIfAbsent(orderId, value);
	}

	public Object getRechargeRetryOrderId(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			return get(orderId);
		}
		return null;
	}

	public void deletRechargeRetryOrderId(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			delete(orderId);
		}
	}

	public void setHLROperatorRetry(String orderId, Boolean value) {
		set(orderId, value);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 6);
		date = cl.getTime();
		expireAt(orderId, date);
	}

	public Boolean getHLROperatorRetry(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			if ((Boolean) get(orderId) != null) {
				return (Boolean) get(orderId);
			} else
				return false;
		}
		return false;
	}

	public void setHLRRetryData(HLRResponse hlrResponse, String orderId) {
		set(HLR_RETRY_DATA_KEY + "_" + orderId, hlrResponse);
		logger.info("Setting HLR Retry data with key " + HLR_RETRY_DATA_KEY + "_" + orderId);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 6);
		date = cl.getTime();
		expireAt(orderId, date);
	}

	public HLRResponse getHLRRetryData(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			if ((HLRResponse) get(HLR_RETRY_DATA_KEY + "_" + orderId) != null) {
				return (HLRResponse) get(HLR_RETRY_DATA_KEY + "_" + orderId);
			} else
				return null;
		}
		return null;
	}
	
	public void setPlanLevelRetryData(String orderId, PlanLevelRetryConfig planRetryObj) {
		set(PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY + "_" + orderId, planRetryObj);
		logger.info("Setting Plan level retry config data with key : " + PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY + "_"
				+ orderId);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 3);
		date = cl.getTime();
		expireAt(PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY+"_"+orderId, date);
		
	}

	public PlanLevelRetryConfig getPlanLevelRetryData(String orderId) {
		try {
			if (!StringUtils.isBlank(orderId)) {
				if ((PlanLevelRetryConfig) get(PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY + "_" + orderId) != null) {
					return (PlanLevelRetryConfig) get(PLAN_LEVEL_RECHARGE_RETRY_DATA_KEY + "_" + orderId);
				} else
					return null;
			}
		} catch (Exception e) {
			logger.info("An error occurred trying to get data from cache for Plan Level Retry");
		}
		return null;
	}

	public void setPlanLevelRetryKey(String orderId, Boolean value) {
		set(PLAN_LEVEL_RETRY_KEY + "_" + orderId, value);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 3);
		date = cl.getTime();
		expireAt(PLAN_LEVEL_RETRY_KEY+"_"+orderId, date);
	}

	public Boolean getPlanLevelRetryKey(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			if ((Boolean) get(PLAN_LEVEL_RETRY_KEY + "_" + orderId) != null) {
				return (Boolean) get(PLAN_LEVEL_RETRY_KEY + "_" + orderId);
			} else
				return false;
		}
		return false;
	}
	
	public void deletePlanLevelRetryKey(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			delete(PLAN_LEVEL_RETRY_KEY + "_" + orderId);
		}
	}
	
	public void setScheduleInfo(FulfillmentScheduleDO fulfillmentScheduleDO, String orderId) {
		set(SCHEDULE_KEY+"_"+orderId,fulfillmentScheduleDO);
		logger.info("Setting ScheduleInfo with key "+ SCHEDULE_KEY+"_"+orderId);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 3);
		date = cl.getTime();
		expireAt(SCHEDULE_KEY+"_"+orderId, date);
		
	}
	
	public FulfillmentScheduleDO getScheduleInfo(String orderId){
		if (!StringUtils.isBlank(orderId)) {
			if((FulfillmentScheduleDO) get(SCHEDULE_KEY+"_"+orderId)!=null){
				return (FulfillmentScheduleDO) get(SCHEDULE_KEY+"_"+orderId);
			}
			else
				return null;
		}
		return null;
	}
	
	public void deleteScheduleInfo(String orderId) {
        if (!StringUtils.isBlank(orderId)) {
            delete(SCHEDULE_KEY+"_"+orderId);
        }
    }

	/**
	 * Used to get the LOB3 key value from the cache
	 * 
	 * @param orderId
	 * @return Boolean - the boolean value
	 */
	public Boolean getLOBKey(String orderId) {
		if (!StringUtils.isBlank(orderId)) {
			if ((Boolean) get(LOB3_FLAG_KEY + orderId) != null) {
				return (Boolean) get(LOB3_FLAG_KEY + orderId);
			}
		}
		return true;
	}

	/**
	 * Used to set the LOB3 key value to the cache and expire it after 24 hours
	 * 
	 * @param orderId
	 * @param value
	 */
	public void setLOBKey(String orderId, Boolean value) {
		set(LOB3_FLAG_KEY + orderId, value);
		Date date = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(date);
		cl.add(Calendar.HOUR, 120);
		date = cl.getTime();
		expireAt(LOB3_FLAG_KEY + orderId, date);
	}
}
