package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.CouponPinDAO;
import com.freecharge.app.domain.entity.winpin.CouponPin;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;


/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: Apr 21, 2012
 * Time: 1:51:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("couponRedeemptionService")
public class CouponRedeemptionService implements ICouponRedeemptionService {
     private static Logger logger = LoggingFactory.getLogger(CouponRedeemptionService.class);
     private CouponPinDAO couponPinDAO;
     private EPinEncrypt ePinEncrypt;

    @Autowired
    public void setEPinEncrypt(EPinEncrypt ePinEncrypt) {
        this.ePinEncrypt = ePinEncrypt;
    }

     @Autowired
      public void setCouponPinDAO(CouponPinDAO couponPinDAO) {
          this.couponPinDAO = couponPinDAO;
      }

      /**
     * Returns the boolean type value after validating the coupon.
     *
     * @return  return integer value that will used to check coupon
       * validity.
     * @see # public String redeemCouponAction(String couponPin)
     */
      private int couponValidation(CouponPin couponPin) {
        try{
        if(couponPin!=null){
          Date  date=new Date();

         if(!couponPin.getCpnActive()){
            return 1;
         }
            else if(couponPin.getCpnIsRedeemed()){
             return 2;
         }
            else if(couponPin.getCpnInRecharge()){
             return 3;
         }
            else if(couponPin.getCfActiveDate().compareTo(date)< 0){
              return 4;
         }
          else if(couponPin.getCpnBlock()){
            return 5;
         }
            else {
            return 0;
         }
        }
        }
        catch (Exception e){
                       logger.error("Error in Coupon Validation",e);
            return -1;
        }
      return -1;
    }
           /**
     * Returns the float type value that return the coupon amount.
     *
     * @return  if return coupon value is greater than 0 that means coupon is valid.
            * or another hand if value if equal to zero that means coupon is not valid
            * and also retruns coupon validation result code.

     */
   @Transactional
    public Map redeemCouponAction(String couponPin)  {
        CouponPin cpnPin=null;
        String sEncrypCoupon= null;
         Map redeemResult = new HashMap();
        int couponValidation=-1;
        try {
            sEncrypCoupon = ePinEncrypt.getEncryptedPin(couponPin);
            cpnPin=couponPinDAO.findOneCpnPin(sEncrypCoupon);
            if(cpnPin!=null){
            couponValidation=couponValidation(cpnPin);
            switch (couponValidation){
              case 0:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon is valid for further processing");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,true);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  cpnPin.setCpnLockStatus(true);
                  Calendar calendar =Calendar.getInstance();
                  cpnPin.setCpnLockDatetime(new Timestamp(calendar.getTimeInMillis()));
                  couponPinDAO.create(cpnPin);
                  break;
              case 1:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon is not active for further processing");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  break;
              case 2:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon has redeemed. So it cann`t process");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  break;
              case 3:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon is under redeemption or locked");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  break;
              case 4:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon has expired. Please contact us for more details");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  break;
              case 5:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"This Coupon has permanently  blocked, for more detail please contact us");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());
                  break;
              default:
                  redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"Sorry for inconvenience, its technical fault");
                  redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                  redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,cpnPin.getCpnDenomination().floatValue());

            }
           }
        else {
                redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"Sorry this coupon is not exists");
                redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
                redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,0);            
            }

        } catch (Exception e) {
           logger.error("error in redeemCouponAction(String couponPin)",e);
            redeemResult.put(FCConstants.COUPON_REDEEM_MESSAGE_KEY,"Sorry for inconvenience, its technical fault");
            redeemResult.put(FCConstants.COUPON_REDEEM_STATUS_KEY,false);
            redeemResult.put(FCConstants.COUPON_REDEEM_VALUE_KEY,0);
        }
           return redeemResult;
    }

      /**   afterRedeemCouponAction this function update the value to coupon details on the basis
       * recharge status is recharge successful that means rechargeStatus value should be true,
       * else the rechargeStatus value would be false then coupon details again same as begining.
     * @return  its not return any value.
     * @see # public String redeemCouponAction(String couponPin)
     */

     @Transactional
    public void afterRedeemCouponAction(String couponPin,boolean rechargeStatus) {
        CouponPin cpnPin=null;
        String sEncrypCoupon= null;
        try {
         sEncrypCoupon = ePinEncrypt.getEncryptedPin(couponPin);
         cpnPin=couponPinDAO.findOneCpnPin(sEncrypCoupon);
        if(rechargeStatus){
          cpnPin.setCpnUsed(true);
          couponPinDAO.update(cpnPin); 
        }
        else  if(!rechargeStatus){
            cpnPin.setCpnUsed(false);
            cpnPin.setCpnLockStatus(false);
            couponPinDAO.update(cpnPin);
        }

        } catch (Exception e) {
          logger.error("Error in afterRedeemCouponAction(String couponPin,boolean rechargeStatus)",e);
        }
    }


}
