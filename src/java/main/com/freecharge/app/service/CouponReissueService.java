package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.common.businessdo.CouponInventoryBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;

@Service
public class CouponReissueService {
	private static Logger logger = LoggingFactory.getLogger(CouponReissueService.class);
	public static final String METRIC_SERVICE_REISSUE = "couponReissued";
    private static final String METRIC_NAME = "reissuedCoupons";
    
	@Autowired
	private VoucherService voucherService;

	@Autowired
    protected MetricsClient metricsClient;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	public List<Map<String, Object>> addCouponCodesinList(List<Map<String, Object>> couponList, OrderId orderId) {
		logger.debug("inside addCouponCodesinList for orderid:"+orderId);
		List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<Map<String, Object>>();
		
		CouponInventoryBusinessDO couponInventoryBusinessDO = voucherService.getCouponInventoryBusinessDO(couponList, orderId.getOrderId(), orderId.getLookupId());
		voucherService.updateCouponInventoryNew(couponInventoryBusinessDO);

		Map<String, List<String>> couponCodes = couponInventoryBusinessDO.getCouponCodes();
		
		
		logger.debug("reissued codes for orderid: "+orderId.getOrderId() + " = "+couponCodes);
		if(couponCodes != null) {
			logger.debug("couponList = "+couponList);
			for (int i = 0; i < couponList.size(); i++) {
				if(couponList.get(i).get("couponType") != null && (couponList.get(i).get("couponType").equals("E") || couponList.get(i).get("couponType").equals("P"))) {
					if (couponList.get(i).get("count") != null && couponList.get(i).get("couponStoreId") != null) {
						Integer couponCount = Integer.parseInt(couponList.get(i).get("count").toString());
						String couponStoreId = couponList.get(i).get("couponStoreId").toString();
						
						for(int j = 0; j < couponCount; j ++) {
							Map<String, Object> map = new HashMap<String, Object>();
							map.putAll(couponList.get(i));
							logger.debug("all coupon codes = "+ couponCodes);
							Boolean isUnique = new Boolean(map.get("isUnique").toString());
							if(couponCodes.get(couponStoreId) != null && couponCodes.get(couponStoreId).size() == couponCount){
								logger.debug("couponcode for mail = "+ couponCodes.get(couponStoreId).get(j));
								map.put("couponCode", couponCodes.get(couponStoreId).get(j));
							}else if(!isUnique) {
								if(couponCodes.get(couponStoreId) != null) {
									logger.debug("couponcode for mail = "+ couponCodes.get(couponStoreId).get(0));
									map.put("couponCode", couponCodes.get(couponStoreId).get(0));
								}else {
									logger.error("There is an error for a uniue coupon in inventory for which coupon codes are not received. coupon_store_id is = "+couponStoreId+" order_id is = "+orderId);
								}
							}
							
							map.put("count", "1");
							modifiedcouponListForEmail.add(map);																
						}
					}else {
						modifiedcouponListForEmail.add(couponList.get(i));
					}
				}else {
					modifiedcouponListForEmail.add(couponList.get(i));
				}
					
			}
		}
		logger.debug("Exiting from addCouponCodesinList for orderid:"+orderId);
		return modifiedcouponListForEmail;
	}
	
	public List<Map<String, Object>> addCouponCodesinList(Map<String, Object> couponMap, List<CouponCode> couponCodes, OrderId orderId) {
		List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<Map<String, Object>>();
		List<String> codes = voucherService.getCouponCodes(couponCodes);
		Integer couponCount = Integer.parseInt(couponMap.get("count").toString());
		String couponStoreId = couponMap.get("couponStoreId").toString();
		if(couponMap.get("couponType") != null && (couponMap.get("couponType").equals("E") || couponMap.get("couponType").equals("P"))) {
			if (couponMap.get("count") != null && couponMap.get("couponStoreId") != null) {
				for(int j = 0; j < couponCount; j ++) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.putAll(couponMap);
					logger.debug("all coupon codes = "+ couponCodes);
					Boolean isUnique = new Boolean(map.get("isUnique").toString());
					if(codes != null && codes.size() == couponCount){
						logger.debug("couponcode for mail = "+ codes.get(j));
						map.put("couponCode", codes.get(j));
					}else if(!isUnique) {
						if(codes != null) {
							logger.debug("couponcode for mail = "+ codes.get(0));
							map.put("couponCode", codes.get(0));
						}else {
							logger.error("There is an error for a uniue coupon in inventory for which coupon codes are not received. coupon_store_id is = "+couponStoreId+" order_id is = "+orderId.getOrderId());
						}
					}
					
					map.put("count", "1");
					modifiedcouponListForEmail.add(map);																
				}
			}else {
				modifiedcouponListForEmail.add(couponMap);
			}
		}else {
			modifiedcouponListForEmail.add(couponMap);
		}
				
		return modifiedcouponListForEmail;
	}
	
	public List<Map<String, Object>> getAllAvailableCouponReissue() {
		return couponServiceProxy.getAllAvailableCouponReissue();
	}
	
}
