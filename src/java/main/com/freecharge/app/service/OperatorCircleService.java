package com.freecharge.app.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.*;
import com.freecharge.common.businessdo.HomeBusinessDo;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.services.PrefixMappingCache;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by IntelliJ IDEA. User: Jitender Date: Apr 19, 2012 Time: 11:06:54 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OperatorCircleService {

    private static final Logger logger = LoggingFactory.getLogger(OperatorCircleService.class);

	private OperatorCircleReadDAO operatorCircleReadDAO;
	@Autowired
	private InCachedService inCachedService;

	@Autowired
	VoucherService voucherService;
	
	@Autowired
	private OperatorAlertService operatorAlertService;

    @Autowired
    AmazonDynamoDBAsync dynamoClientMum;
 
    @Autowired
    MetricsClient metricsClient;
    
	@Autowired
	PrefixMappingCache prefixMappingCache;
	
    @Autowired
    AppConfigService appConfig;

    private final String NO_AUTO_SELECT_OPERATORS = "noAuoSelectOperators";

	@Autowired
	public void setOperatorCircleReadDAO(OperatorCircleReadDAO operatorCircleReadDAO) {
		this.operatorCircleReadDAO = operatorCircleReadDAO;
	}
	
	@Transactional(readOnly = true)
	public void doOperatorCircleService(BaseBusinessDO baseBusinessDO) {

		HomeBusinessDo homeBusinessDo = (HomeBusinessDo) baseBusinessDO;

		homeBusinessDo.setOperatorList(getAllOperators());
	}

	@Transactional
    public CircleOperatorMapping getCircleOperatorMapping(String operator, String circle) {
          
            CircleOperatorMapping circleOperatorMapping = null;
            
            OperatorMaster operatorMaster = getOperator(operator);
            Integer operatorCode = operatorMaster.getOperatorMasterId();
            CircleMaster circleMaster = getCircle(circle);
            Integer circleCode = circleMaster.getCircleMasterId();
            
            circleOperatorMapping = getCircleOperatorMapping(operatorCode, circleCode);

            return circleOperatorMapping;
    }

	@Transactional
	public void getOperatorCircleList(BaseBusinessDO baseBusinessDO) {
	    try {
	        OperatorCircleBusinessDO operatorCircleBusinessDO = ((OperatorCircleBusinessDO) (baseBusinessDO));
	        boolean found = false;
	        List<Map<String, Object>> operatorCircleData = this.getOperatorCircleByPrefix(operatorCircleBusinessDO.getPrefix().substring(0, 4), operatorCircleBusinessDO.getProductType());
	        if (operatorCircleData.size() == 0) {
	            operatorCircleBusinessDO.setPrefixData(operatorCircleData);
	        } else if (operatorCircleData.size() > 1) {
	            List<Map<String, Object>> operatorCircleInfo = new ArrayList<Map<String, Object>>();
	            Iterator<Map<String, Object>> iter = operatorCircleData.iterator();
	        
	            // Checking Mapping with 5 digit database entry
	            while (iter.hasNext()) {
	                Map<String, Object> prefixMap = iter.next();
	                if (prefixMap.get("prefixValue").toString().equals(operatorCircleBusinessDO.getPrefix().substring(0, FCConstants.PREFIX_LENGTH_FIVE_DIGIT))) {
	                    operatorCircleInfo.add(prefixMap);
	                    operatorCircleBusinessDO.setPrefixData(operatorCircleInfo);
	                    found = true;
	                    break;
	                }
	            }
	            
	            //Checking Mapping with 4 digit database entry
	            if(!found)
	            {
	                iter = operatorCircleData.iterator();
	                while (iter.hasNext()) {
	                    Map<String, Object> prefixMap = iter.next();
	                    if (prefixMap.get("prefixValue").toString().equals(operatorCircleBusinessDO.getPrefix().substring(0, FCConstants.PREFIX_LENGTH_FOUR_DIGIT))) {
	                        operatorCircleInfo.add(prefixMap);
	                        operatorCircleBusinessDO.setPrefixData(operatorCircleInfo);
	                        break;
	                    }
	                }
	            }
	            
	            if (operatorCircleInfo.size() == 0) { // in case we don't have exact 5 digit or 4 digit  prefix mapping in to our database prefix table sending emty list
	                operatorCircleBusinessDO.setPrefixData(new ArrayList<Map<String, Object>>());
	            }
	        } else {
	            operatorCircleBusinessDO.setPrefixData(operatorCircleData);
	        }
        } catch (Exception e) {
            //Adding this to investigate the NPE occurring in prod
           logger.error("Error in getOperatorCircleList ", e);
           throw e;
        }
	}

	public List getAllCircles() {
		return operatorCircleReadDAO.getCircles();
	}
	// Circle Service
	@Transactional
	public List<CircleMaster> getCircles() {
		List<CircleMaster> circleMasters = null;
		try {
			List<CircleMaster> circleMaster = inCachedService.getCircles();
			if (circleMaster != null) {
				return circleMaster;
			} else {
				circleMasters = operatorCircleReadDAO.getCircles();
				inCachedService.setCircles(circleMasters);
			}
		} catch (Exception e) {
			circleMasters = operatorCircleReadDAO.getCircles();
		}
		return circleMasters;
	}

	@Transactional
	public CircleMaster getCircle(Integer circleId) {
		try {
			CircleMaster circleMaster = inCachedService.getCirclesByID(circleId);
			if (circleMaster != null) {
				return circleMaster;
			} else {
				inCachedService.setCircleByField(operatorCircleReadDAO.getCircles());
			}
		} catch (Exception e) {
            logger.error("error in getting circle from cache: circleId: "+circleId, e);
		}

		return operatorCircleReadDAO.getCircle(circleId);
	}

    @Transactional
    public CircleMaster getCircle(String circleName) {
        if (FCUtil.isEmpty(circleName)) {
            return null;
        }
        try {
            CircleMaster circleMaster = inCachedService.getCirclesByName(circleName);
            if (circleMaster != null) {
                return circleMaster;
            } else {
                inCachedService.setCircleByField(operatorCircleReadDAO.getCircles());
            }
        } catch (Exception e) {
            logger.error("error in getting circle from cache: circleName: " + circleName, e);
        }
        return operatorCircleReadDAO.getCircle(circleName);
    }

	// Operator service

	@Transactional
	public List<OperatorMaster> getOperators() {
	List<OperatorMaster> operatorMasters = null;
		try {
			List<OperatorMaster> operatorMaster = inCachedService.getOperators();
			if (operatorMaster != null) {
				return operatorMaster;
			} else {
				operatorMasters = operatorCircleReadDAO.getOperators();
				inCachedService.setOperators(operatorMasters);
			}
		} catch (Exception e) {
            logger.error("error in getting operators from cache: ", e);
			operatorMasters = operatorCircleReadDAO.getOperators();
		}
		return operatorMasters;
	}

	@Transactional
	public OperatorMaster getOperator(Integer operatorId) {
		try {
			OperatorMaster operatorMaster = inCachedService.getOperatorsByFieldID(operatorId);
			if (operatorMaster != null) {
				return operatorMaster;
			} else {
				inCachedService.setOperatorsByField(operatorCircleReadDAO.getOperators());
			}
		} catch (Exception e) {
            logger.error("error in getting operator from cache: operatorId: "+operatorId, e);
		}
		return operatorCircleReadDAO.getOperator(operatorId);
	}

	@Transactional
	public OperatorMaster getOperator(String operator) {
		try {
			OperatorMaster operatorMaster = inCachedService.getOperatorsByFieldCode(operator);
			if (operatorMaster != null) {
				return operatorMaster;
			} else {
				inCachedService.setOperatorsByField(operatorCircleReadDAO.getOperators());
			}
		} catch (Exception e) {
            logger.error("error in getting operator from cache: Operator: "+operator, e);
		}
		return operatorCircleReadDAO.getOperator(operator);
	}

	@Transactional
	public List<OperatorMaster> getOperatorsByProductId(Integer productId) {
		try {
			List<OperatorMaster> operatorMaster = inCachedService.getOperatorsByProductID(productId);
			if (operatorMaster != null) {
				return operatorMaster;
			} else {
				inCachedService.setOperatorsByField(operatorCircleReadDAO.getOperators());
			}
		} catch (Exception e) {
            logger.error("error getting getOperatorsByProductId: "+productId, e);
		}
		return operatorCircleReadDAO.getOperatorsByProductId(productId);
	}

	@Transactional
	public List<Map<String, Object>> getAllOperators() {
		List<Map<String, Object>> listOfMap = null; 
		try {
			logger.info("getting list of operators from cache");
			List<Map<String, Object>> list = inCachedService.getAllOperators();
			if (list != null && list.size() > 0) {
				logger.info("Listing objects of list "+list.toString());
				return list;
			} else {
				logger.info("Couldn't read from cache npow trying from DB to Retrieve all Operators");
				listOfMap = operatorCircleReadDAO.getAllOperators();
				logger.info("got the following list of operators from DB "+listOfMap +"\n setting the same in the Cache");
				inCachedService.setAllOperators(listOfMap);
			}
		} catch (Exception e) {
            logger.error("error getting all operators from cache. returning from db", e);
			listOfMap = operatorCircleReadDAO.getAllOperators();
		}
		return listOfMap;
	}

	@Transactional
	public List<Map<String, Object>> getOperatorCircleByPrefix() {
		List<Map<String, Object>> listOfMap = null;
		try {
			List<Map<String, Object>> list = inCachedService.getAllOperatorCircleByPrefix();
			if (list != null && list.size() > 0) {
				return list;
			} else {
				listOfMap = operatorCircleReadDAO.getOperatorCircleByPrefix();
				inCachedService.setAllOperatorCircleByPrefix(listOfMap);
			}
		} catch (Exception e) {
            logger.error("error in getOperatorCircleByPrefix getting from cache.", e);
			listOfMap = operatorCircleReadDAO.getOperatorCircleByPrefix();
		}
		return listOfMap;
	}

	@Transactional
	public List<Map<String, Object>> getAllPrefixAllOperatorAllCircle() {
		List<Map<String, Object>> listofmap = null;
		try {
			List<Map<String, Object>> list = inCachedService.getAllPrefixAllOperatorAllCircle();
			if (list != null && list.size() > 0) {
				return list;
			} else {
				listofmap = operatorCircleReadDAO.getAllPrefixAllOperatorAllCircle();
				inCachedService.setAllPrefixAllOperatorAllCircle(listofmap);
			}
		} catch (Exception e) {
            logger.error("error in getAllPrefixAllOperatorAllCircle. cache prb", e);
			listofmap = operatorCircleReadDAO.getAllPrefixAllOperatorAllCircle();
		}
		return listofmap;
	}

	// Circle Operator Mapping

	@Transactional
	public CircleOperatorMapping getCircleOperatorMapping(Integer circleOperatorMappingId) {
		try {
			CircleOperatorMapping circleOperatorMapping = inCachedService.getCircleOperatorMapping(circleOperatorMappingId);
			if (circleOperatorMapping != null) {
				return circleOperatorMapping;
			} else {
				inCachedService.setCircleOperatorMapping(operatorCircleReadDAO.getCircleOperatorMapping());
			}
		} catch (Exception e) {
            logger.error("error in getting circleOperatorMapping: "+circleOperatorMappingId, e);
		}
		return operatorCircleReadDAO.getCircleOperatorMapping(circleOperatorMappingId);
	}

	@Transactional
	public CircleOperatorMapping getCircleOperatorMapping(Integer operatorId, Integer circleId) {
		try {
			CircleOperatorMapping circleOperatorMapping = inCachedService.getCircleOperatorMapping(operatorId, circleId);
			if (circleOperatorMapping != null) {
				return circleOperatorMapping;
			} else {
				inCachedService.setCircleOperatorMapping(operatorCircleReadDAO.getCircleOperatorMapping());
			}
		} catch (Exception e) {
            logger.error("error in getCircleOperatorMapping: opearatorId"+operatorId+" circleId: "+circleId, e);
		}
		return operatorCircleReadDAO.getCircleOperatorMapping(operatorId, circleId);
	}

	// Aggregator Operator Circle Mapping

	@Transactional
	public List<InAggrOprCircleMap> getInAggrOprCircleMap(Integer operatorId, Integer circleId) {
		try {
			logger.info("Fetching aggr map for key " + operatorId + '-' + circleId);
			List<InAggrOprCircleMap> inAggrOprCircleMap = inCachedService.getInAggrOprCircleMapByID(operatorId, circleId);
			if (inAggrOprCircleMap != null) {
				return inAggrOprCircleMap;
			} else {
				logger.info("Re-instantiating aggr operator circle map");
				inCachedService.setInAggrOprCircleMap(operatorCircleReadDAO.getInAggrOprCircleMap());
			}
		} catch (Exception e) {
            logger.error("error in getInAggrOprCircleMap: opearatorId"+operatorId+" circleId: "+circleId, e);
		}

		return operatorCircleReadDAO.getInAggrOprCircleMap(operatorId, circleId);
	}

	@Transactional
	public InOprAggrPriority getInOprAggrPriority(Integer circleOperatorMappingId) {

		try {
			InOprAggrPriority inOprAggrPriority = inCachedService.getInOprAggrPriorityByID(circleOperatorMappingId);
			if (inOprAggrPriority != null) {
				return inOprAggrPriority;
			} else {
				inCachedService.setInOprAggrPriority(operatorCircleReadDAO.getInOprAggrPriorities());
			}
		} catch (Exception e) {
            logger.error("error in getInOprAggrPriority circleOperatorMappingId: "+circleOperatorMappingId);
		}
		return operatorCircleReadDAO.getInOprAggrPriority(circleOperatorMappingId);
	}
	
	// Circle Service
	@Transactional
	public List<ProductMaster> getProducts() {
		List<ProductMaster> productMasters = null;
		try {
			List<ProductMaster> productMaster = inCachedService.getProducts();
			if (productMaster != null) {
				return productMaster;
			} else {
				productMasters = operatorCircleReadDAO.getProducts();
				inCachedService.setProducts(productMasters);
			}
		} catch (Exception e) {
			productMasters = operatorCircleReadDAO.getProducts();
		}
		return productMasters;
	}
	
	// Names Service
	@Transactional
	public List<String> getNames() {
		List<String> nameMasters = null;
		try {
			List<String> nameMaster = inCachedService.getNames();
			if (nameMaster != null) {
				return nameMaster;
			} else {
				nameMasters = Arrays.asList("Topup","Data/2G","3G","Other");
				inCachedService.setNames(nameMasters);
			}
		} catch (Exception e) {
			nameMasters = Arrays.asList("Topup","Data/2G","3G","Other");
			logger.warn("Exception when trying to fetch from Cache.",e);
		}
		return nameMasters;
	}
	
	
    @Transactional(readOnly = true)
    private List<Map<String, Object>> getOperatorCircleByPrefix(String prefix, String productMasterId) {
        List<Map<String,Object>> returnValue = prefixMappingCache.getFromCache(prefix, productMasterId);
        
        if (returnValue == null) {
            returnValue = operatorCircleReadDAO.getOperatorCircleByPrefix(prefix, productMasterId);
            
            if (returnValue != null && !returnValue.isEmpty()) {
                prefixMappingCache.cacheOperatorCircleByPrefix(prefix, productMasterId, returnValue);
            }
            
        }
        
        return returnValue;
    }

    public String getCircleNameForServiceNumAndProduct(String serviceNum, String productType) {

        String prefixVal = serviceNum.substring(0, 4);
        List<Map<String, Object>> returnValList = this.getOperatorCircleByPrefix(prefixVal, FCConstants.productMasterMapReverse.get(productType).toString());
        if (returnValList == null || returnValList.isEmpty()) {
            return null;
        }

        Map<String, Object> returnVal = returnValList.get(0);
        String circleName = (String) returnVal.get("circleName");
        return circleName;
    }

    private List<Map<String, Object>> getAllCircleList() {
        List<Map<String, Object>> allCircles = new ArrayList<Map<String, Object>>();
        List<CircleMaster> circleMasterList = getCircles();
        Iterator<CircleMaster> iterator = circleMasterList.iterator();
        while (iterator.hasNext()) {
            CircleMaster circleMaster = iterator.next();
            Map<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("circleMasterId", circleMaster.getCircleMasterId() + "");
            dataMap.put("circleName", circleMaster.getName());
            allCircles.add(dataMap);
        }
        return allCircles;
    }
    
    /*
     * IN case of when circle is not present into txn_home_page table
     */
    public String getCircle(String serviceNumber, String circle) {
        String circleName=circle;
        if(serviceNumber!=null && serviceNumber.length()>4) {
            String prefixValue = serviceNumber.substring(0,  4);
            List<Map<String, Object>> listobject= operatorCircleReadDAO.getOperatorCircleByPrefixId(prefixValue);
            if(listobject!=null && listobject.size()>0) {
                    Map<String, Object> map= listobject.get(0);
                    String circleMasterId = map.get("circleMasterId").toString();
                    CircleMaster circleMaster = this.getCircle(Integer.parseInt(circleMasterId));
                    circleName = circleMaster.getName();
                    return circleName;
            }
        }
        return circleName;
    }              

    public Map<String, Object> getOperatorPrefixStateMap(String prefix, Integer productId){
        return this.operatorCircleReadDAO.getOperatorStateByPrefixAndProduct(prefix, productId);
    }
    /**
	 * Find operator details by mobile number and return it; return NULL if not
	 * found.
	 * @param mobilePrefix
	 * @return
	 */
	public Map<String, Object> findOperatorByMobilePrefix(String mobilePrefix) {
		if (FCUtil.isEmpty(mobilePrefix)) {
			throw new IllegalArgumentException(
					String.format("Expected non-empty mobile prefix, but found %s",
							mobilePrefix==null? "NULL":
								String.format("'%s'", mobilePrefix)));
		}
		List<Map<String, Object>> operatorCircleData = this.getOperatorCircleByPrefix(mobilePrefix.substring(0, 4), "1");
		if (operatorCircleData.size() == 1) {
			return operatorCircleData.get(0);
		}
		return null;
	}
	
	public List<Map<String, Object>> getOperatorCircleByPrefixId(String serviceNumber) {
	    String prefixValue = serviceNumber.substring(0,  4);
	    List<Map<String, Object>> listobject= operatorCircleReadDAO.getOperatorCircleByPrefixId(prefixValue);
	    return listobject;
	}
	
	public boolean isValidOperator(int productId, String operatorIdString) {
	    if (!StringUtils.isNumeric(operatorIdString)) {
	        return false;
	    }
	    
	    int operatorId = Integer.parseInt(operatorIdString);
	    
	    OperatorMaster operatorMaster = this.getOperator(operatorId);
	    
	    if (operatorMaster == null) {
	        return false;
	    }
	    
	    if (!operatorMaster.getIsActive()) {
	        return false;
	    }
	    
	    if (operatorMaster.getFkProductMasterId().intValue() != productId) {
	        return false;
	    }
	    
	    return true;
	}
	
	/**
	 * Returns non-empty value if the given service number is ported.
	 * @param serviceNumber
	 * @param productType
	 * @return
	 */
	public List<Map<String, Object>> getMNPOperator(String serviceNumber, String productType) {
	    List<Map<String, Object>> prefixData = fetchFromDynamo(serviceNumber, productType);
        
        if (!prefixData.isEmpty()) {
            return prefixData;
        } else {
            OperatorCircleBusinessDO getPrefixRequest = new OperatorCircleBusinessDO();
            getPrefixRequest.setPrefix(serviceNumber);
            getPrefixRequest.setProductType(productType);
            
            this.getOperatorCircleList(getPrefixRequest);
            
            return getPrefixRequest.getPrefixData();
        }
	}

    private List<Map<String, Object>> fetchFromDynamo(String serviceNumber, String productType) {
        GetItemRequest getItemRequest = new GetItemRequest("mnpMap", ImmutableMap.of("serviceNumber", new AttributeValue(serviceNumber)));
	    getItemRequest.setConsistentRead(false);
	    
        final long startTime = System.currentTimeMillis();
        
        List<Map<String, Object>> prefixData = new ArrayList<>();
        
        try {
            GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);

            metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
            metricsClient.recordEvent("DynamoDB", "GetItem", "Success");
            
            prefixData = createOperatorMapping(getItemResult, productType);
        } catch (AmazonServiceException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonServiceException while fetching MNP", e);
        } catch (AmazonClientException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonClientException while fetching MNP", e);
        }
        return prefixData;
    }

    private List<Map<String, Object>> createOperatorMapping(GetItemResult getItemResult, String productType) {
        List<Map<String, Object>> response = new ArrayList<>();
        Map<String, Object> operatorMapping = new HashMap<>();

        if (getItemResult !=null && getItemResult.getItem() != null) {
            Map<String, AttributeValue> mnpItem = getItemResult.getItem();

            final String mnpCircle = mnpItem.get("ci").getS();
            final String mnpProduct = mnpItem.get("pr").getS();
            final String mnpOperator = mnpItem.get("oi").getS();
            final String mnpNumber = mnpItem.get("serviceNumber").getS();
            
            if (!StringUtils.equals(productType, mnpProduct)) {
                metricsClient.recordEvent("Recharge", "MNP", "ProductMisMatch." + mnpProduct + "." + productType);
                return response;
            }
            CircleMaster circleMaster = null;
            try {
                circleMaster = this.getCircle(Integer.parseInt(mnpCircle));
            } catch (NumberFormatException e) {
                // mnpCircle is not a valid integer maybe "NA"
            }
            operatorMapping.put("prefixValue", mnpNumber);
            operatorMapping.put("productMasterId", mnpProduct);
            operatorMapping.put("operatorMasterId", mnpOperator);
            operatorMapping.put("circleMasterId", mnpCircle);
            if (circleMaster != null) {
                operatorMapping.put("circleName", circleMaster.getName());
            } else {
                circleMaster = getCircle(Integer
                        .parseInt(fetchCircleMasterId(mnpNumber)));
                operatorMapping.put("circleName", circleMaster.getName());
                operatorMapping.put("circleMasterId", circleMaster.getCircleMasterId());
            }
            response.add(operatorMapping);
            
            metricsClient.recordEvent("Recharge", "MNP.Match", productType + "." + mnpOperator + "." + mnpCircle);
        }
        
        return response;
    }
	
    public boolean isExcludedForAutoDetection(String operatorId) {
        JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);
        
        boolean operatorExcluded = false;

        if (configJson != null) {
            final String excludedOperatorString = (String) configJson.get(NO_AUTO_SELECT_OPERATORS);
            
            if (StringUtils.isNotBlank(excludedOperatorString)) {
                List<String> excludedOperators = Arrays.asList(excludedOperatorString.split(","));
                
                if (excludedOperators.contains(operatorId)) {
                    operatorExcluded = true;
                }
                
            }
            
        }
        return operatorExcluded;
    }
    
    public String fetchCircleMasterId(String serviceNumber) {
        if (serviceNumber != null && serviceNumber.length() > 4) {
            String prefixValue = serviceNumber.substring(0, 4);
            List<Map<String, Object>> listobject = operatorCircleReadDAO.getOperatorCircleByPrefixId(prefixValue);
            if (listobject != null && listobject.size() > 0) {
                Map<String, Object> map = listobject.get(0);
                return map.get("circleMasterId").toString();
            }
        }
        return null;
    }

    public Map<String, Object> getPrePaidServiceProvider(String mobNumber) {

        Map<String, Object> returnData = new HashMap<>();
        // Prepaid Mobile number only.
        String productType = RechargeConstants.MOBILE_PRODUCT_TYPE;
        String productId = Integer.toString(ProductName.fromPrimaryProductType(productType).getProductId());
        List<Map<String,Object>> mnpData = this.getMNPOperator(mobNumber, productId);
        List<OperatorAlert> operatorAlerts = null;
        if (!FCUtil.isEmpty(mnpData)) {
            String operatorId = mnpData.get(0).get("operatorMasterId") == null ? null : mnpData.get(0)
                    .get("operatorMasterId").toString();

            boolean operatorExcluded = this.isExcludedForAutoDetection(operatorId);

            if (operatorExcluded) {
                returnData.put("prefixData", new ArrayList<>());
                return returnData;
            }

            String circleId = mnpData.get(0).get("circleMasterId") == null ? null : mnpData.get(0)
                    .get("circleMasterId").toString();
            if (operatorId != null && circleId != null) {
                operatorAlerts = operatorAlertService
                        .processAndGetOperatorAlert(Integer.parseInt(operatorId), circleId);
           }
        }
        if (!FCUtil.isEmpty(operatorAlerts)) {
            returnData.put("operatorAlert", operatorAlerts.get(0));
        }
        returnData.put("prefixData", mnpData);

        return returnData;
    }

	public Map<String, Object> getProductTypeData(String serviceNumber, String productType) {
		Map<String, Object> productTypeData;
		productTypeData = fetchFromDynamoProductTypeData(serviceNumber);
		if (!FCUtil.isEmpty(productTypeData)) {
			if (FCUtil.isMobileRecharge(productType)) {
				String dynamoProduct = (String) productTypeData.get("productType");
				if (FCUtil.isMobileRecharge(dynamoProduct)) {
					return productTypeData;
				} else {
					return new HashMap<String, Object>();
				}
			} else if (FCUtil.isDataCard(productType)) {
				String dynamoProduct = (String) productTypeData.get("productType");
				if (FCUtil.isDataCard(dynamoProduct)) {
					return productTypeData;
				} else {
					return new HashMap<String, Object>();
				}
			}
		}
		return productTypeData;

	}

    public Map<String, Object> fetchFromDynamoProductTypeData(String serviceNumber) {
    	
        GetItemRequest getItemRequest = new GetItemRequest("serviceNumberProductMap", ImmutableMap.of("serviceNumber", new AttributeValue(serviceNumber)));
        getItemRequest.setConsistentRead(false);
        
        final long startTime = System.currentTimeMillis();
        
        Map<String, Object> productPredictorData = new HashMap<String,Object>();
        
        try {
            GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);

            metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
            metricsClient.recordEvent("DynamoDB", "GetItem", "Success");
            
            productPredictorData = createProductPredictionMapping(getItemResult);
            logger.info("Fetched data from Product Prediction is "+productPredictorData);
        } catch (AmazonServiceException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonServiceException while fetching ProductPredictorData", e);
        } catch (AmazonClientException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
            logger.error("AmazonClientException while fetching ProductPredictorData", e);
        }
        return productPredictorData;
      
    }

    public Map<String, Object> createProductPredictionMapping(GetItemResult getItemResult) {
        
        Map<String, Object> response = new HashMap<>();

        if (getItemResult != null && getItemResult.getItem() != null) {
            Map<String, AttributeValue> mappingItem = getItemResult.getItem();

            final String serviceNumber = mappingItem.get("serviceNumber").getS();
            final String productType = mappingItem.get("productType").getS();
            final String operatorId = mappingItem.get("operatorId").getS();
            final String circleId = mappingItem.get("circleId").getS();

            response.put("serviceNumber", serviceNumber);
            response.put("productType", productType);
            response.put("operatorId", operatorId);
            response.put("circleId", circleId);

        }

        return response;
    }

   
}
