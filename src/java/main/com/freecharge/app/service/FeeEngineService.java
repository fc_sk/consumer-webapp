package com.freecharge.app.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Timer;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.freecharge.web.webdo.CalculateChargeRequest;
import com.freecharge.web.webdo.CalculateChargeResponse;
import com.freecharge.common.util.ServiceResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.ClientDetails;
import com.freecharge.app.domain.entity.HttpClientEntity;
import com.freecharge.app.service.HttpFeeEngineConnectionFactory;
import com.freecharge.common.util.FeeEngineUtil;
import com.freecharge.web.webdo.FeeEngineException;

//@Service
public class FeeEngineService {
	
	
	
	//TODO : Read clientName, key and url while initaliazing the service at the caller point.
	private Logger log = LoggingFactory.getLogger(getClass());
	
	private ClientDetails clientDetails;
	private HttpClientEntity httpClientEntity;
	private HttpFeeEngineConnectionFactory httpClientFactory = HttpFeeEngineConnectionFactory.getInstance();
	private Timer timer;
	
	public FeeEngineService(String serviceHostUrl, String clientName, String clientKey) throws Exception {
		ClientDetails details = new ClientDetails();
		details.setUrl(serviceHostUrl);
		details.setClientName(clientName);
		details.setClientKey(clientKey);
		this.clientDetails = details;
		intializeHttpClient(details);
	}
	
	private void intializeHttpClient(ClientDetails clientDetails)
			throws UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException,
			CertificateException, FileNotFoundException, IOException {
		this.httpClientEntity = httpClientFactory.createHttpClient(clientDetails);
	}


	public CalculateChargeResponse calculateFeeCharge(CalculateChargeRequest request) {
		
		String requestUri = clientDetails.getUrl() + "/feeengine/calculateCharge";
		return processPostRequest(requestUri, request, new TypeReference<ServiceResponse<CalculateChargeResponse>>() {
		});
		
	}
	
	@SuppressWarnings({ "unchecked" })
	private <T, R> R processPostRequest(String requestUri, T request, TypeReference<ServiceResponse<R>> typeReference)
			throws FeeEngineException {

		ServiceResponse<R> responseBody = null;
		try {
			responseBody = (ServiceResponse<R>) FeeEngineUtil.getInstance().processHttpRequest(requestUri,
					typeReference, request, httpClientEntity.getHttpClient(), clientDetails);
		} catch (Exception e) {
			log.error("exception caught", e);
		}
		if(responseBody != null) {
			R json = responseBody.getResponse();
			return json;
		}
		return null;
		
	}

}
