package com.freecharge.app.service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.app.service.exception.CampaignConditionException;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.web.webdo.UpiPaymentData;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.exception.CardValidationException;
import com.freecharge.app.service.exception.FraudDetectedException;
import com.freecharge.app.service.exception.InvalidPaymentRequestException;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.PaymentOptionListBusinessDO;
import com.freecharge.common.businessdo.PaymentOptionsBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCInfraUtils;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.GSTService;
import com.freecharge.freefund.services.UtmTrackerService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.mobile.web.util.ConvenienceFeeUtil;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.FcUpiGatewayHandler;
import com.freecharge.payment.services.KlickPayGatewayHandler;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.services.binrange.CardBinRange;
import com.freecharge.payment.services.binrange.CardBinRangeService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletController;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.ProductPaymentWebDO;
import com.freecharge.zeropayment.ZeroPaymentController;

/**
 * Created by IntelliJ IDEA. User: user Date: May 5, 2012 Time: 11:51:29 AM To
 * change this template use File | Settings | File Templates.
 */
@Service
public class PaymentService {

    public static final String            METRICS_SERVICE      = "Payment";
    public static final String            METRIC_PAYMENT       = "Attempt";
    public static final String            ERROR_CODE           = "errorCode";
    public static final String            ERROR_MSG            = "errorMessage";
    public static final String            METRIC_ATTEMPT       = "attempt";
    public static final String            METRIC_SUCCESS       = "success";
    public static final String            METRIC_FAILURE       = "failure";
    public static final String            METRIC_REQUEST       = "Request";
    public static final String            METRIC_REQUEST_START = "start";
    public static final String            METRIC_REQUEST_END   = "end";

    @Autowired
    HomeBusinessDao                       homeBusinessDao;

    @Autowired
    private PaymentTransactionService     paymentTransactionService;

    @Autowired
    private MetricsClient                 metricsClient;

    @Autowired
    private CardBinRangeService           cardBinRangeService;

    @Autowired
    private FCProperties                  fcProperties;

    @Autowired
    private CheckoutService               checkoutService;

    @Autowired
    private WalletController              walletController;

    @Autowired
    private ZeroPaymentController         zeroPaymentController;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private CartService                   cartService;

    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService               billPayServiceRemote;

    @Autowired
    private TxnHomePageService            txnHomePageService;

    @Autowired
    private OrderService                  orderService;

    @Autowired
    private PricingService                pricingService;

    @Autowired
    UserServiceProxy                      userServiceProxy;

    @Autowired
    OneCheckWalletService                 oneCheckWalletService;
    
    @Autowired
    FcUpiGatewayHandler                   fcUpigatewayHandler;

    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Autowired
    private AppConfigService appConfigService;
    
    private ExecutorService campaignExecutor = Executors.newCachedThreadPool();
	 
	private final Logger logger = LoggingFactory.getLogger(getClass());

    public void getPaymentOptions(BaseBusinessDO baseBusinessDO) {
        List<PaymentTypeMaster> paymentTypeMastersList = paymentTransactionService.getPaymentOptions();
        ((PaymentOptionsBusinessDO) baseBusinessDO).setPaymentTypeMastersList(paymentTypeMastersList);
    }

    public String getPaymentTypeString(Integer paymentTypeCode) {
        return PaymentTypeURLName.fromPaymentTypeCode(paymentTypeCode);
    }

    private static enum PaymentTypeURLName {
        CREDIT_CARD(1, "creditCard"), DEBIT_CARD(2, "debitCard"), NET_BANKING(3, "netBanking"), CASH_CARD(4,
                "cashCard"), ATM_CARD(5, "atmCard"), FC_BALANCE(6, "fcBalance"), HDFC_DEBIT_CARD(7,
                        "hdfcDebitCard"), ZERO_PAY(8, "zeroPay"), ONE_CHECK_WALLET(9, "ocWallet"), FC_UPI(16, "fcUpi");
        private final int paymentTypeCode;
        private String    urlString;

        PaymentTypeURLName(int code, String text) {
            this.paymentTypeCode = code;
            this.urlString = text;
        }

        public String getUrlString() {
            return this.urlString;
        }

        public int getPaymentTypeCode() {
            return this.paymentTypeCode;
        }

        public static PaymentTypeURLName fromString(String text) {
            if (text != null) {
                for (PaymentTypeURLName b : PaymentTypeURLName.values()) {
                    if (text.equalsIgnoreCase(b.urlString)) {
                        return b;
                    }
                }
            }
            return null;
        }

        public static String fromPaymentTypeCode(int paymentTypeCode) {
            for (PaymentTypeURLName b : PaymentTypeURLName.values()) {
                if (paymentTypeCode == b.paymentTypeCode) {
                    return b.urlString;
                }
            }
            return null;
        }
    }

    @Transactional
    public void getPaymentOptionTypeList(BaseBusinessDO baseBusinessDO) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String type = FCSessionUtil.getRechargeType(baseBusinessDO.getLookupID(), fs);
        if (!FCUtil.isEmpty(type)) {
            Integer productId = 0;
            if (type.equals("V")) {
                productId = 1;
            } else if (type.equals("D")) {
                productId = 2;
            } else if (type.equals("C")) { // added for confusion in C and DC
                                           // for datacards, need to remove one
                                           // confirmed
                productId = 3;
            } else if (type.equals("F")) {
                productId = 12;
            } else if (type.equals("DC")) {
                productId = 3;
            }
            ((PaymentOptionListBusinessDO) baseBusinessDO).setProductMasterId(productId);
        } else {
            ((PaymentOptionListBusinessDO) baseBusinessDO).setProductMasterId(1);
        }
        List<Map<String, Object>> list = paymentTransactionService.getPaymentOptionsList(baseBusinessDO);
        ((PaymentOptionListBusinessDO) baseBusinessDO).setPaymentOptionsList(list);
    }

    @Transactional
    public void getPaymentTypeOptionsForWeb(String productType, Integer affiliateId) {
        Integer productId = FCConstants.productMasterMapReverse.get(productType);
        String key = productType + "_" + affiliateId.toString();

        if (FCConstants.paymentTypesForProductAffiliate.get(key) == null) {
            List<PaymentTypeMaster> ptms = paymentTransactionService.getPaymentType(productId, affiliateId);
            List<PaymentTypeMasterBusinessDO> ptmbs = null;
            if (ptms != null && ptms.size() > 0) {
                ptmbs = new ArrayList<PaymentTypeMasterBusinessDO>();
                for (PaymentTypeMaster ptm : ptms) {
                    PaymentTypeMasterBusinessDO ptmb = new PaymentTypeMasterBusinessDO();
                    ptmb.setCode(ptm.getCode());
                    ptmb.setId(ptm.getPaymentTypeMasterId().longValue());
                    ptmb.setName(ptm.getName());
                    ptmb.setRelationId(ptm.getRelationId());
                    ptmbs.add(ptmb);
                    if (FCConstants.paymentTypesForRelation.get(ptm.getRelationId()) == null) {
                        List<PaymentTypeOptions> ptos = paymentTransactionService.getPaymentType(ptm.getRelationId());
                        List<PaymentTypeOptionBusinessDO> ptobs = null;
                        if (ptos != null && ptos.size() > 0) {
                            ptobs = new ArrayList<PaymentTypeOptionBusinessDO>();
                            for (PaymentTypeOptions pto : ptos) {
                                PaymentTypeOptionBusinessDO ptob = new PaymentTypeOptionBusinessDO();
                                ptob.setCode(pto.getCode());
                                ptob.setDisplayImg(pto.getDisplayImg());
                                ptob.setDisplayName(pto.getDisplayName());
                                ptob.setId(pto.getPaymentTypeOptionsId().longValue());
                                ptob.setImgUrl(pto.getImgUrl());
                                ptobs.add(ptob);
                            }
                        }
                        FCConstants.paymentTypesForRelation.put(ptm.getRelationId(), ptobs);
                    }
                }
            }
            FCConstants.paymentTypesForProductAffiliate.put(key, ptmbs);
        }
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public PaymentResponseBusinessDO getUserDetailsByUsingOrderId(PaymentResponseBusinessDO paymentResponseBusinessDO) {
        try {
            String orderNo = null;
            Map<String, Object> dataMap = (Map<String, Object>) paymentResponseBusinessDO.getResultMap()
                    .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
            orderNo = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
            logger.info("orderid in payment Service " + orderNo);
            if (orderNo != null && !orderNo.isEmpty()) {
                Map<String, Object> userDetail = homeBusinessDao.getUserDetailsFromOrderId(orderNo);
                if (userDetail != null && userDetail.size() > 0) {
                    // TBD: This sort of creating HTML here is not a good thing.
                    // Its not used now in rechargecomplete.jsp. Remove
                    // shippingaddress if unused anywhere.
                    StringBuffer address = new StringBuffer("");
                    address.append((String) userDetail.get("deliveryNmae") + "<br>");
                    if ((String) userDetail.get("address") != null)
                        address.append((String) userDetail.get("address") + ",");
                    address.append((String) userDetail.get("deliveryArea") + ",");
                    if ((String) userDetail.get("deliveryLandmark") != null)
                        address.append((String) userDetail.get("deliveryLandmark") + "(Landmark) ,");
                    address.append(userDetail.get("deliveryCity") + "<br>" + (String) userDetail.get("stateName") + ","
                            + (String) userDetail.get("deliveryPinCode"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_SHIPPING_ADDRESS, address.toString());
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_RECHARGE_MOBILE_NUMBER,
                            (String) userDetail.get("mobileno"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_EMAIL_ID, (String) userDetail.get("email"));

                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_NAME, (String) userDetail.get("deliveryNmae"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_ADDRESS, (String) userDetail.get("address"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_AREA, (String) userDetail.get("deliveryArea"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_LANDMARK,
                            (String) userDetail.get("deliveryLandmark"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_CITY, (String) userDetail.get("deliveryCity"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_STATE, (String) userDetail.get("stateName"));
                    dataMap.put(PaymentConstants.PAYMENT_PORTAL_CUSTOMER_PINCODE,
                            (String) userDetail.get("deliveryPinCode"));
                }
            } else {
                metricsClient.recordEvent("Payment", "Attempt." + paymentResponseBusinessDO.getPaymentType(),
                        "orderIdNullDelegate");
            }
            paymentResponseBusinessDO.getResultMap().put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, dataMap);
        } catch (Exception e) {
            logger.error(
                    "Error occur while adding the user address and recharge mobile number in to the PaymentResponseBusinessDO result map");
        }
        return paymentResponseBusinessDO;
    }

    public void validateAndSetCardDetails(ProductPaymentWebDO productPaymentWebDO) {
        productPaymentWebDO.setCardDetails();
        if (shouldDeterminePaymentOption(productPaymentWebDO)) {
            String binHead = StringUtils.left(productPaymentWebDO.getCardNumber(), 6);
            String binTail = StringUtils.right(productPaymentWebDO.getCardNumber(), 4);
            CardBinRange cardBinRange = cardBinRangeService.getCardBinRange(binHead, binTail);
            if (PaymentConstants.cardTypeMasterMap.get(PaymentConstants.PAYMENT_TYPE_CREDITCARD)
                    .equals(cardBinRange.getCardNature())) {
                productPaymentWebDO.setPaymenttype(PaymentConstants.PAYMENT_TYPE_CREDITCARD);
                productPaymentWebDO.setPyop("");
                productPaymentWebDO.setDebitCard(false);
            } else {
                productPaymentWebDO.setPaymenttype(PaymentConstants.PAYMENT_TYPE_DEBITCARD);
                productPaymentWebDO.setDebitCard(true);
                String pyop = getDebitCardPyop(cardBinRange, productPaymentWebDO.getCardType());
                productPaymentWebDO.setPyop(pyop);
            }
            productPaymentWebDO.setCardType(productPaymentWebDO.getCardType().substring(0, 1));
        }
    }

    public void updateTransactionHistoryForTransaction(String orderId, int userId, String responseCode) {
    	
    	logger.info("Updating Txn History for Order:" + orderId + "|ResponseCode:" + responseCode);
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                .findUserTransactionHistoryByOrderId(orderId);
        if (userTransactionHistory != null
            && PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS.equalsIgnoreCase(userTransactionHistory
                .getTransactionStatus())) {
            return;
        }
        if (userTransactionHistory != null && !PaymentConstants.paymentTransactionStatusToExclude
                .contains(userTransactionHistory.getTransactionStatus())) {
            return;
        }
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
        TxnHomePageBusinessDO homePage = txnHomePageService.getTransactionDetails(cartBusinessDo.getLookupID());
        String productType = null;
        Map<String, Object> txnDetailMap = new HashMap<>();
        /*
         * txn_home_page is null for HCoupons so pull data from some other table
         */
        if (homePage == null || homePage.getAmount() == null) {
            OrderId orderIdObj = orderService.getByOrderId(orderId);
            ProductName productName = ProductName.fromProductId(orderIdObj.getFkProductMasterId());
            Float amountPaid = pricingService.getPayableAmountByOrderId(orderId).floatValue();
            productType = productName.getProductType();
            txnDetailMap.put("amount", amountPaid);
            txnDetailMap.put("mobileno", "");
            txnDetailMap.put("operatorName", "");
            txnDetailMap.put("circleName", "");
        } else {
            productType = homePage.getProductType();
            txnDetailMap.put("operatorName", homePage.getOperatorName());
            txnDetailMap.put("circleName", homePage.getCircleName());
            txnDetailMap.put("amount", homePage.getAmount());
            txnDetailMap.put("mobileno", homePage.getServiceNumber());
        }

        txnDetailMap.put("userId", userId);
        txnDetailMap.put("productType", productType);

        if (FCUtil.isUtilityPaymentType(productType)) {
            BillWithSubscriberNumber billWithSubscriberNumber = billPayServiceRemote
                    .getBillDetails(homePage.getServiceNumber());
            if (billWithSubscriberNumber != null) {
                txnDetailMap.put("mobileno", billWithSubscriberNumber.getAdditionalInfo1());
            }
        }

        userTransactionHistoryService.createOrUpdateRechargeHistory(orderId, txnDetailMap, responseCode);
    	logger.info("Updated Txn History for Order:" + orderId + "|ResponseCode:" + responseCode);
    }

    private boolean shouldDeterminePaymentOption(ProductPaymentWebDO productPaymentWebDO) {
        if (FCUtil.isEmpty(productPaymentWebDO.getCcNum()) && FCUtil.isEmpty(productPaymentWebDO.getDbNum())
                && !FCUtil.isEmpty(productPaymentWebDO.getCardType())
                && !PaymentUtil.isStoredCardPayment(productPaymentWebDO)
                && PaymentUtil.isCardPayment(productPaymentWebDO.getPaymenttype())) {
            return true;
        }
        return false;
    }

    private String getDebitCardPyop(CardBinRange cardBinRange, String cardType) {
        String pyop = "";
        cardType = cardType == null ? "" : cardType;
        if (PaymentConstants.debitCardNonMaestroList.contains(cardType)) {
            pyop = PaymentConstants.debitCardStorageMasterMap.get(cardType);
        } else if (PaymentConstants.debitCardMaestroText.equalsIgnoreCase(cardType)) {
            String cardIssuerName = cardBinRangeService.getCardIssuerFromId(cardBinRange.getCardIssuerId());
            if (cardIssuerName != null && cardIssuerName.toLowerCase().equals("state bank of india")) {
                pyop = PaymentConstants.debitCardStorageMasterMap.get(cardType + "_SBI");
            } else {
                pyop = PaymentConstants.debitCardStorageMasterMap.get(cardType);
            }
        } else {
            metricsClient.recordEvent("Payment", "AutoDetectCard.pyopMapError", cardType);
        }
        return pyop;
    }

    /**
     * Method logs the PG to which the request should be redirected
     * 
     * @param
     */
    private void logRequestToPGMetric(String redirectingToPG, int channel) {
        if (redirectingToPG != null) {
            metricsClient.recordEvent("Payment", "PG.Request." + channel, redirectingToPG);
        }
    }

    /**
     * Method to log completion of payment request
     * 
     * @param channelId
     */
    private void logPaymentRequestCompleteMetric(int channelId) {
        metricsClient.recordEvent(METRIC_PAYMENT, METRIC_REQUEST + "." + channelId, METRIC_REQUEST_END);
    }

    /**
     * Method to log start of payment request
     * 
     * @param channelId
     */
    private void logPaymentRequestStartMetric(int channelId) {
        metricsClient.recordEvent(METRIC_PAYMENT, METRIC_REQUEST + "." + channelId, METRIC_REQUEST_START);
    }

    private Boolean isCardDetailsValid(ProductPaymentWebDO productPaymentWebDO, HttpServletRequest request,
            Map<String, Object> errorMsg) {

        // Return true if the stored card is being used.
        Map<String, String> requestMap = productPaymentWebDO.getParams();
        final String cardToken = requestMap.get(PaymentConstants.CARD_TOKEN);
        final String UNDEFINED = "undefined";
        if (!StringUtils.isEmpty(cardToken) && !UNDEFINED.equals(cardToken)) {
            productPaymentWebDO.setSavedCardTxn("true");
            return true;
        }

        if (PaymentUtil.isCreditCard(productPaymentWebDO.getPaymenttype())
                || PaymentUtil.isDebitCard(productPaymentWebDO.getPaymenttype())) {
            Boolean isCardNumberValid = true;
            isCardNumberValid = (productPaymentWebDO.getCardNumber().length() >= PaymentConstants.CARD_NUM_MIN_LENGTH
                    && productPaymentWebDO.getCardNumber().length() <= PaymentConstants.CARD_NUM_MAX_LENGTH);
            if (!isCardNumberValid) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_NUMBER);
                return false;
            }
            isCardNumberValid = productPaymentWebDO.getCardNumber().matches("\\d+");
            if (!isCardNumberValid) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_NUMBER);
                return false;
            }

            String paymentOption = productPaymentWebDO.getPaymentoption();
            // If it is Maestro card, year and month details will not be
            // present.
            if (PaymentConstants.MAESTRO_CITI.equals(paymentOption)
                    || PaymentConstants.MAESTRO_SBI.equals(paymentOption))
                return true;

            String cardExpMonth = productPaymentWebDO.getCardExpMonth();
            String cardExpYear = productPaymentWebDO.getCardExpYear();
            String cardCvv = productPaymentWebDO.getCardCvv();
            String CvvPattern = "\\d{3,4}";

            Map<String,String> transactionTypeStatusMap = PaymentUtil.convertJsonToMap(productPaymentWebDO.getTransactionTypeStatus());
            if (!transactionTypeStatusMap.containsKey(WebConstants.CVVLESS_TXN_TYPE) && !Boolean.valueOf(transactionTypeStatusMap.get(WebConstants.CVVLESS_TXN_TYPE))
                    && !cardCvv.matches(CvvPattern )) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_CVV);
                return false;
            }

            String expYearPattern = "\\d{4}";
            if (!cardExpYear.matches(expYearPattern)) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_EXPIRY_YEAR);
                return false;
            }

            String expMonthPattern = "\\d{1,2}";
            if (!cardExpMonth.matches(expMonthPattern)) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_EXPIRY_MONTH);
                return false;
            }
            Integer expMonthInt = Integer.parseInt(cardExpMonth.trim());
            if (expMonthInt > 12 && expMonthInt <= 0) {
                errorMsg.put(ERROR_MSG, PaymentConstants.INVALID_CARD_EXPIRY_MONTH);
                return false;
            }
        }

        if (productPaymentWebDO.getCardType() == null && (PaymentUtil.isCreditCard(productPaymentWebDO.getPaymenttype())
                || PaymentUtil.isDebitCard(productPaymentWebDO.getPaymenttype()))) {
            logger.error("Invalid Card type for debit card : " + productPaymentWebDO.getCardType() + "  | Channel : "
                    + request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY) + " | Appversion : "
                    + MobileUtil.getMobileAppVersionFromRequest(request));
            metricsClient.recordEvent("Payment",
                    "cardType." + request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY) + "."
                            + MobileUtil.getMobileAppVersionFromRequest(request) + "."
                            + productPaymentWebDO.getPaymenttype(),
                    "Invalid");
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> doCompletePayment(ProductPaymentWebDO productPaymentWebDO, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response, String paymentType) {
        logger.info("Transaction type map is :"+productPaymentWebDO.getTransactionTypeStatus());
        boolean isITTPTxn = productPaymentWebDO.isIttpTxn();
        long start = System.currentTimeMillis();
        String channelRequestId = request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY);
        if (channelRequestId == null) {
            channelRequestId = (String) request.getAttribute(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY);
        }
        
        int channelId = FCConstants.CHANNEL_ID_WEB;
        if (channelRequestId != null) {
            channelId = Integer.parseInt(channelRequestId);
        }
        
        if (channelId == FCConstants.CHANNEL_ID_AUTO_PAY) {
            paymentType = PaymentTypeURLName.ONE_CHECK_WALLET.getUrlString();
            productPaymentWebDO.setPaymenttype(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET);
        }
        logger.info("Channel request ID : "+channelRequestId);
        logPaymentRequestStartMetric(channelId);
        this.validateAndSetCardDetails(productPaymentWebDO);

        HashMap<String, Object> errorMsg = new HashMap<>();
        if (!isCardDetailsValid(productPaymentWebDO, request, errorMsg))
            return errorMsg;

        metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." + productPaymentWebDO.getPaymenttype(),
                METRIC_ATTEMPT);

        HashMap<String, Object> responseMsg = new HashMap<>();
        // Check the paymentType
        PaymentTypeURLName paymentTypeVal = PaymentTypeURLName.fromString(paymentType);
        if (null == paymentTypeVal) {
            logger.error("Incorrect payment mode");
            errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_INVALID_PAY_TYPE);
            errorMsg.put(ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_INVALID_PAY_TYPE));
            logPaymentRequestCompleteMetric(channelId);
            return errorMsg;
        }
        int paymentCode = paymentTypeVal.getPaymentTypeCode();
        if (paymentCode == Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET)
                && productPaymentWebDO.getProductType().equals(FCConstants.WALLET_FUND_PRODUCT_TYPE)) {
            logger.error("Trying to add credits using freecharge balance");
            errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CODE_FRAUD);
            errorMsg.put(ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_CODE_FRAUD));
            errorMsg.put(ERROR_MSG, "You cannot add credits using freecharge balance");
            logPaymentRequestCompleteMetric(channelId);
            return errorMsg;
        }
        // TODO: Check if below login checks are required. As Login Overhaul
        // should have taken care of this.
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        if (channelId != FCConstants.CHANNEL_ID_AUTO_PAY && !oneCheckWalletService.isLoggedInOCUser(request)) {
            errorMsg.put(ERROR_CODE, "ER-2005");
            errorMsg.put(ERROR_MSG, "User is not logged in. Please re-login and try payment again.");
            logPaymentRequestCompleteMetric(channelId);
            return errorMsg;
        }

        @SuppressWarnings("rawtypes")
        Map map = fs.getSessionData();
        Boolean isloggedin = false;
        if (map != null) {
            if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
            }
            String realIp=request.getHeader("X-Real-IP");
            if(StringUtils.isNotEmpty(realIp))
                map.put(WebConstants.CLIENT_REAL_IP, realIp);
            logger.info("Client real IP: "+realIp);
        }
        Boolean isNodeEndPoint = Boolean.valueOf(request.getParameter(FCConstants.IS_NODE));
        if (isNodeEndPoint) {
            fs.getSessionData().put(FCConstants.IS_NODE, true);
        } else {
            fs.getSessionData().put(FCConstants.IS_NODE, false);
        }
        
        String fingerPrint = null;
        if (fs.getSessionData().get(WebConstants.SESSION_FINGER_PRINT) != null) {
            fingerPrint = String.valueOf(fs.getSessionData().get(WebConstants.SESSION_FINGER_PRINT));
        }
        ProductPaymentBusinessDO productPaymentBusinessDO = PaymentUtil.convertWebDOToBusinessDO(productPaymentWebDO);
        productPaymentBusinessDO.setChannelId(channelId);
        boolean saveCard = false;
        if (productPaymentWebDO.isSaveCardChecked() && isloggedin) {
            saveCard = true;
        }

        responseMsg.put("isloggedin", String.valueOf(isloggedin));
        String hostPrefix = fcProperties.getProperty("hostprefix");
        hostPrefix = hostPrefix.trim();
        if (FCUtil.isEmpty(productPaymentWebDO.getLookupID()) || FCUtil.isEmpty(productPaymentWebDO.getPaymenttype())) {
            logger.error("No lid recieved or no payment type recieed. Lid: " + productPaymentWebDO.getLookupID());
            errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_INCOMPLETE_PAY_REQUEST);
            errorMsg.put(ERROR_MSG,
                    PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_INCOMPLETE_PAY_REQUEST));
            logPaymentRequestCompleteMetric(channelId);
            return errorMsg;
        }

        Users user = userServiceProxy.getLoggedInUser();
        Integer userId = user.getUserId();
        productPaymentBusinessDO.setUser(user);
        productPaymentBusinessDO.setUserId(userId);

        if (!bindingResult.hasErrors()) {
            try {
            	logger.info("Binding has no issues");
                String remoteIp = FCInfraUtils.getSecureCookie(request);
                if (StringUtils.isEmpty(remoteIp)) {
                    remoteIp = FCInfraUtils.getRemoteAddr(request);
                }
                String iciciSecureCookie = FCInfraUtils.getSecureCookie(request);
                fs.getSessionData().put(PaymentConstants.NEW_PAYMENTS_FLOW, "true");
                fs.getSessionData().put(PaymentConstants.NEW_PAYMENTS_PAGE,
                        request.getParameter(PaymentConstants.NEW_PAYMENTS_PAGE));

                // Putting session data for mobile site
                fs.getSessionData().put(PaymentConstants.IS_MOBILE_WEB, Boolean.FALSE);
                if (channelId == FCConstants.CHANNEL_ID_MOBILE) {
                    fs.getSessionData().put(PaymentConstants.IS_MOBILE_WEB, Boolean.TRUE);
                }
                // Putting session data for mobile apps.
                fs.getSessionData().put(PaymentConstants.IS_MOBILE_APP, Boolean.FALSE);
                if (MobileUtil.isAppChannel(channelId)) {
                    fs.getSessionData().put(PaymentConstants.IS_MOBILE_APP, Boolean.TRUE);
                    fingerPrint = request.getParameter(FCConstants.IMEI);
                }
                Integer appVersion = MobileUtil.getMobileAppVersionFromRequest(request);
                productPaymentBusinessDO.setAppVersion(appVersion);
                productPaymentBusinessDO.setDeviceId(fingerPrint);
                productPaymentBusinessDO.setUtmDetails(productPaymentWebDO.getUtmDetails());
                Map<String, String> requestMap = checkoutService.doCompletePayment(productPaymentWebDO.getParams(),
                        productPaymentBusinessDO, productPaymentWebDO.getLookupID(), remoteIp, iciciSecureCookie,
                        channelId, saveCard);

                String paymentGatewayName = requestMap.get(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY);
                String mtxnId = requestMap.get(PaymentConstants.MERCHANT_ORDER_ID);

                if (!StringUtils.isEmpty(paymentGatewayName) && !StringUtils.isEmpty(mtxnId)) {
                    checkoutService.publishPaymentAttempt(productPaymentBusinessDO, mtxnId, channelId,
                            paymentGatewayName);
                }

                if ("true".equalsIgnoreCase(requestMap.get(PaymentConstants.IS_METHOD_SUCCESSFUL))) {
                    requestMap.remove("key");
                    responseMsg.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                            requestMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                    responseMsg.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                            requestMap.get(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL));
                    if (requestMap.containsKey(PaymentConstants.ORDER_ID_KEY)) {
                        responseMsg.put(PaymentConstants.ORDER_ID_KEY, requestMap.get(PaymentConstants.ORDER_ID_KEY));
                    }
                    if (requestMap.containsKey(PaymentConstants.UPI_PAYMENT_FLAG)) {
                        responseMsg.put(PaymentConstants.UPI_PAYMENT_FLAG,
                                requestMap.get(PaymentConstants.UPI_PAYMENT_FLAG));
                    }
                    // hack needs to be removed once webapp issues are fixed 
                    String kpCookie = (String) fs.getSessionData().get(KlickPayGatewayHandler.KP_COOKIE_KEY);
                    if(!StringUtils.isEmpty(kpCookie)) {
                    	Cookie cookie = new Cookie(KlickPayGatewayHandler.KP_COOKIE_NAME, kpCookie);
                    	cookie.setMaxAge(900);
                    	cookie.setPath("/");
                    	cookie.setDomain(".freecharge.in");
                    	if(!(fcProperties.isDevMode() || fcProperties.isQaMode())) {
                    		cookie.setSecure(true);
                    	}
                    	response.addCookie(cookie);
                    }
                } else {
                    logger.error("Payment requestMap creation failed for lookup id: "
                            + productPaymentBusinessDO.getLookupID());
                    errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CODE_GENERAL);
                    errorMsg.put(ERROR_MSG,
                            PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_CODE_GENERAL));
                    logPaymentRequestCompleteMetric(channelId);
                    return errorMsg;
                }
            } catch (CardValidationException e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(),
                        "CardValidation.Failure");
                logger.info("Card validation failed with message : " + e.getCustomMsg(), e);
                errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CARD_DETAILS);
                errorMsg.put(ERROR_MSG, e.getCustomMsg());
                logPaymentRequestCompleteMetric(channelId);
                return errorMsg;
            } catch (FraudDetectedException e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(), "Fraud");
                logger.warn("Fraud detected in Freecharge Serving Request. Message : " + e.getCustomMsg(), e);
                errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CODE_FRAUD);
                errorMsg.put(ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_CODE_FRAUD));
                logPaymentRequestCompleteMetric(channelId);
                return errorMsg;
            } catch (InvalidPaymentRequestException e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(),
                        "InvalidRequest");
                logger.error("Error in Freecharge Serving Request. Message : " + e.getErrorMessages(), e);
                errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_INCOMPLETE_PAY_REQUEST);
                errorMsg.put(ERROR_MSG,
                        PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_INCOMPLETE_PAY_REQUEST));
                logPaymentRequestCompleteMetric(channelId);
                return errorMsg;
            } catch (CampaignConditionException e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(),
                        "CampaignConditionFailure");
                logger.error("Condition evaluation failure in campaign service ", e);
                errorMsg.put(ERROR_CODE, FCConstants.CAMPAIGN_PAYMENT_CONDITION_FAILURE);
                errorMsg.put(ERROR_MSG, e.getCustomMsg());
                return errorMsg;
            } catch (FCRuntimeException fcExp) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(),
                        "RunTimeException");
                logger.error("Error in Freecharge Serving Request. Message : " + fcExp.getMessage(), fcExp);
                errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CODE_GENERAL);
                errorMsg.put(ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_CODE_GENERAL));
                logPaymentRequestCompleteMetric(channelId);
                return errorMsg;
            } catch (Exception e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(),
                        "Exception");
                logger.error("Exception while trying to make payment.", e);
                errorMsg.put(ERROR_CODE, PaymentConstants.ERROR_CODE_GENERAL);
                errorMsg.put(ERROR_MSG, PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_CODE_GENERAL));
                logPaymentRequestCompleteMetric(channelId);
                return errorMsg;
            }
        } else { 
        	logger.error("Binding failed");
        	List<ObjectError> bindingErrors = bindingResult.getAllErrors();
        	if(bindingErrors != null) {
        		for(ObjectError error : bindingErrors) {
        			logger.error(error.toString());
        			logger.error("ErrorCode:" + error.getCode() + "|ObjectName:" + error.getObjectName());
        		}
        	}
        }
        this.updateTransactionHistoryForTransaction(productPaymentBusinessDO.getOrderId(), userId,
                PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK);

        HashMap<String, String> resultMap = (HashMap<String, String>) responseMsg
                .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
        String mtxnId = resultMap.get(PaymentConstants.MERCHANT_ORDER_ID);
        String orderId = productPaymentBusinessDO.getOrderId();
        logger.info("order id " + orderId + " mtxnid " + mtxnId);
        if (null != orderId && (null == mtxnId || orderId.equalsIgnoreCase(mtxnId))) {
            Map<String, Object> transactionInfo = new HashMap<>();
            transactionInfo.put(InTxnDataConstants.ORDER_ID, orderId);
            transactionInfo.put(InTxnDataConstants.ORDER_ID_GENERATED_ON, new Date());
            try {
                saveInTxnData1(productPaymentBusinessDO.getLookupID(),
                        FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
            } catch (Exception e) {
                logger.error("Exception in saving intxndata for uniqueId "
                        + productPaymentBusinessDO.getLookupID() + ", orderId " + orderId
                        + " and data " + transactionInfo, e);
            }
        }
        if (PaymentTypeURLName.FC_BALANCE.getUrlString().equals(paymentType)) {
            responseMsg = new HashMap<>();
            Map<String, String> resp = walletController.processWalletPaymentRequest(request, response, userId,
                    productPaymentBusinessDO.getOrderId(), mtxnId, productPaymentBusinessDO.getProductType());
            responseMsg.putAll(resp);
        } else if (PaymentTypeURLName.ONE_CHECK_WALLET.getUrlString().equals(paymentType)) {
            mtxnId = resultMap.get(PaymentConstants.MERCHANT_ORDER_ID);
            Map<String, String> resp = walletController.processOneCheckWalletPaymentRequest(request, response, userId,
                    productPaymentBusinessDO.getOrderId(), mtxnId, productPaymentBusinessDO.getProductType(),
                    responseMsg, productPaymentBusinessDO.getLookupID());
            responseMsg = new HashMap<>();
            responseMsg.putAll(resp);
        } else if(PaymentTypeURLName.FC_UPI.getUrlString().equals(paymentType)){
            if(!isValidBeneficiaryVPA(productPaymentBusinessDO)) {
                logger.info("Invalid beneficiary VPA");
                errorMsg.put(ERROR_CODE,PaymentConstants.ERROR_INVALID_BENEFICIARY_VPA);
                errorMsg.put(ERROR_MSG,PaymentConstants.paymentErrorCodeMap.get(PaymentConstants.ERROR_INVALID_BENEFICIARY_VPA));
                return errorMsg;
            }
            mtxnId = resultMap.get(PaymentConstants.MERCHANT_ORDER_ID);
            Map<String, String> resp = fcUpigatewayHandler.processUpiPaymentRequest(request, response, user,
                    productPaymentBusinessDO, mtxnId);
            responseMsg = new HashMap<>();
            responseMsg.putAll(resp);
        } else if (PaymentTypeURLName.ZERO_PAY.getUrlString().equals(paymentType)) {
            Map<String, Object> respMap = zeroPaymentController.processPaymentAndRecharge(
                    productPaymentBusinessDO.getOrderId(), mtxnId, productPaymentBusinessDO.getLookupID());
            responseMsg.putAll(respMap);
        }
        /*
         * do not add this again - PaymentUtil
                .getPaymentRequestLog((responseMsg.get()).
         * removed final map log because of card information and other sensitive data may expose.
         * do not add this log in future.[security concern]
         */
        metricsClient.recordLatency("Payment", "New.Request." + productPaymentBusinessDO.getPaymenttype(),
                System.currentTimeMillis() - start);
        String paymentGatewayName = "";
        if (responseMsg.containsKey(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL)) {
            paymentGatewayName = PaymentUtil.getPaymentGatewayNameFromUrl(
                    (String) responseMsg.get(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL));
            logRequestToPGMetric(paymentGatewayName, channelId);
        }
        logPaymentRequestCompleteMetric(channelId);

        return responseMsg;
    }

    private boolean isValidBeneficiaryVPA(ProductPaymentBusinessDO productPaymentBusinessDO) {

        final String delimiter = ",";
        String[] beneVpas = appConfigService.getFCVpaList(fcProperties.getBeneficiaryVpaList(), delimiter);
        UpiPaymentData upiPaymentData = getUpiPaymentObject(productPaymentBusinessDO.getUpiInfo());
        logger.info("Upi payment data: "+upiPaymentData);
        if(upiPaymentData == null || CollectionUtils.isEmpty(upiPaymentData.getPayees()) || (upiPaymentData.getPayees().size()>1)
                || !validateFcVpaFromList(beneVpas,upiPaymentData.getPayees().get(0).getBeneVpa())) {
            return false;
        }
        return true;
    }

    private UpiPaymentData getUpiPaymentObject(String upiInfo)  {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<UpiPaymentData>(){
            }.getType();
            return gson.fromJson(upiInfo, type);
        } catch (Exception e) {
            logger.error("upiInfo object parsing Exception for upiInfo:"+upiInfo);
            throw e;
        }
    }

    private boolean validateFcVpaFromList(String[] vpas, String beneVpa) {
        if(!ArrayUtils.isEmpty(vpas)) {
            for (String vpa:vpas) {
                if (vpa.equalsIgnoreCase(beneVpa)) {
                    return true;
                }
            }
        }
        return false;
    }

    void storeUtmDetailsToDynamoDB() {
        
    }

    public void doWalletPayment(ProductPaymentWebDO productPaymentWebDO) {
        metricsClient.recordEvent("Payment", "Attempt" + "." + productPaymentWebDO.getPaymenttype(), "attempt");
    }

    public void saveInTxnData1(final String uniqueId, final Integer merchantId, final Map<String, Object> data){
        Callable<Void> task = new Callable<Void>() {
            public Void call() throws CampaignConditionException, InvalidPaymentRequestException {
                campaignServiceClient.saveInTxnData1(uniqueId, merchantId, data);
                return null;
            }
        };
        Future<Void> future = campaignExecutor.submit(task);
        try {
            Void response = future.get(FCConstants.CAMPAIGN_SERVICE_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (TimeoutException e) {
            logger.error("Timeout exception while calling campaign client for uniqueId: " + uniqueId);
        } catch (InterruptedException e) {
            logger.error("InterruptedException exception while calling campaign client for uniqueId: " + uniqueId);
        } catch (ExecutionException e) {
            logger.error("ExecutionException exception while calling campaign client for uniqueId: " + uniqueId);
        } finally {
            future.cancel(true); // may or may not desire this
        }
    }
    
    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    private ConvenienceFeeUtil convenienceFeeUtil;
    
    @Autowired
    private PaymentSNSService paymentSNSService;
    
    @Autowired
    private GSTService gstService;
    
    public boolean sendNotification(PaymentTransaction paymentTransaction,
        PaymentResponse paymentResponse, String transactionId) {
        boolean status = false;
        boolean manualStatusCheck = true;
        logger.info("paymentSNSService publish triggered for orderId : " + paymentTransaction.getOrderId());
        PaymentAlertBean paymentAlertBean = new PaymentAlertBean();
        try {
            paymentAlertBean.setManualStatusCheck(manualStatusCheck);
            paymentAlertBean.setMtxnId(paymentTransaction.getMerchantTxnId());
            paymentAlertBean.setOrderId(paymentTransaction.getOrderId());
            //paymentAlertBean.setUpiAccountDetails(getFCUpiPaymentObject(paymentResponse.getUpiAccountDetails()));
            paymentAlertBean.setRrn(paymentResponse.getRrnNo());
            paymentAlertBean.setPgMessage(paymentResponse.getPgMessage());
            paymentAlertBean.setMessageType(paymentTransaction.getIsSuccessful() ? PaymentStatus.PAYMENT_SUCCESS.getStatus()
                    : PaymentStatus.PAYMENT_FAILURE.getStatus());

            String paymentTypeName = PaymentType.PaymentTypeName.fromPaymentTypeCode(paymentTransaction.getPaymentType()).toString();
            paymentAlertBean.setPaymentType(paymentTypeName);

            if(paymentTransaction.getPaymentType() == 3){
                paymentAlertBean.setNetBankingName(paymentTransaction.getPaymentOption());
            }

            paymentAlertBean.setChannel(FCUtil.getChannelFromChannelType(getChannelFromOrderId(paymentTransaction.getOrderId())));
            if (!StringUtils.isEmpty(paymentResponse.getPaymentGateway())) {
                logger.info("Payment Gateway for MTxn Id : " + paymentTransaction.getMerchantTxnId() + " : " + paymentResponse.getPaymentGateway());
                logger.info("ECI for MTxn Id : " + paymentTransaction.getMerchantTxnId() + " : " + paymentResponse.getEci());
                if (!paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE)) {
                    paymentAlertBean.setPaymentGateway(paymentResponse.getPaymentGateway());
                } else {
                    if (!StringUtils.isEmpty(paymentResponse.getEci())) {
                        paymentAlertBean.setPaymentGateway(paymentResponse.getEci());
                    } else {
                        paymentAlertBean.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
                    }
                }
            }
            
            CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(paymentTransaction.getOrderId());
            
            convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, paymentAlertBean);
            
            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
            paymentAlertBean.setProduct(primaryProduct.name());
            paymentAlertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()).longValue());
            
            com.freecharge.payment.dos.entity.PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlanFromMtxnId(paymentTransaction.getMerchantTxnId(), paymentTransaction.getOrderId());
            if(fcPaymentPlan==null) {
                fcPaymentPlan = paymentPlanService.getPaymentPlan(paymentTransaction.getOrderId());
            }
            paymentAlertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
            int paymentType= paymentTransaction.getPaymentType();
            PaymentPlan paymentPlan = new PaymentPlan();
            if (fcPaymentPlan != null) {
                Double creditsAmount = fcPaymentPlan.getWalletAmount().getAmount().doubleValue();
                Double pgAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                if ((paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE) || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType) && creditsAmount == 0.0) {
                    creditsAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                    pgAmount = 0.0;
                }
                
                paymentPlan.setPg(pgAmount);
                
                if (paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                    paymentPlan.setCredits(creditsAmount);
                } else {
                    paymentPlan.setOcw(creditsAmount);
                }
                paymentAlertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
                paymentAlertBean.setPaymentPlan(paymentPlan);
            }
            
            paymentAlertBean.setPaymentPlan(paymentPlan);
            
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
            String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
            String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
            String operator = userTransactionHistory.getServiceProvider();
            paymentAlertBean.setTransactionId(transactionId);
            paymentAlertBean.setImsId(oneCheckIdentity);
            gstService.populateGST(paymentAlertBean, primaryProduct,operator);
        } catch (Exception e) {
            logger.error("payment notification publish failure for orderId : " + paymentTransaction.getOrderId(), e);
        } finally {
            paymentAlertBean.setReconMetaData(paymentResponse.getReconMetaData());
            try {
                logger.info("PG Alert after response:"+PaymentAlertBean.getJsonString(paymentAlertBean));
                paymentSNSService.publish(paymentTransaction.getOrderId(), "{\"message\" : " + PaymentAlertBean.getJsonString(paymentAlertBean) + "}");
                return true;
            } catch (Exception e) {
                logger.error("Exception while publishing notification for alert bean:" + paymentTransaction.getOrderId(), e);
            }
        }
    
        return status;
    }
    
    private String getChannelFromOrderId(final String orderId) {
        try {
            if(orderId!=null && orderId.length() >= 4) {
                String fourthChar = String.valueOf(orderId.charAt(3));
                return fourthChar;
            }
        } catch (Exception e) {
            logger.error("Exception on getting channel from order id ", e);
        }
        return null;
    }
}
