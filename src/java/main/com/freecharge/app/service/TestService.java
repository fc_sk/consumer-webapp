package com.freecharge.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.TestDao;
import com.freecharge.common.businessdo.TestBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

@Service
public class TestService {

    private TestDao testDao;

    @Autowired
    public void setTestDao(TestDao testDao) {
        this.testDao = testDao;
    }

    @Transactional(readOnly = true)
    public void doTestService(BaseBusinessDO baseBusinessDO){
        TestBusinessDO testBusinessDO = (TestBusinessDO)baseBusinessDO;
        testBusinessDO.setConstants(testBusinessDO.getOne()*5000);
        String name = testDao.getById(1).getName();
        testBusinessDO.setDbName(name);
    }
}
