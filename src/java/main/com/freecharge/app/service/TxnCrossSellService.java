package com.freecharge.app.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.freecharge.app.domain.entity.jdbc.CartItemsMetadata;
import com.freecharge.util.JsonUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.TxnCrossSellDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.TxnCrossSell;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.exception.CouponTamperedException;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.rest.billpay.Exception.ServiceUnavailableException;
import com.freecharge.web.webdo.CalculateChargeRequest;
import com.freecharge.web.webdo.CalculateChargeResponse;
import com.freecharge.web.webdo.CartAndCartItemsVO;

/**
 * Created by IntelliJ IDEA. User: Jitender Date: May 11, 2012 Time: 10:22:56 AM
 * To change this template use File | Settings | File Templates.
 */
@Service("txnCrossSellService")
public class TxnCrossSellService {
	@Autowired
	private FCProperties fcproperties;
	private Logger logger = LoggingFactory.getLogger(getClass());
	private TxnCrossSellDAO txnCrossSellDAO;
	private CartDAO cartDao;
	@Autowired
	private PricingService pricingService;

	@Autowired
	private CouponService couponService;

	@Autowired
	BillReadDAO billDao;

	@Autowired
	public void setTxnCrossSellDAO(TxnCrossSellDAO txnCrossSellDAO) {
		this.txnCrossSellDAO = txnCrossSellDAO;
	}

	@Autowired
	private CartService cartService;

	@Autowired
	private TxnHomePageDAO txnHomePageDAO;

	@Autowired
	private AppConfigService appConfigService;

	@Value("${fee.engine.endpoint.url}")
	private String feeEngineUrl;
    
    @Value("${fee.engine.client.name}")
	private String feeEngineClientName;
    @Value("${fee.engine.client.key}")
	private String feeEngineClientKey;

	@Autowired
	public void setCartDao(CartDAO cartDao) {
		this.cartDao = cartDao;
	}
	
	@Autowired ConvinenceFeeDetailsDao convinenceFeeDetailsDao;

	@Transactional
	public void saveAllTxnCrossSellService(BaseBusinessDO baseBusinessDO) {
		logger.info("Coming in TxnCrossSellService : saveAllTxnCrossSellService");
		List<CartItems> cartItemsList=new ArrayList<CartItems>();
		//			int itemIdAmt=0;
		TxnCrossSellBusinessDo txnCrossSellBusinessDo = (TxnCrossSellBusinessDo) baseBusinessDO;
		logger.info("txnCrossSellBusinessDo values " + txnCrossSellBusinessDo);
		String couponArray = txnCrossSellBusinessDo.getItemIdArray();
		Integer cart_id=null;
		String lookupID = txnCrossSellBusinessDo.getLookupID();

		logger.info("LookupId from txnCrossSellBusinessDo is"+lookupID);
		if (lookupID == null){
			logger.warn("lookupId is null");
			return;
		}

		TxnHomePage txnHomePage = txnHomePageDAO.findByLookupId(lookupID).get(0);
		BillTxnHomePage billTxnHomePage = txnHomePage.getBillTxnHomePage();

		Float prepaidAmount = txnHomePage.getAmount();

		String postpaidAmount = "";
		if (billTxnHomePage != null) {
			postpaidAmount = billTxnHomePage.getBillAmount().toString(); 
		}

		Double amount=0.0;
		if(prepaidAmount != null){
			amount = prepaidAmount.doubleValue();
		} else if (!FCUtil.isEmpty(postpaidAmount)) {
			amount=Double.parseDouble(postpaidAmount);
		} else {
			throw new FCRuntimeException(String.format(
					"Expected either of valid rechargeAmount/postpaidAmount, found rechargeAmount=%s, postpaidAmount=%s",
					String.valueOf(prepaidAmount), postpaidAmount));
		}
		if(!FCUtil.isEmpty(couponArray))
		{
			int handlingCharge = pricingService.getHandlingCharge(txnCrossSellBusinessDo.getProType());
			txnCrossSellBusinessDo.setServiceCharge(handlingCharge);
		}
		else{
			txnCrossSellBusinessDo.setServiceCharge(0);
		}
		//			Integer roundOffAmount= getRoundOffAmount(amount);

		List<TxnCrossSell> txnCrossSellList=new ArrayList<TxnCrossSell>();

		boolean containsPaidCoupon = false;
		if (!FCUtil.isEmpty(couponArray)) {
			if(couponArray.startsWith(",")){
				couponArray=couponArray.replaceFirst(",", "");
			}
			String splictedCouponArr[] = couponArray.split(",");
			logger.info("Splicted  Coupon array is " + splictedCouponArr);
			List<String> splictedCouponList = Arrays.asList(splictedCouponArr);
			logger.info("Splicted  Coupon array is after Making it as List is " + splictedCouponList);
			Set<String> splictedCouponset = new HashSet<String>(splictedCouponList);
			Iterator<String> ite = splictedCouponset.iterator();

			while (ite.hasNext()) {
				String id = (String) ite.next();
				Integer frequency = Collections.frequency(splictedCouponList, id);
				short eligiableCoupons = Short.parseShort(fcproperties.getProperty(FCConstants.ELIGIABLECOUPONS));
				if(frequency > eligiableCoupons ){
					logger.error("frequency > eligiableCoupons  : " + frequency +" > "+ eligiableCoupons );
					throw new CouponTamperedException();
				}
				CouponStore couponStore = couponService.getCouponStore(Integer.parseInt(id));
				//TODO: Enable after adding support for error handling in UI when coupon goes out of stock.
				/*if (couponStore == null || couponStore.getIsActive() == null ||
							!couponStore.getIsActive()){
						throw new IllegalStateException("Coupon with id: " + id + 
								" is not available. coupon_store = " + couponStore);
					}*/
				if(couponStore!=null) {
					if (txnCrossSellBusinessDo.getType().equals("crosssell")) {
						TxnCrossSell txnCrossSell = new TxnCrossSell();
						txnCrossSell.setCorssSellId(couponStore.getFkCrosssellId());
						txnCrossSell.setQuantity(frequency);
						txnCrossSell.setCouponId(couponStore.getCouponStoreId());
						txnCrossSell.setTxnHomePageId(txnCrossSellBusinessDo.getHomePageId());
						txnCrossSell.setCreatedOn((new Timestamp(new Date().getTime())));

						txnCrossSellList.add(txnCrossSell);
					}
					logger.info("About to create Cart");

					CartItems cartItems = new CartItems();
					Cart cartdata = new Cart();
					cartdata.setCartId(cart_id);
					cartItems.setFkItemId(Integer.parseInt(id));
					cartItems.setCart(cartdata);
					cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
					/*itemIdAmt=itemIdAmt +(couponStore.getCouponValue().intValue() * frequency);	
						if(itemIdAmt>roundOffAmount){
							logger.error("itemIdAmt>roundOffAmount  : " + itemIdAmt +" > "+ roundOffAmount );
							throw new CouponTamperedException();
						}*/
					cartItems.setIsDeleted(false);
					cartItems.setQuantity(frequency);
					if(txnCrossSellBusinessDo.getType().equals("coupon")){
						cartItems.setEntityId("site_merchant");
						cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_COUPONS);
						cartItems.setPrice(couponStore.getCouponValue().doubleValue());
					}else if (txnCrossSellBusinessDo.getType().equals("crosssell")){
						cartItems.setEntityId("site_merchant");
						cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_CROSS_SELL);
						cartItems.setPrice(couponStore.getCouponValue().doubleValue());
					}else if (txnCrossSellBusinessDo.getType().equals("pcoupon")){
						cartItems.setEntityId("site_merchant");
						cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_PAID_COUPON);
						cartItems.setPrice(couponStore.getPrice().doubleValue());
						containsPaidCoupon = true;
					}
					cartItemsList.add(cartItems);
				}
			}
		}
		List<CartAndCartItemsVO> list = cartDao.getCartByLookUpId(lookupID);

		if (!FCUtil.isEmpty(list)) {
			cartDao.deleteCartItems(list.get(0).getCartId());
			txnCrossSellDAO.markIsDeleted(txnCrossSellBusinessDo.getHomePageId());
			cart_id=list.get(0).getCartId();
		} else {
			Cart cart = cartService.buildCart(lookupID);
			cart_id = cartDao.saveCart(cart);
		}
		cartDao.saveCartItems(cartItemsList , cart_id);
		logger.info("Cart created with cart id: " + cart_id);
		if (!txnCrossSellList.isEmpty()) {
			txnCrossSellDAO.save(txnCrossSellList); 
		}

		List<CartItems> cartItemsListUpdated = saveExtraItems(amount, txnCrossSellBusinessDo.getServiceNo(), txnCrossSellBusinessDo.getServiceCharge(), 
				cart_id, txnCrossSellBusinessDo.getProType(), txnCrossSellBusinessDo.getOperatorName(), containsPaidCoupon, txnCrossSellBusinessDo.getPlanId(), txnCrossSellBusinessDo.getPlanCategory());
		//adding for convenience fee a check to see if its utility product and it goes through convenience fee flow
		String merchantId=appConfigService.getChildProperty(AppConfigService.MERCHANT_ID_MAPPING,String.valueOf(txnHomePage.getFkOperatorId()));
		if (FCUtil.isUtilityPaymentType(txnHomePage.getProductType()) && appConfigService.isConvenienceFeeEnabled() && merchantId != null) {
			logger.info(" Will add conv fee");
			CartItems convenienceFeeCartItem = getconvenienceFeeToSaveInCart(amount, txnCrossSellBusinessDo.getServiceNo(), txnCrossSellBusinessDo.getServiceCharge(), 
					cart_id, txnCrossSellBusinessDo.getProType(), txnCrossSellBusinessDo.getOperatorName(), containsPaidCoupon, lookupID,txnHomePage.getFkOperatorId());
			if(convenienceFeeCartItem != null) {
				cartItemsListUpdated.add(convenienceFeeCartItem);
			}else {
				throw new ServiceUnavailableException("Exception caught while getting fee details and pushing them to DDB for cartId: " + cart_id);
			}

		}
		cartDao.saveCartItems(cartItemsListUpdated ,cart_id);
		logger.info("Data saved in TxnCrossSellService for txnHomePageId :" + txnCrossSellBusinessDo.getHomePageId() + " , rows inserted : ");
	}

	private static class ExtraItems {
		public final String displayName;
		public final String providerName;
		public final String productType;
		public final Integer serviceCharge;
		public ExtraItems(TxnCrossSellBusinessDo bean, TxnHomePageDAO txnHomePageDAO, FCProperties fcproperties) {
			List<TxnHomePage> thpList = txnHomePageDAO.findByLookupId(bean.getLookupID());
			if (FCUtil.isEmpty(thpList) || thpList.size() != 1) {
				String msg = "Unable to find TxnHomePage instance by Lookup-ID: " + bean.getLookupID() == null? "NULL":
					("'" + bean.getLookupID() + "'");
				throw new IllegalStateException(msg);
			}
			TxnHomePage thp = thpList.get(0);
			this.productType = bean.getProType();
			final String type = bean.getProType();
			final String couponArray = bean.getItemIdArray();
			final int couponCharges = (!FCUtil.isEmpty(couponArray))? Integer.parseInt(fcproperties.getProperty(
					FCConstants.APPLICATION_SERVICE_CHARGE_KEY)): 0;
			switch (type) {
			case "V":
			case "D":
			case "C":
			case "F":
				this.serviceCharge = couponCharges;
				this.displayName = bean.getServiceNo();
				this.providerName = bean.getOperatorName();
				break;

			case "M":
				BillTxnHomePage bthp = thp.getBillTxnHomePage();
				if (bthp == null) {
					throw new IllegalStateException("Unable to load BillTxnHomePage instance");
				}
				this.serviceCharge = couponCharges + Integer.parseInt(fcproperties.getProperty(
						FCConstants.FREEBILL_CONVENIENCE_CHARGE_KEY));
				this.displayName = "" + bthp.getPostpaidNumber() + " (postpaid)";
				this.providerName = bthp.getPostpaidMerchantName();
				break;

			default:
				throw new IllegalArgumentException("Unsupported product type: " + (FCUtil.isEmpty(type)? "NULL": type));
			}
		}
	}


	public List<CartItems> saveExtraItems(Double amount, String displayLabel, Integer handlingCharges, Integer cartId, String type,
			String providerName, boolean containsPaidCoupon, Integer planId, String planCategory){
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

		List<CartItems> cartItemsList=new ArrayList<CartItems>();
		CartItems cartItems = new CartItems();
		Cart cartdata = new Cart();
		cartdata.setCartId(cartId);
		cartItems.setCart(cartdata);
		cartItems.setDisplayLabel(displayLabel);
		cartItems.setPrice(amount);
		cartItems.setFlProductMasterId(FCConstants.productMasterMapReverse.get(type));
		cartItems.setIsDeleted(false);
		cartItems.setQuantity(1);
		cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
		CartItemsMetadata cartItemsMetadata = new CartItemsMetadata();
		cartItemsMetadata.setPlanId(planId);
		cartItemsMetadata.setPlanCategory(planCategory);
		cartItems.setMetadata(JsonUtil.getJson(cartItemsMetadata));

		String rechargePlanType = fs.getRechargePlanType();

		if (StringUtils.isNotBlank(rechargePlanType)) {
			cartItems.setEntityId(rechargePlanType);
		}
		cartItemsList.add(cartItems);

		//Save handling charge only if priced coupons are disabled.
		if (!containsPaidCoupon){
			cartItems = new CartItems();
			cartdata = new Cart();
			cartdata.setCartId(cartId);
			cartItems.setCart(cartdata);
			cartItems.setPrice(handlingCharges.doubleValue());
			cartItems.setFlProductMasterId(6);
			cartItems.setDisplayLabel("Handling Charges");
			cartItems.setIsDeleted(false);
			cartItems.setQuantity(1);
			cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
			cartItemsList.add(cartItems);
		}
		return cartItemsList;

	}


	public CartItems getconvenienceFeeToSaveInCart(Double amount, String displayLabel, Integer handlingCharges, Integer cartId, String type,
			String providerName, boolean containsPaidCoupon, String lookupId, Integer operatorId){
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Double convenienceFee = 0.0;
		CalculateChargeResponse response = null;
		logger.info("Saving extra items with cart_id : " + cartId);
		try {
			FeeEngineService feeService = new FeeEngineService(feeEngineUrl,feeEngineClientName, feeEngineClientKey);
			CalculateChargeRequest request = new CalculateChargeRequest();
//			request.setCategoryId("CatagoryId"); //Optional
			
			logger.info("BillerID : " + String.valueOf(operatorId));
			JSONObject json = appConfigService.getCustomProp(AppConfigService.MERCHANT_ID_MAPPING);
        	logger.info("json : " + json.toJSONString());
        	if(json.containsKey(operatorId)) {
                String temp = String.valueOf(json.get(operatorId));
            }else {
            	logger.info("json doesn't contain biller details");
            }
        	
        	
			String merchantId=appConfigService.getChildProperty(AppConfigService.MERCHANT_ID_MAPPING,String.valueOf(operatorId));
			request.setMerchantId(merchantId);
//			request.setPartnerId("partnerId"); //Optional
//			request.setPlatformId("platformId"); //Optional
			request.setTransactionTime(new Date());
			BigDecimal amountBd = BigDecimal.valueOf(amount);
			request.setTxnAmount(amountBd);
			response = feeService.calculateFeeCharge(request);
			if(response != null) {
				convenienceFee = response.getFeeCharge().getFeeAmount().doubleValue();
				long startTime = System.currentTimeMillis(); 
				convinenceFeeDetailsDao.insert(response.getFeeCharge(), lookupId);
				long endTime = System.currentTimeMillis(); 
				long duration = endTime - startTime;
				logger.info("Total duration of dynamo insert: " + duration);
			}else {
				logger.error("Got null from fee engine for cartId: " + cartId);
				return null;
			}
			
			
		} catch (Exception e) {
			logger.error("Exception caught while calling feeService for cartId : " + cartId,e);
			throw new ServiceUnavailableException("Exception caught while getting fee details and pushing them to DDB for cartId: " + cartId);
		}
		//  Adding convience fee in cart items.
		if(response != null) {
			
			Cart cartdata = new Cart();
			CartItems cartItems1 = new CartItems();
			cartdata.setCartId(cartId);
			cartItems1.setCart(cartdata);
			cartItems1.setDisplayLabel(appConfigService.getChildProperty(AppConfigService.CONVENIENCE_FEE_TITLE,"title"));
			cartItems1.setPrice(convenienceFee);
			cartItems1.setFlProductMasterId(ProductName.ConvenienceFee.getProductId());
			cartItems1.setIsDeleted(false);
			cartItems1.setQuantity(1);
			cartItems1.setEntityId(response.getFeeCharge().getFeeId());//FeeId
			cartItems1.setAddedOn(new Timestamp(System.currentTimeMillis()));
			return cartItems1;
		}else {
			return null;
		}
		


	}



	public Integer getRoundOffAmount(Double amount){
		Integer roundOffAmount=0;
		for (int index = 0; amount <= FCConstants.MAX_RECHARGE_AMOUNT; index++) {
			roundOffAmount = roundOffAmount + FCConstants.RECHARGE_INC_AMOUNT;
			if (roundOffAmount >= amount) {
				break;
			}
		}
		return roundOffAmount;
	}

	@Transactional
	public List<TxnCrossSell> getActiveItemByTxnHomePageIdAndCouponId(Integer txnHomePageId, Integer couponId) {
		return txnCrossSellDAO.getActiveItemByTxnHomePageIdAndCouponId(txnHomePageId, couponId);
	}

	@Transactional
	public List<TxnCrossSell> getActiveCrossSellByTxnHomePageIdAndcrossSellId(Integer txnHomePageId, Integer crossSellId) {
		return txnCrossSellDAO.getActiveCrossSellByTxnHomePageIdAndcrossSellId(txnHomePageId, crossSellId);
	}

	@Transactional
	public List<TxnCrossSell> getActiveItemByTxnHomePageId(Integer txnHomePageId) {
		return txnCrossSellDAO.findByTxnHomePageId(txnHomePageId);
	}



	@Transactional
	public boolean updateTxnCrosssell(TxnCrossSell txnCrossSell) {
		return txnCrossSellDAO.updateTxnCrosssell(txnCrossSell);
	}

	@Transactional
	public Integer saveTxnCrossSell(TxnCrossSell txnCrossSell) {
		return txnCrossSellDAO.save(txnCrossSell);
	}
}
