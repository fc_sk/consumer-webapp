package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.cache.CacheManager;

public class InCachedService extends CacheManager {
	private static String ALL_OPERATORS_CACHE = "fcAllOperatorsCache";
	private static String OPERATORS_CACHE = "fcOperatorsCache";
	private static String NAMES_CACHE = "fcNamesCache";
	private static String PRODUCTS_CACHE = "fcProductsCache";
	private static String CIRCLE_CACHE = "fcCircleCache";
	private static String OPERATOR_CIRCLE_BY_PREFIX = "operatorCircleByPrefix";
	private static String IN_OPPR_AGGR_PRIORITY_CACHE = "inOperatorAggrPriority";
	private static String IN_AGGR_OPR_CIRCLE_MAP_CACHE = "InAggrOprCircleMap";
	private static String CIRCLE_OPERATOR_MAPPING_CACHE = "CircleOperatorMapping";
	private static String IN_ERRORCODE_MAP_CACHE = "InErrorCodeMap";
	private static String IN_RECHARGERETRY_MAP_CACHE = "InRechargeRetryMap";
	private static String MAP_BY_CIRCLE_OPERATOR_MAPPING_ID = "mapByCircleOperatorMappingID";
	private static String MAP_OPERATOR_MASTER_ID = "mapOperatorMasterID";
	private static String MAP_OPERATOR_MASTER_CODE = "mapOperatorMasterCode";
	private static String LIST_OPERATOR_MASTER_PRODUCT_ID = "mapProductMasterID";
	private static String MAP_CIRCLE_MASTER_ID = "mapCirclemasterID";
	private static String MAP_CIRCLE_MASTER_NAME = "mapCirclemasterName";
	private static String CIRCLE_OPERATOR_MAPPING_ID = "circleOperatorMappingID";
	private static String CIRCLE_AND_OPERATOR_MAPPING_ID = "circleAndOperatorMappingID";
	private static String IN_AGGR_OPR_CIRCLE_BY_ID = "InAggrOprCircleMapByID";
	private static String ALL_PREFIX_OPERATOR_CIRCLE = "allPrefixOperatorCircle";
	private static String ALL_OPERATOR_CIRCLE_BY_PREFIX = "allOperatorCircleByPrefix";
    private static String ALL_CITIES = "allCities";
    private static final String CARD_ISSUERS = "cardIssuers";
    private static final String CARD_ISSUERS_ID_MAP = "cardIssuersIdMap";

    public Map<String, Integer> getAllCardIssuers() {
        return ((Map<String, Integer>) get(CARD_ISSUERS));
    }

    public void setAllCardIssuers(Map<String, Integer> map) {
        if (map != null && !map.isEmpty()) {
            set(CARD_ISSUERS, map);
        }
    }
    
    public Map<String, String> getAllCardIssuersIdMap() {
        return ((Map<String, String>) get(CARD_ISSUERS_ID_MAP));
    }

    public void setAllCardIssuersIdMap(Map<String, String> map) {
        if (map != null && !map.isEmpty()) {
            set(CARD_ISSUERS_ID_MAP, map);
        }
    }
    
    public Map<Integer, CityMaster> getAllCities() {
        return (Map<Integer, CityMaster>) get(ALL_CITIES);
    }

    public void setAllCities(Map<Integer, CityMaster> allCities) {
        if(allCities!=null && allCities.size()>0) set(ALL_CITIES, allCities);
    }

	public List<InAggrOprCircleMap> getInAggrOprCircleMap() {
		return (List<InAggrOprCircleMap>) get(IN_AGGR_OPR_CIRCLE_MAP_CACHE);
	}

	public List<InOprAggrPriority> getInOprAggrPriority() {
		return (List<InOprAggrPriority>) get(IN_OPPR_AGGR_PRIORITY_CACHE);
	}

	public List<CircleOperatorMapping> getCircleOperatorMapping() {
		return (List<CircleOperatorMapping>) get(CIRCLE_OPERATOR_MAPPING_CACHE);
	}

	// Error Code Mapping

	public List<InErrorcodeMap> getInErrorcodeMap(String aggrRespCode, String aggrName) {
		Map<String, List<InErrorcodeMap>> listOfMap = (Map<String, List<InErrorcodeMap>>) get(IN_ERRORCODE_MAP_CACHE);
		if (listOfMap != null && listOfMap.size() > 0 && aggrRespCode != null && aggrName != null) {
			return listOfMap.get(aggrRespCode +"-"+ aggrName);
		}
		return null;
	}

	public void setInErrorcodeMap(List<InErrorcodeMap> inErrorCodeList) {
		Map<String, List<InErrorcodeMap>> listOfMap = new HashMap<String, List<InErrorcodeMap>>();
		if (inErrorCodeList != null && inErrorCodeList.size() > 0) {
			for (InErrorcodeMap inErrorCodeMap : inErrorCodeList) {
				if (listOfMap.get(inErrorCodeMap.getAggrRespCode() +"-"+ inErrorCodeMap.getAggrName()) != null) {
					List<InErrorcodeMap> inIterateErrorCodeList = listOfMap.get(inErrorCodeMap.getAggrRespCode() +"-"+ inErrorCodeMap.getAggrName());
					inIterateErrorCodeList.add(inErrorCodeMap);
					listOfMap.put(inErrorCodeMap.getAggrRespCode() +"-"+ inErrorCodeMap.getAggrName(), inIterateErrorCodeList);
				} else {
					List<InErrorcodeMap> inIterateErrorCodeList = new ArrayList<InErrorcodeMap>();
					inIterateErrorCodeList.add(inErrorCodeMap);
					listOfMap.put(inErrorCodeMap.getAggrRespCode() +"-"+ inErrorCodeMap.getAggrName(), inIterateErrorCodeList);
				}
			}
		}
		set(IN_ERRORCODE_MAP_CACHE, listOfMap);
	}
	
    // Recharge Retry data cache

    @SuppressWarnings("unchecked")
    public List<InRechargeRetryMap> getOperatorRetryConfig(String aggrRespCode, String aggrName, String operatorId, boolean isOperatorRetryForAllError) {
        Map<String, List<InRechargeRetryMap>> listOfMap = (Map<String, List<InRechargeRetryMap>>) get(IN_RECHARGERETRY_MAP_CACHE);
        if (listOfMap != null && listOfMap.size() > 0 && aggrRespCode != null && aggrName != null && operatorId != null) {
            List<InRechargeRetryMap> inRechargeRetryMaps = listOfMap.get(aggrRespCode + "-" + aggrName + "-" + operatorId);
            if (isOperatorRetryForAllError && listOfMap.get("*-" + aggrName + "-" + operatorId) != null) {
                if (inRechargeRetryMaps == null) {
                    inRechargeRetryMaps = new ArrayList<>();
                }
                inRechargeRetryMaps.addAll(listOfMap.get("*-" + aggrName + "-" + operatorId));
            }
            return inRechargeRetryMaps;
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<InRechargeRetryMap> getAllOperatorRetryConfig() {
        Map<String, List<InRechargeRetryMap>> listOfMap = (Map<String, List<InRechargeRetryMap>>) get(IN_RECHARGERETRY_MAP_CACHE);
        List<InRechargeRetryMap> finalRetryMap = new ArrayList<>();
        if (listOfMap == null) {
            return finalRetryMap;
        }
        for (Entry<String, List<InRechargeRetryMap>> retryEntry : listOfMap.entrySet()) {
            finalRetryMap.addAll(retryEntry.getValue());
        }
        return finalRetryMap;
    }

	public void setOperatorRetryConfig(List<InRechargeRetryMap> inRechargeRetryList) {
		Map<String, List<InRechargeRetryMap>> listOfMap = new HashMap<String, List<InRechargeRetryMap>>();
		if (inRechargeRetryList != null && inRechargeRetryList.size() > 0) {
			for (InRechargeRetryMap inRechargeRetryMap : inRechargeRetryList) {
				if (listOfMap.get(inRechargeRetryMap.getAggrRespCode() +"-"+ inRechargeRetryMap.getAggrName() +"-"+ inRechargeRetryMap.getFkOperatorMasterId()) != null) {
					List<InRechargeRetryMap> inIterateRechargeRetryList = listOfMap.get(inRechargeRetryMap.getAggrRespCode() +"-"+ inRechargeRetryMap.getAggrName()
							+"-"+ inRechargeRetryMap.getFkOperatorMasterId());
					inIterateRechargeRetryList.add(inRechargeRetryMap);
					listOfMap.put(inRechargeRetryMap.getAggrRespCode() +"-"+ inRechargeRetryMap.getAggrName() +"-"+ inRechargeRetryMap.getFkOperatorMasterId(), inIterateRechargeRetryList);
				} else {
					List<InRechargeRetryMap> inIterateRechargeRetryList = new ArrayList<InRechargeRetryMap>();
					inIterateRechargeRetryList.add(inRechargeRetryMap);
					listOfMap.put(inRechargeRetryMap.getAggrRespCode() +"-"+ inRechargeRetryMap.getAggrName()+"-"+ inRechargeRetryMap.getFkOperatorMasterId(), inIterateRechargeRetryList);
				}
			}
		}
		set(IN_RECHARGERETRY_MAP_CACHE, listOfMap);
	}

	// catching for circle masters

	public List<CircleMaster> getCircles() {
		return (List<CircleMaster>) get(CIRCLE_CACHE);
	}

	public void setCircles(List<CircleMaster> circles) {
	    set(CIRCLE_CACHE, circles);
	}

	public CircleMaster getCirclesByID(Integer circleID) {

		if (circleID != null) {
			Map<Integer, CircleMaster> map = (HashMap<Integer, CircleMaster>) get(MAP_CIRCLE_MASTER_ID);
			if (map != null && map.size() > 0) {
				return map.get(circleID);
			}
		}
		return null;
	}

	public void setCircleByField(List<CircleMaster> operators) {
		Map<Integer, CircleMaster> mapByCircleMaster = new HashMap<Integer, CircleMaster>();
		Map<String, CircleMaster> mapByCirclerMasterName = new HashMap<String, CircleMaster>();
		if (operators != null && operators.size() > 0) {
			for (CircleMaster circleMaster : operators) {
				mapByCircleMaster.put(circleMaster.getCircleMasterId(), circleMaster);
				mapByCirclerMasterName.put(circleMaster.getName(), circleMaster);
			}
			set(MAP_CIRCLE_MASTER_ID, mapByCircleMaster);
			set(MAP_CIRCLE_MASTER_NAME, mapByCirclerMasterName);
		}
	}

	public CircleMaster getCirclesByName(String circleName) {

		if (circleName != null) {
			Map<String, CircleMaster> map = (HashMap<String, CircleMaster>) get(MAP_CIRCLE_MASTER_NAME);
			if (map != null && map.size() > 0) {
				return map.get(circleName);
			}
		}
		return null;
	}

	/* catching for operator master */

	public List<OperatorMaster> getOperators() {
		return (List<OperatorMaster>) get(OPERATORS_CACHE);
	}

	public void setOperators(List<OperatorMaster> operators) {
	    set(OPERATORS_CACHE, operators);
	}
	
	// catching for product masters

	public List<ProductMaster> getProducts() {
		return (List<ProductMaster>) get(PRODUCTS_CACHE);
	}

	public void setProducts(List<ProductMaster> products) {
		set(PRODUCTS_CACHE, products);
	}
	
	// catching for Names masters

		public List<String> getNames() {
			return (List<String>) get(NAMES_CACHE);
		}

		public void setNames(List<String> names) {
			set(NAMES_CACHE, names);
		}

	public OperatorMaster getOperatorsByFieldID(Integer operatorMasterID) {

		if (operatorMasterID != null) {
			Map<Integer, OperatorMaster> map = (HashMap<Integer, OperatorMaster>) get(MAP_OPERATOR_MASTER_ID);
			if (map != null && map.size() > 0) {
				return map.get(operatorMasterID);
			}
		}
		return null;
	}

	public OperatorMaster getOperatorsByFieldCode(String operatorMasterCode) {
		if (operatorMasterCode != null) {
			Map<String, OperatorMaster> map = (HashMap<String, OperatorMaster>) get(MAP_OPERATOR_MASTER_CODE);
			if (map != null && map.size() > 0) {
				Iterator it = map.keySet().iterator();
				return map.get(operatorMasterCode.toString());
			}
		}
		return null;
	}

	public List<OperatorMaster> getOperatorsByProductID(Integer productID) {
		if (productID != null) {
			Map<String, List<OperatorMaster>> map = (HashMap<String, List<OperatorMaster>>) get(LIST_OPERATOR_MASTER_PRODUCT_ID);
			if (map != null && map.size() > 0)
				return map.get(productID);
		}
		return null;
	}

	public void setOperatorsByField(List<OperatorMaster> operators) {
		boolean status = false;
		Map<Integer, OperatorMaster> mapByOperatorMaster = new HashMap<Integer, OperatorMaster>();
		Map<String, OperatorMaster> mapByOperatorMasterName = new HashMap<String, OperatorMaster>();
		Map<Integer, List<OperatorMaster>> mapByOperatorMasterProductID = new HashMap<Integer, List<OperatorMaster>>();
		if (operators != null && operators.size() > 0) {
			for (OperatorMaster operatorMaster : operators) {
				mapByOperatorMaster.put(operatorMaster.getOperatorMasterId(), operatorMaster);
				mapByOperatorMasterName.put(operatorMaster.getOperatorCode().toString(), operatorMaster);
				if (mapByOperatorMasterProductID.get(operatorMaster.getFkProductMasterId()) != null) {
					List<OperatorMaster> listOfProductMasterID = mapByOperatorMasterProductID.get(operatorMaster.getFkProductMasterId());
					listOfProductMasterID.add(operatorMaster);
					mapByOperatorMasterProductID.put(operatorMaster.getFkProductMasterId(), listOfProductMasterID);
				} else {
					List<OperatorMaster> operatorMasters = new ArrayList<OperatorMaster>();
					operatorMasters.add(operatorMaster);
					mapByOperatorMasterProductID.put(operatorMaster.getFkProductMasterId(), operatorMasters);
				}

			}
			set(MAP_OPERATOR_MASTER_ID, mapByOperatorMaster);
			set(MAP_OPERATOR_MASTER_CODE, mapByOperatorMasterName);
			if (status)
			    set(LIST_OPERATOR_MASTER_PRODUCT_ID, mapByOperatorMasterProductID);
		}
	}

	public List<Map<String, Object>> getAllOperators() {
		return (List<Map<String, Object>>) get(ALL_OPERATORS_CACHE);
	}

	public void setAllOperators(List<Map<String, Object>> allOperators) {
		if (allOperators != null && allOperators.size() > 0) {
		    set(ALL_OPERATORS_CACHE, allOperators);
		}
	}

	public void setAllOperatorsByPrefix(List<Map<String, Object>> allOperatorsByPrefix, String prefixValue, String productMasterId) {

		Map<String, List<Map<String, Object>>> map = new HashMap<String, List<Map<String, Object>>>();

		if (allOperatorsByPrefix != null && allOperatorsByPrefix.size() > 0) {
			for (Map<String, Object> allOperatorsByPrefixs : allOperatorsByPrefix) {
				if (allOperatorsByPrefixs.get(prefixValue) != null && allOperatorsByPrefixs.get(productMasterId) != null) {

				}

			}
		}

		if (allOperatorsByPrefix != null && allOperatorsByPrefix.size() > 0) {
		    set(OPERATOR_CIRCLE_BY_PREFIX, allOperatorsByPrefix);
		}
	}

	public List<Map<String, Object>> getAllOperatorsByPrefix() {
		return (List<Map<String, Object>>) get(OPERATOR_CIRCLE_BY_PREFIX);
	}

	// Operator Circle Prefix

	public List<Map<String, Object>> getAllOperatorCircleByPrefix() {
		return (List<Map<String, Object>>) get(ALL_OPERATOR_CIRCLE_BY_PREFIX);
	}

	public void setAllOperatorCircleByPrefix(List<Map<String, Object>> list) {
		if (list != null && list.size() > 0) {
		    set(ALL_OPERATOR_CIRCLE_BY_PREFIX, list);
		}
	}

	public List<Map<String, Object>> getAllPrefixAllOperatorAllCircle() {
		return (List<Map<String, Object>>) get(ALL_PREFIX_OPERATOR_CIRCLE);
	}

	public void setAllPrefixAllOperatorAllCircle(List<Map<String, Object>> list) {
		if (list != null && list.size() > 0) {
		    set(ALL_PREFIX_OPERATOR_CIRCLE, list);
		}
	}

	// Circle Operator Mapping

	public CircleOperatorMapping getCircleOperatorMapping(Integer circleOperatorMappingId) {
		if (circleOperatorMappingId != null) {
			Map<Integer, CircleOperatorMapping> map = (HashMap<Integer, CircleOperatorMapping>) get(CIRCLE_OPERATOR_MAPPING_ID);
			if (map != null && map.size() > 0) {
				return map.get(circleOperatorMappingId);
			}
		}
		return null;
	}

	public CircleOperatorMapping getCircleOperatorMapping(Integer operatorId, Integer circleId) {
		if (operatorId != null && circleId != null) {
			Map<String, CircleOperatorMapping> map = (HashMap<String, CircleOperatorMapping>) get(CIRCLE_AND_OPERATOR_MAPPING_ID);
			if (map != null && map.size() > 0) {
				return map.get(operatorId +"-"+ circleId);
			}
		}
		return null;
	}

	public void setCircleOperatorMapping(List<CircleOperatorMapping> circleOperatorMappings) {
		boolean status = false;
		Map<Integer, CircleOperatorMapping> mapByCircleOperatorMappingID = new HashMap<Integer, CircleOperatorMapping>();
		Map<String, CircleOperatorMapping> mapByCircleANDOperatorMappingID = new HashMap<String, CircleOperatorMapping>();
		if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
			for (CircleOperatorMapping circleOperatorMapping : circleOperatorMappings) {
				mapByCircleOperatorMappingID.put(circleOperatorMapping.getCircleOperatorMappingId(), circleOperatorMapping);
				mapByCircleANDOperatorMappingID.put(circleOperatorMapping.getFkOperatorMasterId() +"-"+ circleOperatorMapping.getFkCircleMasterId(), circleOperatorMapping);
			}
			set(CIRCLE_OPERATOR_MAPPING_ID, mapByCircleOperatorMappingID);
			if (status)
			    set(CIRCLE_AND_OPERATOR_MAPPING_ID, mapByCircleANDOperatorMappingID);

		}
	}

	// Aggregator Operator Circle Mapping

	public List<InAggrOprCircleMap> getInAggrOprCircleMapByID(Integer operatorId, Integer circleID) {
		if (operatorId != null && circleID != null) {
			Map<String, List<InAggrOprCircleMap>> map = (HashMap<String, List<InAggrOprCircleMap>>) get(IN_AGGR_OPR_CIRCLE_BY_ID);
			if (map != null && map.size() > 0) {
				return map.get(operatorId +"-"+ circleID);
			}
		}
		return null;
	}

	public void setInAggrOprCircleMap(List<InAggrOprCircleMap> inAggrOprCircleList) {
		boolean status = false;
		Map<String, List<InAggrOprCircleMap>> listOfMap = new HashMap<String, List<InAggrOprCircleMap>>();
		if (inAggrOprCircleList != null && inAggrOprCircleList.size() > 0) {
			for (InAggrOprCircleMap aggregatorOperatorcircleMap : inAggrOprCircleList) {
				if (listOfMap.get(aggregatorOperatorcircleMap.getFkOperatorId() +"-"+ aggregatorOperatorcircleMap.getFkCircleId()) != null) {
					List<InAggrOprCircleMap> list = listOfMap.get(aggregatorOperatorcircleMap.getFkOperatorId() +"-"+ aggregatorOperatorcircleMap.getFkCircleId());
					list.add(aggregatorOperatorcircleMap);
					listOfMap.put(aggregatorOperatorcircleMap.getFkOperatorId() +"-"+ aggregatorOperatorcircleMap.getFkCircleId(), list);
				} else {
					List<InAggrOprCircleMap> list = new ArrayList<InAggrOprCircleMap>();
					list.add(aggregatorOperatorcircleMap);
					listOfMap.put(aggregatorOperatorcircleMap.getFkOperatorId() +"-"+ aggregatorOperatorcircleMap.getFkCircleId(), list);
				}
			}
		}

		if (listOfMap != null && listOfMap.size() > 0) {
		    set(IN_AGGR_OPR_CIRCLE_BY_ID, listOfMap);
		}
	}

	public InOprAggrPriority getInOprAggrPriorityByID(Integer circleOperatorMappingId) {
		if (circleOperatorMappingId != null) {
			Map<Integer, InOprAggrPriority> map = (HashMap<Integer, InOprAggrPriority>) get(MAP_BY_CIRCLE_OPERATOR_MAPPING_ID);
			if (map != null && map.size() > 0) {
				return map.get(circleOperatorMappingId);
			}
		}
		return null;
	}

	public void setInOprAggrPriority(List<InOprAggrPriority> inOprAggrPriorities) {

		Map<Integer, InOprAggrPriority> mapByCircleOperatorMappingID = new HashMap<Integer, InOprAggrPriority>();
		if (inOprAggrPriorities != null && inOprAggrPriorities.size() > 0) {
			for (InOprAggrPriority inoprAggrPriority : inOprAggrPriorities) {
				mapByCircleOperatorMappingID.put(inoprAggrPriority.getFkcircleOperatorMappingId(), inoprAggrPriority);
			}
			set(MAP_BY_CIRCLE_OPERATOR_MAPPING_ID, mapByCircleOperatorMappingID);
		}
	}

	public void deleteInOprAggrPriority() {
		delete(MAP_BY_CIRCLE_OPERATOR_MAPPING_ID);
	}
	
	public List<InAggrOprCircleMap> getAllInAggrOprCircleMap(){
        List<InAggrOprCircleMap> allInAggrOprCircleMap= new ArrayList<InAggrOprCircleMap>();
        Map<String, List<InAggrOprCircleMap>> map = (HashMap<String, List<InAggrOprCircleMap>>) get(IN_AGGR_OPR_CIRCLE_BY_ID);
        if (map != null && map.size() > 0) {
            for(List<InAggrOprCircleMap> aggrCircleMap:map.values()){
                if(aggrCircleMap != null && aggrCircleMap.size() > 0){
                    allInAggrOprCircleMap.addAll(aggrCircleMap);
                }
            }
        }
        return allInAggrOprCircleMap.size() > 0 ? allInAggrOprCircleMap : null;
    }

	/*
	 * public List<Map<String, Object>> getOperatorCircleByPrefix() { return (List<Map<String, Object>>) get(OPERATOR_CIRCLE_BY_PREFIX); }
	 */

	/*
	 * public boolean setOperatorCircleByPrefix(List<Map<String, Object>> operatorCircleByPrefix) { return add(OPERATOR_CIRCLE_BY_PREFIX, operatorCircleByPrefix); }
	 */

}