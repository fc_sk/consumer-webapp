package com.freecharge.app.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.service.model.CouponCartItem;
import com.freecharge.app.domain.dao.CouponCartDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class CouponCartService {
	
	private static Logger logger = LoggingFactory.getLogger(CouponCartDao.class);
	
	@Autowired
	private CouponCartDao couponCartDao;
	
	@Autowired
	private OrderIdReadDAO orderIdDao;
	
	public List<CouponCartItem> getCouponCartItemsForOrderId(String orderId) {
		String lookupId = orderIdDao.getLookupIdForOrderId(orderId);
		if (lookupId == null){
			throw new IllegalStateException("No lookup id found for orderId: " + orderId);
		}
		return couponCartDao.getCouponCartItems(lookupId);
	}
	
	public Long getFreefundCouponIdInCartForLookupId(String lookupId) {
		try{
			return couponCartDao.getFreefundCouponIdInCartForLookupId(lookupId);
		} catch (Exception e) {
			logger.error("getting error in getFreefundCouponIdInCartForLookupId",e);
		}
		return null;
	}

	public List<Map<String, Object>> getMastreMestroPaymentTypeStatus(String lookupId){
		return couponCartDao.getMastreMestroPaymentTypeStatus(lookupId);
	}
	
}