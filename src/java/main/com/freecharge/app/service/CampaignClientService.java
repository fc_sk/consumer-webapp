package com.freecharge.app.service;

import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.exception.CampaignConditionException;
import com.freecharge.app.service.exception.InvalidPaymentRequestException;
import com.freecharge.campaign.ICampaignService;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.services.PaymentType.PaymentTypeName;
import com.freecharge.payment.util.BilldeskCardHash;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.webdo.FCUPIWebDO;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CampaignClientService {

	private static Logger logger = LoggingFactory
			.getLogger(CampaignClientService.class);

	@Autowired
	private CartItemsDAO cartItemsDAO;

	@Autowired
	private CartService cartService;

	@Autowired
	private ICampaignService campaignServiceClient;

	@Autowired
	private FreefundService freefundService;

	@Autowired
	private PricingService pricingService;

	@Autowired
	private WalletWrapper walletWrapper;
	
	@Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

	public void evaluateCampaignOnPaymentInitiation(
			ProductPaymentBusinessDO productPaymentBusinessDO, String lookupId,
			Map<String, String> storedCardDetails)
			throws CampaignConditionException, InvalidPaymentRequestException {
		try {
			Integer freefundCouponId = getFreefundCouponIdIfExistsForLookupId(lookupId);
			if ((freefundCouponId != null && freefundCouponId != -1)
					|| "UPI".equals(productPaymentBusinessDO.getPaymentoption())) {
				FreefundCoupon freefundCoupon = null;
				if (freefundCouponId != null && freefundCouponId != -1) {
					freefundCoupon = freefundService
							.getFreefundCoupon(freefundCouponId.longValue());
				}
				Map<String, Object> intxnDataMap = new HashMap<>();
				if (productPaymentBusinessDO != null
						&& productPaymentBusinessDO
								.getPaymentRequestCardDetails() != null
						&& !FCUtil
								.isEmpty(productPaymentBusinessDO
										.getPaymentRequestCardDetails()
										.getCardNumber())
						|| (!FCUtil.isEmpty(storedCardDetails) && storedCardDetails
								.containsKey(CardStorageKey.CARD_NUMBER))) {
					String cardNumber = FCUtil.isEmpty(productPaymentBusinessDO
							.getPaymentRequestCardDetails().getCardNumber()) ? storedCardDetails
							.get(CardStorageKey.CARD_NUMBER)
							: productPaymentBusinessDO
									.getPaymentRequestCardDetails()
									.getCardNumber();
					if (!FCUtil.isEmpty(cardNumber)) {
						intxnDataMap.put(InTxnDataConstants.CARD_BIN,
								cardNumber.substring(0, 6));
						intxnDataMap.put(InTxnDataConstants.CARD_HASH,
								BilldeskCardHash.generateHash(cardNumber));
					}
				}

				if (productPaymentBusinessDO.getLookupID() != null) {
					CartBusinessDo cartBusinessDo = cartService
							.getCartBusinessDoByLid(productPaymentBusinessDO
									.getLookupID());
					if (cartBusinessDo == null) {
						throw new InvalidPaymentRequestException(
								Arrays.asList("No cart present for payment processing of lookupid : "
										+ productPaymentBusinessDO
												.getLookupID()));
					}
					Double expectedPayAmount = pricingService
							.getPayableAmount(cartBusinessDo);
					logger.info("Payment type "
							+ Integer.parseInt(productPaymentBusinessDO
									.getPaymenttype()) + " " + lookupId);
					if (((!FCUtil.isEmpty(productPaymentBusinessDO
							.getPaymenttype())
							&& FCUtil.isInteger(productPaymentBusinessDO
									.getPaymenttype()) && Integer
							.parseInt(productPaymentBusinessDO.getPaymenttype()) == com.freecharge.payment.services.PaymentType.PaymentTypeName.ONECHECK_WALLET
							.getPaymentTypeCode()) || productPaymentBusinessDO
								.getUsePartialWalletPayment())
							&& expectedPayAmount != null
							&& expectedPayAmount > 0) {
						Integer userId = cartBusinessDo.getUserId();
						Amount walletBalance = walletWrapper
								.getTotalBalance(userId);
						Amount walletAmount = walletBalance;
						Amount pgAmount = new Amount(expectedPayAmount
								- walletAmount.getAmount().doubleValue());
						intxnDataMap.put(InTxnDataConstants.PG_AMOUNT, pgAmount
								.getAmount().doubleValue());
						intxnDataMap.put(InTxnDataConstants.WALLET_AMOUNT,
								walletAmount.getAmount().doubleValue());
					} else {
						intxnDataMap.put(InTxnDataConstants.PG_AMOUNT,
								expectedPayAmount);
						intxnDataMap.put(InTxnDataConstants.WALLET_AMOUNT, 0);
					}
				}

				if ("UPI".equals(productPaymentBusinessDO.getPaymentoption())) {
					logger.info("Payment code for UPI: "
							+ PaymentType.PaymentTypeName.UPI
									.getPaymentTypeCode());
					intxnDataMap.put(InTxnDataConstants.PAYMENT_TYPE,
							PaymentType.PaymentTypeName.UPI
									.getPaymentTypeCode());
					intxnDataMap.put("paymentOption",
							productPaymentBusinessDO.getPaymentoption());
					int count = 0 ;
					try
					{
						 count = userTransactionHistoryDAO.countOfTransactionByUserForOption(productPaymentBusinessDO.getUserId(), productPaymentBusinessDO.getOrderId(), productPaymentBusinessDO.getPaymentoption());
					}catch(Exception exception)
					{
						logger.error(
								"Error while fetching user transaction history data: ",
								exception);
					}
					intxnDataMap.put("transactionCountForPaymentOption", count);
				} else if (FCUtil.isInteger(productPaymentBusinessDO
						.getPaymenttype())) {
					intxnDataMap.put(InTxnDataConstants.PAYMENT_TYPE,
							Integer.parseInt(productPaymentBusinessDO
									.getPaymenttype()));
				} else {
					logger.error("Couldnt parse payment type "
							+ productPaymentBusinessDO.getPaymenttype()
							+ " for " + lookupId);
				}
				addFCUPIInfoForCampaign(productPaymentBusinessDO, intxnDataMap);

				try {
					logger.info("Populating payment data to intxndata "
							+ intxnDataMap + " for lookupid " + lookupId);
					campaignServiceClient.saveInTxnData1(lookupId,
							FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID,
							intxnDataMap);
				} catch (Exception e) {
					logger.error(
							"Error saving intxndata in payment gateway handler: ",
							e);
				}

				if (freefundCoupon != null) {
					CampaignResult campaignResult = campaignServiceClient
							.checkAndProcessCampaign1(lookupId, freefundCoupon
									.getFreefundClassId().intValue(),
									"PAYMENT_SUCCESS",
									FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
					if (!campaignResult.getStatus()) {
						logger.info(freefundCoupon.getFreefundCode()
								+ " ["
								+ freefundCoupon.getFreefundClassId()
								+ "] "
								+ campaignResult.getConditionResult()
										.getFailedRule().getFailureMessage());
						throw new CampaignConditionException(campaignResult
								.getConditionResult().getFailedRule()
								.getDetailedFailureMessage());
					}
				}
			} else {
				logger.info("Promocode not found in cart for lookupId "
						+ lookupId);
			}
		} catch (Exception e) {
			if (e instanceof CampaignConditionException
					|| e instanceof InvalidPaymentRequestException) {
				throw e;
			}
			logger.error(
					"Error while processing campaign check on payment success for "
							+ lookupId, e);
		}
	}

	private Integer getFreefundCouponIdIfExistsForLookupId(String lookupId) {
		CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
		if (cartBusinessDo != null) {
			List<CartItems> cartItemsToUpdate = cartItemsDAO
					.findByFkCartId(cartBusinessDo.getId().intValue());
			if (cartItemsToUpdate != null && !cartItemsToUpdate.isEmpty()) {
				for (CartItems cartItemToUpdate : cartItemsToUpdate) {
					if (FCConstants.FFREEFUND_PRODUCT_TYPE
							.equals(cartItemToUpdate.getEntityId())
							&& !cartItemToUpdate.getIsDeleted()) {
						return cartItemToUpdate.getFkItemId();
					}
				}
			}
		} else {
			logger.info("Cart not found for lookupId " + lookupId);
		}
		return new Integer(-1);
	}
	
	private void addFCUPIInfoForCampaign(ProductPaymentBusinessDO productPaymentBusinessDO, Map<String, Object> intxnDataMap){
	    
	    if(!PaymentConstants.PAYMENT_TYPE_FCUPI.equals(productPaymentBusinessDO.getPaymenttype()))
	        return;
	    
	    FCUPIWebDO fcUpiObject = getFCUpiPaymentObject(productPaymentBusinessDO.getUpiInfo());
	    intxnDataMap.put(InTxnDataConstants.SOURCE_VPA, fcUpiObject.getAc().getVpa());
	    intxnDataMap.put(InTxnDataConstants.DESTINATION_VPA, fcUpiObject.getPayees().get(0).getBeneVpa());
	    intxnDataMap.put(InTxnDataConstants.REMITTER_BANK_IFSC, fcUpiObject.getAc().getIfsc());
	    
	}
	
	public FCUPIWebDO getFCUpiPaymentObject(String upiInfo)  {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<FCUPIWebDO>(){
            }.getType();
            return gson.fromJson(upiInfo, type);
        } catch (Exception e) {
            logger.error("upiInfo object parsing Exception for upiInfo:"+upiInfo);
            throw e;
        }
    }
}
