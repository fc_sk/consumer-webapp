package com.freecharge.app.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.mobile.web.util.MobileUtil;

@Component
public class OrderService {
	
    @Autowired
    private OrderIdSlaveDAO orderSlaveDao;
    
    @Autowired
    private OrderIdWriteDAO orderIdDao;
    
    private static Logger logger = LoggingFactory.getLogger(OrderService.class);
    
	public Integer getUserIdForOrder(String orderId){
	    return orderSlaveDao.getUserIdFromOrderId(orderId);
	}
	
	public TxnHomePageBusinessDO getHomePageForOrder(String orderId) {
	    return orderSlaveDao.getTxnHomePageByOrderId(orderId);
	}
	
	@Transactional
	public OrderId getByOrderId(String orderIdStr) {
		OrderId orderId = null;
		List<OrderId> orderIds = orderIdDao.getByOrderId(orderIdStr);
		if (orderIds != null && orderIds.size() > 0){
			orderId = orderIds.get(0);
		}
		return orderId;
	}
	
	public boolean isAppOrder(String orderIdStr){
		boolean isAppOrder = false;
		List<OrderId> orderIdList = orderIdDao.getByOrderId(orderIdStr);
		if (orderIdList == null || orderIdList.isEmpty()) {
            logger.error("No order found for orderID: [" + orderIdStr + "]. Doing nothing");
            return false;
        }
        
		if (orderIdList.size() > 1) {
            logger.error("More than one order found for orderID: [" + orderIdStr + "]. Taking first one in the list and proceeding");
        }
        OrderId orderToInspect = orderIdList.get(0);
        if (orderToInspect != null){
        	Integer channelId = orderToInspect.getChannelId();
        	if (channelId == null){
        		logger.error("Got null channel id for orderId: " + orderIdStr);
        	} else{
        		isAppOrder = MobileUtil.isAppChannel(channelId);
        	}
        }
        return isAppOrder;
	}
	
	public String getChannelName(String orderIdStr){
        String channel = null;
        List<OrderId> orderIdList = orderIdDao.getByOrderId(orderIdStr);
        if (orderIdList == null || orderIdList.isEmpty()) {
            logger.error("No order found for orderID: [" + orderIdStr + "]. Doing nothing");
            return null;
        }
        
        if (orderIdList.size() > 1) {
            logger.error("More than one order found for orderID: [" + orderIdStr + "]. Taking first one in the list and proceeding");
        }
        OrderId orderToInspect = orderIdList.get(0);
        if (orderToInspect != null){
            Integer channelId = orderToInspect.getChannelId();
            if (channelId == null){
                logger.error("Got null channel id for orderId: " + orderIdStr);
            } else{
               channel = getChannelName(channelId); 
            }
        }
        return channel;
    }
	
	public String getChannelName(Integer channelId){
	    return FCConstants.channelNameMasterMap.get(FCConstants.channelMasterMap.get(channelId));
	}

	public String getProductTypeForAllRechargeCase(String orderId) {
		return orderSlaveDao.getProductTypeForAllRechargeCase(orderId);
	}
	
	public String getBillIdForOrderId(String orderId,String productType) {
        return orderSlaveDao.getBillIdForOrderId(orderId,productType);
    }
	
	public String getLookupIdForOrderId(String orderId){
		return orderSlaveDao.getLookupIdForOrderId(orderId);
	}
}
