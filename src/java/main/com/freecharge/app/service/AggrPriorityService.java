package com.freecharge.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class AggrPriorityService {

    private static final Logger            logger = LoggingFactory.getLogger(AggrPriorityService.class);

    @Autowired
    private AggrPriorityTemplateService aggrPriorityTemplateService;
    
    @Autowired
    private OperatorCircleWriteDAO operatorCircleWriteDao;
    
    @Autowired
    private InCachedService        cachedService;

    public Map<String, Object> activateTemplate(String templateName, String currentAdminUser) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        int[] updatesStatus = null;
        List<CircleOperatorMapping> circleOperatorMappings = aggrPriorityTemplateService.getTemplate(templateName);
        if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
            updatesStatus = operatorCircleWriteDao.batchUpdateAGPriority(circleOperatorMappings, currentAdminUser, null);
        }
        respMap.put("updates", updatesStatus!=null?sum(updatesStatus):0);
        cachedService.deleteInOprAggrPriority();
        return respMap;
    }
    
    private int sum(int[] updatesStatus) {
        int sum = 0;
        for(int i : updatesStatus) {
            sum+=i;
        }
        return sum;
    }

}
