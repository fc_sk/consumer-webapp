package com.freecharge.app.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.ICartDAO;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.IStateMasterDAO;
import com.freecharge.app.domain.dao.UserBillPaymentHistoryReadDAO;
import com.freecharge.app.domain.dao.UserProfileDAO;
import com.freecharge.app.domain.dao.UserRechargeContactDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.UserRechargeContact;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.common.businessdo.InvoiceBusinessDO;
import com.freecharge.common.businessdo.MyContactsBussinessDO;
import com.freecharge.common.businessdo.MyContactsListBusinessDo;
import com.freecharge.common.businessdo.MyRechargeContactProfileBusinessDO;
import com.freecharge.common.businessdo.MyRechargesBusinessDO;
import com.freecharge.common.businessdo.ProfileBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.exception.IdentityAuthorizedException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.service.SessionService;
import com.freecharge.common.framework.session.service.UserSessionService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.coupon.service.CouponServiceProxy;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.UserDataPointSNSService;
import com.freecharge.uuip.UserDataPointType;
import com.freecharge.web.mapper.UIResponseMapper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CartAndCartItemsVO;
import com.freecharge.web.webdo.FeeCharge;
import com.freecharge.web.webdo.FeeDetails;
import com.freecharge.web.webdo.FeeShare;
import com.freecharge.web.webdo.MyContactsWebDO;
import com.freecharge.web.webdo.RechargeInfoVO;
import com.freecharge.web.webdo.ServiceChargeEntity;

/**
 * Service class for My Account functionality.
 */
@Service
public class MyAccountService {
    /*
     * Android App Version until which rechargeDetails call should not return
     * Add Cash transactions
     */
    private static final Integer          ANDROID_VERSION_NO_CASH_TXNS = 34;

    /*
     * IOS App Version until which rechargeDetails call should not return Add
     * Cash transactions
     */
    private static final Integer          IOS_VERSION_NO_CASH_TXNS     = 4;

    private Logger                        logger                       = LoggingFactory.getLogger(getClass());
    private HomeBusinessDao               homeBusinessDao;
    private OperatorCircleService         operatorCircleService;

    @Autowired
    private IRegistrationDAO              registrationDAO;

    @Autowired
    private UserBillPaymentHistoryReadDAO userBillPaymentReadDAO;

    @Autowired
    private CityMasterDAO                 cityMasterDAO;

    @Autowired
    private ICartDAO                      cartDao;

    @Autowired
    private IStateMasterDAO               stateMasterDAO;

    @Autowired
    private UserWriteDao                  userWriteDao;

    @Autowired
    private UserProfileDAO                userProfileDAO;

    @Autowired
    private OperatorCircleReadDAO         operatorCircleDAO;

    @Autowired
    private UserRechargeContactDAO        userRechargeContactDAO;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    SessionService sessionService;
    
    @Autowired
    UserSessionService userSessionService;

    @Autowired
    private UserDataPointSNSService            userDataPointSNSService;

    @Autowired
    private FreefundService               freefundService;

    @Autowired
    private CrosssellService              crosssellService;

    @Autowired
    private EPinEncrypt                   ePinEncrypt;
    
    @Autowired
    private CouponServiceProxy couponServiceProxy;

    @Autowired
	private OrderIdWriteDAO orderIdDAO;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private ConvinenceFeeDetailsDao convinenceDao;
    
    
    @Autowired
    public void setHomeBusinessDao(final HomeBusinessDao homeBusinessDao) {
        this.homeBusinessDao = homeBusinessDao;
    }

    @Autowired
    public void setOperatorCircleService(final OperatorCircleService operatorCircleService) {
        this.operatorCircleService = operatorCircleService;
    }
    
    @Transactional(readOnly = true)
    public void getUserTransactionHistory(final BaseBusinessDO baseBusinessDO) {
        logger.debug("Entered into the getUserTransactionHistory()");
        MyRechargesBusinessDO myRechargesBusinessDO = (MyRechargesBusinessDO) baseBusinessDO;

        final SimpleDateFormat yMD                   = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        final SimpleDateFormat rechargeHistoryFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Integer userId = myRechargesBusinessDO.getUserid();

            List<UserTransactionHistory> userTransactionHistory = userTransactionHistoryService
                    .findUserTransactionHistoryFromUserId(userId, WebConstants.RECHARGE_HISTORY_LIMIT);

            List<Map<String, String>> rechargeHistoryList = new ArrayList<Map<String, String>>();
            for (UserTransactionHistory list : userTransactionHistory) {
            	if(!myRechargesBusinessDO.isReturnPaymentTxns() && 
            			PaymentConstants.paymentTransactionStatusToExclude.contains(list.getTransactionStatus())){
            		continue;
            	}
                String productType = FCConstants.productTypeToNameMap.get(list.getProductType().toString());
                if (productType.equals("WalletCash") && !FCUtil.isEmpty(myRechargesBusinessDO.getChannel())) {
                    if (!returnAddCashTxnsForMobileApp(myRechargesBusinessDO.getChannel(),
                            myRechargesBusinessDO.getAppVersion())) {
                        continue;
                    }
                }
                String rechargeDate = rechargeHistoryFormat.format((Date) list.getCreatedOn());
                String rechargeYmd = yMD.format((Date) list.getCreatedOn());

                Map<String, String> map = new HashMap<String, String>();
                map.put("createdYmd", rechargeYmd);
                map.put("createdOn", rechargeDate);
                map.put("orderId", list.getOrderId());
                map.put("txnAmount", list.getTransactionAmount() + "");
                map.put("serviceNumber", list.getSubscriberIdentificationNumber());
                map.put("productName", FCConstants.productTypeToNameMap.get(list.getProductType().toString()) + "");
                map.put("operatorCode", list.getServiceProvider() + "");
                map.put("circleName", list.getServiceRegion());
                if(list.getServiceProvider() == null || list.getServiceProvider().isEmpty()){
                	RechargeDetails rechargeDetails = orderIdDAO.getRechargeDetailsFromHomePageByOrderID(list.getOrderId());
                	if(rechargeDetails != null){
                		map.put("operatorCode", rechargeDetails.getOperatorName());
                		map.put("circleName", rechargeDetails.getCircleName());
                	}
                }
                map.put("rechargeStatus", UIResponseMapper.getRechargeReponseForUser(list.getTransactionStatus()) + "");
                map.put("retryNumber", "");

                rechargeHistoryList.add(map);
            }

            // sort for chronology
            Collections.sort(rechargeHistoryList, new Comparator<Map<String, String>>() {
                @Override
                public int compare(final Map<String, String> element1, final Map<String, String> element2) {
                    return element2.get("createdYmd").compareTo(element1.get("createdYmd"));
                }
            });

            myRechargesBusinessDO.setRechargeHistoryList(rechargeHistoryList);
            logger.debug("Leaving  from the getUserTransactionHistory()");
        } catch (Exception e) {
            logger.error(
                    "Exception Raised while fetching recharge History form Db for email "
                            + myRechargesBusinessDO.getEmail(), e);
        }
    }

    private boolean returnAddCashTxnsForMobileApp(final String channel, final Integer mobileAppVersionFromRequest) {
        if (!FCUtil.isEmpty(channel)) {
            int channelNo = Integer.parseInt(channel);
            if ((channelNo == FCConstants.CHANNEL_ID_ANDROID_APP
                    && mobileAppVersionFromRequest <= ANDROID_VERSION_NO_CASH_TXNS)
                    || (channelNo == FCConstants.CHANNEL_ID_IOS_APP
                    && mobileAppVersionFromRequest <= IOS_VERSION_NO_CASH_TXNS)) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    public void getProfile(final BaseBusinessDO baseBusinessDO) {
    	List<List<Object>> objectList = homeBusinessDao.getProfileInfo(baseBusinessDO);
    	populateUserProfileData(baseBusinessDO, objectList);
    }

    private void populateUserProfileData(final BaseBusinessDO baseBusinessDO, final List<List<Object>> objectList) {
        Users users = null;
        UserProfile userProfile = null;
        List<StateMaster> stateMasterList = registrationDAO.getStateMasterList();
        // EPinEncrypt ePinEncrypt = new EPinEncrypt();
        try {
            if (objectList != null && objectList.size() == 1) {
                List<Object> list = objectList.get(0);
                users = (Users) list.get(0);
                userProfile = (UserProfile) list.get(1);
                Integer userstate = null;
                List<StateMaster> userstatelist = registrationDAO.getStateNameByStateId(userProfile
                        .getFkStateMasterId());
                if (!FCUtil.isEmpty(userstatelist)) {
                    userstate = userstatelist.get(0).getStateMasterId();
                }
                List<CityMaster> usercitylist = null;
                if (userstate != null) {
                    try {
                        usercitylist = cityMasterDAO.findByFkStateMasterId(userstate);
                    } catch (NullPointerException ne) {
                        logger.error("Excepiton caused by findByFkStateMasterId(" + userstate + ")", ne);
                    }
                }

                ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
                profilebusinessdo.setArea(userProfile.getArea());
                profilebusinessdo.setCity(userProfile.getFkCityMasterId() == null ? null : String.valueOf(userProfile
                        .getFkCityMasterId()));
                profilebusinessdo.setCitylist(usercitylist);
                profilebusinessdo.setEmail(users.getEmail());
                profilebusinessdo.setLandmar(userProfile.getLandmark());
                profilebusinessdo.setName(userProfile.getFirstName());
                profilebusinessdo.setState(userstate == null ? null : userstate.toString());
                // profilebusinessdo.setOldpassword(ePinEncrypt.getDecryptedPin(users.getPassword()));
                profilebusinessdo.setPincode(userProfile.getPostalCode());
                profilebusinessdo.setStatelist(stateMasterList);
                profilebusinessdo.setAddress(userProfile.getAddress1());
                profilebusinessdo.setUserid(users.getUserId());
                profilebusinessdo.setTitel(userProfile.getTitle());
                profilebusinessdo.setDob(users.getDob());
                profilebusinessdo.setMobileNo(users.getMobileNo());
            }
        } catch (Exception e) {
            logger.error("freecharge error in getprofile" + e);
        }
    }

    @Transactional
    public void getProfileByUserId(final BaseBusinessDO baseBusinessDO) {
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        List<List<Object>> objectList = homeBusinessDao.getProfileInfoByUserId(profilebusinessdo.getUserid());
        populateUserProfileData(baseBusinessDO, objectList);
    }

    @Transactional
    public BaseBusinessDO getRechargeContactProfile(final BaseBusinessDO baseBusinessDO) {

        MyRechargeContactProfileBusinessDO myRchgContactProfileBusinessDO =
                (MyRechargeContactProfileBusinessDO) baseBusinessDO;
        try {

            UserContactDetail userContactDetail = homeBusinessDao.getLastestUserContactProfile(baseBusinessDO);
            if (userContactDetail == null) {
                return null;
            }
            String userstate = null;
            List<StateMaster> userstatelist = registrationDAO.getStateNameByStateId(userContactDetail.getStateId());
            if (!FCUtil.isEmpty(userstatelist)) {
                userstate = userstatelist.get(0).getStateName();
            }
            // List<CityMaster>
            // usercitylist=cityMasterDAO.findByFkStateMasterId(userstate);
            myRchgContactProfileBusinessDO.setEmail(userContactDetail.getEmail());
            myRchgContactProfileBusinessDO.setName(userContactDetail.getFirstName());
            myRchgContactProfileBusinessDO.setTitle(userContactDetail.getTitle());
            myRchgContactProfileBusinessDO.setAddress1(userContactDetail.getAddress1());
            myRchgContactProfileBusinessDO.setArea(userContactDetail.getArea());
            myRchgContactProfileBusinessDO.setLandmark(userContactDetail.getLandmark());
            myRchgContactProfileBusinessDO.setPincode(userContactDetail.getPostalCode());
            myRchgContactProfileBusinessDO.setCity(userContactDetail.getCityName());
            // myRchgContactProfileBusinessDO.setCitylist(usercitylist);
            myRchgContactProfileBusinessDO.setState(userstate);
            // myRchgContactProfileBusinessDO.setStatelist(stateMasterList);
            myRchgContactProfileBusinessDO.setUserId(userContactDetail.getUserId());
            myRchgContactProfileBusinessDO.setUserProfileId(userContactDetail.getUserProfileId());
            myRchgContactProfileBusinessDO.setRechargeContactNo(userContactDetail.getServiceNumber());
        } catch (Exception e) {
            logger.error("Error in fetching UserContactProfile for UserId : "
                    + myRchgContactProfileBusinessDO.getUserId() + "Contact No: "
                    + myRchgContactProfileBusinessDO.getRechargeContactNo() + " Exception: ", e);
        }

        return myRchgContactProfileBusinessDO;
    }

    @Transactional
    public List<ProfileBusinessDO> getUserProfileList(final BaseBusinessDO baseBusinessDO) {

        List<ProfileBusinessDO> profileBusinessDOList = new ArrayList<ProfileBusinessDO>();
        try {

            List<UserProfile> userProfileList = homeBusinessDao.getUserProfileInfo(baseBusinessDO);
            // List<StateMaster> stateMasterList =
            // registrationDAO.getStateMasterList();

            if (!FCUtil.isEmpty(userProfileList)) {
                for (UserProfile userProfile : userProfileList) {
                    Users users = userProfile.getUsers();

                    String userstate = null;

                    List<StateMaster> userstatelist = registrationDAO.getStateNameByStateId(userProfile
                            .getFkStateMasterId());
                    if (!FCUtil.isEmpty(userstatelist)) {
                        userstate = userstatelist.get(0).getStateName();
                    }
                    // List<CityMaster>
                    // usercitylist=cityMasterDAO.findByFkStateMasterId(userstate);

                    DateFormat formatter;
                    formatter = new SimpleDateFormat("yyyy-mm-dd");

                    ProfileBusinessDO profilebusinessdo = new ProfileBusinessDO();
                    profilebusinessdo.setEmail(users.getEmail());
                    profilebusinessdo.setMorf(users.getMorf());
                    profilebusinessdo.setDob(users.getDob());
                    profilebusinessdo.setMobileNo(users.getMobileNo());
                    if (users.getDob() != null) {
                        profilebusinessdo.setDobstring(formatter.format(users.getDob()));
                    }
                    profilebusinessdo.setName(userProfile.getFirstName());
                    profilebusinessdo.setAddress(userProfile.getAddress1());
                    profilebusinessdo.setArea(userProfile.getArea());
                    profilebusinessdo.setLandmar(userProfile.getLandmark());
                    profilebusinessdo.setPincode(userProfile.getPostalCode());
                    profilebusinessdo.setCity(String.valueOf(userProfile.getFkCityMasterId()));
                    // profilebusinessdo.setCitylist(usercitylist);
                    profilebusinessdo.setState(userstate);
                    // profilebusinessdo.setStatelist(stateMasterList);
                    profilebusinessdo.setUserid(users.getUserId());
                    profilebusinessdo.setUserProfileId(userProfile.getUserProfileId());

                    profileBusinessDOList.add(profilebusinessdo);
                }
            }
        } catch (Exception e) {
            logger.error("freecharge error in getprofile" + e);
        }

        return profileBusinessDOList;
    }

    public void updateProfile(final BaseBusinessDO baseBusinessDO) {

        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        boolean validOldPassword = false;
        Users user = new Users();

        user.setEmail(profilebusinessdo.getEmail());
        user.setUserId(profilebusinessdo.getUserid());
        user.setDob(profilebusinessdo.getDob());
        user.setMorf(profilebusinessdo.getMorf());
        user.setMobileNo(profilebusinessdo.getMobileNo());
        if (!FCUtil.isEmpty(profilebusinessdo.getConfirmpassword()) && profilebusinessdo.isIspasswordupdate()) {
            try {
                String encripwd = ePinEncrypt.getEncryptedPin(profilebusinessdo.getOldpassword());
                profilebusinessdo.setOldpassword(encripwd);
                validOldPassword = homeBusinessDao.checkUserPassword(profilebusinessdo);
                user.setPassword(ePinEncrypt.getEncryptedPin(profilebusinessdo.getConfirmpassword()));
            } catch (Exception e) {
                logger.error("Error in updated profile password encriptiond", e);
            }
        }

        UserProfile profile = new UserProfile();
        if(!FCUtil.isWelFormedName(profilebusinessdo.getName())) {
        	throw new IllegalArgumentException("Invalid name for userId:" + profilebusinessdo.getUserid());
        } else {
             profile.setFirstName(profilebusinessdo.getName());
        }
        String title = null;
        if ("F".equals(profilebusinessdo.getMorf())) {
            title = "Ms";
        } else if ("M".equals(profilebusinessdo.getMorf())) {
            title = "Mr";
        }
        profile.setTitle(title);

        if (profilebusinessdo.getMorf() != null) {
            profile.setTitle(title);
        } else {
            profile.setTitle(profilebusinessdo.getTitle());
        }
        List list = new LinkedList();
        list.add(user);
        list.add(profile);
        homeBusinessDao.updateProfile(list, validOldPassword);
        profilebusinessdo.setOldpassword(null);
        profilebusinessdo.setNewpassword(null);
        profilebusinessdo.setConfirmpassword(null);
        getProfile(profilebusinessdo);
        // Publishing user data points for Mystique (Unique User Identification Service)
        userDataPointSNSService.publishUserDataPoint(profilebusinessdo.getUserid(), profilebusinessdo.getMobileNo(),
                UserDataPointType.PROFILE_NO, new Date());

        if (validOldPassword) {
        	List<String> sessionIds = userSessionService.retriveSessionIdsByUserId(user.getUserId());
        	for (String sessionId : sessionIds) {
        		sessionService.deleteSession(sessionId);
        		userSessionService.deleteSessionByUserId(user.getUserId());
        		}
        	FreechargeContextDirectory.invalidateData();
        	}
        }
    
    public boolean updateDefaultProfile(final BaseBusinessDO baseBusinessDO) {
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        boolean passwordupdate = false;

        Users user = new Users();
        user.setEmail(profilebusinessdo.getEmail());
        user.setUserId(profilebusinessdo.getUserid());
        user.setDob(profilebusinessdo.getDob());
        user.setMorf(profilebusinessdo.getMorf());
        user.setMobileNo(profilebusinessdo.getMobileNo());

        if (!FCUtil.isEmpty(profilebusinessdo.getConfirmpassword()) && profilebusinessdo.isIspasswordupdate()) {
            try {
                String encripwd = ePinEncrypt.getEncryptedPin(profilebusinessdo.getOldpassword());
                profilebusinessdo.setOldpassword(encripwd);
                passwordupdate = homeBusinessDao.checkUserPassword(profilebusinessdo);
                user.setPassword(ePinEncrypt.getEncryptedPin(profilebusinessdo.getConfirmpassword()));
            } catch (Exception e) {
                logger.error("Error in updated profile password encriptiond ", e);
            }
        }

        UserProfile profile = new UserProfile();
        profile.setFirstName(profilebusinessdo.getName());
        String morf = profilebusinessdo.getMorf();
        if (morf != null) {
            String title = "F".equalsIgnoreCase(morf) ? "Ms." : ("M".equalsIgnoreCase(morf) ? "Mr." : null);
            profile.setTitle(title);
        } else {
            profile.setTitle(profilebusinessdo.getTitle());
        }
        Integer stateId = 1;
        List<StateMaster> stateMasterList = stateMasterDAO.findByProperty("stateName", profilebusinessdo.getState());
        if (!FCUtil.isEmpty(stateMasterList)) {
            stateId = stateMasterList.get(0).getStateMasterId();
        }
        profile.setFkStateMasterId(stateId);
        profile.setArea(profilebusinessdo.getArea());

        // CityMaster cityMasters =
        // this.cityMasterDAO.read(Integer.parseInt(profilebusinessdo.getCity().trim()));
        try {
            profile.setFkCityMasterId(Integer.parseInt(profilebusinessdo.getCity()));
        } catch (RuntimeException e) { // this covers both NumberFormatException and NullPointerException
            profile.setFkCityMasterId(1);
        }

        profile.setLandmark(profilebusinessdo.getLandmar());
        profile.setAddress1(profilebusinessdo.getAddress());
        profile.setPostalCode(profilebusinessdo.getPincode());
        profile.setFkUserId(user.getUserId());

        List list = new LinkedList();
        list.add(user);
        list.add(profile);
        boolean updateStatus = homeBusinessDao.updateDefaultProfile(list, profilebusinessdo.isIspasswordupdate());
        profilebusinessdo.setOldpassword(null);
        profilebusinessdo.setNewpassword(null);
        profilebusinessdo.setConfirmpassword(null);

        return updateStatus;
    }

    @Transactional
    public boolean updateRechargeContactProfile(final BaseBusinessDO baseBusinessDO) {

        MyRechargeContactProfileBusinessDO myRchgContactProfileBusinessDO =
                (MyRechargeContactProfileBusinessDO) baseBusinessDO;
        boolean updateStatus = false;
        try {

            // Check if the userId + email is valid combination for a user.
            List<Users> userList = homeBusinessDao.validateUserIdAndEmail(myRchgContactProfileBusinessDO.getUserId(),
                    myRchgContactProfileBusinessDO.getEmail());
            if (FCUtil.isEmpty(userList)) {
                return updateStatus;
            }
            UserProfile userProfile = new UserProfile();

            List<StateMaster> stateMasterList = stateMasterDAO.findByProperty("stateName",
                    myRchgContactProfileBusinessDO.getState());
            Integer stateId = FCUtil.isEmpty(stateMasterList) ? 0 : stateMasterList.get(0).getStateMasterId();
            Integer userRechargeContactId = 0;
            List<UserRechargeContact> list = registrationDAO.getUserRechargeContact(
                    myRchgContactProfileBusinessDO.getEmail(), myRchgContactProfileBusinessDO.getRechargeContactNo());
            if (!FCUtil.isEmpty(list)) {
                userRechargeContactId = list.get(0).getUserRechargeContactId();
            } else {
                Integer circleId = 0;
                Integer operatorId = 0;

                CircleMaster circleMaster = operatorCircleDAO.getCircle(myRchgContactProfileBusinessDO.getCircle());
                if (circleMaster != null) {
                    circleId = circleMaster.getCircleMasterId();
                }
                OperatorMaster operatorMaster = operatorCircleDAO.getOperator(myRchgContactProfileBusinessDO
                        .getOperatorCode());
                if (operatorMaster != null) {
                    operatorId = operatorMaster.getOperatorMasterId();
                }
                com.freecharge.app.domain.entity.jdbc.UserRechargeContact userRechargeContact =
                        new com.freecharge.app.domain.entity.jdbc.UserRechargeContact();
                userRechargeContact.setUserRechargeContactId(myRchgContactProfileBusinessDO.getUserRechargeContactId());
                userRechargeContact.setFkUsersId(myRchgContactProfileBusinessDO.getUserId());
                userRechargeContact.setIsActive(true);
                userRechargeContact.setServiceNumber(myRchgContactProfileBusinessDO.getRechargeContactNo());
                userRechargeContact.setCreatedOn(new Timestamp((new Date()).getTime()));
                userRechargeContact.setFkCircleMasterId(circleId);
                userRechargeContact.setFkOperatorMasterId(operatorId);
                userRechargeContact.setFkProductMasterId(Integer.parseInt(myRchgContactProfileBusinessDO.getProduct()));
                userRechargeContact.setLastRechargeAmount(myRchgContactProfileBusinessDO.getLastRechargeAmount());

                userRechargeContact = userWriteDao.insertUserRechargeContactWithId(userRechargeContact);
                userRechargeContactId = userRechargeContact.getUserRechargeContactId();
            }

            /*
             * TODO Change this to pick countryId from countryMaster. Currently
             * using 99 by default for India.
             */
            final int countryId = 99;

            userProfile.setAddress1(myRchgContactProfileBusinessDO.getAddress1());
            userProfile.setArea(myRchgContactProfileBusinessDO.getArea());

            // List<CityMaster> cityMasters =
            // this.cityMasterDAO.findByProperty("cityName",
            // myRchgContactProfileBusinessDO.getCity());
            userProfile.setFkCityMasterId(Integer.parseInt(myRchgContactProfileBusinessDO.getCity()));

            userProfile.setFirstName(myRchgContactProfileBusinessDO.getName());
            userProfile.setFkCountryMasterId(countryId);
            userProfile.setFkStateMasterId(stateId);
            userProfile.setFkUserId(myRchgContactProfileBusinessDO.getUserId());
            userProfile.setFkUserRechargeContactId(userRechargeContactId);
            userProfile.setIsActive(true);
            userProfile.setIsDefault(false);
            userProfile.setLandmark(myRchgContactProfileBusinessDO.getLandmark());
            userProfile.setPostalCode(myRchgContactProfileBusinessDO.getPincode());
            userProfile.setTitle(myRchgContactProfileBusinessDO.getTitle());
            userProfile.setCreatedOn(new Timestamp((new Date()).getTime()));

            UserProfile newUserProfile = userProfileDAO.create(userProfile);

            if (newUserProfile == null) {
                return updateStatus;
            } else {
                updateStatus = true;
            }
            String userstate = null;

            List<StateMaster> userstatelist = registrationDAO
                    .getStateNameByStateId(newUserProfile.getFkStateMasterId());
            if (!FCUtil.isEmpty(userstatelist)) {
                userstate = userstatelist.get(0).getStateName();
            }
            myRchgContactProfileBusinessDO.setName(newUserProfile.getFirstName());
            myRchgContactProfileBusinessDO.setTitle(newUserProfile.getTitle());
            myRchgContactProfileBusinessDO.setAddress1(newUserProfile.getAddress1());
            myRchgContactProfileBusinessDO.setArea(newUserProfile.getArea());
            myRchgContactProfileBusinessDO.setLandmark(newUserProfile.getLandmark());
            myRchgContactProfileBusinessDO.setPincode(newUserProfile.getPostalCode());
            myRchgContactProfileBusinessDO.setCity(newUserProfile.getFkCityMasterId().toString());
            myRchgContactProfileBusinessDO.setState(userstate);
            myRchgContactProfileBusinessDO.setUserId(newUserProfile.getFkUserId());
            myRchgContactProfileBusinessDO.setUserProfileId(newUserProfile.getUserProfileId());

        } catch (Exception e) {
            logger.error("Error in updateRechargeContactProfile for UserId : "
                    + myRchgContactProfileBusinessDO.getUserId() + "Contact No: "
                    + myRchgContactProfileBusinessDO.getRechargeContactNo() + " Exception: " + e);
        }

        return updateStatus;
    }

    @Transactional
    public void checkUserOldPassword(final BaseBusinessDO baseBusinessDO) throws Exception {
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        String password = ePinEncrypt.getEncryptedPin(profilebusinessdo.getOldpassword());
        profilebusinessdo.setOldpassword(password);
        boolean pswdchek = homeBusinessDao.checkUserPassword(profilebusinessdo);
        profilebusinessdo.setIsoldpswdcorrect(pswdchek);
    }

    @Transactional
    public void checkOldPassword(final BaseBusinessDO baseBusinessDO) throws Exception {
        ProfileBusinessDO profilebusinessdo = ((ProfileBusinessDO) (baseBusinessDO));
        String password = ePinEncrypt.getEncryptedPin(profilebusinessdo.getOldpassword());
        profilebusinessdo.setOldpassword(password);
        boolean pswdchek = homeBusinessDao.checkUserPasswordWithEmailUserId(profilebusinessdo);
        profilebusinessdo.setIsoldpswdcorrect(pswdchek);
    }

    public void updateAddress(final BaseBusinessDO baseBusinessDO) {
        ProfileBusinessDO businessdo = ((ProfileBusinessDO) (baseBusinessDO));
        Users user = new Users();
        UserProfile profile = new UserProfile();
        profile.setArea(businessdo.getArea());

        try {
            Integer cityId = Integer.parseInt(businessdo.getCity());
            profile.setFkCityMasterId(cityId);
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot convert [" + businessdo.getCity() + "] to Integer, not updating city");
        }

        try {
            Integer stateId = Integer.parseInt(businessdo.getState());
            profile.setFkStateMasterId(stateId);
        } catch (NumberFormatException nfe) {
            logger.warn("Cannot convert [" + businessdo.getState() + "] to Integer, not updating state");
        }

        profile.setLandmark(businessdo.getLandmar());
        profile.setAddress1(businessdo.getAddress());
        profile.setPostalCode(businessdo.getPincode());
        user.setEmail(businessdo.getEmail());
        user.setUserId(businessdo.getUserid());
        List userandprofile = new LinkedList();
        userandprofile.add(user);
        userandprofile.add(profile);
        homeBusinessDao.updateAddress(userandprofile);
        getProfile(businessdo);
    }

    @Transactional
    public void getMyRechargeContacts(final BaseBusinessDO baseBusinessDO) {
        MyContactsListBusinessDo mycontactslistbusinessdo = ((MyContactsListBusinessDo) (baseBusinessDO));

        try {
            List<UserRechargeContact> userRechargeContactList = userRechargeContactDAO.findByProperty("fkUsersId",
                    mycontactslistbusinessdo.getUserid());

            List<MyContactsBussinessDO> myContactsBusinessDOList = new ArrayList<MyContactsBussinessDO>();
            for (UserRechargeContact userRechargeContact : userRechargeContactList) {
                if (userRechargeContact.getIsActive()) {
                    MyContactsBussinessDO myContactsBusinessDO = new MyContactsBussinessDO();

                    String circle = null;
                    CircleMaster circleMaster = operatorCircleDAO.getCircle(userRechargeContact.getFkCircleMasterId());
                    if (circleMaster != null) {
                        circle = circleMaster.getName();
                    }
                    myContactsBusinessDO.setCircle(circle);

                    myContactsBusinessDO.setContactid(Integer.toString(userRechargeContact.getUserRechargeContactId()));
                    myContactsBusinessDO.setName(userRechargeContact.getNickAlias());

                    String operatorname = null;

                    OperatorMaster operatorMaster = operatorCircleDAO.getOperator(userRechargeContact
                            .getFkOperatorMasterId());
                    if (operatorMaster != null) {
                        operatorname = operatorMaster.getOperatorCode();
                    }
                    myContactsBusinessDO.setOperatorname(operatorname);
                    myContactsBusinessDO.setProduct(userRechargeContact.getFkProductMasterId());
                    myContactsBusinessDO.setServiceno(userRechargeContact.getServiceNumber());
                    myContactsBusinessDO.setUserid(userRechargeContact.getFkUsersId());

                    myContactsBusinessDOList.add(myContactsBusinessDO);
                }
            }

            mycontactslistbusinessdo.setContactslist(myContactsBusinessDOList);
        } catch (Exception e) {
            logger.error("Exception caught while fetching userRechargeContacts : " + e);
        }
    }

    @Transactional
    public void getMycontacts(final BaseBusinessDO baseBusinessDO) {
        MyContactsListBusinessDo mycontactslistbusinessdo = ((MyContactsListBusinessDo) (baseBusinessDO));
        List contactslist = new LinkedList();
        Integer prodid = 0;
        List<List<Object>> objectlist = homeBusinessDao.getMycontacts(mycontactslistbusinessdo.getUserid());
        List<ProductMaster> allproducts = homeBusinessDao.getAllProducts();
        for (ProductMaster promst : allproducts) {
            if (promst.getProductName().equalsIgnoreCase("mobile")) {
                prodid = promst.getProductMasterId();
            }
            break;
        }
        List<OperatorMaster> operatorslist = operatorCircleService.getOperatorsByProductId(prodid);
        final int serviceNumIndex = 0;
        final int nameIndex = 1;
        final int contactIdIndex = 2;
        final int prodIdIndex = 3;
        final int operatorIdIndex = 4;
        for (List<Object> list : objectlist) {
            Object serviceNoObject = list.get(serviceNumIndex);
            if (serviceNoObject != null && !serviceNoObject.toString().isEmpty()
                    && !"undefined".equals(serviceNoObject.toString()) && ((Integer) list.get(prodIdIndex) == prodid)) {
                MyContactsWebDO contactswebdo = new MyContactsWebDO();
                contactswebdo.setEmail(mycontactslistbusinessdo.getEmail());
                contactswebdo.setServiceno(serviceNoObject.toString());
                contactswebdo.setName((String) list.get(nameIndex));
                contactswebdo.setProduct(prodid);
                contactswebdo.setOperator(((Integer) list.get(operatorIdIndex)).toString());
                // contactswebdo.setCircle(list.get(5).toString());
                try {
                    String contactid = ePinEncrypt.getEncryptedPin(list.get(contactIdIndex).toString());
                    contactswebdo.setContactid(contactid);
                } catch (Exception e) {
                    // TODO handle exception
                    logger.error("Exception while encrypting Contact ID: ", e);
                }
                int operatorid = (Integer) list.get(operatorIdIndex);
                for (OperatorMaster operator : operatorslist) {
                    if (operator.getOperatorMasterId() == operatorid) {
                        contactswebdo.setOperatorname(operator.getOperatorCode());
                        break;
                    }
                }

                contactslist.add(contactswebdo);
                contactswebdo.setOperatorlist(operatorslist);
            }
        }
        mycontactslistbusinessdo.setContactslist(contactslist);
        // mycontactslistbusinessdo.setOperatorlist(alloperators);
    }

    @Transactional
    public void updateContact(final BaseBusinessDO baseBusinessDO) {
        MyContactsBussinessDO mycontactsbusinessdo = ((MyContactsBussinessDO) (baseBusinessDO));
        int status = 0;
        /*
         * List<Map>
         * circlemap=homeBusinessDao.getAllOperatorsByPrefix(mycontactsbusinessdo
         * .getServiceno().toString().substring(0, 4),
         * mycontactsbusinessdo.getProduct().toString()); if(circlemap != null){
         * Map map=(Map)circlemap.get(0); Integer circlid=new
         * Integer(map.get("circleMasterId").toString());
         * mycontactsbusinessdo.setCircle(circlid);
         * //mycontactsbusinessdo.setOperator
         * ((Integer)map.get("operatorMasterId")); }
         */
        try {
            String contactid = ePinEncrypt.getDecryptedPin(mycontactsbusinessdo.getContactid());
            List<UserProfile> profilelist = homeBusinessDao.profileNotexitIncontacts(baseBusinessDO);
            boolean contstatusinprofile = false;
            UserProfile defaltprofile = null;
            for (UserProfile profile : profilelist) {
                Integer fkcontactid = profile.getFkUserRechargeContactId();
                if (fkcontactid != null && fkcontactid.toString().equals(contactid)) {
                    contstatusinprofile = true;
                    break;
                }
                if (profile.getIsDefault()) {
                    defaltprofile = profile;
                }
            }
            if (!contstatusinprofile) {
                defaltprofile.setIsDefault(false);
                defaltprofile.setFirstName(mycontactsbusinessdo.getName());
                defaltprofile.setUserProfileId(null);
                defaltprofile.setFkUserRechargeContactId(new Integer(contactid));
                status = homeBusinessDao.saveProfile(defaltprofile);
            } else {
                mycontactsbusinessdo.setContactid(contactid.toString());
                status = homeBusinessDao.updaeContact(baseBusinessDO);
            }
        } catch (Exception e) {
            logger.error("error in updating the user contact");
        }

        if (status == 0) {
            mycontactsbusinessdo.setUpdatestatus(false);
        } else {
            mycontactsbusinessdo.setUpdatestatus(true);
        }
    }

    @Transactional
    public void getInvoiceInfo(final BaseBusinessDO baseBusinessDO) {
        InvoiceBusinessDO invoiceBusinessDO = (InvoiceBusinessDO) baseBusinessDO;

        if(invoiceBusinessDO.getOrderId() == null || invoiceBusinessDO.getOrderId().equals("")) {
        	logger.error("OrderId cannot be empty");
        }
        UserTransactionHistory uthObj = userTransactionHistoryService.findUserTransactionHistoryByOrderId(invoiceBusinessDO.getOrderId());
        Integer userId = FCSessionUtil.getLoggedInUserId();
        if(uthObj!=null && uthObj.getFkUserId()!=null && !uthObj.getFkUserId().equals(userId)) {
        	throw new IdentityAuthorizedException("Invalid user session");
        }
        
        RechargeInfoVO rechargeInfoVO = new RechargeInfoVO();
        String rechargeDate = uthObj.getCreatedOn().toString();
        rechargeDate = rechargeDate.substring(0, rechargeDate.indexOf(" "));
        rechargeInfoVO.setOrderId(invoiceBusinessDO.getOrderId());

        Float rechargedAmount = uthObj.getTransactionAmount().floatValue();
        Float totalAmount = new Double(rechargedAmount).floatValue();

//            if (freefundService.isFreefundRedeemedForOrder(invoiceBusinessDO.getOrderId())) {
//                Double freefundAmt = freefundService.getFreeFundItemPriceByOrderId(invoiceBusinessDO.getOrderId());
//                totalAmount -= new Float(freefundAmt);
//                rechargeInfoVO.setHasFreeFund(true);
//                // needs to be read from config
//                rechargeInfoVO.setFreeFundAmount(new Float(freefundAmt));
//            }

        rechargeInfoVO.setRechargeAmount(rechargedAmount);
        rechargeInfoVO.setRechargedDate(rechargeDate);
        rechargeInfoVO.setRechargedNo(uthObj.getSubscriberIdentificationNumber());
        rechargeInfoVO.setServiceCharge(Float.parseFloat("10"));

        invoiceBusinessDO.setRechargeInfoVO(rechargeInfoVO);
        if(appConfigService.isConvenienceFeeEnabled()) {
        	String lookupId = orderIdDAO.getLookupIdForOrderId(invoiceBusinessDO.getOrderId());
        	FeeCharge feeCharge=convinenceDao.getFeeDetails(lookupId);
        	logger.info("feeCharge: " +feeCharge);
        	if(feeCharge!=null) {
        		rechargeInfoVO.setHasFeeDetails(true);
        		logger.info("hasFeeDetails: "+rechargeInfoVO.isHasFeeDetails());
        		FeeDetails feesDetails = new FeeDetails();
        		Map<ServiceChargeEntity, FeeShare> serviceChargeShare =  feeCharge.getServiceChargeShare();
        		BigDecimal handlingCharge = feeCharge.getFeeAmount().subtract(serviceChargeShare.get(ServiceChargeEntity.ServiceTax).getAmount()).
        				subtract(serviceChargeShare.get(ServiceChargeEntity.KrishiKalyanCess).getAmount()).
        				subtract(serviceChargeShare.get(ServiceChargeEntity.SwachhBharatCess).getAmount());
        		feesDetails.setPaymentHandlingCharge(handlingCharge.setScale(3, RoundingMode.HALF_UP));
        		feesDetails.setKrishiKalyanCess(serviceChargeShare.get(ServiceChargeEntity.KrishiKalyanCess).getAmount().setScale(3, RoundingMode.HALF_UP));
        		feesDetails.setServiceTax(serviceChargeShare.get(ServiceChargeEntity.ServiceTax).getAmount().setScale(3, RoundingMode.HALF_UP));
        		feesDetails.setSwachhBharatCess(serviceChargeShare.get(ServiceChargeEntity.SwachhBharatCess).getAmount().setScale(3, RoundingMode.HALF_UP));
        		rechargeInfoVO.setFeeDetails(feesDetails);
        	}
        }
        rechargeInfoVO.setTotalAmount(totalAmount);

    }

    @Transactional
    public void getCouponInfo(final BaseBusinessDO baseBusinessDO) {
        logger.debug("Entering into the getCouponInfo method of the class CartDAO, the request input parameters"
                + " are BaseBusinessDO baseBusinessDO:" + baseBusinessDO);
        InvoiceBusinessDO invoiceBusinessDO = ((InvoiceBusinessDO) baseBusinessDO);
        Map<Integer, List<CouponHistory>> couponHistoryMap = couponServiceProxy.getCouponHistoryMap(invoiceBusinessDO
                .getOrderId());
        List<CartAndCartItemsVO> cartAndCartItemsList = new ArrayList<CartAndCartItemsVO>();
        if (couponHistoryMap != null) {
            Iterator<Integer> couponIdIterator = couponHistoryMap.keySet().iterator();
            while (couponIdIterator.hasNext()) {
                List<CouponHistory> couponHistoryList = couponHistoryMap.get(couponIdIterator.next());
                if (!FCUtil.isEmpty(couponHistoryList)) {
                    int quantity = couponHistoryList.size();
                    String campaignName = couponHistoryList.get(0).getCampaignName();
                    int couponValue = couponHistoryList.get(0).getCouponValue();
                    CartAndCartItemsVO cartAndCartItemsVO = new CartAndCartItemsVO();
                    cartAndCartItemsVO.setCampaignName(campaignName);
                    cartAndCartItemsVO.setCouponValue(couponValue * quantity);
                    cartAndCartItemsVO.setQuantity(quantity);
                    cartAndCartItemsList.add(cartAndCartItemsVO);
                }
            }
        }
        invoiceBusinessDO.setCartAndCartItemsList(cartAndCartItemsList);
        logger.debug("Exiting from the getCouponInfo method of the class CartDAO, the request return type void");
    }
}
