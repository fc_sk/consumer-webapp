package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.HomeBusinessReadDao;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserProfileDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserProfile;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.ChangePasswordBusinessDO;
import com.freecharge.common.businessdo.CheckUserBusinessDO;
import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.businessdo.LoginBusinessDO.UserType;
import com.freecharge.common.businessdo.StateMasterBusinessDO;
import com.freecharge.common.businessdo.UserLoginDetailsBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.OrderIdUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.order.service.IOrderMetaDataService;
import com.freecharge.order.service.OrderDataService;
import com.freecharge.order.service.model.OrderMetaData;
import com.freecharge.payment.dos.entity.AppVersionOrder;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.tracker.Tracker;

/**
 * Created by IntelliJ IDEA. User: Manoj Kumar Date: Apr 30, 2012 Time: 12:49:27
 * PM To change this template use File | Settings | File Templates.
 */

@Service
public class UserAuthService {
    private EmailService                    emailService;
    private Logger                          logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private IOrderMetaDataService           orderMetaDataService;

    @Autowired
    private CartService                     cartService;

    @Autowired
    private UserService                     userService;
    
    @Autowired
    private UserServiceProxy                     userServiceProxy;

    @Autowired
    private TxnHomePageDAO                  txnHomePageDao;

    @Autowired
    private UserProfileDAO                  userProfileDAO;

    @Autowired
    private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;

    @Autowired
    private IRegistrationDAO                registrationDAO;

    @Autowired
    private OrderIdWriteDAO                 orderIdWriteDAO;

    @Autowired
    private Tracker                         tracker;

    @Autowired
    private FreechargeEventPublisher        freechargeEventPublisher;

    @Autowired
    private AppVersionInsertDelegate        appVersionInsertDelegate;

    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    private OMSClientService omsClient;

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    HomeBusinessDao     homeBusinessDao;

    @Autowired
    HomeBusinessReadDao homeBusinessReadDao;

    public void setHomeBusinessDao(HomeBusinessDao homeBusinessDao) {
        this.homeBusinessDao = homeBusinessDao;
    }

    @Autowired
    EPinEncrypt   ePinEncrypt;

    @Autowired
    MetricsClient metricsClient;

    public void setEPinEncrypt(EPinEncrypt ePinEncrypt) {
        this.ePinEncrypt = ePinEncrypt;
    }

    @Autowired
    private OrderDataService orderDataService;
        
    /**
     * @param baseBusinessDO
     */
    @Transactional(readOnly = true)
    public void doLogin(BaseBusinessDO baseBusinessDO) throws Exception {
        LoginBusinessDO loginBusinessDO = (LoginBusinessDO) baseBusinessDO;
        Users users = homeBusinessReadDao.getAuthenticateUser(loginBusinessDO.getEmail(),
                ePinEncrypt.getEncryptedPin(loginBusinessDO.getPassword()));
        if (users != null) {
            loginBusinessDO.setLogin(true);
            final Integer userId = users.getUserId();
            loginBusinessDO.setUserId(userId.toString());
            loginBusinessDO.setAffiliateProfileId(users.getFkAffiliateProfileId());
            loginBusinessDO.setUserType(UserType.fromDB(users.getType()));
            tracker.trackUser(userId);
        } else {
            loginBusinessDO.setLogin(false);
        }
        logger.info("isLogin==" + loginBusinessDO.isLogin());
    }

    public void doUserProfile(BaseBusinessDO baseBusinessDO) {
        LoginBusinessDO loginBusinessDO = (LoginBusinessDO) baseBusinessDO;
        List<UserProfile> users = userProfileDAO.findByUserIdAndIsDefalut(
                Integer.parseInt(loginBusinessDO.getUserId()), true);
        if (users != null) {
            UserProfile user = users.get(0);
            if (user != null) {
                loginBusinessDO.setFirstName(user.getFirstName());
            }
        }
    }

    public void changePassword(BaseBusinessDO baseBusinessDO) {
        logger.info("change password method of userauth service");
        int result = homeBusinessDao.changePassword(baseBusinessDO);

        String password = null;
        try {
            password = EPinEncrypt.getDecryptedPin(((ChangePasswordBusinessDO) (baseBusinessDO)).getNewPassword());
        } catch (Exception e) {
            logger.error("exception raised when encrypring password" + e);
        }
        logger.info("password is " + password);
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        Map<String, String> variableValues = new HashMap<String, String>();
        variableValues.put("password", password);
        String imgPrefix = fcPropertyPlaceholderConfigurer.getStringValueStatic("imgprefix1");
        variableValues.put("imgPrefix", imgPrefix);
        logger.info("setting Password Recovery email details");
        ((ChangePasswordBusinessDO) (baseBusinessDO)).setStatus(fcPropertyPlaceholderConfigurer
                .getStringValueStatic("change.password.sussess.msg"));
        String from = fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.from.emailid");
        String subject = fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.sub");
        String replyTo = fcPropertyPlaceholderConfigurer.getStringValueStatic("password.recovery.replyto");
        emailBusinessDO.setVariableValues(variableValues);
        emailBusinessDO.setFrom(from);
        emailBusinessDO.setTo(((ChangePasswordBusinessDO) (baseBusinessDO)).getEmail());
        emailBusinessDO.setTemplateName("ChangePassword.vm");
        emailBusinessDO.setSubject(subject);
        emailBusinessDO.setReplyTo(replyTo);
        emailService.sendEmail(emailBusinessDO);
    }

    public void validateUser(BaseBusinessDO baseBusinessDO) {
        logger.info("validate User Method of userAuth  service");
        logger.info("setting status message based on  the user existance");
        List<Users> users = homeBusinessDao.validateUser(baseBusinessDO);
        if (users != null && users.size() >= 1
                && users.get(0).getEmail().equalsIgnoreCase(((CheckUserBusinessDO) (baseBusinessDO)).getEmail())) {
            logger.info("user is registered with us ");
            ((CheckUserBusinessDO) (baseBusinessDO)).setStatusMsg("0");
        } else {
            logger.info("user is  not registered with us ");
            ((CheckUserBusinessDO) (baseBusinessDO)).setStatusMsg("1");
        }
    }

    @Transactional
    public String generateOrderId(String sessionId, ProductPaymentBusinessDO productPaymentBusinessDO, int affiliateId, int producMasterId,
            int channelId, Integer appVersion) {
    	String lookupId = productPaymentBusinessDO.getLookupID();
        StringBuilder sbld = new StringBuilder().append("session id: ").append(sessionId).append(" lookupid: ")
                .append(lookupId).append(" affiliateid : ").append(affiliateId).append(" product Master Id : ")
                .append(producMasterId).append(" channelid: ").append(channelId).append(" appversion: ")
                .append(appVersion);
        logger.debug("Entered into the  generateOrderId method ..." + sbld.toString());

        // trying to find if there is already a orderid for these parameters
        String orderId = orderIdWriteDAO.getOrderIdByAllParams(sessionId, lookupId, affiliateId, producMasterId,
                channelId);
        if (orderId != null) {
            logger.info("Returning existing orderid : " + orderId + " for " + sbld.toString());
            return orderId;
        }

        String appendedId = OrderIdUtil.getOrderIdPrefix(affiliateId, producMasterId, channelId);

        OrderId generatedOrderId = orderIdWriteDAO.create(getPreparedObjectToSave(sessionId, lookupId, producMasterId,
                affiliateId, channelId, appendedId));
        generatedOrderId = orderIdWriteDAO.update(prepareFullOrderId(generatedOrderId));
        saveOrderMetaData(generatedOrderId.getOrderId(), lookupId);
        logger.debug("Order Id generated is " + generatedOrderId.getOrderId());
        logger.debug("Saving intxndata for lookupId " + lookupId);
        saveInTxnData(lookupId, orderId, appVersion, channelId);
        tracker.trackOrderId(generatedOrderId.getOrderId());
        /*
         * Tracking app version for all the mobile apps.
         */
        if (appVersion != null
                && (channelId == FCConstants.CHANNEL_ID_ANDROID_APP || channelId == FCConstants.CHANNEL_ID_IOS_APP || channelId == FCConstants.CHANNEL_ID_WINDOWS_APP)) {
            AppVersionOrder appVersionOrder = new AppVersionOrder(generatedOrderId.getOrderId(), appVersion,
                    new Timestamp(new Date().getTime()));
            appVersionInsertDelegate.insertAppVersionForOrder(appVersionOrder);
        }
        metricsClient.timeStampEvent(generatedOrderId.getOrderId(), MetricsClient.EventName.PayAttempt);

        return generatedOrderId.getOrderId();
    }

    private void saveOrderMetaData(String orderId, String lookupId) {
        OrderMetaData orderMetaData = new OrderMetaData();
        FreechargeSession session = FCSessionUtil.getCurrentSession();
        if (session != null) {
            String ipAddress = session.getIpAddress();
            
            /*For saving IP address in order_data table */
            orderDataService.insertIPInOrderDataTable(orderId, ipAddress);
           
            String permanentCookie = session.getPermanentCookie();
            orderMetaData.setLookupId(lookupId);
            if (ipAddress != null && !ipAddress.isEmpty()) {
                orderMetaData.setIpAddress(ipAddress);
            }

            if (permanentCookie != null && !permanentCookie.isEmpty()) {
                orderMetaData.setUserCookie(permanentCookie);
            }

            orderMetaData.setCreatedAt(new Date());
            orderMetaData.setOrderId(orderId);
            orderMetaData.setLookupId(lookupId);

            CartBusinessDo cart = cartService.getCartWithNoExpiryCheck(lookupId);
            Integer userId = null;
            if (cart != null) {
                userId = cart.getUserId();
                FreefundCoupon freefundCoupon = cartService.getFreefundCoupon(cart);
                if (freefundCoupon != null) {
                    String freefundCode = freefundCoupon.getFreefundCode();
                    if (freefundCode != null) {
                        orderMetaData.setFreefundCode(freefundCode);
                    }
                }
            }
            if (userId != null) {
                com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getUserByUserId(userId);
                if (user != null) {
                    orderMetaData.setUserId(userId);
                    orderMetaData.setProfileNo(user.getMobileNo());
                    orderMetaData.setUserEmail(user.getEmail());
                }
                TxnHomePage txn = txnHomePageDao.findTxnHomePageByLookUpId(lookupId);
                if (txn != null) {
                    orderMetaData.setRechargeAmount(txn.getAmount());
                    orderMetaData.setServiceNo(txn.getServiceNumber());
                    orderMetaData.setOperatorName(txn.getOperatorName());
                    orderMetaData.setOpertorCircle(txn.getCircleName());
                    orderMetaData.setProductType(txn.getProductType());
                }
            }

            String advertisementId = session.getAdvertisementId();
            String imeiNumber = session.getIMEINumber();
            String deviceUniqueId = session.getDeviceUniqueId();
            String imsi = session.getImsi();
            Boolean isEmulator = session.getIsEmulator();
            logger.info("User auth service inputs:  imei " + imeiNumber + " advID " + advertisementId +
                    "IMSI: " + imsi + "isEmulator: " + isEmulator);
            orderMetaData.setAdvertisementId(advertisementId);
            orderMetaData.setImeiNumber(imeiNumber);
            orderMetaData.setDeviceUniqueId(deviceUniqueId);
            orderMetaData.setImsi(imsi);
            orderMetaData.setIsEmulator(isEmulator);

            orderMetaDataService.saveOrderMetaData(orderMetaData);
        }

    }

    public void saveInTxnData(String lookupId, String orderId, Integer appVersion, int channelId) {
        Map<String, Object> inTxnData = new HashMap<String, Object>();
        inTxnData.put(InTxnDataConstants.CHANNEL, channelId);

        FreechargeSession session = FCSessionUtil.getCurrentSession();
        if (session != null) {
            String ipAddress = session.getIpAddress();
            String permanentCookie = session.getPermanentCookie();
            if (ipAddress != null && !ipAddress.isEmpty()) {
                inTxnData.put(InTxnDataConstants.IP_ADDRESS, ipAddress);
            }

            if (permanentCookie != null && !permanentCookie.isEmpty()) {
                inTxnData.put(InTxnDataConstants.COOKIE, permanentCookie);
            }

            inTxnData.put(InTxnDataConstants.ADVERTISEMENT_ID, session.getAdvertisementId());
            inTxnData.put(InTxnDataConstants.IMEI_NUMBER, session.getIMEINumber());
            inTxnData.put(InTxnDataConstants.DEVICE_UNIQUE_ID, session.getDeviceUniqueId());
            inTxnData.put(InTxnDataConstants.IMSI, session.getImsi());
            inTxnData.put(InTxnDataConstants.IS_EMULATOR, session.getIsEmulator());
        }

        if (appVersion != null
                && (channelId == FCConstants.CHANNEL_ID_ANDROID_APP ||
                    channelId == FCConstants.CHANNEL_ID_IOS_APP ||
                    channelId == FCConstants.CHANNEL_ID_WINDOWS_APP)) {
            inTxnData.put(InTxnDataConstants.APP_VERSION, appVersion);
        }

        try {
            campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, inTxnData);
        }catch (Exception e){
            logger.error("Error occurred while saving intxndata in UserAuthService ", e);
        }
    }

    public OrderId getPreparedObjectToSave(String sessionId, String lookupId, Integer producMasterId,
            Integer affiliateId, Integer channelId, String appendedId) {
        OrderId generatedOrderId = new OrderId();

        generatedOrderId.setSessionId(sessionId);
        generatedOrderId.setLookupId(lookupId);
        generatedOrderId.setFkProductMasterId(producMasterId);
        generatedOrderId.setFkAffiliateProfileId(affiliateId);
        generatedOrderId.setChannelId(channelId);
        generatedOrderId.setOrderId(appendedId);// will be updated just after
                                                // this insert
        generatedOrderId.setOrderIdInc(0);

        return generatedOrderId;
    }

    private OrderId prepareFullOrderId(OrderId orderId) {
        orderId.setOrderId(OrderIdUtil.getFullOrderId(orderId.getOrderId(), String.valueOf(orderId.getPkOrderId())));
        return orderId;
    }

    private String addPrefix(String id) {
        logger.info("entered into the addPrefix() ...");
        StringBuffer sb = new StringBuffer();
        int count = FCConstants.ORDER_ID_LENGTH;
        count = count - id.length();

        while (count > 0) {
            sb.append("0");
            count--;
        }
        logger.info("orderid after added the 0's as prefix is " + sb.toString());
        logger.info("leaving from the addPrefix() ...");
        return sb.toString() + id;
    }

    public List<com.freecharge.web.webdo.StateMaster> getStateMasterList(BaseBusinessDO baseBusinessDO) {
        List<StateMaster> stateMasterList = registrationDAO.getStateMasterList();
        Iterator<StateMaster> ite = stateMasterList.iterator();
        List<com.freecharge.web.webdo.StateMaster> list = new ArrayList<com.freecharge.web.webdo.StateMaster>();
        while (ite.hasNext()) {
            com.freecharge.web.webdo.StateMaster stateList = new com.freecharge.web.webdo.StateMaster();
            StateMaster stateMaster = ite.next();
            stateList.setActive(stateMaster.getIsActive());
            stateList.setStateMasterId(stateMaster.getStateMasterId());
            stateList.setStateName(stateMaster.getStateName());

            list.add(stateList);
        }
        ((StateMasterBusinessDO) (baseBusinessDO)).setStateList(list);
        return list;
    }

    public void getUserLoginInfo(BaseBusinessDO baseBusinessDO) {

        try {

        	UserLoginDetailsBusinessDO userLoginDetailsBusinessDO = ((UserLoginDetailsBusinessDO) (baseBusinessDO));
        	com.freecharge.app.domain.entity.jdbc.UserProfile userProfile = userServiceProxy.getDefaultUserProfile(userLoginDetailsBusinessDO.getUserId());
            userLoginDetailsBusinessDO.setTitle(userProfile.getTitle());
            userLoginDetailsBusinessDO.setFirstName(userProfile.getFirstName());
            userLoginDetailsBusinessDO.setLastName(userProfile.getLastName());
            userLoginDetailsBusinessDO.setUserId(userProfile.getFkUserId());
            userLoginDetailsBusinessDO.setUserProfileId(userProfile.getUserProfileId());

        } catch (Exception exp) {
            logger.error("Exception caught while getting User Login Info: " + exp);
        }

    }

    /**
     * 
     * @param baseBusinessDO
     * @throws Exception
     */
    @Transactional(readOnly = true)
    public void getUserLoginDetailsPasswordless(BaseBusinessDO baseBusinessDO) throws Exception {
        UserLoginDetailsBusinessDO userLoginDetailsBusinessDO = (UserLoginDetailsBusinessDO) baseBusinessDO;
        Users user = homeBusinessReadDao.getOrdinaryUser(userLoginDetailsBusinessDO.getEmail());

        if (user != null) {
            userLoginDetailsBusinessDO.setLoggedIn(true);
            final Integer userId = user.getUserId();
            userLoginDetailsBusinessDO.setUserId(userId);
            getUserLoginInfo(baseBusinessDO);

            tracker.trackUser(userId);
        } else {
            userLoginDetailsBusinessDO.setLoggedIn(false);
        }
        logger.debug("Login Details for User : " + userLoginDetailsBusinessDO.getUserId() + "Name : "
                + userLoginDetailsBusinessDO.getTitle() + " " + userLoginDetailsBusinessDO.getFirstName() + " "
                + userLoginDetailsBusinessDO.getLastName());

    }

    /**
     * 
     * @param baseBusinessDO
     * @throws Exception
     */
    @Transactional(readOnly = true)
    public void getUserLoginDetails(BaseBusinessDO baseBusinessDO) throws Exception {
        UserLoginDetailsBusinessDO userLoginDetailsBusinessDO = (UserLoginDetailsBusinessDO) baseBusinessDO;
        Users user = homeBusinessReadDao.getAuthenticateUser(userLoginDetailsBusinessDO.getEmail(),
                ePinEncrypt.getEncryptedPin(userLoginDetailsBusinessDO.getPassword()));

        if (user != null) {
            userLoginDetailsBusinessDO.setLoggedIn(true);
            final Integer userId = user.getUserId();
            userLoginDetailsBusinessDO.setUserId(userId);
            getUserLoginInfo(baseBusinessDO);

            tracker.trackUser(userId);
        } else {
            userLoginDetailsBusinessDO.setLoggedIn(false);
        }
        logger.debug("Login Details for User : " + userLoginDetailsBusinessDO.getUserId() + "Name : "
                + userLoginDetailsBusinessDO.getTitle() + " " + userLoginDetailsBusinessDO.getFirstName() + " "
                + userLoginDetailsBusinessDO.getLastName());

    }

    public boolean banUser(String email) {
    	boolean deActivate = userServiceProxy.deActivateUser(email);
        if (!deActivate) {
        	return false;
        }
        userService.deleteSessionOfUserTransaction(email);
        return true;	
    }
    
    public boolean unblockUser(String email) {
    	return userServiceProxy.activateUser(email);
    }
    
}
