package com.freecharge.app.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.InRangeReadDAO;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.InWriteDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.recharge.FulfilmentStatus;
import com.freecharge.timeline.event.RechargeAttempt;

@Service
public class InService {
    private static Logger logger = LoggingFactory.getLogger(InService.class);

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private AggregatorFactory aggregatorFactory;

    @Autowired
    private InReadDAO inReadDAO;

    @Autowired
    private InRangeReadDAO inRangeReadDAO;

    @Autowired
    private InWriteDAO inWriteDAO;

    @Autowired
    private InCachedService inCachedService;

    @Autowired
    private HomeBusinessDao homeBusinessDao;
    
    @Autowired
    private AppConfigService appConfigService;

    @Transactional
    public InRequest create(InRequest inRequest) {
        return inWriteDAO.create(inRequest);
    }

    @Transactional
    public InRequest update(InRequest inRequest) {
        inRequest.setUpdatedTime(new Timestamp(new Date().getTime()));
        return inWriteDAO.update(inRequest);
    }

    @Transactional
    public InResponse create(InResponse inResponse) {
        return inWriteDAO.create(inResponse);
    }

    @Transactional
    public InResponse update(InResponse inResponse) {
        inResponse.setUpdatedTime(new Timestamp(new Date().getTime()));
        return inWriteDAO.update(inResponse);
    }

    @Transactional
    public InTransactions update(InTransactions inTransactions) {
        inTransactions.setResponseTime(new Timestamp(new Date().getTime()));
        return inWriteDAO.update(inTransactions);
    }

    @Transactional
    public InTransactions create(InTransactions inTransactions) {
        return inWriteDAO.create(inTransactions);
    }
    
    @Transactional
    public RechargeRetryData create(RechargeRetryData rechargeRetryData) {
        return inWriteDAO.create(rechargeRetryData);
    }

    public static enum INRESPONSE {
        SUCCESS, FAILURE, UNKNOWN
    }

    public RechargeDo insertRequest(RechargeDo rechargeDo) {
        Calendar calendar = Calendar.getInstance();
        InRequest inRequest = new InRequest();
        try {

            inRequest.setAffiliateId(rechargeDo.getAffiliateid());
            inRequest.setAffiliateTransId(rechargeDo.getAffiliateTransId());
            inRequest.setProductType(rechargeDo.getProductType());
            inRequest.setAmount(rechargeDo.getAmount());
            inRequest.setSubscriberNumber(rechargeDo.getMobileNo());

            inRequest.setOperator(rechargeDo.getOperatorCode());
            inRequest.setCircle(rechargeDo.getCircleCode());

            inRequest.setRawRequest("MobileNo : " + rechargeDo.getMobileNo() + ", Affiliate Id : "
                    + rechargeDo.getAffiliateid() + ", AffiliateTransId : " + rechargeDo.getAffiliateTransId()
                    + ", ProductType : " + rechargeDo.getProductType() + ", OperatorCode : "
                    + rechargeDo.getOperatorCode() + ", CircleCode : " + rechargeDo.getCircleCode() + ", Amount : "
                    + rechargeDo.getAmount());

            inRequest.setCreatedTime(new Timestamp(calendar.getTimeInMillis()));
            inRequest.setUpdatedTime(new Timestamp(calendar.getTimeInMillis()));
            inRequest.setRechargePlan(rechargeDo.getRechargePlanType());

            this.create(inRequest);

            rechargeDo.setInRequestId(inRequest.getRequestId());

            return rechargeDo;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }

    }

    public void insertResponse(AggregatorResponseDo aggregatorResponseDo, RechargeDo rechargeDo) {
        logger.info("[IN MODERATOR] Inserting values in InTransaction Table  (insertResponse) ", null);
        InTransactions rechargeTransaction = new InTransactions();
        try {

            rechargeTransaction.setAggrName(aggregatorResponseDo.getAggrName());
            rechargeTransaction.setAggrOprRefernceNo(aggregatorResponseDo.getAggrOprRefernceNo());
            rechargeTransaction.setAggrReferenceNo(aggregatorResponseDo.getAggrReferenceNo());
            rechargeTransaction.setAggrResponseCode(aggregatorResponseDo.getAggrResponseCode());
            rechargeTransaction.setAggrResponseMessage(aggregatorResponseDo.getAggrResponseMessage());
            rechargeTransaction.setFkRequestId(aggregatorResponseDo.getRequestId());
            rechargeTransaction.setRawRequest(aggregatorResponseDo.getRawRequest());
            rechargeTransaction.setRequestTime(new Timestamp(aggregatorResponseDo.getRequestTime().getTime()));
            rechargeTransaction.setResponseTime(new Timestamp(aggregatorResponseDo.getResponseTime().getTime()));
            rechargeTransaction.setRawResponse(aggregatorResponseDo.getRawResponse());
            rechargeTransaction.setTransactionStatus(aggregatorResponseDo.getTransactionStatus());

            this.create(rechargeTransaction);
        } catch (Exception e) {
            logger.error("[IN MODERATOR] Error in Insertion of values in InTransaction Table (insertResponse) ", e);
        }
    }
    
    public void insertRechargeRetryData(RechargeRetryData rechargeRetryData) {
        Calendar calendar = Calendar.getInstance();
        try {         
        	rechargeRetryData.setCreatedTime(new Timestamp(calendar.getTimeInMillis()));
        	rechargeRetryData.setUpdatedTime(new Timestamp(calendar.getTimeInMillis()));
            this.create(rechargeRetryData);
        } catch (RuntimeException re) {
            logger.error("insertion in insertRechargeRetryData failed", re);
            throw re;
        }
    }

    @Transactional
    public InRequest getInRequest(Long requestId) {
        return inReadDAO.getInRequest(requestId);
    }

    @Transactional
    public InRequest getInRequest(String orderId) {
        return inReadDAO.getInRequest(orderId);
    }

    @Transactional
    public InResponse getInResponseByRequestId(Long requestId) {
        return inReadDAO.getInResponseByRequestId(requestId);
    }

    @Transactional
    public InResponse getInResponse(Long responseId) {
        return inReadDAO.getInResponse(responseId);
    }

    @Transactional
    public List<InTransactions> getInTransactionsByRequestId(Long requestId) {
        return inReadDAO.getInTransactionsByRequestId(requestId);
    }
    
    public List<InTransactions> getSortedInTransactionsByRequestId(Long requestId) {
        return inReadDAO.getSortedInTransactionsByRequestId(requestId);
    }

    @Transactional
    public InTransactions getLatestInTransactionsByRequestIdAndAggregaor(Long requestId, String aggrName) {
        return inReadDAO.getLatestInTransactionsByRequestIdAndAggregaor(requestId, aggrName);
    }

    @Transactional
    public InTransactions getInTransactions(Long transactionId) {
        return inReadDAO.getInTransactions(transactionId);
    }

    @Transactional
    public InTransactionData getInTransactionData(Long requestId) {
        return inReadDAO.getInTransactionData(requestId);
    }
    
    @Transactional
    public List<RechargeAttempt> getAllRechargesForEmail(String email) {
        return inReadDAO.getAllRechargesForEmail(email);
    }
    
    @Transactional
    public List<RechargeAttempt> getAllRechargesForEmailFromDate(String email, Timestamp fromDate, Timestamp toDate) {
        return inReadDAO.getAllRechargesForEmailFromDate(email, fromDate, toDate);
    }
    
    @Transactional
    public List<InRequest> getAllInRequests(String orderId) {
        return inReadDAO.getAllInRequests(orderId);
    }
    
    @Transactional
    public List<InTransactionData> getInTransactionList(String orderId) {
        List<InTransactionData> returnList = new ArrayList<>();

        List<InRequest> requestList = this.getAllInRequests(orderId);
        
        if (requestList != null) {
            for (InRequest inRequest : requestList) {
                
                if (inRequest != null) {
                    InTransactionData data = new InTransactionData();

                    data.setInRequest(inRequest);
                    data.setInResponse(getInResponseByRequestId(data.getInRequest().getRequestId()));
                    data.setTransactions(getInTransactionsByRequestId(data.getInRequest().getRequestId()));
                    
                    returnList.add(data);
                }
            }
        }
        
        return returnList;
    }

    @Transactional
    public InTransactionData getInTransactionData(String affiliateTransId) {
        return inReadDAO.getInTransactionData(affiliateTransId);
    }
    @Transactional
    public List<InTransactions> getInTransactionsListByOrder(String orderId) {
        List<InTransactions> responseList = new ArrayList<>();
        List<InRequest> requestList = this.getAllInRequests(orderId);
        if (requestList != null) {
            for (InRequest inRequest : requestList) {

                if (inRequest != null) {
                    List<InTransactions> inTransactionsList = this.getSortedInTransactionsByRequestId(inRequest.
                            getRequestId());
                    Collections.reverse(inTransactionsList);
                    for(InTransactions inTransactions : inTransactionsList) {
                        if(null != inTransactions) {
                            responseList.add(inTransactions);
                        }
                    }
                }
            }
        }
        return responseList;
    }
    @Transactional
    public InTransactionData getLatestInTransactionData(String orderId) {
        InTransactionData data = new InTransactionData();

        InRequest latestRequest = null;
        
        List<InRequest> allInRequests = getAllInRequests(orderId);
        if (allInRequests != null) {
            for (InRequest inRequest : allInRequests) {
                if (latestRequest == null) {
                    latestRequest = inRequest;
                } else if (inRequest.getRequestId() > latestRequest.getRequestId()) {
                    latestRequest = inRequest;
                }
            }
        }
        
        if (latestRequest ==  null) {
            return null;
        }
        
        data.setInRequest(latestRequest);
        data.setInResponse(getInResponseByRequestId(data.getInRequest().getRequestId()));
        data.setTransactions(getInTransactionsByRequestId(data.getInRequest().getRequestId()));
        
        return data;
    }

    public InRequest getLatestInRequestId(String orderId) {
    	 InRequest latestRequest = null;
    	 List<InRequest> allInRequests = this.getAllInRequests(orderId);
    	 if (allInRequests != null) {
             for (InRequest inRequest : allInRequests) {
                 if (latestRequest == null) {
                     latestRequest = inRequest;
                 } else if (inRequest.getRequestId() > latestRequest.getRequestId()) {
                     latestRequest = inRequest;
                 }
             }
         }
    	return latestRequest;
    }

    @Transactional
    public List<InErrorcodeMap> getErrorCodeMap(String aggrRespCode, String aggrName) {
        try {
            List<InErrorcodeMap> inErrorcodeMaps = inCachedService.getInErrorcodeMap(aggrRespCode, aggrName);
            if (inErrorcodeMaps != null && inErrorcodeMaps.size() > 0) {
                return inErrorcodeMaps;
            } else {
                inCachedService.setInErrorcodeMap(inReadDAO.getErrorCodeMap());
            }
        } catch (Exception e) {
        }
        return inReadDAO.getErrorCodeMap(aggrRespCode, aggrName);
    }
    
    /*below method is for getting data from either from cache or in_recharge_retry table*/
    
    @Transactional
    public List<InRechargeRetryMap> getOperatorRetryConfig(String aggrRespCode, String aggrName, String operatorId) {
        boolean isOperatorRetryForAllError = appConfigService.isOperatorRetryAllErrorCodeEnabled();
        try {
            List<InRechargeRetryMap> inRechargeRetryMaps = inCachedService.getOperatorRetryConfig(aggrRespCode, aggrName, operatorId, isOperatorRetryForAllError);
            if (inRechargeRetryMaps != null && inRechargeRetryMaps.size() > 0) {
                return inRechargeRetryMaps;
            } else {
            	inCachedService.setOperatorRetryConfig(inReadDAO.getAllOperatorRetryConfig());
            }
        } catch (Exception e) {
        }
        return inReadDAO.getOperatorRetryConfig(aggrRespCode, aggrName, operatorId, isOperatorRetryForAllError);
    }
    
    public List<InRechargeRetryMap> getAllOperatorRetryConfig() {
        List<InRechargeRetryMap> listFromCache = inCachedService.getAllOperatorRetryConfig();
        if (FCUtil.isEmpty(listFromCache)) {
            inCachedService.setOperatorRetryConfig(inReadDAO.getAllOperatorRetryConfig());
        } else {
            return listFromCache;
        }
        return inCachedService.getAllOperatorRetryConfig();
    }
    
    public InRechargeRetryMap getOperatorRetryConfigFromId(Integer id) {
        List<InRechargeRetryMap> listFromCache = inCachedService.getAllOperatorRetryConfig();
        if (FCUtil.isEmpty(listFromCache)) {
            listFromCache = inReadDAO.getAllOperatorRetryConfig();
            inCachedService.setOperatorRetryConfig(listFromCache);
        }
        for (InRechargeRetryMap inRechargeRetryMap : listFromCache) {
            if (inRechargeRetryMap.getId().equals(id)) {
                return inRechargeRetryMap;
            }
        }
        return null;
    }
    
    public void deleteOperatorRetryConfig(Integer id) {
        inWriteDAO.deleteOperatorRetryConfig(id);
    }
    
    @Transactional
    public List<InResponse> getInResponsesWithTimeRange(Date fromDate, Date toDate) {
        List<InResponse> responseList = inRangeReadDAO.getInResponsesWithTimeRange(fromDate, toDate);

        if (responseList == null) {
            return new ArrayList<>();
        }

        return responseList;
    }

    @Transactional
    public List<String> getPendingRechargesInTimeRange(Date from, Date to) {
        List<String> responseList = inRangeReadDAO.getPendingRechargesInTimeRange(from, to);

        if (responseList == null) {
            return new ArrayList<>();
        }

        return responseList;
    }

    @Transactional
    public List<InResponse> getInResponsesWithTimeRange(Long requestID) {
        return inReadDAO.getInResponsesByRequestId(requestID);
    }

    @Transactional
    public InResponse insertResponse(RechargeDo rechargeDo, String priority) {
        InResponse inResponse = new InResponse();
        try {

            Calendar cal = Calendar.getInstance();
            Date startDate = cal.getTime();

            inResponse.setAggrName(priority);
            inResponse.setAggrRespCode("");
            inResponse.setAggrRespMsg("");
            inResponse.setAggrTransId("");
            inResponse.setRequestId(rechargeDo.getInRequestId());
            inResponse.setRawRequest("");
            inResponse.setRawResponse("");
            inResponse.setOprTransId("");
            inResponse.setRetryNumber(0);
            inResponse.setCreatedTime(new Timestamp(startDate.getTime()));
            inResponse.setUpdatedTime(new Timestamp(startDate.getTime()));
            inResponse.setInRespCode("-1");
            inResponse.setInRespMsg("Pending");

            this.create(inResponse);
        } catch (Exception e) {
            logger.error("[IN MODERATOR] Error in inserting values in DB (insertResponse) ", e);
            throw e;
        }
        return inResponse;
    }

    @Transactional
    public InResponse getResponseStatus(Long requestID) {
        return inReadDAO.getInResponseStatus(requestID);
    }
    
    /**
     * Returns true if this is a repeat recharge and it is
     * safe to retry. This function is no-op (i.e., returns true)
     * in case there hasn't been any previous recharge attempt.
     * @param orderId
     * @return true if this is a first attempt OR none of the previous recharge attempts have succeeded
     */
    @Transactional
    public boolean validateForRechargeRetry(String orderId) {
        List<InTransactionData> transactionList = this.getInTransactionList(orderId);
        
        if (transactionList == null) {
            /*
             * First attempt so go ahead.
             */
            return true;
        }
        
        for (InTransactionData inTransactionData : transactionList) {
            logger.info("Response Code" + inTransactionData.getInResponse().getInRespCode());
            if (!this.isFailure(inTransactionData.getInResponse().getInRespCode())) {
                /*
                 * If this isn't a failed recharge then
                 * it's either "success or pending or 
                 * under process" and hence is not safe
                 * to retry 
                 */
                return false;
            }
        }
        
        return true;
    }

    public List<InRequest> getInRequests(Date from, Date to) {
        return inRangeReadDAO.getInRequests(from, to);
    }
    
    public List<String> getFailedInRequests(Date from, Date to) {
        return inRangeReadDAO.getFailedRechargesInTimeRange(from, to);
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContextDev.xml",
                "web-delegates.xml");
        InService inService = ((InService) ctx.getBean("inService"));

        Calendar start = Calendar.getInstance();
        start.set(Calendar.YEAR, 2012);
        start.set(Calendar.MONTH, Calendar.SEPTEMBER);
        start.set(Calendar.DAY_OF_MONTH, 15);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);

        Calendar end = Calendar.getInstance();
        end.set(Calendar.YEAR, 2012);
        end.set(Calendar.MONTH, Calendar.SEPTEMBER);
        end.set(Calendar.HOUR_OF_DAY, 4);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);

        List<Integer> requestIds = Arrays.asList(new Integer[] { 1286, 2458, 2569, 1436, 1586, 2795, 2816, 1811 });

        for (Integer requestId : requestIds) {
            String respCode = inService.getInResponseByRequestId((long) requestId).getInRespCode();

            System.out.println(respCode);
        }

    }

    public INRESPONSE getStatusAtIn(InResponse inResponse, UserTransactionHistory userTransactionHistory) throws IOException {
        if (inResponse == null || StringUtils.isEmpty(inResponse.getInRespCode()) || userTransactionHistory == null
                || StringUtils.isEmpty(userTransactionHistory.getTransactionStatus())) {
            return INRESPONSE.UNKNOWN;
        }

        if (!AggregatorInterface.SUCCESS_OR_PENDING_RESPONSES.contains(inResponse.getInRespCode())
                && !AggregatorInterface.SUCCESS_OR_PENDING_RESPONSES.contains(userTransactionHistory
                        .getTransactionStatus())) {
            return INRESPONSE.FAILURE;
        } else {
            return INRESPONSE.UNKNOWN;
        }
    }
    
    public boolean isSuccessful(InResponse inResponse) throws IOException {
        if (inResponse != null) {
            if (isSuccessfulStatusCode(inResponse.getInRespCode())) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isSuccessfulStatusCode(String inResponse) {
        if (StringUtils.isNotBlank(inResponse) && AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(inResponse)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isFailure(String inResponseCode) {
        if (StringUtils.isNotBlank(inResponseCode)) {
            if (!AggregatorInterface.SUCCESS_OR_PENDING_RESPONSES.contains(inResponseCode)) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean isPending(String inResponseCode) {
        if (StringUtils.isNotBlank(inResponseCode)) {
            if (AggregatorInterface.PENDING_RESPONSES.contains(inResponseCode)) {
                return true;
            }
        }
        return false;
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(InService.class.getSimpleName());
    }

    public InResponse getInResponseByOrderID(String orderID) {
        return inReadDAO.getInResponseByOrderID(orderID);
    }
    
    public List<InResponse> getAllInResponseForOrderID(String orderID) {
        return inReadDAO.getAllInResponsesForOrderID(orderID);
    }

    public TransactionStatus getRechargeStatus(String orderId) {
        TransactionStatus transactionStatus = new TransactionStatus();
        InResponse inResponseObj = getInResponseByOrderID(orderId);
        if(inResponseObj==null){
        	logger.info("InResponse Object Not found for orderId:"+orderId);
        	return null;
        }
        InRequest inRequestObj= getInRequest(inResponseObj.getRequestId());
        logger.info("create transaction status for recharge for order id " + orderId);
        transactionStatus.setTransactionStatus(inResponseObj.getInRespCode());
        transactionStatus.setServiceProvider(inResponseObj.getAggrName());
        transactionStatus.setOperator(inRequestObj.getOperator());
        transactionStatus.setSubscriberIdentificationNumber(inRequestObj.getSubscriberNumber());
        transactionStatus.setPlanType(inRequestObj.getRechargePlan());
        return transactionStatus;
    }

    public void createInErrorcodeMapEntry(String aggrName, String aggrResponseCode, String aggrResponseMessage,
            String inRespCode) {
        InErrorcodeMap inErrorcodeMap = new InErrorcodeMap();
        inErrorcodeMap.setAggrName(aggrName);
        inErrorcodeMap.setAggrRespCode(aggrResponseCode);
        inErrorcodeMap.setInErrorMsg(aggrResponseMessage);
        inErrorcodeMap.setInRespCode(inRespCode);
        inErrorcodeMap.setIsDisplayEnabled(false);
        inErrorcodeMap.setIsRetryable(false);
        inWriteDAO.create(inErrorcodeMap);
    }
    
    public StringTokenizer fetchAggregatorRechargePriority(String operatorName, String circleName) {
           InOprAggrPriority priority = operatorCircleService.getInOprAggrPriority
                         (operatorCircleService.getCircleOperatorMapping(operatorName, circleName).getCircleOperatorMappingId());
           String aggrpriorityvalue = priority.getAggrPriority();
           StringTokenizer aggrprioritytokens = new StringTokenizer(aggrpriorityvalue, ",");
           return aggrprioritytokens;
    }
    
    @Transactional
    public CircleOperatorMapping getCircleOperatorMapping(String orderId) {
        Map<String, Object> map = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
        
        String operatorName = (String) map.get("operatorName");
        String circleName = (String) map.get("circleName");
        String mobileNo = (String) map.get("mobileno");
        
        if ( StringUtils.isBlank(circleName) || ( circleName.equalsIgnoreCase("ALL") &&
                !( (RechargeConstants.DTH_PRODUCT_TYPE).equals( (String)map.get("productType")) ))) {
            logger.info("Circle field is ALL for gievn order id:[ " + orderId + "]");
            circleName = operatorCircleService.getCircle(mobileNo, circleName);
        }
        
        if (StringUtils.isBlank(operatorName)) {

            logger.info("operatorName field is Blank for gievn order id:[ " + orderId + "]");
            Map<String, Object> operator = operatorCircleService.findOperatorByMobilePrefix(mobileNo);

            if (operator != null) {
                Integer prepaidMobileOperatorId = (Integer) operator.get("operatorMasterId");
                if (prepaidMobileOperatorId != null) {
                    OperatorMaster operatorMaster = operatorCircleService.getOperator(prepaidMobileOperatorId);
                    if (operatorMaster!=null) {
                        operatorName = operatorMaster.getOperatorCode();
                    }
                }
            }
        }
        
        CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(operatorName, circleName);

        return circleOperatorMapping;
    }
    
    @Transactional
    public InRechargeRetryMap insertInRechargeRetry(InRechargeRetryMap inRechargeRetryMap) {
        return inWriteDAO.insertInRechargeRetry(inRechargeRetryMap);
    }

    public void updateInRechargeRetry(InRechargeRetryMap inRechargeRetryMap) {
        inWriteDAO.updateInRechargeRetry(inRechargeRetryMap);
    }
    
    public List<InRechargeRetryMap> getInRechargeRetryDetails() {
        return inReadDAO.getAllOperatorRetryConfig();
    }
    
    public List<RechargeRetryData> getRechargeRetryDataForOrder(String orderId) {
        return inReadDAO.getRechargeRetryDataForOrder(orderId);
    }
    
    public Object fetchAgPreferenceByOperatorId(Integer operatorId) {
           return inReadDAO.fetchAgPreferenceByOperatorId(operatorId);
    }
    
    public List<Map<String,Object>>  fetchAgPreferenceObj() {
          return inReadDAO.fetchAgPreferenceObj();
    }
    
    public Integer  updateAgPreference(Map<String, Object> map) {
           return inWriteDAO.updateAgPreference(map);
     }
	
	public Boolean updateStatusChange(InResponse inResponse) {
	    inResponse.setUpdatedTime(new Timestamp(new Date().getTime()));
		return inWriteDAO.updateStatusChange(inResponse) == 1 ? true : false;
	}
	
	public Boolean updateStartupImpactedTransactions(InResponse inResponse) {
	    inResponse.setUpdatedTime(new Timestamp(new Date().getTime()));
		return inWriteDAO.updateStartupImpactedTransactions(inResponse) == 1 ? true : false;
	}

    
    public FulfilmentStatus getFulfilmentStatus(String respCode) {
        if (isSuccessfulStatusCode(respCode)) {
            return FulfilmentStatus.SUCCESS;
        } else if (isFailure(respCode) || RechargeConstants.INITIAL_RESPONSE_STATUS.equals(respCode)) {
            return FulfilmentStatus.FAILED;
        } else if (isPending(respCode)) {
            return FulfilmentStatus.PENDING;
        }
        return null;
    }

	public OperatorMaster getOperatorFromOrderId(String orderId) {
		Map<String, Object> map = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);

		String operatorName = (String) map.get("operatorName");

		OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorName);

		if (operatorMaster == null) {
			logger.info("OperatorMaster Not found for operator:" + operatorName);
			return null;
		}
		return operatorMaster;
	}
}
