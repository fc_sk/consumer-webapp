package com.freecharge.app.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.CityMasterDAO;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 9/19/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class LocationService {
    @Autowired
    private InCachedService inCachedService;

    @Autowired
    private CityMasterDAO cityMasterDAO;

    public Map<Integer, CityMaster> getAllCities(){
        Map<Integer, CityMaster> allCities = null;
        try {
            allCities = this.inCachedService.getAllCities();
        }catch (Exception e){
            logger.error("error getting all cities from cache", e);
        }
        if(allCities==null || allCities.size()<1){
            allCities = this.cityMasterDAO.getAllCitiesMap();
            try{
                this.inCachedService.setAllCities(allCities);
            }catch (Exception e){
                logger.error("error setting all cities to cache");
            }
        }
        return allCities;
    }

    public CityMaster getCityWithId(Integer cityId) {
        Map<Integer, CityMaster> allCities = this.getAllCities();
        if (allCities!=null){
            return allCities.get(cityId);
        }
        return null;
    }

    private static  final Logger logger = LoggingFactory.getLogger(LocationService.class);
}
