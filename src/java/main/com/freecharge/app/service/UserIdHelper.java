package com.freecharge.app.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.growthevent.service.UserServiceProxy;

public class UserIdHelper {
	
	@Autowired
    private UserServiceProxy userServiceProxy;
	
	public Integer getUserIdFromEmail(String emailId) {
		return (int) userServiceProxy.getUserIdByEmailId(emailId);
	}
}
