package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.web.util.WebConstants;

@Service
public class RechargeDetailsService {
	
    /*
     * Android App Version until which rechargeDetails call should not return
     * Add Cash transactions
     */
	private static final Integer ANDROID_VERSION_NO_CASH_TXNS = 34;

	/*
     * IOS App Version until which rechargeDetails call should not return
     * Add Cash transactions
     */
    private static final Integer IOS_VERSION_NO_CASH_TXNS = 4;
    
    /*
     * Android App version which supports Hcoupons
     */
    private static final Integer ANDROID_VERSION_HCOUPON_SUPPORT = 49;
    
	@Autowired
	UserTransactionHistoryService userTransactionHistory;

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private OrderIdSlaveDAO orderReadDao;

	@Autowired
	private OrderIdWriteDAO orderIdDAO;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private WalletService walletService;
	
	@Autowired
	private AppConfigService appConfigSerice;

	@Autowired
	private UserTransactionHistoryDAO userTransactionHistoryDao;

	@Autowired
	private InReadDAO inReadDao;

	@Autowired
	private UserTransactionHistoryService userTransactionHistoryService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
    private UserServiceProxy userServiceProxy;
	
	private static final List<String> excludedTxnStatus = Arrays.asList(
			PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE,
			PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED,
			PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK,
			PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED,
			PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS);

	public List<RechargeDetails> getAllUserRecharges(String userEmailId, HttpServletRequest request) {

		Users users = userServiceProxy.getUserByEmailId(userEmailId);
		List<UserTransactionHistory> txnHistoryList = userTransactionHistoryService
				.findUserTransactionHistoryFromUserId(users.getUserId(), WebConstants.RECHARGE_HISTORY_LIMIT);
		return getRechargeDetailsFromTxnHisList(txnHistoryList, request, true);

	}

	private List<RechargeDetails> getRechargeDetailsFromTxnHisList(List<UserTransactionHistory> txnHistoryList,
			HttpServletRequest request, boolean returnPaymentTxns) {
		  List<RechargeDetails> userRechargeDetailsList = new ArrayList<RechargeDetails>();
			// store the recharge details for all the successful orderIds
	        if (txnHistoryList == null) {
	            return userRechargeDetailsList;
	        }
	        JSONObject json = appConfigSerice.getCustomProp(AppConfigService.MERCHANT_ID_MAPPING);
			for (UserTransactionHistory uth : txnHistoryList) {
				String orderId = uth.getOrderId();
				RechargeDetails userRechargeDetails = userTransactionHistoryService.constructRechargeDetails(uth);
						
				if ( userRechargeDetails != null ) {
					
					
					/*
					 * Handle HCoupons
					 */
					 ProductName productName = ProductName.fromCode(FCConstants.productNameToTypeMap.get(userRechargeDetails.getProductName()));
					 String txnType = FCConstants.productNameToTxnTypeMap.get(userRechargeDetails.getProductName());
					 userRechargeDetails.setTxnType(txnType);
					 if (productName == ProductName.HCoupons) {
						 if (!isHCouponsSupported(request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY), MobileUtil.getMobileAppVersionFromRequest(request))) {
							 continue;
						 }
					 }
				
				    if(productName == ProductName.WalletCash && request != null && !returnAddCashTxnsForMobileApp(request.getParameter(FCConstants.PAYMENT_REQUEST_CHANNEL_KEY), MobileUtil.getMobileAppVersionFromRequest(request))){
				        continue;
				    }
				    
					String rechargeType = "topup";
					userRechargeDetails.setRechargeType(rechargeType);
					String status = null;
					if (productName == ProductName.WalletCash) {
						userRechargeDetails.setServiceNumber(null);
						Integer successCount = this.successfulTransaction(orderId);
						if (successCount > 0) {
							status = "AddCashSuccess";
						} else {
							status = "AddCashFailed";
						}
					} else {
						status = getTransactionStatus(userRechargeDetails.getTransactionStatusCode(), userRechargeDetails.getRechargedOn().getTime(), returnPaymentTxns);
						if(status == null){
							continue;
						}
						if(status.equals("PaymentFailure")){
						    userRechargeDetails.setTransactionStatusCode("05");
						}
						if (status.equalsIgnoreCase("Failure")) {
							logger.info("Fetching refund transactions for Order : "
									+ orderId);
							List<PaymentRefundTransaction> refundsList = paymentTransactionService
	                                .getPaymentRefundTransactions(orderId);
	                        logger.info("Refund transactions for Order : " + orderId + " : " + refundsList);
							setTransactionRefundOption(orderId, userRechargeDetails, refundsList);
							if (refundsList != null && !refundsList.isEmpty()) {
								PaymentRefundTransaction.Status refundStatus = null;
								for(PaymentRefundTransaction refundTransaction : refundsList){
								    if(!StringUtils.isEmpty(refundTransaction.getRefundTo()) &&
	                                        !refundTransaction.getRefundTo().equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE) &&
	                                        !refundTransaction.getRefundTo().equalsIgnoreCase(PaymentConstants.ONE_CHECK_WALLET_CODE)){
								        refundStatus = refundTransaction.getStatus();
								    }
								}
	                            if (refundStatus != null) {
	                                logger.info("Refund status for Order " + orderId + " : " + refundStatus.toString());
	                                if (refundStatus.equals(PaymentRefundTransaction.Status.SUCCESS) || refundStatus.equals(PaymentRefundTransaction.Status.INITIATED)) {
	                                    userRechargeDetails.setRefundStatus("Refunded");
	                                } else if (refundStatus.equals(PaymentRefundTransaction.Status.FAILURE)) {
	                                    userRechargeDetails.setRefundStatus("Refund Failed");
	                                }
	                            }
							}
						}
					}
					
					// Add invoice flag
					Calendar gstStart = Calendar.getInstance();
					gstStart.set(2017, 6, 1, 0, 0);
					Calendar convenienceFeeDisableDate = Calendar.getInstance();
					convenienceFeeDisableDate.set(2017, 7, 1, 12, 56);
					if( userRechargeDetails.getRechargedOn().after(gstStart.getTime()) ) {
						if( productName == ProductName.Mobile || productName == ProductName.DTH ) {
							userRechargeDetails.setInvoice(true);
						}
						if( productName.isUtilityPaymentProduct() && userRechargeDetails.getRechargedOn().before(convenienceFeeDisableDate.getTime()) ) {
							if( json.containsKey(userRechargeDetails.getOperatorMasterId()) ) {
								userRechargeDetails.setInvoice(true);
							}
						}
					}
					
					userRechargeDetails.setRechargeStatus(status);
					
					userRechargeDetailsList.add(userRechargeDetails);
				} else {
				    logger.info("No data found for order : " + orderId);
				}
			}
			return userRechargeDetailsList;
	}

	public List<RechargeDetails> getSuccessfulUserRecharges(String userEmailId, HttpServletRequest request) {
		Long userId = userServiceProxy.getUserIdByEmailId(userEmailId);
		List<UserTransactionHistory> txnHistoryList = userTransactionHistoryService
				.findSuccessfulUTHByUserId(userId, WebConstants.RECHARGE_HISTORY_LIMIT);		
		return getRechargeDetailsFromTxnHisList(txnHistoryList, request, false);
	
	}

	public List<RechargeDetails> getSuccessfulUserRechargesByUserEmail(String userEmail, String productType,
			HttpServletRequest request) {
		// get all the successful orderId's from database
		List<UserTransactionHistory> txnHistoryList = userTransactionHistory.findSuccessfulUTHByUserEmailAndProductType(
				userEmail, productType, WebConstants.RECHARGE_HISTORY_LIMIT);
		return getRechargeDetailsFromTxnHisList(txnHistoryList, request, false);

	}
	

    private boolean isHCouponsSupported(String channelId,
			Integer mobileAppVersionFromRequest) {
    	if (channelId != null
                && !channelId.isEmpty()
                && ((Integer.parseInt(channelId) == FCConstants.CHANNEL_ID_ANDROID_APP && mobileAppVersionFromRequest < ANDROID_VERSION_HCOUPON_SUPPORT)
                || (Integer.parseInt(channelId) == FCConstants.CHANNEL_ID_WINDOWS_APP) || (Integer.parseInt(channelId) == FCConstants.CHANNEL_ID_IOS_APP))) {
            return false;
        }
        return true;
	}

	/**
     * Failed transaction is refundable only if the entire transaction amount is
     * paid through payment gateway There is no refund option for partial
     * payments
     * 
     * @param orderId
     * @param userRechargeDetails
     */
    private void setTransactionRefundOption(String orderId, RechargeDetails userRechargeDetails, List<PaymentRefundTransaction> refundsList) {
        userRechargeDetails.setRefundable(false);
        if (refundsList != null && !refundsList.isEmpty()) {
            for (PaymentRefundTransaction refundTransaction : refundsList) {
                if (refundTransaction.getRefundTo() == null ||
                        (!refundTransaction.getRefundTo().equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE) &&
                                !refundTransaction.getRefundTo().equalsIgnoreCase(PaymentConstants.ONE_CHECK_WALLET_CODE)
                                && !refundTransaction.getStatus().equals(PaymentRefundTransaction.Status.FAILURE))) {
                    return;
                }
            }
        }
        List<PaymentTransaction> paymentDetails = paymentTransactionService.getPaymentTxnDetails(orderId);
        Double pgAmount = 0.0;
        Double transactionAmount = 0.0;
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
		for (CartItemsBusinessDO cartItemsBusinessDO : cartBusinessDo
				.getCartItems()) {
			if (cartItemsBusinessDO.getEntityId() != null
					&& cartItemsBusinessDO.getEntityId().equals("FreeFund")) {
				continue;
			}
			transactionAmount += (cartItemsBusinessDO.getPrice() == null ? 0.0
					: cartItemsBusinessDO.getPrice());

		}
        
        for (PaymentTransaction paymentTransaction : paymentDetails) {
            if (paymentTransaction.getIsSuccessful()) {
                if (!paymentTransaction.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)
                        && !paymentTransaction.getPaymentGateway().equalsIgnoreCase(
                                PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {
                    pgAmount += paymentTransaction.getAmount();
                }
            }
        }
        logger.error("[" + orderId + "] Txn Amount / PG Amount : " + transactionAmount + " / " + pgAmount);
        if (transactionAmount.equals(pgAmount)) {
            userRechargeDetails.setRefundable(true);
            userRechargeDetails.setRefundAmount(pgAmount);
        }
    }

	private boolean returnAddCashTxnsForMobileApp(String channel, Integer mobileAppVersionFromRequest) {
        if (channel != null
                && !channel.isEmpty()
                && ((Integer.parseInt(channel) == FCConstants.CHANNEL_ID_ANDROID_APP && mobileAppVersionFromRequest <= ANDROID_VERSION_NO_CASH_TXNS) || 
                    (Integer.parseInt(channel) == FCConstants.CHANNEL_ID_IOS_APP) && mobileAppVersionFromRequest <= IOS_VERSION_NO_CASH_TXNS)) {
            return false;
        }
        return true;
    }

	private String getTransactionStatusForOrder(String orderId, boolean returnPaymentTxns) {
		String orderRechargeStatus = null;
		String status = getStatusFromTransactionHistory(orderId);
		if (status == null) {
			status = getStatusFromUserRechargeHistory(orderId);
		}
		if(!returnPaymentTxns && excludedTxnStatus.contains(status)){
			return orderRechargeStatus;
		}
		if (status != null
				&& (status
						.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED) || status
						.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK) ||
						status.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED))) {
			orderRechargeStatus = "PaymentInitiated";
			UserTransactionHistory uth = userTransactionHistoryDao
					.findUserTransactionHistoryByOrderId(orderId);
			if (uth != null && (System.currentTimeMillis() - uth.getCreatedOn().getTime() > (20 * 60 * 1000))) {
				orderRechargeStatus = "PaymentFailure";
			}
		} else if (status != null
				&& status
						.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS)) {
			orderRechargeStatus = "PaymentSuccess";
		} else if (status != null
				&& status
						.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE)) {
			orderRechargeStatus = "PaymentFailure";
		} else if (status != null
				&& status
						.equalsIgnoreCase(RechargeConstants.SUCCESS_RESPONSE_CODE)) {
			orderRechargeStatus = "Success";
		} else if (status != null
				&& (status
						.equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE) || status
						.equalsIgnoreCase("RR"))) {
			orderRechargeStatus = "UnderProcess";
		} else {
			orderRechargeStatus = "Failure";
		}
		return orderRechargeStatus;
	}
	
	private String getTransactionStatus(String status, long createdOnTime, boolean returnPaymentTxns) {
        String orderRechargeStatus = null;
        if(!returnPaymentTxns && excludedTxnStatus.contains(status)){
            return orderRechargeStatus;
        }
        if (status != null
                && (status
                        .equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED) || status
                        .equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK) ||
                        status.equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED))) {
            orderRechargeStatus = "PaymentInitiated";
            if ((System.currentTimeMillis() - createdOnTime) > (20 * 60 * 1000)) {
                orderRechargeStatus = "PaymentFailure";
            }
        } else if (status != null
                && status
                        .equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS)) {
            orderRechargeStatus = "PaymentSuccess";
        } else if (status != null
                && status
                        .equalsIgnoreCase(PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE)) {
            orderRechargeStatus = "PaymentFailure";
        } else if (status != null
                && status
                        .equalsIgnoreCase(RechargeConstants.SUCCESS_RESPONSE_CODE)) {
            orderRechargeStatus = "Success";
        } else if (status != null
                && (status
                        .equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE) || status
                        .equalsIgnoreCase("RR"))) {
            orderRechargeStatus = "UnderProcess";
        } else if (status != null) {
            orderRechargeStatus = "Failure";
        }
        return orderRechargeStatus;
    }

	private String getStatusFromUserRechargeHistory(String orderId) {
		String status = null;
		InResponse response = inReadDao.getInResponseByOrderID(orderId);
		if (response != null) {
			status = response.getInRespCode();
		}
		return status;
	}

	private String getStatusFromTransactionHistory(String orderId) {
		String status = null;
		UserTransactionHistory uth = userTransactionHistoryDao
				.findUserTransactionHistoryByOrderId(orderId);
		if (uth != null) {
			status = uth.getTransactionStatus();
		}
		return status;
	}

	@Transactional(readOnly = true)
	private Integer successfulTransaction(String orderId) {
		return paymentTransactionService.getSuccessFullTxnForOrderId(orderId);
	}
}
