package com.freecharge.app.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dao.AppVersionOrderDAO;
import com.freecharge.payment.dos.entity.AppVersionOrder;

@Service
@EnableAsync
public class AppVersionInsertDelegate {
    private Logger             logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppVersionOrderDAO appVersionOrderDAO;

    @Async
    public void insertAppVersionForOrder(AppVersionOrder appVersionOrder) {
        logger.debug("Starting app_version insert asyncronously. " + appVersionOrder);
        appVersionOrderDAO.insertAppVersionOrder(appVersionOrder);
        logger.debug("Completed app_version insert asyncronously.");
    }
}
