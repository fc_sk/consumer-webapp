package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.AggrPriorityTemplateRepository;

@Service
public class AggrPriorityTemplateService {

    private static final Logger            logger = LoggingFactory.getLogger(AggrPriorityTemplateService.class);

    @Autowired
    private AggrPriorityTemplateRepository aggrPriorityTemplateRepository;

    public List<String> getAllTemplates() {
        return aggrPriorityTemplateRepository.findAllTemplates();
    }

    public void saveTemplate(String templateName, Map<Integer, CircleOperatorMapping> circleOperatorMappings) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(AggrPriorityTemplateRepository.TEMPLATE, templateName);
        data.put(AggrPriorityTemplateRepository.CREATEDAT, new Date());
        data.put(AggrPriorityTemplateRepository.MAPPINGS, getMappingsList(circleOperatorMappings));
        aggrPriorityTemplateRepository.updateOrCreateEntry(templateName, data);
    }
    
    public void deleteTemplate(String templateName) {
        aggrPriorityTemplateRepository.deleteTemplate(templateName);
    }

    public List<CircleOperatorMapping> getTemplate(String templateName) {
        Map<String, Object> dbObject = aggrPriorityTemplateRepository.getDataByTempalte(templateName);
        if (dbObject == null)
            return null;
        List<Map<String, Object>> mappings = (List<Map<String, Object>>) dbObject
                .get(AggrPriorityTemplateRepository.MAPPINGS);
        List<CircleOperatorMapping> circleOperatorMappings = getCircleOperatorMappings(mappings);
        return circleOperatorMappings;
    }

    private List<CircleOperatorMapping> getCircleOperatorMappings(List<Map<String, Object>> mappings) {
        List<CircleOperatorMapping> circleOperatorMappings = new ArrayList<CircleOperatorMapping>();
        for (Map<String, Object> objMap : mappings) {
            circleOperatorMappings.add(getObjFromMap(objMap));
        }
        return circleOperatorMappings;
    }

    private CircleOperatorMapping getObjFromMap(Map<String, Object> objMap) {
        CircleOperatorMapping circleOperatorMapping = new CircleOperatorMapping();
        circleOperatorMapping.setCircleOperatorMappingId((Integer) objMap.get(AggrPriorityTemplateRepository.COMID));
        circleOperatorMapping.setFkOperatorMasterId((Integer) objMap.get(AggrPriorityTemplateRepository.OPERATOR));
        circleOperatorMapping.setFkCircleMasterId((Integer) objMap.get(AggrPriorityTemplateRepository.CIRCLE));
        circleOperatorMapping.setAggrPriority((String) objMap.get(AggrPriorityTemplateRepository.PRIORITY));
        return circleOperatorMapping;
    }

    private List<Map<String, Object>> getMappingsList(Map<Integer, CircleOperatorMapping> circleOperatorMappings) {

        List<Map<String, Object>> mappings = new ArrayList<Map<String, Object>>();
        for (Integer id : circleOperatorMappings.keySet()) {
            mappings.add(createMapFromObject(circleOperatorMappings.get(id)));
        }

        return mappings;
    }

    private Map<String, Object> createMapFromObject(CircleOperatorMapping circleOperatorMapping) {
        Map<String, Object> objMap = new HashMap<String, Object>();
        objMap.put(AggrPriorityTemplateRepository.COMID, circleOperatorMapping.getCircleOperatorMappingId());
        objMap.put(AggrPriorityTemplateRepository.OPERATOR, circleOperatorMapping.getFkOperatorMasterId());
        objMap.put(AggrPriorityTemplateRepository.CIRCLE, circleOperatorMapping.getFkCircleMasterId());
        objMap.put(AggrPriorityTemplateRepository.PRIORITY, circleOperatorMapping.getAggrPriority());
        return objMap;
    }
}
