package com.freecharge.app.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import com.freecharge.app.domain.dao.RewardRechargeDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.RewardRecharge;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.businessdo.TxnCrossSellBusinessDo;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.FulfillmentServiceThread;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.service.helper.SessionHelper;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.autorefund.PaymentRefund.RefundAction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.services.IreffRechargeDataImportService;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.businessDo.RechargeResponseWebDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.exception.RechargeEnqueFailedException;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.services.TransactionDelegate;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.recharge.LogRequestService;
import com.freecharge.tracker.Tracker;
import com.freecharge.web.controller.HomeController;
import com.freecharge.web.mapper.UIResponseMapper;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.ProductPaymentWebDO;
import com.freecharge.web.webdo.RechargeInitiateWebDo;
import com.freecharge.app.domain.dao.jdbc.ReconMappingDao;
import com.freecharge.app.domain.entity.jdbc.ReconMapping;

@Service
public class RechargeInitiateService {

    private static Logger logger = LoggingFactory.getLogger(RechargeInitiateService.class);

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private FulfillmentService fulfillmentService;

    @Autowired
    private TxnFulFilmentService txnFulFilmentService;

    @Autowired
    private TransactionDelegate transactionDelegate;

    @Autowired
    private InReadDAO inReadDAO;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private KestrelWrapper kestrelWrapper;

    @Autowired
    private AppConfigService appConfig;
    
    @Autowired
    private ReconMappingDao reconMappingDao;
    
    @Autowired
    private BillWriteDAO billWriteDAO;
    
    @Autowired
    private FulfillmentScheduleService fulfillmentScheduleService;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO; 
        
    @Autowired
    private UserTransactionHistoryService historyService;
    
    @Autowired
    private Tracker                   tracker;

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    private MobileCheckoutService mCheckoutService;

    @Autowired
    private SessionHelper sessionHelper;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    UserServiceProxy userServiceProxy;
    
    @Autowired
    private RewardRechargeDAO rewardRechargeDAO;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private PaymentRefund paymentRefund;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    private BillPaymentService billPaymentService;
    
    @Autowired
    private AutoPayAppService autoPayAppService;
    
    @Autowired
    private RechargeRetryConfigReadThroughCache rechargeRetryConfigCache;
    
    @Autowired
    private LogRequestService logRequestService;
    
    public static enum ProductType{RechargeType,PostPaidType,UtilityType}
    
    public static final List<String> circleTypeALLProductsList =  new ArrayList<String>(Arrays.asList(RechargeConstants.DTH_PRODUCT_TYPE, RechargeConstants.GOOGLE_CREDITS_PRODUCT_TYPE));
    
    public static String getAddress(Map<String, Object> getUserDetailsFromOrderIdMap) {
        List<String> addressParts = new ArrayList<String>(3);

        String deliveryAdd = (String) getUserDetailsFromOrderIdMap.get("deliveryAdd");
        if (deliveryAdd != null && StringUtils.isNotBlank(deliveryAdd)) {
            addressParts.add(deliveryAdd);
        }

        String deliveryArea = (String) getUserDetailsFromOrderIdMap.get("deliveryArea");
        if (deliveryArea != null && !deliveryArea.trim().equalsIgnoreCase("null") && StringUtils.isNotBlank(deliveryArea)) {
            addressParts.add(deliveryArea);
        }
        String deliveryLandmark = (String) getUserDetailsFromOrderIdMap.get("deliveryLandmark");
        if (deliveryLandmark != null && StringUtils.isNotBlank(deliveryLandmark)) {
            addressParts.add(deliveryLandmark);
        }

        return StringUtils.join(addressParts, ", ");
    }
    
    public Map<String, String> getOrderDetailByReconId(String reconId) {
    	ReconMapping reconMapping = reconMappingDao.getReconMapping(reconId);

    	if(reconMapping != null && reconMapping.getOrderId() != null) {
    		Map<String, String> result = new HashMap();
    		result.put("OrderId", reconMapping.getOrderId());
    		return result;
    	}


    	return null;
    }

    public RechargeInitiateWebDo getRechargeInfo(String orderID) {
        RechargeInitiateWebDo riwd = new RechargeInitiateWebDo();
        InResponse inResponse=null;
        Map<String, Object> map = homeService.getUserDetailsFromOrderIdWithAddressIfAvailable(orderID);
        if (map == null || map.isEmpty()) {
            String errorString = "No recharge info found for orderID : [" + orderID + "]";
            
            logger.info(errorString);
            throw new IllegalStateException(errorString);
        }
        UserProfile userProfile = userServiceProxy.getUserProfile((String)map.get("email"));
        if(userProfile != null){
            map.put("firstname", userProfile.getFirstName());
            map.put("userId", userProfile.getFkUserId());
        }
        UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderID);
        if(userTransactionHistory!=null){        	
        	map.put("operatorName", userTransactionHistory.getServiceProvider());
        	map.put("circleName", userTransactionHistory.getSubscriberIdentificationNumber());
        	map.put("productType",userTransactionHistory.getProductType());
        	map.put("transactionStatus",userTransactionHistory.getTransactionStatus());
        }
        riwd.setProductType((String) map.get("productType"));
       // riwd.setTransactionStatus((Integer) map.get("transactionStatus"));
        
        if (riwd.getProductType().equals(ProductName.Bill.getProductType())) {
            // TODO: Read this from txn_fulfillment.transaction_status column.
            inResponse = new InResponse();
            
            if (riwd.getTransactionStatus() == null) {
                inResponse.setInRespCode(RechargeConstants.IN_UNDER_PROCESS_CODE);
            } else {
                if (riwd.getTransactionStatus().intValue() == 1) {
                    inResponse.setInRespCode(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE);
                } else {
                    inResponse.setInRespCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
                }
            }
            
        } else if(riwd.getProductType().equals(ProductName.MobilePostpaid.getProductType())){
        	 BillPaymentStatus billPaymentStatus = billWriteDAO.findBillPaymentStatusByOrderId(orderID);
        	 if(billPaymentStatus != null) {
              if (billPaymentStatus.getSuccess()!=null && billPaymentStatus.getSuccess()) {
                inResponse = new InResponse();
                inResponse.setInRespCode(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE);
               } else if (userTransactionHistory == null) {
                   inResponse = new InResponse();
                   inResponse.setInRespCode(RechargeConstants.IN_UNDER_PROCESS_CODE);
               } else {
                inResponse = new InResponse();
                inResponse.setInRespCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
              }
          }
        } else {
            inResponse = inReadDAO.getInResponseByOrderID(orderID);
        }
        
        if (inResponse==null||  (RechargeConstants.IN_UNDER_PROCESS_CODE.equalsIgnoreCase((String)map.get("transactionStatus")))){
            riwd.setRechargeResponse(RechargeConstants.IN_UNDER_PROCESS_CODE);
        } else {
            riwd.setRechargeResponse(UIResponseMapper.getRechargeReponseForUser(inResponse.getInRespCode()));
        }
        
        riwd.setOrderId(orderID);
        riwd.setMobileNo((String) map.get("mobileno"));
        riwd.setOperator((String) map.get("operatorName"));
        riwd.setCircleCode((String) map.get("circleName"));
        riwd.setAmount(Float.parseFloat(map.get("amount").toString()));
        riwd.setProductType((String) map.get("productType"));
        riwd.setLookupID((String) map.get("lookupId"));
        
        if (map.get("deliveryNmae") != null)
            riwd.setDeliveryName((String) map.get("deliveryNmae"));

        riwd.setDeliveryAdd(getAddress(map));

        if (map.get("deliveryCity") != null)
            riwd.setDeliveryCity((String) map.get("deliveryCity"));

        if (map.get("stateName") != null)
            riwd.setDeliveryState((String) map.get("stateName"));

        if (map.get("deliveryCountry") != null)
            riwd.setDeliveryCountry(map.get("deliveryCountry").toString());
        if (map.get("deliveryPinCode") != null)
            riwd.setDeliveryPincode((String) map.get("deliveryPinCode"));
        if (map.get("email") != null)
            riwd.setEmail((String) map.get("email"));
        
        UserTransactionHistory rechargeTransaction = historyService.findUserTransactionHistoryByOrderId(orderID);
        
        if (rechargeTransaction != null && 
        		!rechargeTransaction.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED) && 
        		!rechargeTransaction.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK) &&
        		!rechargeTransaction.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED) &&
        		!rechargeTransaction.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS) &&
        	    !rechargeTransaction.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE)) {
            riwd.setRechargeResponse(rechargeTransaction.getTransactionStatus());
        }
        riwd.setRefundTo(getRefundTo(orderID));
        return riwd;
    }

    public void doRechargeWithAsyncConfig(String orderId) {
        if (appConfig.isAsyncRechargeEnabled()) {
            doAsyncRecharge(orderId);
        } else {
            doSyncRecharge(orderId, null);
        }
    }
    
    private void doAsyncRecharge(String orderId) {
        try {
            kestrelWrapper.enqueueRechargeAsync(orderId);
            logger.info(String.format("Asynchronous recharge requested; orderID %s is queued", orderId));
        } catch(RechargeEnqueFailedException e) {
            logger.error("Error queueing orderID [" +  orderId + "]. Going ahead with synchronous recharge");
            doSyncRecharge(orderId, null);
        }
    }
    
    public void doSyncRecharge(String orderId, InRechargeRetryMap inRechargeRetryMap) {
        
        if (!fulfillmentScheduleService.isScheduledForRecharge(orderId)) {
            logger.error("Trying to recharge ahead of schedule not doing anything : [" + orderId + "]");
            return;
        }
        
        txnFulFilmentService.updateTxnFulfilmentAndSaveRechargeHistory(orderId);

        Map<String, Object> map = null;
        map = homeService.getUserRechargeDetailsFromOrderId(orderId);

        if (map == null || map.isEmpty()) {
        	String errorString = "No recharge details found for orderID : [" + orderId + "]";
        	logger.info(errorString);
        	throw new IllegalStateException(errorString);
        }

        String lookupId = (String) map.get("lookupId");
        String mobileNo = (String) map.get("mobileno");

        String operatorName = (String) map.get("operatorName");
        String circleName = (String) map.get("circleName");
		
        //int operatorId = (Integer) map.get("operatorId");

		if (StringUtils.isBlank(circleName) || (circleName.equalsIgnoreCase("ALL")
				&& !(circleTypeALLProductsList.contains((String) map.get("productType"))))) {
            logger.info("Circle field is ALL for gievn order id:[ " + orderId + "]");
            String productType = (String)map.get("productType");
            circleName = operatorCircleService.getCircleNameForServiceNumAndProduct(mobileNo, productType);
            //if the circleName is still not found go with a prefix mapping check
			if (StringUtils.isBlank(circleName)) {
				logger.info("Circle Name is either all or blank for the entered mobile no : " + mobileNo);
				String circlNameUsingPrefix = getCircleNameByPrefix(mobileNo, productType);
				if(circlNameUsingPrefix != null){
					logger.info("Circle Name found for mobile no : " + mobileNo + " using prefix mapping is : " + circlNameUsingPrefix);
					circleName = circlNameUsingPrefix;
				}else {
					circleName="ALL";
				}
			}
        }
		if (inRechargeRetryMap != null) {
			if (inRechargeRetryMap.getIsProductTypeRetry()) {
				circleName = inRechargeRetryMap.getRetryCircleName();
				logger.info("Product Type retry with circle:" + circleName);
			}
		}
        
        if (StringUtils.isBlank(operatorName)) {

            logger.info("operatorName field is Blank for gievn order id:[ " + orderId + "]");
            Map<String, Object> operator = operatorCircleService.findOperatorByMobilePrefix(mobileNo);

            if (operator != null) {
                Integer prepaidMobileOperatorId = (Integer) operator.get("operatorMasterId");
                if (prepaidMobileOperatorId != null) {
                    OperatorMaster operatorMaster = operatorCircleService.getOperator(prepaidMobileOperatorId);
                    if (operatorMaster!=null) {
                        operatorName = operatorMaster.getOperatorCode();
                        //operatorId = operatorMaster.getOperatorMasterId();
                    }
                }
            }
        }
        
        CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(operatorName, circleName);

        RechargeDo rechargeDo = new RechargeDo();
        rechargeDo.setAffiliateid(getUniqueAffiliateId(orderId));
        rechargeDo.setAffiliateTransId(orderId);
        rechargeDo.setAmount(Float.parseFloat(map.get("amount").toString()));
        rechargeDo.setMobileNo(mobileNo);
        rechargeDo.setProductType((String) map.get("productType"));
        rechargeDo.setCircleOperatorMapping(circleOperatorMapping);
        rechargeDo.setCircleCode(circleName);
        rechargeDo.setOperatorCode(operatorName);
        rechargeDo.setLookupId(lookupId);
        rechargeDo.setOrderId(orderId);
        rechargeDo.setRechargeRetryCounter(0);
        //rechargeDo.setOperatorId(operatorId);
        
        if (inRechargeRetryMap != null && !inRechargeRetryMap.isAggregatorRetry()) {

			circleName = inRechargeRetryMap.getRetryCircleName();
			operatorName = inRechargeRetryMap.getRetryOperatorName();
			circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(operatorName, circleName);
			rechargeDo.setCircleOperatorMapping(circleOperatorMapping);
			rechargeDo.setCircleCode(circleName);
			logger.info("Retry Circle is " + circleName);

			rechargeDo.setOperatorCode(operatorName);
            rechargeDo.setCircleOperatorMapping(operatorCircleService.getCircleOperatorMapping(
                    operatorName, circleName));
            rechargeDo.setProductType(FCConstants.productMasterMap.get(Integer.parseInt(inRechargeRetryMap
                    .getRetryProductId())));
            populatePreviousRequestDetails(orderId, rechargeDo);
        }
        
        if(inRechargeRetryMap!= null && inRechargeRetryMap.isAggregatorRetry()){
            populatePreviousRequestDetails(orderId, rechargeDo);
            rechargeDo.setAggregatorsToSkip(inRechargeRetryMap.getAggregatorsToSkip());
        }
        
        if(rechargeRetryConfigCache.getPlanLevelRetryKey(orderId)){
        	 populatePreviousRequestDetails(orderId, rechargeDo);
        }
        
        logger.info("calling IN service for recharge for order id " + orderId);
        
        InResponse inResponse = transactionDelegate.rechargeFromIN(rechargeDo);

        if (inResponse != null) {
        	
			logRequestService.logScheduleInfoIfPresent(orderId, inResponse,
					circleOperatorMapping.getFkOperatorMasterId(), circleOperatorMapping.getFkCircleMasterId(),
					mobileNo);
            if (inRechargeRetryMap != null && !inRechargeRetryMap.isAggregatorRetry()) {
                boolean isSuccess = false;
                if (AggregatorInterface.SUCCESS_IN_RESPONSE_CODE.equals(inResponse.getInRespCode())) {
                    isSuccess = true;
                }
                insertRechargeRetryData(rechargeDo, isSuccess, inResponse);
                metricOperatorRetry(inResponse, inRechargeRetryMap);
            }
            
            if (fcProperties.isDevMode()) {
                FulfillmentServiceThread FulfillmentServiceThread = new FulfillmentServiceThread(orderId,
                        fulfillmentService, true);
                FulfillmentServiceThread.start();
                BatchJobUtil.waitForFulfillmentCompletion(FulfillmentServiceThread);
            } else {
                Map<String, String> fulfillmentRequest = new HashMap<>();
                fulfillmentRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
                fulfillmentRequest.put(PaymentConstants.SHOULD_SEND_SMS_KEY, Boolean.TRUE.toString());
                kestrelWrapper.enqueueFulfillment(fulfillmentRequest);
            }
        } else {
            logger.error("inResponse is null not doing any fulfillment for orderID: [" + orderId + "]");
        }
    }

    private void populatePreviousRequestDetails(String orderId, RechargeDo rechargeDo) {
        InResponse inResponse = inService.getInResponseByOrderID(orderId);
        
        if (inResponse != null) {
            rechargeDo.setInRequestId(inResponse.getRequestId());
            if (inResponse.getRetryNumber() != null) {
                rechargeDo.setRechargeRetryCounter(inResponse.getRetryNumber() + 1);
            } else {
                rechargeDo.setRechargeRetryCounter(1);
            }
        }
    }
    
    private void metricOperatorRetry(InResponse inResponse, InRechargeRetryMap inRechargeRetryMap) {
        if (AggregatorInterface.SUCCESS_IN_RESPONSE_CODE.equals(inResponse.getInRespCode())) {
            metricsClient.recordEvent("Recharge", "OperatorRetry.Success", inRechargeRetryMap.getFkOperatorMasterId() + "-" + inRechargeRetryMap.getFkRetryOperatorMasterId());
        } else {
            metricsClient.recordEvent("Recharge", "OperatorRetry.Failure", inRechargeRetryMap.getFkOperatorMasterId() + "-" + inRechargeRetryMap.getFkRetryOperatorMasterId());
        }
    }

    private void insertRechargeRetryData(RechargeDo rechargeDo,boolean retrySuccess,InResponse inResponse){
    	try{
	    	RechargeRetryData rechargeRetryData = new RechargeRetryData();
	    	rechargeRetryData.setOrderId(rechargeDo.getAffiliateTransId());
	    	rechargeRetryData.setProductType(rechargeDo.getProductType());
	    	rechargeRetryData.setRetryOperatorName(rechargeDo.getOperatorCode()); 
	    	rechargeRetryData.setFinalRetry(retrySuccess);
	    	if(inResponse!=null){
	    		rechargeRetryData.setAggrName(inResponse.getAggrName());
	        	rechargeRetryData.setAggrRespCode(inResponse.getAggrRespCode());
	        	rechargeRetryData.setAggrRespMsg(inResponse.getAggrRespMsg());
	        	rechargeRetryData.setAggrTransId(inResponse.getAggrTransId());
	        	rechargeRetryData.setOprTransId(inResponse.getOprTransId());
	        	rechargeRetryData.setInRespCode(inResponse.getInRespCode());
	        	rechargeRetryData.setInRespMsg(inResponse.getInRespMsg());
	    	}
	    	inService.insertRechargeRetryData(rechargeRetryData);
    	}catch(Exception e){
    		logger.error("insertRechargeRetryData insertion failing for orderID: [" + rechargeDo.getAffiliateTransId() + "]");
    	}
    }

    /**
     * Bit of hack to ensure that (order_id, affiliate_id) combination is unique
     * across retries.
     * @param orderId
     * @return
     */
    private int getUniqueAffiliateId(String orderId) {
        List<InRequest> allInRequests = inService.getAllInRequests(orderId);
        final int INITIAL_VALUE = 1;
        final int LEAST_VALUE = 0;
        
        int uniqueAffiliateId = INITIAL_VALUE;
        
        if (allInRequests != null) {
            int latestAffiliateId = LEAST_VALUE;
        
            for (InRequest inRequest : allInRequests) {
                if (inRequest.getAffiliateId() > latestAffiliateId) {
                    latestAffiliateId = inRequest.getAffiliateId();
                }
            }
            
            uniqueAffiliateId = latestAffiliateId + 1;
        }
        
        return uniqueAffiliateId;
    }

    /*
     * This functions creates the txnhomepage and returns the lookupId on success.
     */
    private String saveRechargeInfo(RechargeRequestWebDo rechargeRequestWebDo, TxnHomePageBusinessDO thpDO) {

        metricsClient.recordEvent(HomeController.METRICS_PAGE_ACCESS, "recharge", "click");
        logger.info("Request comes to generate lookupId for mobileNumber: " + rechargeRequestWebDo.getMobileNumber());
        // Skipping all the denominations and the operator checks, which happens in the usual flow.
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String lookupID = FCUtil.getLookupID();
        tracker.trackLookupId(lookupID);
        String sessionId = fs.getUuid();
        

        try {
			thpDO.setProductType(RechargeConstants.MOBILE_PRODUCT_TYPE);
			thpDO.setServiceNumber(rechargeRequestWebDo.getMobileNumber());
			thpDO.setAmount(rechargeRequestWebDo.getAmount().floatValue());
			thpDO.setRechargePlanType(FCConstants.RECHARGE_TYPE_TOPUP);
			if (rechargeRequestWebDo.getPlanType() != null && rechargeRequestWebDo.getPlanType().equals(FCConstants.RECHARGE_TYPE_SPECIAL)) {
				thpDO.setRechargePlanType(FCConstants.RECHARGE_TYPE_SPECIAL);
			}
			thpDO.setOperatorName(rechargeRequestWebDo.getOperatorId().toString());
			thpDO.setCircleName(rechargeRequestWebDo.getCircleId().toString());
			thpDO.setLookupId(lookupID);
			thpDO.setSessionId(sessionId);

			fs.setRechargePlanType(thpDO.getRechargePlanType());
			txnHomePageService.saveTransactionInfo(thpDO);
			sessionHelper.saveDataInSession(thpDO);

			logger.debug("Turbo recharge: putting lookupid in session HomeController  " + lookupID);

			if(thpDO.getServiceStatus().equalsIgnoreCase(FCConstants.FAIL))
			{
			    logger.error("Get Transaction service status Fail");
			    return null;
			}
		} catch (Exception e) {
			logger.error("Exception caught while saving transaction in txn home page for mobileNumber: " + rechargeRequestWebDo.getMobileNumber() +" returning empty lookupId");
			return null;
		}
        return lookupID;
    }

    /*
     * Function to do the recharge for the given mobile number and the amount.
     * Recharge will be for the prepaid number using the wallet.
     */
    public void doTurboRecharge(RechargeRequestWebDo rechargeRequestWebDo, RechargeResponseWebDo rechargeResponseWebDo,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        @SuppressWarnings("rawtypes")
        Map map = fs.getSessionData();
        Boolean isloggedin = false;
        if (map != null) {
            if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
            }
        }

        if (!isloggedin) {
            logger.error("User is not logged in");
            rechargeResponseWebDo.setStatusCode(RechargeConstants.USER_NOT_LOGGED_IN);
            return;
        }

        Map<String, Object> operatorMap = operatorCircleService.getPrePaidServiceProvider(rechargeRequestWebDo.getMobileNumber());
        if(operatorMap == null || operatorMap.size() == 0) {
        	logger.error("Received empty operatorMap from operatorCircleService for mobileNumber: " + rechargeRequestWebDo.getMobileNumber() + "couldn't proceed with recharge");
        	return;
        	
        }
        ArrayList<Map<String, String>> operatorCircleData = (ArrayList<Map<String, String>>) operatorMap
                .get("prefixData");

        if(operatorCircleData == null || operatorCircleData.get(0) == null || operatorCircleData.get(0).get("operatorMasterId") == null || operatorCircleData.get(0).get("circleMasterId") == null) {
        	logger.error("Couldn't find operatorMasterId/operatorCircleId for mobileNumber: " + rechargeRequestWebDo.getMobileNumber() + "couldn't proceed with recharge");
        	return;
        }
        String operatorId = String.valueOf(operatorCircleData.get(0).get("operatorMasterId"));
        String circleId = String.valueOf(operatorCircleData.get(0).get("circleMasterId"));
        rechargeRequestWebDo.setOperatorId(Integer.parseInt(operatorId));
        rechargeRequestWebDo.setCircleId(Integer.parseInt(circleId));

        logger.info("TurboRecharge :  " + operatorId + circleId);

        // Create the txn_home_page entry and get the lookup id as well.
        TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
        String lookupId = this.saveRechargeInfo(rechargeRequestWebDo, txnHomePageBusinessDO);

        if (FCUtil.isEmpty(lookupId)) {
            logger.error("TurboRecharge: LookUpID generation failed for mobile number " + rechargeRequestWebDo.getMobileNumber());
            rechargeResponseWebDo.setStatusCode(RechargeConstants.LOOKUPID_GENERATION_FAIL);
            return;
        }

        logger.info("TurboRecharge : LookupId generated is " + lookupId);
        
        autoPayAppService.insertRequestLookupMap(rechargeRequestWebDo.getRequestId(), lookupId);

        // Initiate the checkout and create the cart item.
        TxnCrossSellBusinessDo bdo = new TxnCrossSellBusinessDo();
		mCheckoutService.createCartAndInitiateCheckout(txnHomePageBusinessDO, bdo);

        logger.info("TurboRecharge : Cart created for lookupId " + lookupId);

        // Now start the payment through wallet.
        String productType = RechargeConstants.MOBILE_PRODUCT_TYPE;
        String productId = Integer.toString(ProductName.fromPrimaryProductType(productType).getProductId());

        ProductPaymentWebDO productPaymentWebDO = new ProductPaymentWebDO();
        // Only Wallet Payment is accepted.
        productPaymentWebDO.setLookupID(lookupId);
        productPaymentWebDO.setPaymenttype(PaymentConstants.PAYMENT_TYPE_WALLET);
        productPaymentWebDO.setProductType(productType);
        productPaymentWebDO.setProductId(productId);

        // Complete the payment.
        Map<String, Object> paymentResponse = paymentService.doCompletePayment(productPaymentWebDO, bindingResult, request, response, "fcBalance");

        String orderId = null;
        if (!FCUtil.isEmpty(paymentResponse)) {
            orderId = (String) paymentResponse.get(PaymentConstants.ORDER_ID);
        }

        if (FCUtil.isEmpty(orderId) || FCUtil.isEmpty(paymentResponse)) {
            rechargeResponseWebDo.setStatusCode(RechargeConstants.PAYMENT_ERROR);
            return;
        }

        logger.info("TurboRecharge : orderid generated is " + orderId);

        // Get the recharge info for the order-id and return it to the user.
        RechargeInitiateWebDo rechargeInitiateWebDo  = this.getRechargeInfo(orderId);
        //RechargeDetails rechargeDetails = orderIDDAO.getRechargeDetailsByOrderID(orderId);
        rechargeResponseWebDo.getRechargeResponseWebDo(rechargeInitiateWebDo);
        rechargeResponseWebDo.setOrderId(orderId);
    }

	public InResponse doRewardRecharge(RewardRecharge rewardRecharge) {
        String orderId = rewardRecharge.getOrderId();
        String mobileNo = rewardRecharge.getServiceNumber();
        int operatorId = rewardRecharge.getOperatorId();
        int circleId = rewardRecharge.getCircleId();
        double amount = rewardRecharge.getAmount();
        String circleName = operatorCircleService.getCircle(circleId).getName();
        InResponse inResponse = null;
        String operatorName = operatorCircleService.getOperator(operatorId).getName();
        CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(operatorId,
                circleId);
        RechargeDo rechargeDo = new RechargeDo();
        rechargeDo.setAffiliateid(getUniqueAffiliateId(orderId));
        rechargeDo.setAffiliateTransId(orderId);
        rechargeDo.setAmount((float) amount);
        rechargeDo.setMobileNo(mobileNo);
        rechargeDo.setProductType(ProductMaster.ProductName.Mobile.getProductType());
        rechargeDo.setCircleOperatorMapping(circleOperatorMapping);
        rechargeDo.setCircleCode(circleName);
        rechargeDo.setOperatorCode(operatorName);
        rechargeDo.setOrderId(orderId);
        boolean isSpecialOperator = IreffRechargeDataImportService.specialOperatorIds.contains(operatorId);
        rechargeDo.setRechargePlanType(isSpecialOperator ? RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL
                : RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE);
        rechargeDo.setRechargeRetryCounter(0);
        logger.info("calling IN service for recharge for order id " + orderId);
        inResponse = transactionDelegate.doRewardRecharge(rechargeDo);
        return inResponse;
    }
    
    public String getRefundTo(String orderId) {
        String refundTo = PaymentConstants.ONE_CHECK_WALLET_CODE;
        try {
            List<PaymentTransaction> paymentTransactions = paymentTransactionService
                    .getSuccessfulPaymentTransactionsByOrderId(orderId);
            RechargeDetails rechargeDetails = orderIdReadDAO.getRechargeDetailsByOrderID(orderId);
            Double totalAmount = StringUtils.isEmpty(rechargeDetails.getAmount()) ? null
                    : Double.valueOf(orderIdReadDAO.getRechargeDetailsByOrderID(orderId).getAmount());
            boolean isOldUser = false;
            for (PaymentTransaction transaction : paymentTransactions) {
                //if recharge details is empty sum up payment txn amounts
                if (StringUtils.isEmpty(rechargeDetails.getAmount())) {
                    totalAmount = totalAmount + transaction.getAmount();
                }
                isOldUser = isOldUser || PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE
                        .equalsIgnoreCase(transaction.getPaymentGateway());
            }
            RefundAction refundAction = paymentRefund.determineRefundDestination(paymentTransactions.get(0),
                    totalAmount);
            if (refundAction == paymentRefund.WALLET_REFUND_ACTION && !isOldUser) {
                logger.info("RefundAction is ocw");
                refundTo = PaymentConstants.ONE_CHECK_WALLET_CODE;
            }
            if (refundAction == paymentRefund.WALLET_REFUND_ACTION && isOldUser) {
                logger.info("RefundAction is fw");
                refundTo = PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE;
            }
            if (refundAction == paymentRefund.PG_REFUND_ACTION) {
                logger.info("RefundAction is source pg");
                refundTo = PaymentConstants.SOURCE_PG;
            }
        } catch (Exception ex) {
            logger.error("Exception identifying refund source for OrderId:" + orderId, ex);
        }
        return refundTo;
    }
    
    @Transactional
    public RewardRecharge getRewardRecharge(String orderId){
        return rewardRechargeDAO.getRewardRecharge(orderId);
    }
    
    @Transactional
    public void createRewardRecharge(RewardRecharge rewardRecharge){
        rewardRechargeDAO.createRewardRecharge(rewardRecharge);
    }
    
    public boolean shouldProceedWithRecharge(String orderId) {
        String mtxnId = paymentTransactionService.getPaymentFulfilmentMtxnFromOrder(orderId);
        Boolean isRefunded = null;
        try {
            isRefunded = paymentTransactionService.isMtxnRefunded(mtxnId);
        } catch (Exception e) {
            logger.error("Caught exception on checking refund for " + mtxnId, e);
        }
        if (isRefunded != null && isRefunded) {
            return false;
        }
        return true;
    }

    public ProductType getFinalProductType(String orderId, String initialProductType) {
        if (!FCUtil.isUtilityPaymentType(initialProductType)) {
            TransactionStatus transactionStatus = null;
            if (FCUtil.isRechargeProductType(initialProductType)) {
                transactionStatus = inService.getRechargeStatus(orderId);
                if (transactionStatus != null) {
                    if (inService.isFailure(transactionStatus.getTransactionStatus())) {
                        BillPaymentStatus billPaymentStatus = billPaymentService
                                .findBillPaymentStatusByOrderId(orderId);
                        if (billPaymentStatus != null) {
                            return ProductType.PostPaidType;
                        }
                    }
                }
                return ProductType.RechargeType;
            }
            if (FCUtil.isBillPostpaidType(initialProductType)) {
                BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
                if (billPaymentStatus != null) {
                    if (!billPaymentStatus.getSuccess()) {
                        TransactionStatus retryTransactionStatus = inService.getRechargeStatus(orderId);
                        if (retryTransactionStatus != null) {
                            return ProductType.RechargeType;
                        }
                    }
                }
                return ProductType.PostPaidType;
            }
        }
        if (FCUtil.isUtilityPaymentType(initialProductType)) {
            return ProductType.UtilityType;
        }
        return ProductType.RechargeType;
    }
    
	private String getCircleNameByPrefix(String mobileNo, String productType) {
		try {
			Map<String, Object> productPredictorData = operatorCircleService.getProductTypeData(mobileNo, productType);
			if (!FCUtil.isEmpty(productPredictorData)) {
				String circleId = productPredictorData.get("circleId").toString();
				CircleMaster circleMaster = operatorCircleService.getCircle(Integer.valueOf(circleId));
				if (circleMaster != null) {
					return circleMaster.getName();
				}
			}
		} catch (Exception e) {
			logger.error(
					"An exception occurred while trying to fetch the circle data using prefix mapping for mobile no : "
							+ mobileNo);
		}
		return null;
	}
}