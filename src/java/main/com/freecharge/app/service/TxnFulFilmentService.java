package com.freecharge.app.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.ITxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;

/**
 * Created by IntelliJ IDEA. User: Jitender Date: May 16, 2012 Time: 05:22:56 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("txnFulFilmentService")
public class TxnFulFilmentService {
    @Autowired
    private ITxnFulfilmentDAO txnFulfilmentDAO;
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private TxnHomePageService txnHomePageService;

    @Autowired
    private OrderIdWriteDAO orderIdWriteDAO;

    @Autowired
    private TxnFulfilmentDAO   txnFulfilDAO;

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Transactional
    public TxnFulfilment updateTxnFulfilment(TxnFulfilment txnFulfilment) {
        logger.info("Updating TxnFulfilment table ", null);
        return txnFulfilmentDAO.updateFulfilmentDB(txnFulfilment);
    }

    @Transactional
    public void updateTxnFulfilmentAndSaveRechargeHistory(String orderId) {
        try {
            logger.info("updating txnfulfillment and user rechargehistory in txnfulfillment service for orderId:" + orderId);
            Double amount = 0d;
            Boolean isSuccessful = false;
            Integer paymentTxnId = 0;

            List<PaymentTransaction> ptList = paymentTransactionService.getPaymentTransactionsByOrderId(orderId);
            if (ptList != null && !ptList.isEmpty()) {
                paymentTxnId = ptList.get(0).getPaymentRequestId();
                amount = ptList.get(0).getAmount();
                
                for (PaymentTransaction paymentTransaction : ptList) {
                    if (paymentTransaction.getIsSuccessful()) {
                        isSuccessful = true;
                        break;
                    }
                }
            }
            
            updateTxnfulfillment(orderId, amount, isSuccessful, 0, paymentTxnId);
        } catch (Exception e) {
            logger.error("Error occured while updating txnfulfillment and user rechargehistory in txnfulfillment service for orderId:" + orderId, e);
        }

    }

    @Transactional
    private void updateTxnfulfillment(String orderId, Double amount, Boolean isSuccessful, Integer gatewayCode, Integer paymentTxnId) {
        logger.debug("Just before updating txn_fulfilment in TxnFulFilmentService, order_id is = " + orderId);
        String lookupId = orderIdWriteDAO.getLookupIdForOrderId(orderId);
        Integer txnhomepageId = txnHomePageService.findTxnHomePageIdBySessionId(lookupId);
        Integer txnfullfillmentId = txnFulfilmentDAO.findByTxnHomePageId(txnhomepageId).get(0).getId();
        
        try {
            txnFulfilmentDAO.updateOrderId(txnfullfillmentId, orderId);
            logger.debug("Updated txn_fulfilment in TxnFulFilmentService, order_id is = " + orderId);
        } catch (Exception exception) {
            logger.error("Exception occurred during user recharge history creation", exception);
        }
    }
    
    @Transactional
    public TxnFulfilment read(int fulfilmentId){
        return txnFulfilDAO.read(fulfilmentId);
    }
}