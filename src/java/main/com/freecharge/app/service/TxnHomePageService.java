package com.freecharge.app.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.HomePageDAO;
import com.freecharge.app.domain.dao.IRegistrationDAO;
import com.freecharge.app.domain.dao.ITxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.businessdo.HomeBusinessDo;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.ProductMasterMapping;
import com.freecharge.mongo.repos.LookupIdRepository;
import com.freecharge.rest.recharge.TxnHomePageBO;
import com.freecharge.web.util.WebConstants;

/**
 * Created by IntelliJ IDEA. User: abc Date: May 17, 2012 Time: 8:08:11 PM To change this template use File | Settings |
 * File Templates.
 */
@Service
public class TxnHomePageService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private ITxnHomePageDAO txnHomePageDAO;
    
    @Autowired
    private CartItemsDAO cartItemsDAO;
    
    @Autowired
    private IRegistrationDAO registrationDAO;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private OperatorCircleReadDAO operatorCircleReadDAO;

    @Autowired
    private LookupIdRepository lookupIdRepository;
    
    @Autowired
    private HomePageDAO homePageDAO;

    @Transactional
    public TxnHomePageBusinessDO getTransactionDetails(String lookupID) {
        List<TxnHomePage> txnHomePageList = txnHomePageDAO.findByProperty("lookupId", lookupID);
        TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
        if (txnHomePageList != null && txnHomePageList.size() == 1) {
            TxnHomePage txnHomePage = txnHomePageList.get(0);
            txnHomePageBusinessDO.setTxnHomePageId(txnHomePage.getId());
            txnHomePageBusinessDO.setServiceNumber(txnHomePage.getServiceNumber());
            txnHomePageBusinessDO.setOperatorName(txnHomePage.getOperatorName());
            txnHomePageBusinessDO.setCircleName(txnHomePage.getCircleName());
            txnHomePageBusinessDO.setAmount(txnHomePage.getAmount());
            txnHomePageBusinessDO.setProductType(txnHomePage.getProductType());
            txnHomePageBusinessDO.setSessionId(txnHomePage.getSessionId());
            txnHomePageBusinessDO.setLookupId(txnHomePage.getLookupId());
        }
        return txnHomePageBusinessDO;
    }

    @Transactional
    public TxnHomePageBusinessDO getTransactionDetails(WebContext webContext) {
        String lookupID = webContext.getRequest().getParameter("lookupID");
        List<TxnHomePage> txnHomePageList = txnHomePageDAO.findByProperty("lookupId", lookupID);
        TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
        if (txnHomePageList != null && txnHomePageList.size() == 1) {
            TxnHomePage txnHomePage = txnHomePageList.get(0);
            txnHomePageBusinessDO.setTxnHomePageId(txnHomePage.getId());
            txnHomePageBusinessDO.setServiceNumber(txnHomePage.getServiceNumber());
            txnHomePageBusinessDO.setOperatorName(txnHomePage.getOperatorName());
            txnHomePageBusinessDO.setCircleName(txnHomePage.getCircleName());
            txnHomePageBusinessDO.setAmount(txnHomePage.getAmount());
            txnHomePageBusinessDO.setProductType(txnHomePage.getProductType());
            txnHomePageBusinessDO.setSessionId(txnHomePage.getSessionId());
            txnHomePageBusinessDO.setLookupId(txnHomePage.getLookupId());
        }
        FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
        if (freechargeSession != null) {
            Map map = freechargeSession.getSessionData();
            if (map != null) {
                String emailId = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
                String serviceNumber = webContext.getRequest().getParameter("serviceNumber");
                Boolean islogin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                if (islogin != null && islogin) {
                    List<UserContactDetail> list = registrationDAO.getUserContactDetails(emailId, serviceNumber);
                    if (list != null && list.size() > 0) {
                        UserContactDetail contactdetails = list.get(0);
                        txnHomePageBusinessDO.setContactDetails(contactdetails);
                        txnHomePageBusinessDO.setDefaultProfile("no");
                        txnHomePageBusinessDO.setServiceStatus("success");
                    } else {
                        List<UserContactDetail> defaultList = registrationDAO.getUserContactDefaultDetails(emailId);
                        if (defaultList != null && defaultList.size() > 0) {
                            UserContactDetail defaultdetails = defaultList.get(0);
                            RegisterBusinessDO registerBusinessDO = new RegisterBusinessDO();
                            txnHomePageBusinessDO.setContactDetails(defaultdetails);
                            txnHomePageBusinessDO.setDefaultProfile("yes");
                            txnHomePageBusinessDO.setServiceStatus("success");
                        }
                    }

                }

            }
        }
        return txnHomePageBusinessDO;
    }

    @Transactional
    public void saveTransactionHomePage(TxnHomePage txnHomePage) {
        txnHomePageDAO.saveTransactionInfo(txnHomePage);
    }
    
    public TxnHomePageBO saveInfoAndCreateCart(BaseBusinessDO baseBusinessDO, String userId, String emailId, String rechargeType, Integer planId, String planCategory) {
        TxnHomePageBusinessDO txnHomePageBusinessDO = (TxnHomePageBusinessDO) baseBusinessDO;
        
        TxnHomePage txnHomePage = createTxnHomePageToInsert(txnHomePageBusinessDO);
            return saveFart(txnHomePage, userId, emailId, rechargeType, planId, planCategory);
    }
    
    @Transactional
    private TxnHomePageBO saveFart(TxnHomePage homePage, String userId, String emailId, String rechargeType, Integer planId, String planCategory) {
        return homePageDAO.saveInfoAndCreateCart(homePage, userId, emailId, rechargeType, planId, planCategory);
    }

    @Transactional
    public void saveTransactionInfo(BaseBusinessDO baseBusinessDO) {
        TxnHomePageBusinessDO txnHomePageBusinessDO = (TxnHomePageBusinessDO) baseBusinessDO;
        
        TxnHomePage txnHomePage = createTxnHomePageToInsert(txnHomePageBusinessDO);

        Integer value = txnHomePageDAO.saveTransactionInfo(txnHomePage);
        ((TxnHomePageBusinessDO) baseBusinessDO).setTxnHomePageId(value);
        if (value != 0)
            ((TxnHomePageBusinessDO) baseBusinessDO).setServiceStatus("success");
        else
            ((TxnHomePageBusinessDO) baseBusinessDO).setServiceStatus("fail");

        // Save festiveMessage into mongo here
        String lookupId = txnHomePageBusinessDO.getLookupId();
        logger.debug("Now going to save festiveMessage");
        try {
            String festiveMessage = txnHomePageBusinessDO.getFestiveMessage();
            logger.debug("festiveMessage=" + festiveMessage);
            if ((festiveMessage != null) && (festiveMessage.length() != 0)) {
                Map<String, Object> data = new HashMap<String, Object>();
                data.put(LookupIdRepository.LOOKUPID_NAME, lookupId);
                data.put(LookupIdRepository.GIFTSMS_NAME, festiveMessage);
                data.put(LookupIdRepository.CREATEDAT_NAME, new Date());
                lookupIdRepository.updateOrCreateLookupIdData(lookupId, data);
            }
        } catch (Exception logNForget) {
            logger.error("Error while saving festiveMessage for lookupId=" + lookupId + ": " + logNForget);
        }
    }

    private TxnHomePage createTxnHomePageToInsert(TxnHomePageBusinessDO txnHomePageBusinessDO) {
                TxnHomePage txnHomePage = new TxnHomePage();
                txnHomePage.setServiceNumber(txnHomePageBusinessDO.getServiceNumber());
                try {
                    txnHomePage.setAmount(new Float(txnHomePageBusinessDO.getAmount()));
                } catch (Exception e) {
                    txnHomePage.setAmount(new Float(0));
                    logger.error("Error while converting sting amount to float amount.." + e);
                }
                Integer operatorId = 1;
                String operatorIdStr = txnHomePageBusinessDO.getOperatorName();
                if (operatorIdStr != null && operatorIdStr != "") {
                    try {
                        operatorId = Integer.parseInt(operatorIdStr);
                    } catch (NumberFormatException nfe) {
                        logger.error("Exception occured while converting operatorId string into integer.", nfe);
                        operatorId = 1;
                    }
                }
                OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorId);

                String operatorName = "";
                if (operatorMaster != null) {
                    operatorName = operatorMaster.getOperatorCode();
                }
                txnHomePage.setOperatorName(operatorName);
                logger.debug("txn homepage details service number " + txnHomePageBusinessDO.getServiceNumber() + " circle "
                        + txnHomePageBusinessDO.getCircleName() + " operator name " + txnHomePageBusinessDO.getOperatorName()
                        + " product type " + txnHomePageBusinessDO.getProductType());
                String productType = txnHomePageBusinessDO.getProductType();
                if (productType != null && productType.equalsIgnoreCase("D")) {
                    txnHomePage.setProductType(productType);
                    txnHomePage.setCircleName(FCConstants.DEFAULT_DTH_CIRCLE);
                    txnHomePage.setFkCircleId(Integer.parseInt(txnHomePageBusinessDO.getCircleName()));
                } else if (productType != null && productType.equalsIgnoreCase(FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS)) {
                    txnHomePage.setProductType(productType);
                    txnHomePage.setCircleName(FCConstants.DEFAULT_DTH_CIRCLE);
                    txnHomePage.setFkCircleId(Integer.parseInt(txnHomePageBusinessDO.getCircleName()));
                } else {
                    String circleMasterId = null;
                    CircleMaster circleMaster = null;
                    txnHomePage.setProductType(productType);
                    if (txnHomePageBusinessDO.getCircleName() != null && !txnHomePageBusinessDO.getCircleName().isEmpty()) {
                        try {
                            Integer.parseInt(txnHomePageBusinessDO.getCircleName());// This field contains an id of Circle Mater
                            circleMasterId = txnHomePageBusinessDO.getCircleName();

                        } catch (Exception ex) {
                            logger.info("request come under catch block for service number "
                                    + txnHomePageBusinessDO.getServiceNumber());
                            circleMasterId = fetchCircleMasterId(txnHomePageBusinessDO);
                        }
                        circleMaster = operatorCircleService.getCircle(Integer.parseInt(circleMasterId));
                        if (circleMaster == null)
                            circleMaster = operatorCircleService.getCircle(Integer
                                    .parseInt(fetchCircleMasterId(txnHomePageBusinessDO)));
                        txnHomePage.setCircleName(circleMaster.getName());
                        txnHomePage.setFkCircleId(circleMaster.getCircleMasterId());
                    }
                }

                txnHomePage.setSessionId(txnHomePageBusinessDO.getSessionId());
                txnHomePage.setLookupId(txnHomePageBusinessDO.getLookupId());
                txnHomePage.setCreatredOn(new Timestamp(System.currentTimeMillis()));

                if (txnHomePageBusinessDO.getOperatorName() != null) {
                    txnHomePage.setFkOperatorId(Integer.parseInt(txnHomePageBusinessDO.getOperatorName()));
                }
                txnHomePage.setFkProductId(ProductMasterMapping.getProductIdbyType(txnHomePageBusinessDO.getProductType()));
                return txnHomePage;
    }

    @Transactional
    public List<HomeBusinessDo> getTxnHomePageDetailsByLookUpId(String lookupId, BaseBusinessDO baseBusinessDO) {
        List<HomeBusinessDo> hoWebDos = txnHomePageDAO.getTxnHomePageDetailsByLookUpId(lookupId, baseBusinessDO);
        return hoWebDos;
    }

    public Integer findTxnHomePageIdBySessionId(Object lookupId) {
        return txnHomePageDAO.findTxnHomePageIdBySessionId(lookupId);
    }

    public List<TxnHomePage> findTxnHomePageByLookupId(String lookupId) {
        return txnHomePageDAO.findByLookupId(lookupId);
    }

    private String fetchCircleMasterId(TxnHomePageBusinessDO txnHomePageBusinessDO) {
        String serviceNumber = txnHomePageBusinessDO.getServiceNumber();
        if (serviceNumber != null && serviceNumber.length() > 4) {
            String prefixValue = serviceNumber.substring(0, 4);
            List<Map<String, Object>> listobject = operatorCircleReadDAO.getOperatorCircleByPrefixId(prefixValue);
            if (listobject != null && listobject.size() > 0) {
                Map<String, Object> map = listobject.get(0);
                return map.get("circleMasterId").toString();
            }
        }
        return null;
    }

    public List<TxnHomePage> findBySessionId(Object sessionId) {
        return txnHomePageDAO.findBySessionId(sessionId);
    }

    public int updateTxnHomePage(String lookupId, String newSessionId) {
        return txnHomePageDAO.updateTxnHomePage(lookupId, newSessionId);
    }
}
