package com.freecharge.app.crosssell;

import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.UserContactDetail;

public interface CrossSellInterface {
	public boolean processCrossSell(UserContactDetail userContactDetail, List<Map<String, Object>> crossSellData, String velocityMailTemplateName, String orderId);
	public boolean isValidCrossSellForUser(Integer userId, Integer crossSellId);
}
