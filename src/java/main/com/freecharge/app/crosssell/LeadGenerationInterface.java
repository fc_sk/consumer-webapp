package com.freecharge.app.crosssell;

import com.freecharge.app.domain.entity.UserContactDetail;

public interface LeadGenerationInterface {
	public boolean processCrossSell(UserContactDetail userContactDetail, Integer crossSellId, String velocityMailTemplateName);
	public boolean isValidCrossSellForUser(Integer userId, Integer crossSellId);

}
