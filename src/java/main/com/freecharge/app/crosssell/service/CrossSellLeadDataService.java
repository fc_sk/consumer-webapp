package com.freecharge.app.crosssell.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.jdbc.CrossSellLeadDataReadDAO;
import com.freecharge.app.domain.dao.jdbc.CrossSellLeadDataWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CrossSellLeadData;

@Service
public class CrossSellLeadDataService {
	
	@Autowired
	private CrossSellLeadDataReadDAO crossSellLeadDataReadDAO;
	
	@Autowired
	private CrossSellLeadDataWriteDAO crossSellLeadDataWriteDAO;
	
	@Transactional
	public List<CrossSellLeadData> getCrossSellLeadData(Integer userId, Integer crossSellId) {
		return crossSellLeadDataReadDAO.getCrossSellLeadData(userId, crossSellId);
	}
	
	@Transactional
	public List<CrossSellLeadData> getCrossSellLeadDataFromUserId(Integer userId) {
		return crossSellLeadDataReadDAO.getCrossSellLeadDataFromUserId(userId);
	}

	@Transactional
	public List<CrossSellLeadData> getCrossSellLeadDataFromCrossSellId(Integer crossSellId) {
		return crossSellLeadDataReadDAO.getCrossSellLeadDataFromCrossSellId(crossSellId);
	}
	
	@Transactional
	public CrossSellLeadData create(CrossSellLeadData crossSellLeadData) {
		return crossSellLeadDataWriteDAO.create(crossSellLeadData);
	}
}
