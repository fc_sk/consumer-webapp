package com.freecharge.app.crosssell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;

@Service("genericCrossSell")
public class GenericCrossSell implements CrossSellInterface {
	
	@Autowired
	private EmailService emailService;

	@Autowired
	private FCProperties fcProperties;
	
	private static Logger logger = LoggingFactory.getLogger(GenericCrossSell.class);
	
	@Override
	public boolean processCrossSell(UserContactDetail userContactDetail, List<Map<String, Object>> crossSellData, String velocityMailTemplateName, String orderId) {
		if(velocityMailTemplateName != null) {
			this.sendEmail(userContactDetail, crossSellData, velocityMailTemplateName, orderId);
		}
		
		return true;
	}
	
	private void sendEmail(UserContactDetail userContactDetail, List<Map<String, Object>> crossSellData, String velocityMailTemplateName, String orderId) {
		EmailBusinessDO businessDO = new EmailBusinessDO();
		logger.info("start sending email for order id for cross sell" + orderId);
		StringBuffer subject = new StringBuffer((String)crossSellData.get(0).get("crosssellMailSubject"));
		subject.insert(0, (String)crossSellData.get(0).get("crosssellname")+" ");
		subject.append(" "+orderId);
		businessDO.setSubject(subject.toString());

		businessDO.setTemplateName(velocityMailTemplateName);
		businessDO.setCouponsList(crossSellData);
		businessDO.setFrom(fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_FROM_EMAILID));
		businessDO.setTo(userContactDetail.getEmail());
		businessDO.setReplyTo(fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_REPLYTO));
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("userName", userContactDetail.getFirstName());
		map.put("crosssellname", (String)crossSellData.get(0).get("crosssellname"));
		map.put("crosssellMailTnc", (String)crossSellData.get(0).get("crosssellMailTnc"));
		
		String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();

        String hostPrefix = fcProperties.getProperty("hostprefix");
        hostPrefix = hostPrefix.trim();

        map.put("hostPrefix", hostPrefix);
        map.put("imgPrefix", imgPrefix);
        
		
		businessDO.setContentHtml(true);
		businessDO.setVariableValues(map);
		
		logger.debug("Send Crossell Mail for orderId:" + orderId +" crossSell:" + map.get("crosssellname") + " code: " + (String)crossSellData.get(0).get("couponCode"));
		emailService.sendEmailWithCouponDetails(businessDO);
		
	}

	@Override
	public boolean isValidCrossSellForUser(Integer userId, Integer crossSellId) {
		return true;
	}
}
