package com.freecharge.app.crosssell;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.crosssell.service.CrossSellLeadDataService;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CrossSellLeadData;

@Service("genericLeadGeneration")
public class GenericLeadGenerationCrossSell implements LeadGenerationInterface {

	@Autowired
	private CrossSellLeadDataService crossSellLeadDataService;

	
	@Override
	public boolean processCrossSell(UserContactDetail userContactDetail, Integer crossSellId, String velocityMailTemplateName) {
		
		CrossSellLeadData crossSellLeadData = new CrossSellLeadData();
		crossSellLeadData.setFkUserId(userContactDetail.getUserId());
		crossSellLeadData.setFkCrossSellId(crossSellId);
		crossSellLeadData.setCrossSellData("Name: "+userContactDetail.getFirstName()+", Email: "+userContactDetail.getEmail()+", Phone Number: "+userContactDetail.getMobile());
		
		crossSellLeadData = crossSellLeadDataService.create(crossSellLeadData);
		
		if(velocityMailTemplateName != null) {
			this.sendEmail(userContactDetail, velocityMailTemplateName);
		}
		
		if(crossSellLeadData.getId() != null && crossSellLeadData.getId() > 0)
			return true;
		else
			return false;
	}
	
	private void sendEmail(UserContactDetail userContactDetail, String velocityMailTemplateName) {
		//TODO: no requirement for email right now in case of lead gens
	}

	@Override
	public boolean isValidCrossSellForUser(Integer userId, Integer crossSellId) {
		List<CrossSellLeadData> crossSellLeadDatas = crossSellLeadDataService.getCrossSellLeadData(userId, crossSellId);
		
		if(crossSellLeadDatas != null && crossSellLeadDatas.size() > 0) {
			return false;
		}else 
			return true;
	}
	
}
