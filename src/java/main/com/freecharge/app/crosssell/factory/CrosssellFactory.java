package com.freecharge.app.crosssell.factory;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class CrosssellFactory implements ApplicationContextAware{
	private ApplicationContext applicationContext;

	public Object getCrossSellBean(String crossSellBeanName) {
		if (this.getApplicationContext().containsBean(crossSellBeanName))
			return this.getApplicationContext().getBean(crossSellBeanName);
		else
			return null;
	}

	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.applicationContext = ctx;
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
