package com.freecharge.uuip;

/**
 * This is an Enum for all the User Data Point Types that will be captured by UserDataPointSNSService.
 *
 */
public enum UserDataPointType {
    //Data point types
    EMAIL_ID("email"),
    PROFILE_NO("profileNumber"),
    RECHARGE_NO("rechargeNumber"),
    IMEI("imei"),
    WINDOWS_UNIQUE_DEVICE_ID("deviceId");

    private String name;

    private UserDataPointType(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
