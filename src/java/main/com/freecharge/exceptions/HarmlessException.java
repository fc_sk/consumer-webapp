package com.freecharge.exceptions;

/**
 * A wrapper over {@link java.lang.Exception} which helps you to wrap harmless exceptions so that we need not worry about these exceptions appearing in
 * the logs.
 *
 * Usage example:
 *
 * <pre>
 * {@code
 * logger.error(new HarmlessException(e));
 * }
 * </pre>
 */
public class HarmlessException extends Exception {
    public HarmlessException(Throwable cause) {
        super(cause);
    }
}
