package com.freecharge.admin.responsehandler;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ResponseStatusFormValidator implements Validator {
	 
	@Override
	public boolean supports(Class<?> aClass) {
		return ResponseStatusForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "affiliateTransId", "Affiliate Transaction Id field cannot be blank.", "Affiliate Transaction Id field cannot be blank.");
    }
}
