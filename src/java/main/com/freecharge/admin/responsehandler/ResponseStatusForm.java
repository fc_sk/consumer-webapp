package com.freecharge.admin.responsehandler;

public class ResponseStatusForm {
	public String affiliateTransId;
	
	public String getAffiliateTransId() {
		return affiliateTransId;
	}

	public void setAffiliateTransId(String affiliateTransId) {
		this.affiliateTransId = affiliateTransId;
	}
}
