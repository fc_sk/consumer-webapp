package com.freecharge.admin.interceptor;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.service.AccessControlService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by vishal on 4/11/15.
 */
public abstract class AccessControlInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggingFactory.getLogger(AccessControlInterceptor.class);

    @Autowired protected AccessControlService accessControlService;

    @Override public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        String path = request.getRequestURI();
        logger.debug("Request URI: " + path);
        String curUserEmail = getLoggedInUserEmailId();
        if (curUserEmail != null && !curUserEmail.isEmpty()) {
            if(!accessControlService.hasRestEndpointAccess(curUserEmail, path))
                return get403HtmlResponse(response, curUserEmail, path);
        } else {
            if(accessControlService.isRestEndPointProtected(path))
                return get403HtmlResponse(response, curUserEmail, path);
        }
        return true;
    }

    private boolean get403HtmlResponse(HttpServletResponse response, String userEmail, String resourceURI)
            throws IOException {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        if(userEmail!=null && !userEmail.isEmpty()) {
            pw.println("Access Denied for user : " + userEmail + " for resource URI : " + resourceURI + "<br><br>");
        }
        else
        {
            pw.println("Access Denied to Unknown User for resource URI : " + resourceURI + "<br><br>");
        }
        pw.println("Please contact - "+ getAdminContactEmail());
        pw.flush();
        return false;

    }

    protected abstract List<String> getAdminContactEmail() ;

    protected abstract String getLoggedInUserEmailId() ;
}