package com.freecharge.admin.interceptor;

import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/22/13
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdminLoginInterceptor extends HandlerInterceptorAdapter {
	
    private final List<String> EXCLUDED_PATHS = ImmutableList.of(
            "/admin/timeline/events"
    );
    
    private boolean shouldCheckForSession(String uri) {
    	logger.info("Request received uri : " + uri);
        if (StringUtils.isNotEmpty(uri)) {
            if (EXCLUDED_PATHS.contains(uri)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getRequestURI();
        logger.debug("Request URI: "+path);
        if(!shouldCheckForSession(path))
        	return true;
        String curUserEmail = null;
		FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
		Map<String, Object> map = freechargeSession.getSessionData();
		if (map != null) {
			curUserEmail = (String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL);
		}

		if (!FCStringUtils.isBlank(curUserEmail)){
            logger.info("current_logged_in_user: " + curUserEmail);
            request.setAttribute("curUser", curUserEmail);
            return true;
        }else {
            if(path.equals("/admin") || path.contains("admin/login") || path.contains("admin/gcallback")){
                return true;
            }
            logger.debug("No user in admin session, redirecting to login");
            response.sendRedirect("/admin/login?next="+ URLEncoder.encode(path+"?"+request.getQueryString(), "utf-8"));
            return false;
        }
    }
    private static final Logger logger = LoggingFactory.getLogger(AdminLoginInterceptor.class);
}
