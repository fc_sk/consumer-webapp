package com.freecharge.admin.interceptor.auditInterceptor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import org.apache.commons.lang.StringUtils;
import com.google.common.collect.ImmutableList;


public class AdminAuditInterceptor extends HandlerInterceptorAdapter {

	private Logger logger=LoggingFactory.getLogger(this.getClass());
	private static final String LOG_TIMESTAMP="TIME";
	private static final String LOG_USER_EMAILID="USER_ID";
	private static final String LOG_REQUEST_TYPE="REQUEST_TYPE";
	private static final String LOG_API_METHOD_CALLED="API_METHOD_CALLED";
	private static final String LOG_REQUEST_ATTRIBUTES="REQUEST_ATTRIBUTES";
	private static final String LOG_REQUEST_PARAMETERS="REQUEST_PARAMETERS";
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
					throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String email="";
		String uri = request.getRequestURI();
		if(!shouldCheckForSession(uri))
			return true;
		FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
		Map<String, Object> map = freechargeSession.getSessionData();
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH-mm-ss");
		String myDateString = sdf.format(myDate);
		if (map != null) {
			email= (String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL);
		}
		HashMap<String,Object> logObject=new HashMap<String, Object>();
		logObject.put(LOG_REQUEST_TYPE, request.getMethod());
		logObject.put(LOG_USER_EMAILID,email);
		logObject.put(LOG_TIMESTAMP,myDateString);
		logObject.put(LOG_API_METHOD_CALLED, request.getServletPath());
		logObject.putAll(getHTTPAttributes(request));
		logObject.putAll(getHTTPParameter(request));
		JSONObject singleLogEvent=new JSONObject(logObject);
		logger.info(singleLogEvent.toJSONString());
		return true;
	}
	
	/*
	 * returns Map of key value pairs of requestAttributes
	 */
	
	    private Map<String,String> getHTTPAttributes(HttpServletRequest request){
	        Map<String,String> attributeMap=new HashMap<String,String>();
	        Enumeration<String> attribute=request.getSession().getAttributeNames();
	        String s;
	        while(attribute.hasMoreElements()){
	            s=attribute.nextElement();
	            attributeMap.put(LOG_REQUEST_ATTRIBUTES+"_"+s,String.valueOf(request.getAttribute(s)));
	    }
	        return attributeMap;
	    }
	  
	    /*
	     * returns a Map of key value pairs of parameter
	     */
	    private Map<String,String> getHTTPParameter(HttpServletRequest request){
	        Map<String,String> parameterMap=new HashMap<String,String>();
            Enumeration<String> parameter=request.getParameterNames();
            String s;
            while(parameter.hasMoreElements()){
            s=parameter.nextElement();
            parameterMap.put(LOG_REQUEST_PARAMETERS+"_"+s,String.valueOf(request.getParameter(s)));
            }
            return parameterMap;
	 
	    }
	    private final List<String> EXCLUDED_PATHS = ImmutableList.of(
	            "/admin/timeline/events"
	    );
	    
	    private boolean shouldCheckForSession(String uri) {
	    	logger.info("Request received uri : " + uri);
	        if (StringUtils.isNotEmpty(uri)) {
	            if (EXCLUDED_PATHS.contains(uri)) {
	                return false;
	            }
	        }
	        return true;
	    }
	    
}
