package com.freecharge.admin.interceptor;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by vishal on 4/11/15.
 */
public class AdminAccessControlInterceptor extends AccessControlInterceptor {
    @Autowired FCProperties fcProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception
    {
        if(fcProperties.isAccessControlEnabled())
            return super.preHandle(request, response, handler);
        else
            return true;
    }

    @Override protected List<String> getAdminContactEmail() {
        return fcProperties.getAdminRootEmails();
    }

    @Override protected String getLoggedInUserEmailId() {
        FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = freechargeSession.getSessionData();
        if (map != null) {
            return (String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL);
        }
        return null;
    }
}
