package com.freecharge.admin.deals;

import com.freecharge.app.domain.entity.jdbc.BinOffer;

public class BinOfferData {
	private BinOffer binOffer;
	private String status;
	private String amount;
	
	public BinOffer getBinOffer() {
		return binOffer;
	}
	public void setBinOffer(BinOffer binOffer) {
		this.binOffer = binOffer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
