package com.freecharge.admin.entity;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 5:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundRow implements Serializable {
    private String paymentTransactionId;
    private String paymentGatewayTransactionId;
    private String orderId;
    private boolean paymentGatewaySuccess;
    private boolean integrationGatewaySuccess;
    private double amount;

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public void setPaymentTransactionId(String paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public String getPaymentGatewayTransactionId() {
        return paymentGatewayTransactionId;
    }

    public void setPaymentGatewayTransactionId(String paymentGatewayTransactionId) {
        this.paymentGatewayTransactionId = paymentGatewayTransactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean isPaymentGatewaySuccess() {
        return paymentGatewaySuccess;
    }

    public void setPaymentGatewaySuccess(boolean paymentGatewaySuccess) {
        this.paymentGatewaySuccess = paymentGatewaySuccess;
    }

    public boolean isIntegrationGatewaySuccess() {
        return integrationGatewaySuccess;
    }

    public void setIntegrationGatewaySuccess(boolean integrationGatewaySuccess) {
        this.integrationGatewaySuccess = integrationGatewaySuccess;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
