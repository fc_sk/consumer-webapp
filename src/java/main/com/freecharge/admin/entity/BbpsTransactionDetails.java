package com.freecharge.admin.entity;

/**
 * @author abhinavdixit
 * 
 */
public class BbpsTransactionDetails {
	
	private String bbpsReferenceId;
	private String orderId;
	private Boolean isBbpsActive;
	
	
	
	public String getBbpsReferenceId() {
		return bbpsReferenceId;
	}

	public void setBbpsReferenceId(String bbpsReferenceId) {
		this.bbpsReferenceId = bbpsReferenceId;
	}

	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public boolean isBbpsActive() {
		return isBbpsActive;
	}

	public void setBbpsActive(boolean isBbpsActive) {
		this.isBbpsActive = isBbpsActive;
	}

	@Override
	public String toString() {
		return "BbpsTransactionDetails [orderId=" + orderId + ", isBbpsActive=" + isBbpsActive + ", bbpsReferenceId="
				+ bbpsReferenceId + "]";
	}

	
	
}
