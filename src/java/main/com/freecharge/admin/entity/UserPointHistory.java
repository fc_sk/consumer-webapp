package com.freecharge.admin.entity;

import java.util.Date;

public class UserPointHistory {
	
	private String userId;
	private Integer points;
	private String createdOn;
	private String updatedOn;
	private String type;
	private String referenceId;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	@Override
	public String toString() {
		return "UserPointHistory [userId=" + userId + ", points=" + points + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + ", type=" + type + ", referenceId=" + referenceId + "]";
	}
	
	
}
