package com.freecharge.admin.entity;

public class DataUserReward {
	
	String data;
	String amount;
	String orderId;
	Byte status;
	long createdOn;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Byte getStatus() {
		return status;
	}
	public void setStatus(Byte status) {
		this.status = status;
	}
	public long getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}
	
	@Override
	public String toString() {
		return "DataUserReward [data=" + data + ", amount=" + amount + ", orderId=" + orderId + ", status=" + status
				+ ", createdOn=" + createdOn + "]";
	}
	
	
}
