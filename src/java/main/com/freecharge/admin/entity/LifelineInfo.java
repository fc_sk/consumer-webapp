package com.freecharge.admin.entity;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class LifelineInfo {

	private List<UserPointHistory> pointHistoryList;
	
	@SerializedName("mobileInformation")
	private UserMobileInformation userMobileInformation;
	
	@SerializedName("dataUserReward")
	private List<DataUserReward> dataUserRewardList;
	
	public List<UserPointHistory> getPointHistoryList() {
		return pointHistoryList;
	}
	public void setPointHistoryList(List<UserPointHistory> pointHistoryList) {
		this.pointHistoryList = pointHistoryList;
	}
	public UserMobileInformation getUserMobileInformation() {
		return userMobileInformation;
	}
	public void setUserMobileInformation(UserMobileInformation userMobileInformation) {
		this.userMobileInformation = userMobileInformation;
	}
	public List<DataUserReward> getDataUserRewardList() {
		return dataUserRewardList;
	}
	public void setDataUserRewardList(List<DataUserReward> dataUserRewardList) {
		this.dataUserRewardList = dataUserRewardList;
	}
	@Override
	public String toString() {
		return "LifelineInfo [pointHistoryList=" + pointHistoryList + ", userMobileInformation=" + userMobileInformation
				+ ", dataUserRewardList=" + dataUserRewardList + "]";
	}
}
