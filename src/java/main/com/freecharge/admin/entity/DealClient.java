package com.freecharge.admin.entity;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 4/14/14
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */

public class DealClient {
    private Integer id;
    private Long dealId;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
