package com.freecharge.admin.entity;

import java.io.Serializable;
import java.util.Map;

import org.hibernate.validator.constraints.NotBlank;

import com.mongodb.DB;
import com.mongodb.DBObject;

public class AdminUserEntity implements Serializable {
	static final long serialVersionUID=827972192180465333L; 
	
	@NotBlank
	private DBObject user;
	
	public AdminUserEntity(DBObject user){
		this.user=user;
	}
	public void setUser(DBObject user){
		this.user=user;
	}
	public DBObject getUser(){
		return this.user;
	}
	public String getId(){
		return user.get("email").toString();
	}
}
