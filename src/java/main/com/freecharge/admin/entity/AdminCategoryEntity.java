package com.freecharge.admin.entity;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import com.mongodb.DBObject;

public class AdminCategoryEntity implements Serializable {
	static final long serialVersionUID=827972193181455333L; 

	@NotBlank
	private DBObject category;
	
	public AdminCategoryEntity(DBObject category){
		this.category=category;
	}
	public void setCategory(DBObject category){
		this.category=category;
	}
	public DBObject getCategory(){
		return this.category;
	}
	public String getId(){
		return category.get("categoryName").toString();
	}
}
