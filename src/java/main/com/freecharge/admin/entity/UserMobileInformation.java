package com.freecharge.admin.entity;

public class UserMobileInformation {
	
	private String mobile;
	private String operatorId;
	private String circleId;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getCircleId() {
		return circleId;
	}

	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}
	
	@Override
	public String toString() {
		return "UserMobileInformation [mobile=" + mobile + ", operatorId=" + operatorId + ", circleId=" + circleId
				+ "]";
	}
}
