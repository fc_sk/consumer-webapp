package com.freecharge.admin.entity;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import com.mongodb.DBObject;

public class AdminComponentEntity implements Serializable {

	static final long serialVersionUID=827962192184463323L;

	@NotBlank
	private DBObject component;

	public AdminComponentEntity(DBObject component){
		this.component=component;
	}
	public void setComponent(DBObject component){
		this.component=component;
	}
	public DBObject getComponent(){
		return this.component;
	}
	public String getId(){
		return component.get("name").toString();
	}
}
