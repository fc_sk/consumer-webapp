package com.freecharge.admin.model;

public class CTActionsControlData {
    private String emailId;
    private String maxCredits;
    private String viewLimits;
    private String availableCredits;
    private String availableViewLimits;
    private String createdAt;

    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }
    public String getMaxCredits() {
        return maxCredits;
    }
    public void setMaxCredits(final String maxCredits) {
        this.maxCredits = maxCredits;
    }
    public String getViewLimits() {
        return viewLimits;
    }
    public void setViewLimits(final String viewLimits) {
        this.viewLimits = viewLimits;
    }
    public String getAvailableCredits() {
        return availableCredits;
    }
    public void setAvailableCredits(final String availableCredits) {
        this.availableCredits = availableCredits;
    }
    public String getAvailableViewLimits() {
        return availableViewLimits;
    }
    public void setAvailableViewLimits(final String availableViewLimits) {
        this.availableViewLimits = availableViewLimits;
    }
    public String getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(final String createdAt) {
        this.createdAt = createdAt;
    }
}
