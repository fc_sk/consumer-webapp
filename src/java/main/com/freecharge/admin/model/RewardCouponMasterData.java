package com.freecharge.admin.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.freefund.services.FreefundService;

@Service
public class RewardCouponMasterData {
	@Autowired
    private FreefundService freefundService;
	
	public List<FreefundClass> getAllRewardCouponCampaign() {
		List<FreefundClass> freefundClassList = freefundService.getFreefundClasses();
		return freefundClassList;
	}	
}
