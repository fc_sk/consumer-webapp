package com.freecharge.admin.model;

import java.sql.Timestamp;

public class ProfileNumberTransaction {
    private String    profileNumber;
    private String    orderId;
    private Integer   userId;
    private String    emailId;
    private String    serviceNumber;
    private Timestamp createdOn;
    private String    rechargeAmount;
    private String    rechargeStatus;
    private String    serviceProvider;
    private String    promocode;
    private String    serviceRegion;
    private String    productName;
    private String    paymentStatus;
    private String    paymentGateway;
    private String    aggregator;

    public String getAggregator() {
        return aggregator;
    }

    public void setAggregator(final String aggregator) {
        this.aggregator = aggregator;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(final String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(final String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getProfileNumber() {
        return profileNumber;
    }

    public void setProfileNumber(final String profileNumber) {
        this.profileNumber = profileNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(final Integer userId) {
        this.userId = userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(final Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(final String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getRechargeStatus() {
        return rechargeStatus;
    }

    public void setRechargeStatus(final String rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(final String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(final String promocode) {
        this.promocode = promocode;
    }

    public String getServiceRegion() {
        return serviceRegion;
    }

    public void setServiceRegion(final String serviceRegion) {
        this.serviceRegion = serviceRegion;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(final String productName) {
        this.productName = productName;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(final String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }
}
