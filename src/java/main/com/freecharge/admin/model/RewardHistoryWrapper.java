package com.freecharge.admin.model;

import java.util.List;

import com.freecharge.campaign.web.RewardHistory;

public class RewardHistoryWrapper {

	private List<RewardHistory> campaignHistoryList;
	private String status;
	
	public List<RewardHistory> getCampaignHistoryList() {
		return campaignHistoryList;
	}
	public void setCampaignHistoryList(List<RewardHistory> campaignHistoryList) {
		this.campaignHistoryList = campaignHistoryList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "RewardHistoryWrapper [campaignHistoryList=" + campaignHistoryList + ", status=" + status + "]";
	}
}
