package com.freecharge.admin.model;

public class RewardCouponForm {
	private String emailId;
	private RewardCouponMasterData rewardCouponMasterData;
	private Integer campaignId;
	
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public RewardCouponMasterData getRewardCouponMasterData() {
		return rewardCouponMasterData;
	}
	public void setRewardCouponMasterData(
			RewardCouponMasterData rewardCouponMasterData) {
		this.rewardCouponMasterData = rewardCouponMasterData;
	}
}
