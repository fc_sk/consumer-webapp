package com.freecharge.admin.model;

import java.util.List;

import com.freecharge.api.coupon.service.model.CityMaster;
import com.freecharge.app.domain.entity.StateMaster;

public class AdminAddressChange {
	private String email;
	private String area;
	private String address;
	private String landmar;
	private String pincode;
	private Integer state;
	private Integer city;
	private List<StateMaster> statelist;
	private List<CityMaster> citylist;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLandmar() {
		return landmar;
	}
	public void setLandmar(String landmar) {
		this.landmar = landmar;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getCity() {
		return city;
	}
	public void setCity(Integer city) {
		this.city = city;
	}
	public List<StateMaster> getStatelist() {
		return statelist;
	}
	public void setStatelist(List<StateMaster> statelist) {
		this.statelist = statelist;
	}
	public List<CityMaster> getCitylist() {
		return citylist;
	}
	public void setCitylist(List<CityMaster> citylist) {
		this.citylist = citylist;
	}	
}
