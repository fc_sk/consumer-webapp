package com.freecharge.admin.model;

import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;

public class FulfilmentShippingAddress {
	private Integer fulfilmentId;
	private String orderId;
	private String serviceNumber;
	private String title;
	private String firstName;
	private String email;
	private String area;
	private String address;
	private String landmar;
	private String pincode;
	private Integer state;
	private Integer city;
	private List<StateMaster> statelist;
	private List<CityMaster> citylist;
	private Integer country;
	private Integer fkUserProfileId;

	public Integer getFulfilmentId() {
		return fulfilmentId;
	}

	public void setFulfilmentId(Integer fulfilmentId) {
		this.fulfilmentId = fulfilmentId;
	}

	public Integer getFkUserProfileId() {
		return fkUserProfileId;
	}

	public void setFkUserProfileId(Integer fkUserProfileId) {
		this.fkUserProfileId = fkUserProfileId;
	}

	public Integer getCountry() {
		return country;
	}

	public void setCountry(Integer country) {
		this.country = country;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandmar() {
		return landmar;
	}

	public void setLandmar(String landmar) {
		this.landmar = landmar;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public List<StateMaster> getStatelist() {
		return statelist;
	}

	public void setStatelist(List<StateMaster> statelist) {
		this.statelist = statelist;
	}

	public List<CityMaster> getCitylist() {
		return citylist;
	}

	public void setCitylist(List<CityMaster> citylist) {
		this.citylist = citylist;
	}
}
