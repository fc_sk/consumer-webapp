package com.freecharge.admin.model;

import java.io.Serializable;
import java.util.Map;

public class FraudCaseHandlerData implements Serializable {
    private String              orderId;
    private String              merchantTxnId;
    private Integer             paymentTxnId;
    private String              paymentGateway;
    private String              service;
    private String              customerName;
    private String              email;
    private String              profileNumber;
    private String              address;
    private String              ipAddress;
    private String              refundedAmount;
    private String              refundDate;
    private String              banEmailId;
    private String              banIPAddress;
    private String              refundedOrderIDs;
    private Map<String, String> refundedOrderIdAmountMap;

    public Map<String, String> getRefundedOrderIdAmountMap() {
        return refundedOrderIdAmountMap;
    }

    public void setRefundedOrderIdAmountMap(Map<String, String> refundedOrderIdAmountMap) {
        this.refundedOrderIdAmountMap = refundedOrderIdAmountMap;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMerchantTxnId() {
        return merchantTxnId;
    }

    public void setMerchantTxnId(String merchantTxnId) {
        this.merchantTxnId = merchantTxnId;
    }

    public Integer getPaymentTxnId() {
        return paymentTxnId;
    }

    public void setPaymentTxnId(Integer paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileNumber() {
        return profileNumber;
    }

    public void setProfileNumber(String profileNumber) {
        this.profileNumber = profileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(String refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(String refundDate) {
        this.refundDate = refundDate;
    }

    public String getBanEmailId() {
        return banEmailId;
    }

    public void setBanEmailId(String banEmailId) {
        this.banEmailId = banEmailId;
    }

    public String getBanIPAddress() {
        return banIPAddress;
    }

    public void setBanIPAddress(String banIPAddress) {
        this.banIPAddress = banIPAddress;
    }

    public String getRefundedOrderIDs() {
        return refundedOrderIDs;
    }

    public void setRefundedOrderIDs(String refundedOrderIDs) {
        this.refundedOrderIDs = refundedOrderIDs;
    }
}
