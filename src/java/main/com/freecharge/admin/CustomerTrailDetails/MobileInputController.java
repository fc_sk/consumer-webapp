package com.freecharge.admin.CustomerTrailDetails;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.service.UPITransactionService;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OrderFulfillmentDetails;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.TransactionFulfilment;
import com.freecharge.app.domain.entity.jdbc.TransactionHomePage;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/mobile_input_transaction")
public class MobileInputController extends BaseAdminController {
	public static final String CUSTOMER_DETAIL_INPUT_MOBILE = "/admin/mobile_input_transaction.htm";
	public static final String MOBILE_INPUT_LINK = "/admin/mobile_input_transaction/do.htm";
	private static Logger logger = LoggingFactory.getLogger(MobileInputController.class);
		
	@Autowired
	private EasyDBWriteDAO easyDBWriteDAO;
	
	@Autowired
    private InService inService;
	
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private OrderIdWriteDAO orderIdDAO;
	
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
	@Autowired
    private TxnHomePageDAO txnHomePageDAO;
	
	@Autowired
	private CartItemsDAO cartItemsDAO;
	
	@Autowired
	private BillPaymentService        billPaymentService;
	
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    @Autowired
    private UPITransactionService upiTransactionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView adminhome(@RequestParam Map<String,String> mapping, @ModelAttribute("command") MobileForm mobileForm, BindingResult bindingResult) {
		return new ModelAndView(ViewConstants.MOBILE_INPUT_TRANSACTION_VIEW, "command", mobileForm);
	}
	
	@RequestMapping(value = "do")
	public String getCustomerDetails(@RequestParam Map<String, String> params, Model model,@ModelAttribute("command") MobileForm mobileForm, BindingResult bindingResult, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
		MobileFormValidator formValidator = new MobileFormValidator();
		formValidator.validate(mobileForm, bindingResult);
		
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}
		
		if (bindingResult.hasErrors()) {
		    model.addAttribute("exception", "Mobile number required.");
		    return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		List<String> messages = new LinkedList<String>();
		List<String> listMessages = new LinkedList<String>();
		model.addAttribute("listmessages", listMessages); 
		
		List<CustomerCare> customerCareList =new ArrayList<CustomerCare>();
		String serviceNumber = params.get("mobile").trim();
		List<TxnHomePage> transactionHomePages = txnHomePageDAO.findByServiceNumber(serviceNumber);
		
		if (transactionHomePages.size() == 0) {
			listMessages.add(String.format(
					"No Data found for mobile number %s or put valid mobile number.",
					serviceNumber));
			model.addAttribute("messages", listMessages);
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		List<String> lookups = new ArrayList<String>();
		for (TxnHomePage txnHomePage : transactionHomePages) {
			lookups.add(txnHomePage.getLookupId());
		}
		
		List<String> orderIdList = new ArrayList<String>();
		for (String lookup_id : lookups) {
			orderIdList.add(orderIdSlaveDAO.getOrderIdForLookUpId(lookup_id));
		}
		
		LinkedHashSet<String> orderlist = new LinkedHashSet<String>(orderIdList);
		orderIdList = new ArrayList<String>(orderlist);
		Collections.reverse(orderIdList);
	
		for (String orderid : orderIdList) {
			String paymentStatus = null;
			String orderDate = null;
			Timestamp orderdate = null;
			String totalAmount = null;
			String shippingAddress = "";
			String rechargeStatus = null;
			String success1 = "00";
			String success2 = "0";
			String underProcessRecharge = "08";
			String pendingRecharge = "-1";
			String couponsStatus = null;
			String dispatchDate = null;
			String trackingNumber = null;
			String pVouchers = "";
			String eVouchers = "";
			String eCoupons = "";
			String pCoupons = "";
			String rechargeAmount = null;
			String pgName = null;
			String agName = null;
			String promocode = null;
			String productType = null;
			
			if (orderid != null) {
				//getting orderDate from OrderID table
				OrderId orderId = new OrderId();
				orderId.setOrderId(orderid);
				List<OrderId> orderIds = easyDBWriteDAO.get(orderId);
				orderId = FCUtil.getFirstElement(orderIds);
				orderdate = orderId.getCreatedOn();
				orderDate = orderdate.toString();
			
				//PaymentStatus,Recharges amount from PaymentTransaction Table 
				Boolean success = paymentTransactionService.isPaymentTransactionSuccessful(orderid);	
				double amount = 0.0;
				
				if (success) {
					paymentStatus = "Payment Successfull";
				} else {
					paymentStatus = "Payment UnSuccessfull";
				}
		
				//Total Amount from Payment transaction table.
				PaymentTransaction paymentTransaction = paymentTransactionService.getLastPaymentTransactionSentToPG(orderId.getOrderId());
				model.addAttribute("paymentTransaction", paymentTransaction);
				
				List<PaymentTransaction> allPaymentTransaction = paymentTransactionService.getAllPaymentTransactionSentToPG(orderId.getOrderId());
	            if (paymentTransaction != null) {
	            model.addAttribute("allPaymentTransaction", allPaymentTransaction);
	            pgName = paymentTransaction.getPaymentGateway();
	            }
				
				if (paymentTransaction != null) {
					amount = paymentTransaction.getAmount();
				} else {
					amount = 0.0;
				}
				totalAmount = Double.toString(amount);
			
				//SippingAddress from TxnFulfilment Table  		
				TransactionHomePage transactionHomePage3 = new TransactionHomePage();
				
				transactionHomePage3.setLookupId(orderId.getLookupId());
				
				List<TransactionHomePage> txnHomePageList = easyDBWriteDAO.get(transactionHomePage3);
			
				if (txnHomePageList.size() > 1) {
					logger.info("More than one transaction home page details found for order id"+orderId+", contact the dev team immediately");
				}
			
				transactionHomePage3 = FCUtil.getFirstElement(txnHomePageList);

				if (transactionHomePage3 !=null) {
					TransactionFulfilment transactionFulfilment = new TransactionFulfilment();
					transactionFulfilment.setTxnHomePageId(transactionHomePage3.getId());
					List<TransactionFulfilment> transactionFulfilments = easyDBWriteDAO.get(transactionFulfilment);
					transactionFulfilment = FCUtil.getFirstElement(transactionFulfilments);

					if (transactionFulfilments.size() > 1) {
						logger.info("More than one transaction fulfilment details found for order id"+orderId+", contact the dev team immediately");
					}
				
					if (transactionFulfilment != null) {
						shippingAddress+= transactionFulfilment.getDeliveryName() +" : "+" "+ transactionFulfilment.getDeliveryAdd()+"," +" "+ transactionFulfilment.getDeliveryArea()+","+" "+ transactionFulfilment.getDeliveryLandmark()+","+" "+ transactionFulfilment.getAddressMobile()+".";
					} else {
						shippingAddress = "Not Available";
					}
					productType = transactionHomePage3.getProductType();
					if(productType != null) {
						productType = ProductName.fromCode(productType).name();
					}
				} else {
					logger.error("TransactionHomePage row is not available for orderid" + orderid);
					shippingAddress = "NotAvailable";
					productType = "NotAvailable";
				}
				
				 /* below code change to handle retry cases*/ 
                
                UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId.getOrderId());
                BillPaymentStatus billPaymentStatus = null;
                if(userTransactionHistory!=null){
                	/* If the transaction is billpayment */
                	if(userTransactionHistory.getProductType().equals(ProductName.MobilePostpaid.getProductType())){
                      	 billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId.getOrderId());
                      	 
                         if (billPaymentStatus != null) {
                             if (billPaymentStatus.getSuccess() == null) {
                                 rechargeStatus = "Billpayment recharge status pending";
                             } else if (billPaymentStatus.getSuccess()) {
                                 rechargeStatus = "Billpayment recharge successful";
                             } else {
                                 rechargeStatus = "Billpayment recharge unsuccessful";
                             } 
                             agName = billPaymentStatus.getBillPaymentGateway();
                         }
                }else{
                	 // Rechargestatus from InResponse Table
                    InResponse inResponse = new InResponse();
                    String responseCode = null;
                    InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId.getOrderId());
                    //NullPointerException
                   /* if (inTransactionData != null) {
                        //model.addAttribute("inTransactionData", inTransactionData);
                        agName = inTransactionData.getInResponse().getAggrName();
                    }*/
                    if (inTransactionData != null) {
                        inResponse = inTransactionData.getInResponse();
                        if (inResponse != null) {
                        	agName = inTransactionData.getInResponse().getAggrName();
                            responseCode = inResponse.getInRespCode();
                        } else {
                            responseCode = "NA";
                            logger.error("InResponse table row is not available for order id :" + orderid);
                        }
                    } else {
                        responseCode = "NA";
                        logger.error("InTransaction table row is not available for order id :" + orderid);
                    }

                    if (responseCode.equals(success1) || responseCode.equals(success2)) {
                        rechargeStatus = "Recharge Successfull";
                    } else if ("NA".equals(responseCode)) {
                        rechargeStatus = " Not Aavailable";
                    } else if (responseCode.equals(underProcessRecharge)) {
                        rechargeStatus = "Transaction Under Process";
                    } else if (responseCode.equals(pendingRecharge)) {
                        rechargeStatus = "Pending Transaction";
                    } else {
                        rechargeStatus = "Recharge UnSuccessfull";
                    }
                }
              }
								
				//couponsStatus, dispatchDate, trackingNumber from orderFulfillmentDetails.
				OrderFulfillmentDetails orderFulfillmentDetails = new OrderFulfillmentDetails();
				orderFulfillmentDetails.setOrderId(orderid);
				List<OrderFulfillmentDetails> orderFulfillmentDetailsList = easyDBWriteDAO.get(orderFulfillmentDetails);
				orderFulfillmentDetails = FCUtil.getFirstElement(orderFulfillmentDetailsList);		
							
				if (orderFulfillmentDetails != null) {
					couponsStatus = orderFulfillmentDetails.getFulfillmentStatus();
					Integer date =  orderFulfillmentDetails.getDateDelivered();
					dispatchDate =  date.toString();
					trackingNumber = orderFulfillmentDetails.getAwbNum();
				} else {
					couponsStatus = "Not Available";
					dispatchDate = "Not Available"; 
					trackingNumber = "Not Available"; 
				}
		
				//getting Vouchers using TransactionHomePage->cartBusinessDO->cartItemsBusinessDO->CouponStore
				TransactionHomePage transactionHomePage2 = new TransactionHomePage();
				transactionHomePage2.setLookupId(orderId.getLookupId());
				List<TransactionHomePage> txnHomePages = easyDBWriteDAO.get(transactionHomePage2);
			
				if (txnHomePages.size() > 1) {
					logger.info("More than one transaction home page details found for order id"+orderId+", contact the dev team immediately");
				}
				
				transactionHomePage2 = FCUtil.getFirstElement(txnHomePages);
				if (transactionHomePage2 != null) {
					CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(transactionHomePage2.getLookupId());
					Long cartid = cartBusinessDo.getId();
					int cart_id = cartid.intValue();
					Integer cartId = new Integer(cart_id);
		          
					if (cartBusinessDo != null) {
						List<CouponStore> couponStores = couponService.getAllCouponStores(cartBusinessDo);
						for (CouponStore couponStore : couponStores) {
							if (couponStore != null) {
								if (FCConstants.COUPON_TYPE_P.equals(couponStore.getCouponType())) {
									pVouchers += couponStore.getCampaignName() + "-" + couponStore.getCouponValue() + "(" + cartItemsDAO.getCouponQuantity(couponStore.getCouponStoreId(), cartId) + ")" + "  " + " " ;
								} else if (FCConstants.COUPON_TYPE_E.equals(couponStore.getCouponType())) {
									eVouchers += couponStore.getCampaignName() + "-" + couponStore.getCouponValue() + "(" + cartItemsDAO.getCouponQuantity(couponStore.getCouponStoreId(), cartId) + ")" + "  " + " " ;
								} 
								else if (FCConstants.COUPON_TYPE_C.equals(couponStore.getCouponType())) {
									logger.info("Crosssell");	
								} 
							}	
						}						
					} else {
						logger.error(cartBusinessDo+"cartBusinessDO row is empty");
						pVouchers = "Not Available";
						eVouchers = "Not Available";
					}		
					
					//Recharge Amount getting from Transaction Home Page.
					Float rechargeValue = transactionHomePage2.getAmount();
					rechargeAmount = Float.toString(rechargeValue);	
				} else {
					logger.error(transactionHomePage2+"txn_home_page row is empty");
					pVouchers = "Not Available";
					eVouchers = "Not AVailable";
					rechargeAmount = "Not Available";	
				}
			} else {
				logger.error(orderid+" : Order id is not avilable in order_id table");
				continue;
			}
			customerCareList.add(new CustomerCare(orderid, orderDate, rechargeAmount, totalAmount, shippingAddress, paymentStatus, rechargeStatus, pVouchers, eVouchers, couponsStatus, dispatchDate, trackingNumber, pgName, agName, promocode, productType));	
		}
		model.addAttribute("upiTransactionsList", upiTransactionService.getUPITransactionsViewDataByMobileNo(serviceNumber));
		model.addAttribute("customerCareList", customerCareList);
		model.addAttribute("mobilenumberentered", serviceNumber);
		model.addAttribute("messages", messages); 
		return ViewConstants.MOBILE_INPUT_RESULT;
	}
}
