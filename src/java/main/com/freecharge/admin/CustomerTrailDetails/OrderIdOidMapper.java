package com.freecharge.admin.CustomerTrailDetails;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderIdOidMapper implements RowMapper<String> {
	@Override
	public String mapRow(ResultSet rs, int rownum) throws SQLException {
		return rs.getString("order_id");
	}

}
