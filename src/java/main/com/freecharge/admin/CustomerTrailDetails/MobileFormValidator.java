package com.freecharge.admin.CustomerTrailDetails;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

public class MobileFormValidator/* implements Validator */{
	 
	/*@Override*/
	public boolean supports(Class<?> aClass) {
		return MobileForm.class.equals(aClass);
	}

	/*@Override*/
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "Mobile Number field cannot be blank.", "Mobile Number field cannot be blank.");
	}
	
}
