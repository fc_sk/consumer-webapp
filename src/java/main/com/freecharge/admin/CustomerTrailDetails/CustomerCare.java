package com.freecharge.admin.CustomerTrailDetails;


public class CustomerCare {
	String orderid;
	String orderDate;
	String rechargeAmount;
	String totalAmount;
	String shippingAddress;
	String paymentStatus;
	String rechargeStatus;
	String pVouchers;
	String eVouchers;
	String couponsStatus;
	String dispatchDate;
	String trackingNumber;
	String pgName;
	String agName;
	String promocode;
	String productType;
	
	public CustomerCare(String orderid, String orderDate,
			String rechargeAmount, String totalAmount, String shippingAddress,
			String paymentStatus, String rechargeStatus, String pVouchers,
			String eVouchers, String couponsStatus, String dispatchDate,
			String trackingNumber, String pgName, String agName, String promocode, String productType) {
		this.orderid = orderid;
		this.orderDate = orderDate;
		this.rechargeAmount = rechargeAmount;
		this.totalAmount = totalAmount;
		this.shippingAddress = shippingAddress;
		this.paymentStatus = paymentStatus;
		this.rechargeStatus = rechargeStatus;
		this.pVouchers = pVouchers;
		this.eVouchers = eVouchers;
		this.couponsStatus = couponsStatus;
		this.dispatchDate = dispatchDate;
		this.trackingNumber = trackingNumber;
		this.pgName = pgName;
		this.agName = agName;
		this.promocode = promocode;
		this.productType = productType;
	}
	
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getRechargeAmount() {
		return rechargeAmount;
	}
	public void setRechargeAmount(String rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getRechargeStatus() {
		return rechargeStatus;
	}
	public void setRechargeStatus(String rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}
	public String getpVouchers() {
		return pVouchers;
	}
	public void setpVouchers(String pVouchers) {
		this.pVouchers = pVouchers;
	}
	public String geteVouchers() {
		return eVouchers;
	}
	public void seteVouchers(String eVouchers) {
		this.eVouchers = eVouchers;
	}
	public String getCouponsStatus() {
		return couponsStatus;
	}
	public void setCouponsStatus(String couponsStatus) {
		this.couponsStatus = couponsStatus;
	}
	public String getDispatchDate() {
		return dispatchDate;
	}
	public void setDispatchDate(String dispatchDate) {
		this.dispatchDate = dispatchDate;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getpgName() {
		return pgName;
	}
	public void setpgName(String pgName) {
		this.pgName = pgName;
	}
	public String getagName() {
		return agName;
	}
	public void setagName(String agName) {
		this.agName = agName;
	}
	public String getPromocode() {
        return promocode;
    }
    public void setPromocode(String agName) {
        this.promocode = promocode;
    }

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
}