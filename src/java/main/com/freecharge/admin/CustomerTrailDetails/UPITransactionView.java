package com.freecharge.admin.CustomerTrailDetails;

import java.math.BigDecimal;
import java.util.Date;

import com.snapdeal.fcpt.txnhistoryview.commons.enums.BusinessUseCase;
import com.snapdeal.fcpt.txnhistoryview.commons.enums.TxnStatus;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.UPIInfo;

public class UPITransactionView {

	private Date transactionDate;
	private BigDecimal txnAmount;
	private TxnStatus txnStatus;
	private BusinessUseCase businessUseCase;
	private String globalTxnId;
	private UPIInfo upiInfo;
	private String globalTxnType;
	private String mobileNumber;
	private String merchantOrderId;
	private String upiNpciErrorMessage;
	private String promoCode;

	public UPITransactionView(UPITransactionViewBuilder builder) {
		this.transactionDate = builder.transactionDate;
		this.txnAmount = builder.txnAmount;
		this.txnStatus = builder.txnStatus;
		this.businessUseCase = builder.businessUseCase;
		this.globalTxnId = builder.globalTxnId;
		this.upiInfo = builder.upiInfo;
		this.globalTxnType = builder.globalTxnType;
		this.mobileNumber = builder.mobileNumber;
		this.merchantOrderId = builder.merchantOrderId;
		this.upiNpciErrorMessage = builder.upiNpciErrorMessage;
		this.promoCode = builder.promoCode;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public TxnStatus getTxnStatus() {
		return txnStatus;
	}

	public BusinessUseCase getBusinessUseCase() {
		return businessUseCase;
	}

	public String getGlobalTxnId() {
		return globalTxnId;
	}

	public UPIInfo getUpiInfo() {
		return upiInfo;
	}

	public String getGlobalTxnType() {
		return globalTxnType;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public String getUpiNpciErrorMessage() {
		return upiNpciErrorMessage;
	}
	
	public String getPromoCode() {
		return promoCode;
	}

	public static class UPITransactionViewBuilder {

		private Date transactionDate;
		private BigDecimal txnAmount;
		private TxnStatus txnStatus;
		private BusinessUseCase businessUseCase;
		private String globalTxnId;
		private UPIInfo upiInfo;
		private String globalTxnType;
		private String mobileNumber;
		private String merchantOrderId;
		private String upiNpciErrorMessage;
		private String promoCode;

		public UPITransactionViewBuilder() {
		}

		public UPITransactionViewBuilder setTransactionDate(Date transactionDate) {
			this.transactionDate = transactionDate;
			return this;
		}

		public UPITransactionViewBuilder setTxnAmount(BigDecimal txnAmount) {
			this.txnAmount = txnAmount;
			return this;
		}

		public UPITransactionViewBuilder setTxnStatus(TxnStatus txnStatus) {
			this.txnStatus = txnStatus;
			return this;
		}

		public UPITransactionViewBuilder setBusinessUseCase(BusinessUseCase businessUseCase) {
			this.businessUseCase = businessUseCase;
			return this;
		}

		public UPITransactionViewBuilder setGlobalTxnId(String globalTxnId) {
			this.globalTxnId = globalTxnId;
			return this;
		}

		public UPITransactionViewBuilder setUpiInfo(UPIInfo upiInfo) {
			this.upiInfo = upiInfo;
			return this;
		}

		public UPITransactionViewBuilder setGlobalTxnType(String globalTxnType) {
			this.globalTxnType = globalTxnType;
			return this;
		}

		public UPITransactionViewBuilder setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
			return this;
		}

		public UPITransactionViewBuilder setMerchantOrderId(String merchantOrderId) {
			this.merchantOrderId = merchantOrderId;
			return this;
		}

		public UPITransactionViewBuilder setUpiNpciErrorMessage(String upiNpciErrorMessage) {
			this.upiNpciErrorMessage = upiNpciErrorMessage;
			return this;
		}
		
		public UPITransactionViewBuilder setPromoCode(String promoCode) {
			this.promoCode = promoCode;
			return this;
		}

		public UPITransactionView build() {
			return new UPITransactionView(this);
		}
	}
}
