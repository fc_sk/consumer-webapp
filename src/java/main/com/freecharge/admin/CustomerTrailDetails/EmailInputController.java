package com.freecharge.admin.CustomerTrailDetails;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.sdmoney.service.model.GetSDMoneyAccountResponse;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.service.UPITransactionService;
import com.freecharge.adminpage.userban.EmailForm;
import com.freecharge.adminpage.userban.EmailFormValidator;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OrderFulfillmentDetails;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.TransactionFulfilment;
import com.freecharge.app.domain.entity.jdbc.TransactionHomePage;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.WalletTransaction;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/email_input_transaction")
public class EmailInputController extends BaseAdminController {
	public static final String CUSTOMER_DETAIL_INPUT_EMAIL = "/admin/email_input_transaction.htm";
	public static final String CUSTOMER_TRAIL_LINK = "/admin/email_input_transaction/do.htm";
	private static Logger logger = LoggingFactory.getLogger(EmailInputController.class);

	@Autowired
	private FulfillmentService fulfillmentService;
	
	@Autowired
	private EasyDBWriteDAO easyDBWriteDAO;
	
	@Autowired
    private InService inService;
	
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
    FreefundService freefundService;
	
	@Autowired
    private TxnHomePageDAO txnHomePageDAO;
	@Autowired
	private OrderIdWriteDAO orderIdDAO;
	
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
	@Autowired
	private CartItemsDAO cartItemsDAO;
	
	@Autowired
	UserWriteDao userWriteDao;
	
	@Autowired
	private WalletService walletService;

    @Autowired
    private BillPaymentService        billPaymentService;
    
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO; 
    
	public static final String EMAIL_INPUT_PARAM_NAME = "email";

    @Autowired
    private OrderIdReadDAO orderIdReadDAO;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private WalletWrapper walletWrapper;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private AdminHelper adminHelper;
    
    @Autowired
    private UPITransactionService upiTransactionService;
    
    @RequestMapping(method = RequestMethod.GET)
	public ModelAndView adminhome(@RequestParam Map<String,String> mapping, @ModelAttribute("command") EmailForm userBanForm, BindingResult bindingResult) {
		return new ModelAndView(ViewConstants.EMAIL_INPUT_TRANSACTION_VIEW, "command", userBanForm);
	}

	@RequestMapping(value = "wallettransaction")
	public String walletTransactionDetails(
			@RequestParam Map<String, String> mapping,
			@ModelAttribute("command") EmailForm userBanForm,
			BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}
		
		List<String> listMessages = new LinkedList<String>();
		EmailFormValidator formValidator = new EmailFormValidator();
		formValidator.validate(userBanForm, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return ViewConstants.EMAIL_INPUT_TRANSACTION_VIEW;
		}
		String emailIdParamValue = mapping.get(EMAIL_INPUT_PARAM_NAME).trim();
		if (emailIdParamValue != null && !emailIdParamValue.isEmpty()
				&& emailIdParamValue.contains("@")) {
			Users users = userServiceProxy.getUserByEmailId(emailIdParamValue);
			if (users != null) {
				int userID = users.getUserId();
				Amount balance = walletWrapper.getTotalBalance(userID);
				Map<String, Object> lastEntry = walletService
						.findLastEntry(userID);
				List<Map<String, Object>> rows = walletService.getBalanceRows(
						userID, lastEntry,false);
				model.addAttribute("balance", balance.getAmount());
				model.addAttribute("lastEntry", lastEntry);
				model.addAttribute("rows", rows);
				return ViewConstants.WALLET_EMAIL_INPUT_RESULT;
			} else {
				listMessages
						.add(String
								.format("No Wallet History found for Email Id %s or put valid Email Id ",
										emailIdParamValue));
				model.addAttribute("listmessages", listMessages);
				return ViewConstants.EMAIL_INPUT_TRANSACTION_VIEW;
			}
		} else {
			listMessages.add(String
					.format("Please Enter correct Email Address"));
			model.addAttribute("listmessages", listMessages);
			return ViewConstants.EMAIL_INPUT_TRANSACTION_VIEW;
		}
	}
	
    @RequestMapping(value = "do")
    public String getCustomerDetails(@RequestParam Map<String, String> params, Model model,
            @ModelAttribute("command") EmailForm userBanForm, BindingResult bindingResult, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
    	if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
            return get403HtmlResponse(response);
        }
    	
    	EmailFormValidator formValidator = new EmailFormValidator();
        formValidator.validate(userBanForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("exception", "Email id is required.");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }

		if(!FCUtil.isValidSearchEmailFormat(userBanForm.getEmail()))
		{
			model.addAttribute("exception", "Email id is not valid.");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}


		List<String> messages = new LinkedList<String>();
        List<String> listMessages = new LinkedList<String>();
        model.addAttribute("listmessages", listMessages);

        String email = params.get("email").trim();
        /*FRCH to SD user Migration status*/
        if (!FCUtil.isEmpty(email)) {
        	MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(email, null);
        	if (migrationStatus != null) {
        		String userMigrationStatus =  migrationStatus.getMigrationStatus();
        		model.addAttribute("fcSdUserMigration", userMigrationStatus + "("+adminHelper.getStatusMsgForUserMigration(userMigrationStatus)+")");
                model.addAttribute("userMigrationToolTipText", adminHelper.getToolTipMsgForUserMigration(userMigrationStatus));
        	}
        }
		if (!FCUtil.isEmpty(email)) {
            try {
                String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
                if (null != sdIdentity) {
                    model.addAttribute("sdIdentity", sdIdentity);
                    GetUserResponse getUserResponse = oneCheckWalletService.getUserByID(sdIdentity, null, null);
                    if (null != getUserResponse) {
                        model.addAttribute("imsDetails", getUserResponse.getUserDetails());
                    }
                    GetSDMoneyAccountResponse getSDMoneyAccountResponse = oneCheckWalletService.getSDMoneyAccountDetails(
                            sdIdentity);
                    if (null != getSDMoneyAccountResponse) {
                        model.addAttribute("ocwalletStatus", getSDMoneyAccountResponse.getSdMoneyAccountStatus());
                    }
                }
            } catch (Exception e) {
                logger.error("Error in fetching one check details from SD/IMS for email: "+email,e);
            }
		}
		try {
			Users users = userServiceProxy.getUserByEmailId(email);
			
	        Calendar currentDateTime = Calendar.getInstance();
	        /* Getting the current date time */
	        Timestamp toDateTime = new Timestamp(currentDateTime.getTime().getTime());

	        /* Getting date time of two months back from now */
	        currentDateTime.add(Calendar.DATE, -60);
	        Timestamp fromDateTime = new Timestamp(currentDateTime.getTime().getTime());

	        /* Getting txnhomepageIds of last two months */
	        List<String> txnHomePageIdList = fulfillmentService.getTxnHomePageIdByEmailFromDate(email, fromDateTime,
	                toDateTime);

	        if ((null == txnHomePageIdList || txnHomePageIdList.size() == 0) && users == null) {
	            listMessages.add(String.format("No Data found for Email Id %s or put valid Email Id.", email));
	            model.addAttribute("messages", listMessages);
	            return ViewConstants.EMAIL_INPUT_RESULT;
	        } else {
	        	/* Getting Addcash wallet transactions in last two months */
	            List<WalletTransaction> walletTransactionList = walletService.getAddCashTransactionByUserIdBetweenDays(
	                    users.getUserId(), fromDateTime, toDateTime);
	            model.addAttribute("walletTransactionList", walletTransactionList);
	        }

	        List<CustomerCare> customerCareList = getTxnByTxnHomePageIds(txnHomePageIdList);
	        model.addAttribute("upiTransactionsList", upiTransactionService.getUPITransactionsViewDataByEmailId(email));
	        model.addAttribute("customerCareList", customerCareList);
	        model.addAttribute("emailentered", email);
	        model.addAttribute("messages", messages);
	        model.addAttribute("searchTime", "TWO");
			
		} catch (Exception e) {
			logger.error("Exception occured while getting userdata for emailid : " +email +" errorMessage "
					+e.getMessage().toString()
					+" error StackTrace " +e.getStackTrace().toString());
		}
        
        return ViewConstants.EMAIL_INPUT_RESULT;
    }

    /*
     * If CS guys want to know all user transactions by emailId Link is in
     * Customer trail search by email Id
     */
    @RequestMapping(value = "alltxns/do")
    public String getCustomerAllTransactons(@RequestParam Map<String, String> params, Model model,HttpServletRequest request,
            HttpServletResponse response) throws IOException {
    	if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
            return get403HtmlResponse(response);
        }
    	
    	if (params.get("email") == null || params.get("email").isEmpty()) {
            model.addAttribute("exception", "Email id is required.");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        String email = params.get("email").trim();
        
        /*Fetch migration status*/
        if (!FCUtil.isEmpty(email)) {
        	MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(email, null);
        	if (migrationStatus != null) {
        		String userMigrationStatus =  migrationStatus.getMigrationStatus();
        		model.addAttribute("fcSdUserMigration", userMigrationStatus + "("+adminHelper.getStatusMsgForUserMigration(userMigrationStatus)+")");
                model.addAttribute("userMigrationToolTipText", adminHelper.getToolTipMsgForUserMigration(userMigrationStatus));
        	}
        }
        
        List<String> listMessages = new LinkedList<String>();
        model.addAttribute("listmessages", listMessages);

        Users users = userServiceProxy.getUserByEmailId(email);
        /* Getting all txnhomepageIds */
        List<String> txnHomePageIdList = fulfillmentService.getTxnHomePageIdByEmail(email);

        if (txnHomePageIdList.size() == 0 && users == null) {
            listMessages.add(String.format("No Data found for Email Id %s or put valid Email Id.", email));
            model.addAttribute("messages", listMessages);
            return ViewConstants.EMAIL_INPUT_RESULT;
        } else {
            /* Getting all wallet transactions */
            List<WalletTransaction> walletTransactionList = walletService.getAddCashTransactionByUserId(users
                    .getUserId());
            model.addAttribute("walletTransactionList", walletTransactionList);
        }

        List<CustomerCare> customerCareList = getTxnByTxnHomePageIds(txnHomePageIdList);
        model.addAttribute("customerCareList", customerCareList);
        model.addAttribute("emailentered", email);
        model.addAttribute("searchTime", "ALL");

        return ViewConstants.EMAIL_INPUT_RESULT;
    }
		
    /**
     * Getting transaction details of given txnhomepageid.
     * @param txnHomePageIdList
     * @return customerCareList
     */
    private List<CustomerCare> getTxnByTxnHomePageIds(List<String> txnHomePageIdList) {
        List<CustomerCare> customerCareList =new ArrayList<CustomerCare>();
		List<String> lookups = new ArrayList<String>();
		for (String txnHomePageId : txnHomePageIdList) {
			lookups.add(txnHomePageDAO.getLookupIdByTxnHomePageId(txnHomePageId));
		}
		
		List<String> orderIdList = new ArrayList<String>();
		for (String lookup_id : lookups) {
			orderIdList.add(orderIdSlaveDAO.getOrderIdForLookUpId(lookup_id));
		}
 	
		LinkedHashSet<String> orderlist = new LinkedHashSet<String>(orderIdList);
		orderIdList = new ArrayList<String>(orderlist);
		Collections.reverse(orderIdList);
		
		for (String orderid : orderIdList) {
			String paymentStatus = null;
			String orderDate = null;
			Timestamp orderdate = null;
			String totalAmount = null;
			String shippingAddress = "";
			String rechargeStatus = null;
			String success1 = "00";
			String success2 = "0";
			String underProcessRecharge ="08";
			String pendingRecharge = "-1";
			String couponsStatus = null;
			String dispatchDate = null;
			String trackingNumber = null;
			String pVouchers = " ";
			String eVouchers = " ";
			String rechargeAmount = null;
			String pgName = null;
			String agName = null;
			String promocode = null;
			String productType = null;
			
			if (orderid != null) {
				//getting orderDate from OrderID table
				OrderId orderId = new OrderId();
				orderId.setOrderId(orderid);
				List<OrderId> orderIds = easyDBWriteDAO.get(orderId);
				orderId = FCUtil.getFirstElement(orderIds);
				orderdate = orderId.getCreatedOn();
				orderDate = orderdate.toString();
			
				//PaymentStatus,Recharges amount from PaymentTransaction Table 
				Boolean success = paymentTransactionService.isPaymentTransactionSuccessful(orderid);	
				double amount = 0.0;
				
				if (success) {
					paymentStatus = "Payment Successfull";
				} else {
					paymentStatus = "Payment UnSuccessfull";
				}  
				//Total Amount from Payment transaction table.
				PaymentTransaction paymentTransaction = paymentTransactionService.getLastPaymentTransactionSentToPG(orderId.getOrderId());
				//model.addAttribute("paymentTransaction", paymentTransaction);
			
				
				List<PaymentTransaction> allPaymentTransaction = paymentTransactionService.getAllPaymentTransactionSentToPG(orderId.getOrderId());
	            if (paymentTransaction != null) {
	            //model.addAttribute("allPaymentTransaction", allPaymentTransaction);
	            pgName = paymentTransaction.getPaymentGateway();
	            }
				if (paymentTransaction != null) {
					amount = paymentTransaction.getAmount();
				} else {
					amount = 0.0;
				}
				totalAmount = Double.toString(amount);
			
				//SippingAddress from TxnFulfilment Table  
				TransactionHomePage transactionHomePage3 = new TransactionHomePage();
				transactionHomePage3.setLookupId(orderId.getLookupId());
				List<TransactionHomePage> txnHomePageList = easyDBWriteDAO.get(transactionHomePage3);
			
				if (txnHomePageList.size() > 1) {
					logger.info("More than one transaction home page details found for order id" +orderId+ ", contact the dev team immediately");
				}
				
				transactionHomePage3 = FCUtil.getFirstElement(txnHomePageList);
				if (transactionHomePage3 !=null) {
					TransactionFulfilment transactionFulfilment = new TransactionFulfilment();
					transactionFulfilment.setTxnHomePageId(transactionHomePage3.getId());
					List<TransactionFulfilment> transactionFulfilments = easyDBWriteDAO.get(transactionFulfilment);
					transactionFulfilment = FCUtil.getFirstElement(transactionFulfilments);

					if (transactionFulfilments.size() > 1) {
						logger.info("More than one transaction fulfilment details found for order id"+orderId+", contact the dev team immediately");
					}
					
					if (transactionFulfilment != null) {
						shippingAddress+= transactionFulfilment.getDeliveryName() +" : "+" "+ transactionFulfilment.getDeliveryAdd()+"," +" "+ transactionFulfilment.getDeliveryArea()+","+" "+ transactionFulfilment.getDeliveryLandmark()+","+" "+ transactionFulfilment.getAddressMobile()+".";
					} else {
						shippingAddress = "Not Available";
					}
					productType = transactionHomePage3.getProductType();
					if(productType != null) {
						productType = ProductName.fromCode(productType).name();
					}
					
				} else {
					logger.error("TransactionHomePage row is not available for orderid" + orderid);
					shippingAddress = "Not Available";
					productType = "Not Available";
				}

				 /* below code change to handle retry cases*/ 
                
                UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId.getOrderId());
                BillPaymentStatus billPaymentStatus = null;
                if(userTransactionHistory!=null){
                	/* If the transaction is billpayment */
                	if(userTransactionHistory.getProductType().equals(ProductName.MobilePostpaid.getProductType())){
                      	 billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId.getOrderId());
                      	 
                         if (billPaymentStatus != null) {
                             if (billPaymentStatus.getSuccess() == null) {
                                 rechargeStatus = "Billpayment recharge status pending";
                             } else if (billPaymentStatus.getSuccess()) {
                                 rechargeStatus = "Billpayment recharge successful";
                             } else {
                                 rechargeStatus = "Billpayment recharge unsuccessful";
                             } 
                             agName = billPaymentStatus.getBillPaymentGateway();
                         }
                }else{
                	 // Rechargestatus from InResponse Table
                    InResponse inResponse = new InResponse();
                    String responseCode = null;
                    InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId.getOrderId());
                    if (inTransactionData != null) {
                        //model.addAttribute("inTransactionData", inTransactionData);
                        agName = inTransactionData.getInResponse().getAggrName();
                    }
                    if (inTransactionData != null) {
                        inResponse = inTransactionData.getInResponse();
                        if (inResponse != null) {
                            responseCode = inResponse.getInRespCode();
                        } else {
                            responseCode = "NA";
                            logger.error("InResponse table row is not available for order id :" + orderid);
                        }
                    } else {
                        responseCode = "NA";
                        logger.error("InTransaction table row is not available for order id :" + orderid);
                    }

                    if (responseCode.equals(success1) || responseCode.equals(success2)) {
                        rechargeStatus = "Recharge Successfull";
                    } else if ("NA".equals(responseCode)) {
                        rechargeStatus = " Not Aavailable";
                    } else if (responseCode.equals(underProcessRecharge)) {
                        rechargeStatus = "Transaction Under Process";
                    } else if (responseCode.equals(pendingRecharge)) {
                        rechargeStatus = "Pending Transaction";
                    } else {
                        rechargeStatus = "Recharge UnSuccessfull";
                    }
                 }
                }
		
				//couponsStatus, dispatchDate, trackingNumber from orderFulfillmentDetails.
				OrderFulfillmentDetails orderFulfillmentDetails = new OrderFulfillmentDetails();
				orderFulfillmentDetails.setOrderId(orderid);
				List<OrderFulfillmentDetails> orderFulfillmentDetailsList = easyDBWriteDAO.get(orderFulfillmentDetails);
				orderFulfillmentDetails = FCUtil.getFirstElement(orderFulfillmentDetailsList);		
							
				if (orderFulfillmentDetails != null) {
					couponsStatus = orderFulfillmentDetails.getFulfillmentStatus();
					Integer date =  orderFulfillmentDetails.getDateDelivered();
					dispatchDate =  date.toString();
					trackingNumber = orderFulfillmentDetails.getAwbNum();
				} else {
					couponsStatus = "Not Available Now";
					dispatchDate = "Not Available Now"; 
					trackingNumber = "Not Available Now"; 
				}
		
				//getting Vouchers using TransactionHomePage->cartBusinessDO->cartItemsBusinessDO->CouponStore
				TransactionHomePage transactionHomePage2 = new TransactionHomePage();
				transactionHomePage2.setLookupId(orderId.getLookupId());
				List<TransactionHomePage> txnHomePages = easyDBWriteDAO.get(transactionHomePage2);
			
				if (txnHomePages.size() > 1) {
					logger.info("More than one transaction home page details found for order id"+orderId+", contact the dev team immediately");
				}
				
				transactionHomePage2 = FCUtil.getFirstElement(txnHomePages);
				if (transactionHomePage2 != null) {
					CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(transactionHomePage2.getLookupId());
					Long cartid = cartBusinessDo.getId();
					int cart_id = cartid.intValue();
					Integer cartId = new Integer(cart_id);
					 List<CartItemsBusinessDO> cartItemList = cartBusinessDo.getCartItems();
			            for (CartItemsBusinessDO cartItemBusinessDo : cartItemList) {
			                if (cartBusinessDo != null && cartItemBusinessDo.getProductMasterId() == 8 && cartItemBusinessDo.getDeleted() == false) {
			                    FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon((long)cartItemBusinessDo.getItemId());
			                    if(freefundCoupon != null) {
			                        List<String> orderidlist = orderIdReadDAO.getOrderIdListForCoupon(freefundCoupon.getFreefundCode());
			                             promocode = freefundCoupon.getFreefundCode();
			                       
			                    }
			                }
			            }
					
					if (cartBusinessDo != null) {
						List<CouponStore> couponStores = couponService.getAllCouponStores(cartBusinessDo);
						for (CouponStore couponStore : couponStores) {
							if (couponStore != null) {
								if (FCConstants.COUPON_TYPE_P.equals(couponStore.getCouponType())) {
									pVouchers += " " + couponStore.getCampaignName() + "-" + couponStore.getCouponValue() + "(" + cartItemsDAO.getCouponQuantity(couponStore.getCouponStoreId(), cartId) + ")" + " " + " " ;
								} else if (FCConstants.COUPON_TYPE_E.equals(couponStore.getCouponType())) {
									eVouchers += " " + couponStore.getCampaignName() + "-" +couponStore.getCouponValue() + "(" + cartItemsDAO.getCouponQuantity(couponStore.getCouponStoreId(), cartId) + ")" + " " + " " ;
								} 
								else if (FCConstants.COUPON_TYPE_C.equals(couponStore.getCouponType())) {
									logger.info("Crosssell");	
								} else {
									logger.error("Other Than P or E or C for orderId"+orderid);
								}
							} else {
								logger.error("couponStore row is empty for orderId"+orderid);							
							}
						}						
					} else {
						logger.error("cartBusinessDO row is empty");
						pVouchers = "Not Available";
						eVouchers = "Not Available";
					}
					
					//Recharge Amount getting from Transaction Home Page.
					Float rechargeValue = transactionHomePage2.getAmount();
					rechargeAmount = Float.toString(rechargeValue);	
				} else {
					logger.error(transactionHomePage2+"txn_home_page row is empty");
					pVouchers = "Not Available";
					eVouchers = "Not Available";
					rechargeAmount = "Not Available";	
				}
			} else {
				logger.error(orderid+" : Order id is not avilable in order_id table");
				continue;
			}
			customerCareList.add(new CustomerCare(orderid, orderDate, rechargeAmount, totalAmount, shippingAddress, paymentStatus, rechargeStatus, pVouchers, eVouchers, couponsStatus, dispatchDate, trackingNumber, pgName, agName, promocode, productType));	
		}
        return customerCareList;
    }
    
}

