package com.freecharge.admin.CustomerTrailDetails;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mongo.repos.BinofferRepository;
import com.mongodb.DBObject;

@Controller
@RequestMapping("/admin/cardhash_input_transaction")
public class CardHashInputController {
    public static final String CUSTOMER_TRAIL_CARDHASH_CASHBACK_RESULT = "admin/cardhash/cashback/result";
    public static final String CARDHASH_PARAM_NAME = "cardhash";
    public static final String INVALID_ENTRY = "Invalid Entry";

    private static Logger logger = LoggingFactory.getLogger(CardHashInputController.class);

    @Autowired
    FreefundService freefundService;

    @Autowired
    private BinofferRepository binofferRepository;

    //Get cashback details by cardHash from mongoDb-binofferRepository
    @RequestMapping("getcashbackdetails")
    public String getCashBackDetailsByCardHash(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        String cardHash = mapping.get(CARDHASH_PARAM_NAME).trim();
        List<DBObject> binOfferRepositoryDBObjects = null;
        List<Map<String, String>> binOfferRepositoryResultMap =  new LinkedList<>();
        model.addAttribute("cardHashEntered", cardHash);

        if(cardHash.isEmpty() || cardHash == null) {
            model.addAttribute("messages", "Invalid cardHash");
            return CUSTOMER_TRAIL_CARDHASH_CASHBACK_RESULT;
        }

        try {
            binOfferRepositoryDBObjects = binofferRepository.getDataByCardHash(cardHash);
        }
        catch (Exception e){
            logger.error("BinOfferRepository retreival failed for cardHash: " + cardHash, e);
        }

        if(binOfferRepositoryDBObjects == null || binOfferRepositoryDBObjects.size() <=0 ){
            model.addAttribute("messages", "No entry found against cardHash: " + cardHash);
            return CUSTOMER_TRAIL_CARDHASH_CASHBACK_RESULT;
        }

        try {
            binOfferRepositoryResultMap = getBinOfferRepositoryResultMap(binOfferRepositoryDBObjects);
        }
        catch (Exception e){
            logger.error("BinOfferRepository data parsing failed for cardHash: " + cardHash, e);
        }
        
        model.addAttribute("cashBackEntries", binOfferRepositoryResultMap);
        return CUSTOMER_TRAIL_CARDHASH_CASHBACK_RESULT;

    }

    private List<Map<String, String>> getBinOfferRepositoryResultMap(List<DBObject> binOfferRepositoryDBObjects){
        List<Map<String, String>> binOfferRepositoryResultMap = new LinkedList<>();

        try {            
            if( binOfferRepositoryDBObjects!= null && binOfferRepositoryDBObjects.size() > 0){
                for(DBObject dBObject: binOfferRepositoryDBObjects){
                    Object cardHashObject = dBObject.get(FCConstants.CARDHASH);
                    Object orderIdObject = dBObject.get(FCConstants.ORDER_ID);
                    Object ffClassObject = dBObject.get(FCConstants.FREEFUNDCLASS);
                    Object createdAtObject = dBObject.get(FCConstants.CREATEDAT);

                    if(!isObjectNull(orderIdObject)){

                        Map<String, String> binOfferRepositoryMap = new HashMap<String, String>();
                        binOfferRepositoryMap.put(FCConstants.CARDHASH, String.valueOf(cardHashObject));
                        binOfferRepositoryMap.put(FCConstants.ORDER_ID, String.valueOf(orderIdObject));

                        if(!isObjectNull(ffClassObject)){
                            FreefundClass freefundClass = freefundService.getFreefundClass((long) ffClassObject);
                            if(freefundClass!= null) {
                                binOfferRepositoryMap.put(FCConstants.FREEFUNDCLASS, freefundClass.getName());
                            } else {
                                logger.warn("FreefundClass returns null for BinOfferRepository.FFCLASS: " + ffClassObject);
                                binOfferRepositoryMap.put(FCConstants.FREEFUNDCLASS, INVALID_ENTRY);
                            }
                        } 

                        if(!isObjectNull(createdAtObject)){    
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            binOfferRepositoryMap.put(FCConstants.CREATEDAT, sdf.format(createdAtObject));                  
                        } 

                        binOfferRepositoryResultMap.add(binOfferRepositoryMap);
                    } 

                }
            }  
        }
        catch(Exception e){
            logger.error("Error while parsing BinOfferRepository data", e);
        }
        return binOfferRepositoryResultMap;
    }

    private boolean isObjectNull(Object object){
        if(object != null){
            return false;
        }       
        return true;
    }
}