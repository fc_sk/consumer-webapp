package com.freecharge.admin.CustomerTrailDetails;

import java.math.BigDecimal;
import java.util.Date;

import com.snapdeal.fcpt.txnhistoryview.commons.enums.BusinessUseCase;
import com.snapdeal.fcpt.txnhistoryview.commons.enums.TxnStatus;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.UPIInfo;
import com.snapdeal.ims.dto.UserDetailsDTO;

public class UPITransactionDetailView {

	private String userId;
	private String globalTxnId;
	private String globalTxnType;
	private UPIInfo upiInfo;
	private BigDecimal txnAmount;
	private Date transactionDate;
	private TxnStatus txnStatus;
	private BusinessUseCase businessUseCase;
	private String promoCode;
	private String merchantOrderId;
	private UserDetailsDTO userDetails;
	private String upiNpciErrorMessage;

	public UPITransactionDetailView() {
		// TODO Auto-generated constructor stub
	}

	public UPITransactionDetailView(UPITransactionDetailViewBuilder builder) {
		this.userId = builder.userId;
		this.globalTxnId = builder.globalTxnId;
		this.globalTxnType = builder.globalTxnType;
		this.upiInfo = builder.upiInfo;
		this.txnAmount = builder.txnAmount;
		this.transactionDate = builder.transactionDate;
		this.txnStatus = builder.txnStatus;
		this.businessUseCase = builder.businessUseCase;
		this.promoCode = builder.promoCode;
		this.userDetails = builder.userDetails;
		this.merchantOrderId = builder.merchantOrderId;
		this.upiNpciErrorMessage = builder.upiNpciErrorMessage;
	}

	public String getUserId() {
		return userId;
	}

	public String getGlobalTxnId() {
		return globalTxnId;
	}

	public String getGlobalTxnType() {
		return globalTxnType;
	}

	public UPIInfo getUpiInfo() {
		return upiInfo;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public TxnStatus getTxnStatus() {
		return txnStatus;
	}

	public BusinessUseCase getBusinessUseCase() {
		return businessUseCase;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public UserDetailsDTO getUserDetails() {
		return userDetails;
	}

	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	public String getUpiNpciErrorMessage() {
		return upiNpciErrorMessage;
	}

	public static class UPITransactionDetailViewBuilder {
		private String userId;
		private String globalTxnId;
		private String globalTxnType;
		private UPIInfo upiInfo;
		private BigDecimal txnAmount;
		private Date transactionDate;
		private TxnStatus txnStatus;
		private BusinessUseCase businessUseCase;
		private String promoCode;
		private UserDetailsDTO userDetails;
		private String merchantOrderId;
		private String upiNpciErrorMessage;

		public UPITransactionDetailViewBuilder() {
			// TODO Auto-generated constructor stub
		}

		public UPITransactionDetailViewBuilder setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public UPITransactionDetailViewBuilder setGlobalTxnId(String globalTxnId) {
			this.globalTxnId = globalTxnId;
			return this;
		}

		public UPITransactionDetailViewBuilder setGlobalTxnType(String globalTxnType) {
			this.globalTxnType = globalTxnType;
			return this;
		}

		public UPITransactionDetailViewBuilder setUpiInfo(UPIInfo upiInfo) {
			this.upiInfo = upiInfo;
			return this;
		}

		public UPITransactionDetailViewBuilder setTxnAmount(BigDecimal txnAmount) {
			this.txnAmount = txnAmount;
			return this;
		}

		public UPITransactionDetailViewBuilder setTransactionDate(Date transactionDate) {
			this.transactionDate = transactionDate;
			return this;
		}

		public UPITransactionDetailViewBuilder setTxnStatus(TxnStatus txnStatus) {
			this.txnStatus = txnStatus;
			return this;
		}

		public UPITransactionDetailViewBuilder setBusinessUseCase(BusinessUseCase businessUseCase) {
			this.businessUseCase = businessUseCase;
			return this;
		}

		public UPITransactionDetailViewBuilder setPromoCode(String promoCode) {
			this.promoCode = promoCode;
			return this;
		}

		public UPITransactionDetailViewBuilder setUserDetails(UserDetailsDTO userDetails) {
			this.userDetails = userDetails;
			return this;
		}

		public UPITransactionDetailViewBuilder setMerchantOrderId(String merchantOrderId) {
			this.merchantOrderId = merchantOrderId;
			return this;
		}

		public UPITransactionDetailViewBuilder setUpiNpciErrorMessage(String upiNpciErrorMessage) {
			this.upiNpciErrorMessage = upiNpciErrorMessage;
			return this;
		}

		public UPITransactionDetailView build() {
			return new UPITransactionDetailView(this);
		}
	}
}
