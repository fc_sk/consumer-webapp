package com.freecharge.admin.CustomerTrailDetails;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OrderUserIdMapper implements RowMapper<String> {
	@Override
	public String mapRow(ResultSet rs, int rownum) throws SQLException {
		return rs.getString("fk_user_id");
	}

}
