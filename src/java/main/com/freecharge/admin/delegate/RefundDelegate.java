package com.freecharge.admin.delegate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;

import com.freecharge.admin.entity.RefundRow;
import com.freecharge.admin.service.RefundService;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundDelegate implements WebDelegate {
    @Autowired
    private RefundService refundService;

    @Override
    public void delegate(WebContext webContext) throws Exception {
        PagedListHolder<RefundRow> pagedListHolder = new PagedListHolder<RefundRow>(this.refundService.get());
        pagedListHolder.setPageSize(2);
        webContext.getRequest().setAttribute("refundRows", pagedListHolder);
        webContext.getRequest().getSession().setAttribute("RefundAdminController_refundRows", pagedListHolder);
    }
}
