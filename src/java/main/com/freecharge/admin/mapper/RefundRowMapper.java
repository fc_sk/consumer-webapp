package com.freecharge.admin.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.admin.entity.RefundRow;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundRowMapper implements RowMapper<RefundRow> {
    public RefundRow mapRow(ResultSet resultSet, int row) throws SQLException {
        RefundRow refundRow = new RefundRow();
        refundRow.setPaymentTransactionId(resultSet.getString("payment_txn_id"));
        refundRow.setPaymentGatewayTransactionId(resultSet.getString("pg_transaction_id"));
        refundRow.setOrderId(resultSet.getString("order_id"));
        refundRow.setIntegrationGatewaySuccess(resultSet.getBoolean("transaction_status"));
        refundRow.setPaymentGatewaySuccess(resultSet.getBoolean("is_successful"));
        refundRow.setAmount(resultSet.getDouble("amount"));
        return refundRow;
    }
}
