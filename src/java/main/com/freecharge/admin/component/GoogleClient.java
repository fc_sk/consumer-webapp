package com.freecharge.admin.component;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.common.timeout.HttpExecutorService;
import com.freecharge.common.timeout.HttpResponse;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.freecharge.common.framework.logging.LoggingFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class GoogleClient {

    public static final int API_TIMEOUT = 5000;
    public static final String GOOGLE_AUTH_URL = "https://accounts.google.com/o/oauth2/auth";
    public static final String GOOGLE_TOKEN_URL = "https://accounts.google.com/o/oauth2/token";
    public static final String GOOGLE_PROFILE_URL = "https://www.googleapis.com/oauth2/v1/userinfo";
    public static final String GOOGLE_PLUS_URL = "https://www.googleapis.com/plus/v1/people/me";

    private Logger logger = LoggingFactory.getLogger(getClass());
    private String clientId;
    private String clientSecret;
    private Integer threadTimeout;
    private Integer socketTimeout;

//    private CommonHttpService commonHttpService;

    @Autowired
    private HttpExecutorService httpExecutorService;

    @Autowired
    private HttpClient commonHttpClient;

    /**
     * This method prepares Google authorization url. User should be redirected to this url for providing permissions
     */
    public final String getAuthorizeUrl(final String redirectUri, final String state) {
        Map<String, String> params = new HashMap<>();
        params.put("scope",
                "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile");
        params.put("response_type", "code");
        params.put("client_id", this.clientId);
        params.put("state", state);
        params.put("redirect_uri", redirectUri);
        StringBuilder builder = new StringBuilder();
        try {
            for (Map.Entry<String, String> pm : params.entrySet()) {
                builder.append("&").append(URLEncoder.encode(pm.getKey(), "utf-8")).append("=").
                        append(URLEncoder.encode(pm.getValue(), "utf-8"));
            }
        } catch (UnsupportedEncodingException use) {
            logger.error("Unsupported encoding for: utf-8");
            return null;
        }
        String queryString = builder.substring(1);
        return GOOGLE_AUTH_URL + "?" + queryString;
    }

    public final Map<String, Object> getAccessTokenMap(final String code, final String redirectUri) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new NameValuePair("code", code));
        params.add(new NameValuePair("client_id", this.clientId));
        params.add(new NameValuePair("client_secret", this.clientSecret));
        params.add(new NameValuePair("redirect_uri", redirectUri));
        params.add(new NameValuePair("grant_type", "authorization_code"));
        PostMethod post = new PostMethod(GOOGLE_TOKEN_URL);
        try {
            //CommonHttpService.HttpResponse responsei = this.commonHttpService.firePostRequestWithTimeOut(
            //        GOOGLE_TOKEN_URL, params, false, API_TIMEOUT);
            post.getParams().setSoTimeout(getSocketTimeout());
            post.setRequestBody(params.toArray(new NameValuePair[params.size()]));
            logger.info("Making POST request for url: " + GOOGLE_PROFILE_URL);
            //int status = this.commonHttpClient.executeMethod(post);
            HttpResponse response = httpExecutorService.executeWithTimeOut(commonHttpClient, post, getThreadTimeout());
            logger.info(String.format("Successful request to: %s", GOOGLE_PROFILE_URL));
            if (response.getStatusCode() == HttpStatus.SC_OK) {
                JSONParser parser = new JSONParser();
                return (JSONObject) parser.parse(response.getResponse());
            }
            logger.error(
                    String.format("Error fetching Google Access Token, received HTTP status code = %d, message = %s",
                            response.getStatusCode(), response.getResponse()));
            return null;
        } catch (Exception e) {
            logger.error("google_access_token_error: " + code, e);
            return null;
        } finally {
            logger.debug("Connection release in post for url: " + GOOGLE_PROFILE_URL);
            post.releaseConnection();
        }
    }

    public final Map<String, Object> getUserProfile(final String accessToken) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new NameValuePair("access_token", accessToken));
        GetMethod get = new GetMethod(GOOGLE_PROFILE_URL);
        try {
            //CommonHttpService.HttpResponse response = this.commonHttpService.fireGetRequestWithTimeOut(
            //        GOOGLE_PROFILE_URL, params, API_TIMEOUT);
            if(getSocketTimeout()!=null)
            get.getParams().setSoTimeout(getSocketTimeout());
            get.setQueryString(params.toArray(new NameValuePair[params.size()]));
            logger.info("Making GET request: " + GOOGLE_PROFILE_URL + " query_string: " + get.getQueryString());
            HttpResponse response = httpExecutorService.executeWithTimeOut(commonHttpClient, get, getThreadTimeout());
            //int statusCode = this.commonHttpClient.executeMethod(get);
            logger.info("Get status: " + response.getStatusCode() + " for url: " + GOOGLE_PROFILE_URL + "?" + get
                    .getQueryString());
            if (response.getStatusCode() == HttpStatus.SC_OK) {
                JSONParser parser = new JSONParser();
                return (JSONObject) parser.parse(response.getResponse());
            }
            logger.error(String.format("Error fetching user profile, received HTTP status code = %d, message = %s",
                    response.getStatusCode(), response.getResponse()));
            return null;
        } catch (Exception e) {
            logger.error("google_profile_error: " + accessToken, e);
            return null;
        } finally {
            logger.debug("Connection release in get for url: " + GOOGLE_PROFILE_URL);
            get.releaseConnection();
        }
    }

    public final Map<String, Object> getGooglePlusProfile(final String accessToken) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new NameValuePair("access_token", accessToken));
        GetMethod get = new GetMethod(GOOGLE_PLUS_URL);
        try {
            //CommonHttpService.HttpResponse response = this.commonHttpService.fireGetRequestWithTimeOut(
            //       GOOGLE_PLUS_URL, params, API_TIMEOUT);
            get.getParams().setSoTimeout(getSocketTimeout());
            get.setQueryString(params.toArray(new NameValuePair[params.size()]));
            logger.info("Making GET request: " + GOOGLE_PLUS_URL + " query_string: " + get.getQueryString());
            HttpResponse response = httpExecutorService.executeWithTimeOut(commonHttpClient, get, getThreadTimeout());
            //int statusCode = this.commonHttpClient.executeMethod(get);
            logger.info("Got status: " + response.getStatusCode() + " for url: " + GOOGLE_PLUS_URL + "?" + get
                    .getQueryString());
            if (response.getStatusCode() == HttpStatus.SC_OK) {
                JSONParser parser = new JSONParser();
                return (JSONObject) parser.parse(response.getResponse());
            }
            logger.error(String.format("Error fetching Google+ profile, received HTTP status code = %d, message = %s",
                    response.getStatusCode(), response.getResponse()));
            return null;
        } catch (Exception e) {
            logger.error("google_plus_profile_error: " + accessToken, e);
            return null;
        } finally {
            logger.debug("Connection release in get for url: " + GOOGLE_PLUS_URL);
            get.releaseConnection();
        }
    }

    public final String getClientId() {
        return clientId;
    }

    public final void setClientId(final String newClientId) {
        this.clientId = newClientId;
    }

    public final String getClientSecret() {
        return clientSecret;
    }

    public final void setClientSecret(final String newClientSecret) {
        this.clientSecret = newClientSecret;
    }

    public Integer getThreadTimeout() {
        if(threadTimeout==null)
            return API_TIMEOUT;
        return threadTimeout;
    }

    public void setThreadTimeout(Integer threadTimeout) {
        this.threadTimeout = threadTimeout;
    }

    public Integer getSocketTimeout() {
        if(socketTimeout==null)
            return API_TIMEOUT;
        return socketTimeout;
    }

    public void setSocketTimeout(Integer socketTimeout) {
        this.socketTimeout = socketTimeout;
    }
}
