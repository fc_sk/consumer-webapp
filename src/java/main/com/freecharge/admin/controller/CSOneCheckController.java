package com.freecharge.admin.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.admin.CustomerTrailDetails.UPITransactionView;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.service.UPITransactionService;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.batcave.VCReconService;
import com.freecharge.batcave.entity.vcrecon.VCTransactionByRefNoWrapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.BatcaveConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.customercare.service.ConsumerCreditService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.merchantview.service.MerchantViewService;
import com.freecharge.mojo.core.MojoEnums;
import com.freecharge.rest.onecheck.model.OneCheckTransactionDetails;
import com.freecharge.rest.onecheck.service.FCWalletService;
import com.freecharge.rest.onecheck.service.OneCheckFCWalletService;
import com.freecharge.rest.user.ConsumerCreditTxnSummary;
import com.freecharge.rest.user.DebitEMITxnSummary;
import com.freecharge.rest.user.OneCheckTransactionSummary;
import com.freecharge.views.P2PAndP2MView;
import com.freecharge.virtualcard.VirtualCardService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.service.OneCheckUserService;
import com.freecharge.web.util.ViewConstants;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;
import com.snapdeal.payments.view.request.commons.dto.RVTransactionDto;

@Controller
@RequestMapping("/admin/customertrail/onecheck/*")
public class CSOneCheckController extends BaseAdminController {
	private static final String ONE_CHECK_PREFIX  = "OC";
	private static       Logger logger            = LoggingFactory.getLogger(CSOneCheckController.class);
	private static final String UPGRADE_COMPLETED = "UPGRADE_COMPLETED";
	private static final String LINK_FC_ACCOUNT   = "LINK_FC_ACCOUNT";
	private static final String LINK_SD_ACCOUNT   = "LINK_SD_ACCOUNT";
	private static final String EVALUATED_KEY_STRING = "evaluatedkey";
	private static final String FC_WALLET_LAST_KEY_STRING = "fcwalletlastkey";


	@Autowired
    OneCheckUserService   oneCheckUserService;

	@Autowired
	VirtualCardService	  virtualCardService;
	
	@Autowired
	MerchantViewService	merchantViewService;

	@Autowired
    private UserServiceProxy userServiceProxy;
	
	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private FCWalletService oneCheckService;

	@Autowired
	private OneCheckFCWalletService walletService;

	@Autowired
	private AdminHelper adminHelper;
	
	@Autowired
	private VCReconService vcReconService;
	
	@Autowired
	private ConsumerCreditService consumerCreditService;
	
	@Autowired
    private UPITransactionService upiTransactionService;

	@RequestMapping(value = "searchbyemail/view", method = RequestMethod.POST)
	public String getCtOncheckTxnView(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String ONECHECK_TXN_LINK = "/admin/customertrail/onecheck/searchbyemail/result.htm";
		String OLD_TXN_LINK = "/admin/email_input_transaction/wallettransaction.htm";
		List<String> migrationStatusList = Arrays.asList(UPGRADE_COMPLETED, LINK_FC_ACCOUNT, LINK_SD_ACCOUNT);
		if (FCUtil.isEmpty(mapping.get("emailid"))) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		String emailId = mapping.get("emailid").trim();
		if (!FCUtil.isValidSearchEmailFormat(emailId)) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

		MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, null);
        String emailEncoded = URLEncoder.encode(emailId, StandardCharsets.UTF_8.toString());
        if (migrationStatus != null && migrationStatusList.contains(migrationStatus.getMigrationStatus())) {
			logger.info("User onecheck wallet transaction details : " + mapping.get("emailid"));
			return String.format("redirect:%s?emailid=%s", ONECHECK_TXN_LINK, emailEncoded);
		} else {
			logger.info("User onecheck wallet OLD transaction details : " + mapping.get("emailid"));
			return String.format("redirect:%s?email=%s", OLD_TXN_LINK, emailEncoded);
		}
	}
	
	@RequestMapping(value = "shopobyemail", method = RequestMethod.POST)
	public String getShopoDetailsResult(@RequestParam final Map<String, String> mapping, final Model model,
										final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		//shopo flag
		model.addAttribute("shopoflag",true);
		
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			model.addAttribute("messages", "Access Denied");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		if (mapping.get("id") == null) {
			model.addAttribute("messages", "Email Id or Mobile No is required");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

		String id = mapping.get("id").trim();
		Boolean isEmailId = false;
		
		if(FCUtil.isValidEmailFormat(id)){
			isEmailId=true;
		}else if(!FCUtil.isMobileNoFormat(id)){
			model.addAttribute("messages", "Enter a valid email Id or Mobile No. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		logger.info("Fetching Shopo History for email Id/mobile No: "+id);
		
		SimpleDateFormat finalformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = null;
		Date endDate = null;
		Date edate = null;
		Calendar currentDateTime = null;
		if (mapping.get("startdate") == null  || mapping.get("enddate") == null) {
			try {
				edate = new Date();
				endDate = FCDateUtil.convertStringToDate(finalformat.format(edate), "yyyy-MM-dd HH:mm:ss");
				currentDateTime = Calendar.getInstance();
				currentDateTime.add(Calendar.DATE, -60);
				startDate = FCDateUtil.convertStringToDate(finalformat.format(currentDateTime.getTime()), "yyyy-MM-dd HH:mm:ss");
			} catch (Exception e) {
				logger.error("Exception while parsing date in Shopo history for email : " + id, e);
				model.addAttribute("messages", "Invalide date formats");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			} 	
			//check this
			model.addAttribute("enddate", finalformat.format(endDate));
			model.addAttribute("startdate", finalformat.format(startDate));
			model.addAttribute("emailid", id);
		} else {
			String fromDate = mapping.get("startdate").trim();
			String toDate = mapping.get("enddate").trim();
			model.addAttribute("startdate", fromDate);
			model.addAttribute("enddate", toDate);
			model.addAttribute("emailid", id);
			try {
				if (FCDateUtil.isValidDateTimeFormat(fromDate, "yyyy-MM-dd HH:mm:ss")) {
					startDate = FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempStart = finalformat.format(FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd"));
					startDate = FCDateUtil.convertStringToDate(tempStart, "yyyy-MM-dd HH:mm:ss");
				}
				
				if (FCDateUtil.isValidDateTimeFormat(toDate, "yyyy-MM-dd HH:mm:ss")) {
					endDate = FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempTo = finalformat.format(FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd"));
					endDate = FCDateUtil.convertStringToDate(tempTo, "yyyy-MM-dd HH:mm:ss");
				}
			} catch (Exception e) {
			    model.addAttribute("messages", "Date entered is not in proper format ");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}
		}

		List<P2PAndP2MView> actionsList = merchantViewService.getShopoTransactionsByEmail(id, startDate, endDate, isEmailId);
		if(actionsList!=null && !actionsList.isEmpty()){
			model.addAttribute("p2pSplitDetailsList", actionsList);
		} else {
			model.addAttribute("messages", "No Shopo details for the user");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		return ViewConstants.P2P_SPLIT_CUSTOMER_TRAIL_LISTING;
	}
	
	

	@RequestMapping(value = "p2psplitbyemail", method = RequestMethod.POST)
	public String getP2PSplitDetailsResult(@RequestParam final Map<String, String> mapping, final Model model,
										final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		//counter shopo flag
		model.addAttribute("shopoflag",false);
		
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			model.addAttribute("messages", "Access Denied");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		if (mapping.get("emailid") == null) {
			model.addAttribute("messages", "Emailid is required");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		String emailid = mapping.get("emailid").trim();
		logger.info("Fetching P2P Split History for emailId: "+emailid);
		
		SimpleDateFormat finalformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = null;
		Date endDate = null;
		Date edate = null;
		Calendar currentDateTime = null;
		if (mapping.get("startdate") == null  || mapping.get("enddate") == null) {
			try {
				edate = new Date();
				endDate = FCDateUtil.convertStringToDate(finalformat.format(edate), "yyyy-MM-dd HH:mm:ss");
				currentDateTime = Calendar.getInstance();
				currentDateTime.add(Calendar.DATE, -60);
				startDate = FCDateUtil.convertStringToDate(finalformat.format(currentDateTime.getTime()), "yyyy-MM-dd HH:mm:ss");
			} catch (Exception e) {
				logger.error("Exception while parsing date in P2P Split history for email : " + emailid, e);
				model.addAttribute("messages", "Invalide date formats");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			} 	
			//check this
			model.addAttribute("enddate", finalformat.format(endDate));
			model.addAttribute("startdate", finalformat.format(startDate));
			model.addAttribute("emailid", emailid);
		} else {
			String fromDate = mapping.get("startdate").trim();
			String toDate = mapping.get("enddate").trim();
			model.addAttribute("startdate", fromDate);
			model.addAttribute("enddate", toDate);
			model.addAttribute("emailid", emailid);
			try {
				if (FCDateUtil.isValidDateTimeFormat(fromDate, "yyyy-MM-dd HH:mm:ss")) {
					startDate = FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempStart = finalformat.format(FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd"));
					startDate = FCDateUtil.convertStringToDate(tempStart, "yyyy-MM-dd HH:mm:ss");
				}
				
				if (FCDateUtil.isValidDateTimeFormat(toDate, "yyyy-MM-dd HH:mm:ss")) {
					endDate = FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempTo = finalformat.format(FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd"));
					endDate = FCDateUtil.convertStringToDate(tempTo, "yyyy-MM-dd HH:mm:ss");
				}
			} catch (Exception e) {
			    model.addAttribute("messages", "Date entered are not in proper format ");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}
		}

		List<P2PAndP2MView> actionsList = merchantViewService.getSplitTransactionsByEmail(emailid, startDate, endDate);
		if(actionsList!=null && !actionsList.isEmpty()){
			model.addAttribute("p2pSplitDetailsList", actionsList);
		} else {
			model.addAttribute("messages", "No P2P/P2M Split details for the user");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		return ViewConstants.P2P_SPLIT_CUSTOMER_TRAIL_LISTING;
	}

	@RequestMapping(value = "p2pandp2mbyemail/view", method = RequestMethod.POST)
	public String getP2PandP2MHistoryView(@RequestParam final Map<String, String> mapping, final Model model,
									  final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String ONECHECK_TXN_LINK = "/admin/customertrail/onecheck/p2pandp2mbyemail/result.htm";
		List<String> migrationStatusList = Arrays.asList(UPGRADE_COMPLETED,LINK_FC_ACCOUNT,LINK_SD_ACCOUNT);
		if (FCUtil.isEmpty(mapping.get("emailid"))) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		String emailId = mapping.get("emailid").trim();
		if (!FCUtil.isValidEmailFormat(emailId)) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

		MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, null);
		if (migrationStatus != null && migrationStatusList.contains(migrationStatus.getMigrationStatus())) {
			logger.info("P2P/P2M details for : " + mapping.get("emailid"));
			return String.format("redirect:%s?emailid=%s",ONECHECK_TXN_LINK , emailId);
		} else {
			model.addAttribute("messages", "User has not migrated to One Check Wallet. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
	}
	
	@RequestMapping(value = "p2pandp2mbyemail/result", method = RequestMethod.GET)
	public String getP2PAndP2MDetailsResult(@RequestParam final Map<String, String> mapping, final Model model,
										final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		List<RVTransactionDto> p2PAndP2MTransactionList = null;
		
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			model.addAttribute("messages", "Access Denied");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		if (mapping.get("emailid") == null) {
			model.addAttribute("messages", "Emailid is required");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

		if(isAuthorizedToUpdateCt(request)) {
			model.addAttribute("isAuthorize", "yes");
		} else {
			model.addAttribute("isAuthorize", "no");
		}
		String emailid = mapping.get("emailid").trim();
		logger.info("Fetching P2P and P2M transactions for emailId: "+emailid);
		
		p2PAndP2MTransactionList = merchantViewService.getTransactionsByEmail(emailid);
		
		if(p2PAndP2MTransactionList!=null && !p2PAndP2MTransactionList.isEmpty()){
			model.addAttribute("p2PAndP2MTransactionList", p2PAndP2MTransactionList);
		} else {
			model.addAttribute("messages", "No P2P/P2M transactions for the user");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		
		return ViewConstants.P2P_P2M_CUSTOMER_TRAIL_LISTING;
	
	}

	
	@RequestMapping(value = "vcardbyemail/view", method = RequestMethod.POST)
	public String getVCardDetailsView(@RequestParam final Map<String, String> mapping, final Model model,
									  final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String ONECHECK_TXN_LINK = "/admin/customertrail/onecheck/vcardbyemail/result.htm";
		List<String> migrationStatusList = Arrays.asList(UPGRADE_COMPLETED,LINK_FC_ACCOUNT,LINK_SD_ACCOUNT);
		if (FCUtil.isEmpty(mapping.get("emailid"))) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		String emailId = mapping.get("emailid").trim();
		if (!FCUtil.isValidSearchEmailFormat(emailId)) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

		MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(emailId, null);
		if (migrationStatus != null && migrationStatusList.contains(migrationStatus.getMigrationStatus())) {
			logger.info("VCard details for : " + mapping.get("emailid"));
			return String.format("redirect:%s?emailid=%s",ONECHECK_TXN_LINK , emailId);
		} else {
			model.addAttribute("messages", "User has not migrated to One Check Wallet. ");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
	}


	@RequestMapping(value = "vcardbyemail/result", method = RequestMethod.GET)
	public String getVCardDetailsResult(@RequestParam final Map<String, String> mapping, final Model model,
										final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "admin/customertrail/onecheck/vcardbyemail/view";
		List<Map<String,Object>> vCardDataList = new ArrayList<Map<String, Object>>();
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}
		if (mapping.get("emailid") == null) {
			model.addAttribute("messages", "Emailid is required");
			return view;
		}

		if(isAuthorizedToUpdateCt(request)) {
			model.addAttribute("isAuthorize", "yes");
		} else {
			model.addAttribute("isAuthorize", "no");
		}
		String emailid = mapping.get("emailid").trim();
		logger.info("Fetching Virtual Card data for emailId: "+emailid);
		
		if(virtualCardService.hasVCard(emailid)) {
			vCardDataList = virtualCardService.getCardsForEmail(emailid);
			if (vCardDataList != null) {
				model.addAttribute("vcardDataList", vCardDataList);
			} else {
				model.addAttribute("messages", "VCard data fetching failed. Try again.");
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}
			return view;
		}
		else {
			model.addAttribute("messages", "Either User does not have an active Virtual Card or an Exception has occured. Try again.");
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}

	}
	
	//Get VCRecon Txn Details Per CardHashId
	@RequestMapping(value = "vcardtxns", method = RequestMethod.GET)
	public String getVCardTxnsDetailsView(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		String view = "/admin/customertrail/onecheck/vcardtxns/result";
		String cardId = request.getParameter(BatcaveConstants.VC_CARDHASH_ID);
		String from = request.getParameter(BatcaveConstants.FROM_DATE);
		String to = request.getParameter(BatcaveConstants.TO_DATE);

		model.addAttribute(BatcaveConstants.VC_TRANSACTIONS, vcReconService.getTransactionDetailsByCardHashId(cardId, from, to)); 

		return view;
	}


	@RequestMapping(value = "vcardstatus/update", method = RequestMethod.POST)
	public String updateVCardStatus(@RequestParam final Map<String, String> mapping, final Model model,
										final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String ONECHECK_TXN_LINK = "/admin/customertrail/onecheck/vcardbyemail/result.htm";
		String emailId = request.getParameter("emailId");
		String statusUpdate = request.getParameter("statusUpdate");
		MojoEnums.UserStatus userStatus;
		if(statusUpdate.equalsIgnoreCase(virtualCardService.DISABLE_CARD_STRING)){
			userStatus = MojoEnums.UserStatus.INACTIVE;
		}
		else {
			userStatus = MojoEnums.UserStatus.ACTIVE;
			if(virtualCardService.hasActiveVCard(emailId))
			{
				model.addAttribute("messages", "User already has 1 active Virtual Card");
				logger.info("Cannot enable more than one card for user: "+emailId);
				return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
			}
		}
		virtualCardService.updateStatus(emailId,userStatus);
		return String.format("redirect:%s?emailid=%s",ONECHECK_TXN_LINK , emailId);
	}

	@RequestMapping(value = "vcardtxnstatus" , method = RequestMethod.GET)
	public String getVCardTxnStatus(@RequestParam final Map<String, String> mapping, final Model model,
									final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "/admin/customertrail/onecheck/vcardtxnstatus/result";
        String txnRefId = request.getParameter("vcardtxnId");
        List<Map<String,Object>> vCardTxnList = new ArrayList<Map<String,Object>>();
        Boolean allowRefund = false;

        Map<String, Object> responseMap = virtualCardService.getTransactionByTxnRefId(txnRefId);
        vCardTxnList = (List<Map<String, Object>>) responseMap.get(VirtualCardService.TXN_LIST_STRING);
        allowRefund = (Boolean) responseMap.get(VirtualCardService.ALLOW_REVERSAL_STRING);
        model.addAttribute("txnrefid", txnRefId);
		if(isAuthorizedToUpdateCt(request)) {
			model.addAttribute("isAuthorize", "yes");
		} else {
			model.addAttribute("isAuthorize", "no");
		}
        if( null != vCardTxnList && !vCardTxnList.isEmpty()) {
            model.addAttribute("vcardtxnlist", vCardTxnList);
			String userId = (String) vCardTxnList.get(0).get("userId");
            GetUserResponse userResponse = oneCheckWalletService.getUserByID(userId, null, null);
            String emailId = null;
            if( null != userResponse && null != userResponse.getUserDetails() && null != userResponse.getUserDetails().getEmailId()) {
                emailId = userResponse.getUserDetails().getEmailId();
            }
            model.addAttribute("emailid", emailId);
            if (null != allowRefund && allowRefund) {
                model.addAttribute(VirtualCardService.ALLOW_REVERSAL_STRING, allowRefund);
            }            
        }

        //Get VCRecon Transaction Details
        VCTransactionByRefNoWrapper vcTransactionWrapper = vcReconService.getTransactionDetailsByTransactionRefNo(txnRefId); 
        model.addAttribute(BatcaveConstants.VC_TRANSACTIONS, vcTransactionWrapper);

        if((null == vCardTxnList || vCardTxnList.isEmpty()) && null == vcTransactionWrapper) {
        	model.addAttribute("messages", "VCard txn details fetching failed.");
        	return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }

		return view;
	}
	
	//Get VCRecon Refund ARN Details
	@RequestMapping(value = "vcardrefundarnstatus" , method = RequestMethod.GET)
	public String getVCardRefundARNStatus(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "/admin/customertrail/onecheck/vcardrefundarnstatus/result";
		String refundARN = request.getParameter(BatcaveConstants.VC_REFUND_ARN);

		model.addAttribute(BatcaveConstants.VC_TRANSACTIONS, vcReconService.getTransactionDetailsByARN(refundARN)); 

		return view;
	}
	
	
    @RequestMapping(value = "vcard/txn/refund" , method = RequestMethod.POST)
    public String refundVCTxn(@RequestParam final Map<String, String> mapping, final Model model,
                              final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String view = "/admin/customertrail/onecheck/vcardtxnstatus/result";
        String txnRefId = request.getParameter("vcardtxnId");
		Map<String,Object> refundResponse = virtualCardService.refundVCardTxnByTxnRefId(txnRefId);
		model.addAttribute(VirtualCardService.VC_REFUND_STATUS_STRING, refundResponse.get(VirtualCardService.VC_REFUND_STATUS_STRING));
		model.addAttribute(VirtualCardService.VC_REFUND_MESSAGE_STRING, refundResponse.get(VirtualCardService.VC_REFUND_MESSAGE_STRING));
        return getVCardTxnStatus(mapping, model, request, response);
    }
    @RequestMapping(value = "searchbyemail/result", method = RequestMethod.GET)
	public String getCtOncheckTxn(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "admin/customertrail/onecheck/searchbyemail/view";
        Integer lastEvaluatedKey = null;
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
	        return null;
	    }
		if (mapping.get("emailid") == null) {
			model.addAttribute("msg", "Emailid is required");
			return view;
		}
        if(mapping.get(EVALUATED_KEY_STRING)==null) {
            lastEvaluatedKey = null;
        }
        else {
            lastEvaluatedKey=Integer.parseInt(mapping.get(EVALUATED_KEY_STRING));
        }

		String emailid = mapping.get("emailid").trim();		
		logger.info("Onecheck Wallet txn: fecthing onecheck txn "+ emailid);
		SimpleDateFormat finalformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate = null;
		Date endDate = null;
		Date edate = null;
		Calendar currentDateTime = null;
		if (mapping.get("startdate") == null  || mapping.get("enddate") == null) {
			try {
				edate = new Date();
				endDate = FCDateUtil.convertStringToDate(finalformat.format(edate), "yyyy-MM-dd HH:mm:ss");
				currentDateTime = Calendar.getInstance();
				currentDateTime.add(Calendar.DATE, -90);
				startDate = FCDateUtil.convertStringToDate(finalformat.format(currentDateTime.getTime()), "yyyy-MM-dd HH:mm:ss");
			} catch (Exception e) {
				logger.error("Exception while parsing date white wallettxn history for email : " + emailid, e);
				model.addAttribute("messages", "Invalide date formats");
				return view;
			} 	
			model.addAttribute("enddate", finalformat.format(edate));
			model.addAttribute("startdate", finalformat.format(currentDateTime.getTime()));
			model.addAttribute("emailid", emailid);
		} else {
			String fromDate = mapping.get("startdate").trim();
			String toDate = mapping.get("enddate").trim();
			model.addAttribute("startdate", fromDate);
			model.addAttribute("enddate", toDate);
			model.addAttribute("emailid", emailid);
			try {
				if (FCDateUtil.isValidDateTimeFormat(fromDate, "yyyy-MM-dd HH:mm:ss")) {
					startDate = FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempStart = finalformat.format(FCDateUtil.convertStringToDate(fromDate, "yyyy-MM-dd"));
					startDate = FCDateUtil.convertStringToDate(tempStart, "yyyy-MM-dd HH:mm:ss");
				}
				
				if (FCDateUtil.isValidDateTimeFormat(toDate, "yyyy-MM-dd HH:mm:ss")) {
					endDate = FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd HH:mm:ss");
				} else {
					String tempTo = finalformat.format(FCDateUtil.convertStringToDate(toDate, "yyyy-MM-dd"));
					endDate = FCDateUtil.convertStringToDate(tempTo, "yyyy-MM-dd HH:mm:ss");
				}
			} catch (Exception e) {
			    model.addAttribute("messages", "Date entered are not in proper format ");
				return view;
			}
		}

		//String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailid);
		String fcWalletId = oneCheckWalletService.getOneCheckIdentityFromIMS(emailid);
		logger.info("search txn -walletid for email "+ emailid +","+ fcWalletId);
		if (FCUtil.isEmpty(fcWalletId)) {
			model.addAttribute("messages", "Emailid is not valid one-check user.");
			return view;
		}

		List<Map<String, Object>> fcWalletMapList = new ArrayList<>();
		List<Map<String, Object>> klickPayMapList = new ArrayList<>();
		List<Map<String, Object>> consumerCreditTxnMapList = new ArrayList<>();
		List<Map<String, Object>> debitEMITxnMapList = new ArrayList<>();
		List<Map<String, Object>> upiTxnMapList = new ArrayList<>();
		try {
			logger.info("Started fetching onecheck txn emailid - " + emailid + ",startDate- " + startDate.toString() + ", endDate- " + endDate.toString());
            Map<String, Object> finalTransactionMap;
            if(lastEvaluatedKey == null) {
                finalTransactionMap = oneCheckUserService.findTransactionsFromUserId(fcWalletId, startDate, endDate, null);
            }
            else {
                finalTransactionMap = oneCheckUserService.findTransactionsFromUserId(fcWalletId, startDate, endDate, lastEvaluatedKey.toString());
            }
            
            List<ConsumerCreditTxnSummary> consumerCreditTxnSummaryList = this.consumerCreditService.getUserTransactions(emailid, startDate, endDate);
			List<OneCheckTransactionSummary>  fcwalletList = (List<OneCheckTransactionSummary>) finalTransactionMap.get("fcwallet");
			List<OneCheckTransactionSummary>  klickpayList = (List<OneCheckTransactionSummary>) finalTransactionMap.get("klickpay");
			List<UPITransactionView> upiTxnsList = upiTransactionService.getUPITransactionsViewDataByEmailId(emailid);
			List<DebitEMITxnSummary> debitEMITxnSummaryList = this.consumerCreditService.getUserDebitEMITransactions(emailid, startDate, endDate);
			
			logger.info("The fcwallet gave a list of txns and each has business txn type; listing all -> ");
			if(!FCUtil.isEmpty(fcwalletList)){
				for (OneCheckTransactionSummary ocs: fcwalletList){
					logger.info("Business transaction type for: (" +ocs.getTxnId() + ", " + ocs.getTxnType() + ", " + ocs.getBusinssTxnType() + ", " + ocs.getAmount());
				}
			}
            if(finalTransactionMap.containsKey(FC_WALLET_LAST_KEY_STRING) && finalTransactionMap.get(FC_WALLET_LAST_KEY_STRING)!=null) {
                lastEvaluatedKey = Integer.parseInt(finalTransactionMap.get(FC_WALLET_LAST_KEY_STRING).toString());
                model.addAttribute("nextkey",lastEvaluatedKey);
                model.addAttribute("prevkey",lastEvaluatedKey+(2*OneCheckUserService.DEFAULT_FCWALLET_TXN_COUNT));
            }
			else
			{
				if(lastEvaluatedKey!=null) {
					model.addAttribute("prevkey", lastEvaluatedKey + OneCheckUserService.DEFAULT_FCWALLET_TXN_COUNT);
				}
			}
			logger.info("Finished fetching one check txn for email : " + emailid);
			if (FCUtil.isEmpty(fcwalletList) && FCUtil.isEmpty(klickpayList)) {
				model.addAttribute("messages", "No transactions found for give email id : ");
				return view;
			} 
			logger.info("fcwallettxn list for email "+ emailid + "size of list : "+ fcwalletList.size());
			logger.info("klickpaytxn list for email "+ emailid + "size of list : "+ klickpayList.size());
			
			if (!FCUtil.isEmpty(fcwalletList)) {
				for (OneCheckTransactionSummary oneCheckTransactionSummary : fcwalletList) {
					 Map<String, Object> userTxnMap = new HashMap<>();
					 if(oneCheckUserService.isVCardTxn(oneCheckTransactionSummary)){
                        userTxnMap.put("type","vcard");
                     }
                     else if(oneCheckUserService.isP2PTxn(oneCheckTransactionSummary)){
                    	 userTxnMap.put("type","p2p-p2m");
                     }else{
                         userTxnMap.put("type","wallet");
                     }
		             userTxnMap.put("transactionDate", oneCheckTransactionSummary.getTransactionDate());
		             userTxnMap.put("merchantName",oneCheckTransactionSummary.getMerchantName());
		             userTxnMap.put("txnId",oneCheckTransactionSummary.getTxnId());
		             String txnType = oneCheckTransactionSummary.getTxnType();
		             String txnTypeString = getTransactionType(txnType);
		             if ("CREDIT".equalsIgnoreCase(txnTypeString)) {
		            	 userTxnMap.put("deposit", oneCheckTransactionSummary.getAmount());
		             } else if ("DEBIT".equalsIgnoreCase(txnTypeString)) {
		            	 userTxnMap.put("withdraw", oneCheckTransactionSummary.getAmount() - oneCheckTransactionSummary.getWithdrawalFee());
		            	 userTxnMap.put("withdrawFee", oneCheckTransactionSummary.getWithdrawalFee());
		             }
		             String txnid = oneCheckTransactionSummary.getTxnId();
		             if ("FREECHARGE".equalsIgnoreCase(oneCheckTransactionSummary.getMerchantName()) && validateOrderIdFormat(txnid)) {
		            	 userTxnMap.put("hyperlinkref", "yes");
		             } else  {
						 if ("FREECHARGE".equalsIgnoreCase(oneCheckTransactionSummary.getMerchantName()) && !FCUtil.isEmpty(txnid) && txnid.startsWith(ONE_CHECK_PREFIX)) {
							 userTxnMap.put("hyperlinkref", ONE_CHECK_PREFIX);
						 }else
						 	userTxnMap.put("hyperlinkref", "no");
		             }
		             userTxnMap.put("runningBalance",oneCheckTransactionSummary.getRunningBalance());
		             userTxnMap.put("transactionType",txnType);
		             userTxnMap.put("txnTypeToolTip", adminHelper.getTransactionStatusTypeDesc(txnType));
		             userTxnMap.put("idempotencyId", oneCheckTransactionSummary.getIdempotencyId());	
		             userTxnMap.put("eventContext", oneCheckTransactionSummary.getMetaData());
		             fcWalletMapList.add(userTxnMap);
		        }
			}
			
			if (!FCUtil.isEmpty(klickpayList)) {
				Map<String, OneCheckTransactionSummary> paymentMap = new HashMap<String,
						OneCheckTransactionSummary>();
				for (OneCheckTransactionSummary oneCheckTransactionSumm : klickpayList) {
					 if(isPGPaymentTxn(oneCheckTransactionSumm)) {
						 paymentMap.put(oneCheckTransactionSumm.getTxnId(), oneCheckTransactionSumm);
					 }
					 Map<String, Object> klickTxnMap = new HashMap<>();
					 klickTxnMap.put("transactionDate", oneCheckTransactionSumm.getTransactionDate());
					 klickTxnMap.put("merchantName",oneCheckTransactionSumm.getMerchantName());
					 klickTxnMap.put("txnId",oneCheckTransactionSumm.getTxnId());
					 klickTxnMap.put("txnamount", oneCheckTransactionSumm.getAmount());
					 klickTxnMap.put("transactionType", oneCheckTransactionSumm.getTxnType());
					 klickTxnMap.put("txnTypeToolTip", adminHelper.getTransactionStatusTypeDesc(oneCheckTransactionSumm.getTxnType()));
					 klickTxnMap.put("eventContext", oneCheckTransactionSumm.getMetaData());
					 klickTxnMap.put("pgtransactionid", oneCheckTransactionSumm.getPgTransactionId());
					 klickTxnMap.put("pgused", oneCheckTransactionSumm.getPgUsed());
					 klickTxnMap.put("pgunderlier", oneCheckTransactionSumm.getPgUnderlier());
					 if(isPGRefundTxn(oneCheckTransactionSumm)) {
						 OneCheckTransactionSummary paymentTransactionSummary = paymentMap.get(oneCheckTransactionSumm.
								 getTxnId());
						 if( null != paymentTransactionSummary) {
							 klickTxnMap.put("pgused", paymentTransactionSummary.getPgUsed());
							 klickTxnMap.put("pgunderlier", paymentTransactionSummary.getPgUnderlier());
						 }
					 }
					 String txnid = oneCheckTransactionSumm.getTxnId();
					if ("FREECHARGE".equalsIgnoreCase(oneCheckTransactionSumm.getMerchantName())
							&& validateOrderIdFormat(txnid)) {
						klickTxnMap.put("hyperlinkref", "yes");
					} else {
						if ("FREECHARGE".equalsIgnoreCase(oneCheckTransactionSumm.getMerchantName()) && !FCUtil
								.isEmpty(txnid) && txnid.startsWith(ONE_CHECK_PREFIX)) {
							klickTxnMap.put("hyperlinkref", ONE_CHECK_PREFIX);
						} else
							klickTxnMap.put("hyperlinkref", "no");
					}
					 klickPayMapList.add(klickTxnMap);
				 }
			}
			 if (!FCUtil.isEmpty(consumerCreditTxnSummaryList)) {
				 logger.info("consumer credit txn list for email "+ emailid + "size of list : "+ consumerCreditTxnSummaryList.size());
				 for (ConsumerCreditTxnSummary consumerCreditTxnSummary : consumerCreditTxnSummaryList) {
					 Map<String, Object> consumerCreditTxnMap = new HashMap<>();
					 consumerCreditTxnMap.put("timestamp", consumerCreditTxnSummary.getTimestamp());
					 consumerCreditTxnMap.put("merchantName", consumerCreditTxnSummary.getMerchantName());
					 consumerCreditTxnMap.put("merchantId", consumerCreditTxnSummary.getMerchantId());
					 consumerCreditTxnMap.put("fcOrderId", consumerCreditTxnSummary.getFcOrderId());
					 consumerCreditTxnMap.put("txnAmount", consumerCreditTxnSummary.getTxnAmount());
					 consumerCreditTxnMap.put("txnStatus", consumerCreditTxnSummary.getTxnStatus());
					 consumerCreditTxnMap.put("lenderTxnId", consumerCreditTxnSummary.getLenderTxnId());
					 consumerCreditTxnMap.put("lenderName", consumerCreditTxnSummary.getLenderName());
					 consumerCreditTxnMap.put("lenderId", consumerCreditTxnSummary.getLenderId());
					 consumerCreditTxnMap.put("emiTenure", consumerCreditTxnSummary.getEmiTenure());
					 consumerCreditTxnMap.put("emiAmount", consumerCreditTxnSummary.getEmiAmount());
					 consumerCreditTxnMap.put("interestRate", consumerCreditTxnSummary.getInterestRate());
					 consumerCreditTxnMap.put("creditAvailabilityStatus", consumerCreditTxnSummary.getCreditAvailabilityStatus());
					 consumerCreditTxnMap.put("totalApprovedCredit", consumerCreditTxnSummary.getTotalApprovedCredit());
					 consumerCreditTxnMap.put("creditCurrentlyAvailable", consumerCreditTxnSummary.getCurrentAvailableCredit());
					 consumerCreditTxnMap.put("loanDestination", consumerCreditTxnSummary.getLoanDestination());
					 consumerCreditTxnMap.put("processingFee", consumerCreditTxnSummary.getProcessingFee());
					 consumerCreditTxnMap.put("bankDetails", consumerCreditTxnSummary.getBankDetails());
					 consumerCreditTxnMap.put("accountNumber", consumerCreditTxnSummary.getAccountNumber());
					 consumerCreditTxnMap.put("loanAmount", (consumerCreditTxnSummary.getLoanAmount()));
					 consumerCreditTxnMapList.add(consumerCreditTxnMap);
				 }
			 }
			 
			 if (!FCUtil.isEmpty(debitEMITxnSummaryList)) {
				 logger.info("debit emi txn list for email "+ emailid + "size of list : "+ debitEMITxnSummaryList.size());
				 for (DebitEMITxnSummary debitEMITxnSummary : debitEMITxnSummaryList) {
					 Map<String, Object> debitEMITxnMap = new HashMap<>();
					 debitEMITxnMap.put("timestamp", debitEMITxnSummary.getTimestamp());
					 debitEMITxnMap.put("merchantName", debitEMITxnSummary.getMerchantName());
					 debitEMITxnMap.put("merchantId", debitEMITxnSummary.getMerchantId());
					 debitEMITxnMap.put("fcOrderId", debitEMITxnSummary.getFcOrderId());
					 debitEMITxnMap.put("txnAmount", debitEMITxnSummary.getTxnAmount());
					 debitEMITxnMap.put("txnStatus", debitEMITxnSummary.getTxnStatus());
					 debitEMITxnMap.put("lenderTxnId", debitEMITxnSummary.getLenderTxnId());
					 debitEMITxnMap.put("lenderName", debitEMITxnSummary.getLenderName());
					 debitEMITxnMap.put("lenderId", debitEMITxnSummary.getLenderId());
					 debitEMITxnMap.put("emiTenure", debitEMITxnSummary.getEmiTenure());
					 debitEMITxnMap.put("emiAmount", debitEMITxnSummary.getEmiAmount());
					 debitEMITxnMap.put("interestRate", debitEMITxnSummary.getInterestRate());
					 debitEMITxnMap.put("loanDestination", debitEMITxnSummary.getLoanDestination());
					 debitEMITxnMap.put("processingFee", debitEMITxnSummary.getProcessingFee());
					 debitEMITxnMap.put("loanAmount", (debitEMITxnSummary.getLoanAmount()));
					 debitEMITxnMapList.add(debitEMITxnMap);
				 }
			 }
			 
			 if (!FCUtil.isEmpty(upiTxnsList)) {
				 logger.info("upi txn list for email "+ emailid + "size of list : "+ upiTxnsList.size());
				 for (UPITransactionView upiTxnsView : upiTxnsList) {
					 Map<String, Object> upiTxnMap = new HashMap<>();
					 upiTxnMap.put("fcTxnId", upiTxnsView.getGlobalTxnId());
					 upiTxnMap.put("txnId", upiTxnsView.getMerchantOrderId());
					 upiTxnMap.put("txnStatus", upiTxnsView.getTxnStatus());
					 upiTxnMap.put("txnDate", upiTxnsView.getTransactionDate());
					 upiTxnMap.put("txnAmount", upiTxnsView.getTxnAmount());
					 upiTxnMap.put("sourceVPA", upiTxnsView.getUpiInfo().getSourceVpa());
					 upiTxnMap.put("destinationVPA", upiTxnsView.getUpiInfo().getDestVpa());
					 upiTxnMap.put("errorMsg", upiTxnsView.getUpiNpciErrorMessage());
					 upiTxnMap.put("businessUseCase", upiTxnsView.getBusinessUseCase());
					 upiTxnMap.put("errorCode", upiTxnsView.getUpiInfo().getErrCode());
					 upiTxnMap.put("promoCode", upiTxnsView.getPromoCode());
					 upiTxnMapList.add(upiTxnMap);
				 }
			 }
			 
		} catch (Exception e) {
			logger.error("Throwing an exception while checking oncheck txn data for email : " + emailid , e);
			model.addAttribute("msg", "Throwing an exception while checking oncheck txn data : ");
		}
		model.addAttribute("fcwalletmaplist", fcWalletMapList);
		model.addAttribute("klickpaymaplist", klickPayMapList);
		model.addAttribute("consumerCreditTxnMapList", consumerCreditTxnMapList);
		model.addAttribute("debitEMITxnMapList", debitEMITxnMapList);
		model.addAttribute("upiTxnMapList", upiTxnMapList);
	   return view;
	 }

	private Boolean isPGPaymentTxn(OneCheckTransactionSummary oneCheckTransactionSummary) {
		String txnType = oneCheckTransactionSummary.getTxnType();
		if(txnType.equals(OneCheckUserService.KlickPayTransactionHistoryType.PAY_FAILURE) ||
				txnType.equals(OneCheckUserService.KlickPayTransactionHistoryType.PAY_PENDING) ||
				txnType.equals(OneCheckUserService.KlickPayTransactionHistoryType.PAY_SUCCESS)) {
			return true;
		}
		return false;
	}

	private Boolean isPGRefundTxn(OneCheckTransactionSummary oneCheckTransactionSummary) {
		String txnType = oneCheckTransactionSummary.getTxnType();
		if(txnType.equals(OneCheckUserService.KlickPayTransactionHistoryType.REFUND_SUCCESS) ||
				txnType.equals(OneCheckUserService.KlickPayTransactionHistoryType.REFUND_FAILED)) {
			return true;
		}
		return false;
	}
	@RequestMapping(value = "OC/gettxndetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	OneCheckTransactionDetails getTransactionDetails(HttpServletRequest httpRequest) {
		String transactionId = httpRequest.getParameter("transactionId");
		return walletService.getTransactionDetails(transactionId, oneCheckService.getHeaders(httpRequest));
	}

	@RequestMapping(value = "OC/searchtxn", method = RequestMethod.GET)
	public String getOCTxn(@RequestParam final Map<String, String> mapping, final Model model,
						   final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "/admin/customertrail/addcash/searchbytxnid/view";
		
		if(isAuthorizedToUpdateCt(request)) {
        	model.addAttribute("isAuthorize", "yes");
        } 
		else {
        	model.addAttribute("isAuthorize", "no");
        }

		String transactionRef = request.getParameter("txnId");
		OneCheckTransactionDetails ocTxnDetails = walletService.getTransactionDetails(transactionRef, oneCheckService.getHeaders(request));
		List<TransactionSummary> txnSummaryList = oneCheckWalletService.findTransactionsByReference(transactionRef, ocTxnDetails.getUserId());
		List<Map<String, Object>> walletTxnList = new ArrayList<Map<String, Object>>();
		
		model.addAttribute("txnRef",transactionRef);

		if(!FCUtil.isEmpty(txnSummaryList)) {
			for(TransactionSummary txnSummary : txnSummaryList) {
				
				TransactionType txnType = txnSummary.getTransactionType();
				if(TransactionType.CREDIT_LOAD.equals(txnType)) {
					model.addAttribute("txnId", txnSummary.getTransactionId());
					break;
				}
			}
		}
		
		GetUserResponse user = oneCheckWalletService.getUserByID(ocTxnDetails.getUserId(), null, null);
		model.addAttribute("userId", user != null ? user.getUserDetails().getEmailId() : ocTxnDetails.getUserId());
		model.addAttribute("amount",ocTxnDetails.getAmount());
		model.addAttribute("txnDetailsList",ocTxnDetails.getDetails());
		
		return view;
	}
	
	@RequestMapping(value = "OC/reversetxn", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String reverseOCTxn(@RequestParam final Map<String, String> mapping, final Model model,
						   final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		
		String transactionId = request.getParameter("txnIdField");
		String reason = request.getParameter("reason");
		StringBuilder reversalErrorStatusMsg = new StringBuilder(FCConstants.WALLET_LOAD_REVERSAL_FAILURE);
		
		logger.info("Received request to reverse wallet add cash transaction id :" + transactionId);
		
		if(FCUtil.isEmpty(transactionId)) {
			reversalErrorStatusMsg.append("|Transaction ID is null. Kindly check if you can see the txn id in the add cash status screen.");
			logger.error(reversalErrorStatusMsg.toString());
			return reversalErrorStatusMsg.toString();
		}
			 
        if (!isAuthorizedToUpdateCt(request)) {
        	reversalErrorStatusMsg.append(getCurrentAdminUser(request) + " doesn't have write access to admin panel to reverse wallet add cash transaction id : " + request.getParameter("txnId"));
            logger.info(reversalErrorStatusMsg);
            return reversalErrorStatusMsg.toString();
        }
        
        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)){
            logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel to component " + request.getParameter("txnId"));
            return get403HtmlResponse(response);
            
        }
		
		if(FCUtil.isEmpty(reason)) {
			reason = FCConstants.DEFAULT_WALLET_LOAD_REVERSE_REASON;
		}
		
		try {
			logger.info("Reversing txn " + transactionId + "| Reason: " + reason);
			oneCheckWalletService.reverseWalletLoadMoney(transactionId, reason);
			return FCConstants.WALLET_LOAD_REVERSAL_SUCCESS;
		} catch (Exception e) {
			reversalErrorStatusMsg.append("Exception Occurred while reversing add cash transaction with ID: " + transactionId + "|ERROR :" + e.toString());
			logger.error(reversalErrorStatusMsg, e);
			return reversalErrorStatusMsg.toString();
		}
	}

	private String getTransactionType(String txnType) {
		if (FCUtil.isNotEmpty(txnType)) {
			String[] txnTypeStrings = txnType.split("_");
			for (String typeString : txnTypeStrings) {
				return typeString;
			}
		}
		return null;
	}
	
	private boolean validateOrderIdFormat(String referenceId) {
		if(FCUtil.isEmpty(referenceId)) {
			return false;
		}
		if (referenceId.length() >= 2) {
			String firstTwoChars = referenceId.substring(0, 2);
			if (FCConstants.afflicateMasterMap.containsValue(firstTwoChars)) {
				return true;
			}
		}
		return false;
	}
}