package com.freecharge.admin.controller;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.adminpage.controller.AdminPageController;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.web.util.ViewConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@Controller
@RequestMapping("/admin/promocode/utility/*")
public class PromocodeUtilitiesController extends AdminPageController{
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    public static final String PROMOCODE_UTILITY_LINK = "/admin/promocode/utility/home";

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.PROMOCODE_UTILITIES)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "home");
        return ViewConstants.PROMOCODE_UTILITY_HOME_VIEW;
    }

    @RequestMapping(value = "home/deleteCodes", method = RequestMethod.GET)
    public String deleteCodesView(Model model, HttpServletRequest request,
                                  HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.PROMOCODE_UTILITIES)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);
        model.addAttribute("page", "deleteCodes");
        return ViewConstants.PROMOCODE_UTILITY_HOME_VIEW;
    }


    @RequestMapping(value = "home/codesCount", method = RequestMethod.GET)
    public String codesCountView(Model model, HttpServletRequest request,
                                  HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND_GENERATE_UPLOAD)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);
        model.addAttribute("page", "codeCount");
        return ViewConstants.PROMOCODE_UTILITY_HOME_VIEW;
    }

    @RequestMapping(value = "codeCount", method = RequestMethod.POST)
    public String deleteCodes(@RequestParam(required = true, value = "class_id") Long classId) {
        return String.format("redirect:%s?message=%s", PROMOCODE_UTILITY_LINK,
                freefundService.getFreefundCodesForFreefundClassId(classId.longValue()) + "");
    }

    @RequestMapping(value = "deleteCodes", method = RequestMethod.POST)
    public String deleteCodes(@RequestParam("freefund_codes") MultipartFile file,
                              @RequestParam(required = true, value = "class_id") Long classId,
                              @RequestParam Map<String, String> mapping,
                              Model model, HttpServletRequest request) {
        Calendar cal = Calendar.getInstance();
        BufferedReader br = null;
        Map<String, FreefundCoupon> freefundCouponMap = new HashMap<>();
        try {
            br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line = "";
            while (!FCUtil.isEmpty(line = br.readLine())) {
            	logger.info("Line:" + line);
                FreefundCoupon freefundCoupon = new FreefundCoupon();
                freefundCoupon.setFreefundCode(line);
                freefundCoupon.setFreefundClassId(classId);
                freefundCoupon.setGeneratedDate(cal.getTime());
                freefundCouponMap.put(line, freefundCoupon);
            }
        } catch (IOException e) {
            logger.error("Error reading input file ", e);
            return String.format("redirect:%s?message=%s", PROMOCODE_UTILITY_LINK, e.getMessage());
        }
        
        logger.info("Freefund Coupon Map :" + freefundCouponMap.toString());
        
        String blockOnly = mapping.get("autoDetect");
        if(!FCUtil.isEmpty(freefundCouponMap)) {

            List<String> existingCodes =
                    freefundService.getFreefundCodesForFreefundClass
                            (new ArrayList(freefundCouponMap.keySet()), classId.longValue());
            logger.info("Existing codes found " + existingCodes.size());

            List<String> redeemedCodes =
                    freefundService.getRedeemedFreefundCoupons(new ArrayList(freefundCouponMap.keySet()),
                            classId.longValue());
            logger.info("Redeemed codes found " + redeemedCodes.size());

            Map<String, FreefundCoupon> nonExistingMap = new HashMap<>(freefundCouponMap);
            Map<String, FreefundCoupon> redeemedMap = null;

            if (!FCUtil.isEmpty(existingCodes)) {
                nonExistingMap.keySet().removeAll(existingCodes);
            }

            if (!FCUtil.isEmpty(redeemedCodes)) {
                redeemedMap = new HashMap<>(freefundCouponMap);
                redeemedMap.keySet().retainAll(redeemedCodes);
            }

            if (!FCUtil.isEmpty(nonExistingMap) && nonExistingMap.keySet().size() <= freefundCouponMap.keySet().size()) {
                freefundCouponMap.keySet().removeAll(nonExistingMap.keySet());
            }

            if (!FCUtil.isEmpty(redeemedMap) && redeemedMap.keySet().size() <= freefundCouponMap.keySet().size()) {
                freefundCouponMap.keySet().removeAll(redeemedMap.keySet());
            }

            if (FCUtil.isNotEmpty(blockOnly) && "on".equalsIgnoreCase(blockOnly)) {
                logger.info("Blocking codes " + freefundCouponMap.keySet().size());
                if(!freefundCouponMap.keySet().isEmpty())
                	freefundService.blockFreefundCodes(new ArrayList(freefundCouponMap.keySet()), classId.longValue());
            } else {
                logger.info("Deleting codes " + freefundCouponMap.keySet().size());
                if(!freefundCouponMap.keySet().isEmpty())
                	freefundService.deleteFreefundCodes(new ArrayList(freefundCouponMap.keySet()), classId.longValue());
            }

            emailService.sendFreeFundCodeDeleteOrBlockNotificationEmail(
                    freefundCouponMap.keySet().size(), nonExistingMap, redeemedMap,
                    "on".equalsIgnoreCase(blockOnly)? "blocked" : "deleted",
                    freefundService.getFreefundClass(classId.longValue()).getName());
        }
        return String.format("redirect:%s?message=%s", PROMOCODE_UTILITY_LINK, "Done processing. " +
                "Please check mails for updates.");
    }
}
