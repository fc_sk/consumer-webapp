package com.freecharge.admin.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.notification.email.SendEmailRequest;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.wallet.CreditsEncashDetailsService;
import com.freecharge.wallet.service.CreditsEncashDetails;

@Controller
@RequestMapping("/admin/tools/onecheck/bank/transfer/*")
public class CsOneCheckBankTransferController extends BaseAdminController {
	private static Logger logger = LoggingFactory.getLogger(CsOneCheckBankTransferController.class);
	public static final String ONECHECK_BANK_TRANS_STATUS = "/admin/tools/onecheck/bank/transfer/status/view.htm";
	public static final String CREDIT_ENCASH_STATUS_CHANGE_VIEW = "/admin/tools/onecheck/bank/transfer/upload/view.htm";

	@Autowired
	private OneCheckWalletService oneCheckWalletService;
	@Autowired
	private WalletService walletService;
	@Autowired
	@Qualifier("notificationApiServiceClient")
	private INotificationToolApiService notificationService;
	
	private static final int          ARRAY_SIZE_ZERO     = 0;
	private static final int          ARRAY_SIZE_ONE      = 1;
	private static final int          ARRAY_SIZE_TWO      = 2;
	private static final int          ARRAY_SIZE_THREE      = 3;
	private static final int          ARRAY_SIZE_FOUR      = 4;
	private static final int 		  ARRAY_SIZE_FIVE	  = 5;
	private static final int		  ARRAY_SIZE_SEVEN	  = 7;
	private static final int          ARRAY_SIZE_EIGHT    = 8;
	private static final int          ARRAY_SIZE_NINE     = 9;
	private static final int          ARRAY_SIZE_TEN     = 10;
	private static final int          ARRAY_SIZE_ELEVEN  = 11;
	
	private static final int          ARRAY_INDEX_ZERO    = 0;
	private static final int          ARRAY_INDEX_ONE     = 1;
	private static final int          ARRAY_INDEX_TWO     = 2;
	private static final int          ARRAY_INDEX_THREE     = 3;
	private static final int          ARRAY_INDEX_FOUR     = 4;
	private static final int	  	  ARRAY_INDEX_FIVE	  = 5;
	private static final int		  ARRAY_INDEX_SEVEN	  = 7;
	private static final int          ARRAY_INDEX_EIGHT   = 8;
	private static final int          ARRAY_INDEX_NINE    = 9;
	private static final int          ARRAY_INDEX_TEN    = 10;
	private static final int          ARRAY_INDEX_ELEVEN = 11;
	private static final String		  AUTO_TRANSFER_STRING = "AUTO_TRANSFER";
	private static final String 	  REFUNDED_TO_WALLET = " - FC Credits refunded to Wallet";
	private static final String		  STATUS_FAILED = "Failed";

	@Autowired
    private CreditsEncashDetailsService creditsEncashDetailsService;
	
	@RequestMapping(value = "status/view", method = RequestMethod.GET)
    public final String getBulkStatusCheckView(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        String view = "admin/tool/onecheck/status/view";
		if (!hasAccess(request, AdminComponent.ONECHECK_BANK_TRANS_STATUS)) {
            return get403HtmlResponse(response);
        }
        return view;
    }

	/*Credit encash bank data for registered emailid*/
	@RequestMapping(value = "searchbyemail/result", method = RequestMethod.GET)
	public String getCtOncheckTxn(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "admin/tool/onecheck/status/view";
		if (!hasAccess(request, AdminComponent.ONECHECK_BANK_TRANS_STATUS)) {
			get403HtmlResponse(response);
	        return null;
	    }
		String emailId = mapping.get("emailid").trim();
		model.addAttribute("emailid", emailId);
		if (FCUtil.isEmpty(emailId) || !(FCUtil.isEmailIdFormat(emailId))) {
			model.addAttribute("msg", "Enter a valid email Id. ");
			return view;
		}

		List<Map<String, Object>> creditEncashMapList = creditsEncashDetailsService.getCreditEncashDetails(emailId);
		if (FCUtil.isEmpty(creditEncashMapList)) {
			model.addAttribute("msg", "No credit Encash bank Deatails for above emailid");
			return view;	
		}
		if (creditEncashMapList.size() > 1) {
			logger.info("More than one bank records for above registered emailid :"+ emailId);
			model.addAttribute("msg", "More than one bank records for above registered emailid please inform dev.");
		}
		model.addAttribute("creditEncashList", creditEncashMapList);
		return view;
	}

	/*Credit encash bank data for entered emailid*/
	@RequestMapping(value = "searchby/enteredemail", method = RequestMethod.GET)
	public String getBankDataForEnteredEmail(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "admin/tool/onecheck/status/view";
		if (!hasAccess(request, AdminComponent.ONECHECK_BANK_TRANS_STATUS)) {
			get403HtmlResponse(response);
	        return null;
	    }
		String email = mapping.get("emailentered").trim();
		model.addAttribute("emailentered", email);
		if (FCUtil.isEmpty(email) || !(FCUtil.isEmailIdFormat(email))) {
			model.addAttribute("msg", "Enter a valid email Id. ");
			return view;
		}
		List<Map<String, Object>> creditEncashMapList = creditsEncashDetailsService.getEncashBanDataForEnteredEmail(email);
		logger.info("More than one bank records for above entered emailid :"+ email);
		if (FCUtil.isEmpty(creditEncashMapList)) {
			model.addAttribute("msg", "No credit Encash bank Deatails for above emailid");
			return view;	
		}
		if (creditEncashMapList.size() > 1) {
			logger.info("More than one bank records for above entered emailid :"+ email);
		}
		model.addAttribute("creditEncashList", creditEncashMapList);
		return view;
	}

	@RequestMapping(value = "upload/view", method = RequestMethod.GET)
    public final String getStatusUploadView(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        String view = "admin/tool/onecheck/upload/status/view";
		if (!hasAccess(request, AdminComponent.CREDIT_ENCASH_STATUS_CHANGE)) {
            return get403HtmlResponse(response);
        }
        return view;
    }
	
	@RequestMapping(value = "upload/do", method = RequestMethod.POST)
    public final String doStatusUpload(@RequestParam("bankdatauploadfile") final MultipartFile file, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String view = "admin/tool/onecheck/upload/status/view";
		if (!hasAccess(request, AdminComponent.CREDIT_ENCASH_STATUS_CHANGE)) {
            return get403HtmlResponse(response);
        }

		Set<String> fileStringSet = FCUtil.getStringSetFromFile(file);
		List<String> successUserIds = new ArrayList<>();
		List<String> failedUserIds = new ArrayList<>();
		for (String bankDataString : fileStringSet) {
			if (FCUtil.isEmpty(bankDataString)) {
				continue;
			}
			/*input : id(0), userId(1),bankAccountNo,ifscCode, beneficiaryName,emailId,referenceId,amount,transferStatus(8),transferDate(9),failureReason(10), utrNumber(11)*/
			String[] split = bankDataString.split(",");
			String id = null;
			String userId = null;
			String transferStatus = null;
			String transferDate = null;
			String failureReason = null;
			String utrNumber = null;
			String refundedToWallet = null;
			String emailid = null;
			String fcWalletId = null;
			String bankAccountNo = null;
			String ifscCode = null;
			String beneficiaryName = null;
			double amount = 0;
			int size = split.length;
			boolean isRefundedToBank = true;
			CreditsEncashDetails creditsEncashDetails = new CreditsEncashDetails();
			try {
				if (size > ARRAY_SIZE_ZERO) {
					id = split[ARRAY_INDEX_ZERO];
				}

				if (size > ARRAY_SIZE_ONE) {
					userId = split[ARRAY_INDEX_ONE];
				}

				if (size > ARRAY_SIZE_TWO) {
					bankAccountNo = split[ARRAY_INDEX_TWO];
				}

				if (size > ARRAY_SIZE_THREE) {
					ifscCode = split[ARRAY_INDEX_THREE];
				}

				if (size > ARRAY_SIZE_FOUR) {
					beneficiaryName = split[ARRAY_INDEX_FOUR];
				}

				if (size > ARRAY_SIZE_FIVE)
				{
					emailid = split[ARRAY_INDEX_FIVE];
				}

				if(size > ARRAY_SIZE_SEVEN)
				{
					amount = Double.parseDouble(split[ARRAY_INDEX_SEVEN]);
				}

				if (size > ARRAY_SIZE_EIGHT) {
					transferStatus = split[ARRAY_INDEX_EIGHT];
				}

				if (size > ARRAY_SIZE_NINE) {
					transferDate = split[ARRAY_INDEX_NINE];
				}

				if (size > ARRAY_SIZE_TEN) {
					failureReason = split[ARRAY_INDEX_TEN];
				}

				if (size > ARRAY_SIZE_ELEVEN) {
					utrNumber = split[ARRAY_INDEX_ELEVEN];
				}
				
				if (FCUtil.isEmpty(id) || FCUtil.isEmpty(userId)
						|| FCUtil.isEmpty(transferStatus) ||FCUtil.isEmpty(emailid) || amount<=0) {
					failedUserIds.add(userId);
					continue;
				}
				creditsEncashDetails.setId(Integer.parseInt(id));
				creditsEncashDetails.setUserId(Integer.parseInt(userId));
				creditsEncashDetails.setTransferStatus(transferStatus);
				creditsEncashDetails.setTransferDate(FCDateUtil
						.convertStringToDate(transferDate, "yyyy-MM-dd HH:mm:ss"));
				creditsEncashDetails.setAmount(amount);
				creditsEncashDetails.setBankAccountNo(bankAccountNo);
				creditsEncashDetails.setIfscCode(ifscCode);
				creditsEncashDetails.setBeneficiaryName(beneficiaryName);
				creditsEncashDetails.setEmailId(emailid);
				creditsEncashDetails.setFailureReason(failureReason);
				creditsEncashDetails.setUtrNumber(utrNumber);
				fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailid);
			} catch (Exception ex) {
				continue;
			}
			if(transferStatus!=null && !transferStatus.isEmpty() && fcWalletId!=null && !fcWalletId.isEmpty())
			{
				if(transferStatus.equalsIgnoreCase(STATUS_FAILED)) {
					logger.info("Starting refund to OCW for userId: " + userId);
					Map<String, Object> refundResponseMap = oneCheckWalletService.creditToOneCheckWallet(Long.parseLong(userId), amount, emailid, emailid, fcWalletId);
					if (refundResponseMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS)) {
						transferStatus = AUTO_TRANSFER_STRING;
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Date currentDate = new Date();
						transferDate = df.format(currentDate);
						creditsEncashDetails.setTransferStatus(transferStatus);
						creditsEncashDetails.setTransferDate(FCDateUtil.convertStringToDate(transferDate, "yyyy-MM-dd HH:mm:ss"));
						refundedToWallet = REFUNDED_TO_WALLET;
						isRefundedToBank = false;
						logger.info("Successfully finished crediting to OCW for userId: " + userId + ", amount: " + amount + ", emailId: " + emailid + ", transferStatus: " + transferStatus + ", transferDate: " + currentDate.toString());
					}
				}
				else
				{
					refundedToWallet = "";
				}
			}
			boolean status = false;
			try {
				logger.info("Initiating updating of status for userId: "+userId);
				logger.info("userId: "+userId+", amount: "+amount+", emailId: "+emailid+", transferStatus: "+transferStatus+", transferDate: "+transferDate);
				status = creditsEncashDetailsService.updateTransactionStatus(creditsEncashDetails);
				if (status) {
					successUserIds.add(userId + refundedToWallet);
					logger.info("Successfully updated status for userId: "+userId);
				} else {
					failedUserIds.add(userId + refundedToWallet);
					logger.info("Failed to update status for userId: "+userId);
				}
			} catch (Exception ex) {
				failedUserIds.add(userId);
				logger.error("Failed to update status for userId: " + userId + " reason: ", ex);
			}
			sendStatusChangeEmail(isRefundedToBank, creditsEncashDetails);
		}
		model.addAttribute("successCount", successUserIds.size());
		model.addAttribute("failureCount", failedUserIds.size());
		model.addAttribute("successUserIds", successUserIds);
		model.addAttribute("failedUserIds", failedUserIds);
		return view;
    }

	public void sendStatusChangeEmail(boolean refundToBankStatus, CreditsEncashDetails creditsEncashDetails)
	{
		List<String> tags = new ArrayList<String>();
		tags.add(creditsEncashDetails.getEmailId());
		tags.add(creditsEncashDetails.getBankAccountNo());
		SendEmailRequest emailRequest = new SendEmailRequest();
		Map<String,Object> emailContentDataMap = new HashMap<String,Object>();
		emailContentDataMap.put("beneficiary", FCStringUtils.capitalizeWords(creditsEncashDetails.getBeneficiaryName()));
		emailContentDataMap.put("ifscCode",creditsEncashDetails.getIfscCode());
		emailContentDataMap.put("accountNo",creditsEncashDetails.getBankAccountNo());
		emailContentDataMap.put("amount",creditsEncashDetails.getAmount());
		emailContentDataMap.put("utrNo",creditsEncashDetails.getUtrNumber());
		emailRequest.setContentTemplateDataMap(emailContentDataMap);
		if(refundToBankStatus) {
			logger.info("Sending email to: "+creditsEncashDetails.getEmailId() + " for successful refund to bank");
			emailRequest.setEmailId(Integer.parseInt(fcProperties.getEmailIdOcwMigrationSuccessfulRefundToBank()));
		}
		else{
			logger.info("Sending email to: "+creditsEncashDetails.getEmailId() + " for failed refund to bank");
			emailRequest.setEmailId(Integer.parseInt(fcProperties.getEmailIdOcwMigrationFailedRefundToBank()));
		}

		Map<String,Object> emailSubjectDataMap = new HashMap<String,Object>();
		emailRequest.setSubjectTemplateDataMap(emailSubjectDataMap);
		emailRequest.setToEmail(creditsEncashDetails.getEmailId());
		emailRequest.setTags(tags);
		notificationService.sendEmail(emailRequest);
	}

	@RequestMapping(value = "download/do", method = RequestMethod.GET)
    public final String doStatusUpload(final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CREDIT_ENCASH_STATUS_CHANGE)) {
            return get403HtmlResponse(response);
        }
        List<Map<String, Object>> creditEncashBankDetailList = creditsEncashDetailsService.getCreditEncashDetailsNotSuccess();
        List<CreditsEncashDetails> creditsEncashDetailsList = new ArrayList<>();  
        for (Map<String, Object> creditEncashMap : creditEncashBankDetailList) {
        	CreditsEncashDetails creditsEncashDetails = new CreditsEncashDetails();
        	creditsEncashDetails.setId((Integer)creditEncashMap.get("id"));
        	creditsEncashDetails.setUserId((Integer)creditEncashMap.get("user_id"));
        	creditsEncashDetails.setBankAccountNo((String)creditEncashMap.get("bank_account_no"));
        	creditsEncashDetails.setIfscCode((String)creditEncashMap.get("ifsc_code"));
        	creditsEncashDetails.setBeneficiaryName((String)creditEncashMap.get("beneficiary_name"));
        	creditsEncashDetails.setEmailId((String)creditEncashMap.get("email_id"));
        	creditsEncashDetails.setReferenceId((String)creditEncashMap.get("reference_id"));
        	BigDecimal amountBg = (BigDecimal)creditEncashMap.get("amount");
        	creditsEncashDetails.setAmount(amountBg.doubleValue());
        	creditsEncashDetails.setTransferStatus((String)creditEncashMap.get("transfer_status"));
        	creditsEncashDetails.setTransferDate((Date)creditEncashMap.get("transfer_date"));
        	creditsEncashDetails.setFailureReason((String)creditEncashMap.get("failure_reason"));
        	creditsEncashDetails.setUtrNumber((String)creditEncashMap.get("utr_number"));
        	creditsEncashDetailsList.add(creditsEncashDetails);
        }
        /*Format id,userId,bankAccountNo,ifscCode,beneficiaryName,emailId,referenceId,amount,transferStatus,transferDate,failureReason;*/
        model.addAttribute("excelCreditEncashData", creditsEncashDetailsList);
		return "creditsEncashBankExcelSummary";
	}
}
