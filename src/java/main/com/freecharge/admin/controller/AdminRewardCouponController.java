package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.model.RewardCouponForm;
import com.freecharge.admin.model.RewardCouponMasterData;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.util.FCUtil;
import com.freecharge.coupon.service.CouponServiceProxy;
import com.freecharge.freefund.dos.entity.PromocodeRedeemedDetails;
import com.freecharge.freefund.services.PromocodeRedeemDetailsService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/reward-coupon/search/*")
public class AdminRewardCouponController extends BaseAdminController {
	public static final String REWARD_COUPON_SEARCH_LINK = "/admin/reward-coupon/search/search.htm";
	
	@Autowired
	private RewardCouponMasterData rewardCouponMasterData;
	
	   
	   @Autowired
	   private PromocodeRedeemDetailsService promocodeRedeemDetailsService;
	   
	   @Autowired
	   private UserServiceProxy userServiceProxy;
	   
	   @Autowired
	   @Qualifier("couponServiceProxy")
	   private ICouponService couponServiceProxy;
	   
	
	 @RequestMapping(value = "search", method = RequestMethod.GET)
	 public final String getRewardCouponDataView(final Model model, final HttpServletRequest request, 
			 final HttpServletResponse response) throws Exception {
		 if (!hasAccess(request, AdminComponent.REWARD_COUPON_SEARCH)) {
	            get403HtmlResponse(response);
	            return null;
	        }
		 
		 RewardCouponForm rewardCouponForm = new RewardCouponForm(); 
		 rewardCouponForm.setRewardCouponMasterData(rewardCouponMasterData);	 
		 model.addAttribute("rewardCouponForm", rewardCouponForm);
	
		 if (!hasAccess(request, AdminComponent.REWARD_COUPON_SEARCH)) {
			 get403HtmlResponse(response);
			 return null;
		 }
		 return ViewConstants.REWARD_COUPON_SEARCH_VIEW;
	 }
	
	 @RequestMapping(value = "display", method = RequestMethod.POST)
	 public final String getRewardCouponData(RewardCouponForm rewardCouponForm, final Model model,
	            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
	        if (!hasAccess(request, AdminComponent.REWARD_COUPON_SEARCH)) {
	            get403HtmlResponse(response);
	            return null;
	        }
	        rewardCouponForm.setRewardCouponMasterData(rewardCouponMasterData);
	        model.addAttribute("rewardCouponForm", rewardCouponForm);
	        List<String> messages = new LinkedList<String>();
	         
	        Map<String, Object> validationMap = isValidationSuccess(rewardCouponForm);
	        boolean valid = (boolean)validationMap.get("valid");
	        if (!valid) {
	        	model.addAttribute("messages", validationMap.get("messages"));
	        	return ViewConstants.REWARD_COUPON_SEARCH_VIEW;
	        }
	        
	        String email = rewardCouponForm.getEmailId();
	        Integer campaignId = rewardCouponForm.getCampaignId();
	        
	        Users user = userServiceProxy.getUserByEmailId(email);
	        if (user == null) {
	        	messages.add("Email id is not in our database");
	        	model.addAttribute("messages", messages);
	        	return ViewConstants.REWARD_COUPON_SEARCH_VIEW;
	        }
	        long userId = user.getUserId();
	        List<PromocodeRedeemedDetails> promocodeRedeemDetailList =  promocodeRedeemDetailsService.getPromocodeRedeemDetailsByUserIdAndCampaign(userId, campaignId);
	        List<List<CouponHistory>> rewardCouponLists = new ArrayList<>();
	        
	        if(FCUtil.isEmpty(promocodeRedeemDetailList)) {
	        	messages.add("No record found for this campaign name ");
	        	model.addAttribute("messages", messages);
	        	return ViewConstants.REWARD_COUPON_SEARCH_VIEW;
	        }
	        
	        for (PromocodeRedeemedDetails promocodeRedeemedDetails : promocodeRedeemDetailList) {
	        	if (promocodeRedeemedDetails != null) {
	        		List<CouponHistory> couponHistory = couponServiceProxy.getCouponsForOrder(promocodeRedeemedDetails.getOrderId());
	        		if(couponHistory!=null) {
	        			rewardCouponLists.add(couponHistory);
	        		}
	        	}
	        }
	    
	        model.addAttribute("rewardCouponLists", rewardCouponLists);     
	        if (promocodeRedeemDetailList.get(0) != null) {
	        	model.addAttribute("rewardCampaign", promocodeRedeemDetailList.get(0).getName());
	        	model.addAttribute("freefundCode", promocodeRedeemDetailList.get(0).getFreefundCode());
	        }
	        
	        model.addAttribute("emailId", rewardCouponForm.getEmailId());
	        return ViewConstants.REWARD_COUPON_SEARCH_VIEW;
	 }

	private Map<String, Object> isValidationSuccess(RewardCouponForm rewardCouponForm) {
		Map<String, Object> validationMap = new HashMap<>(); 
		if (FCUtil.isEmpty(rewardCouponForm.getEmailId())) {
			validationMap.put("messages", "Email id is required");
			validationMap.put("valid", false);
        } else if(rewardCouponForm.getCampaignId() == null || rewardCouponForm.getCampaignId().toString().isEmpty()) {
        	validationMap.put("messages", "Select any reward campaign ");
			validationMap.put("valid", false);
        } else if(!FCUtil.isEmailIdFormat(rewardCouponForm.getEmailId())) {
        	 validationMap.put("messages", "Valid Email id is required ");
 			 validationMap.put("valid", false);
        } else {
			 validationMap.put("valid", true);
        }
		return validationMap;
	}	
}
