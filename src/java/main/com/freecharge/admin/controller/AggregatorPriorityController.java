package com.freecharge.admin.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.AggrPriorityTemplateService;
import com.freecharge.app.service.InCachedService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/aggregatorPriority")
public class AggregatorPriorityController extends BaseAdminController {

    @Autowired
    private OperatorCircleReadDAO  operatorCircleDao;

    @Autowired
    private OperatorCircleWriteDAO operatorCircleWriteDao;

    @Autowired
    private InCachedService        cachedService;

    @Autowired
    private OperatorCircleService  operatorCircleService;

    @Autowired
    private EmailService           emailService;
    
    @Autowired
    private AggrPriorityTemplateService aggrPriorityTemplateService;

    public static final String     AGGREGATOR_PRIORITY_URL = "/admin/aggregatorPriority.htm";
    
    List<String> aggregators = Arrays.asList(new String[]{"euronet", "oxigen", "default"});
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView all(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.AGGREGATOR_PRIORITY)) {
            get403HtmlResponse(response);
            return null;
        }

        ModelAndView modelAndView = new ModelAndView();
        List<OperatorMaster> operators = operatorCircleDao.getOperators();
        List<CircleMaster> circles = operatorCircleDao.getCircles();
        Iterator<CircleMaster> it = circles.iterator();
        while (it.hasNext()) {
            CircleMaster cm = it.next();
            if (cm != null) {
                String cmName = cm.getName();
                if (cmName != null) {
                    cmName = cmName.trim();
                }
                if ("all".equalsIgnoreCase(cmName)) {
                    it.remove();
                }
            }
        }

        modelAndView.addObject("operators", operators);
        modelAndView.addObject("circles", circles);
        modelAndView.addObject("operatorsMap", getOperatorsMap(operators));
        modelAndView.addObject("circlesMap", getCirclesMap(circles));
        List<CircleOperatorMapping>  circleOperatorMappings = null;
        
        String template = request.getParameter("template");
        if(template != null){
            circleOperatorMappings = aggrPriorityTemplateService.getTemplate(template);
        } else {
            circleOperatorMappings = operatorCircleDao.getCircleOperatorMapping();
        }
        Collections.sort(circleOperatorMappings, new Comparator<CircleOperatorMapping>() {
            @Override
            public int compare(CircleOperatorMapping o1, CircleOperatorMapping o2) {
                return (o1.getCircleOperatorMappingId() - o2.getCircleOperatorMappingId());
            }
        });
        modelAndView.addObject("circleOperatorMappings", circleOperatorMappings);
        modelAndView.addObject("templates", aggrPriorityTemplateService.getAllTemplates());
        
        modelAndView.setViewName(ViewConstants.AGGREGATOR_PRIORITY);
        return modelAndView;
    }

    private Object getCirclesMap(List<CircleMaster> circles) {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (CircleMaster circleMaster : circles) {
            map.put(circleMaster.getCircleMasterId(), circleMaster.getName());
        }
        return map;
    }

    private Map<Integer, String> getOperatorsMap(List<OperatorMaster> operators) {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (OperatorMaster operatorMaster : operators) {
            map.put(operatorMaster.getOperatorMasterId(), operatorMaster.getName());
        }
        return map;
    }

    private static <T extends Set<String>> T splitPriority(T tokens, String commaDelimitedPriority) {
        String[] split = new String[] {};
        try {
            split = commaDelimitedPriority.split(",");
        } catch (NullPointerException e) {
        }
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].trim();
        }
        tokens.addAll(Arrays.asList(split));
        return tokens;
    }

    private List<InOprAggrPriority> getInOprAggrPriorityList(int operatorId, int circleId) {
        List<CircleOperatorMapping> allCom = operatorCircleDao.getCircleOperatorMapping();
        Iterator<CircleOperatorMapping> allComIt = allCom.iterator();
        Set<Integer> allComIDs = new HashSet<Integer>();
        CircleOperatorMapping com = null;
        while (allComIt.hasNext()) {
            com = allComIt.next();
            if ((operatorId >= 0 && operatorId != com.getFkOperatorMasterId())
                    || (circleId >= 0 && circleId != com.getFkCircleMasterId())) {
                allComIt.remove();
            } else {
                allComIDs.add(com.getCircleOperatorMappingId());
            }
        }

        List<InOprAggrPriority> ps = operatorCircleDao.getInOprAggrPriorities();
        Iterator<InOprAggrPriority> pit = ps.iterator();
        while (pit.hasNext()) {
            InOprAggrPriority p = pit.next();
            if (!allComIDs.contains(p.getFkcircleOperatorMappingId())) {
                pit.remove();
            }
        }
        return ps;
    }

    @RequestMapping(value = "{operatorId}/{circleId}/aggregators", method = RequestMethod.GET, produces = "application/json")
    public String getAggregatorPriority(@PathVariable int operatorId, @PathVariable int circleId, Model model) {
        LinkedHashSet<String> priorities = new LinkedHashSet<String>();
        if (operatorId >= 0 && circleId >= 0) {
            // individual operator and circle
            CircleOperatorMapping com = operatorCircleDao.getCircleOperatorMapping(operatorId, circleId);
            InOprAggrPriority p = operatorCircleDao.getInOprAggrPriority(com.getCircleOperatorMappingId());
            priorities = splitPriority(priorities, p.getAggrPriority());
        } else {
            // at least one of operatorId and circleId is "ALL" (negative value
            // implies ALL)
            List<InOprAggrPriority> ps = getInOprAggrPriorityList(operatorId, circleId);
            Set<String> added = new HashSet<String>();
            for (InOprAggrPriority p : ps) {
                String pstr = p.getAggrPriority();
                if (!added.contains(pstr)) {
                    priorities = splitPriority(priorities, pstr);
                    added.add(p.getAggrPriority());
                }
            }
        }
        model.addAttribute("data", priorities.toArray(new String[] {}));
        return "jsonView";
    }

    @RequestMapping(value = "{operatorId}/{circleId}/aggregators", method = RequestMethod.POST, produces = "application/json")
    public String setAggregatorPriority(@PathVariable int operatorId, @PathVariable int circleId,
            @RequestParam("ag[]") String[] aggregators, Model model, Principal principal, HttpServletRequest request) {
        StringBuilder agstr = new StringBuilder();
        String aggregatorName = aggregators[0];
        String currentAdminUser = getCurrentAdminUser(request);
        for (String each : aggregators) {
            if (agstr.length() > 0) {
                agstr.append(',');
            }
            agstr.append(each);
        }
        if (operatorId >= 0 && circleId >= 0) {
            // individual operator and circle
            int comId = operatorCircleDao.getCircleOperatorMapping(operatorId, circleId).getCircleOperatorMappingId();
            operatorCircleWriteDao.setInOprAggrPriority(Arrays.asList(new Integer[] { comId }), agstr.toString(),
                    currentAdminUser);
        } else {
            // at least one of operatorId and circleId is "ALL" (negative value
            // implies ALL)
            List<InOprAggrPriority> ps = getInOprAggrPriorityList(operatorId, circleId);
            List<Integer> comIDs = new ArrayList<Integer>();
            for (InOprAggrPriority p : ps) {
                comIDs.add(p.getFkcircleOperatorMappingId());
            }
            operatorCircleWriteDao.setInOprAggrPriority(comIDs, agstr.toString(), currentAdminUser);
        }
        sendEmail(aggregatorName, operatorId, circleId, currentAdminUser);
        cachedService.deleteInOprAggrPriority();
        return "jsonView";
    }
    
    @RequestMapping(value = "/activatetemplate", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> activateTemplate(Model model, Principal principal, HttpServletRequest request) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        String templateName = request.getParameter("template");
        String currentAdminUser = getCurrentAdminUser(request);
        List<CircleOperatorMapping> circleOperatorMappings = aggrPriorityTemplateService.getTemplate(templateName);
        int[] updatesStatus = null;
        if(circleOperatorMappings != null && circleOperatorMappings.size() > 0){
            updatesStatus = operatorCircleWriteDao.batchUpdateAGPriority(circleOperatorMappings, currentAdminUser, null);
        }
        respMap.put("updates", updatesStatus!=null?sum(updatesStatus):0);
        cachedService.deleteInOprAggrPriority();
        return respMap;
    }
    
    @RequestMapping(value = "/deletetemplate", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> deleteTemplate(Model model, Principal principal, HttpServletRequest request) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        String templateName = request.getParameter("template");
        aggrPriorityTemplateService.deleteTemplate(templateName);
        respMap.put("updates", "Template deleted");
        return respMap;
    }
    
    @RequestMapping(value = "/createtemplate", method = RequestMethod.POST)
    public String createTemplate(Model model, Principal principal, HttpServletRequest request) {
        String templateName = request.getParameter("template_name");
        
        String[] ids = request.getParameterValues("chk");
        if(ids == null) {
            return "redirect:/admin/aggregatorPriority?message=Please select the entry using checkbox";
        }
        Map<Integer, String> inputPriorityMap = new HashMap<Integer, String>();
        for(String id : ids) {
            inputPriorityMap.put(Integer.valueOf(id), request.getParameter("prts"+id));
        }
        
        Map<Integer, CircleOperatorMapping> circleOperatorMap = operatorCircleDao.getCircleOperatorMap();
        checkAndSetUpdatePriority(circleOperatorMap, inputPriorityMap);
        
        aggrPriorityTemplateService.saveTemplate(templateName, circleOperatorMap);
        
        return "redirect:/admin/aggregatorPriority";
    }
    
    private void checkAndSetUpdatePriority(Map<Integer, CircleOperatorMapping> circleOperatorMap,
            Map<Integer, String> inputPriorityMap) {

        for (Integer id : inputPriorityMap.keySet()) {

            String alteredPriority = inputPriorityMap.get(id);

            if (StringUtils.isNotEmpty(alteredPriority)) {
                CircleOperatorMapping entry = circleOperatorMap.get(id);
                String availability = entry.getAggrPriority();
                String finalPriorityOrder = removeUnavailableAGs(alteredPriority, availability);
                if (StringUtils.isNotEmpty(finalPriorityOrder)) {
                    entry.setAggrPriority(finalPriorityOrder);
                }
            }
        }
    }

    private String removeUnavailableAGs(String alteredPriority, String availability) {
        String finalString = "";
        List<String> availabilityList = Arrays.asList(availability.split("[,]"));
        List<String> agPriorityList = Arrays.asList(alteredPriority.split("[,]"));
        for(String ag : agPriorityList) {
            if(availabilityList.contains(ag)) {
                finalString += ag + ",";
            }
        }
        if (StringUtils.isNotEmpty(finalString)) {
            finalString = finalString.substring(0, finalString.length() - 1);
        }
        return finalString;
    }

    @RequestMapping(value = "/move/to/{aggregator}", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> moveAllowedToEuronet(@PathVariable String aggregator, Model model, Principal principal, HttpServletRequest request) {
        Map<String, Object> respMap = new HashMap<String, Object>();
        
        if(!aggregators.contains(aggregator)) {
            respMap.put("Allowed Aggregator Options", aggregators);
            return respMap;
        }
        respMap.put("aggregator", aggregator);
        String currentAdminUser = getCurrentAdminUser(request);
        List<CircleOperatorMapping> circleOperatorMappings = null;
        int[] updatesStatus = null;
        if ("default".equals(aggregator)) {
            circleOperatorMappings = operatorCircleDao.getCircleOperatorMapping();
            if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
                updatesStatus = operatorCircleWriteDao.batchUpdateAGPriority(circleOperatorMappings, currentAdminUser, null);
            }
        } else {
            circleOperatorMappings = operatorCircleDao.getCircleOperatorMapping(aggregator);
            if (circleOperatorMappings != null && circleOperatorMappings.size() > 0) {
                updatesStatus = operatorCircleWriteDao.batchUpdateAGPriority(circleOperatorMappings, currentAdminUser, aggregator);
            }
        }
        respMap.put("updates", updatesStatus!=null?sum(updatesStatus):0);
        cachedService.deleteInOprAggrPriority();
        return respMap;
    }

    private int sum(int[] updatesStatus) {
        int sum = 0;
        for(int i : updatesStatus) {
            sum+=i;
        }
        return sum;
    }

    public final void sendEmail(final String aggregatorName, final int operatorId, final int circleId,
            final String currentAdminUser) {
        logger.info("In sending mail for id: " + operatorId);
        try {
            OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorId);
            CircleMaster circleMaster = operatorCircleService.getCircle(circleId);
            String emailContent = null;

            StringBuffer sbf = new StringBuffer("");
            if (operatorMaster != null && circleMaster != null) {
                emailContent = "Aggregator Priority changed for Operator: " + operatorMaster.getName() + "<br>\n" + "to Aggregator: "
                        + aggregatorName + "<br>\n" + "for Circle: " + circleMaster.getName();
                EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
                emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.AGGREGATOR_PRIORITY_CHANGED_SUBJECT)
                        + operatorMaster.getName()+"/"+circleMaster.getName()+" to "+aggregatorName);
                sbf.append("<br>\n");
                sbf.append(emailContent);
                sbf.append("<br>\n");
                sbf.append("<br>\n");
                sbf.append("Changed By: " + currentAdminUser);

                emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.AGGREGATOR_PRIORITY_REPLYTO));
                emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.AGGREGATOR_PRIORITY_FROM));
                emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.AGGREGATOR_PRIORITY_REPLYTO));
                emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
                emailBusinessDO.setContent(sbf.toString());
                emailService.sendNonVelocityEmail(emailBusinessDO);
            }
        } catch (RuntimeException e) {
            logger.error("Error while Sending Email. Stack Trace=", e);
        }
    }

    private static final Logger logger = LoggingFactory.getLogger(AggregatorPriorityController.class);
}
