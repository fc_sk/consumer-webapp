package com.freecharge.admin.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.service.PrefixListService;
import com.freecharge.admin.service.PrefixListService.PrefixBean;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/prefixList")
public class PrefixListController {

	public static final String AGGREGATOR_PRIORITY_URL = "/admin/prefixList.htm";

	private static Logger logger = LoggingFactory.getLogger(PrefixListController.class);

	@Autowired
	private PrefixListService service;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView mainPage() {
		return new ModelAndView(ViewConstants.PREFIX_LIST);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ModelAndView process(@RequestParam("uploadFile") MultipartFile file,
			@RequestParam(required=false, value="ignoreFirst") boolean ignoreFirst,
			@RequestParam(required=false, value="effectChanges") boolean effectChanges) throws IOException {
		System.out.println(String.format("ignoreFirst %s, effectChanges %s", ignoreFirst, effectChanges));
		ModelAndView mav = new ModelAndView(ViewConstants.PREFIX_LIST);
		BufferedReader br = new BufferedReader(new StringReader(new String(file.getBytes())));
		String each;
		int n = 0;
		List<PrefixBean> prefixes = new LinkedList<PrefixListService.PrefixBean>();
		try {
			while ((each = br.readLine()) != null) {
				n++;
				if (!(n == 1 && ignoreFirst)) {
					prefixes.add(service.parse(each));
				}
			}
			if (n == 0) {
				throw new IllegalArgumentException("Please upload a non-empty file");
			}
			service.process(prefixes, effectChanges);
			mav.addObject("result", String.format("Ingested %d rows", n));
		} catch(RuntimeException e) {
			logger.error("Error occured in Parse/update", e);
			mav.addObject("result", "ERROR: " + e.getMessage());
		}
		return mav;
	}
}
