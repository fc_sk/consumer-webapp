package com.freecharge.admin.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.api.coupon.service.model.CouponCode;
import com.freecharge.api.coupon.service.model.CouponInventory;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.api.coupon.service.web.model.CouponStore.CouponNature;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.app.service.CommonService;
import com.freecharge.app.service.CrosssellService;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/admin/crosssell/*")
public class CrosssellAdminController extends BaseAdminController {
	private Logger logger = LoggingFactory.getLogger(getClass().getName());
	public static final String HOME_LINK = "/admin/crosssell/home.htm";
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private CrosssellService crosssellService;

	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, HttpServletRequest request,
                           HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.CROSS_SELL)){
            return get403HtmlResponse(response);
        }
		int crosssellId = -1;
		try {
			crosssellId = Integer.parseInt(mapping.get("crosssell_id"));
		}
		catch (Exception e) {}
		if (crosssellId != -1) {
			CrosssellBusinessDo crosssellBusinessDo = crosssellService.getCrosssell(crosssellId);
			CouponStore couponStore = crosssellService.getCouponStore(crosssellBusinessDo.getStoreId());
			model.addAttribute(WebConstants.ATTR_STORED_CITY_LIST,
					couponStore != null && couponStore.getCityIdList() != null ?Arrays.asList(couponStore.getCityIdList().split(",")): "");
			crosssellBusinessDo.setCouponStore(couponStore);
			model.addAttribute(WebConstants.ATTR_CROSSSELL_BUSSINESS_DO, crosssellBusinessDo);
		}
		List<CrosssellBusinessDo> crosssellBusinessDos = crosssellService.getAllCrosssell();
		model.addAttribute(WebConstants.ATTR_CROSSSELL_BUSSINESS_DO_LIST, crosssellBusinessDos);
		List<CityMaster> cities = commonService.getCities();
		model.addAttribute(WebConstants.ATTR_CITY_LIST, cities);
		return ViewConstants.CROSSSELL_ADMIN_HOME_VIEW;
	}
	
	@RequestMapping(value = "create", method = RequestMethod.POST)
	public String create(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                         HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CROSS_SELL)){
            return get403HtmlResponse(response);
        }
		boolean status = false;
		String messages = "";

		try {

			Crosssell crosssell = new Crosssell();
			crosssell.setCategory(mapping.get("category"));
			crosssell.setCountUses(1);
			crosssell.setCreatedBy("admin");
			Calendar cal = Calendar.getInstance();
			crosssell.setCreatedDate(new Timestamp(cal.getTimeInMillis()));
			crosssell.setEffectiveFrom(cal.getTime());
			crosssell.setIsActive(true);
			crosssell.setLabel(mapping.get("lable"));
			crosssell.setModifiedBy("admin");
			crosssell.setModifiedOn(new Timestamp(cal.getTimeInMillis()));
			try {
				crosssell.setPrice(Integer.parseInt(mapping.get("price").trim()));
			}
			catch (Exception e) {
				messages += "<br/> Price must be a number";
				throw e;
			}
			/*Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.DATE, 30);*/
			Date validDate = sdf.parse(mapping.get("valid_upto"));
			crosssell.setValidUpto(validDate);

			CouponStore couponStore = new CouponStore();
			String cityString = "-1";
			String[] cityList = request.getParameterValues("city_list");
			cityString = Arrays.toString(cityList);
			if (cityString != null && cityString.length() >= 3 && !cityString.equals("null"))
				cityString = cityString.substring(1, cityString.length() - 1);
			else
				cityString = "-1";
			couponStore.setCityIdList(cityString);
			couponStore.setCouponPosition(0);
			couponStore.setCouponType("C");
			couponStore.setCouponValue(0);
			couponStore.setDateAdded(new Timestamp(cal.getTimeInMillis()));
			couponStore.setDateUpdated(new Timestamp(cal.getTimeInMillis()));
			couponStore.setFaceValue(0);
			couponStore.setIsActive(false);
			couponStore.setIsComplimentary(false);
			couponStore.setIsUnique(true);
			couponStore.setCouponImagePath(mapping.get("crosssell_image"));
			couponStore.setCampaignName(mapping.get("title").trim());
			couponStore.setCouponNature(CouponNature.CrossSell.getDbNature());
			
			try {
				/*Date date = sdf.parse(mapping.get("valid_upto"));*/
				Calendar cal3 = Calendar.getInstance();
				cal3.setTime(validDate);
				couponStore.setValidityDate(new Timestamp(cal3.getTimeInMillis()));
			}
			catch (Exception e) {
				messages += "<br/> valid upto should be a date in format YYYY-MM-DD";
				throw e;
			}

			CouponInventory couponInventory = new CouponInventory();
			couponInventory.setCouponCount(0);
			couponInventory.setCreatedAt(new Timestamp(cal.getTimeInMillis()));
			couponInventory.setDailyComplimentaryLimit(0);
			couponInventory.setDailyComplimentarySent(0);
			couponInventory.setSentCouponCount(0);
			couponInventory.setThresholdCount(Integer.parseInt(mapping.get("threshold_count")));
			couponInventory.setUpdatedAt(new Timestamp(cal.getTimeInMillis()));

			CrosssellBusinessDo crosssellBusinessDo = new CrosssellBusinessDo();
			crosssellBusinessDo.setCategory(mapping.get("category").trim());
			crosssellBusinessDo.setClassName(mapping.get("className").trim());
			crosssellBusinessDo.setDescription(mapping.get("description").trim());
			crosssellBusinessDo.setHeadDescription(mapping.get("headDesc").trim());
			crosssellBusinessDo.setImgurl(mapping.get("crosssell_image").trim());
			crosssellBusinessDo.setMailImgUrl(mapping.get("mailImgUrl").trim());
			crosssellBusinessDo.setMailSubject(mapping.get("mailSubject").trim());
			crosssellBusinessDo.setMailTnc(mapping.get("mailTnc").trim());
			try {
				crosssellBusinessDo.setMaxuses(Integer.parseInt(mapping.get("maxuses")));
			}
			catch (Exception e) {
				messages += "<br/> Maxuses must be a number";
				throw e;
			}
			crosssellBusinessDo.setOrder(mapping.get("position"));
			crosssellBusinessDo.setOthercondition(mapping.get("other_condition"));
			try {
				crosssellBusinessDo.setPrice(Integer.parseInt(mapping.get("price").trim()));
			}
			catch (Exception e) {
				messages += "<br/> Price must be a number";
				throw e;
			}

			String[] params = request.getParameterValues("primary_product");
			String primaryProduct = "";
			for (String param : params) {
				primaryProduct += param + "||";
			}
			if (primaryProduct.length() >= 3) {
				primaryProduct = primaryProduct.substring(0, primaryProduct.lastIndexOf("||"));
			}
			crosssellBusinessDo.setPrimaryproduct(primaryProduct);
			try {
				crosssellBusinessDo.setPrimaryproductamount(Integer.parseInt(mapping.get("primary_product_amount")));
			}
			catch (Exception e) {
				messages += "<br/> Primary product amount must be a number";
				throw e;
			}
			crosssellBusinessDo.setRefundable(mapping.get("isRefundable").equals("Y") ? true : false);

			crosssellBusinessDo.setTemplate(mapping.get("template"));
			try {
				crosssellBusinessDo.setThreshold(Integer.parseInt(mapping.get("threshold_count")));
			}
			catch (Exception e) {
				messages += "<br/> Threshold count must be a number";
				throw e;
			}
			crosssellBusinessDo.setTitle(mapping.get("title"));
			crosssellBusinessDo.setTnc(mapping.get("tnc"));
			crosssellBusinessDo.setUniqueCode(mapping.get("uniquecode").equals("Y") ? true : false);

			try {
				status = crosssellService.create(
						crosssell,
							couponStore,
							crosssellBusinessDo,
							couponInventory);
				if (status)
					messages = "Crosssell created successfully";
			}
			catch (Exception e) {
				messages += "<br/>Try again later";
				throw e;
			}
		}
		catch (Exception e) {
			logger.debug("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later";
		}
		/*model.addAttribute("status", status);
		if(status) {
			model.addAttribute("messages", "Crosssell created successfully.");
		} else {
			model.addAttribute("messages", "Failed to create crosssell. <br/>"+messages);
		}*/
		return String.format("redirect:%s?message=%s", HOME_LINK, messages) ;
		// return ViewConstants.CROSSSELL_ADMIN_HOME_VIEW;

	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String save(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
		boolean status = false;
		String messages = "";
		int crosssellId = -1;
		
		try {
			crosssellId = Integer.parseInt(mapping.get("crosssell_id"));

			CrosssellBusinessDo crosssellBusinessDo = crosssellService.getCrosssell(crosssellId);
			Crosssell crosssell = crosssellBusinessDo.getCrosssell();
			CouponStore couponStore = crosssellService.getCouponStore(crosssellBusinessDo.getStoreId());
			CouponInventory couponInventory = crosssellService.getCouponInventoryByCouponStoreId(crosssellBusinessDo.getStoreId());
			
			crosssell.setCategory(mapping.get("category"));
			crosssell.setLabel(mapping.get("lable"));
			Calendar cal = Calendar.getInstance();
			crosssell.setModifiedOn(new Timestamp(cal.getTimeInMillis()));
			try {
				crosssell.setPrice(Integer.parseInt(mapping.get("price")));
			}
			catch (Exception e) {
				messages += "<br/> Price must be a number";
				throw e;
			}
			Date validDate = sdf.parse(mapping.get("valid_upto"));
			crosssell.setValidUpto(validDate);
			
			crosssellBusinessDo.setCategory(mapping.get("category").trim());
			crosssellBusinessDo.setClassName(mapping.get("className").trim());
			crosssellBusinessDo.setDescription(mapping.get("description").trim());
			crosssellBusinessDo.setHeadDescription(mapping.get("headDesc").trim());
			crosssellBusinessDo.setImgurl(mapping.get("crosssell_image").trim());
			crosssellBusinessDo.setMailImgUrl(mapping.get("mailImgUrl").trim());
			crosssellBusinessDo.setMailSubject(mapping.get("mailSubject").trim());
			crosssellBusinessDo.setMailTnc(mapping.get("mailTnc").trim());
			try {
				crosssellBusinessDo.setMaxuses(Integer.parseInt(mapping.get("maxuses")));
			}
			catch (Exception e) {
				messages += "<br/> Maxuses must be a number";
				throw e;
			}
			crosssellBusinessDo.setOrder(mapping.get("position"));
			crosssellBusinessDo.setOthercondition(mapping.get("other_condition"));
			try {
				crosssellBusinessDo.setPrice(Integer.parseInt(mapping.get("price").trim()));
			}
			catch (Exception e) {
				messages += "<br/> Price must be a number";
				throw e;
			}

			String[] params = request.getParameterValues("primary_product");
			String primaryProduct = "";
			for (String param : params) {
				primaryProduct += param + "||";
			}
			if (primaryProduct.length() >= 3) {
				primaryProduct = primaryProduct.substring(0, primaryProduct.lastIndexOf("||"));
			}
			crosssellBusinessDo.setPrimaryproduct(primaryProduct);
			try {
				crosssellBusinessDo.setPrimaryproductamount(Integer.parseInt(mapping.get("primary_product_amount")));
			}
			catch (Exception e) {
				messages += "<br/> Primary product amount must be a number";
				throw e;
			}
			crosssellBusinessDo.setRefundable(mapping.get("isRefundable").equals("Y") ? true : false);

			crosssellBusinessDo.setTemplate(mapping.get("template"));
			try {
				crosssellBusinessDo.setThreshold(Integer.parseInt(mapping.get("threshold_count")));
			}
			catch (Exception e) {
				messages += "<br/> Threshold count must be a number";
				throw e;
			}
			crosssellBusinessDo.setTitle(mapping.get("title"));
			crosssellBusinessDo.setTnc(mapping.get("tnc"));
			crosssellBusinessDo.setUniqueCode(mapping.get("uniquecode").equals("Y") ? true : false);
			
			String cityString = "-1";
			String[] cityList = request.getParameterValues("city_list");
			cityString = Arrays.toString(cityList);
			if (cityString != null && cityString.length() >= 3 && !cityString.equals("null"))
				cityString = cityString.substring(1, cityString.length() - 1);
			else
				cityString = "-1";
			couponStore.setCityIdList(cityString);
			couponStore.setDateUpdated(new Timestamp(cal.getTimeInMillis()));
			couponStore.setCouponImagePath(mapping.get("crosssell_image"));
			couponStore.setCampaignName(mapping.get("title"));
			try {
				Date date = sdf.parse(mapping.get("valid_upto"));
				Calendar cal3 = Calendar.getInstance();
				cal3.setTime(date);
				couponStore.setValidityDate(new Timestamp(cal3.getTimeInMillis()));
			}
			catch (Exception e) {
				messages += "<br/> valid upto should be a date in format YYYY-MM-DD";
				throw e;
			}

			
			couponInventory.setThresholdCount(Integer.parseInt(mapping.get("threshold_count")));
			couponInventory.setUpdatedAt(new Timestamp(cal.getTimeInMillis()));


			try {
				status = crosssellService.update(
						crosssell,
							couponStore,
							crosssellBusinessDo,
							couponInventory);
				if (status)
					messages = "Crosssell updated successfully";
			}
			catch (Exception e) {
				messages += "<br/>Try again later";
				throw e;
			}
		}
		catch (Exception e) {
			logger.debug("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later";
		}
		return String.format("redirect:%s?message=%s&crosssell_id=%s", HOME_LINK, messages, crosssellId) ;
	}

	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public String upload(@RequestParam("crosssell_codes") MultipartFile file,
			@RequestParam(required=false, value="coupon_store_id") Integer couponStoreId, Model model,
			HttpServletRequest request) {
		String messages = "";
		List<CouponCode> newCodes = new ArrayList<CouponCode>();
		List<CouponCode> existingCodes = null;
		try {
			Calendar cal = Calendar.getInstance();

			try {
				CouponStore couponStore = crosssellService.getCouponStore(couponStoreId);
				CouponInventory couponInventory = crosssellService.getCouponInventoryByCouponStoreId(couponStoreId);
				existingCodes = crosssellService.getAllCouponCodes(couponInventory.getCouponInventoryId());

				BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
				String line = "";
				while ((line = br.readLine()) != null) {
					CouponCode couponCode = new CouponCode();
					couponCode.setCouponCode(line);
					couponCode.setCouponInventoryId(couponInventory.getCouponInventoryId());
					couponCode.setCreatedAt(new Timestamp(cal.getTimeInMillis()));
					couponCode.setUpdatedAt(new Timestamp(cal.getTimeInMillis()));
					couponCode.setOrderId(null);
					couponCode.setIsBlocked(false);
					if (!existingCodes.contains(couponCode)) {
						newCodes.add(couponCode);
					}
				}
				if (newCodes.size() > 0) {
					crosssellService.uploadCodes(couponStore, couponInventory, newCodes);
				}
			} catch (Exception e) {
				messages += "Something went wrong. Try again later";
				logger.info("Exception " + messages, e);
				throw new Exception("Try again later: " + e.toString());
			}
			messages += "Codes uploaded successfully.";
		} catch (Exception e) {
			logger.info("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later";
		}
		return String.format("redirect:%s?message=%s", HOME_LINK, messages) ;
	}

	@RequestMapping(value = "updateStatus", method = RequestMethod.POST)
	public String updateStatus(@RequestParam Map<String, String> mapping, Model model) {
		String messages = "";
		try {
			Integer couponStoreId = Integer.parseInt(mapping.get("store_id"));
			Integer status = Integer.parseInt(mapping.get("status"));
			if (status == 1) {
				crosssellService.activateCoupon(couponStoreId);
				model.addAttribute("STATUS", "success");
			}
			else if (status == 0) {
				crosssellService.deactivateCoupon(couponStoreId);
				model.addAttribute("STATUS", "success");
			}
			else {
				model.addAttribute("STATUS", "error");
			}
		}
		catch (Exception e) {
			logger.debug("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later";
			model.addAttribute("STATUS", "error");
		}
		model.addAttribute("message", "messages");
		return "jsonView";
	}
	
	@RequestMapping(value = "blockcodes", method = RequestMethod.POST)
	public String blockCodes(@RequestParam Map<String, String> mapping, Model model) {
		String messages = "Failed to block codes.";
		
		String reasonToBlock = mapping.get("reason_to_block");
		int couponStoreId = -1;
		try {
			couponStoreId = Integer.parseInt(mapping.get("coupon_store_id"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		List<CouponCode> unusedCodes = null;
		if (couponStoreId != -1) {
			CouponStore couponStore = crosssellService.getCouponStore(couponStoreId);
			CouponInventory couponInventory = crosssellService.getCouponInventoryByCouponStoreId(couponStoreId);
			unusedCodes = crosssellService.getUnusedCouponCodes(couponInventory.getCouponInventoryId());
			
			crosssellService.blockCodes(couponStore, couponInventory, unusedCodes, reasonToBlock);
			messages = "Codes blocked succesffully.";
		}
		return String.format("redirect:%s?message=%s", HOME_LINK, messages) ;
	}
	/*
	 * To download unused codes
	 */
	@RequestMapping(value = "download", method = RequestMethod.GET)
	public String download(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request, HttpServletResponse response) {
		String unusedCodes = "";
		int couponStoreId = -1;
		try {
			couponStoreId = Integer.parseInt(mapping.get("coupon_store_id"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		List<CouponCode> existingCodes = null;
		if (couponStoreId != -1) {
			CouponStore couponStore = crosssellService.getCouponStore(couponStoreId);
			CouponInventory couponInventory = crosssellService.getCouponInventoryByCouponStoreId(couponStoreId);
			existingCodes = crosssellService.getUnusedCouponCodes(couponInventory.getCouponInventoryId());
			if(couponStore != null) unusedCodes = couponStore.getCampaignName()+"_unusedCodes.xls";
			else  unusedCodes = "unusedCodes.xls";
		}
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader ("Content-Disposition", "inline; filename="+unusedCodes);
		try {
			ServletOutputStream outStream = response.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(outStream);
			if(existingCodes != null && existingCodes.size() > 0){
				for(CouponCode couponCode : existingCodes) {
					osw.write(couponCode.getCouponCode()+"\n");
				}
			}
			osw.flush();
			osw.close();
			outStream.flush();
			outStream.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
