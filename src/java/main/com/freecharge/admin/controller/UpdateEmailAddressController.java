package com.freecharge.admin.controller;




import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/emailupdate*")
public class UpdateEmailAddressController extends BaseAdminController {
    @Autowired
    private UserServiceProxy userServiceProxy;

    public static final String UPDATE_EMAIL_ID_LINK = "/admin/emailupdate/home.htm";
    public static final String OLD_EMAIL_ADDRESS    = "oldemailid";
    public static final String NEW_EMAIL_ADDRESS    = "newemailid";
    private static final Integer EMAIL_INPUT_LIMIT    = 100;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String home(@RequestParam Map<String, String> mapping, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UPDATE_EMAIL_ID)) {
            return get403HtmlResponse(response);
        }
        return ViewConstants.UPDATE_EMAIL_ID;
    }

    /** Method to Update individual email address */
    @RequestMapping(value = "singleemail", method = RequestMethod.POST)
    public String updateSingleEmailAddress(@RequestParam Map<String, String> mapping, Model model) {
        Integer rowcount = 0;
        String rowCountMessage = null;
        EmailValidator emailValidator = EmailValidator.getInstance();
        if (mapping.get(OLD_EMAIL_ADDRESS).isEmpty() && mapping.get(NEW_EMAIL_ADDRESS).isEmpty()) {
            model.addAttribute("errorMessage","Text fields cannot be Empty");
            return ViewConstants.UPDATE_EMAIL_ID;
        }
        
        if (!emailValidator.isValid(mapping.get(NEW_EMAIL_ADDRESS))) {
        	 model.addAttribute("errorMessage","New emailid is invalid");
             return ViewConstants.UPDATE_EMAIL_ID;
        }
        
        if (!FCUtil.isValidEmailFormat(mapping.get(NEW_EMAIL_ADDRESS))) {
          	 model.addAttribute("errorMessage","New emailid is in invalid format");
             return ViewConstants.UPDATE_EMAIL_ID;
        }
        
        String oldEmailAddress = mapping.get(OLD_EMAIL_ADDRESS).trim();
        String newEmailAddress = mapping.get(NEW_EMAIL_ADDRESS).trim();
        Users  oldUser = userServiceProxy.getUserByEmailId(oldEmailAddress);
        Users newUser = userServiceProxy.getUserByEmailId(newEmailAddress);
        if (oldUser != null && newUser == null) {
            rowcount = userServiceProxy.updateUserEmailId(oldUser.getUserId(), newEmailAddress);
            model.addAttribute("message", " Updated successfully ");
        } else if (oldUser != null && newUser != null)  {
            model.addAttribute("message", "Failed to update: both " + oldEmailAddress + " and " + newEmailAddress
                    + " exists ");
        } else if (oldUser == null && newUser != null) {
            model.addAttribute("message", " Failed to update: only " + newEmailAddress + " exists ");
        } else if (oldUser == null && newUser == null) {
            model.addAttribute("message", " Failed to update: both " + oldEmailAddress + " and " + newEmailAddress
                    + " does not exists ");
        } 
        rowCountMessage = "Number of rows affected: " + rowcount;
        model.addAttribute("rowCountMessage", rowCountMessage);
        model.addAttribute("rowcount", rowcount);
        return ViewConstants.UPDATE_EMAIL_ID;
    }

    /**
     * Method to Update emailAddress in bulk - by pasting the emailIds as
     * oldEmailAddress,newEmailAddress
     */
    @RequestMapping(value = "bulkemailids", method = RequestMethod.POST)
    public String updateEmailAddressInBulk(@RequestParam("emailids") final String emailIds, Model model) {
        Integer rowCount = 0;
        List<String> updateSuccessList = new ArrayList<>();
        List<String> updateFailedList = new ArrayList<>();
        if (emailIds.isEmpty()) {
            model.addAttribute("errorMessage", " Found empty text area - cannot be submitted");
            return ViewConstants.UPDATE_EMAIL_ID;
        }
        
        String emailIdString = emailIds.trim();
        /*
         * entered emailIds will be split based on comma, newline and tab space
         * \\s*, is also treated as a white space character \s - A whitespace
         * character, short for [ \t\n\x0b\r\f]
         */
        String[] split = emailIdString.split("(\\s*,\\s*|\\s+)");
        int emailIdCount = 0;
        if (split.length > EMAIL_INPUT_LIMIT) {
            model.addAttribute("limitExceedMessage",
                    "Email Id Input limit Exceeded - Textarea accepts upto 100 emailIds");
            return ViewConstants.UPDATE_EMAIL_ID;
        }
        
        for (int count = 0; count < split.length; count++) {
        	 if (!FCUtil.isValidEmailFormat(split[count])) {
             	model.addAttribute("errorMessage", "Found Invalid email Id format: " +  split[count] + "- cannot update");
                 return ViewConstants.UPDATE_EMAIL_ID;
             }
             
             if (!EmailValidator.getInstance().isValid(split[count])) {
             	model.addAttribute("errorMessage", "Invalid email ids found" + split[count] + "- cannot update");
                 return ViewConstants.UPDATE_EMAIL_ID;
             }
        }
        
        while (emailIdCount < split.length) {
            String oldEmailAddress = split[emailIdCount];
            String newEmailAddress = split[++emailIdCount];
            Users oldUser = userServiceProxy.getUserByEmailId(oldEmailAddress);
            Users newUser = userServiceProxy.getUserByEmailId(newEmailAddress);
            if (oldUser != null && newUser == null) {
            	rowCount += userServiceProxy.updateUserEmailId(oldUser.getUserId(), newEmailAddress);
                updateSuccessList.add("Successfully updated From email: " + oldEmailAddress + " to " + newEmailAddress);
                model.addAttribute("successList", updateSuccessList);
                String successRowCountMessage = "Number of rows affected:" + rowCount;
                model.addAttribute("successRowCountMessage", successRowCountMessage);
            } else if (oldUser != null && newUser != null) {
                updateFailedList.add("Failed to update: both  " + oldEmailAddress + " and " + newEmailAddress
                        + " exists ");
                model.addAttribute("failedList", updateFailedList);
                model.addAttribute("rowCountMessage", "Number of rows affected: 0");
            } else if (oldUser == null && newUser != null) {
                updateFailedList.add("Failed to update: only newEmailId:" + newEmailAddress + " exists ");
                model.addAttribute("failedList", updateFailedList);
                model.addAttribute("rowCountMessage", "Number of rows affected: 0");
            } else if (oldUser == null && newUser == null) {
                updateFailedList.add("Failed to update: both " + oldEmailAddress + " and " + newEmailAddress
                        + " does not exists");
                model.addAttribute("failedList", updateFailedList);
                model.addAttribute("rowCountMessage", "Number of rows affected: 0");
            }
            emailIdCount += 1;
        }
        return ViewConstants.UPDATE_EAIL_ID_RESULT;
    }
}
