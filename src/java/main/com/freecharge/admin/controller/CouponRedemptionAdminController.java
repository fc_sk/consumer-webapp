package com.freecharge.admin.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.api.coupon.service.web.model.BlockedCouponResponse;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.OrderIdUtil;
import com.freecharge.merchant.MerchantService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;


@Controller
@RequestMapping("/admin/coupon-redemption-admin/*")
public class CouponRedemptionAdminController  extends BaseAdminController{
	
	public static final String ADMIN_COUPON_REDEMPTION_LINK = "/admin/coupon-redemption-admin/home";
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private VoucherService voucherService;
	 
	@Autowired
	private MerchantService merchantService;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@Autowired
    private OrderService orderService;

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String viewHome(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.COUPON_REDEMPTION)){
            get403HtmlResponse(response);
            return null;
        }
        List<CouponMerchant> merchants = merchantService.getAllMerchants();
        model.addAttribute(WebConstants.ALL_MERCHANTS, merchants);
        List<CouponStore> campaigns = voucherService.getAllUniqueCampaigns();
		model.addAttribute(WebConstants.ALL_CAMPAIGNS, campaigns); 
		return ViewConstants.ADMIN_COUPON_REDEMPTION_VIEW;
	}
	
	@RequestMapping(value = "create-login", method = RequestMethod.POST)
    public String createMerchantLogin(@RequestParam Map<String, String> mapping, Model model)  {
		
		try{
			String merchantId = mapping.get("merchant").split("-")[0];
			String merchantTitle = mapping.get("merchant").split("-")[1];
			String username = mapping.get("username").trim();

			String result = merchantService.createMerchantLogin(merchantId,merchantTitle, username);
			model.addAttribute(WebConstants.STATUS, result);
		}catch (Exception exception) {
			model.addAttribute(WebConstants.STATUS, "Error");
 			logger.error("Exception occured while creating merchant login ",exception);
			return "jsonView";
		}
		return "jsonView";
    }
	
	 
	@RequestMapping(value = "block", method = RequestMethod.POST)
    public String blockCouponsForRedemptionTest(@RequestParam Map<String, String> mapping, Model model)  {
		
		try{
			int couponStoreId;
			int couponsCnt;
			if(FCUtil.isInteger(mapping.get("couponStoreId"))){
				couponStoreId = Integer.valueOf(mapping.get("couponStoreId"));
			}else{
				throw new Exception("Invalid couponStoreId "+ mapping.get("couponStoreId") );
			}
			if(FCUtil.isInteger(mapping.get("coupon_count"))){
				couponsCnt = Integer.valueOf(mapping.get("coupon_count"));
			}else{
				throw new Exception("Invalid coupon_count " + mapping.get("coupon_count"));
			}
 			boolean isMCoupon = Boolean.valueOf(mapping.get("ismcoupon"));
			boolean toggleMcouponStatus = Boolean.valueOf(mapping.get("toggleMcouponStatus"));
			CouponStore couponStore = voucherService.getActiveCouponByCouponStoreId(couponStoreId);
			
			if (couponStore == null){
				model.addAttribute("STATUS", "Coupon not active.");
				return "jsonView";
			}
			
			if (!couponStore.getIsUnique()){
				model.addAttribute("STATUS", "Can block only unique coupon types.");
				return "jsonView";
			}
			
			if (toggleMcouponStatus){
				voucherService.setMCouponStatus(couponStoreId, isMCoupon);	
			}
			String orderId = OrderIdUtil.generateWebRedemptionOrderIdForBlockingCodes(couponStoreId);
			Integer userId=orderService.getUserIdForOrder(orderId);
			BlockCouponRequest request = new BlockCouponRequest();
			request.setCouponStoreId(couponStoreId);
			request.setOrderId(orderId);
			request.setQuantity(couponsCnt);
			request.setCouponOptinType(CouponOptinType.TEST);
			BlockedCouponResponse blockedCouponResponse = couponServiceProxy.blockCouponForOrder(request,userId);
			if (blockedCouponResponse != null && blockedCouponResponse.getBlockedCoupon(couponStoreId) != null){
				BlockedCoupon blockedCoupon = blockedCouponResponse.getBlockedCoupon(couponStoreId);
				List<String> blockedCodes = new ArrayList<String>();
				for (BlockedCouponCode couponCode : blockedCoupon.getBlockedCodes()){
					blockedCodes.add(couponCode.getCode());
				}
				if (blockedCodes.size() == couponsCnt){
					model.addAttribute("STATUS", "Blocked codes successfully.");
				} else if (blockedCodes.size() < couponsCnt){
					model.addAttribute("STATUS", "Could not block all codes successfully.");
				}
				model.addAttribute("coupons", blockedCodes);
			} else {
				model.addAttribute("STATUS", "Could not block all codes successfully.");
			}
		}catch (Exception exception) {
			model.addAttribute(WebConstants.STATUS, "Error");
 			logger.error("Exception occured while blocking coupons for couponStoreId = "+ mapping.get("couponStoreId") 
 					+" &  coupon_count " +  mapping.get("coupon_count") ,exception);
 		}
		return "jsonView";
    }
}

