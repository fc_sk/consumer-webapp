package com.freecharge.admin.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.model.CTActionsControlData;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.AdminUserRepository;
import com.freecharge.mongo.repos.CustomerTrailControlRepository;
import com.mongodb.DBObject;

/**
* Controller which displays "CustomertTrail actions control" Tool with
* MaxCredits,MaxViews,Avail_Credits,Avail_views of an Admin user(CS agent).
* And also Saves updated values(updated from tool) into mongo DB.
*/

@Controller
@RequestMapping("/admin/customertrail/actionscontrol/*")
public class CustomerTrailActionsController extends BaseAdminController {
    public static final String             CUSTOMER_TRAIL_ACTIONS_CONTROL_VIEW = "/admin/customertrail/actionscontrol/manage.htm";
    public static final String             AG_PREFERENCE_URL = "/admin/rechargeRequestStatus/agpreference.htm";
    private static final Logger            logger                              = LoggingFactory
                                                                                       .getLogger(CustomerTrailActionsController.class);

    @Autowired
    private CustomerTrailControlRepository customerTrailControlRepository;

    @Autowired
    private AdminUserRepository            adminUserRepository;

    @Autowired
    private CTActionControlService         ctActionControlService;

    @Autowired
    private AdminHelper                    adminHelper;

    @RequestMapping(value = "manage", method = RequestMethod.GET)
    public String getCustometTrailAccessControlPage(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL_ACTIONS_CONTROL)) {
            return get403HtmlResponse(response);
        }

        List<CTActionsControlData> ctActionsControlDataList = new ArrayList<>();
        List<DBObject> adminUsers = this.adminUserRepository.findAllAdminUsers();
        List<String> adminUserEmailList = getEmailListFromCollection(adminUsers);

        for (String email : adminUserEmailList) {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            CTActionsControlData ctActionsControlData = new CTActionsControlData();
            if (dbObject != null) {
                ctActionsControlData.setEmailId((String) dbObject.get(CustomerTrailControlRepository.EMAIL_ID));
                ctActionsControlData.setMaxCredits((String) dbObject.get(CustomerTrailControlRepository.MAX_CREDITS));
                ctActionsControlData.setViewLimits((String) dbObject.get(CustomerTrailControlRepository.VIEW_LIMITS));
                String currentDateTimeString = getCurrentDateTimeString((Date) dbObject
                        .get(CustomerTrailControlRepository.CREATEDAT_NAME));
                ctActionsControlData.setCreatedAt(currentDateTimeString);
                ctActionsControlData.setAvailableCredits((String) dbObject
                        .get(CustomerTrailControlRepository.AVAILABLE_CREDITS));
                ctActionsControlData.setAvailableViewLimits((String) dbObject
                        .get(CustomerTrailControlRepository.AVAILABLE_VIEWS));

            } else {
                Date currentDateTime = new Date();
                Map<String, Object> customerTrailControlMap = getControlMapToMongo(email, "500", "20", currentDateTime);
                customerTrailControlRepository.addCustomerTrailControlRecord(customerTrailControlMap);
                ctActionsControlData.setEmailId(email);
                ctActionsControlData.setMaxCredits("500");
                ctActionsControlData.setViewLimits("20");
                ctActionsControlData.setAvailableCredits("500");
                ctActionsControlData.setAvailableViewLimits("20");
                ctActionsControlData.setCreatedAt(getCurrentDateTimeString(currentDateTime));
            }
            ctActionsControlDataList.add(ctActionsControlData);
        }

        model.addAttribute("ctActionsControlDataList", ctActionsControlDataList);
        return "customertrail/actions/control/manage";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String saveActionControl(@RequestParam final Map<String, String> params, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL_ACTIONS_CONTROL)) {
            return get403HtmlResponse(response);
        }

        String email_id = params.get("emailid");
        String max_credits = params.get("maxcredits");
        String view_limits = params.get("viewlimits");

        try {
            if (email_id != null && !(email_id.isEmpty()) && max_credits != null && view_limits != null) {
                if (max_credits.isEmpty()) {
                    max_credits = "0";
                }
                if (view_limits.isEmpty()) {
                    view_limits = "0";
                }

                if (!(adminHelper.isDouble(max_credits)) || !(StringUtils.isNumeric(view_limits))) {
                    model.addAttribute("status", "failed");
                    model.addAttribute("message", "Wrong input parameters for CT actions control tool : " + email_id);
                    return "jsonView";
                }

                Date currentDateTime = new Date();
                Map<String, Object> customerTrailControlMap = getControlMapToMongo(email_id, max_credits, view_limits,
                        currentDateTime);

                customerTrailControlRepository.addCustomerTrailControlRecord(customerTrailControlMap);

                model.addAttribute("status", "success");
                model.addAttribute("message", "Access control updated for user: " + email_id);
                model.addAttribute("updatedDateTime", getCurrentDateTimeString(currentDateTime));
                model.addAttribute("creditsAvailable", max_credits);
                model.addAttribute("viewsAvailable", view_limits);
                return "jsonView";
            } else {
                model.addAttribute("status", "failed");
                model.addAttribute("message", "Wrong input parameters for CT actions control tool : " + email_id);
                return "jsonView";
            }
        } catch (Exception ex) {
            model.addAttribute("status", "failed");
            model.addAttribute("message", "Exception raised while saving contol data for user: " + email_id);
            return "jsonView";
        }
    }

    private List<String> getEmailListFromCollection(final List<DBObject> dbObjectList) {
        List<String> emailList = new ArrayList<>();
        for (DBObject dbObject : dbObjectList) {
            emailList.add((String) dbObject.get("email"));
        }
        return emailList;
    }

    private String getCurrentDateTimeString(final Date d) {
        if (d != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss a");
            return formatter.format(d);
        }
        return " ";
    }

    /**
     Create Map with for updating mongo
     */
    private Map<String, Object> getControlMapToMongo(final String emailId, final String maxCredits,
            final String viewLimits, final Date currentDateTime) {
        Map<String, Object> customerTrailControlMap = new HashMap<>();
        customerTrailControlMap.put(CustomerTrailControlRepository.EMAIL_ID, emailId);
        customerTrailControlMap.put(CustomerTrailControlRepository.MAX_CREDITS, maxCredits);
        customerTrailControlMap.put(CustomerTrailControlRepository.VIEW_LIMITS, viewLimits);
        customerTrailControlMap.put(CustomerTrailControlRepository.AVAILABLE_CREDITS, maxCredits);
        customerTrailControlMap.put(CustomerTrailControlRepository.AVAILABLE_VIEWS, viewLimits);
        customerTrailControlMap.put(CustomerTrailControlRepository.CREATEDAT_NAME, currentDateTime);
        return customerTrailControlMap;
    }

    @RequestMapping(value = "resetAll", method = RequestMethod.GET)
    public String resetAllEmailsControl(@RequestParam final Map<String, String> params,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) throws IOException {
        try {
            ctActionControlService.resetAllCTActionControl();
            List<CTActionsControlData> ctActionsControlDataList = getCTActionControlDataListOfCollection();
            model.addAttribute("ctActionsControlDataList", ctActionsControlDataList);
            model.addAttribute("message", "Successfully reset all available credits and views......");
            return "customertrail/actions/control/manage";
        } catch (Exception e) {
            List<CTActionsControlData> ctActionsControlList = getCTActionControlDataListOfCollection();
            logger.error("Exception while reseting CT action control from tool at : " + new Date());
            model.addAttribute("ctActionsControlDataList", ctActionsControlList);
            model.addAttribute("message", "Something went wrong while resetting all available credits and views......");
            return "customertrail/actions/control/manage";
        }
    }

    /**
     * Getting list of all records of mongo collection customertrail_control
     */
    private List<CTActionsControlData> getCTActionControlDataListOfCollection() {
        List<CTActionsControlData> ctActionsControlDataList = new ArrayList<>();
        List<DBObject> allRecords = customerTrailControlRepository.getCustomerTrailControllAll();
        if (allRecords != null && allRecords.size() > 0) {
            for (DBObject record : allRecords) {
                CTActionsControlData ctActionsControlData = new CTActionsControlData();
                ctActionsControlData.setEmailId((String) record.get(CustomerTrailControlRepository.EMAIL_ID));
                ctActionsControlData.setMaxCredits((String) record.get(CustomerTrailControlRepository.MAX_CREDITS));
                ctActionsControlData.setViewLimits((String) record.get(CustomerTrailControlRepository.VIEW_LIMITS));
                String currentDateTimeString = getCurrentDateTimeString((Date) record
                        .get(CustomerTrailControlRepository.CREATEDAT_NAME));
                ctActionsControlData.setCreatedAt(currentDateTimeString);
                ctActionsControlData.setAvailableCredits((String) record
                        .get(CustomerTrailControlRepository.AVAILABLE_CREDITS));
                ctActionsControlData.setAvailableViewLimits((String) record
                        .get(CustomerTrailControlRepository.AVAILABLE_VIEWS));
                ctActionsControlDataList.add(ctActionsControlData);
            }
        }
        return ctActionsControlDataList;
    }
}