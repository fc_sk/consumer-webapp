package com.freecharge.admin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.payment.services.PaymentPortalService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.InAuthenticationFetchService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 11:01 AM
 * This is the controller class for auto refund. Read more about the auto refund scenarios here - https://docs.google.com/a/freecharge.com/spreadsheet/ccc?key=0Ag-CAyxWcDYgdDdvVnUwM0VLVGJhMHJ1RXVoN3psOWc#gid=0
 */
public class RefundAdminController {
    @Autowired
    private InAuthenticationFetchService inAuthenticationFetchService;

    @Autowired
    private PaymentPortalService paymentPortalService;

    /**
     * Redirect login requests to the login page.
     * @param adminUsers - Model object auto populated by Spring.
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView viewLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(ViewConstants.REFUND_LOGIN_ADMIN_VIEW);
        return modelAndView;
    }

    /**
     * Handle user login for the refund screen.
     * @param adminUsers - Model object auto populated by spring.
     * @param request
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String adminLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers, HttpServletRequest request) {
        if (inAuthenticationFetchService.isMisUserValid(adminUsers)) {
            request.getSession().setAttribute(WebConstants.MIS_USER_SESSION_PARAM, adminUsers);
            return "redirect:/admin/refund/listRefunds.htm";
        } else {
            return ViewConstants.REFUND_LOGIN_ADMIN_VIEW;
        }
    }

    @RequestMapping(value = "listRefunds", method = RequestMethod.GET)
    public String showRefundListScreen(@ModelAttribute("AdminUsers") AdminUsers adminUsers, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        WebContext webContext = DelegateHelper.processRequest(request, response, "refundDelegate", model, null);
        return ViewConstants.REFUND_LIST_ADMIN_VIEW;
    }

    @RequestMapping(value = "doRefund", method = RequestMethod.GET)
    public String doRefund(@RequestParam Map<String, String> mapping) {
        String orderId = mapping.get("orderId");
        String amount = mapping.get("amount");
        this.paymentPortalService.doRefund(orderId, Double.valueOf(amount), PaymentConstants.ADMIN_REFIND);
        return "redirect:/admin/refund/listRefunds.htm";
    }
}
