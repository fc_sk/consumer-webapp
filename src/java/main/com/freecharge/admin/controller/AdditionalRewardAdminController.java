package com.freecharge.admin.controller;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdditionalRewardAdminHelper;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import com.freecharge.freefund.services.AdditionalRewardService;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.web.util.ViewConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/freefund/additionalreward/*")
public class AdditionalRewardAdminController extends BaseAdminController{
    private Logger logger = LoggingFactory.getLogger(AdditionalRewardAdminController.class);

    public static final String ADDITIONAL_REWARDS_HOME_VIEW = "/admin/freefund/additionalreward/home";

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private AdditionalRewardAdminHelper additionalRewardAdminHelper;

    @Autowired
    private AdditionalRewardService additionalRewardService;

    @Autowired
    private EmailService emailService;

    private String setPageAndReturnHomeView (HttpServletRequest request,
                                             HttpServletResponse response,
                                             Model model,
                                             String page) throws IOException {
        if (!hasAccess(request, AdminComponent.ADDITIONAL_REWARDS)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", page);
        return ViewConstants.FREEFUND_ADDITIONAL_REWARDS_HOME_VIEW;
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(Model model,
                           HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        return setPageAndReturnHomeView(request, response, model, "home");
    }

    @RequestMapping(value="home/create", method = RequestMethod.GET)
    public String viewCreate(Model model,
                             HttpServletRequest request,
                             HttpServletResponse response) throws IOException {
        return setPageAndReturnHomeView(request, response, model, "create");
    }

    @RequestMapping(value = "home/edit", method = RequestMethod.GET)
    public String editReward(@RequestParam(value = "rewardId", required = true) int rewardId, Model model) {
        List<AdditionalReward> additionalRewardList = additionalRewardService.getByRewardId(rewardId);
        model.addAttribute("additionalReward", FCUtil.getFirstElement(additionalRewardList));
        model.addAttribute("page", "edit");
        return ADDITIONAL_REWARDS_HOME_VIEW;
    }

    @RequestMapping(value = "home/delete", method = RequestMethod.GET)
    public String deleteReward(Model model,
                               HttpServletRequest request,
                               HttpServletResponse response) throws IOException {
        List<AdditionalReward> additionalRewards = additionalRewardService.getAllActiveAdditionalRewards();
        model.addAttribute("additionalRewards", additionalRewards);
        return setPageAndReturnHomeView(request, response, model, "delete");
    }

    @RequestMapping(value="home/view", method = RequestMethod.GET)
    public String viewRewards(Model model,
                              HttpServletRequest request,
                              HttpServletResponse response) throws IOException {
        List<AdditionalReward> additionalRewards = additionalRewardService.getAllActiveAdditionalRewards();
        model.addAttribute("additionalRewards", additionalRewards);
        return setPageAndReturnHomeView(request, response, model, "view");
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String createAdditionalReward(@RequestParam Map<String, String> mapping,
                                         Model model) {
        try {
            Map<String, Object> resultMap = additionalRewardAdminHelper.validateAdditionaReward(mapping);
            String logResultInputValidation = FCUtil.isNotEmpty(resultMap)?resultMap.toString():null;
            logger.info("Result map from validating reward input : "
                    + logResultInputValidation);
            if (FCUtil.isNotEmpty(resultMap) && resultMap.containsKey(AdditionalRewardAdminHelper.MESSAGE)) {
                return returnHomeViewWithMessage((String) resultMap.get(AdditionalRewardAdminHelper.MESSAGE));
            } else {
                AdditionalReward additionalReward= (AdditionalReward)
                        resultMap.get(AdditionalRewardAdminHelper.ADDITIONAL_REWARD);
                logger.info("Additional reward to be created " + additionalReward.toString());
                int rowCount = additionalRewardService.create(additionalReward);
                if (rowCount < 1) {
                    return returnHomeViewWithMessage("Failed to create double reward." +
                            " Please report issue to Growth engg team.");
                } else {
                    emailService.sendAdditionalRewardCreatedNotificationEmail(additionalReward);
                    return returnHomeViewWithMessage("Successfully created double reward");
                }
            }

        }catch (RuntimeException e) {
            logger.error("Error creating additional reward", e);
            return String.format("redirect:%s?message=Error creating additional reward. Contact dev team.",
                    ADDITIONAL_REWARDS_HOME_VIEW);
        }
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String editAdditionalReward(@RequestParam Map<String, String> mapping,
                                       HttpServletRequest request) {
        try {
            Map<String, Object> resultMap = additionalRewardAdminHelper.validateAdditionaReward(mapping);
            if (FCUtil.isNotEmpty(resultMap) && resultMap.containsKey(AdditionalRewardAdminHelper.MESSAGE)) {
                return returnHomeViewWithMessage((String) resultMap.get(AdditionalRewardAdminHelper.MESSAGE));
            } else {
                AdditionalReward additionalReward= (AdditionalReward)
                        resultMap.get(AdditionalRewardAdminHelper.ADDITIONAL_REWARD);
                List<AdditionalReward> additionalRewardList = additionalRewardService.getByRewardName(
                        additionalReward.getRewardName());
                AdditionalReward oldAdditionalReward = FCUtil.getFirstElement(additionalRewardList);
                if (oldAdditionalReward == null) {
                    logger.error("Couldnt find an additonal reward for id " + additionalReward.getId());
                    return returnHomeViewWithMessage("Error editing additional reward. Contact dev team.");
                } else {
                    additionalReward.setId(oldAdditionalReward.getId());
                    int rowCount = additionalRewardService.edit(additionalReward);
                    if (rowCount < 1) {
                        return returnHomeViewWithMessage("Error editing additional reward. Contact Groeth Engg team.");
                    } else {
                        emailService.sendAdditionalRewardEditedNotificationEmail(additionalReward, oldAdditionalReward,
                                additionalReward.getRewardName(), getCurrentAdminUser(request));
                        return returnHomeViewWithMessage("Successfully edited double reward");
                    }
                }
            }

        }catch (RuntimeException e) {
            logger.error("Error creating additional reward", e);
            return returnHomeViewWithMessage("Error creating additional reward. Contact Growth Engg team.");
        }
    }

    @RequestMapping(value="delete", method = RequestMethod.GET)
    public String viewDelete(@RequestParam(value = "rewardId", required = true)
                             int rewardId) throws IOException {
        try {
            boolean isDisabled = additionalRewardService.disableAdditionalReward(rewardId);
            if (isDisabled) {
                return returnHomeViewWithMessage("Successfully deleted additional reward");
            } else {
                return returnHomeViewWithMessage("Failed to delete reward " + rewardId);
            }
        } catch (RuntimeException e) {
            logger.error("Error deleting reward " + rewardId, e);
            return returnHomeViewWithMessage("Error deleting additional reward " + rewardId);
        }
    }

    private String returnHomeViewWithMessage(String message) {
        return String.format("redirect:%s?message=" + message,
                ADDITIONAL_REWARDS_HOME_VIEW);
    }
}