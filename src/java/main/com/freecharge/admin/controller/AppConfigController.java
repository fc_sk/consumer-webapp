package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import org.apache.axis.utils.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.InMemoryAppConfigService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/appconfig")
public class AppConfigController extends BaseAdminController {

    public static final String APP_CONFIG_URL = "/admin/appconfig.htm";
    public static final String OLD_VALUE_KEY = "oldValue";
    public static final String NEW_VALUE_KEY = "newValue";
  
    private static final String MONGO_SIZE_RECORD_TOGGLE = "/admin/appconfig/toggle/mongosizerecord.htm";
    private static final String ASYNC_RECHARGE_TOGGLE = "/admin/appconfig/toggle/asyncrecharge.htm";
    private static final String IMS_DIRECT_HIT_TOGGLE= "/admin/appconfig/toggle/imsdirecthit.htm";
    private static final String EMAIL_BANNER_ENABLED_TOGGLE= "/admin/appconfig/toggle/emailbannerenabled.htm";
    private static final String EURONET_HTTPS_ENABLED_TOGGLE= "/admin/appconfig/toggle/euronethttpsenabled.htm";
    private static final String ASYNC_BILL_PAYMENT_TOGGLE = "/admin/appconfig/toggle/asyncbillpayment.htm";
    private static final String BBPS_BILL_PAYMENT_TOGGLE = "/admin/appconfig/toggle/bbpsbillpayment.htm";
    private static final String UNDER_MAINTENANCE_HEADER_BANNER_TOGGLE = "/admin/appconfig/toggle/undermaintenanceheaderbanner.htm";
    private static final String PAYMENT_UNDER_MAINTENANCE_TOGGLE = "/admin/appconfig/toggle/paymentundermaintenance.htm";
    private static final String CASH_BACK_CAMPAIGN_TOGGLE = "/admin/appconfig/toggle/cashbackcampaign.htm";
    private static final String ARDEAL_FEATURE_TOGGLE = "/admin/appconfig/toggle/ardealfeature.htm";
    private static final String WEBSITE_ANALYTICS_FEATURE_TOGGLE = "/admin/appconfig/toggle/websiteanalytics.htm";
    private static final String MOBILESITE_ANALYTICS_FEATURE_TOGGLE = "/admin/appconfig/toggle/mobilesiteanalytics.htm";
    private static final String FINAL_PAGE_COUPONS_TOGGLE = "/admin/appconfig/toggle/finalpagecoupons.htm";
    private static final String USER_AGENT_ENQUEUING_ENABLED_TOGGLE = "/admin/appconfig/toggle/useragentenqueuing.htm";
    private static final String RECAPTCHA_ENABLED_TOGGLE = "/admin/appconfig/toggle/recaptcha.htm";
    private static final String PROMOCODE_ENABLED_TOGGLE = "/admin/appconfig/toggle/promocode.htm";
    private static final String PROMOREWARD_ENABLED_TOGGLE = "/admin/appconfig/toggle/promoreward.htm";
    private static final String FB_FRAUD_CHECK_TOGGLE = "/admin/appconfig/toggle/fbfraudcheck.htm";
    private static final String RECHARGE_RETRY_ENABLED_TOGGLE = "/admin/appconfig/toggle/rechargeretry.htm";
    private static final String ZEDO_AD_ENABLED_TOGGLE = "/admin/appconfig/toggle/zedoad.htm";
    private static final String PEPSI_CAMPAIGN_ENABLED_TOGGLE = "/admin/appconfig/toggle/pepsicampaign.htm";
    private static final String DID_CAMPAIGN_ENABLED_TOGGLE = "/admin/appconfig/toggle/didcampaign.htm";
    private static final String MCD_CAMPAIGN_ENABLED_TOGGLE = "/admin/appconfig/toggle/mcdcampaign.htm";
    private static final String FESTIVE_GIFT_ENABLED_TOGGLE = "/admin/appconfig/toggle/festivegift.htm";
    private static final String SOKARTI_PIXEL_ENABLED_TOGGLE = "/admin/appconfig/toggle/sokratipixel.htm";
    private static final String CARD_STORAGE_ENABLED_TOGGLE = "/admin/appconfig/toggle/cardstorageenabled.htm";
    public static final String ANTIFRAUD_FRAUDFRESH_TOGGLE = "/admin/appconfig/toggle/fraudfresh.htm";
    public static final String ANTIFRAUD_FRAUD3000_TOGGLE  = "/admin/appconfig/toggle/fraud3000.htm";
    public static final String ANTIFRAUD_FRAUD5000_TOGGLE  = "/admin/appconfig/toggle/fraud5000.htm";
    public static final String ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_TOGGLE = "/admin/appconfig/toggle/fraudregistertotxn.htm";
    public static final String ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_TOGGLE = "/admin/appconfig/toggle/fraudticketsize.htm";
    public static final String FB_FRAUD_CONDITIONS_URL = "/admin/appconfig/fraud/fbconditions.htm";
    public static final String FREEFUND_FRAUD_APPLY_CHECK_TOGGLE = "/admin/appconfig/toggle/freefundapplycheck.htm";
    public static final String FACEBOOK_LOGIN_ENABLED_TOGGLE = "/admin/appconfig/toggle/facebooklogin.htm";
    public static final String GOOGLE_LOGIN_ENABLED_TOGGLE = "/admin/appconfig/toggle/googlelogin.htm";
    public static final String MISSED_RECHARGE_ATTEMPT_TOGGLE = "/admin/appconfig/toggle/missedrechargeattempt.htm";
    public static final String BILLDESK_SERVER_TO_SERVER_COMM_ENABLED ="/admin/appconfig/toggle/bds2scommenabled.htm";
    public static final String PAYU_SERVER_TO_SERVER_COMM_ENABLED ="/admin/appconfig/toggle/payus2scommenabled.htm";
    
    public static final String RECHARGE_PERMANENT_FAILURE_BLOCK_ENABLED = "/admin/appconfig/toggle/permfailureblock.htm";
    public static final String POSTPAID_PERMANENT_FAILURE_BLOCK_ENABLED = "/admin/appconfig/toggle/permpostpaidfailureblock.htm";
    public static final String LAPU_ENABLED = "/admin/appconfig/toggle/lapu.htm";
    public static final String RECHARGE_DENOMINATION_FAILURE_NUMBER_BLOCK_ENABLED = "/admin/appconfig/toggle/denfailurenumberblock.htm";
    public static final String RECHARGE_INVALID_DENOMINATION_BLOCK_ENABLED = "/admin/appconfig/toggle/invaliddenominationblock.htm";
    public static final String RECHARGE_OPERATOR_ALERT_BLOCK_ENABLED = "/admin/appconfig/toggle/operatoralertlock.htm";
    public static final String RECHARGE_EG_BLOCK_ENABLED = "/admin/appconfig/toggle/egblock.htm";

    public static final String IP_CHECK_ENABLED = "/admin/appconfig/toggle/ipcheck.htm";
    public static final String IP_CHECK_CONDITIONS = "/admin/appconfig/ipcheckconditions.htm";
    
    public static final String FORCE_PAID_COUPON_TOGGLE  = "/admin/appconfig/toggle/forcepaidcoupons.htm";
    public static final String IS_RUPAY_ENABLED_TOGGLE  = "/admin/appconfig/toggle/isrupayenabled.htm";
    public static final String FORCE_RANDOM_COUPON_ORDER_TOGGLE  = "/admin/appconfig/toggle/forcerandomcouponorder.htm";

    public static final String EXCLUSIVE_COUPON_TOGGLE  = "/admin/appconfig/toggle/exCoupons.htm";
    public static final String RECO_COUPON_TOGGLE  = "/admin/appconfig/toggle/recoCoupons.htm";
    public static final String AB_RECO_COUPON_TOGGLE  = "/admin/appconfig/toggle/abRecoCoupons.htm";
    public static final String AB_CITY_FILTER_COUPON_TOGGLE  = "/admin/appconfig/toggle/abCityFilterCoupons.htm";
    
    public static final String COUPON_REMOTING_TOGGLE  = "/admin/appconfig/toggle/couponRemoting.htm";
    public static final String UMS_REMOTING_TOGGLE  = "/admin/appconfig/toggle/umsRemoting.htm";
    public static final String BILLPAY_REMOTING_TOGGLE  = "/admin/appconfig/toggle/billPayRemoting.htm";
    public static final String HCOUPON_FULFILLMENT_REMOTING_TOGGLE  = "/admin/appconfig/toggle/hCouponFulfillmentRemoting.htm";
    public static final String COUPON_BLOCK_REMOTING_TOGGLE  = "/admin/appconfig/toggle/couponBlockRemoting.htm";
    public static final String COUPON_ALL_FRAUD_CHECK_TOGGLE  = "/admin/appconfig/toggle/couponAllFraud.htm";
    public static final String COUPON_FRAUD_COOKIE_TOGGLE  = "/admin/appconfig/toggle/couponCookieFraud.htm";
    public static final String COUPON_FRAUD_IP_TOGGLE  = "/admin/appconfig/toggle/couponIPFraud.htm";
    public static final String COUPON_FRAUD_EMAIL_TOGGLE  = "/admin/appconfig/toggle/couponEmailFraud.htm";
    public static final String COUPON_FRAUD_PROFILENO_TOGGLE  = "/admin/appconfig/toggle/couponProfileNoFraud.htm";
    public static final String COUPON_CITY_FILTERDOWN_TOGGLE  = "/admin/appconfig/toggle/couponCityFilterDown.htm";
    public static final String COUPON_FRAUD_IMEINO_TOGGLE = "/admin/appconfig/toggle/couponImeiNoFraud.htm";
    public static final String COUPON_FRAUD_USER_ID = "/admin/appconfig/toggle/couponUserIdFraud.htm";

    public static final String SURVEY_TOGGLE  = "/admin/appconfig/toggle/survey.htm";

    public static final String APPGAL_TOGGLE  = "/admin/appconfig/toggle/appgal.htm";
    
    public static final String FINGER_PRINTING_ENABLED_TOGGLE = "/admin/appconfig/toggle/fingerprinting.htm";
    public static final String FINGER_PRINTING_FRAUD_TOGGLE = "/admin/appconfig/toggle/fingerprintingfraud.htm";
    public static final String IMEI_FRAUD_TOGGLE = "/admin/appconfig/toggle/imeifraud.htm";

    public static final String RECHARGE_RETRY_TOGGLE  = "/admin/appconfig/toggle/rechargeRetry.htm";
    public static final String AGGREGATOR_RETRY_TOGGLE  = "/admin/appconfig/toggle/aggregatorRetry.htm";
    public static final String FAILED_LOGIN_CAPTCHA_TOGGLE  = "/admin/appconfig/toggle/failedLoginReCaptchaEnabled.htm";

    public static final String SQS_ENQUEUE_ENABLED_TOGGLE = "/admin/appconfig/toggle/sqsenqueue.htm";
    public static final String ANTIFRAUD_CREDITS_PAY_TOGGLE = "/admin/appconfig/toggle/fraudcreditspay.htm";
    public static final String ANTIFRAUD_ADD_CASH_VELOCITY_TOGGLE = "/admin/appconfig/toggle/fraudaddcashvelocity.htm";
    public static final String ANTIFRAUD_REFUND_TO_PG_TOGGLE = "/admin/appconfig/toggle/fraudrefundtopg.htm";
    public static final String ANTIFRAUD_ADD_CASH_AMOUNT_LIMIT_TOGGLE = "/admin/appconfig/toggle/fraudaddcashamountlimit.htm";
    public static final String RECHARGE_AUTO_ALERT_TOGGLE = "/admin/appconfig/toggle/autoalert.htm";
    public static final String COUPON_SERVICE_PROFILING_TOGGLE = "/admin/appconfig/toggle/couponServiceProfiling.htm";
    private static final String TRACKING_SQS_ENABLED_TOGGLE = "/admin/appconfig/toggle/trackingSqs.htm";
    public static final String COUPON_CONFIGURABLE_CATEGORIES_TOGGLE = "/admin/appconfig/toggle/couponCategoryToggle.htm";
    public static final String READ_DENOMINATION_VALIDITY_FROM_DYNAMO_TOGGLE = "/admin/appconfig/toggle/readDenoValFromDynamoToggle.htm";
    private static final String TRACKER_SERVICE_ENABLED_TOGGLE = "/admin/appconfig/toggle/trackerService.htm";
    private static final String EMAIL_APP_ENABLED_TOGGLE = "/admin/appconfig/toggle/emailapp.htm";
    private static final String WEB_ASSETS_VERSION_TOGGLE = "/admin/appconfig/toggle/webAssetsVersion.htm";
    private static final String EMAIL_APP_ENABLED_FOR_COUPON_TOGGLE = "/admin/appconfig/toggle/emailappForCoupon.htm";
    private static final String ENABLE_RECHARGE_API = "/admin/appconfig/toggle/enablerechargeapi.htm";
    private static final String RECHARGE_OPERATOR_RETRY_TOGGLE = "/admin/appconfig/toggle/rechargeoperatorretry.htm";
    private static final String ENABLE_SMS_TEMPLATES_TOGGLE = "/admin/appconfig/toggle/smstemplates.htm";
    private static final String RECHARGE_CALLBACK_ENABLED = "/admin/appconfig/toggle/enablerechargecallback.htm";
    private static final String AG_PREFERENCE_ENABLED = "/admin/appconfig/toggle/agpreference.htm";
    private static final String AG_EURONET_VAS2_ENABLED = "/admin/appconfig/toggle/euronetvaswsdl.htm";

    private static final String SPLIT_TRAFFIC_WEIGHT = "/admin/appconfig/update/splitTrafficWeight.htm";
    private static final String APP_COUPON_CITY_FILTER_TOGGLE = "/admin/appconfig/toggle/appcityfilter.htm";
    private static final String COUPON_CITY_FILTER_TOGGLE = "/admin/appconfig/toggle/couponcityfilter.htm";
    private static final String LOCATION_SAVING_TOGGLE = "/admin/appconfig/toggle/savelocation.htm";
    private static final String COUPON_SAVE_IMPRESSION_TOGGLE = "/admin/appconfig/toggle/couponSaveImpression.htm";
    private static final String RECHARGE_REMINDER_TOGGLE = "/admin/appconfig/toggle/rechargeReminder.htm";

    private static final String CLOSED_IMEI_GROUP_TOGGLE = "/admin/appconfig/toggle/closedimeigroup.htm";
    private static final String MARS_TOGGLE = "/admin/appconfig/toggle/mars.htm";
    private static final String IVRS_TOGGLE = "/admin/appconfig/toggle/ivrs.htm";
    private static final String POSTPAID_DATACARD_RETRY_TOGGLE = "/admin/appconfig/toggle/postpaidDatacardRetry.htm";

    private static final String USER_MESSAGE_STORE_TOGGLE = "/admin/appconfig/toggle/userMessageStore.htm";
    
    private static final String AIRTEL_EURO_NEW_WALLET_TOGGLE = "/admin/appconfig/toggle/airtelEuroNewWallet.htm";

    private static final String OPERATOR_RETRY_FOR_ALL_ERROR_CODES = "/admin/appconfig/toggle/operatorRetryAllErrorCode.htm";
    private static final String OPERATOR_RETRY_ONLY_FOR_NUTS = "/admin/appconfig/toggle/operatorRetryNuts.htm";
    
    private static final String CENTAUR_ENABLED_FOR_RANDOM_COUPON = "/admin/appconfig/toggle/centaurRandomCoupon.htm";
    private static final String IS_ONE_CHECK_ENABLED_FOR_FORTKNOX = "/admin/appconfig/toggle/isOneCheckFKEnabled.htm";
    
    private static final String IS_ONE_CHECK_WALLET_ENABLED = "/admin/appconfig/toggle/isOneCheckWalletEnabled.htm";
    
    private static final String VCARD_CREATE_ENABLED = "/admin/appconfig/toggle/vCardCreateEnabled.htm";

    private static final String LOAD_ANTI_FRAUD_PROPS_FROM_FILE = "/admin/appconfig/toggle/loadAntiFraudPropsFromFile.htm";
    
    private static final String      CARD_STATUS_TOGGLE                                 = "/admin/appconfig/toggle/cardstatus.htm";
	private static final String SMS_GUPSHUP_ENABLE_TOGGLE = "/admin/appconfig/toggle/smsGupshupHLREnable.htm";
	private static final String INVALID_NUMBER_BLCOKING_TOGGLE ="/admin/appconfig/toggle/invalidNumberBlockingEnable";
	private static final String TD_PLAN_VALIDATION_TOGGLE ="/admin/appconfig/toggle/tdPlanValidationEnabled";
	private static final String REPEAT_TRANSACTION_BLOCKING_TOGGLE ="/admin/appconfig/toggle/repeatRechargeBlockingEnable";
	private static final String CREATE_BILL_V2_ENABLE_TOGGLE ="/admin/appconfig/toggle/createBillV2Enable";
	private static final String POSTPAID_FETCHBILL_VALIDATION_ENABLE_SUPPORT ="/admin/appconfig/toggle/postpaidFetchBillSupportEnable";
	private static final String DB_UPDATE_CALLBACK_ENABLE_TOGGLE ="/admin/appconfig/toggle/dbUpdateCallbackEnable";
	
	private static final String KLICKPAY_DOWNTIME_API ="/admin/appconfig/toggle/klickpaydowntimeAPI";
	private static final String KLICKPAY_NB_DOWNTIME_API ="/admin/appconfig/toggle/klickpayNBdowntimeAPI";
	private static final String KLICKPAY_SERVER_TO_SERVER_COMM_ENABLED = "/admin/appconfig/toggle/kps2scommenabled.htm";
    private static final String IS_ALWAYS_REFUND_ON_DELAYED_PAYMENT_SUCCESS = "/admin/appconfig/toggle/isAlwaysRefundOnDelayedPaymentSuccess.htm";
    private static final String IS_NULL_FAILURE_REFUND_COMMUNICATION_ENABLED = "/admin/appconfig/toggle/isNullToFailureRefundCommunicationEnabled.htm";
    private static final String IS_NULL_SUCCESS_REFUND_COMMUNICATION_ENABLED = "/admin/appconfig/toggle/isNullToSuccessRefundCommunicationEnabled.htm";
    private static final String BURST_DEBIT_ATM_PRODUCTS_CACHE = "/admin/appconfig/cache/debitatmproduct.htm";
    
    private static final String CENTAUR_ENABLED_TOGGLE = "/admin/appconfig/toggle/centaurEnabledABTesting.htm";
    private static final String CONVENIENCE_FEE_ENABLED_TOGGLE = "/admin/appconfig/toggle/convenienceFeeEnabled.htm";
    private static final String NEW_FAIL_FULFILMENT_TOGGLE = "/admin/appconfig/toggle/newFailureFulfilmentEnabled.htm";
    private static final String DTH_NUMBER_VALIDATION_TOGGLE ="/admin/appconfig/toggle/dthNumberValidationEnabled";

    private static final String INTRANSACTION_PLAN_VALIDATION_TOGGLE = "/admin/appconfig/toggle/intransactionValidationEnabled";    
    private static final String TIME_BASED_REFUND_TOGGLE = "/admin/appconfig/toggle/timeBasedRefundEnabled";
    private static final String INVALID_PLANS_DENOMINATION_VALIDATION_TOGGLE = "/admin/appconfig/toggle/invalidPlansDenominationValidationEnabled";
    private static final String DATA_CARD_CIRCLE_BLOCKING_TOGGLE = "/admin/appconfig/toggle/dataCardCircleBlockingEnabled";

    


    private static final String AUTO_PAY_TOGGLE ="/admin/appconfig/toggle/autoPayEnabled";
    
    private static final String AUTO_PAY_UTILITY_TOGGLE ="/admin/appconfig/toggle/autoPayEnabledForUtility";
    
    private static final String FULFILLBILL_NEW_ENABLE_TOGGLE ="/admin/appconfig/toggle/fulfillBillNewEnabled";
    private static final String FETCHBILL_NEW_ENABLE_TOGGLE ="/admin/appconfig/toggle/fetchBillNewEnabled";
    private static final String PLAN_LEVEL_RETRY_TOGGLE = "/admin/appconfig/toggle/planLevelRetryEnabled";
    private static final String EURONET_NEW_ERROR_CODE_TOGGLE = "/admin/appconfig/toggle/euronetNewErrorCodeEnabled";
    private static final String BILLPAY_OPERATOR_CHANNEL_BLOCK_TOGGLE = "/admin/appconfig/toggle/billpayOperatorChannelBlockingEnabled";
    private static final String DTH_PLAN_VALIDATION_ENABLED = "/admin/appconfig/toggle/dthPlanValidationEnabled";
    private static final String JIO_NUMBER_VALIDATION_ENABLED = "/admin/appconfig/toggle/jioNumberValidationEnabled";

    private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
	private InMemoryAppConfigService inMemoryAppConfigService;
	
	@Autowired
	PaymentsCache paymentsCache;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.APP_CONFIG)){
            get403HtmlResponse(response);
            return null;
        }
        ModelAndView modelAndView = new ModelAndView(ViewConstants.APP_CONFIG);
        List<View> view = new LinkedList<View>();
        view.add(new View("Async Recharge", String.valueOf(appConfig.isAsyncRechargeEnabled()), ASYNC_RECHARGE_TOGGLE));
        view.add(new View("IMS Direct hit",String.valueOf(appConfig.isIMSDirectHitEnabled()),IMS_DIRECT_HIT_TOGGLE));
        view.add(new View("Email Banner Enabled",String.valueOf(appConfig.isEmailBannerEnabled()),EMAIL_BANNER_ENABLED_TOGGLE));
        view.add(new View("Euronet Https Enabled",String.valueOf(appConfig.isEuronetHttpsEnabled()),EURONET_HTTPS_ENABLED_TOGGLE));
        view.add(new View("Async Bill Payment", String.valueOf(appConfig.isAsyncBillPaymentEnabled()), ASYNC_BILL_PAYMENT_TOGGLE));
        view.add(new View("BBPS Bill Payment", String.valueOf(appConfig.isAsyncBillPaymentEnabled()), BBPS_BILL_PAYMENT_TOGGLE));
        view.add(new View("Under Maintenance Home Page Banner", String.valueOf(appConfig.isUnderMaintenanceHeaderBannerEnabled()), UNDER_MAINTENANCE_HEADER_BANNER_TOGGLE));
        view.add(new View("Under Maintenance Payment Page", String.valueOf(appConfig.isPaymentUnderMaintenance()), PAYMENT_UNDER_MAINTENANCE_TOGGLE));
        view.add(new View("Cash Back Campaign", String.valueOf(appConfig.isCashBackCampaignEnabled()), CASH_BACK_CAMPAIGN_TOGGLE));
        view.add(new View("ARDeal Feature", String.valueOf(appConfig.isARDealEnabled()), ARDEAL_FEATURE_TOGGLE)); 
        view.add(new View("Website Analytics", String.valueOf(appConfig.isWebSiteAnalyticsEnabled()), WEBSITE_ANALYTICS_FEATURE_TOGGLE));
        view.add(new View("Mobilesite Analytics", String.valueOf(appConfig.isMobileSiteAnalyticsEnabled()), MOBILESITE_ANALYTICS_FEATURE_TOGGLE));
        view.add(new View("Final Page Coupons", String.valueOf(appConfig.isFinalPageCouponsEnabled()), FINAL_PAGE_COUPONS_TOGGLE));
        view.add(new View("Tracking SQS", String.valueOf(appConfig.isTrackingSqsEnabled()), TRACKING_SQS_ENABLED_TOGGLE));
        view.add(new View("Tracker Service", String.valueOf(appConfig.isTrackerServerEnabled()), TRACKER_SERVICE_ENABLED_TOGGLE));
        view.add(new View("User Agent Equeuing", String.valueOf(appConfig.isUserAgentEnqueuingEnabled()), USER_AGENT_ENQUEUING_ENABLED_TOGGLE));
        view.add(new View("ReCaptcha", String.valueOf(appConfig.isRecaptchaEnabled()), RECAPTCHA_ENABLED_TOGGLE));
        view.add(new View("Promocode", String.valueOf(appConfig.isPromocodeEnabled()), PROMOCODE_ENABLED_TOGGLE));
        view.add(new View("PromoReward", String.valueOf(appConfig.isPromoRewardEnabled()), PROMOREWARD_ENABLED_TOGGLE));
        view.add(new View("Recharge Retry Block Enabled", String.valueOf(appConfig.isRechargeRetryBlockEnabled()), RECHARGE_RETRY_ENABLED_TOGGLE));
        view.add(new View("Zedo Ad Enabled", String.valueOf(appConfig.isZedoAdEnabled()), ZEDO_AD_ENABLED_TOGGLE));
        view.add(new View("Pepsi Campaign", String.valueOf(appConfig.isPepsiCampaignEnabled()), PEPSI_CAMPAIGN_ENABLED_TOGGLE));
        view.add(new View("DID Campaign", String.valueOf(appConfig.isDIDCampaignEnabled()), DID_CAMPAIGN_ENABLED_TOGGLE));
        view.add(new View("McD Campaign", String.valueOf(appConfig.isMCDCampaignEnabled()), MCD_CAMPAIGN_ENABLED_TOGGLE));
        view.add(new View("Festive Gift", String.valueOf(appConfig.isFestiveGiftEnabled()), FESTIVE_GIFT_ENABLED_TOGGLE));
        view.add(new View("Sokrati Pixel Enabled", String.valueOf(appConfig.isSokratiPixelEnabled()), SOKARTI_PIXEL_ENABLED_TOGGLE));
        view.add(new View("Card Storage Enabled", String.valueOf(appConfig.isCardStorageEnabled()), CARD_STORAGE_ENABLED_TOGGLE));
        view.add(new View("FB Fraud Check", String.valueOf(appConfig.isFbFraudCheckEnabled()), FB_FRAUD_CHECK_TOGGLE));
        view.add(new View("Freefund Apply Cookie Check Enabled", String.valueOf(appConfig.isFreefundApplycheckEnabled()), FREEFUND_FRAUD_APPLY_CHECK_TOGGLE));
        view.add(new View("Facebook Login Enabled", String.valueOf(appConfig.isFacebookLoginEnabled()), FACEBOOK_LOGIN_ENABLED_TOGGLE));
        view.add(new View("Google Login Enabled", String.valueOf(appConfig.isGoogleLoginEnabled()), GOOGLE_LOGIN_ENABLED_TOGGLE));
        view.add(new View("IP Check Enabled", String.valueOf(appConfig.isIpCheckEnabled()), IP_CHECK_ENABLED));
        view.add(new View("Missed Recharge Attempt", String.valueOf(appConfig.isMissedRechargeAttemptEnabled()), MISSED_RECHARGE_ATTEMPT_TOGGLE));
        view.add(new View("BillDesk Server to Server Enabled",String.valueOf(appConfig.isBillDeskS2SEnabled()), BILLDESK_SERVER_TO_SERVER_COMM_ENABLED));
        view.add(new View("PayU Server to Server Enabled",String.valueOf(appConfig.isPayUS2SEnabled()), PAYU_SERVER_TO_SERVER_COMM_ENABLED));
        
        view.add(new View("Force Paid Coupons", String.valueOf(appConfig.isForcePaidCouponsEnabled()), FORCE_PAID_COUPON_TOGGLE));
        view.add(new View("Force Random Coupons Order", String.valueOf(appConfig.isForceRandomCouponsOrderEnabled()), FORCE_RANDOM_COUPON_ORDER_TOGGLE));
        
        view.add(new View("Is Exclusive Coupons Enabled", String.valueOf(appConfig.isExclusiveCouponsEnabled()), EXCLUSIVE_COUPON_TOGGLE));
        view.add(new View("Is Coupon Recommendations Enabled", String.valueOf(appConfig.isRecoCouponsEnabled()), RECO_COUPON_TOGGLE));
        view.add(new View("Is AB TEST Coupon Recommendations Enabled", String.valueOf(appConfig.isABRecoCouponsEnabled()), AB_RECO_COUPON_TOGGLE));
        view.add(new View("Is AB TEST Coupon City Filter Enabled", String.valueOf(appConfig.isABCityFilterCouponsEnabled()), AB_CITY_FILTER_COUPON_TOGGLE));
        
        view.add(new View("Coupon Remoting Enabled", String.valueOf(appConfig.isCouponRemotingEnabled()), COUPON_REMOTING_TOGGLE));
        view.add(new View("Ums Remoting Enabled", String.valueOf(appConfig.isUmsRemotingEnabled()), UMS_REMOTING_TOGGLE));
        view.add(new View("BillPay Remoting Enabled", String.valueOf(appConfig.isBillPayRemotingEnabled()), BILLPAY_REMOTING_TOGGLE));
        view.add(new View("HCoupon Fulfillment Remoting Enabled", String.valueOf(appConfig.isHCouponFulfillmentEnabled()), HCOUPON_FULFILLMENT_REMOTING_TOGGLE));
        view.add(new View("Coupon Block Remoting Enabled", String.valueOf(appConfig.isCouponBlockRemotingEnabled()), COUPON_BLOCK_REMOTING_TOGGLE));
        view.add(new View("Coupon Fraud Checks Enabled", String.valueOf(appConfig.isCouponFraudCheckEnabled()), COUPON_ALL_FRAUD_CHECK_TOGGLE));
        view.add(new View("Coupon Fraud Cookie Check Enabled", String.valueOf(appConfig.isCouponFraudCookieEnabled()), COUPON_FRAUD_COOKIE_TOGGLE));
        view.add(new View("Coupon Fraud IP Check Enabled", String.valueOf(appConfig.isCouponFraudIPEnabled()), COUPON_FRAUD_IP_TOGGLE));
        view.add(new View("Coupon Fraud Email Check Enabled", String.valueOf(appConfig.isCouponFraudEmailEnabled()), COUPON_FRAUD_EMAIL_TOGGLE));
        view.add(new View("Coupon Fraud Profile Number Check Enabled", String.valueOf(appConfig.isCouponFraudProfileNoEnabled()), COUPON_FRAUD_PROFILENO_TOGGLE));
        view.add(new View("Coupon City Filter Down", String.valueOf(appConfig.isCouponCityFilterDownEnabled()), COUPON_CITY_FILTERDOWN_TOGGLE));
        view.add(new View("Coupon Fraud Imei Number Check Enabled", String.valueOf(appConfig.isCouponFraudImeiNoEnabled()), COUPON_FRAUD_IMEINO_TOGGLE));
        view.add(new View("Coupon Fraud User Id Check Enabled", String.valueOf(appConfig.isCouponFraudImeiNoEnabled()), COUPON_FRAUD_USER_ID));
        
        view.add(new View ("Final Page Survey Enabled", String.valueOf(appConfig.isSurveyEnabled()), SURVEY_TOGGLE));

        view.add(new View ("App Gallery Enabled", String.valueOf(appConfig.isappGalEnabled()), APPGAL_TOGGLE));

        view.add(new View("Recharge Permanent Failure Block Enabled", String.valueOf(appConfig.isPermanentFailureBlockEnabled()), RECHARGE_PERMANENT_FAILURE_BLOCK_ENABLED));
        view.add(new View("Postpaid Permanent Failure Block Enabled", String.valueOf(appConfig.isPostpaidPermanentFailureBlockEnabled()), POSTPAID_PERMANENT_FAILURE_BLOCK_ENABLED));
        view.add(new View("Recharge Invalid Denomination Number Block Enabled", String.valueOf(appConfig.isDenominationNumberBlockEnabled()), RECHARGE_DENOMINATION_FAILURE_NUMBER_BLOCK_ENABLED));
        view.add(new View("Recharge Invalid Denomination Block Enabled", String.valueOf(appConfig.isInvalidDenominationBlockEnabled()), RECHARGE_INVALID_DENOMINATION_BLOCK_ENABLED));
        view.add(new View("Recharge Operator Alert Block Enabled", String.valueOf(appConfig.isOperatorAlertBlockEnabled()), RECHARGE_OPERATOR_ALERT_BLOCK_ENABLED));
        view.add(new View("Recharge EG Block Enabled", String.valueOf(appConfig.isEGNumberBlockEnabled()), RECHARGE_EG_BLOCK_ENABLED));
        
        view.add(new View("SQS Enqueue Enabled", String.valueOf(appConfig.isSQSEnqueueEnabled()), SQS_ENQUEUE_ENABLED_TOGGLE));
        view.add(new View("Recharge Auto Alert Enabled", String.valueOf(appConfig.isRechargeAutoAlertEnabled()), RECHARGE_AUTO_ALERT_TOGGLE));
        
        view.add(new View("Recharge Retry Enabled", String.valueOf(appConfig.isRechargeRetryEnabled()), RECHARGE_RETRY_TOGGLE));
        view.add(new View("Aggregator Retry Enabled", String.valueOf(appConfig.isAggregatorRetryEnabled()), AGGREGATOR_RETRY_TOGGLE));
        view.add(new View("Recharge Operator Retry Enabled", String.valueOf(appConfig.isOperatorRetryEnabled()), RECHARGE_OPERATOR_RETRY_TOGGLE));
        view.add(new View("Failed Login Recaptcha Enabled", String.valueOf(appConfig.isFailedLoginRecaptchaEnabled()),
                FAILED_LOGIN_CAPTCHA_TOGGLE));
        view.add(new View("Is Rupay Enabled", String.valueOf(appConfig.isRupayEnabled()), IS_RUPAY_ENABLED_TOGGLE));
        view.add(new View("Lapu Enabled", String.valueOf(appConfig.isLapuEnabled()), LAPU_ENABLED));
        view.add(new View("Coupon Configurable Category", String.valueOf(appConfig.isConfigurableCouponCategoriesEnabled()), COUPON_CONFIGURABLE_CATEGORIES_TOGGLE));
        view.add(new View("Mongo Size Record", String.valueOf(appConfig.isMongoSizeRecordEnabled()), MONGO_SIZE_RECORD_TOGGLE));
        view.add(new View("Mongo Size Record", String.valueOf(appConfig.isMongoSizeRecordEnabled()), MONGO_SIZE_RECORD_TOGGLE));
        view.add(new View("Read Denomination Validity From Dynamo", String.valueOf(appConfig.isReadDenominationValidityFromDynamo()), READ_DENOMINATION_VALIDITY_FROM_DYNAMO_TOGGLE));

        view.add(new View("Email app enabled", String.valueOf(appConfig.isEmailAppEnabled()), EMAIL_APP_ENABLED_TOGGLE));
        view.add(new View("Enable Recharge API", String.valueOf(appConfig.isRechargeApiEnabled()), ENABLE_RECHARGE_API));
        view.add(new View("Enable SMS Templates", String.valueOf(appConfig.isSMSTemplatesEnabled()), ENABLE_SMS_TEMPLATES_TOGGLE));
        view.add(new View("Recharge Callback enabled", String.valueOf(appConfig.isRechargeCallbackEnabled()), RECHARGE_CALLBACK_ENABLED));
        view.add(new View("Ag preference enabled", String.valueOf(appConfig.isAgPreferenceEnabled()), AG_PREFERENCE_ENABLED));
        view.add(new View("Euronet wsdl vas2 enabled", String.valueOf(appConfig.isEuronetVas2Enabled()), AG_EURONET_VAS2_ENABLED));
        view.add(new View("SMS Gupshup HLR Enabled", String.valueOf(appConfig.isSMSGupshupHLREnabled()), SMS_GUPSHUP_ENABLE_TOGGLE));
        view.add(new View("Invalid Number Blocking Enabled", String.valueOf(appConfig.isInvalidNumberBlockingEnabled()), INVALID_NUMBER_BLCOKING_TOGGLE));

        view.add(new View("Plan Validation Enabled", String.valueOf(appConfig.isPlanValidationEnabled()),TD_PLAN_VALIDATION_TOGGLE));
        view.add(new View("New Create Bill Call Enabled", String.valueOf(appConfig.isCreateBillV2Enabled()),CREATE_BILL_V2_ENABLE_TOGGLE));
        view.add(new View("Enable DB Updation For CallBack APIs", String.valueOf(appConfig.isDBUpdateOnCallBackEnabled()), DB_UPDATE_CALLBACK_ENABLE_TOGGLE));

        view.add(new View("Repeat Recharge Blocking Enabled",String.valueOf(appConfig.isRepeatRechargeBlockingEnabled()),REPEAT_TRANSACTION_BLOCKING_TOGGLE));
        view.add(new View("Fetch Bill Support For Postpaid Enabled",String.valueOf(appConfig.isPostPaidFetchBillSupportEnabled()),POSTPAID_FETCHBILL_VALIDATION_ENABLE_SUPPORT));

        view.add(new View("Use Klickpay for Card Downtime API",String.valueOf(appConfig.useKlickpayForDowntimeAPI()),KLICKPAY_DOWNTIME_API));
        
        view.add(new View("Use Klickpay for NB Downtime API",String.valueOf(appConfig.useKlickpayForNBDowntimeAPI()),KLICKPAY_NB_DOWNTIME_API));
        
        view.add(new View("KlickPay Server to Server Enabled",String.valueOf(appConfig.isKlickPayS2SEnabled()), KLICKPAY_SERVER_TO_SERVER_COMM_ENABLED));

        view.add(new View("DTH Number Validation Enabled",String.valueOf(appConfig.isDTHNumberValidationEnabled()), DTH_NUMBER_VALIDATION_TOGGLE));

        view.add(new View("InTransaction Plan Validation Enabled",String.valueOf(appConfig.isInTransactionValidationEnabled()), INTRANSACTION_PLAN_VALIDATION_TOGGLE));

        view.add(new View("AutoPay Enabled",String.valueOf(appConfig.isAutoPayEnabled()), AUTO_PAY_TOGGLE));
        
        view.add(new View("AutoPay Enabled For Utility",String.valueOf(appConfig.isAutoPayEnabledForUtility()), AUTO_PAY_UTILITY_TOGGLE));

        view.add(new View("Time Based Refund Enabled",String.valueOf(appConfig.isTimeBasedRefundEnabled()),TIME_BASED_REFUND_TOGGLE));
        view.add(new View("Fulfill Bill New Enabled",String.valueOf(appConfig.isFulfillNewEnabled()),FULFILLBILL_NEW_ENABLE_TOGGLE));
        view.add(new View("Fetch Bill New Enabled",String.valueOf(appConfig.isFetchBillNewEnabled()),FETCHBILL_NEW_ENABLE_TOGGLE));

        view.add(new View("Invalid Plans Denomination Validation Enabled",String.valueOf(appConfig.isInvalidPlansDenominationValidationEnabled()), INVALID_PLANS_DENOMINATION_VALIDATION_TOGGLE));
        
        view.add(new View("DataCard Operator Circle Block Enabled",String.valueOf(appConfig.isDataCardCircleBlockingEnabled()), DATA_CARD_CIRCLE_BLOCKING_TOGGLE));
        
        view.add(new View("Plan Level Retry Enabled",String.valueOf(appConfig.isPlanLevelRetryEnabled()), PLAN_LEVEL_RETRY_TOGGLE));
        view.add(new View("Billpay Operator Channel Block Enabled",String.valueOf(appConfig.isBillpayOperatorChannelBlockingEnabled()), BILLPAY_OPERATOR_CHANNEL_BLOCK_TOGGLE));
        view.add(new View("Euronet New Error Codes Enabled",String.valueOf(appConfig.isEuronetNewErrorCodeEnabled()), EURONET_NEW_ERROR_CODE_TOGGLE));
        view.add(new View("DTH Plan Validation Enabled",String.valueOf(appConfig.isDTHPlanValidationEnabled()), DTH_PLAN_VALIDATION_ENABLED));
        view.add(new View("JIO Number Validation Enabled",String.valueOf(appConfig.isJIONumberValidationEnabled()), JIO_NUMBER_VALIDATION_ENABLED));
        
        modelAndView.addObject("view", view);

        final List<View> editableView = new LinkedList<View>();
        editableView.add(new View("splitTrafficWeight", String.valueOf(appConfig.getSplitTrafficWeight()), SPLIT_TRAFFIC_WEIGHT));
        modelAndView.addObject("editableView", editableView);

        // Shirish TODO: Note: you will also need to change appconfig.jsp.. we should have an easier way of supporting runtime properties.
        // Above is now configurable. Check link on appconfig page - 'Manage Custom App Config Properties.'
        modelAndView.addObject("fbFraudConditions", appConfig.getFbFraudConditions().toJSONString());
        modelAndView.addObject("ipCheckConditions", appConfig.getIpCheckConditions().toJSONString());
        modelAndView.addObject("couponFraudValues", appConfig.getCouponFraudValues().toJSONString());
        modelAndView.addObject("atmCoupons", appConfig.getATMCoupons().toJSONString());
        JSONObject carousel = appConfig.getCouponCarousel();
        modelAndView.addObject("CouponCarousel", (carousel == null ? "{}" : carousel.toJSONString() ));
        modelAndView.addObject("InMemoryAppConfig", inMemoryAppConfigService.getInMemoryCacheValues());
        modelAndView.addObject("testconditions", appConfig.getTestConditions().toJSONString());
        modelAndView.addObject("metroReportEmailIds", appConfig.getMetroReportEmailIds().toJSONString());
        modelAndView.addObject("forceUpgradeDetails",appConfig.getForceUpgradeDetails().toJSONString());
        

        view.add(new View("Finger Printing Enabled", String.valueOf(appConfig.isFingerPrintingEnabled()), FINGER_PRINTING_ENABLED_TOGGLE));
        view.add(new View("Web assets version (Toggle will bust cache)", String.valueOf(appConfig.getWebAssetsVersion()), WEB_ASSETS_VERSION_TOGGLE));
        view.add(new View("EmailApp enabled for coupons", String.valueOf(appConfig.isEmailAppEnabledForCoupon()), EMAIL_APP_ENABLED_FOR_COUPON_TOGGLE));
        view.add(new View("App Coupon City Filter Experiment", String.valueOf(appConfig.isAppCouponCityFilterEnabled()), APP_COUPON_CITY_FILTER_TOGGLE));
        view.add(new View("Coupon City Filter", String.valueOf(appConfig.isCityFilterEnabled()), COUPON_CITY_FILTER_TOGGLE));
        view.add(new View("Location Saving toggle", String.valueOf(appConfig.isLocationSavingEnabled()), LOCATION_SAVING_TOGGLE));
        view.add(new View("Coupon Save Impression Enabled", String.valueOf(appConfig.isSaveImpressionEnabled()), COUPON_SAVE_IMPRESSION_TOGGLE));
        view.add(new View("Closed Imei Group", String.valueOf(appConfig.isClosedImeiGroupEnabled()), CLOSED_IMEI_GROUP_TOGGLE));
        view.add(new View("Mars Enabled", String.valueOf(appConfig.isMarsEnabled()), MARS_TOGGLE));
        view.add(new View("IVRS Enabled", String.valueOf(appConfig.isIVRSEnabled()), IVRS_TOGGLE));
        view.add(new View("Postpaid Datacard Retry Enabled", String.valueOf(appConfig.isPostpaidDatacardRetryEnabled()), POSTPAID_DATACARD_RETRY_TOGGLE));
        view.add(new View("Operator Retry For All Error Codes", String.valueOf(appConfig.isOperatorRetryAllErrorCodeEnabled()), OPERATOR_RETRY_FOR_ALL_ERROR_CODES));
        view.add(new View("Operator Retry Only for New Users", String.valueOf(appConfig.isOperatorRetryOnlyNutsEnabled()), OPERATOR_RETRY_ONLY_FOR_NUTS));
        view.add(new View("Recharge Reminder Enabled", String.valueOf(appConfig.isRechargeReminderEnabled()), RECHARGE_REMINDER_TOGGLE));
        view.add(new View("Storing User Messages Enabled", String.valueOf(appConfig.isUserMessageStoreEnabled()), USER_MESSAGE_STORE_TOGGLE));
        view.add(new View("Random Coupon Experiment Enabled", String.valueOf(appConfig.isRandomCouponEnabled()),CENTAUR_ENABLED_FOR_RANDOM_COUPON));
        view.add(new View("Airtel Euronet New Wallet Enabled", String.valueOf(appConfig.isAirtelEuroNewWalletEnabled()), AIRTEL_EURO_NEW_WALLET_TOGGLE));
        view.add(new View("OneCheck + FortKnox or JusPay List Enabled", String.valueOf(appConfig.isOneCheckFKEnabled()), IS_ONE_CHECK_ENABLED_FOR_FORTKNOX));
        view.add(new View("OneCheck Wallet Enabled", String.valueOf(appConfig.isOneCheckWalletEnabled()), IS_ONE_CHECK_WALLET_ENABLED));
        view.add(new View("VCard Create Enabled", String.valueOf(appConfig.isVCardCreateEnabled()), VCARD_CREATE_ENABLED));
        view.add(new View("Always Refund On Delayed Payment Success", String.valueOf(appConfig.isAlwaysRefundOnDelayedPaymentSuccess()), IS_ALWAYS_REFUND_ON_DELAYED_PAYMENT_SUCCESS));
        view.add(new View("Null to failure refund communication enabled", String.valueOf(appConfig.isNullToFailureRefundCommunicationEnabled()), IS_NULL_FAILURE_REFUND_COMMUNICATION_ENABLED));
        view.add(new View("Null to success refund communication enabled", String.valueOf(appConfig.isNullToSuccessRefundCommunicationEnabled()), IS_NULL_SUCCESS_REFUND_COMMUNICATION_ENABLED));
        view.add(new View("Load Anti Fraud Props From File", String.valueOf(appConfig.isLoadAntiFraudPropsFromFileEnabled()), LOAD_ANTI_FRAUD_PROPS_FROM_FILE));
        view.add(new View("Enable Card Status Check", String.valueOf(appConfig.isCardStatusEnabled()), CARD_STATUS_TOGGLE));
        view.add(new View("Clear Debit ATM Products Cache", "", BURST_DEBIT_ATM_PRODUCTS_CACHE));
        view.add(new View("Centaur enabled for AB testing", String.valueOf(appConfig.isCentaurEnabledForABTesting()), CENTAUR_ENABLED_TOGGLE));
        view.add(new View("Convenience Fee Enabled", String.valueOf(appConfig.isConvenienceFeeEnabled()), CONVENIENCE_FEE_ENABLED_TOGGLE));
        view.add(new View("FailureFulfilmentActivity direct enqueue for new refund", String.valueOf(appConfig.isNewFailureFulfilmentEnabled()), NEW_FAIL_FULFILMENT_TOGGLE));
        return modelAndView;
	}

    private class OldAndNew<T> {
        public final T oldValue;
        public final T newValue;
        public OldAndNew(T oldValue, T newValue) {
            this.oldValue = oldValue;
            this.newValue = newValue;
        }
    }

    private interface PropertyChange<T> {
        public OldAndNew<T> change();
    }

    private static <T> String toggle(Model m, PropertyChange<T> change) {
        OldAndNew<T> values = change.change();
        return toggle(m, values.oldValue, values.newValue);
    }

    private static <T> String toggle(Model m, T oldValue, T newValue) {
        m.addAttribute(OLD_VALUE_KEY, oldValue);
        m.addAttribute(NEW_VALUE_KEY, newValue);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/savelocation.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleLocationSaving(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isLocationSavingEnabled();
                appConfig.setLocationSavingEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/convenienceFeeEnabled.htm", method = RequestMethod.POST, produces = "application/json")
	public String toggleConvenienceFeeEnabled(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isConvenienceFeeEnabled();
				appConfig.setConvenienceFeeEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}


    @RequestMapping(value = "toggle/closedimeigroup.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleClosedImeiGroup(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isClosedImeiGroupEnabled();
                appConfig.setClosedImeiGroupEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/isOneCheckWalletEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleOnecheckWallet(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isOneCheckWalletEnabled();
                appConfig.setOneCheckWalletEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/isAlwaysRefundOnDelayedPaymentSuccess.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleAlwaysRefundOnDelayedPaymentSuccess(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isAlwaysRefundOnDelayedPaymentSuccess();
                appConfig.setAlwaysRefundOnDelayedPaymentSuccess(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/isNullToFailureRefundCommunicationEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleNullToFailureRefundCommunicationEnabled(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isAlwaysRefundOnDelayedPaymentSuccess();
                appConfig.setNullToFailureRefundCommunicationEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/isNullToSuccessRefundCommunicationEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleNullToSuccessRefundCommunicationEnabled(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isAlwaysRefundOnDelayedPaymentSuccess();
                appConfig.setNullToSuccessRefundCommunicationEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }

    @RequestMapping(value = "toggle/mars.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleMars(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isMarsEnabled();
                appConfig.setMarsEnabled(!enabled);

                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }

    @RequestMapping(value = "toggle/ivrs.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleIVRS(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isIVRSEnabled();
                appConfig.setIVRSEnabled(!enabled);

                return new OldAndNew<>(enabled, !enabled);
            }
        });
    }

    @RequestMapping(value = "toggle/appcityfilter.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleAppCouponCityFilter(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isAppCouponCityFilterEnabled();
                appConfig.setAppCouponCityFilterEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }

    @RequestMapping(value = "toggle/couponcityfilter.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponCityFilter(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isCityFilterEnabled();
                appConfig.setCityFilterEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/couponSaveImpression.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponSaveImpression(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isSaveImpressionEnabled();
                appConfig.setSaveImpressionEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/rechargeReminder.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleRechareReminder(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isRechargeReminderEnabled();
                appConfig.setRechargeReminderEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }

    @RequestMapping(value = "toggle/autoalert.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleRechargeAutoAlertEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isRechargeAutoAlertEnabled();
                appConfig.setRechargeAutoAlertEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/sqsenqueue.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleSQSEnqueueEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isSQSEnqueueEnabled();
        appConfig.setSQSEnqueueEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/egblock.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleEGBlockEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isEGNumberBlockEnabled();
        appConfig.setEGNumberBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    @RequestMapping(value = "toggle/postpaidDatacardRetry.htm", method = RequestMethod.POST, produces = "application/json")
    public String togglePostpaidDatacardRetry() {
        boolean enabled = appConfig.isPostpaidDatacardRetryEnabled();
        appConfig.setPostpaidDatacardRetryEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/operatorRetryAllErrorCode.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleOperatorRetryAllErrorCode() {
        boolean enabled = appConfig.isOperatorRetryAllErrorCodeEnabled();
        appConfig.setOperatorRetryAllErrorCodeEnabled(!enabled);
        return "jsonView";
    }
    @RequestMapping(value = "toggle/operatorRetryNuts.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleOperatorRetryOnlyNuts() {
        boolean enabled = appConfig.isOperatorRetryOnlyNutsEnabled();
        appConfig.setOperatorRetryOnlyNutsEnabled(!enabled);
        return "jsonView";
    }
    
	@RequestMapping(value = "toggle/failedLoginReCaptchaEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleFailedLoginRecaptchaEnabled(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isFailedLoginRecaptchaEnabled();
        appConfig.setFailedLoginRecaptchaEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
             }
	    });
	}
	
	@RequestMapping(value = "toggle/operatoralertlock.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleOperatorAlrtBlockEnabled(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isOperatorAlertBlockEnabled();
        appConfig.setOperatorAlertBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
	
    @RequestMapping(value = "toggle/invaliddenominationblock.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleInvalidDenominationBlockEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isInvalidDenominationBlockEnabled();
        appConfig.setInvalidDenominationBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    
    @RequestMapping(value = "toggle/permfailureblock.htm", method = RequestMethod.POST, produces = "application/json")
    public String togglePermanentFailureBlockEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPermanentFailureBlockEnabled();
        appConfig.setPermanentFailureBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    
    @RequestMapping(value = "toggle/permpostpaidfailureblock.htm", method = RequestMethod.POST, produces = "application/json")
    public String togglePostpaidPermanentFailureBlockEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPostpaidPermanentFailureBlockEnabled();
        appConfig.setPostpaidPermanentFailureBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/lapu.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleLapuEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isLapuEnabled();
        appConfig.setLapuEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/denfailurenumberblock.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleDenominationNumberBlockEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isDenominationNumberBlockEnabled();
        appConfig.setDenominationNumberBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    
    @RequestMapping(value = "toggle/bds2scommenabled.htm", method = RequestMethod.POST, produces = "application/json")
	public String toggleBillDeskS2SEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isBillDeskS2SEnabled();
	    appConfig.setBillDeskS2SEnabled(!enabled);
	    return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/payus2scommenabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String togglePayUS2SEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPayUS2SEnabled();
        appConfig.setPayUS2SEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/missedrechargeattempt.htm", method = RequestMethod.POST, produces = "application/json")
	public String toggleMissedRechargeAttempt(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isMissedRechargeAttemptEnabled();
	    appConfig.setMissedRechargeAttemptEnabled(!enabled);
	    return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
	
	@RequestMapping(value = "toggle/freefundapplycheck", method = RequestMethod.POST, produces = "application/json")
    public String toggleFreefundapplycheck(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isFreefundApplycheckEnabled();
        appConfig.setFreefundApplycheckEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
	
    @RequestMapping(value = "toggle/cardstorageenabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleCardStorageEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCardStorageEnabled();
        appConfig.setCardStorageEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/exCoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleExclusiveCoupons(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isExclusiveCouponsEnabled();
        appConfig.setExclusiveCouponsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    
    @RequestMapping(value = "toggle/abRecoCoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleABRecoCoupons() {
        boolean enabled = appConfig.isABRecoCouponsEnabled();
        appConfig.setABRecoCouponsEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/abCityFilterCoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleABCityFilterCoupons() {
        boolean enabled = appConfig.isABCityFilterCouponsEnabled();
        appConfig.setABCityFilterCouponsEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/recoCoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleRecoCoupons(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRecoCouponsEnabled();
        appConfig.setRecoCouponsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponRemoting", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponRemoting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponRemotingEnabled();
        appConfig.setCouponRemotingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponBlockRemoting", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponBlockRemoting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponBlockRemotingEnabled();
        appConfig.setCouponBlockRemotingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/umsRemoting", method = RequestMethod.POST, produces = "application/json")
    public String toggleUmsRemoting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isUmsRemotingEnabled();
        appConfig.setUmsRemotingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/billPayRemoting", method = RequestMethod.POST, produces = "application/json")
    public String toggleBillPayRemoting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isBillPayRemotingEnabled();
        appConfig.setBillPayRemotingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/hCouponFulfillmentRemoting", method = RequestMethod.POST, produces = "application/json")
    public String toggleHCouponFulfillmentRemoting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isHCouponFulfillmentEnabled();
        appConfig.setHCouponFulfillmentEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponAllFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponAllFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudCheckEnabled();
        appConfig.setCouponFraudCheckEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/rechargeRetry", method = RequestMethod.POST, produces = "application/json")
    public String toggleRechargeRetry(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRechargeRetryEnabled();
        appConfig.setRechargeRetryEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/aggregatorRetry", method = RequestMethod.POST, produces = "application/json")
    public String toggleAggregatorRetry(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isAggregatorRetryEnabled();
        appConfig.setAggregatorRetryEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/rechargeoperatorretry", method = RequestMethod.POST, produces = "application/json")
    public String toggleOperatorRechargeRetry() {
        boolean enabled = appConfig.isOperatorRetryEnabled();
        appConfig.setOperatorRetryEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/couponCookieFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponCookieFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudCookieEnabled();
        appConfig.setCouponFraudCookieEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponIPFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponIPFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudIPEnabled();
        appConfig.setCouponFraudIPEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    
    @RequestMapping(value = "toggle/couponEmailFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponEmailFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudEmailEnabled();
        appConfig.setCouponFraudEmailEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponProfileNoFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponProfileNoFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudProfileNoEnabled();
        appConfig.setCouponFraudProfileNoEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponImeiNoFraud", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponImeiNoFraud(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCouponFraudImeiNoEnabled();
        appConfig.setCouponFraudImeiNoEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/couponCityFilterDown", method = RequestMethod.POST, produces = "application/json")
    public String toggleCouponCityFilterDown() {
        boolean enabled = appConfig.isCouponCityFilterDownEnabled();
        appConfig.setCouponCityFilterDownEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/survey", method = RequestMethod.POST, produces = "application/json")
    public String toggleSurvey() {
        boolean enabled = appConfig.isSurveyEnabled();
        appConfig.setSurveyEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/appgal", method = RequestMethod.POST, produces = "application/json")
    public String toggleAppgal() {
        boolean enabled = appConfig.isappGalEnabled();
        appConfig.setAppgalEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/rechargeretry", method = RequestMethod.POST, produces = "application/json")
    public String toggleRechargeRetryBlock(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRechargeRetryBlockEnabled();
        appConfig.setRechargeRetryBlockEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/zedoad.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleZedoAd(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isZedoAdEnabled();
        appConfig.setZedoAdEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

	@RequestMapping(value = "/toggle/asyncrecharge", method = RequestMethod.POST, produces = "application/json")
	public String toggleAsyncRecharge(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isAsyncRechargeEnabled();
                appConfig.setAsyncRechargeEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
	}
	
	@RequestMapping(value="/toggle/imsdirecthit", method = RequestMethod.POST , produces = "application/json")
	public String toggleImsDirectHit(Model m) {
		return toggle(m, new PropertyChange<Boolean>(){
			@Override
			public OldAndNew<Boolean> change(){
				boolean enabled = appConfig.isIMSDirectHitEnabled();
				appConfig.setImsDirectHitEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled,!enabled);
			}
		});
	}

	@RequestMapping(value="/toggle/emailbannerenabled", method = RequestMethod.POST , produces = "application/json")
	public String toggleEmailBannerEnabled(Model m) {
		return toggle(m, new PropertyChange<Boolean>(){
			@Override
			public OldAndNew<Boolean> change(){
				boolean enabled = appConfig.isEmailBannerEnabled();
				appConfig.setEmailBannerEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled,!enabled);
			}
		});
	}
	
	@RequestMapping(value="/toggle/euronethttpsenabled", method = RequestMethod.POST , produces = "application/json")
	public String toggleEuronetHttpsEnabled(Model m) {
		return toggle(m, new PropertyChange<Boolean>(){
			@Override
			public OldAndNew<Boolean> change(){
				boolean enabled = appConfig.isEuronetHttpsEnabled();
				appConfig.setEuronetHttpsEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled,!enabled);
			}
		});
	}
	
	@RequestMapping(value = "/toggle/mongosizerecord", method = RequestMethod.POST, produces = "application/json")
	public String toggleMongoSizeRecord(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isMongoSizeRecordEnabled();
		appConfig.setMongoSizeRecordEnabled(!enabled);
		return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
	
	@RequestMapping(value = "/toggle/asyncbillpayment", method = RequestMethod.POST, produces = "application/json")
	public String toggleAsyncBillPayment(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isAsyncBillPaymentEnabled();
		appConfig.setAsyncBillPaymentEnabled(!enabled);
		return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
	
	@RequestMapping(value = "/toggle/bbpsbillpayment", method = RequestMethod.POST, produces = "application/json")
	public String toggleBBPSBillPayment(Model m) {
	    return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
	    boolean enabled = appConfig.isBBPSBillPaymentEnabled();
		appConfig.setBBPSBillPaymentEnabled(!enabled);
		return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/undermaintenanceheaderbanner", method = RequestMethod.POST, produces = "application/json")
    public String toggleUnderMaintenanceHeaderBanner(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isUnderMaintenanceHeaderBannerEnabled();
        appConfig.setUnderMaintenanceHeaderBanner(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/paymentundermaintenance", method = RequestMethod.POST, produces = "application/json")
    public String togglePaymentUnderMaintenance(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPaymentUnderMaintenance();
        appConfig.setPaymentUnderMaintenance(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/cashbackcampaign", method = RequestMethod.POST, produces = "application/json")
    public String toggleCashBackCampaign(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCashBackCampaignEnabled();
        appConfig.setCashBackCampaign(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/ardealfeature", method = RequestMethod.POST, produces = "application/json")
    public String toggleARDealFeature(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isARDealEnabled();
        appConfig.setARDealEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/fbfraudcheck", method = RequestMethod.POST, produces = "application/json")
    public String toggleFbFraudCheck(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        Boolean enabled = appConfig.isFbFraudCheckEnabled();
        appConfig.setFbFraudCheckEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/smsGupshupHLREnable", method = RequestMethod.POST, produces = "application/json")
    public String toggleSMSGupshupHLREnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isSMSGupshupHLREnabled();
        appConfig.setSMSGupshupHLREnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    @RequestMapping(value = "toggle/invalidNumberBlockingEnable", method = RequestMethod.POST, produces = "application/json")
    public String toggleInvalidNumberBlockingEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isInvalidNumberBlockingEnabled();
        appConfig.setNumberBlockingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/tdPlanValidationEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleTDPlanValidationEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPlanValidationEnabled();
        appConfig.setTDPlanValidationEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/repeatRechargeBlockingEnable", method = RequestMethod.POST, produces = "application/json")
    public String toggleRepeatRechargeBlockingEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRepeatRechargeBlockingEnabled();
        appConfig.setRepeatRechargeBlock(!enabled);

        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/createBillV2Enable", method = RequestMethod.POST, produces = "application/json")
    public String toggleCreateBillV2Enable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isCreateBillV2Enabled();
        appConfig.setCreateBillV2Enabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    

    @RequestMapping(value = "toggle/postpaidFetchBillSupportEnable", method = RequestMethod.POST, produces = "application/json")
    public String togglePostpaidFetchBillSupportEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPostPaidFetchBillSupportEnabled();
        appConfig.setFetcBillSupportPostpaidEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

	@RequestMapping(value = "toggle/dbUpdateCallbackEnable", method = RequestMethod.POST, produces = "application/json")
	public String toggleDBUpdateOnCallBackEnable(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isDBUpdateOnCallBackEnabled();
				appConfig.setDBUpdateOnCallBackEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}
    
    @RequestMapping(value = "toggle/klickpaydowntimeAPI", method = RequestMethod.POST, produces = "application/json")
    public String toggleKlickpayDowntimeAPIStatus(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.useKlickpayForDowntimeAPI();
                appConfig.setKlickpayForDowntimeAPI(!enabled);

                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    
    @RequestMapping(value = "toggle/klickpayNBdowntimeAPI", method = RequestMethod.POST, produces = "application/json")
    public String toggleKlickpayNBDowntimeAPIStatus(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.useKlickpayForNBDowntimeAPI();
                appConfig.setKlickpayForNBDowntimeAPI(!enabled);

                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    

	@RequestMapping(value = "toggle/intransactionValidationEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleIntransactionValidationEnable(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isInTransactionValidationEnabled();
				appConfig.setInTransactionValidationEnabled(!enabled);
				
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}


	@RequestMapping(value = "toggle/timeBasedRefundEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleTimeBasedRefundEnable(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isTimeBasedRefundEnabled();
				appConfig.setTimeBasedRefundEnabled(!enabled);

				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}
	
	@RequestMapping(value = "toggle/fulfillBillNewEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleFulfillBillNewEnable(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isFulfillNewEnabled();
				appConfig.setFulfillBillNewEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}
	
	@RequestMapping(value = "toggle/fetchBillNewEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleFetchBillNewEnable(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isFetchBillNewEnabled();
				appConfig.setFetchBillNewEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}
    
    @RequestMapping(value = "/get/userids", method = RequestMethod.GET)
    public String getUserIds(final Model model, HttpServletRequest request) {
        String userIds = appConfig.getUserIds();
        model.addAttribute("userIds", userIds);
        return "admin/appconfig/userids";
    }

    @RequestMapping(value = "/get/blackListedEmailIds", method = RequestMethod.GET)
    public String getBlackListedEmailIds(final Model model, HttpServletRequest request) {
        String emailIds = appConfig.getBlackListedEmailIds();
        model.addAttribute("emailIds", emailIds);
        return "admin/appconfig/blackListedEmailIds";
    }

    @RequestMapping(value = "/save/userids", method = RequestMethod.POST, produces = "application/json")
    public String saveUserIds(final Model model, final HttpServletRequest request) {
        appConfig.setUserIds(request.getParameter("userids"));
        return "redirect:/admin/appconfig/get/userids.htm";
    }

    @RequestMapping(value = "/get/imei-group", method = RequestMethod.GET)
    public String getImeiGroup(final Model model, HttpServletRequest request){
        model.addAttribute("imeiGroup", this.appConfig.getClosedImeiGroup());
        return "admin/appconfig/imeiGroup";
    }

    @RequestMapping(value = "/save/imei-group", method = RequestMethod.POST)
    public String saveImeiGroup(final Model model, final HttpServletRequest request){
        appConfig.setClosedImeiGroupKey(request.getParameter("imeiGroup"));
        return "redirect:/admin/appconfig/get/imei-group";
    }

    @RequestMapping(value = "/get/city-name", method = RequestMethod.GET)
    public String getCityName(final Model model, HttpServletRequest request){
        model.addAttribute("cityName", this.appConfig.getCityName());
        return "admin/appconfig/cityName";
    }

    @RequestMapping(value = "/save/city-name", method = RequestMethod.POST)
    public String saveCityName(final Model model, final HttpServletRequest request){
        appConfig.setCityName(request.getParameter("cityName"));
        return "redirect:/admin/appconfig/get/city-name";
    }
    
    @RequestMapping(value = "/save/blacklistedEmailIds", method = RequestMethod.POST, produces = "application/json")
    public String saveBlacklistedEmailIds(final Model model, final HttpServletRequest request) {
        appConfig.setBlackListedEmailIds(request.getParameter("emailIds"));
        return "redirect:/admin/appconfig/get/blackListedEmailIds.htm";
    }

    @RequestMapping(value = "/get/stackId", method = RequestMethod.GET)
    public String getStackId(final Model model, final HttpServletRequest request) {
        String stackId = appConfig.getStackID();
        model.addAttribute("stackId", stackId);
        return "admin/appconfig/stackId";
    }

    @RequestMapping(value = "/save/stackId", method = RequestMethod.POST, produces = "application/json")
    public String saveStackId(final Model model, final HttpServletRequest request) {
        appConfig.setStackID(request.getParameter("stackId"));
        return "redirect:/admin/appconfig/get/stackId.htm";
    }

    @RequestMapping(value = "/get/newRefundPercentage", method = RequestMethod.GET)
    public String getNewRefundPercentage(final Model model, final HttpServletRequest request) {
        Integer newRefundPercentage;
        try {
            newRefundPercentage = appConfig.getNewRefundPercentage();
        } catch (Exception e) {
            newRefundPercentage = null;
        }
        model.addAttribute("newRefundPercentage", newRefundPercentage);
        return "admin/appconfig/newRefundPercentage";
    }

    @RequestMapping(value = "/save/newRefundPercentage", method = RequestMethod.POST, produces = "application/json")
    public String saveNewRefundPercentage(final Model model, final HttpServletRequest request) {
        appConfig.setNewRefundPercentage(Integer.parseInt(request.getParameter("newRefundPercentage")));
        return "redirect:/admin/appconfig/get/newRefundPercentage.htm";
    }

    @RequestMapping(value = "/toggle/websiteanalytics", method = RequestMethod.POST, produces = "application/json")
    public String toggleWebAnalyticsFeature(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isWebSiteAnalyticsEnabled();
        appConfig.setWebSiteAnalyticsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
    }
    
    @RequestMapping(value = "/toggle/mobilesiteanalytics", method = RequestMethod.POST, produces = "application/json")
    public String toggleMobileAnalyticsFeature(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isMobileSiteAnalyticsEnabled();
        appConfig.setMobileSiteAnalyticsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
    }
    
    @RequestMapping(value = "/toggle/forcepaidcoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleForcePaidCoupons(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isForcePaidCouponsEnabled();
        appConfig.setForcePaidCouponsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "/toggle/isrupayenabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleIsRupayEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRupayEnabled();
        appConfig.setRupayEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "/toggle/forcerandomcouponorder", method = RequestMethod.POST, produces = "application/json")
    public String toggleForceRandomCouponsOrder(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isForceRandomCouponsOrderEnabled();
        appConfig.setForceRandomCouponsOrderEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
    }
    
    @RequestMapping(value = "/toggle/finalpagecoupons", method = RequestMethod.POST, produces = "application/json")
    public String toggleFinalPageCoupons(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isFinalPageCouponsEnabled();
        appConfig.setFinalPageCouponsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/trackingSqs", method = RequestMethod.POST, produces = "application/json")
    public String toggleTrackingSqs(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isTrackingSqsEnabled();
        appConfig.setTrackingSqsEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
    }

    @RequestMapping(value = "/toggle/trackerService", method = RequestMethod.POST, produces = "application/json")
    public String toggleTrackerService(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isTrackerServerEnabled();
        appConfig.setTrackerServerEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "/toggle/useragentenqueuing", method = RequestMethod.POST, produces = "application/json")
    public String toggleUserAgent(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean flag = appConfig.isUserAgentEnqueuingEnabled();
        appConfig.setUserAgentEnqueuing(!flag);
        return new OldAndNew<Boolean>(flag, !flag);
            }
       });
   }

    @RequestMapping(value = "/toggle/recaptcha", method = RequestMethod.POST, produces = "application/json")
    public String toggleRecaptcha(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isRecaptchaEnabled();
        appConfig.setRecaptchaEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "/toggle/promocode", method = RequestMethod.POST, produces = "application/json")
    public String togglePromocode(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPromocodeEnabled();
        appConfig.setPromocodeEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "/toggle/promoreward", method = RequestMethod.POST, produces = "application/json")
    public String togglePromoreward(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPromoRewardEnabled();
        appConfig.setPromoRewardEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/pepsicampaign.htm", method = RequestMethod.POST, produces = "application/json")
    public String togglePepsiCampaign(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isPepsiCampaignEnabled();
        appConfig.setPepsiCampaignEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/didcampaign.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleDIDCampaign(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isDIDCampaignEnabled();
        appConfig.setDIDCampaignEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/mcdcampaign.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleMCDCampaign(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isMCDCampaignEnabled();
        appConfig.setMCDCampaignEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/festivegift.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleFestiveGift(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isFestiveGiftEnabled();
        appConfig.setFestiveGiftEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/ipcheck.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleIPCheck(){
        boolean enabled = this.appConfig.isIpCheckEnabled();
        this.appConfig.setIpCheckEnabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "/toggle/sokratipixel", method = RequestMethod.POST, produces = "application/json")
    public String toggleSokartiPixel(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isSokratiPixelEnabled();
        appConfig.setSokartiPixelEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }

    @RequestMapping(value = "/toggle/fingerprinting", method = RequestMethod.POST, produces = "application/json")
    public String toggleFingerPrinting(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isFingerPrintingEnabled();
        appConfig.setFingerPrintingEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }
    
    @RequestMapping(value = "/toggle/facebooklogin", method = RequestMethod.POST, produces = "application/json")
    public String toggleFacebookLogin(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isFacebookLoginEnabled();
        appConfig.setFacebookLoginEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }

    @RequestMapping(value = "/toggle/googlelogin", method = RequestMethod.POST, produces = "application/json")
    public String toggleGoogleLogin(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isGoogleLoginEnabled();
        appConfig.setGoogleLoginEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }

    @RequestMapping(value = "toggle/couponCategoryToggle", method = RequestMethod.POST, produces = "application/json")
    public String toggleEnableCouponConfigurableCategory(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isConfigurableCouponCategoriesEnabled();
        appConfig.setConfigurableCouponCategoriesEnabled(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }
    
    @RequestMapping(value = "toggle/readDenoValFromDynamoToggle", method = RequestMethod.POST, produces = "application/json")
    public String toggleReadDenoValFromDynamoToggle(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
        boolean enabled = appConfig.isReadDenominationValidityFromDynamo();
        appConfig.setReadDenominationValidityFromDynamo(!enabled);
        return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
  }

    @RequestMapping(value = "toggle/emailapp.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleEnableEmailApp(Model m) {
        toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isEmailAppEnabled();
                appConfig.setEmailAppEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
        return "jsonView";
    }
     
    
    @RequestMapping(value = "toggle/emailappForCoupon.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleEnableEmailAppForCoupon() {
        boolean enabled = appConfig.isEmailAppEnabledForCoupon();
        appConfig.setEmailAppEnabledForCoupon(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/enablerechargeapi.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleEnableRechargeApi() {
        boolean enabled = appConfig.isRechargeApiEnabled();
        appConfig.setRechargeApiEnabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/smstemplates.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleSMSTemplates() {
        boolean enabled = appConfig.isSMSTemplatesEnabled();
        appConfig.setSMSTemplatesEnabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/enablerechargecallback.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleEnableRechargeCallback() {
        boolean enabled = appConfig.isRechargeCallbackEnabled();
        appConfig.setRechargeCallbackEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/airtelEuroNewWallet.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleAirtelEuroNewWallet() {
        boolean enabled = appConfig.isAirtelEuroNewWalletEnabled();
        appConfig.setAirtelEuroNewWalletEnabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/agpreference.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleAgPreference() {
        boolean enabled = appConfig.isAgPreferenceEnabled();
        appConfig.setAgPreferenceEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/euronetvaswsdl.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleAggregatorEuronetVas2Wsdl() {
        boolean enabled = appConfig.isEuronetVas2Enabled();
        appConfig.setEuronetVas2Enabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/webAssetsVersion.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleWebAssetsVersion() {
        appConfig.setWebAssetsVersion();
        return "jsonView";
    }

    @RequestMapping(value = "fraud/fbconditions.htm")
    public String changeFbFraudConditions(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (request.getMethod().equals("GET")){
            JSONObject conditions = this.appConfig.getFbFraudConditions();
            model.addAttribute("conditions", conditions!=null?conditions:new HashMap<>());
            return "appconfig/fbconditions";
        } else {
            String conditions = request.getParameter("conditions");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setFbFraudConditions(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            } catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }
    
    @RequestMapping(value = "toggle/centaurRandomCoupon.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleRandomCoupon() {
        boolean enabled = appConfig.isRandomCouponEnabled();
        appConfig.setRandomCouponEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/centaurEnabledABTesting.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleCentaurEnabledForABTesting() {
        boolean enabled = appConfig.isCentaurEnabledForABTesting();
        appConfig.setCentaurEnabled(!enabled);
        return "jsonView";
    }

    @RequestMapping(value = "toggle/newFailureFulfilmentEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleNewFailureFulfilmentEnabled() {
        boolean enabled = appConfig.isNewFailureFulfilmentEnabled();
        appConfig.setNewFailureFulfilmentEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/isOneCheckFKEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleOneCheckFKEnabled() {
        boolean enabled = appConfig.isOneCheckFKEnabled();
        appConfig.setOneCheckFKEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/loadAntiFraudPropsFromFile.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleLoadAntiFraudPropsFromFile() {
        boolean enabled = appConfig.isLoadAntiFraudPropsFromFileEnabled();
        appConfig.setLoadAntiFraudPropsFromFileEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/cardstatus.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleCardStatusFlag() {
        boolean enabled = appConfig.isCardStatusEnabled();
        appConfig.setCardStatusEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/debitatm.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleDebitATMFlag() {
        boolean enabled = appConfig.isDebitATMEnabled();
        appConfig.setDebitATMEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/iciciqc.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleICICIQCFlag() {
        boolean enabled = appConfig.isICICIQCEnabled();
        appConfig.setICICIQCEnabled(!enabled);
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/vCardCreateEnabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleVCardCreateEnabled() {
    	boolean enabled = appConfig.isVCardCreateEnabled();
    	appConfig.setVCardCreateEnabled(!enabled);
    	return "jsonView";
    }
    
    @RequestMapping(value = "ipcheckconditions")
    public String changeIpCheckConditions(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (request.getMethod().equals("GET")){
            JSONObject conditions = this.appConfig.getIpCheckConditions();
            model.addAttribute("conditions", conditions!=null?conditions:new HashMap<>());
            return "appconfig/ipconditions";
        } else {
            String conditions = request.getParameter("conditions");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setIpCheckConditions(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            } catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }
    
  
    @RequestMapping(value = "update/splitTrafficWeight", method = RequestMethod.POST, produces = "application/json")
    public String splitTrafficWeight(final HttpServletRequest request, final HttpServletResponse response, final Model model) {
        final String oldValue = AppConfigController.this.appConfig.getSplitTrafficWeight();
        final String weightSequence = request.getParameter("splitTrafficWeight");
        try {
            AppConfigController.this.appConfig.setSplitTrafficWeight(weightSequence);
            model.addAttribute("status", "success");
        } catch (RuntimeException e) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            model.addAttribute("status", "error");
            return "jsonView";
        }
        return toggle(model, new PropertyChange<String>() {
            @Override
            public OldAndNew<String> change() {
                return new OldAndNew<String>(oldValue, weightSequence);
            }
        });
    }

    @RequestMapping(value = "atmCoupons")
    public String atmCoupons(HttpServletRequest request, HttpServletResponse response, Model model){
        if (request.getMethod().equals("GET")){
            JSONObject coupons = this.appConfig.getATMCoupons();
            model.addAttribute("values", coupons!=null?coupons:new HashMap<>());
            return "appconfig/atmCoupons";
        }else {
            String conditions = request.getParameter("values");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setATMCoupons(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            }catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }
    
    @RequestMapping(value = "couponCarousel")
    public String couponCarousel(HttpServletRequest request, HttpServletResponse response, Model model){
        if (request.getMethod().equals("GET")){
            JSONObject coupons = this.appConfig.getCouponCarousel();
            model.addAttribute("values", coupons!=null?coupons.toJSONString():new HashMap<>());
            return "appconfig/couponCarousel";
        }else {
            String carousel = request.getParameter("values");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(carousel);
                this.appConfig.setCouponCarousel(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            }catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }
    
    @RequestMapping(value = "couponFraudValues")
    public String couponFraudConditions(HttpServletRequest request, HttpServletResponse response, Model model){
        if (request.getMethod().equals("GET")){
            JSONObject conditions = this.appConfig.getCouponFraudValues();
            model.addAttribute("values", conditions!=null?conditions:new HashMap<>());
            return "appconfig/couponFraudValues";
        }else {
            String conditions = request.getParameter("values");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setCouponFraudValues(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            }catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }
    
    @RequestMapping(value = "metroReportEmailIds")
    public String metroReportEmailIds(HttpServletRequest request, HttpServletResponse response, Model model){
        if (request.getMethod().equals("GET")){
            JSONObject emails = this.appConfig.getMetroReportEmailIds();
            model.addAttribute("values", emails!=null?emails:new HashMap<>());
            return "appconfig/metroReportEmailIds";
        }else {
            String conditions = request.getParameter("values");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setMetroReportEmailIds(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            }catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }
    }

    @RequestMapping(value = "custom-props")
    public String listCustomProperties(HttpServletRequest request, HttpServletResponse response, Model model){
        JSONParser parser = new JSONParser();
        if (request.getMethod().equals("GET")) {
            List<Map<String, Object>> propList = this.appConfig.getCustomProps();
            Map<String, JSONObject> propMap = new HashMap<>();

            for (Map<String, Object> prop : propList){
                String propValue = (String) prop.get("app_value");
                String propKey = (String) prop.get("app_key");
                try {
                    propMap.put(propKey, (JSONObject) parser.parse(propValue));
                }catch (ParseException pe){
                    // Not adding improper values
                }
            }
            model.addAttribute("props", propMap);
            return "admin/custom-props";
        }else {
            //Ajax prop submit
            String key = request.getParameter("key");
            String value = request.getParameter("value");
            try {
                this.appConfig.setCustomProp(key, (JSONObject) parser.parse(value));
            }catch (ParseException pe){
                // Invalid json string value
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                model.addAttribute("message", "Invalid json string for key: "+key);
                return "jsonView";
            }
            model.addAttribute("status", "success");
            model.addAttribute("message", "Custom prop for key: "+key +" updated successfully");
            return "jsonView";
        }
    }

    //To display these values in the FE admin page
    public static class View {
        private String display;
        private String value;
        private String toggleUrl;

        public View(String display, String value, String toggleUrl) {
            this.display = display;
            this.value = value;
            this.toggleUrl = toggleUrl;
        }

        public String getDisplay() {
            return display;
        }

        public void setDisplay(String display) {
            this.display = display;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getToggleUrl() {
            return toggleUrl;
        }

        public void setToggleUrl(String toggleUrl) {
            this.toggleUrl = toggleUrl;
        }
    }
    
    @RequestMapping(value = "updateInMemoryCache", method = RequestMethod.GET)
    public String updateInMemoryCache() {
    	inMemoryAppConfigService.reloadCacheFromRedis();
        return "jsonView";
    }
    
    @RequestMapping(value = "toggle/userMessageStore.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleUserMessageStore(Model m){
        return toggle(m, new PropertyChange<Boolean>() {
            @Override
            public OldAndNew<Boolean> change() {
                boolean enabled = appConfig.isUserMessageStoreEnabled();
                appConfig.setUserMessageStoreEnabled(!enabled);
                return new OldAndNew<Boolean>(enabled, !enabled);
            }
        });
    }
    @RequestMapping(value = "forceUpgradeDetails")
    public String forceUpgradeDetails(HttpServletRequest request, HttpServletResponse response, Model model){
        if (request.getMethod().equals("GET")){
            JSONObject forceUpgradeDetails = this.appConfig.getForceUpgradeDetails();
            model.addAttribute("values", forceUpgradeDetails!=null?forceUpgradeDetails:new HashMap<>());
            return "appconfig/forceUpgradeDetails";
        }else {
            String conditions = request.getParameter("values");
            JSONParser parser = new JSONParser();
            try {
                JSONObject obj = (JSONObject) parser.parse(conditions);
                this.appConfig.setForceUpgradeDetails(obj);
                model.addAttribute("status", "success");
                return "jsonView";
            }catch (ParseException pe){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                model.addAttribute("status", "error");
                return "jsonView";
            }
        }

    }
    
    @RequestMapping(value = "toggle/kps2scommenabled.htm", method = RequestMethod.POST, produces = "application/json")
    public String toggleKlickPayS2SEnabled(Model m) {
    	return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isKlickPayS2SEnabled();
        	 appConfig.setKlickPayS2SEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
         }
         });
    }
    
    @RequestMapping(value = "cache/debitatmproduct.htm", method = RequestMethod.POST, produces = "application/json")
    public String burstDebitATMProductCache(Model m) {
        String keyList = paymentsCache.get(PaymentConstants.DEBIT_ATM_PRODUCTS_CACHE_KEY);
        if(!StringUtils.isEmpty(keyList)){
            paymentsCache.delete(Arrays.asList(keyList.split(",")));
        }
        paymentsCache.delete(PaymentConstants.DEBIT_ATM_PRODUCTS_CACHE_KEY);
        return "";
    }
    
    
    @RequestMapping(value = "toggle/dthNumberValidationEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleDTHNumberValidationEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isDTHNumberValidationEnabled();
        	 appConfig.setDTHNumberValidationEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/invalidPlansDenominationValidationEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleInvalidPlansDenominationValidationEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isInvalidPlansDenominationValidationEnabled();
        	 appConfig.setInvalidPlansDenominationValidationEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }

    @RequestMapping(value = "toggle/dataCardCircleBlockingEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleDataCardCircleBlockingEnabled(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isDataCardCircleBlockingEnabled();
        	 appConfig.setDataCardCircleBlockingEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/autoPayEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleAutoPayEnable(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isAutoPayEnabled();
        	 appConfig.setAutoPayEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/autoPayEnabledForUtility", method = RequestMethod.POST, produces = "application/json")
    public String toggleAutoPayEnabledForUtility(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isAutoPayEnabledForUtility();
        	 appConfig.setAutoPayEnabledForUtility(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/planLevelRetryEnabled", method = RequestMethod.POST, produces = "application/json")
    public String togglePlanLevelRetry(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isPlanLevelRetryEnabled();
        	 appConfig.setPlanLevelRetryEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/euronetNewErrorCodeEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleEuronetNewErrorCodes(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isEuronetNewErrorCodeEnabled();
        	 appConfig.setEuronetNewErrorCodeEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
            }
       });
   }
    
    @RequestMapping(value = "toggle/billpayOperatorChannelBlockingEnabled", method = RequestMethod.POST, produces = "application/json")
    public String toggleBillpayOperatorChannelBlocking(Model m) {
        return toggle(m, new PropertyChange<Boolean>() {
         @Override
         public OldAndNew<Boolean> change() {
        	 boolean enabled = appConfig.isBillpayOperatorChannelBlockingEnabled();
        	 appConfig.setBillpayOperatorChannelBlockingEnabled(!enabled);
        	 return new OldAndNew<Boolean>(enabled, !enabled);
         	}
         });
    }
   
	@RequestMapping(value = "toggle/dthPlanValidationEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleDTHPlanValidation(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isDTHPlanValidationEnabled();
				appConfig.setDTHPlanValidationEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}
	
	@RequestMapping(value = "toggle/jioNumberValidationEnabled", method = RequestMethod.POST, produces = "application/json")
	public String toggleJIONumberValidation(Model m) {
		return toggle(m, new PropertyChange<Boolean>() {
			@Override
			public OldAndNew<Boolean> change() {
				boolean enabled = appConfig.isJIONumberValidationEnabled();
				appConfig.setJIONumberValidationEnabled(!enabled);
				return new OldAndNew<Boolean>(enabled, !enabled);
			}
		});
	}

}
