package com.freecharge.admin.controller;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.web.util.ViewConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin/freefund/generateandupload/*")
public class FreefundGenerateAndUploadController extends BaseAdminController{

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private EmailService emailService;

    public static final String FREEFUND_GENERATE_AND_UPLOAD_LINK = "/admin/freefund/generateandupload/home.htm";

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND_GENERATE_UPLOAD)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "home");
        return ViewConstants.FREEFUND_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "home/generatecodes", method = RequestMethod.GET)
    public String generateCodes(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND_GENERATE_UPLOAD)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);
        model.addAttribute("page", "generatecodes");
        return ViewConstants.FREEFUND_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "home/uploadcodes", method = RequestMethod.GET)
    public String uploadCodes(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                              HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND_GENERATE_UPLOAD)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);
        model.addAttribute("page", "uploadcodes");
        return ViewConstants.FREEFUND_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "generate")
    public void generateFreefundCode(@RequestParam Map<String, String> mapping, Model model,
                                     HttpServletRequest request, HttpServletResponse response) {
        String prefixParamvalue = mapping.get(FCConstants.PREFIX_PARAM_NAME).trim();
        String nuberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();
        try {
            response.reset();
            response.setContentType("text/csv;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=freefundcode.csv");

            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            randomCodeGenerator.setPrefix(prefixParamvalue);
            randomCodeGenerator.setNoOfCodes(Integer.parseInt(nuberOfCodesParamValue));
            randomCodeGenerator.setLength(Integer.parseInt(lengthParamValue));
            randomCodeGenerator.setType(RandomCodeGenerator.TYPE.ALPHANUMERIC);
            randomCodeGenerator.setCustomChar(true);
            Writer writer = response.getWriter();
            randomCodeGenerator.download(writer);
            response.flushBuffer();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.error("IOException:" + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception:" + e.getMessage());
        }

    }

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public String upload(@RequestParam("freefund_codes") MultipartFile file,
                         @RequestParam(required = true, value = "class_id") Long classId, Model model, HttpServletRequest request) {
        String messages = "";
        String fileName = file.getOriginalFilename();
        boolean shouldSendEmailNotification = fcProperties.getBooleanProperty(FCConstants.FREEFUND_COUPON_UPLOAD_NOTIFICATION_ENABLED);
        FreefundClass freefundClass = freefundService.getFreefundClass(classId);
        if (freefundClass == null || freefundClass.getPromoEntity() == null
                || freefundClass.getMinRechargeValue() == null || freefundClass.getFreefundValue() == null) {
            messages = "The freefund class chosen is in invalid state. Please update promo entity, min recharge value, freefund value";
            if(shouldSendEmailNotification) {
            	String currentUserEmailId = getCurrentAdminUser(request);
            	emailService.sendFreeFundCodeUploadNotificationEmail(messages, fileName, currentUserEmailId, null, null);
            }
            return String.format("redirect:%s?message=%s", FREEFUND_GENERATE_AND_UPLOAD_LINK, messages);
        }

        int successfulUploadsCount = 0;
        List<String> inFileDuplicates = new ArrayList<>();
        List<String> duplicateCodes = null;
        Map<String, FreefundCoupon> freefundCouponMap = new HashMap<>();

        try {
            Calendar cal = Calendar.getInstance();
            BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line = "";
            int duplicateCodesCounter = 0;
            while (!FCUtil.isEmpty(line = br.readLine())) {
                if (freefundCouponMap.get(line) == null) {
                    FreefundCoupon freefundCoupon = new FreefundCoupon();
                    freefundCoupon.setFreefundCode(line);
                    freefundCoupon.setFreefundClassId(classId);
                    freefundCoupon.setGeneratedDate(cal.getTime());
                    freefundCouponMap.put(line, freefundCoupon);
                } else {
                    inFileDuplicates.add(line);
                    duplicateCodesCounter++;
                }
            }
            duplicateCodes = freefundService.getFreefundCodes(new ArrayList(freefundCouponMap.keySet()));
            if(!FCUtil.isEmpty(duplicateCodes)) {
                logger.info("Duplicate freefund coupons found " + duplicateCodes.size() + ". For codes "
                        + freefundCouponMap.keySet().size());
                freefundCouponMap.keySet().removeAll(duplicateCodes);
                duplicateCodesCounter += duplicateCodes.size();
            }
            logger.info("Freefund coupon map post duplicate removal " + freefundCouponMap.keySet().size());
            successfulUploadsCount = freefundService.uploadCodes(new ArrayList<FreefundCoupon>(freefundCouponMap.values()));

            messages += (successfulUploadsCount) + " Freefund Codes uploaded successfully. ";

            if (duplicateCodesCounter > 0) {
                messages += duplicateCodesCounter + " duplicate codes found";
            }
        } catch (Exception e) {
            logger.error("Exception uploading codes ", e);
            messages += "Something went wrong. Try again later " + "" + e.getMessage();
        }
        if(shouldSendEmailNotification) {
        	String currentUserEmailId = getCurrentAdminUser(request);
        	emailService.sendFreeFundCodeUploadNotificationEmail(messages, fileName, currentUserEmailId, inFileDuplicates, duplicateCodes);
        }
        return String.format("redirect:%s?message=%s", FREEFUND_GENERATE_AND_UPLOAD_LINK, messages);
    }

    @RequestMapping(value = "generate/validate", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> generateFreefundValidate(@RequestParam Map<String, String> mapping, Model model,
                                                 HttpServletRequest request, HttpServletResponse response) {
        String nuberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();

        Integer givenCodeLength = 0;
        try {
            givenCodeLength = Integer.parseInt(lengthParamValue);
        } catch (Exception e) {
            // TODO: handle exception
        }
        Integer numberOfCodes = 0;
        try {
            numberOfCodes = Integer.parseInt(nuberOfCodesParamValue);
        } catch (Exception e) {
            // TODO: handle exception
        }

        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        Double desiredCodeLength = randomCodeGenerator.desiredCodeLengthFor(numberOfCodes);

        Map<String, String> responseMap = new HashMap<String, String>();
        if (givenCodeLength >= desiredCodeLength) {
            responseMap.put("status", "success");
        } else {
            responseMap.put("status", "fail");
        }
        String message = "The advisable code length should be " + desiredCodeLength.toString()
                + " to avoid generating duplicate codes [ For a duplication reduction factor of "
                + Math.pow(10, -RandomCodeGenerator.DUPLICATION_REDUCTION_FACTOR) + " ]";

        responseMap.put("message", message);
        return responseMap;
    }
}
