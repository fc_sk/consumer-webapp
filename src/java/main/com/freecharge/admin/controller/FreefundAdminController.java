package com.freecharge.admin.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.util.RandomCodeGenerator.TYPE;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.AdditionalRewardService;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.freefund.services.FreefundUsageHistoryServiceRequest;
import com.freecharge.freefund.util.FreefundUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/freefund/*")
public class FreefundAdminController extends BaseAdminController {

    private Logger                      logger                              = LoggingFactory.getLogger(getClass().getName());

    private SimpleDateFormat            sdf3                                  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final String          HOME_LINK                             = "/admin/freefund/home.htm";
    public static final String          EDIT_LINK                             = "/admin/freefund/viewAndedit.htm";

    @Autowired
    private FreefundService             freefundService;

    @Autowired
    private EmailService                emailService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private AdditionalRewardService additionalRewardService;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);
        model.addAttribute("page", "home");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }
    
    @RequestMapping(value = "home/createclass", method = RequestMethod.GET)
    public String createClass(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        model.addAttribute("freefundClasses", freefundClasses);


        List<AdditionalReward> additionalRewards = additionalRewardService.getAllActiveAdditionalRewards();
        model.addAttribute("additionalRewardList", additionalRewards);
        model.addAttribute("page", "createclass");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }
    
    @RequestMapping(value = "home/viewclasses", method = RequestMethod.GET)
    public String viewClasses(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND)) {
            return get403HtmlResponse(response);
        }
        List<FreefundClass> freefundClasses = freefundService.getFreefundClasses();
        checkAndSetStatus(freefundClasses);
        model.addAttribute("freefundClasses", freefundClasses);
        
        model.addAttribute("page", "viewclasses");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }
    
    private void checkAndSetStatus(List<FreefundClass> freefundClasses) {
        Calendar today = Calendar.getInstance();
        for(FreefundClass freefundClass : freefundClasses) {
            if(freefundClass.getValidUpto().before(today)) {
                freefundClass.setIsActive(0);
            }
        }
        
    }

    @RequestMapping(value = "home/editclass", method = RequestMethod.GET)
    public String editClass(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND)) {
            return get403HtmlResponse(response);
        }

        long freefundClassId = -1;
        try {
            freefundClassId = Integer.parseInt(mapping.get("class_id"));
            
            if (freefundClassId != -1) {
                FreefundClass freefundClass = freefundService.getFreefundClass(freefundClassId);
                freefundClass.getDatamapComponents();
                model.addAttribute("freefundClass", freefundClass);

                List<AdditionalReward> additionalRewards = additionalRewardService.getAllActiveAdditionalRewards();
                model.addAttribute("additionalRewardList", additionalRewards);
            }
        } catch (Exception e) {
            logger.error("FreefundClassId : " + freefundClassId + " Edit process failed", e);
        }

        model.addAttribute("page", "editclass");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "bulkreset", method = RequestMethod.GET)
    public String promocodeBulkReset(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        model.addAttribute("page", "bulkreset");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "bulkreset", method = RequestMethod.POST)
    public String doPromocodeBulkReset(@RequestParam("promocodes") MultipartFile file,
            @RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        String requestId = UUID.randomUUID().toString();
        List<String> invalidCodes = new ArrayList<String>();
        List<String> unblockedCodes = new ArrayList<String>();
        List<String> nonblockedCodes = new ArrayList<String>();

        List<String> promocodesList = new ArrayList<String>();
        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null) {
                promocodesList.add(line);
            }
        } catch (Exception e) {
            logger.error("Unable to parse csv file ", e);
            return "redirect:/admin/freefund/bulkreset.htm?message=Unable to parse CSV file";
        }

        for (String promocode : promocodesList) {
            promocode = promocode.trim();
            if (StringUtils.isNotEmpty(promocode)) {
                FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(promocode);
                if (freefundCoupon != null) {
                    boolean status = false;

                    try {
                        status = freefundService.resetCouponFromCSTool(freefundCoupon);
                    } catch (Exception e) {
                        logger.error("Unable to reset promocode " + promocode, e);
                    }

                    if (status) {
                        unblockedCodes.add(promocode);
                    } else {
                        nonblockedCodes.add(promocode);
                    }
                } else {
                    invalidCodes.add(promocode);
                }
            }
        }
        model.addAttribute("invalidCodes", invalidCodes);
        model.addAttribute("unblockedCodes", unblockedCodes);
        model.addAttribute("nonblockedCodes", nonblockedCodes);
        model.addAttribute("page", "bulkreset");
        return ViewConstants.FREEFUND_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "viewAndedit", method = RequestMethod.GET)
    public String viewAllclasses(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        List<FreefundClass> freefund = freefundService.getFreefundClasses();
        model.addAttribute("freefundclasview", freefund);
        long freefundClassId = -1;
        try {
            freefundClassId = Integer.parseInt(mapping.get("class_id"));
        } catch (Exception e) {
        }
        if (freefundClassId != -1) {
            FreefundClass freefundClass = freefundService.getFreefundClass(freefundClassId);
            model.addAttribute("freefundClass", freefundClass);

        }
        return ViewConstants.FREEFUND_CLASS_EDIT;

    }

    @RequestMapping(value = "class/save", method = RequestMethod.POST)
    public String classSave(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        String messages = "";
        long freefundClassId = -1;

        String curUserEmail = "";
        try {
            freefundClassId = Long.parseLong(mapping.get("class_id"));
        } catch (Exception e) {
            messages = "Invalid class id passed.";
            return String.format("redirect:%s?message=%s", EDIT_LINK, messages);
        }

        FreefundClass freefundClass = freefundService.getFreefundClass(freefundClassId);
        if (freefundClass == null) {
            messages = "Invalid class id passed.";
            return String.format("redirect:%s?message=%s", EDIT_LINK, messages);
        }
        try {
            FreefundClass existingFreefundClass = new FreefundClass(freefundClass);
            RequestStatus requestStatus = validateAndSetRequestParams(freefundClass, request);
            if (requestStatus.isValid()) {
                freefundService.saveFreefundClass(freefundClass);
                curUserEmail = getCurrentAdminUser(request);
                sendCampaignCreateOrUpdateMail(freefundClass, existingFreefundClass, curUserEmail);
                messages = "Freefund Class Saved Successfully";
            } else {
                messages = requestStatus.getMessage();
            }

        } catch (Exception pe) {
            logger.error("Exception:" + pe.getMessage(), pe);
            messages = "Exception occured. Freefund class not saved.";
        }
        return String.format("redirect:%s?class_id=%s&message=%s", "/admin/freefund/home/editclass.htm", freefundClassId, messages);
    }

    @RequestMapping(value = "class/upload", method = RequestMethod.POST)
    public String classUpload(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request) {
        String messages = "";
        String curUserEmail = "";
        try {
            FreefundClass freefundClass = null;
            String name = request.getParameter("name");
            freefundClass = freefundService.getFreefundClassForName(name);
            if (freefundClass == null) {
                freefundClass = new FreefundClass();
            } else {
                messages = "Freefund class not created. Freefund class by that name already exists.";
                return String.format("redirect:%s?message=%s", HOME_LINK, messages);
            }
            RequestStatus requestStatus = validateAndSetRequestParams(freefundClass, request);
            if (requestStatus.isValid()) {
                freefundClass.setUpdatedAt(Calendar.getInstance());
                freefundService.createFreefundClass(freefundClass);
                curUserEmail = getCurrentAdminUser(request);
                sendCampaignCreateOrUpdateMail(freefundClass, null, curUserEmail);
                messages = "Freefund class created successfully.";
            } else {
                messages = requestStatus.getMessage();
            }
        } catch (Exception e) {
            logger.error("Exception " + messages, e);
            messages = "Exception occurred. Freefund class not created";

        }

        return String.format("redirect:%s?message=%s", "/admin/freefund/home/viewclasses.htm", messages);
    }

    private RequestStatus validateAndSetRequestParams(FreefundClass freefundClass, HttpServletRequest request) {
        RequestStatus requestStatus = new RequestStatus();


        freefundClass.setName(FCUtil.trim(request.getParameter("name")));
        freefundClass.setDescription(FCUtil.trim(request.getParameter("description")));
        freefundClass.setPromoEntity(FCUtil.trim(request.getParameter("promo_entity")));
        freefundClass.setDiscountType(request.getParameter("discount_type"));

        String campaignSponsorship = FCUtil.trim(request.getParameter("campaign_sponsorship"));   
        try{
            freefundClass.setCampaignSponsorship(FreefundUtil.CampaignSponsorshipEnum.valueOf(campaignSponsorship).name()); 
        } catch (Exception e){
            logger.error("CampaignSponsorshipEnum retrieving failed for value : " + campaignSponsorship, e);
            requestStatus.setValid(false);
            requestStatus.setMessage("Please select a valid Campaign Sponsorship.");
            return requestStatus;
        }

        if(FCUtil.isEmpty(FCUtil.trim(request.getParameter("campaign_description")))) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter detailed Campaign Description.");
            return requestStatus;
        }       
        freefundClass.setCampaignDescription(FCUtil.trim(request.getParameter("campaign_description")));
        
        if(FCConstants.DISCOUNT_TYPE_PERCENT.equals(freefundClass.getDiscountType()) && !request.getParameter("max_discount").isEmpty()) {
            freefundClass.setMaxDiscount(Float.parseFloat(request.getParameter("max_discount").trim()));
        } else {
            freefundClass.setMaxDiscount(0.0f);
        }
        
        Long conditionId = -1l;
        try {
            conditionId = Long.parseLong(FCUtil.trim(request.getParameter("apply_condition_id")));
            freefundClass.setApplyConditionId(conditionId);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid value apply condition id");
            return requestStatus;
        }

        Long paymentConditionId = -1l;
        try {
            paymentConditionId = Long.parseLong(FCUtil.trim(request.getParameter("payment_condition_id")));
            freefundClass.setPaymentConditionId(paymentConditionId);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid value payment condition id.");
            return requestStatus;
        }

        boolean isNewCampaign = false;
        try {
            isNewCampaign = Boolean.parseBoolean(request.getParameter("isNewCampaign"));
            logger.info("is new campaign " + isNewCampaign);
            freefundClass.setIsNewCampaign(isNewCampaign);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Invalid selection of campaign framework type");
            return requestStatus;
        }

        Map dataMapJson = new HashMap<String, String>();
        String prefix = null;
        String codes = null;
        String length = null;
        String redeemConditionId = null;
        String message = null;
        String rechargeSuccessMessage = null;
        String rewardId = null;
        String ivrId = null;
        String ivrsClient = null;
        String[] mappingKeys;
        String arRewardId = null;
        try {
            prefix = FCUtil.trim(request.getParameter(FCConstants.PREFIX_PARAM_NAME));
            if(FCUtil.isEmpty(prefix)){
                throw new Exception("Please enter a valid prefix");
            }
            dataMapJson.put("prefix", prefix);

            codes = FCUtil.trim(request.getParameter(FCConstants.NUMBER_OF_CODES_PARAM_NAME));
            if(codes.length() == 0 || !codes.matches("\\d+")){
                throw new Exception("Please enter valid number of codes");
            }
            dataMapJson.put("codes", codes);

            length = FCUtil.trim(request.getParameter(FCConstants.LENGTH_PARAM_NAME));
            if(length.length() == 0 || !length.matches("^[1-9]\\d*(\\.\\d+)?$")){
                throw new Exception("Please enter valid length of generatable codes");
            }
            dataMapJson.put("length", length);
        }catch (Exception e){
            requestStatus.setValid(false);
            requestStatus.setMessage(e.getMessage());
            return requestStatus;
        }

        redeemConditionId = FCUtil.trim(request.getParameter(FCConstants.REDEEM_CONDITION_ID));
        dataMapJson.put("redeemConditionId", redeemConditionId);

        message = FCUtil.trim(request.getParameter(FCConstants.SUCCESS_MESSAGE_PARAM_NAME));
        dataMapJson.put(FCConstants.SUCCESS_MESSAGE_PARAM_NAME, message);

        rechargeSuccessMessage = FCUtil.trim(request.getParameter(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME));
        dataMapJson.put(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME, rechargeSuccessMessage);

        rewardId = FCUtil.trim(request.getParameter(FCConstants.REWARD_ID));
        dataMapJson.put(FCConstants.REWARD_ID, rewardId);
        logger.debug("Freefund class reward id: " + rewardId);

        arRewardId = FCUtil.trim(request.getParameter(FCConstants.AR_REWARD_ID));
        dataMapJson.put(FCConstants.AR_REWARD_ID, arRewardId);

        ivrId = FCUtil.trim(request.getParameter(FCConstants.IVR_ID));
        dataMapJson.put(FCConstants.IVR_ID, ivrId);
        ivrsClient = FCUtil.trim(request.getParameter(FCConstants.IVRSClient));
        dataMapJson.put(FCConstants.IVRSClient, ivrsClient);
        mappingKeys = request.getParameterValues(FCConstants.MAPPING_KEYS);
        dataMapJson.put(FCConstants.MAPPING_KEYS, FCStringUtils.join(mappingKeys, ","));

        freefundClass.setDatamap(new JSONObject(dataMapJson).toString());

        float minRechargeValue = -1;
        try {
            minRechargeValue = Float.parseFloat(request.getParameter("min_recharge_value"));
            freefundClass.setMinRechargeValue(minRechargeValue);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid value for minimum recharge value.");
            return requestStatus;
        }

        float freefundValue = 0;
        try {
            freefundValue = Float.parseFloat(request.getParameter("freefund_value"));
            freefundClass.setFreefundValue(freefundValue);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid value for freefund value.");
            return requestStatus;
        }

        Calendar cal1 = Calendar.getInstance();
        String validFromDate = FCUtil.trim(request.getParameter("valid_from"));
        String validFromTime = FCUtil.trim(request.getParameter("valid_from_time"));
        if (StringUtils.isEmpty(validFromTime)) {
            validFromTime = "00:00:00";
        }
        try {
            cal1.setTime(sdf3.parse(validFromDate + " " + validFromTime));
            freefundClass.setValidFrom(cal1);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid from date and time in given format.");
            return requestStatus;
        }

        Calendar cal2 = Calendar.getInstance();
        String validUptoDate = FCUtil.trim(request.getParameter("valid_upto"));
        String validUptoTime = FCUtil.trim(request.getParameter("valid_upto_time"));
        if (StringUtils.isEmpty(validUptoTime)) {
            validUptoTime = "00:00:00";
        }
        try {
            cal2.setTime(sdf3.parse(validUptoDate + " " + validUptoTime));
            freefundClass.setValidUpto(cal2);
        } catch (Exception e) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Please enter a valid upto date and time in given format.");
            return requestStatus;
        }

        //Set CreatedAt only first time the FreefundClass is created 
        if(freefundClass.getCreatedAt() == null) {
            Calendar cal = Calendar.getInstance();
            freefundClass.setCreatedAt(cal);
        }
        freefundClass.setIsActive(1);

        if (freefundClass.getValidFrom().after(freefundClass.getValidUpto())) {
            requestStatus.setValid(false);
            requestStatus.setMessage("Unable to create freefund class " + "---"
                    + "  valid From date exceeded the Valid Upto date");
            return requestStatus;
        }
        requestStatus.setValid(true);
        return requestStatus;
    }

    @RequestMapping(value = "getLengthForCodes", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getDesiredLengthForCodes(@RequestParam Map<String, String> mapping, Model model,
                                                 HttpServletRequest request, HttpServletResponse response) {
        String numberOfCodes = FCUtil.trim(mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME));
        Map<String, String> responseMap = new HashMap<String, String>();
        if(numberOfCodes != null && numberOfCodes.length() > 0 && numberOfCodes.matches("\\d+")){
            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            Double desiredCodeLength = randomCodeGenerator.desiredCodeLengthFor(Integer.parseInt(numberOfCodes));
            responseMap.put("status", "success");
            responseMap.put("length", desiredCodeLength + "");
            return responseMap;
        }

        responseMap.put("status", "fail");
        responseMap.put("message", "Enter valid number of codes");
        return responseMap;
    }


    @RequestMapping(value = "generate")
    public void generateFreefundCode(@RequestParam Map<String, String> mapping, Model model,
            HttpServletRequest request, HttpServletResponse response) {
        String prefixParamvalue = FCUtil.trim(mapping.get(FCConstants.PREFIX_PARAM_NAME));
        String nuberOfCodesParamValue = FCUtil.trim(mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME));
        String lengthParamValue = FCUtil.trim(mapping.get(FCConstants.LENGTH_PARAM_NAME));
        try {
            response.reset();
            response.setContentType("text/csv;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=freefundcode.csv");
            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            randomCodeGenerator.setParams(prefixParamvalue, nuberOfCodesParamValue, lengthParamValue, TYPE.ALPHANUMERIC);
            Writer writer = response.getWriter();
            randomCodeGenerator.download(writer);
            response.flushBuffer();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.error("IOException:" + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception:" + e.getMessage());
        }

    }

    class RequestStatus {

        private boolean valid;
        private String  message;

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }

    public void sendCampaignCreateOrUpdateMail(FreefundClass freefundClass, FreefundClass oldFreefundClass, String curUserEmail){
        try{
            logger.info("FreefundClass : " + freefundClass.getName() + " [" + freefundClass.getId() + "] Create/Update notification sending started.");
            String mailSubject = "Freefund Campaign " + freefundClass.getName() + " Created!";
            freefundClass.getDatamapComponents();
            setCampaignMailDefaultValues(freefundClass);

            if(oldFreefundClass != null) {
                oldFreefundClass.getDatamapComponents();
                mailSubject = "Freefund Campaign " + freefundClass.getName() + " Updated!";
                setCampaignMailDefaultValues(oldFreefundClass);
            }
            emailService.sendCampaignCreateOrUpdateMail(freefundClass, oldFreefundClass, mailSubject, curUserEmail);
            logger.info("FreefundClass : " + freefundClass.getName() + " [" + freefundClass.getId() + "] Create/Update notification sent.");
        }
        catch(Exception e){
            logger.error("FreefundClass : " + freefundClass.getName() + " [" + freefundClass.getId() + "] Create/Update notification sending failed", e);
        }
    }

    public void setCampaignMailDefaultValues(FreefundClass freefundClass){  
        freefundClass.setCampaignSponsorship(setDefaultToNotSet(freefundClass.getCampaignSponsorship()));
        freefundClass.setCampaignDescription(setDefaultToNotSet(freefundClass.getCampaignDescription()));
        freefundClass.setRedeemConditionId(setDefaultToNotSet(freefundClass.getRedeemConditionId()));
        freefundClass.setRewardId(setDefaultToNotSet(freefundClass.getRewardId()));
        freefundClass.setArRewardId(setDefaultToNotSet(freefundClass.getArRewardId()));
        freefundClass.setSuccessMessage(setDefaultToNotSet(freefundClass.getSuccessMessage()));
        freefundClass.setRechargeSuccessMessage(setDefaultToNotSet(freefundClass.getRechargeSuccessMessage()));
        freefundClass.setDescription(setDefaultToNotSet(freefundClass.getDescription()));
    }

    public String setDefaultToNotSet(String defaultValue){
        if(defaultValue == null || defaultValue.trim().equals("")){
            return "NOT SET";
        }
        return defaultValue;
    }
}
