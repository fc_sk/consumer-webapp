package com.freecharge.admin.controller;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/generateCodes/*")
public class CodeGeneratorController  extends BaseAdminController {

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    public static final String GENERATE_CODES_LINK = "/admin/generateCodes/home.htm";

    @Autowired
    private FreefundService freefundService;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CODE_GENERATOR)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "home");
        return ViewConstants.GENERATE_CODES_ADMIN_HOME_VIEW;
    }

    @RequestMapping(value = "generate")
    public void generateFreefundCode(@RequestParam Map<String, String> mapping, Model model,
                                     HttpServletRequest request, HttpServletResponse response) {
        String prefixParamvalue = mapping.get(FCConstants.PREFIX_PARAM_NAME).trim();
        String nuberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();
        try {
            response.reset();
            response.setContentType("text/csv;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=freefundcode.csv");

            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            randomCodeGenerator.setPrefix(prefixParamvalue);
            randomCodeGenerator.setNoOfCodes(Integer.parseInt(nuberOfCodesParamValue));
            randomCodeGenerator.setLength(Integer.parseInt(lengthParamValue));
            randomCodeGenerator.setType(RandomCodeGenerator.TYPE.ALPHANUMERIC);
            randomCodeGenerator.setCustomChar(true);
            Writer writer = response.getWriter();
            randomCodeGenerator.download(writer);
            response.flushBuffer();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.error("IOException:" + e.getMessage());
        } catch (Exception e) {
            logger.error("Exception:" + e.getMessage());
        }

    }

    @RequestMapping(value = "generate/validate", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> generateFreefundValidate(@RequestParam Map<String, String> mapping, Model model,
                                                 HttpServletRequest request, HttpServletResponse response) {
        String nuberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();

        Integer givenCodeLength = 0;
        try {
            givenCodeLength = Integer.parseInt(lengthParamValue);
        } catch (Exception e) {
            logger.error("Error parsing length field : " + e.getMessage());
        }
        Integer numberOfCodes = 0;
        try {
            numberOfCodes = Integer.parseInt(nuberOfCodesParamValue);
        } catch (Exception e) {
           logger.error("Error parsing number of codes: " + e.getMessage());
        }

        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        Double desiredCodeLength = randomCodeGenerator.desiredCodeLengthFor(numberOfCodes);

        Map<String, String> responseMap = new HashMap<String, String>();
        if (givenCodeLength >= desiredCodeLength) {
            responseMap.put("status", "success");
        } else {
            responseMap.put("status", "fail");
        }
        String message = "The advisable code length should be " + desiredCodeLength.toString()
                + " to avoid generating duplicate codes [ For a duplication reduction factor of "
                + Math.pow(10, -RandomCodeGenerator.DUPLICATION_REDUCTION_FACTOR) + " ]";

        responseMap.put("message", message);
        return responseMap;
    }

}
