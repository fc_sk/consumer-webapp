package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

@Controller
@RequestMapping("/admin/tools/recharge/*")
public class RechargeAdminToolsController extends BaseAdminController {
	public static final String BULK_STATUS_CHECK_LINK = "/admin/tools/recharge/bulk/statuscheck/view.htm";
	private static Logger logger = LoggingFactory.getLogger(RechargeAdminToolsController.class);
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private KestrelWrapper kestrelWrapper;
	
	@Autowired
	private OrderIdSlaveDAO orderidDao;
	
	@RequestMapping(value = "bulk/statuscheck/view", method = RequestMethod.GET)
    public final String getBulkStatusCheckView(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        String view = "admin/tool/bulkrecharge/statuscheck/viewpage";
		if (!hasAccess(request, AdminComponent.BULK_STATUS_CHECK)) {
            return get403HtmlResponse(response);
        }
        return view;
    }

	@RequestMapping(value = "bulk/statuscheck/result", method = RequestMethod.POST)
    public final String doBulkStatusCheck(@RequestParam("fileInputIds") final MultipartFile file,
                  @RequestParam(required = false, value = "inputIds") final String inputIdsString,
                  final Model model, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
			String view = "admin/tool/bulkrecharge/statuscheck/viewpage";
			if (!hasAccess(request, AdminComponent.BULK_STATUS_CHECK)) {
                  return get403HtmlResponse(response);
			}	
			Set<String> inputIdSet = FCUtil.spiltStringOnNewLine(inputIdsString);
			if (FCUtil.isEmpty(inputIdSet)) {
				inputIdSet = FCUtil.getStringSetFromFile(file);
			}
			
			if (FCUtil.isEmpty(inputIdSet)) {
				model.addAttribute("messages", "Please enter Order Id either in TexArea box or upload a requestIds File.");
				return view;
			}
			
			List<String> successProcessList = new ArrayList<>();
			List<String> failedProcessList = new ArrayList<>();
			for (String input : inputIdSet) {
				if (FCUtil.isEmpty(input)) {
					continue;
				}
				
				if (!FCUtil.validateOrderIdFormat(input)) {
					failedProcessList.add(input);
					continue;
				}
				String productType = null;
				try {
					productType = orderService.getProductTypeForAllRechargeCase(input);
				} catch (Exception e) {
					logger.error("Exception on statuscheck-ex on get productype of : "+ input, e);
					failedProcessList.add(input); 
					continue;
				}
				
				if (FCUtil.isEmpty(productType)) {
					continue;
				}
				
				boolean status = true;
				if (FCUtil.isRechargeProductType(productType)) {
					logger.info("Started recharge status check (recharge-product) : " + input);
					try {
						kestrelWrapper.enqueueForStatusCheck(input, StatusCheckType.Relaxed);
						status = true;
						logger.info("Success status check process(recharge-product) :" + input);
					} catch (Exception ex) {
						logger.error("Exception on status check process(recharge-product) :" + input, ex);
					}		
				} else if (FCUtil.isBillPostpaidType(productType)) {
					logger.info("Started status check process(postpaid) :" + input);
					try {
						kestrelWrapper.enqueueForPostpaidStatusCheck(input);
						status = true;
						logger.info("Successfuly enqueue process done for status check process(postpaid) :" + input);
					} catch (Exception ex) {
						logger.error("Exception while enqueue process for status check process(postpaid) :" + input, ex);
					}
				} else if (FCUtil.isUtilityPaymentType(productType)) {
					logger.info("Started status check process(Billpay) :" + input);
					try {
						String billId = orderidDao.getBillIdForOrderId(input, productType);
						String lookupId = orderidDao.getLookupIdForOrderId(input);
						kestrelWrapper.enqueueForBillPayStatusCheck(billId, lookupId,
								StatusCheckType.Relaxed);
						status = true;
						logger.info("Successfuly enqueue process done for status check process(billpay) :" + input);	
					} catch (Exception ex) {
						logger.error("Exception while enqueue process for status check process(billpay) :" + input, ex);	
					}
				}
				
				if (status) {
					successProcessList.add(input);
				} else {
					failedProcessList.add(input);
				}
			}
			model.addAttribute("successlist", successProcessList);
			model.addAttribute("failedlist", failedProcessList);
          
			return view;
	}
}