package com.freecharge.admin.controller;

import com.freecharge.campaignhistory.CampaignHistoryService;
import com.freecharge.campaignhistory.entity.Campaign;
import com.freecharge.campaignhistory.entity.CampaignHistory;
import com.freecharge.campaignhistory.entity.RewardReattemptResponse;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.util.ViewConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Vivek on 28/03/16.
 */
@Controller
@RequestMapping("/admin/customertrail/campaignhistory/*")
public class CampaignHistoryController extends BaseAdminController {

    @Autowired
    private CampaignHistoryService campaignHistoryService;

    @RequestMapping(value = "searchbyemail", method = RequestMethod.GET)
    public String getCampaignHistoryByEmail(@RequestParam final Map<String, String> mapping, final Model model,
                        final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String view = "admin/customertrail/campaignhistory/searchbyemail/view";
        String emailId = mapping.get("emailid");
        String campaignIdString = mapping.get("campaignid");
        if(FCUtil.isEmpty(emailId) || FCUtil.isEmpty(campaignIdString)) {
            model.addAttribute("messages", "Please enter valid data");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        model.addAttribute("campaignemailid", emailId);
        List<Campaign> campaignList = campaignHistoryService.getAllCampaigns();
        if(null != campaignList) {
            model.addAttribute("campaignList", campaignList);
        }
        Integer campaignId = Integer.parseInt(campaignIdString);
        Map<String, Object> responseCampaignMap = campaignHistoryService.getCampaignByUser(emailId, campaignId);
        if(null != responseCampaignMap) {
            if(responseCampaignMap.get(CampaignHistoryService.STATUS_STRING) ==
                    CampaignHistoryService.STATUS_SUCCESS) {
                List<CampaignHistory> campaignHistoryList =
                        (List<CampaignHistory>) responseCampaignMap.get(CampaignHistoryService.RESPONSE_STRING);
                if (null != campaignHistoryList && !campaignHistoryList.isEmpty()) {
                    model.addAttribute(CampaignHistoryService.STATUS_STRING,
                            CampaignHistoryService.STATUS_SUCCESS);
                    model.addAttribute("campaignHistoryList", campaignHistoryList);
                }
            } else {
                model.addAttribute(CampaignHistoryService.STATUS_STRING, CampaignHistoryService.STATUS_FAILED);
                model.addAttribute(CampaignHistoryService.ERROR_MESSAGE_STRING,
                        responseCampaignMap.get(CampaignHistoryService.ERROR_MESSAGE_STRING));
            }
        }
        return view;
    }
    @RequestMapping(value = "campaignview", method = RequestMethod.GET)
    public String getCampaignHistoryView(@RequestParam final Map<String, String> mapping, final Model model,
                        final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String view = "admin/customertrail/campaignhistory/searchbyemail/view";
        String emailId = mapping.get("emailid");
        if(FCUtil.isEmpty(emailId)) {
            model.addAttribute("messages", "Please enter valid data");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        List<Campaign> campaignList = campaignHistoryService.getAllCampaigns();
        if(null != campaignList) {
            model.addAttribute("campaignList", campaignList);
        }
        model.addAttribute("campaignemailid", emailId);
        return view;
    }
    @RequestMapping(value = "reward/reattempt", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody RewardReattemptResponse reAttemptRewardForCampaign(HttpServletRequest httpRequest,
            @RequestParam("campaignId") Integer campaignId, @RequestParam("trigger") String trigger,
            @RequestParam("uniqueId") String uniqueId) {
        RewardReattemptResponse rewardReattemptResponse = null;
        if(FCUtil.isEmpty(uniqueId) || FCUtil.isEmpty(trigger) || campaignId == null) {
            rewardReattemptResponse = new RewardReattemptResponse();
            rewardReattemptResponse.setStatus(false);
            rewardReattemptResponse.setMessage("Please enter valid data");
            return rewardReattemptResponse;
        }
        return campaignHistoryService.reAttemptReward(campaignId, trigger, uniqueId);
    }
}
