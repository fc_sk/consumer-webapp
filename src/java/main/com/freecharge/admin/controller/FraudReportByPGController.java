package com.freecharge.admin.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.model.FraudCaseHandlerData;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.order.service.OrderDataService;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.RefundResponse;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/fraud/reportbypg/handler/*")
public class FraudReportByPGController extends BaseAdminController {
    private final Logger                  logger                         = LoggingFactory
                                                                                 .getLogger(FraudReportByPGController.class);
    public static final String            FRAUD_REPORT_BYPG_HANDLER_VIEW = "/admin/fraud/reportbypg/handler/view.htm";
    public static final int MIN_SIZE_OF_ORDERID = 4;

    @Autowired
    private PaymentTransactionService     paymentTransactionService;

    @Autowired
    private OrderIdSlaveDAO               orderIdSlaveDAO;
    
    @Autowired
    private OrderIdWriteDAO orderIdWriteDAO;

    @Autowired
    private EasyDBWriteDAO                easyDBWriteDAO;

    @Autowired
    private WalletService                 walletService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired 
    private OrderDataService orderDataService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
	private UserAuthService userAuthService;
    
    @Autowired
    private WalletWrapper walletWrapper;

    @RequestMapping(value = "view", method = RequestMethod.GET)
    public final String getFraudHandlerView(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws Exception {
        if (!hasAccess(request, AdminComponent.FRAUD_REPORT_BY_PG_HANDLER)) {
            get403HtmlResponse(response);
            return null;
        }

        return ViewConstants.FRAUD_HANDLER_ADMIN_VIEW;
    }

    @RequestMapping(value = "process", method = RequestMethod.POST)
    public final ModelAndView handleFraudCase(@RequestParam("fileInputIds") final MultipartFile file,
            @RequestParam(required = false, value = "inputIds") final String inputIdsString, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FRAUD_REPORT_BY_PG_HANDLER)) {
            get403HtmlResponse(response);
            return null;
        }
        
        Set<String> inputIdSet = getInputIdSet(inputIdsString);
        if (inputIdSet == null || inputIdSet.isEmpty()) {
            inputIdSet = getInputIdSetFromFile(file);
        }

        if (inputIdSet == null || inputIdSet.isEmpty()) {
            model.addAttribute("messages",
                    " Please enter Request Id/Order Id for Status change in TexArea box or upload a requestIds File. ");
            return new ModelAndView(ViewConstants.FRAUD_HANDLER_ADMIN_VIEW);
        }

        List<FraudCaseHandlerData> fraudCaseHandlerDataList = new ArrayList<FraudCaseHandlerData>();
        List<String> invalidInputIdList = new ArrayList<String>();
        for (String inputId : inputIdSet) {
            if (inputId == null || inputId.isEmpty()) {
                continue;
            }

            FraudCaseHandlerData fraudCaseHandlerData = new FraudCaseHandlerData();
            if (FCUtil.validateOrderIdFormat(inputId)) {
                PaymentTransaction paymentTransaction = paymentTransactionService
                        .getUniquePaymentTransactionByMerchantOrderId(inputId);
                if (paymentTransaction != null) {
                    fraudCaseHandlerData = getFraudCaseHandlerData(paymentTransaction);
                } else {
                    invalidInputIdList.add(inputId);
                    logger.info("InputId is a orderid/mtxnid format but not in payment_txn table : " + inputId);
                }
            } else {
                PaymentTransaction paymentTransactionInfo = paymentTransactionService.getPaymentTransactionById(Integer
                        .parseInt(inputId));
                if (paymentTransactionInfo != null) {
                    fraudCaseHandlerData = getFraudCaseHandlerData(paymentTransactionInfo);
                } else {
                    invalidInputIdList.add(inputId);
                    logger.info("InputId is not a mtxnid/orderid/paymenttxnid" + inputId);
                }
            }
            fraudCaseHandlerDataList.add(fraudCaseHandlerData);
        }
        String fraudDataObjectId = UUID.randomUUID().toString();
        request.getSession().setAttribute(fraudDataObjectId, fraudCaseHandlerDataList);
        request.setAttribute("fraudDataObjectId", fraudDataObjectId);

        model.addAttribute("fraudCaseHandlerDataList", fraudCaseHandlerDataList);
        model.addAttribute("invalidInputIdList", invalidInputIdList);
        model.addAttribute("fraudDataObjectId", fraudDataObjectId);
        return new ModelAndView(ViewConstants.FRAUD_HANDLER_ADMIN_RESULT);
    }

    private FraudCaseHandlerData getFraudCaseHandlerData(PaymentTransaction paymentTransaction) {
        FraudCaseHandlerData fraudCaseHandlerData = new FraudCaseHandlerData();
        fraudCaseHandlerData.setOrderId(paymentTransaction.getOrderId());
        fraudCaseHandlerData.setPaymentTxnId(paymentTransaction.getPaymentTxnId());
        fraudCaseHandlerData.setPaymentGateway(paymentTransaction.getPaymentGateway());
        fraudCaseHandlerData.setMerchantTxnId(paymentTransaction.getMerchantTxnId());

        fraudCaseHandlerData.setService(orderIdWriteDAO.getProductName(paymentTransaction.getOrderId()).getLabel());  
        String ipAddress =  orderDataService.getIpFromOrderDataByOrderId(paymentTransaction.getOrderId());
        String ip = null;
        if (ipAddress != null) {
            if (ipAddress.contains(",")) {
                ip = splitAndGetString(ipAddress);
            } else {
                ip = ipAddress;
            }
        } else {
            ip = " ";
        }
        
        fraudCaseHandlerData.setIpAddress(ip);
        Users users = userServiceProxy.getUserByUserId(orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()));   
        
        if (users != null) {
            fraudCaseHandlerData.setEmail(users.getEmail());
            fraudCaseHandlerData.setProfileNumber(users.getMobileNo());
            UserProfile userProfile = userServiceProxy.getUserProfile(users.getUserId());
            if (userProfile != null) {
                String address = userProfile.getAddress1() == null ? "" : userProfile.getAddress1();
                String landmark = userProfile.getLandmark() == null ? "" : userProfile.getLandmark();
                String area = userProfile.getArea() == null ? "" : userProfile.getArea();
                String city = userProfile.getCity() == null ? "" : userProfile.getCity();
                fraudCaseHandlerData.setAddress(address + " " + landmark + " " + area + " " + city);
                fraudCaseHandlerData.setCustomerName((userProfile.getFirstName() == null ? "" : userProfile
                        .getFirstName()) + " " + (userProfile.getLastName() == null ? "" : userProfile.getLastName()));
            }

            /* BankRefund */
            Map<String, Object> refundToBankMap = refundToBankForFraudCase(users.getUserId(), paymentTransaction);
            if (refundToBankMap != null) {
                Map<String, String> refundedOrderIdMap = (Map<String, String>) refundToBankMap
                        .get("refundedOrderIdMap");
                List<String> refundedOrderIdList = (List<String>) refundToBankMap.get("refundedOrderIdList");
                String totalRefundedAmount = (String) refundToBankMap.get("refundedTotalAmount");
                String refundedOrderIDs = getStringFormat(refundedOrderIdList);
                fraudCaseHandlerData.setRefundedOrderIDs(refundedOrderIDs);
                fraudCaseHandlerData.setRefundedOrderIdAmountMap(refundedOrderIdMap);
                fraudCaseHandlerData.setRefundedAmount(totalRefundedAmount);
            }

            /* Ban the user-email */
            Boolean active = userAuthService.banUser(users.getEmail());
            if (active == null) {
                fraudCaseHandlerData.setBanEmailId("Fail to ban");
            } else {
                fraudCaseHandlerData.setBanEmailId("Banned");
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            Date currentDate = new Date();
            if (currentDate != null) {
                String refundedDate = dateFormat.format(currentDate);
                fraudCaseHandlerData.setRefundDate(refundedDate);
            }
        }
        return fraudCaseHandlerData;
    }

    private Map<String, Object> refundToBankForFraudCase(Integer userId, PaymentTransaction paymentTransactionFraud) {
        Map<String, Object> bankRefundData = new HashMap<>();
        Map<String, String> refundedOrderIdMap = new HashMap<>();
        List<String> refundedOrderIdList = new ArrayList<>();
        double totalRefundedAmount = 0;
        Amount balance = walletWrapper.getTotalBalance(userId);
        Double walletBalance = balance.getAmount().doubleValue();
 
        if (walletBalance <= paymentTransactionFraud.getAmount() && isBankRefundable(paymentTransactionFraud.getOrderId())) {
            if (walletBalance > 0) {
                try {
                    RefundResponse refundResponse = walletService.refundToPgForPaymentMxtnId(paymentTransactionFraud,
                            walletBalance, PaymentConstants.CS_BANK_REFUND);
                    if (refundResponse.isSuccess()) {
                        refundedOrderIdList.add(paymentTransactionFraud.getOrderId());
                        refundedOrderIdMap.put(paymentTransactionFraud.getOrderId(), walletBalance.toString());
                        totalRefundedAmount += walletBalance;
                        walletBalance = 0.0;
                    } else {
                        logger.error("Failed to refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                                + paymentTransactionFraud.getOrderId());
                    }
                } catch (Exception e) {
                    logger.error("Exception on refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                            + paymentTransactionFraud.getOrderId(), e);
                }
            }
        }

        if (walletBalance > 0) {
            List<String> orderIdList = getOrderIdList(userId);
            Collections.reverse(orderIdList);
            /* Remove fraud orderId and put into first position of the list */
            if (orderIdList.contains(paymentTransactionFraud.getOrderId())) {
                orderIdList.remove(paymentTransactionFraud.getOrderId());
                if (!(refundedOrderIdList.contains(paymentTransactionFraud.getOrderId()))) {
                    orderIdList.add(0, paymentTransactionFraud.getOrderId());
                }
            }

            List<PaymentTransaction> successPaymentTransactionList = getSuccessPaymentTransactionList(orderIdList);
            for (PaymentTransaction paymentTransaction : successPaymentTransactionList) {
                if (walletBalance <= 0) {
                    break;
                }
                if (walletBalance >= paymentTransaction.getAmount()) {
                    if (paymentTransaction.getAmount() > 0) {
                        try {
                            RefundResponse refundResponse = walletService.refundToPgForPaymentMxtnId(
                                    paymentTransaction, paymentTransaction.getAmount(), PaymentConstants.CS_BANK_REFUND);
                            if (refundResponse.isSuccess()) {
                                refundedOrderIdMap.put(paymentTransaction.getOrderId(), paymentTransaction.getAmount()
                                        .toString());
                                refundedOrderIdList.add(paymentTransaction.getOrderId());
                                totalRefundedAmount += paymentTransaction.getAmount();
                                walletBalance = walletBalance - paymentTransaction.getAmount();
                            } else {
                                logger.error("Failed to refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                                        + paymentTransaction.getOrderId());
                            }
                        } catch (Exception e) {
                            logger.error(
                                    "Exception on refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                                            + paymentTransaction.getOrderId(), e);
                        }
                    }
                } else {
                    if (walletBalance > 0) {
                        try {
                            RefundResponse refundResponse = walletService.refundToPgForPaymentMxtnId(
                                    paymentTransaction, walletBalance, PaymentConstants.CS_BANK_REFUND);
                            if (refundResponse.isSuccess()) {
                                refundedOrderIdMap.put(paymentTransaction.getOrderId(), walletBalance.toString());
                                refundedOrderIdList.add(paymentTransaction.getOrderId());
                                totalRefundedAmount += walletBalance;
                                walletBalance = 0.0;
                            } else {
                                logger.error("Failed to refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                                        + paymentTransaction.getOrderId());
                            }
                        } catch (Exception e) {
                            logger.error(
                                    "Exception on refund-to-bank in fraud handler tool-fraud reported by PG,orderId : "
                                            + paymentTransaction.getOrderId(), e);
                        }
                    }
                }
            }
        }

        bankRefundData.put("refundedOrderIdList", refundedOrderIdList);
        bankRefundData.put("refundedTotalAmount", String.valueOf(totalRefundedAmount));
        bankRefundData.put("refundedOrderIdMap", refundedOrderIdMap);
        return bankRefundData;
    }

    private List<PaymentTransaction> getSuccessPaymentTransactionList(List<String> orderIdList) {
        List<PaymentTransaction> successPaymentTransactionList = new ArrayList<>();
        for (String orderId : orderIdList) {
            PaymentTransaction paymentTransaction = paymentTransactionService
                    .getLastPaymentTransactionSentToPG(orderId);
            if (paymentTransaction != null) {
                if (paymentTransaction.getIsSuccessful()) {
                    successPaymentTransactionList.add(paymentTransaction);
                }
            }
        }
        return successPaymentTransactionList;
    }

    private List<String> getOrderIdList(Integer userId) {
        List<String> orderIdList = new ArrayList<String>();
        if (userId != null) {
            List<UserTransactionHistory> userTransactionHistoryList = userTransactionHistoryService
                    .findAllUserTransactionHistoryByUserId(userId);
            if (userTransactionHistoryList != null && userTransactionHistoryList.size() > 0) {
                for (UserTransactionHistory userTransactionHistory : userTransactionHistoryList) {
                    if (isBankRefundable(userTransactionHistory.getOrderId())) {
                        orderIdList.add(userTransactionHistory.getOrderId());
                    }
                }
            }
        }
        return orderIdList;
    }

    /**
     * @param orderIdList
     * @param userTransactionHistory
     */
    private boolean isBankRefundable(String orderid) {
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                .findUserTransactionHistoryByOrderId(orderid);
        
        if (userTransactionHistory != null) {
            if ("T".equals(userTransactionHistory.getProductType())) {
                if ("00".equals(userTransactionHistory.getTransactionStatus())) {
                    return true;
                }
            }
            if (FCUtil.isRechargeProductType(userTransactionHistory.getProductType())) {
                if ("Failed".equals(RechargeUtil.getRechargeStatusMessage(userTransactionHistory.getTransactionStatus()))) {
                    return true;
                }
            }

            if (FCUtil.isBillPostpaidType(userTransactionHistory.getProductType())) {
                if ("55".equals(userTransactionHistory.getTransactionStatus())) {
                    return true;
                }
            }
        } else {
            ProductName productName = orderIdWriteDAO.getProductName(orderid);
            if (ProductName.WalletCash.equals(productName)) 
             {
                PaymentTransaction paymentTxn = paymentTransactionService
                        .getLastPaymentTransactionSentToPG(orderid);
                if (paymentTxn != null) {
                    if (paymentTxn.getIsSuccessful()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Set<String> getInputIdSet(final String inputIdsString) {
        String[] inputIds = inputIdsString.split("\n");
        Set<String> inputIdSet = new HashSet<String>();
        for (String inputid : inputIds) {
            String inputId = StringUtils.trim(inputid);
            if (StringUtils.isNotBlank(inputId)) {
                inputIdSet.add(inputId);
            }
        }
        return inputIdSet;
    }

    private Set<String> getInputIdSetFromFile(final MultipartFile file) throws IOException {
        Set<String> inputIdSet = new HashSet<String>();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String inputId = StringUtils.trim(line);
            inputIdSet.add(inputId);
        }
        return inputIdSet;
    }

    /**
     * To export in CSV File We made a string of all refunded orderIds separated
     * by '/'
     */
    private String getStringFormat(List<String> refundedOrderIdList) {
        StringBuffer refundedOrderIDs = new StringBuffer();
        for (String refundedOrderId : refundedOrderIdList) {
            refundedOrderIDs.append(refundedOrderId);
            refundedOrderIDs.append("/");
        }
        return refundedOrderIDs != null ? refundedOrderIDs.toString() : null;
    }

    private String splitAndGetString(String ipAddress) {
        String ip = null;
        if (ipAddress != null && !(ipAddress.isEmpty())) {
            String[] split = ipAddress.split(",");
            ip = split[0];
        }
        return ip;
    }
   
    /* Excel export code */
    @RequestMapping(value = "/excelReportForFraudCase", method = RequestMethod.GET)
    protected ModelAndView getExcelFIle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fraudDataObjectId = request.getParameter("fraudDataObjectId").trim();
        HttpSession session = request.getSession(false);
        List<FraudCaseHandlerData> fraudCaseHandlerDataList = null;
        if (session != null) {
            Object fraudDataObject = session.getAttribute(fraudDataObjectId);
            if (fraudDataObject != null) {
                fraudCaseHandlerDataList = (List<FraudCaseHandlerData>) fraudDataObject;
            }
        }
        return new ModelAndView("fraudReportByPGHandlerExcelSummary", "excelFraudhandlerData", fraudCaseHandlerDataList);
    }
}
