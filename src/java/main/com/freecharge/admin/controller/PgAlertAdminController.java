package com.freecharge.admin.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.entity.PgAlert;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.services.PaymentGatewayManagementService;
import com.freecharge.recharge.services.PgAlertService;
import com.freecharge.rest.user.PgAlertCacheManager;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/*")
public class PgAlertAdminController extends BaseAdminController {
    
    private Logger                  logger          = LoggingFactory.getLogger(getClass());
    public static final String      PG_ALERT_LINK   = "/admin/pgalert/create.htm";
    public static final String      PG_ID     = "pgkey_list";
    public static final String      START_DATE_TIME = "start_date";
    public static final String      END_DATE_TIME   = "end_date";
    public static final String      ALERT_MESSAGE   = "alertMsg";
    public static final String      PG_ALERT_LIST_LINK = "/admin/pgalert/list.htm";
    @Autowired
    PaymentGatewayManagementService paymentGatewayManagementService;
    
    @Autowired
    PgAlertService pgAlertService;
    
    @Autowired
    PgAlertCacheManager pgAlertCacheManager;
    /**
     * Show the PgAlert form
     */
    @RequestMapping(value = "pgalert/create", method = RequestMethod.GET)
    public String pgAlert(Model model, HttpServletRequest request, HttpServletRequest response) throws Exception {
        Map<Integer, String> pgNames = paymentGatewayManagementService.getBanksGrpMaster();
        model.addAttribute("pgNames", pgNames);

        return ViewConstants.PG_ADMIN_ALERT;
    }
    /**
     * Handle the submit action of the Paymentgateway Alert form and save the PgAlert instance
     */
    @RequestMapping(value = "pgalert/create", method = RequestMethod.POST)
    public String pgAlert(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        logger.debug("Handling POST of pgalert");
        
        String pgIdParamValue = mapping.get(PgAlertAdminController.PG_ID);
        String startDateTimeParamValue = mapping.get(PgAlertAdminController.START_DATE_TIME).trim();
        String endDateTimeParamValue = mapping.get(PgAlertAdminController.END_DATE_TIME).trim();
        String alertMessageParamValue = mapping.get(PgAlertAdminController.ALERT_MESSAGE).trim();
 
        try{
            pgAlertService.savePgAlertId(pgIdParamValue, startDateTimeParamValue, endDateTimeParamValue, alertMessageParamValue);
            model.addAttribute("successMsg", "Saved pgAlert for pgId = " + pgIdParamValue);
        }
        catch (ParseException e) {
        logger.error("Error while parsing date for pgId " + pgIdParamValue, e);
        model.addAttribute("errorMsg", "Failed to save pgAlert while parsing date !!"+ startDateTimeParamValue +" "+endDateTimeParamValue);
        } catch (Exception e) {
            logger.error("Exception raised during pg alert check api for pgId " + pgIdParamValue, e);
            model.addAttribute(ViewConstants.PG_ADMIN_ALERT, "Not able to fetch Pg Id, Please Try After Some Time");
        }
        return ViewConstants.PG_ADMIN_ALERT;
    }
    @RequestMapping(value = "pgalert/list", method = RequestMethod.GET)
    public String getAllPgAlert(ModelMap model) {
        List<PgAlert> allPgAlertsList = pgAlertService.getAllPgAlerts();
        if(allPgAlertsList.size() == 0 || allPgAlertsList == null) 
            model.addAttribute("noDownTime", "No Downtime Specified for any PG");
        else 
            model.addAttribute("allPgAlertsList", allPgAlertsList); 
            
    return ViewConstants.PG_ALERT_LIST;
    }
    
    @RequestMapping(value = "pgalert/delete", method = RequestMethod.GET)
    public String deletePgAlert(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
            String messages = "";
            Long pgId = Long.parseLong(String.valueOf(mapping.get("pgId")));
            pgAlertService.deletePgAlert(pgId);
            messages = "PaymentGateway Alert Deleted Successfuly";
            return String.format("redirect:%s?message=%s", PG_ALERT_LIST_LINK, messages);
           
   }
}
