package com.freecharge.admin.controller;

import com.freecharge.admin.model.RewardHistoryWrapper;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.util.FCUtil;
import com.freecharge.customercare.service.CustomerTrailService;
import com.freecharge.growthclient.GrowthClientService;
import com.freecharge.growthclient.core.*;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.ViewConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Vivek on 16/03/16.
 */
@Controller
@RequestMapping("/admin/customertrail/growthclient/*")
public class GrowthClientController extends BaseAdminController {
    @Autowired
    GrowthClientService growthClientService;
    @Autowired
    UserServiceProxy userServiceProxy;
    @Autowired
    private CustomerTrailService customerTrailService;
    private static final String               BOLT_HISTORY_LINK       = "/admin/customertrail/growthclient/bolthistorybyorder/view.htm";
    @RequestMapping(value = "predictionbyemail/view", method = RequestMethod.POST)
    public String getPredictionView(@RequestParam final Map<String, String> mapping, final Model model,
          final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String emailId = mapping.get("emailid");
        if(FCUtil.isEmpty(emailId)) {
            model.addAttribute("messages", "Enter an email Id. ");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        emailId = emailId.trim();
        if(!FCUtil.isValidSearchEmailFormat(emailId)) {
            model.addAttribute("messages", "Enter a valid email Id. ");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        Map<String, Object> predictionResponseMap = growthClientService.getPredictionsByEmail(emailId);
        if(predictionResponseMap.get(GrowthClientService.STATUS_STRING).equals(GrowthClientService.STATUS_SUCCESS)) {
            MyPredictionsResponse myPredictionsResponse = (MyPredictionsResponse) predictionResponseMap.get(
                    GrowthClientService.RESPONSE_STRING);
            List<PredictionMaster> predictionMasterList = myPredictionsResponse.getPredictionMasterList();
            Users user = userServiceProxy.getUserByUserId(predictionMasterList.get(0).getUserId());
            model.addAttribute("user", user);
            Map<Integer, CricketMatch> cricketMatchMap = myPredictionsResponse.getMapCricketMatch();
            model.addAttribute("predictionmasterlist",predictionMasterList);
            model.addAttribute("cricketmatchmap", cricketMatchMap);
            model.addAttribute("predictionemailid", emailId);
            return ViewConstants.ADMIN_PREDICTION_SUCCESS_LINK;
        }
        else {
            model.addAttribute(GrowthClientService.STATUS_STRING,
                    predictionResponseMap.get(GrowthClientService.STATUS_STRING));
            model.addAttribute(GrowthClientService.ERROR_MESSAGE_STRING,
                    predictionResponseMap.get(GrowthClientService.ERROR_MESSAGE_STRING));
            model.addAttribute(GrowthClientService.ERROR_CODE_STRING,
                    predictionResponseMap.get(GrowthClientService.ERROR_CODE_STRING));
            model.addAttribute("predictionemailid", emailId);
            return ViewConstants.ADMIN_PREDICTION_FAILURE_LINK;
        }
    }

    @RequestMapping(value = "bolthistorybyemail/view", method = RequestMethod.GET)
    public String getBoltByEmailView(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String emailId = mapping.get("emailid");
        if(FCUtil.isEmpty(emailId)) {
            model.addAttribute("messages", "Enter an email Id. ");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        emailId = emailId.trim();
        if(!FCUtil.isValidSearchEmailFormat(emailId)) {
            model.addAttribute("messages", "Enter a valid email Id. ");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        Map<String, Object> boltResponseMap = growthClientService.getBoltHistoryByEmail(emailId);
        if(boltResponseMap.get(GrowthClientService.STATUS_STRING).equals(GrowthClientService.STATUS_SUCCESS)) {
            BoltHistoryResponse boltHistoryResponse = (BoltHistoryResponse) boltResponseMap.get(
                    GrowthClientService.RESPONSE_STRING);
            List<UserPointDetail> userPointDetailList = boltHistoryResponse.getUserPointDetails();
            Users user = userServiceProxy.getUserByUserId(userPointDetailList.get(0).getUserId());
            Integer boltBalance = (Integer) boltResponseMap.get(GrowthClientService.BOLT_BALANCE_STRING);
            model.addAttribute("user", user);
            model.addAttribute("userPointDetailList", userPointDetailList);
            model.addAttribute(GrowthClientService.BOLT_BALANCE_STRING, boltBalance);
            model.addAttribute("boltemailid", emailId);
            model.addAttribute("calledFrom", "BoltEmail");
            return ViewConstants.ADMIN_BOLT_HISTORY_SUCCESS_LINK;
        }
        else {
            model.addAttribute(GrowthClientService.STATUS_STRING,
                    boltResponseMap.get(GrowthClientService.STATUS_STRING));
            model.addAttribute(GrowthClientService.ERROR_MESSAGE_STRING,
                    boltResponseMap.get(GrowthClientService.ERROR_MESSAGE_STRING));
            model.addAttribute(GrowthClientService.ERROR_CODE_STRING,
                    boltResponseMap.get(GrowthClientService.ERROR_CODE_STRING));
            model.addAttribute("boltemailid", emailId);
            return ViewConstants.ADMIN_BOLT_HISTORY_FAILURE_LINK;
        }
    }

    @RequestMapping(value = "bolthistorybyorder/view", method = RequestMethod.GET)
    public String getBoltByOrderView(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String orderId = mapping.get("orderid");
        String creditTokenStatus = mapping.get("creditTokenStatus");
        String creditTokenMessage = mapping.get("creditTokenMessage");
        if( !FCUtil.isEmpty(creditTokenMessage) && !FCUtil.isEmpty(creditTokenStatus)) {
            model.addAttribute("creditTokenStatus", creditTokenStatus);
            model.addAttribute("creditTokenMessage", creditTokenMessage);
        }
        if(FCUtil.isEmpty(orderId)) {
            model.addAttribute("messages", "Enter an order Id. ");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        orderId = orderId.trim();
        Map<String, Object> boltResponseMap = growthClientService.getBoltHistoryByOrder(orderId);
        if(boltResponseMap.get(GrowthClientService.STATUS_STRING).equals(GrowthClientService.STATUS_SUCCESS)) {
            BoltHistoryResponse boltHistoryResponse = (BoltHistoryResponse) boltResponseMap.get(
                    GrowthClientService.RESPONSE_STRING);
            List<UserPointDetail> userPointDetailList = boltHistoryResponse.getUserPointDetails();
            Users user = userServiceProxy.getUserByUserId(userPointDetailList.get(0).getUserId());
            RewardHistoryWrapper rewardHistoryWrapper = customerTrailService.getRewardHistoryListFromOrderId(orderId);
            Integer boltBalance = (Integer) boltResponseMap.get(GrowthClientService.BOLT_BALANCE_STRING);
            model.addAttribute(GrowthClientService.BOLT_BALANCE_STRING, boltBalance);
            model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
            model.addAttribute("user", user);
            model.addAttribute("userPointDetailList", userPointDetailList);
            model.addAttribute("boltorderid", orderId);
            model.addAttribute("calledFrom", "BoltOrder");
            return ViewConstants.ADMIN_BOLT_HISTORY_SUCCESS_LINK;
        }
        else {
            model.addAttribute(GrowthClientService.STATUS_STRING,
                    boltResponseMap.get(GrowthClientService.STATUS_STRING));
            model.addAttribute(GrowthClientService.ERROR_MESSAGE_STRING,
                    boltResponseMap.get(GrowthClientService.ERROR_MESSAGE_STRING));
            model.addAttribute(GrowthClientService.ERROR_CODE_STRING,
                    boltResponseMap.get(GrowthClientService.ERROR_CODE_STRING));
            model.addAttribute("boltorderid", orderId);model.addAttribute("boltorderid", orderId);
            return ViewConstants.ADMIN_BOLT_HISTORY_FAILURE_LINK;
        }
    }

    @RequestMapping(value = "credittokenstouser/view", method = RequestMethod.GET)
    public String creditTokensToUser(@RequestParam final Map<String, String> mapping, final Model model,
             final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        String orderId = mapping.get("orderid");
        String userIdString = mapping.get("userid");
        String pointsString = mapping.get("points");
        if(FCUtil.isEmpty(orderId) || FCUtil.isEmpty(userIdString) || FCUtil.isEmpty(pointsString)) {
            model.addAttribute("messages", "Enter valid data!");
            return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
        }
        Long userId = Long.parseLong(userIdString.trim());
        Integer points = Integer.parseInt(pointsString.trim());
        Map<String, Object> creditTokenResponseMap = growthClientService.creditTokensToUser(userId, orderId, points);
        if(creditTokenResponseMap.get(GrowthClientService.STATUS_STRING).equals(GrowthClientService.STATUS_SUCCESS)) {
            CreditTokensToUserResponse creditTokensToUserResponse = (CreditTokensToUserResponse) creditTokenResponseMap.
                    get(GrowthClientService.RESPONSE_STRING);
            return String.format("redirect:%s?orderid=%s&creditTokenStatus=%s&creditTokenMessage=%s",
                    BOLT_HISTORY_LINK, orderId, GrowthClientService.STATUS_SUCCESS,
                    creditTokensToUserResponse.getMessage());
        }
        else {
            return String.format("redirect:%s?orderid=%s&creditTokenStatus=%s&creditTokenMessage=%s",
                    BOLT_HISTORY_LINK, orderId, GrowthClientService.STATUS_FAILED,
                    creditTokenResponseMap.get(GrowthClientService.ERROR_MESSAGE_STRING));
        }
    }
}
