package com.freecharge.admin.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.InCachedService;
import com.freecharge.freebill.common.BillPaymentCacheManager;
import com.freecharge.freebill.common.BillPaymentGatewayPriority;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.dao.BillWriteDAO;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/postpaidAggregatorPriority")
public class PostpaidAggregatorPriorityController extends BaseAdminController {

	@Autowired
	private OperatorCircleReadDAO operatorCircleDao;

	@Autowired
	private InCachedService cachedService;
	
	@Autowired
	private BillReadDAO billReadDAO;
	
	@Autowired
	private BillWriteDAO billWriteDAO;

    @Autowired
    private BillPaymentCacheManager billPaymentCacheManager;

	public static final String POSTPAID_AGGREGATOR_PRIORITY_URL = "/admin/postpaidAggregatorPriority.htm";

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.POSTPAID_AGGREGATOR_PRIORITY)){
            get403HtmlResponse(response);
            return null;
        }
        ModelAndView modelAndView = new ModelAndView();
        List<OperatorMaster> operators = operatorCircleDao.getOperatorsByProductId(201);
        modelAndView.addObject("operators", operators);
        modelAndView.setViewName(ViewConstants.POSTPAID_AGGREGATOR_PRIORITY);
        return modelAndView;
	}
	
	private static<T extends Set<String>> T splitPriority(T tokens, String commaDelimitedPriority) {
		String[] split = new String[]{};
		try {
			split = commaDelimitedPriority.split(",");
		} catch (NullPointerException e) {}
		for (int i = 0; i < split.length; i++) {
		    split[i] = split[i].trim();
		}
		tokens.addAll(Arrays.asList(split));
		return tokens;
	}
	
	private List<BillPaymentGatewayPriority> getInOprAggrPriorityList(int operatorId) {
		List<BillPaymentGatewayPriority> bpgp= billReadDAO.findBillPaymentGatewayByOperatorId(String.valueOf(operatorId));
		return bpgp;
	}
	
	@RequestMapping(value = "{operatorId}/aggregators", method = RequestMethod.GET, produces = "application/json")
	public String getAggregatorPriority(@PathVariable int operatorId, Model model) {
		LinkedHashSet<String> priorities = new LinkedHashSet<String>();
		if (operatorId >= 0) {
		    List<BillPaymentGatewayPriority> ps = getInOprAggrPriorityList(operatorId);
		    String postpaidAggregatorPriority = ps.get(0).getBillProvider();
			priorities = splitPriority(priorities, postpaidAggregatorPriority);
		} 
		model.addAttribute("data", priorities.toArray(new String[]{}));
		return "jsonView";
	}
	
	@RequestMapping(value = "{operatorId}/aggregators", method = RequestMethod.POST, produces = "application/json")
	public String setAggregatorPriority(@PathVariable int operatorId,
			@RequestParam("ag[]") String[] aggregators, Model model, Principal principal) {
		StringBuilder agstr = new StringBuilder();
		for (String each: aggregators) {
			if (agstr.length() > 0) {
				agstr.append(',');
			}
			agstr.append(each);
		}
		if (operatorId >= 0) {
			billWriteDAO.updateBillPaymentGatewayPriority(String.valueOf(operatorId), agstr.toString());
		}
        billPaymentCacheManager.deletePaymentGatewayPriorityCachedKey();
		return "jsonView";
	}
}
