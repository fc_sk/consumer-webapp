package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.properties.FCProperties;

@Controller
@RequestMapping("/admin/antifraud")

public class AntifraudController extends BaseAdminController{
    
    private static final String JSON_VIEW = "jsonView";
    @Autowired
    private FCProperties fcProperties;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
	public String list(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model)throws IOException{
		String view = "admin/antifraud/viewpage";
		if (!hasAccess(request, AdminComponent.ANTI_FRAUD)) {
            return get403HtmlResponse(response);
        }
	    Set<Object> antifraudKeyNames = fcProperties.getAntiFraudKeyNames();
	    Map<String, Object> props = new HashMap<>();
        for(Object antifraudKeyName: antifraudKeyNames){
            props.put((String)antifraudKeyName, fcProperties.getAntiFraudProperty((String)antifraudKeyName));
        }
		model.addAttribute("props", props);
        return view;
	}
    
    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(final HttpServletRequest request, final HttpServletResponse response, final Model model) {
        String key = request.getParameter("key");
        String value = request.getParameter("value");
        boolean returnVal = fcProperties.putAntiFraudProperty(key, value);
        if(returnVal){
            model.addAttribute("status", "success");
            model.addAttribute("message", "Antifraud prop for key: "+key +" updated successfully");
        }else{
            model.addAttribute("status", "error");
            model.addAttribute("message", "Could not update antifraud prop for key: "+key);
        }
        
        return JSON_VIEW;
    }
}
