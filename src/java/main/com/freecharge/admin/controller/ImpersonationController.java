package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.List;

import com.freecharge.common.framework.util.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.UserLoginDetailsBusinessDO;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.mapper.UserLoginDetailsDOMapper;
import com.freecharge.web.responseMapper.UserAuthLoginResponseDOMapper;
import com.freecharge.web.responsedo.UserAuthLoginResponseDO;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.webdo.UserLoginDetailsWebDO;

@Controller
@RequestMapping("/admin/impersonate")
public class ImpersonationController {
	public static final String IMPERSONATE_URL = "/admin/impersonate";

	@Autowired
	UsersDAO usersDAO;

	@Autowired
	UserAuthService userAuthService;
	
	@Autowired
    private UserServiceProxy userServiceProxy;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showImpersonateForm() {
		return ViewConstants.IMPERSONATE;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public String impersonate(@RequestParam String email, Model model) throws IOException {
		// get user object from DB
		Users user = userServiceProxy.getUserByEmailId(email.trim());
		if (user == null) {
			model.addAttribute("note", "No such userid (email): " + email);
			return ViewConstants.IMPERSONATE;
		}
		// prepare businessDO
		UserLoginDetailsBusinessDO userLoginDetailsBusinessDO =
				new UserLoginDetailsBusinessDO();
		userLoginDetailsBusinessDO.setLoggedIn(true);
		userLoginDetailsBusinessDO.setUserId(user.getUserId());
		userLoginDetailsBusinessDO.setEmail(email);
		userAuthService.getUserLoginInfo(userLoginDetailsBusinessDO);
		// convert businessDO to webDO, and further to responseDO
		UserLoginDetailsDOMapper webDOMapper = new UserLoginDetailsDOMapper();
		UserLoginDetailsWebDO webDO = (UserLoginDetailsWebDO) webDOMapper.convertBusinessDOtoWebDO(userLoginDetailsBusinessDO, null);
		UserAuthLoginResponseDOMapper responseDOMapper = new UserAuthLoginResponseDOMapper();
		UserAuthLoginResponseDO responseDO = (UserAuthLoginResponseDO) responseDOMapper.convertWebDOToResponseDO(webDO, null);
		// if login is alright, then set into the session
		boolean isLoggedIn = responseDO.isLoggedIn();
		if (isLoggedIn) {
			String eMail = webDO.getEmail();
            String firstName = webDO.getFirstName() + " (Impersonated)";
            Integer userId = new Integer(webDO.getUserId());

            FreechargeSession.setUserRelatedSessionData(eMail, firstName, isLoggedIn, userId, 
                    SessionConstants.LoginSource.EMAIL, null, null);

			model.addAttribute("LoginWebDO", webDO);

            return FCUtil.getRedirectAction("/");
		} else {
			model.addAttribute("note", "Error logging into account: " + email);
			return ViewConstants.IMPERSONATE;
		}
	}

}
