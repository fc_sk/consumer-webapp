package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 6/12/14
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/s3/*")
public class S3FileUploadController {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    public static final String FILE_PATH_PREFIX = "https://d32vr05tkg9faf.cloudfront.net/";

    @Autowired
    @Qualifier("s3Client")
    private AmazonS3 s3;

    @RequestMapping(value = "upload")
    public String upload(@RequestParam("file") MultipartFile file, @RequestParam Map<String, String> requestParams,
                         HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        if (file!=null){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.getSize());
            metadata.setContentType(file.getContentType());

            String path = "";
            if (FCStringUtils.isNotBlank(requestParams.get("filePath"))) {
                path = requestParams.get("filePath");
            }
            if (path.length()>0){
                if (path.charAt(0) == '/'){
                    path = path.substring(1);
                }
                if (path.length()>0){
                    if (path.charAt(path.length()-1) != '/'){
                        path+="/";
                    }
                }
            }
            String fileKey = path+file.getOriginalFilename();
            try {
            	s3.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
                PutObjectResult res = s3.putObject(new PutObjectRequest("fccontent-s3", fileKey, file.getInputStream(),
                        metadata));
                model.addAttribute("url", FILE_PATH_PREFIX+fileKey);

                return "admin/choose-file";
            }catch (AmazonServiceException ase){
                logger.error("Unable to upload file to s3. Service Exception", ase);
            }catch (AmazonClientException ace){
                logger.error("Unable to upload file to s3.Client Exception", ace);
            }
        }

        return "admin/choose-file";
    }

    @RequestMapping(value = "choose-file")
    public String chooseFile(HttpServletRequest request, HttpServletResponse response, Model model) {
        return "admin/choose-file";
    }
}
