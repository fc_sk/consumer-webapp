package com.freecharge.admin.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/*")
public class OperatorAlertAdminController extends BaseAdminController {

    private Logger                logger                   = LoggingFactory.getLogger(getClass());
    public static final String    OPERATOR_ALERT_LINK      = "/admin/operatoralert/create.htm";
    public static final String    OPERATOR_ID              = "operator_list";
    public static final String    CIRCLE_ID              = "circle_list";
    public static final String    START_DATE_TIME          = "start_date";
    public static final String    END_DATE_TIME            = "end_date";
    public static final String    ALERT_MESSAGE            = "alertMsg";
    public static final String    OPERATOR_ALERT_LIST_LINK = "/admin/operatoralert/list.htm";
    private static final boolean  RESTRICT_USER_ON_HOME_PAGE = true;
    private static final int CREATE_ALERT_EMAIL = 1;
    private static final int DELETE_ALERT_EMAIL = 0;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private OperatorAlertService  operatorAlertService;

    /**
     * Show the Operator Alert.
     * @return returns the constant to get the jsp page.
     * @param model sets the model.
     * @param request receives the httpServletRequest.
     * @param response receives the httpServletResponse.
     * @throws IOException thorws if any Runtime Exception
     */
    @RequestMapping(value = "operatoralert/create", method = RequestMethod.GET)
    public final String operatorAlert(final Model model, final HttpServletRequest request,
            final HttpServletRequest response) throws IOException {
        List<Map<String, Object>> operators = operatorCircleService.getAllOperators();
        List<CircleMaster> circles = operatorCircleService.getCircles();
        model.addAttribute("operatorList", operators);
        model.addAttribute("circles", circles);
        return ViewConstants.OPERATOR_ALERT;
    }

    /**
     * Handle the submit action of the Operator Alert form and save the Operator
     * Alert instance.
     * @return returns the constant to get the jsp page.
     * @param model sets the model.
     * @param request receives the httpServletRequest.
     * @param response receives the httpServletResponse.
     * @param mapping having the key pair value.
     * @throws IOException Throws the IOException.
     */
    @RequestMapping(value = "operatoralert/create", method = RequestMethod.POST)
    public final String createOperatorAlert(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        logger.debug("Handling POST of operatoralert");
        
        Integer circleId = null;
        long startTimestamp = 0;
        
        String operatorIdParamValue = mapping.get(OperatorAlertAdminController.OPERATOR_ID);
        String cricleIdParamValue = mapping.get(OperatorAlertAdminController.CIRCLE_ID);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        
        try {
               Date date = df.parse(mapping.get("start_date"));
               startTimestamp = date.getTime();
               if(StringUtils.isNotEmpty(cricleIdParamValue)) 
               circleId = Integer.parseInt(cricleIdParamValue);
               this.saveOperatorAlertId(mapping, model, request,circleId);
        } catch (Exception e) {
            logger.error("Error while Creating Operator Alert for operatorIdParamValue=" + operatorIdParamValue, e);
            model.addAttribute("errorMsg", "Error while Creating Operator Alert for operatorIdParamValue= "
                    + operatorIdParamValue);
        }
        
        List<Map<String, Object>> operators = operatorCircleService.getAllOperators();
        model.addAttribute("operatorList", operators);
        return ViewConstants.OPERATOR_ALERT;
    }

    @RequestMapping(value = "operatoralert/list", method = RequestMethod.GET)
    public final String getAllOperatorAlert(final ModelMap model) {
    	logger.info("Inside  Get All Operator Alert Method");
        List<OperatorAlert> allOperatorAlertsList = operatorAlertService.getAllOperatorAlerts();
        if (allOperatorAlertsList.size() == 0 || allOperatorAlertsList == null) {
            model.addAttribute("noDowntime", "No Downtime for any operator");
        } else {
            model.addAttribute("operatorAlertList", allOperatorAlertsList);
        }
        return ViewConstants.OPERATOR_ALERT_LIST;
    }

    @RequestMapping(value = "operatoralert/delete", method = RequestMethod.GET)
    public final String deleteOperatorAlert(@RequestParam final Map<String, String> mapping, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) throws IOException, ParseException {
        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        String emailContent = null;
        String messages = "";
        String currentAdminUser = getCurrentAdminUser(request);
        int operatorId = Integer.parseInt(String.valueOf(mapping.get("operatorId")));
        int circleId = 0;
        try {
            circleId = Integer.parseInt(String.valueOf(mapping.get("circleId")));
        } catch (Exception e) {
            // TODO: handle exception
        }
        Date date = df.parse(mapping.get("startDateTime"));
        
        long startTimestamp = date.getTime();
        
        System.out.print(TimeUnit.MILLISECONDS.toMinutes(startTimestamp));
        
        List<OperatorAlert> operatorAlerts = operatorAlertService.getAllOperatorAlerts();
        for (OperatorAlert operatorAlert : operatorAlerts) {
            if (operatorId == operatorAlert.getOperatorId()) {
                emailContent = String.format("Down Time was From: %s <br>\nTo: %s <br>\nAlert Message was: %s",
                        operatorAlert.getStartDateTime(), operatorAlert.getEndDateTime(),
                        operatorAlert.getAlertMessage());

            }
        }
        deleteOperatorAlert(operatorId, circleId, startTimestamp);
        messages = "Operator Alert Deleted Successfully";
        // Send email only for Prod mode
        if (fcProperties.isProdMode()) {
            operatorAlertService.sendEmail(DELETE_ALERT_EMAIL, operatorId, emailContent, currentAdminUser);
        }
        return String.format("redirect:%s?message=%s", OPERATOR_ALERT_LIST_LINK, messages);
    }
    
    private void saveOperatorAlertId(Map<String, String> mapping, Model model,HttpServletRequest request, Integer circleId) {
           String operatorIdParamValue = mapping.get(OperatorAlertAdminController.OPERATOR_ID);
           String startDateTimeParamValue = mapping.get(OperatorAlertAdminController.START_DATE_TIME).trim();
           String endDateTimeParamValue = mapping.get(OperatorAlertAdminController.END_DATE_TIME).trim();
           String alertMessageParamValue = mapping.get(OperatorAlertAdminController.ALERT_MESSAGE).trim();
           String emailContent = null;
           Integer operatorId = null;
           String currentAdminUser = getCurrentAdminUser(request);
           operatorId = Integer.parseInt(operatorIdParamValue);
        
           operatorAlertService.saveOperatorAlertId(operatorId, circleId, startDateTimeParamValue,
                   endDateTimeParamValue, alertMessageParamValue, RESTRICT_USER_ON_HOME_PAGE);
           emailContent = String.format("<br>\n%s<br>\n DownTime:<br>\nFrom: %s<br>\nTo: %s", alertMessageParamValue,
                   startDateTimeParamValue, endDateTimeParamValue);
           model.addAttribute("successMsg", "Created operatorAlert for operatorId=" + operatorIdParamValue);
           // Send email only for Prod mode
           if (fcProperties.isProdMode()) {
               operatorAlertService.sendEmail(CREATE_ALERT_EMAIL, operatorId, emailContent, currentAdminUser);
           }
    }
    
    private void deleteOperatorAlert(Integer operatorId, Integer circleId, long startTimestamp) {
           operatorAlertService.deleteOperatorAlert(operatorId, circleId, startTimestamp);
    }
}
