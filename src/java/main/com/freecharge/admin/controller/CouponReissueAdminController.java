package com.freecharge.admin.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponHistory;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.service.CouponReissueService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;


@Controller
@RequestMapping("/admin/coupon-reissue-admin/*")
public class CouponReissueAdminController  extends BaseAdminController{
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	public static final String ADMIN_COUPON_REISSUE_LINK = "/admin/coupon-reissue-admin/home";
    public static final String ORDER_ID_PARAM_NAME = "orderId";

	 
	@Autowired
	private CouponReissueService couponReissueService;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

    @Autowired
    private HomeBusinessDao homeBusinessDao;
    
    @Autowired
    private OrderService orderService;
    
	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String viewHome(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.COUPON_REISSUE)){
            get403HtmlResponse(response);
            return null;
        }
		return ViewConstants.ADMIN_COUPON_REISSUE_VIEW;
	}

    @RequestMapping(value = "getCoupons", method = RequestMethod.POST)
    public String getOrderCoupons(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.COUPON_REISSUE)) {
            get403HtmlResponse(response);
            return null;
        }
        try {
            String orderId = null;
            String rechargeDetails = null;
            if (mapping.get("orderId") != null) {
                orderId = mapping.get("orderId").trim();
            }

            Map<String, Object> map = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
            if (map.get("amount") == null) {
                rechargeDetails = "Amount : NA  Operator : NA Dated : NA";
            } else {
                rechargeDetails = "Amount : " + map.get("amount") + " Operator : " + map.get("operatorName")
                        + " Dated : " + map.get("createdAt");
            }

                List<CouponHistory> coupons = new ArrayList<>();
                coupons = couponServiceProxy.getCouponsForOrder(orderId);
                // sort by date,coupon_store
                if(coupons!=null && coupons.size()>0) {
	                Collections.sort(coupons, new Comparator<CouponHistory>() {
	                    public int compare(CouponHistory ch1, CouponHistory ch2) {
	                        int c = ch1.getCreatedOn().compareTo(ch2.getCreatedOn());
	                        if (c != 0) {
	                            return c;
	                        } else {
	                            return ch1.getCouponStoreId().toString().compareTo(ch2.getCouponStoreId().toString());
	                        }
	                    }
	                });
                }

                model.addAttribute("coupons", coupons);

                List<Map<String, Object>> couponStockListMap = couponReissueService.getAllAvailableCouponReissue();
                List<String> couponStockList = new ArrayList<String>();
                if (couponStockListMap != null && !couponStockListMap.isEmpty()) {
                    for (Map<String, Object> listOfMap : couponStockListMap) {
                        Integer couponId = (Integer) listOfMap.get("coupon_store_id");
                        String campaignName = (String) listOfMap.get("campaign_name");
                        Integer couponValue = (Integer) (listOfMap.get("coupon_value"));
                        Integer price = (Integer) (listOfMap.get("price"));
                        String couponType = (String) listOfMap.get("coupon_type");
                        String couponInfo = campaignName + "|" + couponValue + "|" + couponType + "|" + couponId + "|"
                                + price;
                        couponStockList.add(couponInfo);
                    }
                }
                Collections.sort(couponStockList);
                model.addAttribute("order", orderId);
                model.addAttribute("couponStockList", couponStockList);
            
            model.addAttribute("rechargeDetails", rechargeDetails);

        } catch (Exception e) {
            logger.error("Exception occured : ",e);
        }
        return "jsonView";
    }
	
	@RequestMapping(value = "reissue", method = RequestMethod.POST)
	public String replaceCoupons(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws Exception  {
		if (!hasAccess(request, AdminComponent.COUPON_REISSUE)){
            get403HtmlResponse(response);
            return null;
        }	
		String orderId = null;
		String newCouponStr = null;
  		List<BlockCouponRequest> blockCoupons = new ArrayList<>();
		try{
			if (mapping.get("orderId") != null && mapping.get("newCouponStr") != null) {
	            orderId = mapping.get("orderId").trim();
	            newCouponStr = mapping.get("newCouponStr").trim();
 	            String[] newCouponArr = newCouponStr.split(",");
 	            Integer userId = orderService.getUserIdForOrder(orderId);
 	            for(int i = 0 ; i < newCouponArr.length; i++){
	            	String  couponStoreId = newCouponArr[i].split("#")[0];
	            	String qty = newCouponArr[i].split("#")[1];
	            	BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
	            	blockCouponRequest.setCouponStoreId(Integer.parseInt(couponStoreId));
	            	blockCouponRequest.setOrderId(orderId);
	            	blockCouponRequest.setQuantity(Integer.parseInt(qty.trim()));
	            	blockCouponRequest.setCouponOptinType(CouponOptinType.REISSUE);
	            	blockCoupons.add(blockCouponRequest);
	            	couponServiceProxy.blockNewCouponForOrder(blockCouponRequest,userId);
	             }
			}
		}catch(Exception e){
			logger.error("An exception ocurred in reissue coupons ", e);
			throw new Exception("An error occured in reissueing coupons", e);
 		}
		return "jsonView";
	}
	
	
	@RequestMapping(value = "retriggerEmail", method = RequestMethod.POST)
	public String retriggerCouponsEmail(@RequestParam String orderId, Model model, HttpServletRequest request,
            HttpServletResponse response) throws Exception  {
		if (!hasAccess(request, AdminComponent.COUPONS_EMAIL_RETRIGGER)){
            get403HtmlResponse(response);
            return null;
        }	
		try{
			if (orderId != null) {
				
			}
		}catch(Exception e){
			logger.error("An exception ocurred in retriggering coupons email: " + e);
			throw new Exception("An error occured in retriggering coupons email.");
 		}
		return "jsonView";
	}
    	
 
}

