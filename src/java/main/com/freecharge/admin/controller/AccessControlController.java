package com.freecharge.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.admin.service.AccessControlUtil;
import com.mongodb.BasicDBList;
import org.apache.log4j.Logger;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.service.AccessControlService;
import com.freecharge.admin.service.AdminUserService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.mongodb.DBObject;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/22/13
 * Time: 7:29 PM
 * To change this template use File | Settings | File Templates.
 */
/*
 * AccessControlController - MVC controller for the mapping of /admin/access/* requests 
 * The controller returns views to the Access Control Page 
 * The Controller also handles all the API Calls 
 * save , list , addCategory , updateCategory , deleteCategory , addComponent , saveComponent
 * deleteComponent
 */
@Controller
@RequestMapping(value = "/admin/access/*")
public class AccessControlController extends BaseAdminController {

    private static final String ADMIN_STR = "admin";
    private final        Logger logger    = LoggingFactory.getLogger(AccessControlController.class);

    @Autowired
    AdminUserService adminUserService;

    @Autowired
    AccessControlService accessControlService;

    private final RequestMappingHandlerMapping handlerMapping;
    private final Set<String>                  restPatterns;

    @Autowired
    public AccessControlController(RequestMappingHandlerMapping handlerMapping) {
        this.handlerMapping = handlerMapping;
        Set<RequestMappingInfo> requestMappingSet = this.handlerMapping.getHandlerMethods().keySet();
        Set<String> requestPatternSet = new TreeSet<>();
        for (RequestMappingInfo requestMappingInfo : requestMappingSet) {
            Set<String> patterns = requestMappingInfo.getPatternsCondition().getPatterns();
            for (String p : patterns) {
                if (p.contains(ADMIN_STR)) {
                    String cPattern = AccessControlUtil.getRequestURIMatchString(p);
                    if (cPattern != null)
                        requestPatternSet.add(cPattern);
                }

            }
        }
        this.restPatterns = requestPatternSet;

    }

    private List<DBObject> categories;
    private List<DBObject> components;
    private static final String MODEL_ATTRIBUTE_USER_TYPE             = "userType";
    private static final String MODEL_ATTRIBUTE_CATEGORY              = "categories";
    private static final String MODEL_ATTRIBUTE_CATEGORY_NAME         = "categoryName";
    private static final String MODEL_ATTRIBUTE_USER_CATEGORIES       = "userCategories";
    private static final String MODEL_ATTRIBUTE_USER                  = "users";
    private static final String MODEL_ATTRIBUTE_COMPONENTS            = "components";
    private static final String MODEL_ATTRIBUTE_ADMIN_USER            = "adminUser";
    private static final String MODEL_ATTRIBUTE_SUPER_ADMIN_STRING    = "superAdminString";
    private static final String MODEL_ATTRIBUTE_ACCESS_CONTROL_STRING = "AccessControlString";
    private static final String MODEL_PARAMETER_EMAIL                 = "email";
    private static final String MODEL_ATTRIBUTE_QUERY_NAME            = "queryName";
    private static final String MODEL_ATTRIBUTE_NAME                  = "name";
    private static final String MODEL_ATTRIBUTE_URL                   = "URL";
    private static final String MODEL_ATTRIBUTE_ACTIVE                = "active";
    private static final String MODEL_ATTRIBUTE_QUERY                 = "query";
    private static final String MODEL_ATTRIBUTE_UPDATE                = "update";
    private static final String MODEL_ATTRIBUTE_REST_PATTERNS         = "restPatterns";
    private static final String MODEL_ATTRIBUTE_COMP_REST_PATTERNS    = "componentRestPatterns";

    /*TODO : Move Business logic to a service class.*/

    /* Method: listAccess Description: returns The view with model holding the values for page construction*/
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String listAccess(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        curUserEmail = getCurrentAdminUser(request);
        String userType = "";
        String returnView = "admin/manage_access";
        int prec = 0;
        Set allRestPatterns = new HashSet<String>(restPatterns);
        DBObject userObj = this.adminUserService.getAdminUserByEmail(curUserEmail);
        List<DBObject> adminUsers = this.adminUserService.fetchAllUsers();
        List<DBObject> categories = this.accessControlService.fetchAllCategories();
        List<DBObject> components = this.accessControlService.fetchAllComponents();
        for (DBObject component : components) {
            BasicDBList dbl = (BasicDBList) component.get(AccessControlUtil.COMPONENT_REST_PATTERNS);
            if (dbl != null)
                Collections.addAll(allRestPatterns, dbl.toArray());
        }
        List<DBObject> userList = new ArrayList<DBObject>();
        boolean isUserUnderAdmin = false;
        List<String> userCatList;
        this.categories = categories;
        this.components = components;
        if (!hasAccess(request, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }

        /* Selecting users that fall under the Admin  
         * >precedence of user is decieded and an user can fall under all the users 
         * whose precedence is greater than theirs 
         * Super Admin :5
         * Admin : 3 
         * Regular User : 1
         * >User category : users that have a category are only added to admin's assigned with same category
         * users without assigned category are assigned to all admins */
        if (adminUsers != null) {
            userCatList = (List<String>) userObj.get(MODEL_ATTRIBUTE_CATEGORY);
            for (DBObject user : adminUsers) {
                prec = 1;
                if (user.get(MODEL_ATTRIBUTE_USER_TYPE) != null) {
                    userType = String.valueOf(user.get(MODEL_ATTRIBUTE_USER_TYPE));
                } else {
                    userType = FCConstants.ADMIN_USER_TYPE_REGULAR_READER;
                }

                prec = this.accessControlService.getAuthority(userType);
                if (userObj.get(MODEL_ATTRIBUTE_USER_TYPE) != null && (prec < this.accessControlService
                        .getAuthority(userObj.get(MODEL_ATTRIBUTE_USER_TYPE).toString()))) {
                    if (this.accessControlService.getAuthority(String.valueOf(userObj.get(MODEL_ATTRIBUTE_USER_TYPE)))
                            == FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN_PRECEDENCE) {
                        userList.add(user);
                        continue;
                    }
                    List<String> userCategories = (List<String>) user.get(MODEL_ATTRIBUTE_CATEGORY);
                    if (userCategories != null && userCatList != null) {
                        for (String cat : userCategories) {
                            if (userCatList.contains(cat)) {
                                isUserUnderAdmin = true;
                                break;
                            }
                        }
                        if (isUserUnderAdmin == true) {
                            userList.add(user);
                        }
                    } else {
                        userList.add(user);
                    }
                }
            }
        }
        List<String> userTypeList = this.accessControlService.getUserTypeList();
        if (this.accessControlService.getAuthority(String.valueOf(userObj.get(MODEL_ATTRIBUTE_USER_TYPE)))
                != FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN_PRECEDENCE) {
            userTypeList.remove(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN);
        }

        if (this.accessControlService.getAuthority(String.valueOf(userObj.get(MODEL_ATTRIBUTE_USER_TYPE)))
                == FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN_PRECEDENCE) {
            model.addAttribute(MODEL_ATTRIBUTE_USER_CATEGORIES, this.accessControlService
                    .getListOfStringFromDBObjectList(MODEL_ATTRIBUTE_CATEGORY_NAME, categories));
        } else {
            model.addAttribute(MODEL_ATTRIBUTE_USER_CATEGORIES, userObj.get(MODEL_ATTRIBUTE_CATEGORY));
        }

        model.addAttribute(MODEL_ATTRIBUTE_USER_TYPE, userTypeList);
        model.addAttribute(MODEL_ATTRIBUTE_CATEGORY, categories);
        model.addAttribute(MODEL_ATTRIBUTE_USER, userList);
        model.addAttribute(MODEL_ATTRIBUTE_COMPONENTS, components);
        model.addAttribute(MODEL_ATTRIBUTE_REST_PATTERNS, allRestPatterns);
        model.addAttribute(MODEL_ATTRIBUTE_ADMIN_USER, userObj);
        model.addAttribute(MODEL_ATTRIBUTE_SUPER_ADMIN_STRING, FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN);
        model.addAttribute(MODEL_ATTRIBUTE_ACCESS_CONTROL_STRING, AdminComponent.ACCESS_CONTROL.toString());
        return returnView;
    }

    /* Method: saveAccess Description: handler for API call of saveUser*/
    @RequestMapping(value = "saveUser", method = RequestMethod.POST)
    public String saveAccess(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        String email = "";
        String componentStr = "";
        String userType = "";
        String categoriesStr = "";
        String status = "success";
        String message = "Access Components updated for user: ";
        String returnView = "jsonView";
        JSONParser parser = new JSONParser();
        List<String> components;
        List<String> categories;
        curUserEmail = getCurrentAdminUser(request);
        if (FCStringUtils.isBlank(curUserEmail)) {
            return get403HtmlResponse(response);
        }
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        email = params.get(MODEL_PARAMETER_EMAIL);
        componentStr = params.get(MODEL_ATTRIBUTE_COMPONENTS);
        userType = params.get(MODEL_ATTRIBUTE_USER_TYPE);
        categoriesStr = params.get(MODEL_ATTRIBUTE_CATEGORY);
        if (FCStringUtils.isBlank(userType)) {
            userType = FCConstants.ADMIN_USER_TYPE_REGULAR_READER;
        }
        try {
            components = (List<String>) parser.parse(componentStr);
            categories = (List<String>) parser.parse(categoriesStr);
            this.accessControlService.updateComponentsAndUserTypeandCategory(email, components, userType, categories);
            model.addAttribute("status", "success");
            model.addAttribute("message", "Access Components updated for user: " + email);
            return returnView;
        } catch (ParseException pe) {
            return get400Response(response, "application/json",
                    "{\"status\": \"error\", \"message\" : \"Bad components value\"}");
        }
    }

    /* Method: addCategories Description: handler for API call of addCategory */
    @RequestMapping(value = "addCategory", method = RequestMethod.POST)
    public String addCategories(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        String message = "category not update duplicate/wrong entry";
        String status = "Failed";
        String name = "";
        String componentStr = "";
        String returnView = "jsonView";
        JSONParser parser = new JSONParser();
        curUserEmail = getCurrentAdminUser(request);
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        HashMap<String, Object> category = new HashMap<String, Object>();
        name = params.get(MODEL_ATTRIBUTE_CATEGORY_NAME);
        componentStr = params.get(MODEL_ATTRIBUTE_COMPONENTS);
        try {
            if (!this.accessControlService.checkForDuplicate(name, this.categories, MODEL_ATTRIBUTE_CATEGORY_NAME)) {
                List<String> components = (List<String>) parser.parse(componentStr);
                //Update admin user with new component list and userType
                category.put(MODEL_ATTRIBUTE_CATEGORY_NAME, name);
                category.put(MODEL_ATTRIBUTE_COMPONENTS, components);
                this.accessControlService.addCategory(category);
                message = "category added";
                status = "success";
            }
            model.addAttribute("status", status);
            model.addAttribute("message", message + ":" + name);
            return returnView;
        } catch (ParseException pe) {
            return get400Response(response, "application/json",
                    "{\"status\": \"error\", \"message\" : \"Bad components value \"}");
        }
    }

    /* Method: saveAccess Description: handler for API call of deleteCategory*/
    @RequestMapping(value = "deleteCategory", method = RequestMethod.POST)
    public String removeCategory(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        String name;
        String returnView = "jsonView";
        curUserEmail = getCurrentAdminUser(request);
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        name = params.get(MODEL_ATTRIBUTE_QUERY_NAME);
        this.accessControlService.deleteCategory(name);
        model.addAttribute("status", "success");
        model.addAttribute("message", "Category deleted: " + name);
        return returnView;
    }

    /* Method: saveCategory Description: handler for API call of saveCategory*/
    @RequestMapping(value = "saveCategory", method = RequestMethod.POST)
    public String saveCategory(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        curUserEmail = getCurrentAdminUser(request);
        String status = "Failed";
        String message = "The category coudn't be updated due to Duplicate name";
        String queryName = "";
        String name = "";
        String componentStr = "";
        String returnView = "jsonView";
        JSONParser parser = new JSONParser();
        List<String> components;
        HashMap<String, Object> category = new HashMap<String, Object>();
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        queryName = params.get(MODEL_ATTRIBUTE_QUERY_NAME);
        name = params.get(MODEL_ATTRIBUTE_CATEGORY_NAME);
        componentStr = params.get(MODEL_ATTRIBUTE_COMPONENTS);
        try {
            if ((!FCStringUtils.isBlank(queryName) && queryName.equals(name)) || !this.accessControlService
                    .checkForDuplicate(name, this.categories, MODEL_ATTRIBUTE_CATEGORY_NAME)) {
                components = (List<String>) parser.parse(componentStr);
                category.put(MODEL_ATTRIBUTE_CATEGORY_NAME, name);
                category.put(MODEL_ATTRIBUTE_COMPONENTS, components);
                this.accessControlService.updateCategory(queryName, category);
                message = "Category Updated ";
                status = "success";
            }
            model.addAttribute("status", status);
            model.addAttribute("message", message + name);
            return returnView;
        } catch (ParseException pe) {
            return get400Response(response, "application/json",
                    "{\"status\": \"error\", \"message\" : \"Bad components value\"}");
        }
    }

    /* Method: addComponent Description: handler for API call of addComponent*/
    @RequestMapping(value = "addComponent", method = RequestMethod.POST)
    public String addComponent(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = "";
        String name = "";
        String url = "";
        String active = "";
        String message = "component not updated: duplicate/wrong entry";
        String status = "Failed";
        String returnView = "jsonView";

        curUserEmail = getCurrentAdminUser(request);
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }

        name = params.get(MODEL_ATTRIBUTE_NAME);
        url = params.get(MODEL_ATTRIBUTE_URL);
        active = params.get(MODEL_ATTRIBUTE_ACTIVE);

        if (!this.accessControlService.checkForDuplicate(name, this.components, MODEL_ATTRIBUTE_NAME)) {
            if (!FCStringUtils.isBlank(name) && !FCStringUtils.isBlank(url) && !FCStringUtils.isBlank(active)) {
                HashMap<String, Object> component = new HashMap<>();
                component.put(MODEL_ATTRIBUTE_NAME, name);
                component.put(MODEL_ATTRIBUTE_URL, url);
                component.put(MODEL_ATTRIBUTE_ACTIVE, active);
                message = "The Component Added: ";
                status = "success";
                this.accessControlService.addComponent(component);
                model.addAttribute("status", status);
                model.addAttribute("message", message + name);
            }
        }
        return returnView;
    }

    /* Method: updateComponent Description: handler for API call of updateComponent*/
    @RequestMapping(value = "updateComponent", method = RequestMethod.POST)
    public String updateComponent(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {

        String curUserEmail = "";
        curUserEmail = getCurrentAdminUser(request);
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        String status = "failed";
        String message = "Cant make this update the name already exists";
        HashMap<String, String> component = new HashMap<>();
        String returnView = "jsonView";
        String name = params.get(MODEL_ATTRIBUTE_NAME);
        String query = params.get(MODEL_ATTRIBUTE_QUERY);
        String URL = params.get(MODEL_ATTRIBUTE_URL);
        String active = params.get(MODEL_ATTRIBUTE_ACTIVE);
        String rest_patterns_str = params.get(MODEL_ATTRIBUTE_COMP_REST_PATTERNS);
        String update = params.get(MODEL_ATTRIBUTE_UPDATE);
        JSONParser parser = new JSONParser();
        List<String> componentRestPatterns;
        try {
            componentRestPatterns = (List<String>) parser.parse(rest_patterns_str);
        } catch (ParseException pe) {
            return get400Response(response, "application/json",
                    "{\"status\": \"error\", \"message\" : \"Bad rest endpoints value\"}");
        }

        if (query.equals(name) || !this.accessControlService
                .checkForDuplicate(name, this.components, MODEL_ATTRIBUTE_NAME))

        {
            this.accessControlService.updateComponent(update, query, name, URL, active, componentRestPatterns);
            status = "success";
            message = "Component Updated";
        }

        model.addAttribute("status", status);
        model.addAttribute("message", message + " : " + name);
        return returnView;
    }

    /* Method:deleteComponent  Description: handler for API call of addComponent*/
    @RequestMapping(value = "deleteComponent", method = RequestMethod.POST)
    public String deleteComponent(@RequestParam Map<String, String> params, HttpServletRequest request,
            HttpServletResponse response, Model model) throws IOException {
        String curUserEmail = getCurrentAdminUser(request);
        String status = "";
        String message = "";
        if (!accessControlService.hasAccess(curUserEmail, AdminComponent.ACCESS_CONTROL)) {
            return get403HtmlResponse(response);
        }
        status = "success";
        message = "Component deleted :";

        String name = params.get(MODEL_ATTRIBUTE_QUERY_NAME);
        this.accessControlService.deleteComponent(name);
        model.addAttribute("status", status);
        model.addAttribute("message", message + name);
        return "jsonView";
    }

    private String get400Response(HttpServletResponse response, String contentType, String message) throws IOException {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        response.setContentType(contentType);
        PrintWriter pw = response.getWriter();
        pw.write(message);
        pw.flush();
        return null;
    }

    public static final String HOME_LINK = "/admin/access/list";
}