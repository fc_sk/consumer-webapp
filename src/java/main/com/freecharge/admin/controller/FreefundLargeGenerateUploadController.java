package com.freecharge.admin.controller;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.FreefundLargeGenerateUploadAdminHelper;
import com.freecharge.app.domain.dao.jdbc.FreefundReadDAO;
import com.freecharge.common.amazon.s3.service.AmazonS3Service;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.services.UploadCodesService;
import com.freecharge.mongo.service.FreefundTemporaryFilesStatusService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.promo.util.PromocodeConstants;
import com.freecharge.web.util.ViewConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/admin/freefund/largegenerateandupload/*")
public class FreefundLargeGenerateUploadController extends BaseAdminController {

    private static Logger logger = LoggingFactory.getLogger(FreefundLargeGenerateUploadController.class);

    public static final String FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK = "/admin/freefund/largegenerateandupload/home";

    @Autowired
    private FreefundReadDAO freefundService;

    @Autowired
    private FreefundTemporaryFilesStatusService freefundTemporaryFilesStatusService;

    @Autowired
    private UploadCodesService uploadCodesService;

    @Autowired
    @Qualifier("amazonS3Service")
    private AmazonS3Service amazons3Service;

    @Autowired
    private FreefundLargeGenerateUploadAdminHelper freefundLargeGenerateUploadAdminHelper;

    @Autowired
    private MetricsClient metricsClient;

    /**
     *
     * @param mapping
     * @param model
     * @param request
     * @param response
     * @return View for home page
     * @throws IOException
     */
    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        return setModelAttributeAndReturnHomeView("home", model, request, response);
    }

    /**
     *
     * @param model
     * @param request
     * @param response
     * @return View to generate codes page
     * @throws IOException
     */
    @RequestMapping(value = "home/generatecodes", method = RequestMethod.GET)
    public String generateCodes(Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        return setModelAttributeAndReturnHomeView("generateCodes", model, request, response);
    }

    /**
     *
     * @param mapping
     * @param model
     * @param request
     * @param response
     * @return View to download codes page
     * @throws IOException
     */
    @RequestMapping(value = "home/downloadcodes", method = RequestMethod.GET)
    public String downloadCodes(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        return setModelAttributeAndReturnHomeView("downloadCodes", model, request, response);
    }

    private String setModelAttributeAndReturnHomeView(String attribute,
                                  Model model,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.FREEFUND_LARGE_GENERATE_UPLOAD)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("freefundClasses", freefundService.getFreefundClasses());
        model.addAttribute("page", attribute);
        return ViewConstants.FREEFUND_LARGE_GENERATE_AND_UPLOAD_ADMIN_HOME_VIEW;
    }

    /**
     *
     * @param mapping
     * @param model
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "home/uploadcodes", method = RequestMethod.GET)
    public String uploadCodes(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                              HttpServletResponse response) throws IOException {
        return setModelAttributeAndReturnHomeView("uploadCodes", model, request, response);
    }

    /**
     *
     * @param mapping
     * @param classId
     * @param request
     * @return Generate codes for a freefund class. Stores codes temporarily in files across the admin server.
     *   For X number of codes and a Y dividing factor, X/Y + ((X % Y) : 1 : 0) files are created.
     *   Entries are made in mongo for a scheduler job to pick these files. An email is also sent.
     */
    @RequestMapping(value = "generate")
    public String generateFreefundCode(@RequestParam Map<String, String> mapping,@RequestParam(required = true,
            value = "class_id") Long classId, HttpServletRequest request) {
        long startTime = System.currentTimeMillis();
        try {
            String prefixParamvalue = mapping.get(FCConstants.PREFIX_PARAM_NAME).trim();
            String numberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
            String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();
            int codeLength = (int)Double.parseDouble(lengthParamValue);
            int fileDividingFactor =  Integer.parseInt(fcProperties.getProperty(FCConstants.FILE_DIVIDING_FACTOR));
            String curUserEmail = getCurrentAdminUser(request);

            int numberOfCodes = Integer.parseInt(numberOfCodesParamValue);
            int numberOfFiles = numberOfCodes/fileDividingFactor;
            boolean evenCodes = true;
            if (numberOfCodes % fileDividingFactor > 0) {
                numberOfFiles++;
                evenCodes = false;
            }

            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            randomCodeGenerator.setPrefix(prefixParamvalue);
            randomCodeGenerator.setNoOfCodes(numberOfCodes);
            randomCodeGenerator.setLength(codeLength);
            randomCodeGenerator.setType(RandomCodeGenerator.TYPE.ALPHANUMERIC);
            randomCodeGenerator.setCustomChar(true);

            String codeDirectory =  fcProperties.getProperty(FCConstants.GENERATE_CODES_DIRECTORY);
            logger.info("Codes to be generated at : " + codeDirectory + " for " + classId + " generated by " +
                    curUserEmail + ". Number of file created: " + numberOfFiles);
            Calendar calendar = Calendar.getInstance();
            long time = calendar.getTimeInMillis();
            for (int fileCount = 0; fileCount < numberOfFiles; fileCount++) {
                String fileName = getFileName(prefixParamvalue,
                                              codeLength,
                                              fileCount + 1,
                                              time);

                String filePath = codeDirectory + "/" + fileName;

                logger.debug("Codes to be put at : " + filePath + " for " + classId);
                File file = freefundLargeGenerateUploadAdminHelper.writeCodesToFile(randomCodeGenerator,
                        filePath,
                        fileCount,
                        numberOfFiles,
                        fileDividingFactor,
                        evenCodes,
                        numberOfCodes);

                amazons3Service.uploadRegularFileToS3(file, FCConstants.AWS_FREEFUND_BUCKET);
            }

            boolean mongoInsertStatus = freefundTemporaryFilesStatusService.insert(classId,
                    numberOfFiles,
                    numberOfCodes,
                    prefixParamvalue,
                    codeLength,
                    time,
                    PromocodeConstants.STATUS_PENDING,
                    curUserEmail);

            if (!mongoInsertStatus) {
                logger.error("Couldnt insert job into mongodb :" + classId +" " + numberOfFiles + " " + time);
                String messages = "Sorry, couldnt create codes! We failed to insert job into mongo. ";
                return String.format("redirect:%s?message=%s", FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK , messages);
            }
        } catch (RuntimeException e) {
            return setErrorMessageAndReturnToUploadPage(e.getMessage());
        } catch (IOException e) {
            return setErrorMessageAndReturnToUploadPage(e.getMessage());
        }
        long executionTime = System.currentTimeMillis() - startTime;
        metricsClient.recordLatency("FreefundCodeGeneration", classId + ".ExecutionTime", executionTime);
        String messages = "Done creating codes. They're ready for you to download now.";
        return String.format("redirect:%s?message=%s", FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK, messages);
    }

    /**
     *
     * @param prefixParamvalue
     * @param codeLength
     * @param fileCount
     * @param time
     * @return file name
     */
    public static String getFileName(String prefixParamvalue, int codeLength, int fileCount, long time) {
        return  prefixParamvalue + "_" + codeLength + "_" + fileCount + "_" + time + ".csv";
    }

    /**
     *
     * @param freefundClassId
     * @param status
     * @return All generated codes data in a map
     */
    @RequestMapping(value = "getData", method = RequestMethod.GET)
    public @ResponseBody
    Map getDataForFreefundClass(@RequestParam(required = true,
            value = "freefundClassId") Long freefundClassId, @RequestParam(required = false, value = "status") String status) {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, String>> freefundTemporaryFilesStatusServiceData = null;
        if(FCUtil.isEmpty(status)) {
            freefundTemporaryFilesStatusServiceData =  freefundTemporaryFilesStatusService.
                    getDataByCampaignId(freefundClassId);
        } else {
            freefundTemporaryFilesStatusServiceData = freefundTemporaryFilesStatusService.
                    getDataByCampaignIdAndStatus(freefundClassId, status);
        }
        result.put("message", PromocodeConstants.SUCCESS);
        result.put("data", freefundTemporaryFilesStatusServiceData);
        return result;
    }

    /**
     *
     * @param request
     * @param response
     * @throws FileNotFoundException
     * @return Returns a .zip file(embedded into response objject) containing all the files created in /generate
     */
    @RequestMapping(value = "getCodes/*", method = RequestMethod.GET)
    public @ResponseBody
    void downloadFiles(HttpServletRequest request,
                       HttpServletResponse response) throws FileNotFoundException {
        long startTime = System.currentTimeMillis();
        String prefix, classId = null;
        int codeLength, numberOfFiles = 0;
        long time = 0;
        try {
            String requestUri = request.getRequestURI();
            String[] requestUriParts = requestUri.split("/");
            long classid = 0;
            if (requestUriParts != null && requestUriParts.length == 6) {
                classId = requestUriParts[5];
                classid = Long.parseLong(classId);
            }
            prefix = request.getParameter("prefix");
            codeLength = Integer.parseInt(request.getParameter("codeLength"));
            numberOfFiles = Integer.parseInt(request.getParameter("numberOfFiles"));
            time = Long.parseLong(request.getParameter("timestamp"));
            String zipFileName = "freefundCodes" + freefundService.getFreefundClass(classid).getName() + ".zip";
            response.reset();
            response.setContentType("application/zip");
            response.setHeader("Content-disposition", "attachment; filename=" + zipFileName);

            String fileName = null;

            String codeDirectory =  fcProperties.getProperty(FCConstants.GENERATE_CODES_DIRECTORY);
            FileOutputStream fos = new FileOutputStream(codeDirectory + "/" + zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (int fileCount = 0; fileCount < numberOfFiles; fileCount++) {
                fileName = getFileName(prefix, codeLength, fileCount + 1, time);
                logger.debug("Codes to be fetched for " + classid + " created at : " + time);
                freefundLargeGenerateUploadAdminHelper.writeCodesToZipFileAndAttachToResponse(response, fileName, zos);
            }
            zos.closeEntry();
            zos.close();
            FileInputStream is = new FileInputStream(codeDirectory + "/" + zipFileName);
            FileCopyUtils.copy(is, response.getOutputStream());
        }catch (IOException e) {
            logger.error("Failed to add an entry to zip file list ", e);
        } catch (Exception e) {
            logger.error("Failed to create zip entry for request: " + classId + " created at: " + time, e);
        }
        long executionTime = System.currentTimeMillis() - startTime;
        metricsClient.recordLatency("FreefundCodeDownload", classId + ".ExecutionTime", executionTime);
    }

    /**
     *
     * @param mapping
     * @param model
     * @param request
     * @param response
     * @return Validates length generated codes against number of codes generated and suggests ideal length.
     */
    @RequestMapping(value = "generate/validate", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> generateFreefundValidate(@RequestParam Map<String, String> mapping, Model model,
                                                 HttpServletRequest request, HttpServletResponse response) {
        String nuberOfCodesParamValue = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        String lengthParamValue = mapping.get(FCConstants.LENGTH_PARAM_NAME).trim();

        Integer givenCodeLength = 0;
        try {
            givenCodeLength = Integer.parseInt(lengthParamValue);
        } catch (Exception e) {
            logger.error("Error parsing code length param value " + lengthParamValue + " for codes of number " +
                    nuberOfCodesParamValue);
        }
        Integer numberOfCodes = 0;
        try {
            numberOfCodes = Integer.parseInt(nuberOfCodesParamValue);
        } catch (Exception e) {
            logger.error("Error parsing number of codes param value " + nuberOfCodesParamValue + " for codes of length "
                    + lengthParamValue);
        }

        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        Double desiredCodeLength = randomCodeGenerator.desiredCodeLengthFor(numberOfCodes);

        Map<String, String> responseMap = new HashMap<String, String>();
        if (givenCodeLength >= desiredCodeLength) {
            responseMap.put("status", "success");
        } else {
            responseMap.put("status", "fail");
        }
        String message = "The advisable code length should be " + desiredCodeLength.toString()
                + " to avoid generating duplicate codes [ For a duplication reduction factor of "
                + Math.pow(10, -RandomCodeGenerator.DUPLICATION_REDUCTION_FACTOR) + " ]";

        responseMap.put("message", message);
        return responseMap;
    }

    /**
     *
     * @param mapping
     * @param model
     * @param request
     * @param response
     * @return ideal length of codes generated for a given number of codes.
     */
    @RequestMapping(value = "getLengthForCodes", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, String> getDesiredLengthForCodes(@RequestParam Map<String, String> mapping, Model model,
                                                 HttpServletRequest request, HttpServletResponse response) {
        String numberOfCodes = mapping.get(FCConstants.NUMBER_OF_CODES_PARAM_NAME).trim();
        Map<String, String> responseMap = new HashMap<String, String>();
        if(FCUtil.isEmpty(numberOfCodes) && numberOfCodes.matches("\\d+")){
            RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
            Double desiredCodeLength = randomCodeGenerator.desiredCodeLengthFor(Integer.parseInt(numberOfCodes));
            responseMap.put("status", "success");
            responseMap.put("length", desiredCodeLength + "");
            return responseMap;
        }

        responseMap.put("status", "fail");
        responseMap.put("message", "Enter valid number of codes");
        return responseMap;
    }

    /**
     *
     * @param request
     * @param model
     * @return uploads codes for a particular generated amount of codes using upload codes service..
     */
    @RequestMapping(value="/runjob", method = RequestMethod.GET)
    @NoLogin
    @Csrf(exclude = true)
    public String runJob(HttpServletRequest request, Model model) {
        String prefix = null, emailId = null;
        int codeLength = 0, numberOfFiles = 0;
        long time = 0, classId = 0;
        String messages = "";
        String fileCount = null;
        try {
            prefix = request.getParameter("prefix");
            codeLength = Integer.parseInt(request.getParameter("codeLength"));
            numberOfFiles = Integer.parseInt(request.getParameter("numberOfFiles"));
            time = Long.parseLong(request.getParameter("timestamp"));
            classId = Long.parseLong(request.getParameter("classId"));
            emailId = request.getParameter("emailId");
            fileCount = request.getParameter("fileCount");
            uploadCodesService.uploadCodes(prefix, codeLength, numberOfFiles, time, classId, emailId, fileCount);
            messages = "Please check your mail for details of duplicate codes a`nd successfully uploaded codes.";
        } catch (Exception e) {
            logger.error("Error uploading codes into mongo ", e);
            messages = "Error in uploading codes for email : " +  emailId + " The error message" +
                    " we received was : " + e.getMessage();
        }
        return String.format("redirect:%s?message=%s", FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK , messages);
    }

    public String setErrorMessageAndReturnToUploadPage(String errorMessage) {
        logger.error("Exception:" +errorMessage);
        String messages = "Sorry, couldnt create codes! " + errorMessage;
        return String.format("redirect:%s?message=%s", FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK, messages);
    }
}
