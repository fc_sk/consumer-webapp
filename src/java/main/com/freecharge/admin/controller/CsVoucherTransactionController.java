package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.user.OneCheckTransactionSummary;
import com.freecharge.rest.user.VoucherDetails;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.service.OneCheckUserService;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/cs/onecheck/voucher/transaction/*")
public class CsVoucherTransactionController extends BaseAdminController {
	private static Logger logger = LoggingFactory.getLogger(CsVoucherTransactionController.class);
	@Autowired
    private OneCheckUserService   oneCheckUserService;

	@Autowired
    private OneCheckWalletService oneCheckWalletService;

	@RequestMapping(value = "details", method = RequestMethod.GET)
    public final String getVoucherData(@RequestParam final Map<String, String> mapping, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {		
		String view = "admin/customertrail/onecheck/voucher/transaction/view";
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			return get403HtmlResponse(response);
		}
		if (FCUtil.isEmpty(mapping.get("email")) || !FCUtil.isEmailIdFormat(mapping.get("email"))) {
			model.addAttribute("messages", "Enter a valid email Id. ");
			return view;
		}		
		String emailId = mapping.get("email").trim();
		List<VoucherDetails> voucherDetailsList = null;
		List<Map<String, Object>> voucherMapList = new ArrayList<>();
		try {
			String onecheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			logger.info("Voucher data email " + emailId + ", onecheckId "+ onecheckId);
			model.addAttribute("onecheckid", onecheckId);
			voucherDetailsList = oneCheckUserService.getUsersVouchers(onecheckId);
		} catch (Exception ex) {
			logger.error("Exception while fetching user voucher detail for " + emailId, ex);
			model.addAttribute("messages", "Exception while accessing voucher. ");
			model.addAttribute("voucherMapList", voucherMapList);
			return view;
		}
		if (FCUtil.isEmpty(voucherDetailsList)) {
			model.addAttribute("messages", "No voucher details for user. ");
			model.addAttribute("voucherMapList", voucherMapList);
			return view;
		}
        
		logger.info("Size of the voucher data list for email : "+ emailId + " is " + voucherDetailsList.size());
		for (VoucherDetails voucherDetails : voucherDetailsList) {
			Map<String, Object> voucherMap = new HashMap<>();
			voucherMap.put("voucherid", voucherDetails.getVoucherId());
			voucherMap.put("businessentity", voucherDetails.getBusinessEntity());
			voucherMap.put("voucheramount", voucherDetails.getVoucherAmount());
			voucherMap.put("status", voucherDetails.getVoucherStatus());
			voucherMap.put("createdon", voucherDetails.getVoucherCreatedOn());
			voucherMap.put("voucherbalance", voucherDetails.getVoucherBalance());
			voucherMap.put("expirydate", voucherDetails.getVoucherExpiryDate());
			voucherMap.put("orderids", getOrderIds(voucherDetails.getVoucherId()));
			voucherMapList.add(voucherMap);
		}
		model.addAttribute("voucherMapList", voucherMapList);
	return view;
	}

	/*Get expired voucher list*/
	@RequestMapping(value = "expired/details", method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>> getExpiredVoucher(HttpServletRequest request, HttpServletResponse response, Model model){
		List<Map<String, Object>> expiredVoucherMapList = new ArrayList<>();
		String onecheckid = request.getParameter("onecheckid");
		if (FCUtil.isEmpty(onecheckid)) {
			model.addAttribute("messages", "Empty parameters");
			return expiredVoucherMapList;
		}
		List<VoucherDetails> expiredVoucherList = null;
		try{
			expiredVoucherList = oneCheckUserService.getVouchersExpiredForUser(onecheckid);
		} catch (Exception e){
            logger.error("Exception occured while fetching expired vouchers for onecheckid "+ onecheckid, e);
            model.addAttribute("messages", "Exception throwing while fetching expired vouchers.");
            return expiredVoucherMapList;
        }
		
		if (FCUtil.isEmpty(expiredVoucherList)) {
			model.addAttribute("messages", "No expired voucher details for user. ");
			return expiredVoucherMapList;
		}
		
		logger.info("Size of the voucher data list for onecheckid : "+ onecheckid + " is " + expiredVoucherList.size());
		for (VoucherDetails voucherDetails : expiredVoucherList) {
			Map<String, Object> expiredVoucherMap = new HashMap<>();
			expiredVoucherMap.put("voucherid", voucherDetails.getVoucherId());
			expiredVoucherMap.put("businessentity", voucherDetails.getBusinessEntity());
			expiredVoucherMap.put("voucheramount", voucherDetails.getVoucherAmount());
			expiredVoucherMap.put("status", voucherDetails.getVoucherStatus());
			expiredVoucherMap.put("createdon", voucherDetails.getVoucherCreatedOn());
			expiredVoucherMap.put("voucherbalance", voucherDetails.getVoucherBalance());
			expiredVoucherMap.put("expirydate", voucherDetails.getVoucherExpiryDate());
			expiredVoucherMap.put("orderids", getOrderIds(voucherDetails.getVoucherId()));
			expiredVoucherMapList.add(expiredVoucherMap);
		}
        return expiredVoucherMapList;
    }

	private List<String> getOrderIds(String voucherId) {
		List<String> orderIdList = new ArrayList<>();
		try {
			List<OneCheckTransactionSummary> oneCheckTransactionSummaryList  = oneCheckUserService.getTransactionsForVoucher(voucherId);
			if (!FCUtil.isEmpty(oneCheckTransactionSummaryList)) {
				for (OneCheckTransactionSummary oneCheckTransactionSummary : oneCheckTransactionSummaryList) {
					orderIdList.add(oneCheckTransactionSummary.getTxnId());
				}
			}
		} catch (Exception ex) {
			logger.error("Exception while fetching orderids for voucherid: "+ voucherId, ex);
		}
		return orderIdList; 
	}
}
