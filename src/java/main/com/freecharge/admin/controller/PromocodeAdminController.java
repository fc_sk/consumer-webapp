package com.freecharge.admin.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.Promocode;
import com.freecharge.app.service.PromocodeService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;

@Controller
@RequestMapping("/admin/promocode/*")
public class PromocodeAdminController extends BaseAdminController {
	private Logger logger = LoggingFactory.getLogger(getClass().getName());

	public static final String HOME_LINK = "/admin/promocode/home.htm";

	@Autowired
	private PromocodeService promocodeService;

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String viewHome(@RequestParam Map<String, String> mapping, HttpServletRequest request,
                           HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.PROMOCODE)){
            return get403HtmlResponse(response);
        }
		List<Promocode> allPromocodes = promocodeService.getAll();
		model.addAttribute(ViewConstants.ATTR_ALL_PROMOCODES, allPromocodes);
		String promoCodeId = mapping.get("promocode_id");
		if (promoCodeId != null) {
			Integer id = Integer.parseInt(promoCodeId);
			Promocode promocode = promocodeService.getPromocode(id);
			model.addAttribute(ViewConstants.ATTR_PROMOCODE, promocode);
		}
		return ViewConstants.PROMOCODE_ADMIN_HOME_VIEW;
	}

	@RequestMapping(value = "create_save", method = RequestMethod.POST)
	public String createOrSave(@RequestParam Map<String, String> mapping, HttpServletRequest request,
                               HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.PROMOCODE)){
            return get403HtmlResponse(response);
        }
		boolean status = false;
		String messages = "";
		String createOrSave = "create";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String promoCodeId = mapping.get("promocode_id").trim();
		String promoTitle = mapping.get("promo_title").trim();
		String promoCode = mapping.get("promo_code").trim();
		String failureMsg = mapping.get("failure_msg").trim();
		String successMsg = mapping.get("success_msg").trim();
		String validFrom = mapping.get("valid_from").trim();
		String validUpto = mapping.get("valid_upto").trim();
		String validFromTime = mapping.get("valid_from_time").trim();
		String validUptoTime = mapping.get("valid_upto_time").trim();

		Promocode promocode = new Promocode();
		try {
			if (promoCodeId != null) {
				Integer id = Integer.parseInt(promoCodeId);
				promocode = promocodeService.getPromocode(id);
				createOrSave = "save";
			}
			promocode.setPromoTitle(promoTitle);
			promocode.setPromoCode(promoCode);
			promocode.setFailureMsg(failureMsg);
			promocode.setSuccessMsg(successMsg);
			try {
				Date date = sdf.parse(validFrom + " " + validFromTime.trim());
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(date);
				promocode.setValidFrom(cal1.getTime());
			} catch (Exception e) {
				messages += "<br/> valid from should be a date in format YYYY-MM-DD";
				throw e;
			}
			try {
				Date date = sdf.parse(validUpto + " " + validUptoTime.trim());
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(date);
				promocode.setValidUpto(cal2.getTime());
			} catch (Exception e) {
				messages += "<br/> valid upto should be a date in format YYYY-MM-DD";
				throw e;
			}
			List<Promocode> conflictingPromos = promocodeService.getConflictingPromos(promocode);
			if (conflictingPromos != null && conflictingPromos.size() > 0) {
				Promocode promocode2 = conflictingPromos.get(0);
				messages += "A promo campaign with Promocode id <b>"
						+ promocode2.getPromocodeId()
							+ "</b>is already active in this period.";
			} else {
				try {
					status = promocodeService.createOrUpdate(promocode, createOrSave);
					if (status)
						messages = "Promocode created successfully";
				} catch (Exception e) {
					messages += "<br/>Try again later";
					throw e;
				}
			}
		} catch (Exception e) {
			logger.error("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later";
		}
		return String.format("redirect:%s?message=%s", HOME_LINK, messages) ;

	}

}
