package com.freecharge.admin.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.UtmShortCode;
import com.freecharge.app.domain.entity.jdbc.UtmTracker;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.UtmRedirectionTrackingService;
import com.freecharge.freefund.services.UtmShortCodeService;
import com.freecharge.freefund.services.UtmTrackerService;
import com.freecharge.web.util.ViewConstants;

@Service
@Controller
@RequestMapping("/admin/utmtracker/*")
public class UtmTrackerAdminController extends BaseAdminController {

    public static final String FC_MASTER                 = "fc_master";
    public static final String UTM_CAMPAIGN_NAME         = "fc_camp";
    public static final String UTM_MEDIUM                = "fc_med";
    public static final String UTM_SOURCE                = "fc_src";
    public static final String UTM_KEYWORD               = "fc_key";
    public static final String UTM_URL                   = "fc_url";
    public static final String UTM_CONTENT               = "fc_cont";
    public static final String FORWARD_URL               = "forward_url";
    public static final String UTM_CAMPAIGN_ID           = "fc_camp_id";
    public static final String UTM_SHORT_CODE            = "utm_short_code";
    public static final String AUTO_DETECT_USER_PLATFORM = "autoDetect";
    public static final String WEB_URL                   = "webUrl";
    public static final String MOBILE_WEB_URL            = "mobileWebUrl";
    public static final String ANDROID_APP_URL           = "androidAppUrl";
    public static final String IOS_APP_URL               = "iosAppUrl";
    public static final String WINDOWS_APP_URL           = "windowsAppUrl";
    public static final String START_DATE                = "start_date";
    public static final String END_DATE                  = "end_date";
    public static final String UTM_SHORT_CODE_ID        = "utm_shortcode_id";

    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    public static final String UTM_TRACKER_HOME_VIEW = "/admin/utmtracker/home.htm";
    private static final String UTM_TRACKER_EDIT_LINK = "/admin/utmtracker/home/edit.htm";

    @Autowired
    private UtmTrackerService utmTrackerService;

    @Autowired
    private UtmShortCodeService utmShortCodeService;

    @Autowired
    private UtmRedirectionTrackingService utmRedirectionTrackingService;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String viewHome(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "home");
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "home/create", method = RequestMethod.GET)
    public String createCampaign(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                              HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "create");
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String createTrackerCampaign (@RequestParam Map<String, String> mapping, Model model,
                                         HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        String curUserEmail = getCurrentAdminUser(request);
        
        String fcMaster = mapping.get(FC_MASTER);
        String campaignName = mapping.get(UTM_CAMPAIGN_NAME);
        List<UtmTracker> utmTrackers = utmTrackerService.getUtmCampaignForMaster(fcMaster, campaignName);
        String medium = mapping.get(UTM_MEDIUM);
        String source = mapping.get(UTM_SOURCE);
        String keyword = mapping.get(UTM_KEYWORD);
        String url = mapping.get(UTM_URL);
        String content = mapping.get(UTM_CONTENT);
        String forwardUrl = mapping.get(FORWARD_URL);
        logger.info("Fc master " + fcMaster + " utm campaign name" + campaignName + " utm medium " + medium
        + " utm source " + source);

        if (FCUtil.isEmpty(fcMaster) || FCUtil.isEmpty(campaignName)) {
            return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Invalid input. " +
                    "Check mandatory fields");
        } else if (!FCUtil.isEmpty(utmTrackers)) {
            return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Campaign name " + campaignName +
            " already exists for fc master " + fcMaster);
        }

        int utmCampaignId = utmTrackerService.createCampaign(fcMaster, campaignName, medium, source, keyword, url,
                content, forwardUrl, curUserEmail);
        model.addAttribute("campaignId", utmCampaignId);
        model.addAttribute("page", "edit");
        return String.format("redirect:%s?", UTM_TRACKER_EDIT_LINK);
    }

    @RequestMapping(value = "home/edit", method = RequestMethod.GET)
    public String getEditCampaign (@RequestParam Map<String, String> mapping, Model model,
                                         HttpServletRequest request, HttpServletResponse response) throws IOException {
        String scampaignId = mapping.get("campaignId");
        int campaignId = 0;
        if (!FCUtil.isEmpty(scampaignId) && FCUtil.isInteger(scampaignId)) {
            campaignId = Integer.parseInt(scampaignId);
        } else {
            return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Invalid input for editclass " +
                    "Check mandatory fields, . Couldnt find passed id " + scampaignId);
        }

        List<UtmTracker> utmTrackerList = utmTrackerService.getUtmCampaign("utm_campaign_id", String.valueOf(campaignId));

        if (!FCUtil.isEmpty(utmTrackerList)) {
            UtmTracker utmTracker = FCUtil.getFirstElement(utmTrackerList);
            model.addAttribute("utmTracker", utmTracker);
            model.addAttribute("page", "edit");
            model.addAttribute("campaignId", utmTracker.getCampaignId());
            return ViewConstants.UTM_TRACKER_HOME_VIEW;
        } else {
            return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Invalid input for editclass " +
                    "Check mandatory fields, . Couldnt find data for passed id " + scampaignId);
        }
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String edit (@RequestParam Map<String, String> mapping, Model model,
                        HttpServletRequest request, HttpServletResponse response) throws IOException {
        AdminComponent component = AdminComponent.UTM_TRACKER;
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        String curUserEmail = getCurrentAdminUser(request);
        
        String campaignId = mapping.get(UTM_CAMPAIGN_ID);
        String fcMaster = mapping.get(FC_MASTER);
        String campaignName = mapping.get(UTM_CAMPAIGN_NAME);
        String medium = mapping.get(UTM_MEDIUM).trim();
        String source = mapping.get(UTM_SOURCE);
        String keyword = mapping.get(UTM_KEYWORD);
        String url = mapping.get(UTM_URL);
        String content = mapping.get(UTM_CONTENT);
        String forwardUrl = mapping.get(FORWARD_URL);

        List<UtmTracker> utmTrackers = utmTrackerService.getUtmCampaignForMaster(fcMaster, campaignName);
        if (FCUtil.isEmpty(fcMaster) || FCUtil.isEmpty(campaignName)) {
            return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Invalid input. " +
                    "Check mandatory fields");
        } else if (!FCUtil.isEmpty(utmTrackers)) {
            UtmTracker utmTracker = FCUtil.getFirstElement(utmTrackers);
            if (Integer.parseInt(campaignId) != utmTracker.getCampaignId()) {
                return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Campaign name " + campaignName +
                        " already exists for fc master " + fcMaster);
            }
        }

        utmTrackerService.editCampaign(Integer.parseInt(campaignId), fcMaster, campaignName, medium, source, keyword, url,
                content, forwardUrl, curUserEmail);

        return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Updated campaign successfully.");
    }

    @RequestMapping(value = "home/search", method = RequestMethod.GET)
    public String searchCampaign(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        model.addAttribute("page", "search");
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String search(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                 HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        String key = mapping.get("searchKey");

        if (!FCUtil.isEmpty(key)) {
            String value = mapping.get("searchParam");
            List<UtmTracker> utmTrackerList = utmTrackerService.getUtmCampaign(utmTrackerService.getDBColumnNameForKey(key), value);
            if (!FCUtil.isEmpty(utmTrackerList)) {
                model.addAttribute("utmTrackerList", utmTrackerList);
                model.addAttribute("page", "search");
                return ViewConstants.UTM_TRACKER_HOME_VIEW;
            } else {
                model.addAttribute("page", "search");
                model.addAttribute("message", "No results found for current search");
                return ViewConstants.UTM_TRACKER_HOME_VIEW;
            }
        }

        model.addAttribute("page", "search");
        model.addAttribute("message", "No results found for current search");
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "home/createShortcode", method = RequestMethod.GET)
    public String createKeywordPage(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }

        List<UtmTracker> utmTrackerList = utmTrackerService.getAllCampaigns();
        model.addAttribute("utmTrackerCampaignList", utmTrackerList);
        model.addAttribute("page", "createShortcode");
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }
    @RequestMapping(value = "createShortcode", method = RequestMethod.POST)
    public String createKeyword(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        String curUserEmail = getCurrentAdminUser(request);
        
        String shortCode = mapping.get(UTM_SHORT_CODE);
        String startDate = mapping.get(START_DATE);
        String endDate = mapping.get(END_DATE);
        String autoChecked = mapping.get(AUTO_DETECT_USER_PLATFORM);

        if (!FCUtil.isEmpty(shortCode)) {
            List<UtmShortCode> shortCodeDetais = utmShortCodeService.getShortCodeDetails(shortCode);
            if (FCUtil.isEmpty(shortCodeDetais)) {
                String webUrl = null, mobileWebUrl = null, androidAppUrl = null, iosAppUrl = null, windowsAppUrl = null;
                if ("on".equals(autoChecked)) {
                    webUrl = mapping.get(WEB_URL);
                    mobileWebUrl = mapping.get(MOBILE_WEB_URL);
                    androidAppUrl = mapping.get(ANDROID_APP_URL);
                    iosAppUrl = mapping.get(IOS_APP_URL);
                    windowsAppUrl = mapping.get(WINDOWS_APP_URL);
                }

                utmShortCodeService.createShortCode(shortCode, 0, webUrl,
                        mobileWebUrl, androidAppUrl, iosAppUrl, windowsAppUrl, new Date(startDate), new Date(endDate), curUserEmail);

               return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Updated short code details " +
                       "successfully.");
            } else {
                return String.format("redirect:%s?message=%s", ViewConstants.UTM_TRACKER_CREATE_KEYWORD_VIEW,
                        "Short code " + shortCode + " already exists in the system. Please try with another");
            }
        }
        return String.format("redirect:%s?message=%s", ViewConstants.UTM_TRACKER_CREATE_KEYWORD_VIEW, "Invalid" +
                "data passed. Short code and campaignId are mandatory.");
    }

    @RequestMapping(value = "home/editShortCode", method = RequestMethod.GET)
    public String editShortCode(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        String shortCode = mapping.get(UTM_SHORT_CODE);
        List<UtmShortCode> utmShortCodes = utmShortCodeService.getShortCodeDetails(shortCode);

        if (!FCUtil.isEmpty(utmShortCodes)) {
            UtmShortCode utmShortCode = FCUtil.getFirstElement(utmShortCodes);
            model.addAttribute("utmShortCode", utmShortCode);
            model.addAttribute("page", "editShortCode");
            model.addAttribute("shortCodeId", utmShortCode.getShortCodeId());
            List<UtmTracker> utmTrackerList = utmTrackerService.getAllCampaigns();
            model.addAttribute("utmTrackerCampaignList", utmTrackerList);
        }
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "/editShortCode", method = RequestMethod.POST)
    public String editShortCodeDetails(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        String curUserEmail = getCurrentAdminUser(request);
        
        String shortCodeId = mapping.get(UTM_SHORT_CODE_ID);
        String shortCode = mapping.get(UTM_SHORT_CODE);
        String startDate = mapping.get(START_DATE);
        String endDate = mapping.get(END_DATE);
        String autoChecked = mapping.get(AUTO_DETECT_USER_PLATFORM);

        if (!FCUtil.isEmpty(shortCodeId)) {
            List<UtmShortCode> shortCodeDetais = utmShortCodeService.getShortCodeDetails(shortCode);
            if (!FCUtil.isEmpty(shortCodeDetais)) {
                String webUrl = null, mobileWebUrl = null, androidAppUrl = null, iosAppUrl = null, windowsAppUrl = null;
                if ("on".equals(autoChecked)) {
                    webUrl = mapping.get(WEB_URL);
                    mobileWebUrl = mapping.get(MOBILE_WEB_URL);
                    androidAppUrl = mapping.get(ANDROID_APP_URL);
                    iosAppUrl = mapping.get(IOS_APP_URL);
                    windowsAppUrl = mapping.get(WINDOWS_APP_URL);
                }

                utmShortCodeService.editShortCode(Integer.parseInt(shortCodeId), shortCode, 0, webUrl,
                        mobileWebUrl, androidAppUrl, iosAppUrl, windowsAppUrl, new Date(startDate), new Date(endDate), curUserEmail);

                return String.format("redirect:%s?message=%s", UTM_TRACKER_HOME_VIEW, "Updated short code details " +
                        "successfully.");
            } else {
                return String.format("redirect:%s?message=%s", ViewConstants.UTM_TRACKER_CREATE_KEYWORD_VIEW,
                        "Short code " + shortCode + " doesnt exist in the system. Please try with another");
            }
        }
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "home/searchShortCode", method = RequestMethod.GET)
    public String searchForm(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                   HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.UTM_TRACKER)) {
            return get403HtmlResponse(response);
        }
        List<UtmShortCode> utmShortCodeList = utmShortCodeService.getAllShortCodeDetails();
        model.addAttribute("page", "searchShortCode");
        model.addAttribute("utmShortCodeList", utmShortCodeList);
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "home/downloadData", method = RequestMethod.GET)
    public String downloadDataForm(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                       HttpServletResponse response) throws IOException {
        List<UtmShortCode> utmShortCodeList = utmShortCodeService.getAllShortCodeDetails();
        model.addAttribute("page", "downloadData");
        model.addAttribute("utmShortCodeList", utmShortCodeList);
        return ViewConstants.UTM_TRACKER_HOME_VIEW;
    }

    @RequestMapping(value = "/downloadData", method = RequestMethod.POST)
    public void downloadShortCodeData(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
                                        HttpServletResponse response) throws IOException {
        String shortCode = mapping.get(UTM_SHORT_CODE);
        if (!FCUtil.isEmpty(shortCode)) {
            response.reset();
            response.setContentType("text/csv");
            response.setHeader("Content-disposition", "attachment; filename=" + shortCode + ".csv");
            List<Map<String, Object>> utmRedirectionTrackingDetailsList = utmRedirectionTrackingService
                    .getShortCodeData(shortCode);

            response.getWriter().write("Date," + "To Web," + "To mobile web," + "To Android," +
                    "To iTunes," + "To Windows," + "To Null," + "Total" + "\n");
            for (Map<String, Object> utmRedirectionTrackingDetails : utmRedirectionTrackingDetailsList) {
                response.getWriter().write(utmRedirectionTrackingDetails.get("date") + ","
                        + utmRedirectionTrackingDetails.get("web")+ ","
                        + utmRedirectionTrackingDetails.get("mobileWeb") + ","
                        + utmRedirectionTrackingDetails.get("android") + ","
                        + utmRedirectionTrackingDetails.get("ios") + ","
                        + utmRedirectionTrackingDetails.get("windows") + ","
                        + utmRedirectionTrackingDetails.get("nulls") + ","
                        + utmRedirectionTrackingDetails.get("total") + "\n");
            }
        }
        response.getWriter().flush();
        response.getWriter().close();
    }
}
