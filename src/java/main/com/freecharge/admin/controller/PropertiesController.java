package com.freecharge.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.properties.FCProperties;

@Controller
@RequestMapping("/admin/props")
public class PropertiesController {

	public static final String RUNTIME_PROPS_URL = "/admin/props.htm";

	@Autowired
	FCProperties fcProps;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public void display(HttpServletResponse response) throws IOException {
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("text/html");
		response.getWriter().write("<pre>");
		fcProps.getProperties().list(response.getWriter());
		response.getWriter().write("</pre>");
		response.getWriter().flush();
	}

}
