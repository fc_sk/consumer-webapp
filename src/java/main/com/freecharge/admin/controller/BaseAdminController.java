package com.freecharge.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.service.AccessControlService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/23/13
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseAdminController {

    @Autowired
    protected AccessControlService accessControlService;

    @Autowired
    protected FCProperties fcProperties;

    private String getErrorResponse(HttpServletResponse response, Integer status, String contentType, String message)
            throws IOException{
        response.setStatus(status);
        response.setContentType(contentType);
        PrintWriter pw = response.getWriter();
        pw.write(message);
        pw.flush();
        return null;
    }

    protected String get403Response(HttpServletResponse response, String contentType, String message) throws IOException {
        return this.getErrorResponse(response, HttpServletResponse.SC_FORBIDDEN, contentType, message);
    }

    protected String get403HtmlResponse(HttpServletResponse response) throws IOException{
        return this.get403Response(response, "text/html", "Permission Denied. Contact - "+
                this.fcProperties.getAdminRootEmails());
    }

    protected String get403JsonResponse(HttpServletResponse response) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("status", "error");
        obj.put("message", "Permission Denied. Contact - "+this.fcProperties.getAdminRootEmails());
        return this.get403Response(response, "application/json", obj.toJSONString());
    }

    protected Boolean hasAccess(String userEmail, AdminComponent component) {
        Boolean access =  this.accessControlService.hasAccess(userEmail, component);
        logger.info("access_check_current_user:"+userEmail+" admin_component: "+component+" access: "+access);
        return access;
    }

    protected Boolean hasAccess(HttpServletRequest request, AdminComponent component) {
    	String curUserEmail = null;
    	FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
   	 	Map<String, Object> map = freechargeSession.getSessionData();
   	 	if (map != null) {
   	 		curUserEmail =	(String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL); 
   	 	}
    	return !FCStringUtils.isBlank(curUserEmail) && hasAccess(curUserEmail, component);
    }

    protected String getCurrentAdminUser(HttpServletRequest request) {
    	String curUserEmail = null;
    	FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
   	 	Map<String, Object> map = freechargeSession.getSessionData();
   	 	if (map != null) {
   	 		curUserEmail =	(String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL); 
   	 	}
    	return curUserEmail;
    }

    protected Boolean isAuthorizedToUpdateCt(HttpServletRequest request) {
    	String email = getCurrentAdminUser(request);
    	if (FCUtil.isEmpty(email)) {
    		return false;
    	}
    	return accessControlService.isAuthorizedToUpdateCt(email);
    }
    
    
    private static final Logger logger = LoggingFactory.getLogger(BaseAdminController.class);
}
