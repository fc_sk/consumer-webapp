package com.freecharge.admin.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.admin.CustomerTrailDetails.UPITransactionDetailView;
import com.freecharge.admin.CustomerTrailDetails.UPITransactionView;
import com.freecharge.common.client.UserDetailsResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.upi.error.codes.UpiNpciErrorCodes;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.CashbackTxnInfo;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.TxnHistory;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.UPIInfo;
import com.snapdeal.ims.dto.UserDetailsDTO;

@Service
public class UPITransactionService {

	private static Logger LOGGER = LoggingFactory.getLogger(UPITransactionService.class);

	private static final String UPI_PAY = "UPI_PAY";
	
	private static final String UPI_COLLECT = "UPI_COLLECT";
	
	private static final String UPI_ERRORCODE_PREFIX = "UPI_NPCI_";
	
	@Autowired
	private UserServiceProxy userService;

	@Autowired
	private TxnHistoryViewService txnHistoryViewService;

	@Autowired
	private UpiServiceWrapper upiServiceWrapper;

	private List<UPITransactionView> getUPITransactionViewList(List<TxnHistory> txnHistoryList, String mobileNo) {
		List<UPITransactionView> upiTransactionsList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(txnHistoryList)) {
			for (TxnHistory txnHistory : txnHistoryList) {
				UPITransactionView upiTransactionView = new UPITransactionView.UPITransactionViewBuilder()
						.setBusinessUseCase(txnHistory.getBusinessUseCase())
						.setTransactionDate(txnHistory.getTransactionDate()).setTxnAmount(txnHistory.getTxnAmount())
						.setTxnStatus(txnHistory.getTxnStatus()).setGlobalTxnId(txnHistory.getGlobalTxnId())
						.setUpiInfo(txnHistory.getUPIInfo()).setGlobalTxnType(txnHistory.getGlobalTxnType())
						.setMerchantOrderId(txnHistory.getMerchantOrderId()).setMobileNumber(mobileNo)
						.setUpiNpciErrorMessage(getUpiNpciErrorMessage(txnHistory.getUPIInfo()))
						.setPromoCode(txnHistory.getPromoCode()).build();
				upiTransactionsList.add(upiTransactionView);
			}
		}
		return upiTransactionsList;
	}

	private String getUpiNpciErrorMessage(UPIInfo upiInfo) {
		if(upiInfo == null){
			return StringUtils.EMPTY;
		}else{
			String upiNpciErrorMessage = "";
			if (StringUtils.isNotBlank(upiInfo.getErrCode())) {
				String upiErrorCode = new StringBuilder().append(UPI_ERRORCODE_PREFIX).append(upiInfo.getErrCode().trim()).toString();
				UpiNpciErrorCodes upiNpciErrorCodes = UpiNpciErrorCodes.forName(upiErrorCode);
				LOGGER.info(String.format("Response upiNpciErrorCodes from UpiNpciErrorCodes.forName(upiErrorCode) (UpiNpciErrorCodes=%s)",upiNpciErrorCodes));
				upiNpciErrorMessage = null != upiNpciErrorCodes ? upiNpciErrorCodes.errMsg() : "";
			}
			return upiNpciErrorMessage;
		}	
	}
	
	public List<UPITransactionView> getUPITransactionsViewDataByMobileNo(String mobileNo) {
		UserDetailsDTO userDetails = userService.getUserDetailsByMobileNo(mobileNo);
		LOGGER.info(String.format("Response userDetails from userService.getUserDetailsByMobileNo(mobileNo) (UserDetailsDTO=%s)",userDetails));
		return getUPITransactionViewList(userDetails);	
	}
		
	public List<UPITransactionView> getUPITransactionsViewDataByEmailId(String email) {
		UserDetailsDTO userDetails = userService.getUserDetailsByEmailId(email);
		LOGGER.info(String.format("Response userDetails from userService.getUserDetailsByEmailId(email) (UserDetailsDTO=%s)",userDetails));
		return getUPITransactionViewList(userDetails);
	}
	
	private List<UPITransactionView> getUPITransactionViewList(UserDetailsDTO userDetails){
		String mobileNumber = "";
		if (userDetails != null && StringUtils.isNotBlank(userDetails.getUserId())) {
			String userId = userDetails.getUserId();
			List<TxnHistory> txnHistoryList = getAllUPITransactions(userId);
			if (StringUtils.isNotBlank(userDetails.getMobileNumber())) {
				mobileNumber = userDetails.getMobileNumber();
			}
			return getUPITransactionViewList(txnHistoryList, mobileNumber);
		}
		return Collections.emptyList();
	}

	private List<TxnHistory> getAllUPITransactions(String userId) {
		List<TxnHistory> upiPayTransactionsList = txnHistoryViewService.getTransactionHistoryByTxnType(userId, UPI_PAY);
		List<TxnHistory> upiCollectTransactionsList = txnHistoryViewService.getTransactionHistoryByTxnType(userId, UPI_COLLECT);
		List<TxnHistory> allUpiTransactionsList = new ArrayList<>();
		if (null != upiPayTransactionsList) {
			for(TxnHistory upiPayTransactions : upiPayTransactionsList){
				allUpiTransactionsList.add(upiPayTransactions);
				if(upiPayTransactions.getCashbackInfo() != null){
					getCashbackTransactions(upiPayTransactions, allUpiTransactionsList, userId);
				}
			}
		}
		if (null != upiCollectTransactionsList) {
			for(TxnHistory upiCollectTransactions : upiCollectTransactionsList){
				allUpiTransactionsList.add(upiCollectTransactions);
				if(upiCollectTransactions.getCashbackInfo() != null){
					getCashbackTransactions(upiCollectTransactions, allUpiTransactionsList, userId);
				}
			}
		}
		
		if (CollectionUtils.isNotEmpty(allUpiTransactionsList)) {
			Collections.sort(allUpiTransactionsList, new Comparator<TxnHistory>() {
				@Override
				public int compare(TxnHistory o1, TxnHistory o2) {
					Date createdTime1 = o1.getCreatedTime();
					Date createdTime2 = o2.getCreatedTime();

					return createdTime1.compareTo(createdTime2);
				}
			});
		}
		return allUpiTransactionsList;
	}
	
	//to get cashback transactions
	private void getCashbackTransactions(TxnHistory upiTransactions, List<TxnHistory> allUpiTransactionsList, String userId) {
		List<CashbackTxnInfo> cashbackInfoList = upiTransactions.getCashbackInfo();
		for(CashbackTxnInfo cashbackTxnInfo : cashbackInfoList){
			LOGGER.info("getTxnByDetails for userId " +userId);
			GetTxnDetailsResponse cashbackTxnHistory = txnHistoryViewService.getTxnByDetails(userId, cashbackTxnInfo.getTxnId(), cashbackTxnInfo.getTxnType());
			if (null != cashbackTxnHistory && cashbackTxnHistory.getUserTxnDetails() != null) {
				allUpiTransactionsList.add(cashbackTxnHistory.getUserTxnDetails());
			}
		}
	}

	public UPITransactionDetailView getUpiTransactionByDetails(String mobileNo, String txnId, String txnType) {
		LOGGER.info(String.format("Request comes into UPITransactionService.getUpiTransactionByDetails(mobileNo=%s,txnId=%s,txnType=%s)", mobileNo, txnId, txnType));
		UPITransactionDetailView upiTransactionDetailView = new UPITransactionDetailView();
		UserDetailsDTO userDetails = userService.getUserDetailsByMobileNo(mobileNo);

		if (userDetails != null && StringUtils.isNotBlank(userDetails.getUserId())) {
			GetTxnDetailsResponse txnDetailsResponse = txnHistoryViewService.getTxnByDetails(userDetails.getUserId(), txnId, txnType);
			LOGGER.info(String.format("Response UPITransactionService.getUpiTransactionByDetails(GetTxnDetailsResponse=%s)",txnDetailsResponse));
			if (null != txnDetailsResponse && null != txnDetailsResponse.getUserTxnDetails()) {
				TxnHistory txnHistory = txnDetailsResponse.getUserTxnDetails();
				return new UPITransactionDetailView.UPITransactionDetailViewBuilder()
						.setGlobalTxnId(txnHistory.getGlobalTxnId()).setBusinessUseCase(txnHistory.getBusinessUseCase())
						.setGlobalTxnType(txnHistory.getGlobalTxnType())
						.setTransactionDate(txnHistory.getTransactionDate()).setTxnAmount(txnHistory.getTxnAmount())
						.setTxnStatus(txnHistory.getTxnStatus()).setUpiInfo(txnHistory.getUPIInfo())
						.setUserId(txnHistory.getUserId()).setPromoCode(txnHistory.getPromoCode())
						.setMerchantOrderId(txnHistory.getMerchantOrderId()).setUserDetails(userDetails)
						.setUpiNpciErrorMessage(getUpiNpciErrorMessage(txnHistory.getUPIInfo())).build();
			}
		}
		return upiTransactionDetailView;
	}

	public List<UPITransactionView> getUPITransactionsViewDataByVpa(String vpa) {
		String mobileNumber = "";
		if (StringUtils.isNotBlank(vpa)) {
			UserDetailsResponse userDetailsResponse = upiServiceWrapper.getUserByVpa(vpa);
			if (userDetailsResponse != null && StringUtils.isNotBlank(userDetailsResponse.getImsId())) {
				UserDetailsDTO userDetails = userService.getUserDetailsByUserId(userDetailsResponse.getImsId());
				if (userDetails != null && StringUtils.isNotBlank(userDetails.getMobileNumber())) {
					mobileNumber = userDetails.getMobileNumber();
				}
				String imsId = userDetailsResponse.getImsId();
				List<TxnHistory> txnHistoryList = getAllUPITransactions(imsId);
				return getUPITransactionViewList(txnHistoryList, mobileNumber);
			}
		}
		return Collections.emptyList();
	}
}
