package com.freecharge.admin.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mongo.repos.CustomerTrailControlRepository;
import com.mongodb.DBObject;

@Service
public class CTActionControlService {
    public static final double             MAX_CREDITS_LIMIT_TO_EDIT = 500;
    public static final int                MAX_VIEW_LIMIT_TO_EDIT    = 20;
    private static final Logger            logger                    = LoggingFactory
                                                                             .getLogger(CTActionControlService.class);

    @Autowired
    private CustomerTrailControlRepository customerTrailControlRepository;

    @Autowired
    private AdminHelper                    adminHelper;

    @Autowired
    protected FCProperties            fcProperties;
    
    @Autowired
    private EmailService              emailService;
    
    /*
     * This method checks whether adminuser(cs-guy) has sufficient credits to do
     * cashback
     */
    public boolean hasSufficientCreditToDoCashBackOrRefund(final String email, final double cashBackOrRefundAmount) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String availableCreditsLimit = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_CREDITS);
                if (adminHelper.isDouble(availableCreditsLimit)) {
                    double creditLimitAval = Double.parseDouble(availableCreditsLimit);
                    if (cashBackOrRefundAmount <= creditLimitAval) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Something went wrong while checking avail-credits of email :" + email, e);
            return false;
        }
        return false;
    }

    /* Checks adminuser(CS-guy) has crossed CT view limits */
    public boolean hasSufficientViewLimits(final String email) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String viewLimitAavString = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_VIEWS);
                if (StringUtils.isNumeric(viewLimitAavString)) {
                    int viewLimitAval = Integer.parseInt(viewLimitAavString);
                    if (viewLimitAval > 0) {
                        return true;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Something went wrong while checking avail-credits in mongo of email :" + email, ex);
            return false;
        }
        return false;
    }

    /*
     * Substract avail-credits by refund/cashback amount after successful
     * refund/cashback
     */
    public boolean deductAvailableCreditLimits(final String email, final double cashBackOrRefundAmount) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String existAvalCredits = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_CREDITS);
                if (adminHelper.isDouble(existAvalCredits)) {
                    double creditAval = Double.parseDouble(existAvalCredits);
                    double creditAvalAfter = creditAval - cashBackOrRefundAmount;
                    customerTrailControlRepository.updateAvaiableCredits(email, String.valueOf(creditAvalAfter));
                    return true;
                }
            }
        } catch (Exception ex) {
            logger.error(
                    "Failed to deduct avail-credits of email = " + email + " by amount : "
                            + String.valueOf(cashBackOrRefundAmount), ex);
            return false;
        }
        return false;
    }

    public boolean deductAValiableViewLimitsByOne(final String email) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String viewAvalExist = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_VIEWS);
                if (StringUtils.isNumeric(viewAvalExist)) {
                    int avalViewLimits = Integer.parseInt(viewAvalExist) - 1;
                    customerTrailControlRepository.updateAvailableViews(email, String.valueOf(avalViewLimits));
                    return true;
                }
            }
        } catch (Exception ex) {
            logger.error("Failed to deduct avail-views of email = " + email, ex);
            return false;
        }
        return false;
    }

    
    /*Has cs-agent reached his 90% of credit limits*/
    public boolean hasCrossedNinetyPercentOfCredits(final String email) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String availableCredits = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_CREDITS);
                String maxCreditsGiven = (String) dbObject.get(CustomerTrailControlRepository.MAX_CREDITS);
   
                if (adminHelper.isDouble(availableCredits) && adminHelper.isDouble(maxCreditsGiven)) {
                    double availCredit = Double.parseDouble(availableCredits);
                    double maxCreditGiven = Double.parseDouble(maxCreditsGiven);
                    double usedCredits = maxCreditGiven - availCredit; 
                    double ninetyPercentValueOfMaxCredits = (maxCreditGiven*90)/100; 
                    if (usedCredits >= ninetyPercentValueOfMaxCredits) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Something went wrong while checking avail-credits of email :" + email, e);
            return false;
        }
        return false;
    }
    
    /*Has cs-agent reached his 90% of view limits*/
    public boolean hasCrossedNinetyPercentOfViews(final String email) {
        try {
            DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
            if (dbObject != null) {
                String availableViews = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_VIEWS);
                String maxViewsGiven = (String) dbObject.get(CustomerTrailControlRepository.VIEW_LIMITS);
   
                if (StringUtils.isNumeric(availableViews) && StringUtils.isNumeric(maxViewsGiven)) {
                    int availableView = Integer.parseInt(availableViews);
                    int maxViewGiven = Integer.parseInt(maxViewsGiven);                    
                    int usedViews = maxViewGiven - availableView;
                    int ninetyPercentValueOfMaxViews = (int)((maxViewGiven*90)/100); 
                    if (usedViews >= ninetyPercentValueOfMaxViews) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Something went wrong while checking 90% of avail-credits of email :" + email, e);
            return false;
        }
        return false;
    }
    
    /*List for email content map- Alert email for crossing 90% of max credits*/
    public List<String> getEmailContentOfCreditAlert(String email) {
        List<String> results = new ArrayList<String>();
        DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
        try {
            if (dbObject != null) {
                String availableCredits = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_CREDITS);
                String maxCreditsGiven = (String) dbObject.get(CustomerTrailControlRepository.MAX_CREDITS);
                if (adminHelper.isDouble(availableCredits) && adminHelper.isDouble(maxCreditsGiven)) {
                    double availCredit = Double.parseDouble(availableCredits);
                    double maxCreditGiven = Double.parseDouble(maxCreditsGiven);
                    double usedCredits = maxCreditGiven - availCredit;
                    results.add("Alert : " + email + "has crossed 90% of max credits given.");
                    results.add("Max credits given : " + maxCreditGiven);
                    results.add("Used credits : " + usedCredits);
                    return results;
                }
            }
        } catch (Exception ex) {
            logger.error("Exception while getting email content list, email for crossing 90% of given max-credits ", ex);
        }
        return results;
    }
     
    public List<String> getEmailContentOfViewsAlert(String email) {
        List<String> results = new ArrayList<String>();
        DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
        try {
            if (dbObject != null) {
                String availableViews = (String) dbObject.get(CustomerTrailControlRepository.AVAILABLE_VIEWS);
                String maxViewsGiven = (String) dbObject.get(CustomerTrailControlRepository.VIEW_LIMITS);
                if (StringUtils.isNumeric(availableViews) && StringUtils.isNumeric(maxViewsGiven)) {
                    int availableView = Integer.parseInt(availableViews);
                    int maxViewGiven = Integer.parseInt(maxViewsGiven);
                    int usedViews = maxViewGiven - availableView;
                    results.add("Alert : " + email + "has crossed 90% of max views given.");
                    results.add("Max views given : " + maxViewGiven);
                    results.add("Used views  : " + usedViews);
                    return results;
               }
            }
        } catch (Exception ex) {
            logger.error("Exception while getting email content list, email for crossing 90% of given max-views ", ex);
        }
       return results;
    }      
    
    public void emailOnNinetyPercentCreditsReached(String currentAdminUserEmail) {
        Map<String, List<String>> alertEmailMap = new HashMap<String, List<String>>();
        alertEmailMap.put(currentAdminUserEmail, getEmailContentOfCreditAlert(currentAdminUserEmail));
        emailReachedNinetyPercent(alertEmailMap, "Max credits given", currentAdminUserEmail);
    }
    
    public void emailOnNinetyPercentViewReached(String currentAdminUserEmail) {
        Map<String, List<String>> alertEmailMap = new HashMap<String, List<String>>();
        alertEmailMap.put(currentAdminUserEmail, getEmailContentOfViewsAlert(currentAdminUserEmail));
        emailReachedNinetyPercent(alertEmailMap, "Max view limit given", currentAdminUserEmail);
    }
    
    public final void emailReachedNinetyPercent(final Map<String, List<String>> resultObjectMap, final String actionName,
            final String currentAdminUser) {
        logger.info("In sending mail for action : " + actionName + " by " + currentAdminUser);
        try {
            EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.REACHED_NINETY_PERCENT_LIMIT_ALERT_SUBJECT) + actionName
                    + " :" + adminHelper.getFirstKeyOfMap(resultObjectMap) + ":" + dateFormat.format(new Date()));
            emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_TO));
            emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_FROM));
            emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_TO));
            emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());

            StringBuffer sbf = new StringBuffer("");
            sbf.append(actionName + " By " + currentAdminUser);
            sbf.append("<br>\n");
            sbf.append("<br>\n");
            if (resultObjectMap != null && resultObjectMap.size() > 0) {
                for (String key : resultObjectMap.keySet()) {
                    List<String> results = resultObjectMap.get(key);
                    sbf.append("================" + key + "==============");
                    sbf.append("<br>\n");
                    sbf.append(StringUtils.join(results, FCUtil.getNewLineCharacter()));
                    sbf.append("<br>\n");
                    sbf.append("==========================================");
                }
                emailBusinessDO.setContent(sbf.toString());
            }

            emailService.sendNonVelocityEmail(emailBusinessDO);
        } catch (RuntimeException e) {
            logger.error("Error while Sending Email. Stack Trace=", e);
        }
    }
      
    /*
     * Job method: To reset ct action control data at every night Setting
     * max_credits to avail_credits Setting max_views to avail_views
     */

    public void resetAllCTActionControl() {
        List<DBObject> allRecords = customerTrailControlRepository.getCustomerTrailControllAll();
        for (DBObject record : allRecords) {
            customerTrailControlRepository.updateAvaiableCredits(
                    (String) record.get(CustomerTrailControlRepository.EMAIL_ID),
                    (String) record.get(CustomerTrailControlRepository.MAX_CREDITS));
            customerTrailControlRepository.updateAvailableViews(
                    (String) record.get(CustomerTrailControlRepository.EMAIL_ID),
                    (String) record.get(CustomerTrailControlRepository.VIEW_LIMITS));
        }
    }
    
    /*
     * Setting default maxCredits and maxView limits for new email_id
     * MaxCredits to 500, MaxViewLimits to 20
     */
    public void updateCTControlRepoByDefault(final String email) {
        DBObject dbObject = customerTrailControlRepository.getCustomerTrailControlRecords(email);
        if (dbObject == null) {
            Date currentDateTime = new Date();
            Map<String, Object> customerTrailControlMap = new HashMap<>();
            customerTrailControlMap.put(CustomerTrailControlRepository.EMAIL_ID, email);
            customerTrailControlMap.put(CustomerTrailControlRepository.MAX_CREDITS, "500");
            customerTrailControlMap.put(CustomerTrailControlRepository.VIEW_LIMITS, "20");
            customerTrailControlMap.put(CustomerTrailControlRepository.AVAILABLE_CREDITS, "500");
            customerTrailControlMap.put(CustomerTrailControlRepository.AVAILABLE_VIEWS, "20");
            customerTrailControlMap.put(CustomerTrailControlRepository.CREATEDAT_NAME, currentDateTime);
            customerTrailControlRepository.addCustomerTrailControlRecord(customerTrailControlMap);
        }
    }
}
