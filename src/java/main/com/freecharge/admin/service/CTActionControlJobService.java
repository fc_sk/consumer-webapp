package com.freecharge.admin.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.batch.job.CTActionsControlJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.CustomerTrailControlRepository;
import com.mongodb.DBObject;

public class CTActionControlJobService {
    private static final Logger LOGGER = LoggingFactory.getLogger(CTActionControlJobService.class);
    @Autowired
    private CustomerTrailControlRepository customerTrailControlRepository;
    
    /*
     * Job method: To reset ct action control data at every night Setting
     * max_credits to avail_credits Setting max_views to avail_views
     */
    public void updateCTActionControlJobMethod() {
        LOGGER.info("Stared getting customertrailcontrol repos all records");
        List<DBObject> allRecords = customerTrailControlRepository.getCustomerTrailControllAll();
        LOGGER.info("Finished getting customertrailcontrol repos all records");
        for (DBObject record : allRecords) {
            LOGGER.info("Started reseting available credits of : " + (String)record.get(CustomerTrailControlRepository.EMAIL_ID));
            customerTrailControlRepository.updateAvaiableCredits(
                    (String) record.get(CustomerTrailControlRepository.EMAIL_ID),
                    (String) record.get(CustomerTrailControlRepository.MAX_CREDITS));
            LOGGER.info("Finished reseting available credits of : " + (String)record.get(CustomerTrailControlRepository.EMAIL_ID));
            
            LOGGER.info("Started reseting available views of : " + (String)record.get(CustomerTrailControlRepository.EMAIL_ID));
            customerTrailControlRepository.updateAvailableViews(
                    (String) record.get(CustomerTrailControlRepository.EMAIL_ID),
                    (String) record.get(CustomerTrailControlRepository.VIEW_LIMITS));
            LOGGER.info("Finished reseting available views of : " + (String)record.get(CustomerTrailControlRepository.EMAIL_ID));
        }
    }
}
