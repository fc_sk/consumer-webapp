package com.freecharge.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleWriteDAO;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashMapIgnoreCase;

@Service
public class PrefixListService {

    @Autowired
    private OperatorCircleReadDAO operatorCircleDao;

    @Autowired
    private OperatorCircleWriteDAO operatorCircleWriteDao;
    
    @Autowired
    private OperatorCircleReadDAO operatorCircleReadDAO;

    /**
     * Validate prefix-bean list and populate the ID values during the process.
     * In case of validation error throw exception.
     * @param prefixes
     */
    private void validate(List<PrefixBean> prefixes) {
        List<Map<String, Object>> operatorList = operatorCircleDao.getAllOperators();
        List<CircleMaster> circleList = operatorCircleDao.getCircles();
        List<CircleOperatorMapping> com = operatorCircleDao.getCircleOperatorMapping();
        List<Map<String, Object>> prefixMappingList = operatorCircleDao.getOperatorCircleByPrefix();
        if (operatorList == null) {
            throw new IllegalStateException("Unable to fetch operator list from the database");
        }
        if (circleList == null) {
            throw new IllegalStateException("Unable to fetch circle list from the database");
        }
        if (com == null) {
            throw new IllegalStateException("Unable to fetch circle-operator mapping from the database");
        }
        // set up reverse lookup by operatorID, circleID
        Map<String, Integer> reverseCOM = new HashMap<String, Integer>(com.size());
        for (CircleOperatorMapping each: com) {
            OperatorMaster operatorMaster = operatorCircleReadDAO.getOperator(each.getFkOperatorMasterId());
            if(operatorMaster==null) {
                continue;
            }
            String key = "" + each.getFkOperatorMasterId() + ":" + each.getFkCircleMasterId() + ":"
            + operatorMaster.getFkProductMasterId();
            Integer val = reverseCOM.get(key);
            if (val != null) {
                throw new IllegalStateException(String.format(
                "Duplicate keys found in database: circleOperatorMappingID=(%d, %d) --> operatorID=%d, circleID=%d",
                val, each.getCircleOperatorMappingId(), each.getFkOperatorMasterId(), each.getFkCircleMasterId()));
            }
            reverseCOM.put(key, each.getCircleOperatorMappingId());
        }
        
        Map<String, Integer> operators = new HashMapIgnoreCase<Integer>();
        Map<String, Integer> circles = new HashMapIgnoreCase<Integer>();
        Set<String> prefixSet = new HashSet<String>();
        for (Map<String, Object> each: operatorList) {
            String key = (String) each.get("operatorCode");
            Integer val = (Integer) each.get("operatorMasterId");
            if (val == null) {
                throw new IllegalStateException(String.format(
                        "Operator name '%s' has ID NULL in database", key));
            }
            operators.put(key, val);
        }
        for (CircleMaster each: circleList) {
            String key = each.getName();
            Integer val = each.getCircleMasterId();
            if (val == null) {
                throw new IllegalStateException(String.format(
                        "Circle name '%s' has ID NULL in database", key));
            }
            circles.put(key, val);
        }
        for (Map<String, Object> each: prefixMappingList) {
            String key = (String) each.get("prefixValue");
            String productMasterId = String.valueOf(each.get("productMasterId"));
            key = "" + key + ":" + productMasterId; 
            if (key == null) {
                throw new IllegalStateException("Prefix is NULL in database, other fields: " + each.toString());
            }
            prefixSet.add(key);
        }
        // validate
        for (PrefixBean each: prefixes) {
            if (!operators.containsKey(each.operator)) {
                throw new IllegalArgumentException(String.format(
                        "No such operator '%s' in database. Valid operators are: %s", each.operator,
                        operators.keySet().toString()));
            }
            if (!circles.containsKey(each.circle)) {
                throw new IllegalArgumentException(String.format(
                "No such circle '%s' in database. Valid circles are: %s", each.circle, circles.keySet().toString()));
            }
            String key = "" + operators.get(each.operator) + ":" + circles.get(each.circle)+
                    ":" + each.productMasterId;
            Integer ocmID = reverseCOM.get(key);
            if (ocmID == null) {
                throw new IllegalArgumentException(String.format(
                        "Cannot find operator-circle mapping ID for operator '%s' (ID: %d), circle '%s' (ID: %d) [key: %s]",
                        each.operator, operators.get(each.operator), each.circle, circles.get(each.circle), key));
            }
            each.operatorCircleMappingID = ocmID;
            if (prefixSet.contains("" + each.prefix + ":" + each.productMasterId)) {
                each.existsInDB = true;
            }
        }
    }

    @Transactional
    private void ingest(List<PrefixBean> prefixes, boolean effectChanges) {
        List<Map<String, Object>> inserts = new ArrayList<Map<String,Object>>();
        List<Map<String, Object>> updates = new ArrayList<Map<String,Object>>();
        for (PrefixBean each: prefixes) {
            HashMap<String, Object> m = new HashMap<String, Object>();
            m.put("active", each.active);
            m.put("comid", each.operatorCircleMappingID);
            m.put("prefix", each.prefix);
            m.put("productMasterId", each.productMasterId);
            if (each.existsInDB) {
                updates.add(m);
            } else {
                inserts.add(m);
            }
        }
        if (effectChanges) {
            operatorCircleWriteDao.updatePrefix(updates);
            operatorCircleWriteDao.insertPrefix(inserts);
        }
    }

    public void process(List<PrefixBean> prefixes, boolean effectChanges) {
        validate(prefixes);
        ingest(prefixes, effectChanges);
    }

    public PrefixBean parse(String pLine) {
        final String line = FCUtil.throwIfEmpty(pLine, "prefix-list line");
        String[] tokens = line.split(",");
        if (tokens.length < 4) {
            throw new IllegalArgumentException("Expected four comma-separated tokens, but found: " + line);
        }
        return new PrefixBean(FCUtil.trim(tokens[0]), FCUtil.trim(tokens[1]), FCUtil.trim(tokens[2]),
                FCUtil.trim(tokens[3]), FCUtil.trim(tokens[4]));
    }

    public static class PrefixBean extends AbstractDO {
        public final String operator;
        public final String circle;
        public final String prefix;
        public final boolean active;
        public final String productMasterId;
        public int operatorCircleMappingID;
        public boolean existsInDB = false;
        public PrefixBean(String operator, String circle, String prefix, String active, String productMasterId) {
            this.operator = FCUtil.throwIfEmpty(operator, "operator");
            this.circle = FCUtil.throwIfEmpty(circle, "circle");
            this.prefix = FCUtil.throwIfEmpty(prefix, "prefix");
            this.productMasterId = FCUtil.throwIfEmpty(productMasterId, "productmasterid");
            this.active = "yes".equalsIgnoreCase(FCUtil.throwIfEmpty(active, "active"))? true: false;
        }
    }

}