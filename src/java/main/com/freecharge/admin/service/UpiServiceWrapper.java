package com.freecharge.admin.service;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.client.enums.EnvironmentEnum;
import com.freecharge.client.impl.UpiServiceClientImpl;
import com.freecharge.client.utils.ClientDetails;
import com.freecharge.common.client.GetUserByVpaRequest;
import com.freecharge.common.client.UserDetailsResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

@Service
public class UpiServiceWrapper {

	private static Logger LOGGER = LoggingFactory.getLogger(UpiServiceWrapper.class);

    @Autowired
    private FCProperties fcProperties;
    
	private UpiServiceClientImpl upiServiceClient;

	@PostConstruct
	public void init() {
		EnvironmentEnum env = EnvironmentEnum.TESTING;
		if (fcProperties.getProperty(FCProperties.UPI_CLIENT_ENV).equalsIgnoreCase("prod")) {
			env = EnvironmentEnum.PRODUCTION;
		}
		try {
			ClientDetails.init(fcProperties.getProperty(FCProperties.UPI_CLIENT_IP),
					fcProperties.getProperty(FCProperties.UPI_CLIENT_PORT),
					fcProperties.getProperty(FCProperties.UPI_CLIENT_KEY),
					fcProperties.getProperty(FCProperties.UPI_CLIENT_ID), env);
			upiServiceClient = new UpiServiceClientImpl();
		} catch (Exception e) {
			LOGGER.error("Exception while initializing UPI Client Config", e);
		}
	}

	public UserDetailsResponse getUserByVpa(String vpa) {
		LOGGER.info(String.format("Request comes into UpiServiceWrapper.getUserByVpa(vpa=%s)", vpa));
		GetUserByVpaRequest payTransactionRequest = new GetUserByVpaRequest();
		payTransactionRequest.setVpa(vpa);
		UserDetailsResponse response = new UserDetailsResponse();
		try {
			response = upiServiceClient.getUserByVpa(payTransactionRequest);
			LOGGER.info(String.format("Exiting UpiServiceWrapper.getUserByVpa(UserDetailsResponse=%s)", response));
		} catch (Exception e) {
			LOGGER.error("Unknown exception from UpiServiceWrapper by getUserByVpa", e);
		}
		return response;
	}
}
