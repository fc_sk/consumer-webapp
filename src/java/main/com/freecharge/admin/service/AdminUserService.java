package com.freecharge.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.admin.dao.IAdminUserDAO;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.mongo.repos.AdminUserRepository;
import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 9/10/13
 * Time: 4:51 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("adminUserService")
public class AdminUserService {

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    IAdminUserDAO adminUserDAO;

    @Autowired
    AdminUserRepository adminUserRepository;

    private static final String STRING_PARAMS_CODE="code";
    private static final String STRING_PARAMS_EMAIL="email";
    private static final String STRING_PARAMS_SESSEION_DATA_MAP="Session Data Map";
    private static final String STRING_PARAMS_STATE="state";
    private static final String STRING_PARAMS_LOGGED_IN="loggedIn";
    private static final String STRING_PARAMS_USER_OBJECTS="User Object";



    private static final String STRING_PARAMS_MAP="map";
    private static final String STRING_PARAMS_USER="user";
    private static final String STRING_PARAMS_URL="URL";
    private static final String STRING_PARAMS_NAME="name";
    private static final String STRING_PARAMS_ACCESS_TOKEN="access_token";
    private static final String STRING_PARAMS_EXPIRES_IN="expires_in";
    private static final String STRING_COMPONENT_FIRST_LETTERS="componentFirstLetters";
    /*This method returns the email of the user :
     * if the user is a new user it create the user  
     * if the user exists the user is updated with GToken 
     * checks for the users userType: in case of the user being a root User( email id in common.properties) 
     * Then the user is given Regular user in prod and pre prod mode 
     * and Admin in QA and Dev */
    public String getOrCreateAdminUser(Map<String, Object> gUser, Map<String, Object> tokenMap) {
        String email = (String)gUser.get(STRING_PARAMS_EMAIL);
        logger.info("get_or_create_user: "+email);
        DBObject dbUser = this.adminUserDAO.getAdminUserByEmail(email);
        String userType="";
        List<String> components = new ArrayList<>();
        if( this.fcProperties.isProdMode() || this.fcProperties.isPreProdMode()){
            if(dbUser!=null){
                this.adminUserDAO.updateAccessToken(email, (String)tokenMap.get(STRING_PARAMS_ACCESS_TOKEN), (Long)tokenMap.get(STRING_PARAMS_EXPIRES_IN));
                userType=String.valueOf(dbUser.get(AccessControlUtil.STRING_PARAMS_USER_TYPE));
                if(FCStringUtils.isBlank(userType)||userType=="null"){
                    if(isAdminRoot(email)){
                        userType=FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN;
                    }
                    else{
                        userType=FCConstants.ADMIN_USER_TYPE_REGULAR_READER;
                    }
                    this.adminUserDAO.updateUserType(email,userType);
                }        
                return email;
            }else {
                if(isEmailAllowed(email)){
                    gUser.put(STRING_PARAMS_ACCESS_TOKEN, tokenMap.get(STRING_PARAMS_EXPIRES_IN));
                    gUser.put("expiry", tokenMap.get(STRING_PARAMS_EXPIRES_IN));

                    userType=FCConstants.ADMIN_USER_TYPE_REGULAR_READER;

                    if(isAdminRoot(email)){
                        components.add(AdminComponent.ALL.toString());
                        userType=FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN;
                    }
                    gUser.put(AccessControlUtil.STRING_PARAMS_USER_TYPE,userType);
                    gUser.put(AccessControlUtil.STRING_PARAMS_COMPONENTS, components);
                    this.adminUserDAO.addAdminUser(gUser);
                    return email;
                }else {

                    return null;
                }
            }
        }else {     //dev and qa mode
            if(dbUser!=null){
                 components = (List<String>) dbUser.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
                if (components!=null){
                    for (String com : components){
                        if (com.equals(AdminComponent.ALL.toString())){
                            if(dbUser.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)==null||dbUser.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals("")){
                                this.adminUserDAO.updateUserTypeAndComponents(email, FCConstants.ADMIN_USER_TYPE_ADMIN, components);
                            }
                            return email;
                        }
                    }
                }else {
                    components = new ArrayList<>();
                }
                components.add(AdminComponent.ALL.toString());
                if(dbUser.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)==null){
                    this.adminUserDAO.updateUserTypeAndComponents(email, FCConstants.ADMIN_USER_TYPE_ADMIN, components);
                }
                else{
                    this.adminUserDAO.updateComponents(email, components);                	
                }                
                return email;
            }else {
                if(isEmailAllowed(email)){
                    gUser.put(STRING_PARAMS_ACCESS_TOKEN, tokenMap.get("access_token"));
                    gUser.put("expiry", tokenMap.get(STRING_PARAMS_EXPIRES_IN));
                    components = new ArrayList<>();
                    components.add(AdminComponent.ALL.toString());
                     userType="";
                    //QA and Developer always have full access
                    userType=FCConstants.ADMIN_USER_TYPE_ADMIN;
                    if(isAdminRoot(email)){
                        userType=FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN;
                    }                    
                    gUser.put(AccessControlUtil.STRING_PARAMS_USER_TYPE, userType);
                    gUser.put(AccessControlUtil.STRING_PARAMS_COMPONENTS, components);
                    this.adminUserDAO.addAdminUser(gUser);
                    return email;
                }else {
                    return null;
                }
            }
        }
    }
    public DBObject getAdminUserByEmail(String email){
        return this.adminUserDAO.getAdminUserByEmail(email);
    }
    public List<DBObject> fetchAllUsers(){	
        return this.adminUserDAO.findAllAdminUsers();
    }
    protected Boolean isEmailAllowed(String email){
        logger.info("Checking_email: "+email);
        String emailHost = email.substring(email.indexOf("@")+1);
        for (String em : EMAIL_HOSTS){
            if(em.equals(emailHost)){
                return true;
            }
        }
        logger.info("Email: "+email+" not allowed for logging in.");
        return false;
    }
    protected Boolean isAdminRoot(String email){
        for (String em : this.fcProperties.getAdminRootEmails()){
            if(em.equals(email)) {
                return true;
            }
        }
        return false;
    }
    private static final Logger logger = LoggingFactory.getLogger(AdminUserService.class);
    public static final List<String> EMAIL_HOSTS = new ArrayList<>();
    static {
        EMAIL_HOSTS.add("freecharge.com");
        EMAIL_HOSTS.add("freecharge.in");
        EMAIL_HOSTS.add("csfreecharge.com");
    }
}
