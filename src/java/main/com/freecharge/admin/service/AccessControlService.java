package com.freecharge.admin.service;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.admin.controller.AccessControlController;
import com.freecharge.admin.controller.AdditionalRewardAdminController;
import com.freecharge.admin.controller.AdminRewardCouponController;
import com.freecharge.admin.controller.AggregatorPriorityController;
import com.freecharge.admin.controller.AppConfigController;
import com.freecharge.admin.controller.CodeGeneratorController;
import com.freecharge.admin.controller.CouponRedemptionAdminController;
import com.freecharge.admin.controller.CouponReissueAdminController;
import com.freecharge.admin.controller.CrosssellAdminController;
import com.freecharge.admin.controller.CsOneCheckBankTransferController;
import com.freecharge.admin.controller.CustomerTrailActionsController;
import com.freecharge.admin.controller.FraudReportByPGController;
import com.freecharge.admin.controller.FreefundAdminController;
import com.freecharge.admin.controller.FreefundGenerateAndUploadController;
import com.freecharge.admin.controller.FreefundLargeGenerateUploadController;
import com.freecharge.admin.controller.ImpersonationController;
import com.freecharge.admin.controller.OperatorAlertAdminController;
import com.freecharge.admin.controller.PgAlertAdminController;
import com.freecharge.admin.controller.PostpaidAggregatorPriorityController;
import com.freecharge.admin.controller.PrefixListController;
import com.freecharge.admin.controller.PromocodeAdminController;
import com.freecharge.admin.controller.PropertiesController;
import com.freecharge.admin.controller.RechargeAdminToolsController;
import com.freecharge.admin.controller.UpdateEmailAddressController;
import com.freecharge.admin.controller.UtmTrackerAdminController;
import com.freecharge.admin.dao.IAdminCategoryDAO;
import com.freecharge.admin.dao.IAdminComponentDAO;
import com.freecharge.admin.dao.IAdminUserDAO;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.blacklistdomain.admin.BlacklistDomainAdminController;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.customercare.controller.CustomerTrailController;
import com.freecharge.freefund.web.controller.BinOfferController;
import com.freecharge.fulfilment.FulfilmentAdminController;
import com.freecharge.payment.autorefund.controller.ManualRefundController;
import com.freecharge.productdata.controller.RechargePlanController;
import com.freecharge.wallet.WalletAdminController;
import com.freecharge.web.controller.OperatorRetryConfigController;
import com.freecharge.web.controller.PaymentMisController;
import com.freecharge.web.controller.RechargeAdminController;
import com.freecharge.web.controller.RechargeMisController;
import com.freecharge.web.controller.UserBanController;
import com.freecharge.web.controller.WhiteListFraudController;
import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/22/13
 * Time: 5:50 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("accessControlService") public class AccessControlService {

    @Autowired private IAdminUserDAO adminUserDAO;

    @Autowired private IAdminComponentDAO adminComponentDAO;

    @Autowired private IAdminCategoryDAO adminCategoryDAO;

    private static final String STRING_PARAMS_USERTYPE="userType";
    private static final String STRING_PARAMS_CATEGORY_NAME="categoryName";
    private static final String STRING_PARAMS_COMPONENTS="components";
    private static final String STRING_PARAMS_URL="URL";
    private static final String STRING_PARAMS_NAME="name";

    private static final Logger logger = LoggingFactory.getLogger(AccessControlService.class);

    private static final String STRING_PARAMS_ACTIVE = "active";
    private static final int INTEGER_PRECEDENCE_VALUE_REGULAR = 1;
    private static final int INTEGER_PRECEDENCE_VALUE_ADMIN = 3;
    private static final int INTEGER_PRECEDENCE_VALUE_SUPER_ADMIN = 5;
    private static final String STRING_PARAMS_ACTIVE_COMPONENT="1";

    /*
     *> This method checks whether the user has access to a particular 
     * component
     *>Component need to be added to AdminComponent Enum if API logic is being built 
     *>return false when user is null
     *				when user has no components
     *				when user is a Regular user/Regular Writer and it is trying to ACCESS_CONTROL 
     *				default
     *
     *return true 	when user is Super Admin
     *				when user has ALL as its component
     *				when the component is part of user's component list
     */

    public Boolean hasAccess(String email, AdminComponent component) {
        DBObject user = this.adminUserDAO.getAdminUserByEmail(email);
        if (user == null) {
            return false;
        }
        if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN)) {
            return true;
        }
        List<String> components = (List<String>) user.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
        if (components == null || components.size() < 1) {
            return false;
        }
        if (component.toString().equals(AdminComponent.ACCESS_CONTROL.toString())) {
            if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_REGULAR_READER)
                    || user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)
                    .equals(FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER))
                return false;
        }
        for (String comp : components) {
            if (comp.equals(AdminComponent.ALL.toString()) || comp.equals(component.toString())) {
                return true;
            }
        }
        return false;
    }

    /*Checks whether admin-user has authorized to update transaction details from customertrail page*/
    public boolean isAuthorizedToUpdateCt(String email) {
        DBObject user = this.adminUserDAO.getAdminUserByEmail(email);
        if (user == null) {
            return false;
        }
        if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_ADMIN)) {
            return true;
        }

        if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN)) {
            return true;
        }

        if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER)) {
            return true;
        }
        return false;
    }

    public List<DBObject> fetchAllCategories() {
        return this.adminCategoryDAO.findAllCategories();
    }

    public List<DBObject> fetchAllComponents() {
        return this.adminComponentDAO.findAllComponents();
    }

    public List<DBObject> fetchRestURIMatchingComponents(String restURI) {
        restURI = AccessControlUtil.getRequestURIMatchString(restURI);
        if(restURI == null || restURI.isEmpty())
            return null;
        return this.adminComponentDAO.findAllComponentsForRestPattern(restURI);
    }

    public DBObject fetchCategoryUsingName(String name) {
        return this.adminCategoryDAO.getCategoryUsingName(name);
    }

    public DBObject getComponentUsingName(String name) {
        return this.adminComponentDAO.getComponentUsingName(name);
    }

    public void updateComponentsAndUserTypeandCategory(String email, List<String> components, String userType,
            List<String> categories) {
        this.adminUserDAO.updateComponentsAndUserTypeandCategory(email, components, userType, categories);
    }

    public void addCategory(HashMap<String, Object> category) {
        this.adminCategoryDAO.addComponentCategory(category);
    }

    public void deleteCategory(String name) {
        this.adminCategoryDAO.deleteCategory(name);
    }

    public void updateCategory(String queryName, HashMap<String, Object> category) {
        this.adminCategoryDAO.updateCategory(queryName, category);
    }

    public void addComponent(HashMap<String, Object> component) {
        this.adminComponentDAO.addComponent(component);
    }

    public void updateComponent(String update, String query, String name, String URL, String active,
            List<String> restPatterns) {
        updateComponentUsingName(query, name, URL, active, restPatterns);
    }

    public void updateComponentUsingName(String query, String name, String URL, String active,
            List<String> restPatterns) {
        this.adminComponentDAO.updateComponentUsingName(query, name, URL, active, restPatterns);
    }

    public void deleteComponent(String name) {
        this.adminComponentDAO.deleteComponent(name);
    }

    public Map<String, String> getAdminHomeUrlsMap(List<String> components) {
        Map<String, String> urlMap = new TreeMap<>();
        DBObject ob;
        List<DBObject> componentList = this.adminComponentDAO.findAllComponents();
        if(!FCUtil.isEmpty(components)){
            for (String component : components) 
            {				
                if(component.equals(AdminComponent.ALL.toString()))
                {	
                    return getAllComponentsUrlsMap(componentList);	
                }
                else 
                {										
                    ob=this.adminComponentDAO.getComponentUsingName(component);					
                    if(STRING_PARAMS_ACTIVE_COMPONENT.equals(ob.get(STRING_PARAMS_ACTIVE)))
                    {					
                        urlMap.put( component ,String.valueOf(ob.get(STRING_PARAMS_URL)) );
                    }
                }
            }
        }
        return urlMap;
    }

    /* This Method returns a Map of Components with the categories they are placed in 
     * Structure of the Returned Map -
     * Component(String) : Categories (concatenated String of categories) 
     * */ 
    public HashMap<String,String> getComponentCategoryMap(List<String> components)
    {		
        List<DBObject> categories=this.adminCategoryDAO.findAllCategories();
        logger.debug("ALL Categories:" + categories);
        HashMap<String, String> compToCategoryMap = new HashMap<String, String>();
        String component;
        List<String> tempComponents;
        String listOfCategories = "";
        if (components.contains(AdminComponent.ALL.toString())) {
            for (DBObject cat : categories) {
                List<String> categoryComponents = (List<String>) cat.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
                if (categoryComponents != null) {
                    for (String comp : categoryComponents) {
                        listOfCategories = "";
                        if (compToCategoryMap.containsKey(comp)) {
                            listOfCategories = compToCategoryMap.get(comp);
                            listOfCategories =
                                    listOfCategories + ",\"" + cat.get(AccessControlUtil.STRING_PARAMS_CATEGORY_NAME).toString() + "\"";
                            compToCategoryMap.remove(comp);
                            compToCategoryMap.put(comp, listOfCategories);
                        } else {

                            listOfCategories =
                                    listOfCategories + ",\"" + cat.get(AccessControlUtil.STRING_PARAMS_CATEGORY_NAME).toString() + "\"";
                            compToCategoryMap.put(comp, listOfCategories);
                        }
                    }
                }
            }

            return compToCategoryMap;
        }
        for (String ob : components) {
            listOfCategories = "";
            for (DBObject cat : categories) {
                tempComponents = (List<String>) cat.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
                if (tempComponents != null && tempComponents.contains(ob)) {
                    listOfCategories =
                            listOfCategories + ",\"" + cat.get(AccessControlUtil.STRING_PARAMS_CATEGORY_NAME).toString() + "\"";
                }
                logger.debug("components of the category " + cat + "\t are \t" + tempComponents);
            }
            compToCategoryMap.put(ob, listOfCategories);
        }
        logger.debug("The Map returned to the controller : " + compToCategoryMap);
        return compToCategoryMap;
    }

    public Map<String, String> getAllComponentsUrlsMap() {
        return adminUrlsMap;
    }

    public Map<String, String> getAllComponentsUrlsMap(List<DBObject> componentList) {
        logger.debug("ComponentList : user components " + componentList);
        Map<String, String> map = new TreeMap<>();
        for (DBObject ob : componentList) {
            if (ob.get(AccessControlUtil.STRING_PARAMS_NAME) != null && ob.get(AccessControlUtil.STRING_PARAMS_URL) != null) {
                if (Integer.parseInt(ob.get(AccessControlUtil.STRING_PARAMS_ACTIVE).toString())
                        == FCConstants.ADMIN_USER_TYPE_REGULAR_READER_PRECEDENCE) {
                    map.put(ob.get(AccessControlUtil.STRING_PARAMS_NAME).toString(), ob.get(
                            AccessControlUtil.STRING_PARAMS_URL).toString());
                }
            } else {
                logger.error(ob.get(AccessControlUtil.STRING_PARAMS_NAME) + ":null," + ob.get(
                        AccessControlUtil.STRING_PARAMS_URL) + ":,null");
            }
        }
        return map;
    }

    public List<String> getListOfStringFromDBObjectList(String key, List<DBObject> list) {
        ArrayList<String> returnList = new ArrayList<String>();
        for (DBObject ob : list) {
            if (ob.get(key) != null) {
                returnList.add(String.valueOf(ob.get(key)));
            }
        }
        return returnList;
    }

    /*This method return List of User types available */
    public List<String> getUserTypeList() {
        ArrayList<String> userTypeOptions;
        userTypeOptions = new ArrayList<String>();
        userTypeOptions.add(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN);
        userTypeOptions.add(FCConstants.ADMIN_USER_TYPE_ADMIN);
        userTypeOptions.add(FCConstants.ADMIN_USER_TYPE_REGULAR_READER);
        userTypeOptions.add(FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER);
        return userTypeOptions;
    }

    /*This method is used to get the precedence of a user based on his/her userType */
    public int getAuthority(String userType) {
        switch (userType) {
            case FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN:
                return FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN_PRECEDENCE;
            case FCConstants.ADMIN_USER_TYPE_ADMIN:
                return FCConstants.ADMIN_USER_TYPE_ADMIN_PRECEDENCE;
            case FCConstants.ADMIN_USER_TYPE_REGULAR_READER:
                return FCConstants.ADMIN_USER_TYPE_REGULAR_READER_PRECEDENCE;
            case FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER:
                return FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER_PRECEDENCE;
        }
        return FCConstants.ADMIN_USER_TYPE_REGULAR_READER_PRECEDENCE;
    }

    /*This method checks for duplicate values in a list of DBObjects
     * string : value to be checked 
     * list   : list of DBObject
     * keyName: the name of the field that should be checked in each DBObject */
    public boolean checkForDuplicate(String string, List<DBObject> list, String keyName) {
        for (DBObject str : list) {
            if (str != null && str.get(keyName) != null) {
                if (string.equalsIgnoreCase(String.valueOf(str.get(keyName)))) {
                    return true;
                }
            }
        }
        return (false);
    }

    public boolean isComponentsEmpty() {
        List<DBObject> components;
        components = fetchAllComponents();
        if (FCUtil.isEmpty(components)) {
            return true;
        }
        return false;
    }

    public void addDefaultComponents() {

        Map<String,String> map=new HashMap<String,String>();
        String ACTIVE = "1";
        if (isComponentsEmpty()) {
            map = getAllComponentsUrlsMap();
            for (Map.Entry<String, String> component : map.entrySet()) {
                addComponent(component.getKey(), component.getValue(), ACTIVE);
            }
        }
    }

    public void addComponent(String name, String url, String active) {

        if (!FCStringUtils.isBlank(name) && !FCStringUtils.isBlank(url) && !FCStringUtils.isBlank(active)) {
            HashMap<String, Object> component = new HashMap<>();
            component.put(AccessControlUtil.STRING_PARAMS_NAME, name);
            component.put(AccessControlUtil.STRING_PARAMS_URL, url);
            component.put(AccessControlUtil.STRING_PARAMS_ACTIVE, active);
            String resPattern = AccessControlUtil.getRequestURIMatchString(url);
            if(resPattern!=null && !resPattern.isEmpty()) {
                List<String> restPatterns = new ArrayList<>();
                restPatterns.add(resPattern);
                component.put(AccessControlUtil.COMPONENT_REST_PATTERNS, restPatterns);
            }
            addComponent(component);
        }

    }

    static Map<String, String> adminUrlsMap = new HashMap<>();

    static {
        adminUrlsMap.put(AdminComponent.ACCESS_CONTROL.toString(), AccessControlController.HOME_LINK);
        adminUrlsMap.put(AdminComponent.AGGREGATOR_PRIORITY.toString(),
                AggregatorPriorityController.AGGREGATOR_PRIORITY_URL);
        adminUrlsMap.put(AdminComponent.POSTPAID_AGGREGATOR_PRIORITY.toString(),
                PostpaidAggregatorPriorityController.POSTPAID_AGGREGATOR_PRIORITY_URL);
        adminUrlsMap.put(AdminComponent.APP_CONFIG.toString(), AppConfigController.APP_CONFIG_URL);
        adminUrlsMap.put(AdminComponent.CUSTOMER_TRAIL.toString(), CustomerTrailController.CUSTOMER_TRAIL_LINK);
        adminUrlsMap.put(AdminComponent.CUSTOMER_WALLET.toString(), WalletAdminController.CUSTOMER_WALLET_HISTORY_URL);
        adminUrlsMap.put(AdminComponent.FREEFUND.toString(), FreefundAdminController.HOME_LINK);
        adminUrlsMap.put(AdminComponent.IMPERSONATE.toString(), ImpersonationController.IMPERSONATE_URL);
        adminUrlsMap.put(AdminComponent.MANUAL_REFUND.toString(), ManualRefundController.MANUAL_REFUND_TOOL);
        adminUrlsMap.put(AdminComponent.PAYMENT_MIS.toString(), PaymentMisController.PAYMENT_ADMIN_URL);
        adminUrlsMap.put(AdminComponent.PG_MANAGE.toString(), PaymentMisController.MANAGE_PG_LINK);
        adminUrlsMap.put(AdminComponent.PROMOCODE.toString(), PromocodeAdminController.HOME_LINK);
        adminUrlsMap.put(AdminComponent.RECHARGE_MIS.toString(), RechargeMisController.RECHARGE_MIS_URL);
        adminUrlsMap.put(AdminComponent.RECHARGE_PLANS.toString(), RechargePlanController.RECHARGE_PLAN_LINK);
        adminUrlsMap.put(AdminComponent.RUNTIME_CONFIG.toString(), PropertiesController.RUNTIME_PROPS_URL);
        adminUrlsMap.put(AdminComponent.USER_BAN.toString(), UserBanController.USER_BAN_URL);
        adminUrlsMap.put(AdminComponent.CROSS_SELL.toString(), CrosssellAdminController.HOME_LINK);
        adminUrlsMap.put(AdminComponent.PREFIX_UPLOAD.toString(), PrefixListController.AGGREGATOR_PRIORITY_URL);
        adminUrlsMap.put(AdminComponent.BIN_OFFERS.toString(), BinOfferController.ADMIN_HOME_LINK);
        adminUrlsMap.put(AdminComponent.REVOKE_FC_BALANCE.toString(), WalletAdminController.REVOKE_HOME_LINK);
        adminUrlsMap.put(AdminComponent.CASH_BACK_TOOL.toString(), ManualRefundController.CASHBACK_TOOL);
        adminUrlsMap.put(AdminComponent.FULFILMENT.toString(), FulfilmentAdminController.ADMIN_FULFILMENT_LINK);
        adminUrlsMap.put(AdminComponent.OPERATOR_ALERT.toString(), OperatorAlertAdminController.OPERATOR_ALERT_LINK);
        adminUrlsMap.put(AdminComponent.PG_ALERT.toString(), PgAlertAdminController.PG_ALERT_LINK);
        adminUrlsMap.put(AdminComponent.COUPON_REDEMPTION.toString(),
                CouponRedemptionAdminController.ADMIN_COUPON_REDEMPTION_LINK);
        adminUrlsMap.put(AdminComponent.RECHARGE_PLANS_MODERATION.toString(),
                RechargePlanController.RECHARGE_MODERATOR_LINK);
        adminUrlsMap.put(AdminComponent.CASHBACK_FOR_FUNDSOURCE.toString(), ManualRefundController.CASH_BACK);
        adminUrlsMap
                .put(AdminComponent.COUPON_REISSUE.toString(), CouponReissueAdminController.ADMIN_COUPON_REISSUE_LINK);
        adminUrlsMap.put(AdminComponent.FREEFUND_GENERATE_UPLOAD.toString(),
                FreefundGenerateAndUploadController.FREEFUND_GENERATE_AND_UPLOAD_LINK);
        adminUrlsMap.put(AdminComponent.FREEFUND_LARGE_GENERATE_UPLOAD.toString(),
                FreefundLargeGenerateUploadController.FREEFUND_LARGE_GENERATE_AND_UPLOAD_LINK);
        adminUrlsMap.put(AdminComponent.CODE_GENERATOR.toString(), CodeGeneratorController.GENERATE_CODES_LINK);
        adminUrlsMap.put(AdminComponent.UPDATE_EMAIL_ID.toString(), UpdateEmailAddressController.UPDATE_EMAIL_ID_LINK);
        adminUrlsMap.put(AdminComponent.WHITELIST_FRAUD.toString(), WhiteListFraudController.USER_WHITELIST_URL);
        adminUrlsMap
                .put(AdminComponent.REVERSAL_CASE_HANDLER.toString(), RechargeAdminController.REVERSAL_HANDLER_VIEW);
        adminUrlsMap.put(AdminComponent.UTM_TRACKER.toString(), UtmTrackerAdminController.UTM_TRACKER_HOME_VIEW);
        adminUrlsMap.put(AdminComponent.FRAUD_REPORT_BY_PG_HANDLER.toString(),
                FraudReportByPGController.FRAUD_REPORT_BYPG_HANDLER_VIEW);
        adminUrlsMap.put(AdminComponent.CUSTOMER_TRAIL_ACTIONS_CONTROL.toString(),
                CustomerTrailActionsController.CUSTOMER_TRAIL_ACTIONS_CONTROL_VIEW);
        adminUrlsMap.put(AdminComponent.OPERATOR_RETRY_CONFIG.toString(),
                OperatorRetryConfigController.OPERATOR_RETRY_CONFIG_URL);
        adminUrlsMap.put(AdminComponent.AG_PREFERENCE.toString(), RechargeAdminController.AG_PREFERENCE_URL);
        adminUrlsMap.put(AdminComponent.ADDITIONAL_REWARDS.toString(),
                AdditionalRewardAdminController.ADDITIONAL_REWARDS_HOME_VIEW);
        adminUrlsMap.put(AdminComponent.ADMIN_DEBIT_WALLET.toString(), WalletAdminController.ADMIN_DEBIT_WALLET_LINK);
        adminUrlsMap.put(AdminComponent.BLACKLIST_DOMAIN.toString(),
                BlacklistDomainAdminController.BLACKLIST_DOMAIN_HOME_VIEW);
        adminUrlsMap.put(AdminComponent.REWARD_COUPON_SEARCH.toString(),
                AdminRewardCouponController.REWARD_COUPON_SEARCH_LINK);
        adminUrlsMap
                .put(AdminComponent.BULK_STATUS_CHECK.toString(), RechargeAdminToolsController.BULK_STATUS_CHECK_LINK);
        adminUrlsMap.put(AdminComponent.ONECHECK_BANK_TRANS_STATUS.toString(),
                CsOneCheckBankTransferController.ONECHECK_BANK_TRANS_STATUS);
        adminUrlsMap.put(AdminComponent.CREDIT_ENCASH_STATUS_CHANGE.toString(),
                CsOneCheckBankTransferController.ONECHECK_BANK_TRANS_STATUS);
    }

    public boolean hasRestEndpointAccess(String userEmail, String restEndpoint) {
        String restPattern = AccessControlUtil.getRequestURIMatchString(restEndpoint);
        List<DBObject> restMatchingComponents = fetchRestURIMatchingComponents(restPattern);
        if (restMatchingComponents == null || restMatchingComponents.size() == 0) {
            return true;
        }
        DBObject user = this.adminUserDAO.getAdminUserByEmail(userEmail);
        if (user == null) {
            return false;
        }
        if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE).equals(FCConstants.ADMIN_USER_TYPE_SUPER_ADMIN)) {
            return true;
        }
        List<String> components = (List<String>) user.get(AccessControlUtil.STRING_PARAMS_COMPONENTS);
        if (components == null || components.size() < 1) {
            return false;
        }

        if (components.contains(AdminComponent.ALL.toString())) {
            if (restMatchingComponents.contains(AdminComponent.ACCESS_CONTROL) && restMatchingComponents.size() == 1) {
                if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)
                        .equals(FCConstants.ADMIN_USER_TYPE_REGULAR_READER) || user
                        .get(AccessControlUtil.STRING_PARAMS_USER_TYPE)
                        .equals(FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER))
                    return false;
            }
            return true;
        }

        for (DBObject dbObject : restMatchingComponents) {
            String component = (String) dbObject.get(AccessControlUtil.MODEL_ATTRIBUTE_NAME);
            if (components.contains(component)) {
                if (restMatchingComponents.contains(AdminComponent.ACCESS_CONTROL)
                        && restMatchingComponents.size() == 1) {
                    if (user.get(AccessControlUtil.STRING_PARAMS_USER_TYPE)
                            .equals(FCConstants.ADMIN_USER_TYPE_REGULAR_READER) || user
                            .get(AccessControlUtil.STRING_PARAMS_USER_TYPE)
                            .equals(FCConstants.ADMIN_USER_TYPE_REGULAR_WRITER))
                        return false;
                }
                return true;
            }
        }
        return false;
    }

    public boolean isRestEndPointProtected(String restEndpoint) {
        String restPattern = AccessControlUtil.getRequestURIMatchString(restEndpoint);
        List<DBObject> restMatchingComponents = fetchRestURIMatchingComponents(restPattern);
        if (restMatchingComponents == null || restMatchingComponents.size() == 0) {
            return false;
        }
        return true;
    }
}


