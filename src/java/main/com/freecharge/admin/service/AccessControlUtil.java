package com.freecharge.admin.service;

/**
 * Created by vishal on 4/11/15.
 */
public class AccessControlUtil {

    private static final Integer THIRD = 3;

    public static final String STRING_PARAMS_ADMIN_USER    = "admin_user";
    public static final String STRING_PARAMS_EMAIL         = "email";
    public static final String STRING_PARAMS_ACCESS_TOKEN  = "access_token";
    public static final String STRING_PARAMS_EXPIRES_IN    = "expires_in";
    public static final String STRING_PARAMS_USER_TYPE     = "userType";
    public static final String STRING_PARAMS_CATEGORY_NAME = "categoryName";
    public static final String STRING_PARAMS_CATEGORIES    = "categories";
    public static final String STRING_PARAMS_COMPONENTS    = "components";

    public static final String MODEL_ATTRIBUTE_NAME    = "name";
    public static final String MODEL_ATTRIBUTE_URL     = "URL";
    public static final String MODEL_ATTRIBUTE_ACTIVE  = "active";
    public static final String COMPONENT_REPOSITORY    = "component_url";
    public static final String COMPONENT_REST_PATTERNS = "rest_patterns";
    public static final String STRING_PARAMS_URL              = "URL";
    public static final String STRING_PARAMS_NAME             = "name";
    public static final String STRING_PARAMS_ACTIVE           = "active";
    public static final String STRING_PARAMS_ACTIVE_COMPONENT = "1";

    public static String getRequestURIMatchString(String requestURI) {
        return getPartialRequestURI(requestURI, THIRD);
    }

    private static String getPartialRequestURI(String requestURI, int slashCount) {
        if(requestURI==null)
            return requestURI;
        int cnt = slashCount;
        if (!requestURI.startsWith("/")) {
            cnt = slashCount-1;
        }
        //find third occurrence of /
        int j, k = 0;
        for (j = 0; j < requestURI.length(); j++) {
            char c = requestURI.charAt(j);
            if (c == '/')
                k++;
            if (k == cnt)
                break;
        }
        if(k<cnt)
            return null;
        return requestURI.substring(0, ++j);
    }
}
