package com.freecharge.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.autorefund.PaymentRefund;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.admin.dao.RefundReadDAO;
import com.freecharge.admin.entity.RefundRow;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.google.common.collect.ImmutableList;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("refundService")
public class RefundService {
    private static Logger logger = Logger.getLogger(RefundService.class);

    @Autowired
    private RefundReadDAO refundReadDAO;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;

    @Autowired
    private PaymentRefund paymentRefund;
    
    public List<RefundRow> get() {
        List<RefundRow> refundRows = this.refundReadDAO.getUnsuccessfulOrUnknownStateTransactions();
        logger.debug("No of refund rows:" + refundRows.size());
        return refundRows;
    }
    
    private List<String> successOrPendingStatusList = ImmutableList.of("0", "00", "08");
    
    public void initiateRechargeRefund(String orderId, TransactionStatus transactionStatus , String... refundType) {
    
        if (isFailedRecharge(transactionStatus)) {
            PaymentTransaction paymentTransaction = paymentTransactionService.getSuccessfulPaymentTransactionByOrderId(orderId);
            if(paymentTransaction.getPaymentGateway().equalsIgnoreCase(PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {
                /*
                 * There is no recharge refund involved in ZPG case
                 */
                return;
            }
            String mtxnId = paymentTransactionService.getMtxnIdToRefund(orderId);
            Double refundAmount = paymentRefund.calculateRefundAmountForMtxnId(mtxnId);
            kestrelWrapper.decideAndEnqueueRefund(mtxnId, refundAmount, ArrayUtils.isEmpty(refundType) ?PaymentConstants.RECHARGE_FAILURE : refundType[0]);
        }
    }

    
    public void initiateBillPayRefund(String orderId, TransactionStatus transactionStatus, String... refundType) {
       if (isFailedRecharge(transactionStatus)) {
            PaymentTransaction paymentTransaction = paymentTransactionService
                    .getSuccessfulPaymentTransactionByOrderId(orderId);
            if (paymentTransaction.getPaymentGateway().equalsIgnoreCase(PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {
                /*
                 * There is no recharge refund involved in ZPG case
                 */
                return;
            }
            String mtxnId = paymentTransactionService.getMtxnIdToRefund(orderId);
            Double refundAmount = paymentRefund.calculateRefundAmountForMtxnId(mtxnId);
            kestrelWrapper.decideAndEnqueueRefund(mtxnId, refundAmount , ArrayUtils.isEmpty(refundType) ?PaymentConstants.RECHARGE_FAILURE : refundType[0]);
        }
    }
    
    private boolean isFailedRecharge(TransactionStatus status) {
        String statusString = status.getTransactionStatus();
        
        if (StringUtils.isNotBlank(statusString)) {
            if (!successOrPendingStatusList.contains(statusString)) {
                return true;
            }
        }
        
        return false;
    }
}
