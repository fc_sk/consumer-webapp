package com.freecharge.admin.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.snapdeal.fcpt.txnhistoryview.client.ITxnHistoryViewClient;
import com.snapdeal.fcpt.txnhistoryview.client.impl.TxnHistoryViewClientImpl;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetTxnDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetTxnsByTxnTypeRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnHistoryViewResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.TxnHistory;
import com.snapdeal.fcpt.txnhistoryview.exceptions.HttpTransportException;
import com.snapdeal.fcpt.txnhistoryview.exceptions.ServiceException;

@Service
public class TxnHistoryViewService {
	
	private static Logger LOGGER = LoggingFactory.getLogger(TxnHistoryViewService.class);

	private ITxnHistoryViewClient txnHistoryViewClient;
	
    @PostConstruct
	public void init() {
    	txnHistoryViewClient = new TxnHistoryViewClientImpl();
	}
	
	public List<TxnHistory> getTransactionHistoryByTxnType(String userId, String txnType) {
		LOGGER.info(String.format("Request comes into TxnHistoryViewService.getTransactionHistoryByTxnType(userId=%s, txnType=%s)", userId, txnType));
		GetTxnsByTxnTypeRequest txnsByTxnTypeRequest = new GetTxnsByTxnTypeRequest();
		txnsByTxnTypeRequest.setUserId(userId);
		txnsByTxnTypeRequest.setTxnType(txnType);
		try {
			GetTxnHistoryViewResponse txnHistoryViewResponse = txnHistoryViewClient.getTransactionHistoryByTxnType(txnsByTxnTypeRequest);
			if (null != txnHistoryViewResponse && CollectionUtils.isNotEmpty(txnHistoryViewResponse.getGlobalTransactions())) {
				LOGGER.info(String.format("Exiting TxnHistoryViewService.getTransactionHistoryByTxnType(GetTxnHistoryViewResponse=%s)", txnHistoryViewResponse));
				return txnHistoryViewResponse.getGlobalTransactions();
			}
		} catch (HttpTransportException e) {
			LOGGER.error("TxnHistoryView transport error from TxnHistoryViewService", e);
		} catch (ServiceException e) {
			LOGGER.error("TxnHistoryView service exception from TxnHistoryViewService", e);
		} catch (Exception e) {
			LOGGER.error("Unknown exception from TxnHistoryViewService", e);
		}
		return null;
	}

	public GetTxnDetailsResponse getTxnByDetails(String userId, String txnId, String txnType) {
		GetTxnDetailsRequest txnDetailsRequest = new GetTxnDetailsRequest();
		txnDetailsRequest.setTxnType(txnType);
		txnDetailsRequest.setTxnId(txnId);
		txnDetailsRequest.setUserId(userId);
		try {
			GetTxnDetailsResponse txnDetailsResponse = txnHistoryViewClient.getTxnByDetails(txnDetailsRequest);
			LOGGER.info(String.format("Exiting TxnHistoryViewService.getTxnByDetails(GetTxnDetailsResponse=%s)",txnDetailsResponse));
			return txnDetailsResponse;
		} catch (HttpTransportException e) {
			LOGGER.error("TxnHistoryView transport error from TxnHistoryViewService", e);
		} catch (ServiceException e) {
			LOGGER.error("TxnHistoryView service exception from TxnHistoryViewService", e);
		} catch (Exception e) {
			LOGGER.error("Unknown exception from TxnHistoryViewService by getTxnByDetails", e);
		}
		return null;
	}
}
