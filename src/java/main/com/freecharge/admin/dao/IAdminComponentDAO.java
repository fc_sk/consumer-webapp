package com.freecharge.admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public interface IAdminComponentDAO {

	 public void addComponent(Map<String, Object> component);
	 public void updateComponentUsingName(String queryName,String name, String URL, String active, List<String> restPatterns);
	 public void deleteComponent(String name);
	 public DBObject getComponentUsingName(String name);
	 public List<DBObject> findAllComponents();
	 public List<DBObject> findAllComponentsForRestPattern(String restPattern);

	public void addComponentList(List<? extends HashMap<String,String>> components);
	 }

