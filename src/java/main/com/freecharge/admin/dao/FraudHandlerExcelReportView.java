package com.freecharge.admin.dao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.freecharge.admin.model.FraudCaseHandlerData;

public class FraudHandlerExcelReportView extends AbstractView {

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        try {
            response.reset();
            response.setContentType("text/csv");
            response.setHeader("Content-disposition", "attachment; filename=FraudHandlerReport.csv");
            ServletOutputStream out = response.getOutputStream();
            Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)), true);
            List<FraudCaseHandlerData> fraudCaseHandlerDataList = (List<FraudCaseHandlerData>) model
                    .get("excelFraudhandlerData");

            writer.write("Order ID");
            writer.write(",");
            writer.write("Merchant TxnID");
            writer.write(",");
            writer.write("Payment TxnID");
            writer.write(",");
            writer.write("Service");
            writer.write(",");
            writer.write("PG");
            writer.write(",");
            writer.write("Email ID");
            writer.write(",");
            writer.write("Customer Name");
            writer.write(",");
            writer.write("Profile Num");
            writer.write(",");
            writer.write("Address");
            writer.write(",");
            writer.write("Refunded amount");
            writer.write(",");
            writer.write("Refunded date");
            writer.write(",");
            writer.write("IPAddrss");
            writer.write(",");
            writer.write("RefundedOrderIds");
            writer.write("\n");
            if (fraudCaseHandlerDataList != null) {
                for (int i = 0; i < fraudCaseHandlerDataList.size(); i++) {
                    FraudCaseHandlerData fraudCaseHandlerData = fraudCaseHandlerDataList.get(i);
                    writer.write(fraudCaseHandlerData.getOrderId());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getMerchantTxnId() == null ? "" : fraudCaseHandlerData
                            .getMerchantTxnId());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getPaymentTxnId() == null ? "" : fraudCaseHandlerData
                            .getPaymentTxnId().toString());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getService() == null ? "" : fraudCaseHandlerData.getService());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getPaymentGateway() == null ? "" : fraudCaseHandlerData
                            .getPaymentGateway());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getEmail() == null ? "" : fraudCaseHandlerData.getEmail());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getCustomerName() == null ? "" : fraudCaseHandlerData
                            .getCustomerName());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getProfileNumber() == null ? "" : fraudCaseHandlerData
                            .getProfileNumber());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getAddress() == null ? "" : fraudCaseHandlerData.getAddress());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getRefundedAmount() == null ? "" : fraudCaseHandlerData
                            .getRefundedAmount());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getRefundDate() == null ? "" : fraudCaseHandlerData
                            .getRefundDate());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getIpAddress() == null ? "" : fraudCaseHandlerData.getIpAddress());
                    writer.write(",");
                    writer.write(fraudCaseHandlerData.getRefundedOrderIDs().toString() == null ? "" : fraudCaseHandlerData.getRefundedOrderIDs().toString());
                    
                    writer.write("\n");
                }
            }
            response.flushBuffer();
            out.flush();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
