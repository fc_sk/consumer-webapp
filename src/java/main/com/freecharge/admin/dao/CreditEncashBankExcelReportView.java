package com.freecharge.admin.dao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.view.AbstractView;
import com.freecharge.wallet.service.CreditsEncashDetails;

public class CreditEncashBankExcelReportView extends AbstractView {
	 @Override
	 protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {		 
		 try {
	        	response.reset();
	            response.setContentType("text/csv");
	            response.setHeader("Content-disposition", "attachment; filename=CreditEncashBankReport.csv");
	            ServletOutputStream out = response.getOutputStream();
	            Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)), true);
	            List<CreditsEncashDetails> creditsEncashDetailsList = (List<CreditsEncashDetails>) model
	                    .get("excelCreditEncashData");
	            writer.write("ID");
	            writer.write(",");
	            writer.write("User id");
	            writer.write(",");
	            writer.write("Bank acc num");
	            writer.write(",");
	            writer.write("Ifsc code");
	            writer.write(",");
	            writer.write("Beneficiary name");
	            writer.write(",");
	            writer.write("Email ID");
	            writer.write(",");
	            writer.write("Reference id");
	            writer.write(",");
	            writer.write("Amount");
	            writer.write(",");
	            writer.write("Transfer status");
	            writer.write(",");
	            writer.write("Transfer date");
	            writer.write(",");
	            writer.write("failure_reason");
	            writer.write(",");
	            writer.write("utr_number");
	            writer.write("\n");
	            if (creditsEncashDetailsList != null) {
	                for (int i = 0; i < creditsEncashDetailsList.size(); i++) {
	                	CreditsEncashDetails creditsEncashDetails = creditsEncashDetailsList.get(i);
	                    writer.write(String.valueOf(creditsEncashDetails.getId()) == null ? "" : String.valueOf(creditsEncashDetails.getId()));
	                    writer.write(",");
	                    writer.write(String.valueOf(creditsEncashDetails.getUserId()) == null ? "" : String.valueOf(creditsEncashDetails.getUserId()));
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getBankAccountNo() == null ? "" : creditsEncashDetails.getBankAccountNo());
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getIfscCode() == null ? "" : creditsEncashDetails.getIfscCode());
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getBeneficiaryName() == null ? "" : creditsEncashDetails.getBeneficiaryName());
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getEmailId() == null ? "" : creditsEncashDetails.getEmailId());
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getReferenceId() == null ? "" : creditsEncashDetails.getReferenceId());
	                    writer.write(",");
	                    writer.write(String.valueOf(creditsEncashDetails.getAmount()) == null ? "" : String.valueOf(creditsEncashDetails.getAmount()));
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getTransferStatus() == null ? "" : creditsEncashDetails.getTransferStatus());
	                    writer.write(",");
	                    writer.write(String.valueOf(creditsEncashDetails.getTransferDate()) == null ? "" : String.valueOf(creditsEncashDetails.getTransferDate()));
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getFailureReason() == null ? "" : creditsEncashDetails.getFailureReason());
	                    writer.write(",");
	                    writer.write(creditsEncashDetails.getUtrNumber() == null ? "" : creditsEncashDetails.getUtrNumber());
	                    writer.write("\n");
	                }
	            }
	            response.flushBuffer();
	            out.flush();
	            writer.flush();
	            writer.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
}
