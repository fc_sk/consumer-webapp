package com.freecharge.admin.dao;

import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public interface IAdminCategoryDAO {
	 public void addComponentCategory(Map<String, Object> category); 
	    public List<DBObject> findAllCategories();
	    public void deleteCategory(String categoryName);
	    public void updateCategory(String categoryName, Map<String, Object> category );
	    public DBObject getCategoryUsingName(String name);
}
