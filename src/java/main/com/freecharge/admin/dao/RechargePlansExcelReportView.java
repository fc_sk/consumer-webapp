package com.freecharge.admin.dao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.freecharge.productdata.entity.RechargePlan;

public class RechargePlansExcelReportView extends AbstractView {
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		 try {
	            response.reset();
	            response.setContentType("text/csv");
	            response.setHeader("Content-disposition", "attachment; filename=RechargePlans.csv");
	            ServletOutputStream out = response.getOutputStream();
	            Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)), true);
	            List<RechargePlan> rechargePlanList = (List<RechargePlan>) model
	                    .get("excelRechargePlanData");
	            writer.write("Recharge plan Id");
	            writer.write("|");
	            writer.write("Name");
	            writer.write("|");
	            writer.write("Produc Id");
	            writer.write("|");
	            writer.write("Operator Id");
	            writer.write("|");
	            writer.write("Circle Id");
	            writer.write("|");
	            writer.write("Denomination");
	            writer.write("|");
	            writer.write("Amount");
	            writer.write("|");
	            writer.write("Talktime");
	            writer.write("|");
	            writer.write("Validity");
	            writer.write("|");
	            writer.write("Decription");
	            writer.write("\n");
	            if (rechargePlanList != null) {
	                for (int i = 0; i < rechargePlanList.size(); i++) {
	                    RechargePlan planData = rechargePlanList.get(i);
	                    writer.write(String.valueOf(planData.getRechargePlanId()) == null ? "" : String.valueOf(planData.getRechargePlanId()));
	                    writer.write("|");
	                    writer.write(planData.getName() == null ? "" : planData.getName());
	                    writer.write("|");
	                    writer.write(String.valueOf(planData.getProductType()) == null ? "" : String.valueOf(planData.getProductType()));
	                    writer.write("|");
	                    writer.write(String.valueOf(planData.getOperatorMasterId()) == null ? "" : String.valueOf(planData.getOperatorMasterId()));
	                    writer.write("|");
	                    writer.write(String.valueOf(planData.getCircleMasterId()) == null ? "" : String.valueOf(planData.getCircleMasterId()));
	                    writer.write("|");
	                    writer.write(planData.getDenominationType() == null ? "" : planData.getDenominationType());
	                    writer.write("|");
	                    writer.write(String.valueOf(planData.getAmount()) == null ? "" : String.valueOf(planData.getAmount()));
	                    writer.write("|");
	                    writer.write(String.valueOf(planData.getTalktime()) == null ? "" : String.valueOf(planData.getTalktime()));
	                    writer.write("|");
	                    writer.write(planData.getValidity() == null ? "" : planData.getValidity());
	                    writer.write("|");
	                    writer.write(planData.getDescription() == null ? "" : planData.getDescription());
	                    writer.write("\n");
	                }
	            }
	            response.flushBuffer();
	            out.flush();
	            writer.flush();
	            writer.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }	
	}
}
