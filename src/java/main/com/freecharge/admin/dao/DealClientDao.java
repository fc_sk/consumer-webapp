package com.freecharge.admin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.entity.DealClient;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 4/14/14
 * Time: 6:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("dealClientDao")
public class DealClientDao {

    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertClient;
    private static final String TABLE = "deal_clients";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jt = new NamedParameterJdbcTemplate(dataSource);
        this.insertClient = new SimpleJdbcInsert(dataSource).withTableName(TABLE).usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public void addClient(Long dealId, String name){
        Map<String, Object> insertParams = new HashMap<>();
        insertParams.put("deal_id", dealId);
        insertParams.put("name", name);
        this.insertClient.execute(insertParams);
    }

    public void updateClient(Integer id, Long dealId, String name){
        String query = "update "+TABLE+" set deal_id = :dealId, name = :name where id = :id;";
        Map<String, Object> params = new HashMap<>();
        params.put("dealId", dealId);
        params.put("name", name);
        params.put("id", id);
        this.jt.update(query, params);
    }

    public List<DealClient> getAllClients(){
        String query = "select * from "+TABLE+";";
        return this.jt.query(query, new HashMap<String, Object>(), new DealClientRowMapper());
    }

    public DealClient getClient(Integer id){
        String query = "select * from "+TABLE+" where id = :id;";
        Map<String, Object>  params = new HashMap<>();
        params.put("id", id);
        List<DealClient> clients = this.jt.query(query, params, new DealClientRowMapper());
        if (clients!= null && clients.size()>0){
            return clients.get(0);
        }
        return null;
    }

    public static final class DealClientRowMapper implements RowMapper<DealClient> {
        @Override
        public DealClient mapRow(ResultSet rs, int rowNum) throws SQLException {
            DealClient client = new DealClient();
            client.setId(rs.getInt("id"));
            client.setDealId(rs.getLong("deal_id"));
            client.setName(rs.getString("name"));
            return client;
        }
    }
}
