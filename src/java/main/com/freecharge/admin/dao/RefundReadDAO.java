package com.freecharge.admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.admin.entity.RefundRow;
import com.freecharge.admin.mapper.RefundRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/9/12
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundReadDAO {
    private static Logger logger = LoggingFactory.getLogger(RefundReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Find all RefundRow entities.
     *
     * @return List<RefundRow> all RefundRow entities
     */
    public List<RefundRow> getUnsuccessfulOrUnknownStateTransactions() {
        String queryString = "SELECT * FROM payment_txn pg_txn left join txn_fulfilment ful on " +
                "pg_txn.order_id = ful.order_id where transaction_status <> 1";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        List<RefundRow> refundRows = this.jdbcTemplate.query(queryString, paramMap, new RefundRowMapper());
        return refundRows;
    }
}
