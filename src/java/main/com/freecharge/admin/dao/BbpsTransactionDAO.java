package com.freecharge.admin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.entity.BbpsTransactionDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.BBPSTransactionDetails;

/**
 * @author abhinavdixit
 * 
 *         DAO for requests coming through bbps Transaction Id field in Admin
 *         panel
 */
@Repository
public class BbpsTransactionDAO {

	private static Logger logger = LoggingFactory.getLogger(RefundReadDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private static final String NO_RECORD_FOUND = "No Record Found for given key";

	/**
	 * @param dataSource
	 */
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public BbpsTransactionDetails getTransactionDetailsFromBbpsReferenceNumber(String bbpsReferenceId) {

		String sql = "select o.order_id, b.is_bbps_active, b.bbps_reference_number from order_id  o, bbps_details b where  o.lookup_id = b.lookup_id AND b.bbps_reference_number = :bbpsReferenceId ;";
		Map<String, Object>  params = new HashMap<>();
		logger.info("BBPS_REFERENCE ID in DAO = "+ bbpsReferenceId);
        params.put("bbpsReferenceId", bbpsReferenceId);
		List<BbpsTransactionDetails> details = this.jdbcTemplate.query(sql,
				params, new BbpsMapper());

		if(details != null && details.size() > 0) {
			logger.info("Order Id is(DAO):" + details.get(0));
			return details.get(0);
		}
		

		return new BbpsTransactionDetails();
	}
	
	public BbpsTransactionDetails getBbpsReferenceNumberFromOrderId(String orderId) {

		String sql = "select o.order_id, b.is_bbps_active, b.bbps_reference_number from order_id  o, bbps_details b where  o.lookup_id = b.lookup_id AND o.order_id = :orderId ;";
		Map<String, Object>  params = new HashMap<>();
		logger.info("ORDER_ID in DAO = "+ orderId);
        params.put("orderId", orderId);
		List<BbpsTransactionDetails> details = this.jdbcTemplate.query(sql,
				params, new BbpsMapper());

		if(details != null && details.size() > 0) {
			logger.info("BBPS REFERENCE Id is(DAO):" + details.get(0));
			return details.get(0);
		}
		

		return new BbpsTransactionDetails();
	}

	private static final class BbpsMapper implements RowMapper<BbpsTransactionDetails> {
		public BbpsTransactionDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
			BbpsTransactionDetails id = new BbpsTransactionDetails();
			id.setBbpsReferenceId(rs.getString("bbps_reference_number"));
			id.setOrderId(rs.getString("order_id"));
			id.setBbpsActive(rs.getBoolean("is_bbps_active"));
			
			return id;
		}
	}
}
