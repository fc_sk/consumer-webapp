package com.freecharge.admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public interface IAdminUserDAO {
    public void addAdminUser(Map<String, Object> user);
    public DBObject getAdminUserByEmail(String email); 
    public void updateAccessToken(String email,
            String accessToken, 
            Long expiry);
    public List<DBObject> findAllAdminUsers();
    public void updateComponents(String email,
            List<String> components);
    public void updateComponentsAndUserTypeandCategory(String email, 
            List<String> components,String userType,
            List<String> categories);
    public void updateUserTypeAndComponents(String email, 
            String userType,
            List<String> components);
    public void updateUserType(String email, 
            String userType);  
}
