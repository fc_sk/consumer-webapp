package com.freecharge.admin.views;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.view.AbstractView;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.freecharge.common.framework.properties.FCProperties;

public class ReconFileExportView extends AbstractView {
    @Autowired 
    private FCProperties fcProperties;
    
    @Autowired
    @Qualifier("s3Client")
    private AmazonS3 s3;
    
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String reportFileKey = (String) model.get("s3Key");
        
        String[] reportFilePath = reportFileKey.split("/");
        
        String fileNameToStore = reportFilePath[reportFilePath.length - 1];

        response.reset();
        response.setContentType("text/csv");
        response.setHeader("Content-disposition","attachment; filename=" + fileNameToStore);
        ServletOutputStream out = response.getOutputStream();
        
        Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)), true);

        s3.setRegion(Region.getRegion(Regions.AP_SOUTH_1));

        final String reconBucket = fcProperties.getReconBucket();
        S3Object s3Object = s3.getObject(reconBucket, reportFileKey);
        
        S3ObjectInputStream reportContentStream = s3Object.getObjectContent();
        
        IOUtils.copy(reportContentStream, writer);
        
        writer.flush();
        writer.close();
        
        reportContentStream.close();
    }

}
