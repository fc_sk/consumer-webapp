package com.freecharge.admin;

import com.freecharge.mongo.repos.AdminUserRepository;
import com.freecharge.mongo.repos.ComponentCategoryRepository;
import com.freecharge.mongo.repos.ComponentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.entity.AdminCategoryEntity;
import com.freecharge.admin.entity.AdminComponentEntity;
import com.freecharge.admin.entity.AdminUserEntity;
import com.freecharge.common.cache.CacheManager;
import com.mongodb.DBObject;


@Repository("adminCacheManager")
public class AdminCacheManager extends CacheManager{
	private static final String ADMIN_PREFIX = "admin_user_";
	private static final String ADMIN_CATEGORY_PREFIX = "admin_category_";
	private static final String ADMIN_COMPONENT_PREFIX="admin_component_";

	@Autowired
	private AdminUserRepository adminUserRepository;
	@Autowired
	private ComponentCategoryRepository componentCategoryRepository;
	@Autowired
	private ComponentRepository componentRepository;

	// -------------------------Admin User Cache Manager ----------------------------------------------------------------------

	public void setAdminUserById(AdminUserEntity adminUserEntity) {
		set(ADMIN_PREFIX + adminUserEntity.getId(),adminUserEntity );
	}

	public AdminUserEntity getAdminUserById(String id) {
		return (AdminUserEntity)get(ADMIN_PREFIX + id);
	}

	public void deleteAdminUserById(String id) {
		delete(ADMIN_PREFIX + id);
	}

	public String getAdminUserCacheKey(String emailID){
		return String.format(ADMIN_PREFIX + emailID);
	}   
	// --------------------------------------Category Cache Manager --------------------------------------------------------

	public void setCategoryById(AdminCategoryEntity adminCategoryEntity) {
		set(ADMIN_CATEGORY_PREFIX + adminCategoryEntity.getId(),adminCategoryEntity );
	}

	public AdminCategoryEntity getCategoryById(String id) {
		return (AdminCategoryEntity)get(ADMIN_CATEGORY_PREFIX + id);
	}

	public void deleteCategoryById(String id) {
		delete(ADMIN_CATEGORY_PREFIX + id);
	}

	public String getCategoryCacheKey(String catName){
		return String.format(ADMIN_CATEGORY_PREFIX + catName);
	}   
	//     --------------------------------Component cache------------------------------------------------------------------
	public void setComponentById(AdminComponentEntity adminComponentEntity) {
		set(ADMIN_COMPONENT_PREFIX + adminComponentEntity.getId(),adminComponentEntity );
	}

	public AdminComponentEntity getComponentById(String id) {
		return (AdminComponentEntity)get(ADMIN_COMPONENT_PREFIX + id);
	}

	public void deleteComponentById(String id) {
		delete(ADMIN_COMPONENT_PREFIX + id);
	}

	public String getComponentCacheKey(String name){
		return String.format(ADMIN_COMPONENT_PREFIX + name);
	}       
	// ----------------------------------- Flushing the cache ------------------------------------------------------------------------------
	public void flush() {
		for(DBObject adminUser: this.adminUserRepository.findAllAdminUsers()){
			delete(ADMIN_PREFIX + adminUser.get("email"));
		}
		for(DBObject category: this.componentCategoryRepository.findAllCategories()){
			delete(ADMIN_CATEGORY_PREFIX + category.get("categoryName"));
		}
		for(DBObject component :this.componentRepository.findAllComponents()){
			delete(ADMIN_COMPONENT_PREFIX + component.get("name"));
		}
	}

}
