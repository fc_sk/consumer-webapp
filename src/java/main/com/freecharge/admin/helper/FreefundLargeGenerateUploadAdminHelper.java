package com.freecharge.admin.helper;


import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.freecharge.common.amazon.s3.service.AmazonS3Service;
import com.freecharge.common.util.FCConstants;
import com.freecharge.util.RandomCodeGenerator;
import com.google.common.base.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class FreefundLargeGenerateUploadAdminHelper {


    @Autowired
    @Qualifier("amazonS3Service")
    private AmazonS3Service amazons3Service;

    public File writeCodesToFile(RandomCodeGenerator randomCodeGenerator, String filePath,
                                 int fileCount,
                                 int numberOfFiles,
                                 int fileDividingFactor,
                                 boolean evenCodes,
                                 int numberOfCodes) throws IOException {
        File file = new File(filePath);
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        int numCodesToBeWrittenToFile = 0;
        if (fileCount + 1 == numberOfFiles && !evenCodes) {
            numCodesToBeWrittenToFile = numberOfCodes % fileDividingFactor;
        } else {
            numCodesToBeWrittenToFile = fileDividingFactor;
        }
        for (int codeCount = 0; codeCount < numCodesToBeWrittenToFile; codeCount++) {
            bufferedWriter.write(randomCodeGenerator.getCodeWithPrefix() + "\n");
        }
        bufferedWriter.close();
        return file;
    }

    public void writeCodesToZipFileAndAttachToResponse(HttpServletResponse response,
                                                       String fileName,
                                                       ZipOutputStream zos) throws IOException {
        byte[] buffer = new byte[1024];
        Optional s3ObjectContent = amazons3Service.getFileFromS3(fileName, FCConstants.AWS_FREEFUND_BUCKET);
        if (s3ObjectContent.isPresent()) {
            ZipEntry ze= new ZipEntry(fileName);

            zos.putNextEntry(ze);
            S3ObjectInputStream s3ObjectInputStream = (S3ObjectInputStream)s3ObjectContent.get();
            int len;
            while ((len = s3ObjectInputStream.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            zos.flush();
            s3ObjectInputStream.close();
        }

    }
}
