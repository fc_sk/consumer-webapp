package com.freecharge.admin.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mongo.repos.CustomerTrailControlRepository;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;

public class AdminHelper {
    private static final Logger       logger = LoggingFactory.getLogger(AdminHelper.class);
    public static final int           CS_CASHBACK_LIMIT = 500;

    @Autowired
    protected FCProperties            fcProperties;

    @Autowired
    private EmailService              emailService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private CustomerTrailControlRepository customerTrailControlRepository;
    
    @Autowired
    private PaymentRefund             paymentRefund;

    /*Sending alert email on any customertrail action*/
    public final void sendAlertEmail(final Map<String, List<String>> resultObjectMap, final String actionName,
            final String currentAdminUser) {
        logger.info("In sending mail for action : " + actionName + " by " + currentAdminUser);
        try {
            EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
            emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_SUBJECT) + actionName
                    + " : " + getFirstKeyOfMap(resultObjectMap));
            emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_TO));
            emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_FROM));
            emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_TO));
            emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());

            StringBuffer sbf = new StringBuffer("");
            sbf.append(actionName + " By " + currentAdminUser);
            sbf.append("<br>\n");
            sbf.append("<br>\n");
            if (resultObjectMap != null && resultObjectMap.size() > 0) {
                for (String key : resultObjectMap.keySet()) {
                    List<String> results = resultObjectMap.get(key);
                    sbf.append("================" + key + "==============");
                    sbf.append("<br>\n");
                    sbf.append(StringUtils.join(results, FCUtil.getNewLineCharacter()));
                    sbf.append("<br>\n");
                    sbf.append("==========================================");
                }
                emailBusinessDO.setContent(sbf.toString());
            }

            emailService.sendNonVelocityEmail(emailBusinessDO);
        } catch (RuntimeException e) {
            logger.error("Error while Sending Email. Stack Trace=", e);
        }
    }

    public List<String> getResultObjectListForEmail(String mtxnId, String refundedAmount) {
        List<String> results = new ArrayList<String>();
        PaymentTransaction paymentTransaction = paymentTransactionService
                .getUniquePaymentTransactionByMerchantOrderId(mtxnId);
        results.add("OrderId/MtxnId :" + mtxnId);
        results.add("Transaction amount : " + paymentTransaction.getAmount());
        results.add("Refund/Cashback amount : " + refundedAmount);
        return results;
    }

    public List<String> getResultObjectListEmailContent(String orderId, String requestId, String rechargeStatus) {
        List<String> results = new ArrayList<String>();
        results.add("OrderId : " + orderId);
        results.add("RequestId : " + requestId);
        results.add("Reversal to : " + rechargeStatus);
        return results;
    }

    public String getFirstKeyOfMap(final Map<String, List<String>> resultObjectMap) {
        if (resultObjectMap != null && resultObjectMap.size() > 0) {
            for (String key : resultObjectMap.keySet()) {
                return key;
            }
        }
        return " ";
    }
    
    public boolean isExceedCSCashBackLimit(PaymentTransaction paymentTransaction, Double refundAmount) {
        Double cashBackLimit = paymentTransaction.getAmount() + CS_CASHBACK_LIMIT;
        if (refundAmount > cashBackLimit) {
            return true;
        }
        return false;
    }  
    
    /*Sending alert mail if any action done using admin tools*/
    public final void sendEmailOnAdminToolAction(final Map<String, List<String>> resultObjectMap, final String actionName,
            final String currentAdminUser, final String bccEmail) {
        logger.info("In sending email for admin tool action : " + actionName + " by " + currentAdminUser);
        try {
            EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
            emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_SUBJECT) + actionName
                    + " : " + getFirstKeyOfMap(resultObjectMap));
            emailBusinessDO.setTo(currentAdminUser);
            emailBusinessDO.setBcc(fcProperties.getProperty(bccEmail));
            emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_FROM));
            emailBusinessDO.setReplyTo(fcProperties.getProperty(bccEmail));
            emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());

            StringBuffer sbf = new StringBuffer("");
            sbf.append(actionName + " By " + currentAdminUser);
            sbf.append("<br>\n");
            sbf.append("<br>\n");
            if (resultObjectMap != null && resultObjectMap.size() > 0) {
                for (String key : resultObjectMap.keySet()) {
                    List<String> results = resultObjectMap.get(key);
                    sbf.append("================" + key + "==============");
                    sbf.append("<br>\n");
                    sbf.append(StringUtils.join(results, FCUtil.getNewLineCharacter()));
                    sbf.append("<br>\n");
                    sbf.append("==========================================");
                }
                emailBusinessDO.setContent(sbf.toString());
            }
            emailService.sendNonVelocityEmail(emailBusinessDO);
        } catch (RuntimeException e) {
            logger.error("Error while Sending Email. Stack Trace=", e);
        }
    }

    public double getManualRefundAmount(String mtxnId) throws NumberFormatException, Exception { 
        Set<String> merchantOrderIds = new HashSet<String>();
        merchantOrderIds.add(mtxnId);
        List<PaymentTransaction> paymentTransactions = paymentTransactionService
                .getLastPaymentTransactionsByMerchantOrderIds(merchantOrderIds);
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            if (paymentTransaction.getIsSuccessful()) {
                Double refundAmount = Double.parseDouble(paymentRefund.calculateRefundAmountForPaymentTxn(paymentTransaction));
                return refundAmount;
            }
        }
        return 0;
    }
    
    public boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    } 

    /*User Migration status details*/
    public String getToolTipMsgForUserMigration(String migrationStatus) {
    	if ("UPGRADE_RECOMMENDED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.UPGRADE_RECOMMENDED_TOOLTIP_MSG;
    	} else if ("UPGRADE_COMPLETED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.UPGRADE_COMPLETED_TOOLTIP_MSG;
    	} else if ("NO_UPGRADE_REQRUIRED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.NO_UPGRADE_REQRUIRED_TOOLTIP_MSG;
    	} else if ("LINK_SD_ACCOUNT".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.LINK_SD_ACCOUNT_TOOLTIP_MSG;
    	} else if ("LINK_FC_ACCOUNT".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.LINK_FC_ACCOUNT_TOOLTIP_MSG;
    	} else if ("FORCE_UPGRADE".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.FORCE_UPGRADE_TOOLTIP_MSG;
    	}
    	return  " ";
    }
    
    public String getStatusMsgForUserMigration(String migrationStatus) {
    	if ("UPGRADE_RECOMMENDED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.UPGRADE_RECOMMENDED_STATUS_MSG;
    	} else if ("UPGRADE_COMPLETED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.UPGRADE_COMPLETED_STATUS_MSG;
    	} else if ("NO_UPGRADE_REQRUIRED".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.NO_UPGRADE_REQRUIRED_STATUS_MSG;
    	} else if ("LINK_SD_ACCOUNT".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.LINK_SD_ACCOUNT_STATUS_MSG;
    	} else if ("LINK_FC_ACCOUNT".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.LINK_FC_ACCOUNT_STATUS_MSG;
    	} else if ("FORCE_UPGRADE".equalsIgnoreCase(migrationStatus)) {
    		return FCConstants.FORCE_UPGRADE_STATUS_MSG;
    	}
    	return  " ";
    }
    
    public String getTransactionStatusTypeDesc(String transactionStatus) {
    	if ("CREDIT_DEFAULT".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.CREDIT_DEFAULT_TOOLTIP;
    	} else if ("CREDIT_LOAD".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.CREDIT_LOAD_TOOLTIP;
    	} else if ("CREDIT_CASHBACK".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.CREDIT_CASHBACK_TOOLTIP;
    	} else if ("CREDIT_REFUND".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.CREDIT_REFUND_TOOLTIP;
    	} else if ("DEBIT_WITHDRAW".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.DEBIT_WITHDRAW_TOOLTIP;
    	} else if ("DEBIT_DEFAULT".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.DEBIT_DEFAULT_TOOLTIP;
    	} else if ("DEBIT_EXPIRY".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.DEBIT_EXPIRY_TOOLTIP;
    	} else if ("DEBIT_TRANSFER".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.DEBIT_TRANSFER_TOOLTIP;
    	} else if ("PAY_FAILURE".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.PAY_FAILURE_TOOLTIP;
    	} else if ("PAY_PENDING".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.PAY_PENDING_TOOLTIP;
    	} else if ("PAY_SUCCESS".equalsIgnoreCase(transactionStatus)) {
    		return FCConstants.PAY_SUCCESS_TOOLTIP;
    	}
    	return  " ";
    }
    
}
