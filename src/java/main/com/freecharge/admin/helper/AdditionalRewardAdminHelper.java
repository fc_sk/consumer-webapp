package com.freecharge.admin.helper;


import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.promo.util.PromocodeConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AdditionalRewardAdminHelper {

    private Logger logger = LoggingFactory.getLogger(AdditionalRewardAdminHelper.class);

    public static final String REWARD_NAME = "rewardName";
    public static final String REWARD_VALUE = "rewardValue";
    public static final String REWARD_DISCOUNT_TYPE = "rewardDiscountType";
    public static final String REWARD_MAX_DISCOUNT = "rewardMaxDiscount";
    public static final String MESSAGE = "message";
    public static final String REWARD_CONDITION_ID = "rewardConditionId";
    public static final String ADDITIONAL_REWARD = "additionalReward";
    public static final String IS_ENABLED = "isEnabled";

    @Autowired
    private FreefundService freefundService;

    public boolean isValidFreefundClass(long freefundClassId) {
        FreefundClass freefundClass = freefundService.getFreefundClass(freefundClassId);
        if (freefundClass != null) {
            return true;
        }
        return false;
    }

    public Map<String, Object> validateAdditionaReward(Map<String, String> mapping) {
        String rewardName = mapping.get(REWARD_NAME);
        String rewardValue = mapping.get(REWARD_VALUE);
        String rewardDiscountType = mapping.get(REWARD_DISCOUNT_TYPE);
        String rewardMaxDiscount = mapping.get(REWARD_MAX_DISCOUNT);
        String rewardConditionId = mapping.get(REWARD_CONDITION_ID);

        AdditionalReward additionalReward = new AdditionalReward();
        Map<String, Object> resultMap = additionalReward.validate(rewardName,
                rewardValue,
                rewardDiscountType,
                rewardMaxDiscount,
                rewardConditionId);

        additionalReward.setRewardName(rewardName);
        additionalReward.setRewardValue(Float.parseFloat(rewardValue));
        additionalReward.setRewardConditionId(Integer.parseInt(rewardConditionId));
        additionalReward.setRewardDiscountType(rewardDiscountType);
        additionalReward.setRewardMaxDiscount(Float.parseFloat(rewardMaxDiscount));

        if (FCUtil.isNotEmpty(resultMap) && resultMap.containsKey(MESSAGE)) {
            logger.error("Error editing double reward " + rewardName + " with error: "
                    + resultMap.get(MESSAGE));
            return resultMap;
        } else {

            resultMap.put(PromocodeConstants.ADDITIONAL_REWARD, additionalReward);
        }

        return resultMap;
    }
}
