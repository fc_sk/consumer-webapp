package com.freecharge.growthclient;

import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.growthclient.core.*;
import com.freecharge.growthclient.exception.GrowthClientException;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.wallet.OneCheckWalletService;
import org.apache.log4j.Logger;
import com.freecharge.common.framework.logging.LoggingFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vivek on 16/03/16.
 */
@Service("growthClientService")
public class GrowthClientService {
    private static Logger           logger                      = LoggingFactory.getLogger(GrowthClientService.class);
    public final static String     STATUS_SUCCESS              = "Success";
    public final static String     STATUS_FAILED               = "Failed";
    public final static String     STATUS_STRING               = "Status";
    public final static String     RESPONSE_STRING             = "Response";
    public final static String     ERROR_CODE_STRING           = "ErrorCode";
    public final static String     ERROR_MESSAGE_STRING        = "ErrorMessage";
    private final static String     NULL_RESPONSE_CODE          = "NULL_RESPONSE";
    private final static String     NULL_RESPONSE_MESSAGE       = "Response returned was null";
    private final static String     GENERIC_ERROR_CODE          = "GENERIC_ERROR_RETURNED";
    private final static String     USER_DOESNT_EXIT_MESSAGE    = "Could not find User for email: ";
    public final static String     BOLT_BALANCE_STRING           = "BoltBalance";
    private final static String     DEBIT_STRING                =   "DEBIT";
    private final static String     CREDIT_STRING               =   "CREDIT";
    private final static String     MANUAL_CREDIT_STRING        =   "MANUAL_CREDIT";
    @Autowired
    private GrowthClient growthClient;
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    @Autowired
    private UserServiceProxy userServiceProxy;


    public Map<String, Object> getPredictionsByEmail(String email) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        logger.info("Fetching FC userId for email: "+email);
        UserProfile userProfile = userServiceProxy.getUserProfile(email);
        if(null != userProfile && null != userProfile.getFkUserId()) {
            Integer userId = userProfile.getFkUserId();
            logger.info("Fetching predictions for userId: " + userId + ", email: " + email);
            try {
                MyPredictionsRequest myPredictionsRequest = new MyPredictionsRequest();
                myPredictionsRequest.setUserId(userId);
                logger.info("Request being sent to GrowthClient: " + myPredictionsRequest.toString());
                MyPredictionsResponse myPredictionsResponse = growthClient.getMyPredictions(myPredictionsRequest);
                logger.info("Response from GrowthClient: " + myPredictionsResponse.toString());
                if (null != myPredictionsResponse) {
                    if (myPredictionsResponse.getStatus().equalsIgnoreCase(STATUS_SUCCESS)) {
                        if (null != myPredictionsResponse.getPredictionMasterList()) {
                            responseMap.put(STATUS_STRING, STATUS_SUCCESS);
                            responseMap.put(RESPONSE_STRING, myPredictionsResponse);
                        } else {
                            responseMap.put(STATUS_STRING, STATUS_FAILED);
                            responseMap.put(ERROR_CODE_STRING, myPredictionsResponse.getErrorCode());
                            responseMap.put(ERROR_MESSAGE_STRING, myPredictionsResponse.getErrorMessage());
                        }
                    } else {
                        responseMap.put(STATUS_STRING, STATUS_FAILED);
                        responseMap.put(ERROR_CODE_STRING, myPredictionsResponse.getErrorCode());
                        responseMap.put(ERROR_MESSAGE_STRING, myPredictionsResponse.getErrorMessage());
                    }
                } else {
                    responseMap.put(STATUS_STRING, STATUS_FAILED);
                    responseMap.put(ERROR_CODE_STRING, NULL_RESPONSE_CODE);
                    responseMap.put(ERROR_MESSAGE_STRING, NULL_RESPONSE_MESSAGE);
                }
            } catch (GrowthClientException gexp) {
                String errorMessage = "Caught exception from Growth Client with Error Code: " + gexp.getErrorCode().name();
                logger.error(errorMessage, gexp);
                responseMap.put(STATUS_STRING, STATUS_FAILED);
                responseMap.put(ERROR_CODE_STRING, gexp.getErrorCode().name());
                responseMap.put(ERROR_MESSAGE_STRING, errorMessage);

            } catch (Exception e) {
                String errorMessage = "Caught generic exception from Growth Client ";
                logger.error(errorMessage, e);
                responseMap.put(STATUS_STRING, STATUS_FAILED);
                responseMap.put(ERROR_CODE_STRING, GENERIC_ERROR_CODE);
                responseMap.put(ERROR_MESSAGE_STRING, errorMessage);
            }
        } else {
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, GENERIC_ERROR_CODE);
            responseMap.put(ERROR_MESSAGE_STRING, USER_DOESNT_EXIT_MESSAGE+email);
        }
        return responseMap;
    }

    public Map<String, Object> getBoltHistoryByEmail(String email) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        logger.info("Fetching bolt history for email: "+email);
        try {
            BoltHistoryRequest boltHistoryRequest = new BoltHistoryRequest();
            boltHistoryRequest.setSearchByType("emailId");
            boltHistoryRequest.setSearchId(email);
            logger.info("Request being sent to GrowthClient: " + boltHistoryRequest.toString());
            BoltHistoryResponse boltHistoryResponse = growthClient.getBoltHistoryByUser(boltHistoryRequest);
            logger.info("Response from GrowthClient: "+boltHistoryResponse.toString());
            if (null != boltHistoryResponse) {
                if (boltHistoryResponse.getStatus().equalsIgnoreCase(STATUS_SUCCESS)) {
                    if( null != boltHistoryResponse.getUserPointDetails()) {
                        responseMap.put(BOLT_BALANCE_STRING, boltHistoryResponse.getTotalPoints());
                        responseMap.put(STATUS_STRING, STATUS_SUCCESS);
                        responseMap.put(RESPONSE_STRING, boltHistoryResponse);
                    } else {
                        responseMap.put(STATUS_STRING, STATUS_FAILED);
                        responseMap.put(ERROR_MESSAGE_STRING, boltHistoryResponse.getMessage());
                        responseMap.put(ERROR_CODE_STRING, boltHistoryResponse.getErrorCode());
                    }
                } else {
                    responseMap.put(STATUS_STRING, STATUS_FAILED);
                    responseMap.put(ERROR_MESSAGE_STRING, boltHistoryResponse.getMessage());
                    responseMap.put(ERROR_CODE_STRING, boltHistoryResponse.getErrorCode());
                }
            } else {
                responseMap.put(STATUS_STRING, STATUS_FAILED);
                responseMap.put(ERROR_MESSAGE_STRING, NULL_RESPONSE_MESSAGE);
                responseMap.put(ERROR_CODE_STRING, NULL_RESPONSE_CODE);
            }
        } catch (GrowthClientException gexp) {
            String errorMessage = "Caught exception from Growth Client with Error Code: " + gexp.getErrorCode().name();
            logger.error(errorMessage , gexp);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, gexp.getErrorCode().name());
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);

        } catch (Exception e) {
            String errorMessage = "Caught generic exception from Growth Client ";
            logger.error(errorMessage, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, GENERIC_ERROR_CODE);
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);
        }
        return responseMap;
    }

    public Map<String, Object> getBoltHistoryByOrder(String orderId) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        logger.info("Fetching bolt history for orderId: "+orderId);
        try {
            BoltHistoryRequest boltHistoryRequest = new BoltHistoryRequest();
            boltHistoryRequest.setSearchByType("orderId");
            boltHistoryRequest.setSearchId(orderId);
            logger.info("Request being sent to GrowthClient: " + boltHistoryRequest.toString());
            BoltHistoryResponse boltHistoryResponse = growthClient.getBoltHistoryByUser(boltHistoryRequest);
            logger.info("Response from GrowthClient: "+boltHistoryResponse.toString());
            if (null != boltHistoryResponse) {
                if (boltHistoryResponse.getStatus().equalsIgnoreCase(STATUS_SUCCESS)) {
                    if( null != boltHistoryResponse.getUserPointDetails()) {
                        responseMap.put(BOLT_BALANCE_STRING, boltHistoryResponse.getTotalPoints());
                        responseMap.put(STATUS_STRING, STATUS_SUCCESS);
                        responseMap.put(RESPONSE_STRING, boltHistoryResponse);
                    } else {
                        responseMap.put(STATUS_STRING, STATUS_FAILED);
                        responseMap.put(ERROR_MESSAGE_STRING, boltHistoryResponse.getMessage());
                        responseMap.put(ERROR_CODE_STRING, boltHistoryResponse.getErrorCode());
                    }
                } else {
                    responseMap.put(STATUS_STRING, STATUS_FAILED);
                    responseMap.put(ERROR_MESSAGE_STRING, boltHistoryResponse.getMessage());
                    responseMap.put(ERROR_CODE_STRING, boltHistoryResponse.getErrorCode());
                }
            } else {
                responseMap.put(STATUS_STRING, STATUS_FAILED);
                responseMap.put(ERROR_MESSAGE_STRING, NULL_RESPONSE_MESSAGE);
                responseMap.put(ERROR_CODE_STRING, NULL_RESPONSE_CODE);
            }
        } catch (GrowthClientException gexp) {
            String errorMessage = "Caught exception from Growth Client with Error Code: " + gexp.getErrorCode().name();
            logger.error(errorMessage , gexp);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, gexp.getErrorCode().name());
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);

        } catch (Exception e) {
            String errorMessage = "Caught generic exception from Growth Client ";
            logger.error(errorMessage, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, GENERIC_ERROR_CODE);
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);
        }
        return responseMap;
    }

    public Map<String, Object> creditTokensToUser(Long userId, String orderId, Integer points) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        logger.info("Starting crediting of tokens for userId: "+userId+", for orderId: "+orderId);
        try {
            CreditTokensToUserRequest creditTokensToUserRequest = new CreditTokensToUserRequest();
            creditTokensToUserRequest.setOrderId(orderId);
            creditTokensToUserRequest.setUserId(userId);
            creditTokensToUserRequest.setPoints(points);
            logger.info("Request being sent to GrowthClient: " + creditTokensToUserRequest.toString());
            CreditTokensToUserResponse creditTokensToUserResponse = growthClient.creditTokensToUser(
                    creditTokensToUserRequest);
            logger.info("Response from GrowthClient: "+creditTokensToUserResponse.toString());
            if (null != creditTokensToUserResponse) {
                if (creditTokensToUserResponse.getStatus().equalsIgnoreCase(STATUS_SUCCESS)) {
                    responseMap.put(STATUS_STRING, STATUS_SUCCESS);
                    responseMap.put(RESPONSE_STRING, creditTokensToUserResponse);
                } else {
                    responseMap.put(STATUS_STRING, STATUS_FAILED);
                    responseMap.put(ERROR_CODE_STRING, creditTokensToUserResponse.getErrorCode());
                    responseMap.put(ERROR_MESSAGE_STRING, creditTokensToUserResponse.getMessage());
                }
            } else {
                responseMap.put(STATUS_STRING, STATUS_FAILED);
                responseMap.put(ERROR_CODE_STRING, NULL_RESPONSE_CODE);
                responseMap.put(ERROR_MESSAGE_STRING, NULL_RESPONSE_MESSAGE);
            }
        } catch (GrowthClientException gexp) {
            String errorMessage = "Caught exception from Growth Client with Error Code: " + gexp.getErrorCode().name();
            logger.error(errorMessage , gexp);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, gexp.getErrorCode().name());
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);

        } catch (Exception e) {
            String errorMessage = "Caught generic exception from Growth Client ";
            logger.error(errorMessage, e);
            responseMap.put(STATUS_STRING, STATUS_FAILED);
            responseMap.put(ERROR_CODE_STRING, GENERIC_ERROR_CODE);
            responseMap.put(ERROR_MESSAGE_STRING, errorMessage);
        }
        return responseMap;
    }

}
