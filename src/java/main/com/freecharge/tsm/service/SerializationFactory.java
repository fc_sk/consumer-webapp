package com.freecharge.tsm.service;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class SerializationFactory<T> {

	public final static ObjectMapper mapper = new ObjectMapper();
	private final Logger logger = LoggingFactory.getLogger(SerializationFactory.class);

	public T parseJsonToEntity(String jsonContext, Class<T> clazz) {
		T contextEntity = null;

		try {
			contextEntity = mapper.readValue(jsonContext, clazz);
		} catch (IOException e) {
					logger.error("Error while deserializing json message ");
		}
		return contextEntity;
	}

	public String parseEntityToJson(T obj) {
		String json = null;

		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
	logger.error("error while serializing message ");
		}
		return json;
	}

}
