package com.freecharge.tsm.service;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisplayMetadata {
	private String couponId;
	private String categoryId;
	private String couponName;
	private String cities;
	private String label;
	private Integer couponValue;
	private String dateOfAddition;
	private String minDistance;
	private Integer merchantId;
	private String faceValue;
	private Integer rank;
	private String merchantTitle;
	private String termsConditions;
	private String couponShortDesc;
	private String couponImagePath;
	private String couponType;
	private String validityDate;
	private String price;
	private Integer availabilityStatus;
	private String productPaymentsType;
	private String availabilityMsg;

}
