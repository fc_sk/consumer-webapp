package com.freecharge.tsm.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonParseException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.merchantview.entities.ShopoView;
import com.freecharge.merchantview.service.MerchantViewService;
import com.freecharge.views.GlobalTransactionView;
import com.freecharge.views.PaidCouponsTxnView;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.TSMClientGlobalTxnType;
import com.google.common.collect.ImmutableSet;
import com.freecharge.tsm.service.SerializationFactory;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import com.snapdeal.payments.tsm.client.ITxnServiceClient;
import com.snapdeal.payments.tsm.dto.ClientConfig;
import com.snapdeal.payments.tsm.enums.PartyType;
import com.snapdeal.payments.tsm.exception.ClientRequestParameterException;
import com.snapdeal.payments.tsm.exception.ServiceException;
import com.snapdeal.payments.tsm.request.GetTxnByTxnIdRequest;
import com.snapdeal.payments.tsm.response.GetTxnByTxnIdResponseV2;
import com.snapdeal.payments.tsm.response.LinkTxnResponse;
import com.snapdeal.payments.tsm.response.LocalTxnResponse;
import com.snapdeal.payments.tsm.utils.ClientDetails;
import com.snapdeal.payments.view.merchant.commons.dto.MVTransactionDTO;
import com.snapdeal.payments.view.merchant.commons.response.GetTransactionsResponse;
import com.snapdeal.payments.metadata.fulfillment.aggregator.CartFulfillmentMetaData;
import com.snapdeal.payments.metadata.orderCheckout.OrderCheckoutFulfillmentTxnMetadata;
//import com.snapdeal.payments.metadata.interpreter.GlobalMetadataInterpreter;
import com.snapdeal.payments.metadata.interpreter.LocalMetadataInterpreter;

public class TSMService {

	private final Logger logger = LoggingFactory.getLogger(TSMService.class);

	private String clientKey;
	private String clientId;
	private String port;
	private String IP;
	private int apiTimeOut;

	@Autowired
	private ITxnServiceClient iTxnSnapdealServiceClient;
	
	@Autowired
    private OneCheckWalletService oneCheckWalletService;
	
    @Autowired
    private MerchantViewService merchantViewService;

	
//	private static final Set<String> sdGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.CANCELLATION_REFUND.name(),
//    		TSMClientGlobalTxnType.CUSTOMER_PAYMENT.name(), TSMClientGlobalTxnType.FC_WALLET_CASHBACK.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_CREDIT.name(), TSMClientGlobalTxnType.OPS_WALLET_CREDIT_REVERSE.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_CREDIT_VOID.name(), TSMClientGlobalTxnType.OPS_WALLET_DEBIT.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_DEBIT_REVERSE.name(), TSMClientGlobalTxnType.OPS_WALLET_DEBIT_VOID.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_DEBIT_VOID_REVERSE.name(), TSMClientGlobalTxnType.P2P_PAY_TO_MID.name(),
//    		TSMClientGlobalTxnType.P2P_REQUEST_MONEY.name(), TSMClientGlobalTxnType.P2P_SEND_MONEY.name(),
//    		TSMClientGlobalTxnType.PAYABLES_CHARGE.name(), TSMClientGlobalTxnType.PAYABLES_DISBURSEMENT.name(),
//    		TSMClientGlobalTxnType.PAYABLES_LOAD.name(), TSMClientGlobalTxnType.PAYABLES_REFUND.name(),
//    		TSMClientGlobalTxnType.WALLET_CORP_DISBURSEMENT.name());
	
//	private static final Set<String> sdGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.CANCELLATION_REFUND.name(),
//    		TSMClientGlobalTxnType.CUSTOMER_PAYMENT.name(), TSMClientGlobalTxnType.FC_WALLET_CASHBACK.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_CREDIT.name(), TSMClientGlobalTxnType.OPS_WALLET_CREDIT_REVERSE.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_CREDIT_VOID.name(), TSMClientGlobalTxnType.OPS_WALLET_DEBIT.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_DEBIT_REVERSE.name(), TSMClientGlobalTxnType.OPS_WALLET_DEBIT_VOID.name(),
//    		TSMClientGlobalTxnType.OPS_WALLET_DEBIT_VOID_REVERSE.name(), TSMClientGlobalTxnType.PAYABLES_CHARGE.name(),
//    		TSMClientGlobalTxnType.PAYABLES_DISBURSEMENT.name(), TSMClientGlobalTxnType.PAYABLES_LOAD.name(),
//    		TSMClientGlobalTxnType.PAYABLES_REFUND.name(), TSMClientGlobalTxnType.WALLET_CORP_DISBURSEMENT.name());

	private static final Set<String> sdPrimaryGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.CUSTOMER_PAYMENT.name(), TSMClientGlobalTxnType.OPS_WALLET_DEBIT.name());
	
	private static final Set<String> sdP2PAndP2MGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.P2P_PAY_TO_MID.name(), TSMClientGlobalTxnType.P2P_REQUEST_MONEY.name(), TSMClientGlobalTxnType.P2P_SEND_MONEY.name());
	
	private static final Set<String> sdShopoGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.ESCROW_PAYMENT.name(), TSMClientGlobalTxnType.ESCROW_CANCEL.name(), TSMClientGlobalTxnType.ESCROW_FINALIZE.name());
	
	private static final Set<String> sdSplitGlobalTxnTypes = ImmutableSet.<String>of(TSMClientGlobalTxnType.SPLIT_MONEY.name());
	
	private static final String ORDER_FULFILLMENT = "ORDER_FULFILLMENT";
	
	@Autowired
	private SerializationFactory<CartFulfillmentMetaData> cartFulfillmentNotificationSerializationFactory;
	
	@Autowired
	private SerializationFactory<DisplayMetadata> couponFulfillmentNotificationSerializationFactory;
	
	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public int getApiTimeOut() {
		return apiTimeOut;
	}

	public void setApiTimeOut(int apiTimeOut) {
		this.apiTimeOut = apiTimeOut;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientKey() {
		return clientKey;
	}

	public String getClientId() {
		return clientId;
	}

	public ITxnServiceClient getiTxnSnapdealServiceClient() {
		return iTxnSnapdealServiceClient;
	}

	public void setiTxnSnapdealServiceClient(ITxnServiceClient iTxnSnapdealServiceClient) {
		this.iTxnSnapdealServiceClient = iTxnSnapdealServiceClient;
	}

	public GetTxnByTxnIdResponseV2 getTransactionDetailsViaTSMClient(String globalTxnID, String globalTxnType) {
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setApiTimeOut(apiTimeOut);
		clientConfig.setClientId(clientId);
		clientConfig.setClientKey(clientKey);
		
		GetTxnByTxnIdRequest getTxnByTxnIdRequest = new GetTxnByTxnIdRequest();
		getTxnByTxnIdRequest.setClientConfig(clientConfig);
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = null;

		try{
			logger.info("Request: TSM Client - get transaction by txn id (Request) -> " + getTxnByTxnIdRequest);
			logger.info("Request: TSM Client - get transaction by txn id (Global txn Id) -> " + globalTxnID);
			logger.info("Request: TSM Client - get transaction by txn id (Global txn Type) -> " + globalTxnType);
			getTxnByTxnIdResponse = iTxnSnapdealServiceClient
						.getTxnByTxnId(globalTxnID, globalTxnType, getTxnByTxnIdRequest);
			logger.info("Response: TSM Client - get transaction by txn id -> " + getTxnByTxnIdResponse);
		}catch (ServiceException e){
			if(!e.getErrMsg().equals("Global Transaction not found")) logger.error("Exception in TSM Client", e);
		}catch (ClientRequestParameterException r){
			logger.error("Exception in TSM Client", r);
		}
		
		if (getTxnByTxnIdResponse == null) {
			logger.info("The response from TSM client is NULL");
		}
		return getTxnByTxnIdResponse;
	}
	
	
	public List<ShopoView> getShopoTransactionDetailsByOrderId(String orderIdPramValue){
		
		List<GetTxnByTxnIdResponseV2> shopoTxnResponseList = null;
		shopoTxnResponseList = this.getShopoResponseList(orderIdPramValue);
		logger.info("SHOPO Response List from TSM is ->" + shopoTxnResponseList);
		
		List<ShopoView> shopoViewList = new ArrayList<ShopoView>();
		for(GetTxnByTxnIdResponseV2 tsmResponse: shopoTxnResponseList){
			shopoViewList.add(this.parseShopoResponse(tsmResponse));
		}
		logger.info("SHOPO Response View from TSM is ->" + shopoViewList);
		
		return shopoViewList;
		//look for uplink and downlink transactions
	}
	
	public ShopoView parseShopoResponse(GetTxnByTxnIdResponseV2 tsmResponse){
		
		ShopoView shopoView = new ShopoView();
		if (tsmResponse != null) {
			
			JSONParser parser = new JSONParser();
			JSONObject metaData = new JSONObject();
			if (tsmResponse.getGlobalTxnMetaData() != null) {
				if ((tsmResponse.getGlobalTxnMetaData() instanceof String)) {
					// parse
					try {
						metaData = (JSONObject) parser
								.parse((String) tsmResponse.getGlobalTxnMetaData());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else {
					logger.error("Global Metadata is not in right format");
					return null;		
				}
			}
			
			
			logger.info("parsed TSM metadata for Shopo is -> " + metaData);

			
			if (metaData != null) {
				logger.info("Order details from parsed Shopo metadata is -> " + metaData.get("orderDetails"));

				JSONObject json_orderDetails = new JSONObject();
				if (metaData.get("orderDetails") != null) {
					if ((metaData.get("orderDetails")) instanceof String) {
						// parse
						try {
							json_orderDetails = (JSONObject) parser
									.parse((String) metaData.get("orderDetails"));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else {
						json_orderDetails = (JSONObject) metaData.get("orderDetails");
					}
				}
				logger.info("Order Details Info: parsed json -> " + json_orderDetails);
				
				if(json_orderDetails!=null){
					if (json_orderDetails.get("orderId") != null) shopoView.setOrderId((String) json_orderDetails.get("orderId"));
					if (json_orderDetails.get("orderName") != null) shopoView.setOrderName((String) json_orderDetails.get("orderName"));
					if (json_orderDetails.get("quantity") != null) shopoView.setQuantity((Long) json_orderDetails.get("quantity"));
					if (json_orderDetails.get("itemPrice") != null) shopoView.setItemPrice(new BigDecimal((Double) json_orderDetails.get("itemPrice"),MathContext.UNLIMITED));
					if (json_orderDetails.get("seller") != null) shopoView.setSeller((String) json_orderDetails.get("seller"));			
				}

				// fetch email, name and mobile using IMS id for source 
				if(metaData.get("sourceImsId")!=null){
					GetUserResponse sourceUserResponse = oneCheckWalletService
							.getUserByID((String) metaData.get("sourceImsId"), null, null);
					logger.info("Source user response is -> " + sourceUserResponse);
					if (sourceUserResponse != null) {
						if (sourceUserResponse.getUserDetails() != null) {
							shopoView.setEmail(sourceUserResponse.getUserDetails().getEmailId());
							shopoView.setName(sourceUserResponse.getUserDetails().getFirstName() + " "
									+ sourceUserResponse.getUserDetails().getLastName());
							shopoView.setContact(sourceUserResponse.getUserDetails().getMobileNumber());
						}
					}
				}
				
				
				// fetch email, name and mobile using IMS id for source
				if(metaData.get("destinationImsId")!=null){
					GetUserResponse destUserResponse = oneCheckWalletService
							.getUserByID((String) metaData.get("destinationImsId"), null, null);
					logger.info("Destination user response is -> " + destUserResponse);
					if (destUserResponse != null) {
						if (destUserResponse.getUserDetails() != null) {
							shopoView.setDestEmail(destUserResponse.getUserDetails().getEmailId());
							shopoView.setDestName(destUserResponse.getUserDetails().getFirstName() + " "
									+ destUserResponse.getUserDetails().getLastName());
							shopoView.setDestContact(destUserResponse.getUserDetails().getMobileNumber());
						}
					}
				}
				
				// capture that its a SHOPO transaction
				// field
				shopoView.setPlatform("SHOPO");
				
				//set remaining parameters
				if (metaData.get("idempotencyId") != null) shopoView.setIdempotencyId((String) metaData.get("idempotencyId"));
				if (metaData.get("holdingDuration") != null) shopoView.setHoldingDuration((Long) metaData.get("holdingDuration"));
				if (metaData.get("walletTxnId") != null) shopoView.setWalletTxnId((String) metaData.get("walletTxnId"));
				if (metaData.get("txnDisplayId") != null) shopoView.setTxnDisplayId((String) metaData.get("txnDisplayId"));
				if (metaData.get("finalizedAmount") != null) shopoView.setFinalizedAmount(new BigDecimal((Double) metaData.get("finalizedAmount"),MathContext.UNLIMITED));
				if (metaData.get("cancelledAmount") != null) shopoView.setCancelledAmount(new BigDecimal((Long) metaData.get("cancelledAmount"),MathContext.UNLIMITED));
			}

			//set global response parameters
			shopoView.setTxnId(tsmResponse.getGlobalTxnId());
			shopoView.setTimeStamp(tsmResponse.getCreatedTime());
			shopoView.setState(tsmResponse.getGlobalTxnState());
			shopoView.setStatus(tsmResponse.getTsmState().getValue());
			shopoView.setAmount(tsmResponse.getGlobalTxnAmount());
			shopoView.setType(tsmResponse.getGlobalTxnType());
		}
		return shopoView;	
	}

	
	public List<GetTxnByTxnIdResponseV2> getShopoResponseList(String orderIdPramValue){
		
		List<GetTxnByTxnIdResponseV2> shopoTxnResponseList = new ArrayList<GetTxnByTxnIdResponseV2>();
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = null;
		for (String gbTxnTypes : sdShopoGlobalTxnTypes) {

			getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(orderIdPramValue, gbTxnTypes);

			if (getTxnByTxnIdResponse != null) {
				// Just found the response for the Global Txn Id and Type
				shopoTxnResponseList.add(getTxnByTxnIdResponse);
			}
		}
		return shopoTxnResponseList;
	}
	
	public GetTxnByTxnIdResponseV2 getP2PAndP2MResponse(String orderIdPramValue){
	
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = null;
		for (String gbTxnTypes : sdP2PAndP2MGlobalTxnTypes) {

			getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(orderIdPramValue, gbTxnTypes);

			if (getTxnByTxnIdResponse != null) {
				// Just found the response for the Global Txn Id and Type
				break;
			}
		}
		return getTxnByTxnIdResponse;
	}
	
	public GetTxnByTxnIdResponseV2 getP2PSplitResponse(String orderIdPramValue){
		
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = null;
		for (String gbTxnTypes : sdSplitGlobalTxnTypes) {

			getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(orderIdPramValue, gbTxnTypes);

			if (getTxnByTxnIdResponse != null) {
				// Just found the response for the Global Txn Id and Type
				break;
			}
		}
		return getTxnByTxnIdResponse;
	}
	
	
	
	/*
	*	Flow -
	*	1. Fetch Global and Local txn details from TSM Client assuming this is a Global txn Id
	*	2. For all local txns call SD Money client to fetch wallet details
	*	3. If SD Money doesn't return a response then call Payment (Kpay) API to fetch PG details
	*	4. If Global transaction response is not received from TSM Client then try to find global Txn id using the transaction id entered.	
	*/
	public List<GlobalTransactionView> getCompleteTransactionView(String orderIdPramValue) {

		// iterate over the sdPrimaryGlobalTxnTypes SET
		List<GlobalTransactionView> globalTransactionViewList = new ArrayList<GlobalTransactionView>();
		List<GetTxnByTxnIdResponseV2> globalTxnResponseList = new ArrayList<GetTxnByTxnIdResponseV2>();
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = null;

		for (String gbTxnTypes : sdPrimaryGlobalTxnTypes) {

			getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(orderIdPramValue, gbTxnTypes);

			if (getTxnByTxnIdResponse != null) {
				// Just found the response for the Global Txn Id and Type
				// combination. Add this response in the response list.
				globalTxnResponseList.add(getTxnByTxnIdResponse);
			}
		}
		
		List<GetTxnByTxnIdResponseV2> globalTxnResponseListSecondary = new ArrayList<GetTxnByTxnIdResponseV2>();
		
		if (globalTxnResponseList.size() != 0) {
			//for each getTxnByTxnIdResponse in globalTxnResponseList, fetch its downlinks and hit TSM to get the corresponding getTxnByTxnIdResponse
			//add this(ese) to the globalTxnResponseList
			for(GetTxnByTxnIdResponseV2 primaryGlobalTxnResponse: globalTxnResponseList) {
				List<LinkTxnResponse> downlinkTxns = primaryGlobalTxnResponse.getDownlinkTxns();
				//for each downlink transaction hit TSM to get the transaction details
				for(LinkTxnResponse linkTxnResponse: downlinkTxns){
					String downlinkTxnType = linkTxnResponse.getTxnType();
					String downlinkTxnId = linkTxnResponse.getTxnId();
					if(downlinkTxnType!=null & downlinkTxnId!=null){
						//check if downlink txns are linked with global txn id or not
						if(downlinkTxnId.equals(orderIdPramValue)){
							//hit TSM
							getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(orderIdPramValue, downlinkTxnType);
							if(getTxnByTxnIdResponse!=null) globalTxnResponseListSecondary.add(getTxnByTxnIdResponse);
						}
					}
				}
			}
			
			//merge globalTxnResponseListSecondary list with globalTxnResponseList
			globalTxnResponseList.addAll(globalTxnResponseListSecondary);
			
			// We now have found a responses from TSM client. Now for each local
			// transaction of each response, call the SD Money client.
			// We also hit TSM client again for all downlinks
			for (GetTxnByTxnIdResponseV2 responseTSM : globalTxnResponseList) {

				List<LocalTxnResponse> listOfLocalTxnResponse = responseTSM.getLocalTxns();
				GlobalTransactionView globalTransactionView = new GlobalTransactionView();
				String merchantId = responseTSM.getMerchantId();
				
				if (listOfLocalTxnResponse != null) {
					List<ArrayList<TransactionSummary>> listOfAllTransaction = new ArrayList<ArrayList<TransactionSummary>>();
					List<LocalTxnResponse> listOfAllTransactionPG = new ArrayList<LocalTxnResponse>();
					for (LocalTxnResponse locaTxnResponse : listOfLocalTxnResponse) {
						
						Boolean isPG = true;

						if (locaTxnResponse.getTxnReferenceNumber() != null) {
							List<TransactionSummary> transactions = oneCheckWalletService
									.findTransactionsById(locaTxnResponse.getTxnReferenceNumber());
							if (transactions != null) {
								// check if size is non zero
								if (transactions.size() > 0) {
									// None empty response received, add to the
									// list
									listOfAllTransaction.add((ArrayList<TransactionSummary>) transactions);
									isPG = false;
								}
							} else {
								// Exception must have occured while getting
								// details from SD Money client. It has been
								// caught, just logging it here
								logger.error("Caught exception while fetching records from SD Money Client");
							}
						}

						if (isPG) {
							// Either an exception/empty response was received
							// from SDMoney Client or the transaction reference
							// received from TSM was null
							// This means the transaction can be a PG
							// transaction

							// Call Kpay to fetch the PG response, if no pg
							// response found simply add the local txn response
							// to the global transaction view object
							// Kpay call: Yet to be implemented

							// For now add the local txn response to the
							// localTxnListPG of global txn view
							listOfAllTransactionPG.add(locaTxnResponse);
						}
					}
					// Here we add listOfAllTransactions to the
					// GlobalTransactionView List
					globalTransactionView.setLocalTxnList(listOfAllTransaction);
					// Add the transactions received to the global transaction
					// view.
					globalTransactionView.setLocalTxnListPG(listOfAllTransactionPG);
				}

				// Here we parse the Global txn metadata and create a map out of
				// it before adding it to the list of global metadata
				// This is to fetch name and email id of the customer

				Map<String, Object> metaData = this.getParsedMetadata(responseTSM.getGlobalTxnMetaData());

				globalTransactionView.setGlobalTxnMetadataMap(metaData);
				globalTransactionView.setGlobalTxnResponse(responseTSM);
				
				// We still need to fetch merchant name from the merchant id
				GetTransactionsResponse merchantViewResponse = merchantViewService
						.getTransactionDetailsViaMerchantViewClient(responseTSM.getGlobalTxnId(), merchantId);
				
				//we now fetch Request View Details - Test
				//GetRequestViewResponse requestViewResponse = merchantViewService.getTransactionDetailsViaRequestViewClient((String) metaData.get("senderIMSId"));
				
				String merchantName = "N/A";
				if (merchantViewResponse != null) {
					List<MVTransactionDTO> mvTransactions = merchantViewResponse.getMvTransactions();
					if (mvTransactions != null) {
						if(!mvTransactions.isEmpty()) {
							merchantName = merchantViewResponse.getMvTransactions().get(0).getMerchantName();
						} else {
							logger.error("Merchant View returned empty transaction DTP response, therefore setting Merchant Id as N/A");
						}
					} else {
						logger.error("Merchant View returned empty list response, therefore setting Merchant Id as N/A");
					}
				} else {
					logger.error("Merchant View returned null response, therefore setting Merchant Id as N/A");
				}
				// set merchant name to the view
				globalTransactionView.setMerchantName(merchantName);
				// set view to the viewList
				globalTransactionViewList.add(globalTransactionView);
			}
		}
		return globalTransactionViewList;
	}
	
	public PaidCouponsTxnView getPaidCouponsTxnView(final String globalTxnId) {
		LocalMetadataInterpreter localInterpreter = new LocalMetadataInterpreter();
		PaidCouponsTxnView paidCouponsTxnView = null;
		GetTxnByTxnIdResponseV2 getTxnByTxnIdResponse = this.getTransactionDetailsViaTSMClient(globalTxnId, ORDER_FULFILLMENT);
		logger.info("TSM response for globalTxnId : "+globalTxnId + " is" + getTxnByTxnIdResponse);
		if (getTxnByTxnIdResponse != null) {
			paidCouponsTxnView = new PaidCouponsTxnView();
			paidCouponsTxnView.setGlobalTxnMetadataMap(this.getParsedMetadata(getTxnByTxnIdResponse.getGlobalTxnMetaData()));
			paidCouponsTxnView.setCreatedDate(getTxnByTxnIdResponse.getCreatedTime());
			List<Map<String, Object>> localPGWalletTxns =  null;
			List<Map<String, Object>> localCouponsTxns  = null;
			if (CollectionUtils.isNotEmpty(getTxnByTxnIdResponse.getLocalTxns())) {
				localPGWalletTxns = new ArrayList<>();
				localCouponsTxns = new ArrayList<>();
				for (LocalTxnResponse localTxnResponse : getTxnByTxnIdResponse.getLocalTxns()) {
					if (PartyType.FULFILMENT.equals(localTxnResponse.getPartyType())) {
						OrderCheckoutFulfillmentTxnMetadata localMetadata = (OrderCheckoutFulfillmentTxnMetadata) localInterpreter
								.readjson(localTxnResponse.getMetadata());
						logger.info("Got OrderCheckoutFulfillmentTxnMetadata.........");
						CartFulfillmentMetaData cartFulfillmentMetaData = cartFulfillmentNotificationSerializationFactory
								.parseJsonToEntity(localMetadata.getFulfillmentMetadata(), CartFulfillmentMetaData.class);
						logger.info("Got Fulfillmentmetadata............");
							DisplayMetadata displayInfo = couponFulfillmentNotificationSerializationFactory.parseJsonToEntity(
									cartFulfillmentMetaData.getDisplayMetadata(), DisplayMetadata.class);
							logger.info("Terms and conditions------->>>>"+displayInfo.getTermsConditions());
						Map<String, Object> fulfulmentTxn = new HashMap<String, Object>();
							fulfulmentTxn.put("quantity", cartFulfillmentMetaData.getQuantity());
							fulfulmentTxn.put("totalPrice", cartFulfillmentMetaData.getTotalPrice());
							if (displayInfo != null) {
								fulfulmentTxn.put("couponName",displayInfo.getCouponName());
								fulfulmentTxn.put("couponValue",displayInfo.getCouponValue());
								fulfulmentTxn.put("couponType",displayInfo.getCouponType());
								fulfulmentTxn.put("validityDate",displayInfo.getValidityDate());
								fulfulmentTxn.put("termsConditions",displayInfo.getTermsConditions());
							}
						
							localCouponsTxns.add(fulfulmentTxn);
						
					} else {
						Map<String, Object> pgWalletTxn = new HashMap<>();
						pgWalletTxn.put("pgTxnId", localTxnResponse.getTxnReferenceNumber());
						pgWalletTxn.put("merchantTxnId", globalTxnId);
						pgWalletTxn.put("amount", localTxnResponse.getAmount());
						pgWalletTxn.put("status", localTxnResponse.getState());
						pgWalletTxn.put("sendToPg", localTxnResponse.getCreatedTime());
						pgWalletTxn.put("receivedFromPg", localTxnResponse.getLastUpdatedTime());
						if (PartyType.PG.equals(localTxnResponse.getPartyType())) {
							Map<String, Object> pgTxMetadataMap = getParsedMetadata(localTxnResponse.getMetadata());
							if (pgTxMetadataMap != null) {
								pgWalletTxn.put("paymentGateway", pgTxMetadataMap.get("pgUsed"));
							}
						} else {
							pgWalletTxn.put("paymentGateway", "Wallet");
						}
						localPGWalletTxns.add(pgWalletTxn);
					}
				}
			}
			if (CollectionUtils.isNotEmpty(localCouponsTxns)) {
				paidCouponsTxnView.setLocalCouponsTxns(localCouponsTxns);
			}
			if (CollectionUtils.isNotEmpty(localPGWalletTxns)) {
				paidCouponsTxnView.setLocalPGWalletTxns(localPGWalletTxns);
			}
		}
		return paidCouponsTxnView;
		
	}

	
	private Map<String, Object> getParsedMetadata(String globalTxnMetaData) {
		
		ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> metaData = new HashMap<String, Object>();
        try {
            if(globalTxnMetaData!=null) {
                metaData = mapper.readValue(globalTxnMetaData, new TypeReference<Map<String, String>>() {
                });
            }
        } catch (JsonMappingException e) {
        	  logger.error("ERROR IN MAPPING...."+ e.getMessage());
        	}
          catch (JsonParseException e) {
        	  logger.error("Error in parsing ", e);
     
	   	}
	  catch (IOException e) {
              logger.error("Error in parsing global metadata ", e);       
        }
        return metaData;
	}
	@PostConstruct
    public void init() throws Exception {
        
        try {
        	ClientDetails.init(IP,port,clientKey,clientId,apiTimeOut);
        } catch (Exception e) {
            logger.error("Exception while initializing TSM Client Config", e);
        }

    }

}
