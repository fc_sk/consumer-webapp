package com.freecharge.mongo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundUsageCounterService;
import com.freecharge.growthevent.service.UserServiceProxy;

/**
 * Takes a CSV file as input and updates the usage count in Mongo DB for freefund usage.
 * File format should be below,
 * 
 * used_email_id,used_service_number,cnt
 * kannanmech85@gmail.com,9962898330,7
 * .
 * .
 * @author shirish
 *
 */
@Service
public class MongoCSVFreefundUpdater {

	@Autowired
	private FreefundUsageCounterService freefundUsageCounterService;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	private static final Logger logger = LoggingFactory
			.getLogger(MongoCSVFreefundUpdater.class);
	
	public void doUpdate(String csvFilePath){
		try {
			BufferedReader br = new BufferedReader(new FileReader(csvFilePath));
			String line = null;
			boolean first = true;
			Map<String, Integer> userCountMap = new LinkedHashMap<>();
			Map<String, Integer> numberCountMap = new LinkedHashMap<>();
			int processedCount = 0;
			while ((line = br.readLine()) != null) {
				if(!first){
					StringTokenizer tokenizer = new StringTokenizer(line, ",");
					String userEmail = tokenizer.nextToken();
					String number = tokenizer.nextToken();
					String countStr = tokenizer.nextToken();
					if (!FCUtil.isInteger(countStr)){
						logger.error("Invalid count: " + countStr);
					}
					
					// update user count
					Integer userCount = userCountMap.get(userEmail);
					if (userCount == null){
						userCountMap.put(userEmail, Integer.parseInt(countStr));
					} else{
						logger.info("Duplicates User=" + userEmail + " has duplicates.");
						userCount = userCount + Integer.parseInt(countStr);
						userCountMap.put(userEmail, userCount);
					}
					
					// update number count
					Integer numberCount = numberCountMap.get(number);
					if (numberCount == null){
						numberCountMap.put(number, Integer.parseInt(countStr));
					} else{
						logger.info("Duplicates Number=" + number + " has duplicates.");
						numberCount = numberCount + Integer.parseInt(countStr);
						numberCountMap.put(number, numberCount);
					}
					
					processedCount++;
				} else{
					first = false;
				}
			} // end while
			
			
			logger.info("Total To Be Processed Count: " + processedCount);
			Iterator<String> userEmailIterator = userCountMap.keySet().iterator();
			int usersProcessedCount = 0;
			logger.info("<<<<<<<<<<< Starting to process users >>>>>>>>>>>");
			while (userEmailIterator.hasNext()){
				String email = userEmailIterator.next();
				Users user = userServiceProxy.getUserByEmailId(email);
				if (user != null){
					int userId = user.getUserId();
					updateUserCount(userId, userCountMap.get(email));
					usersProcessedCount++;
				} else{
					logger.error("User not found for email : " + email);
				}
			}
			logger.info("Users Processed Count: " + usersProcessedCount);
			
			
			logger.info("<<<<<<<<<<< Starting to process numbers >>>>>>>>>>>");
			Iterator<String> numberIterator = numberCountMap.keySet().iterator();
			int numbersProcessedCount = 0;
			while (numberIterator.hasNext()){
				String number = numberIterator.next();
				updateNumberCount(number, numberCountMap.get(number));
				numbersProcessedCount++;
			}
			logger.info("Numbers Processed Count: " + numbersProcessedCount);
			
		} catch (FileNotFoundException e) {
			logger.error("Error reading file: " + csvFilePath);
		} catch (IOException e) {
			logger.error("Error reading file: " + csvFilePath);
		}
	}
	
	private boolean doUpdate(String userEmail, String number, int count){
		Users user = userServiceProxy.getUserByEmailId(userEmail);
		int userId = user.getUserId();
		boolean isUserCountUpdated = updateUserCount(userId, count);
		boolean isNumberCountUpdated = updateNumberCount(number, count);
		logger.info("userEmail=" + userEmail + " number=" + number + " count="
				+ count + " isUserCountUpdated=" + isUserCountUpdated
				+ " isNumberCountUpdated=" + isNumberCountUpdated);
		return (isUserCountUpdated && isNumberCountUpdated);
	}
	
	private boolean updateUserCount(int userId, int count) {
		freefundUsageCounterService.setUseCountForEmail(Long.valueOf(userId), 177, count);
		return true;
	}
	
	private boolean updateNumberCount(String number, int count) {
		freefundUsageCounterService.setUseCountForNumber(number, 177, count);
		return true;
	}

	/**
	 * First argument should be CSV file path.
	 * @param args
	 */
	public static void main(String[] args) {
		if (args[0] == null || args[0].isEmpty()){
			throw new RuntimeException("Usage: First argument should be the path of the file.  Example, java MongoCSVFreefundUpdater '/home/test.csv'");
		}
		Thread.currentThread().setName("MongoCSVFreefundUpdater");
		ClassPathXmlApplicationContext ctx = null;
        try {
			ctx = BatchJobUtil.initBatchJobContext();
			MongoCSVFreefundUpdater mongoCSVFreefundUpdater = (MongoCSVFreefundUpdater) ctx.getBean("mongoCSVFreefundUpdater");
			mongoCSVFreefundUpdater.doUpdate(args[0]);
		} catch (BeansException e) {
			logger.error("Error occured initializing context, " + e);
		}finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

}
