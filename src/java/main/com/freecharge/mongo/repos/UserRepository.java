package com.freecharge.mongo.repos;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository handles all the user queries relating to MongoDB.
 * Any user related query functions should be added here.
 */
@Repository("userRepository")
public class UserRepository extends MetricsBaseMongoRepository {
    public static final String USER_ID_KEY = "user_id";

    @Value("${mongo.db.username}")
    private String userName;
    @Value("${mongo.db.password}")
    private String password;

    private final Logger logger = LoggingFactory.getLogger(getClass());
	public static final String RECHARGE_AGGREGATE_Key = "recharge_aggregate";



    @Autowired
    public UserRepository(DB mongoDB, MetricsClient metricsClient) {
        super("users", mongoDB, metricsClient);
        logger.info("Lakshya landed here:");
        logger.info("Lakshya username: "+userName);
        logger.info("Lakshya pswd: "+ password);
        logger.info("Lakshya landed here:");
        logger.info("Lakshya Stats: "+ mongoDB.getMongo().getDB("freecharge").getStats());
        logger.info("Lakshya Mongo object: "+ mongoDB.getMongo());
        logger.info("Lakshya Write concern: "+ mongoDB.getWriteConcern());
        logger.info("Lakshya options "+ mongoDB.getMongo().getDB("freecharge").getOptions());
    }

    public DBObject getUserByUserId(Long userId){
        return this.getDocument(new BasicDBObject(USER_ID_KEY, String.valueOf(userId)));
    }

    public void updateOrCreateUser(Long userId, Map<String, Object> update){
        if (update.containsKey(USER_ID_KEY) && !(update.get(USER_ID_KEY) instanceof String)) {
            logger.error("user_id field of users document has to be of type string, passed type is - " + update.get(USER_ID_KEY).getClass().getSimpleName());
        }
        this.upsertDocument(new BasicDBObject(USER_ID_KEY, String.valueOf(userId)), new BasicDBObject(update));
    }
    
    @PostConstruct
    public void ensureIndexes(){

        logger.info("$$Lakshya username: "+userName);
        logger.info("$$Lakshya pswd: "+ password);
    	if (collection == null){
    		logger.error("MongoDB not initialized.");
    		return;
    	}
    	logger.info("Ensuring user_id index.");
    	// User Id index for faster reads.
    	collection.ensureIndex(new BasicDBObject(USER_ID_KEY, "hashed"));
    	logger.info("Ensuring index done.");
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserRepository userRepository = context.getBean(UserRepository.class);

        Map<String, ?> userProfile = ImmutableMap.of(
                "key0", "value0",
                "key1", "value1",
                USER_ID_KEY, 1l
        );

        userRepository.updateOrCreateUser(1l, ((Map<String, Object>) userProfile));

        ((ConfigurableApplicationContext) context).close();
    }
}
