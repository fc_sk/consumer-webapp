package com.freecharge.mongo.repos;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores transient stuff about the transaction - hence its indexed with lookupid
 * It also has a TTL index of 1 day
 *
 *   {
 *     lookupId: "abcd-123",
 *
 *     giftSms: "This is the gift SMS text",
 *     createdAt: new Date('July 22, 2013: 13:00:00'),
 *     somename: somevalue
 *   }
 *
 */
@Repository("lookupIdRepository")
public class LookupIdRepository extends MetricsBaseMongoRepository {
    public static final String LOOKUPID_NAME = "lookupId";
    public static final String GIFTSMS_NAME = "giftSms";
    public static final String CREATEDAT_NAME = "createdAt";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
        public LookupIdRepository(DB mongoDB, MetricsClient metricsClient) {
        super("lookupId", mongoDB, metricsClient);
    }

    public DBObject getDataByLookupId(String lookupId){
        return getDocument(new BasicDBObject(LOOKUPID_NAME, lookupId));
    }

    public void updateOrCreateLookupIdData(String lookupId, Map<String, Object> data) {
        if (data.get(CREATEDAT_NAME) == null) {
            logger.error("No createdAt field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.upsertDocument(new BasicDBObject(LOOKUPID_NAME, lookupId), new BasicDBObject(data));
    }

    @PostConstruct
        public void ensureIndexes(){
        if (collection == null){
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring lookupId index on lookupId collection");
        collection.ensureIndex(new BasicDBObject(LOOKUPID_NAME, 1));
        logger.info("Ensuring lookupId index done.");
        logger.info("Ensuring TTL index on lookupId collection");
        // 1 day = 24 * 60 * 60 = 86400 seconds
        collection.ensureIndex(new BasicDBObject(CREATEDAT_NAME, 1), new BasicDBObject("expireAfterSeconds", 86400));
        logger.info("Ensuring TTL index done.");
    }
}
