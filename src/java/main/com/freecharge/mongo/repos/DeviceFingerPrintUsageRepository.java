package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * Mongo Repo for storing promocode/freefund redemptions per Device Finger Print Id
 *
 */
@Repository("deviceFingerPrintUsageRepository")
public class DeviceFingerPrintUsageRepository extends MetricsBaseMongoRepository {

    public static final String DEVICE_FINGERPRINT = "deviceFingerPrint";
    public static final String USER_ID = "userId";
    public static final String PROMOCODE = "promocode";
    public static final String FREEFUND_CLASS_ID = "freefundClassId";
    public static final String AR_DEAL_ID = "dealId";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";

    private Logger logger = LoggingFactory.getLogger(DeviceFingerPrintUsageRepository.class);

    @Autowired
    public DeviceFingerPrintUsageRepository(DB mongoDb, MetricsClient metricsClient) {
        super("deviceFingerPrintUsageRepo", mongoDb, metricsClient);
    }

    public void insert(Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No createdAt field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(String deviceFingerPrint, Integer userId, String promocode, String key, Long value) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(DEVICE_FINGERPRINT, deviceFingerPrint);
        data.put(USER_ID, userId);
        data.put(PROMOCODE, promocode);
        data.put(key, value);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT,new Date());
        this.insert(data);
    }


    public void deleteOneRecord(String deviceFingerPrint, Integer userId, String promocode, String key, Long value) {
        BasicDBObject query = new BasicDBObject().append(key, value).
                append(PROMOCODE, promocode).
                append(DEVICE_FINGERPRINT, deviceFingerPrint).
                append(USER_ID, userId);
        this.deleteOneDocument(query);
    }
    
    public List<DBObject> getData(String searchKey, Long searchValue, String deviceFingerPrint) {
        BasicDBObject query = new BasicDBObject().append(searchKey, searchValue).append(DEVICE_FINGERPRINT, deviceFingerPrint);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }
    
    public List<DBObject> getDataByUserId(String searchKey, Long searchValue, String deviceFingerPrint, Integer userId) {
        BasicDBObject query = new BasicDBObject().append(searchKey, searchValue).append(DEVICE_FINGERPRINT, deviceFingerPrint).append(USER_ID, userId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public Integer getDeviceFPUsageCount(Long freefundClassId, String deviceFingerPrint) {
        List<DBObject> entries = getData(FREEFUND_CLASS_ID, freefundClassId, deviceFingerPrint);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public Integer getDeviceFPUsageCountForArDeals(Long dealId, String deviceFingerPrint) {
        List<DBObject> entries = getData(AR_DEAL_ID, dealId, deviceFingerPrint);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public Integer getDeviceFPPerUserUsageCount(Long freefundClassId, String deviceFingerPrint, Integer userId) {
        List<DBObject> entries = getDataByUserId(FREEFUND_CLASS_ID, freefundClassId, deviceFingerPrint, userId);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public Integer getDeviceFPPerUserUsageCountForArDeals(Long dealId, String deviceFingerPrint, Integer userId) {
        List<DBObject> entries = getDataByUserId(AR_DEAL_ID, dealId, deviceFingerPrint, userId);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        logger.info("Ensuring FreefundClass-DeviceFingerPrint compound index on deviceFingerPrintUsageRepo");
        BasicDBObject obj = new BasicDBObject();
        obj.put(FREEFUND_CLASS_ID, 1);
        obj.put(DEVICE_FINGERPRINT, 1);
        collection.ensureIndex(obj);
        logger.info("FreefundClass-DeviceFingerPrint compound index ensured on deviceFingerPrintUsageRepo");

        logger.info("Ensuring UserId index on deviceFingerPrintUsageRepo");
        collection.ensureIndex(new BasicDBObject(USER_ID, 1));
        logger.info("UserId index ensured on deviceFingerPrintUsageRepo");

        logger.info("Ensuring index on deviceFingerPrintUsageRepo collection for deviceFingerPrint, dealId");
        obj = new BasicDBObject();
        obj.put(AR_DEAL_ID, 1);
        obj.put(DEVICE_FINGERPRINT, 1);
        obj.put(USER_ID, 1);
        collection.ensureIndex(obj);
        logger.info("Ensured index on deviceFingerPrintUsageRepo collection for deviceFingerPrint, dealId is done");

        logger.info("Ensuring DeviceFingerPrint index on deviceFingerPrintUsageRepo");
        collection.ensureIndex(new BasicDBObject(DEVICE_FINGERPRINT, "hashed"));
        logger.info("DeviceFingerPrint index ensured on deviceFingerPrintUsageRepo");

        logger.info("Ensuring TTL index on deviceFingerPrintUsageRepo collection");
        // 180 days = 180 * 24 * 60 * 60 = 15552000 seconds
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1), new BasicDBObject("expireAfterSeconds", 15552000));
        logger.info("Ensuring TTL index on deviceFingerPrintUsageRepo done.");

    }

}
