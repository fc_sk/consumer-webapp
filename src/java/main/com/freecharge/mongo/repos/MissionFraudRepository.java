package com.freecharge.mongo.repos;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 7/1/14
 * Time: 12:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("missionFraudRepository")
public class MissionFraudRepository extends MetricsBaseMongoRepository {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public MissionFraudRepository(DB mongoDB, MetricsClient metricsClient) {
        super("missionFraudCounts", mongoDB, metricsClient);
    }

    public int getCookieCount(int dealId, String cookie, Date since){
        BasicDBObject query = new BasicDBObject("cookie", cookie);
        query.append("deal_id", dealId);

        //Query for created_on
        BasicDBObject rangeQuery = new BasicDBObject("$gte", since);

        //Add range query to main query
        query.append("created_on", rangeQuery);
        return this.getCount(query).intValue();
    }

    public int getIPCount(int dealId, String ip, Date since){
        BasicDBObject query = new BasicDBObject("ip", ip);
        query.append("deal_id", dealId);

        //Query for created_on
        BasicDBObject rangeQuery = new BasicDBObject("$gte", since);

        //Add range query to main query
        query.append("created_on", rangeQuery);
        return this.getCount(query).intValue();
    }

    public int getMobileNumberCount(int dealId, String mobileNumber, Date since){
        BasicDBObject query = new BasicDBObject("mobile_number", mobileNumber);
        query.append("deal_id", dealId);

        //Query for created_on
        BasicDBObject rangeQuery = new BasicDBObject("$gte", since);

        //Add range query to main query
        query.append("created_on", rangeQuery);
        return this.getCount(query).intValue();
    }

    public void saveFraudCounts(String cookie, String ip, String mobileNumber, String lookupId, int dealId, int userId) {
        BasicDBObject fraudCountObject = new BasicDBObject("deal_id", dealId);
        fraudCountObject.append("cookie", cookie);
        fraudCountObject.append("ip", ip);
        fraudCountObject.append("mobile_number", mobileNumber);
        fraudCountObject.append("user_id", userId);
        fraudCountObject.append("lookup_id", lookupId);
        this.insertDocument(fraudCountObject);
    }

    @Override
    public void ensureIndexes() {
        logger.info("Ensuring indexs for missions fraud repository");
        BasicDBObject indexObj1 = new BasicDBObject();
        indexObj1.append("cookie", 1);
        indexObj1.append("deal_id", 1);
        indexObj1.append("created_on", 1);

        BasicDBObject indexObj2 = new BasicDBObject();
        indexObj2.append("ip", 1);
        indexObj2.append("deal_id", 1);
        indexObj2.append("created_on", 1);

        BasicDBObject indexObj3 = new BasicDBObject();
        indexObj3.append("mobile_number", 1);
        indexObj3.append("deal_id", 1);
        indexObj3.append("created_on", 1);

        this.collection.createIndex(indexObj1);
        this.collection.createIndex(indexObj2);
        this.collection.createIndex(indexObj3);
    }
}
