package com.freecharge.mongo.repos;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores aggregator priority for a given circle_operator_mapping_id
 * 
 * { template : "templateName", List{comid: "88", operator: "Airtel", circle : "karnataka", priority : "euronet,oxigen"}, createdAt: new Date('July 22,
 * 2013: 13:00:00')} }
 * 
 */
@Repository("aggrPriorityTemplateRepository")
public class AggrPriorityTemplateRepository extends MetricsBaseMongoRepository {

    public static final String TEMPLATE  = "template";
    public static final String MAPPINGS  = "mappings";
    public static final String COMID     = "comid";
    public static final String OPERATOR  = "operator";
    public static final String CIRCLE    = "circle";
    public static final String PRIORITY  = "priority";
    public static final String CREATEDAT = "createdat";

    private final Logger       logger    = LoggingFactory.getLogger(getClass());

    @Autowired
    public AggrPriorityTemplateRepository(DB mongoDB, MetricsClient metricsClient) {
        super("aggr_priority_template", mongoDB, metricsClient);
    }

    public List<String> findAllTemplates() {
        List<String> templates = new ArrayList<String>();
        List<DBObject> docs = this.findDocuments();
        if(docs != null) {
            for(DBObject doc : docs ) {
                templates.add((String)doc.get(TEMPLATE));
            }
        }
        return templates;
    }
    
    public Map<String, Object> getDataByTempalte(String template) {
        return (Map<String, Object>)getDocument(new BasicDBObject(TEMPLATE, template));
    }

    public void updateOrCreateEntry(String template, Map<String, Object> data) {
        //data : {"aggr_priority_list" : List{PriorityEntryMap}, createdat:""}
        if (data.get(CREATEDAT) == null) {
            logger.error("No createdat field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.upsertDocument(new BasicDBObject(TEMPLATE, template), new BasicDBObject(data));
    }
    
    public void deleteTemplate(String template) {
        this.deleteDocument(new BasicDBObject(TEMPLATE, template));
    }

    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring template index on aggr_priority collection");
        collection.ensureIndex(new BasicDBObject(TEMPLATE, 1));
        logger.info("Ensuring template index done.");

    }

}
