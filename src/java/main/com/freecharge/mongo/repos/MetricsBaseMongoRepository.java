package com.freecharge.mongo.repos;


import com.freecharge.common.framework.properties.InMemoryAppConfigService;
import com.freecharge.mongo.BaseRepository;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Map;

/**
 * Created by shwetanka on 3/23/15.
 */
public abstract class MetricsBaseMongoRepository extends BaseRepository {

    private MetricsClient metricsClient;
    @Autowired
    private InMemoryAppConfigService inMemAppConfigService;

    public MetricsBaseMongoRepository(String collectionName, DB mongoDB, MetricsClient metricsClient) {
        super(collectionName, mongoDB);
        this.metricsClient = metricsClient;
    }

    @Override
    protected Object makeQuery(MongoOps ops, Map<String, ?> params) {
        long startTime = System.currentTimeMillis();
        Object result =  super.makeQuery(ops, params);
        long endTime = System.currentTimeMillis();
        //log time
        if (this.collection.getName().equals("session") && ((endTime % 1000) != 0)) {
            // Don't log most of the session metrics
        } else {
            if (result != null && inMemAppConfigService.isMongoLogSizeEnabled()){
                this.metricsClient.recordCount(SERVICE_NAME, this.collection.getName()+"-"+ops.toString() + "-SIZE", result.toString().length());
            }
            this.metricsClient.recordEvent(SERVICE_NAME, this.collection.getName(), ops.toString());
            this.metricsClient.recordLatency(SERVICE_NAME, this.collection.getName()+"-"+ops.toString(), endTime-startTime);
        }
        return result;
    }

    public MetricsClient getMetricsClient() {
        return metricsClient;
    }

}
