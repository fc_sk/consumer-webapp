package com.freecharge.mongo.repos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.dao.IAdminCategoryDAO;
import com.freecharge.mongo.repos.MetricsBaseMongoRepository;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
@Repository("componentCategoryRepository")
public class ComponentCategoryRepository extends MetricsBaseMongoRepository implements IAdminCategoryDAO {

    private static final String STRING_PARAMS_REPOSITORY_NAME = "component_category";
    private static final String STRING_PARAMS_CATEGORY_NAME = "categoryName";

    @Autowired
    public ComponentCategoryRepository(DB mongoDB, MetricsClient metricsClient) {
        super(STRING_PARAMS_REPOSITORY_NAME, mongoDB, metricsClient);
    }

    @Override
    public void addComponentCategory(Map<String, Object> category) {
        this.insertDocument(new BasicDBObject(category));
    }

    @Override
    public List<DBObject> findAllCategories(){
        return this.collection.find().sort(new BasicDBObject(STRING_PARAMS_CATEGORY_NAME,1)).toArray();
    }

    @Override
    public void deleteCategory(String categoryName){
        this.deleteDocument(new BasicDBObject(STRING_PARAMS_CATEGORY_NAME,categoryName));
    } 

    @Override
    public void updateCategory(String categoryName, Map<String, Object> category ){    	
        this.updateDocument(new BasicDBObject(STRING_PARAMS_CATEGORY_NAME, categoryName), new BasicDBObject(category),false);
    }

    @Override
    public DBObject getCategoryUsingName(String name){
        return this.getDocument(new BasicDBObject(STRING_PARAMS_CATEGORY_NAME,name)); 
    }

    @Override
    public void ensureIndexes() {
        //No op
    }

}
