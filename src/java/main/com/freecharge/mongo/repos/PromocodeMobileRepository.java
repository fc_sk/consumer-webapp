package com.freecharge.mongo.repos;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores promocode per mobile - hence its indexed with mobile It also has a TTL index of 90 days
 * 
 * { mobile: "8812389988998", promocode: "SKDSDDSDS", ffClass : "466",createdAt: new Date('July 22, 2013: 13:00:00') }
 * 
 */
@Repository("promocodeMobileRepository")
public class PromocodeMobileRepository extends MetricsBaseMongoRepository {
    
    public static final String PROMOCODELIST = "promocodeList";
    public static final String CREATEDAT = "createdAt";

    public static final String PROMOCODE = "promocode";
    public static final String VALUE = "value";
    public static final String FFCLASS   = "ffClass";
    public static final String VALIDUPTO = "validUpto";
    
    public static final String MOBILE    = "mobile";

    private final Logger       logger    = LoggingFactory.getLogger(getClass());

    @Autowired
    public PromocodeMobileRepository(DB mongoDB, MetricsClient metricsClient) {
        super("promocodeMobile", mongoDB, metricsClient);
    }

    public DBObject getDataByMobile(String mobile) {
        return getDocument(new BasicDBObject(MOBILE, mobile));
    }
    
    //Map<promocode, promocodeObj>
    public void updateOrCreateEntry(String mobile, Map<String, Object> data) {
        if (data.get(CREATEDAT) == null) {
            logger.error("No createdAt field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.upsertDocument(new BasicDBObject(MOBILE, mobile), new BasicDBObject(data));
    }
        
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring userId index on promocodeMoble collection");
        collection.ensureIndex(new BasicDBObject(MOBILE, 1));
        logger.info("Ensuring userId index done.");

        logger.info("Ensuring TTL index on promocodeMoble collection");
        // 90 days = 90 * 24 * 60 * 60 = 7776000 seconds
        collection.ensureIndex(new BasicDBObject(CREATEDAT, 1), new BasicDBObject("expireAfterSeconds", 7776000));
        logger.info("Ensuring TTL index done.");
    }

    public List<Map<String, Object>> query(String mobile) {
        DBObject dbObject = getDocument(new BasicDBObject(MOBILE, mobile));
        if (dbObject != null) {
            Map<String, Object> dbEntryObj = (Map<String, Object>)dbObject;
            if(dbEntryObj != null) {
                List<Map<String, Object>> promocodeList = (List<Map<String, Object>>)dbEntryObj.get(PromocodeMobileRepository.PROMOCODELIST);
                return promocodeList;
            }
        }
        return null;
    }
}
