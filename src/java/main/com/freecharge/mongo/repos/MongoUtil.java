package com.freecharge.mongo.repos;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * MongoUtil class encapsulates the commonly used functions related to mongo db usage.
 * @author shirish
 *
 */
public class MongoUtil {
	/**
	 * Get the next unique ID for a named sequence.
	 * @param db 				Mongo database to work with
	 * @param sequenceName 		The name of your sequence (I name mine after my collections)
	 * @return The next ID
	 */
	public static String getNextId(DB db, String sequenceName) {
	    String sequenceCollection = "sequence"; // the name of the sequence collection
	    String sequenceNameField = "sequenceName"; // the name of the field which holds the sequence
	 
	    DBCollection sequence = db.getCollection(sequenceCollection); // get the collection (this will create it if needed)
	 
	    // this object represents your "query", its analogous to a WHERE clause in SQL
	    DBObject query = new BasicDBObject();
	    query.put("_id", sequenceName); // where _id = the input sequence name
	 
	    // this object represents the "update" or the SET blah=blah in SQL
	    DBObject change = new BasicDBObject(sequenceNameField, 1);
	    DBObject update = new BasicDBObject("$inc", change); // the $inc here is a mongodb command for increment
	 
	    // Atomically updates the sequence field and returns the value for you
	    DBObject res = sequence.findAndModify(query, new BasicDBObject(), new BasicDBObject(), false, update, true, true);
	    return res.get(sequenceNameField).toString();
	}
}
