package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("imeiRepository")
public class IMEIRepository extends MetricsBaseMongoRepository{

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public IMEIRepository(DB mongoDB, MetricsClient metricsClient) {
        super("imeiRepository", mongoDB, metricsClient);
    }

    public boolean insert(String imeiNumber, String advertisementId, Long freefundClassId, Integer userId,
                          String freefundCode, String status) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(FCConstants.IMEI_NUMBER, imeiNumber);
        data.put(FCConstants.ADVERTISEMENT_ID, advertisementId);
        data.put(FCConstants.FREEFUND_CLASS_ID, freefundClassId);
        data.put(FCConstants.USERID, userId);
        data.put(FCConstants.PROMOCODE, freefundCode);
        data.put(FCConstants.STATUS, status);
        data.put(FCConstants.CREATED_ON, new Date());
        return this.insert(data);
    }

    private boolean insert(Map<String, Object> data) {
        if (data.get(FCConstants.CREATED_ON) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return false;
        }
        try {
            this.insertDocumentWithACK(new BasicDBObject(data));
        } catch (Exception e) {
            logger.error("Error inserting into repository imeiRepository ", e);
            return false;
        }
        return true;
    }

    public Integer getImeiCount(String imeiNumber, Long freefundClassId, String status){
        List<DBObject> entries = getDataForSingleKey(freefundClassId, FCConstants.IMEI_NUMBER, imeiNumber, status);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public Integer getAdvertisementIdCountOnly(String advertisementId, Long freefundClassId, String status){
        List<DBObject> entries = getDataForSingleKey(freefundClassId, FCConstants.ADVERTISEMENT_ID, advertisementId, status);
        if (entries != null) {
            return entries.size();
        }
        return 0;    
    }

    public List<DBObject> getDataForSingleKey(Long freefundClassId, String key, String value, String status) {
        BasicDBObject query = new BasicDBObject().append(FCConstants.FREEFUND_CLASS_ID, freefundClassId).append(key,
                value).append(FCConstants.STATUS, status);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getData(Long freefundClassId, String imeiNumber, String advertisementId, String status) {
        BasicDBObject query = new BasicDBObject().append(FCConstants.FREEFUND_CLASS_ID, freefundClassId)
                .append(FCConstants.IMEI_NUMBER, imeiNumber).append(FCConstants.ADVERTISEMENT_ID, advertisementId)
                .append(FCConstants.STATUS, status);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }


    //------------- Events --------------  
    public boolean insert(Long eventId, String imeiNumber, Integer userId, String status) {
        Map<String, Object> data = new HashMap<String, Object>();    
        data.put(FCConstants.GROWTH_EVENT_ID, eventId);
        data.put(FCConstants.IMEI_NUMBER, imeiNumber);
        data.put(FCConstants.USERID, userId);   
        data.put(FCConstants.STATUS, status);
        data.put(FCConstants.CREATED_ON, new Date());
        return this.insert(data);
    }

    private List<DBObject> getIMEIDataForEvent(Long eventId, String imeiNumber, String status) {
        BasicDBObject query = new BasicDBObject().append(FCConstants.GROWTH_EVENT_ID, eventId)
                .append(FCConstants.IMEI_NUMBER, imeiNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public int getImeiCountForEvent(Long eventId, String imeiNumber, String status) {
        List<DBObject> entries = getIMEIDataForEvent(eventId, imeiNumber, status);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    
    @Override
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring index on IMEIRepository collection for Imei, status, freefundClassId");
        BasicDBObject obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUND_CLASS_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.IMEI_NUMBER, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.STATUS, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on IMEIRepository collection for imei, status, freefundClassId is done");

        logger.info("Ensuring index on IMEIRepository collection for AdvId, status, freefundClassId");
        obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUND_CLASS_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.ADVERTISEMENT_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.STATUS, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on IMEIRepository collection for AdvId, status, freefundClassId is done");

        logger.info("Ensuring index on IMEIRepository collection for userId, freefundClassId");
        obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUND_CLASS_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.USERID, 1);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on IMEIRepository collection for userId, freefundClassId is done");

        logger.info("Ensuring index on IMEIRepository collection for userId and freefundCode");
        obj = new BasicDBObject();
        obj.put(FCConstants.USERID, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        obj = new BasicDBObject();
        obj.put(FCConstants.PROMOCODE, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on IMEIRepository collection for userId and freefundCode is done");

        logger.info("Ensuring index on IMEIRepository collection for eventId, imei, status");
        obj = new BasicDBObject();
        obj.put(FCConstants.GROWTH_EVENT_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.IMEI_NUMBER, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.STATUS, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on IMEIRepository collection for eventId, imei, status is done");

    }
}
