package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository controls customertrail daily access.It stores daily
 * view-limit and maximum amount of credits for CS guy to do cashback,manul
 * refund.
 * { email_id: "abcd@freecharge.com", daily_views : "50", max_credits : "1000"
 * createdAt: new Date('July 22, 2013: 13:00:00') }
 * 
 */
@Repository("customerTrailControlRepository")
public class CustomerTrailControlRepository extends MetricsBaseMongoRepository {
    public static final String EMAIL_ID       = "email_id";
    public static final String MAX_CREDITS    = "max_credits";
    public static final String VIEW_LIMITS    = "view_limits";
    public static final String AVAILABLE_CREDITS    = "available_credits";
    public static final String AVAILABLE_VIEWS    = "available_views";
    
    public static final String CREATEDAT_NAME = "createdAt";
    private Logger             logger         = LoggingFactory.getLogger(getClass());

    @Autowired
    public CustomerTrailControlRepository(DB mongoDB, MetricsClient metricsClient) {
        super("customertrail_control", mongoDB, metricsClient);
    }

    public void addCustomerTrailControlRecord(Map<String, Object> customerTrailControlMap) {
        this.upsertDocument(new BasicDBObject(EMAIL_ID, customerTrailControlMap.get("email_id")), new BasicDBObject(
                customerTrailControlMap));
    }

    public DBObject getCustomerTrailControlRecords(String emailId) {
        return this.getDocument(new BasicDBObject(EMAIL_ID, emailId));
    }

    public List<DBObject> getCustomerTrailControllAll() {
        return this.findDocuments();
    }
    
    public void updateAvaiableCredits(String email, String creditLimits){
        Map<String, Object> map = new HashMap<>();
        map.put(AVAILABLE_CREDITS, creditLimits);
        this.updateDocument(new BasicDBObject(EMAIL_ID, email), new BasicDBObject(map), false);
    }
    
    public void updateAvailableViews(String email, String viewLimits) {
        Map<String, Object> map = new HashMap<>();
        map.put(AVAILABLE_VIEWS, viewLimits);
        this.updateDocument(new BasicDBObject(EMAIL_ID, email), new BasicDBObject(map), false);
    }
    
    public void updateMaxCreditsLimits(String email, String creditLimits){
        Map<String, Object> map = new HashMap<>();
        map.put(MAX_CREDITS, creditLimits);
        this.updateDocument(new BasicDBObject(EMAIL_ID, email), new BasicDBObject(map), false);
    }
    
    public void updateMaxViewLimits(String email, String viewLimits) {
        Map<String, Object> map = new HashMap<>();
        map.put(VIEW_LIMITS, viewLimits);
        this.updateDocument(new BasicDBObject(EMAIL_ID, email), new BasicDBObject(map), false);
    }
    
    @Override
    public void ensureIndexes() {
        //No op
    }
}