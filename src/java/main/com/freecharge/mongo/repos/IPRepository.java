package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 10/12/13
 * Time: 9:51 AM
 * To change this template use File | Settings | File Templates.
 */

@Repository("ipRepository")
public class IPRepository extends MetricsBaseMongoRepository {

    public static final String IP = "ip";
    public static final String USER_ID = "userId";
    public static final String PROMOCODE = "promocode";
    public static final String FREEFUND_CLASS_ID = "freefundClassId";
    public static final String CREATED_ON = "created_on";


    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public IPRepository(DB mongoDB, MetricsClient metricsClient) {
        super("iprecords", mongoDB, metricsClient);
    }


    public void insert(Map<String, Object> data) {
        if (data.get(CREATED_ON) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(String ip, Integer userId, String promocode, Long freefundClassId){

        Map<String, Object> data = new HashMap<String, Object>();
        data.put(IP, ip);
        data.put(USER_ID, userId);
        data.put(PROMOCODE, promocode);
        data.put(FREEFUND_CLASS_ID, freefundClassId);
        data.put("created_on", new Date());
        this.insert(data);
    }

    public void deleteOneRecord(String ip, Integer userId, String promocode, Long freefundClassId) {
        BasicDBObject query = new BasicDBObject().append(FREEFUND_CLASS_ID, freefundClassId).
                append(PROMOCODE, promocode).
                append(IP, ip).
                append(USER_ID, userId);
        this.deleteOneDocument(query);
    }

    public List<DBObject> getData(Long freefundClassId, String ip) {
        BasicDBObject query = new BasicDBObject().append(FREEFUND_CLASS_ID, freefundClassId).append(IP,ip);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public Integer getIPCount(String ip, Long freefundClassId){

        List<DBObject> entries = getData(freefundClassId, ip);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }


    @Override
    public void ensureIndexes() {

            if (collection == null) {
                logger.error("MongoDB not initialized.");
                return;
            }

            logger.info("Ensuring FreefundClass-IP compound index on IPRepository");
            BasicDBObject obj = new BasicDBObject();
            obj.put(FREEFUND_CLASS_ID, 1);
            obj.put(IP, 1);
            collection.ensureIndex(obj);
            logger.info("FreefundClass-IP compound index ensured on IPRepository");

            logger.info("Ensuring TTL index on IPRepository collection");
            // 180 days = 180 * 24 * 60 * 60 = 15552000 seconds
            collection.ensureIndex(new BasicDBObject(CREATED_ON, 1), new BasicDBObject("expireAfterSeconds", 15552000));
            logger.info("Ensuring TTL index on IPRepository done.");

    }
}
