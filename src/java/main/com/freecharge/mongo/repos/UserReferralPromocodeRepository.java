package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository
public class UserReferralPromocodeRepository extends MetricsBaseMongoRepository {

    private final Logger logger = LoggingFactory.getLogger(this.getClass());

    public static final String USER_ID = "userId";
    public static final String PROMOCODE_ID   = "promocodeId";
    public static final String CAMPAIGN_ID   = "campaignId";
    public static final String AMOUNT_REWARDED = "amountRewarded";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";

    @Autowired
    public UserReferralPromocodeRepository(DB mongoDB, MetricsClient metricsClient) {
        super("userReferralPromocodeRepository", mongoDB, metricsClient);
    }

    public boolean insert(String userId, String promocodeId, String campaignId, String amountRewarded) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(USER_ID, userId);
        data.put(PROMOCODE_ID, promocodeId);
        data.put(CAMPAIGN_ID, campaignId);
        data.put(AMOUNT_REWARDED, amountRewarded);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        return this.insert(data);
    }

    public boolean insert(Map<String, Object> data) {
        try{
            if (data.get(CREATED_AT) == null) {
                logger.error("No createdAt field in data, required for TTL. Not storing document to userReferralPromocodeRepository! data : " + data);
                return false;
            }
            this.insertDocumentWithACK(new BasicDBObject(data));
        } catch(Exception e) {
            logger.error("Failed to insert userReferralPromocodeMapping for data : " + data, e);
            return false;
        }

        logger.debug("userReferralPromocodeMapping inserted with data : " + data);
        return true;
    }

    public List<DBObject> getDataByUserIdAndCampaign(String userId, String campaignId) {
        BasicDBObject query = new BasicDBObject().append(CAMPAIGN_ID, campaignId).append(USER_ID, userId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }
    
    public List<DBObject> getDataByUserId(String userId) {
        BasicDBObject query = new BasicDBObject().append(USER_ID, userId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }
    
    public List<DBObject> getDataByPromocodeId(String promocodeId) {
        BasicDBObject query = new BasicDBObject().append(PROMOCODE_ID, promocodeId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getDataByPromocodeAndCampaign(String promocodeId, String campaignId) {
        BasicDBObject query = new BasicDBObject().append(CAMPAIGN_ID, campaignId).append(PROMOCODE_ID, promocodeId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public boolean updateDataForUserIdAndCampaign(String userId, String campaignId, Map<String, Object> updates){
        try{
            updates.put(UPDATED_AT, new Date());
            this.updateDocumentWithACK(new BasicDBObject(CAMPAIGN_ID, campaignId).append(USER_ID, userId), new BasicDBObject(updates), false);

        } catch(Exception e) {
            logger.error("Failed to update userReferralPromocodeMapping for userId : " + userId + ", campaignId : " + campaignId, e);
            return false;
        }

        logger.debug("userReferralPromocodeMapping updated for userId :" + userId + ", campaignId:" + campaignId);
        return true;
    }

    public List<Map<String, String>> getUserReferralPromocodeList(List<DBObject> userReferrelPromocodeDBObjects){
        List<Map<String, String>> userReferrelPromocodeResultList = new LinkedList<>();

        if( userReferrelPromocodeDBObjects!= null && userReferrelPromocodeDBObjects.size() > 0){
            for(DBObject dBObject: userReferrelPromocodeDBObjects){
                userReferrelPromocodeResultList.add(getUserReferralPromocodeMap(dBObject));
            }
        }

        return userReferrelPromocodeResultList;
    }

    public Map<String, String> getUserReferralPromocodeMap(DBObject userReferrelPromocodeDBObject){
        Map<String, String> userReferrelPromocodeMap = new HashMap<String, String>();

        try {
            if( userReferrelPromocodeDBObject!= null){
                Object userId = userReferrelPromocodeDBObject.get(USER_ID);
                Object promocodeId = userReferrelPromocodeDBObject.get(PROMOCODE_ID);
                Object campaignId = userReferrelPromocodeDBObject.get(CAMPAIGN_ID);
                Object amountRewarded = userReferrelPromocodeDBObject.get(AMOUNT_REWARDED);
                Object createdAt = userReferrelPromocodeDBObject.get(CREATED_AT);
                Object updatedAt = userReferrelPromocodeDBObject.get(UPDATED_AT);

                userReferrelPromocodeMap.put(USER_ID, String.valueOf(userId));
                userReferrelPromocodeMap.put(PROMOCODE_ID, String.valueOf(promocodeId));
                userReferrelPromocodeMap.put(CAMPAIGN_ID, String.valueOf(campaignId));
                userReferrelPromocodeMap.put(AMOUNT_REWARDED, String.valueOf(amountRewarded));
                userReferrelPromocodeMap.put(CREATED_AT, String.valueOf(createdAt));
                userReferrelPromocodeMap.put(UPDATED_AT, String.valueOf(updatedAt));
            }
        }
        catch(Exception e){
            logger.error("Error while parsing UserReferralPromocodeRepository data", e);
        }
        return userReferrelPromocodeMap;
    }

    @Override
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring Campaign-UserId compound index on userReferralPromocodeRepository");
        BasicDBObject obj = new BasicDBObject();
        obj.put(CAMPAIGN_ID, 1);
        obj.put(USER_ID, 1);
        collection.ensureIndex(obj);
        logger.info("Campaign-UserId compound index ensured on userReferralPromocodeRepository");

        logger.info("Ensuring Campaign-PromocodeId compound index on userReferralPromocodeRepository");
        obj = new BasicDBObject();
        obj.put(CAMPAIGN_ID, 1);
        obj.put(PROMOCODE_ID, 1);
        collection.ensureIndex(obj);
        logger.info("Campaign-PromocodeId compound index ensured on userReferralPromocodeRepository");

        logger.info("Ensuring PromocodeId index on userReferralPromocodeRepository");
        collection.ensureIndex(new BasicDBObject(PROMOCODE_ID, 1));
        logger.info("PromocodeId index ensured on userReferralPromocodeRepository");
    }

}
