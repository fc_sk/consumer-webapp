package com.freecharge.mongo.repos;


import java.util.*;

import com.freecharge.admin.service.AccessControlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.dao.IAdminUserDAO;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("adminUserRepository")
public class AdminUserRepository extends MetricsBaseMongoRepository implements IAdminUserDAO{


	@Autowired
    public AdminUserRepository(DB mongoDB, MetricsClient metricsClient) {
        super(AccessControlUtil.STRING_PARAMS_ADMIN_USER, mongoDB, metricsClient);
    }
    @Override
    public void addAdminUser(Map<String, Object> user) {
        this.insertDocument(new BasicDBObject(user));
    }
    @Override
    public DBObject getAdminUserByEmail(String email) {
        return this.getDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email));
    }
    @Override
    public void updateAccessToken(String email, String accessToken, Long expiry){
        Map<String, Object> map = new HashMap<>();
        map.put(AccessControlUtil.STRING_PARAMS_ACCESS_TOKEN, accessToken);
        map.put("expiry", expiry);
        this.updateDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email), new BasicDBObject(map), false);
    }
    @Override
    public List<DBObject> findAllAdminUsers(){
        return this.collection.find().sort(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, 1)).toArray();
    }
    @Override
    public void updateComponents(String email, List<String> components){
    	
        this.updateDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email), new BasicDBObject(AccessControlUtil.STRING_PARAMS_COMPONENTS, components),false);
    }
    //update Components and userType 
    @Override
    public void updateUserTypeAndComponents(String email,String userType,List<String> components){	
    	Map<String, Object> map = new HashMap<>();
    	map.put(AccessControlUtil.STRING_PARAMS_COMPONENTS, components);
    	map.put(AccessControlUtil.STRING_PARAMS_USER_TYPE,userType);
    	this.updateDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email),  new BasicDBObject(map),false);
    }
    
    @Override
    public void updateUserType(String email,String userType){
        this.updateDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email),  new BasicDBObject(AccessControlUtil.STRING_PARAMS_USER_TYPE,userType),false);
    }
    
    @Override
    public void updateComponentsAndUserTypeandCategory(String email, List<String> components,String userType,List<String> categories){
    	Map<String, Object> map = new HashMap<>();
    	map.put(AccessControlUtil.STRING_PARAMS_COMPONENTS, components);
    	map.put(AccessControlUtil.STRING_PARAMS_USER_TYPE,userType);
    	map.put(AccessControlUtil.STRING_PARAMS_CATEGORIES,categories);
    	//combining the components list and userType String into a map and sending the map as an update
        this.updateDocument(new BasicDBObject(AccessControlUtil.STRING_PARAMS_EMAIL, email), new BasicDBObject(map),false);
    }
    
    
    @Override
    public void ensureIndexes() {
        //No op
    }
}
