
package com.freecharge.mongo.repos;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores all the cookies related data to MongoDB.
 */
@Repository("cookieRepository")
public class CookieRepository extends MetricsBaseMongoRepository {
	public static final String COOKIE_ID_FIELD_NAME = "cookie_id";
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
    @Autowired
    public CookieRepository(DB mongoDB, MetricsClient metricsClient) {
        super("cookie", mongoDB, metricsClient);
    }

    public DBObject getDataByCookie(String cookie){
        return getDocument(new BasicDBObject(COOKIE_ID_FIELD_NAME, cookie));
    }

    public void updateOrCreateCookieData(String number, Map<String, Object> update){
        this.upsertDocument(new BasicDBObject(COOKIE_ID_FIELD_NAME, number), new BasicDBObject(update));
    }
    
    
    @PostConstruct
    public void ensureIndexes(){
    	if (collection == null){
    		logger.error("MongoDB not initialized.");
    		return;
    	}
    	// Session Id index for faster reads.
    	logger.info("Ensuring cookie_id index.");
    	collection.ensureIndex(new BasicDBObject(COOKIE_ID_FIELD_NAME, "hashed"));
    	logger.info("Ensuring cookie_id index done.");
    }
}
