package com.freecharge.mongo.repos;

import static com.freecharge.mongo.entity.PaymentHistoryEntry.CARD_HASH;
import static com.freecharge.mongo.entity.PaymentHistoryEntry.CREATED_AT;
import static com.freecharge.mongo.entity.PaymentHistoryEntry.PRODUCT_ID;
import static com.freecharge.mongo.entity.PaymentHistoryEntry.USER_ID;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.entity.PaymentHistoryEntry;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;

@Repository("paymentHistoryRepository")
public class PaymentHistoryRepository extends MetricsBaseMongoRepository {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    private static final String GREATER_THAN_EQUAL = "$gte";
    private static final String LESS_THAN          = "$lt";

    @Autowired
    public PaymentHistoryRepository(DB mongoDB, MetricsClient metricsClient) {
        super("paymentHistoryRepository", mongoDB, metricsClient);
    }

    public void insertAmount(PaymentHistoryEntry paymentHistoryEntry) {
        BasicDBObject document = new BasicDBObject(paymentHistoryEntry.asMap());
        this.insertDocument(document);
    }

    public double getTotalTxAmount(String cardHash, int productMasterId, int rollingWindowSeconds) {
        if (rollingWindowSeconds == 0) {
            return 0;
        }
        BasicDBObject query = new BasicDBObject(CARD_HASH, cardHash);
        query.append(PRODUCT_ID, productMasterId);

        DateTime endOfRollingWindow = new DateTime();
        DateTime startOfRollingWindow = endOfRollingWindow.minusSeconds(rollingWindowSeconds);
        query.append(CREATED_AT, new BasicDBObject(GREATER_THAN_EQUAL, startOfRollingWindow.toDate()).append(LESS_THAN,
                endOfRollingWindow.toDate()));

        String map = "function() {emit(this.cardHash,this.amount);}";
        String reduce = "function(key, values){return Array.sum(values)}";
        MapReduceCommand cmd = new MapReduceCommand(this.collection, map, reduce, null,
                MapReduceCommand.OutputType.INLINE, query);
        long startTime = System.currentTimeMillis();
        MapReduceOutput output = this.collection.mapReduce(cmd);
        long endTime = System.currentTimeMillis();
        MetricsClient metricsClient = getMetricsClient();
        if (metricsClient != null) {
            metricsClient.recordEvent(SERVICE_NAME, this.collection.getName(), "MAPREDUCE");
            metricsClient.recordLatency(SERVICE_NAME, this.collection.getName() + "-MAPREDUCE", endTime - startTime);
        }
        double totalAmount = 0;
        if (output.results() != null) {
            for (DBObject obj : output.results()) {
                if (obj.get("value") != null) {
                    totalAmount = (double) obj.get("value");
                }
            }
        }
        return totalAmount;
    }

    public long getTotalTxCount(int productMasterId, int userId, int rollingWindowSeconds) {
        if (rollingWindowSeconds == 0) {
            return 0;
        }
        BasicDBObject matchQuery = new BasicDBObject(PRODUCT_ID, productMasterId);
        matchQuery.append(USER_ID, userId);

        DateTime endOfRollingWindow = new DateTime();
        DateTime startOfRollingWindow = endOfRollingWindow.minusSeconds(rollingWindowSeconds);
        matchQuery.append(CREATED_AT, new BasicDBObject(GREATER_THAN_EQUAL, startOfRollingWindow.toDate())
                .append(LESS_THAN, endOfRollingWindow.toDate()));

        return this.collection.getCount(matchQuery);
    }

    @Override
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        BasicDBObject hashProductIndex = new BasicDBObject();
        hashProductIndex.put(CARD_HASH, 1);
        hashProductIndex.put(PRODUCT_ID, 1);
        logger.info("Starting creation of cardhHash index on paymentHistoryRepository collection.");
        collection.ensureIndex(hashProductIndex);
        logger.info("Completed creation of cardhHash index on paymentHistoryRepository collection.");
        BasicDBObject productUserIDIndex = new BasicDBObject();
        productUserIDIndex.put(USER_ID, 1);
        productUserIDIndex.put(PRODUCT_ID, 1);
        logger.info("Starting creation of productUserId index on paymentHistoryRepository collection.");
        collection.ensureIndex(productUserIDIndex);
        logger.info("Completed creation of productUserId index on paymentHistoryRepository collection.");
        logger.info("Starting creation of TTL index on createdAt for paymentHistoryRepository collection.");
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1), new BasicDBObject("expireAfterSeconds", 86400 * 30));
        logger.info("Completed creation of TTL index on createdAt for paymentHistoryRepository collection.");
    }

}
