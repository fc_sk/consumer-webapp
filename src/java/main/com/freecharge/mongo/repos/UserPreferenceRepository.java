package com.freecharge.mongo.repos;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.app.service.UserPreferenceService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("userPreferenceRepository")
public class UserPreferenceRepository extends MetricsBaseMongoRepository {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    protected UserPreferenceRepository(final DB mongoDB, final MetricsClient metricsClient) {
        super("userPreference", mongoDB, metricsClient);
    }

    public void savePreference(final Map<String, Object> preference) {
        if (!preferenceExists(preference)) {
            preference.put(UserPreferenceService.PREF_ID, getNextUserPrefSequence());
            this.insertDocument(new BasicDBObject(preference));
        }
    }

    
    private boolean preferenceExists(final Map<String, Object> preference) {
    	if (preference == null || preference.get(UserPreferenceService.PREF_ID) == null){
    		return false;
    	}
        BasicDBObject query = new BasicDBObject()
                .append(UserPreferenceService.USEREMAIL_KEY, preference.get(UserPreferenceService.USEREMAIL_KEY))
                .append(UserPreferenceService.PREF_ID, preference.get(UserPreferenceService.PREF_ID))
                .append(UserPreferenceService.TYPE_KEY, preference.get(UserPreferenceService.TYPE_KEY));
        return this.getCount(query) > 0 ? true : false;
    }

    public List<DBObject> getUserPreferencesByUserandType(final String userEmail, final String type) {
        BasicDBObject query = new BasicDBObject().append(UserPreferenceService.USEREMAIL_KEY, userEmail).append(
                UserPreferenceService.TYPE_KEY, type);
        List<DBObject> pref = this.findDocuments(query, null, null);
        return pref;

    }

    public List<DBObject> getAllUserPreferences(final String userEmail) {
        BasicDBObject query = new BasicDBObject().append(UserPreferenceService.USEREMAIL_KEY, userEmail);
        List<DBObject> pref = this.findDocuments(query, null, null);
        return pref;
    }

    public void deletePreference(final String userEmail, final String preferenceId) {
        BasicDBObject query = new BasicDBObject();
        query.put(UserPreferenceService.PREF_ID, preferenceId);
        query.put(UserPreferenceService.USEREMAIL_KEY, userEmail);
        this.deleteDocument(query);
    }

    public void deleleAllUserPreferences(final String userEmail) {
        BasicDBObject query = new BasicDBObject();
        query.put(UserPreferenceService.USEREMAIL_KEY, userEmail);
        this.deleteDocument(query);
    }

    public void upsertPreference(final Map<String, Object> preference) {
    	if (!preferenceExists(preference)) {
            preference.put(UserPreferenceService.PREF_ID, getNextUserPrefSequence());
            this.insertDocument(new BasicDBObject(preference));
        } else {
        	BasicDBObject query = new BasicDBObject()
            .append(UserPreferenceService.USEREMAIL_KEY, preference.get(UserPreferenceService.USEREMAIL_KEY))
            .append(UserPreferenceService.TYPE_KEY, preference.get(UserPreferenceService.TYPE_KEY))
            .append(UserPreferenceService.PREF_ID, preference.get(UserPreferenceService.PREF_ID));
        	this.upsertDocument(query, new BasicDBObject(preference));
        }
    }
    
    public void editPreference(final Map<String, Object> requestMap) {
        BasicDBObject query = new BasicDBObject();
        query.put(UserPreferenceService.PREF_ID, requestMap.get(UserPreferenceService.PREF_ID));
        query.put(UserPreferenceService.USEREMAIL_KEY, requestMap.get(UserPreferenceService.USEREMAIL_KEY));
        BasicDBObject update = new BasicDBObject();
        update.put(UserPreferenceService.USERPREFMAP_KEY, requestMap.get(UserPreferenceService.USERPREFMAP_KEY));
        update.put(UserPreferenceService.USER_RATING_ANDROID_APP,
                requestMap.get(UserPreferenceService.USER_RATING_ANDROID_APP));
        this.updateDocument(query, update, true);
    }

    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        logger.info("Ensuring user_id index.");
        collection.ensureIndex(UserPreferenceService.USEREMAIL_KEY);
        logger.info("Ensuring index done.");
    }

    public String getNextUserPrefSequence() {
        return MongoUtil.getNextId(mongoDB, UserPreferenceService.USER_PREF_ID_SEQUENCE_NAME);
    }

}
