package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This repository maintains the counts of promocodes created and redeemed against campaigns
 **/
@Repository("promocodeCountRepository")
public class PromocodeCountRepository extends MetricsBaseMongoRepository {

    public static final String CAMPAIGN_ID = "campaignId";
    public static final String CREATION_COUNT = "creationCount";
    public static final String REDEMPTION_COUNT = "redemptionCount";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    private static final long ONE_DAY_IN_SECONDS = 86400; // 1 day = 24 * 60 * 60 = 86400 seconds

    private Logger logger = LoggingFactory.getLogger(PromocodeCountRepository.class);

    @Autowired
    public PromocodeCountRepository(final DB mongoDb, final MetricsClient metricsClient) {
        super("promocodeCount", mongoDb, metricsClient);
    }

    public void insert(final Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(final long campaignId, final int creationCount, final int redemptionCount) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(CAMPAIGN_ID, campaignId);
        data.put(CREATION_COUNT, creationCount);
        data.put(REDEMPTION_COUNT, redemptionCount);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public List<DBObject> getTodaysCountsByCampaignId(final long campaignId) {
        BasicDBObject query =
            new BasicDBObject().append(CAMPAIGN_ID, campaignId)
                               .append(CREATED_AT, new BasicDBObject("$gte", FCDateUtil.getStartOfDay(new Date())));
        return this.findDocuments(query, null, null);
    }

    public void updateDataForCampaign(final long campaignId, Map<String, Object> updates){
        BasicDBObject query =
                new BasicDBObject().append(CAMPAIGN_ID, campaignId)
                                   .append(CREATED_AT, new BasicDBObject("$gte", FCDateUtil.getStartOfDay(new Date())));
        updates.put(UPDATED_AT, new Date());
        this.updateDocument(query, new BasicDBObject(updates), false);
        logger.info("Count updated for campaignId:" + campaignId);
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        // campaignId index for faster reads
        logger.info("Ensuring campaignId index on promocodeCountRepository");
        collection.ensureIndex(CAMPAIGN_ID);
        logger.info("Ensuring campaignId index on promocodeCountRepository done");

        logger.info("Ensuring TTL index on promocodeCountRepository collection");
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1),
                               new BasicDBObject("expireAfterSeconds", ONE_DAY_IN_SECONDS));
        logger.info("Ensuring TTL index on promocodeCountRepository done.");
    }
}
