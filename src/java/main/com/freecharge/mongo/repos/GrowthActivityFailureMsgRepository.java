package com.freecharge.mongo.repos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This repository includes all the failure details while processing a reward for growth events
 **/
@Repository("growthActivityFailureMsgRepository")
public class GrowthActivityFailureMsgRepository extends MetricsBaseMongoRepository {

    public static final String USER_ID = "userId";
    public static final String GROWTH_EVENT_ID = "growthEventId";
    public static final String REWARD_FAILURE_REASON = "rewardFailureReason";
    public static final String ORDER_ID = "orderId";
    public static final String RECHARGE_MOBILE_NUMBER = "rechargeMobileNumber";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    private static final long NINETY_DAYS_IN_SECONDS = 7776000; // 90 days = 90 * 24 * 60 * 60 = 7776000 seconds

    private Logger logger = LoggingFactory.getLogger(GrowthActivityFailureMsgRepository.class);

    @Autowired
    public GrowthActivityFailureMsgRepository(final DB mongoDb, final MetricsClient metricsClient) {
        super("growthActivityFailureMsg", mongoDb, metricsClient);
    }

    public void insert(final Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(final long userId, final long growthEventId, final String rewardFailureReason,
                       final String orderId, final String rechargeMobileNumber) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(USER_ID, userId);
        data.put(GROWTH_EVENT_ID, growthEventId);
        data.put(REWARD_FAILURE_REASON, rewardFailureReason);
        data.put(ORDER_ID, orderId);
        data.put(RECHARGE_MOBILE_NUMBER, rechargeMobileNumber);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public List<DBObject> getData(final long growthEventId, final String key, final Object value) {
        BasicDBObject query = new BasicDBObject().append(key, value).append(GROWTH_EVENT_ID, growthEventId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        // UserId index for faster reads
        logger.info("Ensuring userId index.");
        collection.ensureIndex(USER_ID);
        logger.info("Ensuring userId index done.");

        // growthEventId index for faster reads
        logger.info("Ensuring growthEventId index on growthActivityFailureMsgRepository");
        collection.ensureIndex(GROWTH_EVENT_ID);
        logger.info("eventType growthEventId ensured on growthActivityFailureMsgRepository");

        // rechargeMobileNumber index for faster reads
        logger.info("Ensuring rechargeMobileNumber index on growthActivityFailureMsgRepository");
        collection.ensureIndex(RECHARGE_MOBILE_NUMBER);
        logger.info("eventType rechargeMobileNumber ensured on growthActivityFailureMsgRepository");

        // orderId index for faster reads
        logger.info("Ensuring orderId index on growthActivityFailureMsgRepository");
        collection.ensureIndex(ORDER_ID);
        logger.info("eventType orderId ensured on growthActivityFailureMsgRepository");

        logger.info("Ensuring TTL index on GrowthActivityFailureMsgRepository collection");
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1),
                               new BasicDBObject("expireAfterSeconds", NINETY_DAYS_IN_SECONDS));
        logger.info("Ensuring TTL index on GrowthActivityFailureMsgRepository done.");
    }
}
