package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores the Failed promocode details. 
 * This database later used in admin panel customer trail page.
 * 
 * { order_id : "abcd-123", 
 *   promocode : "a1b2c3", 
 *   failedMsg : "network problem"
 * }
 */

@Repository("promocodeFailMsgRepository")
public class PromocodeFailMsgRepository extends MetricsBaseMongoRepository {
    public static final String ORDERID = "order_id";
    public static final String CREATEDAT_NAME = "createdAt";
    private static final String UPDATED_AT = "updated_at";
    private Logger             logger       = LoggingFactory.getLogger(getClass());

    @Autowired
    public PromocodeFailMsgRepository(DB mongoDB, MetricsClient metricsClient) {
        super("promocodeFailMsg", mongoDB, metricsClient);
    }

    public void addPromocodeFailMsg(Map<String, Object> promocodeFailMsgMap) {
        promocodeFailMsgMap.put(CREATEDAT_NAME, new Date());
        promocodeFailMsgMap.put(UPDATED_AT, new Date());
        this.upsertDocument(new BasicDBObject(ORDERID, promocodeFailMsgMap.get("order_id")), new BasicDBObject(
                promocodeFailMsgMap));
    }

    public DBObject getPromocodeFailMsg(String orderId) {
        return this.getDocument(new BasicDBObject(ORDERID, orderId));
    }

    @Override
    public void ensureIndexes() {
        if (collection == null){
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring order_id index on promocodeFailMsg collection");
        collection.ensureIndex(new BasicDBObject(ORDERID, 1));
        logger.info("Ensuring order_id index done.");
        logger.info("Ensuring TTL index on promocodeFailMsg collection");
        // 3 days = 30 * 24 * 60 * 60 = 2592000 seconds
        collection.ensureIndex(new BasicDBObject(CREATEDAT_NAME, 1),
                               new BasicDBObject("expireAfterSeconds", 2592000));
        logger.info("Ensuring TTL index done.");
    }
}
