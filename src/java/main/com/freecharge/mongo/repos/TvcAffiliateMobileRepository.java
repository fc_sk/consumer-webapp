package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This repository is used to store user details like mobile number and email address
 **/
@Repository("tvcAffiliateMobileRepository")
public class TvcAffiliateMobileRepository extends MetricsBaseMongoRepository {

    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String USER_ID = "userId";
    public static final String SMS_SENT_DATE = "smsSentDate";
    public static final String SMS_SENT = "smsSent";
    public static final String EMAIL_ID = "emailId";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";

    private static final int THREE_MONTHS_IN_SECS = 2592000; // 90 * 24 * 60 * 60 = 2592000 seconds

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public TvcAffiliateMobileRepository(final DB mongoDB, final MetricsClient metricsClient) {
        super("tvcAffiliateMobileRepository", mongoDB, metricsClient);
    }

    public void insert(final Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(final String mobileNumber, final Integer userId, final Date smsSentDate, final Boolean smsSent) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(MOBILE_NUMBER, mobileNumber);
        data.put(USER_ID, userId);
        data.put(SMS_SENT_DATE, smsSentDate);
        data.put(SMS_SENT, smsSent);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public void insertEmailId(final String emailId) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(EMAIL_ID, emailId);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public DBObject getData(final String mobileNumber) {
        DBObject result = null;
        BasicDBObject query = new BasicDBObject().append(MOBILE_NUMBER, mobileNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        logger.debug("No of entries with mobileNumber " + mobileNumber + " is " + entries.size());
        if (!FCUtil.isEmpty(entries)) {
            result = entries.get(0);
        }
        return result;
    }

    public DBObject getDataByEmailId(final String emailId) {
        DBObject result = null;
        BasicDBObject query = new BasicDBObject().append(EMAIL_ID, emailId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        logger.debug("No of entries with emailId " + emailId + " is " + entries.size());
        if (!FCUtil.isEmpty(entries)) {
            result = entries.get(0);
        }
        return result;
    }

    public void updateData(final String mobileNumber, final Map<String, Object> update) {
        this.upsertDocument(new BasicDBObject(MOBILE_NUMBER, mobileNumber), new BasicDBObject(update));
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        // Mobile number index for faster reads.
        logger.info("Ensuring mobile number index.");
        collection.ensureIndex(MOBILE_NUMBER);
        logger.info("Ensuring mobile number index done.");

        logger.info("Ensuring SmsSentStatus index on tvcAffiliateMobileRepository");
        collection.ensureIndex(SMS_SENT);
        logger.info("SmsSentStatus index ensured on tvcAffiliateMobileRepository");

        logger.info("Ensuring TTL index on TvcAffiliateMobileRepository collection");
        collection.ensureIndex(new BasicDBObject(UPDATED_AT, 1),
                               new BasicDBObject("expireAfterSeconds", THREE_MONTHS_IN_SECS));
        logger.info("Ensuring TTL index on TvcAffiliateMobileRepository done.");
    }
}
