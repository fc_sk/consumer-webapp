package com.freecharge.mongo.repos;

import com.freecharge.mongo.repos.ComponentRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.AdminCacheManager;
import com.freecharge.admin.dao.IAdminComponentDAO;
import com.freecharge.admin.entity.AdminComponentEntity;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Primary
@Repository("componentCachingRepository")
public class ComponentCachingRepository implements IAdminComponentDAO {

    @Autowired
    private ComponentRepository componentRepository;

    @Autowired
    private AdminCacheManager adminCacheManager;

    private Logger logger = LoggingFactory.getLogger(this.getClass());

    @Override
    public void addComponent(Map<String, Object> component) {
        this.componentRepository.addComponent(component);
        AdminComponentEntity adminComponentEntity = new AdminComponentEntity(
                new BasicDBObject(component));
        try {
            this.adminCacheManager.setComponentById(adminComponentEntity);
        } catch (RuntimeException e) {
            logger.error("Error occured while adding component to cache:"
                    + component.get("name"), e);
        }
    }
    // update component
    @Override
    public void updateComponentUsingName(String queryName, String name,
            String URL, String active, List<String> restPatterns) {
       
        try {
            this.adminCacheManager.deleteComponentById(queryName);
            this.componentRepository.updateComponentUsingName(queryName, name, URL,
                    active, restPatterns);
        } catch (RuntimeException e) {
            logger.error("Error occured while updating component to cache"
                    + queryName, e);
        }
    }

    // remove component
    @Override
    public void deleteComponent(String name) {
        this.componentRepository.deleteComponent(name);
        try {

            this.adminCacheManager.deleteComponentById(name);
        } catch (RuntimeException e) {
            logger.error("Error occured while deleting component to cache:"
                    + name, e);
        }
    }

    // getComponent as per name
    @Override
    public DBObject getComponentUsingName(String name) {
        AdminComponentEntity adminComponentEntity = null;
        try {
            adminComponentEntity = this.adminCacheManager
                    .getComponentById(name);
            logger.info("No Error occured while fetching component from cache:"
                    + name);
        } catch (RuntimeException e) {
            logger.error("Error occured while fetching component from cache:"
                    + name, e);
        }
        if (adminComponentEntity == null) {
            adminComponentEntity = new AdminComponentEntity(
                    this.componentRepository.getComponentUsingName(name));
            if (adminComponentEntity != null) {
                try {
                    adminCacheManager.setComponentById(adminComponentEntity);
                } catch (RuntimeException e) {
                    logger.error(
                            "Error occured while setting component from cache:"
                                    + name, e);
                }
            }
        }
        return adminComponentEntity.getComponent();
    }
    // getAll Components
    @Override
    public List<DBObject> findAllComponents() {
        return this.componentRepository.findAllComponents();
    }

    @Override public List<DBObject> findAllComponentsForRestPattern(String restPattern) {
        return this.componentRepository.findAllComponentsForRestPattern(restPattern);
    }

    // inserting a list of docs:component
    @Override
    public void addComponentList(
            List<? extends HashMap<String, String>> components) {
        this.componentRepository.addComponentList(components);
    }
}
