package com.freecharge.mongo.repos;
import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.affiliate.order.AffiliateOrder;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository handles all the affiliate order related queries to MongoDB.
 * Any affiliate order related query functions should be added here.
 */
@Repository("affiliateOrderRepository")
public class AffiliateOrderRepository extends MetricsBaseMongoRepository {
	public static final String ORDER_ID_FIELD_NAME = "orderId";
	public static final String AFFILIATE_ID_FIELD_NAME = "affiliateId";
	public static final String ORDER_ATTRIBUTES_FIELD_NAME = "order_attributes";
	public static final String ORDER_CREATED_TIME_FIELD_NAME = "order_created_time";
	public static final String FIELD_NAME_ORDER_ATTRIBUTES_USER_ID = "user_id";
	public static final String AFFILIATE_ORDER_ID_SEQUENCE_NAME = "affiliate_order_id_sequence";
	
	private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public AffiliateOrderRepository(DB mongoDB, MetricsClient metricsClient) {
        super("affiliate_order", mongoDB, metricsClient);
    }

    public DBObject getOrderByOrderId(String orderId){
    	collection.setObjectClass(AffiliateOrder.class);
        return getDocument(new BasicDBObject(ORDER_ID_FIELD_NAME, orderId));
    }

    public void updateOrCreateAffiliateOrder(String orderId, BasicDBObject orderData){
        upsertDocument(new BasicDBObject(ORDER_ID_FIELD_NAME, orderId), orderData);
    }
    
    public void deleteAffiliateOrder(String orderId){
    	deleteDocument(new BasicDBObject(ORDER_ID_FIELD_NAME, orderId));
    }
    
    public String getNextAffiliateOrderSequence(){
    	return MongoUtil.getNextId(mongoDB, AFFILIATE_ORDER_ID_SEQUENCE_NAME);
    }
    
    @PostConstruct
    public void ensureIndexes(){
    	if (collection == null){
    		logger.error("MongoDB not initialized.");
    		return;
    	}
    	// Session Id index for faster reads.
    	logger.info("Ensuring affiliate orders order_id index.");
    	collection.ensureIndex(ORDER_ID_FIELD_NAME);
    	logger.info("Ensuring index done.");
    	logger.info("Ensuring affiliate orders affiliateId index.");
    	collection.ensureIndex(AFFILIATE_ID_FIELD_NAME);
    	logger.info("Ensuring index done.");
    }
}
