package com.freecharge.mongo.repos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.AdminCacheManager;
import com.freecharge.admin.dao.IAdminUserDAO;
import com.freecharge.admin.entity.AdminUserEntity;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.AdminUserRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Primary
@Repository("adminUserCachingRepository")
public class AdminUserCachingRepository implements IAdminUserDAO {

    @Autowired
    AdminUserRepository adminUserRepository;

    @Autowired
    AdminCacheManager adminCacheManager;

    private Logger logger = LoggingFactory.getLogger(this.getClass());

    @Override
    public void addAdminUser(Map<String, Object> user) {
        this.adminUserRepository.addAdminUser(user);
        DBObject cachedUser=new BasicDBObject(user);
        try{
            AdminUserEntity adminUserEntity=new AdminUserEntity(cachedUser);
            //setting the cache 
            adminCacheManager.setAdminUserById(adminUserEntity);
        }catch(RuntimeException e){
            logger.error("error while setting admin user in cache: ", e);            
        }
    }
    @Override
    public DBObject getAdminUserByEmail(String email) {

        AdminUserEntity adminUserEntity=null;
        try{
            adminUserEntity=this.adminCacheManager.getAdminUserById(email);
        }catch(RuntimeException e){
            logger.error("error while getting admin user in cache: ", e);
        }
        if( adminUserEntity == null ){
            adminUserEntity=new AdminUserEntity( this.adminUserRepository.getAdminUserByEmail(email) );
            if(adminUserEntity!=null){	
                try{
                    adminCacheManager.setAdminUserById(adminUserEntity);
                }catch (RuntimeException e){
                    logger.error("Error while setting admin user in cache:" + email, e);
                }
            }
        }
        return adminUserEntity.getUser();
    }
    
    @Override
    public void updateAccessToken(String email, String accessToken, Long expiry){
        try{
            this.adminCacheManager.deleteAdminUserById(email);
            this.adminUserRepository.updateAccessToken(email, accessToken, expiry);
        }
        catch(RuntimeException e){
            logger.error("error while updating access token of admin user in cache: "+email+":",e);
        }
    }
    
    @Override
    public List<DBObject> findAllAdminUsers(){	
        return this.adminUserRepository.findAllAdminUsers();
    }
    
    @Override
    public void updateComponents(String email, List<String> components){
        AdminUserEntity adminUserEntity=this.adminCacheManager.getAdminUserById(email);
        try{
            this.adminCacheManager.deleteAdminUserById(email); 
            this.adminUserRepository.updateComponents(email, components);
        }
        catch(RuntimeException e){
            logger.error("Error occured while deleting updated admin user in cache: "+email,e);
        }
    }
    
    @Override
    public void updateComponentsAndUserTypeandCategory(String email, List<String> components,String userType,List<String> categories){
        //combining the components list and userType String into a map and sending the map as an update
        try{
            this.adminCacheManager.deleteAdminUserById(email); 
            this.adminUserRepository.updateComponentsAndUserTypeandCategory(email, components, userType, categories);
        }
        catch(RuntimeException e){
            logger.error("Error while Deleting user from cache : "+email,e);
        }
    }
    
    @Override
    public void updateUserTypeAndComponents(String email, String userType,List<String> components){
        try{
            this.adminCacheManager.deleteAdminUserById(email); 
            this.adminUserRepository.updateUserTypeAndComponents(email,userType,components);
        }
        catch(RuntimeException e){
            logger.error("Error while Deleting user from cache : "+email,e);
        } 
    }
    
    @Override
    public void updateUserType(String email, String userType){
        try{
            this.adminCacheManager.deleteAdminUserById(email); 
            this.adminUserRepository.updateUserType(email,userType);
        }
        catch(RuntimeException e){
            logger.error("Error while Deleting user from cache : "+email,e);
        } 
    }
}
