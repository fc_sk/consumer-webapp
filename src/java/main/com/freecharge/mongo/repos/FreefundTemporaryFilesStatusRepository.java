package com.freecharge.mongo.repos;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class FreefundTemporaryFilesStatusRepository extends MetricsBaseMongoRepository{

    public static final String NUMBER_OF_FILES = "numberOfFiles";
    public static final String FILE_CREATION_TIMESTAMP = "fileCreationTimeStamp";
    public static final String CAMPAIGN_ID = "campaignId";
    public static final String STATUS = "status";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    public static final String CREATED_BY_EMAILID = "createdByEmailId";
    public static final String NUMBER_OF_CODES = "numberOfCodes";
    public static final String PREFIX = "prefix";
    public static final String CODE_LENGTH = "codeLength";
    public static final String FILE_COUNT = "fileCount";

    private final Logger logger = LoggingFactory.getLogger(this.getClass());


    @Autowired
    protected FreefundTemporaryFilesStatusRepository(DB mongoDB, MetricsClient metricsClient) {
        super("freefundTemporaryFilesStatusRepository", mongoDB, metricsClient);
    }

    public boolean insert(long campaignId, int numberOfFiles, String prefix, int codeLength, int numberOfCodes, long fileCreationTimeStamp, String status, String emailId) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(CAMPAIGN_ID, campaignId);
        data.put(NUMBER_OF_FILES, numberOfFiles);
        data.put(PREFIX, prefix);
        data.put(CODE_LENGTH, codeLength);
        data.put(NUMBER_OF_CODES, numberOfCodes);
        data.put(FILE_CREATION_TIMESTAMP, fileCreationTimeStamp);
        data.put(STATUS, status);
        data.put(CREATED_BY_EMAILID, emailId);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        return this.insert(data);
    }

    private boolean insert(Map<String, Object> data) {
        try{
            if (data.get(CREATED_AT) == null) {
                logger.error("No createdAt field in data, required for TTL. Not storing document to FreefundTemporaryFilesStatusRepository! data : " + data);
                return false;
            }
            this.insertDocumentWithACK(new BasicDBObject(data));
        } catch(Exception e) {
            logger.error("Failed to insert FreefundTemporaryFilesStatusRepository for data : " + data, e);
            return false;
        }

        logger.debug("FreefundTemporaryFilesStatusRepository inserted with data : " + data);
        return true;
    }
    
    public boolean updateStatus(long time, String status) {
        try{
            Map<String, Object> updates = new HashMap<>();
            updates.put(UPDATED_AT, new Date());
            updates.put(STATUS, status);
            this.updateDocumentWithACK(new BasicDBObject(FILE_CREATION_TIMESTAMP, time),
                    new BasicDBObject(updates), false);
        } catch(Exception e) {
            logger.error("Failed to update status for time : " + time + ", status : " +
                    status, e);
            return false;
        }

        logger.debug("FreefundTemporaryFilesStatus updated for time :" + time + ", status:" + status);
        return true;
    }


    public boolean updateFileCount(long time, String fileCount) {
        try{
            Map<String, Object> updates = new HashMap<>();
            updates.put(UPDATED_AT, new Date());
            updates.put(FILE_COUNT, fileCount);
            this.updateDocumentWithACK(new BasicDBObject(FILE_CREATION_TIMESTAMP, time),
                    new BasicDBObject(updates), false);
        } catch(Exception e) {
            logger.error("Failed to update file count for time : " + time + ", count : " +
                    fileCount, e);
            return false;
        }

        logger.debug("FreefundTemporaryFilesStatus updated for time :" + time + ", count:" + fileCount);
        return true;
    }

    public List<Map<String, String>> getDataByStatus(String status) {
        BasicDBObject query = new BasicDBObject().append(STATUS, status);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return getCreatedFilesObjectMap(entries);
    }

    public List<Map<String, String>> getDataByCampaignId(long campaignId) {
        BasicDBObject query = new BasicDBObject().append(CAMPAIGN_ID, campaignId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return getCreatedFilesObjectMap(entries);
    }

    private List<Map<String, String>> getCreatedFilesObjectMap(List<DBObject> entries) {
        List<Map<String, String>> temporaryFilesStatusMapList = new LinkedList<>();

        try {
            if( entries!= null){
                for (DBObject entry : entries) {
                    Object fileCreationTimestamp = entry.get(FILE_CREATION_TIMESTAMP);
                    Object numberOfFiles = entry.get(NUMBER_OF_FILES);
                    Object numberOfCodes = entry.get(NUMBER_OF_CODES);
                    Object prefix = entry.get(PREFIX);
                    Object codeLength = entry.get(CODE_LENGTH);
                    Object campaignId = entry.get(CAMPAIGN_ID);
                    Object status = entry.get(STATUS);
                    Object createdByEmailId = entry.get(CREATED_BY_EMAILID);
                    Object createdAt = entry.get(CREATED_AT);
                    Object updatedAt = entry.get(UPDATED_AT);
                    Object fileCount = entry.get(FILE_COUNT);

                    Map<String, String> temporaryFilesStatusMap = new HashMap<String, String>();

                    temporaryFilesStatusMap.put(FILE_CREATION_TIMESTAMP, String.valueOf(fileCreationTimestamp));
                    temporaryFilesStatusMap.put(NUMBER_OF_FILES, String.valueOf(numberOfFiles));
                    temporaryFilesStatusMap.put(NUMBER_OF_CODES, String.valueOf(numberOfCodes));
                    temporaryFilesStatusMap.put(PREFIX, String.valueOf(prefix));
                    temporaryFilesStatusMap.put(CODE_LENGTH, String.valueOf(codeLength));
                    temporaryFilesStatusMap.put(CAMPAIGN_ID, String.valueOf(campaignId));
                    temporaryFilesStatusMap.put(STATUS, String.valueOf(status));
                    temporaryFilesStatusMap.put(CREATED_BY_EMAILID, String.valueOf(createdByEmailId));
                    temporaryFilesStatusMap.put(CREATED_AT, String.valueOf(createdAt));
                    temporaryFilesStatusMap.put(UPDATED_AT, String.valueOf(updatedAt));
                    temporaryFilesStatusMap.put(FILE_COUNT, String.valueOf(fileCount));

                    temporaryFilesStatusMapList.add(temporaryFilesStatusMap);
                }
            }
        }
        catch(Exception e){
            logger.error("Error while parsing FreefundTemporaryFilesStatusRepository data", e);
        }
        return temporaryFilesStatusMapList;
    }

    @Override
    public void ensureIndexes() {
        logger.info("Ensuring TTL index on FreefundTemporaryFilesStatusRepository");
        // 14 days = 14 * 24 * 60 * 60 = 1209600 seconds
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1), new BasicDBObject("expireAfterSeconds", 1209600));
        logger.info("TTL index ensured on FreefundTemporaryFilesStatusRepository");
    }

    public List<Map<String, String>> getDataByCampaignIdAndStatus(Long classId, String status) {
        BasicDBObject query = new BasicDBObject().append(CAMPAIGN_ID, classId).append(STATUS, status);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return getCreatedFilesObjectMap(entries);
    }
}
