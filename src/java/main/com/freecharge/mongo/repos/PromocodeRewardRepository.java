package com.freecharge.mongo.repos;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores promocode reward per user - hence its indexed with userId It also has a TTL index of 30 days
 * 
 * { userId: "88123", promocode: "SKDSDDSDS", ffClass : "466",createdAt: new Date('July 22, 2013: 13:00:00') }
 * 
 */
@Repository("promocodeRewardRepository")
public class PromocodeRewardRepository extends MetricsBaseMongoRepository {

    public static final String USERID    = "userId";
    public static final String ORDERID = "orderId";
    public static final String PROMOCODE = "promocode";
    public static final String VALUE = "value";
    public static final String FFCLASS   = "ffClass";
    public static final String CREATEDAT = "createdAt";
    public static final String VALIDUPTO = "validUpto";
    public static final String HASUSED = "hasused";

    private final Logger       logger    = LoggingFactory.getLogger(getClass());

    @Autowired
    public PromocodeRewardRepository(DB mongoDB, MetricsClient metricsClient) {
        super("promocodeReward", mongoDB, metricsClient);
    }

    public DBObject getDataByUserId(Integer userId) {
        return getDocument(new BasicDBObject(USERID, userId));
    }

    public void updateOrCreateUserIdData(Integer userId, Map<String, Object> data) {
        if (data.get(CREATEDAT) == null) {
            logger.error("No createdAt field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.upsertDocument(new BasicDBObject(USERID, userId), new BasicDBObject(data));
    }
        
    public Map<String, Object> doesRewardExist(Integer userId) {
        Calendar todayCal = Calendar.getInstance();
        Calendar validUptoCal = Calendar.getInstance();
        DBObject dbObject = getDocument(new BasicDBObject(USERID, userId));
        if (dbObject == null) {
           return null;
        }
        Object validUptoObj = dbObject.get(VALIDUPTO);
        if (validUptoObj == null) {
            return null;
        } else {
            Date validUpto = (Date) validUptoObj;
            validUptoCal.setTime(validUpto);
            if(todayCal.before(validUptoCal)) {
                logger.debug("Valid reward "+(String)dbObject.get(PROMOCODE)+" exist for userId : " + userId);
                
                return (Map<String, Object>)dbObject;
            } 
        }
        return null;
    }
    
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring userId index on promocodeReward collection");
        collection.ensureIndex(new BasicDBObject(USERID, 1));
        logger.info("Ensuring userId index done.");

        logger.info("Ensuring TTL index on promocodeReward collection");
        // 30 days = 30 * 24 * 60 * 60 = 2592000 seconds
        collection.ensureIndex(new BasicDBObject(CREATEDAT, 1), new BasicDBObject("expireAfterSeconds", 2592000));
        logger.info("Ensuring TTL index done.");
    }
}
