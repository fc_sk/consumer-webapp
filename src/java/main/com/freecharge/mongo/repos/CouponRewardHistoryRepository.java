package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This repository has the history of coupons rewarded to users
 **/
@Repository("couponRewardHistoryRepository")
public class CouponRewardHistoryRepository extends MetricsBaseMongoRepository {

    public static final String USER_ID = "userId";
    public static final String GROWTH_EVENT_ID = "growthEventId";
    public static final String GROWTH_EVENT_TYPE = "growthEventType";
    public static final String RECHARGE_MOBILE_NUMBER = "rechargeMobileNumber";
    public static final String COUPON_CODE = "couponCode";
    public static final String FREEFUNDCLASS_ID = "freefundClassId";
    public static final String ORDER_ID = "orderId";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    private static final long NINETY_DAYS_IN_SECONDS = 7776000; // 90 days = 90 * 24 * 60 * 60 = 7776000 seconds

    private Logger logger = LoggingFactory.getLogger(CouponRewardHistoryRepository.class);

    @Autowired
    public CouponRewardHistoryRepository(final DB mongoDb, final MetricsClient metricsClient) {
        super("couponRewardHistory", mongoDb, metricsClient);
    }

    public void insert(final Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(final long userId, final Long growthEventId, final String growthEventType,
                       final String rechargeMobileNumber, final String couponCode, final Long freefundClassId,
                       final String orderId) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(USER_ID, userId);
        data.put(GROWTH_EVENT_ID, growthEventId);
        data.put(GROWTH_EVENT_TYPE, growthEventType);
        data.put(RECHARGE_MOBILE_NUMBER, rechargeMobileNumber);
        data.put(COUPON_CODE, couponCode);
        data.put(FREEFUNDCLASS_ID, freefundClassId);
        data.put(ORDER_ID, orderId);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public List<DBObject> getCouponRewardHistoryByEventAndMobileNumber(final long growthEventId, final String mobileNumber) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_ID, growthEventId).append(RECHARGE_MOBILE_NUMBER, mobileNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getCouponRewardHistoryByEventAndMobileNumber(final String growthEventType, final String mobileNumber) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_TYPE, growthEventType).append(RECHARGE_MOBILE_NUMBER, mobileNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getCouponRewardHistoryByMobileNumber(final String mobileNumber) {
        BasicDBObject query = new BasicDBObject().append(RECHARGE_MOBILE_NUMBER, mobileNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        // RechargeMobileNumber index for faster reads
        logger.info("Ensuring rechargeMobileNumer index on couponRewardHistoryRepository");
        collection.ensureIndex(RECHARGE_MOBILE_NUMBER);
        logger.info("Ensuring rechargeMobileNumber index done on couponRewardHistoryRepository");

        // growthEventId index for faster reads
        logger.info("Ensuring growthEventId index on couponRewardHistoryRepository");
        collection.ensureIndex(GROWTH_EVENT_ID);
        logger.info("eventType growthEventId ensured on couponRewardHistoryRepository");

        // growthEventType index for faster reads
        logger.info("Ensuring growthEventType index on couponRewardHistoryRepository");
        collection.ensureIndex(GROWTH_EVENT_TYPE);
        logger.info("eventType growthEventType ensured on couponRewardHistoryRepository");

        // freefundClassId index for faster reads
        logger.info("Ensuring freefundClassId index on couponRewardHistoryRepository");
        collection.ensureIndex(FREEFUNDCLASS_ID);
        logger.info("eventType freefundClassId ensured on couponRewardHistoryRepository");

        logger.info("Ensuring TTL index on couponRewardHistoryRepository collection");
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1),
                               new BasicDBObject("expireAfterSeconds", NINETY_DAYS_IN_SECONDS));
        logger.info("Ensuring TTL index on couponRewardHistoryRepository done.");
    }
}
