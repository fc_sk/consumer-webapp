package com.freecharge.mongo.repos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.entity.RechargePlan;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("rechargePlanModerationRepository")
// Repository used to save recharge plans which requires moderation.
public class RechargePlanModerationRepository extends MetricsBaseMongoRepository {
	// 'recharge_plan_id' from the in_recharge_plans table
	public final static String ID = "_id";
	public final static String AMOUNT = "amount";
	public final static String TALKTIME = "talktime";
	public final static String NAME = "name";
	public final static String OPERATOR = "operatorMasterId";
	public final static String CIRCLE = "circleMasterId";
	public final static String SOURCE = "source";
	public final static String DESC = "description";
	public final static String VALIDITY = "validity";
	public final static String CAUSE = "cause";
	
	//Sources
	public final String SOURCE_IREFF = "IREFF";
	public final String SOURCE_DATAWEAVE = "DATAWEAVE";
	
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	 @Autowired
     public RechargePlanModerationRepository(DB mongoDB, MetricsClient metricsClient) {
		 super("rechargePlan", mongoDB, metricsClient);
	 }
	 
	 public boolean addRechargePlan(RechargePlan rechargePlan, String cause) throws RuntimeException {
		 try{
			 Map<String, Object> rp = new HashMap<>();
			 rp.put(ID, rechargePlan.getRechargePlanId());
			 rp.put(AMOUNT, rechargePlan.getAmount().toString());
			 rp.put(TALKTIME,(rechargePlan.getTalktime() == null) ? "UNKNOWN":  rechargePlan.getTalktime().toString());
			 rp.put(NAME, rechargePlan.getName());
			 rp.put(OPERATOR, rechargePlan.getOperatorMasterId());
			 rp.put(CIRCLE, rechargePlan.getCircleMasterId());
			 rp.put(DESC, rechargePlan.getDescription());
			 rp.put(VALIDITY, rechargePlan.getValidity());
			 rp.put(CAUSE, cause);
			 if(rechargePlan.getIreffID()!=null)
				 rp.put(SOURCE, SOURCE_IREFF + "-" + rechargePlan.getIreffID());
			 else
				 rp.put(SOURCE,null);
			 this.insertDocument(new BasicDBObject(rp));
			 return true;
		 } catch(Exception e) {
			 logger.error("Unable to Push to moderation Queue : " + e.toString());
			 return false;
		 }
	 }
	 
	 public void removeRechargePlan(int rechargePlanID) throws RuntimeException {
		 BasicDBObject document = new BasicDBObject();
		 document.put(ID, rechargePlanID);
		 if(collection.getCount(document) == 0){
			 IllegalStateException e = new IllegalStateException("Unknown planID");
			 throw e;
		 }
		 collection.remove(document);
	 }
	 
	 public void updateRechargePlan(BasicDBObject searchQuery, BasicDBObject userSets) throws RuntimeException {
		 try{
			 this.collection.update(searchQuery, userSets);
		 }catch(Exception e){
			 logger.error("Unable to update Moderation Queue : " + e.toString());
			 throw e;
		 }
	 }
	 
	 public List<DBObject> getAllRechargePlan() throws RuntimeException {
		 try{
			 return collection.find().toArray();
		 }catch(Exception e){
			 logger.error("Unable to fetch from Moderation Queue : " + e.toString());
			 throw e;
		 }
	 }
	 
	 public DBObject getRechargePlan(int planID) throws RuntimeException {
		 try{
			 BasicDBObject document = new BasicDBObject();
			 document.put(ID, planID);
			 if(collection.getCount(document) == 0){
				 IllegalStateException e = new IllegalStateException("Unknown planID");
				 throw e;
			 }
			 return collection.findOne(document);
		 }catch(RuntimeException e){
			 logger.error("Unable to fetch from Moderation Queue : " + e.toString());
			 throw e;
		 }
	 }

    @Override
    public void ensureIndexes() {
        //No op
    }
}
