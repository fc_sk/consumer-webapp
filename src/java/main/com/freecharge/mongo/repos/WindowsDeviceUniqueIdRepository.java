package com.freecharge.mongo.repos;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("windowsDeviceUniqueIdRepository")
public class WindowsDeviceUniqueIdRepository extends MetricsBaseMongoRepository{

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    public WindowsDeviceUniqueIdRepository(DB mongoDB, MetricsClient metricsClient) {
        super("windowsDeviceUniqueIdRepository", mongoDB, metricsClient);
    }

    public boolean insert(String campaignKey, Long campaignId, String deviceUniqueId, Integer userId,
            String freefundCode, String status) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(campaignKey, campaignId);  
        data.put(FCConstants.DEVICE_UNIQUE_ID, deviceUniqueId);
        data.put(FCConstants.USERID, userId);        
        data.put(FCConstants.STATUS, status);
        data.put(FCConstants.CREATED_ON, new Date());
        return this.insert(data);
    }

    private boolean insert(Map<String, Object> data) {
        if (data.get(FCConstants.CREATED_ON) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return false;
        }
        try {
            this.insertDocumentWithACK(new BasicDBObject(data));
        } catch (Exception e) {
            logger.error("Error inserting into repository windowsDeviceUniqueIdRepository ", e);
            return false;
        }
        return true;
    }

    public Integer getDeviceUniqueIdOnlyCount(String campaignKey, Long campaignId, String deviceUniqueId, String status){

        List<DBObject> entries = new ArrayList<>();
        entries = getForSingleKey(campaignKey, campaignId, FCConstants.DEVICE_UNIQUE_ID, deviceUniqueId, status);    

        if (entries != null) {
            return entries.size();
        }

        return 0;
    }

    public List<DBObject> getForSingleKey(String campaignKey, Long campaignId, String key, String value, String status) {
        BasicDBObject query = new BasicDBObject().append(campaignKey, campaignId).append(key,
                value).append(FCConstants.STATUS, status);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    
    /*Indexes to be on (freefundClassId,DeviceUniqueId,Status), (freefundclassId, UserId) and userId and promcode*/
    @Override
    public void ensureIndexes() {
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for deviceUniqueId, status, freefundClassId");
        BasicDBObject obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUND_CLASS_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.DEVICE_UNIQUE_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.STATUS, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for deviceUniqueId, status, " +
                "freefundClassId is done");

        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for userId, freefundClassId");
        obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUND_CLASS_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.USERID, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for userId, freefundClassId is done");

        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for userId and freefundCode");
        obj = new BasicDBObject();
        obj.put(FCConstants.USERID, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        obj = new BasicDBObject();
        obj.put(FCConstants.PROMOCODE, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for userId and freefundCode is done");
        
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for eventId, deviceUniqueId, status");
        obj = new BasicDBObject();
        obj.put(FCConstants.GROWTH_EVENT_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.DEVICE_UNIQUE_ID, FCConstants.MONGO_ASC_INDEX);
        obj.put(FCConstants.STATUS, FCConstants.MONGO_ASC_INDEX);
        collection.ensureIndex(obj);
        logger.info("Ensuring index on windowsDeviceUniqueIdRepository collection for eventId, deviceUniqueId, "
                + "status is done");

    }
}
