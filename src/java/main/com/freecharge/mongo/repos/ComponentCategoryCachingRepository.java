package com.freecharge.mongo.repos;


import com.freecharge.admin.dao.IAdminCategoryDAO;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.AdminCacheManager;
import com.freecharge.admin.entity.AdminCategoryEntity;
import com.freecharge.admin.entity.AdminUserEntity;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.mongo.repos.ComponentCategoryRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Primary
@Repository("componentCategoryCachingRepository")
public class ComponentCategoryCachingRepository implements IAdminCategoryDAO {
    @Autowired
    private ComponentCategoryRepository componentCategoryRepository;

    @Autowired
    private AdminCacheManager adminCacheManager;

    private Logger logger = LoggingFactory.getLogger(this.getClass());
    private static final String STRING_PARAMS_CODE="code";
    private static final String STRING_PARAMS_EMAIL="email";
    private static final String STRING_PARAMS_SESSEION_DATA_MAP="Session Data Map";
    private static final String STRING_PARAMS_STATE="state";
    private static final String STRING_PARAMS_LOGGED_IN="loggedIn";
    private static final String STRING_PARAMS_USER_OBJECTS="User Object";
    private static final String STRING_PARAMS_USERTYPE="userType";
    private static final String STRING_PARAMS_CATEGORY_NAME="categoryName";
    private static final String STRING_PARAMS_CATEGORIES="categories";
    private static final String STRING_PARAMS_COMPONENTS="components";
    private static final String STRING_PARAMS_MAP="map";
    private static final String STRING_PARAMS_USER="user";
    private static final String STRING_PARAMS_URL="URL";
    private static final String STRING_PARAMS_NAME="name";
    private static final String STRING_PARAMS_ACCESS_TOKEN="access_token";
    private static final String STRING_PARAMS_EXPIRES_IN="expires_in";
    private static final String STRING_COMPONENT_FIRST_LETTERS="componentFirstLetters";

    @Override
    public void addComponentCategory(Map<String, Object> category){
        AdminCategoryEntity adminCategoryEntity=new AdminCategoryEntity(new BasicDBObject(category));
        try{
            this.componentCategoryRepository.addComponentCategory(category);
            this.adminCacheManager.setCategoryById(adminCategoryEntity);
            logger.info("The Category was added to cache:"+category.get(STRING_PARAMS_CATEGORY_NAME));
        }
        catch(RuntimeException e){
            logger.error("The Category was not added to cache:"+category.get(STRING_PARAMS_CATEGORY_NAME),e);
        }
    }

    @Override
    public List<DBObject> findAllCategories(){
        return this.componentCategoryRepository.findAllCategories();
    }

    @Override
    public void deleteCategory(String categoryName){
        try{
            this.componentCategoryRepository.deleteCategory(categoryName);
            this.adminCacheManager.deleteCategoryById(categoryName);
            logger.info("The Category was deleted post deletion from database:"+categoryName);
        }catch(RuntimeException e){
            logger.error("The Category was not deleted from cache :"+categoryName,e);
        }

    }

    @Override
    public void updateCategory(String categoryName, Map<String, Object> category ){
        this.componentCategoryRepository.updateCategory(categoryName, category);
        AdminCategoryEntity adminCategoryEntity=this.adminCacheManager.getCategoryById(categoryName);
        try{
            this.adminCacheManager.deleteCategoryById(categoryName);
            logger.info("category was deleted post update in DataBase: "+categoryName);
        }
        catch(RuntimeException e){
            logger.error("Error occured while deleting Category in cache post update in DateBase: "+categoryName+":",e);
        }
    }

    @Override
    public DBObject getCategoryUsingName(String categoryName){
        AdminCategoryEntity adminCategoryEntity=null;
        try{
            adminCategoryEntity=this.adminCacheManager.getCategoryById(categoryName);
            logger.info("No error occured while fetching Category from cache: "+categoryName);
        }catch(RuntimeException e){
            logger.error("Error occured while fetching Category from cache: "+categoryName+":",e);
        }
        if(adminCategoryEntity==null){
            adminCategoryEntity=new AdminCategoryEntity(this.componentCategoryRepository.getCategoryUsingName(categoryName));
            if(adminCategoryEntity != null){
                try{
                    adminCacheManager.setCategoryById(adminCategoryEntity);
                }catch (RuntimeException e){
                    logger.error("Error occured while setting category in cache " + categoryName, e);
                }
            }
        }
        return adminCategoryEntity.getCategory();
    }
}
