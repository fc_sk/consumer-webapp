
package com.freecharge.mongo.repos;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository handles all the session related queries to MongoDB.
 * Any session related query functions should be added here.
 */
@Repository("profileNumberRepository")
public class ProfileNumberRepository extends MetricsBaseMongoRepository {
	public static final String FREEFUND_CLASS_BLOCK_PREFIX = "FREEFUND_CLASS_BLOCK";
	public static final String FREEFUND_CLASS_PREFIX = "FREEFUND_CLASS:";
	public static final String NUMBER_ID_FIELD_NAME = "number_id";
	public static final String NUMBER_OFFERS_DATA_KEY = "offers";
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
    @Autowired
    public ProfileNumberRepository(DB mongoDB, MetricsClient metricsClient) {
        super("profilenumber", mongoDB, metricsClient);
    }

    public DBObject getDataByProfileNumber(String number){

        if (FCStringUtils.isBlank(number) || "0000000000".equals(number)) {
            /*
             * Some old Users have blank profile numbers
             * Facebook login is putting 0000000000 as profileNo, hence this fix is to check the same
             */
            return null;
        }
        return getDocument(new BasicDBObject(NUMBER_ID_FIELD_NAME, number));
    }

    public void updateOrCreateNumberData(String number, Map<String, Object> update){
        if (FCStringUtils.isBlank(number) || "0000000000".equals(number)) {
            /*
             * Some old Users have blank profile numbers
             * Facebook login is putting 0000000000 as profileNo, hence this fix is to check the same
             */
            return;
        }
        this.upsertDocument(new BasicDBObject(NUMBER_ID_FIELD_NAME, number), new BasicDBObject(update));
    }
    
    
    @PostConstruct
    public void ensureIndexes(){
    	if (collection == null){
    		logger.error("MongoDB not initialized.");
    		return;
    	}
    	// Session Id index for faster reads.
    	logger.info("Ensuring number_id index.");
    	collection.ensureIndex(new BasicDBObject(NUMBER_ID_FIELD_NAME, "hashed"));
    	logger.info("Ensuring index done.");
    }
}
