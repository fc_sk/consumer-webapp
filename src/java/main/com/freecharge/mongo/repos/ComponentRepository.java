package com.freecharge.mongo.repos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.admin.service.AccessControlUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.admin.dao.IAdminComponentDAO;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

@Repository("componentRepository") public class ComponentRepository extends MetricsBaseMongoRepository
        implements IAdminComponentDAO {

    Logger logger = LoggingFactory.getLogger(ComponentRepository.class);

    @Autowired public ComponentRepository(DB mongoDB, MetricsClient metricsClient) {
        super(AccessControlUtil.COMPONENT_REPOSITORY, mongoDB, metricsClient);
    }

    @Override public void addComponent(Map<String, Object> component) {
        this.insertDocument(new BasicDBObject(component));
    }

    @Override public void updateComponentUsingName(String queryName, String name, String URL, String active,
            List<String> restPatterns) {
        Map<String, Object> map = new HashMap<>();
        map.put(AccessControlUtil.MODEL_ATTRIBUTE_NAME, name);
        map.put(AccessControlUtil.MODEL_ATTRIBUTE_URL, URL);
        map.put(AccessControlUtil.MODEL_ATTRIBUTE_ACTIVE, active);
        map.put(AccessControlUtil.COMPONENT_REST_PATTERNS, restPatterns);
        this.updateDocument(new BasicDBObject(AccessControlUtil.MODEL_ATTRIBUTE_NAME, queryName), new BasicDBObject(map), false);
    }

    @Override public void deleteComponent(String name) {
        this.deleteDocument(new BasicDBObject(AccessControlUtil.MODEL_ATTRIBUTE_NAME, name));
    }

    @Override public DBObject getComponentUsingName(String name) {
        return this.getDocument(new BasicDBObject(AccessControlUtil.MODEL_ATTRIBUTE_NAME, name));
    }

    @Override public List<DBObject> findAllComponents() {
        return this.collection.find().sort(new BasicDBObject(AccessControlUtil.MODEL_ATTRIBUTE_NAME, 1)).toArray();
    }

    @Override public List<DBObject> findAllComponentsForRestPattern(String restPattern) {
        BasicDBObject query = new BasicDBObject().append(AccessControlUtil.COMPONENT_REST_PATTERNS, restPattern);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    @Override public void addComponentList(List<? extends HashMap<String, String>> components) {
        for (Map<String, String> component : components) {
            this.insertDocument(new BasicDBObject(component));
        }
    }

    @Override public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        // Session Id index for faster reads.
        logger.info("Ensuring affiliate res patterns index.");
        collection.ensureIndex(AccessControlUtil.COMPONENT_REST_PATTERNS);
    }
}
