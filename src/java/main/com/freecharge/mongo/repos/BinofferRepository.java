package com.freecharge.mongo.repos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores bin offer details per bin - hence its indexed with mobile It also has a TTL index of 90 days
 *
 * { orderid: "FC000000001", ffclass : "466", createdAt: new Date('July 22, 2013: 13:00:00') }
 *
 */
@Repository("binofferRepository")
public class BinofferRepository extends MetricsBaseMongoRepository {

    private static final int NINETY_DAYS_IN_SECS = 7776000; // 90 * 24 * 60 * 60 = 7776000 seconds

    private final Logger       logger    = LoggingFactory.getLogger(getClass());

    @Autowired
    public BinofferRepository(DB mongoDB, MetricsClient metricsClient) {
        super("binofferRepository", mongoDB, metricsClient);
    }

    public void insert(String cardHash, String orderId, String key, Long value) {
        logger.info("Adding data to binOfferRepository with cardHash : " + cardHash + ", orderId : " + orderId + ", key : " + key + ", value : " + value);
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(FCConstants.CARDHASH, cardHash);
        data.put(FCConstants.ORDER_ID, orderId);
        data.put(key, value);
        data.put(FCConstants.CREATEDAT, new Date());
        this.insert(data);

    }

    public void insert(Map<String, Object> data) {
        if (data.get(FCConstants.CREATEDAT) == null) {
            logger.error("No createdAt field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public Integer getOfferAvailCount(String cardHash, Long ffclass) {
        List<DBObject> entries = getData(cardHash, FCConstants.FREEFUNDCLASS, ffclass);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public List<String> getOrderIds(String cardHash, String key, Long value) {
    	logger.info("Fetching orders with cardHash : " + cardHash + ", key : " + key + ", value : " + value);
        List<String> orderIds = new ArrayList<String>();
        try{
            List<DBObject> entries = getData(cardHash, key, value);
            for(DBObject object : entries){
                orderIds.add(String.valueOf(object.get(FCConstants.ORDER_ID)));
            }
        } catch (Exception e){
            logger.error("Failed to retrieve OrderIds for cardHash : " + cardHash + ", key : " + key + ", value : " + value, e);
        }
        return orderIds;
    }
    
    public List<DBObject> getData(String cardHash, String key, Long value) {
        BasicDBObject query = new BasicDBObject().append(key, value).append(FCConstants.CARDHASH, cardHash);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getDataByCardHash(String cardHash) {
        BasicDBObject query = new BasicDBObject().append(FCConstants.CARDHASH, cardHash);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring FreefundClass-CardHash compound index on binofferRepository");
        BasicDBObject obj = new BasicDBObject();
        obj.put(FCConstants.FREEFUNDCLASS, 1);
        obj.put(FCConstants.CARDHASH, 1);
        collection.ensureIndex(obj);
        logger.info("FreefundClass-CardHash compound index ensured on binofferRepository");

        logger.info("Ensuring CardHash index on binofferRepository");
        collection.ensureIndex(new BasicDBObject(FCConstants.CARDHASH, 1));
        logger.info("CardHash index ensured on binofferRepository");

        logger.info("Ensuring TTL index on binofferRepository collection");

        collection.ensureIndex(new BasicDBObject(FCConstants.CREATEDAT, 1),
                               new BasicDBObject("expireAfterSeconds", NINETY_DAYS_IN_SECS));
        logger.info("Ensuring TTL index done.");
    }

}
