package com.freecharge.mongo.repos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This repository has the history of growth rewards
 **/
@Repository("growthRewardHistoryRepository")
public class GrowthRewardHistoryRepository extends MetricsBaseMongoRepository {

    public static final String USER_ID = "userId";
    public static final String GROWTH_EVENT_ID = "growthEventId";
    public static final String GROWTH_EVENT_TYPE = "growthEventType";
    public static final String REWARD_ID = "rewardId";
    public static final String REWARD_VALUE = "rewardValue";
    public static final String ORDER_ID = "orderId";
    public static final String RECHARGE_MOBILE_NUMBER = "rechargeMobileNumber";
    public static final String PARAMS = "params";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";
    private static final long NINETY_DAYS_IN_SECONDS = 7776000; // 90 days = 90 * 24 * 60 * 60 = 7776000 seconds

    private Logger logger = LoggingFactory.getLogger(GrowthRewardHistoryRepository.class);

    @Autowired
    public GrowthRewardHistoryRepository(final DB mongoDb, final MetricsClient metricsClient) {
        super("growthRewardHistory", mongoDb, metricsClient);
    }

    public void insert(final Map<String, Object> data) {
        if (data.get(CREATED_AT) == null) {
            logger.error("No created_on field in data, required for TTL. Not storing document! data=" + data);
            return;
        }
        this.insertDocument(new BasicDBObject(data));
    }

    public void insert(final long userId, final long growthEventId, final String growthEventType, final long rewardId,
                       final String rewardValue, final String orderId, final String rechargeMobileNumber, final String params) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(USER_ID, userId);
        data.put(GROWTH_EVENT_ID, growthEventId);
        data.put(GROWTH_EVENT_TYPE, growthEventType);
        data.put(REWARD_ID, rewardId);
        data.put(REWARD_VALUE, rewardValue);
        data.put(ORDER_ID, orderId);
        data.put(RECHARGE_MOBILE_NUMBER, rechargeMobileNumber);
        data.put(PARAMS, params);
        data.put(CREATED_AT, new Date());
        data.put(UPDATED_AT, new Date());
        this.insert(data);
    }

    public List<DBObject> getRewardsByEventIdAndUserId(final long growthEventId, final long userId) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_ID, growthEventId).append(USER_ID, userId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getRewardsByEventIdAndMobileNumber(final long growthEventId, final String mobileNumber) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_ID, growthEventId).append(RECHARGE_MOBILE_NUMBER, mobileNumber);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getRewardsByEventIdMobileNumberRewardId(final long growthEventId, final String mobileNumber, final long rewardId) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_ID, growthEventId)
                                                 .append(RECHARGE_MOBILE_NUMBER, mobileNumber)
                                                 .append(REWARD_ID, rewardId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getRewardsByEventIdUserIdRewardId(final long growthEventId, final long userId, final long rewardId) {
        BasicDBObject query = new BasicDBObject().append(GROWTH_EVENT_ID, growthEventId)
                                                 .append(USER_ID, userId)
                                                 .append(REWARD_ID, rewardId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getData(final long growthEventId, final String key, final Object value) {
        BasicDBObject query = new BasicDBObject().append(key, value).append(GROWTH_EVENT_ID, growthEventId);
        List<DBObject> entries = this.findDocuments(query, null, null);
        return entries;
    }

    public List<DBObject> getDataByOrderId(String orderId){
        BasicDBObject query = new BasicDBObject().append(ORDER_ID, orderId);
        List<DBObject> result = this.findDocuments(query, null, null);
        return result;
    }

    @Override
    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        // UserId index for faster reads
        logger.info("Ensuring userId index.");
        collection.ensureIndex(USER_ID);
        logger.info("Ensuring userId index done.");

        // growthEventId index for faster reads
        logger.info("Ensuring growthEventId index on growthRewardHistoryRepository");
        collection.ensureIndex(GROWTH_EVENT_ID);
        logger.info("eventType growthEventId ensured on growthRewardHistoryRepository");

        // rechargeMobileNumber index for faster reads
        logger.info("Ensuring rechargeMobileNumber index on growthRewardHistoryRepository");
        collection.ensureIndex(RECHARGE_MOBILE_NUMBER);
        logger.info("eventType rechargeMobileNumber ensured on growthRewardHistoryRepository");

        logger.info("Ensuring TTL index on growthRewardHistoryRepository collection");
        collection.ensureIndex(new BasicDBObject(CREATED_AT, 1),
                               new BasicDBObject("expireAfterSeconds", NINETY_DAYS_IN_SECONDS));
        logger.info("Ensuring TTL index on growthRewardHistoryRepository done.");
    }
}
