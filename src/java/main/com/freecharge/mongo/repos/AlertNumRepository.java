/*package com.freecharge.mongo.repos;


import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

*//**
 * This Repository stores the mobile number given by the user for sending an SMS alert about the transaction.
 * As the txn can take upto 2-3 days (extreme case) to finish, we will expire this after 3 days
 *
 *   {
 *     order_id: "abcd-123",
 *     alert_numb: "9876543210", 
 *     createdAt: new Date('July 22, 2013: 13:00:00')
 *   }
 *
 *//*
@Repository("alertNumRepository")
public class AlertNumRepository extends BaseRepository {
    public static final String ORDERID_NAME = "order_id";
    public static final String CREATEDAT_NAME = "createdAt";

    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    public AlertNumRepository(DB mongoDB, MetricsClient metricsClient) {
        super("sms_alert2", mongoDB, metricsClient);
    }

    public void addSmsAlert(Map<String, Object> alertNumMap) {
        alertNumMap.put(CREATEDAT_NAME, new Date());
        this.upsertDocument(new BasicDBObject(ORDERID_NAME, alertNumMap.get("order_id")), new BasicDBObject(alertNumMap));
    }

    public DBObject getSmsAlertByOrderId(String orderId){
        return this.getDocument(new BasicDBObject(ORDERID_NAME, orderId));
    }

    @PostConstruct
    public void ensureIndexes(){
        if (collection == null){
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring order_id index on sms_alert2 collection");
        collection.ensureIndex(new BasicDBObject(ORDERID_NAME, 1));
        logger.info("Ensuring order_id index done.");
        logger.info("Ensuring TTL index on sms_alert2 collection");
        // 3 days = 3 * 24 * 60 * 60 = 259200 seconds
        collection.ensureIndex(new BasicDBObject(CREATEDAT_NAME, 1),
                               new BasicDBObject("expireAfterSeconds", 259200));
        logger.info("Ensuring TTL index done.");
    }
}
*/