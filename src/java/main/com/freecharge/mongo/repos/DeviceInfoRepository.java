package com.freecharge.mongo.repos;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.mongo.entity.DeviceInfo;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/16/14
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("deviceInfoRepository")
public class DeviceInfoRepository extends MetricsBaseMongoRepository {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    private final String UID_KEY = "uid";
    private final String IMEI_KEY = "imei";

    @Autowired
    public DeviceInfoRepository(DB mongoDB, MetricsClient metricsClient) {
        super("app_device_info", mongoDB, metricsClient);
    }

    public void addDeviceInfo(DeviceInfo deviceInfo){
        if (FCStringUtils.isBlank(deviceInfo.getUid()) || FCStringUtils.isBlank(deviceInfo.getImei())){
            logger.info(String.format("Device Info could not be saved for: %s as uid is blank: %s",
                    deviceInfo.getCurAddress(), deviceInfo.getUid()));
            return;
        }
        this.insertDocument(new BasicDBObject(deviceInfo.asMap()));
    }

    public DBObject getDeviceInfo(String uid, String imei){
        Map<String, Object> query = new HashMap<>();
        query.put(IMEI_KEY, imei);
        return this.getDocument(new BasicDBObject(query));
    }

    public void updateCurrentCity(String uid, String imei, String city, Map<String, Object> addressMap,
                                  Integer cityId, String rawAddress){
        BasicDBObject query = new BasicDBObject(IMEI_KEY, imei);

        Map<String, Object> update = new HashMap<>();

        //For updating simple keys
        Map<String, Object> keyUpdate = new HashMap<>();
        keyUpdate.put("city_id", cityId);
        keyUpdate.put("cur_address", addressMap);
        keyUpdate.put("cur_city", city);


        //For updating the list
        Map<String, Object> setUpdate = new HashMap<>();
        setUpdate.put("cities", city); //city will be added to cities list if not already present

        Map<String, Object> listUpdate = new HashMap<>();
        listUpdate.put("raw_addresses", rawAddress);
        listUpdate.put("addresses", addressMap);

        //Now add to update object
        update.put("$set", keyUpdate);
        update.put("$addToSet", setUpdate);
        update.put("$push", listUpdate);
        this.updateRaw(query, new BasicDBObject(update));
    }

    @Override
    public void ensureIndexes() {
        BasicDBObject indexes = new BasicDBObject();
        indexes.put(UID_KEY, 1);
        indexes.put(IMEI_KEY, 1);
        collection.ensureIndex(indexes);
        logger.info("Indexes ensured for UID and IMEI for DeviceInfoRepository");
    }


}
