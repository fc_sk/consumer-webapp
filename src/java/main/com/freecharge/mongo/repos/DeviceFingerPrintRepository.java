package com.freecharge.mongo.repos;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;

/**
 * User: abhi
 * Date: 20/2/14
 * Time: 1:26 PM
 */
@Repository("deviceFingerPrintRepository")
public class DeviceFingerPrintRepository extends MetricsBaseMongoRepository {
    private Logger logger = LoggingFactory.getLogger(DeviceFingerPrintRepository.class);

    @Autowired
    public DeviceFingerPrintRepository(DB mongoDb, MetricsClient metricsClient) {
        super("deviceFingerPrint", mongoDb, metricsClient);
    }

    @PostConstruct
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }

        logger.info("Ensuring finPrint has index on deviceFingerPrintRepository -- start");
        collection.ensureIndex(new BasicDBObject("finPrint", "hashed"));
        logger.info("Ensuring finPrint has index on deviceFingerPrintRepository -- end");
    }

    public void update(String fingerPrint, int userId) {
        Map<String, ?> map = ImmutableMap.of(
                "usrIds", userId
        );
        this.upsertAddToSet(new BasicDBObject("finPrnt", fingerPrint), new BasicDBObject(map));
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        DeviceFingerPrintRepository repository = context.getBean(DeviceFingerPrintRepository.class);
        repository.update("gooboomoo", 2);
        ((ConfigurableApplicationContext) context).close();
    }
}
