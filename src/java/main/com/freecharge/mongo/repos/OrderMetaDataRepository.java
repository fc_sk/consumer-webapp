
package com.freecharge.mongo.repos;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;

/**
 * This Repository stores all the coupon IP related data to MongoDB.
 */
@Repository("orderMetadataRepository")
public class OrderMetaDataRepository extends MetricsBaseMongoRepository {
	public static final String ORDER_ID_FIELD_NAME = "orderId";
	public static final String LOOKUP_ID_FIELD_NAME = "lookupId";
	public static final String ORDER_METADATA_CREATED = "createdAt";
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
    @Autowired
    public OrderMetaDataRepository(DB mongoDB, MetricsClient metricsClient) {
        super("orderMetaData", mongoDB, metricsClient);
    }

    public Map getDataByOrderId(String orderId){
        DBObject dbObject = getDocument(new BasicDBObject(ORDER_ID_FIELD_NAME, orderId));
        Map dataMap = null;
        if (dbObject != null){
        	dataMap = dbObject.toMap();
        }
        return dataMap;
    }
    
    public Map getDataByLookupId(String lookupId){
        DBObject dbObject = getDocument(new BasicDBObject(LOOKUP_ID_FIELD_NAME, lookupId));
        Map dataMap = null;
        if (dbObject != null){
        	dataMap = dbObject.toMap();
        }
        return dataMap;
    }

    public void updateOrCreateOrderMetaData(String orderId, Map<String, Object> update){
        this.upsertDocument(new BasicDBObject(ORDER_ID_FIELD_NAME, orderId), new BasicDBObject(update));
    }
    
    public void updateOrCreateOrderMetaDataWithLookupId(String lookupId, Map<String, Object> update){
        this.upsertDocument(new BasicDBObject(LOOKUP_ID_FIELD_NAME, lookupId), new BasicDBObject(update));
    }
    
    
    @PostConstruct
    public void ensureIndexes(){
    	if (collection == null){
    		logger.error("MongoDB not initialized.");
    		return;
    	}
    	// Session Id index for faster reads.
    	logger.info("Ensuring order_id index.");
    	collection.ensureIndex(ORDER_ID_FIELD_NAME);
    	logger.info("Ensuring order_id index done.");
    	logger.info("Ensuring lookup_id index.");
    	collection.ensureIndex(LOOKUP_ID_FIELD_NAME);
    	logger.info("Ensuring lookup_id index done.");
    	
    	// Session start TTL index for order metadata expiry.
    	logger.info("Ensuring order metadata ttl index.");
    	BasicDBObject orderMetadataCreateIndex = new BasicDBObject();
    	orderMetadataCreateIndex.put(ORDER_METADATA_CREATED, 1);
    	BasicDBObject orderMetatDataTtl = new BasicDBObject();
    	orderMetatDataTtl.put("expireAfterSeconds", (1 * 86400)); // 24 * 60 * 60 = 86400 seconds
    	collection.ensureIndex(orderMetadataCreateIndex, orderMetatDataTtl);
    	logger.info("Ensuring index done.");
    }
}
