package com.freecharge.mongo.repos;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("promocodeUsageRepository")
public class PromocodeUsageRepository extends MetricsBaseMongoRepository {

    public static final String PROMOCODE = "promocode";
    public static final String USER_EMAIL = "email_id";
    public static final String SERVICE_NUMBER = "service_number";
    public static final String ORDER_ID = "order_id";
    public static final String MAPPING_KEY = "mapping_key";
    public static final String KEY_VALUE = "key_value";
    public static final String STATUS = "status";
    public static final String CREATED_AT = "created_at";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    protected PromocodeUsageRepository(DB mongoDB, MetricsClient metricsClient) {
        super("promocodeRepository", mongoDB, metricsClient);
    }

    public void insert(String promocode, String orderId, String serviceNumber,
                       String status, String userEmail) {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put(PROMOCODE, promocode);
        dataMap.put(USER_EMAIL, userEmail);
        dataMap.put(ORDER_ID, orderId);
        dataMap.put(STATUS, status);
        dataMap.put(SERVICE_NUMBER, serviceNumber);
        dataMap.put(CREATED_AT, new Date());
        insert(dataMap);
    }

    public void insert(Map<String, Object> data) {
        insertDocument(new BasicDBObject(data));
    }

    public int getPromocodeBlockedCount(String promocode) {
        List<DBObject> entries = getPromocodeDataForBlockedStatus(promocode);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public List<DBObject> getPromocodeDataForBlockedStatus(String promocode) {
        return getPromocodeData(promocode, FCConstants.BLOCKED, null);
    }

    public int getPromocodeRedeemedCount(String promocode) {
        List<DBObject> entries = getPromocodeDataForRedeemedStatus(promocode);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public List<DBObject> getPromocodeDataForRedeemedStatus(String promocode) {
        return getPromocodeData(promocode, FCConstants.REDEEMED, null);
    }

    public int getPromocodeUnBlockedCount(String promocode) {
        List<DBObject> entries = getPromocodeDataForUnBlockedStatus(promocode);
        if (entries != null) {
            return entries.size();
        }
        return 0;
    }

    public List<DBObject> getPromocodeDataForUnBlockedStatus(String promocode) {
        return getPromocodeData(promocode, FCConstants.UNBLOCKED, null);
    }

    public List<DBObject> getPromocodeData(String promocode,
                                           String status, String orderId) {
        BasicDBObject query = new BasicDBObject().append(PROMOCODE, promocode);
        if(!FCUtil.isEmpty(status)) {
            query.append(STATUS, status);
        }
        if(!FCUtil.isEmpty(orderId)) {
            query.append(ORDER_ID, orderId);
        }
        return this.findDocuments(query, null, null);
    }

    public List<DBObject> getUsedPromocodeData(String promocode) {
        BasicDBObject query = new BasicDBObject().append(PROMOCODE, promocode).append(STATUS, FCConstants.BLOCKED);
        List<DBObject> blockedList = this.findDocuments(query, null, null);
        List<DBObject> usedCodeList = new ArrayList<>();
        for(DBObject dbObject : blockedList) {
            Object orderIdObj = dbObject.get("order_id");
            if(orderIdObj != null) {
                String orderId = String.valueOf(orderIdObj);
                query = new BasicDBObject().append(PROMOCODE, promocode).append(ORDER_ID, orderId).append(STATUS, FCConstants.REDEEMED);
                List<DBObject> redeemedList = this.findDocuments(query, null, null);
                if(redeemedList != null && redeemedList.size() >= 1) {
                    usedCodeList.add(redeemedList.get(0));
                } else {
                    query = new BasicDBObject().append(PROMOCODE, promocode).append(ORDER_ID, orderId).append(STATUS, FCConstants.UNBLOCKED);
                    List<DBObject> unblockedList = this.findDocuments(query, null, null);
                    if(unblockedList == null || unblockedList.size() < 1) {
                        usedCodeList.add(dbObject);
                    }
                }
            }

        }
        return usedCodeList ;
    }

    @Override
    public void ensureIndexes() {
        if (collection == null) {
            logger.error("MongoDB not initialized.");
            return;
        }
        logger.info("Ensuring index on PromocodeUsageRepository for promocode, mappingKey, keyValue, status");
        BasicDBObject obj = new BasicDBObject();
        obj.put(PROMOCODE, 1);
        obj.put(STATUS, 1);
        collection.ensureIndex(obj);
        logger.info("Ensured index on PromocodeUsageRepository for promocode, mappingKey, keyValue, status");

        logger.info("Ensuring TTL index on PromocodeUsageRepository");
        obj = new BasicDBObject(CREATED_AT, 1);
        // 6 months = 6 * 30 * 24 * 60 * 60 = 15552000
        collection.ensureIndex(obj, new BasicDBObject("expireAfterSeconds", 15552000));
        logger.info("TTL index ensured on PromocodeUsageRepository");
    }
}
