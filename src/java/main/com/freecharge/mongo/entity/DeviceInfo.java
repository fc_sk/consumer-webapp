package com.freecharge.mongo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/16/14
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class DeviceInfo {
    private String imei;
    private String imsi;
    private String deviceId;
    private String advertiserId;
    private String uid;
    private List<String> cities;
    private String curCity;
    private List<Map<String, Object>> addresses;
    private String curAddress;
    private Integer cityId;
    private List<String> rawAddresses;

    public DeviceInfo(String imei, String imsi, String deviceId, String advertiserId, String uid) {
        this.imei = imei;
        this.imsi = imsi;
        this.deviceId = deviceId;
        this.advertiserId = advertiserId;
        this.uid = uid;
    }

    public List<String> getRawAddresses() {
        return rawAddresses;
    }

    public void setRawAddresses(List<String> rawAddresses) {
        this.rawAddresses = rawAddresses;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public String getCurCity() {
        return curCity;
    }

    public void setCurCity(String curCity) {
        this.curCity = curCity;
    }

    public List<Map<String, Object>> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Map<String, Object>> addresses) {
        this.addresses = addresses;
    }

    public String getCurAddress() {
        return curAddress;
    }

    public void setCurAddress(String curAddress) {
        this.curAddress = curAddress;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Map<String, Object> asMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("uid", this.uid);
        map.put("imei", imei);
        map.put("imsi", imsi);
        map.put("device_id", deviceId);
        map.put("advertiser_id", advertiserId);
        map.put("cities", cities==null||cities.size()<1?new ArrayList<String>():cities);
        map.put("cur_city", curCity);
        map.put("cur_address", curAddress);
        map.put("addresses", addresses==null||addresses.size()<1?new ArrayList<Map<String, Object>>():addresses);
        map.put("city_id", cityId);
        map.put("raw_addresses", rawAddresses==null||rawAddresses.size()<1?new ArrayList<String>():rawAddresses);
        return map;
    }
}
