package com.freecharge.mongo.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PaymentHistoryEntry {

    public static final String IS_PAYMENT_SUCCESSFUL = "isPaymentSuccessful";
    public static final String USER_ID               = "userId";
    public static final String PRODUCT_ID            = "producType";
    public static final String CARD_HASH             = "cardHash";
    public static final String AMOUNT                = "amount";
    public static final String CREATED_AT            = "createdAt";

    private String  cardHash;
    private boolean isPaymentSuccessFul;
    private int     userId;
    private double  amount;
    private int     productId;

    public String getCardHash() {
        return cardHash;
    }

    public void setCardHash(String cardHash) {
        this.cardHash = cardHash;
    }

    public boolean isPaymentSuccessFul() {
        return isPaymentSuccessFul;
    }

    public void setPaymentSuccessFul(boolean isPaymentSuccessFul) {
        this.isPaymentSuccessFul = isPaymentSuccessFul;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getAmount() {
        return amount;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Map<String, Object> asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(USER_ID, userId);
        map.put(CARD_HASH, cardHash);
        map.put(PRODUCT_ID, productId);
        map.put(AMOUNT, amount);
        map.put(IS_PAYMENT_SUCCESSFUL, isPaymentSuccessFul);
        map.put(CREATED_AT, new Date());
        return map;
    }

}
