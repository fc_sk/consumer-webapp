package com.freecharge.mongo.service;

import com.freecharge.mongo.repos.FreefundTemporaryFilesStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("freefundTemporaryFilesStatusService")
public class FreefundTemporaryFilesStatusService {

    @Autowired
    private FreefundTemporaryFilesStatusRepository freefundTemporaryFilesStatusRepository;

    public boolean insert(long campaignId, int numberOfFiles, int numberOfCodes, String prefix, int codeLength, long fileCreationTimeStamp, String status,
                          String emailId) {
        return freefundTemporaryFilesStatusRepository.insert(campaignId, numberOfFiles, prefix, codeLength, numberOfCodes, fileCreationTimeStamp,
                status, emailId);
    }

    public List<Map<String, String>> getDataByStatus(String status) {
        return freefundTemporaryFilesStatusRepository.getDataByStatus(status);
    }

    public List<Map<String, String>> getDataByCampaignId(long campaignId) {
        return freefundTemporaryFilesStatusRepository.getDataByCampaignId(campaignId);
    }

    public boolean updateStatus(long time, String status) {
        return freefundTemporaryFilesStatusRepository.updateStatus(time, status);
    }

    public List<Map<String, String>> getDataByCampaignIdAndStatus(Long classId, String status) {
        return freefundTemporaryFilesStatusRepository.getDataByCampaignIdAndStatus(classId, status);
    }

    public boolean updateFileCounter(long time, String fileCount) {
        return freefundTemporaryFilesStatusRepository.updateFileCount(time, fileCount);
    }
}
