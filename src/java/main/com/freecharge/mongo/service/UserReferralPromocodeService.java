package com.freecharge.mongo.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.UserReferralPromocodeRepository;
import com.mongodb.DBObject;

@Service
public class UserReferralPromocodeService {

    @Autowired
    UserReferralPromocodeRepository userReferralPromocodeRepository;

    @Autowired
    private UserServiceProxy			userServiceProxy;

    @Autowired
    private FCProperties fcProperties;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    public boolean storeUserReferrelPromocodeMapping(String userId, String promocodeId, String campaignId, String amountRewarded) {
        boolean result = false;
        try {        

            if(userId == null || promocodeId == null || campaignId == null){
                logger.error("Data cant be stored into userReferralPromocodeRepository for  userId: " + userId + 
                        ", promocodeId :" + promocodeId + ", campaignId:" + campaignId);
                return result;
            }  
            result = userReferralPromocodeRepository.insert(userId, promocodeId, campaignId, amountRewarded);

        } catch (Exception e) {
            logger.error("Error occured while storing userReferralPromocodeRepository for userId: " + userId, e); 
        }

        return result;
    }

    public List<Map<String, String>> getMappingByUserIdAndCampaign(String userId, String campaignId) {
        List<DBObject> userReferrelPromocodeList = null;
        List<Map<String, String>> userReferralPromocodeParsedList = new LinkedList<>();

        try {               
            if(userId != null && !userId.isEmpty() && campaignId != null && !campaignId.isEmpty()){
                userReferrelPromocodeList = userReferralPromocodeRepository.getDataByUserIdAndCampaign(userId, campaignId);
                userReferralPromocodeParsedList = userReferralPromocodeRepository.getUserReferralPromocodeList(userReferrelPromocodeList);
            }                      
        }
        catch (Exception e){
            logger.error("UserReferrelPromocodeMapping retrieval failed for userId: " + userId, e);
        }

        return userReferralPromocodeParsedList;
    }

    public List<Map<String, String>> getMappingByPromocodeIdAndCampaign(String promocodeId, String campaignId) {
        List<DBObject> userReferrelPromocodeList = null;
        List<Map<String, String>> userReferralPromocodeParsedList = new LinkedList<>();

        try {               
            if(promocodeId != null && !promocodeId.isEmpty() && campaignId != null && !campaignId.isEmpty()){
                userReferrelPromocodeList = userReferralPromocodeRepository.getDataByPromocodeAndCampaign(promocodeId, campaignId);
                userReferralPromocodeParsedList = userReferralPromocodeRepository.getUserReferralPromocodeList(userReferrelPromocodeList);
            }                      
        }
        catch (Exception e){
            logger.error("UserReferrelPromocodeMapping retrieval failed for promocodeId: " + promocodeId, e);
        }

        return userReferralPromocodeParsedList;
    }
    
    public List<Map<String, String>> getMappingByUserId(String userId) {
        List<DBObject> userReferrelPromocodeList = null;
        List<Map<String, String>> userReferralPromocodeParsedList = new LinkedList<>();

        try {               
            if(userId != null && !userId.isEmpty()){
                userReferrelPromocodeList = userReferralPromocodeRepository.getDataByUserId(userId);
                userReferralPromocodeParsedList = userReferralPromocodeRepository.getUserReferralPromocodeList(userReferrelPromocodeList);
            }                      
        }
        catch (Exception e){
            logger.error("getMappingByUserId retrieval failed for userId: " + userId, e);
        }

        return userReferralPromocodeParsedList;
    }
    
    public List<Map<String, String>> getMappingByPromocodeId(String promocodeId) {
        List<DBObject> userReferrelPromocodeList = null;
        List<Map<String, String>> userReferralPromocodeParsedList = new LinkedList<>();

        try {               
            if(promocodeId != null && !promocodeId.isEmpty()){
                userReferrelPromocodeList = userReferralPromocodeRepository.getDataByPromocodeId(promocodeId);
                userReferralPromocodeParsedList = userReferralPromocodeRepository.getUserReferralPromocodeList(userReferrelPromocodeList);
            }                      
        }
        catch (Exception e){
            logger.error("getMappingByPromocodeId retrieval failed for promocodeId: " + promocodeId, e);
        }

        return userReferralPromocodeParsedList;
    }

    public boolean updateDataForUserAndCampaign(String userId, String campaignId, Map<String, Object> updates){
        boolean result = false;
        try {               
            if(userId != null && !userId.isEmpty() && campaignId != null && !campaignId.isEmpty()){
                result = userReferralPromocodeRepository.updateDataForUserIdAndCampaign(userId, campaignId, updates);
            }                      
        }
        catch (Exception e){
            logger.error("UserReferrelPromocodeMapping update failed for userId: " + userId + ", campaignId:" + campaignId, e);
        }
        return result;
    }
    
    public Double getTotalRewardedValue(Double rewardAmount, Double amountRewarded) {
        return amountRewarded + rewardAmount;
    }

    public String getUserDetailByPropertyName(String userId, String campaignId, String propertyName) {
        List<Map<String, String>> userReferralMapList = getMappingByUserIdAndCampaign(userId, campaignId);
        Map<String, String> userReferralMap = FCUtil.getFirstElement(userReferralMapList);

        if (!FCUtil.isEmpty(userReferralMap) && userReferralMap.get(propertyName) != null) {
            return userReferralMap.get(propertyName);
        }

        return "";
    }

    public Double getRewardEarned(String userId, String campaignId, String propertyName) {

        String rewardEarnedStr = this.getUserDetailByPropertyName(userId, campaignId, propertyName);

        Double rewardEarned = 0d;
        if(!FCUtil.isEmpty(rewardEarnedStr)) {
            rewardEarned = Double.valueOf(rewardEarnedStr);
        }

        return rewardEarned;
    }


    public Map<String, String> validateRequest(String email, String campaignId, String apiSecret) {
        Map<String, String> validationMap = new HashMap<>();

        String secret =  fcProperties.getProperty(FCConstants.REFERRAL_API_SECRET_PROPERTY);

        if (FCStringUtils.isBlank(apiSecret) || !apiSecret.equals(secret)) {
            validationMap.put("status", "failure");
            validationMap.put("message", "Authentication failure");
            return validationMap;
        }

        if (FCStringUtils.isBlank(email)) {
            validationMap.put("status", "failure");
            validationMap.put("message", "Invalid email id");
            return validationMap;
        }

        if (FCStringUtils.isBlank(campaignId) || !FCUtil.isInteger(campaignId)) {
            validationMap.put("status", "failure");
            validationMap.put("message", "Invalid campaignId. Please pass a valid integer");
            return validationMap;
        }

        Users user = userServiceProxy.getUserByEmailId(email);
        if (user == null) {
            logger.info("No user found with email " + email);
            validationMap.put("status", "failure");
            validationMap.put("message", "No user found with email " + email);
            return validationMap;
        }

        return null;
    }


}