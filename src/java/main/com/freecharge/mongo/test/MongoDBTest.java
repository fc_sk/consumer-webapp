package com.freecharge.mongo.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 7/25/13
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class MongoDBTest {

    @Test
    public void testConnection() throws Exception {
        MongoClient mongoClient= new MongoClient("localhost");
        DB mongoDb = mongoClient.getDB("freecharge");
        assertNotNull(mongoDb);
    }


}
