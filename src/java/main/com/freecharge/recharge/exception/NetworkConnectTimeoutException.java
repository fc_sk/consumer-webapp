package com.freecharge.recharge.exception;

import org.apache.commons.httpclient.ConnectTimeoutException;

public class NetworkConnectTimeoutException extends ConnectTimeoutException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public NetworkConnectTimeoutException() {
        super();
    }
    
    public NetworkConnectTimeoutException(final String message) {
        super(message);
    }
}
