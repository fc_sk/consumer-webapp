package com.freecharge.recharge.exception;

public class RechargeEnqueFailedException extends RuntimeException {

    public RechargeEnqueFailedException(Exception e) {
        super(e);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
