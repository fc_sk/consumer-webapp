package com.freecharge.recharge.exception;

public class InvalidOrderIDException extends IllegalArgumentException {
    private static final long serialVersionUID = 1L;
    
    public InvalidOrderIDException(String string) {
        super(string);
    }
}
