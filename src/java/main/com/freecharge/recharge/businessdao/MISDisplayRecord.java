package com.freecharge.recharge.businessdao;

import java.io.Serializable;
import java.util.Date;

/*import com.freecharge.app.domain.entity.CircleMaster;
 import com.freecharge.app.domain.entity.InRequest;
 import com.freecharge.app.domain.entity.OperatorMaster;*/

public class MISDisplayRecord implements Serializable, Comparable<MISDisplayRecord> {

	private static final long serialVersionUID = 1L;
	private Long txnId;
	private String appName;
	private String mobileNo;
	private String operator;
	private String circle;
	private String amount;
	private String appTxnId;
	private String respCode;
	private String gatewayRespCode;
	private String respMessage;
	private String gatewayRefNo;
	private String operatorRefNo;
	private Date date;
	private Date time;
	private String gateway;
	private String options;
	private String status;
	private String attemptOrder;
	private String productType;

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MISDisplayRecord() {
		// TODO Auto-generated constructor stub
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTxnId() {
		return txnId;
	}

	public void setTxnId(Long txnId) {

		this.txnId = txnId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAppTxnId() {
		return appTxnId;
	}

	public void setAppTxnId(String appTxnId) {
		this.appTxnId = appTxnId;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getGatewayRespCode() {
		return gatewayRespCode;
	}

	public void setGatewayRespCode(String gatewayRespCode) {
		this.gatewayRespCode = gatewayRespCode;
	}

	public String getRespMessage() {
		return respMessage;
	}

	public void setRespMessage(String respMessage) {
		this.respMessage = respMessage;
	}

	public String getGatewayRefNo() {
		return gatewayRefNo;
	}

	public void setGatewayRefNo(String gatewayRefNo) {
		this.gatewayRefNo = gatewayRefNo;
	}

	public String getOperatorRefNo() {
		return operatorRefNo;
	}

	public void setOperatorRefNo(String operatorRefNo) {
		this.operatorRefNo = operatorRefNo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getAttemptOrder() {
		return attemptOrder;
	}

	public void setAttemptOrder(String attemptOrder) {
		this.attemptOrder = attemptOrder;
	}

	public int compareTo(MISDisplayRecord o) {
		if (this.getDate().getTime() > o.getDate().getTime())
			return -1;
		else if (this.getDate().getTime() < o.getDate().getTime())
			return 1;
		else
			return 0;
	}

}
