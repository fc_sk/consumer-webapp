package com.freecharge.recharge.businessdao;

public class StringUtils {
	
	public static String trim(String s){
		String trimmed="";
		if(s !=null) {
			for(int i =0; i<s.length() ; i++){
				if(s.charAt(i) != -0){
					trimmed+= s.charAt(i);
				}
			}
		}
		return trimmed;
	}

}
