package com.freecharge.recharge.businessdao;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.domain.entity.jdbc.mappers.AdminUserRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.MISDisplayRecordRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;

public class INAuthenticationFetchDAOImpl  implements INAuthenticationFetchDAO {
	
	private static Logger logger = LoggingFactory.getLogger(INAuthenticationFetchDAOImpl.class);
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
	
	@Override
	public boolean isMisUserValid(AdminUsers adminUsers) {
		
	String adminUserName = null;
	try {
		   if(adminUsers.getAdminUserName()!=null && !adminUsers.getAdminUserName().isEmpty()) {
			   adminUserName = adminUsers.getAdminUserName();
		    String queryString = "select * from admin_users where admin_user_name = :adminUserName";
		   
		    Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("adminUserName",adminUserName);
		   
		   List<AdminUsers> adminUserList = this.jdbcTemplate.query(queryString, paramMap, new AdminUserRowMapper());
		   
		   if(adminUserList!=null && !adminUserList.isEmpty() && adminUserList.size()>0) {
		   AdminUsers userStaus = adminUserList.get(0);
		   if(userStaus.getAdminUserName().equalsIgnoreCase(adminUsers.getAdminUserName()) && userStaus.getAdminUserPassword().equalsIgnoreCase(adminUsers.getAdminUserPassword()) && userStaus.getAdminUserActiveStatus()==true) {
		   return true;
		   }
		 }
	  } 
    }     catch (RuntimeException re) {
		  logger.error("[IN MODERATOR] finding User admin list from Database: ", re);
		  }
		  return false;
	}
	
	public List<MISDisplayRecord> getMisData(MisRequestData misRequestData, int pageCount) {
		List<MISDisplayRecord> records = getMisRecords(misRequestData,pageCount);
		return records;
	}
	
	public Integer getMisDataCount(MisRequestData misRequestData)
	{
		return  getMisRecordCounts(misRequestData);
	}
	
	
	public String prepareMisQuery(MisRequestData misRequestData, int pageCount)
	{
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("select inr.request_id, inr.subscriber_number, inr.amount, inr.circle,inr.product_type, inrs.in_resp_code, it.aggr_response_code, it.aggr_name, " +
		" it.aggr_response_message, it.aggr_reference_no, it.aggr_opr_refernce_no, it.request_time,  it.transaction_status, inr.operator, inr.affiliate_trans_id from in_request inr, in_response inrs, " +
		" in_transactions it where inr.request_id = inrs.request_id and  inrs.request_id = it.fk_request_id" );
		
	if(misRequestData.getMisStartDate() != null) {
		queryString.append(firstClause ? " and " : " and ");
		
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		//date = "2012-1-16 0:0:0";
		queryString.append("inr.created_time >= '"+date+"'");
        parameters.put("startDate",date);
        firstClause = false;
	}
	if (misRequestData.getMisEndDate() != null) {
		queryString.append(" and ");
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisEndDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		queryString.append("inr.created_time <= '"+date+"'");
        parameters.put("endDate",date);
    }
	if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) {
		queryString.append(" and ");
		queryString.append("inr.subscriber_number = " + misRequestData.getMobileNo());
		parameters.put("MobileNo", misRequestData.getMobileNo());
	}
	if (misRequestData.getOperator() != null && !misRequestData.getOperator().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inr.operator = '" + misRequestData.getOperator() + "'");
	    parameters.put("operator", misRequestData.getOperator());
	}
	if (misRequestData.getTxnID() != null) {
		queryString.append(" and ");
		queryString.append("inr.request_id = " + misRequestData.getTxnID());
		parameters.put("txnID", misRequestData.getTxnID());
	}
	if (misRequestData.getCircle() != null) {
		queryString.append(" and ");
		queryString.append("inr.circle = '" + misRequestData.getCircle()+"'");
		parameters.put("circle", misRequestData.getCircle());
	}
	if (misRequestData.getGateway() != null && !misRequestData.getGateway().isEmpty()) {
		queryString.append(" and ");
		queryString.append("it.aggr_name = '" + misRequestData.getGateway()+"'");
		parameters.put("gateway", misRequestData.getGateway());
	}
	if (misRequestData.getAppTxnID() != null && !misRequestData.getAppTxnID().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inr.affiliate_trans_id = '" + misRequestData.getAppTxnID() + "'");
		parameters.put("appTxnID", misRequestData.getAppTxnID());
	}
	if (misRequestData.getStatus() != null && !misRequestData.getStatus().isEmpty()) {
		queryString.append(" and ");
		queryString.append("it.transaction_status = '" + misRequestData.getStatus()+"'");
		parameters.put("status", misRequestData.getStatus());
	}
	if(misRequestData.getPageSize() != null ) {
		queryString.append(" ORDER BY inr.request_id DESC");
	}
	if(pageCount!=500) {
	queryString.append(" limit "+(pageCount-1)*80+", 80");
	} 
	  parameters.put("query", queryString.toString());
	  return queryString.toString();
	}
	
	public List<MISDisplayRecord> getMisRecords(final MisRequestData misRequestData, int pageCount) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		int year = startCalendar.get(Calendar.YEAR);
		int month = startCalendar.get(Calendar.MONTH);
		int date = startCalendar.get(Calendar.DAY_OF_MONTH);
		startCalendar.set(year, month, date, 0, 0, 0);
		misRequestData.setMisStartDate(startCalendar.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getMisEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 59, 59);
		misRequestData.setMisEndDate(endCalendar.getTime());
		
		String queryString = this.prepareMisQuery(misRequestData,pageCount);
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		   
		List<MISDisplayRecord> adminUserList =  this.jdbcTemplate.query(queryString, paramMap, new MISDisplayRecordRowMapper());
		
		 return adminUserList;
	}
	
	public Integer getMisRecordCounts(final MisRequestData misRequestData) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		int year = startCalendar.get(Calendar.YEAR);
		int month = startCalendar.get(Calendar.MONTH);
		int date = startCalendar.get(Calendar.DAY_OF_MONTH);
		startCalendar.set(year, month, date, 0, 0, 0);
		misRequestData.setMisStartDate(startCalendar.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getMisEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 59, 59);
		misRequestData.setMisEndDate(endCalendar.getTime());
		
		String queryString = this.prepareMisQueryForCount(misRequestData);
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		   
		Integer adminUserList =  this.jdbcTemplate.queryForInt(queryString, paramMap);
		
		 return adminUserList;
	}
	
	public String prepareMisQueryForCount(MisRequestData misRequestData)
	{
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("select count(*)  from in_request inr, in_response inrs, " +
		" in_transactions it where inr.request_id = inrs.request_id and  inrs.request_id = it.fk_request_id" );
		
	if(misRequestData.getMisStartDate() != null) {
		queryString.append(firstClause ? " and " : " and ");
		
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		date = "2012-1-16 0:0:0";
		queryString.append("inr.created_time >= '"+date+"'");
        parameters.put("startDate",date);
        firstClause = false;
	}
	if (misRequestData.getMisEndDate() != null) {
		queryString.append(" and ");
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisEndDate());
		String date = ""+startCalendar.get(Calendar.YEAR)+"-"+(startCalendar.get(Calendar.MONTH)+1)+"-"+startCalendar.get(Calendar.DAY_OF_MONTH)+" "+startCalendar.get(Calendar.HOUR_OF_DAY)+":"+startCalendar.get(Calendar.MINUTE)+":"+startCalendar.get(Calendar.SECOND);
		queryString.append("inr.created_time <= '"+date+"'");
        parameters.put("endDate",date);
    }
	if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) {
		queryString.append(" and ");
		queryString.append("inr.subscriber_number = " + misRequestData.getMobileNo());
		parameters.put("MobileNo", misRequestData.getMobileNo());
	}
	if (misRequestData.getOperator() != null && !misRequestData.getOperator().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inr.operator = '" + misRequestData.getOperator() + "'");
	    parameters.put("operator", misRequestData.getOperator());
	}
	if (misRequestData.getTxnID() != null) {
		queryString.append(" and ");
		queryString.append("inr.request_id = " + misRequestData.getTxnID());
		parameters.put("txnID", misRequestData.getTxnID());
	}
	if (misRequestData.getCircle() != null) {
		queryString.append(" and ");
		queryString.append("inr.circle = '" + misRequestData.getCircle()+"'");
		parameters.put("circle", misRequestData.getCircle());
	}
	if (misRequestData.getGateway() != null && !misRequestData.getGateway().isEmpty()) {
		queryString.append(" and ");
		queryString.append("it.aggr_name = '" + misRequestData.getGateway()+"'");
		parameters.put("gateway", misRequestData.getGateway());
	}
	if (misRequestData.getAppTxnID() != null && !misRequestData.getAppTxnID().isEmpty()) {
		queryString.append(" and ");
		queryString.append("inr.affiliate_trans_id = '" + misRequestData.getAppTxnID() + "'");
		parameters.put("appTxnID", misRequestData.getAppTxnID());
	}
	if (misRequestData.getStatus() != null && !misRequestData.getStatus().isEmpty()) {
		queryString.append(" and ");
		queryString.append("it.transaction_status = '" + misRequestData.getStatus()+"'");
		parameters.put("status", misRequestData.getStatus());
	}
	if(misRequestData.getPageSize() != null ) {
		queryString.append(" ORDER BY inr.request_id DESC");
	}
	  parameters.put("query", queryString.toString());
	  return queryString.toString();
	}
	
	
}
