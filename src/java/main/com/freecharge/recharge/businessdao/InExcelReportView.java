package com.freecharge.recharge.businessdao;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

public class InExcelReportView extends AbstractExcelView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) {
		try {
			List<MISDisplayRecord> excelData = (List<MISDisplayRecord>) model.get("excelData");
			HSSFSheet sheet = workbook.createSheet("Revenue Report");
			HSSFRow header = sheet.createRow(0);
			
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename=Report.xlt");
			OutputStream outSteram = response.getOutputStream();
			
			header.createCell(0).setCellValue("IN Txn ID");
			header.createCell(1).setCellValue("Client Name");
			header.createCell(2).setCellValue("Mobile NO");
			header.createCell(3).setCellValue("Operator");
			header.createCell(4).setCellValue("Circle");
			header.createCell(5).setCellValue("Amount");
			header.createCell(6).setCellValue("Order ID");
			header.createCell(7).setCellValue("IN Resp Code");
			header.createCell(8).setCellValue("Aggregator Resp Code");
			header.createCell(9).setCellValue("Message");
			header.createCell(10).setCellValue("Aggregator Ref No");
			header.createCell(11).setCellValue("Operator Ref No");
			header.createCell(12).setCellValue("Date and Time");
			header.createCell(13).setCellValue("Aggregator");
			header.createCell(14).setCellValue("Recharge Type");
			header.createCell(15).setCellValue("Txn Status");

			int rowNum = 1;

			if (excelData != null) {
				for (int i = 0; i < excelData.size(); i++) {
					sheet.autoSizeColumn(rowNum);
					MISDisplayRecord displayRecord = (MISDisplayRecord) excelData.get(i);
					HSSFRow row = sheet.createRow(rowNum++);
					row.createCell(0).setCellValue(displayRecord.getTxnId());
					row.createCell(1).setCellValue("Freecharge");
					row.createCell(2).setCellValue(displayRecord.getMobileNo());
					row.createCell(3).setCellValue(displayRecord.getOperator());
					row.createCell(4).setCellValue(displayRecord.getCircle());
					row.createCell(5).setCellValue(displayRecord.getAmount());
					row.createCell(6).setCellValue(displayRecord.getAppTxnId());
					row.createCell(7).setCellValue(displayRecord.getRespCode());
					row.createCell(8).setCellValue(displayRecord.getGatewayRespCode());
					row.createCell(9).setCellValue(displayRecord.getRespMessage());
					row.createCell(10).setCellValue(displayRecord.getGatewayRefNo());
					row.createCell(11).setCellValue(displayRecord.getOperatorRefNo());
					row.createCell(12).setCellValue(displayRecord.getDate());
					row.createCell(13).setCellValue(displayRecord.getGateway());
					row.createCell(14).setCellValue(displayRecord.getOptions());
					row.createCell(15).setCellValue(displayRecord.getStatus());
				}

			}
			//response.setContentType("application/vnd.ms-excel");
			/*response.setContentType("application/txt");
			response.setHeader("Content-   Disposition","attachment;filename=ESTHotelPerfByMonthExcelReport.xls");*/
			/*ServletOutputStream outputStream = response.getOutputStream();*/
			workbook.write(outSteram);
			outSteram.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
