package com.freecharge.recharge.businessdao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReportData implements Serializable {
	
private static final long	serialVersionUID	= -8870980322592439523L;
	
	private final List<List<String>> data ;
	
	
	public ReportData(List<List<String>> data){
		this.data = new ArrayList<List<String>>();
		if(data != null){
			for(List<String> row : data){
				this.data.add(row);
			}
		}
	}

	public List<List<String>> getData() {
		return data;
	}

	
	public List<String> getRow(int index){
		List<String> row = null;
		if(!isEmpty()){
			row = data.get(index);
		}
		return row;
	}
	
	
	public String toCSV(){
		StringBuilder builder = new StringBuilder();
		for(List<String> rows : data){
			appendDataInRow(builder, rows);
		}
		return builder.toString();
	}

	
	
	public String toCSV(int rowIndex){
		StringBuilder builder = new StringBuilder();
		List<String> row = getRow(rowIndex);
		if(row != null){
			appendDataInRow(builder, row);
		}
		return builder.toString();
	}
	
	
	public String toCSVFrom(int rowIndex){
		StringBuilder builder = new StringBuilder();
		List<List<String>> rows = getRowsFrom(rowIndex);
		if(rows != null){
			for(List<String> row : rows){
				appendDataInRow(builder, row);
			}
			
		}
		return builder.toString();
	}
	
	
	public String toCSVFrom(int rowIndex, int endIndex){
		StringBuilder builder = new StringBuilder();
		List<List<String>> rows = getRowsFrom(rowIndex, endIndex);
		if(rows != null){
			for(List<String> row : rows){
				appendDataInRow(builder, row);
			}
			
		}
		return builder.toString();
	}

	public List<List<String>> getRowsFrom(int rowIndex) {
		int n = data.size();
		List<List<String>> rows = getRowsFrom(rowIndex, n);
		return rows;
	}

	public List<List<String>> getRowsFrom(int startIndex, int endIndex) {
		if(isEmpty())
			return null;
		if(startIndex < 0){
			startIndex = 0;
		}
		List<List<String>> rows = data.subList(startIndex, endIndex);
		return rows;
	}
	
		
	
	public boolean isEmpty(){
		boolean result = true;
		if(data != null && !data.isEmpty() && data.size() != 0){
			result = false;
		}
		
		return result;
	}
	
	public int size(){
		if(isEmpty()){
			return 0;
		}else{
			return data.size();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ReportData))
			return false;
		ReportData other = (ReportData) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}
	
	
	private void appendDataInRow(StringBuilder builder, List<String> rows) {
		for(String row : rows ){
			builder.append(row);
			builder.append(",");
		}
		builder.append("\n");
	}
	
	

}
