package com.freecharge.recharge.businessdao;

import java.util.List;

import com.freecharge.app.domain.entity.AdminUsers;

public interface INAuthenticationFetchDAO {
	
	public boolean isMisUserValid(AdminUsers adminUsers);
	
	public List<MISDisplayRecord> getMisData(MisRequestData misRequestData, int pageNumber);
	
	public Integer getMisDataCount(MisRequestData misRequestData);
	
}
