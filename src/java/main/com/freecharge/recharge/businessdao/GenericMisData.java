package com.freecharge.recharge.businessdao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GenericMisData {

	private Integer inRequestID;
	private Integer pgRequestID;
	private String mobileNo;
	private String orderID;
	private String inAggregator;
	private String paymentGateway;
	private String appTxnID;
	private Boolean userTxnStatus;
	private Date startDate;
	private Date endDate;
	private Integer pageSize = 50;
	private Integer pageNo;

	public Integer getInRequestID() {
		return inRequestID;
	}

	public void setInRequestID(Integer inRequestID) {
		this.inRequestID = inRequestID;
	}

	public Integer getPgRequestID() {
		return pgRequestID;
	}

	public void setPgRequestID(Integer pgRequestID) {
		this.pgRequestID = pgRequestID;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getInAggregator() {
		return inAggregator;
	}

	public void setInAggregator(String inAggregator) {
		this.inAggregator = inAggregator;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getAppTxnID() {
		return appTxnID;
	}

	public void setAppTxnID(String appTxnID) {
		this.appTxnID = appTxnID;
	}

	public Boolean getUserTxnStatus() {
		return userTxnStatus;
	}

	public void setUserTxnStatus(Boolean userTxnStatus) {
		this.userTxnStatus = userTxnStatus;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public void setStartDate(String startDate) {
		// 2011/09/17
		if (startDate != null && !startDate.equals("")) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				this.startDate = df.parse(startDate);
			} catch (ParseException pe) {
				pe.printStackTrace();
			}
		}
	}

	public void setEndDate(String endDate) {
		if (endDate != null && !endDate.equals("")) {
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				this.endDate = df.parse(endDate);
			} catch (ParseException pe) {
				pe.printStackTrace();
			}
		}
	}

}
