package com.freecharge.recharge.businessdao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class InExcelReader {
	
private static Logger logger = LoggerFactory.getLogger(InExcelReader.class);
	
	
	public ReportData read(String fileName) {
		ReportData data = null;
		try {
			File file = new File(fileName);
			FileInputStream inputStream = new FileInputStream(file);
			POIFSFileSystem fileSystem = new POIFSFileSystem (inputStream);
			
			HSSFWorkbook      workBook = new HSSFWorkbook (fileSystem);
			HSSFSheet         sheet    = workBook.getSheetAt (0);
			@SuppressWarnings("rawtypes")
			Iterator rows     = sheet.rowIterator ();
			
			List<List<String>> rowData = readRows(rows);
			data = new ReportData(rowData);
			//logger.info("Excel Data :  " + data.toCSVFrom(0,1));
			
		} catch (FileNotFoundException e) {
			logger.error("The file " + fileName  + " could not be read.",e);
		} catch (IOException e) {
			logger.error("The file " + fileName  + " could not be read.",e);
		}
		return data;
	}

	@SuppressWarnings("rawtypes")
	private List<List<String>> readRows(Iterator rows) {
		List<List<String>> data = new ArrayList<List<String>>();
		while(rows.hasNext()){
			HSSFRow myRow = (HSSFRow) rows.next();
			Iterator cellIter = myRow.cellIterator();
			List<String> rowData = new ArrayList<String>();
			while(cellIter.hasNext()){
				HSSFCell myCell = (HSSFCell) cellIter.next();
				rowData.add(StringUtils.trim(myCell.toString()));
			}
			data.add(rowData);
			
		}
		return data;
   }

}
