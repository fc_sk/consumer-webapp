package com.freecharge.recharge.businessdao;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Set;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.formula.udf.AggregatingUDFFinder;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.google.common.collect.ImmutableSet;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: May 9, 2012
 * Time: 5:21:46 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AggregatorInterface {
    public static final String EURONET_IN = "euronet";
    public static final String OXIGEN_IN = "oxigen";
    public static final String SUVIDHAA_IN = "suvidhaa";
    public static final String DUMMY_EURONET_IN = "dummy_euronet";
    public static final String DUMMY_OXIGEN_IN = "dummy_oxigen";
    public static final String LAPU_IN = "lapu";
    
    public enum AggregatorName {
        EURONET(EURONET_IN), OXIGEN(OXIGEN_IN), SUVIDHAA(SUVIDHAA_IN), DUMMY_EURONET(DUMMY_EURONET_IN), DUMMY_OXIGEN(DUMMY_OXIGEN_IN);

        private String name;

        private AggregatorName(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
        
        public static AggregatorName fromName(String name){
            //just to avoid NPE at caller passing dummy
            AggregatorName aggrName = DUMMY_EURONET;
            if(StringUtils.isNotEmpty(name)){
                for(AggregatorName value:values()){
                    if(value.getName().equalsIgnoreCase(name)){
                        aggrName = value;
                        break;
                    }
                }
            }
            return aggrName;
        }
    }

    public enum TransactionStatuses {
        SUCCESS(SUCCESS_STATUS), FAILURE("failure"), PENDING("pending");

        private String status;

        private TransactionStatuses(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }

    public static final String SUCCESS_STATUS = "success";

    public static final String PENDING_IN_RESPONSE_CODE = "-1";
    public static final String SUCCESS_IN_RESPONSE_CODE = "00";

    public static final Set<String> RECHARGE_SUCCESS_AGGREGATOR_RESPONSES = ImmutableSet.of("00", "0");
    public static final Set<String> SUCCESS_OR_PENDING_AGGREGATOR_RESPONSES = ImmutableSet.of("0", "00", "UP", "US", "", "-10", "ES", "2");
    public static final Set<String> RECHARGE_PENDING_AGGREGATOR_RESPONSE = ImmutableSet.of("UP", "US", "-10", "ES", "2");
    
    public static final Set<String> SUCCESS_OR_PENDING_RESPONSES = ImmutableSet.of("00", "0", "08", "-1");
    
    public static final Set<String> PENDING_RESPONSES = ImmutableSet.of("08", "-1");
    
    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed, List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,ConnectTimeoutException, SocketException, SocketTimeoutException,IOException;
    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest) throws HttpException, ConnectException,ConnectTimeoutException, SocketException, SocketTimeoutException,IOException;
    public Double getWalletBalance() throws IOException;
}
