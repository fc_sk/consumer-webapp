package com.freecharge.recharge.businessDo;

import java.util.Date;

import com.freecharge.common.util.RechargeProductType;

public class SuccessfulBillRechargeDo implements RechargeDoType{

    private String serviceNumber;
    private int    operatorId;
    private int    circleId;
    private String amount;
    private String orderId;
    private String dueDate;
    private String billDate;
	private int    billerType;
    private String additionalInfo1;
    private String additionalInfo2;
    private String additionalInfo3;
    private String additionalInfo4;
    private String additionalInfo5;
    private long timestamp;
    private int frequency;
    private transient RechargeProductType rechargeProductType;
    private transient int rank;

    public String getServiceNumber() {
        return serviceNumber;
    }
    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }
    public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
    public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
    public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAdditionalInfo1() {
        return additionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        this.additionalInfo1 = additionalInfo1;
    }

    public String getAdditionalInfo2() {
        return additionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        this.additionalInfo2 = additionalInfo2;
    }

    public String getAdditionalInfo3() {
        return additionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        this.additionalInfo3 = additionalInfo3;
    }

    public String getAdditionalInfo4() {
        return additionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        this.additionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo5() {
        return additionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        this.additionalInfo5 = additionalInfo5;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public RechargeProductType getRechargeProductType() {
        return rechargeProductType;
    }

    public void setRechargeProductType(RechargeProductType rechargeProductType) {
        this.rechargeProductType = rechargeProductType;
    }

    public int getBillerType() {
        return billerType;
    }

    public void setBillerType(int billerType) {
        this.billerType = billerType;
    }

    @Override
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "SuccessfulBillRechargeDo [serviceNumber=" + serviceNumber + ", operatorId=" + operatorId + ", circleId="
                + circleId + ", amount=" + amount + ", additionalInfo1=" + additionalInfo1 + ", additionalInfo2="
                + additionalInfo2 + ", additionalInfo3=" + additionalInfo3 + ", additionalInfo4=" + additionalInfo4
                + ", additionalInfo5=" + additionalInfo5 + "]";
    }

   

}
