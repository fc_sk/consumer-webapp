package com.freecharge.recharge.businessDo;

import java.math.BigDecimal;

public class EuroDTHValidationResponseDo {
	
	/**
	 * Response code for transaction
	 */
	private String responseCode;
	
	/**
	 * Response Message for transaction
	 */
	private String responseMessage;
	
	/**
	 * Response Action for transaction
	 */
	private String responseAction;
	
	/**
	 * Minimum applicable denomination
	 */
	private String minReloadAmount;
	
	
	public String getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getResponseAction() {
		return responseAction;
	}
	public void setResponseAction(String responseAction) {
		this.responseAction = responseAction;
	}
	public String getMinReloadAmount() {
		return minReloadAmount;
	}
	public void setMinReloadAmount(String minReloadAmount) {
		this.minReloadAmount = minReloadAmount;
	}
	
	@Override
	public String toString() {
		return "EuroDTHValidationResponseDo [responseCode=" + responseCode + ", responseMessage=" + responseMessage
				+ ", responseAction=" + responseAction + ", minReloadAmount=" + minReloadAmount + "]";
	}
	

}
