package com.freecharge.recharge.businessDo;

import com.freecharge.common.util.RechargeProductType;

import java.util.LinkedList;
import java.util.List;

public class SuccessfulRechargeDo implements RechargeDoType{

    private String serviceNumber;
    private int operatorId;
    private int circleId;
    private long timestamp;
    private int frequency;
    private transient RechargeProductType rechargeProductType;
    private transient int rank;

    private List<Object> denomList = new LinkedList<Object>();

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public List<Object> getDenomList() {
        return denomList;
    }

    public void setDenomList(List<Object> denomList) {
        this.denomList = denomList;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public RechargeProductType getRechargeProductType() {
        return rechargeProductType;
    }

    public void setRechargeProductType(RechargeProductType rechargeProductType) {
        this.rechargeProductType = rechargeProductType;
    }

    @Override
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}