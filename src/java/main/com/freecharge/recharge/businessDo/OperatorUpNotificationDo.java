package com.freecharge.recharge.businessDo;

import java.io.Serializable;

/**
 * Created by yashveer on 31/5/16.
 */
public class OperatorUpNotificationDo implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    private String userEmailId;
    private String userMobileNo;
    private String serviceNumber;
    private Integer operatorId;
    private Integer circleId;
    private Float amount;
    private String productType;
    private long dateCreated;
    private boolean smsSent;
    private long smsTimestamp;
    private boolean notificationSent;
    private long notificationTimestamp;

    private transient String firstName;
    private transient String operatorName;
    private transient String circleName;


    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isSmsSent() {
        return smsSent;
    }

    public void setSmsSent(boolean smsSent) {
        this.smsSent = smsSent;
    }

    public boolean isNotificationSent() {
        return notificationSent;
    }

    public void setNotificationSent(boolean notificationSent) {
        this.notificationSent = notificationSent;
    }


    public long getSmsTimestamp() {
        return smsTimestamp;
    }

    public void setSmsTimestamp(long smsTimestamp) {
        this.smsTimestamp = smsTimestamp;
    }

    public long getNotificationTimestamp() {
        return notificationTimestamp;
    }

    public void setNotificationTimestamp(long notificationTimestamp) {
        this.notificationTimestamp = notificationTimestamp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    @Override
    public boolean equals(Object obj) {
        if((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        OperatorUpNotificationDo notificationObj = (OperatorUpNotificationDo)obj;

        return userEmailId.equals(notificationObj.getUserEmailId()) && serviceNumber.equals(notificationObj.getServiceNumber());
    }

    @Override public int hashCode() {
        final int prime = 31; int result = 1;
        result = prime * result + ((userEmailId == null) ? 0 : userEmailId.hashCode());
        result = prime * result + ((userMobileNo == null) ? 0 : userMobileNo.hashCode());
        result = prime * result + ((serviceNumber == null) ? 0 : serviceNumber.hashCode());
        result = prime * result + operatorId;
        result = prime * result + circleId;
        result = prime * result + ((productType == null) ? 0 : productType.hashCode());
        //result = prime * result + ((amount == null) ? 0 : amount.intValue());;
        return result;
    }
    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }

    @Override
    public String toString() {
        return "OperatorUpNotificationDo{" +
                "userEmailId='" + userEmailId + '\'' +
                ", serviceNumber='" + serviceNumber + '\'' +
                ", operatorId=" + operatorId +
                ", circleId=" + circleId +
                ", amount=" + amount +
                ", productType='" + productType + '\'' +
                ", dateCreated=" + dateCreated +
                ", smsSent=" + smsSent +
                ", smsTimestamp=" + smsTimestamp +
                ", notificationSent=" + notificationSent +
                ", notificationTimestamp=" + notificationTimestamp +
                '}';
    }
}
