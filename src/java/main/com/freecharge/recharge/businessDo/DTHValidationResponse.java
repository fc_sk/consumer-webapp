package com.freecharge.recharge.businessDo;

public class DTHValidationResponse {
	private String responseCode;
	private String responseMessage;
	
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	public DTHValidationResponse(String responseCode, String responseMessage){
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}
	
	public DTHValidationResponse(){
		
	}
	
}
