package com.freecharge.recharge.businessDo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: May 8, 2012
 * Time: 12:49:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class RechargeDo {

    private String operatorCode;
    private String circleCode;
    private String mobileNo;
    private Float amount;
    private Integer affiliateid;                                                                   
    private String affiliateTransId;
    private String productType;
    private Long inRequestId;
    private CircleOperatorMapping circleOperatorMapping;
    private Integer rechargeRetryCounter;
    private String lookupId;
    private Date transactionDate;
    private String rechargePlanType;
    private String orderId;
    private Integer operatorId;
    private List<String> aggregatorsToSkip = new ArrayList<>();
    
    public RechargeDo() {
		// TODO Auto-generated constructor stub
	}
	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public Integer getRechargeRetryCounter() {
        return rechargeRetryCounter;
    }

    public void setRechargeRetryCounter(Integer rechargeRetryCounter) {
        this.rechargeRetryCounter = rechargeRetryCounter;
    }   
    
    public Long getInRequestId() {
        return inRequestId;
    }

    public void setInRequestId(Long inRequestId) {
        this.inRequestId = inRequestId;
    }

    public String getAffiliateTransId() {
        return affiliateTransId;
    }

    public void setAffiliateTransId(String affiliateTransId) {
        this.affiliateTransId = affiliateTransId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getAffiliateid() {
        return affiliateid;
    }

    public void setAffiliateid(Integer affiliateid) {
        this.affiliateid = affiliateid;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

	public CircleOperatorMapping getCircleOperatorMapping() {
		return circleOperatorMapping;
	}

	public void setCircleOperatorMapping(CircleOperatorMapping circleOperatorMapping) {
		this.circleOperatorMapping = circleOperatorMapping;
	}
    
	 
	public String getRechargePlanType() {
		return rechargePlanType;
	}

	public void setRechargePlanType(String rechargePlanType) {
		this.rechargePlanType = rechargePlanType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public Integer getOperatorId() {
	       return operatorId;
	}
	public void setOperatorId(Integer operatorId) {
	       this.operatorId = operatorId;
	}
    public List<String> getAggregatorsToSkip() {
        return aggregatorsToSkip;
    }

    public void setAggregatorsToSkip(List<String> aggregatorsToSkip) {
        this.aggregatorsToSkip = aggregatorsToSkip == null ? new ArrayList<String>() : aggregatorsToSkip;
    }
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RechargeDo [operatorCode=").append(operatorCode).append(", circleCode=").append(circleCode)
				.append(", mobileNo=").append(mobileNo).append(", amount=").append(amount).append(", affiliateid=")
				.append(affiliateid).append(", affiliateTransId=").append(affiliateTransId).append(", productType=")
				.append(productType).append(", inRequestId=").append(inRequestId).append(", circleOperatorMapping=")
				.append(circleOperatorMapping).append(", rechargeRetryCounter=").append(rechargeRetryCounter)
				.append(", lookupId=").append(lookupId).append(", transactionDate=").append(transactionDate)
				.append(", rechargePlanType=").append(rechargePlanType).append(", orderId=").append(orderId)
				.append(", operatorId=").append(operatorId).append(", aggregatorsToSkip=").append(aggregatorsToSkip)
				.append("]");
		return builder.toString();
	}
    
    
}
