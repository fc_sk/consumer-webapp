package com.freecharge.recharge.businessDo;

public class RechargeResInfo {

	private String Code;
	private String ClientRefId;
	private String Description;
	private String Commission;
	private String TransactionId;
	private String TransactionValue;
	private String UpdatedBalance;
	private String Status;
	private String CardSerialNo;

	public String getCardSerialNo() {
		return CardSerialNo;
	}

	public void setCardSerialNo(String cardSerialNo) {
		CardSerialNo = cardSerialNo;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getClientRefId() {
		return ClientRefId;
	}

	public void setClientRefId(String clientRefId) {
		ClientRefId = clientRefId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getCommission() {
		return Commission;
	}

	public void setCommission(String commission) {
		Commission = commission;
	}

	public String getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}

	public String getTransactionValue() {
		return TransactionValue;
	}

	public void setTransactionValue(String transactionValue) {
		TransactionValue = transactionValue;
	}

	public String getUpdatedBalance() {
		return UpdatedBalance;
	}

	public void setUpdatedBalance(String updatedBalance) {
		UpdatedBalance = updatedBalance;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
	
}
