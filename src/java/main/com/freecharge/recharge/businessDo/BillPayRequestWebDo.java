package com.freecharge.recharge.businessDo;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.freecharge.infrastructure.billpay.beans.CreateBillPayRequest;

public class BillPayRequestWebDo {

	private String requestId;
	private Integer userId;
	private String productType;
	private CreateBillPayRequest createBillPayRequest;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate lastPaidDate;

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public CreateBillPayRequest getCreateBillPayRequest() {
		return createBillPayRequest;
	}

	public void setCreateBillPayRequest(CreateBillPayRequest createBillPayRequest) {
		this.createBillPayRequest = createBillPayRequest;
	}

	public LocalDate getLastPaidDate() {
		return lastPaidDate;
	}

	public void setLastPaidDate(LocalDate lastPaidDate) {
		this.lastPaidDate = lastPaidDate;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

}
