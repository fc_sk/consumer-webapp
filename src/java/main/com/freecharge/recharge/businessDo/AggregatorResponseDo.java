package com.freecharge.recharge.businessDo;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: May 10, 2012
 * Time: 12:25:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AggregatorResponseDo {
   
    private Long requestId;
    private String aggrName;
    private String aggrResponseCode;
    private String aggrResponseMessage;
    private String aggrReferenceNo;
    private String aggrOprRefernceNo;
    private String rawRequest;
    private String rawResponse;
    private Date requestTime;
    private Date responseTime;
    private String transactionStatus;
    private boolean aggregatorErrorWhileQuerying;
    private String googleSerial;
    private String googleCode;


    public String getGoogleSerial() {
		return googleSerial;
	}

	public void setGoogleSerial(String googleSerial) {
		this.googleSerial = googleSerial;
	}

	public String getGoogleCode() {
		return googleCode;
	}

	public void setGoogleCode(String googleCode) {
		this.googleCode = googleCode;
	}

	public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }
    
    public String getAggrName() {
        return aggrName;
    }

    public void setAggrName(String aggrName) {
        this.aggrName = aggrName;
    }

    public String getAggrResponseCode() {
        return aggrResponseCode;
    }

    public void setAggrResponseCode(String aggrResponseCode) {
        this.aggrResponseCode = aggrResponseCode;
    }

    public String getAggrResponseMessage() {
        return aggrResponseMessage;
    }

    public void setAggrResponseMessage(String aggrResponseMessage) {
        this.aggrResponseMessage = aggrResponseMessage;
    }

    public String getAggrReferenceNo() {
        return aggrReferenceNo;
    }

    public void setAggrReferenceNo(String aggrReferenceNo) {
        this.aggrReferenceNo = aggrReferenceNo;
    }

    public String getAggrOprRefernceNo() {
        return aggrOprRefernceNo;
    }

    public void setAggrOprRefernceNo(String aggrOprRefernceNo) {
        this.aggrOprRefernceNo = aggrOprRefernceNo;
    }

    public String getRawRequest() {
        return rawRequest;
    }

    public void setRawRequest(String rawRequest) {
        this.rawRequest = rawRequest;
    }

    public String getRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(String rawResponse) {
        this.rawResponse = rawResponse;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public boolean isAggregatorErrorWhileQuerying() {
        return aggregatorErrorWhileQuerying;
    }

    public void setAggregatorErrorWhileQuerying(boolean aggregatorErrorWhileQuerying) {
        this.aggregatorErrorWhileQuerying = aggregatorErrorWhileQuerying;
    }

    @Override
    public String toString() {
        return "AggregatorResponseDo{" +
                "requestId=" + requestId +
                ", aggrName='" + aggrName + '\'' +
                ", aggrResponseCode='" + aggrResponseCode + '\'' +
                ", aggrResponseMessage='" + aggrResponseMessage + '\'' +
                ", aggrReferenceNo='" + aggrReferenceNo + '\'' +
                ", aggrOprRefernceNo='" + aggrOprRefernceNo + '\'' +
                ", rawRequest='" + rawRequest + '\'' +
                ", rawResponse='" + rawResponse + '\'' +
                ", requestTime=" + requestTime +
                ", responseTime=" + responseTime +
                ", transactionStatus='" + transactionStatus + '\'' +
                ", aggregatorErrorWhileQuerying=" + aggregatorErrorWhileQuerying +
                '}';
    }
}
