package com.freecharge.recharge.businessDo;

public class EuroValidationResponseDo {

	private String rawRequest;
	private String responseCode;
	private String responseMessage;
	private String responseAction;
	private String planType;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseAction() {
		return responseAction;
	}

	public void setResponseAction(String responseAction) {
		this.responseAction = responseAction;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}	

	public String getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	@Override
	public String toString() {
		return "EuroValidationResponseDo [rawRequest=" + rawRequest + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", responseAction=" + responseAction + ", planType="
				+ planType + "]";
	}	

}
