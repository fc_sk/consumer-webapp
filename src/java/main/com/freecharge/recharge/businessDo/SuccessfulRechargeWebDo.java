package com.freecharge.recharge.businessDo;

import java.util.LinkedList;
import java.util.List;

public class SuccessfulRechargeWebDo {
    private String serviceNumber;
    private int operatorId;
    private int circleId;
    private long timestamp;
    private boolean status;

    private List<Object> denomList = new LinkedList<Object>();

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public List<Object> getDenomList() {
        return denomList;
    }

    public void setDenomList(List<Object> denomList) {
        this.denomList = denomList;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @SuppressWarnings("unchecked")
    public void setRechargeDetails(SuccessfulRechargeDo successfulRechargeDo) {
        this.serviceNumber = successfulRechargeDo.getServiceNumber();
        this.operatorId = successfulRechargeDo.getOperatorId();
        this.circleId = successfulRechargeDo.getCircleId();
        this.timestamp = successfulRechargeDo.getTimestamp();
        List<Object> successfulDenomList=successfulRechargeDo.getDenomList();
        if (!hasTopupSpecial(this.operatorId)) {
            List<String> denomDetails = null;
            for(Object denoms: successfulDenomList){
                denomDetails = (List<String>)denoms;
                if("topup".equals(denomDetails.get(1))){
                    denomDetails.set(1,"");
                }
            }
        }
        this.setDenomList(successfulDenomList);
        // setting true by default.
        this.status = true;
    }

    private boolean hasTopupSpecial(int operatorId) {
        int[] validOperatorIds = { 3, 5, 9, 13, 24, 30, 43, 54 };
        for (int validOperatorId : validOperatorIds) {
            if (operatorId == validOperatorId)
                return true;
        }
        return false;
    }

}