package com.freecharge.recharge.businessDo;

/**
 * A simple pojo to encapsulate whether
 * an aggregator's wallet balance is
 * within alerting limits.
 * @author arun
 *
 */
public class AggregatorBalanceStatus {
    public static enum BalanceStatus {
        OK, Warn, Critical;
    }
    
    private BalanceStatus balanceStatus;
    private Double currentBalance;
    private Double warnThresholdValue;
    private Double criticalThresholdValue;
    
    public BalanceStatus getBalanceStatus() {
        return balanceStatus;
    }
    public void setBalanceStatus(BalanceStatus balanceStatus) {
        this.balanceStatus = balanceStatus;
    }
    
    public Double getCurrentBalance() {
        return currentBalance;
    }
    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }
    
    public Double getWarnThresholdValue() {
        return warnThresholdValue;
    }
    public void setWarnThresholdValue(Double warnThresholdValue) {
        this.warnThresholdValue = warnThresholdValue;
    }
    
    public Double getCriticalThresholdValue() {
        return criticalThresholdValue;
    }
    public void setCriticalThresholdValue(Double criticalThresholdValue) {
        this.criticalThresholdValue = criticalThresholdValue;
    }
}
