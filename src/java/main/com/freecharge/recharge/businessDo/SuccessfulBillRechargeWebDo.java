package com.freecharge.recharge.businessDo;

public class SuccessfulBillRechargeWebDo {

    private String serviceNumber;
    private int    operatorId;
    private int    circleId;
    private String amount;
    private int    billerType;
    private String additionalInfo1;
    private String additionalInfo2;
    private String additionalInfo3;
    private String additionalInfo4;
    private String additionalInfo5;
    private long timestamp;
    private int rank;

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAdditionalInfo1() {
        return additionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        this.additionalInfo1 = additionalInfo1;
    }

    public String getAdditionalInfo2() {
        return additionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        this.additionalInfo2 = additionalInfo2;
    }

    public String getAdditionalInfo3() {
        return additionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        this.additionalInfo3 = additionalInfo3;
    }

    public String getAdditionalInfo4() {
        return additionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        this.additionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo5() {
        return additionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        this.additionalInfo5 = additionalInfo5;
    }
    
    

    public int getBillerType() {
        return billerType;
    }

    public void setBillerType(int billerType) {
        this.billerType = billerType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setBillRechargeDetails(SuccessfulBillRechargeDo successfulBillRechargeDo) {
        this.serviceNumber = successfulBillRechargeDo.getServiceNumber();
        this.operatorId = successfulBillRechargeDo.getOperatorId();
        this.circleId = successfulBillRechargeDo.getCircleId();
        this.amount = successfulBillRechargeDo.getAmount();
        this.billerType=successfulBillRechargeDo.getBillerType();
		this.additionalInfo1 = successfulBillRechargeDo.getAdditionalInfo1() != null
				? successfulBillRechargeDo.getAdditionalInfo1() : this.serviceNumber;
        this.additionalInfo2 = successfulBillRechargeDo.getAdditionalInfo2();
        this.additionalInfo3 = successfulBillRechargeDo.getAdditionalInfo3();
        this.additionalInfo4 = successfulBillRechargeDo.getAdditionalInfo4();
        this.additionalInfo5 = successfulBillRechargeDo.getAdditionalInfo5();
        this.timestamp = successfulBillRechargeDo.getTimestamp();
        this.rank = successfulBillRechargeDo.getRank();

    }

}
