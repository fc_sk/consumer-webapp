package com.freecharge.recharge.businessDo;

public class SuccessfulRechargeDetails {
    private String emailId;
    private String serviceNumber;
    private String productType;
    private String rechargePlanType;
    private String amount;
    private int operatorId;
    private int circleId;

    public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    public String getServiceNumber() {
        return serviceNumber;
    }
    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getProductType() {
        return productType;
    }
    public int getOperatorId() {
        return operatorId;
    }
    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }
    public int getCircleId() {
        return circleId;
    }
    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getRechargePlanType() {
        return rechargePlanType;
    }
    public void setRechargePlanType(String rechargePlanType) {
        this.rechargePlanType = rechargePlanType;
    }
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
	@Override
	public String toString() {
		return "SuccessfulRechargeDetails [emailId=" + emailId + ", serviceNumber=" + serviceNumber + ", productType="
				+ productType + ", rechargePlanType=" + rechargePlanType + ", amount=" + amount + ", operatorId="
				+ operatorId + ", circleId=" + circleId + "]";
	}  
    

}
