package com.freecharge.recharge.businessDo;

import com.freecharge.common.util.RechargeProductType;

/**
 * Created by yashveer on 26/5/16.
 */
public interface RechargeDoType {


    public int getFrequency();
    public RechargeProductType getRechargeProductType();
    public void setRechargeProductType(RechargeProductType rechargeProductType);
    public long getTimestamp();
    public int getRank();
    public void setRank(int rank);
}
