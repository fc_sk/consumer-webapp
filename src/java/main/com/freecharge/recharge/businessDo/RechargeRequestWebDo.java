package com.freecharge.recharge.businessDo;

import java.io.Serializable;
import java.util.Map;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RechargeRequestWebDo implements Serializable {
    private static final long   serialVersionUID = 1L;
    private String              mobileNumber;
    

	private Integer             serviceType;
    private Integer             operatorId;
    private Integer             circleId;
    private Double              amount;
    private String              txnReferenceNumber;
    private String              requestId;
    private String 				planType;
    private Map<String, String> metaData;
    private String              accessKey;
    private String              signature;
    private String              apiVersion;
    private int                 userId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate           lastPaidDate;

    public LocalDate getLastPaidDate() {
        return lastPaidDate;
    }

    public void setLastPaidDate(LocalDate lastPaidDate) {
        this.lastPaidDate = lastPaidDate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getTxnReferenceNumber() {
        return txnReferenceNumber;
    }

    public void setTxnReferenceNumber(String txnReferenceNumber) {
        this.txnReferenceNumber = txnReferenceNumber;
    }

    public Map<String, String> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, String> metaData) {
        this.metaData = metaData;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
		return requestId;
	}

	public String getOrderId() {
        return this.requestId;
    }

    public void setOrderId(String orderId) {
        this.requestId = orderId;
    }

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}
	
	@Override
	public String toString() {
		return "RechargeRequestWebDo [mobileNumber=" + mobileNumber
				+ ", serviceType=" + serviceType + ", operatorId=" + operatorId
				+ ", circleId=" + circleId + ", amount=" + amount
				+ ", txnReferenceNumber=" + txnReferenceNumber + ", requestId="
				+ requestId + ", planType=" + planType + ", metaData="
				+ metaData + ", accessKey=" + accessKey + ", signature="
				+ signature + ", apiVersion=" + apiVersion + ", userId="
				+ userId + ", lastPaidDate=" + lastPaidDate + "]";
	}
}
