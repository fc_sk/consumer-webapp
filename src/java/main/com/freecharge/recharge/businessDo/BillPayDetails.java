package com.freecharge.recharge.businessDo;

import com.fasterxml.jackson.annotation.JsonInclude;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.infrastructure.billpay.types.GenericBillWithSubscriberNumber;

@JsonInclude(Include.NON_NULL)
public class BillPayDetails {

	private String serviceNumber;
	private int operatorId;
	private int circleId;
	private String operatorName;
	private String circleName;
	private double amount;
	private GenericBillWithSubscriberNumber billWithSubscriberNumber;
	private ViewUserBillsResponse viewUserBillsResponse;
	private String userMobileNumber;
	private String userEmailId;
	private String userName;

	public String getServiceNumber() {
		return serviceNumber == null ? billWithSubscriberNumber.getAdditionalInfo1() : serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public int getCircleId() {
		return circleId;
	}

	public void setCircleId(int circleId) {
		this.circleId = circleId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public GenericBillWithSubscriberNumber getBillWithSubscriberNumber() {
		return billWithSubscriberNumber;
	}

	public void setBillWithSubscriberNumber(GenericBillWithSubscriberNumber billWithSubscriberNumber) {
		this.billWithSubscriberNumber = billWithSubscriberNumber;
	}

	public String getUserMobileNumber() {
		return userMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public ViewUserBillsResponse getViewUserBillsResponse() {
		return viewUserBillsResponse;
	}

	public void setViewUserBillsResponse(ViewUserBillsResponse viewUserBillsResponse) {
		this.viewUserBillsResponse = viewUserBillsResponse;
	}
}
