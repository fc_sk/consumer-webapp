package com.freecharge.recharge.businessDo;

import java.sql.Timestamp;
import java.util.Map;

import com.freecharge.web.webdo.RechargeInitiateWebDo;

public class RechargeResponseWebDo {
    private String statusCode;
    private String freechargeTransactionId;
    private String operatorTransactionId;
    private String transactionDate;
    private boolean status;
    private String requestId;
    private String apiVersion;
    private String orderId;
    private Timestamp autopayRefillTransactionDate;
    
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getFreechargeTransactionId() {
        return freechargeTransactionId;
    }

    public void setFreechargeTransactionId(String freechargeTransactionId) {
        this.freechargeTransactionId = freechargeTransactionId;
    }

    public String getOperatorTransactionId() {
        return operatorTransactionId;
    }

    public void setOperatorTransactionId(String operatorTransactionId) {
        this.operatorTransactionId = operatorTransactionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}	
	
	public Timestamp getAutopayRefillTransactionDate() {
		return autopayRefillTransactionDate;
	}

	public void setAutopayRefillTransactionDate(Timestamp autopayRefillTransactionDate) {
		this.autopayRefillTransactionDate = autopayRefillTransactionDate;
	}

	public void getRechargeResponseWebDo(Map<String, Object> rechargeResponse) {
        this.setApiVersion((String) rechargeResponse.get("apiVersion"));
        this.setFreechargeTransactionId((String) rechargeResponse.get("freechargeTransactionId"));
        this.setOperatorTransactionId((String) rechargeResponse.get("operatorTransactionId"));
        this.setRequestId((String) rechargeResponse.get("requestId"));
        this.setStatusCode((String) rechargeResponse.get("statusCode"));
        this.setTransactionDate((String) rechargeResponse.get("transactionDate"));
    }

    public void getRechargeResponseWebDo(RechargeInitiateWebDo rechargeInitiateWebDo) {
        this.statusCode = rechargeInitiateWebDo.getRechargeResponse();
        this.freechargeTransactionId = rechargeInitiateWebDo.getOrderId();   
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
}
