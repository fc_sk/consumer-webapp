package com.freecharge.recharge.businessDo;



public class ArrayOfTransactionStatus {

	private String Code;
	private String ClientRefId;
	private String Description;
	private String Commission;
	private String TransactionId;
	private String TransactionValue;
	private String UpdatedBalance;
	private String Status;
	private String CardSerialNo;
	private String ServiceProviderId;
	private String TransactionAmount;
	private String TransactionDate;
	private String TransactionRefId;
	private String TransactionStatus;
	
	public String getServiceProviderId() {
		return ServiceProviderId;
	}

	public void setServiceProviderId(String serviceProviderId) {
		ServiceProviderId = serviceProviderId;
	}

	public String getTransactionAmount() {
		return TransactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		TransactionAmount = transactionAmount;
	}

	public String getTransactionDate() {
		return TransactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}

	public String getTransactionRefId() {
		return TransactionRefId;
	}

	public void setTransactionRefId(String transactionRefId) {
		TransactionRefId = transactionRefId;
	}

	public String getTransactionStatus() {
		return TransactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		TransactionStatus = transactionStatus;
	}

	public String getCardSerialNo() {
		return CardSerialNo;
	}

	public void setCardSerialNo(String cardSerialNo) {
		CardSerialNo = cardSerialNo;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getClientRefId() {
		return ClientRefId;
	}

	public void setClientRefId(String clientRefId) {
		ClientRefId = clientRefId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getCommission() {
		return Commission;
	}

	public void setCommission(String commission) {
		Commission = commission;
	}

	public String getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}

	public String getTransactionValue() {
		return TransactionValue;
	}

	public void setTransactionValue(String transactionValue) {
		TransactionValue = transactionValue;
	}

	public String getUpdatedBalance() {
		return UpdatedBalance;
	}

	public void setUpdatedBalance(String updatedBalance) {
		UpdatedBalance = updatedBalance;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
	
}
