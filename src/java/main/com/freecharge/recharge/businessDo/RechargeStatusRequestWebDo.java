package com.freecharge.recharge.businessDo;

public class RechargeStatusRequestWebDo {
    private String freechargeTransactionId;
    private String accessKey;
    private String signature;
    private String apiVersion;
    
    public String getFreechargeTransactionId() {
        return freechargeTransactionId;
    }

    public void setFreechargeTransactionId(String freechargeTransactionId) {
        this.freechargeTransactionId = freechargeTransactionId;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
