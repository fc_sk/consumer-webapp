package com.freecharge.recharge.eventprocessing;

import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.eventprocessing.event.FreefundBaseEvent;

public class FulfillmentEvent extends FreefundBaseEvent {
	private TransactionStatus transactionStatus;
    private UserDetails userDetails;

    public FulfillmentEvent(Object source) {
        super(source);
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }


}
