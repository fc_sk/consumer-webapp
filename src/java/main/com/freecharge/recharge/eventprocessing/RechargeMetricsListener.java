package com.freecharge.recharge.eventprocessing;

import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.exceptions.HarmlessException;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.util.RechargeConstants;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RechargeMetricsListener implements ApplicationListener<FulfillmentEvent> {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    MetricsClient metricsClient;
    
    @Autowired
    private InService inService;

    @Autowired
    private RechargeMetricService rechargeMetricService;
    
    @Autowired
    private BillPaymentService billPaymentService;

    @Autowired
    private OrderService orderService;
    
    @Override
    public void onApplicationEvent(FulfillmentEvent event) {
        TransactionStatus transactionStatus = event.getTransactionStatus();
        UserDetails userDetails = event.getUserDetails();

        try {
            InResponse inResponse = inService.getInResponseByOrderID(userDetails.getOrderId());
            TxnHomePageBusinessDO homePage = orderService.getHomePageForOrder(userDetails.getOrderId());
            String productType = homePage.getProductType();
            
            boolean isOperatorRetry = false;
            List<RechargeRetryData> operaratorRetryDatas = inService.getRechargeRetryDataForOrder(userDetails.getOrderId());
            if (!FCUtil.isEmpty(operaratorRetryDatas)) {
                isOperatorRetry = true;
            }
            
            if (inResponse != null){
                InRequest inRequest = inService.getInRequest(inResponse.getRequestId());
                List<InTransactions> inTransaction = inService.getInTransactionsByRequestId(inRequest.getRequestId());
                rechargeMetricService.cacheRechargeDetails(inRequest, inResponse, inTransaction, homePage, isOperatorRetry);
            }
            
            if (!isOperatorRetry && RechargeConstants.POST_PAID_PRODUCT_TYPE.equalsIgnoreCase(userDetails.getProductType())) {
                BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(userDetails .getOrderId());
                if (billPaymentStatus != null) {
                    rechargeMetricService.postpaidCacheFailureDetails(homePage, billPaymentStatus);
                }
            }
            
            if (isRechargeSuccess(transactionStatus, userDetails)) {
                metricsClient.timeStampEvent(userDetails.getOrderId(), MetricsClient.EventName.RechargeSuccess);
                metricsClient.recordLatency("UserSession", "RechargeSuccess", userDetails.getOrderId(), MetricsClient.EventName.RechargeAttempt);
                metricsClient.recordLatency("UserSession", "RechargePendingToSuccess", userDetails.getOrderId(), MetricsClient.EventName.RechargePending);
                metricsClient.recordEvent("Recharge", transactionStatus.getServiceProvider() + "." + transactionStatus.getOperator(), "Success");
                metricsClient.recordEvent("Recharge.Product", productType, "Success");
                logger.info("Publishing success transaction even to metric client for orderId" + userDetails.getOrderId());
                metricsClient.recordLatency("UserSession", "FulfillmentComplete", userDetails.getOrderId(), MetricsClient.EventName.RechargeSuccess);
            }

            if (isRechargeFailure(transactionStatus, userDetails)) {
                metricsClient.timeStampEvent(userDetails.getOrderId(), MetricsClient.EventName.RechargeFailure);
                metricsClient.recordLatency("UserSession", "RechargePendingToFailure", userDetails.getOrderId(), MetricsClient.EventName.RechargePending);
                metricsClient.recordLatency("UserSession", "RechargeFailure", userDetails.getOrderId(), MetricsClient.EventName.RechargeAttempt);
                metricsClient.recordEvent("Recharge", transactionStatus.getServiceProvider() + "." + transactionStatus.getOperator(), "Failure");
                metricsClient.recordEvent("Recharge.Product", productType, "Failure");

                if (inResponse != null) {
                    if (isRecentTransaction(inResponse)) {
                        metricsClient.recordEvent("Recharge." + inResponse.getAggrName(), "ResponseCode." + transactionStatus.getOperator(), inResponse.getAggrRespCode());
                    }
                }
                logger.info("Publishing failure transaction even to metric client for orderId" + userDetails.getOrderId());
                metricsClient.recordLatency("UserSession", "FulfillmentComplete", userDetails.getOrderId(), MetricsClient.EventName.RechargeFailure);
            }

            if (isRechargePending(transactionStatus, userDetails) ) {
                metricsClient.timeStampEvent(userDetails.getOrderId(), MetricsClient.EventName.RechargePending);
                metricsClient.recordLatency("UserSession", "RechargePending", userDetails.getOrderId(), MetricsClient.EventName.RechargeAttempt);
            }

            if (isBillPaymentSuccess(transactionStatus, userDetails)) {
            	metricsClient.recordEvent("Postpaid",transactionStatus.getOperator(), "Success");
            	metricsClient.recordEvent("Recharge.Product", productType, "Success");
            	metricsClient.recordEvent("Bill Payment", "bpf_success", "bill_payment_fulfilment_successful");
            }
            
            if (isBillPaymentFailure(transactionStatus, userDetails) ) {
            	metricsClient.recordEvent("Postpaid",transactionStatus.getOperator(), "Failure");
            	metricsClient.recordEvent("Recharge.Product", productType, "Failure");
            	metricsClient.recordEvent("Bill Payment", "bpf_failure", "bill_payment_fulfilment_failure");
            }
        } catch (Exception e) {
            metricsClient.recordEvent("Recharge", "MetricsHandling", "Error");
            logger.error(String.format("Exception while recording metrics: %s", userDetails.getOrderId()),
                    new HarmlessException(e));
        }
    }

    private boolean isBillPaymentFailure(TransactionStatus transactionStatus, UserDetails userDetails) {
        return !inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus() )
                && FCUtil.isBillPostpaidType(userDetails.getProductType());
    }

    private boolean isBillPaymentSuccess(TransactionStatus transactionStatus, UserDetails userDetails) {
        return inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())
                && FCUtil.isBillPostpaidType(userDetails.getProductType());
    }

    private boolean isRechargePending(TransactionStatus transactionStatus, UserDetails userDetails) {
        return transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)
                && FCUtil.isRechargeProductType(userDetails.getProductType());
    }

    private boolean isRechargeFailure(TransactionStatus transactionStatus, UserDetails userDetails) {
        return !transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)
                && FCUtil.isRechargeProductType(userDetails.getProductType()) && !inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus());
    }

    private boolean isRechargeSuccess(TransactionStatus transactionStatus, UserDetails userDetails) {
        return inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus()) && FCUtil.isRechargeProductType(userDetails.getProductType());
    }

    private boolean isRecentTransaction(InResponse inResponse) {
        final int UPDATION_WINDOW = 30;

        DateTime attemptTime = new DateTime(inResponse.getCreatedTime());
        DateTime minAttemptTime = new DateTime().minusMinutes(UPDATION_WINDOW);
        
        if (attemptTime.isAfter(minAttemptTime.getMillis())) {
            return true;
        }
        
        return false;
    }
    
}
