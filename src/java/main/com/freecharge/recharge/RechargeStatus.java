package com.freecharge.recharge;

public enum RechargeStatus {

	NOT_AVAILABLE("Not available"),
	RECHARGE_SUCCESSFUL("Recharge successful"),
	RECHARGE_UNDER_PROCESS("Recharge under process"),
	RECHARGE_PENDING("Recharge pending"),
	RECHARGE_FAILED("Recharge failed");

	private final String message;

	private RechargeStatus(String msg) {
		this.message = msg;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return getMessage();
	}

}
