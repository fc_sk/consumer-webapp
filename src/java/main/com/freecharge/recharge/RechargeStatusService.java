package com.freecharge.recharge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.service.InService;

@Service
public class RechargeStatusService {

	public static final String NA = "NA";
	public static final String SUCCESS1 = "00";
	public static final String SUCCESS2 = "0";
	public static final String RECHARGE_UNDER_PROCESS ="08";
	public static final String RECHARGE_PENDING = "-1";

	@Autowired
    private InService inService;
	
	@Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

	public RechargeStatus getRechargeStatus(String orderID) {
		String responseCode = NA;
		UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderID);
    	if(userTransactionHistory!=null){
    		responseCode = userTransactionHistory.getTransactionStatus();
    	}
		if (responseCode.equals(SUCCESS1) || responseCode.equals(SUCCESS2)) {
			return RechargeStatus.RECHARGE_SUCCESSFUL;
		} else if (NA.equals(responseCode)) {
			return RechargeStatus.NOT_AVAILABLE;
		} else if (responseCode.equals(RECHARGE_UNDER_PROCESS)) {
			return RechargeStatus.RECHARGE_UNDER_PROCESS;
		} else if (responseCode.equals(RECHARGE_PENDING)) {
			return RechargeStatus.RECHARGE_UNDER_PROCESS;
		}
		return RechargeStatus.RECHARGE_FAILED;
	}

}
