package com.freecharge.recharge;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.google.common.collect.ImmutableMap;

/**
 * Listens to recharge success event to record numbers into
 * serviceNumberProductMap incase the servicenumber is new to the system or if
 * the productType has been updated.
 * 
 * @author sayak
 *
 */
@Service
public class ProductPredictionListener implements ApplicationListener<RechargeSuccessEvent> {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    MetricsClient metricsClient;

    @Autowired
    AmazonDynamoDBAsync dynamoClientMum;
    
    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    OperatorCircleService operatorCircleService;
   
    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {
        String orderId = event.getOrderId();

       
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                .findUserTransactionHistoryByOrderId(orderId);

        String serviceNumber=userTransactionHistory.getSubscriberIdentificationNumber();
        String productType= userTransactionHistory.getProductType();
        CircleMaster circleMaster = operatorCircleService.getCircle(userTransactionHistory.getServiceRegion());
        OperatorMaster operatorMaster = operatorCircleService.getOperator(userTransactionHistory.getServiceProvider());
        String operatorId= String.valueOf(operatorMaster.getOperatorMasterId());
        String circleId= String.valueOf(circleMaster.getCircleMasterId());
        
        logger.debug("obtained transaction details from order id. OrderID :  " + orderId);
        if(FCUtil.isMobileRecharge(productType)||FCUtil.isDataCard(productType)){
        Map<String, Object> fetchedItem = operatorCircleService.fetchFromDynamoProductTypeData(serviceNumber);
        if (fetchedItem.isEmpty()) {
            makeEntryinDynamoDB(serviceNumber, productType, operatorId, circleId);
        } else {
            if ((!fetchedItem.get("productType").equals(productType))
                    || (!fetchedItem.get("operatorId").equals(operatorId))
                    || (!fetchedItem.get("circleId").equals(circleId))) {
                updateEntryinDynamoDB(serviceNumber, productType,operatorId,circleId);
            }
        }
    }
    }

    private void updateEntryinDynamoDB(String serviceNumber, String productType, String operatorId, String circleId) {

        Map<String, AttributeValue> updateItemKey = new HashMap<String, AttributeValue>();
        updateItemKey.put("serviceNumber", new AttributeValue(serviceNumber));

        Map<String, AttributeValueUpdate> updateItemAttrs = new HashMap<String, AttributeValueUpdate>();
        updateItemAttrs.put("productType",
                new AttributeValueUpdate().withValue(new AttributeValue().withS(productType)));
        updateItemAttrs.put("operatorId",
                new AttributeValueUpdate().withValue(new AttributeValue().withS(operatorId)));
        updateItemAttrs.put("circleId",
                new AttributeValueUpdate().withValue(new AttributeValue().withS(circleId)));
        final long startTime = System.currentTimeMillis();
        try {
            UpdateItemRequest updateItemRequest = new UpdateItemRequest("serviceNumberProductMap", updateItemKey,
                    updateItemAttrs);
            dynamoClientMum.updateItemAsync(updateItemRequest);
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while recording ProductPredictorData", e);
            metricsClient.recordEvent("DynamoDB", "updateItemAsync", "AmazonServiceException");
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while recording ProductPredictorData", e);
            metricsClient.recordEvent("DynamoDB", "updateItemAsync", "AmazonClientException");
        } finally {
            metricsClient.recordLatency("DynamoDB", "updateItemAsync", System.currentTimeMillis() - startTime);
        }

    }

    private void makeEntryinDynamoDB(String serviceNumber, String productType, String operatorId, String circleId) {

        Map<String, AttributeValue> item = new HashMap<>();

        item.put("serviceNumber", new AttributeValue(serviceNumber));
        item.put("productType", new AttributeValue(productType));
        item.put("operatorId", new AttributeValue(operatorId));
        item.put("circleId", new AttributeValue(circleId));

        final long startTime = System.currentTimeMillis();
        try {
            dynamoClientMum.putItem(new PutItemRequest("serviceNumberProductMap", item));
            metricsClient.recordEvent("DynamoDB", "PutItem", "Success");
            logger.info("Done recording ProductPredictor Data [ServiceNumber:" + serviceNumber + ", ProductType: "
                    + productType + ", OperatorId: " + operatorId + ", CircleId: " + circleId + "]");
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while recording ProductPredictorData", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonServiceException");
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while recording ProductPredictorData", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonClientException");
        } finally {
            metricsClient.recordLatency("DynamoDB", "PutItem", System.currentTimeMillis() - startTime);
        }

    }

   
}
