package com.freecharge.recharge.retry;

import org.joda.time.DateTime;

public class RetryCreationResponse {
    private RetryCreationStatus status;
    private DateTime nextRetryTime;
    
    public static final RetryCreationResponse NotCreated = new RetryCreationResponse(RetryCreationStatus.NotCreated, null);
    public static final RetryCreationResponse MarkedAsExpired = new RetryCreationResponse(RetryCreationStatus.MarkedAsExpired, null);
    public static final RetryCreationResponse MarkedAsComplete = new RetryCreationResponse(RetryCreationStatus.MarkedAsComplete, null);
    
    public RetryCreationResponse(RetryCreationStatus status, DateTime nextRetryTime) {
        this.status = status;
        this.nextRetryTime = nextRetryTime;
    }
    
    public RetryCreationResponse() {
        // TODO Auto-generated constructor stub
    }
    
    public RetryCreationStatus getStatus() {
        return status;
    }
    public void setStatus(RetryCreationStatus status) {
        this.status = status;
    }
    
    public DateTime getNextRetryTime() {
        return nextRetryTime;
    }
    public void setNextRetryTime(DateTime nextRetryTime) {
        this.nextRetryTime = nextRetryTime;
    }
}
