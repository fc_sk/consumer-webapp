package com.freecharge.recharge.retry;

public enum RetryCreationStatus {
    Created,
    Updated,
    MarkedAsComplete,
    MarkedAsExpired,
    NotCreated;
}
