package com.freecharge.recharge.cron;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This class holds the order IDs that are
 * enqueued for status check in order to 
 * prevent duplicate status check calls.
 * @author arun
 *
 */
@Service
public class RechargeStatusIdempotencyCache extends RedisCacheManager  {
    private Logger logger = LoggingFactory.getLogger(getClass());

    private final int QUEUE_LINGER_PERIOD_MINUTES = 15;
    private final int PROMOCODE_IDEMPOTENCY_WINDOW_DAYS = 2;

    @Autowired
    private RedisTemplate<String, Object> rechargeIdempotencyCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.rechargeIdempotencyCacheTemplate;
    }
    
    public boolean isInQueue(String queuePayLoad, String queueName) {
        try {
            return this.isInQueue(getQueueKey(queueName, queuePayLoad));
        } catch (Exception e) {
            logger.error("Got exception while checking for enqueue for queuePayLoad: [" + String.valueOf(queuePayLoad) + "]. Queue Name:[" + String.valueOf(queueName) + "]", e);
            return false;
        }
    }

    public void markEnqueue(String queuePayLoad, String queueName) {
        try {
            this.markEnqueue(queuePayLoad, queueName, QUEUE_LINGER_PERIOD_MINUTES, TimeUnit.MINUTES);
        } catch (Exception e) {
            logger.error("Got exception while marking as enqueued for queuePayLoad: [" + String.valueOf(queuePayLoad) + "]. Queue Name:[" + String.valueOf(queueName) + "]", e);
        }
    }
    
    public void markDequeue(String queuePayLoad, String queueName) {
        try {
            this.delete(getQueueKey(queueName, queuePayLoad));
        } catch (Exception e) {
            logger.error("Got exception while marking as dequeued for queuePayLoad: [" + String.valueOf(queuePayLoad) + "]. Queue Name:[" + String.valueOf(queueName) + "]", e);
        }
    }

    private boolean isInQueue(String queuePayLoad) {
        Boolean isEnqueued = (Boolean) this.get(queuePayLoad);
        if (isEnqueued != null && isEnqueued) {
            return true;
        }
        
        return false;
    }
    
    public void markEnqueue(String queuePayLoad, String queueName, int timeOut, TimeUnit timeUnit) {
        try {
            this.set(getQueueKey(queueName, queuePayLoad), Boolean.TRUE, timeOut, timeUnit);
        } catch (Exception e) {
            logger.error("Got exception while marking as enqueued for queuePayLoad: [" + String.valueOf(queuePayLoad) + "]. Queue Name:[" + String.valueOf(queueName) + "]", e);
        }
    }
    
    
    public void markPromocodeFulfillmentSuccess(String orderId, String promoCode) {
        try {
            this.set(getPromoCodeFulfillmentKey(orderId, promoCode), Boolean.TRUE, PROMOCODE_IDEMPOTENCY_WINDOW_DAYS, TimeUnit.DAYS);
        } catch (Exception e) {
            logger.error("Got exception while marking promocode fulfillment success for orderId: [" + String.valueOf(orderId) + "]. promoCode: [" + String.valueOf(promoCode) + "]", e);
        }
    }

    public boolean isDuplicateRequest(String orderId, String promoCode) {
        try {
            Boolean isDuplicate = (Boolean) this.get(getPromoCodeFulfillmentKey(orderId, promoCode));
            
            if (isDuplicate != null && isDuplicate) {
                return true;
            }
            
        } catch (Exception e) {
            logger.error("Got exception while checking for fulfillment success for orderId: [" + String.valueOf(orderId) + "]. promoCode: [" + String.valueOf(promoCode) + "]", e);
        }

        return false;
    }

    private String getPromoCodeFulfillmentKey(String orderId, String promoCode) {
        return "promo_" + orderId + "_" + promoCode + "_promo";
    }
    
    private String getQueueKey(String queueName, String queuePayLoad) {
        return queueName + "_" + queuePayLoad;
    }
}
