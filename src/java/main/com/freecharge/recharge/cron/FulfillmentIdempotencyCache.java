package com.freecharge.recharge.cron;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * 
 * @author ravikumar
 * 
 * Created to implement idempotent behavior in fulfillment service for recharge and billpayment.
 * It will maintained a cache of all the orders for which fulfillment was success in last 15 minutes
 * And same can be queried to check whether it is a duplicate request or not.
 *
 */
@Service
public class FulfillmentIdempotencyCache  extends RedisCacheManager  {
    private Logger LOG = LoggingFactory.getLogger(getClass());

    private static final String SUFFIX_KEY = "_idempotencyList";
    private final int CACHE_EXPIRY_TIME_IN_MINUTES = 30;
  
    @Autowired
    private RedisTemplate<String, Object> fulFillmentIdempotencyCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.fulFillmentIdempotencyCacheTemplate;
    }
    
    public void markEnqueue(String orderId) {
        try {
            this.markEnqueue(orderId, CACHE_EXPIRY_TIME_IN_MINUTES, TimeUnit.MINUTES);
        } catch (Exception e) {
            LOG.error("Got exception while marking as enqueued for OrderId:[" + String.valueOf(orderId) + "]", e);
        }
    }
    
    private String getKey(String orderId) {
    	return orderId +SUFFIX_KEY;
	}

	public void markDequeue(String orderId) {
        try {
            this.delete(getKey(orderId));
        } catch (Exception e) {
            LOG.error("Got exception while marking as dequeued for Queue Name:[" + String.valueOf(orderId) + "]", e);
        }
    }

    public void markEnqueue(String orderId, int timeOut, TimeUnit timeUnit) {
        try {
            this.set(getKey(orderId), Boolean.TRUE, timeOut, timeUnit);
        } catch (Exception e) {
            LOG.error("Got exception while marking as enqueued for Queue Name/OrderId :[" + String.valueOf(orderId) + "]", e);
        }
    }

    public boolean isDuplicateRequest(String orderId) {
        try {
            Boolean isDuplicate = (Boolean) this.get(getKey(orderId));
            
            if (isDuplicate != null && isDuplicate) {
                return true;
            }
            
        } catch (Exception e) {
            LOG.error("Got exception while checking for fulfillment success for orderId: [" + String.valueOf(orderId) + "]", e);
        }

        return false;
    }

}
