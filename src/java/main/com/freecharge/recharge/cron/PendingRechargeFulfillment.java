package com.freecharge.recharge.cron;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.OrderService;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.recharge.services.RechargeSchedulerService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.RechargeSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.wallet.OneCheckWalletService;

@Service
public class PendingRechargeFulfillment {
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private InReadDAO dao;

    @Autowired
    private RechargeSchedulerService rechargeSchedulerService;
   
    @Autowired
    private UserWriteDao userWriteDao;
    
    @Autowired
    EmailService emailService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private RechargeSNSService rechargeSNSService;
    
    @Autowired
    InService inService;
    
    @Autowired 
    private HomeService homeService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private OperatorCircleService         operatorCircleService;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private UserServiceProxy              userServiceProxy;
   
    /* Reversal case handler API */
    public Boolean updateRechargeStatus(final String requestId, final String finalStatus,final String givenAggregatorName,final String aggrTransId) {
        Boolean reversalHandleSuccess = true;
        RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
        try {
            InRequest inRequestObj = dao.getInRequest(Long.parseLong(requestId.substring(requestId.indexOf("_")+1)));
            if (inRequestObj != null) {
                InResponse inResponse = dao.getInResponseByRequestId(inRequestObj.getRequestId());
                /* This will handle oxi-to-euronet retry transactions
                 * We will check whether given aggregatorName(by cs guy) matches  
                 * aggregator name of final inResponse object of transaction.
                 */
                if (!FCUtil.isEmpty(givenAggregatorName)) {
                	if (!(givenAggregatorName.equalsIgnoreCase(inResponse.getAggrName()))) {
                		return false;
                	}
                }
                
                if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)
                        && org.apache.commons.lang.StringUtils.isEmpty(aggrTransId)) {
                    logger.error("AGTransaction id is compulsory for success transactions");
                    return false;
                }
                
                if (inResponse != null && finalStatus != null && !finalStatus.isEmpty()) {
                    String initialResponseCode = inResponse.getInRespCode();
                    boolean isOldSuccess = RechargeUtil.isSuccessInResponseCode(initialResponseCode);
                    
                    // Old status - failure , New Status - Success
                    // We do not allow failure to success as failed orders are
                    // already refunded to customer.This amount should be
                    // recovered from AG during recon hence we do not update.
                    if (RechargeUtil.isFailedInResponseCode(inResponse.getInRespCode())
                            && finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                        logger.error("Cannot reverse status from fail to success for order id :"
                                + inRequestObj.getAffiliateTransId());
                        return false;
                    }
                    
                    //No status change. Old status - failure , New Status - failure
                    boolean underProcessForceReversal = finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_UP_FORCE_FAILED);
                    if (RechargeUtil.isFailedInResponseCode(inResponse.getInRespCode())
                            && (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_FAILED)||
                                    underProcessForceReversal)) {
                        return false;
                    }
                    //No status change. Old status - success , New Status - success
                    if (RechargeUtil.isSuccessInResponseCode(inResponse.getInRespCode())
                            && finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                        if (!org.apache.commons.lang.StringUtils.isEmpty(aggrTransId)
                                && (org.apache.commons.lang.StringUtils.isEmpty(inResponse.getAggrTransId()) || "unknown"
                                        .equalsIgnoreCase(inResponse.getAggrTransId()))) {
                            inResponse.setAggrTransId(aggrTransId);
                            inService.update(inResponse);
                            return true;
                        }
                        return false;
                    }
                    
                    if (!org.apache.commons.lang.StringUtils.isEmpty(aggrTransId) &&
                            org.apache.commons.lang.StringUtils.isEmpty(inResponse.getAggrTransId())) {
                        inResponse.setAggrTransId(aggrTransId);
                    }
                    
                    if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_FAILED) ||
                            underProcessForceReversal) {
                        if (isOldSuccess) {
                            inResponse.setInRespCode(RechargeConstants.SUCCESS_REVERSAL_IN_RESP_CODE);
                        } else if (underProcessForceReversal) {
                            inResponse.setInRespCode(RechargeConstants.UP_REVERSAL_IN_RESP_CODE);
                        } else {
                            inResponse.setInRespCode(RechargeConstants.REVERSAL_IN_RESP_CODE);
                        }
                        inResponse.setAggrRespMsg(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                        inResponse.setInRespMsg(
                                underProcessForceReversal
                                        ? RechargeConstants.RECHARGE_REVERSAL_FORCE_IN_RESP_MSG
                                        : RechargeConstants.RECHARGE_REVERSAL_IN_RESP_MSG);
                        rechargeSchedulerService.saveResponseAndInitiateFulfillment(inRequestObj, inResponse,false,false);
                        rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus());
                    }
                    if (finalStatus.equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                        inResponse.setInRespCode(RechargeConstants.SUCCESS_RESPONSE_CODE);
                        inResponse.setAggrRespMsg(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
                        inResponse.setInRespMsg(RechargeConstants.RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG);
                        rechargeSchedulerService.saveResponseAndInitiateFulfillment(inRequestObj, inResponse,false,false);
                        rechargeAlertBean.setMessageType(RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus());
                    }
                    rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(inResponse.getAggrName()));
                    rechargeAlertBean.setAgRequestId(fcProperties.getInTransIdPrefix() + inResponse.getRetryNumber()
                            + "_" + inResponse.getRequestId());
                    rechargeAlertBean.setOrderId(inRequestObj.getAffiliateTransId());
                    rechargeAlertBean.setOperatorTxnId(inResponse.getOprTransId());
                } else {
                    logger.error("InResponse is null for request id [" + requestId + "," + finalStatus + "]");
                    reversalHandleSuccess = false;
                }
            } else {
                logger.error("InRequest is null for request id [" + requestId + "," + finalStatus + "]");
                reversalHandleSuccess = false;                
            }
            if(inRequestObj!= null){
            	try{
            	populateRechargeInfo(rechargeAlertBean,inRequestObj.getAffiliateTransId(),reversalHandleSuccess);
            	}catch(Exception e){
            		logger.error("Caught exception in populating rechargeAlertBean",e);
            	}
            }
            
        } catch (Exception e) {
            logger.error("Something BAD happened while doing status change for reversal case for : " + "[" + requestId
                    + "  to  " + finalStatus + "].", e);
            reversalHandleSuccess = false;
        }
        if (reversalHandleSuccess) {
            try {            	
                rechargeSNSService.publish(rechargeAlertBean.getOrderId(), "{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean)
                        + "}");
            } catch (IOException e) {
                logger.error("Failed to push reversal notification for orderId : " + rechargeAlertBean.getOrderId());
            }
        }
        return reversalHandleSuccess;
    }
    /* End of reversal case handler */

	private void populateRechargeInfo(RechargeAlertBean rechargeAlertBean, String orderId, Boolean reversalHandleSuccess) {
		Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
		UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                .findUserTransactionHistoryByOrderId(orderId);
        if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
            userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
            userDetail.put("circleName", userTransactionHistory.getServiceRegion());
            userDetail.put("productType", userTransactionHistory.getProductType());
            try {
            	Integer operatorMasterId = null;
            	Integer circleMasterId = -1;
            	String operatorUIName = null;
            	String circleUIName = null;
            	
            	if (FCUtil.isUtilityPaymentType(userTransactionHistory.getProductType())) {
            		BillPayOperatorMaster operator = billPayServiceRemote.getBillPayOperatorFromName(userTransactionHistory.getServiceProvider());
            		operatorMasterId = operator.getOperatorMasterId();
            		operatorUIName = operator.getName();
            	} else {
            		OperatorMaster operator = operatorCircleService.getOperator(userTransactionHistory.getServiceProvider());
                	CircleMaster circle = operatorCircleService.getCircle(userTransactionHistory.getServiceRegion());
                	operatorMasterId = operator.getOperatorMasterId();
                	operatorUIName = operator.getName();
					if (circle != null) {
						circleMasterId = circle.getCircleMasterId();
						circleUIName = circle.getName();
					}
				}

				userDetail.put("operatorMasterId", operatorMasterId);
				userDetail.put("circleMasterId", circleMasterId);
				userDetail.put("operatorUIName", operatorUIName);
				userDetail.put("circleUIName", circleUIName);
				logger.info("userDetails set operator:" + operatorMasterId + ", circle: " + circleMasterId);
			} catch (Exception e) {
				logger.error(" Error while converting the operator/circle id  " + orderId, e);
			}
        }
        UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
		String emailId = userDetailsObj.getUserEmail();
		
		 Users user = userServiceProxy.getUserByEmailId(userDetailsObj.getUserEmail());
         Long userId = Long.parseLong(user.getUserId() + "");
		String oneCheckId = null;
		try {
			oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
		} catch (Exception e) {
			logger.error("Caught exception in fetching oneCheckId", e);
		}
        rechargeAlertBean.setOrderId(orderId);
        rechargeAlertBean.setOneCheckId(oneCheckId);
        rechargeAlertBean.setChannel(orderService.getChannelName(userDetailsObj.getChannelId()));
        rechargeAlertBean.setServiceNumber(userDetailsObj.getUserMobileNo());
        rechargeAlertBean.setServiceProvider(userDetailsObj.getOperatorName());
        rechargeAlertBean.setServiceRegion(userDetailsObj.getCircleName());
        rechargeAlertBean.setAmount(Double.valueOf(userDetailsObj.getAmount()));
        
        String rechargedProductType = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType())
                .getLabel();

        rechargeAlertBean.setProduct(rechargedProductType);
        rechargeAlertBean.setUserId(userId); 
        rechargeAlertBean.setEmailId(emailId);
        rechargeAlertBean.setRechargeReversal(reversalHandleSuccess);
        
		
	}    
}
