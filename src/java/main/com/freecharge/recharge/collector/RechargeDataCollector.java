package com.freecharge.recharge.collector;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.platform.metrics.DataPoint;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.platform.metrics.collector.GraphiteMetricsCollector;
import com.freecharge.recharge.services.OperatorAlertService;

/**
 * This class encapsulates the data recharge statistics collection logic.
 * It pulls the appropriate data from {@link GraphiteMetricsCollector} and
 * updates it in redis which can then be consumed by interested parties. 
 * @author arun
 *
 */
@Component
public class RechargeDataCollector {
    private static Logger logger = LoggingFactory.getLogger(RechargeDataCollector.class);
    
    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private EmailService emailService;
    
    private final int DEFAULT_AGGREGATION_DELAY_MINUTES = 5;
    private final int DEFAULT_AGGREGATION_PERIOD_MINUTES = 30;
    private final static int DEFAULT_ALERT_PERIOD_MINUTES = 30;
    private final static Double DEFAULT_MIN_SUCCESS_RATE = new Double("30");
    private final static Double DEFAULT_MAX_OPERATOR_DOWN_RATE = new Double("10");
    
    private final String AGGREGATION_DELAY_KEY = "aggr_delay_mins";
    private final String AGGREGATION_PERIOD__KEY = "aggr_period_mins";
    private final String ALERT_PERIOD_KEY = "alert_period_mins";
    private final String MIN_SUCCESS_RATE_KEY = "min_success_rate";
    private final String MAX_OPERATOR_DOWN_RATE_KEY = "max_op_down_rate";

    private final String DATA_COLLECTOR_CONFIG = "dcProps";

    private GraphiteMetricsCollector graphiteClient = new GraphiteMetricsCollector();
    
    private static OperatorMetricDataBean operatorMetricData = new OperatorMetricDataBean();
        
    private RechargeAggregateMetric retrieve(RechargeMetrics rechargeMetric) {
        
        DataPoint successCount = retrieveAndAggregate(rechargeMetric.getSuccessCountMetricString(), rechargeMetric.getCollectionPeriod());
        DataPoint failureCount = retrieveAndAggregate(rechargeMetric.getFailureCountMetricString(), rechargeMetric.getCollectionPeriod());
        DataPoint totalCount = new DataPoint(successCount.getTime(), successCount.getValue() + failureCount.getValue());
        
        RechargeAggregateMetric aggregateMetric = new RechargeAggregateMetric();
        aggregateMetric.setAggregator(rechargeMetric.getAggregator());
        aggregateMetric.setSuccessCount(successCount);
        aggregateMetric.setFailureCount(failureCount);
        aggregateMetric.setTotalCount(totalCount);
        aggregateMetric.setOperator(rechargeMetric.getOperatorName());

        DataPoint operatorDownCount = retrieveAndAggregateDownMetrics(rechargeMetric.getOperatorDownMetricStrings(), rechargeMetric.getCollectionPeriod());
        aggregateMetric.setOperatorDownCount(operatorDownCount);
        
        return aggregateMetric;
    }
    
    private DataPoint retrieveAndAggregate(String successCountMetricString, String collectionPeriod) {
        List<DataPoint> dataPoints = graphiteClient.retrieve(successCountMetricString, collectionPeriod);
        
        Integer aggregationDelay = getDataCollectorProperty(AGGREGATION_DELAY_KEY, DEFAULT_AGGREGATION_DELAY_MINUTES);
        Integer aggregationPeriod = getDataCollectorProperty(AGGREGATION_PERIOD__KEY, DEFAULT_AGGREGATION_PERIOD_MINUTES);
        
        DataPoint aggregateDataPoint = aggregate(dataPoints, aggregationDelay, aggregationPeriod);
        
        return aggregateDataPoint;
    }

    private DataPoint retrieveAndAggregateDownMetrics(List<String> downMetricStrings, String collectionPeriod) {
        List<DataPoint> dataPoints = new ArrayList<DataPoint>();
        for(String downMetricString : downMetricStrings) {
            dataPoints.addAll(graphiteClient.retrieve(downMetricString, collectionPeriod));
        }
        Integer aggregationDelay = getDataCollectorProperty(AGGREGATION_DELAY_KEY, DEFAULT_AGGREGATION_DELAY_MINUTES);
        Integer aggregationPeriod = getDataCollectorProperty(AGGREGATION_PERIOD__KEY, DEFAULT_AGGREGATION_PERIOD_MINUTES);
        
        DataPoint aggregateDataPoint = aggregate(dataPoints, aggregationDelay, aggregationPeriod);
        
        return aggregateDataPoint;
    }
    /**
     * Given a list of data points this function returns a single data point aggregated over aggregationPeriodHours
     * with a lag of aggregationDelayMinutes.
     * @param dataPoints
     * @param aggregationDelayMinutes
     * @param aggregationPeriodMinutes
     * @return
     */
    private DataPoint aggregate(List<DataPoint> dataPoints, int aggregationDelayMinutes, int aggregationPeriodMinutes) {
        DateTime currentHour = new DateTime();
        DateTime aggregationEndHour = currentHour.minusMinutes(aggregationDelayMinutes);
        DateTime aggregationStartHour = aggregationEndHour.minusMinutes(aggregationPeriodMinutes);
        
        Interval aggregateInterval = new Interval(aggregationStartHour, aggregationEndHour);
        
        Double aggregateValue = new Double(0);
        
        for (DataPoint dataPoint : dataPoints) {
            if (aggregateInterval.contains(dataPoint.getTime().getTime())) {
                aggregateValue += dataPoint.getValue();
            }
        }
        
        DataPoint aggregateDataPoint = new DataPoint(aggregationEndHour.toDate(), aggregateValue);
        return aggregateDataPoint;
    }
    
    private static void recordEvent(MetricsClient metricsClient, String serviceName, String metricName, String eventName){
    	metricsClient.recordEvent(serviceName, metricName, eventName);
    }
    
    public void sendInfrastructureTeamMail(String metricKey, Double operatorDownRate) {
        String to = fcProperties.getProperty("team.infrastructure.emailid");
        emailService.sendTeamMail(to, "Hello, " + metricKey + " reached to down rate of "+ operatorDownRate);
    }
    
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        RechargeMetricsCache rmc = ctx.getBean(RechargeMetricsCache.class);
        MetricsClient metricsClient = ctx.getBean(MetricsClient.class);
        RechargeDataCollector rdc = ctx.getBean(RechargeDataCollector.class);
        RechargeMetricService rms = ctx.getBean(RechargeMetricService.class);
        OperatorAlertService alertService = ctx.getBean(OperatorAlertService.class);
        AppConfigService appConfig = ctx.getBean(AppConfigService.class);

        for (RechargeMetrics operatorMetric : operatorMetricData.getOperatorMetricList()) {
            RechargeAggregateMetric ram = rdc.retrieve(operatorMetric);
            
            String metricKey = ram.getAggregator() + "." + ram.getOperator();
            
            rmc.set(metricKey, ram, 30, TimeUnit.MINUTES);
            
            RechargeAggregateMetric rm = (RechargeAggregateMetric) rmc.get(metricKey);
            String successMessage = String.format("[%s:%s] Success: %s : %s", rm.getAggregator(), rm.getOperator(), rm.getSuccessCount().getTime(), rm.getSuccessCount().getValue());
            String failureMessage = String.format("[%s:%s] Failure: %s : %s", rm.getAggregator(), rm.getOperator(), rm.getFailureCount().getTime(), rm.getFailureCount().getValue());
            String downMessage = String.format("[%s:%s] Down: %s : %s", rm.getAggregator(), rm.getOperator(), rm.getOperatorDownCount().getTime(), rm.getOperatorDownCount().getValue());
            
            logger.info(successMessage);
            logger.info(failureMessage);
            logger.info(downMessage);
            
            Double successCount = rm.getSuccessCount().getValue();
            Double totalCount   = rm.getTotalCount().getValue();
            Double operatorDownCount = rm.getOperatorDownCount().getValue();
            
            if(totalCount > 0){
                logger.info("Total count for " + metricKey + " is " + totalCount);
	            Double successRate  = (successCount/totalCount)*100;
	            if(successRate < rdc.getMinSuccessRate()){
	                logger.info("Success rate for " + metricKey + " is " + successRate);
	            	recordEvent(metricsClient ,"Recharge.failedOperators",rm.getAggregator()+ "."+rm.getOperator(), successRate.toString());
	            }
	            
	            if (operatorDownCount > 0) {
	                final double failureCount = totalCount - successCount;
	                logger.info("Failure count for " + metricKey + " is " + failureCount);
	                logger.info("Down count for " + metricKey + " is " + operatorDownCount);
                    if (failureCount > 0) {
                        Double operatorDownRate = (operatorDownCount / failureCount) * 100;
                        
                        if (operatorDownRate > rdc.getMaxOperatorDownRate()) {
                            logger.info("Down rate for " + metricKey + " is " + operatorDownRate);
                            rdc.sendInfrastructureTeamMail(metricKey, operatorDownRate) ;
                            metricsClient.recordEvent("Recharge.OperatorDown", rm.getOperator(), "Down");
                            
                            if (appConfig.isRechargeAutoAlertEnabled()) {
                                String operatorId = OperatorMetricDataBean.getOperatorId(ram.getOperator());
                                
                                if (!alertService.isOperatorInAlert(Integer.parseInt(operatorId))) {
                                    createAlert(alertService, operatorId, ram.getOperator(), rdc);
                                }
                            }
                        }
                        
	                }
	            }
            }
        }
        
        rms.discardStaleDenominationMetric();
        
        ctx.close();
    }
    
    private static void createAlert(OperatorAlertService alertService, String operatorId, String operatorName, RechargeDataCollector rdc) {
        SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");

        String messageFormat = "We are facing a high failure rate in the last few minutes, please try after some time";

        final DateTime alertStartDate = DateTime.now();
        final DateTime alertEndDate = alertStartDate.plusMinutes(rdc.getAlertPeriodMinutes());
       
        alertService.saveOperatorAlertId(Integer.parseInt(operatorId), SDF.format(alertStartDate.toDate()), SDF.format(alertEndDate.toDate()), messageFormat, true);
    }
    
    private Integer getDataCollectorProperty(String configKey, int defaultValue) {
        JSONObject configJson = appConfig.getCustomProp(DATA_COLLECTOR_CONFIG);
        
        if (configJson != null) {
            final String valueString = (String) configJson.get(configKey);
            
            if (StringUtils.isNotBlank(valueString)) {
                return Integer.parseInt(valueString);
            }
        }
        
        return defaultValue;
    }
    
    private double getMinSuccessRate() {
        return this.getDataCollectorProperty(MIN_SUCCESS_RATE_KEY, DEFAULT_MIN_SUCCESS_RATE.intValue()).doubleValue();
    }
    
    private double getMaxOperatorDownRate() {
        return this.getDataCollectorProperty(MAX_OPERATOR_DOWN_RATE_KEY, DEFAULT_MAX_OPERATOR_DOWN_RATE.intValue()).doubleValue();
    }
    
    private int getAlertPeriodMinutes() {
        return this.getDataCollectorProperty(ALERT_PERIOD_KEY, DEFAULT_ALERT_PERIOD_MINUTES);
    }
}