package com.freecharge.recharge.collector;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A cache to hold recharge aggregator/operator level metrics.
 * @author arun
 *
 */
@Component
class RechargeMetricsCache extends RedisCacheManager {
    private final String BLOCK_INVALID_NUMBER_PREFIX = "invnum";

	private Logger logger = LoggingFactory.getLogger(getClass());

    private final String INVALID_DENOMINATION_PREFIX = "inv";
    
    private final String PERMANENT_FAILURE_PREFIX = "per";
    
    private final String EG_FAILURE_PREFIX = "egf";
    
    private final String EI_FAILURE_PREFIX = "eif";
    
    private final String _3596_FAILURE_PREFIX = "3596";
    
    @Autowired
    private RedisTemplate<String, Object> rechargeMetricsCacheTemplate;

    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.rechargeMetricsCacheTemplate;
    }
    
    @Override                                                                                                                                                                               
    public RechargeAggregateMetric get(String key) {
        return (RechargeAggregateMetric) super.get(key);
    }

    public void notePermanentFailure(String productCode, String operator, String serviceNumber) {
        final String cacheKey = PERMANENT_FAILURE_PREFIX + "_" + productCode + "_" + operator + "_" + serviceNumber;

        setWithOneDayExpiry(cacheKey);
    }

    public void notePostpaidPermanentFailure(String productCode, String operator, String serviceNumber) {
        final String cacheKey = PERMANENT_FAILURE_PREFIX + "_" + productCode + "_" + operator + "_" + serviceNumber;

        setWithTwelveHoursExpiry(cacheKey);
    }

    
    public void noteInvalidDenominationNumber(String serviceNumber, String operator, Double amount, String rechargeType) {
        final String cacheKey = INVALID_DENOMINATION_PREFIX + "_" + serviceNumber + "_" + operator + "_" + amount.toString() + "_" + rechargeType;
        
        setWithOneDayExpiry(cacheKey);
    }
    
    public void noteEGFailureNumber(String serviceNumber, String operator, Double amount) {
        final String cacheKey = EG_FAILURE_PREFIX + "_" + serviceNumber + "_" + operator + "_" + amount.toString();
        
        setWithOneDayExpiry(cacheKey);
    }
    
    public void noteEIFailureNumber(String serviceNumber, String operator, Double amount) {
        final String cacheKey = EI_FAILURE_PREFIX + "_" + serviceNumber + "_" + operator + "_" + amount.toString();
        setWithOneDayExpiry(cacheKey);
    }
    
    public Boolean isInvalidDenominationNumber(String serviceNumber, String operator, Double amount, String rechargeType) {
        final String cacheKey = INVALID_DENOMINATION_PREFIX + "_" + serviceNumber + "_" + operator + "_" + amount.toString() + "_" + rechargeType;
        
        return fetchBoolean(cacheKey);
    }
    
    public boolean isPermanentFailure(String productCode, String operator, String serviceNumber) {
        final String cacheKey = PERMANENT_FAILURE_PREFIX + "_" + productCode + "_" + operator + "_" + serviceNumber;

        return fetchBoolean(cacheKey);
    }
    
    public boolean isPostpaidPermanentFailure(String productCode, String operator, String serviceNumber) {
        final String cacheKey = PERMANENT_FAILURE_PREFIX + "_" + productCode + "_" + operator + "_" + serviceNumber;

        return fetchBoolean(cacheKey);
    }
    
    public boolean isEGFailureNumber(String serviceNumber, String operator, Double amount) {
        final String cacheKey = EG_FAILURE_PREFIX + "_" + serviceNumber + "_" + operator + "_" + amount.toString();
        
        return fetchBoolean(cacheKey);
    }
    
    public Object getAggregatorPreferenceValue(String key) {
           return super.get(key);
    }
    
    public void setAggregatorPreferenceValue(String key, Object value) {
           super.set(key, value);
    }
    
    public void deleteAggregatorPreferencekey(String key) {
           super.delete(key);
    }

    private boolean fetchBoolean(final String cacheKey) {
        try {
            Boolean value = (Boolean) super.get(cacheKey);
            
            if (value != null && value) {
                return true;
            }
            
        } catch (Exception e) {
            logger.error("Exception while fetching key: " + cacheKey, e);
        }
        
        return false;
    }
    
    private void setWithOneDayExpiry(final String cacheKey) {
        try {
            this.set(cacheKey, Boolean.TRUE, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            logger.error("Exception while setting key: " + cacheKey, e);
        }
    }
    
    private void setWithTwelveHoursExpiry(final String cacheKey) {
        try {
            this.set(cacheKey, Boolean.TRUE, 12, TimeUnit.HOURS);
        } catch (Exception e) {
            logger.error("Exception while setting postpaid key: " + cacheKey, e);
        }
    }

	public void blockInvalidNumber(String serviceNumber) {
		final String cacheKey = BLOCK_INVALID_NUMBER_PREFIX + "_" + serviceNumber;
		setWithOneDayExpiry(cacheKey);
		logger.info(cacheKey+" is set");
	}

	public boolean isInvalidNumber(String serviceNumber) {
		final String cacheKey = BLOCK_INVALID_NUMBER_PREFIX + "_" + serviceNumber;
		return fetchBoolean(cacheKey);
	}
	
}
