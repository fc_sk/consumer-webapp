package com.freecharge.recharge.collector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.ImmutableMap;

/**
 * This class just contains metrics data objects
 * for a few prominent operators. Later on, this will
 * be moved to Mongo or some data store.
 * @author arun
 *
 */
public class OperatorMetricDataBean {
    private List<RechargeMetrics> operatorMetricList = new ArrayList<>();
    
    private static final ImmutableMap<String, String> operatorIdMapping = new ImmutableMap.Builder<String, String>()
            .put("1","Aircel")
            .put("2","Airtel")
            .put("3","BSNL")
            .put("4","Dish-Tv")
            .put("5","Tata-Docomo-GSM")
            .put("6","Idea")
            .put("7","Indicom-Walky")
            .put("8","Loop")
            .put("9","MTNL-Delhi")
            .put("10","Reliance-CDMA")
            .put("11","Reliance-GSM")
            .put("12","Tata-Indicom")
            .put("13","Uninor")
            .put("14","Vodafone")
            .put("16","Big-Tv")
            .put("17","Airtel-Tv")
            .put("18","Sun-Tv")
            .put("19","Tata-Sky")
            .put("20","Videocon-Tv")
            .put("21","Tata-Photon-Plus")
            .put("22","MTS")
            .put("23","Reliance-Netconnect")
            .put("24","Videocon-Mobile")
            .put("30","Virgin-GSM")
            .put("31","Virgin-CDMA")
            .put("40","MTS-MBrowse")
            .put("41","MTS-Mblaze")
            .put("42","Tata-Docomo-CDMA")
            .put("43","T24")
            .put("44","Airtel-DataCard")
            .put("45","BSNL-DataCard")
            .put("46","MTNL-DataCard-Delhi")
            .put("47","Vodafone-DataCard")
            .put("48","Aircel-DataCard")
            .put("49","Idea-DataCard")
            .put("50","Reliance-GSM-DataCard")
            .put("51","Tata-Photon-Whiz-DataCard")
            .put("52","Tata-Docomo-GSM-DataCard")
            .put("53","T24-DataCard")
            .put("54","MTNL-Mumbai")
            .put("55","MTNL-Datacard-Mumbai")
            .put("56","Airtel-Postpaid")
            .put("57","Loop-Postpaid")
            .put("58","Bsnl-Postpaid")
            .put("59","Docomo-Postpaid")
            .put("60","Idea-Postpaid")
            .put("61","Indicom-Postpaid")
            .put("62","Vodafone-Postpaid")
            .put("63","Reliance-Postpaid")
            .build(); 
    
    private List<String> operatorList = Arrays.asList(
            "Indicom-Walky", "Tata-Photon-Whiz-DataCard",
            "Virgin-CDMA", "Aircel-DataCard", "MTNL-DataCard-Delhi",
            "MTS-MBrowse", "Tata-Docomo-GSM-DataCard", "Reliance-GSM-DataCard",
            "MTS-Mblaze", "Vodafone-DataCard", "Airtel-DataCard", "T24",
            "Virgin-GSM", "Idea-DataCard", "BSNL-DataCard",
            "Tata-Photon-Plus", "MTNL-Mumbai", "Tata-Docomo-CDMA",
            "Sun-Tv", "Reliance-Netconnect", "Big-Tv", "MTS",
            "MTNL-Delhi", "Videocon-Mobile", "Loop", "Dish-Tv",
            "Videocon-Tv", "Tata-Indicom", "Airtel-Tv", "Tata-Sky",
            "Reliance-CDMA", "Uninor", "Aircel", "BSNL",
            "Reliance-GSM", "Tata-Docomo-GSM", "Idea", "Vodafone",
            "Airtel"
            );
    
    public static String getOperatorCacheKey(String operatorId) {
        return operatorIdMapping.get(operatorId);
    }
    
    public static String getOperatorId(String operatorName) {
        for (Entry<String, String> operatorIdMap : operatorIdMapping.entrySet()) {
            if (StringUtils.equals(operatorName, operatorIdMap.getValue())) {
                return operatorIdMap.getKey();
            }
        }
        
        throw new IllegalStateException("Unknown operator:[" + String.valueOf(operatorName) + "]");
    }
    
    public List<RechargeMetrics> getOperatorMetricList() {
        String from = "-720min";

        String metricTemplate = "summarize(" +
        		"sumSeries(" +
        		    "stats.counts.application.*.Recharge.*.OPERATOR.STATUS_increment" +
        		 "), \"5min\")";
        
        
        String operatorDownTemplateEuronet = "summarize(" +
                "sumSeries(" +
                    "stats.counts.application.*.Recharge.euronet.ResponseCode.OPERATOR.ES_increment" +
                 "), \"5min\")";
        
        String airtelDownTemplateEuronet = "summarize(" +
                "sumSeries(" +
                    "stats.counts.application.*.Recharge.*.ResponseCode.Airtel.ES_increment, " +
                    "stats.counts.application.*.Recharge.euronet.ResponseCode.Airtel.EI_increment" +
                 "), \"5min\")";
        
        String operatorDownTemplateOxigen = "summarize(" +
                "sumSeries(" +
                    "stats.counts.application.*.Recharge.oxigen.ResponseCode.OPERATOR.17478_increment" +
                 "), \"5min\")";
        
        
        //Recharge.oxigen.ResponseCode.Aircel.17478_increment=1
        // TODO: Needs to be made thread safe.
        if (operatorMetricList.isEmpty()) {
            for (String operator : operatorList) {
                RechargeMetrics rm = new RechargeMetrics();
                rm.setAggregator("ALL");
                rm.setOperatorName(operator);
                rm.setCollectionPeriod(from);
                
                /*
                 * Status metric strings
                 */
                String successMetricString = metricTemplate.
                        replaceAll("OPERATOR", operator).
                        replaceAll("STATUS", "Success");
                
                String failureMetricString = metricTemplate.
                        replaceAll("OPERATOR", operator).
                        replaceAll("STATUS", "Failure");
                
                rm.setSuccessCountMetricString(successMetricString);
                rm.setFailureCountMetricString(failureMetricString);
                
                /*
                 * Make down metric strings
                 */
                List<String> operatorDownMetricStrings = new ArrayList<String>();
                operatorDownMetricStrings.add(operatorDownTemplateEuronet.replaceAll("OPERATOR", operator));
                operatorDownMetricStrings.add(operatorDownTemplateOxigen.replaceAll("OPERATOR", operator));
                if("Airtel".equals(operator)) {
                    operatorDownMetricStrings.add(airtelDownTemplateEuronet);
                }
                
                rm.setOperatorDownMetricStrings(operatorDownMetricStrings);
                operatorMetricList.add(rm);
          }
        }
        return operatorMetricList;
    }
}