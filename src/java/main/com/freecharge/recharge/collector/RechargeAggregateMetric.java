package com.freecharge.recharge.collector;

import java.io.Serializable;

import com.freecharge.platform.metrics.DataPoint;

/**
 * This class encapsulates various aggregate recharge metrics.
 * @author arun
 *
 */
public class RechargeAggregateMetric implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String aggregator;
    private String operator;
    private DataPoint successCount;
    private DataPoint failureCount;
    private DataPoint operatorDownCount;
    
    private DataPoint totalCount;

    public String getAggregator() {
        return aggregator;
    }
    public void setAggregator(String aggregator) {
        this.aggregator = aggregator;
    }
    
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }
    
    public DataPoint getFailureCount() {
        return failureCount;
    }
    public void setFailureCount(DataPoint failureCount) {
        this.failureCount = failureCount;
    }
    
    public DataPoint getSuccessCount() {
        return successCount;
    }
    public void setSuccessCount(DataPoint successCount) {
        this.successCount = successCount;
    }
    
    public DataPoint getTotalCount() {
        return totalCount;
    }
    public void setTotalCount(DataPoint totalCount) {
        this.totalCount = totalCount;
    }
    
    public DataPoint getOperatorDownCount() {
        return operatorDownCount;
    }
    public void setOperatorDownCount(DataPoint operatorDownCount) {
        this.operatorDownCount = operatorDownCount;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
