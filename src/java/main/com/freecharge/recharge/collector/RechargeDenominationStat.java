package com.freecharge.recharge.collector;

public class RechargeDenominationStat {

	private Long operatorId;
	private int circleId;
	private float rechargeAmount;
	private double successRate;
	
	public Long getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}
	public float getRechargeAmount() {
		return rechargeAmount;
	}
	public void setRechargeAmount(float rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}
	public double getSuccessRate() {
		return successRate;
	}
	public void setSuccessRate(double successRate) {
		this.successRate = successRate;
	}
	public int getCircleId() {
		return circleId;
	}
	public void setCircleId(int circleId) {
		this.circleId = circleId;
	}
	
}
