package com.freecharge.recharge.collector;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemResult;
import com.amazonaws.services.dynamodbv2.model.DeleteRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ReturnConsumedCapacity;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.entity.RechargePlan;
import com.google.common.collect.ImmutableMap;

/**
 * This cache holds, with timeout, various
 * denominations that have failed with
 * "invalid denomination" error code. To be
 * on safer side, this has time out of 2 days.
 * @author arun
 *
 */
@Component
public class InvalidDenominationCache extends RedisCacheManager {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RedisTemplate<String, Object> invalidDenominationCacheTemplate;
    @Autowired
    AmazonDynamoDBAsync dynamoClient;
    @Autowired
    MetricsClient metricsClient;
    @Autowired
    AppConfigService appConfigService;
    
    @Autowired
    FCProperties fcProperties;
    
    @Autowired
    RechargeMetricService rechargeMetricService;
    
    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    private final int DATA_COLLECTION_WINDOW_DAYS = 29;
    
    private final String TABLE = "amountCache";
    private final String COUNT_ATTRIBUTE = "count";
    private final String HASH_KEY = "key";
    
    private final int THRESHOLD_BATCH_WRITE = 20;

    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        this.invalidDenominationCacheTemplate.setValueSerializer(new GenericToStringSerializer<>(Long.class));
        return this.invalidDenominationCacheTemplate;
    }

    public void noteInvalidDenomination(String operatorId, String circleId, Double amount, String rechargeType) {
        final String keySuffix = "_invalid";
        recordAndSetExpiry(operatorId, circleId, amount, rechargeType, keySuffix);
    }
    
    public void noteAirtelInvalidDenomination(String circleId, Double amount) {
        final DateTime today = new DateTime();
        final String dateKey = DATE_FORMAT.format(today.toDate());
        final String keyToIncrement = "artinvalid" + "_" + circleId + "_" + dateKey + "_" + amount.toString() + "_artinvalid";
        recordKeyAndSetExpiry(keyToIncrement);
    }
    
    public void notebBsnlInvalidDenomination(String circleId, Double amount, String rechargePlan) {
        final DateTime today = new DateTime();
        final String dateKey = DATE_FORMAT.format(today.toDate());
        final String keyToIncrement = "bslinvalid" + "_" + circleId + "_" + dateKey + "_" + amount.toString() + "_" + rechargePlan + "_bslinvalid";
        recordKeyAndSetExpiry(keyToIncrement);
    }
    
    public Double getBsnlInvalidDenomination(String circle, String dateKey, Double amount, String rechargePlanType) {
             String key = "bslinvalid_" + circle + "_" + dateKey + "_" + amount.toString() + "_" + rechargePlanType + "_bslinvalid";
        return getWithDefault(0d, key);
    }
    
    public Double getAirtelInvalidDenomination(String circle, String dateKey, Double amount) {
        String key = "artinvalid_" + circle + "_" + dateKey + "_" + amount.toString() + "_artinvalid";
        
        return getWithDefault(0d, key);
    }

    public void noteValidDenomination(String operatorId, String circleId, Double amount, String rechargeType) {
        final String keySuffix = "_valid";
        recordAndSetExpiry(operatorId, circleId, amount, rechargeType, keySuffix);
    }
    
    public Boolean isDenominationInvalid(final String operatorId, final String circleId, final Double amount) {
        try {
            Boolean isDenominationInvalid = (Boolean) this.get(operatorId + "_" + circleId + "_" + amount.toString());

            if (isDenominationInvalid != null && isDenominationInvalid) {
                return true;
            }

            return false;
        } catch (Exception e) {
            logger.error("Exception while fetching denomination", e);
        }

        return false;
    }

    public Double getSuccessCount(String operatorId, String circleId, Double amount, String dateKey, String rechargeType) {
        final Double defaultCount = new Double(100000d);
        final String keyToFetch = operatorId + "_" + circleId + "_" + dateKey + "_" + amount.toString() + "_" + rechargeType + "_valid";

        return getWithDefault(defaultCount, keyToFetch);
    }

    public Double getFailureCount(String operatorId, String circleId, Double amount, String dateKey, String rechargeType) {
        final Double defaultCount = new Double(0);
        final String keyToFetch = operatorId + "_" + circleId + "_" + dateKey + "_" + amount.toString() + "_" + rechargeType + "_invalid";

        return getWithDefault(defaultCount, keyToFetch);
    }

    /**
     * Deletes all invalid denomination metrics
     * for a given date.
     * @param date
     */
    public void deleteMetrics(final Date date) {
        String dateKey = DATE_FORMAT.format(date);
        String keyPatternToDelete = "*" + dateKey + "*";

        Collection<String> keysToDelete = this.getKeys(keyPatternToDelete);

        this.deleteKeys(keysToDelete);
    }
    
    public void deleteMetricsFromDynamo(final Date date) {
        String dateKey = DATE_FORMAT.format(date);
        String keyPatternToDelete = dateKey;
        //Fetch all the attributes matching 'keyPatternToDelete'
        List<Map<String, AttributeValue>> keysToDelete = this.fetchPatternFromDymano(keyPatternToDelete);
        if (keysToDelete.isEmpty()) {
            return;
        }

        Map<String, List<WriteRequest>> requestItems = new HashMap<>();
        List<WriteRequest> threadList = new ArrayList<>();
        for (Map<String, AttributeValue> key : keysToDelete) {
            Map<String, AttributeValue> threadDeleteKey = new HashMap<>();
            threadDeleteKey.put(HASH_KEY, key.get(HASH_KEY));
            threadList.add(new WriteRequest().withDeleteRequest(new DeleteRequest().withKey(threadDeleteKey)));
        }
        
        BatchWriteItemResult result;
        BatchWriteItemRequest batchWriteItemRequest = new BatchWriteItemRequest()
                .withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL);
        
        for (int start = 0; start < threadList.size(); start += THRESHOLD_BATCH_WRITE) {
            do {
                int end = Math.min(start + THRESHOLD_BATCH_WRITE, threadList.size());
                List<WriteRequest> subThreadList = threadList.subList(start, end);
                requestItems.put(TABLE, subThreadList);
                batchWriteItemRequest.withRequestItems(requestItems);
                result = batchDeleteItemFromDynamo(batchWriteItemRequest);
                if (result == null) {
                    return;
                }
                // Check for unprocessed keys which could happen if you exceed
                // provisioned throughput
                requestItems = result.getUnprocessedItems();
            } while (result.getUnprocessedItems().size() > 0);
        }
    }
    
    private List<Map<String, AttributeValue>> fetchPatternFromDymano(String searchPattern) {
        List<Map<String, AttributeValue>> attributesToDelete = new ArrayList<>();
        ScanResult result = null;

        do {
            ScanRequest scanRequest = new ScanRequest();
            scanRequest.setTableName(TABLE);
            if (result != null) {
                scanRequest.setExclusiveStartKey(result.getLastEvaluatedKey());
            }
            final long startTime = System.currentTimeMillis();
            try {
                result = dynamoClient.scan(scanRequest);
            } catch (AmazonClientException e) {
                logger.error("AmazonClientException while scanning table " + TABLE, e);
                metricsClient.recordEvent("DynamoDB", "ScanTable", "AmazonClientException");
            } catch (Exception e) {
                logger.error("Exception while scanning table " + TABLE, e);
                metricsClient.recordEvent("DynamoDB", "ScanTable", "Exception");
            } finally {
                metricsClient.recordLatency("DynamoDB", "ScanTable", System.currentTimeMillis() - startTime);
            }
            List<Map<String, AttributeValue>> rows = result.getItems();
            for (Map<String, AttributeValue> map : rows) {
                try {
                    AttributeValue v = map.get(HASH_KEY);
                    String key = v.getS();
                    if (key.contains(searchPattern)) {
                        attributesToDelete.add(map);
                    }
                } catch (NumberFormatException e) {
                    logger.error("NumberFormatException while parsing table " + TABLE, e);
                }
            }
        } while (result.getLastEvaluatedKey() != null);
        return attributesToDelete;
    }
    
    private BatchWriteItemResult batchDeleteItemFromDynamo(BatchWriteItemRequest batchDeleteRequest) {
        BatchWriteItemResult result = null;
        final long startTime = System.currentTimeMillis();
        try {
            result = dynamoClient.batchWriteItem(batchDeleteRequest);
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while deleteting InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "DeleteItem", "AmazonServiceException");
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while deleting InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "DeleteItem", "AmazonClientException");
        } catch (Exception e) {
            logger.error("Exception while deleting InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "DeleteItem", "Exception");
        } finally {
            metricsClient.recordLatency("DynamoDB", "DeleteItem", System.currentTimeMillis() - startTime);
        }
        return result;
    }

    public Collection<String> getValidDenominationKeys(final int operatorId, final int circleId) {
        String validKeyPattern = operatorId + "_" + circleId + "_*_*_valid";
        return this.getKeys(validKeyPattern);
    }

    private Double getWithDefault(final Double defaultCount, final String keyToFetch) {
        try {
            Long successCount = null;
            // Read from Dynamo
            if (appConfigService.isReadDenominationValidityFromDynamo()) {
                successCount =  getDataForKeyFromDynamo(keyToFetch);
            } else {
                successCount = (Long) this.get(keyToFetch);
            }
            if (successCount == null) {
                return new Double(0);
            }

            return successCount.doubleValue();
        } catch (Exception e) {
            logger.error("Exception while fetching denomination", e);
            return defaultCount;
        }
    }

    private void recordAndSetExpiry(String operatorId, String circleId, Double amount, String rechargeType, final String keySuffix) {
        final DateTime today = new DateTime();
        final String dateKey = DATE_FORMAT.format(today.toDate());
        final String keyToIncrement = operatorId + "_" + circleId + "_" + dateKey + "_" + amount.toString() + "_" + rechargeType + keySuffix;

        recordKeyAndSetExpiry(keyToIncrement);
    }

    private void recordKeyAndSetExpiry(final String keyToIncrement) {
        try {
            // Record in dynamo as well as Redis
            recordKeyInDynamo(keyToIncrement);
            this.increment(keyToIncrement);
            this.expireAt(keyToIncrement, new DateTime().plusDays(DATA_COLLECTION_WINDOW_DAYS).toDate());
        } catch (Exception e) {
            logger.error("Exception while setting [" + keyToIncrement + "]", e);
        }
    }
    
    private void recordKeyInDynamo(final String keyToIncrement) {
        AttributeValue attributeValue = new AttributeValue().withN("1");
        AttributeValueUpdate attributeUpdates = new AttributeValueUpdate().withValue(attributeValue).withAction(AttributeAction.ADD);
        Map<String, AttributeValueUpdate> update = new HashMap<>();
        update.put(COUNT_ATTRIBUTE, attributeUpdates);
        UpdateItemRequest updateItemRequest = new UpdateItemRequest().withTableName(TABLE).withKey(
                ImmutableMap.of(HASH_KEY, new AttributeValue().withS(keyToIncrement)));
        updateItemRequest.setAttributeUpdates(update);
        final long startTime = System.currentTimeMillis();
        try {
            dynamoClient.updateItem(updateItemRequest);
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while recording InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonServiceException");
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while recording InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonClientException");
        } catch (Exception e) {
            logger.error("Exception while recording InvalidDenomination", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "Exception");
        } finally {
            metricsClient.recordLatency("DynamoDB", "PutItem", System.currentTimeMillis() - startTime);
        }
    }

    private Long getDataForKeyFromDynamo(final String key) {
        String count = null;
        GetItemRequest getItemRequest = new GetItemRequest().withTableName(TABLE).withKey(
                ImmutableMap.of(HASH_KEY, new AttributeValue().withS(key)));
        getItemRequest.setConsistentRead(false);
        final long startTime = System.currentTimeMillis();
        try {
            GetItemResult getItemResult = dynamoClient.getItem(getItemRequest);
            metricsClient.recordEvent("DynamoDB", "GetItem", "Success");
            if (getItemResult != null && getItemResult.getItem() != null) {
                Map<String, AttributeValue> denominationItem = getItemResult.getItem();
                count = denominationItem.get(COUNT_ATTRIBUTE).getN();
            }
        } catch (AmazonServiceException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "AmazonServiceException");
            logger.error("AmazonServiceException while fetching " + TABLE, e);
        } catch (AmazonClientException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "AmazonClientException");
            logger.error("AmazonClientException while fetching " + TABLE, e);
        } finally {
            metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
        }
        return count == null ? null : Long.parseLong(count);
    }

	public void noteTDInvalidDenomination(int circleMasterId, int amount, String planType) {
		final String cacheKey = "tdInvalid" + "_" + String.valueOf(circleMasterId) + "_" + String.valueOf(amount) + "_" + planType;
		setWithOneDayExpiry(cacheKey);
		logger.info(cacheKey + " is set");
	}

	private void setWithOneDayExpiry(String cacheKey) {
		try {
			this.set(cacheKey, 1L, 1, TimeUnit.DAYS);
		} catch (Exception e) {
			logger.error("Exception while setting key: " + cacheKey, e);
		}

	}
	
	public Boolean getTDPlanValidity(String circleId, int amount, String planType) {
		final String cacheKey = "tdInvalid" + "_" + circleId + "_" + String.valueOf(amount) + "_" + planType;
		return fetchBoolean(cacheKey);
	}

	private Boolean fetchBoolean(final String cacheKey) {
		Long value;
		try {
			 value = (Long) this.get(cacheKey);
			if (value != null && value==1L ) {
				return true;
			}
		} catch (Exception e) {
			logger.error("Exception while fetching key: " + cacheKey, e);
		}
		return false;
	}

	public void noteValidatedInvalidDenomination(int operatorId, int circleId, int amount, String planType) {
		final String cacheKey = "invalidDenom" + "_" + String.valueOf(operatorId) + "_" + String.valueOf(circleId) + "_"
				+ String.valueOf(amount) + "_" + planType;
		setWithOneDayExpiry(cacheKey);
		logger.info(cacheKey + " is set");

	}
	
	public Boolean getValidatedInvalidDenomination(String operatorId, String circleId, int amount, String planType) {
		final String cacheKey = "invalidDenom" + "_" + operatorId + "_" + circleId + "_" + String.valueOf(amount) + "_"
				+ planType;
		return fetchBoolean(cacheKey);
	}

	public Set<RechargePlan> getAllDenominationsTried(String operatorId, String circleId,String productId,
			Integer allDenominationCheckDataWindowDays) {
		Set<RechargePlan> allDenominationsTried = new HashSet<RechargePlan>();
		String pattern = null;
		for (String dateKey : rechargeMetricService.getDateKeys(fcProperties.getAllDenominationCheckDataWindowDays())) {
			pattern = formPattern(operatorId, circleId, dateKey);			
			Collection<String> redisKeys = this.getKeys(pattern);
			if (!CollectionUtils.isEmpty(redisKeys)) {
				List<RechargePlan> rechargePlanList = formPlans(redisKeys,productId);
				for (RechargePlan newPlan : rechargePlanList) {
					logger.info("User Plan to add "+newPlan);
					allDenominationsTried.add(newPlan);
				}
			}
		}
		return allDenominationsTried;
	}

	private List<RechargePlan> formPlans(Collection<String> redisKeys,String productId) {
		List<RechargePlan> planList = new ArrayList<RechargePlan>();
		for(Iterator<String> iterator =redisKeys.iterator(); iterator.hasNext();){
			RechargePlan rechargePlan = formRechargePlanFromKey(iterator.next(),productId);
			planList.add(rechargePlan);
		}
		
		return planList;
	}

	private RechargePlan formRechargePlanFromKey(String key ,String productId) {		
		RechargePlan rechargePlan = new RechargePlan();
		String splitKey[] = key.split("_");
		rechargePlan.setOperatorMasterId(Integer.valueOf(splitKey[0]));
		rechargePlan.setCircleMasterId(Integer.valueOf(splitKey[1]));
		rechargePlan.setAmount(BigDecimal.valueOf(Float.valueOf(splitKey[3])));
		rechargePlan.setDenominationType(splitKey[4]);
		rechargePlan.setProductType(Integer.valueOf(productId));
		return rechargePlan;
	}

	private String formPattern(String operatorId, String circleId, String dateKey) {
		String pattern = operatorId + "_" + circleId + "_" + dateKey + "_" + "*";
		return pattern;
	}


}
