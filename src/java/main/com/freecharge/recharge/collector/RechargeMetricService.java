package com.freecharge.recharge.collector;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.SuccessDenominationCount;
import com.freecharge.common.util.SuccessDenominationType;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.platform.metrics.CloudWatchMetricService;
import com.freecharge.recharge.services.PlanValidatorService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.reward.util.FCUtil;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RechargeMetricService {
    @Autowired
    private InvalidDenominationCache denominationCache;

    @Autowired
    private RechargeMetricsCache rechargeMetricsCache;
    
    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private CloudWatchMetricService cwService;
    
    @Autowired
    private PlanValidatorService planValidatorService;
    
    private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    
    private final List<String> euronetInvalidDenominationCodeList = ImmutableList.of("AI", "EAI");
    
    private final List<String> euronetPermanentFailureCodes = ImmutableList.of("CI", "ECI");
    
    private final List<String> postpaidPermanentFailureCodes = ImmutableList.of("18587","CI","17047","16989");
    
    private final String AIRTEL = "2";
    
    private final String BSNL = "3";
    
    private final String MTSMBrowse = "40";
    
    private final String MTSMblaze = "41";
    
    private final String TataIndicomDelhi = "21";
    
    private final String VODAFONE = "14";
    
    private final String SPECIAL_RECHARGE_TYPE = "special";
    private final String TOPUP_RECHARGE_TYPE = "topup";
    
    private final String DATACARD_TXN_COUNT_BENCHMARK = "dcardTxnBenchmark";
    private final String DEFAULT_TXN_COUNT_BENCHMARK = "defaultTxnBenchmark";

    public RechargeDenominationStat getRechargeMetricsBasedOnRechargeAmount(Long operatorId, Integer circleId, Float rechargeAmount, String rechargeType, String productType) {
        Double benchMarkCount = getBenchmarkCount(productType);
        Double successCount = 0d;
        Double failureCount = 0d;

        for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckDataWindowDays())) {
            successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), rechargeAmount.doubleValue(), dateKey, rechargeType);
            failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), rechargeAmount.doubleValue(), dateKey, rechargeType);
        }

        Double totalCount = successCount + failureCount;

        RechargeDenominationStat rechargeDenominationStat = new RechargeDenominationStat();
        rechargeDenominationStat.setCircleId(circleId);
        rechargeDenominationStat.setOperatorId(operatorId);
        rechargeDenominationStat.setRechargeAmount(rechargeAmount);
        if (totalCount > benchMarkCount) {
            Double successRate = (successCount / totalCount) * 100;
            rechargeDenominationStat.setSuccessRate(successRate.intValue());
        } else {
            successCount = 0d;
            failureCount = 0d;

            for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckSpecialDataWindowDays())) {
                successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), rechargeAmount.doubleValue(), dateKey, rechargeType);
                failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), rechargeAmount.doubleValue(), dateKey, rechargeType);
            }

            totalCount = successCount + failureCount;
            if (totalCount > 0) {
                Double successRate = (successCount / totalCount) * 100;
                rechargeDenominationStat.setSuccessRate(successRate.intValue());
            } else {
                rechargeDenominationStat.setSuccessRate(100);
            }
        }
        return rechargeDenominationStat;
    }
    
	private Double getBenchmarkCount(String productType) {

		String customizedBenchMarkCount = getCustomizedBenchMarkCount(productType);

		if (!FCUtil.isEmpty(customizedBenchMarkCount)) {
			return Double.valueOf(customizedBenchMarkCount);
		}
		if ("C".equals(productType)) {
			return fcProperties.getDatacardBenchMarkCount().doubleValue();
		}
		return fcProperties.getBenchMarkCount().doubleValue();
	}
    
	private String getCustomizedBenchMarkCount(String productType) {
		String benchMark = null;
		JSONObject configJson = appConfigService.getCustomProp(appConfigService.RECHARGE_CUSTOM_CONFIG);

		if (configJson != null) {
			if ("C".equals(productType)) {
				benchMark = (String) configJson.get(DATACARD_TXN_COUNT_BENCHMARK);
			} else {
				benchMark = (String) configJson.get(DEFAULT_TXN_COUNT_BENCHMARK);
			}

		}
		return benchMark;
	}

	private Double getBenchMarkSuccessRate(String productType) {
        if("C".equals(productType)) {
            return fcProperties.getDatacardBenchMarkSuccessRate().doubleValue();
        }
        return fcProperties.getBenchMarkSuccessRate().doubleValue();
    }

    public Boolean isInvalidDenomination(String operatorId, String circleId, Double amount, String rechargeType, String productType) {
        if (!appConfigService.isInvalidDenominationBlockEnabled()) {
            return false;
        }
        
        Double benchMarkCount = getBenchmarkCount(productType);
        Double benchMarkSuccessRate =  getBenchMarkSuccessRate(productType); 

        Double successCount = 0d;
        Double failureCount = 0d;
        
        for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckDataWindowDays())) {
            successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
            failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
        }

        Double totalCount = successCount + failureCount;
        Double successRate = 100d;

        if (totalCount > benchMarkCount) {
            successRate = (successCount / totalCount) * 100;
        } else {
            successCount = 0d;
            failureCount = 0d;
            
            for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckSpecialDataWindowDays())) {
                successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
                failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
            }

            totalCount = successCount + failureCount;
            if (totalCount > benchMarkCount) {
                successRate = (successCount / totalCount) * 100;
            }
        }

        if (successRate < benchMarkSuccessRate) {
            return true;
        }

        return false;
    }
    /*
     * To check if successRate for a particular operator, circle and amount is greater than 'benchMarkSuccessRate'
     */
    public Boolean isInvalidDenomination(final String operatorId, final String circleId, final Double amount, final Double benchMarkSuccessRate, final String rechargeType) {
        Double benchMarkCount = fcProperties.getBenchMarkCount().doubleValue();
        Double successCount = 0d;
        Double failureCount = 0d;

        for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckDataWindowDays())) {
            successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), amount,
                    dateKey, rechargeType);
            failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), amount,
                    dateKey, rechargeType);
        }
        Double totalCount = successCount + failureCount;
        Double successRate = 100d;
        if (totalCount > benchMarkCount) {
            successRate = (successCount / totalCount) * 100;
        } else {
            successCount = 0d;
            failureCount = 0d;

            for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckSpecialDataWindowDays())) {
                successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
                failureCount += denominationCache.getFailureCount(operatorId.toString(), circleId.toString(), amount, dateKey, rechargeType);
            }
            totalCount = successCount + failureCount;
            if (totalCount > benchMarkCount) {
                successRate = (successCount / totalCount) * 100;
            }
        }

        if (successRate < benchMarkSuccessRate) {
            return true;
        }
        return false;
    }

    public void cacheRechargeDetails(InRequest inRequest, InResponse inResponse, List<InTransactions> inTransactionsList, TxnHomePageBusinessDO homePage, boolean isOperatorRetry) {
        if (inResponse != null && inRequest != null) {
            
            final String product = homePage.getProductType();
            final String circleId = homePage.getCircleName();
            final String operatorId = homePage.getOperatorName();
            final String serviceNumber = homePage.getServiceNumber();
            final Double rechargeAmount = homePage.getAmount().doubleValue();
            final String rechargePlan = inRequest.getRechargePlan();
            final String validDenominationMetric = "Valid:" + rechargeAmount.toString(); 
            final String invalidDenominationMetric = "Invalid:" + rechargeAmount.toString(); 
                    
            if (isSuccessfulTransaction(inResponse.getInRespCode())) {
                if(!isOperatorRetry) {
                    denominationCache.noteValidDenomination(operatorId, circleId, rechargeAmount, rechargePlan);
                    cwService.recordDenominationMetric(product, operatorId, circleId, rechargePlan, validDenominationMetric);
                }
            } else {
                if (isPermanentFailure(inResponse)) {
                    rechargeMetricsCache.notePermanentFailure(homePage.getProductType(), operatorId, serviceNumber);
                } else if (isInvalidDenomination(inRequest, inTransactionsList ,product)) {
                    rechargeMetricsCache.noteInvalidDenominationNumber(serviceNumber, operatorId, rechargeAmount, rechargePlan);
                    if(!StringUtils.isBlank(circleId)) {
                        cwService.recordDenominationMetric(product, operatorId, circleId, rechargePlan, invalidDenominationMetric);
                        denominationCache.noteInvalidDenomination(operatorId, circleId, rechargeAmount, rechargePlan);
                    }
                } else {
                    if (StringUtils.equals(operatorId, AIRTEL)) {
                        if (isEGFailure(inTransactionsList)) {
                            rechargeMetricsCache.noteEGFailureNumber(serviceNumber, operatorId, rechargeAmount);
                            denominationCache.noteAirtelInvalidDenomination(circleId, rechargeAmount);
                        }
                    }
                    
                    if (StringUtils.equals(operatorId, BSNL)) {
                        if (isEIFailure(inTransactionsList)) {
                            rechargeMetricsCache.noteEIFailureNumber(serviceNumber, operatorId, rechargeAmount);
                            denominationCache.notebBsnlInvalidDenomination(circleId, rechargeAmount, rechargePlan);
                        }
                   }
                   if (StringUtils.equals(operatorId, MTSMBrowse) || StringUtils.equals(operatorId, MTSMblaze)) {
                        if (is3596Failure(inTransactionsList)) {
                            rechargeMetricsCache.noteInvalidDenominationNumber(serviceNumber, operatorId, rechargeAmount, rechargePlan);
                            denominationCache.noteInvalidDenomination(operatorId, circleId, rechargeAmount, rechargePlan);
                        }
                   }
                   if (StringUtils.equals(operatorId, TataIndicomDelhi)) {
                          if (is17011Failure(inTransactionsList)) {
                              rechargeMetricsCache.noteInvalidDenominationNumber(serviceNumber, operatorId, rechargeAmount, rechargePlan);
                              denominationCache.noteInvalidDenomination(operatorId, circleId, rechargeAmount, rechargePlan);
                          }
                   }
                   
                   if (StringUtils.equals(operatorId, VODAFONE)) {
                          if (!isOperatorDown(inRequest, inTransactionsList)) {
                              denominationCache.noteInvalidDenomination(operatorId, circleId, rechargeAmount, rechargePlan);
                          }
                   }
                }
            }
        }
    }
    
    public void postpaidCacheFailureDetails(TxnHomePageBusinessDO homePage, BillPaymentStatus billPaymentStatus) {
        final String operatorName = homePage.getOperatorName();
        final String serviceNumber = homePage.getServiceNumber();
        if (isPostpaidPermanentFailure(billPaymentStatus)) {
            rechargeMetricsCache.notePostpaidPermanentFailure(homePage.getProductType(), operatorName, serviceNumber);
        }
    }
    
    private Boolean isPostpaidPermanentFailure(BillPaymentStatus billPaymentStatus) {
        if (postpaidPermanentFailureCodes.contains(billPaymentStatus.getAuthStatus())) {
            return true;
        } 
        return false;
    }
    
    public boolean isEGFailureNumber(String serviceNumber, String operator, Double amount) {
        if (!appConfigService.isEGNumberBlockEnabled()) {
            return false;
        }
        
        return rechargeMetricsCache.isEGFailureNumber(serviceNumber, operator, amount);
    }

    public boolean isInvalidDenominationNumber(String productType, String serviceNumber, String operator, Double amount, String rechargeType) {
        if (!appConfigService.isDenominationNumberBlockEnabled()) {
            return false;
        }
        
        return rechargeMetricsCache.isInvalidDenominationNumber(serviceNumber, operator, amount, rechargeType);
    }
    
    public boolean isAirtelInvalidDenomination(String operator, String circle, Double amount) {
        if (!StringUtils.equals(operator, AIRTEL)) {
            return false;
        }
        
        final int scanWindow = fcProperties.getAirtelInvalidDenominationScanWindowDays();
        final int maxFailureThreshold = fcProperties.getAirtelMaxFailureThreshold();
        
        List<String> dateKeys = getDateKeys(scanWindow);
        
        Double successCount = 0d;
        Double failureCount = 0d;
        
        for (String dateKey : dateKeys) {
            failureCount += denominationCache.getAirtelInvalidDenomination(circle, dateKey, amount);
            successCount += denominationCache.getSuccessCount(operator, circle, amount, dateKey, RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE);
        }
        
        if (successCount == 0) {
            if (failureCount >= maxFailureThreshold) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean isAirtelInvalidDenominationCheck(String operator, String circle, Double amount) {
           if (!StringUtils.equals(operator, AIRTEL)) {
               return false;
           }
           
           final int scanWindow = fcProperties.getAirtelInvalidDenominationScanWindowDays();
           final int maxSuccessPlusFailureThreshold = fcProperties.getAirtelMaxFailurePlusSuccessThreshold();
           final int maxFailurePercentage = fcProperties.getAirtelBenchMarkFailureRate();
           
           List<String> dateKeys = getDateKeys(scanWindow);

           Double successCount = 0d;
           Double failureCount = 0d;
           
           for (String dateKey : dateKeys) {
               failureCount += denominationCache.getAirtelInvalidDenomination(circle, dateKey, amount);
               successCount += denominationCache.getSuccessCount(operator, circle, amount, dateKey, RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE);
           }
           
           final double totalCount = successCount + failureCount;

           if (totalCount >= maxSuccessPlusFailureThreshold) {
               double failureRate = (failureCount / totalCount) * 100;
               
               if ( failureRate >= maxFailurePercentage) {
                   return true;
               }
           }
           
           return false;
       }



    public boolean isBsnlInvalidDenomination(String operator, String circle, Double amount, String rechargePlanType) {
           if (!StringUtils.equals(operator, BSNL)) {
             return false;
           }
           
           final int scanWindow = fcProperties.getBsnlInvalidDenominationScanWindowDays();
           final int maxFailureThreshold = fcProperties.getBsnlMaxFailureThreshold();
           final int maxFailurePercentageThreshold = fcProperties.getBsnlBenchMarkfailureRate();
           List<String> dateKeys = getDateKeys(scanWindow);
           Double successCount = 0d;
           Double failureCount = 0d;
           
           for (String dateKey : dateKeys) {
                  failureCount += denominationCache.getBsnlInvalidDenomination(circle, dateKey, amount, rechargePlanType);
                  successCount += denominationCache.getSuccessCount(operator, circle, amount, dateKey, rechargePlanType);
           }
           if (successCount+failureCount>=maxFailureThreshold) {
           if ((failureCount*100/(successCount+failureCount)) >= maxFailurePercentageThreshold) {
                  return true;
                  }
           }
                  return false;
    }
    
    public boolean isPermanentFailure(String productType, String operator, String serviceNumber) {
        if (!appConfigService.isPermanentFailureBlockEnabled()) {
            return false;
        }
        
        return rechargeMetricsCache.isPermanentFailure(productType, operator, serviceNumber);
    }
    
    public boolean isPostpaidPermanentFailure(String productType, String operator, String serviceNumber) {
        if (!appConfigService.isPostpaidPermanentFailureBlockEnabled()) {
            return false;
        }
        
        return rechargeMetricsCache.isPostpaidPermanentFailure(productType, operator, serviceNumber);
    }
    
    /**
     * Certain types of failures e.g., wrong product type (trying to
     * recharge a postpaid), wrong operator are of permanent in nature.
     * This method returns if the failure response is of permanent
     * in nature.
     * @param inResponse
     * @return
     */
    private boolean isPermanentFailure(InResponse inResponse) {
        final String aggrRespCode = inResponse.getAggrRespCode();
        final String aggrName = inResponse.getAggrName();
        final String aggrMessage = inResponse.getAggrRespMsg();
        
        if (StringUtils.equals("euronet", aggrName)) {
            if (StringUtils.isNotBlank(aggrRespCode)) {
                if (euronetPermanentFailureCodes.contains(aggrRespCode)) {
                    return true;
                }
            }
        } else if (StringUtils.equals("oxigen", aggrName)) {
            if (StringUtils.containsIgnoreCase(aggrMessage, "is not a valid")) {
                return true;
            } else if (StringUtils.containsIgnoreCase(aggrMessage, "Unable to read config Details")) {
                return true;
            } else if (StringUtils.containsIgnoreCase(aggrMessage, "No circle found for this")) {
                return true;
            }
        }
        
        return false;
    }
    
    private boolean isEGFailure(List<InTransactions> inTransactionsList) {
        return isErrorCodePresent(inTransactionsList, "euronet", "EG");
    }
    
    /**
     * This is to delete keys that are not to be
     * used anymore. The method fetches all keys
     * older than DATA_COLLECTION_WINDOW_DAYS days
     * and deletes them.
     */
    public void discardStaleDenominationMetric() {
        DateTime today = new DateTime();
        denominationCache.deleteMetrics(today.minusDays(fcProperties.getInvalidDenomincationCheckSpecialDataWindowDays()).toDate());
    }
    /**
     * This is to delete keys that are not to be
     * used anymore. The method fetches all keys
     * older than DATA_COLLECTION_WINDOW_DAYS days
     * and deletes them.
     */
    public void discardStaleDenominationMetricFromDynamo() {
        DateTime today = new DateTime();
        denominationCache.deleteMetricsFromDynamo(today.minusDays(fcProperties.getInvalidDenomincationCheckSpecialDataWindowDays()).toDate());
    }

	private Boolean isInvalidDenomination(InRequest inRequest, List<InTransactions> inTransactionsList,
			String productType) {
		Boolean isInvalidDenomination = false;
		for (InTransactions inTransaction : inTransactionsList) {
			final String aggregatorName = inTransaction.getAggrName();
			final String aggregatorResponseCode = inTransaction.getAggrResponseCode();
			final String aggregatorResponseMessage = inTransaction.getAggrResponseMessage();

			if (planValidatorService.isInvalidDenominationResponse(aggregatorResponseCode,
					ProductMaster.ProductName.fromPrimaryProductType(productType).getProductId())) {
				isInvalidDenomination = true;
				break;
			}

			/*
			 * if (aggregatorName.equals("euronet")) { if
			 * (!StringUtils.isBlank(aggregatorResponseCode) &&
			 * euronetInvalidDenominationCodeList.contains(
			 * aggregatorResponseCode)) { isInvalidDenomination = true; } else {
			 * isInvalidDenomination =
			 * isInvalidDenominationMessage(aggregatorResponseMessage); } } else
			 * if (aggregatorName.equals("oxigen")) { isInvalidDenomination =
			 * isInvalidDenominationMessage(aggregatorResponseMessage); } if
			 * (isInvalidDenomination == true) { break; }
			 */
		}
		return isInvalidDenomination;
	}

    private Boolean isInvalidDenominationMessage(final String message) {
        if (!StringUtils.isBlank(message)) {
            if (StringUtils.containsIgnoreCase(message, "denomination")) {
                return true;
            } else if (StringUtils.containsIgnoreCase(message, "transfer amount is not")) {
                return true;
            } else if (StringUtils.containsIgnoreCase(message, "Invalid Amount")) {
                return true;
            }
        }

        return false;
    }

    private boolean isSuccessfulTransaction(String inResponse) {
        return ("0".equals(inResponse) || "00".equals(inResponse));
    }

    public List<String> getDateKeys(final int dataWindowDays) {
        List<String> dateKeyList = new ArrayList<>();

        DateTime today = new DateTime();

        dateKeyList.add(DATE_FORMAT.format(today.toDate()));
        
        for (int i = 1; i <= dataWindowDays; i++) {
            dateKeyList.add(DATE_FORMAT.format(today.minusDays(i).toDate()));
        }
        return dateKeyList;
    }
    /*
     * Returns the top denominations (ordered by recharge success count)
     */
    public SuccessDenominationCount[] getTopRechargeDenominations(final Integer operatorId, final Integer circleId) {
        Double successCount;
        Collection<String> successKeys = denominationCache.getValidDenominationKeys(operatorId, circleId);
        Set<SuccessDenominationType> successDenominationTypes = new LinkedHashSet<>();
        for (String successKey : successKeys) {
            SuccessDenominationType successDenominationType = new SuccessDenominationType();
            String[] parts = successKey.split("_");
            Double amount = Double.parseDouble(parts[3]);
            successDenominationType.setAmount(BigDecimal.valueOf(amount.intValue()));
            if (successKey.contains(SPECIAL_RECHARGE_TYPE)) {
                successDenominationType.setSpecial(true);
            } else {
                successDenominationType.setSpecial(false);
            }
            successDenominationTypes.add(successDenominationType);
        }
        List<SuccessDenominationCount> successDenominationCounts = new ArrayList<>();
        for (SuccessDenominationType successAmountType : successDenominationTypes) {
            successCount = 0d;
            boolean isSpecial = successAmountType.isSpecial();
            BigDecimal successAmount = successAmountType.getAmount();
            String rechargeType = TOPUP_RECHARGE_TYPE;
            if (isSpecial) {
                rechargeType = SPECIAL_RECHARGE_TYPE;
            }
            for (String dateKey : getDateKeys(fcProperties.getInvalidDenomincationCheckDataWindowDays())) {
                successCount += denominationCache.getSuccessCount(operatorId.toString(), circleId.toString(),
                        successAmount.doubleValue(), dateKey, rechargeType);
            }
            SuccessDenominationCount succDenCount = new SuccessDenominationCount(successAmount, successCount, isSpecial);
            successDenominationCounts.add(succDenCount);
        }
        SuccessDenominationCount[] successDenominationCountsArray = new SuccessDenominationCount[successDenominationCounts
                .size()];
        successDenominationCountsArray = successDenominationCounts.toArray(successDenominationCountsArray);
        Arrays.sort(successDenominationCountsArray);
        return successDenominationCountsArray;
    }

    private boolean isEIFailure(List<InTransactions> inTransactionsList) {
        return isErrorCodePresent(inTransactionsList, "euronet", "EI");
    }

    private boolean is3596Failure(List<InTransactions> inTransactionsList) {
        return isErrorCodePresent(inTransactionsList, "oxigen", "3596");
    }

    private boolean is17011Failure(List<InTransactions> inTransactionsList) {
        return isErrorCodePresent(inTransactionsList, "oxigen", "17011");
    }
    
    private boolean isErrorCodePresent(List<InTransactions> inTransactionsList, String expectedAggrName, String expecedErrorCode) {
        for (InTransactions inTransaction : inTransactionsList) {
            final String aggrRespCode = inTransaction.getAggrResponseCode();
            final String aggrName = inTransaction.getAggrName();
            if (StringUtils.equals(expectedAggrName, aggrName)) {
                if (StringUtils.equals(aggrRespCode, expecedErrorCode)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private boolean isErrorMessagePresent(List<InTransactions> inTransactionsList, String agName, String errorMessage) {
        for (InTransactions inTransaction : inTransactionsList) {
            final String aggrRespMessage = inTransaction.getAggrResponseMessage();
            final String aggrName = inTransaction.getAggrName();
            
            if (StringUtils.equals(agName, aggrName)) {
                if (StringUtils.equalsIgnoreCase(aggrRespMessage, errorMessage)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    private boolean isOperatorDown(InRequest inRequest, List<InTransactions> inTransactionsList) {
        if (isErrorCodePresent(inTransactionsList, "euronet", "ES") 
                || isErrorMessagePresent(inTransactionsList, "oxigen", "service is currently unavailable")
                || isErrorMessagePresent(inTransactionsList, "oxigen", "please try after some time")
                || isErrorMessagePresent(inTransactionsList, "oxigen", "Service Unavailable")) {
            return true;
        }
        
        return false;
    }
       
    public Object getAggregatorPreferenceValue(String key) {
        return rechargeMetricsCache.getAggregatorPreferenceValue(key);
    }
       
    public void setAggregatorPreferenceValue(String key, Object value) {
        rechargeMetricsCache.setAggregatorPreferenceValue(key, value);
    }
       
    public void deleteAggregatorPreferencekey(String key) {
        rechargeMetricsCache.deleteAggregatorPreferencekey(key);
    }

	public void blockInvalidNumber(String serviceNumber) {
		rechargeMetricsCache.blockInvalidNumber(serviceNumber);
		
	}
	
	public boolean isInvalidNumber(String serviceNumber) {
		return rechargeMetricsCache.isInvalidNumber(serviceNumber);
	}
}