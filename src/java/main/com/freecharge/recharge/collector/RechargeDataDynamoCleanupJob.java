package com.freecharge.recharge.collector;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class RechargeDataDynamoCleanupJob {
    private static Logger logger = LoggingFactory.getLogger(RechargeDataDynamoCleanupJob.class);

    public static void main(String[] args) {
        logger.info("Starting with Recharge Data dynamo cleanup job");
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        try {
            RechargeMetricService rms = ctx.getBean(RechargeMetricService.class);
            rms.discardStaleDenominationMetricFromDynamo();
        } catch (Exception e) {
            logger.error("ERROR occured in executing RechargeDataDynamoCleanupJob.");
            logger.error(e.getCause(), e);
        } finally {
            try {
                logger.info("Start batch job context destroy.");
                BatchJobUtil.destroyBatchJobContext(ctx);
                logger.info("Completed batch job context destroy.");
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }
}
