package com.freecharge.recharge.collector;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.OperatorAlert;

/**
 * A service to expose success/failure rates of a
 * given operator.
 * @author arun
 *
 */
@Service
public class OperatorMetricService {
    @Autowired
    private RechargeMetricsCache rechargeMetricsCache;
    
    private List<String> aggregatorList = Arrays.asList("euronet", "oxigen");
    
    /*
     * The total count should be above this threshold to
     * be able to taken into consideration. 
     * 
     * TODO: Move this to config
     */
    private final Double countThreshold = new Double("100");
    
    /*
     * TODO: Move this to config
     */
    private final Double failureRateThreshold = new Double("30");
    
    /*
     * If data point is older than this then
     * it won't be considered for calculation.
     * 
     * TODO: Move this to config.
     */
    private final int metricExpiryWindowMinutes = 30;
    
    public OperatorAlert getRechargeMetricsAlert(Long operatorId) {
        
        String operatorKey = OperatorMetricDataBean.getOperatorCacheKey(operatorId.toString());
        
        double failureCount = 0;
        double totalCount = 0;
        double failureRate = 0;
        
        for (String aggregatorKey : aggregatorList) {
            RechargeAggregateMetric aggregateMetric = rechargeMetricsCache.get(aggregatorKey + "." + operatorKey);
            
            if (aggregateMetric != null) {
                DateTime dataPointTime = new DateTime(aggregateMetric.getTotalCount().getTime());
                
                if (dataPointTime.isAfter(DateTime.now().minusMinutes(metricExpiryWindowMinutes))) {
                    totalCount += aggregateMetric.getTotalCount().getValue();
                    failureCount += aggregateMetric.getFailureCount().getValue();
                }
            }
        }
        
        if (totalCount > countThreshold) {
            failureRate = new Double((failureCount / totalCount) * 100);
        }
        
        if (failureRate > failureRateThreshold) {
            String messageFormat = "We are facing a high failure rate in the last few minutes, please try after some time";
           
            OperatorAlert alert = new OperatorAlert();
            alert.setOperatorId(Integer.parseInt(operatorId+""));
            alert.setStartDateTime(DateTime.now().minusMinutes(5).toDate());
            alert.setEndDateTime(DateTime.now().plusMinutes(5).toDate());
            alert.setAlertMessage(messageFormat);
            
            return alert;
        }
        
        return null;
    }
}
