package com.freecharge.recharge.collector;

import java.util.List;

/**
 * This class encapsulates various metrics of interest for a given
 * aggregator and operator. 
 * @author arun
 *
 */
public class RechargeMetrics {
    private String aggregator;
    private String operatorName;
    private String operatorCode;
    private String collectionPeriod;
    private String successCountMetricString;
    private String failureCountMetricString;
    private String totalCountMetricString;
    private List<String> operatorDownMetricStrings;
    
    public String getAggregator() {
        return aggregator;
    }
    public void setAggregator(String aggregator) {
        this.aggregator = aggregator;
    }
    
    public String getOperatorName() {
        return operatorName;
    }
    public void setOperatorName(String operator) {
        this.operatorName = operator;
    }
    
    public String getSuccessCountMetricString() {
        return successCountMetricString;
    }
    public void setSuccessCountMetricString(String successCountMetricString) {
        this.successCountMetricString = successCountMetricString;
    }
    
    public String getTotalCountMetricString() {
        return totalCountMetricString;
    }
    public void setTotalCountMetricString(String totalCountMetricString) {
        this.totalCountMetricString = totalCountMetricString;
    }
    
    public String getFailureCountMetricString() {
        return failureCountMetricString;
    }
    public void setFailureCountMetricString(String failureCountMetricString) {
        this.failureCountMetricString = failureCountMetricString;
    }
    
    public String getCollectionPeriod() {
        return collectionPeriod;
    }
    public void setCollectionPeriod(String collectionPeriod) {
        this.collectionPeriod = collectionPeriod;
    }
    
    public String getOperatorCode() {
        return operatorCode;
    }
    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }
    public List<String> getOperatorDownMetricStrings() {
        return operatorDownMetricStrings;
    }
    public void setOperatorDownMetricStrings(List<String> operatorDownMetricStrings) {
        this.operatorDownMetricStrings = operatorDownMetricStrings;
    }
    
}
