package com.freecharge.recharge;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;

@Service
public class SaveLastRechargeListener implements ApplicationListener<RechargeSuccessEvent> {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    OperatorCircleService operatorCircleService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private FulfillmentService fulfillmentService;

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {
        String orderId = event.getOrderId();

        logger.info("RechargeSuccessEvent caught by Listener");
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService
                .findUserTransactionHistoryByOrderId(orderId);
        Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);

        if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
            userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
            userDetail.put("circleName", userTransactionHistory.getServiceRegion());
            userDetail.put("productType", userTransactionHistory.getProductType());
        }
        UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);

        TransactionStatus transactionStatus = fulfillmentService.getTransactionStatus(userDetailsObj, orderId);

        if (transactionStatus == null && FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
            transactionStatus = fulfillmentService.getTransactionStatusForBillPay(userDetailsObj.getBillId());
            transactionStatus.setSubscriberIdentificationNumber(userDetailsObj.getUserMobileNo());
            transactionStatus.setOperator(userDetailsObj.getOperatorName());
            transactionStatus.setServiceRegion(userDetailsObj.getCircleName());
            transactionStatus.setProductType(userDetailsObj.getProductType());
        }

        SuccessfulRechargeInfo successRechargeInfo = SuccessfulRechargeInfo
                .createSuccessfullRechargeInfo(transactionStatus, userDetailsObj);
        userService.setLatestSuccessfulRecharge(userDetailsObj.getUserEmail(), successRechargeInfo);
        logger.info("Latest Successful Recharge is set in cache for orderId:" + orderId);

    }

}
