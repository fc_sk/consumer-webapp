package com.freecharge.recharge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.businessdo.OperatorCircleBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;
import com.freecharge.freebill.service.MobilePostpaidService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;

/**
 * Listens to recharge success event to record MNP numbers if any.
 * @author arun
 *
 */
@Service
public class MNPListener implements ApplicationListener<RechargeSuccessEvent> {
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    MetricsClient metricsClient;
    
    @Autowired
    AmazonDynamoDBAsync dynamoClientMum;
    
    @Autowired
    OperatorCircleService prefixService;

    @Autowired
    MobilePostpaidService mobilePostpaidService;
    
    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    OperatorCircleService operatorCircleService;
    
    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {
        String orderId = event.getOrderId();
        
        UserTransactionHistory uth = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
        
        if (uth == null) {
            logger.error("Unable to find user_transaction_history for orderId : " + orderId);
            return;
        }
        
        if (FCUtil.isUtilityPaymentType(uth.getProductType())) {
            return;
        }
        
        CircleMaster circleMaster = operatorCircleService.getCircle(uth.getServiceRegion());
        OperatorMaster operatorMaster = operatorCircleService.getOperator(uth.getServiceProvider());
        
        String serviceNumber = uth.getSubscriberIdentificationNumber();
        String selectedOperator = String.valueOf(operatorMaster.getOperatorMasterId());
        String selectedCircle = String.valueOf(circleMaster.getCircleMasterId());
        String selectedProduct = String.valueOf(FCConstants.productMasterMapReverse.get(uth.getProductType()));
        
        if (isMNPNumber(serviceNumber, selectedOperator, selectedProduct)) {
            metricsClient.recordEvent("Recharge", "MNP", selectedProduct + "." + selectedOperator + "." + selectedCircle);
            recordMNP(serviceNumber, selectedProduct, selectedOperator, selectedCircle == null ? "" : selectedCircle);
        }
    }

    public boolean isMNPNumber(String serviceNumber, String selectedOperator, String selectedProduct) {
        ProductName productName = ProductName.fromProductId(Integer.parseInt(selectedProduct));
        
        switch (productName) {
            case MobilePostpaid:
                Integer inferredPostpaidOperator = mobilePostpaidService.findOperatorIdByMobileNumber(serviceNumber);
                
                if (inferredPostpaidOperator != null) {
                    if (StringUtils.equals(selectedOperator, inferredPostpaidOperator.toString())) {
                        return false;
                    }
                }
                
                return true;
            
            case DataCard:
            case PostPaidDataCard:
            case Mobile:

                OperatorCircleBusinessDO getPrefixRequest = new OperatorCircleBusinessDO();
                getPrefixRequest.setPrefix(serviceNumber);
                getPrefixRequest.setProductType(selectedProduct);
                
                prefixService.getOperatorCircleList(getPrefixRequest);
                
                List<Map<String,Object>> prefixData = getPrefixRequest.getPrefixData();
                
                if (prefixData != null) {
                    for (Map<String, Object> prefixMap : prefixData) {
                        Integer inferredOperator = (Integer) prefixMap.get("operatorMasterId");
                        if (StringUtils.equals(selectedOperator, inferredOperator.toString())) {
                            return false;
                        }
                    }
                }
                
                return true;
                
            default: return false;
        }
        
    }
    
    private void recordMNP(String serviceNumber, String product, String operator, String circle) {
        Map<String, AttributeValue> item = new HashMap<>();
        if (circle.equalsIgnoreCase("-1")) {
            metricsClient.recordEvent("DynamoDB", "PutItem", "Faulure");
            logger.error("Circle Id is -1 when trying to enter mnpData to dynamo for " + serviceNumber);
            return;
        }
        item.put("serviceNumber", new AttributeValue(serviceNumber));
        item.put("pr", new AttributeValue(product));
        item.put("oi", new AttributeValue(operator));
        
        if (StringUtils.isBlank(circle)) {
            item.put("ci", new AttributeValue("NA"));
        } else {
            item.put("ci", new AttributeValue(circle));
        }
        
        logger.info("About to record MNP [Number:" + serviceNumber + ", Product: " + product + ", Operator: " + operator + ", Circle: " + circle + "]");

        final long startTime = System.currentTimeMillis();
        try {
           
            dynamoClientMum.putItem(new PutItemRequest("mnpMap", item));
            metricsClient.recordEvent("DynamoDB", "PutItem", "Success");
            logger.info("Done recording MNP [Number:" + serviceNumber + ", Product: " + product + ", Operator: " + operator + ", Circle: " + circle + "]");
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while recording MNP", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonServiceException");
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while recording MNP", e);
            metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonClientException");
        } finally {
            metricsClient.recordLatency("DynamoDB", "PutItem", System.currentTimeMillis() - startTime);
        }
    }
}
