package com.freecharge.recharge.util;

public enum CallSourceType {

	RECHARGE_SAVE(1), AGGREGATOR_SERVICE(2);

	private int id;

	private CallSourceType(int id) {
		this.id = id;
	}

	public int getName() {
		return this.id;
	}

	public static CallSourceType fromId(int id) {
		switch (id) {
		case 1:
			return CallSourceType.RECHARGE_SAVE;

		case 2:
			return CallSourceType.AGGREGATOR_SERVICE;

		default:
			throw new IllegalArgumentException("Unknown CallType ID : " + id);
		}
	}
}
