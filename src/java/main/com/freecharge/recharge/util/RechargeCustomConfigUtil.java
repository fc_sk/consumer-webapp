package com.freecharge.recharge.util;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.AppConfigService;

@Service
public class RechargeCustomConfigUtil {

	@Autowired
	private AppConfigService appConfig;

	public List<String> getEventListForKey(String key) {

		JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);

		if (configJson != null) {
			final String enabledEventListString = (String) configJson.get(key);

			if (!StringUtils.isBlank(enabledEventListString)) {
				List<String> eventList = Arrays.asList(enabledEventListString.split(","));
				return eventList;

			}
		}
		return null;

	}

}
