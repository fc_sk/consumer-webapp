package com.freecharge.recharge.util;

import com.freecharge.common.timeout.ExternalClientCallCommand;
import com.freecharge.common.timeout.TimeoutKey;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

import java.util.concurrent.Callable;

public class PlanValidationCommandBuilder {

	public static ExternalClientCallCommand<Integer> getPlanValidationResponseCommand(final HttpClient httpClient,
			final HttpMethod method, final String serviceName, final String mtricName, final CallSourceType type) {
		TimeoutKey timeoutKey;
		if (type == CallSourceType.RECHARGE_SAVE) {
			timeoutKey = TimeoutKey.INTRANSACTION_VALIDATION_TIMEOUT;
		} else {
			timeoutKey = TimeoutKey.DEFAULT_VALIDATION_TIMEOUT;
		}
		return new ExternalClientCallCommand<Integer>(serviceName, mtricName, new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				return httpClient.executeMethod(method);
			}
		}, timeoutKey);
	}

}