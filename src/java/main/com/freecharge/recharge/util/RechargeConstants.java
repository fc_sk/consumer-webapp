package com.freecharge.recharge.util;

import java.util.Collection;
import java.util.Collections;

import com.freecharge.common.util.FCUtil;

public class RechargeConstants {
	public final static String  OCTANE_EMAIL_API_URL = "octane.email.api.url" ;
    public final static String DEFAULT_IN_RESPONSE_CODE = "01";
    public final static String EURONET_SUCCESS_RESPONSE_CODE = "00" ;
    
    // HCoupon Status Codes
    public final static String HCOUPON_FAILURE_PROCESS_CODE = "09" ;
    public final static String HCOUPON_REFUND_PROCESS_CODE = "10" ;
    		
    public final static String SUCCESS_RESPONSE_CODE = "00" ;
    public final static String EURONET_TRANSACTION_NOT_FOUND_RESPONSE_CODE = "ETI" ;
    public final static String OXIGEN_SUCCESS_RESPONSE_CODE = "0" ;
    public static final String GENERIC_BILL_PAYMENT_FAILURE_CODE = "55";
    public final static String SUVIDHAA_TXN_SUCCESS_RESPONSE_CODE = "0" ;
    public final static String SUVIDHAA_TXN_UNDERPROCESS_RESPONSE_CODE = "8" ;
    public final static String SUVIDHAA_REQUEST_CONTENT_TYPE = "Content-type";
    public final static String SUVIDHAA_REQUEST_CONTENT_TYPE_VALUE = "text/xml";
    public final static String  EURONET_UNDER_PROCESS_CODE = "UP" ;
    public final static String  IN_UNDER_PROCESS_CODE = "08";
    public final static String  RECHARGE_RETRY_INTERNAL_CODE = "RR";
    public final static String  AGGREGATOR_RETRY_CACHE_PREFIX = "AggregatorRetry-";
    public final static String  IN_PERMANENT_FAILURE_RESPONSE_CODE = "07" ;
    public final static String RECHARGE_NOT_ATTEMPTED = "Recharge Not Attempted";
    public final static String  IN_INVALID_MOBILE_RESPONSE_CODE = "05" ;
    public final static String DUMMY_RECHARGE_FAILURE_CODE = "29";
    public final static String INVALID_JIO_NON_PRIME_PLAN = "20061";
    public final static String INVALID_JIO_PRIME_PLAN = "20062";
    public final static String INVALID_JIO_PLAN_ERROR_CODE = "20063";
    public final static String JIO_POSTPAID_ERROR_CODE = "20064";
    public final static String INVALID_JIO_PLAN_JIO_PHONE_NON_PRIME_ONLY_ERROR_CODE = "20065";
    public final static String INVALID_JIO_PLAN_JIO_PHONE_PRIME_ONLY_ERROR_CODE = "20066";
    public final static String INVALID_JIO_PLAN_JIO_PHONE_ONLY_ERROR_CODE = "20067";
    public final static String INVALID_JIO_PLAN_PRIME_NON_PRIME_ONLY_ERROR_CODE = "20068";
    
    public final static String  IN_TRANSACTION_SUCCESSFUL_MESSAGE = "success" ;
    public final static String TRANSACTION_STATUS_UNSUCCESSFUL = "failure";
    public final static String TRANSACTION_STATUS_UNDER_PROCESS = "underprocess";
    public final static String TRANSACTION_STATUS_PENDING = "PENDING";
    public final static String TRANSACTION_STATUS_SUCCESS = "SUCCESS";
    public final static String TRANSACTION_STATUS_FAILED = "FAILED";
    public final static String TRANSACTION_STATUS_UP_FORCE_FAILED = "FAILED";
    public final static String POSTPAID_ADMIN_STATUS_UPDATE_AUTH_DETAIL = "Admin Txn Status Update";
    public final static String RECHARGE_REVERSAL_IN_RESP_MSG ="Transaction UnSuccessful via Reversal API";
    public final static String RECHARGE_REVERSAL_FORCE_IN_RESP_MSG ="Transaction force Unsuccessful";
    public final static String RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG ="Transaction Successful via Reversal API";
    
    public final static String INVALID_JIO_NON_PRIME_PLAN_MESSAGE = "The selected plan is valid only for prime user";
    public final static String INVALID_JIO_PRIME_PLAN_MESSAGE = "The selected plan is valid only for non prime user";
    public final static String INVALID_JIO_PLAN_JIO_PHONE_NON_PRIME_ONLY_ERROR_MESSAGE = "The selected amount is valid only for Jio PHONE non prime user"; 
    public final static String INVALID_JIO_PLAN_JIO_PHONE_PRIME_ONLY_ERROR_MESSAGE = "The selected amount is valid only for Jio PHONE prime user";
    public final static String INVALID_JIO_PLAN_JIO_PHONE_ONLY_ERROR_MESSAGE = "Selected amount is valid only for a Phone user";
    public final static String INVALID_JIO_PLAN_PRIME_NON_PRIME_ONLY_ERROR_MESSAGE = "Selected amount is valid for Prime/Non-Prime user";

    public final static String INVALID_JIO_PLAN_ERROR_CODE_MESSAGE = "No plans available for selected amount";
    public final static String JIO_POSTPAID_ERROR_CODE_MESSAGE = "Please chose a valid postpaid plan";
    public final static String POSTPAID_REVERSAL_AUTH_STATUS = "REV";
    public final static String POSTPAID_UP_FORCE_REVERSAL_AUTH_STATUS = "UP_REV";
    public final static String POSTPAID_SUCCESS_REVERSAL_AUTH_STATUS = "SUC_REV";
    
    public final static String REVERSAL_IN_RESP_CODE = "10900";
    public final static String SUCCESS_REVERSAL_IN_RESP_CODE = "10901";
    public final static String UP_REVERSAL_IN_RESP_CODE = "10902";

    public final static int  	HTTP_SUCCESS_STATUS_CODE = 200 ;
    public final static String IN_RETRYABLE_FAILURE_RESPONSE_CODE = "09";
    public final static String SUVIDHAA_TRANSACTION_STATUS_NOT_FOUND = "Transaction status not found in suvidhaa responde String";
    public final static String OXIGEN_RECHARGE_TYPE = "TOPUP";
    public final static String OXIGEN_DTH_RECHARGE_TYPE = "ETOP";
    public final static String OXIGEN_BILL_PAYMENT_TYPE = "BILL";
    public final static String SUVIDHAA_RESPONSE_XML_STRING = "xmlns:i="+"http://www.w3.org/2001/XMLSchema-instance";
    public final static String SUVIDHAA_STATUS_CHECK_RESPONSE_XML_STRING = "http://schemas.datacontract.org/2004/07/SIPL.Business.Common.Entity.ServiceUtilities.ThirdPartyAPI";
    public final static String SUVIDHAA_STATUS_CHECK_RESPONSE_XML_STRING1 ="http://www.w3.org/2001/XMLSchema-instance";
    public final static String MOBILE_PRODUCT_TYPE = "V";
    public final static String DTH_PRODUCT_TYPE = "D";
    public final static String DATA_CARD_PRODUCT_TYPE = "C";
    public final static String POSTPAID_DATA_CARD_PRODUCT_TYPE = "F";
    public final static String POST_PAID_PRODUCT_TYPE = "M";
    public static final String LANDLINE_PRODUCT_TYPE= "L";
    public static final String BROADBAND_PRODUCT_TYPE= "Y";
    public static final String ELECTRICITY_PRODUCT_TYPE = "E";    
    public static final String GAS_PRODUCT_TYPE = "G";
    public static final String METRO_PRODUCT_TYPE = "Z";
    public static final String WATER_PRODUCT_TYPE = "W";
    public static final String GOOGLE_CREDITS_PRODUCT_TYPE = "X";
    
    public final static String VIDEOCON_TV_OPERATOR_CODE = "Videocon Tv";
    public final static String SUN_TV_OPERATOR_CODE = "Sun Tv";
    

    public final static String INITIAL_RESPONSE_STATUS = "-1";

    public final static String NO_RESPONSE_STATUS = "";
    public final static String EURONET_TXN_NOT_FOUND_RES_CODE = "ETI";
    
    public final static String  OCTANE_ACCESS_PASSWORD = "octane.access.password" ;
    public final static String  OCTANE_ACCESS_KEY = "octane.access.key" ;

	public static final Object EURONET_RESPONSE_CODE = 1;
	public final static String TRANSACTION_TYPE_DEBIT = "withdrawal";
	public final static String TRANSACTION_TYPE_CREDIT = "deposit";
	public final static String RECHARGE_TYPE = "_Recharge";
	public final static String HCOUPON_TYPE = "_HCoupon";
	public final static String DEBIT_TYPE = "_AdminDebit";
	public final static String AGGR_NAME_OXIGEN = "oxigen";
	public final static String AGGR_NAME_EURONET = "euronet";
	public final static String AGGR_NAME_SUVIDHAA = "suvidhaa";
	public static final String AGGR_NAME_LAPU = "Lapu";
	public final static String SUVIDHAA_RESPONSE_OBJ_MAP = "RechargeResInfo";
	public final static String SUVIDHAA_STATUSCHECK_RESPONSE_OBJ_MAP = "ArrayOfTransactionStatus";
	
	public final static String SUVIDHAA_REQUEST_TYPE = "type";
	public final static String SUVIDHAA_REQUEST_USER_NAME = "userName";
	public final static String SUVIDHAA_REQUEST_CLIENT_TXN_ID = "orderID";
	public final static String SUVIDHAA_REQUEST_MOBILE_NUMBER = "mobileNumber"; //IN CASE OF DTH WE ARE PASSING DTH NUIMBER IN THIS FIELD
	public final static String SUVIDHAA_REQUEST_SERVICE_PROVIDER_ID = "serviceProviderID";
	public final static String SUVIDHAA_REQUEST_TXN_AMOUNT = "amount";
	public final static String SUVIDHAA_REQUEST_TXN_DATE = "transactionDate";
	public final static String SUVIDHAA_REQUEST_TOKEN_KEY = "tokenkey";
	public final static String SUVIDHAA_REQUEST_TIME = "requestTime";
	public final static String SUVIDHAA_REQUEST_CIRCLE_ID = "circleID";
	public final static String SUVIDHAA_REQUEST_RECHARGE_TYPE = "rechargeType";
	public final static String SUVIDHAA_REQUEST_PRODUCT_CODE = "productCode"; // OPTIONAL DTH FIELD VALUE WE CAN PASS BLANK
	public final static String SUVIDHAA_REQUEST_SUBSCRIBER_ID = "subscriberId"; // MANDATORY FIELD FOR DTH RECHARGE THIS IS THE FIELD CONSIDER BY SUVIDHAA FOR DTH NUMBER RECHARGE
	public final static String RECHARGE_PLAN_TYPE = "rechargePlanType";
	public final static String RECHARGE_PLAN_TYPE_SPECIAL = "special";
	public final static String DEFAULT_RECHARGE_PLAN_TYPE_VALUE = "topup";
	public final static String RECHARGE_TYPE_FLEXI = "FLEXI";
	
	public final static String BSNL_DataCard = "BSNL DataCard";
	public final static String MTNL_DataCard_MUMBAI = "MTNL Datacard Mumbai";
	public final static String MTNL_DataCard_DELHI = "MTNL DataCard Delhi";
	public final static String Tata_Docomo_GSM_DataCard = "Tata Docomo GSM DataCard";
	public final static String T24_DataCard = "T24 DataCard";
	public final static String TATA_SKY_DTH = "Tata Sky";
	public final static String DISH_TV_DTH = "Dish TV";
	
	
	public final static String UNINOR_MOBILE_OPERATOR = "Uninor";
	public final static String VIDEOCON_MOBILE_OPERATOR = "Videocon Mobile";
	public final static String TATA_DOCOMO_GSM_MOBILE = "Tata Docomo GSM";
	public static final String AGGREGATOR_PREFERENCE = "aggregatorPreference";
	public static final String IS_ENABLED = "is_enabled";
	public static final String AG_TXN_RATIO = "aggregator_txn_ratio";
	public static final String OPERATOR_ID = "operator_id";
	
    public static final String INVALID_REQUEST = "1000";
    public static final String RECHARGE_API_DISABLED = "1001";
    public static final String LOOKUPID_GENERATION_FAIL = "1002";
    public static final String PAYMENT_ERROR = "1003";
    public static final String EXCEPTION = "1004";
    public static final String USER_NOT_LOGGED_IN = "1005";
    
    public static final int INSUFFICIENT_BALANCE = 100;
    public static final int RECHARGE_FAILURE = 101;
    public static final int RECHARGE_ATTEMPTED = 200;
    public static final long RECHARGE_RETRY_TIMEOUT_IN_MILLIS = 3600000;
    
    public static final int AIRTEL_OPERATOR_ID = 2;
    public static final int TELENOR_OPERATOR_ID = 13;

	public static enum StatusCheckType {
	    Aggressive, Relaxed
	}
	
	public static final String REQUEST_ID = "requestId";
	public static final String AG_REF_ID = "agRefId";
	public static final String RECHARGE_STATUS = "rechargeStatus";
	public static final String ADHOC_BILLER_TYPE = "0";
	public static final String FETCHBILL_BILLER_TYPE = "3";
	public static final Object BDK_AG = "billPaymentBillDeskClient";
	public static final String FETCH_BILL_FAILURE_GENERIC_ERROR_MSG = "Sorry. We were unable to fetch bill amount. Please try again after some time.";
	
	public static final int AUTO_PAY_MERCHANT_ID = 1;
	
	public static final Object EURONET_AG = "billPaymentEuronetClient";
	
	public static final String RECHARGE_CIRCLE_FOR_ALL = "33";
	public static final String PLAN_LEVEL_RETRY_BUCKET_S3 = "fc.gw.rechargeplan.qa.retry";
	public static final String JIO_DATACARD = "JIO Mifi";
	public static final Integer JIO_DATACARD_OP_ID = 92;
	public static final String JIO_OP_ID = "90";
	
	public static final Collection<String> RECHARGE_STATUS_AMBIGUOUS_RESPONSE_CODES = Collections
			.unmodifiableList(FCUtil.asList("US", "UP", "ES", "EF"));
	
	public static final Collection<String> RECHARGE_STATUS_AMBIGUOUS_RESPONSE_CODES_NEW = Collections
			.unmodifiableList(FCUtil.asList("US", "UP", "ES", "EF"));
	
	public static final Collection<String> EURO_ERROR_CODES = Collections
			.unmodifiableList(FCUtil.asList("AB","AI","CB","CI","EDT","EAI","EBO","ES","EF","EBT","ECI","ECL","ECP","ECR","EG","EI","EIC","EIE","EIM","EIP","EIU","EMI","EML","ERC","ESM","ESP","ESS","ETC","ETI","SC","SI","E05","E18","E19","E20","E21","E22","E23","E24","E25","E50"));
	
    public static final String UTM_SOURCE_TABLE="utm_details_order_mapping";
    public static final String UTM_ORDER_KEY="order_id";
    public static final String UTM_REF_KEY="utm_content";
    public static final String UTM_SOURCE_TYPE_KEY="utm_source";
    public static final String UTM_SOURCE_CHANNEL_ID="channel_id";
    public final static String  FF_RETRY_CRED = "FF007%fc&team" ;
    public final static String  BBPS_SERVICE_NAME = "bbps" ;
    public final static String  GOOGLE_RECHARGES_FAILURE_ERROR_CODE = "OPERATOR_ERROR";
	public static final String BDK_SOURCE_STRING="FRC";



}

