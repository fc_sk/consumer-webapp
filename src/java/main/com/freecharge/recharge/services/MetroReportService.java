package com.freecharge.recharge.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.util.TimeUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.MetroReportDAO;
import com.freecharge.app.domain.entity.MetroReportDataEntity;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.PaymentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.rest.model.MetroReportData;

@Service
public class MetroReportService {
	protected final Logger logger = LoggingFactory.getLogger(getClass()
			.getName());
	
	@Autowired
	private MetroReportDAO metroReportDao;
	
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private PgReadDAO pgReadDAO;
    
    @Autowired
    private PaymentService paymentService;

    
    private final static String SUCCESS = "Success";
    private final static String FAILURE = "Failure";
	public List<MetroReportData> searchMetroTransactionHistory(Date toDate,
			Date fromDate, String afcCardNo, Boolean paymentStatus,
			String orderId) {
		List<MetroReportDataEntity> metroReportDataEntityList = new ArrayList<>();
		try {
			metroReportDataEntityList = metroReportDao
					.searchMetroTransactionHistoryForDay(toDate, fromDate, paymentStatus, orderId);
		} catch (Exception e) {
			logger.error("Error searching metro transactions.", e);
		}
		
		List<MetroReportData> result = new ArrayList<>();
		String previousOrderId = null;
		MetroReportData previousMetroReportData = null;
        if(null != metroReportDataEntityList && !metroReportDataEntityList.isEmpty()) {
            for (MetroReportDataEntity metroReportDataEntity : metroReportDataEntityList) {
                // handle search for afc card no
                if (afcCardNo != null) {
                    if (!afcCardNo.equalsIgnoreCase(metroReportDataEntity.getAfcCardNumber())) {
                        continue;
                    }
                }
                UserProfile userProfile = userServiceProxy
                        .getUserProfile(metroReportDataEntity.getUserId());
                Users user = userServiceProxy.getUserByUserId(metroReportDataEntity
                        .getUserId());
                String mobileNumber = null;
                String firstName = null;
                String email = null;
                if (user != null) {
                    mobileNumber = user.getMobileNo();
                    email = user.getEmail();
                }
                if (userProfile != null)
                    firstName = userProfile.getFirstName();
                MetroReportData metroData = mapTransactionToMetroData(
                        metroReportDataEntity, mobileNumber, firstName, email);
                if( null != metroData.getPaymentStatus() && !metroData.getPaymentStatus().isEmpty() && metroData.getPaymentStatus().equals(SUCCESS)) {
                    setRefundDetails(metroData);
                }

                // Check if any of the payment txns are success and use that one in report.
                if (previousOrderId != null && previousMetroReportData != null) {
                    if (metroReportDataEntity.getOrderId().equals(previousOrderId)) {
                        if((null != metroReportDataEntity.getPaymentStatus() && !metroReportDataEntity.getPaymentStatus().isEmpty()) && metroReportDataEntity.getPaymentStatus().equalsIgnoreCase("1")) {
                            if (getPaymentType(metroReportDataEntity.getPaymentType()).equals("fcBalance")) {
                                continue;
                            }
                            previousMetroReportData.setPaymentStatus(SUCCESS);
                            previousMetroReportData.setPaymentTime(metroReportDataEntity.getPaymentTime());
                            previousMetroReportData.setPaymentType(getPaymentType(metroReportDataEntity.getPaymentType()));
                        }
                    } else {
                        // if previous order id is different add the new order id in list.
                        result.add(metroData);
                    }
                } else {
                    // first object in list being traversed.
                    result.add(metroData);
                }
                previousMetroReportData = metroData;
                previousOrderId = metroReportDataEntity.getOrderId();
            }
        }
		return result;
	}

	public Long getNumberOfPagesOfMetroData(Date toDate, Date fromDate) {
        Long dayDifference = (long)Math.ceil(TimeUtil.getDiffInMinutes(fromDate, toDate)/(60*24));
        dayDifference += 1;
        return dayDifference;
	}

	private void setRefundDetails(MetroReportData metroReportData)
    {
		try {
			List<PaymentRefundTransaction> refundedTransactions = pgReadDAO
					.getSuccessfulRefundsForMtxnId(metroReportData.getOrderId());
			logger.info("Refunded transactions for order: " + metroReportData.getOrderId() + " are: " + refundedTransactions);
			if ((null != refundedTransactions) && !refundedTransactions.isEmpty()) {
				logger.info("Successful refund for order ID: " + metroReportData.getOrderId());
				PaymentRefundTransaction lastRefundedTransaction = refundedTransactions.get(0);
				metroReportData.setRefundStatus(lastRefundedTransaction.getStatus().getStatusString());
				metroReportData.setRefundDate(lastRefundedTransaction.getReceivedFromPG());
			} else {
				logger.info("No refund for order ID: " + metroReportData.getOrderId());
				metroReportData.setRefundStatus("-");
				metroReportData.setRefundDate(null);
			}
		}
		catch (Exception e) {
			logger.error("Could not retrieve refund transactions for order: "+metroReportData.getOrderId());
			metroReportData.setRefundStatus("-");
			metroReportData.setRefundDate(null);
		}
    }

	private MetroReportData mapTransactionToMetroData(
			MetroReportDataEntity metroReportDataEntity, String mobileNumber,
			String firstName, String email) {
		MetroReportData metroData = new MetroReportData();

		metroData.setAfcCardNumber(metroReportDataEntity.getAfcCardNumber());
		metroData.setEmail(email);
		metroData.setOrderId(metroReportDataEntity.getOrderId());
		String paymentStatus = metroReportDataEntity.getPaymentStatus();
		String displayPaymentStatus = SUCCESS;
		if ((null == paymentStatus || paymentStatus.isEmpty()) || "0".equals(paymentStatus)){
			displayPaymentStatus = FAILURE;
		}
		metroData.setPaymentStatus(displayPaymentStatus);
		metroData.setPaymentTime(metroReportDataEntity.getPaymentTime());
		metroData.setPaymentType(getPaymentType(metroReportDataEntity
				.getPaymentType()));
		metroData.setPhoneNo(mobileNumber);
		metroData.setRechargeStatus(metroReportDataEntity.getRechargeStatus());
		metroData.setRechargeTime(metroReportDataEntity.getRechargeTime());
		metroData.setAmount(metroReportDataEntity.getAmount());
		metroData.setFirstName(firstName);
	  	String displayRechargeStatus = "Y";
	  	String rechargeStatus = metroReportDataEntity.getRechargeStatus();
    	if(rechargeStatus == null || rechargeStatus.equals("01")|| 
    			rechargeStatus.equals("02")||rechargeStatus.equals("03")||rechargeStatus.equals("04")||rechargeStatus.equals("05")) {
    		displayRechargeStatus = "N";
    	}
    	metroData.setRechargeStatus(displayRechargeStatus);
        return metroData;

	}

	private String getPaymentType(Integer paymentTypeCode) {
		return paymentService.getPaymentTypeString(paymentTypeCode);
	}
}
