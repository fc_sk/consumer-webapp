package com.freecharge.recharge.services;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class PrefixMappingCache extends RedisCacheManager {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private OperatorCircleReadDAO operatorCircleReadDAO;

    @Autowired
    private RedisTemplate<String, Object> prefixMappingCacheTemplate;

    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.prefixMappingCacheTemplate;
    }
    
    public List<Map<String, Object>> getFromCache(String prefix, String productMasterId) {
        final String cacheKey = getCacheKey(prefix, productMasterId);
        
        try {
            return (List<Map<String, Object>>) super.get(cacheKey);
        } catch (Exception e) {
            logger.error("Exception while getting key: " + cacheKey, e);
            return null;
        }
    }
    
    public void cacheOperatorCircleByPrefix(String prefix, String productMasterId, List<Map<String, Object>> prefixData) {
        final String cacheKey = getCacheKey(prefix, productMasterId);

        try {
            super.set(cacheKey, prefixData);
        } catch (Exception e) {
            logger.error("Exception while setting key: " + cacheKey, e);
        }
    }
    
    private String getCacheKey(String prefix, String productMasterId) {
        return productMasterId + "_" + prefix;
    }

      
}
