package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NoHttpResponseException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

import COM.rsa.jsafe.bl;

import com.freecharge.app.domain.dao.ICustomRechargePlansTxnTrackerReadDAO;
import com.freecharge.app.domain.entity.CustomRechargePlanTxnTracker;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdesk.RawBillValidationAPIResponse;
import com.freecharge.google.GoogleCreditsService;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.businessdao.AggregatorInterface.AggregatorName;
import com.freecharge.recharge.collector.RechargeMetricService;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.fulfillment.type.ScheduleTriggerEvent;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.RechargeAttemptSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.sns.bean.RechargeAttemptBean;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.mongodb.util.Hash;

@Service
public class CommonRechargeService {

       private static final String OPERATOR_RETRY_WAIT_TIME = "OperatorRetryWaitTime";

    private static Logger logger = LoggingFactory.getLogger(CommonRechargeService.class);

       private Map<String, Integer> BlockedOperators = ImmutableMap.of("Vodafone", 5 * 60, "Aircel", 5 * 60, "Idea",
                     8 * 60);

       @Autowired
       private InService inService;

       @Autowired
       private OperatorCircleService operatorCircleService;

       @Autowired
       private AggregatorFactory aggregatorFactory;

       @Autowired
       private FCProperties fcProperties;

       @Autowired
       private AppConfigService appConfig;

       @Autowired
       protected MetricsClient metricsClient;

       @Autowired
       private RechargeMetricService metricService;

       @Autowired
       private OperatorDownService operatorDownService;
       
       @Autowired
       private RechargeAttemptSNSService rechargeAttemptSNSService;
       
       @Autowired
       private GoogleCreditsService googleCreditsService;
       
       @Autowired
       private ICustomRechargePlansTxnTrackerReadDAO customRechargePlansTxnTrackerReadDAO;

       private final String BLOCKED_OPERATORS = "BlockedOperators";
    
    private static class RetryBlockConfig {
        public final String operator;
        public final boolean isRetryBlocked;
        public final boolean isScheduleEnabled;
        public final boolean isSameAmountBlocked;
        public final long blockTimeSeconds;
        public final long blockPercent;
        
        public RetryBlockConfig(String operator, boolean isRetryBlocked, boolean isScheduleEnabled, boolean isSameAmountBlocked, long blockTimeSeconds, long blockPercent) {
            this.operator = operator;
            this.isRetryBlocked = isRetryBlocked;
            this.isScheduleEnabled = isScheduleEnabled;
            this.blockTimeSeconds = blockTimeSeconds;
            this.blockPercent = blockPercent;
            this.isSameAmountBlocked = isSameAmountBlocked;
        }
    }
    
    public InResponse getRecharge(RechargeDo rechargeDo, InResponse inResponseFromRequest, InOprAggrPriority priority) {
        InResponse inResponse = null;
        logger.info("[IN MODERATOR] Processing Recharge operation "+ rechargeDo.getAffiliateTransId());
        List<InTransactions> inTransactionsList = new ArrayList<InTransactions>();

        String aggrpriorityvalue = null;
        InErrorcodeMap inErrorCodeMap = null;

        AggregatorResponseDo gCreditsResponseDo = null;
        if (priority != null) {
            aggrpriorityvalue = priority.getAggrPriority();
        } else {
            priority = operatorCircleService.getInOprAggrPriority(rechargeDo.getCircleOperatorMapping()
                    .getCircleOperatorMappingId());
        }
        logger.info("priority for order id " + rechargeDo.getAffiliateTransId() + " is " + aggrpriorityvalue);
        CustomRechargePlanTxnTracker customRechargePlanTxnTracker = null;
        try {
        	customRechargePlanTxnTracker = customRechargePlansTxnTrackerReadDAO.findByLookupId(rechargeDo.getLookupId());
        	if(customRechargePlanTxnTracker != null) {
        		logger.info(String.format("Found entry in custom_recharge_plans_txn_track table for lookup id %s and order id %s", rechargeDo.getLookupId(), rechargeDo.getAffiliateTransId()));
        	}
        }catch (Exception e) {
			logger.error(String.format("Unable to read from CustomRechargePlanTxnTracker for lookup id %s and order id %s because of exception : ", rechargeDo.getLookupId(), rechargeDo.getAffiliateTransId()), e);
		}
        if (priority != null)
            aggrpriorityvalue = priority.getAggrPriority();
        if (aggrpriorityvalue != null && !aggrpriorityvalue.equalsIgnoreCase("")) {
            StringTokenizer aggrprioritytokens = new StringTokenizer(aggrpriorityvalue, ",");
            long strTime = 0;

            Calendar cal1 = Calendar.getInstance();
            Date dt1 = cal1.getTime();
            
            while (aggrprioritytokens.hasMoreElements()) {
                StringBuffer parametersPassed = new StringBuffer();
                AggregatorResponseDo aggregatorResponseDo = null;
                String aggregatorName = aggrprioritytokens.nextElement().toString().toLowerCase();
                if (rechargeDo.getAggregatorsToSkip().contains(aggregatorName)) {
                    logger.info("Skipping aggregator : " + aggregatorName + "for Order Id:"
                            + rechargeDo.getAffiliateTransId());
                    continue;
                }
                if(customRechargePlanTxnTracker != null && StringUtils.isNotEmpty(customRechargePlanTxnTracker.getFulfillmentIdf()) 
                		&& !isFreshRechargeCall(rechargeDo.getAffiliateTransId())
                		&& !aggregatorName.equalsIgnoreCase(RechargeConstants.AGGR_NAME_EURONET)) {
                	/*
                	 * If there is any entry related to the lookup id in 
                	 * custom_recharge_plan_tracker table and the aggregator is n't euronet and recharge is
                	 * being retried (for any inResponse the recharge is retried ; not the fresh recharge call-first ever)
                	 * then we will skip the recharge try for any other operator since the
                	 * custom recharge plans are provided by euronet only at the time implementation
                	 * */
                	logger.info(String.format("Skipping aggregator %s for order with lookup id %s because the recharge plan is specifically provided by %s agrregator only.", aggregatorName, rechargeDo.getLookupId(), RechargeConstants.AGGR_NAME_EURONET));
                	continue;
                }
                if (fcProperties.shouldMockAG()) {
                    logger.info("[MOCK] For orderid: " + rechargeDo.getInRequestId() + ", operator priority was: "
                            + aggregatorName + " - changing to: dummy_" + aggregatorName);
                    aggregatorName = "dummy_" + aggregatorName;
                }
                
                if (!appConfig.isLapuEnabled() && aggregatorName.equalsIgnoreCase(RechargeConstants.AGGR_NAME_LAPU)) {
                    continue;
                }

                logger.info("[IN MODERATOR] Processing getRecharge from operator id : "
                        + rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId() + ",circle id : "
                        + rechargeDo.getCircleOperatorMapping().getFkCircleMasterId() + " CircleOperatorMappingId "
                        + rechargeDo.getCircleOperatorMapping().getCircleOperatorMappingId());
                List<InAggrOprCircleMap> inAggrOprCircleMapsList = operatorCircleService.getInAggrOprCircleMap(
                        rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId(), rechargeDo
                                .getCircleOperatorMapping().getFkCircleMasterId());

                logger.info("[IN MODERATOR] Processing getRecharge from " + aggregatorName, null);
                AggregatorInterface aggregatorInterface = aggregatorFactory.getAggregator(aggregatorName);
                try {

                    // Generating order id to send to IN
                    String inOrderId = rechargeDo.getInRequestId().toString();
                    // The prefixing is done because if the transaction id gets
                    // repeated, then oxigen just sends the response of the
                    // previously done recharge.
                    // this property will be set in local.properties file of
                    // each qa/dev host and will have to be unique for that
                    // host. Also, when the db
                    // is bootstrapped this property will have to be reset.
                    inOrderId = fcProperties.getInTransIdPrefix() + rechargeDo.getRechargeRetryCounter() + "_"
                            + inOrderId;
                    notifyRechargeAttempt(RechargeUtil.getAggregatorName(aggregatorName), inOrderId, rechargeDo);
                    logger.info("[IN MODERATOR] Processing Recharge operation[Generated request id = " + inOrderId
                            + "] ", null);
                    strTime = Calendar.getInstance().getTimeInMillis();
                    aggregatorResponseDo = aggregatorInterface.doRecharge(rechargeDo, inOrderId, parametersPassed,
                            inAggrOprCircleMapsList);
                    //Added so that needed it globally and object not declared globally coz of modification issues
                    gCreditsResponseDo = aggregatorResponseDo;
                } catch (HttpException httpException) {
                    logger.error("http exception in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", httpException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "HttpException",
                            httpException.getMessage(), dt1, parametersPassed);
                } catch (ConnectException connectException) {
                    logger.error("connect exception in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", connectException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "ConnectException",
                            connectException.getMessage(), dt1, parametersPassed);
                } catch (NetworkConnectTimeoutException ncte) {
                    logger.error("connect time out exception error in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", ncte);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "NetworkConnectTimeoutException",
                            ncte.getMessage(), dt1, parametersPassed);
                } catch (ConnectTimeoutException connectTimeoutException) {
                    logger.error("connect time out exception error in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", connectTimeoutException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "ConnectTimeoutException",
                            connectTimeoutException.getMessage(), dt1, parametersPassed);
                } catch (SocketException socketException) {
                    logger.error("[IN MODERATOR] socket exception Error in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", socketException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "SocketException",
                            socketException.getMessage(), dt1, parametersPassed);
                } catch (SocketTimeoutException socketTimeoutException) {
                    logger.error(
                            "[IN MODERATOR] socket time out exception error in Processing getRecharge for order id "
                                    + rechargeDo.getInRequestId().toString() + " ", socketTimeoutException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "SocketTimeoutException",
                            socketTimeoutException.getMessage(), dt1, parametersPassed);
                } catch (NoHttpResponseException socketTimeoutException) {
                    logger.warn(
                            "[IN MODERATOR] socket time out exception error in Processing getRecharge for order id "
                                    + rechargeDo.getInRequestId().toString() + " ", socketTimeoutException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "SocketTimeoutException",
                            socketTimeoutException.getMessage(), dt1, parametersPassed);
                } catch (IOException iOException) {
                    logger.warn("[IN MODERATOR] ioexception in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString());
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "IOException",
                            iOException.getMessage(), dt1, parametersPassed);
                } catch (Exception generalException) {
                    logger.error("[IN MODERATOR] general error in Processing getRecharge for order id "
                            + rechargeDo.getInRequestId().toString() + " ", generalException);
                    aggregatorResponseDo = getResponseBody(rechargeDo, aggregatorName, "Exception",
                            generalException.getMessage(), dt1, parametersPassed);
                } finally {
                    long totalRequestProcessTime = (Calendar.getInstance().getTimeInMillis() - strTime) / 1000;
                    logger.info("total request process time in seconds " + totalRequestProcessTime + "for order id "
                            + rechargeDo.getAffiliateTransId() + " for operator " + rechargeDo.getOperatorCode());
                    InTransactions inTransactions = getTransactoinObject(aggregatorResponseDo, rechargeDo,
                            totalRequestProcessTime);
                    inTransactionsList.add(inTransactions);
                    inService.create(inTransactions);
                    inErrorCodeMap = getInErrorCodeMapping(aggregatorResponseDo);
                    if(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS.equals(aggregatorResponseDo.getTransactionStatus())) {
                    	try {
                    		OperatorMaster operatorMaster = operatorCircleService.getOperator(rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId());
                            CircleMaster circleMaster = operatorCircleService.getCircle(rechargeDo.getCircleOperatorMapping().getFkCircleMasterId());
                            metricsClient.recordEvent("aggegator_circle", operatorMaster.getName() + "-" + aggregatorName + "-" + circleMaster.getName(), RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
                            metricsClient.recordEvent("aggegator_up", aggregatorName + "." + operatorMaster.getName(), RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
						} catch (Exception e) {
							logger.error("Exception caught while publishing under-process metrics for orderId: " + rechargeDo.getAffiliateTransId(), e);
						}
                    }
                    else if(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL.equals(aggregatorResponseDo.getTransactionStatus()) || RechargeConstants.TRANSACTION_STATUS_FAILED.equals(aggregatorResponseDo.getTransactionStatus())) {
                        try {
                            OperatorMaster operatorMaster = operatorCircleService.getOperator(rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId());
                            metricsClient.recordEvent("aggegator_unsuccessful", aggregatorName + "." + operatorMaster.getName(), RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                        } catch (Exception e) {
                            logger.error("Exception caught while publishing unsuccessful metrics for orderId: " + rechargeDo.getAffiliateTransId(), e);
                        }
                    }
                    
                }
                if (inErrorCodeMap != null && inErrorCodeMap.getIsRetryable() == false) {
                    break;
                }
                if (aggregatorResponseDo != null
                        && ((RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE).equalsIgnoreCase(aggregatorResponseDo
                                .getTransactionStatus()))) {
                    break;
                }
            } // end while
        }

        if (inTransactionsList.size() > 0) {
            inResponse = this.updateResponse(inTransactionsList.get(inTransactionsList.size() - 1), rechargeDo);
            //save google credits in cache
			if (RechargeConstants.GOOGLE_CREDITS_PRODUCT_TYPE.equalsIgnoreCase(rechargeDo.getProductType())) {
				saveGoogleCreditsCache(rechargeDo, gCreditsResponseDo);
			}
            
        }
        return inResponse;
    }
    
    private boolean isFreshRechargeCall(String orderID) {
    	List<InResponse> inResponses = inService.getAllInResponseForOrderID(orderID);
    	if(inResponses != null) {
    		for(InResponse inResponse : inResponses) {
        		if(inResponse.getRetryNumber() != null && inResponse.getRetryNumber() > 0) {
        			return false;
        		}
        	}
    	}
    	return true;
    }

    private void notifyRechargeAttempt(String aggregatorName, String inOrderId, RechargeDo rechargeDo) {
        try {
            // Sending Recharge Attempt notifications for recon
            RechargeAttemptBean rechargeAttemptBean = getRechargeAttemptBean(aggregatorName, inOrderId,
                    rechargeDo);
            rechargeAttemptSNSService.publish(rechargeDo.getAffiliateTransId(), "{\"message\" : "
                    + RechargeAttemptBean.getJsonString(rechargeAttemptBean) + "}");
        } catch (Exception e) {
            logger.error("Caught some exception pushing recharge attempt notification for orderId " + rechargeDo.getAffiliateTransId(), e);
        }
    }
    
    private RechargeAttemptBean getRechargeAttemptBean(String aggregatorName, String inOrderId, RechargeDo rechargeDo) {
        RechargeAttemptBean rechargeAttemptBean = new RechargeAttemptBean();
        rechargeAttemptBean.setAg(aggregatorName);
        rechargeAttemptBean.setAgRequestId(inOrderId);
        rechargeAttemptBean.setAmount(rechargeDo.getAmount());
        rechargeAttemptBean.setOrderId(rechargeDo.getAffiliateTransId());
        rechargeAttemptBean.setProduct(ProductMaster.ProductName.fromCode(rechargeDo.getProductType())
                .getLabel());
        rechargeAttemptBean.setServiceNumber(rechargeDo.getMobileNo());
        return rechargeAttemptBean;
    }


    public boolean isSameAmountRetryBlocked(String operatorName) {
        RetryBlockConfig retryConfig = getRetryBlockConfig(operatorName);
        
        if (retryConfig != null) {
            if (retryConfig.isSameAmountBlocked) {
                return true;
            }
        }
        
        return false;
    }
    

       private AggregatorResponseDo getResponseBody(RechargeDo rechargeDo, String aggregatorName, String errorType,
                     String exMessage, Date startTime, StringBuffer parametersPassed) {
              AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

              /* Getting Response Time */
              Calendar cal = Calendar.getInstance();
              Date dt = cal.getTime();

              aggregatorResponseDo.setAggrName(aggregatorName);
              aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
              aggregatorResponseDo.setRequestTime(startTime);
              aggregatorResponseDo.setResponseTime(dt);
              aggregatorResponseDo.setRawRequest(parametersPassed.toString());
              aggregatorResponseDo.setRawResponse(exMessage);
              aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);

              if (errorType.equalsIgnoreCase("HttpException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_HE");
                     aggregatorResponseDo.setAggrResponseMessage("Connection Error");
              } else if (errorType.equalsIgnoreCase("ConnectException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_CE");
                     aggregatorResponseDo.setAggrResponseMessage("Connect Exception");
              } else if (errorType.equalsIgnoreCase("ConnectTimeoutException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_CTE");
                     aggregatorResponseDo.setAggrResponseMessage("Connection Time Out");
              } else if (errorType.equalsIgnoreCase("SocketException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_SE");
                     aggregatorResponseDo.setAggrResponseMessage("Socket Exception");
              } else if (errorType.equalsIgnoreCase("SocketTimeoutException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_STE");
                     aggregatorResponseDo.setAggrResponseMessage("SocketTimeoutException");
              } else if (errorType.equalsIgnoreCase("NoHttpResponseException")) {
                     aggregatorResponseDo.setAggrResponseCode("NO_HRE");
                     aggregatorResponseDo.setAggrResponseMessage("NoHttpResponse Exception");
              } else if (errorType.equalsIgnoreCase("IOException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_IOE");
                     aggregatorResponseDo.setAggrResponseMessage("IO Exception");
              } else if (errorType.equals("NetworkConnectTimeoutException")) {
                     aggregatorResponseDo.setAggrResponseCode("IN_NCTE");
                     aggregatorResponseDo.setAggrResponseMessage("NetworkConnectTimeout Exception");
              } else {
                     aggregatorResponseDo.setAggrResponseCode("IN_E");
                     aggregatorResponseDo.setAggrResponseMessage("General Error");
              }

              return aggregatorResponseDo;
       }

       private InTransactions getTransactoinObject(AggregatorResponseDo aggregatorResponseDo, RechargeDo rechargeDo,
                     Long requestResponseTime) {
              logger.info("[IN MODERATOR] Inserting values in InTransaction Table  (insertResponse) ", null);

              InTransactions rechargeTransaction = new InTransactions();
              try {
                     logger.info("[IN MODERATOR] aggregatorResponseDo.getAggrName():"
                                   + aggregatorResponseDo.getAggrName(), null);
                     rechargeTransaction.setAggrName(aggregatorResponseDo.getAggrName());
                     rechargeTransaction.setAggrOprRefernceNo(aggregatorResponseDo.getAggrOprRefernceNo());
                     rechargeTransaction.setAggrReferenceNo(aggregatorResponseDo.getAggrReferenceNo());
                     rechargeTransaction.setAggrResponseCode(aggregatorResponseDo.getAggrResponseCode());
                     rechargeTransaction.setAggrResponseMessage(aggregatorResponseDo.getAggrResponseMessage());
                     rechargeTransaction.setFkRequestId(aggregatorResponseDo.getRequestId());
                     rechargeTransaction.setRawRequest(aggregatorResponseDo.getRawRequest());
                     rechargeTransaction.setRequestTime(new Timestamp(aggregatorResponseDo.getRequestTime().getTime()));

                     if (aggregatorResponseDo.getResponseTime() == null) {
                            rechargeTransaction.setResponseTime(new Timestamp(aggregatorResponseDo.getRequestTime()
                                          .getTime()));
                     } else {
                            rechargeTransaction.setResponseTime(new Timestamp(aggregatorResponseDo.getResponseTime()
                                          .getTime()));
                     }

                     rechargeTransaction.setRawResponse(aggregatorResponseDo.getRawResponse());
                     rechargeTransaction.setTransactionStatus(aggregatorResponseDo.getTransactionStatus());
                     rechargeTransaction.setRequestProcessTime(requestResponseTime);
              } catch (Exception e) {
                     logger.error("[IN MODERATOR] Error in Insertion of values in InTransaction Table (insertResponse) ",
                                   e);
              }
              return rechargeTransaction;
       }

       private InResponse updateResponse(InTransactions inTransactions, RechargeDo rechargeDo) {
              logger.info("[IN MODERATOR] Updating values in DB (updateResponse) requestId : "
                            + rechargeDo.getInRequestId(), null);
              InResponse inResponse = inService.getInResponseByRequestId(rechargeDo.getInRequestId());
              try {
                     inResponse.setAggrName(inTransactions.getAggrName());
                     inResponse.setAggrRespCode(inTransactions.getAggrResponseCode());
                     inResponse.setAggrRespMsg(inTransactions.getAggrResponseMessage());
                     inResponse.setAggrTransId(inTransactions.getAggrReferenceNo());
                     inResponse.setRawRequest(inTransactions.getRawRequest());
                     if (inTransactions.getRawResponse() != null) {
                            inResponse.setRawResponse(inTransactions.getRawResponse());
                     } else {
                            inResponse.setRawResponse("No raw_response present in in_transaction table");
                     }
                     inResponse.setOprTransId(inTransactions.getAggrOprRefernceNo());
                     inResponse.setRetryNumber(rechargeDo.getRechargeRetryCounter());
                     inResponse.setCreatedTime(inTransactions.getRequestTime());
                     inResponse.setUpdatedTime(inTransactions.getResponseTime());

                     List<InErrorcodeMap> inErrorCodeMapList = inService.getErrorCodeMap(
                                   inTransactions.getAggrResponseCode(), inTransactions.getAggrName());

                     String inRespCode = null;
                     String inRespMsg = null;
                     if (inErrorCodeMapList != null && inErrorCodeMapList.size() > 0) {
                            inRespCode = inErrorCodeMapList.get(0).getInRespCode();
                            inRespMsg = inErrorCodeMapList.get(0).getInErrorMsg();
                     } else {
                            inRespCode = "29";
                            inRespMsg = "This is because of a technical error at operator's end.";
                            metricsClient.recordEvent("Recharge", inTransactions.getAggrName(), "UnknownCode."
                                          + inTransactions.getAggrResponseCode());
                     }

                     inResponse.setInRespCode(inRespCode);
                     if (isUnderProcess(inResponse, inRespCode)) {
                            logger.info("response code 0 without aggregator transaction id will be treated as transaction under process"
                                          + inResponse.getRequestId());
                            inResponse.setInRespCode(RechargeConstants.IN_UNDER_PROCESS_CODE);
                     }
                     inResponse.setInRespMsg(inRespMsg);
                     logger.info("[IN MODERATOR] Updating values in DB (updateResponse) :InRespCode=" + inRespCode
                                   + " &InRespMsg = " + inRespMsg, null);

                     inResponse = inService.update(inResponse);
              } catch (Exception e) {
                     logger.error("[IN MODERATOR] Error in Updation of values in DB (updateResponse) ", e);
              }
              return inResponse;
       }

       private boolean isUnderProcess(InResponse inResponse, String inRespCode) {
              return inRespCode != null && inRespCode.equalsIgnoreCase(RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE)
                            && inResponse.getAggrName().equalsIgnoreCase("oxigen")
                            && (inResponse.getAggrTransId() == null || inResponse.getAggrTransId().isEmpty());
       }

       private InErrorcodeMap getInErrorCodeMapping(AggregatorResponseDo aggregatorResponseDo) {
              List<InErrorcodeMap> inErrorcodeMap = inService.getErrorCodeMap(
                            aggregatorResponseDo.getAggrResponseCode(), aggregatorResponseDo.getAggrName());

              if (inErrorcodeMap != null && inErrorcodeMap.size() > 0) {
                     return inErrorcodeMap.get(0);
              }
              return null;
       }

       public Long getSuccessBlockSeconds(String operatorName) {
              RetryBlockConfig blockConfig = getRetryBlockConfig(operatorName);

              if (blockConfig != null) {
                     return blockConfig.blockTimeSeconds;
              }

              return 0l;
       }

       public boolean isRetryAttemptBlocked(String operatorName) {
              RetryBlockConfig retryConfig = getRetryBlockConfig(operatorName);

              if (retryConfig != null) {
                     if (retryConfig.isRetryBlocked) {
                            return true;
                     }

                     if (retryConfig.blockPercent >= 100) {
                            return true;
                     }

                     final int randomSampleInt = RandomUtils.nextInt(9999);

                     if (randomSampleInt < retryConfig.blockPercent * 100) {
                            return true;
                     }
              }

              return false;
       }

       public boolean isScheduleEnabled(String operatorName) {
              RetryBlockConfig retryConfig = getRetryBlockConfig(operatorName);

              if (retryConfig != null) {
                     if (retryConfig.isScheduleEnabled) {
                            return true;
                     }
              }

              return false;
       }

       public Long getFailureBlockSeconds(String operatorName) {
              if (isFailureRetryAttemptBlocked(operatorName)) {
                     return getFailureRetryBlockOperators().get(operatorName).longValue() * 60;
              } else {
                     return 0l;
              }
       }

       public boolean isFailureRetryAttemptBlocked(String operatorName) {
              return getFailureRetryBlockOperators().containsKey(operatorName);
       }

       public boolean isOperatorDownBlockEnabled(Integer operatorId) {

           boolean operatorBlockEnabled = appConfig.isOperatorAlertBlockEnabled();
           JSONObject configJson = appConfig.getCustomProp(appConfig.OPERATOR_DOWN_CONFIG);

           if (configJson == null) {
               return operatorBlockEnabled;
           }

           String enabledString = (String) configJson.get("enabled");
           if (StringUtils.isNotBlank(enabledString)) {
               if (!Boolean.parseBoolean(enabledString)) {
                   return operatorBlockEnabled;
               }
           }

           String maxCountString = (String) configJson.get("maxCount");
           if (StringUtils.isNotBlank(maxCountString)) {
               int maxCount = Integer.parseInt(maxCountString);

               long currCount = operatorDownService.getOperatorQueueCount(operatorId);

               if (maxCount <= currCount) {
                   logger.info("Operator queue count exceeded the limit " +  maxCount +
                           " for operator id" + operatorId + ", blocking the operator");
                   return true;
               }
               logger.info("Operator queue count = " +  currCount +
                       " for operator id" + operatorId + " max count = " + maxCount);
               return false;
           }
           return operatorBlockEnabled;
       }

       private RetryBlockConfig getRetryBlockConfig(String operator) {
              List<RetryBlockConfig> retryConfigList = getRetryBlockConfig();

              for (RetryBlockConfig retryConfig : retryConfigList) {
                     if (StringUtils.equals(operator, retryConfig.operator)) {
                            return retryConfig;
                     }
              }

              return null;
       }

       private List<RetryBlockConfig> getRetryBlockConfig() {
              List<RetryBlockConfig> returnList = new ArrayList<>();
              
              Gson gson = new Gson();

              JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);
              
              if (configJson != null) {
                  String blockConfigJson = (String) configJson.get(BLOCKED_OPERATORS);
                  
                  if (!StringUtils.isBlank(blockConfigJson)) {
                      List<Map<String, Object>> blockList = gson.fromJson(blockConfigJson, List.class);
                      
                      for (Map<String, Object> blockedOperator : blockList) {
                          String operator  = (String) blockedOperator.get("bo");

                          Double blockTimeSeconds = (Double) blockedOperator.get("btm");
                          Double blockPercent = (Double) blockedOperator.get("bp");
                          
                          if (blockPercent == null) {
                              blockPercent = 100d;
                          }
                          
                          Boolean isAmountBlockEnabled = (Boolean) blockedOperator.get("isabe");
                          if (isAmountBlockEnabled == null) {
                              isAmountBlockEnabled = false;
                          }
                          
                          Boolean isRetryBlocked = (Boolean) blockedOperator.get("irb");
                          Boolean isScheduleEnabled = (Boolean) blockedOperator.get("ise");
                          
                          returnList.add(new RetryBlockConfig(operator, isRetryBlocked, isScheduleEnabled, isAmountBlockEnabled, blockTimeSeconds.longValue(), blockPercent.longValue()));
                      }
                  }
                  
              }
              return returnList;
          }
       
    public Integer getOperatorRetryBlockTime() {
        Integer blockTime = null;
        try {
            JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);
            if (configJson != null) {
                blockTime = Integer.parseInt((String) configJson.get(OPERATOR_RETRY_WAIT_TIME));
            }
        } catch (RuntimeException e) {
            logger.error("Exception in trying to getch the operator retry schedule time", e);
        }
        return blockTime == null ? 120 : blockTime;
    }
       
       
       private Map<String, Double> getFailureRetryBlockOperators() {
              Gson gson = new Gson();

              Map<String, Double> blockedOperators = new HashMap<>();

              JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_FAILURE_CONFIG);

              if (configJson != null) {
                     String blockConfigJson = (String) configJson.get(BLOCKED_OPERATORS);

                     if (!StringUtils.isBlank(blockConfigJson)) {
                            List<Map<String, Object>> blockList = gson.fromJson(blockConfigJson, List.class);

                            for (Map<String, Object> blockedOperator : blockList) {
                                   String operator = (String) blockedOperator.get("bo");
                                   Double blockTimeMinutes = (Double) blockedOperator.get("btm");

                                   blockedOperators.put(operator, blockTimeMinutes);
                            }
                     }

              }

              return blockedOperators;
       }

       private String aggregatorPreferenceName(Integer operatorId, String aggrPriorityValue, String orderId) {
              String aggPercentageValue = null;
              int count = 0;
              int txn_count = 0;
              String key = null;
              String aggName = null;
              try {
                     SqlRowSet srs = (SqlRowSet) inService.fetchAgPreferenceByOperatorId(operatorId);
                     if (srs != null && srs.next() && srs.getBoolean(RechargeConstants.IS_ENABLED)) {
                            String aggregatorPreference = srs.getString(RechargeConstants.AG_TXN_RATIO);
                            String[] aggPercentageObj = aggregatorPreference==null?null: aggregatorPreference.split("~");
                            if (aggPercentageObj != null && aggPercentageObj.length > 0) {

                                   for (int agPercentageIndexValue = 0; agPercentageIndexValue < aggPercentageObj.length; agPercentageIndexValue++) {
                                          aggName = (aggPercentageObj[agPercentageIndexValue].split("-"))[0];
                                          key = aggName + "_" + operatorId + "_"+ RechargeConstants.AGGREGATOR_PREFERENCE;
                                          aggPercentageValue = (aggPercentageObj[agPercentageIndexValue].split("-"))[1];
                                          if (Integer.parseInt(aggPercentageValue) <= 0) {
                                                 continue;
                                          } else {
                                                 Object object = metricService.getAggregatorPreferenceValue(key);
                                                 if (object != null) {
                                                        txn_count = ((Integer) object);
                                                        if (txn_count < Integer.parseInt(aggPercentageValue)) {
                                                               txn_count = txn_count + 1;
                                                               Object value = (Integer) txn_count;
                                                               metricService.setAggregatorPreferenceValue(key, value);
                                                               count = 1;
                                                               break;
                                                        } else {
                                                               continue;
                                                        }
                                                 } else {
                                                        txn_count = txn_count + 1;
                                                        metricService.setAggregatorPreferenceValue(key, txn_count);
                                                        count = 1;
                                                        break;
                                                 }
                                          }
                                   }
                                   if (count == 0) {
                                          for (int i = 0; i < aggPercentageObj.length; i++) {
                                                 aggName = (aggPercentageObj[i].split("-"))[0];
                                                 metricService.deleteAggregatorPreferencekey(key);
                                                 aggName = null;
                                          }
                                   }
                            }
                     }

                     if (aggName != null && aggrPriorityValue.contains(aggName)) {
                            return aggrPriorityValue = changeAggregatorPriority(aggrPriorityValue, aggName);
                     }
              } catch (Exception e) {
                     logger.error("exception raised while fetching aggregator transaction split ratio" + " for order id " + orderId, e);
              }
              return aggrPriorityValue;
       }

       private String changeAggregatorPriority(String aggPriority, String aggName) {
              String[] aggPriorityArrayObj = aggPriority.split(",");
              StringBuffer sb = new StringBuffer(aggName);
              for (String aggValue : aggPriorityArrayObj) {
                     if (!aggValue.equals(aggName)) {
                            sb.append("," + aggValue);
                     }
              }
              return sb.toString();
       }
       
       
       
	private void saveGoogleCreditsCache(RechargeDo rechargeDo, AggregatorResponseDo responseDo) {
		logger.info("Inside save credits in cache method for orderId : " + rechargeDo.getAffiliateTransId());
		try {
			if (!StringUtils.isEmpty(responseDo.getGoogleCode())
					&& !StringUtils.isEmpty(responseDo.getGoogleSerial())) {
				logger.info("Google Serial and Google code present.Going ahead for caching");
				googleCreditsService.saveGoogleCreditsCache(rechargeDo.getAffiliateTransId(), responseDo.getGoogleSerial(), responseDo.getGoogleCode());
			}
		} catch (Exception e) {
			logger.error("An error occurred trying to save the credits in cache for order id : "
					+ rechargeDo.getAffiliateTransId(), e);
		}

	}
}
