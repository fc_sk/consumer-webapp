package com.freecharge.recharge.services;

import java.util.concurrent.Callable;

import org.tempuri.IEPayServiceProxy;

public class EuronetHttpMethodExecutorTask implements Callable<String> {
    private final IEPayServiceProxy euronetClient;
    private final String request;
    
    public EuronetHttpMethodExecutorTask(IEPayServiceProxy client, String request) {
        this.euronetClient = client;
        this.request = request;
    }

    @Override
    public String call() throws Exception {
        return euronetClient.sendEuroVasRequest(request);
    }

}
