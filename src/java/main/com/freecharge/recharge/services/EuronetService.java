package com.freecharge.recharge.services;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.timeout.TimeoutKey;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.LoggingConstants;
import com.freecharge.freebill.common.BillPaymentVerificationAPIResponse;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.EuroDTHValidationResponseDo;
import com.freecharge.recharge.businessDo.EuroValidationResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.CallSourceType;
import com.freecharge.recharge.util.PlanValidationCommandBuilder;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.recharge.LogRequestService;
import com.google.common.base.Strings;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.tempuri.IEPayServiceProxy;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: May 8, 2012 Time: 11:18:22 AM
 * To change this template use File | Settings | File Templates.
 */

public class EuronetService extends AggregatorService implements AggregatorInterface {

	public static final String SPCODE = "spcode";
	public static final String SSPCODE = "sspcode";

	@Autowired
	private AppConfigService appConfig;

	@Autowired
	private OperatorCircleService operatorCircleService;

	@Autowired
	private BillPaymentService billPayService;

	@Autowired
	private ExternalClientCallService externalClientCallService;

	@Autowired
	private EuronetHttpsHostConfig euronetHttpsHostConfig;

	@Autowired
	private LogRequestService logRequestService;
	
	public static final String EURONET_BALANCE_KEY = "EURONET_BALANCE_KEY";
	public static final String EURONET_BALANCE_LAST_UPDATE_TIME_KEY = "EURONET_BALANCE_LAST_UPDATE_TIME_KEY";

	private static Logger logger = LoggingFactory.getLogger(EuronetService.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	public static final String RESPONSE_CODE_PARAM = "responsecode";
	public static final String MERCHANT_REFERENCE_NO_PARAM = "merchantrefno";
	public static final String EURONET_REFERENCE_NO_PARAM = "enrefno";
	public static final String RESPONSE_MESSAGE_PARAM = "responsemessage";
	public static final String GOOGLE_SERIAL = "cardserial";
	public static final String GOOGLE_PIN = "cardpin";

	private static final String JIO_SPECIAL_SERVICE_CODE = "DR";

	private String euromerchantcode;
	private String eurousername;
	private String eurouserpass;
	private String euroPostpaidUserName;
	private String euroPostpaidPassword;
	private String europaymentprovider;
	private String europaymentmode;
	private String eurochannelcode;
	private String euroservicecode;
	private String europpservicecode;
	private String eurodthservicecode;
	private String euroOptional1;
	private String eurostorecode;
	private String euroterminalcode;
	private String eurocustomerid;
	private String eurotcflag;
	private String euroverificationrequrl;
	private String eurotrnsstatusreq;
	private String euroBalenceReqUrl;
	private String euronetValidationUrl;
	private String rechargeAPI;
	private HttpClient httpClient;
	private HttpConnectionManagerParams connectionManagerConfig;
	private HostConfiguration euronetHostConfig;
	private String eurowsdlvas2topupurl;
	private String eurowsdlvas2statusurl;
	private String eurowsdlvas2balanceurl;
	private String euroGoogleCreditsServiceCode;
	private String euroGoogleCreditsCustomerId;
	private String euronetGCRefetchURL;
	private String euronetGCRefetchMerchantCode;

	public String getEuronetGCRefetchURL() {
		return euronetGCRefetchURL;
	}

	public void setEuronetGCRefetchURL(String euronetGCRefetchURL) {
		this.euronetGCRefetchURL = euronetGCRefetchURL;
	}

	public String getEuronetGCRefetchMerchantCode() {
		return euronetGCRefetchMerchantCode;
	}

	public void setEuronetGCRefetchMerchantCode(String euronetGCRefetchMerchantCode) {
		this.euronetGCRefetchMerchantCode = euronetGCRefetchMerchantCode;
	}

	public String getEuroGoogleCreditsCustomerId() {
		return euroGoogleCreditsCustomerId;
	}

	public void setEuroGoogleCreditsCustomerId(String euroGoogleCreditsCustomerId) {
		this.euroGoogleCreditsCustomerId = euroGoogleCreditsCustomerId;
	}

	public String getEuroGoogleCreditsServiceCode() {
		return euroGoogleCreditsServiceCode;
	}

	public void setEuroGoogleCreditsServiceCode(String euroGoogleCreditsServiceCode) {
		this.euroGoogleCreditsServiceCode = euroGoogleCreditsServiceCode;
	}

	public HostConfiguration getEuronetHostConfig() {
		return euronetHostConfig;
	}

	public void setEuronetHostConfig(HostConfiguration euronetHostConfig) {
		this.euronetHostConfig = euronetHostConfig;
	}

	public HttpConnectionManagerParams getConnectionManagerConfig() {
		return connectionManagerConfig;
	}

	public void setConnectionManagerConfig(HttpConnectionManagerParams connectionManagerConfig) {
		this.connectionManagerConfig = connectionManagerConfig;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setRechargeAPI(String rechargeAPI) {
		this.rechargeAPI = rechargeAPI;
	}

	public String getRechargeAPI() {
		return rechargeAPI;
	}

	public void setEuronetValidationUrl(String euronetValidationUrl) {
		this.euronetValidationUrl = euronetValidationUrl;
	}

	public void setEuroBalenceReqUrl(String euroBalenceReqUrl) {
		this.euroBalenceReqUrl = euroBalenceReqUrl;
	}

	public void setEuromerchantcode(String euromerchantcode) {
		this.euromerchantcode = euromerchantcode;
	}

	public void setEurousername(String eurousername) {
		this.eurousername = eurousername;
	}

	public void setEurouserpass(String eurouserpass) {
		this.eurouserpass = eurouserpass;
	}

	public void setEuroPostpaidUserName(String euroPostpaidUserName) {
		this.euroPostpaidUserName = euroPostpaidUserName;
	}

	public void setEuroPostpaidPassword(String euroPostpaidPassword) {
		this.euroPostpaidPassword = euroPostpaidPassword;
	}

	public void setEuropaymentprovider(String europaymentprovider) {
		this.europaymentprovider = europaymentprovider;
	}

	public void setEuropaymentmode(String europaymentmode) {
		this.europaymentmode = europaymentmode;
	}

	public void setEurochannelcode(String eurochannelcode) {
		this.eurochannelcode = eurochannelcode;
	}

	public void setEuroservicecode(String euroservicecode) {
		this.euroservicecode = euroservicecode;
	}

	public void setEuroOptional1(String euroOptional1) {
		this.euroOptional1 = euroOptional1;
	}

	public void setEurostorecode(String eurostorecode) {
		this.eurostorecode = eurostorecode;
	}

	public void setEuroterminalcode(String euroterminalcode) {
		this.euroterminalcode = euroterminalcode;
	}

	public void setEurodthservicecode(String eurodthservicecode) {
		this.eurodthservicecode = eurodthservicecode;
	}

	public String getEuroppservicecode() {
		return europpservicecode;
	}

	public void setEuroppservicecode(String europpservicecode) {
		this.europpservicecode = europpservicecode;
	}

	public void setEurocustomerid(String eurocustomerid) {
		this.eurocustomerid = eurocustomerid;
	}

	public void setEurotcflag(String eurotcflag) {
		this.eurotcflag = eurotcflag;
	}

	public void setEurotrnsstatusreq(String eurotrnsstatusreq) {
		this.eurotrnsstatusreq = eurotrnsstatusreq;
	}

	public String getEurowsdlvas2topupurl() {
		return eurowsdlvas2topupurl;
	}

	public void setEurowsdlvas2topupurl(String eurowsdlvas2topupurl) {
		this.eurowsdlvas2topupurl = eurowsdlvas2topupurl;
	}

	public String getEurowsdlvas2statusurl() {
		return eurowsdlvas2statusurl;
	}

	public void setEurowsdlvas2statusurl(String eurowsdlvas2statusurl) {
		this.eurowsdlvas2statusurl = eurowsdlvas2statusurl;
	}

	public String getEurowsdlvas2balanceurl() {
		return eurowsdlvas2balanceurl;
	}

	public void setEurowsdlvas2balanceurl(String eurowsdlvas2balanceurl) {
		this.eurowsdlvas2balanceurl = eurowsdlvas2balanceurl;
	}

	public String getEuroverificationrequrl() {
		return euroverificationrequrl;
	}

	public void setEuroverificationrequrl(String euroverificationrequrl) {
		this.euroverificationrequrl = euroverificationrequrl;
	}

	public void serEuronetValidationUrl(String euronetValidationUrl) {
		this.euronetValidationUrl = euronetValidationUrl;
	}

	public String getEuronetValidationUrl() {
		return euronetValidationUrl;
	}

	@Override
	public String getBalanceCachekey() {
		return EuronetService.EURONET_BALANCE_KEY;
	}

	@Override
	public String getBalanceUpdateTimeCachekey() {
		return EuronetService.EURONET_BALANCE_LAST_UPDATE_TIME_KEY;
	}

	@Autowired
	private FCProperties fcProperties;

	public void init() {
		logger.info("Calling init method euronet service");
		euronetHostConfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HOST), 443, "https");
		httpClient.setHostConfiguration(euronetHostConfig);
	}

	public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
			List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
			ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
		logger.info("[IN MODERATOR] processing doRecharge for euronet ", null);
		String euroOperatorCode = null;
		String euroCircleCode = null;
		String euronetRechargePlanType = null;

		InAggrOprCircleMap euroNetInAggrOprCircleMap = FCUtil.getInAggrOprCircleMap(inAggrOprCircleMaps,
				AggregatorInterface.EURONET_IN);

		if (euroNetInAggrOprCircleMap == null && !rechargeDo.getProductType().equalsIgnoreCase("D")) {
			return doActionAfterFail(rechargeDo, orderNo, parametersPassed);
		}

		if (euroNetInAggrOprCircleMap != null) {
			euroOperatorCode = euroNetInAggrOprCircleMap.getAggrOperatorCode();
			euroCircleCode = euroNetInAggrOprCircleMap.getAggrCircleCode();
			euronetRechargePlanType = euroNetInAggrOprCircleMap.getRechargeplanType();
		}

		// Adding euronet recharge condition for google credit recharges
		//
		AggregatorResponseDo aggregatorResponse = null;
		if ("X".equalsIgnoreCase(rechargeDo.getProductType())) {
			aggregatorResponse = this.doCreditRechargeHelper(rechargeDo, orderNo, parametersPassed, euroOperatorCode,
					euroCircleCode, euronetRechargePlanType, euroNetInAggrOprCircleMap.getShouldTimeOut(),
					euroNetInAggrOprCircleMap.getTimeOutInMillis());
		} else {
			aggregatorResponse = this.doRechargeHelper(rechargeDo, orderNo, parametersPassed, euroOperatorCode,
					euroCircleCode, euronetRechargePlanType, euroNetInAggrOprCircleMap.getShouldTimeOut(),
					euroNetInAggrOprCircleMap.getTimeOutInMillis());
		}

		return aggregatorResponse;
	}

	public AggregatorResponseDo doRechargeHelper(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed,
			String operatorCode, String circleCode, String euronetRechargePlanType, Boolean shouldTimeOut,
			Integer timeOutValue) throws HttpException, ConnectException, ConnectTimeoutException, SocketException,
			SocketTimeoutException, IOException {

		String euroStoreCode = eurostorecode, euroUserName = eurousername, euroUserPass = eurouserpass,
				euroTerminalCode = euroterminalcode;
		// We are setting different store code for Airtel transactions (As Telenor is
		// merged in Airtel same is passed for Telenor also)
		if (appConfig.isAirtelEuroNewWalletEnabled() && rechargeDo.getCircleOperatorMapping() != null
				&& (rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId()
						.equals(RechargeConstants.AIRTEL_OPERATOR_ID)
						|| rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId()
								.equals(RechargeConstants.TELENOR_OPERATOR_ID))) {
			euroStoreCode = fcProperties.getProperty(FCProperties.EURONET_AIRTEL_STORE_CODE);
			euroUserName = fcProperties.getProperty(FCProperties.EURONET_AIRTEL_USERNAME);
			euroUserPass = fcProperties.getProperty(FCProperties.EURONET_AIRTEL_USERPASS);
			euroTerminalCode = fcProperties.getProperty(FCProperties.EURONET_AIRTEL_TERMINAL_CODE);
		}

		logger.info("[IN MODERATOR] processing doRecharge for euronet ", null);

		AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

		String euronetOption1ParamValue = "TOPUP";

		logger.info("[IN MODERATOR] processing doRecharge for euronet  with operator : " + operatorCode
				+ " and Circle : " + circleCode, null);

		Calendar cal = Calendar.getInstance();
		Date dt = cal.getTime();

		NameValuePair[] euronetParam = new NameValuePair[18];

		euronetParam[0] = new NameValuePair(MERCHANT_REFERENCE_NO_PARAM, orderNo);
		euronetParam[1] = new NameValuePair("merchantcode", euromerchantcode);
		euronetParam[2] = new NameValuePair("username", euroUserName);
		euronetParam[3] = new NameValuePair("userpass", euroUserPass);
		euronetParam[4] = new NameValuePair("paymentprovider", europaymentprovider);
		euronetParam[5] = new NameValuePair("paymentmode", europaymentmode);
		euronetParam[6] = new NameValuePair("channelcode", eurochannelcode);
		euronetParam[7] = new NameValuePair("servicecode", euroservicecode);
		euronetParam[8] = new NameValuePair("amount", rechargeDo.getAmount().intValue() + "");
		euronetParam[9] = new NameValuePair("storecode", euroStoreCode);
		euronetParam[10] = new NameValuePair("terminalcode", euroTerminalCode);
		euronetParam[11] = new NameValuePair("customerid", eurocustomerid);
		euronetParam[12] = new NameValuePair("tcflag", eurotcflag);
		euronetParam[13] = new NameValuePair(SPCODE, operatorCode);
		euronetParam[14] = new NameValuePair("consumerno", "91" + rechargeDo.getMobileNo());
		euronetParam[15] = new NameValuePair("optional1", euronetOption1ParamValue);
		euronetParam[16] = new NameValuePair("optional2", "");
		euronetParam[17] = new NameValuePair("requesttype", "service");

		if (fcProperties.isPreProdMode()) {
			euroUserPass = "abcd1234*";
			euronetParam[3] = new NameValuePair("userpass", euroUserPass);

		}

		// special recharge request creation for Euronet for Mobile
		if (rechargeDo.getRechargePlanType() != null && !rechargeDo.getRechargePlanType().isEmpty()
				&& rechargeDo.getProductType().equals(RechargeConstants.MOBILE_PRODUCT_TYPE)
				&& rechargeDo.getRechargePlanType().equalsIgnoreCase(RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL)) {
			euronetOption1ParamValue = euronetRechargePlanType;
			euronetParam[15] = new NameValuePair("optional1", euronetOption1ParamValue);
			metricsClient.recordEvent("Recharge", "Euronet.RechargeType." + rechargeDo.getCircleCode(), "Special");
		}

		// special recharge request Creation for Euronet for Data Card
		if (rechargeDo.getProductType().equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE)
				&& (rechargeDo.getOperatorCode().equals(RechargeConstants.BSNL_DataCard)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.MTNL_DataCard_MUMBAI)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.MTNL_DataCard_DELHI)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.Tata_Docomo_GSM_DataCard)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.T24_DataCard))) {
			euronetOption1ParamValue = euronetRechargePlanType;
			euronetParam[15] = new NameValuePair("optional1", euronetOption1ParamValue);
		}

		if (rechargeDo.getRechargePlanType().equals(RechargeConstants.DEFAULT_RECHARGE_PLAN_TYPE_VALUE)
				&& rechargeDo.getProductType().equals(RechargeConstants.MOBILE_PRODUCT_TYPE)
				&& (rechargeDo.getOperatorCode().equals(RechargeConstants.UNINOR_MOBILE_OPERATOR)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.VIDEOCON_MOBILE_OPERATOR)
						|| rechargeDo.getOperatorCode().equals(RechargeConstants.TATA_DOCOMO_GSM_MOBILE))) {
			euronetOption1ParamValue = RechargeConstants.RECHARGE_TYPE_FLEXI;
			euronetParam[15] = new NameValuePair("optional1", euronetOption1ParamValue);
		}
		logger.info("special recharge plan value for order id " + orderNo + " is " + euronetRechargePlanType);
		if (rechargeDo.getProductType().equalsIgnoreCase("C")) {
			euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
		}

		if (rechargeDo.getProductType().equalsIgnoreCase("D")) {

			if (rechargeDo.getOperatorCode().trim().equalsIgnoreCase("Dish TV")) {
				euronetParam[13] = new NameValuePair(SPCODE, "DTV");
				euronetParam[16] = new NameValuePair("SspCode", "DTVAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);
				logger.debug("dish tv param " + euronetParam[13].getName() + euronetParam[16].getName()
						+ euronetParam[14].getName() + euronetParam[7].getName());
			} else if (rechargeDo.getOperatorCode().equalsIgnoreCase("Tata Sky")) {

				euronetParam[13] = new NameValuePair(SPCODE, "TDH");
				euronetParam[16] = new NameValuePair("SspCode", "TDHAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);

			} else if (rechargeDo.getOperatorCode().equalsIgnoreCase("Big TV")) {
				euronetParam[13] = new NameValuePair(SPCODE, "BTV");
				euronetParam[16] = new NameValuePair("SspCode", "BTVAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);

			} else if (rechargeDo.getOperatorCode().equalsIgnoreCase("Videocon TV")) {
				euronetParam[13] = new NameValuePair(SPCODE, "VDH");
				euronetParam[16] = new NameValuePair("SspCode", "VDHAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);

			} else if (rechargeDo.getOperatorCode().equalsIgnoreCase("Sun Tv")) {
				euronetParam[13] = new NameValuePair(SPCODE, "STV");
				euronetParam[16] = new NameValuePair("SspCode", "STVAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);

			} else if (("Airtel Tv").equalsIgnoreCase(rechargeDo.getOperatorCode())) {
				euronetParam[13] = new NameValuePair(SPCODE, "ATV");
				euronetParam[16] = new NameValuePair("SspCode", "ATVAI");
				euronetParam[14] = new NameValuePair("consumerno", rechargeDo.getMobileNo());
				euronetParam[7] = new NameValuePair("servicecode", eurodthservicecode);
			}
		}

		if (rechargeDo.getProductType().equalsIgnoreCase("V") || rechargeDo.getProductType().equalsIgnoreCase("C")) {
			euronetParam[16] = new NameValuePair("SspCode", circleCode);
		}

		// special recharge request creation for JIO Datacard
		if (rechargeDo.getProductType().equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE)) {
			if (rechargeDo.getOperatorId() == RechargeConstants.JIO_DATACARD_OP_ID) {
				euroservicecode = JIO_SPECIAL_SERVICE_CODE;
				euronetParam[7] = new NameValuePair("servicecode", euroservicecode);
				euronetParam[16] = new NameValuePair("SspCode", "JIOAI");

			}
		}

		parametersPassed.append("merchantrefno = " + orderNo + ", ");
		parametersPassed.append("merchantcode = " + euromerchantcode + ", ");
		parametersPassed.append("username = " + euroUserName + ", ");
		parametersPassed.append("userpass = " + euroUserPass + ", ");
		parametersPassed.append("paymentprovider = " + europaymentprovider + ", ");
		parametersPassed.append("paymentmode = " + europaymentmode + ", ");
		parametersPassed.append("channelcode = " + eurochannelcode + ", ");
		parametersPassed.append("servicecode = " + euronetParam[7] + ", ");
		parametersPassed.append("consumerno = " + euronetParam[14] + ", ");
		parametersPassed.append("amount = " + rechargeDo.getAmount().intValue() + ", ");
		parametersPassed.append("storecode = " + euroStoreCode + ", ");
		parametersPassed.append("terminalcode = " + euroTerminalCode + ", ");
		parametersPassed.append("customerid = " + eurocustomerid + ", ");
		parametersPassed.append("tcflag = " + eurotcflag + ", ");
		parametersPassed.append("spcode = " + operatorCode + ", ");
		parametersPassed.append("optional1 = " + euronetOption1ParamValue + ", ");
		parametersPassed.append("spcode = " + euronetParam[13] + ", ");
		parametersPassed.append("optional2 = " + euronetParam[16] + ", ");

		logger.info("[IN MODERATOR] Executing recharge request from euronet for operatorCode : " + operatorCode
				+ " && mobile no :" + rechargeDo.getMobileNo(), null);

		String response = null;
		Map<String, String> topupResponseMap = null;
		if (!appConfig.isEuronetVas2Enabled()) {
			GetMethod method = new GetMethod(this.getRechargeAPI());
			logger.info("EURO URL : " + this.getRechargeAPI());
			method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
			method.setQueryString(euronetParam);
			logger.info("Euronet request URI " + method.getQueryString());
			try {
				/*int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), httpClient, method, shouldTimeOut,
						timeOutValue);*/
				
				int statusCode = httpClient.executeMethod(method);

				if (statusCode == HttpStatus.SC_OK) {
					response = method.getResponseBodyAsString();
				} else {
					logger.info("[IN MODERATOR] Received HTTP status code" + statusCode);
				}
				logger.info("Operator recharge request and response for : "+operatorCode + "Request:: "+method.getQueryString()+ "Response:: "+response + "Status Code: "+statusCode);
			} catch (ConnectTimeoutException | SocketException e) {
				logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ConnectTimeoutException";
	        } catch (IOException e) {
	        	logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ReadTimeoutException";
	        } catch (Exception ex) {
				logger.error("Exception : " + ex);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=UnknownException";
			}  finally {

				logger.info("[IN MODERATOR] Releasing connection from IN in doRecharge(finally) for euronet", null);
				method.releaseConnection();
			}
		} else {
			try {
				String request = RechargeUtil.createSubmitData(euronetParam, PaymentConstants.UTF_8_ENCODING);
				logger.info("Request going via eurovas2.0 : " + request + ",  EURO URL : " + eurowsdlvas2topupurl);
				IEPayServiceProxy client = new IEPayServiceProxy(eurowsdlvas2topupurl.replace("SPCODE", operatorCode));
				response = executeEurovas2WithTimeout(rechargeDo.getAffiliateTransId(), client, request, shouldTimeOut,
						timeOutValue);

			} catch (Exception exception) {
				logger.error("Exception raised while doing recharge for merchantrefno : " + orderNo, exception);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=UnknownException";
			}
		}

		logger.info("[IN MODERATOR] Output of recharge request from Euronet "+response, null);

		/*
		 * output from above url is loke below merchantrefno<transaction
		 * ID>=10082007118673195573&enrefno<aggregator transaction
		 * ID>=2007118673195573123 &responsecode=00&operatorrefno<operatortransacrion
		 * ID>= MH001200345 &responsemessage=Transaction
		 * Successful&responseaction=Transaction Successful Note : code here to update
		 * the response details in fc_recharge_transaction table
		 */

		/* Getting Response Time */
		Calendar cal1 = Calendar.getInstance();
		Date dt1 = cal1.getTime();

		if (response != null && !response.isEmpty()) {
			topupResponseMap = FCUtil.stringToHashMap(response);
			if (CollectionUtils.isEmpty(topupResponseMap)) {
				logger.info("Invalid Response Received  : " + topupResponseMap);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=EmptyResponseMap";
				topupResponseMap = FCUtil.stringToHashMap(response);
			}
		} else {
			response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
					+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=Empty Response From Aggregator";
			topupResponseMap = FCUtil.stringToHashMap(response);
		}

		String euronetResponseCode = topupResponseMap.get(RESPONSE_CODE_PARAM);
		if (StringUtils.isEmpty(euronetResponseCode)) {
			euronetResponseCode = "UP";
		}

		if (euronetResponseCode != null && !euronetResponseCode.isEmpty()) {
			metricsClient.recordEvent("Recharge", "Euronet.ResponseCode", euronetResponseCode);
			metricsClient.recordEvent("Recharge", "Euronet.ResponseCode." + operatorCode, euronetResponseCode);
		}

		aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_EURONET);
		aggregatorResponseDo.setAggrOprRefernceNo(topupResponseMap.get("operatorrefno"));
		aggregatorResponseDo.setAggrReferenceNo(topupResponseMap.get(EURONET_REFERENCE_NO_PARAM));
		aggregatorResponseDo.setAggrResponseCode(euronetResponseCode);
		aggregatorResponseDo.setAggrResponseMessage(topupResponseMap.get(RESPONSE_MESSAGE_PARAM));
		aggregatorResponseDo.setRawRequest(parametersPassed.toString());
		aggregatorResponseDo.setRawResponse(response);
		aggregatorResponseDo.setRequestId(getRequestId(orderNo));
		aggregatorResponseDo.setRequestTime(dt);
		aggregatorResponseDo.setResponseTime(dt1);

		logger.info("topupResponseMap ### : " + topupResponseMap);
		if (CollectionUtils.isEmpty(topupResponseMap)) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);

		} else if (isSuccess(topupResponseMap))
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
		else if (RechargeConstants.EURO_ERROR_CODES.contains(aggregatorResponseDo.getAggrResponseCode())) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
		} else {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
		}

		return aggregatorResponseDo;
	}

	private static boolean isSuccess(Map<String, String> responseMap) {
		return RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(responseMap.get(RESPONSE_CODE_PARAM));
	}

	public static long getRequestId(String orderNo) {
		return Long.parseLong(orderNo.substring(orderNo.indexOf("_") + 1, orderNo.length()));
	}

	private AggregatorResponseDo doActionAfterFail(RechargeDo rechargeDo, String orderNo,
			StringBuffer parametersPassed) {

		metricsClient.recordEvent("Recharge", "Euronet.NoOperatorCode." + String.valueOf(rechargeDo.getOperatorCode())
				+ "." + String.valueOf(rechargeDo.getCircleCode()), "NoCode");

		logger.info("[IN MODERATOR] Recharge process fail for EURONET: No Operator code found "
				+ "in DB(euronet_operator_map) for operator =  " + rechargeDo.getOperatorCode(), null);

		AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

		/* Getting Request Time */
		Calendar cal = Calendar.getInstance();
		Date dt = cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String requestdate = sdf.format(dt);

		parametersPassed.append("merchantrefno = " + orderNo + ", ");
		parametersPassed.append("merchantcode = " + euromerchantcode + ", ");
		parametersPassed.append("username = " + eurousername + ", ");
		parametersPassed.append("userpass = " + eurouserpass + ", ");
		parametersPassed.append("paymentprovider = " + europaymentprovider + ", ");
		parametersPassed.append("paymentmode = " + europaymentmode + ", ");
		parametersPassed.append("channelcode = " + eurochannelcode + ", ");
		parametersPassed.append("servicecode = " + euroservicecode + ", ");
		parametersPassed.append("consumerno = " + rechargeDo.getMobileNo() + ", ");
		parametersPassed.append("amount = " + rechargeDo.getAmount().intValue() + ", ");
		parametersPassed.append("storecode = " + eurostorecode + ", ");
		parametersPassed.append("terminalcode = " + euroterminalcode + ", ");
		parametersPassed.append("customerid = " + eurocustomerid + ", ");
		parametersPassed.append("tcflag = " + eurotcflag + ", ");
		parametersPassed.append("operator = " + "" + ", ");
		parametersPassed.append("Circle = " + "" + ", ");
		parametersPassed.append("optional1 = " + "" + ", ");

		aggregatorResponseDo.setAggrName("euronet");
		aggregatorResponseDo.setRawRequest(parametersPassed.toString());
		aggregatorResponseDo.setRequestId(getRequestId(orderNo));
		aggregatorResponseDo.setRequestTime(dt);

		aggregatorResponseDo.setAggrOprRefernceNo("");
		aggregatorResponseDo.setAggrResponseMessage("Entry missing for operator circle mapping in the database");
		aggregatorResponseDo.setRawResponse("");
		aggregatorResponseDo.setResponseTime(dt);
		aggregatorResponseDo.setAggrResponseCode("ETI");
		aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
		return aggregatorResponseDo;
	}

	public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
			throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
			IOException {

		if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
			return refetchCreditRecharge(rechargeDo, inResponseFromRequest);
		}

		String merchantReferenceCode = fcProperties.getInTransIdPrefix() + inResponseFromRequest.getRetryNumber() + "_"
				+ rechargeDo.getInRequestId().toString();
		String euronetTransactionId = inResponseFromRequest.getAggrTransId();
		NameValuePair[] nameValuePairs = new NameValuePair[4];

		if (StringUtils.isEmpty(inResponseFromRequest.getAggrTransId())
				|| inResponseFromRequest.getAggrTransId().equals("unknown")) {
			nameValuePairs[0] = new NameValuePair("merchantrefno", merchantReferenceCode);
			nameValuePairs[1] = new NameValuePair("merchantcode", euromerchantcode);
			nameValuePairs[2] = new NameValuePair("enrefno", "");
		} else {
			nameValuePairs[0] = new NameValuePair("merchantrefno", "");
			nameValuePairs[1] = new NameValuePair("merchantcode", euromerchantcode);
			nameValuePairs[2] = new NameValuePair("enrefno", euronetTransactionId);
		}
		nameValuePairs[3] = new NameValuePair("requesttype", "status");
		

		Map<String, String> responseMap = new HashMap<String, String>();

		String response = "";

		final Date requestTime = new Date();
		Date responseTime = null;

		AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
		GetMethod method = new GetMethod(eurotrnsstatusreq);
		method.setQueryString(nameValuePairs);

		try {
			logger.debug("euronet Status check request param is:" + method.getQueryString());
			logger.info("euronet Status check request param is: " + method.getQueryString()
					+ " and status check url is : " + eurotrnsstatusreq);
			try {
				if (!appConfig.isEuronetVas2Enabled()) {
					/*int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), httpClient, method,
							true, 60000);*/
				//	int statusCode = executeWithoutTimeout(rechargeDo.getAffiliateTransId(), httpClient, method,"euronet");
					
					int statusCode = httpClient.executeMethod(method);

					if (statusCode == HttpStatus.SC_OK) {
						response = method.getResponseBodyAsString();
						responseTime = new Date();
					} else {
						logger.info("[IN MODERATOR] Received HTTP status code for Transaction Status Request:"
								+ statusCode);
						aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
					}

					logger.info("euronet status check url is " + method.getURI()
							+ " euronet Status check request param is: " + method.getQueryString()
							+ " [IN MODERATOR] Output for Transaction Status Request from Euronet : " + response, null);
				} else {
					try {
						String request = RechargeUtil.createSubmitData(nameValuePairs, PaymentConstants.UTF_8_ENCODING);
						logger.info("euronet status check url is " + eurowsdlvas2statusurl
								+ " euronet Status check request param is: " + request);
						IEPayServiceProxy client = new IEPayServiceProxy(eurowsdlvas2statusurl);
						response = client.sendEuroVasRequest(request);
						logger.info("euronet status check url is " + eurowsdlvas2statusurl
								+ " euronet Status check request param is: " + request
								+ " [IN MODERATOR] Output for Transaction Status Request from Euronet : " + response);
					} catch (Exception exception) {
						logger.error("Exception raised while doing status check for for merchantrefno : "
								+ merchantReferenceCode, exception);
						aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
					}
				}
			}  catch (ConnectTimeoutException | SocketException e) {
				logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ConnectTimeoutException";
	        } catch (IOException e) {
	        	logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ReadTimeoutException";
	        } catch (Exception ex) {
				logger.error("Exception : " + ex);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=UnknownException";
			}
			
			if (response != null && !response.isEmpty()) {
				responseMap = FCUtil.stringToHashMap(response);
				if (CollectionUtils.isEmpty(responseMap)) {
					logger.info("Invalid Response Received  : " + responseMap);
					response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
							+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=EmptyResponseMap";
					responseMap = FCUtil.stringToHashMap(response);
				}
				// replacing the string since we can't store the google code and serial
				if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
					List<String> exclusionList = new ArrayList<String>();
					exclusionList.add("cardpin");
					exclusionList.add("cardserial");
					response = FCUtil.hashMapToString(responseMap, exclusionList);
				}
			} else {
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=Empty Response From Aggregator";
				responseMap = FCUtil.stringToHashMap(response);
			}

		} finally {
			logger.info("[IN MODERATOR] Releasing connection from IN in getTransactionStatus for euronet", null);
			method.releaseConnection();
		}
		String resCode = responseMap.get(RESPONSE_CODE_PARAM);
		if (StringUtils.isEmpty(resCode)) {
			resCode = "UP";
		}
		aggregatorResponseDo.setAggrName(AggregatorInterface.EURONET_IN);
		aggregatorResponseDo.setAggrReferenceNo(responseMap.get(EURONET_REFERENCE_NO_PARAM));
		aggregatorResponseDo.setAggrResponseCode(resCode);
		aggregatorResponseDo.setAggrResponseMessage(responseMap.get(RESPONSE_MESSAGE_PARAM));
		aggregatorResponseDo.setRawRequest(eurotrnsstatusreq);
		aggregatorResponseDo.setAggrOprRefernceNo(responseMap.get("operatorrefno"));
		aggregatorResponseDo.setGoogleSerial(responseMap.get(GOOGLE_SERIAL));
		aggregatorResponseDo.setGoogleCode(responseMap.get(GOOGLE_PIN));
		aggregatorResponseDo.setRawResponse(response);
		aggregatorResponseDo.setRequestTime(requestTime);
		aggregatorResponseDo.setResponseTime(responseTime);
		/*
		 * if (isSuccess(responseMap)) {
		 * aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS)
		 * ; } else { if (this.isEuronetAmbiguousResponse(aggregatorResponseDo)) {
		 * aggregatorResponseDo.setAggregatorErrorWhileQuerying(true); } else {
		 * aggregatorResponseDo.setTransactionStatus(RechargeConstants.
		 * TRANSACTION_STATUS_UNSUCCESSFUL); } }
		 */

		logger.info("topupResponseMap ### : " + responseMap);
		if (CollectionUtils.isEmpty(responseMap)) {
			aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);

		} else if (isSuccess(responseMap))
			aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
		else if (RechargeConstants.EURO_ERROR_CODES.contains(aggregatorResponseDo.getAggrResponseCode())) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
		} else {
			aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
		}

		return aggregatorResponseDo;
	}

	// TODO : Need to remove else block
	private boolean isEuronetAmbiguousResponse(AggregatorResponseDo aggregatorResponseDo) {
		if (appConfig.isEuronetNewErrorCodeEnabled()) {
			return RechargeConstants.RECHARGE_STATUS_AMBIGUOUS_RESPONSE_CODES_NEW
					.contains(aggregatorResponseDo.getAggrResponseCode());
		} else {
			return RechargeConstants.RECHARGE_STATUS_AMBIGUOUS_RESPONSE_CODES
					.contains(aggregatorResponseDo.getAggrResponseCode());
		}
	}

	public EuroValidationResponseDo validatePlan(RechargePlan rechargePlan, String serviceNumber,
			CallSourceType callSourceType) {

		EuroValidationResponseDo validationResponse = null;
		NameValuePair[] euronetParam = new NameValuePair[9];
		String euronetOptional1ParamValue = "FLEXI";

		String euroOperatorCode = null;
		String euroCircleCode = null;
		int connectionTimeout = TimeoutKey.DEFAULT_VALIDATION_TIMEOUT.getTimeoutInMillis();
		try {
			List<InAggrOprCircleMap> inAggrOprCircleMaps = operatorCircleService
					.getInAggrOprCircleMap(rechargePlan.getOperatorMasterId(), rechargePlan.getCircleMasterId());
			InAggrOprCircleMap euroNetInAggrOprCircleMap = FCUtil.getInAggrOprCircleMap(inAggrOprCircleMaps,
					AggregatorInterface.EURONET_IN);
			if (euroNetInAggrOprCircleMap != null) {
				euroOperatorCode = euroNetInAggrOprCircleMap.getAggrOperatorCode();
				euroCircleCode = euroNetInAggrOprCircleMap.getAggrCircleCode();
			}
		} catch (Exception e) {
			logger.error("Exception caught is ", e);
		}

		String planType = rechargePlan.getName();
		String serviceCode = euroservicecode;

		if (!FCUtil.isEmpty(planType) && !"topup".equalsIgnoreCase(planType)) {
			euronetOptional1ParamValue = "SPECIAL";
		}

		// override params for postpaid case
		if (ProductMaster.ProductName.MobilePostpaid.getProductId() == rechargePlan.getProductType()) {
			serviceCode = europpservicecode;
			BillPostpaidMerchant postpaidMerchant = billPayService
					.getPostpaidMerchantByOperatorId(rechargePlan.getOperatorMasterId());
			euroOperatorCode = postpaidMerchant.getEuronetMerchantId();
			euroCircleCode = "";
			euronetOptional1ParamValue = "";
		}

		euronetParam[0] = new NameValuePair("merchantcode", euromerchantcode);
		euronetParam[1] = new NameValuePair("username", eurousername);
		euronetParam[2] = new NameValuePair("userpass", eurouserpass);
		euronetParam[3] = new NameValuePair("servicecode", serviceCode);
		euronetParam[4] = new NameValuePair("consumerno", "91" + serviceNumber);
		euronetParam[5] = new NameValuePair("amount", String.valueOf(rechargePlan.getAmount().intValue()));
		euronetParam[6] = new NameValuePair("Optional1", euronetOptional1ParamValue);
		euronetParam[7] = new NameValuePair("Spcode", euroOperatorCode);
		euronetParam[8] = new NameValuePair("SSpCode", euroCircleCode);

		final SimpleHttpConnectionManager euronetConnectionManager = new SimpleHttpConnectionManager(true);
		euronetConnectionManager.setParams(connectionManagerConfig);

		HttpClient rechargeClient = new HttpClient(euronetConnectionManager);

		int timeoutFromAppConfig = getConnectionTimeoutFromAppConfig();
		if (timeoutFromAppConfig > 0) {
			connectionTimeout = timeoutFromAppConfig;
			connectionManagerConfig.setConnectionTimeout(connectionTimeout);
		}

		rechargeClient.setHostConfiguration(euronetHostConfig);

		GetMethod method = new GetMethod(this.getEuronetValidationUrl());
		method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
		method.setQueryString(euronetParam);
		logger.info("Euronet request URI: " + method.getQueryString() + " ,circle:" + rechargePlan.getCircleMasterId()
				+ " ,denomination:" + rechargePlan.getAmount() + " ,plan:" + rechargePlan.getName());
		String response = null;
		Map<String, String> topupResponseMap = null;
		try {
			// int statusCode =
			// externalClientCallService.executeWithTimeOut(PlanValidationCommandBuilder
			// .getPlanValidationResponseCommand(rechargeClient, method, "Recharge",
			// "PlanValidation",callSourceType));
			int statusCode = executeWithTimeout(rechargeClient, method, connectionTimeout, true, "Recharge",
					"PlanValidation");

			if (statusCode == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
				logger.info("Euronet Response: " + response + " ,circle:" + rechargePlan.getCircleMasterId()
						+ " ,denomination:" + rechargePlan.getAmount() + " ,plan:" + rechargePlan.getName());
			} else {
				logger.info("[IN MODERATOR] Received HTTP status code" + statusCode);
			}
			validationResponse = new EuroValidationResponseDo();
			validationResponse.setRawRequest(method.getQueryString());
			if (response != null && !response.isEmpty()) {
				topupResponseMap = FCUtil.stringToHashMap(response);
				validationResponse.setResponseCode(topupResponseMap.get("responsecode"));
				validationResponse.setResponseMessage(topupResponseMap.get("responsemessage"));
				validationResponse.setResponseAction(topupResponseMap.get("responseaction"));
				if (euronetOptional1ParamValue.equals("FLEXI")) {
					validationResponse.setPlanType("topup");
				} else {
					validationResponse.setPlanType("special");
				}
			}
		} catch (Exception e) {
			logger.error("Exception in Validation API for serviceNumber:" + serviceNumber + ", Request - "
					+ method.getQueryString(), e);
		} finally {
			logger.info("[IN MODERATOR] Releasing connection from IN in doValidation(finally) for euronet", null);
			method.releaseConnection();
		}

		return validationResponse;
	}

	@Override
	public Double getWalletBalance() throws IOException {
		NameValuePair[] euronetParam = { new NameValuePair("merchantcode", "ACC"),
				new NameValuePair("username", "ACC_01"), new NameValuePair("userpass", "abcd1234*") };

		GetMethod getBalanceRequest = new GetMethod(euroBalenceReqUrl);
		getBalanceRequest.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
		getBalanceRequest.setQueryString(euronetParam);
		int httpStatus = this.getHttpClient().executeMethod(getBalanceRequest);
		String getBalanceResponse = getBalanceRequest.getResponseBodyAsString();

		/*
		 * amount=000008154714&merchantcode=ACC
		 */
		logger.info("Euronet Balance Response: " + getBalanceResponse);
		if (httpStatus == HttpStatus.SC_OK) {
			String responseMessage = getBalanceResponse.split("&")[0];

			if (responseMessage.contains("amount=")) {
				String balanceAmount = responseMessage.split("=")[1];
				return new Double(balanceAmount);
			}
		}
		throw new IOException("Did not get valid balance response from Euronet");
	}

	@Override
	protected Double getBalanceWarnThreshold() {
		return fcProperties.getEuronetBalanceWarnThreshold();
	}

	@Override
	protected Double getBalanceCriticalThreshold() {
		return fcProperties.getEuronetBalanceCriticalThreshold();
	}

	public EuroDTHValidationResponseDo validateNumber(String productType, Integer operatorId, String circleId,
			String serviceNumber, Integer connectionTimeout, Float amount) {
		EuroDTHValidationResponseDo validationResponse = new EuroDTHValidationResponseDo();
		if (productType.equalsIgnoreCase(RechargeConstants.DTH_PRODUCT_TYPE)) {
			validationResponse = validateDTHNumber(operatorId, serviceNumber, connectionTimeout, amount);
		}
		return validationResponse;
	}

	private EuroDTHValidationResponseDo validateDTHNumber(Integer operatorId, String serviceNumber,
			Integer connectionTimeout, Float amount) {
		String defaultAmount = "100";
		
		if(amount != null && amount!=0.0f) { 
			logger.info("Taking actual amount");
			//INFO: Downcast amount value + convert data type from float to integer 
			defaultAmount = String.valueOf(amount.longValue());
		}
		
		EuroDTHValidationResponseDo validationResponse = new EuroDTHValidationResponseDo();

		NameValuePair[] euronetParam = new NameValuePair[8];
		euronetParam[0] = new NameValuePair("merchantcode", euromerchantcode);
		euronetParam[1] = new NameValuePair("username", eurousername);
		euronetParam[2] = new NameValuePair("userpass", eurouserpass);
		euronetParam[3] = new NameValuePair("servicecode", eurodthservicecode);
		euronetParam[4] = new NameValuePair("consumerno", serviceNumber);
		euronetParam[5] = new NameValuePair("amount", defaultAmount);
		euronetParam[6] = new NameValuePair("Spcode", "");
		euronetParam[7] = new NameValuePair("SSpCode", "");

		String operatorName = operatorCircleService.getOperator(operatorId).getName();

		if (operatorName != null && operatorName.trim().equalsIgnoreCase("Dish TV")) {
			euronetParam[6] = new NameValuePair("Spcode", "DTV");
			euronetParam[7] = new NameValuePair("SSpCode", "DTVAI");
		} else if (operatorName != null && operatorName.trim().equalsIgnoreCase("Tata Sky")) {
			euronetParam[6] = new NameValuePair("Spcode", "TDH");
			euronetParam[7] = new NameValuePair("SSpCode", "TDHAI");
		} else if (operatorName != null && operatorName.trim().equalsIgnoreCase("Reliance Digital TV")) {
			euronetParam[6] = new NameValuePair("Spcode", "BTV");
			euronetParam[7] = new NameValuePair("SSpCode", "BTVAI");
		} else if (operatorName != null && operatorName.trim().equalsIgnoreCase("Videocon D2H")) {
			euronetParam[6] = new NameValuePair("Spcode", "VDH");
			euronetParam[7] = new NameValuePair("SSpCode", "VDHAI");
		} else if (operatorName != null && operatorName.trim().equalsIgnoreCase("Sun Direct")) {
			euronetParam[6] = new NameValuePair("Spcode", "STV");
			euronetParam[7] = new NameValuePair("SSpCode", "STVAI");
		} else if (operatorName != null && operatorName.trim().equalsIgnoreCase("Airtel Digital TV")) {
			euronetParam[6] = new NameValuePair("Spcode", "ATV");
			euronetParam[7] = new NameValuePair("SSpCode", "ATVAI");
		}
		
		final SimpleHttpConnectionManager euronetConnectionManager = new SimpleHttpConnectionManager(true);
		connectionManagerConfig.setConnectionTimeout(connectionTimeout);
		euronetConnectionManager.setParams(connectionManagerConfig);

		HttpClient rechargeClient = new HttpClient(euronetConnectionManager);

		if (fcProperties.isQaMode()) {
			euronetHostConfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HOST), 443, "https");
			euronetParam[2] = new NameValuePair("userpass", "ACC_01");
		}
		rechargeClient.setHostConfiguration(euronetHostConfig);

		GetMethod method = new GetMethod(this.getEuronetValidationUrl());
		method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
		method.setQueryString(euronetParam);
		logger.info("Euronet request URI: " + "Host Configuration : " + rechargeClient.getHostConfiguration()
				+ "GET method : " + this.getEuronetValidationUrl() + "Query String : " + method.getQueryString());
		logger.info("DTH Consumer No Validation Request for :" + operatorName);
		String response = null;
		Map<String, String> validationResponseMap = null;
		try {
			int statusCode = rechargeClient.executeMethod(method);

			if (statusCode == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
				logger.info("Euronet Response: " + response);
			} else {
				logger.info("[IN MODERATOR] Received HTTP status code " + statusCode);
			}

			if (response != null && !response.isEmpty()) {
				validationResponseMap = FCUtil.stringToHashMap(response);
				validationResponse.setResponseCode(validationResponseMap.get("responsecode"));
				validationResponse.setResponseMessage(validationResponseMap.get("responsemessage"));
				validationResponse.setResponseAction(validationResponseMap.get("responseaction"));
			}
		} catch (Exception e) {
			logger.error("Exception in Validation API for serviceNumber:" + serviceNumber, e);
		} finally {
			logger.info("[IN MODERATOR] Releasing connection from IN in validateDTHNumber(finally) for euronet", null);
			method.releaseConnection();
		}
		return validationResponse;
	}

	private NameValuePair[] createEuronetPostpaidVerificationRequest(String mobileNumber, Integer operatorId) {

		Integer amount = 50;
		String euronetSPCode = "";
		String sspCode = getAggregatorCircleCodeByMobileNumAndOperatorId(mobileNumber, operatorId);

		if (!Strings.isNullOrEmpty(getBillpaymentMerchantIdByOperatorId(operatorId))) {
			euronetSPCode = getBillpaymentMerchantIdByOperatorId(operatorId);
			logger.info("inside if spcode:" + euronetSPCode);
		} else {
			if (operatorId.equals(fcProperties.getProperty(FCProperties.DOCOMO_POSTPAID_CDMA))) {
				euronetSPCode = "TAT";
			} else if (operatorId.equals(fcProperties.getProperty(FCProperties.DOCOMO_POSTPAID_GSM))) {
				euronetSPCode = "TAD";
			}
			logger.info("inside else spcode:" + euronetSPCode);
		}

		if (Strings.isNullOrEmpty(sspCode) && euronetSPCode.equals("TAT") || euronetSPCode.equals("TAD")) {
			sspCode = euronetSPCode + "AP";
		}

		logger.info("sspcode:" + sspCode + " and spcode:" + euronetSPCode);
		NameValuePair[] euronetParam = new NameValuePair[15];

		euronetParam[0] = new NameValuePair("merchantcode", euromerchantcode);
		euronetParam[1] = new NameValuePair("username", euroPostpaidUserName);
		euronetParam[2] = new NameValuePair("userpass", euroPostpaidPassword);
		euronetParam[3] = new NameValuePair("paymentprovider", europaymentprovider);
		euronetParam[4] = new NameValuePair("paymentmode", europaymentmode);
		euronetParam[5] = new NameValuePair("channelcode", eurochannelcode);
		euronetParam[6] = new NameValuePair("servicecode", "PP");
		euronetParam[7] = new NameValuePair("consumerno", mobileNumber);
		euronetParam[8] = new NameValuePair("amount", Integer.toString(amount.intValue()));
		euronetParam[9] = new NameValuePair("storecode", eurostorecode);
		euronetParam[10] = new NameValuePair("terminalcode", "");
		euronetParam[11] = new NameValuePair("customerid", eurocustomerid);
		euronetParam[12] = new NameValuePair("tcflag", eurotcflag);
		euronetParam[13] = new NameValuePair("spcode", euronetSPCode);
		euronetParam[14] = new NameValuePair("sspcode", sspCode);

		return euronetParam;
	}

	public BillPaymentVerificationAPIResponse verificationForOperator(String aggregatorTxnId, String mobilNumber,
			Integer operatorId) {

		logger.info("inside euronet service verificationForOperator method");
		String response = null;
		Boolean shouldTimeOut = true;
		Integer timeOutValue = 1000 * 5;
		NameValuePair[] euronetParam = null;
		BillPaymentVerificationAPIResponse billPayVerifyResponse = new BillPaymentVerificationAPIResponse();
		Map<String, String> validationResponseMap = new HashMap<String, String>();

		final SimpleHttpConnectionManager euronetConnectionManager = new SimpleHttpConnectionManager(true);
		euronetConnectionManager.setParams(connectionManagerConfig);
		HttpClient verificationClient = new HttpClient(euronetConnectionManager);
		euronetParam = createEuronetPostpaidVerificationRequest(mobilNumber, operatorId);
		verificationClient.setHostConfiguration(euronetHostConfig);
		GetMethod method = null;
		method = new GetMethod(this.getEuroverificationrequrl());

		method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
		method.setQueryString(euronetParam);
		logger.info("Raw request for verification api:" + method.getQueryString());
		Map<String, Object> fetchBillLogDataMap = logRequestService.initValidationLogDataMap(mobilNumber, operatorId,
				33, "euronet", LoggingConstants.ValidationCallType.POSTPAID_FETCH_BILL.getName());
		fetchBillLogDataMap.put(LoggingConstants.RAW_REQUEST, method.getQueryString());
		try {
			int statusCode = executeWithTimeout(aggregatorTxnId, verificationClient, method, shouldTimeOut,
					timeOutValue);
			if (statusCode == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
				logger.info("Response from verification api: " + response);
			}
			if (response != null && !response.isEmpty()) {
				validationResponseMap = FCUtil.stringToHashMap(response);
				billPayVerifyResponse.setAccountNumber(validationResponseMap.get("AccountNumber"));
				billPayVerifyResponse.setBillAmount(validationResponseMap.get("BillAmount"));
				billPayVerifyResponse.setCustomerEmail(validationResponseMap.get("customeremail"));
				billPayVerifyResponse.setCustomerName(validationResponseMap.get("CustomerFirstName"));
				billPayVerifyResponse.setDueDate(validationResponseMap.get("DueDate"));
				billPayVerifyResponse.setInvoiceNumber(validationResponseMap.get("InvoiceNumber"));
				billPayVerifyResponse.setLastBilledAmount(validationResponseMap.get("lastbilledamount"));
				billPayVerifyResponse.setResponseAction(validationResponseMap.get("responseaction"));
				billPayVerifyResponse.setResponseCode(validationResponseMap.get("responsecode"));
				billPayVerifyResponse.setResponseMessage(validationResponseMap.get("responsemessage"));

			} else {
				logger.info("Received HTTP status code" + statusCode);
			}
		} catch (Exception e) {
			logger.error("exception occured for euronet postpaid payment ", e);
		}
		fetchBillLogDataMap.put(LoggingConstants.RAW_RESPONSE, response);
		fetchBillLogDataMap.put(LoggingConstants.RESPONSE_TIMESTAMP, sdf.format(new Date()));

		if (!FCUtil.isEmpty(validationResponseMap) && !FCUtil.isEmpty(validationResponseMap.get("responsecode"))) {
			fetchBillLogDataMap.put(LoggingConstants.RESPONSE_CODE, validationResponseMap.get("responsecode"));
		}
		logRequestService.logInTransactionValidation(fetchBillLogDataMap);
		return billPayVerifyResponse;
	}

	private String getBillpaymentMerchantIdByOperatorId(Integer operatorId) {
		String euronetSPCode = "";
		try {
			BillPostpaidMerchant euronetSPCodeObj = billPayService.getPostpaidMerchantByOperatorId(operatorId);
			if (euronetSPCodeObj != null) {
				if (!Strings.isNullOrEmpty(euronetSPCodeObj.getEuronetMerchantId())) {
					euronetSPCode = euronetSPCodeObj.getEuronetMerchantId();
				}
			}
			logger.info("euronet spcode:" + euronetSPCodeObj.getEuronetMerchantId() + "for operator id:" + operatorId);
		} catch (NullPointerException npe) {
			logger.info("Exception in getting Merchant Id:", npe);
		}
		return euronetSPCode;
	}

	private String getAggregatorCircleCodeByMobileNumAndOperatorId(String mobileNumber, Integer operatorId) {
		String circleMasterId = null;
		String aggregatorCircleCode = null;
		try {
			circleMasterId = operatorCircleService.fetchCircleMasterId(mobileNumber);

			List<InAggrOprCircleMap> operatroCircleMap = operatorCircleService.getInAggrOprCircleMap(operatorId,
					Integer.parseInt(circleMasterId));
			for (InAggrOprCircleMap igcm : operatroCircleMap) {
				if (operatorId.equals(igcm.getFkOperatorId()) || circleMasterId.equals(igcm.getFkCircleId())) {
					aggregatorCircleCode = igcm.getAggrCircleCode();
				}
			}
			logger.info("aggregator circle code:" + aggregatorCircleCode + "" + "for circle masterid:" + circleMasterId
					+ " and operatorId:" + operatorId);
		} catch (Exception e) {
			logger.error("Exception while getting circlecode:", e);
		}
		return aggregatorCircleCode;
	}

	public AggregatorResponseDo doCreditRechargeHelper(RechargeDo rechargeDo, String orderNo,
			StringBuffer parametersPassed, String operatorCode, String circleCode, String euronetRechargePlanType,
			Boolean shouldTimeOut, Integer timeOutValue) throws HttpException, ConnectException,
			ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {

		String euroStoreCode = eurostorecode, euroUserName = eurousername, euroUserPass = eurouserpass,
				euroTerminalCode = euroterminalcode;

		logger.info("[IN MODERATOR] processing doRecharge for euronet ", null);

		AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

		logger.info("[IN MODERATOR] processing doRecharge for euronet  with operator : " + operatorCode
				+ " and Circle : " + circleCode, null);

		/**
		 * 
		 * Mandatory Parameter Value Merchantcode ACC - old Username ACC_01 - old
		 * Userpass ACC_01 - old Paymentprovider ACC - old Paymentmode CA - old
		 * Channelcode INR - INR Servicecode DV - need to add this Spcode ETR - need to
		 * check on this as well Sspcode ETRDV - need to check on this as well Storecode
		 * ACC_01 - old Terminalcode ACC_01 - old Customerid TESTATM001 - being injected
		 * from properties file Tcflag 1 - this needs to be added VoucherBrandCode
		 * GOOGLEWEB - this needs to be added
		 * 
		 */
		Calendar cal = Calendar.getInstance();
		Date dt = cal.getTime();

		NameValuePair[] euronetParam = new NameValuePair[24];

		euronetParam[0] = new NameValuePair(MERCHANT_REFERENCE_NO_PARAM, orderNo);
		euronetParam[1] = new NameValuePair("merchantcode", euromerchantcode);
		euronetParam[2] = new NameValuePair("username", euroUserName);
		euronetParam[3] = new NameValuePair("userpass", euroUserPass);
		euronetParam[4] = new NameValuePair("paymentprovider", europaymentprovider);
		euronetParam[5] = new NameValuePair("paymentmode", europaymentmode);
		euronetParam[6] = new NameValuePair("channelcode", eurochannelcode);
		euronetParam[7] = new NameValuePair("servicecode", euroGoogleCreditsServiceCode);
		euronetParam[8] = new NameValuePair("amount", rechargeDo.getAmount().intValue() + "");
		euronetParam[9] = new NameValuePair("storecode", euroStoreCode);
		euronetParam[10] = new NameValuePair("terminalcode", euroTerminalCode);
		euronetParam[11] = new NameValuePair("customerid", euroGoogleCreditsCustomerId);
		euronetParam[12] = new NameValuePair("tcflag", eurotcflag);
		euronetParam[13] = new NameValuePair(SPCODE, operatorCode);
		euronetParam[14] = new NameValuePair(SSPCODE, circleCode);
		euronetParam[15] = new NameValuePair("consumerno", rechargeDo.getMobileNo());

		if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
			euronetParam[16] = new NameValuePair("customeremail", "");
			euronetParam[17] = new NameValuePair("customername", "");
			euronetParam[18] = new NameValuePair("deliveryaddress", "");
			euronetParam[19] = new NameValuePair("deliverycharge", "");
			euronetParam[20] = new NameValuePair("VoucherBrandCodes", "GOOGLEWEB");
			euronetParam[21] = new NameValuePair("VoucherDenominations", rechargeDo.getAmount().intValue() + "");
			euronetParam[22] = new NameValuePair("VoucherQuantities", 1 + "");
			euronetParam[23] = new NameValuePair("discount", 0 + "");

		}

		parametersPassed.append("merchantrefno = " + orderNo + ", ");
		parametersPassed.append("merchantcode = " + euromerchantcode + ", ");
		parametersPassed.append("username = " + euroUserName + ", ");
		parametersPassed.append("userpass = " + euroUserPass + ", ");
		parametersPassed.append("paymentprovider = " + europaymentprovider + ", ");
		parametersPassed.append("paymentmode = " + europaymentmode + ", ");
		parametersPassed.append("channelcode = " + eurochannelcode + ", ");
		parametersPassed.append("servicecode = " + euronetParam[7] + ", ");
		parametersPassed.append("consumerno = " + euronetParam[15] + ", ");
		parametersPassed.append("amount = " + rechargeDo.getAmount().intValue() + ", ");
		parametersPassed.append("storecode = " + euroStoreCode + ", ");
		parametersPassed.append("terminalcode = " + euroTerminalCode + ", ");
		parametersPassed.append("customerid = " + euronetParam[11] + ", ");
		parametersPassed.append("tcflag = " + eurotcflag + ", ");
		parametersPassed.append("spcode = " + euronetParam[13] + ", ");
		parametersPassed.append("sspcode = " + euronetParam[14] + ", ");
		parametersPassed.append("VoucherBrandCodes = " + euronetParam[20] + ", ");
		parametersPassed.append("VoucherDenominations = " + rechargeDo.getAmount().intValue() + ", ");
		parametersPassed.append("customeremail = " + ", ");
		parametersPassed.append("customername = " + ", ");
		parametersPassed.append("deliveryaddress = " + ", ");
		parametersPassed.append("deliverycharge = " + ", ");
		parametersPassed.append("VoucherQuantities = " + 1 + ", ");
		parametersPassed.append("discount = " + 0 + ", ");

		logger.info("[IN MODERATOR] Executing Google Credit Recharge request from euronet for operatorCode : "
				+ operatorCode + " && mobile no :" + rechargeDo.getMobileNo(), null);

		String response = null;
		Map<String, String> topupResponseMap = null;
		if (!appConfig.isEuronetVas2Enabled()) {
			GetMethod method = new GetMethod(this.getRechargeAPI());
			method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
			method.setQueryString(euronetParam);
			logger.info("Euronet request URI " + method.getQueryString());
			try {
				/*int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), httpClient, method,
						shouldTimeOut, timeOutValue);*/
				int statusCode = executeWithoutTimeout(rechargeDo.getAffiliateTransId(), httpClient, method,"euronet");


				if (statusCode == HttpStatus.SC_OK) {
					response = method.getResponseBodyAsString();
				} else {
					logger.info("[IN MODERATOR] Received HTTP status code" + statusCode);
				}
			}catch (ConnectTimeoutException | SocketException e) {
				logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ConnectTimeoutException";
	        } catch (IOException e) {
	        	logger.error("Exception : " + e);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=ReadTimeoutException";
	        } catch (Exception ex) {
				logger.error("Exception : " + ex);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=UnknownException";
			}  finally {
				logger.info("[IN MODERATOR] Releasing connection from IN in doCreditRecharge(finally) for euronet",
						null);
				method.releaseConnection();
			}
		} else {
			try {
				String request = RechargeUtil.createSubmitData(euronetParam, PaymentConstants.UTF_8_ENCODING);
				logger.info("Request going via eurovas2.0 : " + request);
				IEPayServiceProxy client = new IEPayServiceProxy(eurowsdlvas2topupurl.replace("SPCODE", operatorCode));
				response = executeEurovas2WithTimeout(rechargeDo.getAffiliateTransId(), client, request, shouldTimeOut,
						timeOutValue);
			} catch (Exception exception) {
				logger.error("Exception raised while doing credit recharge for merchantrefno : " + orderNo, exception);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
				+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=UnknownException";
			}
		}
		// Need to remove this after QA
		logger.info("[IN MODERATOR] Output of recharge request from Euronet : " + response, null);

		/*
		 * output from above url looks like below
		 * cardamount=1000&cardpin=RLBV-QZD1-AT34-MRJ9-VQG6&cardserial=
		 * 8121628976721239&enrefno=1612131808187B6AAFF5&merchantrefno=
		 * 100820673195574&operatorrefno=1612131808187B6AAFF5&responseaction=
		 * Transaction Successful&responsecode=00&responsemessage=Transaction Successful
		 */

		/* Getting Response Time */
		Calendar cal1 = Calendar.getInstance();
		Date dt1 = cal1.getTime();

		if (response != null && !response.isEmpty()) {
			topupResponseMap = FCUtil.stringToHashMap(response);
			if (CollectionUtils.isEmpty(topupResponseMap)) {
				logger.info("Invalid Response Received  : " + topupResponseMap);
				response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
						+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=EmptyResponseMap";
				topupResponseMap = FCUtil.stringToHashMap(response);

			}
			// replacing the string since we can't store the google code and serial
			if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
				List<String> exclusionList = new ArrayList<String>();
				exclusionList.add("cardpin");
				exclusionList.add("cardserial");
				response = FCUtil.hashMapToString(topupResponseMap, exclusionList);
			}
		} else {
			response = "enrefno=unknown&merchantrefno=0_" + rechargeDo.getInRequestId()
					+ "&operatorrefno=unknown&responseaction=Transaction status not known&responsecode=UP&responsemessage=Empty Response From Aggregator";
			topupResponseMap = FCUtil.stringToHashMap(response);
		}

		String euronetResponseCode = topupResponseMap.get(RESPONSE_CODE_PARAM);
		if (StringUtils.isEmpty(euronetResponseCode)) {
			euronetResponseCode = "UP";
		}

		if (euronetResponseCode != null && !euronetResponseCode.isEmpty()) {
			metricsClient.recordEvent("GoogleRecharge", "Euronet.ResponseCode", euronetResponseCode);
			metricsClient.recordEvent("GoogleRecharge", "Euronet.ResponseCode." + operatorCode, euronetResponseCode);
		}

		aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_EURONET);
		aggregatorResponseDo.setAggrOprRefernceNo(topupResponseMap.get("operatorrefno"));
		aggregatorResponseDo.setAggrReferenceNo(topupResponseMap.get(EURONET_REFERENCE_NO_PARAM));
		aggregatorResponseDo.setAggrResponseCode(euronetResponseCode);
		aggregatorResponseDo.setAggrResponseMessage(topupResponseMap.get(RESPONSE_MESSAGE_PARAM));
		aggregatorResponseDo.setGoogleSerial(topupResponseMap.get(GOOGLE_SERIAL));
		aggregatorResponseDo.setGoogleCode(topupResponseMap.get(GOOGLE_PIN));
		aggregatorResponseDo.setRawRequest(parametersPassed.toString());
		aggregatorResponseDo.setRawResponse(response);
		aggregatorResponseDo.setRequestId(getRequestId(orderNo));
		aggregatorResponseDo.setRequestTime(dt);
		aggregatorResponseDo.setResponseTime(dt1);

		logger.info("topupResponseMap ### : " + topupResponseMap);
		if (CollectionUtils.isEmpty(topupResponseMap)) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);

		} else if (isSuccess(topupResponseMap))
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
		else if (RechargeConstants.EURO_ERROR_CODES.contains(aggregatorResponseDo.getAggrResponseCode())) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
		} else {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
		}
		/*
		 * if (isSuccess(topupResponseMap))
		 * aggregatorResponseDo.setTransactionStatus(RechargeConstants.
		 * IN_TRANSACTION_SUCCESSFUL_MESSAGE); else { if
		 * (this.isEuronetAmbiguousResponse(aggregatorResponseDo))
		 * aggregatorResponseDo.setTransactionStatus(RechargeConstants.
		 * TRANSACTION_STATUS_UNDER_PROCESS); else {
		 * aggregatorResponseDo.setTransactionStatus(RechargeConstants.
		 * TRANSACTION_STATUS_UNSUCCESSFUL); } }
		 */
		return aggregatorResponseDo;
	}

	public void fillTransactionStatus(AggregatorResponseDo aggregatorResponseDo, Map<String, String> topupResponseMap) {

	}

	public AggregatorResponseDo refetchCreditRecharge(RechargeDo rechargeDo, InResponse inResponseFromRequest)
			throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
			IOException {
		String merchantReferenceCode = fcProperties.getInTransIdPrefix() + inResponseFromRequest.getRetryNumber() + "_"
				+ rechargeDo.getInRequestId().toString();
		String euronetTransactionId = inResponseFromRequest.getAggrTransId();
		NameValuePair[] nameValuePairs = new NameValuePair[6];

		nameValuePairs[0] = new NameValuePair("SPCODE", "ETR");
		nameValuePairs[1] = new NameValuePair("SSPCODE", "ETRDV");
		nameValuePairs[2] = new NameValuePair("VOUCHERBRANDCODES", "GOOGLEWEB");
		nameValuePairs[3] = new NameValuePair("MerchantCode", this.getEuronetGCRefetchMerchantCode());
		nameValuePairs[4] = new NameValuePair("EnRefno", euronetTransactionId);
		nameValuePairs[5] = new NameValuePair("MerchantRefno", merchantReferenceCode);

		Map<String, String> responseMap = new HashMap<String, String>();

		String response = "";

		final Date requestTime = new Date();
		Date responseTime = null;

		AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

		final SimpleHttpConnectionManager euronetConnectionManager = new SimpleHttpConnectionManager(true);
		euronetConnectionManager.setParams(connectionManagerConfig);

		HttpClient refetchClient = new HttpClient(euronetConnectionManager);
		refetchClient.setHostConfiguration(euronetHostConfig);
		GetMethod method = new GetMethod(this.getEuronetGCRefetchURL());
		method.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");
		method.setQueryString(nameValuePairs);
		logger.info("Euronet request URI " + method.getQueryString());
		try {
			logger.info("euronet Refetch request param is: " + method.getQueryString() + " and refetch url is : "
					+ this.getEuronetGCRefetchURL());
			int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), refetchClient, method, true, 60000);
			if (statusCode == HttpStatus.SC_OK) {
				response = method.getResponseBodyAsString();
				responseTime = new Date();
			} else {
				logger.info("[IN MODERATOR] Received HTTP status code for Refetch Request:" + statusCode);
				aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
			}

			logger.info("euronet refetch url is " + method.getURI() + " euronet refetch request param is: "
					+ method.getQueryString() + " [IN MODERATOR] Output for Refetch Request from Euronet : " + response,
					null);
			if (response != null && !response.isEmpty()) {
				responseMap = FCUtil.stringToHashMapInLowerCaseKeys(response);
				// replacing the string since we can't store the google code and serial
				if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
					List<String> exclusionList = new ArrayList<String>();
					exclusionList.add("pin");
					exclusionList.add("serialno");
					response = FCUtil.hashMapToString(responseMap, exclusionList);
				}
			}

		} finally {
			logger.info("[IN MODERATOR] Releasing connection from IN in refetchCreditRechargeTransaction for euronet",
					null);
			method.releaseConnection();
		}

		aggregatorResponseDo.setAggrName(AggregatorInterface.EURONET_IN);
		aggregatorResponseDo.setAggrReferenceNo(responseMap.get(EURONET_REFERENCE_NO_PARAM));
		aggregatorResponseDo.setAggrResponseCode(responseMap.get(RESPONSE_CODE_PARAM));
		aggregatorResponseDo.setAggrResponseMessage(responseMap.get("response"));
		aggregatorResponseDo.setRawRequest(nameValuePairs.toString());
		aggregatorResponseDo.setAggrOprRefernceNo(responseMap.get("operatorrefno"));
		aggregatorResponseDo.setGoogleSerial(responseMap.get("serial"));
		aggregatorResponseDo.setGoogleCode(responseMap.get("pin"));
		aggregatorResponseDo.setRawResponse(response);
		aggregatorResponseDo.setRequestTime(requestTime);
		aggregatorResponseDo.setResponseTime(responseTime);

		/*
		 * if (isSuccess(responseMap)) {
		 * aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS)
		 * ; } else { if (this.isEuronetAmbiguousResponse(aggregatorResponseDo)) {
		 * aggregatorResponseDo.setAggregatorErrorWhileQuerying(true); } else {
		 * aggregatorResponseDo.setTransactionStatus(RechargeConstants.
		 * TRANSACTION_STATUS_UNSUCCESSFUL); } }
		 */

		logger.info("topupResponseMap ### : " + responseMap);
		if (CollectionUtils.isEmpty(responseMap)) {
			aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
		} else if (isSuccess(responseMap))
			aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
		else if (RechargeConstants.EURO_ERROR_CODES.contains(aggregatorResponseDo.getAggrResponseCode())) {
			aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
		} else {
			aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
		}
		return aggregatorResponseDo;

	}

	private int getConnectionTimeoutFromAppConfig() {
		int euronetConnectionTimeout = 0;
		try {
			String returnValue = (String) FCUtil.getCustomPropertyFromAppConfig(
					appConfig.getCustomProp(appConfig.VALIDATE_RECHARGE_CUSTOM_CONFIG).toJSONString(),
					"euronet.connection.timeout");
			logger.info("The euronet connection time out value for Validation API is : " + returnValue);
			euronetConnectionTimeout = Integer.parseInt(returnValue);
		} catch (Exception e) {
			logger.error("An error occured trying to get the timeout details from App config", e);
		}
		return euronetConnectionTimeout;
	}

}
