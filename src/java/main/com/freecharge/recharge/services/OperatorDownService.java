package com.freecharge.recharge.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.rest.user.OperatorDownCacheManager;

@Service
public class OperatorDownService {

    @Autowired
    private OperatorDownCacheManager operatorDownCacheManager;

    private static Logger            logger = LoggingFactory.getLogger(OperatorDownService.class);

    public void incOperatorDownQueueCount(Integer operatorId) {
        if (operatorId == null) {
            logger.error("Operator master id is null, Cannot increment the value");
            return;
        }
        try {
            long count = operatorDownCacheManager.incOperatorDownQueueCount(operatorId);
            logger.info("Operator " + operatorId + " down queue count is : " + count);
        } catch (Exception e) {
            logger.error("Some error while incrementing the operator down queue count for operator id" + operatorId, e);
        }
    }

    public void decOperatorDownQueueCount(Integer operatorId) {
        if (operatorId == null) {
            logger.error("Operator master id is null, Cannot deccrement the value");
            return;
        }

        try {
            long count = operatorDownCacheManager.decOperatorDownQueueCount(operatorId);
            if (count <= 0) {
                logger.error("Queue count for operator " + operatorId + " is less than zero");
                return;
            }
            logger.info("Operator " + operatorId + " down queue count is : " + count);
        } catch (Exception e) {
            logger.error("Some error while decrementing the operator down queue count for operator id" + operatorId, e);
        }

    }

    public long getOperatorQueueCount(int operatorId) {
        return operatorDownCacheManager.getOperatorQueueCount(operatorId);
    }
}