package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.productdata.dao.jdbc.InRechargePlanReadDAO;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.recharge.businessDo.EuroValidationResponseDo;
import com.freecharge.recharge.collector.InvalidDenominationCache;
import com.freecharge.recharge.util.CallSourceType;
import com.freecharge.rest.rechargeplans.PlanValidationOperator;
import com.freecharge.web.service.CommonHttpService;
import com.freecharge.web.service.CommonHttpService.HttpResponse;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class PlanValidatorService {

	@Autowired
	private InRechargePlanReadDAO inRechargePlanReadDAO;

	@Autowired
	private EuronetService euronetService;

	@Autowired
	private InvalidDenominationCache denominationCache;	
	
	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	private CommonHttpService commonHttpService;
	
	private static Logger logger = LoggingFactory.getLogger(PlanValidatorService.class);

	private  final static List<String> notFailure =Arrays.asList("00","UP","US");
	
	private final static Integer TD_OPERATOR_ID =5;
	private final static Integer IDEA_OPERATOR_ID =6;
	private final static Integer TD_GSM_DATACARD_OPERATOR_ID = 52;
	
	private static final String INVALID_DENOMINATION_ERROR ="InvalidDenomError";
	private static final String INVALID_DENOMINATION_ERROR_DATACARD = "InvalidDenomDCError";

	private static final String OP_PLAN_VALIDATION_API = "operatorForPlanValidationAPI";

	private static final String PLAN_URL_PROD = "https://api-internal.freecharge.in/rds/plan/info/{operatorId}/{circleId}/{amount}";

	private static final String PLAN_URL_QA = "https://www.freecharge.in/rds/plan/info/{operatorId}/{circleId}/{amount}";

	private static final String PLAN_API_TIMEOUT = "plan.api.timeout";
	
	private static final String DTH_PLAN_VALIDATION_OPERATOR_LIST = "dthPlanValidationOperatorList";
	

	public void populatePlanValidity() {
		List<PlanValidationOperator> planValidationOperatorList = populatePlanValidationOperatorList();
		for (PlanValidationOperator pvo : planValidationOperatorList) {
			Map<String, String> circleNumberMap = populateTestNumbersMap(pvo.getOperatorId());
			if (!FCUtil.isEmpty(circleNumberMap)) {
				Iterator<Entry<String, String>> iterator = circleNumberMap.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry<String, String> pair = (Entry<String, String>) iterator.next();
					validatePlansInCircle(pvo.getOperatorId(), pair.getKey(), pvo.getProductMasterId(),
							pair.getValue(),pvo.getIsPlanSelectionEnabled());
				}
			}
		}

	}

	private List<PlanValidationOperator> populatePlanValidationOperatorList() {
		List<PlanValidationOperator> list = new ArrayList<PlanValidationOperator>();
	 	list.add(new PlanValidationOperator(TD_OPERATOR_ID, ProductMaster.ProductName.Mobile.getProductId(), true));
		list.add(new PlanValidationOperator(IDEA_OPERATOR_ID, ProductMaster.ProductName.Mobile.getProductId(), false));
		list.add(new PlanValidationOperator(TD_GSM_DATACARD_OPERATOR_ID, ProductMaster.ProductName.DataCard.getProductId(), true));
		return list;
	}

	private void validatePlansInCircle(Integer operatorId, String circleId, Integer productId, String testNumber,
			Boolean isPlanSelectionEnabled) {
		List<RechargePlan> inRechargePlans = inRechargePlanReadDAO.getActivePlansOperatorCircle(operatorId,
				Integer.valueOf(circleId), productId);
		List<Task> tasks = new ArrayList<>();
		for (RechargePlan rechargePlan : inRechargePlans) {
			tasks.add(new Task(rechargePlan, testNumber, isPlanSelectionEnabled));
		}

		Set<RechargePlan> userDenominations = denominationCache.getAllDenominationsTried(String.valueOf(operatorId),
				circleId, String.valueOf(productId), fcProperties.getAllDenominationCheckDataWindowDays());

		for (Iterator<RechargePlan> iterator = userDenominations.iterator(); iterator.hasNext();) {
			RechargePlan newPlan = iterator.next();
			tasks.add(new Task(newPlan, testNumber, isPlanSelectionEnabled));
		}

		ExecutorService executor = Executors.newFixedThreadPool(10);
		try {
			executor.invokeAll(tasks);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			executor.shutdown();
		}
	}

	private Map<String, String> populateTestNumbersMap(Integer operatorId) {
		Map<String, String> testNumberMap = null;
		if (operatorId == TD_OPERATOR_ID) {
			testNumberMap = ImmutableMap.<String, String> builder().put("1", "7416658988").put("2", "8121502702")
					.put("3", "8092272862").put("5", "9283142803").put("6", "8092146267").put("7", "9067255525")
					.put("8", "7404221834").put("10", "8904320172").put("11", "8089229365").put("12", "9038721961")
					.put("13", "8871527168").put("14", "9028027026").put("15", "8097213296").put("17", "8984341658")
					.put("18", "8699989585").put("19", "7737133136").put("20", "8122595958").put("21", "7275568132")
					.put("22", "8791914161").put("23", "7501222490").build();
		} else if (operatorId == IDEA_OPERATOR_ID) {
			testNumberMap = ImmutableMap.<String, String> builder().put("1", "9177732754").put("2", "9134460090")
					.put("3", "8228056907").put("5", "9841391187").put("6", "9718155884").put("7", "8154062696")
					.put("8", "9728556918").put("9", "8350970204").put("10", "9844409650").put("11", "9562903203")
					.put("12", "9062545310").put("13", "8224022537").put("14", "9822020173").put("15", "9702097927")
					.put("16", "8188984533").put("17", "9090181052").put("18", "7275502011").put("19", "8742037031")
					.put("20", "9514204896").put("21", "8726688967").put("22", "8192068633").put("23", "8343849285")
					.put("35", "7055425915").build();
		}else if(operatorId == TD_GSM_DATACARD_OPERATOR_ID){
			testNumberMap = ImmutableMap.<String, String> builder().put("1", "9032407917").put("5", "7845474902").put("7", "9033401897")
					.put("8", "7206837099").put("10", "7204494170").put("11", "8089459537")
					.put("12", "8981310563").put("13", "8982463668").put("14", "8793474871").put("15", "8097890605")
					.put("17", "8093145745").put("18", "9056976474").put("19", "7820844737")
					.put("20", "8148415710").put("21", "8604332158").put("22", "9045030871").put("23", "8961748129")
					.put("35", "8439024089").build();
		}

		return testNumberMap;
	}

	public class Task implements Callable<Void> {

		private RechargePlan rechargePlan;
		private String testNumber;
		private Boolean isPlanSelectionEnabled;

		public Task(RechargePlan plan, String testNumber, Boolean isPlanSelectionEnabled) {
			this.rechargePlan = plan;
			this.testNumber = testNumber;
			this.isPlanSelectionEnabled = isPlanSelectionEnabled;
		}

		@Override
		public Void call() throws Exception {
			doValidation(rechargePlan, testNumber);
			if (isPlanSelectionEnabled) {
				String planType = getComplimentaryPlanType(rechargePlan);
				rechargePlan.setName(planType);
				doValidation(rechargePlan, testNumber);
			}
			return null;
		}

		private void doValidation(RechargePlan rechargePlan, String testNumber) {
			EuroValidationResponseDo validationResponse = euronetService.validatePlan(rechargePlan, testNumber ,CallSourceType.AGGREGATOR_SERVICE);
			if (validationResponse != null && isInvalidDenominationResponse(validationResponse.getResponseCode(), rechargePlan.getProductType())) {			

				if (rechargePlan.getOperatorMasterId() == TD_OPERATOR_ID) {
					denominationCache.noteTDInvalidDenomination(rechargePlan.getCircleMasterId(),
							rechargePlan.getAmount().intValue(), validationResponse.getPlanType());
				}else{
					denominationCache.noteValidatedInvalidDenomination(rechargePlan.getOperatorMasterId(), rechargePlan.getCircleMasterId(), rechargePlan.getAmount().intValue(),validationResponse.getPlanType());
				}
			}
		}

		private String getComplimentaryPlanType(RechargePlan rechargePlan) {
			if (rechargePlan.getName() != null && "topup".equalsIgnoreCase(rechargePlan.getName())) {
				return "special";
			} else {
				return "topup";
			}
		}

		@SuppressWarnings("unused")
		private boolean isNotFailure(EuroValidationResponseDo validationResponse) {
			if (notFailure.contains(validationResponse.getResponseCode())){
				logger.info("Success Response for :"+validationResponse);
				return true;
			}
			return false;
		}		
	
	}
	
	public boolean isInvalidDenominationResponse(String agResponse, Integer productMasterId) {
		JSONObject configJson = appConfigService.getCustomProp(appConfigService.RECHARGE_CUSTOM_CONFIG);

		if (configJson != null) {
			String enabledEventListString = (String) configJson.get(INVALID_DENOMINATION_ERROR);

			if (productMasterId == ProductMaster.ProductName.DataCard.getProductId()) {
				enabledEventListString = (String) configJson.get(INVALID_DENOMINATION_ERROR_DATACARD);
			}

			if (!StringUtils.isBlank(enabledEventListString)) {
				List<String> eventList = Arrays.asList(enabledEventListString.split(","));

				if (eventList.contains(agResponse)) {
					return true;
				}

			}
		}
		return false;
	}

	public boolean isNotFailure(EuroValidationResponseDo validationResponse) {
		if (notFailure.contains(validationResponse.getResponseCode())){
			logger.info("Success Response for :"+validationResponse);
			return true;
		}
		return false;
	}

	public boolean isPlanValidThroughValidationAPI(String serviceNumber, String productType, Float rechargeAmount,
			String operator, int circleId, String rechargePlanType) {
		try {
			if (isOpEligibleForPlanValidation(operator)) {
				return isValidPlan(rechargeAmount, operator, String.valueOf(circleId), rechargePlanType);
			}
		} catch (Exception e) {
			logger.error("Caught exception is ", e);
		}
		return true;
	}

	private boolean isValidPlan(Float rechargeAmount, String operator, String circleId, String rechargePlanType) {
		try {
			UriComponentsBuilder builder = null;
			if (fcProperties.isProdMode() || fcProperties.isPreProdMode()) {
				builder = UriComponentsBuilder.fromHttpUrl(PLAN_URL_PROD);
			} else {
				builder = UriComponentsBuilder.fromHttpUrl(PLAN_URL_QA);
			}

			Map<String, Object> pathParams = new HashMap<>();
			pathParams.put("operatorId", operator);
			pathParams.put("circleId", circleId);
			pathParams.put("amount", String.valueOf(rechargeAmount));
			URI uri = builder.buildAndExpand(pathParams).toUri();
			
			try {
				HttpResponse validityResponse = commonHttpService.fireGetRequestWithTimeOut(uri.toURL().toString(),
						new HashMap<String, String>(), fcProperties.getIntProperty(PLAN_API_TIMEOUT));
				if(validityResponse!=null){
					return isPlanValidFromResponse(validityResponse.getResponseBody());
				}				
				
			} catch (IOException ioe) {
				logger.error(
						"IOException caught for planValidation for " + rechargeAmount + " " + operator + " " + circleId,
						ioe);
			} catch (HTTPException te) {
				logger.error("TimeoutException caught for planValidation for " + rechargeAmount + " " + operator + " "
						+ circleId, te);
			}
			return true;
		} catch (Exception e) {
			logger.error("Caught exception is ", e);
		}
		return true;
	}

	private Boolean isPlanValidFromResponse(String planJson) {
		logger.info("Plan details : " + planJson);
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(planJson);
		JsonObject obj = element.getAsJsonObject();
		Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();
		JsonElement temp = null;
		for (Map.Entry<String, JsonElement> entry : entries) {
			temp = entry.getValue().getAsJsonObject().get("rechargePlanMasterId");
			if(temp == null || temp.isJsonNull()){
				return false;
			}
			if(temp.getAsString()!=null && temp.getAsString().equals("null")){
				return false;
			}
		}
		return true;
		
	}

	private boolean isOpEligibleForPlanValidation(String operator) {
		  JSONObject configJson = appConfigService.getCustomProp(appConfigService.RECHARGE_CUSTOM_CONFIG);
	        
	        if (configJson != null) {
	            final String enabledEventListString = (String) configJson.get(OP_PLAN_VALIDATION_API);
	            
	            if (!StringUtils.isBlank(enabledEventListString)) {
	                List<String> eventList = Arrays.asList(enabledEventListString.split(","));
	                
	                if (eventList.contains(operator)) {
	                    return true;
	                }
	                
	            }
	        }
	        
	        return false;
	}
	
	
	/**
	 * This method is used to check if the DTH operator is enabled for plan validation from RDS or not
	 * 
	 * @param operator - the operator id for which plan validation needs to be done
	 * @return boolean - the boolean value true if operator is eligible for Plan Validation else false
	 */
	public boolean isOpEligibleForDTHPlanValidation(String operator) {
		JSONObject configJson = appConfigService.getCustomProp(appConfigService.DTH_PLAN_VALIDATION_CONFIG);
		if (configJson != null) {
			final String opJsonList = (String) configJson.get(DTH_PLAN_VALIDATION_OPERATOR_LIST);
			logger.info("The DTH validation operator list from app config is : " + opJsonList);
			if (!StringUtils.isBlank(opJsonList)) {
				List<String> opList = Arrays.asList(opJsonList.split(","));
				if (opList.contains(operator)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	
	/**
	 * Used to get the plan info from RDS using the product type, operator id, circle id and amount value.
	 * 
	 * @param operatorId - the DTH operator id
	 * @param serviceNumber - the service number of DTH
	 * @param famount - the amount value
	 * @return - HashMap 
	 * 				- the recharge plan map from RDS	
	 */
	public boolean isValidDTHPlan(String operatorId, String serviceNumber, Float famount) {
		String url = fcProperties.getProperty(FCConstants.NEW_PLANS_GET_CATEGORY_API) + "dth/" + operatorId + "/" + 0
				+ "/" + String.valueOf(famount);
		try {
			HttpResponse validityResponse = commonHttpService.fireGetRequestWithTimeOut(url,
					new HashMap<String, String>(), fcProperties.getIntProperty(PLAN_API_TIMEOUT));
			if (validityResponse != null) {
				return isPlanValidFromResponse(validityResponse.getResponseBody());
			}
		} catch (Exception e) {
			logger.error("Exception getting new plans category for " + operatorId + " " + serviceNumber + " " + famount
					+ " Url " + url, e);
		}
		return true;
	}
}
