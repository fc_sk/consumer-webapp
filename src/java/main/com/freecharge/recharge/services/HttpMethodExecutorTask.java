package com.freecharge.recharge.services;

import java.util.concurrent.Callable;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

public class HttpMethodExecutorTask implements Callable<Integer> {
    private final HttpClient httpClient;
    private final HttpMethod method;
    
    public HttpMethodExecutorTask(HttpClient httpClient, HttpMethod method) {
        this.httpClient = httpClient;
        this.method = method;
    }

    @Override
    public Integer call() throws Exception {
        return httpClient.executeMethod(method);
    }

}
