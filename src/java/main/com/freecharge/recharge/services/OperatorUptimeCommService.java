package com.freecharge.recharge.services;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.fulfillment.RechargeNotificationCommandBuilder;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.recharge.businessDo.OperatorUpNotificationDo;
import com.freecharge.rest.model.RechargeRequest;
import com.freecharge.rest.user.OperatorAlertCacheManager;
import com.freecharge.rest.user.UserNotificationCacheManager;
import com.google.gson.Gson;

/**
 * Created by yashveer on 30/5/16.
 */
@Service
public class OperatorUptimeCommService {

    private static final Logger logger = LoggingFactory.getLogger(OperatorUptimeCommService.class);
    private static final String INACTIVE_OPERATOR_LIST_KEY = "inactiveOpList";
    private static final String OPERATOR_UP_NOTIFIED_USER_SET_KEY_PREFIX = "operatorUpNotifiedUserSet_";
    private static final String SMS_NOTIFICATION_URL = "https://freecharge.com/fc/app/?action=recharge&";
    private static final String ANDROID_PUSH_NOTIFICATION_DEEPLINK_URL_PREFIX = "recharge:";
    private static final String IOS_PUSH_NOTIFICATION_DEEPLINK_URL_PREFIX = "freechargeapp://freecharge?action=recharge";
    private static final String TITLE = "Complete your payment now.";
    private static final int USER_INFO_MAX_RETENTION_DAYS = 2;
    private static final Integer POST_CONNECTION_TIMEOUT = 2000;
    private static int NOTIFIED_USER_SET_MAX_SIZE = 1000000;


    // Required fields for push notifications
    private static final String USER_MAIL = "usermail";
    private static final String MESSAGE_ACTION = "messageaction";
    private static final String DEFAULT_MESSAGE_ACTION = "sbs";
    private static final String MESSAGE_TITLE = "messagetitle";
    private static final String MESSAGE_DESC = "messagedesc";
    private static final String MESSAGE_URL = "messageurl";

    @Autowired
    private UserNotificationCacheManager userNotificationCacheManager;

    @Autowired
    private OperatorAlertCacheManager operatorAlertCacheManager;

    @Autowired
    private OperatorAlertService operatorAlertService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private ExternalClientCallService externalClientCallService;

    @Autowired
    @Qualifier("notificationConnectionManager")
    private MultiThreadedHttpConnectionManager notificationConnectionManager;

    /**
     *  Stores the current user details and recharge request against the inactive Operator.
     *  Uses a redis set internally.
     * @param userEmail
     * @param mobileNo
     * @param request
     */
    public void storeImpactedUser(String userEmail, String mobileNo, RechargeRequest request) {
        try {
            OperatorUpNotificationDo notificationDo = createNotificationDo(userEmail, mobileNo, request);
            logger.info(notificationDo.toString());
            Integer operatorId = notificationDo.getOperatorId();
            Integer circleId = notificationDo.getCircleId();

            String operatorKey = userNotificationCacheManager.getKey(operatorId, circleId);
            Integer maxSetSize = Integer.valueOf(fcProperties.getProperty(FCProperties.OPERATOR_UPTIME_NOTIFY_SMSID));
            if(userNotificationCacheManager.getSetCardinality(operatorKey) <= maxSetSize) {
                userNotificationCacheManager.addToSet(operatorKey, notificationDo);
                // configure set expiry to expiry time of alert
                List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlert(operatorId, circleId);
                if (operatorAlerts != null) {
                    OperatorAlert alert = operatorAlerts.get(0);
                    userNotificationCacheManager.expireAt(operatorKey, new DateTime(alert.getEndDateTime().getTime()).plusDays(1).toDate());
                }
                //Add this operator key to inactive list if not present
                userNotificationCacheManager.addToSet(INACTIVE_OPERATOR_LIST_KEY, operatorKey);
            }
            else{
                logger.warn(operatorKey + " set at max capacity. Ignoring user - " + userEmail);
            }
        }
        catch (Exception e){
            logger.info("Error storing user information for - " + userEmail);
        }
        //printMembers(operatorKey);
        //printMembers(INACTIVE_OPERATOR_LIST_KEY);
    }

    private void printMembers(String key){
        Set<?> mySet = userNotificationCacheManager.fetchSetMembers(key);
        if(mySet!=null){
            logger.info(key + " Set size " + mySet.size());
            for(Object obj: mySet){
                logger.info(obj.toString());
            }
        }
    }


    public void checkInactiveOperators() {
        Set<?> inactiveOpSet = userNotificationCacheManager.fetchSetMembers(INACTIVE_OPERATOR_LIST_KEY);
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        DateTime currentDate = new DateTime();
        String dateSuffix = dateFormat.format(currentDate.toDate());
        String notifiedUserSetkey = OPERATOR_UP_NOTIFIED_USER_SET_KEY_PREFIX + dateSuffix;
        if (inactiveOpSet != null) {
            logger.debug("Inactive operator set size - " + inactiveOpSet.size());
            for (Object member : inactiveOpSet) {
                /** 1. check if alert still exists
                 *  2. send notifications to users
                 *  3. remove this entry from list
                 */
                String key = (String) member;
                logger.info("Checking alerts for - " + key);
                Object randomMember = userNotificationCacheManager.fetchSetMember(key);
                OperatorUpNotificationDo userNotificationDo = (OperatorUpNotificationDo) randomMember;

                if(userNotificationDo==null){
                    logger.error("No users exist for - " + key + ". Removing it from list");
                    userNotificationCacheManager.removeFromSet(INACTIVE_OPERATOR_LIST_KEY, key);
                }
                else {
                    if (!(operatorAlertService.isOperatorInAlert(userNotificationDo.getOperatorId(), userNotificationDo.getCircleId())
                            || operatorAlertService.isOperatorInAlert(userNotificationDo.getOperatorId()))) {
                        // send notification

                        while (userNotificationCacheManager.getSetCardinality(key) != 0) {
                            Object setObj = userNotificationCacheManager.pop(key);
                            OperatorUpNotificationDo notificationDo = (OperatorUpNotificationDo) setObj;
                            try {
                                // set timestamp and flags
                                //OperatorUpNotificationDo oldNotificationDo = (OperatorUpNotificationDo) notificationDo.clone();
                                doNotify(notificationDo);
                                // add to current day set and remove it from existing list
                                //userNotificationCacheManager.removeFromSet(key, oldNotificationDo);
                            } catch (Exception e) {
                                logger.info("Error while notifying user -" + notificationDo.getUserEmailId(), e);
                            } finally {
                                // for metrics
                                userNotificationCacheManager.addToSet(notifiedUserSetkey, notificationDo);
                            }

                        }
                    /* 1. clear out the key data if all notifications have been sent
                       2. set expiry date for the logged events
                    */
                        if (userNotificationCacheManager.getSetCardinality(key) == 0) {
                            logger.info("Clearing out data for - " + key);
                            userNotificationCacheManager.removeFromSet(INACTIVE_OPERATOR_LIST_KEY, key);
                            userNotificationCacheManager.expireAt(notifiedUserSetkey, currentDate.plusDays(USER_INFO_MAX_RETENTION_DAYS).withTimeAtStartOfDay().toDate());
                        }
                    } else {
                        logger.info("Alert still valid. Ignoring - " + key);
                    }
                }

            }
        }
    }


    private OperatorUpNotificationDo createNotificationDo(String userEmailId, String mobileNo, RechargeRequest request) {
        OperatorUpNotificationDo notificationDo = new OperatorUpNotificationDo();
        notificationDo.setUserEmailId(userEmailId);
        notificationDo.setUserMobileNo(mobileNo);
        notificationDo.setServiceNumber(request.getServiceNumber());
        notificationDo.setAmount(request.getAmount());
        notificationDo.setOperatorId(Integer.parseInt(request.getOperator()));
        notificationDo.setCircleId(Integer.parseInt(request.getCircleName()));
        notificationDo.setProductType(request.getProductType());
        notificationDo.setDateCreated(System.currentTimeMillis());
        return notificationDo;
    }

    private void doNotify(OperatorUpNotificationDo notificationDo) {
        logger.info("Notifying user - " + notificationDo.getUserEmailId());
        String firstName = userServiceProxy.getUserProfile(notificationDo.getUserEmailId()).getOnlyFirstName();
        String operatorName = operatorCircleService.getOperator(notificationDo.getOperatorId()).getName();
        String circleName = operatorCircleService.getCircle(notificationDo.getCircleId()).getName();
        notificationDo.setFirstName(firstName);
        notificationDo.setOperatorName(operatorName);
        notificationDo.setCircleName(circleName);

        String urlParams = null;
        List<String> transactionOrderIdList = userTransactionHistoryService.findOrderIdByUserEmail(notificationDo.getUserEmailId(), 1);
        String deviceCode = null;
        if(!FCUtil.isEmpty(transactionOrderIdList)){
            String orderId = transactionOrderIdList.get(0);
            logger.debug("Last orderId - " + orderId);
            deviceCode = orderId.substring(3,4);
            logger.debug("Device code - " + deviceCode);

        }
        try {
            if((deviceCode != null) && deviceCode.equals("i")){
                urlParams =  craftUrlParamsIos(notificationDo);
                //sendIOSPush(urlParams, notificationDo);

            }
            else {
                urlParams =  craftUrlParamsAndroid(notificationDo);
                sendAndroidPush(urlParams, notificationDo);
            }
            notificationDo.setNotificationSent(true);
            notificationDo.setNotificationTimestamp(System.currentTimeMillis());
        }catch (Exception e) {
            //logger.info("Exception while sending payment retry SMS for orderId: " + orderId, e);
            logger.error("Exception sending push notification for email - " + notificationDo.getUserEmailId(), e);
        }

        try{
            sendSMS(urlParams, notificationDo);
            notificationDo.setSmsSent(true);
            notificationDo.setSmsTimestamp(System.currentTimeMillis());
        }
        catch (Exception e){
            logger.error("Exception sending SMS notification for email - " + notificationDo.getUserEmailId(), e);
        }

    }


    private void sendSMS(String urlParams, OperatorUpNotificationDo notificationDo) {
            String messageUrl = ("https://freecharge.com/fc/app/?action=recharge&" + urlParams);
            String bitlyUrl = getBitlyUrlFromUrl(messageUrl);
            logger.debug("bitlyUrl for sms: " +bitlyUrl);

            Map<String, String> templateDataMap = new HashMap<String, String>();
            //templateDataMap.put("name", notificationDo.getFirstName());
            templateDataMap.put("amount", notificationDo.getAmount().toString());
            //templateDataMap.put("operatorName", notificationDo.getOperatorName());
            templateDataMap.put("rechargeNumber", notificationDo.getServiceNumber());
            templateDataMap.put("bitlyUrl", bitlyUrl);

            List<String> tags = new ArrayList<String>();
            //tags.add(orderId);

            Map<String, Object> reqMap = new HashMap<String, Object>();
            reqMap.put("smsId", Integer.valueOf(fcProperties.getProperty(FCProperties.OPERATOR_UPTIME_NOTIFY_SMSID)));
            reqMap.put("templateDataMap", templateDataMap);
            reqMap.put("contactNumber", notificationDo.getUserMobileNo());
            reqMap.put("tags", tags);

            String postUrl = fcProperties.getProperty(FCProperties.RETRY_SMS_END_POINT);
            postHttpMessage(postUrl, reqMap);

    }

    private String getBitlyUrlFromUrl(String url) {
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod("https://api-ssl.bitly.com/v3/shorten");
        NameValuePair[] urlParam = new NameValuePair[4];
        try {
            urlParam[0] = new NameValuePair("access_token", "80bfc330a5521624fd75671095dc968ba0665e0f");
            urlParam[1] = new NameValuePair("longUrl", url);
            urlParam[2] = new NameValuePair("domain", "bit.ly");
            urlParam[3] = new NameValuePair("format", "json");
            method.setQueryString(urlParam);
            client.executeMethod(method);
            String response = method.getResponseBodyAsString();
            logger.debug("Bitly url response:" + response);
            JSONObject jsonObj = new JSONObject(response);
            JSONObject shortUrl = (JSONObject) jsonObj.get("data");
            return (String) shortUrl.get("url");
        } catch (Exception exception) {
            logger.error("not able to access bitly api", exception);
        }
        return url;
    }

    private void sendAndroidPush(String urlParams, OperatorUpNotificationDo notificationDo) {
        String messageUrl = (ANDROID_PUSH_NOTIFICATION_DEEPLINK_URL_PREFIX + urlParams);
        logger.debug("mesageUrl :" + messageUrl);
        // android default


        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("rechargeNumber", notificationDo.getServiceNumber());
        Map<String, String> descriptionMap = new HashMap<String, String>();
        //descriptionMap.put("name", notificationDo.getFirstName());
        descriptionMap.put("amount", notificationDo.getAmount().toString());
        descriptionMap.put("rechargeNumber", notificationDo.getServiceNumber());
        descriptionMap.put("messageUrl", messageUrl);

        Map<String, Object> reqMap = new HashMap<String, Object>();
        reqMap.put("templateId", Integer.valueOf(fcProperties.getProperty(FCProperties.OPERATOR_UPTIME_NOTIFY_ANDROID_TEMPLATE_ID)));
        reqMap.put("titleMap", titleMap);
        reqMap.put("descriptionMap", descriptionMap);
        reqMap.put("emailId", notificationDo.getUserEmailId());

        String pushUrl = fcProperties.getProperty(FCProperties.ANDROID_RETRY_PUSH_ENDPOINT);

        postHttpMessage(pushUrl, reqMap);
    }

    private void sendIOSPush(String urlParams, OperatorUpNotificationDo notificationDo) {
        String messageUrl = IOS_PUSH_NOTIFICATION_DEEPLINK_URL_PREFIX + urlParams;
        logger.debug("ios messageUrl :" +messageUrl);
        String message = "ABC";//getRetryMessageForProductType(productType, firstName, amount, operatorName);
        String pushUrl = fcProperties.getProperty(FCProperties.IOS_RETRY_PUSH_ENDPOINT);
        Map<String, String> payLoadMap = new HashMap<String, String>();
        payLoadMap.put("Title", TITLE);
        payLoadMap.put("message", message);
        payLoadMap.put("url", messageUrl);

        Map<String, Object> reqMap = new HashMap<String, Object>();
        reqMap.put("templateId", fcProperties.getProperty(FCProperties.RETRY_CAMPAIGN_TOOL_IOSTEMPLATEID));
        reqMap.put("userEmail", notificationDo.getUserEmailId());
        reqMap.put("payLoadMap", payLoadMap);

        postHttpMessage(pushUrl, reqMap);
    }


    private String craftUrlParamsAndroid(OperatorUpNotificationDo notificationDo) {
        StringBuilder urlParams = new StringBuilder();
        String productType = notificationDo.getProductType();
        String type = "";
        switch (productType) {
            case "V":
                type = "prepaid";
                break;

            case "M":
                type = "postpaid";
                break;

            case "D":
                type = "dth";
                break;

            case "C":
            case "F":
                type = "datacard";
                break;

        }
        urlParams.append("type=").append(type).append("&number=").append(notificationDo.getServiceNumber())
                .append("&amount=").append(notificationDo.getAmount()).append("&operator=").append(notificationDo.getOperatorName()).append("&circle=").append(notificationDo.getCircleName());

        return urlParams.toString();

    }

    private String craftUrlParamsIos(OperatorUpNotificationDo notificationDo) {
        StringBuilder urlParams = new StringBuilder();
        String productType = notificationDo.getProductType();
        urlParams.append("&number=").append(notificationDo.getServiceNumber()).append("&productType=").append(productType)
                .append("&amount=").append(notificationDo.getAmount()).append("&operator=").append(notificationDo.getOperatorName()).append("&circle=")
                .append(notificationDo.getCircleName()).append("&operatorId=").append(notificationDo.getOperatorId()).append("&circleId=")
                .append(notificationDo.getCircleId());
        return urlParams.toString();

    }

    @SuppressWarnings("deprecation")
    private void postHttpMessage(String postUrl, Map reqMap) {
        HttpClient client = new HttpClient();
        client.setHttpConnectionManager(notificationConnectionManager);
        PostMethod postMethod = new PostMethod(postUrl);
        postMethod.setRequestHeader("Content-Type", "application/json");
        Gson gson = new Gson();
        String postData = gson.toJson(reqMap);
        logger.info("Posting json - " + postData);
        logger.info("Post URL - " + postUrl);
        postMethod.setRequestEntity(new StringRequestEntity(postData));
        int postStatus;
        InputStream postResponse;
        try {
            postStatus = externalClientCallService.executeWithTimeOut(RechargeNotificationCommandBuilder
                    .getNotificationServiceResponseCommand(client, postMethod, "Fulfillment", "SuccessNotification"));
            postResponse = postMethod.getResponseBodyAsStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (HttpStatus.SC_OK == postStatus) {
            logger.debug("Web service response for Notification : " + postResponse.toString());
            //recordPaymentRetryNotification(orderId, emailId);
        } else {
            throw new RuntimeException("Error sending Notification with status: " + postStatus + " with response: " + postResponse.toString());
        }
    }


}


