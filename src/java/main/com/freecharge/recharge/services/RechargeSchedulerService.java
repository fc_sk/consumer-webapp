package com.freecharge.recharge.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Calendar;

import com.freecharge.app.domain.entity.UserTransactionHistory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.RewardRechargeDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.RewardRecharge;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.EuronetCallbackRequest;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.google.GoogleCreditsService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.payment.autorefund.ForceRefundService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

import flexjson.JSONSerializer;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: May 19, 2012 Time: 11:35:20 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class RechargeSchedulerService {
    private static final int FIFTEEN_MINUTES = 900;
    private static Logger logger = LoggingFactory.getLogger(RechargeSchedulerService.class);
    private JSONSerializer jsonSerializer = new JSONSerializer();

    @Autowired
    AggregatorFactory aggregatorFactory;

    @Autowired
    private FulfillmentService fulfillmentService;

    @Autowired
    private CommonRechargeService commonRechargeService;

    @Autowired
    private InService inService;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    HomeBusinessDao homeBusinessDao;

    @Autowired
    InReadDAO inReadDAO;
    
    @Autowired
    private RewardRechargeDAO rewardRechargeDAO;

    @Autowired
    FCProperties fcProperties;

    @Autowired
    KestrelWrapper kestrelWrapper;
    
    @Autowired
    MetricsClient metricsClient;
    
    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    AppConfigService appConfigService;
    
    @Autowired
    UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    @Autowired
    ForceRefundService forceRefundService;
    
    @Autowired
    GoogleCreditsService googleCreditsService;
    
    private final int MAX_STATUS_CHECK_COUNT = 3;
    private final int FIRST_STATUS_CHECK = 0;

    public void getRetryRechargeStatus(Integer m1, Integer m2) {
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -m1); 
        Date from = DateUtils.addMinutes(to, -m2);
        logger.info("checking recharge status from " + from + " to " + to);
        
        processRechargeStatusAsync(from, to, "Pending." + m2.toString());
    }
    
    public void rechargeStatusForTMinisSixMinusTwoDays() {
        Date now = new Date();
        Date to = DateUtils.addDays(now, -2);
        Date from = DateUtils.addDays(now, -6);
        from = FCDateUtil.getStartOfDay(from);
        processRechargeStatusAsync(from, to, "ManualInitiator");
    }
    
    private void processRechargeStatusAsync(Date from, Date to, String metricName) {
        List<String> orderIdList = inService.getPendingRechargesInTimeRange(from, to);
        
        long secondsBetween = (to.getTime()- from.getTime())/1000;
        
        metricsClient.recordCount("Recharge", metricName, orderIdList.size());
        
        for (String orderId : orderIdList) {
            if (secondsBetween <= FIFTEEN_MINUTES) {
                kestrelWrapper.enqueueForQuickStatusCheck(orderId, StatusCheckType.Relaxed);
            } else {
                kestrelWrapper.enqueueForStatusCheck(orderId, StatusCheckType.Relaxed);
            }
        }
    }
    
    public void checkStatusAndUpdate(String orderId, StatusCheckType statusCheckType, InResponse inResponse, boolean isStartupTxnStatusChk) {
        logger.info("Processing status check for [ " + inResponse.getAggrName() + "]: aggregator tranasction ID: " + inResponse.getAggrName() + " with initial response of : " + inResponse.getAggrRespCode());
        
        InResponse originalResponse = inResponse.clone();
        InRequest inRequest = inService.getInRequest(inResponse.getRequestId());
 
        RechargeDo rechargeDo = createRechargeDO(inResponse);
        
        int statusCheckTryNumber = 0;
        
        while (shouldCheckStatus(statusCheckTryNumber, statusCheckType, inResponse, isStartupTxnStatusChk)) {
            statusCheckTryNumber++;
            this.checkRechargeStatus(inResponse, rechargeDo, aggregatorFactory);

            if (!inResponse.getInRespCode().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
               processRechargeStatusChange(inRequest, inResponse, originalResponse, false,isStartupTxnStatusChk);
               metricsClient.recordEvent("Recharge", "UnderProcess." + rechargeDo.getOperatorCode(), "Updated");
               break;
            } else {
                metricsClient.recordEvent("Recharge", "UnderProcess." + rechargeDo.getOperatorCode(), "NotUpdated");
            }
        }
    }
    
    private boolean shouldCheckStatus(int statusCheckTryNumber, StatusCheckType statusCheckType, InResponse inResponse,boolean isStartupTxnStatusChk) {
        if (inResponse.getInRespCode().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE ) || isStartupTxnStatusChk) {
            if (statusCheckTryNumber == FIRST_STATUS_CHECK) {
                return true;
            } else if (statusCheckType == StatusCheckType.Aggressive) {
                if (statusCheckTryNumber < MAX_STATUS_CHECK_COUNT) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * Returns true if it's safe to act on failed recharge. This depends on
     * a bunch of factors; such as wheather the original recharge had timed out
     * ; if it was in flight etc., Read the code, which is fairly self explanatory,
     * if you really want to know!
     * @param inResponse
     * @return true if it's safe to act on failed recharge status response.
     */
    private boolean canActOnFailedStatus(InResponse inResponse) {
        final String TIMEOUT_STATUS_CODE = "IN_IOE";
        
        Long inFlightStatusCheckDelay = fcProperties.getLongProperty(FCConstants.RECHARGE_SCHEDULER_TIME_DIFFERENCE);
        Long timedOutStatusCheckDelay = fcProperties.getLongProperty(FCConstants.TIMED_OUT_STATUS_CHECK_DELAY);
        Long underProcessStatusCheckDelay = fcProperties.getLongProperty(FCConstants.UNDER_PROCESS_STATUS_CHECK_DELAY);
        
        String pendingRechargeStatusCode = fcProperties.getProperty(FCConstants.RECHARGE_TRANSACTION_PENDING_STATUS_CODE);
        Long timeDifference = getTimeDifferenceInSeconds(inResponse.getCreatedTime());

        logger.info("timeDifference in seconds " + timeDifference + " inResponse code " + inResponse.getInRespCode());

        boolean hasTimedOut = inResponse.getAggrRespCode().equals(TIMEOUT_STATUS_CODE);
        boolean isInFlight = inResponse.getInRespCode().equals(pendingRechargeStatusCode);
        boolean isUnderProcess = inResponse.getInRespCode().equals(RechargeConstants.IN_UNDER_PROCESS_CODE) && !hasTimedOut;
        
        boolean isUnderProcessSafeToAct = isUnderProcess && timeDifference > underProcessStatusCheckDelay;
        boolean isInFlightSafeToAct = isInFlight && timeDifference > inFlightStatusCheckDelay;
        boolean isTimeOutSafeToAct = hasTimedOut && timeDifference > timedOutStatusCheckDelay;
        
        boolean canDoStatusCheck = isUnderProcessSafeToAct || isInFlightSafeToAct || isTimeOutSafeToAct;
        
        logger.info("canActOnFailedStatus for responded : " + canDoStatusCheck + "; isUnderProcessSafeToAct: "
                + isUnderProcessSafeToAct + " isInFlightSafeToCheck: " + isInFlightSafeToAct
                + "; isTimeOutSafeToCheck: " + isTimeOutSafeToAct);
        
        return canDoStatusCheck;
    }
    
    public void saveResponseAndInitiateFulfillment(InRequest inRequest, InResponse inResponse,Boolean isStatusCheck, boolean isStartupTxnStatusChk) {

        Long requestId = inResponse.getRequestId();
        String orderId = inRequest.getAffiliateTransId();

		if (requestId != null) {
			if (isStatusCheck) {
				if (!inService.updateStatusChange(inResponse)) {
					logger.info("InResponse already updated for requestId:" + requestId);
					return;
				}
			} else if(isStartupTxnStatusChk) {
				if (!inService.updateStartupImpactedTransactions(inResponse)) {
					logger.info("InResponse already updated for requestId:" + requestId);
					return;
				}
			} else {
				inService.update(inResponse);
			}
            logger.info("updated in_response " + requestId);
            InTransactions latestTransaction = inService.getLatestInTransactionsByRequestIdAndAggregaor(requestId, inResponse.getAggrName());
            updateInTransaction(latestTransaction, inResponse);
            logger.info("updated in_transaction " + requestId);
            metricOperatorRetry(inRequest, inResponse);
            if (isRewardRechargeStatusCheck(inResponse, orderId)) {
                logger.info("Skipping fulfilment for reward orderId :" + orderId);
                // skip fulfillment
                return;
            }
            
            Map<String, String> fulfillmentRequest = new HashMap<>();
            fulfillmentRequest.put(PaymentConstants.ORDER_ID_KEY, orderId);
            fulfillmentRequest.put(PaymentConstants.SHOULD_SEND_SMS_KEY,
                    Boolean.TRUE.toString());

            kestrelWrapper.enqueueFulfillment(fulfillmentRequest);
        } else {
            logger.error("Something bad happend; null requestID for orderID: [" + orderId
                    + "]. No fulfillment");
        }
    
    }

    private boolean isRewardRechargeStatusCheck(InResponse inResponse, String orderId) {
        if(FCUtil.validateOrderIdFormat(orderId)){
            return false;
        }
        RewardRecharge rewardRecharge = rewardRechargeDAO.getRewardRecharge(orderId);
        boolean isRewardRecharge = (rewardRecharge != null);
        if (isRewardRecharge) {
            logger.info("Enqueuing reward response for order ID: " + orderId + " with status:"
                    + inResponse.getInRespCode());
            Map<String, String> responseMap = new HashMap<>();
            responseMap.put("success", Boolean.toString(inResponse != null && inResponse.isSuccessful()));
            responseMap.put("txnReferenceNumber", rewardRecharge.getSourceOrderId());
            responseMap.put("requestId", rewardRecharge.getOrderId());
            kestrelWrapper.enqueueRewardRechargeResponse(responseMap);
        }
        return isRewardRecharge;
    }

    private void metricOperatorRetry(InRequest inRequest, InResponse inResponse) {
        if (inResponse.getRetryNumber() != null && inResponse.getRetryNumber() > 0) {
            Map<String, Object> map = homeBusinessDao.getUserRechargeDetailsFromOrderId(inRequest.getAffiliateTransId());

            if (map == null || map.size() < 1) {
                logger.error(" Could not get the user details for the order ID " + inRequest.getAffiliateTransId());
                return;
            }

            String operatorName = (String) map.get("operatorName");
            String circleName = (String) map.get("circleName");

            CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(
                    operatorName, circleName);
            OperatorMaster retryOperatorMaster = operatorCircleService.getOperator(inRequest.getOperator());
            if (retryOperatorMaster!=null && retryOperatorMaster.getOperatorMasterId() != circleOperatorMapping.getFkOperatorMasterId()) {
                if (AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(inResponse.getAggrRespCode())) {
                    metricsClient.recordEvent("Recharge", "OperatorRetry.Success", circleOperatorMapping.getFkOperatorMasterId() + "-" + retryOperatorMaster.getOperatorMasterId());
                } else {
                    metricsClient.recordEvent("Recharge", "OperatorRetry.Failure", circleOperatorMapping.getFkOperatorMasterId() + "-" + retryOperatorMaster.getOperatorMasterId());
                }
            }
        }
    }

    private void updateInTransaction(InTransactions latestTransaction, InResponse inResponse) {
        if (latestTransaction != null && latestTransaction.getAggrResponseCode() != null
                && !latestTransaction.getAggrResponseCode().equalsIgnoreCase(inResponse.getAggrRespCode())) {
            latestTransaction.setAggrResponseCode(inResponse.getAggrRespCode());
            latestTransaction.setResponseTime(inResponse.getUpdatedTime());
            latestTransaction.setRawResponse(inResponse.getRawResponse());
            latestTransaction.setResponseTime(inResponse.getUpdatedTime());
            latestTransaction.setAggrResponseMessage(inResponse.getAggrRespMsg());
            if (AggregatorInterface.RECHARGE_SUCCESS_AGGREGATOR_RESPONSES.contains(inResponse.getAggrRespCode())) {
                latestTransaction.setTransactionStatus(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
            } else {
                latestTransaction.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            }
            inService.update(latestTransaction);
        }
    }

    public String getRechargeStatus(String requestId) throws Exception {
    	
    		String aggrOperatorRefernceNo="";
    		String aggrRefernceNo="";
    		logger.info("request for recharge status for request id" + requestId);
    		String inResponseStatus="";
    		InRequest inRequestObj = inReadDAO.getInRequest(Long.parseLong(requestId));
    		InResponse inResponseObj = inReadDAO.getInResponseByRequestId(Long.parseLong(requestId));
    		if(inRequestObj!=null) {
    		aggrOperatorRefernceNo = inResponseObj.getOprTransId();	
    		aggrRefernceNo = inResponseObj.getAggrTransId();
			RechargeDo rechargeDo = new RechargeDo();
			rechargeDo.setInRequestId(inResponseObj.getRequestId());
			rechargeDo.setAffiliateTransId(inRequestObj.getAffiliateTransId());
			rechargeDo.setTransactionDate(inResponseObj.getCreatedTime());
			rechargeDo.setMobileNo(inRequestObj.getSubscriberNumber());
			rechargeDo.setProductType(inRequestObj.getProductType());
			AggregatorInterface aggregatorInterface = aggregatorFactory.getAggregator(inResponseObj.getAggrName());
			AggregatorResponseDo aggregatorResponseDo = aggregatorInterface.getTransactionStatus(rechargeDo, inResponseObj);
			if(aggregatorResponseDo!=null && aggregatorResponseDo.getAggrResponseCode()!=null && !aggregatorResponseDo.getAggrResponseCode().isEmpty()) {
				String aggrResponseCode= aggregatorResponseDo.getAggrResponseCode();
				if(aggregatorResponseDo.getAggrReferenceNo()==null || aggregatorResponseDo.getAggrReferenceNo().isEmpty()) {
					aggregatorResponseDo.setAggrReferenceNo(aggrRefernceNo);
				} else {
				if(aggregatorResponseDo.getAggrOprRefernceNo()==null || aggregatorResponseDo.getAggrOprRefernceNo().isEmpty()) {
					aggregatorResponseDo.setAggrOprRefernceNo((aggrOperatorRefernceNo));
				 }
			   }
				if(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE.equals(aggrResponseCode) || RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE.equals(aggrResponseCode)) {
					inResponseStatus=RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE;
				} else {
					inResponseStatus = RechargeResponseStatusCode.getFundSourceValue(aggrResponseCode, requestId, inRequestObj.getAffiliateTransId());
				}
				inResponseStatus = inResponseStatus +"\n" + "aggr_trans_id: " + aggregatorResponseDo.getAggrReferenceNo() + "\n"+ "opr_trans_id: " +  aggregatorResponseDo.getAggrOprRefernceNo();
				
				return inResponseStatus;
			}
		} 
	
		return RechargeConstants.RECHARGE_NOT_ATTEMPTED; 
   }
    
    public static enum RechargeResponseStatusCode {
    	 ES, US, UP, _0,_00;

        public static String getFundSourceValue(String val, String requestId, String orderId) {
            try {
                return RechargeResponseStatusCode.valueOf(val).getDescriptionString();
            } catch (Exception exception) {
            	logger.info("exception occured while checking recharge status for CS Panel for requestid  " + requestId +"order Id  " + orderId + exception);
                return RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL;
            }
        }
        public String getDescriptionString() {
        switch (this) {
		case ES:	
		return RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
		case US:	
		return RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
		case UP:	
		return RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
		default:
		return RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL;
		}
       }
    }
    /*
     *  We are getting time difference b/w in_response table created_time column and current time
     *   
     */
    private Long getTimeDifferenceInSeconds(Date inResponseRequestCreatedTime) {
        Long inResponseRequestTimeInMilliSeconds = inResponseRequestCreatedTime.getTime();
        Long currentTimeInMilliSeconds = new Date().getTime();
        Long timeDifferenceInseconds = (currentTimeInMilliSeconds - inResponseRequestTimeInMilliSeconds)/1000;
        return timeDifferenceInseconds;
    }
    
    public void checkRechargeStatus(InResponse inResponse, RechargeDo rechargeDo, AggregatorFactory aggregatorFactory) {
        AggregatorInterface aggregatorInterface = aggregatorFactory.getAggregator(inResponse.getAggrName());
        AggregatorResponseDo aggregatorResponseDo = null;
        try {
            aggregatorResponseDo = aggregatorInterface.getTransactionStatus(rechargeDo, inResponse);
            String responseCode = aggregatorResponseDo.getAggrResponseCode();
            logger.info("status  for request Id "+inResponse.getRequestId() + responseCode);
            updateInResponseFromResponseCode(responseCode, inResponse, aggregatorResponseDo.getAggrResponseMessage(),
                    aggregatorResponseDo.getRawResponse(), rechargeDo);
            if (!FCUtil.isEmpty(aggregatorResponseDo.getAggrReferenceNo())) {
                inResponse.setAggrTransId(aggregatorResponseDo.getAggrReferenceNo());
            }
            if (!FCUtil.isEmpty(aggregatorResponseDo.getAggrOprRefernceNo())) {
                inResponse.setOprTransId(aggregatorResponseDo.getAggrOprRefernceNo());
            }
            
          //condition added to cache the google response in case of successful status check
			if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
				if (RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(inResponse.getInRespCode())) {
					saveGCreditsToCache(rechargeDo.getAffiliateTransId(), aggregatorResponseDo.getGoogleSerial(),
							aggregatorResponseDo.getGoogleCode());
				}
			}
        } catch (Exception e) {
            logger.error("Error in processing transaction status and recharge process from scheduler for Aggregator " + inResponse.getAggrName() + " : " + e);
        }
    }
    
    public boolean updateStatusForEuronetCallback(InRequest inRequest, InResponse inResponse,
            EuronetCallbackRequest euronetCallbackRequest) {
       if(appConfigService.isDBUpdateOnCallBackEnabled()){
    	InResponse originalResponse = inResponse.clone();
        RechargeDo rechargeDo = createRechargeDO(inResponse);       
        updateInResponseFromResponseCode(euronetCallbackRequest.getResponsecode(), inResponse, euronetCallbackRequest.getResponsemessage(), euronetCallbackRequest.toString(), rechargeDo);
        updateAggrTransIdEuronetCallback(euronetCallbackRequest.getResponsecode(),inResponse,euronetCallbackRequest,rechargeDo);
        if (!inResponse.getInRespCode().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
            if(inResponse.getInRespCode().equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_SUCCESS)){
            	if(FCUtil.isEmpty(euronetCallbackRequest.getEnrefno())){
            		logger.info("enRefNo is empty in SUCCESS callback request .. so not updating DB");
            		return false;
            	}
            }
        	processRechargeStatusChange(inRequest, inResponse, originalResponse, true,false);
            return true;
        }
       }
        return false;
    }

    private void updateAggrTransIdEuronetCallback(String responseCode, InResponse inResponse,
			EuronetCallbackRequest euronetCallbackRequest, RechargeDo rechargeDo) {
    	   if (responseCode != null
                   && responseCode.equalsIgnoreCase(RechargeConstants.EURONET_TRANSACTION_NOT_FOUND_RESPONSE_CODE)) {              
               inResponse.setAggrTransId(euronetCallbackRequest.getEnrefno());             
           } else {
               if (RechargeUtil.isFailedAggrResponseCode(responseCode)) {
                   logger.info("Recharge Status from Recharge Scheduler for order id:" + rechargeDo.getAffiliateTransId()
                           + ", operator id : " + rechargeDo.getCircleOperatorMapping() + " and circle id : "
                           + rechargeDo.getCircleOperatorMapping().getFkCircleMasterId() + " == " + inResponse.toString(),
                           null);
                   inResponse.setAggrTransId(euronetCallbackRequest.getEnrefno());                   
               } else if (RechargeUtil.isSuccessAggrResponseCode(responseCode)) {               
                   inResponse.setAggrTransId(euronetCallbackRequest.getEnrefno());                  
               } else if (RechargeUtil.isUnderProcessAggrResponseCode(responseCode)) {                   
                   inResponse.setAggrTransId(euronetCallbackRequest.getEnrefno());                   
               }
           }
		
	}

	private void processRechargeStatusChange(InRequest inRequest, InResponse inResponse, InResponse originalResponse,
            boolean isCallback,  boolean isStartupTxnStatusChk) {
		if (RechargeUtil.isSuccessInResponseCode(inResponse.getInRespCode())) {
			Map<String, Object> updatedUserDetail = new HashMap<String, Object>();
			updatedUserDetail.put("circleName", inRequest.getCircle());
			updatedUserDetail.put("operatorName", inRequest.getOperator());
			updatedUserDetail.put("productType", inRequest.getProductType());
			userTransactionHistoryService.createOrUpdateRechargeHistory(inResponse.getAggrTransId(), updatedUserDetail,
					RechargeConstants.IN_UNDER_PROCESS_CODE);
		}

		
		if (RechargeUtil.isSuccessInResponseCode(inResponse.getInRespCode()) && FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(inRequest.getProductType())) {
			logger.info("Google Credits Response received for order id : " + inResponse.getAggrTransId());
			//googleCreditsService.
		}
        /*
         * If it's success then go ahead and fulfill; if it's failed then check
         * if it's safe to fulfill i.e., do refund
         */
        if (RechargeUtil.isFailedInResponseCode(inResponse.getInRespCode())) {
            if (isCallback || canActOnFailedStatus(originalResponse)) {
                saveResponseAndInitiateFulfillment(inRequest,inResponse,true,isStartupTxnStatusChk);   
                String orderId =inRequest.getAffiliateTransId();
                UserTransactionHistory uth = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId);
                try{
                forceRefundService.adjustAmountRefunded(uth);
                }catch(Exception e){
                	logger.error("Exception occured while decrementing Forced Refund Amount ",e);
                }
            }
        } else {
        	saveResponseAndInitiateFulfillment(inRequest,inResponse,true,isStartupTxnStatusChk);
        }
    }    

	private void updateInResponseFromResponseCode(String responseCode, InResponse inResponse, String aggrRespMessage, String rawResponse,
            RechargeDo rechargeDo) {
        if (responseCode != null
                && responseCode.equalsIgnoreCase(RechargeConstants.EURONET_TRANSACTION_NOT_FOUND_RESPONSE_CODE)) {
            inResponse.setAggrRespCode(responseCode);
            inResponse.setInRespCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
            inResponse.setAggrRespMsg(aggrRespMessage);
            inResponse.setInRespMsg(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            inResponse.setRawResponse(rawResponse);
        } else {
            if (RechargeUtil.isFailedAggrResponseCode(responseCode)) {
                logger.info("Recharge Status from Recharge Scheduler for order id:" + rechargeDo.getAffiliateTransId()
                        + ", operator id : " + rechargeDo.getCircleOperatorMapping() + " and circle id : "
                        + rechargeDo.getCircleOperatorMapping().getFkCircleMasterId() + " == " + inResponse.toString(),
                        null);

                inResponse.setInRespCode(RechargeConstants.IN_PERMANENT_FAILURE_RESPONSE_CODE);
                inResponse.setInRespMsg(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                inResponse.setAggrRespMsg(aggrRespMessage);
                inResponse.setAggrRespCode(responseCode);
                inResponse.setRawResponse(rawResponse);
            } else if (RechargeUtil.isSuccessAggrResponseCode(responseCode)) {
                inResponse.setAggrRespCode(responseCode);
                inResponse.setInRespCode(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE);
                inResponse.setAggrRespMsg(aggrRespMessage);
                inResponse.setInRespMsg(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
                inResponse.setRawResponse(rawResponse);
            } else if (RechargeUtil.isUnderProcessAggrResponseCode(responseCode)) {
                inResponse.setAggrRespCode(responseCode);
                inResponse.setInRespCode(RechargeConstants.IN_UNDER_PROCESS_CODE);
                inResponse.setAggrRespMsg(aggrRespMessage);
                inResponse.setInRespMsg(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
                inResponse.setRawResponse(rawResponse);
            }
        }
    }
    
    public RechargeDo createRechargeDO(InResponse inResponse) {
        InRequest inRequest = inService.getInRequest(inResponse.getRequestId());
        
        OperatorMaster operatorMaster = operatorCircleService.getOperator(inRequest.getOperator());
        CircleMaster circleMaster = operatorCircleService.getCircle(inRequest.getCircle());
        
        RechargeDo rechargeDo = new RechargeDo();
        rechargeDo.setAffiliateid(inRequest.getAffiliateId());
        rechargeDo.setAffiliateTransId(inRequest.getAffiliateTransId());
        rechargeDo.setAmount(inRequest.getAmount());
        rechargeDo.setCircleOperatorMapping(operatorCircleService.getCircleOperatorMapping(operatorMaster.getOperatorMasterId(), circleMaster.getCircleMasterId()));
        rechargeDo.setOperatorCode(operatorMaster.getOperatorCode());
        rechargeDo.setMobileNo(inRequest.getSubscriberNumber());
        rechargeDo.setProductType(inRequest.getProductType());
        rechargeDo.setInRequestId(inRequest.getRequestId());
        rechargeDo.setTransactionDate(inRequest.getCreatedTime());
        rechargeDo.setOrderId(inRequest.getAffiliateTransId());
        return rechargeDo;
    }

    public Boolean isEligibleForStatusCheck(String orderId) {
        Boolean statusCheckEligibility = true;
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService.
                findUserTransactionHistoryByOrderId(orderId);
        logger.info("userTransactionHistory found for "+orderId+" is: "+userTransactionHistory.toString());
        if(null == userTransactionHistory) {
            statusCheckEligibility = false;
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
            Calendar currentDateTime = Calendar.getInstance();
            currentDateTime.add(Calendar.DATE, -5);
            Date cutOffDate = FCDateUtil.convertStringToDate(simpleDateFormat.format(currentDateTime.getTime()), null);
            Date orderCreatedDate = userTransactionHistory.getLastUpdated();
            logger.info("Current cut off date: "+cutOffDate);
            logger.info("Created date of order: "+orderCreatedDate);
            if(orderCreatedDate.compareTo(cutOffDate) < 0) {
                statusCheckEligibility = false;
            }
        }
        return statusCheckEligibility;
    }
    
    /**
     * This method is used to cache the google credits details for 5 minutes in case if its a successful response
     * @param orderId
     * @param inResponse
     * @param googleSerial
     * @param googleCode
     */
	private void saveGCreditsToCache(String orderId, String googleSerial, String googleCode) {
		if (!StringUtils.isEmpty(googleSerial) && !StringUtils.isEmpty(googleCode)) {
			logger.info("Trying to save the Google Credits to cache for order id : " + orderId);
			googleCreditsService.saveGoogleCreditsCache(orderId, googleSerial, googleCode);
		}
	}
	
	
	public boolean refetchGoogleCredits(InResponse inResponse, RechargeDo rechargeDo, AggregatorFactory aggregatorFactory) {
        AggregatorInterface aggregatorInterface = aggregatorFactory.getAggregator(RechargeConstants.AGGR_NAME_EURONET);
        AggregatorResponseDo aggregatorResponseDo = null;
        try {
            aggregatorResponseDo = aggregatorInterface.getTransactionStatus(rechargeDo, inResponse);
            String responseCode = aggregatorResponseDo.getAggrResponseCode();
            logger.info("status  for request Id "+inResponse.getRequestId() + responseCode);
            //condition added to cache the google response in case of successful status check
			if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(rechargeDo.getProductType())) {
				if (RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(responseCode)) {
					saveGCreditsToCache(rechargeDo.getAffiliateTransId(), aggregatorResponseDo.getGoogleSerial(),
							aggregatorResponseDo.getGoogleCode());
					return true;
				}
			}
        } catch (Exception e) {
            logger.error("Error in processing transaction status and recharge process from scheduler for Aggregator " + inResponse.getAggrName() + " : " + e);
        }
        return false;
    }
}
