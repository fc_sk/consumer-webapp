package com.freecharge.recharge.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.PgAlert;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.services.PaymentGatewayManagementService;
import com.freecharge.rest.user.PgAlertCacheManager;

@Service
public class PgAlertService {

    private static Logger           logger = LoggingFactory.getLogger(PgAlertService.class);

    @Autowired
    PaymentGatewayManagementService paymentGatewayManagementService;

    @Autowired
    private PgAlertCacheManager     pgAlertCacheManager;

    public PgAlert getPgAlertId(Long pgId) {
        PgAlert pgAlert = pgAlertCacheManager.getPgAlertById(pgId);
        if (pgAlert != null) {
            Date startDateTime = pgAlert.getStartDateTime();
            Date endDateTime = pgAlert.getEndDateTime();

            if (Calendar.getInstance().getTime().compareTo(startDateTime) > 0
                    && Calendar.getInstance().getTime().compareTo(endDateTime) < 0)
                return pgAlert;
            else {
                return null;
            }
        } else {
            return null;
        }
    }

    public List<PgAlert> getAllPgAlerts() {
        return pgAlertCacheManager.getAllPgAlerts();
    }

    public List<PgAlert> getAllActivePgAlerts() {
        List<PgAlert> pgAlertList = pgAlertCacheManager.getAllPgAlerts();
        List<PgAlert> pgAlertObjectList = new ArrayList<PgAlert>();
        for(PgAlert pgAlert: pgAlertList) {
            if(isValidAlert(pgAlert)){
                pgAlertObjectList.add(pgAlert);
            }
        }
        return pgAlertObjectList;
    }

    public void deletePgAlert(Long pgId) {
        pgAlertCacheManager.clearPgrAlertId(pgId);
    }

    public void savePgAlertId(String pgId, String startDateTime, String endDateTime, String alertMessage)
            throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        Date startDatetime = sdf.parse(startDateTime);
        Date endDatetime =  sdf.parse(endDateTime);
        PgAlert pgAlert = new PgAlert(Long.valueOf(pgId), startDatetime, endDatetime,
                alertMessage);
        logger.debug("Created pgAlert object: " + pgAlert);
        pgAlertCacheManager.setPgAlertId(Long.valueOf(pgId), pgAlert);
        logger.info("Saved pgAlert object: " + pgAlert);
    }

    private boolean isValidAlert(PgAlert pgAlert) {
        if (pgAlert != null) {
            Date startDateTime = pgAlert.getStartDateTime();
            Date endDateTime = pgAlert.getEndDateTime();
            
            Date now = new Date();

            if (now.after(startDateTime) && now.before(endDateTime)) {
                return true;
            }
        }
        
        return false;
    }
}
