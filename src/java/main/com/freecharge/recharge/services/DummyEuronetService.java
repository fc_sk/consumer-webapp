package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.util.RechargeConstants;

/**
 * This is a dummy Euronet recharge aggregator service class - used only for testing
 */
public class DummyEuronetService extends AbstractDummyService {
    private static Logger logger = LoggingFactory.getLogger(DummyEuronetService.class);

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    String getMockeeName() {
	return AggregatorInterface.EURONET_IN;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest)
            throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
            IOException {
        logger.info("In DummyEuronetService.getTransactionStatus()");

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        aggregatorResponseDo.setAggrName(getMockeeName());
        aggregatorResponseDo.setAggrOprRefernceNo("1234");
        aggregatorResponseDo.setAggrReferenceNo("1234");
        aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
        aggregatorResponseDo.setRequestTime(new Date());
        aggregatorResponseDo.setResponseTime(new Date());

        String mobileNo = rechargeDo.getMobileNo();

        // 93421 38044
        // check for last 2 digits
        String aggrResponseCode = mobileNo.substring(mobileNo.length() - 2);
        String transactionStatus = "";

        if (aggrResponseCode.equals("10")) {
            throw new IOException("Mock IO Exception for order ID: " + rechargeDo.getAffiliateTransId());
        }

        if (aggrResponseCode.equals("00")) {
            transactionStatus = RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE;
        } else if (aggrResponseCode.equals("08")) {
            transactionStatus = RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
        } else {
            transactionStatus = RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL;
        }

        String aggrResponseMessage = "Message from dummy aggregator: " + getMockeeName();

        if (aggrResponseCode.equals("28")) {
            aggrResponseCode = "AI";
            aggrResponseMessage = "Sorry, Invalid amount, Please try doing a recharge for a valid denomination";
        }

        if (aggrResponseCode.equals("29")) {
            aggrResponseCode = "CI";
            aggrResponseMessage = "Invalid Mobile / Subscriber No";
        }

        if (aggrResponseCode.equals("30")) {
            aggrResponseCode = "EG";
            aggrResponseMessage = "Operator System General Error";
        }

        if (aggrResponseCode.equals("31")) {
            aggrResponseCode = "ECI";
            aggrResponseMessage = "Invalid Mobile Number";
        }

        if (aggrResponseCode.equals("08")) {
            aggrResponseCode = "UP";
            aggrResponseMessage = "Transaction under process.";
        }

        if (aggrResponseCode.equals("32")) {
            aggrResponseCode = "ES";
            aggrResponseMessage = "Operator Is Down";
        }

        if (aggrResponseCode.equals("48")) {
            throw new ConnectTimeoutException();
        }

        if (aggrResponseCode.equals("58")) {
            throw new NetworkConnectTimeoutException();
        }

        aggregatorResponseDo.setAggrResponseCode(aggrResponseCode);
        aggregatorResponseDo.setAggrResponseMessage(aggrResponseMessage);
        aggregatorResponseDo.setRawResponse("Raw response from the dummy aggregator: " + getMockeeName());
        aggregatorResponseDo.setTransactionStatus(transactionStatus);

        logger.info("[MOCK] returning responseCode=" + aggregatorResponseDo.getAggrResponseCode());
        return aggregatorResponseDo;
    }

    @Override
    public Double getWalletBalance() throws IOException {
        throw new RuntimeException("Not implemented");
    }
}
