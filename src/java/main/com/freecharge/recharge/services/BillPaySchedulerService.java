package com.freecharge.recharge.services;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;

public class BillPaySchedulerService {
    private static Logger logger = LoggingFactory.getLogger(BillPaySchedulerService.class);
    
    @Autowired
    KestrelWrapper kestrelWrapper;
    
    @Autowired
    MetricsClient metricsClient;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @Autowired
    private UserTransactionHistoryService transactionHistoryService;
    
    @Autowired
    private OrderIdSlaveDAO orderIdDAO;
    
    public void getRetryBillPayStatus(Integer m1, Integer m2) throws ParseException {
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -m1); 
        Date from = DateUtils.addMinutes(to, -m2);
        logger.info("checking billpay status from " + from + " to " + to);
        
        processBillPayStatusAsync(from, to, "Pending." + m2.toString());
    }
    
    private void processBillPayStatusAsync(Date from, Date to, String metricName) throws ParseException {
        List<UserTransactionHistory> userTransactionHistories = transactionHistoryService.findUnderProcessBillPayTxn(from, to);
        metricsClient.recordCount("BillPay", metricName, userTransactionHistories.size());
        for (UserTransactionHistory userTransactionHistory : userTransactionHistories) {
            try {
                logger.info("Got order id " + userTransactionHistory.getOrderId() + " for status check");
                String billId = orderIdDAO.getBillIdForOrderId(userTransactionHistory.getOrderId(), userTransactionHistory.getProductType());
                String lookupId = orderIdDAO.getLookupIdForOrderId(userTransactionHistory.getOrderId());
                logger.info("BillId : " + billId + ", lookupId " + lookupId + ", OrderId "
                        + userTransactionHistory.getOrderId());
                kestrelWrapper.enqueueForBillPayStatusCheck(billId, lookupId, StatusCheckType.Relaxed);
            } catch (Exception e) {
                logger.error("Exception on status check for orderId " + userTransactionHistory.getOrderId(), e);
            }
        }
    }
}
