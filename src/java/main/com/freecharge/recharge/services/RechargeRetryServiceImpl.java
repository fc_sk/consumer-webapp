package com.freecharge.recharge.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.IRechargeRetryConfigDao;
import com.freecharge.app.domain.dao.IRechargeRetryStatusDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.PlanLevelRetryConfig;
import com.freecharge.app.domain.entity.CommonLogDetails;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.RechargeRetryConfig;
import com.freecharge.app.domain.entity.RechargeRetryStatus;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.InRechargeRetryMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.RechargeRetryData;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.InCachedService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.LoggingConstants;
import com.freecharge.freebill.domain.BillPaymentAttempt;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.hlr.service.HLRResponse;
import com.freecharge.hlr.service.HLRService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mgage.service.MgageApiService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.retry.RetryCreationResponse;
import com.freecharge.recharge.retry.RetryCreationStatus;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

@Service
public class RechargeRetryServiceImpl implements IRechargeRetryService{
	
	private Logger planRetryLogger = LoggingFactory
			.getLogger(LoggingFactory.getPlanRetryLoggerName(FulfillmentService.class.getName()));
    
    private static final int MAX_AGGREGATOR_RETRY = 5;
	
    private static Logger logger = LoggingFactory.getLogger(RechargeRetryServiceImpl.class);
    
	private Map<String, String> prePaidPostPaidOperatorMap = ImmutableMap.<String, String> builder()
			.put("Vodafone", "Vodafone Postpaid").put("Airtel", "Airtel Postpaid").put("BSNL", "Bsnl Postpaid")
			.put("Tata Docomo GSM", "Docomo Postpaid-GSM").put("Tata Docomo CDMA", "Docomo Postpaid-CDMA")
			.put("MTS", "MTS Postpaid").put("Reliance-GSM", "Reliance Postpaid-GSM").put("Aircel", "Aircel Postpaid")
			.put("Idea", "Idea Postpaid").put("Reliance CDMA", "Reliance Postpaid-CDMA")
			.build();
	
	private Map<String, String> postPaidPrepaidOperatorMap = ImmutableMap.<String, String> builder()
			.put("Vodafone Postpaid", "Vodafone").put("Airtel Postpaid", "Airtel").put("Bsnl Postpaid", "BSNL")
			.put("Docomo Postpaid-GSM", "Tata Docomo GSM").put("Docomo Postpaid-CDMA", "Tata Docomo CDMA")
			.put("MTS Postpaid", "MTS").put("Reliance Postpaid-GSM", "Reliance-GSM").put("Aircel Postpaid", "Aircel")
			.put("Idea Postpaid", "Idea").put("Reliance Postpaid-CDMA", "Reliance CDMA")
			.build();
	
	private Map<String, String> prepaidDataCardMap = ImmutableMap.<String, String> builder()
			.put("Vodafone", "Vodafone DataCard").put("Airtel", "Airtel DataCard").put("Aircel", "Aircel DataCard")
			.put("MTS", "MTS Mblaze").put("Reliance-GSM", "Reliance Netconnect").put("Idea", "Idea DataCard")
			.put("BSNL", "BSNL DataCard").put("Tata Docomo GSM", "Tata Docomo GSM DataCard").build();

	private Map<String, String> postpaidDataCardMap = ImmutableMap.<String, String> builder()
			.put("Vodafone", "Vodafone DataCard Postpaid").put("Airtel", "Airtel DataCard Postpaid")
			.put("MTS", "MTS Postpaid Datacard").put("Reliance-GSM", "Reliance-CDMA Datacard Postpaid")
			.put("Idea", "Idea DataCard Postpaid").put("Tata Docomo GSM", "Tata Photon Postpaid").build();
	
	private Map<String, String> postpaidprepaidDataCardMap = ImmutableMap.<String, String> builder()
			.put("Vodafone DataCard Postpaid", "Vodafone DataCard").put("Airtel DataCard Postpaid", "Airtel DataCard")
			.put("MTS Postpaid Datacard", "MTS Mblaze").put("Reliance-CDMA Datacard Postpaid", "Reliance Netconnect")
			.put("Idea DataCard Postpaid", "Idea DataCard").put("Tata Photon Postpaid", "Tata Docomo GSM DataCard")
			.build();
	
	private Map<String, String> prepaidpostpaidDataCardMap = ImmutableMap.<String, String> builder()
			.put("Vodafone DataCard", "Vodafone DataCard Postpaid").put("Airtel DataCard", "Airtel DataCard Postpaid")
			.put("MTS Mblaze", "MTS Postpaid Datacard").put("Reliance Netconnect", "Reliance-CDMA Datacard Postpaid")
			.put("Idea DataCard", "Idea DataCard Postpaid").put("Tata Docomo GSM DataCard", "Tata Photon Postpaid")
			.build();
	

	@Autowired
	private RechargeRetryConfigReadThroughCache rechargeRetryConfigCache;
	
	@Autowired
	private IRechargeRetryStatusDao rechargeRetryStatusDao;
	
	@Autowired
    private IRechargeRetryConfigDao rechargeRetryConfigDao;
	
	@Autowired
	private FCProperties fcProperties;
	
	@Autowired
    private AppConfigService appConfigService;
	
	@Autowired
    private KestrelWrapper kestrelWrapper;
	
    @Autowired
    private FulfillmentScheduleService fulfillmentScheduleService;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private OrderIdWriteDAO orderIdDao; 
    
    @Autowired
    private HomeBusinessDao homeBusinessDao;
    
    @Autowired
    private OperatorCircleService operatorCircleService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private BillPaymentService billPaymentService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private MgageApiService mgageApiService;
    
	@Autowired
	private HLRService hlrService;
	
	@Autowired
	private InCachedService inCachedService;
	
	@Autowired
	private AppConfigService appConfig;
	
	@Autowired
	OrderIdSlaveDAO orderIdSlaveDao;
    
    @Override
    @Transactional
    public boolean checkAndScheduleForRetry(String orderId, Map<String, Object> userDetail) {
       
    	ProductName productName = getProductName(orderIdDao.getProductName(orderId),orderId);
    	String productType = productName.getProductType();

        if(!fulfillmentScheduleService.isScheduledForRecharge(orderId)){
        	logger.info("Skipping Fulfillment as already scheduled for recharge");
        	return true;
        }
    	
    	if (appConfigService.isRechargeRetryEnabled()) {
            String responseCode = "";
            String responseMsg = "";
            String aggrName ="";
            Timestamp rechargeTime = null;
            if (productName == ProductName.Mobile || productName == ProductName.DTH || productName == ProductName.DataCard || productName == ProductName.PostPaidDataCard || productName== ProductName.RechargeReward) {
                InResponse inResponse = inService.getInResponseByOrderID(orderId);
                if (inResponse != null) {
                    rechargeTime = inResponse.getCreatedTime();
                    responseCode = inResponse.getAggrRespCode();
                    responseMsg = inResponse.getAggrRespMsg();
                    aggrName = inResponse.getAggrName();
                    if (appConfigService.isAggregatorRetryEnabled() && isRetriable(orderId, productType)
                            && isRetryInThresholdTime(rechargeTime)) {
                        if(scheduleAggregatorRetry(orderId, userDetail, inResponse)){
                            return true;
                        }
                    }
                }
            } else if (productName == ProductName.MobilePostpaid) {
                BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
                if (billPaymentStatus != null) {
                    rechargeTime = billPaymentStatus.getCreatedAt();
                    responseCode = billPaymentStatus.getAuthStatus();
                    responseMsg = billPaymentStatus.getAuthDetail();
                    aggrName = billPaymentStatus.getBillPaymentGateway();
                }
            }
            

            if (inService.isSuccessfulStatusCode(responseCode) || inService.isPending(responseCode) || !isRetryInThresholdTime(rechargeTime)) {
                return false;
            }
            

            RetryCreationResponse retryCreationResponse = checkAndCreateRetryStatus(responseCode, responseMsg, aggrName,  orderId);

            logger.info("RetryCreationResponse for orderID: [" + orderId + "] is : "
                    + retryCreationResponse.getStatus());

            switch (retryCreationResponse.getStatus()) {
                case Created:
                    return fulfillmentScheduleService.createScheduleForRetry(orderId,
                            retryCreationResponse.getNextRetryTime());
                case Updated:
                    return fulfillmentScheduleService.updateRetrySchedule(orderId,
                            retryCreationResponse.getNextRetryTime());
                case MarkedAsComplete:
                case MarkedAsExpired:
                    fulfillmentScheduleService.markRetryScheduleComplete(orderId);
                    return false;
                case NotCreated:
                    return false;
            }
        }
  
        
        return false;
    }

	private ProductName getProductName(ProductName productName, String orderId) {
		if (!rechargeRetryConfigCache.getHLROperatorRetry("HLRProductRetry-" + orderId)) {
			return productName;
		}
		if ((productName == ProductName.Mobile) || (productName == ProductName.MobilePostpaid)) {
			if (productName == ProductName.Mobile) {
				return ProductName.MobilePostpaid;
			} else {
				return ProductName.Mobile;
			}
		} else if ((productName == ProductName.DataCard) || (productName == ProductName.PostPaidDataCard)) {
			if (productName == ProductName.DataCard) {
				return ProductName.PostPaidDataCard;
			} else {
				return ProductName.DataCard;
			}
		}
		return productName;
	}

	private boolean scheduleAggregatorRetry(String orderId, Map<String, Object> userDetail, InResponse inResponse) {
        if (userDetail == null || userDetail.size() < 1) {
            logger.error("Could not get the user details for the orderId " + orderId);
            return false;
        }
        try {
            List<InRequest> inrequests = inService.getAllInRequests(orderId);
            if(inrequests != null && inrequests.size() > 1 ){
                logger.error("Not doing agrgegator retry as more than one requests are created");
                return false;
            }
            List<String> aggregatorsTried = getAggregatorsTried(inResponse);
            if (!wereAllAggregatorsTried(aggregatorsTried, userDetail)) {
                logger.info("Enqueued order ID :[" + orderId + "] for aggregator retry.");
                InRechargeRetryMap retryMap = new InRechargeRetryMap();
                retryMap.setAggregatorRetry(true);
                retryMap.setAggregatorsToSkip(aggregatorsTried);
                rechargeRetryConfigCache.setIfAbsentRechargeRetryOrderId(
                        RechargeConstants.AGGREGATOR_RETRY_CACHE_PREFIX + orderId, retryMap);
                kestrelWrapper.enqueueRechargeRetryAsync(orderId);
                return true;
            }
        } catch (Exception ex) {
            logger.error("Exception doing an aggregator retry.", ex);
        }
        return false;
    }
    
    private boolean wereAllAggregatorsTried(List<String> aggregatorsTried, Map<String, Object> userDetail) {
        logger.info("Aggregators previously tried : " + aggregatorsTried);
        String operatorName = (String) userDetail.get("operatorName");
        String circleName = (String) userDetail.get("circleName");
        if (aggregatorsTried == null || aggregatorsTried.isEmpty() || StringUtils.containsIgnoreCase(circleName,"BSNL")) {
            // scenario where not even single try was done or retry attempts
            // exceeded
            return true;
        }
        CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(operatorName,
                circleName);
        InOprAggrPriority aggregatorPriority = operatorCircleService
                .getInOprAggrPriority(circleOperatorMapping.getCircleOperatorMappingId());
        if (StringUtils.isEmpty(aggregatorPriority.getAggrPriority())) {
            return true;
        }
        List<String> aggregatorsToTry = new ArrayList<String>(
                Arrays.asList(aggregatorPriority.getAggrPriority().split(",")));
        if (aggregatorsToTry != null) {
            for (String name : aggregatorsTried) {
                Iterator<String> it = aggregatorsToTry.iterator();
                while (it.hasNext()) {
                    if (it.next().toLowerCase().trim().equalsIgnoreCase(name.toLowerCase().trim())) {
                        it.remove();
                    }
                }
            }
        }
        logger.info("Operator Name: " + operatorName + " Circle Name: " + circleName);
        logger.info("Aggregators yet to be tried : " + aggregatorsToTry);
        return aggregatorsToTry.isEmpty();
    }

    private List<String> getAggregatorsTried(InResponse inResponse) {
        Set<String> aggregatorNames = new HashSet<>();
        List<InTransactions> inTransactions = inService.getInTransactionsByRequestId(inResponse.getRequestId());
        String latestOperatorCodeTried = inResponse.getOperatorCode();
        String previousOperatorCodeTried = null;
        List<InAggrOprCircleMap> operatorCircleMap = inCachedService.getAllInAggrOprCircleMap();
        // this logic is to ensure that we retried with all aggregators for the
        // operator present in latest in_response
        Set<Integer> lastestOperatorMasterIds = getOperatorMasterIds(latestOperatorCodeTried, operatorCircleMap);
        logger.info("Latest operator code: " + latestOperatorCodeTried+" Latest operatorMasterIds:" + lastestOperatorMasterIds);
        Set<Integer> previousOperatorMasterIds = null;
        if (inTransactions != null && !inTransactions.isEmpty() && inTransactions.size() <= MAX_AGGREGATOR_RETRY) {
            for (InTransactions inTransaction : inTransactions) {
                previousOperatorCodeTried = inTransaction.getOperatorCode();
                logger.info("Previous operatorCode:" + previousOperatorCodeTried + " Previous operatorMasterIds:" + previousOperatorMasterIds);
                previousOperatorMasterIds = getOperatorMasterIds(previousOperatorCodeTried, operatorCircleMap);
                previousOperatorMasterIds.retainAll(lastestOperatorMasterIds);
                if (previousOperatorMasterIds.size() > 0) {
                    aggregatorNames.add(StringUtils.trim(inTransaction.getAggrName()));
                }
            }
        }
        return new ArrayList<String>(aggregatorNames);
    }

    private Set<Integer> getOperatorMasterIds(String latestOperatorCodeTried,
            List<InAggrOprCircleMap> operatorCircleMap) {
        Set<Integer> operatorIds = new HashSet<Integer>();
        String operatorCodeTried = StringUtils.trim(latestOperatorCodeTried);
        for (InAggrOprCircleMap oprCircle : operatorCircleMap) {
            if ((oprCircle.getAggrOperatorCode().trim().equalsIgnoreCase(operatorCodeTried))
                    || (oprCircle.getRechargeplanType() != null
                            && oprCircle.getRechargeplanType().trim().equalsIgnoreCase(operatorCodeTried))) {
                operatorIds.add(oprCircle.getFkOperatorId());
            }
        }
        return operatorIds;
    }

    private boolean isRetryInThresholdTime(Timestamp rechargeTime) {
        Timestamp currentTime = new Timestamp(new Date().getTime());
        if (rechargeTime != null) {
            Timestamp thresholdTime = new Timestamp(rechargeTime.getTime() + RechargeConstants.RECHARGE_RETRY_TIMEOUT_IN_MILLIS);
            if (currentTime.after(thresholdTime)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public List<String> getRetryRechargeStatusOrderIds(Integer start, Integer end) {
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -start); 
        Date from = DateUtils.addMinutes(to, -end);
        List<RechargeRetryStatus> rechargeRetryStatusObjList = rechargeRetryStatusDao.getActiveRetryRechargeStatusOrderIds(
                from, to);
        logger.info("size of rechargeRetryStatusObjList is " + rechargeRetryStatusObjList.size());
        for (RechargeRetryStatus rechargeRetryStatus : rechargeRetryStatusObjList) {
            String errorCode = rechargeRetryStatus.getErrorCode();
            logger.info("errorCode is " + errorCode);
            if (!StringUtils.isBlank(errorCode)) {
               List<RechargeRetryConfig> config =  rechargeRetryConfigCache.getRechargeRetryConfigs();
               if (config!=null && config.size()>0) {
                   for (RechargeRetryConfig rechargeRetryConfig : config) {
                       if (rechargeRetryConfig.getErrorCode().equalsIgnoreCase(rechargeRetryStatus.getErrorCode())) {
                           logger.info("rechargeRetryConfig.getErrorCode() "+ rechargeRetryConfig.getErrorCode());
                           long maxRetryDurationMinutes = rechargeRetryConfig.getMaxRetryDuration();
                           long retryIntervalMinutes = rechargeRetryConfig.getRetryInterval();
                           long createdOn = rechargeRetryStatus.getCreatedOn().getTime();
                           long updatedOn = rechargeRetryStatus.getUpdatedOn().getTime();
                           
                           logger.info("maxRetryDurationMinutes = " + maxRetryDurationMinutes);
                           logger.info("retryIntervalMinutes = " + retryIntervalMinutes);
                           logger.info("createdOn = " + createdOn);
                           logger.info("updatedOn = " + updatedOn);
                           Timestamp currentTs = new Timestamp(System.currentTimeMillis());
                           logger.info("first if condition" + ((currentTs.getTime()-createdOn / (1000*60)) % 60));
                           logger.info("second if condition" + ((currentTs.getTime()-updatedOn / (1000*60)) % 60));
                           if ( ((currentTs.getTime()-createdOn / (1000*60)) % 60) <=maxRetryDurationMinutes ) {
                               if (((currentTs.getTime()-updatedOn / (1000*60)) % 60)>=retryIntervalMinutes) {
                                   Boolean cachedStatus = rechargeRetryConfigCache.setIfAbsentRechargeRetryOrderId("Retrying-" + rechargeRetryStatus.getOrderId(), "Retrying-" + rechargeRetryStatus.getOrderId());
                                   logger.info("cachedStatus for order id " + rechargeRetryStatus.getOrderId());
                                   if (cachedStatus!=null && cachedStatus) {
                                       logger.info("enque request for order id " + rechargeRetryStatus.getOrderId());
                                       kestrelWrapper.enqueueRechargeRetryAsync(rechargeRetryStatus.getOrderId());
                                   }
                               }
                           } else {
                               updateRechargeStatusForOrderId(rechargeRetryStatus);
                           }
                       }
                    }
               } else {
                   updateRechargeStatusForOrderId(rechargeRetryStatus);
               }
            } else {
                updateRechargeStatusForOrderId(rechargeRetryStatus);
            }
        }
        return null;
    }


    @Transactional
    private RetryCreationResponse checkAndCreateRetryStatus(String errorCode, String errorMsg, String aggrName, String orderId) {
        RetryCreationResponse creationResponse = new RetryCreationResponse();
        creationResponse.setStatus(RetryCreationStatus.NotCreated);
        
		RechargeRetryConfig retryConfig;
		retryConfig = getRechargeRetryConfig(errorCode, errorMsg);
        

        if (retryConfig != null) {
            logger.info("Using the  retry recharge config with error code " + retryConfig.getErrorCode() + " for aggregator : " + aggrName);
            /**
             * 1. If retry status has already been completed then don't do anything.
             * 2. If retry period has elapsed then don't do anything.
             * 3. If number of retries has exceeded a limit then don't do anything.
             * 4. If we are under retry limit then update the status table and schedule for next retry.
             * 5. If none of the above then create a new retry status
             * 
             * Schedule creation/updation, if required, has to be taken care by the caller.
             */
            RechargeRetryStatus status = rechargeRetryStatusDao.getRechargeRetryStatus(orderId);
            int retryInterval = ((Long) retryConfig.getRetryInterval()).intValue();
            DateTime nextRetryTime = DateTime.now().plusSeconds(retryInterval);
            if (status != null) {
                if (status.isRetryComplete()) {
                    return RetryCreationResponse.NotCreated;
                }
                
                DateTime creationTS = new DateTime(status.getCreatedOn());
                int maxRetryDurationSeconds = ((Long) retryConfig.getMaxRetryDuration()).intValue();
                
                if (creationTS.plusSeconds(maxRetryDurationSeconds).isBeforeNow()) {
                    
                    status.setRetryComplete(true);
                    creationResponse = RetryCreationResponse.MarkedAsExpired;
                    
                } else {

                    Integer countRemaining = status.getRetryCountRemaining();
                    Integer newCountRemaining = 0;

                    if (countRemaining != null) {
                        newCountRemaining = --countRemaining;
                    } 
                    
                    if (newCountRemaining > 0) {
                        status.setRetryCountRemaining(newCountRemaining);
                        
                        creationResponse.setNextRetryTime(nextRetryTime);
                        creationResponse.setStatus(RetryCreationStatus.Updated);
                        
                    } else {
                        status.setRetryComplete(true);
                        creationResponse = RetryCreationResponse.MarkedAsComplete;
                    } 
                }
                
                rechargeRetryStatusDao.updateRechargeRetryStatus(status);

            } else {
                RechargeRetryStatus rechargeRetryStatus = new RechargeRetryStatus();
                rechargeRetryStatus.setOrderId(orderId);
                rechargeRetryStatus.setRetryComplete(false);
                rechargeRetryStatus.setErrorCode(errorCode);
                rechargeRetryStatus.setRetryCountRemaining(retryConfig.getMaxRetryCount());
                rechargeRetryStatus.setCreatedOn(new Timestamp(System.currentTimeMillis()));
                
                boolean created = rechargeRetryStatusDao.createRechargeRetryStatus(rechargeRetryStatus);
                
                if (created) {
                    creationResponse.setNextRetryTime(nextRetryTime);
                    creationResponse.setStatus(RetryCreationStatus.Created);
                }
            }
        }
        
        return creationResponse;
    }    

    
	private RechargeRetryConfig getRechargeRetryConfig(String errorCode, String errorMsg) {
		try {
			List<RechargeRetryConfig> rechargeRetryConfigs = rechargeRetryConfigCache.getRechargeRetryConfigs();
			if (rechargeRetryConfigs != null) {
				for (RechargeRetryConfig config : rechargeRetryConfigs) {
					if (FCUtil.isEmpty(config.getErrorMsg())) {
						if (StringUtils.equals(errorCode, config.getErrorCode())) {
							return config;
						}
					} else {
						String configErrorMsg = config.getErrorMsg().toLowerCase();
						errorMsg = errorMsg.toLowerCase();
						if (errorMsg.contains(configErrorMsg)) {
							return config;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error caught is ", e);
		}
		return null;
	}

    private void updateRechargeStatusForOrderId(RechargeRetryStatus rechargeRetryStatus) {
        rechargeRetryStatus.setRetryComplete(true);
        rechargeRetryStatusDao.updateRechargeRetryStatus(rechargeRetryStatus);
        rechargeRetryConfigCache.deletRechargeRetryOrderId("Retrying-" + rechargeRetryStatus.getOrderId());
    }

    @Override
    public boolean checkForOperatorRetry(String orderId, Map<String, Object> userDetail) {
        try {
            if (appConfigService.isOperatorRetryEnabled()) {
            	
            	if(!isEligibleForRetry(orderId)){
            		return false;
            	}
            	
            	
            	if (userDetail == null || userDetail.size() < 1) {
                    logger.error("Could not get the user details for the orderId " + orderId);
                    return false;
                }
    
                String operatorName = (String) userDetail.get("operatorName");
                String circleName = (String) userDetail.get("circleName");
                String productType = (String) userDetail.get("productType");
                
                if (ProductName.fromPrimaryProductType(productType) == ProductName.DataCard || ProductName.fromPrimaryProductType(productType) == ProductName.PostPaidDataCard) {
                    logger.info("Datacard recharge, so operator retry is enabled for orderID " + orderId);
                } else if (appConfigService.isOperatorRetryOnlyNutsEnabled()) {
                    Users user = userServiceProxy.getUserFromOrderId(orderId);
                    if (userTransactionHistoryService.hasSuccessfullyTransacted(user.getUserId())) {
                        logger.info("Not a new user, so not going ahead for orderId - " + orderId );
                        return false;
                    }
                }
                
                logger.info("Operator : " + operatorName + " Circle : " + circleName);
    
                OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorName);
    
                List<InRechargeRetryMap> inRechargeRetryList = null;
                Timestamp rechargeTime = null;
                if (FCUtil.isRechargeProductType(productType)) {
                    CircleOperatorMapping circleOperatorMapping = operatorCircleService.getCircleOperatorMapping(
                            operatorName, circleName);
    
                    InResponse inResponse = inService.getInResponseByOrderID(orderId);
    
                    if (inResponse != null && inService.isFailure(inResponse.getInRespCode())
                            && circleOperatorMapping != null) {
                        rechargeTime = inResponse.getCreatedTime();
                        List<InTransactions> inTransactionsList = inService.getSortedInTransactionsByRequestId(inResponse
                                .getRequestId());
    
                        int aggregatorCount = 0;
                        InOprAggrPriority aggregatorPriority = operatorCircleService
                                .getInOprAggrPriority(circleOperatorMapping.getCircleOperatorMappingId());
                        if (aggregatorPriority != null) {
                            aggregatorCount = aggregatorPriority.getAggrPriority().split(",").length;
                        }
    
                        /*
                         * This loop is to get the inRechargeRetryList for the
                         * initial operator. It checks if data is available for the
                         * initial operator with any of the aggregator and its
                         * corresponding response code.
                         */
                        int iterator = 0;
                        for (InTransactions inTransaction : inTransactionsList) {
                            inRechargeRetryList = getInRechargeRetryMapping(inTransaction.getAggrResponseCode(),
                                    inTransaction.getAggrName(), circleOperatorMapping.getFkOperatorMasterId());
                            if (inRechargeRetryList != null || iterator >= aggregatorCount) {
                                break;
                            }
                            iterator++;
                        }
                   
                    }
                } else if (FCUtil.isBillPostpaidType(productType)) {
                    BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
                    if (billPaymentStatus != null && billPaymentStatus.getSuccess() != null
                            && !billPaymentStatus.getSuccess() && operatorMaster != null
                            && inService.isFailure(billPaymentStatus.getAuthStatus())) {
                        rechargeTime = billPaymentStatus.getCreatedAt();
                        StringTokenizer aggregatorPriority = billPaymentService.getBillPaymentAggregators(operatorMaster
                                .getOperatorMasterId().toString());
                        List<BillPaymentAttempt> billPaymentAttempts = billPaymentService
                                .findBillPaymentAttemptsByStatusId(billPaymentStatus.getId());
    
                        int iterator = 0;
                        int maxCount = aggregatorPriority.countTokens();
                        for (BillPaymentAttempt billPaymentAttempt : billPaymentAttempts) {
                            while (aggregatorPriority.hasMoreElements()) {
                                
                                String billPaymentGateway = aggregatorPriority.nextElement().toString();
                                inRechargeRetryList = getInRechargeRetryMapping(billPaymentAttempt.getAuthStatus(),
                                        billPaymentGateway, operatorMaster.getOperatorMasterId());
                                if (inRechargeRetryList != null) {
                                    break;
                                }
                            }
                            if (inRechargeRetryList != null || iterator >= maxCount) {
                                break;
                            }
                            iterator++;
                        }
    
                    }
                }
                
                if (inRechargeRetryList != null && isRetryInThresholdTime(rechargeTime)) {
                    List<RechargeRetryData> currentRechargeRetryDatas = inService.getRechargeRetryDataForOrder(orderId);
    
                    for (InRechargeRetryMap inRechargeRetryMap : inRechargeRetryList) {
                        boolean isOperatorRechargeAttempted = false;
                        boolean retryComplete = false;
                        for (RechargeRetryData rechargeRetryData : currentRechargeRetryDatas) {
                            if (rechargeRetryData.getRetryOperatorName().equals(inRechargeRetryMap.getRetryOperatorName())) {
                                isOperatorRechargeAttempted = true;
                                break;
                            }
                            if (rechargeRetryData.isFinalRetry() || FCUtil.isEmpty(rechargeRetryData.getInRespCode())
                                    || inService.isSuccessfulStatusCode(rechargeRetryData.getInRespCode())) {
                                retryComplete = true;
                                break;
                            }
                        }
                        if (retryComplete) {
                            break;
                        }
                        if (!isOperatorRechargeAttempted && !retryComplete && inRechargeRetryMap.getIsRetryable()) {
                           
                        	boolean enqueueForRetry = rechargeRetryConfigCache.setIfAbsentRechargeRetryOrderId(
                                    "OperatorRetry-" + orderId, inRechargeRetryMap);
                            if (enqueueForRetry) {
                                logger.info(String.format(
                                        "Operator retry is enabled for order : %s, retrying with operator : %s", orderId,
                                        inRechargeRetryMap.getRetryOperatorName()));
                                metricsClient.recordEvent(
                                        "Recharge",
                                        "OperatorRetry.Attempt",
                                        operatorMaster.getOperatorMasterId() + "-"
                                                + inRechargeRetryMap.getFkRetryOperatorMasterId());
                                if (!fulfillmentScheduleService.checkAndCreateOperatorRetrySchedule(orderId)) {
                                    kestrelWrapper.enqueueRechargeRetryAsync(orderId);
                                }
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Failed to check operator retry case", e);
        }
        return false;
    }
    
	private boolean isEligibleForRetry(String orderId) {
		List<OrderId> orderIdList = orderIdDao.getByOrderId(orderId);

		if (orderIdList == null || orderIdList.isEmpty()) {
			logger.error("No order found for Order ID : [" + orderId + "]. Nothing to be done returning");
			return false;
		}

		if (orderIdList.size() > 1) {
			logger.error(
					"Something bad, found multiple orders for Order ID : [" + orderId + "]. Working the first one");
		}

		OrderId orderToWork = orderIdList.get(0);

		logger.debug("Order id to work with:" + orderToWork);

		if (orderToWork.isMoreThanThreeHoursOld()) {
			logger.info("Skipping Order id :" + orderId + " as it is more than three hours old.");
			return false;
		}

		return true;
	}

	public boolean checkForHLRServiceRetry(String orderId, Map<String, Object> userDetail) {
		if (!isEligibleForRetry(orderId)) {
			return false;
		}
		logger.info("Checking Mgage for orderID:" + orderId);
		if (rechargeRetryConfigCache.getHLROperatorRetry("HLRProductRetry-" + orderId)) {
			logger.info("HLRProductRetry-" + orderId + " key found...so not going ahead");
			return false;
		} else {
			try {
				if (!appConfigService.isOperatorRetryEnabled()) {
					return false;
				}
				if (userDetail == null || userDetail.size() < 1) {
					logger.error("Could not get the user details for the orderId " + orderId);
					return false;
				}

				String operatorName = (String) userDetail.get("operatorName");
				String circleName = (String) userDetail.get("circleName");
				String productType = (String) userDetail.get("productType");
				String serviceNumber = (String) userDetail.get("mobileno");
				Float amount = (Float) userDetail.get("amount");
				
				OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorName);
				CircleMaster circleMaster =operatorCircleService.getCircle(circleName);

				logger.info("Operator : " + operatorName + " Circle : " + circleName);

				if (FCUtil.isMobileRecharge(productType) || FCUtil.isDataCard(productType)) {

					if (isRetriable(orderId, productType)) {
						HLRResponse hlrResponse = null;
						if (rechargeRetryConfigCache.getHLROperatorRetry("HLROperatorRetry-" + orderId)) {
							hlrResponse = rechargeRetryConfigCache.getHLRRetryData(orderId);
							logger.info("HLRResponse from cache is " + hlrResponse);
							if (hlrResponse == null) {
								hlrResponse = hlrService.getResponse(serviceNumber, orderId,
										operatorMaster.getOperatorMasterId(), circleMaster.getCircleMasterId());
							}
						} else {
							hlrResponse = hlrService.getResponse(serviceNumber, orderId,
									operatorMaster.getOperatorMasterId(), circleMaster.getCircleMasterId());
						}
						if (hlrResponse != null) {							
							String operatorToRetry = hlrResponse.getOperator();
							String circleToRetry = hlrResponse.getCircle();
							logger.info("Operator to retry is " + operatorToRetry + " for orderId " + orderId);							
							boolean productTypeRetry = false;

							operatorToRetry = getOpertarToRetry(operatorToRetry, productType);
							if (operatorToRetry == null) {
								logger.info("No mapping found for operator:" + operatorToRetry);
								return false;
							}
							hlrResponse.setOperator(operatorToRetry);
							rechargeRetryConfigCache.setHLRRetryData(hlrResponse, orderId);

							if (FCUtil.isDataCard(productType)) {
								if (productType.equals("C")) {
									if (operatorName.contains("Tata")) {
										operatorName = "Tata Docomo GSM Datacard";
									} else if (operatorName.contains("MTS")) {
										operatorName = "MTS Mblaze";
									}
								}
							}
							if (operatorName.equals(operatorToRetry)
									|| (rechargeRetryConfigCache.getHLROperatorRetry("HLROperatorRetry-" + orderId))) {
								rechargeRetryConfigCache.setHLROperatorRetry("HLRProductRetry-" + orderId, true);								
								if (rechargeRetryConfigCache.getHLROperatorRetry("HLROperatorRetry-" + orderId)) {
									operatorName = hlrResponse.getOperator();
								}
								if (productType.equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
									if (prePaidPostPaidOperatorMap.containsKey(operatorName)) {
										operatorToRetry = prePaidPostPaidOperatorMap.get(operatorName);
										circleToRetry = "ALL";
										productType = RechargeConstants.POST_PAID_PRODUCT_TYPE;
										logger.info("ProductType retry with operator:" + operatorToRetry);
									}else{
										return false;
									}
								} else if (productType.equals(RechargeConstants.POST_PAID_PRODUCT_TYPE)) {
									if (postPaidPrepaidOperatorMap.containsKey(operatorName)) {
										operatorToRetry = postPaidPrepaidOperatorMap.get(operatorName);
										productType = RechargeConstants.MOBILE_PRODUCT_TYPE;
										logger.info("ProductType retry with operator:" + operatorToRetry);
									}else{
										return false;
									}
								} else if (productType.equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE)) {
									if (prepaidpostpaidDataCardMap.containsKey(operatorName)) {
										operatorToRetry = prepaidpostpaidDataCardMap.get(operatorName);
										productType = RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE;
										logger.info("ProductType retry with operator:" + operatorToRetry);
									}
								} else if (productType.equals(RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE)) {
									if (postpaidprepaidDataCardMap.containsKey(operatorName)) {
										operatorToRetry = postpaidprepaidDataCardMap.get(operatorName);
										productType = RechargeConstants.DATA_CARD_PRODUCT_TYPE;
										logger.info("ProductType retry with operator:" + operatorToRetry);
									}
								}
								operatorName = (String) userDetail.get("operatorName");
								logger.info("OperatorToRetry: " + operatorToRetry + " CircleToRetry:" + operatorName
										+ " ProductToRetry:" + productType);
								productTypeRetry = true;
							}
							if (!rechargeRetryConfigCache.getHLROperatorRetry("HLROperatorRetry-" + orderId)) {
								rechargeRetryConfigCache.setHLROperatorRetry("HLROperatorRetry-" + orderId, true);
							}
							if(FCUtil.isEmpty(operatorToRetry) || !isRetriableForAmount(amount,productType)){
								return false;
							}
							OperatorMaster operatorMasterRetry = operatorCircleService.getOperator(operatorToRetry);
							operatorMaster = operatorCircleService.getOperator(operatorName);
							InRechargeRetryMap inRechargeRetryMap = new InRechargeRetryMap();
							inRechargeRetryMap.setRetryOperatorName(operatorToRetry);
							inRechargeRetryMap.setFkRetryOperatorMasterId(
									String.valueOf(operatorMasterRetry.getOperatorMasterId()));
							inRechargeRetryMap
									.setFkOperatorMasterId(String.valueOf(operatorMaster.getOperatorMasterId()));
							inRechargeRetryMap.setRetryProductId(String.valueOf(
									ProductMaster.ProductName.fromPrimaryProductType(productType).getProductId()));
							inRechargeRetryMap.setIsProductTypeRetry(productTypeRetry);
							inRechargeRetryMap.setIsRetryable(true);
							CircleMaster circleMasterRetry = operatorCircleService.getCircle(circleToRetry);
							inRechargeRetryMap.setRetryCircleName(circleToRetry);
							inRechargeRetryMap
									.setFkRetryCirlcleMasterId(String.valueOf(circleMasterRetry.getCircleMasterId()));

							logger.info("inRechargeRetryMap set for order " + orderId);
							boolean enqueueForRetry = rechargeRetryConfigCache
									.setIfAbsentRechargeRetryOrderId("OperatorRetry-" + orderId, inRechargeRetryMap);

							if (enqueueForRetry) {
								logger.info(String.format(
										"Operator retry is enabled for order : %s, retrying with HLR operator : %s",
										orderId, inRechargeRetryMap.getRetryOperatorName()));

								metricsClient.recordEvent("Recharge", "OperatorRetry.Attempt.HLR",
										operatorMaster.getOperatorMasterId() + "-"
												+ inRechargeRetryMap.getFkRetryOperatorMasterId());
								if (!fulfillmentScheduleService.checkAndCreateOperatorRetrySchedule(orderId)) {
									kestrelWrapper.enqueueRechargeRetryAsync(orderId);
								}
								return true;
							}
						}
					}
				}
			} catch (Exception e) {
				logger.info("Caught exception in HLRService:", e);
			}
		}
		return false;
	}

	private boolean isRetriableForAmount(Float amount, String productType) {
		switch (productType) {
		case RechargeConstants.MOBILE_PRODUCT_TYPE:
			if (amount > Float.valueOf(fcProperties.getProperty(FCConstants.PREPAID_AMOUNT_THRESHOLD))) {
				logger.info("Skipping retry because amount of " + String.valueOf(amount)
						+ " not allowed for productType " + productType);
				return false;
			}

		}
		return true;
	}

	private String getOpertarToRetry(String operatorToRetry, String productType) {
		if (productType.equals(RechargeConstants.POST_PAID_PRODUCT_TYPE)
				&& prePaidPostPaidOperatorMap.containsKey(operatorToRetry)) {
			return prePaidPostPaidOperatorMap.get(operatorToRetry);
		} else if (productType.equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE)
				&& prepaidDataCardMap.containsKey(operatorToRetry)) {
			return prepaidDataCardMap.get(operatorToRetry);
		} else if (productType.equals(RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE)
				&& postpaidDataCardMap.containsKey(operatorToRetry)) {
			return postpaidDataCardMap.get(operatorToRetry);
		}
		return operatorToRetry;
	}

	private boolean isRetriable(String orderId, String productType) {
    	String agRespCode = null;
        String inResponseCode = null;
        String authDetail = null;        
    
		List<String> reversalMessageList = new ArrayList<String>(Arrays.asList(
				RechargeConstants.RECHARGE_REVERSAL_IN_RESP_MSG, RechargeConstants.RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG, RechargeConstants.POSTPAID_ADMIN_STATUS_UPDATE_AUTH_DETAIL));
        if (FCUtil.isRechargeProductType(productType)) {
            InResponse inResponse = inService.getInResponseByOrderID(orderId);
            agRespCode = inResponse.getAggrRespCode();
            inResponseCode = inResponse.getInRespCode();
            authDetail = inResponse.getInRespMsg();
        } else if (FCUtil.isBillPostpaidType(productType)) {
            BillPaymentStatus billPaymentStatus = billPaymentService
                    .findBillPaymentStatusByOrderId(orderId);
            agRespCode = billPaymentStatus.getAuthStatus();
            authDetail = billPaymentStatus.getAuthDetail();
            if (billPaymentStatus.getSuccess()) {
                inResponseCode = AggregatorInterface.SUCCESS_IN_RESPONSE_CODE;
            } else {
                // Some dummy failure code
                inResponseCode = "29";
            }
        }

        if (!inService.isFailure(inResponseCode)) {
            logger.info("Recharge not failed for order " + orderId);
            return false;
        }
        
        if(reversalMessageList.contains(authDetail)){
        	logger.info("Transaction failed via Reversal API ...so not going ahead");
        	return false;
        }

        logger.info("Aggr code" + agRespCode);

        if (!fulfillmentScheduleService.isOpRetryAgCodes(agRespCode)) {
            logger.info("Not a retriable errorCode for OrderId " + orderId);
            return false;
        }

        logger.info(agRespCode + " is present in List");
        return true;
	}

	private List<InRechargeRetryMap> getInRechargeRetryMapping(String aggResponseCode,String aggName,int operatorId) {
        List<InRechargeRetryMap> inRechargeRetryMap = inService.getOperatorRetryConfig(aggResponseCode,aggName,String.valueOf(operatorId));
        if (inRechargeRetryMap != null && inRechargeRetryMap.size() > 0) {
            return inRechargeRetryMap;
        }
        return null;
    }

	@Override
	public boolean checkForPlanLevelRetry(String orderId, Map<String, Object> userDetail) {
		// eligible for retry check
		try {
			if (!appConfig.isPlanLevelRetryEnabled()) {
				logger.info("Plan Level Retry not enabled, hence not going for retry");
				return false;
			}

			if (!isEligibleForRetry(orderId)) {
				return false;
			}

			String operatorName = (String) userDetail.get("operatorName");
			String circleName = (String) userDetail.get("circleName");
			String productType = (String) userDetail.get("productType");
			String serviceNumber = (String) userDetail.get("mobileno");
			Float amount = (Float) userDetail.get("amount");
			String lookupId = (String) userDetail.get("lookupId");
			
			OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorName);
			CircleMaster circleMaster =operatorCircleService.getCircle(circleName);
			

			if (!isPlanLevelRetryOperator(String.valueOf(operatorMaster.getOperatorMasterId()))) {
				logger.info("Not a valid operator for plan level retry. The operator is : " + operatorName);
				return false;
			}

			if (!isPlanLevelRetriable(orderId, productType)) {
				logger.info("Not Retriable for plan level retry");
				return false;
			}

			boolean isRetrySet = rechargeRetryConfigCache.getPlanLevelRetryKey(orderId);

			if (!isRetrySet) {
				logger.info("Plan Retry not set, going ahead with plan level retry for order id : [" + orderId + "]");
				PlanLevelRetryConfig retryConfigData = new PlanLevelRetryConfig();
				retryConfigData.setCircle(String.valueOf(circleMaster.getCircleMasterId()));
				retryConfigData.setOperator(String.valueOf(operatorMaster.getOperatorMasterId()));
				retryConfigData.setProductType(productType);
				retryConfigData.setServiceNumber(serviceNumber);
				retryConfigData.setRechargeAmount(String.valueOf(amount));

				String rechargeType = orderIdSlaveDao.getRechargeTypeByLookupId(lookupId);
				String complimentaryPlanType = FCUtil.getComplimentaryPlanType(rechargeType);

				retryConfigData.setPlanType(complimentaryPlanType);
				rechargeRetryConfigCache.setPlanLevelRetryKey(orderId, true);
				rechargeRetryConfigCache.setPlanLevelRetryData(orderId, retryConfigData);
				kestrelWrapper.enqueueRechargeRetryAsync(orderId);
				Map<String,Object> loggingData = createDataMapForPlanRetry(rechargeType, complimentaryPlanType, orderId,
						String.valueOf(operatorMaster.getOperatorMasterId()),
						String.valueOf(circleMaster.getCircleMasterId()), serviceNumber);
				CommonLogDetails retryDetails = FCUtil.createLoggingObject(LoggingConstants.PLAN_RETRY_DATABASE,
						LoggingConstants.PLAN_RETRY_TABLE, LoggingConstants.DBActionType.INSERT.toString(),
						loggingData);
				Gson gosGson = new Gson();
				String data = gosGson.toJson(retryDetails);
				planRetryLogger.info(data);
				return true;

			}
		} catch (Exception e) {
			logger.error("An error occurred trying to perform plan level retry for order id : " + orderId);
		}
		return false;
	}
	
	private Map<String, Object> createDataMapForPlanRetry(String rechargeType, String complimentaryPlanType,
			String orderId, String operatorId, String circleId, String serviceNumber) {
		Map <String,Object> data = new HashMap<>();
		data.put("rechargeType", rechargeType);
		data.put("complimentaryPlanType", complimentaryPlanType);
		data.put("orderId", orderId);
		data.put("operatorMasterId", operatorId);
		data.put("circleMasterId", circleId);
		data.put("serviceNumber", serviceNumber);
		return data;
	}

	private boolean isPlanLevelRetriable(String orderId, String productType) {
    	String agRespCode = null;
        String inResponseCode = null;
        String authDetail = null;        
    
		List<String> reversalMessageList = new ArrayList<String>(Arrays.asList(
				RechargeConstants.RECHARGE_REVERSAL_IN_RESP_MSG, RechargeConstants.RECHARGE_ADMIN_SUCCESS_IN_RESP_MSG, RechargeConstants.POSTPAID_ADMIN_STATUS_UPDATE_AUTH_DETAIL));
        if (FCUtil.isRechargeProductType(productType)) {
            InResponse inResponse = inService.getInResponseByOrderID(orderId);
            agRespCode = inResponse.getAggrRespCode();
            inResponseCode = inResponse.getInRespCode();
            authDetail = inResponse.getInRespMsg();
        } 

        if (!inService.isFailure(inResponseCode)) {
            logger.info("Recharge not failed for order " + orderId);
            return false;
        }
        
        if(reversalMessageList.contains(authDetail)){
        	logger.info("Transaction failed via Reversal API ...so not going ahead");
        	return false;
        }

        logger.info("Aggr code" + agRespCode);

        if (!isPlanLevelRetryErrorCode(agRespCode)) {
            logger.info("Not a plan level retriable errorCode for OrderId " + orderId);
            return false;
        }
        logger.info(agRespCode + " is present in List");
        return true;
	}
	
	public boolean isPlanLevelRetryErrorCode(String errorCode) {
		JSONObject configJson = appConfig.getCustomProp(appConfig.PLAN_LEVEL_RETRY_CONFIG);
		String configString = configJson.toJSONString();
		Map<String, Object> configMap = FCUtil.getMapFromString(configString);
		if (configMap != null) {
			final List<String> errorCodeList = FCUtil
					.getListFromCommaSeparatedString((String) configMap.get("errorCodes"));
			logger.info("Plan level retriable error code list is : " + errorCodeList);
			logger.info("The error code to validate is : " + errorCode);
			if (!errorCodeList.isEmpty()) {
				if (errorCodeList.contains(errorCode)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isPlanLevelRetryOperator(String operator) {
		JSONObject configJson = appConfig.getCustomProp(appConfig.PLAN_LEVEL_RETRY_CONFIG);
		String configString = configJson.toJSONString();
		Map<String, Object> configMap = FCUtil.getMapFromString(configString);
		if (configMap != null) {
			final List<String> operatorList = FCUtil
					.getListFromCommaSeparatedString((String) configMap.get("operators"));
			logger.info("The list of operators valid for plan level retry is : " +operatorList );
			logger.info("The operator to validate is : " + operator);
			if (!operatorList.isEmpty()) {
				if (operatorList.contains(operator)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
}
