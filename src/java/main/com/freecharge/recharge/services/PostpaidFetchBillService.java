package com.freecharge.recharge.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.billdeskpostpaid.BDKFetchBillService;
import com.freecharge.freebill.billdeskpostpaid.BillPaymentBillDeskUtil;
import com.freecharge.freebill.billdeskpostpaid.FetchAndPayResultBDK;
import com.freecharge.freebill.domain.BillPostpaidMerchant;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freebill.service.EuronetFetchAndPayUtil;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.beans.FetchAndPayResult;
import com.freecharge.recharge.util.RechargeConstants;

@Service
public class PostpaidFetchBillService {

	protected final Logger logger = LoggingFactory.getLogger(getClass().getName());

	private static final Object BDK_SUCCESS = "0";

	private static final String FETCH_BILL_VALIDATION_TIMEOUT = "fetchbill.validation.timeout";

	private static final String DEFAULT = "NA";

	private static final Object EURONET_SUCCESS = "00";

	@Autowired
	private BillPaymentBillDeskUtil billDeskUtil;

	@Autowired
	private BillPaymentService billPayService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private BDKFetchBillService bdkFetchBillService;

	@Autowired
	private EuronetFetchAndPayUtil euronetPaymentUtil;

	@Autowired
	private AppConfigService appConfig;

	public Map<String, Object> fetchPostpaidBill(String operatorId, String mobileNumber) {
		// TODO Auto-generated method stub
		Map<String, Object> result = new HashMap<>();
		String billerType = RechargeConstants.ADHOC_BILLER_TYPE;
		if (appConfig.isPostPaidFetchBillSupportEnabled()) {
			FetchAndPayResultBDK fetchPayResult = null;
			FetchAndPayResult fpr = null;
			if (billPayService.isFetchBillOperator(operatorId)) {
				StringTokenizer aggrprioritytokens = billPayService.getBillPaymentAggregators(operatorId);
				if (billPayService.toFetchBillFromEuronet(operatorId)) {					
						String aggregatorName = aggrprioritytokens.nextElement().toString();
						if (aggregatorName.equals(RechargeConstants.EURONET_AG)) {
							try {
								Integer oprId = Integer.parseInt(operatorId);
								fpr = euronetPaymentUtil.getEuroFetchbillResult(mobileNumber, oprId);
								if (fpr != null && EURONET_SUCCESS.equals(fpr.getErrorCode())) {
									sanitizeFetchPayResultEuronet(fpr, operatorId);
									billerType = RechargeConstants.FETCHBILL_BILLER_TYPE;
									result.put("fetchBillResult", fpr);
								} else {
									billerType = RechargeConstants.ADHOC_BILLER_TYPE;
								}
								logger.info("fpr:" + fpr);
							} catch (Exception e) {
								logger.info("fpr:" + fpr);
								logger.info("Exception:", e);
							}

						}					
				} else if (billPayService.toFetchBillFromBilldesk(operatorId)) {
					BillPayValidator validators = bdkFetchBillService.getBillPayValidator(operatorId);
					if (aggrprioritytokens.nextElement().toString().equals(RechargeConstants.BDK_AG)) {
						BillPostpaidMerchant postpaidMerchant = getPostpaidMerchantByOperatorId(operatorId);
						String merchantId = postpaidMerchant.getBilldeskMerchantId();
						HttpConnectionManagerParams connectionManagerConfig = initializeConnectionManager();
						try {
							fetchPayResult = billDeskUtil.getFetchAndPayResultBDK(connectionManagerConfig, mobileNumber,
									Integer.valueOf(operatorId), merchantId);
							fpr = sanitizeFetchPayResult(fetchPayResult, validators);
						} catch (Exception e) {
							logger.error("Exception in FetchBill is ", e);
						}
					}
				}
				if (fetchPayResult != null) {
					billerType = RechargeConstants.FETCHBILL_BILLER_TYPE;
					result.put("fetchBillResult", fpr);
				}

			}
		}
		result.put("billerType", billerType);

		return result;
	}

	private void sanitizeFetchPayResultEuronet(FetchAndPayResult fpr, String operatorId) {
		fpr.setBillerId(getPostpaidMerchantByOperatorId(operatorId).getEuronetMerchantId());
		fpr.setPartialPayment(true);
		fpr.setErrorCode("0");
		fpr.setInfo(fromEuroNetInfo(fpr));

	}

	private FetchAndPayResult sanitizeFetchPayResult(FetchAndPayResultBDK fetchPayResult, BillPayValidator validators) {
		FetchAndPayResult fpr = new FetchAndPayResult();
		fpr.setBillAmount(fetchPayResult.getBillAmount());
		fpr.setBillDate(fetchPayResult.getBillDate());
		fpr.setBillDueDate(fetchPayResult.getBillDueDate());
		fpr.setBillerId(fetchPayResult.getBillerId());
		fpr.setBillNumber(fetchPayResult.getBillNumber());
		fpr.setErrorCode(fetchPayResult.getErrorCode());
		fpr.setErrorMessage(fetchPayResult.getErrorMessage());
		if (BDK_SUCCESS.equals(fpr.getErrorCode())) {
			fpr.setInfo(formInfo(fetchPayResult.getInfo(), validators.getValidators()));
		}

		return fpr;
	}

	private List<HashMap<String, String>> formInfo(List<HashMap<String, String>> info,
			List<HashMap<String, String>> validator) {
		List<HashMap<String, String>> infoList = new ArrayList<>();
		for (int i = 0; i < validator.size(); i++) {
			HashMap<String, String> data = new HashMap<>();
			data.put("name", validator.get(i).get("name"));
			if (!FCUtil.isEmpty(info.get(i).get("value"))) {
				data.put("value", info.get(i).get("value"));
			} else {
				data.put("value", DEFAULT);
			}
			infoList.add(i, data);
		}

		return infoList;
	}

	private List<HashMap<String, String>> fromEuroNetInfo(FetchAndPayResult fpr) {
		List<HashMap<String, String>> info = fpr.getInfo();
		List<HashMap<String, String>> infoList = new ArrayList<HashMap<String, String>>();
		for (Map.Entry<String, String> entry : info.get(0).entrySet()) {
			HashMap<String, String> data = new HashMap<>();
			data.put("name", entry.getKey());
			data.put("value", entry.getValue());
			infoList.add(data);
		}
		return infoList;
	}

	private BillPostpaidMerchant getPostpaidMerchantByOperatorId(String operatorId) {
		try {
			return billPayService.getPostpaidMerchantByOperatorId(Integer.valueOf(operatorId));
		} catch (NullPointerException npe) {
			logger.error("Null Pointer Exception:", npe);
		}
		return null;
	}

	private HttpConnectionManagerParams initializeConnectionManager() {
		HttpConnectionManagerParams connectionManagerConfig = new HttpConnectionManagerParams();
		connectionManagerConfig.setSoTimeout(fcProperties.getIntProperty(FETCH_BILL_VALIDATION_TIMEOUT));
		connectionManagerConfig.setConnectionTimeout(fcProperties.getIntProperty(FETCH_BILL_VALIDATION_TIMEOUT));
		connectionManagerConfig.setStaleCheckingEnabled(true);
		return connectionManagerConfig;
	}

}
