package com.freecharge.recharge.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.UserRechargeHistoryReadDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freebill.common.BillPaymentCacheManager;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.gson.Gson;

@Service
public class UserTransactionHistoryService {
    
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    @Autowired
    private OrderIdWriteDAO orderIdDao;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDao;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private UserRechargeHistoryReadDAO rechargeHistoryDAO;
    
    @Autowired
    private OperatorCircleService operatorCircleService;
    
    @Autowired
    private BillPaymentCacheManager billPayCacheManager;

    @Transactional
    public void createOrUpdateRechargeHistory(String orderId, Map<String, Object> userDetail, String responseCode) {
        UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId);
        if (userTransactionHistory == null) {
            UserTransactionHistory userTransactionHistoryObj = userTransactionHistoryDAO.createUserTransactionHistory(userDetail, orderId, responseCode);
            if (userTransactionHistoryObj != null) {
                userTransactionHistoryDAO.insertUserTransactionHistory(userTransactionHistoryObj);
            }
        } else{
            logger.info("Previous status 1:"+userTransactionHistory.getTransactionStatus());
            if(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND.equals(userTransactionHistory.getTransactionStatus())){
                responseCode = PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND;
            }
            logger.info("responseCode:"+responseCode);
            // Left intentionally to figure out unknown status in UTH by status 09
            if(!userTransactionHistory.getTransactionStatus().equalsIgnoreCase(PaymentConstants.FULFILLMENT_UNKNOWN_STATUS)) {
               userTransactionHistoryDAO.updateUserTransactionHistory(orderId, responseCode, userDetail);
            }else {
            	logger.info("Doing nothing for unknown status UTH");
            }
        }
    }
    
    @Transactional
    public void updateStatus(String orderId, String status) {
        UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                .findUserTransactionHistoryByOrderId(orderId);
        logger.info("Previous status 2:"+userTransactionHistory.getTransactionStatus());
        if (PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND.equals(userTransactionHistory.getTransactionStatus())) {
            status = PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND;
        }
        logger.info("Status:"+status);
        userTransactionHistoryDAO.updateStatus(orderId, status);
    }
    
    /**
     * Create user transaction history entry with
     * recharge stauts as under process ("08").
     * @param rechargeDo
     */
    public void markRechargeInit(RechargeDo rechargeDo) {
        Integer userId = orderIdDao.getUserIdFromOrderId(rechargeDo.getOrderId());
        
        Map<String, Object> userDetail = new HashMap<>();
        userDetail.put("amount", rechargeDo.getAmount());
        userDetail.put("mobileno", rechargeDo.getMobileNo());
        userDetail.put("productType", rechargeDo.getProductType());
        userDetail.put("operatorName", rechargeDo.getOperatorCode());
        userDetail.put("circleName", rechargeDo.getCircleCode());
        userDetail.put("usersId", userId);
        
        createOrUpdateRechargeHistory(rechargeDo.getOrderId(), userDetail, RechargeConstants.IN_UNDER_PROCESS_CODE);
    }
    
    public List<UserTransactionHistory> findSuccessfulUTHByUserEmailAndProductType(String emailId, String productType ,Integer limit) {
    	Long userId = userServiceProxy.getUserIdByEmailId(emailId);
    	return userTransactionHistoryDAO.findSuccessfulUTHByUserIdAndProductType(userId, productType, limit);
    }
    
    public List<UserTransactionHistory> findSuccessfulUTHByUserId(Long userId, int limit) {
    	return userTransactionHistoryDAO.findSuccessfulUTHByUserId(userId, limit);
    }
    
//    public long insertMetadata(String metadata, String orderId) {
//    	logger.info("[AnantQa] here in uth service");
//    	Gson serializer = new Gson();
//        Map<String, Object> metaDataMap = new HashMap();
//        metaDataMap.put("BBPSReferenceNumber", metadata);
//        logger.info("[AnantQA]metaDataMAp into String"+ serializer.toJson(metaDataMap));
//    	return userTransactionHistoryDAO.insertMetadata(serializer.toJson(metaDataMap), orderId);
//    }
    
    public List<String> findOrderIdByUserEmail(String emailId, int limit) {
        logger.info("Fetching orders for user : " + emailId);
        Users users = userServiceProxy.getUserByEmailId(emailId);
        if(users == null){
            return new ArrayList<>();
        }
        final List<String> returnList = userTransactionHistoryDAO.findOrderIdByUserId(users.getUserId(), limit);
        
        if (returnList == null) {
            return new ArrayList<>();
        }
        
        return returnList;
    }
    
    public List<String> findSuccessfulOrderIdByUserEmail(String emailId, int limit) {
    	Long userId = userServiceProxy.getUserIdByEmailId(emailId);
        final List<String> returnList = userTransactionHistoryDAO.findSuccessfulOrderIdByUserEmail(userId, limit);
        
        if (returnList == null) {
            return new ArrayList<>();
        }
        
        return returnList;
    }
    
    public List<UserTransactionHistory> findUserTransactionHistoryByUserId(Integer userId, Timestamp fromDateTime) {
        return userTransactionHistoryDAO.findUserTransactionHistoryByUserId(userId, fromDateTime);
    }

    public List<UserTransactionHistory> findAllUserTransactionHistoryByUserId(Integer userId) {
        return userTransactionHistoryDAO.findAllUserTransactionHistoryByUserId(userId);
    }
    
    public UserTransactionHistory findUserTransactionHistoryByOrderId(String orderID) {
        return  userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderID);
    }
    @Transactional(readOnly = true)
    public boolean hasTransacted(Integer userId) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime beginning = formatter.parseDateTime("1900-01-01");
        
        return hasTransactedSince(userId, beginning);
    }
    
    @Transactional(readOnly = true)
    public boolean hasTransactedSince(Integer userId, DateTime since) {
        int countInUserTransactionHistoryTable = userTransactionHistoryDAO.getTransactionCountSince(userId, since);
        
        if (countInUserTransactionHistoryTable > 0) {
            return true;
        }
        /*
         * user_recharge_history table may have older data.
         */
        int countInRechargeHistoryTable = rechargeHistoryDAO.getRechargeHistoryCountSince(userId, since);
        if (countInRechargeHistoryTable > 0) {
            return true;
        }
        return false;
    }
    
    @Transactional(readOnly = true)
    public boolean hasSuccessfullyTransacted(Integer userId) {
        List<String> ordersInUserTransactionHistoryTable = userTransactionHistoryDAO.getRechargeSuccessDistinctOrderIdsByUserId(userId, 1);
        if (ordersInUserTransactionHistoryTable!=null && ordersInUserTransactionHistoryTable.size() >= 1) {
            return true;
        }
        return false;
    }
    
    @Transactional
    public void markTransactionFailed(String orderId) {
        userTransactionHistoryDAO.markPendingTransactionAsFailure(orderId);
    }
    
    @Transactional
    public List<String> getDistinctOrderIdsByUserId(int userId, int limit) {
        return userTransactionHistoryDAO.getDistinctOrderIdsByUserId(userId, limit);
    }
    
    @Transactional
    public List<String> getRechargeSuccessDistinctOrderIdsByUserId(int userId, int limit) {
        return userTransactionHistoryDAO.getRechargeSuccessDistinctOrderIdsByUserId(userId, limit);
    }
    
    public List<String> getRechargeSuccessDistinctOrderIdsByUserId(Integer userId, Timestamp fromDateTime, int limit) {
        return userTransactionHistoryDAO.getRechargeSuccessDistinctOrderIdsByUserId(userId, fromDateTime, limit);
    }

    @Transactional
    public List<UserTransactionHistory> findUserTransactionHistoryFromUserId (int userId, int limit) {
        return userTransactionHistoryDAO.getUserTransactionHistory(userId, limit);
    }
    
    @Transactional
    public List<UserTransactionHistory> getTransactionHistoryForCombination(Integer userId, String serviceNumber, int noOfMonths, Double minTxnAmount, Double maxTxnAmount){
    	return userTransactionHistoryDAO.getTransactionHistoryForCombination(userId, serviceNumber, noOfMonths, minTxnAmount, maxTxnAmount);
    }

    public List<UserTransactionHistory> getTransactionHistory(Integer userId, String rechargeNo,
            Date insertedOn, Double txnAmount) {
        return userTransactionHistoryDAO.getTransactionHistory(userId, rechargeNo,
                insertedOn, txnAmount);
    }
    
    public List<UserTransactionHistory> getSuccessfulTransactionHistoryByCombination(Integer userId, String rechargeNo,
            Date from, Date to, Float txnAmount) {
        return userTransactionHistoryDAO.getSuccessfulTransactionHistoryByCombination(userId, rechargeNo, from, to, txnAmount);
    }   
    
    public RechargeDetails getRechargeDetailsByOrderID(String orderID) {
        UserTransactionHistory uth = findUserTransactionHistoryByOrderId(orderID);
        if (uth == null) {
            return orderIdDao.getRechargeDetailsFromHomePageByOrderID(orderID);
        }
        return constructRechargeDetails(uth);
    }

    public RechargeDetails getRechargeDetailsByOrderIDFromSlave(String orderID) {
        UserTransactionHistory uth = findUserTransactionHistoryByOrderId(orderID);
        if (uth == null) {
            return orderIdReadDao.getRechargeDetailsFromHomePageByOrderID(orderID);
        }
        return constructRechargeDetails(uth);
    }

    public RechargeDetails constructRechargeDetails(UserTransactionHistory uth) {
        String productType = uth.getProductType();
        
        RechargeDetails rechargeDetails = new RechargeDetails();
        rechargeDetails.setProductName(FCConstants.productTypeToNameMap.get(productType));
        rechargeDetails.setOperatorName(uth.getServiceProvider());
        if (ProductName.fromPrimaryProductType(productType).isUtilityPaymentProduct()) {
            try {
                BillPayOperatorMaster bopm = billPayCacheManager.getBillPayOperatorFromName(uth.getServiceProvider());
                rechargeDetails.setOperatorImageURL(bopm != null ? bopm.getImgUrl() : "");
                rechargeDetails.setOperatorMasterId(bopm != null ? String.valueOf(bopm.getOperatorMasterId()) : null);
            } catch (Exception e) {
                rechargeDetails.setOperatorImageURL("");
                rechargeDetails.setOperatorMasterId(null);
            }
        } else {
            OperatorMaster opm = operatorCircleService.getOperator(uth.getServiceProvider());
            rechargeDetails.setOperatorImageURL(opm != null ? opm.getImgUrl() : "");
            rechargeDetails.setOperatorMasterId(opm != null ? String.valueOf(opm.getOperatorMasterId()) : null);
        }
        rechargeDetails.setCircleName(uth.getServiceRegion());
        CircleMaster cim = operatorCircleService.getCircle(uth.getServiceRegion());
        rechargeDetails.setCircleId(cim != null ? String.valueOf(cim.getCircleMasterId()) : null);
        rechargeDetails.setAmount(String.valueOf(uth.getTransactionAmount().intValue()));
        rechargeDetails.setServiceNumber(uth.getSubscriberIdentificationNumber());
        rechargeDetails.setRechargedOn(new java.sql.Date(uth.getLastUpdated().getTime()));
        rechargeDetails.setOrderId(uth.getOrderId());
        rechargeDetails.setTransactionStatusCode(uth.getTransactionStatus());
        if (productType != null && FCConstants.PRODUCT_TYPE_HCOUPON.equals(productType)) {
           rechargeDetails.setServiceNumber("Deals");
        }
        return rechargeDetails;
    }

    public List<UserTransactionHistory> findUnderProcessBillPayTxn(Date from, Date to) {
        return userTransactionHistoryDAO.getBillPayUPTransactionHistory(from, to);
    }
    
    public List<String> findFailedTransactionsBetween(Date from, Date to) {
        return userTransactionHistoryDAO.findFailedTransactionsBetween(from, to);
    }
    
    public List<String> findPendingTransactionsBetween(Date from, Date to) {
        return userTransactionHistoryDAO.findPendingTransactionsBetween(from, to);
    }
}
