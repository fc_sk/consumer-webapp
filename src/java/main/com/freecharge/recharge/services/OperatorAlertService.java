package com.freecharge.recharge.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.OperatorAlert;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.user.OperatorAlertCacheManager;

@Service
public class OperatorAlertService {
    private static Logger             logger = LoggingFactory.getLogger(OperatorAlertService.class);

    @Autowired
    private OperatorAlertCacheManager operatorAlertCacheManager;

    @Autowired
    private OperatorCircleService operatorCircleService;

    @Autowired
    private FCProperties          fcProperties;

    @Autowired
    private EmailService          emailService;
    
    @Autowired
    private AppConfigService appConfig;
    
    @Autowired
    private CommonRechargeService rechargeService;
    
    public boolean isOperatorInAlert(Integer operatorId) {
        if (isOperatorInAlertNoConfigCheck(operatorId)) {
            if (rechargeService.isOperatorDownBlockEnabled(operatorId)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    
    public boolean isOperatorInAlertNoConfigCheck(Integer operatorId) {
        List<OperatorAlert> operatorAlert = this.getValidOperatorAlert(operatorId);
        
        if (!FCUtil.isEmpty(operatorAlert)) {
            return true;
        }
        
        return false;
    }

    public boolean isOperatorInAlert(int operatorId, int circleId) {
        if(circleId != -1 && appConfig.isOperatorAlertBlockEnabled()) {
            List<OperatorAlert> operatorAlert = this.getValidOperatorAlert(operatorId, circleId);
            
            if (!FCUtil.isEmpty(operatorAlert)) {
                return true;
            }
        }
        return false;
    }

    public List<OperatorAlert> getValidOperatorAlert(final int operatorId) {
        List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlert(operatorId);
        List<OperatorAlert> activeOperatorAlert = new ArrayList<>();
        for (OperatorAlert operAlert : operatorAlerts) {
            if (isValidAlert(operAlert)) {
                activeOperatorAlert.add(operAlert);
            }
        }
        return activeOperatorAlert;
    }
    
    public List<OperatorAlert> getValidOperatorAlert(final int operatorId, final int circleId) {
        List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlert(operatorId, circleId);
        logger.debug("Found " + operatorAlerts.size() + " alerts for operatorId " + operatorId +" and circleId " + circleId);
        List<OperatorAlert> activeOperatorAlert = new ArrayList<>();
        for (OperatorAlert operAlert : operatorAlerts) {
            if (isValidAlert(operAlert)) {
                activeOperatorAlert.add(operAlert);
            }
        }
        logger.debug("Found active " + activeOperatorAlert.size() + " alerts for operatorId " + operatorId +" and circleId " + circleId);
        return activeOperatorAlert;
    }

    public List<OperatorAlert> getAllOldOperatorAlerts(final int operatorId, final long score) {
        List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlertForScoreRange(operatorId, 0,
                score);
        return operatorAlerts;
    }
    
    public List<OperatorAlert> getAllOldOperatorAlerts(final int operatorId, final int circleId, final long score) {
           List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlertForScoreRange(operatorId, 0,
                   score);
           return operatorAlerts;
    }

    public List<OperatorAlert> getOperatorAlertId(final Integer operatorId) {
        List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlert(operatorId);
        return operatorAlerts;
    }
    
    public List<OperatorAlert> getOperatorAlertId(final Integer operatorId, final Integer circleId) {
        List<OperatorAlert> operatorAlerts = operatorAlertCacheManager.getOperatorAlert(operatorId, circleId);
        return operatorAlerts;
    }

    private boolean isValidAlert(final OperatorAlert operatorAlert) {
        if (operatorAlert != null) {
            Calendar startDateTime = Calendar.getInstance();
            startDateTime.setTime(operatorAlert.getStartDateTime());
            Calendar endDateTime =  Calendar.getInstance();
            endDateTime.setTime(operatorAlert.getEndDateTime());
            Calendar now = Calendar.getInstance();
            logger.debug("startDateTime : " + startDateTime + " , endDateTime : " + endDateTime + " , now : " + now);
            logger.debug("now.after(startDateTime) : " + now.after(startDateTime));
            logger.debug("now.before(endDateTime) : " + now.before(endDateTime));
            if (now.after(startDateTime) && now.before(endDateTime)) {
                return true;
            }
        }
        return false;
    }

    private boolean validateCreateAlert(final OperatorAlert operatorAlert) {
        List<OperatorAlert> operatorAlerts = null;
        if(operatorAlert.getCircleId()!=null && operatorAlert.getCircleId() != 0) {
            operatorAlerts = getOperatorAlertId(operatorAlert.getOperatorId(), operatorAlert.getCircleId());
        } else {
            operatorAlerts = getOperatorAlertId(operatorAlert.getOperatorId());
        }
        
        setOperatorCircleName(operatorAlert);
        
        Date alertExpiryTime = operatorAlert.getEndDateTime();
        
        for (OperatorAlert operAlert : operatorAlerts) {
            if (isConflictAlerts(operAlert, operatorAlert)) {
                   deleteOperatorAlert(operAlert.getOperatorId(), operAlert.getCircleId(), operAlert.getStartDateTime().getTime());
            } else if (operAlert.getEndDateTime().after(alertExpiryTime)) {
                alertExpiryTime = operAlert.getEndDateTime();
            }
        }
        operatorAlert.setExpiryTime(alertExpiryTime);
        return true;
    }

    private void setOperatorCircleName(OperatorAlert operatorAlert) {
        if(operatorAlert.getCircleId()!=null && operatorAlert.getCircleId() != 0) {
            CircleMaster circleMaster = operatorCircleService.getCircle(operatorAlert.getCircleId());
            operatorAlert.setCircleName(circleMaster.getName());
        }
        OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorAlert.getOperatorId());
        operatorAlert.setOperatorName(operatorMaster.getName());
        operatorAlert.setOperatorCode(operatorMaster.getOperatorCode());
    }

    private boolean validateDate(final OperatorAlert operatorAlert) {
        if (operatorAlert.getStartDateTime().after(operatorAlert.getEndDateTime())) {
            throw new IllegalStateException(String.format("Invalid start (%s) and end (%s) time",
                    operatorAlert.getStartDateTime(), operatorAlert.getEndDateTime()));
        }
        return true;
    }

    private boolean isConflictAlerts(final OperatorAlert existingAlert, final OperatorAlert newAlert) {
        Date existingStart = existingAlert.getStartDateTime();
        Date existingEnd = existingAlert.getEndDateTime();
        Date newStart = newAlert.getStartDateTime();
        Date newEnd = newAlert.getEndDateTime();
        if (newStart.equals(existingStart) || newEnd.equals(existingEnd)) {
            return true;
        }
        if ((newStart.after(existingStart) && newStart.before(existingEnd))
                || (newEnd.after(existingStart) && newEnd.before(existingEnd))
                || (newStart.before(existingStart) && newEnd.after(existingEnd))) {
            return true;
        }
        return false;
    }

    public List<OperatorAlert> getAllOperatorAlerts() {
    	logger.info("Getting All Operator Alerts from OperatorAlert Service Call");
        return operatorAlertCacheManager.getAllOperatorAlerts();
    }

    public void deleteOperatorAlert(final int operatorId, final long score) {
        operatorAlertCacheManager.clearOperatorAlertId(operatorId, score, score);
    }
    
    public void deleteOperatorAlert(final Integer operatorId, final Integer circleId, final long score) {
        if(circleId==null || circleId == 0) {
            operatorAlertCacheManager.clearOperatorAlertId(operatorId, score, score);
        } else {
            operatorAlertCacheManager.clearOperatorAlertId(operatorId, circleId, score, score);
        }
    }

    public void deleteAllOldOperatorAlert(final Integer operatorId, final long score) {
        operatorAlertCacheManager.clearOperatorAlertId(operatorId, 0, score);
    }

    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");

    public void saveOperatorAlertId(Integer operatorId, final String startDateTime,
            final String endDateTime, final String alertMessage, final boolean isBlocking) throws IllegalStateException{
        saveOperatorAlertId(operatorId, null, startDateTime, endDateTime, alertMessage, isBlocking);
    }       
    
    public void saveOperatorAlertId(Integer operatorId, Integer circleId, final String startDateTime,
            final String endDateTime, final String alertMessage, final boolean isBlocking) throws IllegalStateException{
        Date startDate;
        Date endDate;
        try {
            startDate = SDF.parse(startDateTime);
            endDate = SDF.parse(endDateTime);
            OperatorAlert operatorAlert = null;
            if(circleId == null || circleId==0){
                operatorAlert = new OperatorAlert(operatorId, startDate, endDate, alertMessage);
            } else {
                operatorAlert = new OperatorAlert(operatorId, circleId, startDate, endDate, alertMessage);
            }
            operatorAlert.setIsBlocking(isBlocking);
            validateDate(operatorAlert);
            validateCreateAlert(operatorAlert);
            if(circleId == null ||  circleId==0){
                operatorAlertCacheManager.setOperatorAlertId(operatorId, operatorAlert, operatorAlert.getStartDateTime()
                    .getTime());
                sendEmail(1, operatorId, "", "");
            } else {
                operatorAlertCacheManager.setOperatorAlertId(operatorId, circleId, operatorAlert, operatorAlert.getStartDateTime()
                        .getTime());
                sendEmail(1, operatorId, "For circle " + circleId, "");
            }
            logger.info("Saved operatorAlert obj: " + operatorAlert);
        } catch (ParseException e) {
            logger.error("Invalid Date Format: ", e);
            throw new IllegalArgumentException(e);
        }
    }
    
    /** This method is responsible for sending the emails as and
     * when the operator down Alert created and deleted from the
     * admin panel[By Rajani N].
     * @param statusCode indicates whether operator Alert created [will send the status code as 1]
     *        or deleted [will send the statusCode as 0].
     * @param operatorId passes the operatorId which operato Alert has been created or deleted.
     * @param emailContent passes the relevant email Content.
     */
     public final void sendEmail(final int statusCode, final int operatorId, final String emailContent,
             final String currentAdminUser) {
         logger.info("In sending mail for id: " + operatorId);
         try {
             OperatorMaster operatorMaster = operatorCircleService.getOperator(operatorId);
             StringBuilder sbf = new StringBuilder();
             if (operatorMaster != null) {
                 EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
                 if (statusCode == 0) {
                     emailBusinessDO
                             .setSubject(fcProperties.getProperty(FCConstants.OPERATOR_ALERT_DELETED_SUCCESS_SUBJECT)
                                     + operatorMaster.getName());
                     sbf.append(emailBusinessDO.getSubject());
                     sbf.append("<br>\n");
                     sbf.append(emailContent);
                     if(!FCUtil.isEmpty(currentAdminUser)) {
                         sbf.append("<br>\n");
                         sbf.append("Operator Alert deleted by : ");
                         sbf.append(currentAdminUser);
                     }
                 } else {
                     emailBusinessDO
                             .setSubject(fcProperties.getProperty(FCConstants.OPERATOR_ALERT_CREATED_SUCCESS_SUBJECT)
                                     + operatorMaster.getName());
                     sbf.append(emailBusinessDO.getSubject());
                     sbf.append("\n<br>\n");
                     sbf.append(emailContent);
                     if(!FCUtil.isEmpty(currentAdminUser)) {
                         sbf.append("<br>\n");
                         sbf.append("Operator Alert created by : ");
                         sbf.append(currentAdminUser);
                     }
                 }
                 emailBusinessDO.setTo(fcProperties.getProperty(FCConstants.OPERATOR_ALERT_SUCCESS_REPLYTO));
                 emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.OPERATOR_ALERT_SUCCESS_FROM));
                 emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.OPERATOR_ALERT_SUCCESS_REPLYTO));
                 emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
                 emailBusinessDO.setContent(sbf.toString());
                 emailService.sendNonVelocityEmail(emailBusinessDO);
             }
         } catch (RuntimeException e) {
             logger.debug("Error while Sending Email. Stack Trace=", e);
         }
     }
     
     public List<OperatorAlert> processAndGetOperatorAlert(int operatorId, String circle) {
         int circleId = 0;
         if(!"ALL".equalsIgnoreCase(circle)) {
             circleId = Integer.parseInt(circle);
         } 
         
         List<OperatorAlert> operatorAlerts = null;
         if(circleId == 0) {
             operatorAlerts = getValidOperatorAlert(operatorId);
         } else {
             operatorAlerts = getValidOperatorAlert(operatorId, circleId);
             if(FCUtil.isEmpty(operatorAlerts)) {
                 operatorAlerts = getValidOperatorAlert(operatorId);
             }
         }
         return operatorAlerts;
     }

}
