package com.freecharge.recharge.services;

import java.util.List;
import java.util.Map;

/**
 * Encapsulates the recharge retry logic related functions.
 * 
 * @author shirish
 * 
 */
public interface IRechargeRetryService {

    /**
     * Schedule a failed recharge for retry
     * 
     * @param inResponse
     * @param orderId
     * @return true if recharge was scheduled for retry.
     */
    public boolean checkAndScheduleForRetry(String orderId, Map<String, Object> userDetail);

    public List<String> getRetryRechargeStatusOrderIds(Integer start, Integer end);

    /**
     * Retry a failed recharge with different operator.
     * 
     * @param inResponse
     * @param orderId
     * @return true if recharge is to be retried with a different operator.
     */
    public boolean checkForOperatorRetry(String orderId, Map<String, Object> userDetail);

	public boolean checkForHLRServiceRetry(String orderId, Map<String, Object> userDetail);
	
	/**
	 * Used to check for plan level retry
	 * @param orderId
	 * @param userDetail
	 * @return true
	 * 				- if  a plan level retry is to be done
	 */
	public boolean checkForPlanLevelRetry(String orderId, Map<String, Object> userDetail);
}
