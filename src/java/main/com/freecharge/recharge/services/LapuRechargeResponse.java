package com.freecharge.recharge.services;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Data")
public class LapuRechargeResponse {

    private String Status;

    private String TalkTime;

    private String OperatorTxnId;

    private String Description;

    private String Mobile;

    private String Denomination;

    private String MerOid;

    private String OrderId;

    private String MerNotes;

    private String CreditUsed;

    private String BalanceBefore;

    private String Balance;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getOperatorTxnId() {
        return OperatorTxnId;
    }

    public void setOperatorTxnId(String operatorTxnId) {
        OperatorTxnId = operatorTxnId;
    }

    public String getTalkTime() {
        return TalkTime;
    }

    public void setTalkTime(String talkTime) {
        TalkTime = talkTime;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getDenomination() {
        return Denomination;
    }

    public void setDenomination(String denomination) {
        Denomination = denomination;
    }

    public String getMerOid() {
        return MerOid;
    }

    public void setMerOid(String merOid) {
        MerOid = merOid;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getMerNotes() {
        return MerNotes;
    }

    public void setMerNotes(String merNotes) {
        MerNotes = merNotes;
    }

    public String getCreditUsed() {
        return CreditUsed;
    }

    public void setCreditUsed(String creditUsed) {
        CreditUsed = creditUsed;
    }

    public String getBalanceBefore() {
        return BalanceBefore;
    }

    public void setBalanceBefore(String balanceBefore) {
        BalanceBefore = balanceBefore;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }
}
