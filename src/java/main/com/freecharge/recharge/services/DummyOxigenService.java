package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;

/**
 * This is a dummy Oxigen recharge aggregator service class - used only for testing
 */
public class DummyOxigenService extends AbstractDummyService {
    private static Logger logger = LoggingFactory.getLogger(DummyOxigenService.class);

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    String getMockeeName() {
	return AggregatorInterface.OXIGEN_IN;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest) throws HttpException, ConnectException,ConnectTimeoutException, SocketException, SocketTimeoutException,IOException {
	logger.info("In DummyOxigenService.getTransactionStatus()");
	throw new IOException("NYI");
    }

    @Override
    public Double getWalletBalance() throws IOException {
        throw new RuntimeException("Not implemented");
    }
    
}
