package com.freecharge.recharge.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.castor.CastorMarshaller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.XMLConverter;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessDo.RechargeResInfo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.thoughtworks.xstream.XStream;

public class SuvidhaaService extends AggregatorService {

	private static Logger logger = LoggingFactory.getLogger(SuvidhaaService.class);

    @Autowired
    private FCProperties fcProperties;
	private String suvidhaamobilerequesturl;
	private String suvidhaaMobileStatusCheckRequestUrl;
	private String suvidhaaDthStatusCheckRequestUrl;
	private String suvidhaadthrequesturl;
    private String suvidhaausername;
    private String suvidhaaaccesskey;
    
    @Autowired
    private CastorMarshaller suvidhaUnMarshaller;

    @Autowired
    private VelocityEngine velocityEngine;
    
    @Autowired
    private XMLConverter XMLConverter;
    
	@Autowired
	private MultiThreadedHttpConnectionManager connectionManager;
	
	@Override
	public AggregatorResponseDo doRecharge(RechargeDo rechargeDo,String orderNo, StringBuffer parametersPassed,List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException,ConnectException, ConnectTimeoutException, SocketException,SocketTimeoutException, IOException {
        InAggrOprCircleMap inAggrOprCircleMap = FCUtil.getInAggrOprCircleMap(inAggrOprCircleMaps, AggregatorInterface.SUVIDHAA_IN);

        if (inAggrOprCircleMap == null) {
            return doActionAfterFail(rechargeDo, orderNo, parametersPassed);
        }
        String suvidhaaOperatorCode = inAggrOprCircleMap.getAggrOperatorCode();
        String suvidhaaCircleCode = inAggrOprCircleMap.getAggrCircleCode();
        
        return this.doRechargeHelper(rechargeDo, orderNo, parametersPassed, suvidhaaOperatorCode, suvidhaaCircleCode, inAggrOprCircleMap.getShouldTimeOut(), inAggrOprCircleMap.getTimeOutInMillis());
	}


    public AggregatorResponseDo doRechargeHelper(RechargeDo rechargeDo,String orderNo, StringBuffer parametersPassed, String operatorCode,
                                                 String circleCode, Boolean shouldTimeOut, Integer timeOutValue) throws HttpException,ConnectException, ConnectTimeoutException, SocketException,SocketTimeoutException, IOException {

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        String response = null;
        HttpClient client = new HttpClient();
        client.setHttpConnectionManager(connectionManager);
        logger.info("[IN MODERATOR] processing doRecharge for suvidhaa");

        PostMethod method;
        if (rechargeDo.getProductType() != null && rechargeDo.getProductType().equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
        	method = new PostMethod(suvidhaamobilerequesturl);
        } else {
        	method = new PostMethod(suvidhaadthrequesturl);	
        }
        	
        try {
        	Calendar cal = Calendar.getInstance();
            Date dt = cal.getTime();
            String inputXml = getRechargeInputXml(rechargeDo , orderNo ,parametersPassed, operatorCode, circleCode);
            method.setRequestEntity(new StringRequestEntity(inputXml, RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE_VALUE, "ISO-8859-1"));
            method.setRequestHeader(RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE, RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE_VALUE);
            aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_SUVIDHAA);
            aggregatorResponseDo.setRawRequest(inputXml);
            aggregatorResponseDo.setRequestId(Long.parseLong(orderNo.substring(orderNo.indexOf("_") + 1, orderNo.length())));
            aggregatorResponseDo.setRequestTime(dt);
            logger.info("suvidhaa request uri " + method.getURI() + " Suvidhaa request param : " + inputXml.toString().trim());
            int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), client, method, shouldTimeOut, timeOutValue);

            metricsClient.recordEvent("Recharge", "Suvidhaa.HttpStatusCode", String.valueOf(statusCode));

            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
            } 
            logger.info(" Received HTTP status code for suvidha : " + statusCode + response);
            aggregatorResponseDo=responseHandler(response, aggregatorResponseDo);
        } catch (Exception e) {
            logger.error("Error rised while rechrging for suvidhaa " + e);
            throw e;
        } finally{
            method.releaseConnection();
        }
        return aggregatorResponseDo;
    }

	 private String getRechargeInputXml(RechargeDo rechargeDo, String orderNo,  StringBuffer parametersPassed , String suvidhaaOperatorCode, String suvidhaaCircleCode) {
 		String inputXml = "";
 		try {
 			Map<String, Object> templateValues = new HashMap<String, Object>();
 			
 			templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TYPE, rechargeDo.getProductType());
 			templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_USER_NAME, suvidhaausername);
            templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_CLIENT_TXN_ID, orderNo);
            templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_SERVICE_PROVIDER_ID, suvidhaaOperatorCode);
 			templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TXN_AMOUNT, rechargeDo.getAmount());
 			templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TOKEN_KEY, suvidhaaaccesskey);
            templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TIME, new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
            if (rechargeDo.getProductType() != null && rechargeDo.getProductType().equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_CIRCLE_ID, suvidhaaCircleCode);
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_RECHARGE_TYPE, "ESR");
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_MOBILE_NUMBER, rechargeDo.getMobileNo());
            } else if (rechargeDo.getProductType() != null && rechargeDo.getProductType().equals(RechargeConstants.DTH_PRODUCT_TYPE)) {
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_PRODUCT_CODE,"");
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_SUBSCRIBER_ID, rechargeDo.getMobileNo());
            	templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_MOBILE_NUMBER, "9999669859");
            } 
 			inputXml = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "suvidhaaRequest.vm", templateValues);
 		} catch (Exception e) {
 			logger.info("Exception while creating xml " + e);
 		}
 		return inputXml.toString().trim();
 	}

	 private AggregatorResponseDo responseHandler(String response, AggregatorResponseDo aggregatorResponseDo) 
		{
			RechargeResInfo suvidhaaResponse = null;
			 if(response!=null && !response.isEmpty())
	         {
				try {
				 response = response.replace(RechargeConstants.SUVIDHAA_RESPONSE_XML_STRING, "");
				 response = response.replace("i:nil="+"true","");
				 XStream xstream = new XStream();
				 xstream.alias(RechargeConstants.SUVIDHAA_RESPONSE_OBJ_MAP, RechargeResInfo.class);
				 suvidhaaResponse = (RechargeResInfo)xstream.fromXML(response);
				Calendar cal1 = Calendar.getInstance();
			    Date dt1 = cal1.getTime();
			    if(suvidhaaResponse!=null) {
				if(suvidhaaResponse.getDescription()!=null && !suvidhaaResponse.getDescription().isEmpty()){
					aggregatorResponseDo.setAggrResponseMessage(suvidhaaResponse.getDescription());
				}
				if(suvidhaaResponse.getTransactionId()!=null && !suvidhaaResponse.getTransactionId().isEmpty()) {
					aggregatorResponseDo.setAggrReferenceNo(suvidhaaResponse.getTransactionId());
				}
				aggregatorResponseDo.setRawResponse(response);
				aggregatorResponseDo.setAggrResponseCode(suvidhaaResponse.getCode());
				aggregatorResponseDo.setAggrReferenceNo(suvidhaaResponse.getTransactionId());
				aggregatorResponseDo.setResponseTime(dt1);
				aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_SUVIDHAA);
				if(aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(RechargeConstants.SUVIDHAA_TXN_SUCCESS_RESPONSE_CODE)) {
					aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
				}  else {
					aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
				} 
				if(aggregatorResponseDo.getAggrResponseCode().equalsIgnoreCase(RechargeConstants.SUVIDHAA_TXN_UNDERPROCESS_RESPONSE_CODE)) {
					aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
				}
				 if(suvidhaaResponse.getCode()==null || suvidhaaResponse.getCode().isEmpty()) {
				   	aggregatorResponseDo.setAggrResponseCode(RechargeConstants.SUVIDHAA_TXN_UNDERPROCESS_RESPONSE_CODE);
				   	aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.SUVIDHAA_TRANSACTION_STATUS_NOT_FOUND);
			    } 
				} 
			}
			catch (Exception e) {
				logger.info("exception occur while unMarhalling the suvidhaa response" + e);
					}
				
	         }
			 return aggregatorResponseDo;
		}
	 
	@Override
	public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo,InResponse inResponseFromRequest) throws HttpException,ConnectException, ConnectTimeoutException, SocketException,SocketTimeoutException, IOException {
		logger.info("[IN MODERATOR] processing getTransactionStatus for suvidhaa");
		String response = "";
		Integer statusCode = null;

        HttpClient client = new HttpClient();
        client.setHttpConnectionManager(connectionManager);
        PostMethod method;
        
        if (rechargeDo.getProductType() != null && rechargeDo.getProductType().equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
        	method = new PostMethod(suvidhaaMobileStatusCheckRequestUrl);
        } else {
        	method = new PostMethod(suvidhaaDthStatusCheckRequestUrl);	
        }
        
        Map<String, Object> templateValues = new HashMap<String, Object>();
        templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TYPE, rechargeDo.getProductType());
        templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TIME, new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TOKEN_KEY, suvidhaaaccesskey);
        templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_USER_NAME, suvidhaausername);
        templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_CLIENT_TXN_ID, fcProperties.getInTransIdPrefix() + "0_" + rechargeDo.getInRequestId());
		templateValues.put(RechargeConstants.SUVIDHAA_REQUEST_TXN_DATE, new SimpleDateFormat("ddMMMyyyy").format(rechargeDo.getTransactionDate()));
		String inputXml = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "suvidhaaStatusCheckRequestTemplate.vm", templateValues);
		method.setRequestEntity(new StringRequestEntity(inputXml, RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE_VALUE, "ISO-8859-1"));
        method.setRequestHeader(RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE, RechargeConstants.SUVIDHAA_REQUEST_CONTENT_TYPE_VALUE);
        logger.info("suvidhaa request uri " + method.getURI() + " Suvidhaa request param : " + inputXml.toString().trim());
        try {
        	
        	statusCode = client.executeMethod(method);
        	if (statusCode == HttpStatus.SC_OK) {
        		response = method.getResponseBodyAsString();
          } 
        } catch (Exception exception) {
            logger.error(" Exception occured during getTransactionStatus for suvidhaa : " + exception);
        }
        logger.info("response format from suvidhaa " + response);
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_SUVIDHAA);
        aggregatorResponseDo.setRawRequest(inputXml);
        aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
        aggregatorResponseDo.setRequestTime(new Date());
        response = response.replace(RechargeConstants.SUVIDHAA_STATUS_CHECK_RESPONSE_XML_STRING, "");
        response = response.replace(RechargeConstants.SUVIDHAA_STATUS_CHECK_RESPONSE_XML_STRING1, "");
		response = response.replace("xmlns:i="+"","");
		response = response.replace("xmlns=","");
		response = response.replace("xmlns=","");
		response = response.replace("\"\"\"\"","");
		response = response.replace("\"\"","");
		try {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(response.getBytes())));
		doc.getDocumentElement().normalize();
		Element docEle = doc.getDocumentElement();
	    NodeList nList = docEle.getElementsByTagName("TransactionStatus");
	    Node nNode = nList.item(0);
	    Element eElement = (Element) nNode;
	    String statuscheckResponseCode  = eElement.getElementsByTagName("Code").item(0).getTextContent();
	    String statuscheckResponseMessage  = eElement.getElementsByTagName("Description").item(0).getTextContent();
	    aggregatorResponseDo.setAggrResponseCode(statuscheckResponseCode);
	    aggregatorResponseDo.setAggrResponseMessage(statuscheckResponseMessage);
	    aggregatorResponseDo.setRawResponse(response);
	    if(statuscheckResponseCode.equals(RechargeConstants.SUVIDHAA_TXN_UNDERPROCESS_RESPONSE_CODE)) {
	    	aggregatorResponseDo.setAggrResponseCode("UP");
	    } 
	    
		} catch(Exception exception) {
			 logger.error("exception occured during suvidhaa status check api for order number " + rechargeDo.getAffiliateTransId() + exception);
		}

        logger.info("suvidhaa response statuscode " + statusCode + "[IN MODERATOR] Get Transaction Status from suvidhaa : " + response.toString(), null);
        return aggregatorResponseDo;
	}
	
	private AggregatorResponseDo doActionAfterFail(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed) {
        logger.info("[IN MODERATOR] Recharge process fail for Suvidhaa: No Operator code found in DB(oxigen_operator_map) for operator =  " + rechargeDo.getOperatorCode(), null);
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

        /* Getting Request Time */
        Calendar cal = Calendar.getInstance();
        Date dt = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
        String requestdate = sdf.format(dt);

        parametersPassed.append("id = " + "6" + ", ");
        parametersPassed.append("clid = " + orderNo + ", ");
        parametersPassed.append("opcd = " + "" + ", ");
        parametersPassed.append("circd = " + "" + ", ");
        parametersPassed.append("spcd = " + "" + ", ");
        parametersPassed.append("mnum = " + rechargeDo.getMobileNo() + ", ");
        parametersPassed.append("amt = " + rechargeDo.getAmount() + ", ");
        parametersPassed.append("tu = " + "" + ", ");
        parametersPassed.append("rtcd = " + "ESR");

        aggregatorResponseDo.setAggrName("suvidhaa");
        aggregatorResponseDo.setRawRequest(parametersPassed.toString());
        aggregatorResponseDo.setRequestId(Long.parseLong(orderNo.substring(orderNo.indexOf("_") + 1, orderNo.length())));
        aggregatorResponseDo.setRequestTime(dt);

        aggregatorResponseDo.setAggrOprRefernceNo("");
        aggregatorResponseDo.setAggrResponseMessage("");
        aggregatorResponseDo.setRawResponse("");
        aggregatorResponseDo.setResponseTime(dt);
        aggregatorResponseDo.setAggrResponseCode(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
        aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
        return aggregatorResponseDo;
    }
	
		public void setSuvidhaamobilerequesturl(String suvidhaamobilerequesturl) {
		this.suvidhaamobilerequesturl = suvidhaamobilerequesturl;
	}
		
		public void setSuvidhaaDthStatusCheckRequestUrl(
				String suvidhaaDthStatusCheckRequestUrl) {
			this.suvidhaaDthStatusCheckRequestUrl = suvidhaaDthStatusCheckRequestUrl;
		}

		public void setSuvidhaaMobileStatusCheckRequestUrl(
				String suvidhaaMobileStatusCheckRequestUrl) {
			this.suvidhaaMobileStatusCheckRequestUrl = suvidhaaMobileStatusCheckRequestUrl;
		}

		public void setSuvidhaadthrequesturl(String suvidhaadthrequesturl) {
		this.suvidhaadthrequesturl = suvidhaadthrequesturl;
	}


		public void setSuvidhaausername(String suvidhaausername) {
			this.suvidhaausername = suvidhaausername;
		}

		public void setSuvidhaaaccesskey(String suvidhaaaccesskey) {
			this.suvidhaaaccesskey = suvidhaaaccesskey;
		}

		public void setConnectionManager(
				MultiThreadedHttpConnectionManager connectionManager) {
			this.connectionManager = connectionManager;
		}
		
	    @Override
	    public Double getWalletBalance() throws IOException {
	        throw new RuntimeException("Not implemented");
	    }
	    
	    @Override
	    protected Double getBalanceWarnThreshold() {
	        throw new RuntimeException("Not implemented");
	    }

	    @Override
	    protected Double getBalanceCriticalThreshold() {
            throw new RuntimeException("Not implemented");
	    }


        @Override
        public String getBalanceCachekey() {
            throw new RuntimeException("Not implemented");
        }


        @Override
        public String getBalanceUpdateTimeCachekey() {
            throw new RuntimeException("Not implemented");
        }
}
