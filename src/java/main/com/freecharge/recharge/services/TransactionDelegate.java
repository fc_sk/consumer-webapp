package com.freecharge.recharge.services;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.PlanLevelRetryConfig;
import com.freecharge.app.domain.entity.jdbc.InOprAggrPriority;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.service.Wallet.FundDestination;

@Service
public class TransactionDelegate {
	private static Logger logger = LoggingFactory.getLogger(TransactionDelegate.class);

	@Autowired
	private CommonRechargeService commonRechargeService;

	@Autowired
	private InService inService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
	@Autowired
	private OperatorCircleService operatorCircleService;
	
	@Autowired
	OrderIdWriteDAO orderIdDAO;
	
	@Autowired
	OrderIdSlaveDAO orderIdSlaveDao;

	@Autowired
	TxnHomePageDAO txnHomePageDAO;
	
	@Autowired
	AppConfigService appConfigService;
	
    @Autowired
    private FulfillmentScheduleService fulfillmentScheduleService;
    
    @Autowired
    private UserTransactionHistoryService transactionHistoryService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private RechargeRetryConfigReadThroughCache rechargeRetryCache;
    
	private boolean validateRequest(RechargeDo rechargeDo) {
		logger.info("[IN MODERATOR] validate Request and insert in DB "+ rechargeDo);
		boolean isValid = false;
		try {
			if (
			        rechargeDo.getAmount() > 0 && 
			        StringUtils.isNotBlank(rechargeDo.getMobileNo()) &&
			        StringUtils.isNotBlank(rechargeDo.getProductType()) &&
			        StringUtils.isNotBlank(rechargeDo.getProductType()) &&
			        rechargeDo.getCircleOperatorMapping() != null && 
			        rechargeDo.getCircleOperatorMapping().getCircleOperatorMappingId() > 0
			        ) {
			    
			    logger.info("Recharge details mobile Numnber" +rechargeDo.getMobileNo()+"OperatorMasterId"+rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId()+"CircleMasterID"+rechargeDo.getCircleOperatorMapping().getFkCircleMasterId());
		       
			    if (rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId() != null && 
		                rechargeDo.getCircleOperatorMapping().getFkCircleMasterId() != null) {
					isValid = true;
				}
			}
		} catch (Exception e) {
			logger.error("[IN MODERATOR] Error in validate Request and insert in DB ", e);
		}
		return isValid;
	}

	public InResponse rechargeFromIN(RechargeDo rechargeDo) {
		logger.info("[IN MODERATOR] Processing for recharge from IN (RechargeDo object) ", null);
		InResponse inResponse = null; 
		String priorityArray = null;
		try {
		    if (validateRequest(rechargeDo)) {
		        
		        boolean safeForRetry = inService.validateForRechargeRetry(rechargeDo.getOrderId());
		        
		        if (!safeForRetry) {
		            logger.info("Not safe for retry; so not going ahead with recharge : [" + rechargeDo.getOrderId() + "]");
		            return null;
		        }
		        
                getRechargePlanType(rechargeDo);
                
                InOprAggrPriority priority = operatorCircleService.getInOprAggrPriority(rechargeDo.getCircleOperatorMapping().getCircleOperatorMappingId());
                if (priority!=null) {
                    priorityArray = (priority.getAggrPriority().split(","))[0].toLowerCase();
                }
                
                if(rechargeDo.getRechargeRetryCounter()==0){
                    transactionHistoryService.markRechargeInit(rechargeDo);
                	paymentTransactionService.assertPaymentSuccessAndDebitWallet(rechargeDo.getOrderId(), FundDestination.RECHARGE, RechargeConstants.RECHARGE_TYPE);
                	boolean scheduleCreated = fulfillmentScheduleService.checkAndcreateSchedule(rechargeDo);
                    if (scheduleCreated) {
                        logger.info("Created schedule for recharge for orderID "+ rechargeDo.getAffiliateTransId() +". Exiting");
                        return null;
                    }
                    inService.insertRequest(rechargeDo);
                    inService.insertResponse(rechargeDo,priorityArray);
                }else{
                	updateInRequest(rechargeDo);
                }
                
                inResponse = doRecharge(rechargeDo, priority);
                
                if (rechargeDo.getRechargeRetryCounter() > 0
                        && AggregatorInterface.SUCCESS_IN_RESPONSE_CODE.equals(inResponse.getInRespCode())) {
                    transactionHistoryService.markRechargeInit(rechargeDo);
                }
		    } else {
		        logger.error("Validation failed; not going ahead with recharge for orderID: [" + rechargeDo.getAffiliateTransId() + "]");
		    }
		} catch (Exception e) {
		    logger.error(String.format("Error in processing recharge order id %s. Exception %s", rechargeDo.getAffiliateTransId(), ExceptionUtils.getFullStackTrace(e)));
		    List<InRequest> list = inService.getAllInRequests(rechargeDo.getOrderId());
		    if(list == null || list.isEmpty()){
		    	logger.info("Capturing No recharge Attempt for orderId "+ rechargeDo.getOrderId());
		    	transactionHistoryService.updateStatus(rechargeDo.getOrderId(), RechargeConstants.DUMMY_RECHARGE_FAILURE_CODE);
		    	kestrelWrapper.enqueueUPUpdate(rechargeDo.getOrderId());
		    	metricsClient.recordEvent("Recharge", "Missed.recharge", "Exception");
		    }		   
		}
		
		return inResponse;
	}

	private InResponse doRecharge(RechargeDo rechargeDo, InOprAggrPriority priority) {
		InResponse inResponse = new InResponse();
		try {
			inResponse = commonRechargeService.getRecharge(rechargeDo, inResponse, priority);
		} catch (Exception e) {
			logger.error("[IN MODERATOR] Error in Processing doRecharge from delegator for IN :", e);
		}
		return inResponse;
	}

	private RechargeDo getRechargePlanType(RechargeDo rechargeDo) {
	    String lookupId = rechargeDo.getLookupId();
	    
	    String rechargeType = orderIdSlaveDao.getRechargeTypeByLookupId(lookupId);
	    
	    //Added when plan level retry happens
		if (rechargeRetryCache.getPlanLevelRetryKey(rechargeDo.getAffiliateTransId())) {
			PlanLevelRetryConfig retryCache = rechargeRetryCache
					.getPlanLevelRetryData(rechargeDo.getAffiliateTransId());
			if (retryCache != null && StringUtils.isNotBlank(retryCache.getPlanType())) {
				rechargeType = retryCache.getPlanType();
			}
		}
		rechargeDo.setRechargePlanType(rechargeType);
	    return rechargeDo;
	}
	
	private void updateInRequest(RechargeDo rechargeDo){
        Calendar calendar = Calendar.getInstance();
        InRequest inRequest = new InRequest();
        inRequest.setAffiliateId(rechargeDo.getAffiliateid());
        inRequest.setAffiliateTransId(rechargeDo.getAffiliateTransId());
        inRequest.setProductType(rechargeDo.getProductType());
        inRequest.setAmount(rechargeDo.getAmount());
        inRequest.setSubscriberNumber(rechargeDo.getMobileNo());
        inRequest.setOperator(rechargeDo.getOperatorCode());
        inRequest.setCircle(rechargeDo.getCircleCode());

        inRequest.setRawRequest("MobileNo : " + rechargeDo.getMobileNo() + ", Affiliate Id : "
                + rechargeDo.getAffiliateid() + ", AffiliateTransId : " + rechargeDo.getAffiliateTransId()
                + ", ProductType : " + rechargeDo.getProductType() + ", OperatorCode : "
                + rechargeDo.getOperatorCode() + ", CircleCode : " + rechargeDo.getCircleCode() + ", Amount : "
                + rechargeDo.getAmount());

        inRequest.setCreatedTime(new Timestamp(calendar.getTimeInMillis()));
        inRequest.setUpdatedTime(new Timestamp(calendar.getTimeInMillis()));
        inRequest.setRechargePlan(rechargeDo.getRechargePlanType());
        inRequest.setRequestId(rechargeDo.getInRequestId());
        inService.update(inRequest);
	 }
	
    public InResponse doRewardRecharge(RechargeDo rechargeDo) {
        logger.info("[IN MODERATOR] Processing for recharge from IN (RechargeDo object) ", null);
        InResponse inResponse = null;
        String priorityArray = null;
        try {
            if (validateRequest(rechargeDo)) {

                boolean safeForRetry = inService.validateForRechargeRetry(rechargeDo.getOrderId());
                if (!safeForRetry) {
                    logger.info(
                            "Not safe for retry; so not going ahead with recharge : [" + rechargeDo.getOrderId() + "]");
                    return null;
                }
                InOprAggrPriority priority = operatorCircleService
                        .getInOprAggrPriority(rechargeDo.getCircleOperatorMapping().getCircleOperatorMappingId());
                if (priority != null) {
                    priorityArray = (priority.getAggrPriority().split(","))[0].toLowerCase();
                }

                if (rechargeDo.getRechargeRetryCounter() == 0) {
                    boolean scheduleCreated = fulfillmentScheduleService.checkAndcreateSchedule(rechargeDo);
                    if (scheduleCreated) {
                        logger.info("Created schedule for recharge for orderID " + rechargeDo.getAffiliateTransId()
                                + ". Exiting");
                        return null;
                    }
                    inService.insertRequest(rechargeDo);
                    inService.insertResponse(rechargeDo, priorityArray);
                } else {
                    updateInRequest(rechargeDo);
                }

                inResponse = doRecharge(rechargeDo, priority);
                if (rechargeDo.getRechargeRetryCounter() > 0
                        && AggregatorInterface.SUCCESS_IN_RESPONSE_CODE.equals(inResponse.getInRespCode())) {
                    transactionHistoryService.markRechargeInit(rechargeDo);
                }
            } else {
                logger.error("Validation failed; not going ahead with recharge for orderID: ["
                        + rechargeDo.getAffiliateTransId() + "]");
            }
        } catch (Exception e) {
            logger.error(String.format("Error in processing recharge order id %s. Exception %s",
                    rechargeDo.getAffiliateTransId(), ExceptionUtils.getFullStackTrace(e)));
        }

        return inResponse;
    }
	
}
