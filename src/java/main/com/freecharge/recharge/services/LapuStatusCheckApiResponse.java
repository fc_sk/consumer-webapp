package com.freecharge.recharge.services;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RechargeStatus")
public class LapuStatusCheckApiResponse {

    private String OrderId;
    private String MerOid;
    private String Mobile;
    private String Denomination;
    private String Status;
    private String OperatorTxnId;
    private String Description;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getOperatorTxnId() {
        return OperatorTxnId;
    }

    public void setOperatorTxnId(String operatorTxnId) {
        OperatorTxnId = operatorTxnId;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getDenomination() {
        return Denomination;
    }

    public void setDenomination(String denomination) {
        Denomination = denomination;
    }

    public String getMerOid() {
        return MerOid;
    }

    public void setMerOid(String merOid) {
        MerOid = merOid;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

}
