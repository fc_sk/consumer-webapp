package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.util.RechargeConstants;

/**
 * This is the base class for all dummy recharge aggregator service classes - used only for testing
 */
public abstract class AbstractDummyService implements AggregatorInterface {
    private static Logger logger = LoggingFactory.getLogger(AbstractDummyService.class);
    
    private static final ConcurrentMap<String, AtomicInteger> attempts = new ConcurrentHashMap<>();
    
    @Autowired
    MetricsClient metricsClient;

    private static String STATUS_RC_SETTER_PREFIX = "AG_";
    private static String STATUS_RC_SETTER_SUFFIX = "_STATUS_RC=";

    abstract String getMockeeName();

    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer parametersPassed, List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        metricsClient.timeStampEvent(rechargeDo.getAffiliateTransId(), MetricsClient.EventName.RechargeAttempt);
        
        metricsClient.recordLatency("UserSession", "RechargeAttempt", rechargeDo.getAffiliateTransId(), MetricsClient.EventName.PaySuccess);

        
	logger.info("[MOCK] Doing a mock recharge with for: " + getMockeeName());
	
	AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
	aggregatorResponseDo.setAggrName(getMockeeName());
	aggregatorResponseDo.setAggrOprRefernceNo("1234");
	aggregatorResponseDo.setAggrReferenceNo("1234");
	aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
	aggregatorResponseDo.setRequestTime(new Date());
	aggregatorResponseDo.setResponseTime(new Date());
	
	String mobileNo = rechargeDo.getMobileNo();
	
	// 93421 38044
	// check for last 2 digits
	String aggrResponseCode = mobileNo.substring(mobileNo.length() - 2);
    String transactionStatus = "";
    
    if (aggrResponseCode.equals("10")) {
        throw new IOException("Mock IO Exception for order ID: " + rechargeDo.getAffiliateTransId());
    }
    /*
     * Dummy Test code here
     */
    /*
    if (rechargeDo.getOperatorCode().equalsIgnoreCase("vodafone")) {
        aggrResponseCode="29";
    } else if (rechargeDo.getOperatorCode().equalsIgnoreCase("airtel")) {
        aggrResponseCode="08";
    }
    */
    if (aggrResponseCode.equals("00")) {
        transactionStatus = RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE;    
    } else if (aggrResponseCode.equals("08")) {
        transactionStatus = RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS;
    } else {
        transactionStatus = RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL;    
    }
    
    String aggrResponseMessage = "Message from dummy aggregator: " + getMockeeName();

    if (aggrResponseCode.equals("11")) {
        aggrResponseCode = "ETI";
        aggrResponseMessage = "Sorry, Failed due to temp failure";
    }
    
    if (aggrResponseCode.equals("28")) {
        aggrResponseCode = "AI";
        aggrResponseMessage = "Sorry, Invalid amount, Please try doing a recharge for a valid denomination";
    }
    
    if (aggrResponseCode.equals("29")) {
        aggrResponseCode = "CI";
        aggrResponseMessage = "Invalid Mobile / Subscriber No";
    }
    
    if (aggrResponseCode.equals("30")) {
        aggrResponseCode = "EG";
        aggrResponseMessage = "Operator System General Error";
    }
    
    if (aggrResponseCode.equals("31")) {
        aggrResponseCode = "ECI";
        aggrResponseMessage = "Invalid Mobile Number";
    }
    
    if (aggrResponseCode.equals("08")) {
        aggrResponseCode = "UP";
        aggrResponseMessage = "Transaction under process.";
    }

    if (aggrResponseCode.equals("32")) {
        aggrResponseCode = "ES";
        aggrResponseMessage = "Operator Is Down";

        int maxTries = Character.getNumericValue(mobileNo.charAt(7));
        
        if (maxTries != 0) {
            if (attempts.containsKey(mobileNo)) {
                int triesSoFar = attempts.get(mobileNo).get();
                
                logger.info("max; tries so far" + maxTries + ":" + triesSoFar);
                
                if (triesSoFar > maxTries) {
                    aggrResponseCode = "00";
                    aggrResponseMessage = "Retry Success";
                }
            } else {
                logger.info("max; tries so far" + maxTries + ": null");
            }
        }
    }
    
    if (aggrResponseCode.equals("48")) {
        throw new ConnectTimeoutException();
    }
    
    if (aggrResponseCode.equals("58")) {
        throw new NetworkConnectTimeoutException();
    }
    
    aggregatorResponseDo.setAggrResponseCode(aggrResponseCode);
    aggregatorResponseDo.setAggrResponseMessage(aggrResponseMessage);
	aggregatorResponseDo.setRawRequest(parametersPassed.toString());
	aggregatorResponseDo.setRawResponse("Raw response from the dummy aggregator: " + getMockeeName());
    aggregatorResponseDo.setTransactionStatus(transactionStatus);

	logger.info("[MOCK] returning responseCode=" + aggregatorResponseDo.getAggrResponseCode());

	if (attempts.putIfAbsent(rechargeDo.getMobileNo(), new AtomicInteger(1)) != null) {
        attempts.get(rechargeDo.getMobileNo()).incrementAndGet();
    }
	
	return aggregatorResponseDo;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponseFromRequest, List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,ConnectTimeoutException, SocketException, SocketTimeoutException,IOException {
	logger.info("[MOCK] Getting mock transaction status: " + getMockeeName());
	
	AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
	aggregatorResponseDo.setAggrName(getMockeeName());
	aggregatorResponseDo.setAggrOprRefernceNo("1234");
	aggregatorResponseDo.setAggrReferenceNo("1234");
	aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
	aggregatorResponseDo.setRequestTime(new Date());
	aggregatorResponseDo.setResponseTime(new Date());

	   String mobileNo = rechargeDo.getMobileNo();
	    
	    // 93421 38044
	// check for last 2 digits
	   String aggrResponseCode = mobileNo.substring(mobileNo.length() - 2);
	    String transactionStatus = "";
	    
	    if (aggrResponseCode.equals("00")) {
	        transactionStatus = "success";    
	    } else if (aggrResponseCode.equals("08")) {
	        transactionStatus = "underprocess";
	    } else {
	        transactionStatus = "failure";    
	    }

	// Change the following attributes based on parameters
	aggregatorResponseDo.setAggrResponseCode(aggrResponseCode); // SUCCESS
	aggregatorResponseDo.setAggrResponseMessage("Message from dummy aggregator: " + getMockeeName());
	aggregatorResponseDo.setRawResponse("Raw response from the dummy aggregator: " + getMockeeName());
	aggregatorResponseDo.setTransactionStatus(transactionStatus);

	// Address based
	String rcSetterStr = STATUS_RC_SETTER_PREFIX + getMockeeName().toUpperCase() + STATUS_RC_SETTER_SUFFIX;
	logger.debug("[MOCK] rcSetterStr=" + rcSetterStr);

	logger.info("[MOCK] returning responseCode=" + aggrResponseCode);
	
	return aggregatorResponseDo;
    }
}
