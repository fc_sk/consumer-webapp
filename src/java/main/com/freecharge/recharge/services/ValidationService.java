package com.freecharge.recharge.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.DTHValidationConstants;
import com.freecharge.recharge.businessDo.DTHValidationResponse;
import com.freecharge.recharge.businessDo.EuroDTHValidationResponseDo;
import com.google.common.collect.ImmutableList;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * @author fcaa17922
 * Will be used to validate the number using Euronet Service.
 * Will fetch the details from cache and then proceed accordingly whether to validate or not for the given operator
 *
 */
@Service
public class ValidationService {
	
	@Autowired
	AppConfigService appConfigService;
	
	@Autowired
	EuronetService euronetService;
	
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	/*
	 * VALIDATE Recharge custom config will be of the form where we will
	 *  have separate keys for all product types such as dthOperators , rechargeOperators etc.
	 *  And also separate keys for aggregators like euoronet.connection.timeout, oxigen.connection.timoeut etc
	 *  
	 * 
	 */
	
	
	public DTHValidationResponse validateNumber(String productType, String operatorId, String circleId, String serviceNumber, Float amount){
		JSONObject configJson = appConfigService.getCustomProp(appConfigService.VALIDATE_RECHARGE_CUSTOM_CONFIG);
		String configString = configJson.toJSONString();
		Map<String, Object> configMap = getMapFromString(configString);
		List<String> operatorList = getListFromCommaSeparatedString((String) configMap.get("dthOperators"));
		final List<String> errorCodeList = getListFromCommaSeparatedString((String) configMap.get("euronet.validation.error.codes"));
		logger.info("The euronet connection time out value is : " + configMap.get("euronet.connection.timeout"));
		String euronetConnectionTimeoutString = (String) configMap.get("euronet.connection.timeout");
		Integer euronetConnectionTimeout = Integer.parseInt(euronetConnectionTimeoutString) ;
		
	    //final List<String> invalidCodes = ImmutableList.of("ECI","CI","EMI");
		logger.info("The error code list from app config is : " + errorCodeList);
		logger.info("The euronet time out value from app config is : " + euronetConnectionTimeout);
		logger.info("The operator list from app config is : " + operatorList);
		
		DTHValidationResponse dthValidationResponse = new DTHValidationResponse(DTHValidationConstants.NO_ERROR,DTHValidationConstants.VALID_DTH_NUMBER_RESPONSE_MESSAGE);
	    if(operatorList.contains(operatorId)){
	       Integer opId = Integer.parseInt(operatorId);
	       EuroDTHValidationResponseDo euroDTHValidationResponse = euronetService.validateNumber(productType, opId, circleId, serviceNumber, euronetConnectionTimeout, amount);
	       if(euroDTHValidationResponse != null){
	    	   logger.info("The validation error code from Euronet is : " + euroDTHValidationResponse.getResponseCode());
	    	   if(errorCodeList.contains(euroDTHValidationResponse.getResponseCode())){
	    		  dthValidationResponse.setResponseCode(DTHValidationConstants.DTH_INVALID_NUMBER_ERROR_CODE);
	    		  dthValidationResponse.setResponseMessage(DTHValidationConstants.INVALID_DTH_NUMBER_RESPONSE_MESSAGE);
	    	   }
	       	}
	    }
		return dthValidationResponse;
	}
	
	
	@SuppressWarnings("serial")
	private Map<String, Object> getMapFromString(String configString){
		Map<String, Object> map = new Gson().fromJson(configString, new TypeToken<HashMap<String, String>>() {
		}.getType());
		return map;
	}
	
	private List<String> getListFromCommaSeparatedString(String operatorString){
		List<String> operatorsList = new ArrayList<String>();
		String [] opArray = operatorString.split(",");
		
		for(String operator : opArray){
			operatorsList.add(operator);
		}
		return operatorsList;
	}
	
}
