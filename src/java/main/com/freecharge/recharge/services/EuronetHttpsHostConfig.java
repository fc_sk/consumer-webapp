package com.freecharge.recharge.services;

import org.apache.commons.httpclient.HostConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;

@Component
public class EuronetHttpsHostConfig {

//	@Autowired
	private HostConfiguration euronetHostConfig = new HostConfiguration();
	
	@Autowired
	private FCProperties fcProperties;
	
	public HostConfiguration getHttpsConfig() {
		euronetHostConfig.setHost(fcProperties.getProperty(FCConstants.EURONET_HTTPS_HOST), 443, "https");
		return euronetHostConfig;
	}
}
