package com.freecharge.recharge.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;

public class PlanValidatorCron {

	private static final Logger logger = LoggingFactory
			.getLogger(PlanValidatorCron.class);
	
	@Autowired
	PlanValidatorService service;
	
	public void populatePlanStatus() {
		logger.info("About to populate Plan Status for TataDocomo");
		service.populatePlanValidity();
		logger.info("Done populating Plan Status for TataDocomo");		
	}
}
