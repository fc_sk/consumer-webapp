

package com.freecharge.recharge.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.GenericMisDataFetchDAO;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.service.GenericMisDisplayRecords;
import com.freecharge.recharge.businessdao.GenericMisData;
import com.freecharge.recharge.businessdao.INAuthenticationFetchDAO;
import com.freecharge.recharge.businessdao.MISDisplayRecord;
import com.freecharge.recharge.businessdao.MisRequestData;
public class InAuthenticationFetchService {
	
	@Autowired
	private INAuthenticationFetchDAO inAuthenticationFetchDAO;
	
	@Autowired
	private GenericMisDataFetchDAO genericMisDataFetchDAO;

	public boolean isMisUserValid(AdminUsers adminUsers)
	{
	return inAuthenticationFetchDAO.isMisUserValid(adminUsers);
	}
	
	public List<MISDisplayRecord> getMisData(MisRequestData misRequestData, int pageNumber)
	{
		return inAuthenticationFetchDAO.getMisData(misRequestData,pageNumber);
	}
	
	public Integer getMisDataCount(MisRequestData misRequestData)
	{
		return inAuthenticationFetchDAO.getMisDataCount(misRequestData);
	}
	
	public Integer getGenericMisDataCount(GenericMisData genericMisData){
		return genericMisDataFetchDAO.getMisDataCount(genericMisData);
	}
	
	public List<GenericMisDisplayRecords> getGenericMisData(GenericMisData misRequestData, int pageNumber)
	{
		return genericMisDataFetchDAO.getMisData(misRequestData,pageNumber);
	}
	
}
