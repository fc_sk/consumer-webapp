package com.freecharge.recharge.services;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.thoughtworks.xstream.XStream;

/**
 * Created by Vipin Goyal on june 19, 2014
 */
public class LapuRechargeService extends AggregatorService {

    private static Logger logger = LoggingFactory.getLogger(LapuRechargeService.class);

    @Autowired
    private FCProperties fcProperties;
    private String resellerId;
    private String resellerPass;
    private String rechargeApiUrl;
    private String statusCheckApiUrl;
    private String balanceCheckApiUrl;
    private String listOfOperatorsApiUrl;
    private HttpClient rechargeHttpClient;
    private HttpClient statusCheckHttpClient;
    private HostConfiguration hostConfig;
    private HttpConnectionManagerParams connectionManagerConfig;

    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderId, StringBuffer parametersPassed,
            List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {

        SimpleHttpConnectionManager connectionManager = new SimpleHttpConnectionManager(true);
        connectionManager.setParams(connectionManagerConfig);
        HttpClient rechargeClient = new HttpClient(connectionManager);
        rechargeClient.setHostConfiguration(hostConfig);

        String requestParam = createRequestParam(rechargeDo);
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        PostMethod method = new PostMethod();
        try {
            Boolean shouldTimeOut = null;
            Integer timeOutValue = null;
            String operatorCode = null;

            if (inAggrOprCircleMaps != null && inAggrOprCircleMaps.size() > 0) {
                for (InAggrOprCircleMap inAggrOprCircleMap : inAggrOprCircleMaps) {
                    operatorCode = inAggrOprCircleMap.getAggrOperatorCode();
                    shouldTimeOut = inAggrOprCircleMap.getShouldTimeOut();
                    timeOutValue = inAggrOprCircleMap.getTimeOutInMillis();
                }
            } else {
                return doActionAfterFail(rechargeDo, requestParam);
            }

            method = createRechargeRequest(rechargeDo, operatorCode);

            String response = null;

            int statusCode = executeWithTimeout(orderId, rechargeClient, method, shouldTimeOut, timeOutValue);
            metricsClient.recordEvent("Recharge", "Lapu.HttpStatusCode", String.valueOf(statusCode));

            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
            } else {
                logger.info("Received HTTP status code for Lapu : " + statusCode + " for order id " + orderId);
            }
            logger.info("Received lapu response : " + response + " for order id " + orderId);
            aggregatorResponseDo = handleLapuRechargeApiResponse(response, requestParam, rechargeDo);

            if (aggregatorResponseDo != null && aggregatorResponseDo.getAggrResponseCode() != null) {
                metricsClient.recordEvent("Recharge", "Lapu.ResponseCode", aggregatorResponseDo.getAggrResponseCode());
                metricsClient.recordEvent("Recharge", "Lapu.ResponseCode." + operatorCode,
                        aggregatorResponseDo.getAggrResponseCode());
            }

        } catch (Exception exception) {
            logger.error("Exception raised during lapu recharge api request for orderid=" + orderId);
            throw exception;
        } finally {
            method.releaseConnection();
        }
        return aggregatorResponseDo;
    }

    private AggregatorResponseDo handleLapuRechargeApiResponse(String response, String requestParam,
            RechargeDo rechargeDo) throws HttpException, ConnectException, ConnectTimeoutException, SocketException,
            SocketTimeoutException, IOException {

        LapuRechargeResponse respObj = new LapuRechargeResponse();
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        String orderId = rechargeDo.getAffiliateTransId();

        try {
            if (!StringUtils.isBlank(response)) {
                response = response.replace("<?xml version=" + "\"1.0\"" + "?>", "");
                XStream xstream = new XStream();
                xstream.alias("Data", LapuRechargeResponse.class);
                respObj = (LapuRechargeResponse) xstream.fromXML(response);
            } else {
                respObj.setOperatorTxnId(null);
                respObj.setDescription("Received blank or null response from lapu recharge api response");
                respObj.setStatus(RechargeConstants.TRANSACTION_STATUS_PENDING);
                respObj.setMerOid(null);
            }

            aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_LAPU);
            aggregatorResponseDo.setRawRequest(requestParam);
            aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
            aggregatorResponseDo.setAggrOprRefernceNo(respObj.getOperatorTxnId());
            aggregatorResponseDo.setAggrResponseMessage(respObj.getDescription());
            aggregatorResponseDo.setRawResponse(response);
            aggregatorResponseDo.setTransactionStatus(respObj.getStatus());
            aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            aggregatorResponseDo.setAggrReferenceNo(respObj.getOrderId());
            aggregatorResponseDo.setRequestTime(new Date());
            aggregatorResponseDo.setAggrResponseCode(respObj.getStatus());

            if (respObj.getStatus().equalsIgnoreCase(RechargeConstants.TRANSACTION_STATUS_PENDING)) {
                aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS);
            }
            logger.info("Recharge request Url " + rechargeApiUrl + " with param " + requestParam
                    + " with Recharge Response from Lapu " + response + " for orderId recharge from lapu " + orderId);
        } catch (Exception exception) {
            logger.error("Exception raised while handling lapu response for orderid= " + orderId);
            throw exception;
        }

        return aggregatorResponseDo;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse inResponse)
            throws HttpException, ConnectException, ConnectTimeoutException, SocketException, SocketTimeoutException,
            IOException {
        String orderId = rechargeDo.getAffiliateTransId();
        logger.info("About to call Lapu status check for Order ID: " + orderId);
        String response = "";
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        PostMethod method = createStatusCheckRequest(rechargeDo, inResponse);

        try {
            int statusCode = this.getStatusCheckHttpClient().executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
                LapuStatusCheckApiResponse respObj = new LapuStatusCheckApiResponse();

                if (!StringUtils.isBlank(response)) {
                    response = response.replace("<?xml version=" + "\"1.0\"" + "?>", "");
                    XStream xstream = new XStream();
                    xstream.alias("RechargeStatus", LapuStatusCheckApiResponse.class);
                    respObj = (LapuStatusCheckApiResponse) xstream.fromXML(response);
                } else {
                    respObj.setOperatorTxnId(null);
                    respObj.setDescription("Received blank or null response from lapu status check api");
                    respObj.setStatus(RechargeConstants.TRANSACTION_STATUS_PENDING);
                    respObj.setMerOid(Long.toString(inResponse.getRequestId()));
                    respObj.setOrderId(inResponse.getAggrTransId());
                }

                aggregatorResponseDo = handleLapuStatusCheckApiresponse(respObj, response);

                logger.info("status check api response for lapu for order id " + orderId + "  " + response);

            } else {
                logger.info("Received HTTP status code " + " for Lapu=" + statusCode + "orderid=" + orderId);
                aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            method.releaseConnection();
        }
        logger.info("status check response from Lapu=" + response + "orderId=" + orderId);
        return aggregatorResponseDo;
    }

    private AggregatorResponseDo handleLapuStatusCheckApiresponse(LapuStatusCheckApiResponse respObj, String response) {

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        if (RechargeConstants.TRANSACTION_STATUS_SUCCESS.equals(respObj.getStatus())) {
            aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
            aggregatorResponseDo.setAggrResponseCode(RechargeConstants.EURONET_SUCCESS_RESPONSE_CODE);
            aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
            aggregatorResponseDo.setRawResponse(response);
            aggregatorResponseDo.setAggrReferenceNo(respObj.getOrderId());
        }
        if (RechargeConstants.TRANSACTION_STATUS_FAILED.equals(respObj.getStatus())) {
            aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            aggregatorResponseDo.setAggrResponseCode(respObj.getStatus());
            aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            aggregatorResponseDo.setRawResponse(response);
            aggregatorResponseDo.setAggrReferenceNo(respObj.getOrderId());
        }
        if (RechargeConstants.TRANSACTION_STATUS_PENDING.equals(respObj.getStatus())) {
            aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            aggregatorResponseDo.setAggrResponseCode(RechargeConstants.EURONET_UNDER_PROCESS_CODE);
            aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
            aggregatorResponseDo.setRawResponse(response);
            aggregatorResponseDo.setAggrReferenceNo(respObj.getOrderId());
        }
        return aggregatorResponseDo;
    }

    @Override
    public Double getWalletBalance() throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

        PostMethod getBalanceRequest = new PostMethod("/transservice.aspx");

        getBalanceRequest.setParameter("Transid", String.valueOf(System.currentTimeMillis()));
        getBalanceRequest.setParameter("merchantrefno", "getbalance");
        getBalanceRequest.setParameter("requestdate", sdf.format(new Date()));
        getBalanceRequest.setParameter("status", "0");
        getBalanceRequest.setParameter("bankrefno", "test01");
        getBalanceRequest.setParameter("amount", "0");

        int statusCode = this.getRechargeHttpClient().executeMethod(getBalanceRequest);

        String getBalanceResponse = getBalanceRequest.getResponseBodyAsString();
        logger.info("Lapu Balance Response: " + getBalanceResponse);

        if (statusCode == HttpStatus.SC_OK) {
            String responseMessage = getBalanceResponse.split("\\|")[1];

            if (responseMessage.contains("your merchant account balance is")) {
                String balanceAmount = responseMessage.split(" ")[9];
                return new Double(balanceAmount);
            }
        }

        throw new IOException("Did not valid balance response from Lapu");
    }

    private AggregatorResponseDo doActionAfterFail(RechargeDo rechargeDo, String parametersPassed) {
        String orderId = rechargeDo.getAffiliateTransId();
        logger.info("No data found in DB" + " operator=" + rechargeDo.getOperatorCode() + " orderid=" + orderId);
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();
        aggregatorResponseDo.setAggrName(AggregatorInterface.LAPU_IN);
        aggregatorResponseDo.setRawRequest(parametersPassed);
        aggregatorResponseDo.setRequestId(rechargeDo.getInRequestId());
        aggregatorResponseDo.setAggrOprRefernceNo("");
        aggregatorResponseDo.setAggrResponseMessage("Entry missing in database");
        aggregatorResponseDo.setRawResponse("");
        aggregatorResponseDo.setAggrResponseCode("1012");
        aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
        return aggregatorResponseDo;
    }

    private PostMethod createRechargeRequest(RechargeDo rechargeDo, String operatorCode) {

        PostMethod method = new PostMethod(rechargeApiUrl);
        method.setParameter("operatorid", operatorCode);
        method.setParameter("circleid", "*");
        method.setParameter("reseller_id", resellerId);
        method.setParameter("reseller_pass", resellerPass);
        method.setParameter("mobilenumber", rechargeDo.getMobileNo());
        method.setParameter("meroid", Long.toString(rechargeDo.getInRequestId()));
        method.setParameter("voucher", "");
        method.setParameter("denomination", Float.toString(rechargeDo.getAmount()));
        method.setParameter("textmode", "1");
        return method;
    }

    private PostMethod createStatusCheckRequest(RechargeDo rechargeDo, InResponse inResponse) {

        PostMethod method = new PostMethod(statusCheckApiUrl);
        method.setParameter("reseller_id", resellerId);
        method.setParameter("reseller_pass", resellerPass);
        method.setParameter("orderid", inResponse.getAggrTransId());
        method.setParameter("meroid", Long.toString(inResponse.getRequestId()));
        return method;
    }

    private String createRequestParam(RechargeDo rechargeDo) {

        StringBuilder parametersPassed = new StringBuilder();
        parametersPassed.append("operatorid = " + "9" + ", ");
        parametersPassed.append("textmode = " + "1" + ", ");
        parametersPassed.append("circleid = " + "*" + ", ");
        parametersPassed.append("reseller_id = " + resellerId + ", ");
        parametersPassed.append("reseller_pass = " + resellerPass + ", ");
        parametersPassed.append("mobilenumber = " + rechargeDo.getMobileNo() + ", ");
        parametersPassed.append("meroid = " + rechargeDo.getAffiliateTransId() + ", ");
        parametersPassed.append("voucher = " + "" + ", ");
        parametersPassed.append("amount = " + rechargeDo.getAmount().toString());
        return parametersPassed.toString();
    }

    @Override
    protected Double getBalanceWarnThreshold() {
        return fcProperties.getEuronetBalanceWarnThreshold();
    }

    @Override
    protected Double getBalanceCriticalThreshold() {
        return fcProperties.getEuronetBalanceCriticalThreshold();
    }

    public HttpConnectionManagerParams getConnectionManagerConfig() {
        return connectionManagerConfig;
    }

    public void setConnectionManagerConfig(HttpConnectionManagerParams connectionManagerConfig) {
        this.connectionManagerConfig = connectionManagerConfig;
    }

    public HostConfiguration getHostConfig() {
        return hostConfig;
    }

    public void setHostConfig(HostConfiguration hostConfig) {
        this.hostConfig = hostConfig;
    }

    @Override
    public String getBalanceCachekey() {
        return OxigenService.OXIGEN_BALANCE_KEY;
    }

    @Override
    public String getBalanceUpdateTimeCachekey() {
        return OxigenService.OXIGEN_BALANCE_LAST_UPDATE_TIME_KEY;
    }

    public HttpClient getStatusCheckHttpClient() {
        return statusCheckHttpClient;
    }

    public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
        this.statusCheckHttpClient = statusCheckHttpClient;
    }

    public void setRechargeHttpClient(HttpClient httpClient) {
        this.rechargeHttpClient = httpClient;
    }

    public HttpClient getRechargeHttpClient() {
        return rechargeHttpClient;
    }

    public String getResellerId() {
        return resellerId;
    }

    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    public String getResellerPass() {
        return resellerPass;
    }

    public void setResellerPass(String resellerPass) {
        this.resellerPass = resellerPass;
    }

    public String getRechargeApiUrl() {
        return rechargeApiUrl;
    }

    public void setRechargeApiUrl(String rechargeApiUrl) {
        this.rechargeApiUrl = rechargeApiUrl;
    }

    public String getStatusCheckApiUrl() {
        return statusCheckApiUrl;
    }

    public void setStatusCheckApiUrl(String statusCheckApiUrl) {
        this.statusCheckApiUrl = statusCheckApiUrl;
    }

    public String getBalanceCheckApiUrl() {
        return balanceCheckApiUrl;
    }

    public void setBalanceCheckApiUrl(String balanceCheckApiUrl) {
        this.balanceCheckApiUrl = balanceCheckApiUrl;
    }

    public String getListOfOperatorsApiUrl() {
        return listOfOperatorsApiUrl;
    }

    public void setListOfOperatorsApiUrl(String listOfOperatorsApiUrl) {
        this.listOfOperatorsApiUrl = listOfOperatorsApiUrl;
    }
}
