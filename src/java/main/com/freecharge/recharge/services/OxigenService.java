package com.freecharge.recharge.services;

import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.AggregatorService;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.common.primitives.Ints;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: May 8, 2012 Time: 11:19:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class OxigenService extends AggregatorService {
    public static final String MERCHANTREFNO = "merchantrefno";
    public static final String TRANSACTION_STATUS_CHECK_RESPONSE_SUCCESSFUL = "0";
    private static Logger logger = LoggingFactory.getLogger(OxigenService.class);
    
    public static final String OXIGEN_BALANCE_KEY                  = "OXIGEN_BALANCE_KEY";
    public static final String OXIGEN_OPERATOR_CODE_TATA_SKY_RMN            = "TATASKYRMN";
    public static final String OXIGEN_OPERATOR_CODE_VIDEOCON_D2H_RMN            = "videocond2hrmn";
    public static final String DISH_TV_OXIGEN_OPERATOR_CODE_RMN    = "DishTVrmn";
    public static final String OXIGEN_BALANCE_LAST_UPDATE_TIME_KEY = "OXIGEN_BALANCE_LAST_UPDATE_TIME_KEY";
    public static final String MOBILE_PATTERN = "^[7-9]{1}[0-9]{9}";
    public static final Pattern masterPattern = Pattern.compile("([^=]+)=([^=]+)(?:,|$)");
    public static final Pattern pattern1 = Pattern.compile("([a-zA-Z0-9]*),([a-zA-Z0-9]*),([a-zA-Z0-9]*)(.*)");
    public static final Pattern pattern2 =  Pattern.compile("([a-zA-Z0-9]*)\\*([a-zA-Z0-9]*),([a-zA-Z0-9]*),([a-zA-Z0-9]*)(.*)");
    public static final Pattern pattern3 = Pattern.compile("([a-zA-Z0-9]*)\\*([a-zA-Z0-9]*),([a-zA-Z0-9]*)(.*)");
    public static final Pattern pattern4 = Pattern.compile("([a-zA-Z0-9]*\\*[a-zA-Z0-9]*\\*)([a-zA-Z0-9]*),([a-zA-Z0-9]*)(.*)");
	private static final String OXI_AUTH_KEY = "oxigen.auth.key";
        
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private AppConfigService appConfigService;
    
    private String oxibankrefno;
    private String oxirequesturl;
    private String oxistatus;
    private String statusCheckAPI;
    private String oxiusername;
    private String oxipassword;
    private String balanceCheckUrl;
    private HttpClient rechargeHttpClient;
    private HttpClient statusCheckHttpClient;
    private HttpConnectionManagerParams connectionManagerConfig;
    private HostConfiguration oxigenHostConfig;
    
    public String getStatusCheckAPI() {
        return statusCheckAPI;
    }
    
    public void setStatusCheckAPI(String statusCheckAPI) {
        this.statusCheckAPI = statusCheckAPI;
    }

    public HttpClient getStatusCheckHttpClient() {
        return statusCheckHttpClient;
    }
    
    public void setStatusCheckHttpClient(HttpClient statusCheckHttpClient) {
        this.statusCheckHttpClient = statusCheckHttpClient;
    }
    
    public void setRechargeHttpClient(HttpClient httpClient) {
        this.rechargeHttpClient = httpClient;
    }
    
    public HttpClient getRechargeHttpClient() {
        return rechargeHttpClient;
    }

    public void setBalanceCheckUrl(String balanceCheckUrl) {
		this.balanceCheckUrl = balanceCheckUrl;
	}

	public void setOxibankrefno(String oxibankrefno) {
        this.oxibankrefno = oxibankrefno;
    }

    public void setOxirequesturl(String oxirequesturl) {
        this.oxirequesturl = oxirequesturl;
    }

    public void setOxistatus(String oxistatus) {
        this.oxistatus = oxistatus;
    }

    public void setOxiusername(String oxiusername) {
        this.oxiusername = oxiusername;
    }

    public void setOxipassword(String oxipassword) {
        this.oxipassword = oxipassword;
    }
    
    @Override
    public String getBalanceCachekey(){
        return OxigenService.OXIGEN_BALANCE_KEY;
    }
    
    @Override
    public String getBalanceUpdateTimeCachekey(){
        return OxigenService.OXIGEN_BALANCE_LAST_UPDATE_TIME_KEY;
    }

    public AggregatorResponseDo doRecharge(RechargeDo rechargeDo, String orderNo, StringBuffer 
    		parametersPassed, List<InAggrOprCircleMap> inAggrOprCircleMaps) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        String merchantRefNo = "";
        String OxigenMobileRechargeType = RechargeConstants.OXIGEN_RECHARGE_TYPE;
        String OxigenDthRechargeType = RechargeConstants.OXIGEN_DTH_RECHARGE_TYPE;
        String OxigenDataCardRechargeTypePostpaid = RechargeConstants.OXIGEN_BILL_PAYMENT_TYPE;
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

        final SimpleHttpConnectionManager oxigenConnectionManager = new SimpleHttpConnectionManager(true);
        oxigenConnectionManager.setParams(connectionManagerConfig);
        
        HttpClient rechargeClient = new HttpClient(oxigenConnectionManager);
        rechargeClient.setHostConfiguration(oxigenHostConfig);

        logger.info("[IN MODERATOR] processing doRecharge for oxigen ", null);
        PostMethod method = new PostMethod(oxirequesturl);
        try {
            String oxiOperatorCode = null;
            String oxiCircleCode = null;
            String merchantKeyword = null;
            Boolean shouldTimeOut = false;
            Integer timeOutValue = -1;
            if (inAggrOprCircleMaps.size() > 0) {
                boolean isDataExist = false;
                for (InAggrOprCircleMap inAggrOprCircleMap : inAggrOprCircleMaps) {
                    if (inAggrOprCircleMap.getAggrName().equalsIgnoreCase(AggregatorInterface.OXIGEN_IN)) {
                        oxiOperatorCode = inAggrOprCircleMap.getAggrOperatorCode();
                        oxiCircleCode = inAggrOprCircleMap.getAggrCircleCode();
                        merchantKeyword = inAggrOprCircleMap.getRechargeplanType();
                        shouldTimeOut = inAggrOprCircleMap.getShouldTimeOut();
                        timeOutValue = inAggrOprCircleMap.getTimeOutInMillis();
                        isDataExist = true;
                    }
                }
                if (!isDataExist) {
                    return doActionAfterFail(rechargeDo, orderNo, parametersPassed);
                }
            } else {
                return doActionAfterFail(rechargeDo, orderNo, parametersPassed);
            }
            logger.info("[IN MODERATOR] processing doRecharge for oxigen  with merchantKeyword : " 
            + merchantKeyword + " and Circle : " + oxiCircleCode, null);
            Calendar cal = Calendar.getInstance();
            Date dt = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
            String requestdate = sdf.format(dt);
            
            if (rechargeDo.getProductType() != null && rechargeDo.getProductType()
            		.equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
                merchantRefNo = OxigenMobileRechargeType + "," + rechargeDo.getMobileNo() + "," + oxiOperatorCode;
            } else if (rechargeDo.getProductType() != null && rechargeDo.getProductType()
            		.equals(RechargeConstants.DTH_PRODUCT_TYPE)) {
                merchantRefNo = OxigenDthRechargeType + "*" + oxiOperatorCode + "," + rechargeDo.getMobileNo()
                		+ "," + oxiCircleCode;
            } else if (rechargeDo.getProductType() != null
                    && rechargeDo.getProductType().equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE)) {
                merchantRefNo = OxigenMobileRechargeType + "," + rechargeDo.getMobileNo() + "," + oxiOperatorCode;
            } else if (rechargeDo.getProductType() != null
                    && rechargeDo.getProductType().equals(RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE)) {
                merchantRefNo = OxigenDataCardRechargeTypePostpaid + "*" + oxiOperatorCode + ","
                        + rechargeDo.getMobileNo();
            }
            
            //special recharge request generation for Mobile
            if (merchantKeyword!=null && !merchantKeyword.isEmpty() && rechargeDo.getRechargePlanType()
            		.equalsIgnoreCase(RechargeConstants.RECHARGE_PLAN_TYPE_SPECIAL) && rechargeDo.getProductType()
            		.equals(RechargeConstants.MOBILE_PRODUCT_TYPE)) {
            	merchantRefNo = merchantKeyword + oxiCircleCode + "," + rechargeDo.getMobileNo();
                metricsClient.recordEvent("Recharge", "Oxigen.RechargeType." + rechargeDo.getCircleCode(), "Special");
           }
            
           //special recharge request generation for Data Card
            if (merchantKeyword!=null && !merchantKeyword.isEmpty() && rechargeDo.getProductType()
                    .equals(RechargeConstants.DATA_CARD_PRODUCT_TYPE) &&
                    (rechargeDo.getOperatorCode().equals(RechargeConstants.BSNL_DataCard) ||
                               rechargeDo.getOperatorCode().equals(RechargeConstants.MTNL_DataCard_MUMBAI) ||
                               rechargeDo.getOperatorCode().equals(RechargeConstants.MTNL_DataCard_DELHI) || 
                               rechargeDo.getOperatorCode().equals(RechargeConstants.Tata_Docomo_GSM_DataCard) ||
                            rechargeDo.getOperatorCode().equals(RechargeConstants.T24_DataCard))) {
                merchantRefNo = merchantKeyword + oxiCircleCode + "," + rechargeDo.getMobileNo();
                metricsClient.recordEvent("Recharge", "Oxigen.RechargeType." + rechargeDo.getCircleCode(), "Special");
           }
            
			// special recharge request generation for Tata Sky and Videocon D2H
			// using Registered Mobile No
			if (rechargeDo.getProductType() != null
					&& rechargeDo.getProductType().equals(RechargeConstants.DTH_PRODUCT_TYPE)) {
				if (rechargeDo.getMobileNo().matches(MOBILE_PATTERN)
						&& rechargeDo.getOperatorCode().equals(RechargeConstants.TATA_SKY_DTH)) {
					oxiOperatorCode = OXIGEN_OPERATOR_CODE_TATA_SKY_RMN;
					merchantRefNo = OxigenDthRechargeType + "*" + oxiOperatorCode + "," + rechargeDo.getMobileNo();
				} else if (rechargeDo.getMobileNo().matches(MOBILE_PATTERN)
						&& rechargeDo.getOperatorCode().equalsIgnoreCase(RechargeConstants.VIDEOCON_TV_OPERATOR_CODE)) {
					oxiOperatorCode = OXIGEN_OPERATOR_CODE_VIDEOCON_D2H_RMN;
					merchantRefNo = OxigenDthRechargeType + "*" + oxiOperatorCode + "," + rechargeDo.getMobileNo();
				} else if (rechargeDo.getMobileNo().matches(MOBILE_PATTERN)
						&& rechargeDo.getOperatorCode().trim().equalsIgnoreCase(RechargeConstants.DISH_TV_DTH)) {
					oxiOperatorCode = DISH_TV_OXIGEN_OPERATOR_CODE_RMN;
					merchantRefNo = OxigenDthRechargeType + "*" + oxiOperatorCode + "," + rechargeDo.getMobileNo();
				}
			}
            
            logger.info("special recharge keyword merchantRefNo " + merchantRefNo + rechargeDo.getRechargePlanType());

            method.setParameter("Transid", orderNo);
            method.setParameter(MERCHANTREFNO, merchantRefNo);
            method.setParameter("requestdate", requestdate);
            method.setParameter("status", oxistatus);
            method.setParameter("bankrefno", oxibankrefno);
            method.setParameter("amount", rechargeDo.getAmount().toString());           
            method.setRequestHeader("Authorization","BASIC "+getAuthKey());

            parametersPassed.append("Transid = " + orderNo + ", ");
            parametersPassed.append("merchantrefno = " + merchantRefNo + ", ");
            parametersPassed.append("requestdate = " + requestdate + ", ");
            parametersPassed.append("status = " + oxistatus + ", ");
            parametersPassed.append("bankrefno = " + oxibankrefno + ", ");
            parametersPassed.append("amount = " + rechargeDo.getAmount().toString());

            aggregatorResponseDo.setAggrName(RechargeConstants.AGGR_NAME_OXIGEN);
            aggregatorResponseDo.setRawRequest(parametersPassed.toString());
            aggregatorResponseDo.setRequestId(Long.parseLong(orderNo.substring(orderNo.indexOf("_") 
                    + 1, orderNo.length())));
            aggregatorResponseDo.setRequestTime(dt);

            logger.info("[IN MODERATOR] Executing recharge request from oxigen for operatorCode : " + oxiOperatorCode
            		+ " && mobile no :" + rechargeDo.getMobileNo(), null);
            // Execute and print response

            String response = null;
            String[] agrResult = null;

            int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), rechargeClient, method, shouldTimeOut, timeOutValue);

            if (statusCode == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
                //response = "123|Transaction Successful OxiTransID - 04215608282210201318 |";
            } else {
                logger.info("[IN MODERATOR] Received HTTP status code for oxigen : " + statusCode, null);
            }
            /* Getting Response Time */
            Calendar cal1 = Calendar.getInstance();
            Date dt1 = cal1.getTime();
            String[] outputResult = new String[0];
                outputResult = response.split("\\|");
                if (response != null && !response.equalsIgnoreCase("")) {
            }
            aggregatorResponseDo.setAggrOprRefernceNo("");
            aggregatorResponseDo.setAggrResponseMessage(response);
            aggregatorResponseDo.setRawResponse(response);
            aggregatorResponseDo.setResponseTime(dt1);

            if (outputResult.length > 0) {
                aggregatorResponseDo.setAggrResponseCode(outputResult[0]);
                if (outputResult[0].trim().equals(RechargeConstants.OXIGEN_SUCCESS_RESPONSE_CODE)) {
                    aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
                } else {
                    aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                }

                //because sometime we are getting wrong response format String from oxigen aggregator

                if (outputResult[1].contains("-")) {
                    agrResult = outputResult[1].split("-");
                aggregatorResponseDo.setAggrReferenceNo(agrResult[1].trim());
                } else {
                    int index = response.indexOf("-");
                    aggregatorResponseDo.setAggrReferenceNo(response.substring(index + 2));
            }
				if (outputResult.length > 2 && outputResult[2].contains("-")) {
					agrResult = outputResult[2].split("-");
					aggregatorResponseDo.setAggrOprRefernceNo(agrResult[1].trim());
				}

            }
            logger.info("Oxigen Recharge request Url "+oxirequesturl+" with param "+parametersPassed.toString()
            		+" with Recharge Response from Oxigen " + response);
            
            if (aggregatorResponseDo != null && aggregatorResponseDo.getAggrResponseCode()!=null) {
                metricsClient.recordEvent("Recharge", "Oxigen.ResponseCode", aggregatorResponseDo.getAggrResponseCode());
                metricsClient.recordEvent("Recharge", "Oxigen.ResponseCode." + oxiOperatorCode, aggregatorResponseDo.getAggrResponseCode());
            }
            
        } finally {
            // release connection
            logger.info("[IN MODERATOR] Releasing connection from IN in doRecharge(finally) for oxigen ", null);
            method.releaseConnection();
        }
        return aggregatorResponseDo;
    }

    public String getAuthKey() {
		return fcProperties.getProperty(OXI_AUTH_KEY);
	}

	private AggregatorResponseDo doActionAfterFail(RechargeDo rechargeDo, String orderNo,
    		StringBuffer parametersPassed) {
        logger.info("Recharge process fail for oxigen: No Operator code found in DB(oxigen_operator_map)" +
        		" for operator =  " + rechargeDo.getOperatorCode(), null);
        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

        /* Getting Request Time */
        Calendar cal = Calendar.getInstance();
        Date dt = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
        String requestdate = sdf.format(dt);

        parametersPassed.append("Transid = " + orderNo + ", ");
        parametersPassed.append("merchantrefno = null, ");
        parametersPassed.append("requestdate = " + requestdate + ", ");
        parametersPassed.append("status = " + oxistatus + ", ");
        parametersPassed.append("bankrefno = " + oxibankrefno + ", ");
        parametersPassed.append("amount = " + rechargeDo.getAmount().toString());

        aggregatorResponseDo.setAggrName(AggregatorInterface.OXIGEN_IN);
        aggregatorResponseDo.setRawRequest(parametersPassed.toString());
        aggregatorResponseDo.setRequestId(Long.parseLong(orderNo.substring(orderNo.indexOf("_")
        		+ 1, orderNo.length())));
        aggregatorResponseDo.setRequestTime(dt);

        aggregatorResponseDo.setAggrOprRefernceNo("");
        aggregatorResponseDo.setAggrResponseMessage("Entry missing for operator circle mapping in the database");
        aggregatorResponseDo.setRawResponse("");
        aggregatorResponseDo.setResponseTime(dt);
        aggregatorResponseDo.setAggrResponseCode("1012");
        aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
        return aggregatorResponseDo;
    }

    public AggregatorResponseDo getTransactionStatus(RechargeDo rechargeDo, InResponse 
    		inResponseFromRequest) throws HttpException, ConnectException,
            ConnectTimeoutException, SocketException, SocketTimeoutException, IOException {
        String response = "";

        String merchantRefNo = "Checkstatus," + rechargeDo.getMobileNo();

        PostMethod method;
        method = new PostMethod(statusCheckAPI);

        AggregatorResponseDo aggregatorResponseDo = new AggregatorResponseDo();

        try {
            String aggregatorOrderId = fcProperties.getInTransIdPrefix() + inResponseFromRequest.getRetryNumber() +"_" + 
            rechargeDo.getInRequestId().toString();
            String aggrReferenceNo="";
            String aggrOpReferenceNo="";

            method.setParameter("Transid", aggregatorOrderId);
            method.setParameter(MERCHANTREFNO, merchantRefNo);
            method.setRequestHeader("Authorization", "BASIC "+getAuthKey());
            StringBuffer parametersPassed = new StringBuffer();
            parametersPassed.append("Transid = " + aggregatorOrderId + ", ");
            parametersPassed.append("merchantrefno = " + merchantRefNo);
            logger.info("Oxigen Status check request Url " + statusCheckAPI + " with param "
                    + parametersPassed.toString());
            logger.info("About to call Oxi status check for Order ID: " + rechargeDo.getAffiliateTransId()); 
            int statusCode = executeWithTimeout(rechargeDo.getAffiliateTransId(), this.getStatusCheckHttpClient(), method, true, 60000);

            if (statusCode == HttpStatus.SC_OK) {
                //Should be of the format <status code>|<status message>
                //0 - successful
                //1 - transaction not found
                response = method.getResponseBodyAsString();

                String[] outputResult = new String[0];
                if (response != null && !response.equalsIgnoreCase("")) {
                    outputResult = response.split("\\|");
                }

                String status = outputResult[0].trim();
                logger.info("Oxigen status check api response string for order id " + 
                rechargeDo.getAffiliateTransId() + "  " + response) ;
                if(outputResult.length>=3 && outputResult[2].trim().split("-").length==2)
                aggrReferenceNo = outputResult[2].trim().split("-")[1].trim();

                if(outputResult.length>=4 && outputResult[3].trim().split("-").length==2){
                    aggrOpReferenceNo = outputResult[3].trim().split("-")[1].trim();
                }

                if (TRANSACTION_STATUS_CHECK_RESPONSE_SUCCESSFUL.equals(status)) {
                    aggregatorResponseDo.setTransactionStatus(AggregatorInterface.SUCCESS_STATUS);
                    aggregatorResponseDo.setAggrResponseCode(status);
                    aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.IN_TRANSACTION_SUCCESSFUL_MESSAGE);
                    aggregatorResponseDo.setRawResponse(response);
                    aggregatorResponseDo.setAggrReferenceNo(aggrReferenceNo);
                    aggregatorResponseDo.setAggrOprRefernceNo(aggrOpReferenceNo);
                }  else {
                    aggregatorResponseDo.setTransactionStatus(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                    aggregatorResponseDo.setAggrResponseCode(status);
                    aggregatorResponseDo.setAggrResponseMessage(RechargeConstants.TRANSACTION_STATUS_UNSUCCESSFUL);
                    aggregatorResponseDo.setRawResponse(response);
                }
            } else {
                logger.info("[IN MODERATOR] Received HTTP status code in getTransactionStatus" +
                		" for oxigen : " + statusCode, null);
                aggregatorResponseDo.setAggregatorErrorWhileQuerying(true);
            }
            logger.info("Oxigen Status check request Url "+statusCheckAPI+" with param "+parametersPassed.toString()
                    +" with Recharge Response from Oxigen " + response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally{
			method.releaseConnection();
		}
        logger.info("[IN MODERATOR] Get Transaction Status from oxigen : " + response, null);

        return aggregatorResponseDo;
    }
    
    @Override
    public Double getWalletBalance() throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

        PostMethod getBalanceRequest = new PostMethod("/transservice.aspx");

        getBalanceRequest.setParameter("Transid", String.valueOf(System.currentTimeMillis()));
        getBalanceRequest.setParameter(MERCHANTREFNO, "getbalance");
        getBalanceRequest.setParameter("requestdate", sdf.format(new Date()));
        getBalanceRequest.setParameter("status", "0");
        getBalanceRequest.setParameter("bankrefno", "test01");
        getBalanceRequest.setParameter("amount", "0");
        getBalanceRequest.setRequestHeader("Authorization", "BASIC QWNjZWx5c3Q6QWNjZWx5c3RAb3hpMTIz");

        int statusCode = this.getRechargeHttpClient().executeMethod(getBalanceRequest);

        String getBalanceResponse = getBalanceRequest.getResponseBodyAsString();
        logger.info("Oxigen Balance Response: " + getBalanceResponse);        

        /*
         * 0|Dear Oxigen Retailer, your merchant account balance is Rs 2348199.25 | 
         */
        
        if (statusCode == HttpStatus.SC_OK) {
            String responseMessage = getBalanceResponse.split("\\|")[1];
            StringTokenizer st = new StringTokenizer(responseMessage);
            
            if (responseMessage.contains("your merchant account balance is")) {
                while (st.hasMoreTokens()) {
                    String currToken = st.nextToken().trim();

                    if (StringUtils.contains(currToken, '.')) {
                        final String firstPart = currToken.split("\\.")[0];
                        if (Ints.tryParse(firstPart) != null) {
                            return new Double(firstPart);
                        }
                    }
                }
            }
        }
        
        throw new IOException("Did not valid balance response from Oxigen");
    }
    
    @Override
    protected Double getBalanceWarnThreshold() {
        return fcProperties.getEuronetBalanceWarnThreshold();
    }

    @Override
    protected Double getBalanceCriticalThreshold() {
        return fcProperties.getEuronetBalanceCriticalThreshold();
    }

    public HttpConnectionManagerParams getConnectionManagerConfig() {
        return connectionManagerConfig;
    }

    public void setConnectionManagerConfig(HttpConnectionManagerParams connectionManagerConfig) {
        this.connectionManagerConfig = connectionManagerConfig;
    }

    public HostConfiguration getOxigenHostConfig() {
        return oxigenHostConfig;
    }

    public void setOxigenHostConfig(HostConfiguration oxigenHostConfig) {
        this.oxigenHostConfig = oxigenHostConfig;
    }
}
