package com.freecharge.recharge.services;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.jdbc.EuronetCallbackRequest;
import com.freecharge.app.domain.entity.jdbc.EuronetCallbackResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.InTransactions;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.util.RechargeConstants;

@Service
public class EuronetCallbackService {
	
	  private final Logger             logger                                  = LoggingFactory.getLogger(getClass());

	    @Autowired
	    private InService                inService;

	    @Autowired
	    private MetricsClient            metricsClient;

	    @Autowired
	    private RechargeSchedulerService rechargeSchedulerService;
	    
	    @Autowired
	    private AppConfigService         appConfig;   

	    private static final String      EURONET_VALID_SECOND_RESPONSE           = "0";
	    private static final String      EURONET_SUCCESS_TO_SUCCESS              = "1";
	    private static final String      EURONET_FAILURE_TO_FAILURE              = "2";
	    private static final String      EURONET_FAILURE_TO_SUCCESS              = "3";
	    private static final String      EURONET_INTERNAL_EXCEPTION              = "4";
	    private static final String      EURONET_TRANSACTION_NOT_FOUND_OR_NO_ACK = "5";
	    private static final String      EURONET_DUPLICATE_RESPONSE              = "6";



	public EuronetCallbackResponse handleStatusCheck(EuronetCallbackRequest euronetCallbackRequest) {
		EuronetCallbackResponse euronetCallbackResponse = new EuronetCallbackResponse();
		Long requestId = null;
		Integer retryNumber = null;
		try {
            long startTime = System.currentTimeMillis();
            logger.info("Callback request from euronet : " + euronetCallbackRequest.toString());
            if (!appConfig.isRechargeCallbackEnabled()) {
                logger.error("Recharge Callback is not enabled.");
                euronetCallbackResponse.setResult(EURONET_TRANSACTION_NOT_FOUND_OR_NO_ACK);
                return euronetCallbackResponse;
            }
            validateRequest(euronetCallbackRequest);
            requestId = Long.parseLong(euronetCallbackRequest.getMerchantrefno().split("_")[1]);
            retryNumber = Integer.parseInt(euronetCallbackRequest.getMerchantrefno().split("_")[0]);
            InTransactionData inTransaction = inService.getInTransactionData(requestId);
            String operator = inTransaction.getInRequest().getOperator();
            String circle = inTransaction.getInRequest().getCircle();
            Timestamp requestTime = getRequestTimeForLastIntransaction(inTransaction);

            if (inTransaction == null || inTransaction.getInRequest() == null || inTransaction.getInResponse() == null
                    || !inTransaction.getInResponse().getAggrName().equals(RechargeConstants.AGGR_NAME_EURONET)) {
                logger.error("No recharge transaction data found for request id [" + requestId
                        + "]. Not updating status.");
                euronetCallbackResponse.setResult(EURONET_TRANSACTION_NOT_FOUND_OR_NO_ACK);
                return euronetCallbackResponse;
            }
            
            if (!retryNumber.equals(inTransaction.getInResponse().getRetryNumber())) {
            	logger.error("Callback is not for the latest request. Callback AgRequestId "
            			+ euronetCallbackRequest.getMerchantrefno() + ". Latest AgRequestId "
            			+ inTransaction.getInResponse().getRetryNumber() + "_"
            			+ inTransaction.getInResponse().getRequestId());
                euronetCallbackResponse.setResult(EURONET_TRANSACTION_NOT_FOUND_OR_NO_ACK);
                return euronetCallbackResponse;
            }

            String currentRechargeStatus = RechargeUtil.getRechargeStatusFromAggCode(euronetCallbackRequest
                    .getResponsecode());
            String initialRechargeStatus = RechargeUtil.getRechargeStatusFromInResponseCode(inTransaction
                    .getInResponse().getInRespCode());

			if (FCUtil.isEmpty(initialRechargeStatus)
					|| initialRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_UNDER_PROCESS)) {
				/*
				 * Initial transaction is UP and there is a state change now.
				 * Update final response.
				 */
				if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
					// TODO: Process requests which are from UP => Success.
					logger.info("UP to Success captured");
					recordLatency(requestTime,operator,circle);					
					metricsClient.recordEvent("Recharge", "Callback.euronet", "underprocessToSuccess");
				} else if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
					logger.info("UP to Failure captured");
					recordLatency(requestTime,operator,circle);
					metricsClient.recordEvent("Recharge", "Callback.euronet", "underprocessToFailure");
				}
				boolean isSuccessfulUpdate = rechargeSchedulerService.updateStatusForEuronetCallback(
						inTransaction.getInRequest(), inTransaction.getInResponse(), euronetCallbackRequest);
				if (isSuccessfulUpdate) {
					logMetrics(inTransaction, requestId);
					euronetCallbackResponse.setResult(EURONET_VALID_SECOND_RESPONSE);
				} else {
					euronetCallbackResponse.setResult(EURONET_INTERNAL_EXCEPTION);
				}
			} else if (initialRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                /*
                 * Initial transaction is a success.
                 */
                if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                    logger.info("1st transaction response was successful & 2nd response is also successful for merchant request id : "
                            + euronetCallbackRequest.getMerchantrefno());
                    metricsClient.recordEvent("Recharge", "Callback.euronet", "successToSuccess");
                    euronetCallbackResponse.setResult(EURONET_SUCCESS_TO_SUCCESS);
                } else if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
                    /*
                     * TODO: Success ==> Failure. Currently we are just pumping
                     * a metric.
                     */
                    logger.info("1st transaction response was successful & 2nd response falied for merchant request id : "
                            + euronetCallbackRequest.getMerchantrefno());
                    metricsClient.recordEvent("Recharge", "Callback.euronet", "successToFailure");
                    euronetCallbackResponse.setResult(EURONET_VALID_SECOND_RESPONSE);
                }
            } else if (initialRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
                /*
                 * Initial transaction is a failure.
                 */
                if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_FAILED)) {
                    logger.info("1st transaction response was failed & 2nd response is also failed for merchant request id : "
                            + euronetCallbackRequest.getMerchantrefno());
                    metricsClient.recordEvent("Recharge", "Callback.euronet", "failureToFailure");
                    euronetCallbackResponse.setResult(EURONET_FAILURE_TO_FAILURE);
                } else if (currentRechargeStatus.equals(RechargeConstants.TRANSACTION_STATUS_SUCCESS)) {
                    /*
                     * TODO: Failure ==> Success. Currently we are just pumping
                     * a metric.
                     */
                    logger.info("1st transaction response was failed & 2nd response is success : "
                            + euronetCallbackRequest.getMerchantrefno());
                    metricsClient.recordEvent("Recharge", "Callback.euronet", "failureToSuccess");
                    euronetCallbackResponse.setResult(EURONET_FAILURE_TO_SUCCESS);
                }
            }
            metricsClient.recordLatency("Recharge", "Callback.Euronet", System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            euronetCallbackResponse.setResult(EURONET_INTERNAL_EXCEPTION);
            logger.error("Error processing callback for request id: " + euronetCallbackRequest.getMerchantrefno(), e);
        }
        logger.info("FC acknowledgement for requestId:"+requestId+" is "+euronetCallbackResponse);
        return euronetCallbackResponse;
	}

	private void recordLatency(Timestamp requestTime, String operator, String circle) {
		if (requestTime != null) {
			metricsClient.recordLatency("Recharge", "Callback.euronet" + operator + "." + circle,
					System.currentTimeMillis() - requestTime.getTime());
		}
	}

	private Timestamp getRequestTimeForLastIntransaction(InTransactionData inTransaction) {
		List<InTransactions> txns = inTransaction.getTransactions();
		int length = txns.size();
		if (length > 0) {
			return txns.get(length - 1).getRequestTime();
		}
		logger.info("No InTransaction Data found");
		return null;
	}

	private void logMetrics(InTransactionData inTransaction, Long requestId) {
		logger.info("Updated DB for requestId:"+requestId);
        metricsClient.recordEvent("Recharge", "Callback.euronet", "DBUpdated");
        metricsClient.recordCount("Recharge", "Callback.euronet.DBUpdate", 1);
		
	}

	private void validateRequest(EuronetCallbackRequest euronetCallbackResponse) throws InvalidParameterException {
        if (FCUtil.isEmpty(euronetCallbackResponse.getEnrefno())) {
            throw new InvalidParameterException("enrefno", euronetCallbackResponse.getEnrefno(),
                    "Euronet Transaction Id cannot be null", ValidationErrorCode.REQUIRED_RECHARGE_CALLBACK_PARAMETER);
        } else if (FCUtil.isEmpty(euronetCallbackResponse.getMerchantrefno())) {
            throw new InvalidParameterException("merchantrefno", euronetCallbackResponse.getMerchantrefno(),
                    "Merchant Transaction Id cannot be null", ValidationErrorCode.REQUIRED_RECHARGE_CALLBACK_PARAMETER);
        } else if (!euronetCallbackResponse.getMerchantrefno().contains("_")) {
            throw new InvalidParameterException("merchantrefno", euronetCallbackResponse.getMerchantrefno(),
                    "Invalid Merchant Transaction Id", ValidationErrorCode.INVALID_RECHARGE_CALLBACK_PARAMETER);
        }
    }
	
}
