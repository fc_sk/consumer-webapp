package com.freecharge.recharge.fulfillment.db;

import java.io.Serializable;
import java.sql.Timestamp;

import com.freecharge.recharge.fulfillment.type.ScheduleCreator;
import com.freecharge.recharge.fulfillment.type.ScheduleReason;
import com.freecharge.recharge.fulfillment.type.ScheduleStatus;
import com.freecharge.recharge.fulfillment.type.ScheduleTriggerEvent;

public class FulfillmentScheduleDO implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer scheduleId;
    private String orderId;
    private ScheduleTriggerEvent triggerEvent;
    private ScheduleCreator createdBy;
    private ScheduleReason scheduleReason;
    private ScheduleStatus status; 
    private Timestamp expiryTime;
    private Timestamp createdTime;
    
    public Integer getScheduleId() {
        return scheduleId;
    }
    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }
    
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public ScheduleTriggerEvent getTriggerEvent() {
        return triggerEvent;
    }
    public void setTriggerEvent(ScheduleTriggerEvent triggerEvent) {
        this.triggerEvent = triggerEvent;
    }
    
    public ScheduleCreator getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(ScheduleCreator createdBy) {
        this.createdBy = createdBy;
    }
    
    public ScheduleReason getScheduleReason() {
        return scheduleReason;
    }
    public void setScheduleReason(ScheduleReason scheduleReason) {
        this.scheduleReason = scheduleReason;
    }
    
    public ScheduleStatus getStatus() {
        return status;
    }
    public void setStatus(ScheduleStatus status) {
        this.status = status;
    }
    
    public Timestamp getExpiryTime() {
        return expiryTime;
    }
    public void setExpiryTime(Timestamp expiryTime) {
        this.expiryTime = expiryTime;
    }
    
    public Timestamp getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }
}
