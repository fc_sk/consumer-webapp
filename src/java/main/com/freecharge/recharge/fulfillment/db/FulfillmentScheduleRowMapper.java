package com.freecharge.recharge.fulfillment.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.recharge.fulfillment.type.ScheduleCreator;
import com.freecharge.recharge.fulfillment.type.ScheduleReason;
import com.freecharge.recharge.fulfillment.type.ScheduleStatus;
import com.freecharge.recharge.fulfillment.type.ScheduleTriggerEvent;

public class FulfillmentScheduleRowMapper implements RowMapper<FulfillmentScheduleDO> {
    
    @Override
    public FulfillmentScheduleDO mapRow(ResultSet rs, int rowNum) throws SQLException {
        FulfillmentScheduleDO toReturn = new FulfillmentScheduleDO();
        
        toReturn.setScheduleId(rs.getInt("id"));
        toReturn.setOrderId(rs.getString("order_id"));
        toReturn.setStatus(ScheduleStatus.fromCode(rs.getInt("status")));
        toReturn.setTriggerEvent(ScheduleTriggerEvent.fromCode(rs.getInt("trigger_event")));
        toReturn.setCreatedBy(ScheduleCreator.fromCode(rs.getInt("created_by")));
        toReturn.setScheduleReason(ScheduleReason.fromCode(rs.getInt("schedule_reason")));
        toReturn.setCreatedTime(rs.getTimestamp("created_on"));
        toReturn.setExpiryTime(rs.getTimestamp("trigger_expiry"));
        
        return toReturn;
    }

}
