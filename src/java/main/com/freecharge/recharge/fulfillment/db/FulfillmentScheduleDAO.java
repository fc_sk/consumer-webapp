package com.freecharge.recharge.fulfillment.db;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class FulfillmentScheduleDAO {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<FulfillmentScheduleDO> getByOrderId(String orderId) {
        String sql = "select * from recharge_fulfillment_schedule where order_id = :orderId";
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", orderId);

        List<FulfillmentScheduleDO> returnList = jdbcTemplate.query(sql, params, new FulfillmentScheduleRowMapper());
        
        return returnList;
    }
    
    public List<FulfillmentScheduleDO> getScheduledStatusByOrderId(String orderId) {
        String sql = "select * from recharge_fulfillment_schedule where order_id = :orderId and status = 0";
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", orderId);

        List<FulfillmentScheduleDO> returnList = jdbcTemplate.query(sql, params, new FulfillmentScheduleRowMapper());
        
        return returnList;
    }
    
    public List<FulfillmentScheduleDO> getRetryScheduleBeyondTriggerExpiry(DateTime triggerExpiryThreshold, DateTime createdFrom) {
        String sql = "select * from recharge_fulfillment_schedule where " +
        		"trigger_expiry < :triggerExpiryThreshold " +
        		"AND " +
        		"created_on > :createdFrom " +
        		"AND " +
        		"status = 0 ";
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("triggerExpiryThreshold", new Timestamp(triggerExpiryThreshold.getMillis()));
        params.put("createdFrom", new Timestamp(createdFrom.getMillis()));

        List<FulfillmentScheduleDO> returnList = jdbcTemplate.query(sql, params, new FulfillmentScheduleRowMapper());
        
        return returnList;
    }
    
    public List<FulfillmentScheduleDO> getRetryScheduleByOrderId(String orderId) {
        String sql = "select * from recharge_fulfillment_schedule where order_id = :orderId and schedule_reason = 2";
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", orderId);

        List<FulfillmentScheduleDO> returnList = jdbcTemplate.query(sql, params, new FulfillmentScheduleRowMapper());
        
        return returnList;
    }
    
    public List<FulfillmentScheduleDO> getSchedulesReadyForFulfillment(DateTime createdFrom, DateTime createdTill) {
        String sql = "select * from recharge_fulfillment_schedule where " +
        		"created_on > :createdFrom and created_on < :createdTill AND " +
        		"status = 0 " +
        		"AND " +
        		"((schedule_reason in (0, 2, 3) and trigger_expiry <= current_timestamp)" +
        		" OR " +
        		"(schedule_reason = 1))";
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("createdFrom", new Timestamp(createdFrom.getMillis()));
        params.put("createdTill", new Timestamp(createdTill.getMillis()));
        
        List<FulfillmentScheduleDO> returnList = jdbcTemplate.query(sql, params, new FulfillmentScheduleRowMapper());
        
        if (returnList == null) {
            return new ArrayList<>();
        }
        
        return returnList;
    }
    
    public int create(FulfillmentScheduleDO scheduleDo) {
        String insertSql = "insert into recharge_fulfillment_schedule( " +
        		"order_id, trigger_event, trigger_expiry, " +
        		"created_by, schedule_reason, status) " +
        		"values ( " +
        		":orderId, :triggerEvent, :triggerExpiry, " +
        		":createdBy, :scheduleReason, :status" +
        		")";
        
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("orderId", scheduleDo.getOrderId());
        insertParams.put("triggerEvent", scheduleDo.getTriggerEvent().getCode());
        insertParams.put("triggerExpiry", scheduleDo.getExpiryTime());
        insertParams.put("createdBy", scheduleDo.getCreatedBy().getCode());
        insertParams.put("scheduleReason", scheduleDo.getScheduleReason().getCode());
        insertParams.put("status", scheduleDo.getStatus().getCode());
        
        int updateCount = this.jdbcTemplate.update(insertSql, insertParams);
        
        return updateCount;
    }
    
    public int extendExpiryForCompletedBlockExpiry(FulfillmentScheduleDO scheduleDo, Timestamp newExpiry) {
        String updateSql = "update recharge_fulfillment_schedule " +
                "set status = 0, trigger_expiry = :triggerExpiry where " +
                "id = :scheduleId and " +
                "status = 2";
        
        Map<String, Object> updateParams = new HashMap<>();
        updateParams.put("scheduleId", scheduleDo.getScheduleId());
        updateParams.put("triggerExpiry", newExpiry);
        
        int updateCount = this.jdbcTemplate.update(updateSql, updateParams);
        
        return updateCount;
    }
    
    public int extendExpiryForRetrySchedule(FulfillmentScheduleDO scheduleDo, Timestamp newExpiry) {
        String updateSql = "update recharge_fulfillment_schedule " +
                "set trigger_expiry = :triggerExpiry,  status = 0 where " +
                "id = :scheduleId and " +
                "status = 2 and " +
                "schedule_reason = 2";
        
        Map<String, Object> updateParams = new HashMap<>();
        updateParams.put("scheduleId", scheduleDo.getScheduleId());
        updateParams.put("triggerExpiry", newExpiry);
        
        int updateCount = this.jdbcTemplate.update(updateSql, updateParams);
        
        return updateCount;
    }

    public int markComplete(FulfillmentScheduleDO scheduleDo) {
        String updateSql = "update recharge_fulfillment_schedule " +
        		"set status = 2 where " +
        		"id = :scheduleId and " +
        		"status = 0";
        
        Map<String, Object> updateParams = new HashMap<>();
        updateParams.put("scheduleId", scheduleDo.getScheduleId());
        
        int updateCount = this.jdbcTemplate.update(updateSql, updateParams);
        
        return updateCount;
    }
    
    public void markCancelledByAdmin(FulfillmentScheduleDO scheduleDo) {
        String updateSql = "update recharge_fulfillment_schedule " +
                "set status = 4 where " +
                "id = :scheduleId and " +
                "status = 0";
        
        Map<String, Object> updateParams = new HashMap<>();
        updateParams.put("scheduleId", scheduleDo.getScheduleId());
        
        int updateCount = this.jdbcTemplate.update(updateSql, updateParams);
        
        if (updateCount != 1) {
            throw new IllegalStateException("Failed to update recharge_fulfillment_schedule for orderID: " + scheduleDo.getOrderId());
        }
    }
}
