package com.freecharge.recharge.fulfillment.db;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.hlr.service.HLRResponse;
import com.freecharge.hlr.service.HLRService.HLR_SERVICE;
import com.freecharge.hlr.service.HLRService.STATUS;

@Component
public class HLRResponseDAO {

	private NamedParameterJdbcTemplate jt;
	Logger logger = Logger.getLogger(HLRResponseDAO.class);
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
    }
	
	private static final String INSERT_TXN_SUCCESS = "INSERT INTO"
            + " hlr_response(request_id,order_id,serviceNumber, hlr_service,input_operator_id,input_circle_id,hlr_operator,hlr_circle,status,request_time)"
            + " VALUES (:request_id, :order_id, :serviceNumber, :hlr_service,:input_operator,:input_circle,:hlr_operator,:hlr_circle,:status,:request_time)";
	private static final String INSERT_TXN_FAILURE = "INSERT INTO"
            + " hlr_response(request_id,order_id,serviceNumber, hlr_service,input_operator_id,input_circle_id,status,reason,request_time)"
            + " VALUES (:request_id, :order_id, :serviceNumber, :hlr_service,:input_operator,:input_circle,:status,:reason,:request_time)";
	private static final String INSERT_TXN_TIMEOUT = "INSERT INTO"
            + " hlr_response(order_id,serviceNumber, hlr_service,input_operator_id,input_circle_id,status,request_time)"
            + " VALUES (:order_id, :serviceNumber, :hlr_service,:input_operator,:input_circle,:status,:request_time)";


	public void logHLRSuccess(HLRResponse hlrResponse,String requestId, String serviceNumber, String orderId, Integer operatorId,
			Integer circleId, STATUS status, HLR_SERVICE hlrService) {
		Map<String, Object> insertParams = new HashMap<String, Object>();
		insertParams.put("request_id", requestId);
		insertParams.put("order_id", orderId);
		insertParams.put("serviceNumber", serviceNumber);
		insertParams.put("hlr_service", hlrService.getHlrId());
		insertParams.put("input_operator", operatorId);
		insertParams.put("input_circle", circleId);
		insertParams.put("hlr_operator", hlrResponse.getOperator());
		insertParams.put("hlr_circle", hlrResponse.getCircle());
		insertParams.put("status", status.getStatusId());
		insertParams.put("request_time", new Timestamp(System.currentTimeMillis()));
		jt.update(INSERT_TXN_SUCCESS, insertParams);
		
	}

	public void logHLRFailure(String requestId,String reason, String serviceNumber, String orderId, Integer operatorId,
			Integer circleId, STATUS status, HLR_SERVICE hlrService) {
		Map<String, Object> insertParams = new HashMap<String, Object>();
		insertParams.put("request_id", requestId);
		insertParams.put("order_id", orderId);
		insertParams.put("serviceNumber", serviceNumber);
		insertParams.put("hlr_service", hlrService.getHlrId());
		insertParams.put("input_operator", operatorId);
		insertParams.put("input_circle", circleId);		
		insertParams.put("status", status.getStatusId());
		insertParams.put("reason", reason);
		insertParams.put("request_time", new Timestamp(System.currentTimeMillis()));
		jt.update(INSERT_TXN_FAILURE, insertParams);
		
	}

	public void logHLRTimeout(String orderId, String serviceNumber, Integer operatorId, Integer circleId,
			STATUS status, HLR_SERVICE hlrService) {
		Map<String, Object> insertParams = new HashMap<String, Object>();		
		insertParams.put("order_id", orderId);
		insertParams.put("serviceNumber", serviceNumber);
		insertParams.put("hlr_service", hlrService.getHlrId());
		insertParams.put("input_operator", operatorId);
		insertParams.put("input_circle", circleId);		
		insertParams.put("status", status.getStatusId());		
		insertParams.put("request_time", new Timestamp(System.currentTimeMillis()));
		jt.update(INSERT_TXN_TIMEOUT, insertParams);
		
		
	}

}
