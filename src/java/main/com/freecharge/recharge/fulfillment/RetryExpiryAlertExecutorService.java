package com.freecharge.recharge.fulfillment;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDAO;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;

public class RetryExpiryAlertExecutorService {
	private final String SERVICE_NAME = "RetryExpiryAlertExecutorService";
	private static Logger logger = LoggingFactory.getLogger(RetryExpiryAlertExecutorService.class);
	
	@Autowired
    private FulfillmentScheduleDAO scheduleDAO;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
    private FCProperties fcProperties;
	
	public void executeRetryAlerting(DateTime thresholdTime, DateTime creationThreshold) {
		logger.info("Generating alret for retry requests pending before [" + thresholdTime +
				"] and created after [" +
				creationThreshold + "]");
		
		List<FulfillmentScheduleDO> triggerExpiredList = scheduleDAO.getRetryScheduleBeyondTriggerExpiry(thresholdTime, creationThreshold);
		
		// no expired retry request found
		if(triggerExpiredList == null || triggerExpiredList.isEmpty()) {
			return;
		}
		
		logger.info("Processing readyList size: " + triggerExpiredList.size());
		
		sendAlertNotification(triggerExpiredList);
	}	
	
	private void sendAlertNotification(List<FulfillmentScheduleDO> triggerExpiredList) {
		String emailIdFrom = "nobody@freecharge.com";
		String emailSubject = "Alert: recharge retry time expired";
		String velocityTemplate = "templates/mail/retryExpiryAlert.vm";
		String emailToAddresses = fcProperties.getProperty(FCConstants.RETRY_ALERT_EMAIL_TO);
		Map<String, Object> paramsMap = new HashMap<>();
		paramsMap.put("message",
				"Pending retry requests with expired retry trigger time"
				);
		DataSource dataSource = prepareAttahchmentData(triggerExpiredList);
		emailService.sendEmailNotificationWithAttachment(emailToAddresses,
				emailIdFrom, emailSubject, velocityTemplate,
				"retry_schedule_expiry_data.csv", dataSource,
				paramsMap, true);
	}
	
	
	private DataSource prepareAttahchmentData(List<FulfillmentScheduleDO> triggerExpiredList) {
		StringBuilder data = new StringBuilder();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		data.append("Order ID" + "," + "Trigger Expiry Time" + "\n");
		for (FulfillmentScheduleDO triggerExpired: triggerExpiredList) {
			data.append(triggerExpired.getOrderId() + "," +
					dateFormat.format(triggerExpired.getExpiryTime())+"\n");
		}
		byte[] bytes = data.toString().getBytes();
		return new ByteArrayDataSource(bytes, "text/plain");
	}
}
