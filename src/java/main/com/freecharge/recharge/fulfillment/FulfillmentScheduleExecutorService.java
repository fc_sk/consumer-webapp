package com.freecharge.recharge.fulfillment;

import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.admin.service.RefundService;
import com.freecharge.app.domain.dao.RewardRechargeDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.CircleOperatorMapping;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.InService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeRequestWebDo;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDAO;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;
import com.freecharge.recharge.fulfillment.type.ScheduleReason;
import com.freecharge.recharge.fulfillment.type.ScheduleStatus;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.services.OperatorDownService;
import com.freecharge.recharge.services.UserTransactionHistoryService;

public class FulfillmentScheduleExecutorService {
    private static Logger logger = LoggingFactory.getLogger(FulfillmentScheduleExecutorService.class);

    @Autowired
    private FulfillmentScheduleDAO scheduleDAO;
    
    @Autowired
    private RewardRechargeDAO rewardRechargeDAO;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private OperatorAlertService alertService;
    
    @Autowired
    private UserTransactionHistoryService historyService;
    
    @Autowired
    private RefundService refundService;
    
    @Autowired
    private InService inService;
    
    @Autowired
    private OrderIdWriteDAO orderIdDAO;
    
    @Autowired
    private BillPaymentService billPaymentService;

    @Autowired
    private OperatorDownService operatorDownService;
    
    private final boolean SUCCESS = true;
    private final boolean FAILURE = false;
    
    private final String SERVICE_NAME = "FulfillmentScheduleExecutorService";
    
    public void executeSchedule(DateTime createdFrom, DateTime createdTill) {
        logger.info("Executing schedule from [" + createdFrom + "] to [" + createdTill + "]");
        
        List<FulfillmentScheduleDO> readyList = scheduleDAO.getSchedulesReadyForFulfillment(createdFrom, createdTill);
        
        logger.info("Processing readyList size: " + readyList.size());
        
        for (FulfillmentScheduleDO readySchedule : readyList) {
            try {
                execute(readySchedule);
            } catch (Exception e) {
                logger.error("Something went wrong with order ID: [" + readySchedule.getOrderId() + "] going ahead with the rest", e);
            }
        }
    }
    
    /**
     * Force immediate execution of a Scheduled fulfillment
     * @param orderId
     * @return <code>true</code> if execution succeeded.
     */
    @Transactional
    public boolean executeNow(String orderId) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return FAILURE;
        }
        
        if (scheduleList.size() != 1) {
            return FAILURE;
        }
        
        FulfillmentScheduleDO scheduleToExecute = scheduleList.get(0);
        
        return markCompleteAndExecute(scheduleToExecute);
    }
    
    /**
     * Cancels a scheduled order ID.
     * @param orderId
     * @return
     */
    @Transactional
    public boolean cancel(String orderId) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return FAILURE;
        }
        
        if (scheduleList.size() != 1) {
            return FAILURE;
        }
        
        FulfillmentScheduleDO scheduleToCancel = scheduleList.get(0);
        
        if (scheduleToCancel.getStatus() != ScheduleStatus.Scheduled) {
            return FAILURE;
        }
        
        historyService.markTransactionFailed(scheduleToCancel.getOrderId());
        scheduleDAO.markCancelledByAdmin(scheduleToCancel);
        
        /*
         * TODO: Ugly but works for now.
         */
        TransactionStatus dummyTransactionStatus = new TransactionStatus();
        dummyTransactionStatus.setTransactionStatus("07");
        
        refundService.initiateRechargeRefund(scheduleToCancel.getOrderId(), dummyTransactionStatus);
        
        return SUCCESS;
    }

    private void execute(FulfillmentScheduleDO readySchedule) {
        if (isReadyForExecution(readySchedule)) {
            markCompleteAndExecute(readySchedule);
        }
    }
    
    private boolean isReadyForExecution(FulfillmentScheduleDO readySchedule) {
        switch (readySchedule.getScheduleReason()) {
            
            case OperatorDown:
                int operatorId = readySchedule.getTriggerEvent().getCode();
                
                if (!alertService.isOperatorInAlertNoConfigCheck(operatorId)) {
                    return true;
                }

                DateTime scheduleExpiry = new DateTime(readySchedule.getExpiryTime().getTime());
                
                if (scheduleExpiry.isBeforeNow()) {
                    return true;
                }
                
                return false;
                
            case RepeatBlock:
                return true;
                
            case RechargeRetry:
                int fkOperatorMasterId = 0;
                try {
                    CircleOperatorMapping circleOperatorMapping = inService
                            .getCircleOperatorMapping(readySchedule.getOrderId());
                    
                    OperatorMaster operatorMaster = inService.getOperatorFromOrderId(readySchedule.getOrderId());
                    fkOperatorMasterId = operatorMaster.getOperatorMasterId();
                } catch (Exception ex) {
                    logger.error("Exception getting circle operator mapping for orderId:" + readySchedule.getOrderId(), ex);
                }
                if (fkOperatorMasterId != 0) {
                    final boolean isOperatorInAlert = alertService.isOperatorInAlertNoConfigCheck(fkOperatorMasterId);

                    if (isOperatorInAlert) {
                        logger.info("Operator is in alert not scheduling for execution: [" + readySchedule.getOrderId()
                                + "], [" + fkOperatorMasterId + "]");
                        return false;
                    }
                }
                return true;
            case InternalOperatorRetryBlock:
                return true;
                
            default:
                logger.error("Unable to handle schedule reason: " + String.valueOf(readySchedule.getScheduleReason()));
        }
        
        return false;
    }
    
    @Transactional
    private boolean markCompleteAndExecute(FulfillmentScheduleDO readySchedule) {
        int updateCount = scheduleDAO.markComplete(readySchedule);
        
        if (updateCount == 1) {
            /*
             * Check for operator Retry schedule
             */
            if (readySchedule.getScheduleReason() == ScheduleReason.InternalOperatorRetryBlock) {
                kestrelWrapper.enqueueRechargeRetryAsync(readySchedule.getOrderId());
                return SUCCESS;
            }
            
            ProductName productName = orderIdDAO.getProductName(readySchedule.getOrderId());
            if (productName == ProductName.Mobile || productName == ProductName.DTH || productName == ProductName.DataCard || productName == ProductName.PostPaidDataCard) {
                kestrelWrapper.enqueueRechargeAsync(readySchedule.getOrderId());
            } else if(productName == ProductName.MobilePostpaid) {
                billPaymentService.initiatePostpaidFulfillment(readySchedule.getOrderId());
            } else if(productName == ProductName.RechargeReward){
                RechargeRequestWebDo rechargeRequest = new RechargeRequestWebDo();
                rechargeRequest.setOrderId(readySchedule.getOrderId());
                kestrelWrapper.enqueueReward(rechargeRequest);
            }
            operatorDownService.decOperatorDownQueueCount(readySchedule.getTriggerEvent().getCode());
            metricsClient.recordEvent(SERVICE_NAME, "executeSchedule." + readySchedule.getScheduleReason(), "Success");
            return SUCCESS;
        } else {
            logger.error("Error in scheduling orderID: [" + readySchedule.getOrderId() + "]");
            metricsClient.recordEvent(SERVICE_NAME, "executeSchedule." + readySchedule.getScheduleReason(), "Failure");
            return FAILURE;
        }
    }
}
