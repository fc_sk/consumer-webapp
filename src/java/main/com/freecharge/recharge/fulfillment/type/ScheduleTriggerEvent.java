package com.freecharge.recharge.fulfillment.type;

public enum ScheduleTriggerEvent {
    /*
     * Repeat block expiry
     */
    BlockExpiry(0),
    
    /*
     * Operator down triggers.
     */
    AircelUP(1), AirtelUP(2), BSNLUP(3),
    DishTvUP(4), TataDocomoGSMUP(5), IdeaUP(6), IndicomWalkyUP(7),
    LoopUP(8), MTNLDelhiUP(9), RelianceCDMAUP(10), RelianceGSMUP(11),
    TataIndicomUP(12), UninorUP(13), VodafoneUP(14), BigTvUP(16),
    AirtelTvUP(17), SunTvUP(18), TataSkyUP(19), VideoconTvUP(20),
    TataPhotonPlusUP(21), MTSUP(22), RelianceNetconnectUP(23),
    VideoconMobileUP(24), VirginGSMUP(30), VirginCDMAUP(31),
    MTSMBrowseUP(40), MTSMblazeUP(41), TataDocomoCDMAUP(42),
    T24UP(43), AirtelDataCardUP(44), BSNLDataCardUP(45),
    MTNLDataCardDelhiUP(46), VodafoneDataCardUP(47),
    AircelDataCardUP(48), IdeaDataCardUP(49),
    RelianceGSMDataCardUP(50), TataIndicomDataCardUP(51),
    TataDocomoGSMDataCardUP(52), T24DataCardUP(53),
    MTNLMumbaiUP(54), MTNLDatacardMumbaiUP(55),
    AirtelPostpaidUP(56), LoopPostpaidUP(57),
    BsnlPostpaidUP(58), DocomoPostpaidGSMUP(59),
    IdeaPostpaidUP(60), DocomoPostpaidCDMAUP(61),
    VodafonePostpaidUP(62), ReliancePostpaidCDMAUP(63),
    ReliancePostpaidGSMUP(64),JIOUP(90),JIOPostpaidUP(91),JIOMifiUP(92),
    
    /*
     * Timeout of failed recharge.
     */
    RetryTimeout(65),
    OperatorRetry(101)
    ;
    
    private final int code;
    
    private ScheduleTriggerEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
    
    public static ScheduleTriggerEvent fromCode(int code) {
        switch (code) {
            case 0: return BlockExpiry;
            case 1: return AircelUP;
            case 2: return AirtelUP;
            case 3: return BSNLUP;
            case 4: return DishTvUP;
            case 5: return TataDocomoGSMUP;
            case 6: return IdeaUP;
            case 7: return IndicomWalkyUP;
            case 8: return LoopUP;
            case 9: return MTNLDelhiUP;
            case 10: return RelianceCDMAUP;
            case 11: return RelianceGSMUP;
            case 12: return TataIndicomUP;
            case 13: return UninorUP;
            case 14: return VodafoneUP;
            case 16: return BigTvUP;
            case 17: return AirtelTvUP;
            case 18: return SunTvUP;
            case 19: return TataSkyUP;
            case 20: return VideoconTvUP;
            case 21: return TataPhotonPlusUP;
            case 22: return MTSUP;
            case 23: return RelianceNetconnectUP;
            case 24: return VideoconMobileUP;
            case 30: return VirginGSMUP;
            case 31: return VirginCDMAUP;
            case 40: return MTSMBrowseUP;
            case 41: return MTSMblazeUP;
            case 42: return TataDocomoCDMAUP;
            case 43: return T24UP;
            case 44: return AirtelDataCardUP;
            case 45: return BSNLDataCardUP;
            case 46: return MTNLDataCardDelhiUP;
            case 47: return VodafoneDataCardUP;
            case 48: return AircelDataCardUP;
            case 49: return IdeaDataCardUP;
            case 50: return RelianceGSMDataCardUP;
            case 51: return TataIndicomDataCardUP;
            case 52: return TataDocomoGSMDataCardUP;
            case 53: return T24DataCardUP;
            case 54: return MTNLMumbaiUP;
            case 55: return MTNLDatacardMumbaiUP;
            case 56: return AirtelPostpaidUP;
            case 57: return LoopPostpaidUP;
            case 58: return BsnlPostpaidUP;
            case 59: return DocomoPostpaidGSMUP;
            case 60: return IdeaPostpaidUP;
            case 61: return DocomoPostpaidCDMAUP;
            case 62: return VodafonePostpaidUP;
            case 63: return ReliancePostpaidCDMAUP;
            case 64: return ReliancePostpaidGSMUP;
            case 65: return RetryTimeout;
            case 90: return JIOUP;
            case 91: return JIOPostpaidUP;
            case 92: return JIOMifiUP;
            case 101: return OperatorRetry;
            default: throw new IllegalArgumentException("Unknown code : " + String.valueOf(code));
        }
    }
}
