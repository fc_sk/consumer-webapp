package com.freecharge.recharge.fulfillment.type;

public enum ScheduleStatus {
    Scheduled(0), 
    InProgress(1),
    Completed(2),
    Expired(3),
    CancelledByCS(4),
    CancelledByUser(5);
    
    private final int code;
    
    private ScheduleStatus(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return code;
    }
    
    public static ScheduleStatus fromCode(int code) {
        switch (code) {
            case 0: return Scheduled;
            case 1: return InProgress;
            case 2: return Completed;
            case 3: return Expired;
            case 4: return CancelledByCS;
            case 5: return CancelledByUser;
            default: throw new IllegalArgumentException("Unknown code " + String.valueOf(code));
        }
    }
}
