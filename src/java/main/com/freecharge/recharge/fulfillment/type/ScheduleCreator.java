package com.freecharge.recharge.fulfillment.type;

public enum ScheduleCreator {
    FreeCharge(0),
    User(1);
    
    private final int code;
    
    private ScheduleCreator(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
    
    public static ScheduleCreator fromCode(int code) {
        switch (code) {
            case 0: return FreeCharge;
            case 1: return User;
            default: throw new IllegalArgumentException("Unknown code " + String.valueOf(code));
        }
    }
}
