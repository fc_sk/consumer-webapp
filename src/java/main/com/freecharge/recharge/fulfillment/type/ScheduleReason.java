package com.freecharge.recharge.fulfillment.type;


public enum ScheduleReason {
    RepeatBlock(0),
    OperatorDown(1),
    RechargeRetry(2),
    
    InternalOperatorRetryBlock(3);
    
    private final int code;
    
    private ScheduleReason(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
    
    public static ScheduleReason fromCode(int code) {
        switch (code) {
            case 0: return RepeatBlock;
            case 1: return OperatorDown;
            case 2: return RechargeRetry;
            case 3: return InternalOperatorRetryBlock;
            default: throw new IllegalArgumentException("Unknown code: " + String.valueOf(code));
        }
    }
}
