package com.freecharge.recharge.fulfillment.scheduler;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.fulfillment.RetryExpiryAlertExecutorService;

public class RetryExpiryAlertJob extends FreeChargeJob{
	private static final Logger logger = LoggingFactory.getLogger(RetryExpiryAlertJob.class);

    private RetryExpiryAlertExecutorService retryExpiryService;

    private void execute(int scanningDelay, int scanningWindow) {
        DateTime thresholdTime = DateTime.now().minusSeconds(scanningDelay);
        DateTime creationThreshold = DateTime.now().minusSeconds(scanningWindow);
        
        getHandler().executeRetryAlerting(thresholdTime, creationThreshold);
    }
    
    public static void main(String[] args) {
        Thread.currentThread().setName("RetryExpiryAlertJob");
        
        ClassPathXmlApplicationContext ctx = null;
        
        try {
            ctx = BatchJobUtil.initBatchJobContext();
            RetryExpiryAlertJob job = (RetryExpiryAlertJob) ctx.getBean("retryExpiryAlertJob");
            
            int scanningDelay = Integer.parseInt(args[0]);
            int scanningWindow = Integer.parseInt(args[1]);
            
            job.execute(scanningDelay, scanningWindow);
            
            logger.info("Done with execution");
        } catch (Exception e) {
            logger.error("ERROR occured in executing retryExpiryAlertJob", e);
        } finally {
            if (ctx != null) {
                try {
                    BatchJobUtil.destroyBatchJobContext(ctx);
                } catch (IOException ioe) {
                    logger.error("ERROR while destroying batch job context.", ioe);
                }
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
    	RetryExpiryAlertExecutorService retryExpService = jobContext.getBean(RetryExpiryAlertExecutorService.class);
    	int scanningDelay = Integer.parseInt(arguments[0]);
    	int scanningWindow = Integer.parseInt(arguments[1]);
    	DateTime thresholdTime = DateTime.now().minusSeconds(scanningDelay);
    	DateTime creationThreshold = DateTime.now().minusSeconds(scanningWindow);
    	retryExpService.executeRetryAlerting(thresholdTime, creationThreshold);
    }

    public RetryExpiryAlertExecutorService getHandler() {
        return retryExpiryService;
    }

    public void setHandler(RetryExpiryAlertExecutorService handler) {
        this.retryExpiryService = handler;
    }

}
