package com.freecharge.recharge.fulfillment.scheduler;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleExecutorService;

public class FulfillmentSchedulerJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory.getLogger(FulfillmentSchedulerJob.class);

    private FulfillmentScheduleExecutorService scheduleService;

    private void execute(int scanningDelay, int scanningWindow) {
        DateTime to = DateTime.now().minusMinutes(scanningDelay);
        DateTime from = to.minusMinutes(scanningWindow);
        
        getHandler().executeSchedule(from, to);
    }
    
    public static void main(String[] args) {
        Thread.currentThread().setName("FulfillmentSchedulerJob");
        
        ClassPathXmlApplicationContext ctx = null;
        
        try {
            ctx = BatchJobUtil.initBatchJobContext();
            FulfillmentSchedulerJob job = (FulfillmentSchedulerJob) ctx.getBean("fulfillmentSchedulerJob");

            int scanningDelay = Integer.parseInt(args[0]);
            int scanningWindow = Integer.parseInt(args[1]);
            
            job.execute(scanningDelay, scanningWindow);
            
            logger.info("Done with execution");
        } catch (Exception e) {
            logger.error("ERROR occured in executing FulfillmentSchedulerJob", e);
        } finally {
            if (ctx != null) {
                try {
                    BatchJobUtil.destroyBatchJobContext(ctx);
                } catch (IOException ioe) {
                    logger.error("ERROR while destroying batch job context.", ioe);
                }
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        int scanningDelay = Integer.parseInt(arguments[0]);
        int scanningWindow = Integer.parseInt(arguments[1]);
        
        DateTime to = DateTime.now().minusMinutes(scanningDelay);
        DateTime from = to.minusMinutes(scanningWindow);
        
        FulfillmentScheduleExecutorService schedulerService = jobContext.getBean(FulfillmentScheduleExecutorService.class);
        schedulerService.executeSchedule(from, to);
    }

    public FulfillmentScheduleExecutorService getHandler() {
        return scheduleService;
    }

    public void setHandler(FulfillmentScheduleExecutorService handler) {
        this.scheduleService = handler;
    }
}
