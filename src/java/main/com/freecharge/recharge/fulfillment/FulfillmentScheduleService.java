package com.freecharge.recharge.fulfillment;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.service.UserService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDAO;
import com.freecharge.recharge.fulfillment.db.FulfillmentScheduleDO;
import com.freecharge.recharge.fulfillment.type.ScheduleCreator;
import com.freecharge.recharge.fulfillment.type.ScheduleReason;
import com.freecharge.recharge.fulfillment.type.ScheduleStatus;
import com.freecharge.recharge.fulfillment.type.ScheduleTriggerEvent;
import com.freecharge.recharge.services.CommonRechargeService;
import com.freecharge.recharge.services.OperatorAlertService;
import com.freecharge.recharge.services.OperatorDownService;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;

@Service
public class FulfillmentScheduleService {
    private static Logger logger = LoggingFactory.getLogger(FulfillmentScheduleService.class);
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private FulfillmentScheduleDAO scheduleDAO;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private AppConfigService appConfig;
    
    @Autowired
    private CommonRechargeService rechargeService;
    
    @Autowired
    private OperatorAlertService alertService;

    @Autowired
    private OperatorDownService operatorDownService;
    
    @Autowired
	private FCProperties fcProperties;
    
    private final long SCHEDULING_THRESHOLD_SECONDS = 2;

	private final String SERVICE_NAME = "FulfillmentScheduleService";
    
    // TODO: Make this configurable.
    private final int OPERATOR_DOWN_EXPIRY_HOURS = 3;
    private final int SCHEDULE_EXPIRY_RANDOMIZER_ULIMIT = 120000; 
    
    private final String EVENTS_ENABLED_FOR_SCHEDULE = "scheduleEnabledEventList";
    
    private final String           AG_CODE_FOR_OP_RETRY              = "agCodeForOpRetry";
    
    public List<FulfillmentScheduleDO> getSchedule(String orderId) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null) {
            return new ArrayList<>();
        }
        
        return scheduleList;
    }
    
    @Transactional(readOnly = true)
    public boolean isScheduledForRecharge(String orderId) {
        FulfillmentScheduleDO scheduleToCheck = this.getScheduledRecharge(orderId);
        
        if (scheduleToCheck == null) {
            /*
             * Not scheduled in the first place 
             * so go ahead.
             */
            return true;
        }

        return hasExpired(scheduleToCheck);
    }
    
    @Transactional(readOnly = true)
    public boolean isInScheduledState(String orderId) {
        return getScheduledRecharge(orderId) != null;
    }
    
    @Transactional
    public boolean createScheduleForRetry(String orderId, DateTime retryTime) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return createRetrySchedule(orderId, retryTime);
        }
        
        logger.info("Duplicate creation request :[" + orderId + "]");
        metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForRetry", "DuplicateRequest");
        
        if (scheduleList.size() > 1) {
            metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForRetry", "MultipleEntries");
            return false;
        }
        
        FulfillmentScheduleDO existingSchedule = scheduleList.get(0);
        
        if (existingSchedule.getScheduleReason() == ScheduleReason.OperatorDown || existingSchedule.getScheduleReason() == ScheduleReason.RepeatBlock) {
            if (existingSchedule.getStatus() == ScheduleStatus.Completed) {
                /*
                 * Don't bother if schedule was canceled by CS
                 * or by user.
                 */
                return createRetrySchedule(orderId, retryTime);
            }
        }
        
        return false;
    }
    
    @Transactional(readOnly = true)
    public FulfillmentScheduleDO getScheduledRecharge(String orderId) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getScheduledStatusByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return null;
        }
        
        /*
         * Verify that exactly one schedule is in Scheduled state
         */
        if (scheduleList.size() > 1) {
            throw new IllegalStateException("More than one schedule found for orderID: [" + String.valueOf(orderId) + "]");
        }
        
        FulfillmentScheduleDO scheduleToReturn = scheduleList.get(0);
        
        return scheduleToReturn;
    }
    
    @Transactional
    private boolean createRetrySchedule(String orderId, DateTime retryTime) {
    	
    	FulfillmentScheduleDO scheduleDo = new FulfillmentScheduleDO();
        
        scheduleDo.setCreatedBy(ScheduleCreator.FreeCharge);
        scheduleDo.setExpiryTime(new Timestamp(retryTime.getMillis()));
        scheduleDo.setOrderId(orderId);
        scheduleDo.setScheduleReason(ScheduleReason.RechargeRetry);
        scheduleDo.setStatus(ScheduleStatus.Scheduled);
        scheduleDo.setTriggerEvent(ScheduleTriggerEvent.RetryTimeout);
        
        int updateCount = scheduleDAO.create(scheduleDo);
        
        final boolean scheduleCreated = updateCount == 1;
        
        if (scheduleCreated) {
            metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForRetry", "Created");
        }
        
        return scheduleCreated;
    }
    
    @Transactional
    public boolean updateRetrySchedule(String orderId, DateTime retryTime) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getRetryScheduleByOrderId(orderId);
        final Timestamp triggerTime = new Timestamp(retryTime.getMillis());
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            metricsClient.recordEvent(SERVICE_NAME, "UpdateRetrySchedule", "NoSchedule");
            return false;
        }
        
        if (scheduleList.size() > 1) {
            metricsClient.recordEvent(SERVICE_NAME, "UpdateRetrySchedule", "MultipleSchedule");
            return false;
        }
        
        FulfillmentScheduleDO scheduleToUpdate = scheduleList.get(0);
        
        int updateCount = scheduleDAO.extendExpiryForRetrySchedule(scheduleToUpdate, triggerTime);

        return updateCount == 1;
    }
    
    public boolean checkAndcreateSchedule(RechargeDo rechargeDo) {
        boolean scheduleCreated = false;
        
        if (isScheduleEnabled(ScheduleTriggerEvent.BlockExpiry)) {
            SuccessfulRechargeInfo lastRecharge = userService.getLastSuccessfulRechargeNoConfigCheck(rechargeDo.getMobileNo());
            
            if (shouldCreateSchedule(lastRecharge, rechargeDo.getAmount())) {
                DateTime nextSafeAttemptTime = getSchedule(lastRecharge);
				logger.info("Next Safe Attempt for serviceNumber" + rechargeDo.getMobileNo() + " is "
						+ nextSafeAttemptTime.toString());
                /*
                 * This is to ensure that in case user tries multiple
                 * recharges even when there's an existing schedule
                 * we don't schedule all of them at the same instant
                 * and get repeat failure again. So we randomly spread
                 * out the expiry time. 
                 * 
                 * Adding a 5 seconds time out as even if Random is 0, there is a 5 seconds delay.
                 */
                DateTime scheduleExpiry = nextSafeAttemptTime.plusMillis(RandomUtils.nextInt(SCHEDULE_EXPIRY_RANDOMIZER_ULIMIT)).plusMillis(5000);
                
                scheduleCreated = this.createScheduleForRepeatBlock(rechargeDo.getOrderId(), new Timestamp(scheduleExpiry.getMillis()));
                
                if (scheduleCreated) {
                    metricsClient.recordEvent(SERVICE_NAME, "BlockExpirySchedule", "Created");
                    return true;
                }
            }
        }
        
        if (alertService.isOperatorInAlertNoConfigCheck(rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId())) {
            scheduleCreated = this.createScheduleForOperatorDown(rechargeDo);
            
            if (scheduleCreated) {
                operatorDownService.incOperatorDownQueueCount(rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId());
                metricsClient.recordEvent(SERVICE_NAME, "OperatorDown." + rechargeDo.getOperatorCode(), "Created");
                return true;
            }
        }
        return scheduleCreated;
    }
    
    /**
     * Given an order ID checks whether valid schedule
     * exists and that it is in <b>Completed</b> state.
     * @param orderId
     * @return
     */
    @Transactional
    public boolean hasScheduleExecuted(String orderId) {
        // TODO: Revisit whether we need to use InProgress state here. 
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return false;
        }
        
        if (scheduleList.size() > 1) {
            logger.error("More than one schedule found for orderID: [" + String.valueOf(orderId) + "]");
            for (FulfillmentScheduleDO scheduleDo : scheduleList) {
                if (scheduleDo.getStatus() != ScheduleStatus.Completed) {
                    return false;
                }
            }
            // Return true if all the schedules are completed
            return true;
        }
        
        FulfillmentScheduleDO scheduleDo = scheduleList.get(0);
        
        if (scheduleDo.getStatus() == ScheduleStatus.Completed) {
            return true;
        }
        
        return false;
    }
    
    @Transactional
    public boolean markRetryScheduleComplete(String orderId) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            return false;
        }

        for (FulfillmentScheduleDO scheduleDO : scheduleList) {
            if (scheduleDO.getStatus() != ScheduleStatus.Completed) {
                logger.error("Marking schedule as completed orderID: [" + String.valueOf(orderId) + "]");
                return scheduleDAO.markComplete(scheduleDO) == 1;
            }
        }
        return false;
    }
    
    private DateTime getSchedule(SuccessfulRechargeInfo lastRecharge) {
        DateTime rechargeTime = new DateTime(lastRecharge.getRechargeTime());
        String lastRechargedOperator = lastRecharge.getOperatorCode();
        
        DateTime scheduleTime = rechargeTime.plusSeconds(rechargeService.getSuccessBlockSeconds(lastRechargedOperator).intValue());
        
        return scheduleTime;
    }
    
    private DateTime getOperatorRetrySchedule() {
        DateTime rechargeTime = new DateTime();
        DateTime scheduleTime = rechargeTime.plusSeconds(rechargeService.getOperatorRetryBlockTime());
        return scheduleTime;
    }

    private boolean shouldCreateSchedule(SuccessfulRechargeInfo lastRecharge, Float rechargeAmount) {
        if (lastRecharge == null) {
            return false;
        }
        
        DateTime rechargeTime = new DateTime(lastRecharge.getRechargeTime());
        
        if (rechargeTime.isAfterNow()) {
            logger.error("Illegal recharge time [" + lastRecharge.getOrderID() + "]");
            metricsClient.recordEvent(SERVICE_NAME, "LastRecharge", "Error");
            return false;
        }
        
        String lastRechargedOperator = lastRecharge.getOperatorCode();
        
        if (!rechargeService.isScheduleEnabled(lastRechargedOperator)) {
            return false;
        }
        
        if (rechargeService.isSameAmountRetryBlocked(lastRechargedOperator)) {
            Float lastRechargeAmount = lastRecharge.getAmount().getAmount().floatValue();
            if (!lastRechargeAmount.equals(rechargeAmount)) {
                return false;
            }
        }
        
        DateTime scheduleTime = getSchedule(lastRecharge);
        
        /*
         * We are past the block period.
         */
        if (scheduleTime.isBeforeNow()) {
            return false;
        }
        
        final int secondsBeforeNextAttempt = Seconds.secondsBetween(DateTime.now(), scheduleTime).getSeconds();
        
        if (secondsBeforeNextAttempt <= SCHEDULING_THRESHOLD_SECONDS) {
            return false;
        }
        
        return true;
    }
    
    private boolean hasExpired(FulfillmentScheduleDO scheduleDo) {
        DateTime expiryTime = new DateTime(scheduleDo.getExpiryTime());
        
        return expiryTime.isBeforeNow();
    }
    
    private boolean isScheduleEnabled(ScheduleTriggerEvent event) {
        JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);
        
        if (configJson != null) {
            final String enabledEventListString = (String) configJson.get(EVENTS_ENABLED_FOR_SCHEDULE);
            logger.info("Events Enabled are "+enabledEventListString);
            
            if (!StringUtils.isBlank(enabledEventListString)) {
                List<String> eventList = Arrays.asList(enabledEventListString.split(","));
                
                if (eventList.contains(event.name())) {
                    return true;
                }
                
            }
        }else{
        	logger.info("RECHARGE_CUSTOM_CONFIG is null");
        }
        
        return false;
    }
    
    public boolean isOpRetryAgCodes(String errorCode) {
        JSONObject configJson = appConfig.getCustomProp(appConfig.RECHARGE_CUSTOM_CONFIG);
        
        if (configJson != null) {
            final String enabledEventListString = (String) configJson.get(AG_CODE_FOR_OP_RETRY);
            
            if (!StringUtils.isBlank(enabledEventListString)) {
                List<String> eventList = Arrays.asList(enabledEventListString.split(","));
                
                if (eventList.contains(errorCode)) {
                    return true;
                }
                
            }
        }
        
        return false;
    }
    
    public boolean checkAndCreateOperatorRetrySchedule(String orderId) {
    	
        if (isScheduleEnabled(ScheduleTriggerEvent.OperatorRetry)) {
            logger.info("Scheduling recharge");
        	DateTime nextSafeAttemptTime = getOperatorRetrySchedule();
            
            DateTime scheduleExpiry = nextSafeAttemptTime.plusMillis(RandomUtils.nextInt(SCHEDULE_EXPIRY_RANDOMIZER_ULIMIT)).plusMillis(5000);
            
            boolean scheduleCreated = this.createScheduleForOperatorRetry(orderId, new Timestamp(scheduleExpiry.getMillis()));
            
            if (scheduleCreated) {
                metricsClient.recordEvent(SERVICE_NAME, "OperatorRetrySchedule", "Created");
                return true;
            }
        }
        return false;
    }

    private boolean createScheduleForRepeatBlock(String orderId, Timestamp triggerTime) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            FulfillmentScheduleDO scheduleDo = new FulfillmentScheduleDO();
            
            scheduleDo.setCreatedBy(ScheduleCreator.FreeCharge);
            scheduleDo.setExpiryTime(triggerTime);
            scheduleDo.setOrderId(orderId);
            scheduleDo.setScheduleReason(ScheduleReason.RepeatBlock);
            scheduleDo.setStatus(ScheduleStatus.Scheduled);
            scheduleDo.setTriggerEvent(ScheduleTriggerEvent.BlockExpiry);
            
            int updateCount = scheduleDAO.create(scheduleDo);
            
            return updateCount == 1;
        }

        logger.info("Duplicate creation request :[" + orderId + "]");
        metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForRepeatBlock", "DuplicateRequest");
        
        if (scheduleList.size() > 1) {
            metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForRepeatBlock", "MultipleEntries");
            return false;
        }
        
        FulfillmentScheduleDO scheduleToUpdate = scheduleList.get(0);
        
        int updateCount = scheduleDAO.extendExpiryForCompletedBlockExpiry(scheduleToUpdate, triggerTime);
        
        return updateCount == 1;
    }
    
    private boolean createScheduleForOperatorRetry(String orderId, Timestamp triggerTime) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(orderId);
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            FulfillmentScheduleDO scheduleDo = new FulfillmentScheduleDO();
            
            scheduleDo.setCreatedBy(ScheduleCreator.FreeCharge);
            scheduleDo.setExpiryTime(triggerTime);
            scheduleDo.setOrderId(orderId);
            scheduleDo.setScheduleReason(ScheduleReason.InternalOperatorRetryBlock);
            scheduleDo.setStatus(ScheduleStatus.Scheduled);
            scheduleDo.setTriggerEvent(ScheduleTriggerEvent.OperatorRetry);
            
            int updateCount = scheduleDAO.create(scheduleDo);
            
            return updateCount == 1;
        }

        logger.info("Duplicate creation request :[" + orderId + "]");
        metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForOperatorRetryBlock", "DuplicateRequest");
        
        if (scheduleList.size() > 1) {
            metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForOperatorRetryBlock", "MultipleEntries");
            return false;
        }
        
        FulfillmentScheduleDO scheduleToUpdate = scheduleList.get(0);
        
        int updateCount = scheduleDAO.extendExpiryForCompletedBlockExpiry(scheduleToUpdate, triggerTime);
        
        return updateCount == 1;
    }
    
    private boolean createScheduleForOperatorDown(RechargeDo rechargeDo) {
        List<FulfillmentScheduleDO> scheduleList = scheduleDAO.getByOrderId(rechargeDo.getOrderId());
        
        if (scheduleList == null || scheduleList.isEmpty()) {
            Timestamp triggerTime = new Timestamp(DateTime.now().plusHours(OPERATOR_DOWN_EXPIRY_HOURS).getMillis());
            FulfillmentScheduleDO scheduleDo = new FulfillmentScheduleDO();
            
            scheduleDo.setCreatedBy(ScheduleCreator.FreeCharge);
            scheduleDo.setExpiryTime(triggerTime);
            scheduleDo.setOrderId(rechargeDo.getOrderId());
            scheduleDo.setScheduleReason(ScheduleReason.OperatorDown);
            scheduleDo.setStatus(ScheduleStatus.Scheduled);
            scheduleDo.setTriggerEvent(ScheduleTriggerEvent.fromCode(rechargeDo.getCircleOperatorMapping().getFkOperatorMasterId()));
            
            int updateCount = scheduleDAO.create(scheduleDo);
            
            return updateCount == 1;
        }
        
        logger.error("Duplicate creation request :[" + rechargeDo.getOrderId() + "]");
        metricsClient.recordEvent(SERVICE_NAME, "CreateScheduleForOperatorDown", "DuplicateRequest");
        return false;
    }
}
