package com.freecharge.recharge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/order/{orderID}")
public class RechargeStatusController {

	public static final String RECHARGE_STATUS_URL_FORMAT = "/app/order/%s/recharge-status.htm";
	public static final String RECHARGE_STATUS_MESSAGE_URL_FORMAT = "/app/order/%s/recharge-status-message.htm";

	@Autowired
    private RechargeStatusService reshargeStatusService;

	public static String getTrackingURL(String orderID) {
		return String.format(RECHARGE_STATUS_URL_FORMAT, orderID);
	}

	@RequestMapping(value = "recharge-status", method = RequestMethod.GET)
	public ModelAndView showStatus(@PathVariable String orderID) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("orderID", orderID);
		modelAndView.addObject("ajaxURL", String.format(RECHARGE_STATUS_MESSAGE_URL_FORMAT, orderID));
		modelAndView.setViewName("recharge/status");
		return modelAndView;
	}

	@RequestMapping(value = "recharge-status-message", method = RequestMethod.GET, produces = "application/json")
	public String getStatusString(@PathVariable String orderID, Model model) {
		model.addAttribute("message", reshargeStatusService.getRechargeStatus(orderID).getMessage());
		return "jsonView";
	}
}
