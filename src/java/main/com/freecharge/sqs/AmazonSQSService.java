package com.freecharge.sqs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.amazonaws.services.sqs.model.SendMessageBatchResultEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.freecharge.common.framework.logging.LoggingFactory;

public class AmazonSQSService {

    private Logger              logger = LoggingFactory.getLogger(getClass());

    private AmazonSQSProp       amazonsqsProps;
    private BasicAWSCredentials credentials;
    private AmazonSQS           sqs;

    public AmazonSQSService(AmazonSQSProp amazonsqs) {
        this.amazonsqsProps = amazonsqs;
        this.credentials = new BasicAWSCredentials(this.amazonsqsProps.getAwsAccessKey(),
                this.amazonsqsProps.getAwsSecretKey());
        this.sqs = new AmazonSQSClient(this.credentials);
    }

    public String getQueueUrl(String queueName) {
        
        /*
         * GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(queueName); return
         * this.sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl();
         */
        // return "https://sqs.ap-southeast-1.amazonaws.com/695210568016/"+queueName;
        
        return String.format("%s/%s/%s", this.amazonsqsProps.getSqsUrl(), this.amazonsqsProps.getPrincipal(),
                this.amazonsqsProps.getSqsName());
    }

    public List<Message> getMessagesFromQueue() {
        String queueUrl = getQueueUrl(this.amazonsqsProps.getSqsName());
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        receiveMessageRequest.withMaxNumberOfMessages(this.amazonsqsProps.getReadCount());
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        return messages;
    }

    public Message getMessageFromQueue() {
        String queueUrl = getQueueUrl(this.amazonsqsProps.getSqsName());
        logger.debug("queueUrl : " + queueUrl);
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        receiveMessageRequest.withMaxNumberOfMessages(1);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        if (messages != null && messages.size() > 0) {
            return messages.get(0);
        }
        return null;
    }

    public void deleteMessageFromQueue(Message message) {
        String queueUrl = getQueueUrl(this.amazonsqsProps.getSqsName());
        String messageRecieptHandle = message.getReceiptHandle();
        logger.debug("Message to be deleted : " + message.getBody() + "." + message.getReceiptHandle());
        sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageRecieptHandle));
        logger.debug("Message Deleted...");
    }

    public SendMessageResult sendMessageToQueue(String message) {
        String queueUrl = getQueueUrl(this.amazonsqsProps.getSqsName());
        SendMessageRequest sendMessageRequest = new SendMessageRequest(queueUrl, message);
        SendMessageResult sendMessageResult = sqs.sendMessage(sendMessageRequest);
        logger.debug("Message send..." + sendMessageResult.getMessageId());
        return sendMessageResult;
    }

    public String postMessageToQueue(String mobile, String message) {
        SendMessageBatchRequestEntry messageEntry = new SendMessageBatchRequestEntry(mobile, message);
        List<SendMessageBatchRequestEntry> entries = new ArrayList<SendMessageBatchRequestEntry>();
        entries.add(messageEntry);
        List<String> result = sendBatchMessageToQueue(entries);
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public List<String> sendBatchMessageToQueue(List<SendMessageBatchRequestEntry> entries) {
        List<String> successList = new ArrayList<String>();
        String queueUrl = getQueueUrl(this.amazonsqsProps.getSqsName());
        SendMessageBatchRequest sendMessageBatchRequest = new SendMessageBatchRequest(queueUrl, entries);
        SendMessageBatchResult sendMessageBatchResult = sqs.sendMessageBatch(sendMessageBatchRequest);
        List<SendMessageBatchResultEntry> list = sendMessageBatchResult.getSuccessful();
        for (SendMessageBatchResultEntry entry : list) {
            successList.add(entry.getMessageId());
        }
        return successList;
    }

}
