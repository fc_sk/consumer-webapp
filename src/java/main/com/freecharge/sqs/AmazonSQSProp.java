package com.freecharge.sqs;

public class AmazonSQSProp {

    private String  awsAccessKey;
    private String  awsSecretKey;
    private String  sqsUrl;
    private String  principal;
    private String  sqsName;
    private Integer readCount;

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public void setAwsAccessKey(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }

    public void setAwsSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    public String getSqsUrl() {
        return sqsUrl;
    }

    public void setSqsUrl(String sqsUrl) {
        this.sqsUrl = sqsUrl;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getSqsName() {
        return sqsName;
    }

    public void setSqsName(String sqsName) {
        this.sqsName = sqsName;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

}
