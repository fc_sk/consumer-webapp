package com.freecharge.sqs;

import com.freecharge.common.util.FCConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

@Configuration
public class AmazonSQSConfig {

    private Logger       logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;

    @Bean
    public AmazonSQSProp sqsDID() {

        AmazonSQSProp amazonSQSprop = new AmazonSQSProp();
        amazonSQSprop.setAwsAccessKey(fcProperties.getAwsSqsAccessKey());
        amazonSQSprop.setAwsSecretKey(fcProperties.getAwsSqsSecretKey());
        amazonSQSprop.setSqsName(fcProperties.getDidMainQueue());
        amazonSQSprop.setSqsUrl(fcProperties.getAwsSqsUrl());
        amazonSQSprop.setPrincipal(fcProperties.getAwsSqsPrincipal());
        amazonSQSprop.setReadCount(1);
        return amazonSQSprop;
    }

    @Bean
    public AmazonSQSProp sqsDIDSMS() {

        AmazonSQSProp amazonSQSprop = new AmazonSQSProp();
        amazonSQSprop.setAwsAccessKey(fcProperties.getAwsSqsAccessKey());
        amazonSQSprop.setAwsSecretKey(fcProperties.getAwsSqsSecretKey());
        amazonSQSprop.setSqsName(fcProperties.getDidSmsQueue());
        amazonSQSprop.setSqsUrl(fcProperties.getAwsSqsUrl());
        amazonSQSprop.setPrincipal(fcProperties.getAwsSqsPrincipal());
        amazonSQSprop.setReadCount(1);
        return amazonSQSprop;
    }

    @Bean
    public AmazonSQSProp sqsRewardService() {
        AmazonSQSProp amazonSQSprop = new AmazonSQSProp();
        amazonSQSprop.setAwsAccessKey(fcProperties.getProperty(FCConstants.REWARD_REQUEST_SQS_ACCESS_KEY));
        amazonSQSprop.setAwsSecretKey(fcProperties.getProperty(FCConstants.REWARD_SQS_SECRET));
        amazonSQSprop.setSqsName(fcProperties.getProperty(FCConstants.REWARD_REQUEST_QUEUE_NAME));
        amazonSQSprop.setSqsUrl(fcProperties.getProperty(FCConstants.REWARD_REQUEST_QUEUE_URL));
        amazonSQSprop.setPrincipal(fcProperties.getAwsSqsPrincipal());
        amazonSQSprop.setReadCount(1);
        logger.info("Reward Service queue props " + amazonSQSprop.getAwsAccessKey() + " " +
                amazonSQSprop.getAwsSecretKey() + " " + amazonSQSprop.getSqsName() + " " +
        amazonSQSprop.getSqsUrl() + " " + amazonSQSprop.getPrincipal());
        return amazonSQSprop;
    }
}
