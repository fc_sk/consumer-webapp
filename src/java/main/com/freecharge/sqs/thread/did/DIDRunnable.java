package com.freecharge.sqs.thread.did;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sqs.did.DIDSQSService;

public class DIDRunnable implements Runnable {

    private Logger        logger = LoggingFactory.getLogger(getClass());

    private DIDSQSService didSQSService;

    public DIDSQSService getDidSQSService() {
        return didSQSService;
    }

    public void setDidSQSService(DIDSQSService didSQSService) {
        this.didSQSService = didSQSService;
    }

    @Override
    public void run() {
        while (true) {
            try {
                didSQSService.processMessage();
            } catch (Exception e) {
                logger.error("Exception occurred running thread. ", e);
            }
        }
    }

}
