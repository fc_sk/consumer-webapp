package com.freecharge.sqs.thread.did;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sqs.did.DIDSQSService;

public class DIDMain {

    private static Logger    logger  = LoggingFactory.getLogger(DIDMain.class.getName());

    private static final int NTHREDS = 100;

    /*
     * 100 Threads : 166/s | 200 Threads : 206/s | 300 Threads : 250/s
     */

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = null;
        try {
            logger.info("Starting to run the Job....");
            ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
            DIDSQSService didSQSService = (DIDSQSService) ctx.getBean(DIDSQSService.getBeanName());
            logger.info("Spring context initialization done");
            ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
            for (int i = 0; i < NTHREDS; i++) {
                DIDRunnable worker = new DIDRunnable();
                worker.setDidSQSService(didSQSService);
                executor.execute(worker);
            }
            executor.shutdown();
        } catch (Exception e) {
            logger.error("Exception running the batch job ", e);
        }
    }
}
