package com.freecharge.sqs.thread.did;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sqs.did.DIDSMSSQSService;

public class DIDSMSMain {

    private static Logger    logger  = LoggingFactory.getLogger(DIDSMSMain.class.getName());

    private static final int NTHREDS = 100;

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = null;
        try {
            logger.info("Starting to run the Job....");
            ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
            DIDSMSSQSService didSMSSQSService = (DIDSMSSQSService) ctx.getBean(DIDSMSSQSService.getBeanName());
            logger.info("Spring context initialization done");
            ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
            for (int i = 0; i < NTHREDS; i++) {
                DIDSMSRunnable worker = new DIDSMSRunnable();
                worker.setDidSMSSQSService(didSMSSQSService);
                executor.execute(worker);
            }
            executor.shutdown();
        } catch (Exception e) {
            logger.error("Exception running the batch job ", e);
        }
    }
}
