package com.freecharge.sqs.thread.did;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sqs.did.DIDSMSSQSService;

public class DIDSMSRunnable implements Runnable {

    private Logger           logger = LoggingFactory.getLogger(getClass());

    private DIDSMSSQSService didSMSSQSService;

    public DIDSMSSQSService getDidSMSSQSService() {
        return didSMSSQSService;
    }

    public void setDidSMSSQSService(DIDSMSSQSService didSMSSQSService) {
        this.didSMSSQSService = didSMSSQSService;
    }

    @Override
    public void run() {
        while (true) {
            try {
                didSMSSQSService.deliverMessages();
            } catch (Exception e) {
                logger.error("Exception occurred running thread. ", e);
            }
        }
    }

}
