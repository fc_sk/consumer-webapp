package com.freecharge.sqs.did;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.model.Message;
import com.freecharge.common.comm.sms.SMSACLService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.sqs.AmazonSQSProp;
import com.freecharge.sqs.AmazonSQSService;

@Service("didSMSSQSService")
public class DIDSMSSQSService extends AmazonSQSService {

    private Logger        logger = LoggingFactory.getLogger("did.sms."+DIDSMSSQSService.class.getName());

    @Autowired
    @Qualifier("smsACLService")
    private SMSACLService smsACLService;
    
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    public DIDSMSSQSService(@Qualifier("sqsDIDSMS") AmazonSQSProp amazonDIDsqs) {
        super(amazonDIDsqs);
    }

    public static String getBeanName() {
        return "didSMSSQSService";
    }

    public void deliverMessage() {
        try {
            Message message = getMessageFromQueue();
            if (message == null) {
                return;
            }
            Map<String, String> msgMap = parseMessage(message);
            if(msgMap.size() >= 3) {
                String mobile = msgMap.get("mobile");
                String smsTxt = msgMap.get("smstext");
                String promocode = msgMap.get("promocode");
                
                boolean isSMSsent = smsACLService.sendSMS(mobile, smsTxt, promocode);
                if (isSMSsent) {
                    deleteMessageFromQueue(message);
                } else {
                    logger.error("SMS sent failed for mobile " + mobile);
                }
                
            } else {
                logger.error("SMS message parse failed for message " + message);
            }

        } catch (AmazonServiceException ase) {
            logger.error(
                    "Caught an AmazonServiceException, which means your request made it to Amazon SQS, but was rejected with an error response for some reason."
                            + "Error Message: "
                            + ase.getMessage()
                            + ",HTTP Status Code: "
                            + ase.getStatusCode()
                            + ",AWS Error Code: "
                            + ase.getErrorCode()
                            + ",Error Type: "
                            + ase.getErrorType()
                            + ",Request ID: " + ase.getRequestId(), ase);
        } catch (AmazonClientException ace) {
            logger.error(
                    "Caught an AmazonClientException, which means the client encountered a serious internal problem while trying to communicate with SQS, such as not being able to access the network."
                            + "Error Message: " + ace.getMessage(), ace);
        }

    }
    
    public void deliverMessages() {
        
        Integer aclbatchSize = fcProperties.getSmsAclMultiSmsSize();
        Map<String, String> mutliSMSmap = new HashMap<String, String>();
        List<String> promocodes = new ArrayList<String>();
        List<Message> processedMessages = new ArrayList<Message>();
        try {
            for (int i = 0; i < aclbatchSize; i++) {
                Message message = getMessageFromQueue();
                if (message != null) {
                    Map<String, String> msgMap = parseMessage(message);
                    if(msgMap.size() >= 3) {
                        String mobile = msgMap.get("mobile");
                        String smsTxt = msgMap.get("smstext");
                        String promocode = msgMap.get("promocode");
                        
                        if(mutliSMSmap.containsValue(smsTxt)) {
                            continue;
                        }
                        mutliSMSmap.put(mobile, smsTxt);
                        promocodes.add(promocode);
                        processedMessages.add(message);
                        
                    } else {
                        logger.error("SMS message parse failed for message " + message);
                    }
                    
                } else {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        logger.error("Error " + message, e);
                    }
                    logger.debug("No message received from queue.");
                    break;
                }
            }
            
            if(mutliSMSmap.size() > 0) {
                boolean isSMSsent = false;
                try {
                    isSMSsent = smsACLService.sendMultiSMS(mutliSMSmap, promocodes);
                } catch (IOException e) {
                    logger.error("Error posting to SMS service ", e);
                }
                if (isSMSsent) {
                    for(Message message : processedMessages) {
                        deleteMessageFromQueue(message);
                    }
                } else {
                    logger.error("SMS sent failed for mobiles " + mutliSMSmap.keySet() + " with promocodes " + promocodes);
                }
            }
        } catch (AmazonServiceException ase) {
            logger.error(
                    "Caught an AmazonServiceException, which means your request made it to Amazon SQS, but was rejected with an error response for some reason."
                            + "Error Message: "
                            + ase.getMessage()
                            + ",HTTP Status Code: "
                            + ase.getStatusCode()
                            + ",AWS Error Code: "
                            + ase.getErrorCode()
                            + ",Error Type: "
                            + ase.getErrorType()
                            + ",Request ID: " + ase.getRequestId(), ase);
        } catch (AmazonClientException ace) {
            logger.error(
                    "Caught an AmazonClientException, which means the client encountered a serious internal problem while trying to communicate with SQS, such as not being able to access the network."
                            + "Error Message: " + ace.getMessage(), ace);
        }

    }
    
    private Map<String, String> parseMessage(Message message) {
        Map<String, String> msgMap = new HashMap<String, String>();
        try {

            JSONObject jsonMsg = new JSONObject(message.getBody());

            String mobile = jsonMsg.getString("mobile");
            msgMap.put("mobile", mobile);
            String promocode = jsonMsg.getString("promocode");
            msgMap.put("promocode", promocode);
            String smstext = jsonMsg.getString("smstext");
            msgMap.put("smstext", smstext);

            logger.debug("Read " + msgMap + " from queue");

        } catch (Exception e) {
            logger.error("Error : " + e.getMessage(), e);
        }
        return msgMap;
    }
}
