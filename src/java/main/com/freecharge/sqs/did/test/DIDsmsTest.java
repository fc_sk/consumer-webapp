package com.freecharge.sqs.did.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.comm.sms.SMSACLService;
import com.freecharge.common.framework.logging.LoggingFactory;

public class DIDsmsTest {
    private  static Logger              logger = LoggingFactory.getLogger(DIDsmsTest.class.getName());
    
    public static void main(String[] args) throws JSONException, IOException{
        ClassPathXmlApplicationContext ctx = null;
        try {
            logger.info("Starting to run the Job....");
            ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
            logger.info("Spring context initialization done");
            SMSACLService smsACLService = (SMSACLService)ctx.getBean(SMSACLService.getBeanName());
            Map<String, String> mutliSMSmap = new HashMap<String, String>();
            mutliSMSmap.put("919986648913", "Thx for voting! Use promocode (XXXXX1) n get Rs10 off on mobile/dth/datacard recharge on www.freecharge.com once per week, valid on all operators TnC apply.");
            mutliSMSmap.put("919844036073", "Thx for voting! Use promocode (XXXXX2) n get Rs10 off on mobile/dth/datacard recharge on www.freecharge.com once per week, valid on all operators TnC apply.");
            mutliSMSmap.put("919741803053", "Thx for voting! Use promocode (XXXXX3) n get Rs10 off on mobile/dth/datacard recharge on www.freecharge.com once per week, valid on all operators TnC apply.");
            mutliSMSmap.put("919535138614", "Thx for voting! Use promocode (XXXXX3) n get Rs10 off on mobile/dth/datacard recharge on www.freecharge.com once per week, valid on all operators TnC apply.");
            smsACLService.sendMultiSMS(mutliSMSmap, null);
            System.out.println("Spring context initialization done");
        } catch (Exception e) {
           logger.error("Exception running the batch job " , e);
        } finally {
            if(ctx != null) {
                ctx.close();
            }
        }
    }
}
