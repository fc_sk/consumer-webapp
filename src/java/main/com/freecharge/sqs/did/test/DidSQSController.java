package com.freecharge.sqs.did.test;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.freecharge.common.comm.sms.SMSACLService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.sqs.did.DIDSQSService;

@Controller
@RequestMapping("/didsqs")
public class DidSQSController {
    
    @Autowired
    @Qualifier("didSQSService")
    private DIDSQSService didSQSService;
    
    @Autowired
    @Qualifier("smsACLService")
    private SMSACLService smsACLService;
    
    @RequestMapping("/test")
    @NoLogin
    public void test(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        didSQSService.processMessage();
    }
    
    @RequestMapping("/smstest")
    @NoLogin
    public void smstest(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        smsACLService.sendSMS("919986648913", "Thx for voting! Use promocode (456dfy456i) n get Rs10 off on mobile/dth/datacard recharge on www.freecharge.com once per week, valid on all operators TnC apply.", "456dfy456i");
    }
}
