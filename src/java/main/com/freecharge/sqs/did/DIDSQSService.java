package com.freecharge.sqs.did;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.freecharge.common.comm.sms.SMSACLService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.sqs.AmazonSQSProp;
import com.freecharge.sqs.AmazonSQSService;

@Service("didSQSService")
public class DIDSQSService extends AmazonSQSService {

    private Logger           logger = LoggingFactory.getLogger("did.main."+DIDSQSService.class.getName());

    @Autowired
    @Qualifier("didSMSSQSService")
    private DIDSMSSQSService didSMSSQSService;

    @Autowired
    private FreefundService  freefundService;

    @Autowired
    @Qualifier("smsACLService")
    private SMSACLService    smsACLService;

    @Autowired
    public DIDSQSService(@Qualifier("sqsDID") AmazonSQSProp amazonDIDsqs) {
        super(amazonDIDsqs);
    }

    public void processMessage() {
        try {

            Message message = getMessageFromQueue();
            if (message == null) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    logger.error("Error " + message, e);
                }
                logger.debug("No message received from queue.");
                return;
            }
            String mobile = parseMessage(message);

            String promocode = null;
            if (mobile != null) {
                promocode = freefundService.issuePromocodeToMobile(mobile, "did-campaign", 10);
            } else {
                logger.error("Queue message failed to give mobile no. " + message);
                return;
            }
            boolean deleteMsg = true;
            if (promocode != null) {
                String smsMessage = "#DanceItOut Your vote is registered. Here's ur promocode ("+promocode+") worth Rs10/. Redeem it NOW on www.freecharge.com/did4. Watch DID4 Sat-Sun 9pm on Zee TV";
                
                String pushMessage = "{mobile: \"91" + mobile + "\", smstext: \"" + smsMessage + "\", promocode: \"" + promocode + "\"}";
                
                SendMessageResult sendMessageResult = didSMSSQSService.sendMessageToQueue(pushMessage);
                if (sendMessageResult != null && sendMessageResult.getMessageId() != null) {
                    logger.info("SMS message ( "+promocode+" ) enqued for mobile " + mobile);
                } else {
                    logger.error("SMS message ( "+promocode+" ) enque failed for mobile " + mobile);
                    boolean isSMSsent = smsACLService.sendSMS("91" + mobile, smsMessage, promocode);
                    if (!isSMSsent) {
                        deleteMsg = false;
                        logger.error("Even SMS sent failed for mobile " + mobile + " , promocode " + promocode);
                    } else {
                        logger.info("SMS message ( "+promocode+" ) sent for mobile " + mobile);
                    }
                }
            } else {
                logger.debug("Promocode generate failed, hence not assigned to mobile : " + mobile);
            }
            
            if(deleteMsg) {
                logger.info("Deleting message : " + message);
                deleteMessageFromQueue(message);
            } 
            return;
        } catch (AmazonServiceException ase) {
            logger.error(
                    "Caught an AmazonServiceException, which means your request made it to Amazon SQS, but was rejected with an error response for some reason."
                            + "Error Message: "
                            + ase.getMessage()
                            + ",HTTP Status Code: "
                            + ase.getStatusCode()
                            + ",AWS Error Code: "
                            + ase.getErrorCode()
                            + ",Error Type: "
                            + ase.getErrorType()
                            + ",Request ID: " + ase.getRequestId(), ase);
        } catch (AmazonClientException ace) {
            logger.error(
                    "Caught an AmazonClientException, which means the client encountered a serious internal problem while trying to communicate with SQS, such as not being able to access the network."
                            + "Error Message: " + ace.getMessage(), ace);
        }

    }

    public void fillQueueWithSampleData() {
        /*for (int i = 0; i < 100000; i++) {
            sendMessageToQueue("{mobile: \"" + (9988100000L + i) + "\", tstamp: \"217985573" + i + "\"}");
        }*/
        sendMessageToQueue("{mobile: \"9986648913\", tstamp: \"217985573\"}");
        sendMessageToQueue("{mobile: \"9986648913\", tstamp: \"217985573\"}");        
        
    }

    private String parseMessage(Message message) {

        try {
            JSONObject jsonMsg = new JSONObject(message.getBody());
            String mobile = jsonMsg.getString("mobile");
            logger.debug("Read " + mobile + " from queue");
            return mobile;

        } catch (Exception e) {
            logger.error("Error : " + e.getMessage(), e);
        }
        return null;
    }

    public static String getBeanName() {
        return "didSQSService";
    }
}
