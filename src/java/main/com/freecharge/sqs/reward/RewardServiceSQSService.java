package com.freecharge.sqs.reward;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.sqs.AmazonSQSProp;
import com.freecharge.sqs.AmazonSQSService;

@Service("rewardServiceSQSService")
public class RewardServiceSQSService extends AmazonSQSService {

    private Logger        logger = LoggingFactory.getLogger(RewardServiceSQSService.class);

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    public RewardServiceSQSService(@Qualifier("sqsRewardService") AmazonSQSProp amazonDIDsqs) {
        super(amazonDIDsqs);
    }

    public static String getBeanName() {
        return "rewardServiceSQSService";
    }
}
