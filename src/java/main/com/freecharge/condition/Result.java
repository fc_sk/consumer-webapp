package com.freecharge.condition;

public class Result {
    private boolean isSuccess;	
    private String failureMessage; // For debugging and CS team	
    private String detailedFailureMessage; // Visible for customers via e-mail or any other source 
	private String errorCode;

    public boolean isSuccess() {
        return isSuccess;
    }
    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    public String getFailureMessage() {
        return failureMessage;
    }
    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }
    public String getDetailedFailureMessage() {
        return detailedFailureMessage;
    }
    public void setDetailedFailureMessage(String detailedFailureMessage) {
        this.detailedFailureMessage = detailedFailureMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}