package com.freecharge.coupon.web;

public class WebRedemptionRequest {
    private String code;
    private String merchantId;
    private String requestId;
    private String fcRequestId;
    private String location;
    private String city;
    private String state;
    
    
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getMerchantId() {
        return merchantId;
    }
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
    
    public String getRequestId() {
        return requestId;
    }
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    
    public String getFcRequestId() {
        return fcRequestId;
    }
    public void setFcRequestId(String fcRequestId) {
        this.fcRequestId = fcRequestId;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
}
