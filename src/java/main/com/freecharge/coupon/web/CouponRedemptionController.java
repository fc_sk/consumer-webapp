package com.freecharge.coupon.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.APIAuthPolicy;
import com.freecharge.api.coupon.service.exception.ResourceNotFoundException;
import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.api.coupon.service.model.CouponRedemptionResponse;
import com.freecharge.api.coupon.service.model.CouponRedemptionResponse.RedemptionStatus;
import com.freecharge.api.coupon.service.model.MCoupon;
import com.freecharge.api.coupon.service.model.RedemptionErrorInfo;
import com.freecharge.api.coupon.service.model.RedemptionErrorInfo.ErrorCode;
import com.freecharge.api.coupon.service.model.Request.CouponRedemptionRequest;
import com.freecharge.app.domain.dao.IAffiliateProfileDAO;
import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.AffiliateProfile;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.merchant.MerchantService;
import com.freecharge.merchant.bean.Merchant;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.util.ViewConstants;

/**
 * This class exposes different ways of coupon
 * redemption. As of now there are two ways.
 * <ul>
 * <li> Through rest API.</li>
 * <li> Through web form.</li>
 * </ul>
 * 
 * This would be expanded to other modes of 
 * redemption such as SMS, IVR etc.,
 * @author arun
 *
 */
@Controller
public class CouponRedemptionController {
    private Logger logger = LoggingFactory.getLogger(getClass());

    
    @Autowired
    FCProperties fcProperties;
    
    @Autowired
    private MerchantService merchantService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private UsersDAO usersDao;
    
    @Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private IAffiliateProfileDAO affiliateProfileDao;
    
    private final String GET_REDEMPTION_RESULT = "/merchant/redemptionresult.htm";
    private final String GET_REDEMPTION_CHECK_RESULT = "/merchant/redemptioncheckresult.htm";
    private final String REDEMPTION_FORM = "/merchant/redemptionform.htm";
    private final String REDEMPTION_CHECK = "/merchant/checkwebredemption.htm";
    private final String REDEMPTION_HANDLER = "/merchant/handlewebredemption.htm";
    private final String REST_REDEMPTION_API = "/coupons/redeem";
    private final String MERCHANT_LOGIN = "/merchant/login.htm";

    
    private interface JSPConstants {
        public final String REDEMPTION_HANDLER_KEY = "redemptionHandler";
        
        public final String REDEMPTION_REQUEST_OBJECT = "webRedemptionRequest";
        
        public final String COUPON_CODE = "couponCode";
        public final String REDEMPTION_STATUS = "redemptionStatus";
        public final String REDEMPTION_TIME = "redemptionTime";
        public final String REDEMPTION_STATUS_CODE = "redeemStateCode";
        public final String MERCHANT_NAME = "merchantName";
        public final String COUPON_VALUE = "couponValue";
        public final String VALIDITY = "validity";
        public final String CAMPAIGN_ID = "campaignId";
        public final String CAMPAIGN_DETAILS = "campaignDetails";
        public final String TERMS_AND_CONDITIONS = "termsAndConditions";
        public final String REDEEMED_LOCATION = "redeemLocation";
        public final String REDEEMED_CITY = "redeemCity";
        public final String REDEEMED_STATE = "redeemState";
        public final String ERROR_CODE = "errorCode";
        public final String ERROR_MESSAGE = "errorMessage";
        public final String ERROR_REASON = "errorReason";
        
        public final String FC_REQUEST_ID = "fcRequestId";
        public final String REQUEST_ID = "requestId";
        
    }
    
    private interface QueryParameters {
        public final String COUPON_CODE = "code";
        public final String MERCHANT_ID = "merchantId";
        public final String REQUEST_ID = "requestId";
        public final String LOCATION = "location";
        public final String REDEMPTION_STATUS = "status";
        public final String ERROR_CODE = "errorCode";
    }
    
    /**
     * Web redemption form.
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = REDEMPTION_FORM)
    public ModelAndView renderRedeemForm(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (!merchantService.isMerchantLoggedIn()) {
            return new ModelAndView(FCUtil.getRedirectAction(MERCHANT_LOGIN));
        }
        
        WebRedemptionRequest redemptionRequest = createWebRedemptionRequest(request);
        
        model.addAttribute(JSPConstants.REDEMPTION_HANDLER_KEY, REDEMPTION_CHECK);
        model.addAttribute(JSPConstants.REDEMPTION_REQUEST_OBJECT, redemptionRequest);
        
        return new ModelAndView(ViewConstants.COUPON_REDEMPTION_FORM);
    }
    
    /**
     * Check web redemption and redirects to controller
     * that displays redemption status
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = REDEMPTION_CHECK, method = RequestMethod.POST)
    public String checkWebRedemption(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (!merchantService.isMerchantLoggedIn()) {
            return FCUtil.getRedirectAction(MERCHANT_LOGIN);
        }

        CouponRedemptionRequest redemptionRequest = createRedemptionRequest(request);
        
        CouponRedemptionResponse redemptionResponse = couponServiceProxy.checkCoupon(redemptionRequest);
        
        RedemptionStatus redemptionStatus = redemptionResponse.getRedemptionStatus();
        
        RedemptionErrorInfo errorInfo = redemptionResponse.getErrorInfo();
        
        String couponCode = redemptionRequest.getCouponCode();
        String merchantId = redemptionRequest.getMerchantId();
        String requestId = redemptionRequest.getRequestId();
        String location = redemptionRequest.getLocation();
        String errorCode = "";
        
        if (redemptionStatus == RedemptionStatus.Failure) {
            errorCode = errorInfo.getErrorCode().name();
        }
        
        String queryStringForRedemptionGet = QueryParameters.REDEMPTION_STATUS
                + "=" + redemptionStatus.name() + "&"
                + QueryParameters.ERROR_CODE + "=" + errorCode + "&"
                + QueryParameters.COUPON_CODE + "=" + couponCode + "&"
                + QueryParameters.MERCHANT_ID + "=" + merchantId + "&"
                + QueryParameters.REQUEST_ID + "=" + requestId + "&" 
                + QueryParameters.LOCATION + "=" + location;
        
        return FCUtil.getRedirectAction(GET_REDEMPTION_CHECK_RESULT + "?" + queryStringForRedemptionGet);
    }
    
    @RequestMapping(value = GET_REDEMPTION_CHECK_RESULT)
    public String showRedemptionCheckResult(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (!merchantService.isMerchantLoggedIn()) {
            return FCUtil.getRedirectAction(MERCHANT_LOGIN);
        }

        String couponCode = request.getParameter(QueryParameters.COUPON_CODE);
        String merchantId = request.getParameter(QueryParameters.MERCHANT_ID);
        String statusString = request.getParameter(QueryParameters.REDEMPTION_STATUS);
        String errorCodeString = request.getParameter(QueryParameters.ERROR_CODE);
        
        RedemptionStatus redemptionStatus = RedemptionStatus.valueOf(statusString);

        model.addAttribute(JSPConstants.COUPON_CODE, couponCode);
        model.addAttribute(JSPConstants.REDEMPTION_STATUS_CODE, redemptionStatus.name());

        if (redemptionStatus == RedemptionStatus.Redeemable) {
            model.addAttribute(JSPConstants.REDEMPTION_STATUS, "Coupon Redeemable!");

            addRedeemableDetails(model, couponCode, merchantId);
            
        } else if (redemptionStatus == RedemptionStatus.Success) {
            model.addAttribute(JSPConstants.REDEMPTION_STATUS, "Redemption Success!");

            addRedemptionDetails(model, couponCode, merchantId);
            
        } else {
            model.addAttribute(JSPConstants.REDEMPTION_STATUS, "Redemption Failure!");

            ErrorCode errorCode = ErrorCode.valueOf(errorCodeString);
            model.addAttribute(JSPConstants.ERROR_CODE, errorCode.name());
            model.addAttribute(JSPConstants.ERROR_MESSAGE, errorCode.getErrorMessage());
            model.addAttribute(JSPConstants.ERROR_REASON, errorCode.getErrorReason());

            if (errorCode == ErrorCode.ExpiredCouponCode || errorCode == ErrorCode.RedeemedCode) {
                addRedemptionDetails(model, couponCode, merchantId);
            }
        }
        
        WebRedemptionRequest redemptionRequest = createWebRedemptionRequest(request);

        model.addAttribute(JSPConstants.REDEMPTION_HANDLER_KEY, REDEMPTION_HANDLER);
        model.addAttribute(JSPConstants.REDEMPTION_REQUEST_OBJECT, redemptionRequest);

        return ViewConstants.COUPON_REDEMPTION_CHECK_RESULT;
    }
    
    /**
     * Handle web redemption and redirects to controller
     * that displays redemption result
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = REDEMPTION_HANDLER, method = RequestMethod.POST)
    public String handleWebRedemption(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (!merchantService.isMerchantLoggedIn()) {
            return FCUtil.getRedirectAction(MERCHANT_LOGIN);
        }

        CouponRedemptionRequest redemptionRequest = createRedemptionRequest(request);
        
        CouponRedemptionResponse redemptionResponse = couponServiceProxy.redeemCoupon(redemptionRequest);
        
        RedemptionStatus redemptionStatus = redemptionResponse.getRedemptionStatus();
    	metricsClient.recordEvent("CouponRedemption", "WebRedemption", redemptionResponse.getRedemptionStatus().toString());

        RedemptionErrorInfo errorInfo = redemptionResponse.getErrorInfo();
        
        String couponCode = redemptionRequest.getCouponCode();
        String merchantId = redemptionRequest.getMerchantId();
        String requestId = redemptionRequest.getRequestId();
        String fcRequestId = redemptionRequest.getFcRequestId();
        String location = redemptionRequest.getLocation();
        String errorCode = "";
        
        if (redemptionStatus == RedemptionStatus.Failure) {
            errorCode = errorInfo.getErrorCode().name();
        }
        
        String queryStringForRedemptionGet = QueryParameters.REDEMPTION_STATUS
                + "=" + redemptionStatus.name() + "&"
                + QueryParameters.ERROR_CODE + "=" + errorCode + "&"
                + QueryParameters.COUPON_CODE + "=" + couponCode + "&"
                + QueryParameters.MERCHANT_ID + "=" + merchantId+ "&"
                + QueryParameters.REQUEST_ID + "=" + requestId + "&" 
                + QueryParameters.LOCATION + "=" + location;
        
        return FCUtil.getRedirectAction(GET_REDEMPTION_RESULT + "?" + queryStringForRedemptionGet);
    }
    
    /**
     * Display mCoupon details for a given coupon code and merchant ID.
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = GET_REDEMPTION_RESULT)
    public String showRedemptionResult(HttpServletRequest request, HttpServletResponse response, Model model) {
        if (!merchantService.isMerchantLoggedIn()) {
            return FCUtil.getRedirectAction(MERCHANT_LOGIN);
        }

        String couponCode = request.getParameter(QueryParameters.COUPON_CODE);
        String merchantId = request.getParameter(QueryParameters.MERCHANT_ID);
        String statusString = request.getParameter(QueryParameters.REDEMPTION_STATUS);
        String errorCodeString = request.getParameter(QueryParameters.ERROR_CODE);
        
        RedemptionStatus redemptionStatus = RedemptionStatus.valueOf(statusString);

        model.addAttribute(JSPConstants.COUPON_CODE, couponCode);
        model.addAttribute(JSPConstants.REDEMPTION_STATUS_CODE, redemptionStatus.name());

        if (redemptionStatus == RedemptionStatus.Success) {
            model.addAttribute(JSPConstants.REDEMPTION_STATUS, "Redemption Success!");

            addRedemptionDetails(model, couponCode, merchantId);
            
        } else {
            model.addAttribute(JSPConstants.REDEMPTION_STATUS, "Redemption Failure!");

            ErrorCode errorCode = ErrorCode.valueOf(errorCodeString);
            model.addAttribute(JSPConstants.ERROR_CODE, errorCode.name());
            model.addAttribute(JSPConstants.ERROR_MESSAGE, errorCode.getErrorMessage());
            model.addAttribute(JSPConstants.ERROR_REASON, errorCode.getErrorReason());

            if (errorCode == ErrorCode.ExpiredCouponCode || errorCode == ErrorCode.RedeemedCode) {
                addRedemptionDetails(model, couponCode, merchantId);
            }
        }
        
        WebRedemptionRequest redemptionRequest = createWebRedemptionRequest(request);

        model.addAttribute(JSPConstants.REDEMPTION_HANDLER_KEY, REDEMPTION_CHECK);
        model.addAttribute(JSPConstants.REDEMPTION_REQUEST_OBJECT, redemptionRequest);

        return ViewConstants.COUPON_REDEMPTION_RESULT;
    }

    private void addRedemptionDetails(Model model, String couponCode, String merchantId) {
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat redemptionTimeFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
        
        MCoupon mCoupon = couponServiceProxy.getMCoupon(couponCode, merchantId);
        if(mCoupon!=null) {
	        model.addAttribute(JSPConstants.FC_REQUEST_ID, mCoupon.getRedemptionInfo().getFcRequestId());
	        model.addAttribute(JSPConstants.REQUEST_ID, mCoupon.getRedemptionInfo().getRedemptionRequestId());
	        
	        model.addAttribute(JSPConstants.VALIDITY, df.format(mCoupon.getCouponStore().getValidityDate()));
	        
	        model.addAttribute(JSPConstants.COUPON_VALUE, mCoupon.getCouponStore().getCouponValue());
	        model.addAttribute(JSPConstants.MERCHANT_NAME, mCoupon.getMerchantInfo().getMerchantTitle());
	        model.addAttribute(JSPConstants.VALIDITY, df.format(mCoupon.getCouponStore().getValidityDate()));
	        model.addAttribute(JSPConstants.CAMPAIGN_ID, mCoupon.getCouponStore().getCampaignName());
	        model.addAttribute(JSPConstants.CAMPAIGN_DETAILS, mCoupon.getCouponStore().getCouponShortDesc());
	        model.addAttribute(JSPConstants.TERMS_AND_CONDITIONS, mCoupon.getCouponStore().getShortTermsAndConditions());
	        model.addAttribute(JSPConstants.REDEEMED_LOCATION, mCoupon.getRedemptionInfo().getRedeemedLocation());
	        model.addAttribute(JSPConstants.REDEEMED_CITY, mCoupon.getRedemptionInfo().getRedeemedCity());
	        model.addAttribute(JSPConstants.REDEEMED_STATE, mCoupon.getRedemptionInfo().getRedeemedState());
	        model.addAttribute(JSPConstants.REDEMPTION_TIME, redemptionTimeFormat.format(mCoupon.getRedemptionInfo().getRedemptionTime()));
        }
    }
    
    private void addRedeemableDetails(Model model, String couponCode, String merchantId) {
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        MCoupon mCoupon = couponServiceProxy.getMCoupon(couponCode, merchantId);
        if (mCoupon!=null) {
	        model.addAttribute(JSPConstants.COUPON_VALUE, mCoupon.getCouponStore().getCouponValue());
	        model.addAttribute(JSPConstants.MERCHANT_NAME, mCoupon.getMerchantInfo().getMerchantTitle());
	        model.addAttribute(JSPConstants.VALIDITY, df.format(mCoupon.getCouponStore().getValidityDate()));
	        model.addAttribute(JSPConstants.CAMPAIGN_ID, mCoupon.getCouponStore().getCampaignName());
	        model.addAttribute(JSPConstants.CAMPAIGN_DETAILS, mCoupon.getCouponStore().getCouponShortDesc());
	        model.addAttribute(JSPConstants.TERMS_AND_CONDITIONS, mCoupon.getCouponStore().getShortTermsAndConditions());
	        model.addAttribute(JSPConstants.REDEEMED_LOCATION, mCoupon.getRedemptionInfo().getRedeemedLocation());
	        model.addAttribute(JSPConstants.REDEEMED_CITY, mCoupon.getRedemptionInfo().getRedeemedCity());
	        model.addAttribute(JSPConstants.REDEEMED_STATE, mCoupon.getRedemptionInfo().getRedeemedState());
	        model.addAttribute(JSPConstants.REDEMPTION_TIME, "");
        }
    }

    /**
     * This is the coupon redemption REST API. As of now PineLabs
     * has integrated with this.
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = REST_REDEMPTION_API)
    @NoLogin
    public String redeem(HttpServletRequest request, HttpServletResponse response, Model model) {
    	//localhost:8080/coupons/redeem?code=t13&merchantId=342&requestId=11&fcRequestId=11&locationId=in&city=bangalore&state=karnataka
        if (!authorizeRedeemRequest(request)) {
            throw new ResourceNotFoundException();
        }
        try{

            CouponRedemptionRequest redemptionRequest = createRedemptionRequest(request);
            CouponRedemptionResponse redemptionResponse = couponServiceProxy.redeemCoupon(redemptionRequest);
            model.addAttribute("response", redemptionResponse.toMap());
        	metricsClient.recordEvent("CouponRedemption", "RedemptionApi", redemptionResponse.getRedemptionStatus().toString());
        	if(redemptionResponse.getRedemptionStatus() != null && 
        			redemptionResponse.getRedemptionStatus().toString().equals(CouponRedemptionResponse.RedemptionStatus.Failure.toString()) &&
        			redemptionResponse.getErrorInfo() != null && redemptionResponse.getErrorInfo().getErrorCode() != null	){
            	metricsClient.recordEvent("CouponRedemption", "RedemptionApiFailure", 
            			redemptionResponse.getErrorInfo().getErrorCode().toString());        		
        	}

            return "jsonView";
        }catch(Exception e){
        	logger.error("Error redeeming coupon code.", e);
        	metricsClient.recordEvent("CouponRedemption", "RedemptionApi", "Exception");
        	logger.error("An exception is thrown in redeem method in couponredemptioController");
        	throw new  RuntimeException("Invalid arguements passed to redeem coupon");
        }
        
    }
    
    private CouponRedemptionRequest createRedemptionRequest(HttpServletRequest request) {
    	String code = request.getParameter("code").trim();
        String merchantIdStr = request.getParameter("merchantId");
        if(merchantIdStr == null){
        	String email = request.getParameter("email");
        	if(email != null){ 
        		com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getUserByEmailId(email);
        		if(user != null ) {
        			List<AffiliateProfile> affiliateProfiles=affiliateProfileDao.findByUserId(user.getUserId());
        			if(affiliateProfiles!=null && affiliateProfiles.size()<0) {
	        			merchantIdStr = affiliateProfiles.get(0).getAffliateUniqueId();
	        			CouponMerchant merchant = couponServiceProxy.getMerchantFromMerchantId(merchantIdStr);
	        			if(merchant != null) {
	        				merchantIdStr = merchant.getMerchantReference();
	        			}
        			}
        		}
        	}
        }else {
        	merchantIdStr = merchantIdStr.trim();
        }
        
        String requestId = request.getParameter("requestId");
        String fcRequestId = request.getParameter("fcRequestId");
        String location = request.getParameter("location");
        if(location != null){
        	location = location.trim();
        }
        String city = request.getParameter("city");
        if( city != null){
        	city = city.trim();
        }
        String state = request.getParameter("state");
        if(state != null){
        	state = state.trim();
        }
        logger.info("redeemCoupon controller method called with code=" + code +
                ", merchantIdStr=" + merchantIdStr + ", requestId=" + requestId +
                ", location=" + location);

        CouponRedemptionRequest redemptionRequest = new CouponRedemptionRequest();
        redemptionRequest.setCouponCode(code);
        redemptionRequest.setRequestId(requestId);
        redemptionRequest.setFcRequestId(fcRequestId);
        redemptionRequest.setMerchantId(merchantIdStr);
        redemptionRequest.setLocation(location);
        redemptionRequest.setCity(city);
        redemptionRequest.setState(state);
        return redemptionRequest;
    }
    
    private boolean authorizeRedeemRequest(HttpServletRequest request) {
        APIAuthPolicy authPolicy = fcProperties.getAPIAuthPolicy();
        
        switch (authPolicy) {
            case None: return true;
            case IPWhiteList: return isIPWhiteListed(request);
            case AccessKey: throw new RuntimeException("AccessKey policy is not yet implemented");
            case Both: return isIPWhiteListed(request);
            default: throw new RuntimeException("Unknown auth policy");
        }
    }
    
    private boolean isIPWhiteListed(HttpServletRequest request) {
        String requestingIP = request.getRemoteAddr();
        
        if (requestingIP == null || requestingIP.isEmpty()) {
            return false;
        }
        
        List<String> whitelistedIPList = fcProperties.getWhitelistedIPList();
        
        if (whitelistedIPList == null || whitelistedIPList.isEmpty()) {
            return false;
        }
        
        return whitelistedIPList.contains(requestingIP);
    }

    private WebRedemptionRequest createWebRedemptionRequest(HttpServletRequest request) {
        Merchant merchantBean = merchantService.getMerchantFromSession();

        WebRedemptionRequest redemptionRequest = new WebRedemptionRequest();
        redemptionRequest.setCity(merchantBean.getMerchantProfile().getCity());
        redemptionRequest.setMerchantId(merchantBean.getCouponMerchant().getMerchantReference());
        redemptionRequest.setState(merchantBean.getMerchantProfile().getState());
        
        String fcRequestId = request.getParameter("fcRequestId");
        if(StringUtils.isEmpty(fcRequestId)) {
            redemptionRequest.setFcRequestId(RandomStringUtils.randomNumeric(10));
        } else {
            redemptionRequest.setFcRequestId(fcRequestId);
        }
        
        String code = request.getParameter("code");
        String location = request.getParameter("location");
        String requestId = request.getParameter("requestId");
        redemptionRequest.setRequestId(requestId);
        redemptionRequest.setCode(code);
        redemptionRequest.setLocation(location);
        
        return redemptionRequest;
    }
}
