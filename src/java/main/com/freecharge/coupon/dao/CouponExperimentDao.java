package com.freecharge.coupon.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/17/14
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("couponExperimentDao")
public class CouponExperimentDao implements ICouponExperimentDao {

    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertMeasure;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertMeasure = new SimpleJdbcInsert(dataSource).withTableName("coupon_experiments").usingGeneratedKeyColumns("id");
    }

    @Override
    public void addCouponExperimentMeasure(String uid, String imei, Integer measureId) {
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("imei", imei);
        params.put("measure_id", measureId);
        insertMeasure.execute(params);
    }

    @Override
    public void updateCouponExperimentMeasure(String uid, String lookupId, String imei) {
        String sql = "select id from coupon_experiments where imei = :imei order by created_on desc limit 1";
        Map<String, Object> params = new HashMap<>();
        params.put("imei", imei);
        try {
            Integer rowId = this.jt.queryForInt(sql, params);
            String sql1 = "update coupon_experiments set lookup_id = :lookupId where id = :id";
            params.put("lookupId", lookupId);
            params.put("id", rowId);
            this.jt.update(sql1, params);
        }catch (Exception e){
            logger.error(String.format("Could not find entry for uid: %s and imei: %s", uid, imei));
        }
    }

    private static Logger logger = LoggingFactory.getLogger(CouponExperimentDao.class);
}
