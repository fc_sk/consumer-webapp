package com.freecharge.coupon.dao;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/17/14
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ICouponExperimentDao {
    public void addCouponExperimentMeasure(String uid, String imei, Integer measureId);

    public void updateCouponExperimentMeasure(String uid, String lookupId, String imei);
}
