package com.freecharge.coupon.service.exclusive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CouponService;
import com.freecharge.common.encryption.mdfive.MD5;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.timeout.HttpExecutorService;
import com.freecharge.common.timeout.HttpExecutorService.HttpMethodType;
import com.freecharge.common.timeout.HttpResponse;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.UserRepository;
import com.freecharge.payment.services.EasySSLProtocolSocketFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.DBObject;

@Service
public class ExclusiveCouponService {
	
	private static final String MYNRA_API_URL = "https://54.255.17.154/isRegistered";
	private static final Integer MYNTRA_COUPON_ID = 867;
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	private static final String EXCLUSIVE_COUPON_DATA_KEY = "exCoupon";
	private static final String IS_MYNTRA_DATA_KEY = "isMyntra";
	@Autowired
	private HttpExecutorService httpService;
	
	
	@Autowired
	private CouponService couponService;
	
	private Coupon myntraCoupon;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
	@Autowired
	@Qualifier("couponExternalHttpConnectionManager")
	private HttpConnectionManager connectionManager;
	
	@Autowired
    private MetricsClient metricsClient;
	
	@Autowired
	private AppConfigService appConfigService;
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	public void initCoupon(){
		myntraCoupon = couponServiceProxy.getCoupon(MYNTRA_COUPON_ID, FCConstants.CHANNEL_ID_WEB);
	}
	

	public List<Coupon> getExclusiveCouponsForUser(String email, int channelId){
		if (myntraCoupon == null){
			myntraCoupon = couponServiceProxy.getCoupon(MYNTRA_COUPON_ID, FCConstants.CHANNEL_ID_WEB);
		}
		List<Coupon> coupons = new ArrayList<>();
		if (!appConfigService.isExclusiveCouponsEnabled()){
			return coupons;
		}
		try {
			DBObject userObject = getUserDBObject(email);
			if (userObject != null && isKnownMyntraUser(email, userObject)){
				return coupons;
			}
			
			HttpResponse response = getMyntraResponse(email);
			
			if (response != null){
				String responseString = response.getResponse();
				logger.info("Got response: " + responseString + " for email:" +email);
				boolean isMyntraUser = false;
				if (FCUtil.isInteger(responseString)){
					int responseInt = Integer.parseInt(responseString);
					if (responseInt == 1){
						isMyntraUser = true;
					} else if (responseInt == 0){
						isMyntraUser = false;
						coupons.add(myntraCoupon);
					}
					saveInfoInMongoDB(email, isMyntraUser, userObject);
				}
			}
		} catch (ConnectTimeoutException e) {
			logger.warn("Timeout while fetching Myntra exclusive coupon data from Myntra.");
		}	
		return coupons;
	}

	private HttpResponse getMyntraResponse(String email)
			throws ConnectTimeoutException {
		long startTime = System.currentTimeMillis();
		Map<String, String> requestParams = new HashMap<>(); 
		requestParams.put("email_hash", MD5.hashWithoutSalt(email));
		HttpMethod method = httpService.getMethod(MYNRA_API_URL, HttpMethodType.POST, requestParams, null);
		Protocol.registerProtocol("https", 
				new Protocol("https", new EasySSLProtocolSocketFactory(), 443));
		HttpClient client = new HttpClient();
		client.setHttpConnectionManager(connectionManager);
		HttpResponse response = httpService.executeWithTimeOut(client, method, 5000);
		long endTime = System.currentTimeMillis();
		long diff = endTime - startTime;
		metricsClient.recordLatency("ExclusiveCouponService", "getMyntraResponse", diff);
		return response;
	}

	private void saveInfoInMongoDB(String email, boolean isMyntraUser, DBObject userObject) {
		Map<String, Object> userData = new HashMap<String, Object>();
		Map<String, Object> exclusiveData = null;
		if (userObject == null){
			exclusiveData = new HashMap<String, Object>();
		} else{
			exclusiveData = (Map<String, Object>) userObject.get(EXCLUSIVE_COUPON_DATA_KEY);
			if (exclusiveData == null){
				exclusiveData = new HashMap<String, Object>();
			}
		}
		exclusiveData.put(IS_MYNTRA_DATA_KEY, isMyntraUser);
		userData.put(EXCLUSIVE_COUPON_DATA_KEY, exclusiveData);
	}
	
	private DBObject getUserDBObject(String email){
		Users user = userServiceProxy.getUserByEmailId(email);
		if (user == null){
			return null;
		}
		Integer userId = user.getUserId();
		DBObject userObject = userRepository.getUserByUserId(new Long(userId));
		return userObject;
	}
	
	private boolean isKnownMyntraUser(String email, DBObject userObject) {
		boolean isKnownMyntraUser = false;
		Map<String, Object> exclusiveData = null;
		if (userObject != null){
			exclusiveData = (Map<String, Object>) userObject.get(EXCLUSIVE_COUPON_DATA_KEY);
			if (exclusiveData != null){
				Boolean isMyntra = (Boolean) exclusiveData.get(IS_MYNTRA_DATA_KEY);
				if (isMyntra != null && isMyntra){
					isKnownMyntraUser = true;
				}
			}
		}
		return isKnownMyntraUser;
	}
	
	
}
