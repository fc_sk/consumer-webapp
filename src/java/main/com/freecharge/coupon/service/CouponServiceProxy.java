package com.freecharge.coupon.service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.common.entity.CouponStoreLocation;
import com.freecharge.api.coupon.common.entity.LocData;
import com.freecharge.api.coupon.service.exception.CouponNotAvailableException;
import com.freecharge.api.coupon.service.model.*;
import com.freecharge.api.coupon.service.model.Request.CouponFraudCheckRequest;
import com.freecharge.api.coupon.service.model.Request.CouponRedemptionRequest;
import com.freecharge.api.coupon.service.web.model.*;
import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;

@Service
public class CouponServiceProxy implements ICouponService {
	private Logger logger = LoggingFactory.getLogger(getClass());
	private ICouponService remoteCouponService;
	
	@PostConstruct
	public void init(){
		try{
			ApplicationContext applicationContextClient = new ClassPathXmlApplicationContext("classpath:coupon-service-client-beans.xml");
			remoteCouponService = (ICouponService) applicationContextClient.getBean("couponApiServiceClient");
		} catch (Exception e){
			logger.error("Error initializing coupon service client context. "
					+ "Make sure coupon-service war is deployed and reachable else turn of coupon remoting in app_config.", e);
		}
	}	
	
	private ICouponService getActiveCouponService(){
		return remoteCouponService;
	}
	
	private ICouponService getActiveHCouponFulfillmentService(){
		return remoteCouponService;
	}
	
	private ICouponService getActiveCouponBlockService(){
		return remoteCouponService;
	}

	@Override
	public CouponData getFilteredCoupons(String startStr, String countStr,
			String categoryIdsStr, String minValueStr, String maxValueStr, String minPriceStr, String maxPriceStr, 
			int channel, CouponFraudCheckRequest fraudCheckRequestData, boolean includeTnC, boolean isPromoted,
            String city, int appVersion, int txnAmount) throws InvalidParameterException {
		CouponData couponData=null;
		try {
			couponData = getActiveCouponService().getFilteredCoupons(startStr, countStr, categoryIdsStr, minValueStr, 
					maxValueStr, minPriceStr, maxPriceStr, channel, fraudCheckRequestData, includeTnC, isPromoted, city,
					appVersion, txnAmount);
		} catch (Exception e) {
			logger.error("Error in  getFilteredCoupons",e);
		}
		return couponData;
	}

	@Override
	public List<CouponHistory> getCouponHistoryList(List<String> orderIdList)
			throws InvalidParameterException {
		if (FCUtil.isEmpty(orderIdList)) {
			return null;
		}
		List<CouponHistory> couponHistories = null;
		try {
			couponHistories = getActiveCouponService().getCouponHistoryList(orderIdList);
		} catch (Exception e) {
			logger.error("Error in  getCouponHistoryList",e);
		}
		return couponHistories;
	}

	public List<CouponHistory> getCouponHistoryListByOrderId(String arg0)
			throws InvalidParameterException {
		return null;
	}
	
	@Override
	public List<CouponHistory> getCouponHistoryListByUserId(Integer userId) throws InvalidParameterException {
		List<CouponHistory> couponHistories = null;
		try {
			couponHistories = getActiveCouponService().getCouponHistoryListByUserId(userId);
		} catch (Exception e) {
			logger.error("Error in  getCouponHistoryListByUserId",e);
		}
		return couponHistories;
	}
	@Override
	public List<CouponHistoryMaster> getCouponHistoryMasterListByUserId(Integer userId) throws InvalidParameterException {
		List<CouponHistoryMaster> couponHistories = null;
		try {
			couponHistories = getActiveCouponService().getCouponHistoryMasterListByUserId(userId);
		} catch (Exception e) {
			logger.error("Error in  getCouponHistoryMasterListByUserId",e);
		}
		return couponHistories;
	}
	
	@Override
	public CouponHistory getCouponHistoryById(Integer id) throws InvalidParameterException {
		CouponHistory couponHistory = null;
		try {
			couponHistory = getActiveCouponService().getCouponHistoryById(id);
		} catch (Exception e) {
			logger.error("Error in  getCouponHistoryById",e);
		}
		return couponHistory;
	}
	
	public CouponData getFilteredCouponsForDisplay(String startStr, String countStr,
			String categoryIdsStr, String minValueStr, String maxValueStr, String minPriceStr, String maxPriceStr, 
			int channel, CouponFraudCheckRequest fraudCheckRequestData, boolean includeTnC, boolean isPromoted,
            String city, int appVersion, int txnAmount) throws InvalidParameterException {
		CouponData couponData = null;
		try {
			couponData = getActiveCouponService().getFilteredCouponsForDisplay(startStr, countStr, categoryIdsStr, minValueStr, 
					maxValueStr, minPriceStr, maxPriceStr, channel, fraudCheckRequestData, includeTnC, isPromoted, city, appVersion,
					txnAmount);
		} catch (Exception e) {
			logger.error("Error in getFilteredCouponsForDisplay",e);
		}
		return couponData;
	}
	
	public CouponData getFilteredHCouponsForDisplay(String startStr, String countStr,
			String categoryIdsStr, String minValueStr, String maxValueStr, String minPriceStr, String maxPriceStr, 
			int channel, CouponFraudCheckRequest fraudCheckRequestData, boolean includeTnC, boolean isPromoted,
            String city, int appVersion, String latStr, String longStr, boolean isInTxn, int txnAmount) throws InvalidParameterException {
		CouponData couponData = null;
		try {
			couponData = getActiveCouponService().getFilteredHCouponsForDisplay(startStr, countStr, categoryIdsStr, minValueStr, 
					maxValueStr, minPriceStr, maxPriceStr, channel, fraudCheckRequestData, includeTnC, isPromoted, city, appVersion, latStr, longStr, isInTxn, txnAmount);
		} catch (Exception e) {
			logger.error("Error in getFilteredHCouponsForDisplay",e);
		}
		return couponData;
	}

	@Override
	public List<Coupon> getCouponsForKeySearch(String searchKey, String startStr, String countStr,
			String categoryIdsStr, String minValueStr, String maxValueStr, String minPriceStr, String maxPriceStr, 
			int channel, CouponFraudCheckRequest fraudCheckRequestData, boolean includeTnC, boolean isPromoted,
            String city, int appVersion) throws InvalidParameterException {
		List<Coupon> coupons = null;
		try {
			coupons = getActiveCouponService().getCouponsForKeySearch(searchKey, startStr, countStr, categoryIdsStr, minValueStr, 
					maxValueStr, minPriceStr, maxPriceStr, channel, fraudCheckRequestData, includeTnC, isPromoted, city, appVersion);
		} catch (Exception e) {
			logger.error("Error in getCouponsForKeySearch",e);
		} 
		return coupons;
	}

	@Override
	public CouponStoreLocation getCouponLocationById(Integer couponId)
			throws InvalidParameterException {
		CouponStoreLocation couponStoreLocation = null;
		try {
			couponStoreLocation = getActiveCouponService().getCouponLocationById(couponId);
		} catch (Exception e) {
			logger.error("Error in getCouponLocationById",e);
		}
		return couponStoreLocation;
	}

	@Override
	public Map<Integer, BlockedCoupon> doHCouponFulfilment(List<CouponCartItem> coupons,String orderId,Integer userId)
			throws InvalidParameterException, CouponNotAvailableException {
		Map<Integer, BlockedCoupon> hcouponFulfilmentMap = null;
		try {
			hcouponFulfilmentMap = getActiveHCouponFulfillmentService().doHCouponFulfilment(coupons,orderId,userId);
		} catch (Exception e) {
			logger.error("Error in doHCouponFulfilment",e);
		}
		return hcouponFulfilmentMap;
	}

	@Override
	public boolean sendHCouponSuccessNotifications(String arg0)
			throws InvalidParameterException {
		boolean status = false;
		try {
			status = getActiveHCouponFulfillmentService().sendHCouponSuccessNotifications(arg0);
		} catch (Exception e) {
			logger.error("Error in sendHCouponSuccessNotifications",e);
		} 
		return status;
	}

	@Override
	public Integer addCouponLocData(Integer arg0, LocData arg1) {
		return null;
	}

	@Override
	public CouponStoreLocation getCouponLocations(Integer arg0) {
		return null;
	}

	@Override
	public void updateCouponLocData(Integer arg0, LocData arg1) {
	}

	@Override
	public Integer getTotalRewardCouponWorthValue() {
		Integer totalValue = null;
		try {
			totalValue = getActiveCouponService().getTotalRewardCouponWorthValue();
		} catch (Exception e) {
			logger.error("Error in getTotalRewardCouponWorthValue",e);
		}
		return totalValue;
	}

	@Override
	public List<Coupon> getFixedRewardCoupon(String orderId) {
		List<Coupon> coupons = null;
		try {
			coupons = getActiveCouponService().getFixedRewardCoupon(orderId);
		} catch (Exception e) {
			logger.error("Error in getFixedRewardCoupon",e);
		}
		return coupons;
	}

	@Override
	public Boolean getFixedRewardCouponsEligible(String orderId) {
		boolean status = false;
		try {
			status = getActiveCouponService().getFixedRewardCouponsEligible(orderId);
		} catch (Exception e) {
			logger.error("Error in getFixedRewardCouponsEligible",e);
		} 
		return status;
	}

	@Override
	public BlockedCouponResponse blockAllCartCouponsForFreechargeOrder(List<CouponCartItem> couponCartItems,String orderId, int couponScalingFactor,Integer userId) {
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = getActiveCouponBlockService().blockAllCartCouponsForFreechargeOrder(couponCartItems,orderId, couponScalingFactor,userId);
		} catch (Exception e) {
			logger.error("Error in blockAllCartCouponsForFreechargeOrder",e);
		} 
		return blockedCouponResponse;
	}

	@Override
	public BlockedCouponResponse blockCouponCodesForUser(Long userId, List<BlockCouponRequest> blockRequests) {
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = getActiveCouponBlockService().blockCouponCodesForUser(userId, blockRequests);
		} catch (Exception e) {
			logger.error("Error in blockAllCartCouponsForFreechargeOrder",e);
		} 
		return blockedCouponResponse;
	}

	@Override
	public BlockedCouponResponse blockCouponForOrder(BlockCouponRequest blockCouponRequest,Integer userId) {
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = getActiveCouponBlockService().blockCouponForOrder(blockCouponRequest,userId);
		} catch (Exception e) {
			logger.error("Error in blockCouponForOrder",e);
		}
		return blockedCouponResponse;
	}

	@Override
	public BlockedCouponResponse blockMultipleCouponsForOrder(List<BlockCouponRequest> blockCouponRequest,Integer userId) {
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = getActiveCouponBlockService().blockMultipleCouponsForOrder(blockCouponRequest,userId);
		} catch (Exception e) {
			logger.error("Error in blockMultipleCouponsForOrder",e);
		}
		return blockedCouponResponse;
	}

	@Override
	public BlockedCouponResponse blockNewCouponForOrder(BlockCouponRequest blockCouponRequest,Integer userId) {
		BlockedCouponResponse blockedCouponResponse = null;
		try {
			blockedCouponResponse = getActiveCouponBlockService().blockNewCouponForOrder(blockCouponRequest,userId);
		} catch (Exception e) {
			logger.error("Error in blockNewCouponForOrder",e);
		}
		return blockedCouponResponse;
	}

	@Override
	public List<BlockedCoupon> getBlockedCouponListForOrder(String orderId) {
		List<BlockedCoupon> blockedCoupons = null;
		try {
			blockedCoupons = getActiveCouponBlockService().getBlockedCouponListForOrder(orderId);
		} catch (Exception e) {
			logger.error("Error in getBlockedCouponListForOrder",e);
		} 
		return blockedCoupons;
	}

	@Override
	public List<CouponImage> getCouponImages(Integer couponId, String channel, String resolutions) {
		List<CouponImage> couponImages = null;
		try {
			couponImages = getActiveCouponService().getCouponImages(couponId, channel, resolutions);
		} catch (Exception e) {
			logger.error("Error in getCouponImages",e); 
		}
		return couponImages;
		
	}
	
	@Override
	public Map<Integer, List<CouponImage>> getCouponImagesInMemAppConfigService(){
		Map<Integer, List<CouponImage>> couponImageMap = null;
		try {
			couponImageMap = getActiveCouponService().getCouponImagesInMemAppConfigService();
		} catch (Exception e) {
			logger.error("Error in getCouponImagesInMemAppConfigService",e); 
		}
		return couponImageMap;
	}

	@Override
	public Boolean isValidChannel(int channel){
		boolean status = false; 
		try {
			status = getActiveCouponService().isValidChannel(channel);
		} catch (Exception e) {
			logger.error("Error in isValidChannel",e);
		}
		return status;
	}
	
	@Override
	public String getCommaSeparatedCityNamesWithCouponStore(CouponStore couponStore){
		String cityNames = null;
		try {
			cityNames = getActiveCouponService().getCommaSeparatedCityNamesWithCouponStore(couponStore);
		} catch(Exception e) {
			logger.error("Error in getCommaSeparatedCityNamesWithCouponStore",e);
		}
		return cityNames;
	}
	
	@Override
	public String getCommaSeparatedCityNamesWithCouponStoreId(int couponStoreId){
		String cityNames = null;
		try {
			cityNames = getActiveCouponService().getCommaSeparatedCityNamesWithCouponStoreId(couponStoreId);
		} catch(Exception e) {
			logger.error("Error in getCommaSeparatedCityNamesWithCouponStoreId",e);
		}
		return cityNames;
	}
	
	@Override
	public Boolean createCoupon(CouponStore couponStore){
		boolean status = false; 
		try {
			status = getActiveCouponService().createCoupon(couponStore);
		} catch (Exception e) {
			logger.error("Error in createCoupon",e);
		}
		return status;
	}
	
	@Override
	public Boolean createCouponInventory(CouponInventory couponInventory){
		boolean status = false;
		try {
			status = getActiveCouponService().createCouponInventory(couponInventory);
		} catch (Exception e) {
			logger.error("Error in createCouponInventory",e);
		}
		return status;
	}
	
	@Override
	public Boolean deactivateCoupon(Integer couponStoreId){
		boolean status = false;
		try {
			status = getActiveCouponService().deactivateCoupon(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in deactivateCoupon",e);
		} 
		return status;
	}
	
	@Override
	public Boolean activateCoupon(Integer couponStoreId){
		boolean status = false;
		try {
			status = getActiveCouponService().activateCoupon(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in activateCoupon",e);
		} 
		return status;
	}
	
	@Override
	public List<CouponCode> getAllCouponCodes(Integer couponInventoryId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getAllCouponCodes(couponInventoryId);
		} catch(Exception e) {
			logger.error("Error in getAllCouponCodes",e);
		}
		return couponCodes;
	}
	
	@Override
	public List<CouponCode> getUnusedCouponCodes(Integer couponInventoryId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getUnusedCouponCodes(couponInventoryId);
		} catch(Exception e) {
			logger.error("Error in getUnusedCouponCodes",e);
		}
		return couponCodes;
	}
	
	@Override
	public CouponInventory getCouponInventoryByCouponStoreId(Integer couponStoreId){
		CouponInventory couponInventory = null;
		try {
			couponInventory = getActiveCouponService().getCouponInventoryByCouponStoreId(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponInventoryByCouponStoreId",e);
		}
		return couponInventory;
	}
	
	@Override
	public Boolean updateInventory(CouponInventory couponInventory){
		boolean status = false;
		try {
			status = getActiveCouponService().updateInventory(couponInventory);
		} catch (Exception e) {
			logger.error("Error in deactivateCoupon",e);
		} 
		return status;
	}
	
	@Override
	public Boolean updateCoupon(CouponStore couponStore){
		boolean status = false;
		try {
			status = getActiveCouponService().updateCoupon(couponStore);
		} catch (Exception e) {
			logger.error("Error in deactivateCoupon",e);
		} 
		return status;
	}
	
	@Override
	public void upload(List<CouponCode> newCodes){
		try {
			getActiveCouponService().upload(newCodes);
		} catch (Exception e) {
			logger.error("Error in upload",e);
		}
	}
	@Override
	public void updateCouponCount(int couponInventoryId, int quantity){
		try {
			getActiveCouponService().updateCouponCount(couponInventoryId,quantity);
		} catch (Exception e) {
			logger.error("Error in updateCouponCount",e);
		}
	}
	
	@Override
	public void block(List<CouponCode> codesToBlock,String reasonToBlock){
		try {
			getActiveCouponService().block(codesToBlock,reasonToBlock);
		} catch (Exception e) {
			logger.error("Error in block",e);
		}
	}
	
	@Override
	public Integer updateCouponInventoryCount(int couponInventoryId, int size){
		Integer rowCount = 0;
		try {
			rowCount = getActiveCouponService().updateCouponInventoryCount(couponInventoryId,size);
		} catch(Exception e) {
			logger.error("Error in updateCouponInventoryCount",e);
		}
		return rowCount;
	}
	
	@Override
	public String getCityName(String ip){
		String cityName = null;
		try {
			cityName = getActiveCouponService().getCityName(ip);
		} catch (Exception e) {
			logger.error("Error in getCityName",e);
		}
		return cityName;
	}
	
	@Override
	public Map<Integer, List<CouponHistory>> getCouponHistoryMap(String orderId){
		Map<Integer, List<CouponHistory>> couponHistoryMap = null;
		try {
			couponHistoryMap = getActiveCouponService().getCouponHistoryMap(orderId);
		} catch (Exception e) {
			logger.error("Error in getCouponHistoryMap",e);
		}
		return couponHistoryMap;
	}
	
	@Override
	public Map<String, List<CouponVO>> getAllActiveCouponsOrdered(int channelId){
		Map<String, List<CouponVO>> couponOrdered = null;
		try {
			couponOrdered = getActiveCouponService().getAllActiveCouponsOrdered(channelId);
		} catch(Exception e) {
			logger.error("Error in getAllActiveCouponsOrdered",e);
		}
		return couponOrdered;
	}
	
	@Override
	public void setAllActiveCouponsOrdered(Map<String, List<CouponVO>> allActiveCoupons, int channelId){
		try {
			getActiveCouponService().setAllActiveCouponsOrdered(allActiveCoupons,channelId);
		} catch (Exception e) {
			logger.error("Error in setAllActiveCouponsOrdered",e);
		}
	}
	
	@Override
	public Set<Integer> getInvalidCouponIds(String couponCookie, String ipAddress, String emailId,
			String profileNo, String imeiNo, int channelId, String userId){
		Set<Integer> invalidCouponIds = new HashSet<>();
		try {
			invalidCouponIds = getActiveCouponService().getInvalidCouponIds(couponCookie,ipAddress,emailId,profileNo,imeiNo,channelId,userId);
		} catch(Exception e) {
			logger.error("Error in getInvalidCouponIds",e);
		}
		return invalidCouponIds;
	}
	
	@Override
	public Set<Integer> getInvalidCouponIds(int channelId, int appVersion){
		Set<Integer> invalidCouponIds = new HashSet<>();
		try {
			invalidCouponIds = getActiveCouponService().getInvalidCouponIds(channelId,appVersion);
		} catch(Exception e) {
			logger.error("Error in getInvalidCouponIds",e);
		}
		return invalidCouponIds;
	}
	
	@Override
	public List<VoucherCat> getVoucherCategories(){
		List<VoucherCat> voucherCats = null;
		try {
			voucherCats = getActiveCouponService().getVoucherCategories();
		} catch(Exception e) {
			logger.error("Error in getVoucherCategories",e);
		}
		return voucherCats;
	}
	
	@Override
	public List<CouponStore> getAvailableCouponsOrdered(){
		List<CouponStore> coupons = null;
		try {
			coupons = getActiveCouponService().getAvailableCouponsOrdered();
		} catch(Exception e) {
			logger.error("Error in getAvailableCouponsOrdered",e);
		}
		return coupons;
	}
	
	@Override
	public List<VoucherCat> getAllVoucherCategoriesOrdered(){
		List<VoucherCat> voucherCats = null;
		try {
			voucherCats = getActiveCouponService().getAllVoucherCategoriesOrdered();
		} catch(Exception e) {
			logger.error("Error in getAllVoucherCategoriesOrdered",e);
		}
		return voucherCats;
	}
	
	@Override
	public void setVoucherCategories(List<VoucherCat> voucherCats){
		try {
			getActiveCouponService().setVoucherCategories(voucherCats);
		} catch(Exception e) {
			logger.error("Error in setVoucherCategories",e);
		}
	}
	
	@Override
	public List<CouponStore> getAvailableCouponsForChannel(int channel,String type){
		List<CouponStore> coupons = null;
		try {
			coupons = getActiveCouponService().getAvailableCouponsForChannel(channel,type);
		} catch(Exception e) {
			logger.error("Error in getAvailableCouponsForChannel",e);
		}
		return coupons;
	}
	
	@Override
	public void setAvailableCouponsOrdered(List<CouponStore> couponStores){
		try {
			getActiveCouponService().setAvailableCouponsOrdered(couponStores);
		} catch(Exception e) {
			logger.error("Error in setAvailableCouponsOrdered",e);
		}
	}
	
	@Override
	public List<CouponStore> getAvailableCoupons(){
		List<CouponStore> coupons = null;
		try{
			coupons = getActiveCouponService().getAvailableCoupons();
		} catch(Exception e) {
			logger.error("Error in getAvailableCoupons",e);
		}
		return coupons;
	}
	
	@Override
	public Map<Integer, Integer> getCouponStoreInferredRankMap(){
		Map<Integer, Integer> couponInferredRankMap = null;
		try {
			couponInferredRankMap = getActiveCouponService().getCouponStoreInferredRankMap();
		} catch (Exception e) {
			logger.error("Error in getCouponStoreInferredRankMap",e);
		}
		return couponInferredRankMap;
	}
	
	@Override
	public Map<Integer, Integer> getCouponStoreManualRankMap(){
		Map<Integer, Integer> couponManualRankMap = null;
		try {
			couponManualRankMap = getActiveCouponService().getCouponStoreManualRankMap();
		} catch (Exception e) {
			logger.error("Error in getCouponStoreManualRankMap",e);
		}
		return couponManualRankMap;
	}
	
	@Override
	public void updateCache(){
		try {
			getActiveCouponService().updateCache();
		} catch(Exception e) {
			logger.error("Error in updateCache",e);
		}
	}
	
	@Override
	public List<Integer> getAvailableCouponIds(){
		List<Integer> couponIds = null;
		try{
			couponIds = getActiveCouponService().getAvailableCouponIds();
		} catch(Exception e) {
			logger.error("Error in getAvailableCouponIds");
		}
		return couponIds;
	}
	
	@Override
	public void setAvailableCouponIds(List<Integer> couponStores){
		try {
			getActiveCouponService().setAvailableCouponIds(couponStores);
		} catch(Exception e) {
			logger.error("Error in setAvailableCouponIds");
		}
	}
	
	@Override
	public CouponMerchant getCouponMerchant(Integer couponMerchantId){
		CouponMerchant couponMerchant = null;
		try {
			couponMerchant = getActiveCouponService().getCouponMerchant(couponMerchantId);
		} catch(Exception e) {
			logger.error("Error in getCouponMerchant");
		} 
		return couponMerchant;
	}
	
	@Override
    public void createRedemption(String couponCode, Integer couponStoreId, String merchantReference){
		try {
			getActiveCouponService().createRedemption(couponCode,couponStoreId,merchantReference);
		} catch(Exception e) {
			logger.error("Error in createRedemption");
		} 
	}
	
	@Override
	public String getGenericCoupons() {
		String genericCode = null;
		try {
			genericCode = getActiveCouponService().getGenericCoupons();
		} catch (Exception e) {
			logger.error("Error in getGenericCoupons",e);
		} 
		return genericCode;
	}
	
	@Override
	public Boolean blockAndGetCouponCodesNEW(Integer couponInventoryId, Integer quantity, String orderId) {
		boolean status = false;
		try {
			status = getActiveCouponService().blockAndGetCouponCodesNEW(couponInventoryId,quantity,orderId);
		} catch (Exception e) {
			logger.error("Error in blockAndGetCouponCodesNEW",e);
		} 
		return status;
	}
	
	@Override
	public List<CouponCode> getCouponCodesFromCouponInventoryAndOrderId(Integer couponInventoryId, String orderId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getCouponCodesFromCouponInventoryAndOrderId(couponInventoryId,orderId);
		} catch (Exception e) {
			logger.error("Error in getCouponCodesFromCouponInventoryAndOrderId",e);
		} 
		return couponCodes;
	}
	
	@Override
	public List<Map<String, Object>> getCouponStoreAndInventoryDetails(Integer couponStoreId){
		List<Map<String, Object>> couponMaps = null;
		try {
			couponMaps = getActiveCouponService().getCouponStoreAndInventoryDetails(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponStoreAndInventoryDetails",e);
		}
		return couponMaps;
	}
	
	@Override
	public List<CouponCode> getCouponCodesFromCouponInventoryIdForNonUniqueCoupons(Integer couponInventoryId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getCouponCodesFromCouponInventoryIdForNonUniqueCoupons(couponInventoryId);
		} catch (Exception e) {
			logger.error("Error in getCouponCodesFromCouponInventoryIdForNonUniqueCoupons",e);
		} 
		return couponCodes;
	}
	
	@Override
	public Boolean resetCouponInventory(Integer couponStoreId, Integer quantity, String orderId,
			boolean isComplimentary){
		boolean status = false;
		try {
			status = getActiveCouponService().resetCouponInventory(couponStoreId, quantity, orderId, isComplimentary);
		} catch (Exception e) {
			logger.error("Error in resetCouponInventory",e);
		} 
		return status;
	}
	
	@Override
	public List<CouponInventory> getCouponInventoriesFromCouponStore(Integer couponStoreId){
		List<CouponInventory> couponInventories = null;
		try {
			couponInventories = getActiveCouponService().getCouponInventoriesFromCouponStore(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponInventoriesFromCouponStore",e);
		}
		return couponInventories;
	}
	
	@Override
	public Integer releaseCouponInventoryForUniqueCouponsAndReturnCount(Integer couponInventoryId,
			String orderId){
		Integer count = 0;
		try {
			count = getActiveCouponService().releaseCouponInventoryForUniqueCouponsAndReturnCount(couponInventoryId,orderId);
		} catch (Exception e) {
			logger.error("Error in getCouponInventoriesFromCouponStore",e);
		}
		return count;
	}
	
	@Override
	public Boolean resetCouponInventoryCount(Integer couponInventoryId, Integer quantity){
		boolean status = false;
		try {
			status = getActiveCouponService().resetCouponInventoryCount(couponInventoryId, quantity);
		} catch (Exception e) {
			logger.error("Error in resetCouponInventoryCount",e);
		} 
		return status;
	}
	
	@Override
	public List<VoucherCat> getAllActiveVaucherCatCouponMemCache(){
		List<VoucherCat> coupons = null;
		try {
			coupons = getActiveCouponService().getAllActiveVaucherCatCouponMemCache();
		} catch (Exception e) {
			logger.error("Error in getAllActiveVaucherCatCouponMemCache",e);
		}
		return coupons;
	}
	
	@Override
	public List<VoucherCat> getAllActiveVaucherCatDao() {
		List<VoucherCat> coupons = null;
		try {
			coupons = getActiveCouponService().getAllActiveVaucherCatDao();
		} catch (Exception e) {
			logger.error("Error in getAllActiveVaucherCatDao",e);
		}
		return coupons;
	}
	
	@Override
	public void setAllActiveVaucherCat(List<VoucherCat> voucherCats){
		try {
			getActiveCouponService().setAllActiveVaucherCat(voucherCats);
		} catch(Exception e) {
			logger.error("Error in setAllActiveVaucherCat",e);
		}
	}
	
	@Override
	public List<CouponInventory> getCouponInventoryWhichReachedThresholdValue(int additionalThresholdvalue){
		List<CouponInventory> couponInvetories = null;
		try {
			couponInvetories = getActiveCouponService().getCouponInventoryWhichReachedThresholdValue(additionalThresholdvalue);
		} catch(Exception e) {
			logger.error("Error in getCouponInventoryWhichReachedThresholdValue",e);
		}
		return couponInvetories;
	}
	
	@Override
	public CouponStore getCouponStore(Integer couponStoreId){
		CouponStore couponStore = null;
		try {
			couponStore = getActiveCouponService().getCouponStore(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponStore",e);
		}
		
		return couponStore;
	}
	
	@Override
	public Boolean updateOrderIdForReplacedCoupons(String orderId, String newOrderId , Integer couponInventoryId, Integer quantity){
		boolean status = false;
		try {
			status = getActiveCouponService().updateOrderIdForReplacedCoupons(orderId,newOrderId,couponInventoryId,quantity);
		} catch (Exception e) {
			logger.error("Error in updateOrderIdForReplacedCoupons",e);
		} 
		return status;
	}
	
	@Override
	public List<Map<String, Object>> getCouponDetailsFromCouponIdsAndCouponType(List<Integer> couponIds, String couponType){
		List<Map<String, Object>> couponMaps = null;
		try {
			couponMaps = getActiveCouponService().getCouponDetailsFromCouponIdsAndCouponType(couponIds, couponType);
		} catch (Exception e) {
			logger.error("Error in getCouponDetailsFromCouponIdsAndCouponType",e);
		}
		return couponMaps;
	}
	
	@Override
	public List<Map<String, Object>> getCouponDetailsFromCouponIds(List<Integer> couponIds) {
		List<Map<String, Object>> couponMaps = null;
		try {
			couponMaps = getActiveCouponService().getCouponDetailsFromCouponIds(couponIds);
		} catch (Exception e) {
			logger.error("Error in getCouponDetailsFromCouponIds",e);
		}
		return couponMaps;
	}
	
	@Override
	public String getHowRedeemOffer(Integer CouponStoreId)
	{
		return getActiveCouponService().getHowRedeemOffer(CouponStoreId);
	}
	
	@Override
	public List<Integer> getCouponHistoryStatus(String orderId)
	{
		return getActiveCouponService().getCouponHistoryStatus(orderId);
	}
	
	@Override
	public List<Map<String, Object>> getReissuedCouponMaps(String orderId){
		List<Map<String, Object>> couponMaps = null;
		try {
			couponMaps = getActiveCouponService().getReissuedCouponMaps(orderId);
		} catch (Exception e) {
			logger.error("Error in getReissuedCouponMaps",e);
		}
		return couponMaps;
	}
	
	@Override
	public List<CouponStore> getAvailableUniqueCoupons(){
		List<CouponStore> coupons = null;
		try {
			coupons = getActiveCouponService().getAvailableUniqueCoupons();
		} catch (Exception e) {
			logger.error("Error in getAvailableUniqueCoupons",e);
		}
		return coupons;
	}
	
	@Override
	public Integer setmCoupon(Integer couponStoreId, boolean isMCoupon){
		Integer rowCount = 0;
		try {
			rowCount = getActiveCouponService().setmCoupon(couponStoreId, isMCoupon);
		} catch(Exception e) {
			logger.error("Error in setmCoupon",e);
		}
		return rowCount;
	}
	
	@Override
	public CouponStore getActiveCouponByCouponStoreId(int couponStoreId){
		CouponStore coupon = null;
		try {
			coupon = getActiveCouponService().getActiveCouponByCouponStoreId(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getActiveCouponByCouponStoreId",e);
		}
		return coupon;
	}
	
	@Override
	public CouponStore getCouponStoreByCrosssellId(Integer crosssellId){
		CouponStore coupon = null;
		try {
			coupon =  getActiveCouponService().getCouponStoreByCrosssellId(crosssellId);
		} catch (Exception e) {
			logger.error("Error in getCouponStoreByCrosssellId",e);
		}
		return coupon;
	}
	
	@Override
	public List<CouponCode> getCouponCodesFromCouponStoreAndOrderId(Integer couponStoreId, String orderId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getCouponCodesFromCouponStoreAndOrderId(couponStoreId,orderId);
		} catch (Exception e) {
			logger.error("Error in getCouponCodesFromCouponStoreAndOrderId",e);
		}
		return couponCodes;
	}
	
	@Override
	public List<CouponCode> getCouponCodesFromCouponStoreIdForNonUniqueCoupons(Integer couponStoreId){
		List<CouponCode> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getCouponCodesFromCouponStoreIdForNonUniqueCoupons(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponCodesFromCouponStoreIdForNonUniqueCoupons",e);
		}
		return couponCodes;
	}
	
	@Override
	public Integer getMissionIdByCouponId(int couponStoreId){
		Integer missionId = null;
		try {
			missionId = getActiveCouponService().getMissionIdByCouponId(couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getMissionIdByCouponId",e);
		}
		return missionId;
	}
	
	@Override
	public CouponRedemptionResponse redeemCoupon(CouponRedemptionRequest request){
		CouponRedemptionResponse couponRedemptionResponse = null;
		try {
			couponRedemptionResponse = getActiveCouponService().redeemCoupon(request);
		} catch (Exception e) {
			logger.error("Error in redeemCoupon",e);
		}
		return couponRedemptionResponse;
	}
	
	@Override
	public CouponRedemptionResponse checkCoupon(CouponRedemptionRequest request){
		CouponRedemptionResponse couponRedemptionResponse = null;
		try {
			couponRedemptionResponse = getActiveCouponService().checkCoupon(request);
		} catch (Exception e) {
			logger.error("Error in checkCoupon",e);
		}
		return couponRedemptionResponse;
	}
	
	@Override
	public MCoupon getMCoupon(String couponCode, String merchantId){
		MCoupon mCoupon = null;
		try {
			mCoupon = getActiveCouponService().getMCoupon(couponCode,merchantId);
		} catch (Exception e) {
			logger.error("Error in redeemCoupon",e);
		}
		return mCoupon;
	}
	
	@Override
	public CouponMerchant getMerchantFromMerchantId(String merchantId){
		CouponMerchant couponMerchant = null;
		try {
			couponMerchant = getActiveCouponService().getMerchantFromMerchantId(merchantId);
		} catch (Exception e) {
			logger.error("Error in getMerchantFromMerchantId",e);
		}
		return couponMerchant;
	}
	
	@Override
	public List<CouponHistory> getCouponsOptinHistory(String orderId, Integer couponStoreId){
		List<CouponHistory> couponHistories = null;
		try {
			couponHistories = getActiveCouponService().getCouponsOptinHistory(orderId,couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponsOptinHistory",e);
		}
		return couponHistories;
	}
	
	@Override
	public List<RedemptionInfo> findByCouponCodeAndMerchantId(String couponCode, String merchantId){
		List<RedemptionInfo> redemptionInfos = null;
		try {
			redemptionInfos = getActiveCouponService().findByCouponCodeAndMerchantId(couponCode,merchantId);
		} catch (Exception e) {
			logger.error("Error in findByCouponCodeAndMerchantId",e);
		}
		return redemptionInfos;
	}
	
	@Override
	public List<String> getCouponsCodeByOrderIdCouponId(String orderId, Integer couponStoreId){
		List<String> couponCodes = null;
		try {
			couponCodes = getActiveCouponService().getCouponsCodeByOrderIdCouponId(orderId,couponStoreId);
		} catch (Exception e) {
			logger.error("Error in getCouponsCodeByOrderIdCouponId",e);
		}
		return couponCodes;
	}
	
	@Override
	public CouponOrderStatus getOrderStatus(String orderId){
		CouponOrderStatus couponOrderStatus = null;
		try {
			couponOrderStatus = getActiveCouponService().getOrderStatus(orderId);
		} catch (Exception e) {
			logger.error("Error in getOrderStatus",e);
		}
		return couponOrderStatus;
	}
	
	@Override
	public CouponData getCouponDataLite(CouponFraudCheckRequest fraudCheckRequest, int channel, int appVersion){
		CouponData couponData = null;
		try {
			couponData = getActiveCouponService().getCouponDataLite(fraudCheckRequest,channel,appVersion);
		} catch (Exception e) {
			logger.error("Error in getCouponDataLite",e);
		}
		return couponData;
	}
	
	@Override
	public Map<String, String> getRecoStoreData(Integer userId, Integer channelId){
		Map<String, String> couponStoreData = null;
		try {
			couponStoreData = getActiveCouponService().getRecoStoreData(userId,channelId);
		} catch (Exception e) {
			logger.error("Error in getRecoStoreData",e);
		}
		return couponStoreData;
	}
	
	@Override
	public CouponData getDesktopCouponDataNonPromotedLite(CouponFraudCheckRequest fraudCheckRequest, int channel, int appVersion){
		CouponData coupon=null;
		try {
			coupon = getActiveCouponService().getDesktopCouponDataNonPromotedLite(fraudCheckRequest,channel,appVersion);
		} catch (Exception e) {
			logger.error("Error in getDesktopCouponDataNonPromotedLite",e);
		}
		return coupon;
	}
	
	@Override
	public CouponData getCouponData(CouponFraudCheckRequest fraudCheckRequest, int channel, int appVersion){
		CouponData coupon=null;
		try {
			coupon = getActiveCouponService().getCouponData(fraudCheckRequest,channel,appVersion);
		} catch (Exception e) {
			logger.error("Error in getCouponData",e);
		}
		return coupon;
	}
	
	@Override
	public CouponTnC getCouponTnC(int couponId){
		CouponTnC couponTnc = null;
		try{
			couponTnc = getActiveCouponService().getCouponTnC(couponId);
		} catch(Exception e) {
			logger.error("Error in getCouponTnC",e);
		}
		return couponTnc;
	}
	
	@Override
	public List<CouponCity> getCouponCities(Integer couponId){
		List<CouponCity> couponCities = null;
		try {
			couponCities = getActiveCouponService().getCouponCities(couponId);
		} catch(Exception e) {
			logger.error("Error in getCouponCities",e);
		}
		return couponCities;
	}
	
	@Override
	public Map<String, Boolean> doCacheRefresh(){
		Map<String, Boolean> refreshedMap = null;
		try {
			refreshedMap = getActiveCouponService().doCacheRefresh();
		} catch (Exception e){
			logger.error("Error in doCacheRefresh",e);
		}
		return refreshedMap;
	}
	
	@Override
	public void refreshCouponMerchantTrieMap(){
		try {
			getActiveCouponService().refreshCouponMerchantTrieMap();
		} catch (Exception e)  {
			logger.error("Error in refreshCouponMerchantTrieMap",e);
		}
	}
	
	@Override
	public List<Integer> getRecommendedCouponsForUser(Integer userId, Integer channelId){
		List<Integer> couponIds = null;
		try {
			couponIds = getActiveCouponService().getRecommendedCouponsForUser(userId,channelId);
		} catch (Exception e) {
			logger.error("Error in getRecommendedCouponsForUser",e);
		}
		return couponIds;
	}
	
	@Override
	public CouponData getDesktopCouponDataPromotedLite(CouponFraudCheckRequest fraudCheckRequest, int channel, int appVersion){
		CouponData couponData = null;
		try {
			couponData  = getActiveCouponService().getDesktopCouponDataPromotedLite(fraudCheckRequest,channel,appVersion);
		} catch (Exception e) {
			logger.error("Error in getDesktopCouponDataPromotedLite",e);
		}
		return couponData;
	}
	
	@Override
	public Coupon getCoupon(Integer couponStoreId, int channel){
		Coupon coupon = null;
		try {
			coupon = getActiveCouponService().getCoupon(couponStoreId,channel);
		} catch (Exception e) {
			logger.error("Error in getCoupon",e);
		}
		return coupon;
	}
	
	@Override
	public List<CouponMerchant> getAllMerchants(){
		List<CouponMerchant> couponMerchants = null;
		try {
			couponMerchants = getActiveCouponService().getAllMerchants();
		} catch (Exception e) {
			logger.error("Error in getAllMerchants",e);
		}
		return couponMerchants;
	}
	
	@Override
	public void updateMerchantReference(String merchantId){
		try {
			getActiveCouponService().updateMerchantReference(merchantId);
		} catch(Exception e) {
			logger.error("Error in updateMerchantReference",e);
		}
	}
	
	@Override
	public Coupon getCouponWithTnc(Integer couponStoreId, int channel){
		Coupon coupon = null;
		try {
			coupon = getActiveCouponService().getCouponWithTnc(couponStoreId,channel);
		} catch (Exception e) {
			logger.error("Error in getCouponWithTnc",e);
		}
		return coupon;
	}
	
	@Override
	public  Map<String, CityMaster> getAllCitiesNameMasterMap() {
		Map<String, CityMaster> cityNameMap = null;
		try {
			cityNameMap = getActiveCouponService().getAllCitiesNameMasterMap();
		} catch(Exception e){
			logger.error("Error in getAllCitiesNameMasterMap",e);
		}
		return cityNameMap;
	}
	
	@Override
	public void recordCouponOptinCount(Map<Integer, Integer> couponCounts,
			String couponCookie, String emailId,
			String profileNo, String ipAddress, String imeiNo, String userId){
		try {
			getActiveCouponService().recordCouponOptinCount(couponCounts,couponCookie,emailId,profileNo,ipAddress,imeiNo,userId);
		} catch (Exception e){
			logger.error("Error in recordCouponOptinCount",e);
		}
	}
	
	@Override
	public List<CouponHistory> getCoupons(List<String> orderIds){
		List<CouponHistory> coupons = null;
		try {
			coupons = getActiveCouponService().getCoupons(orderIds);
		} catch (Exception e) {
			logger.error("Error in getCoupons",e);
		}
		return coupons;
	}
	
	@Override
	public List<CouponHistory> getCouponsForOrder(String orderId){
		List<CouponHistory> couponHistories = null;
		try {
			couponHistories = getActiveCouponService().getCouponsForOrder(orderId);
		} catch (Exception e) {
			logger.error("Error in getCouponsForOrder",e);
		}
		return couponHistories;
	}

	@Override
	public CouponHistory getCouponTncByHistoryId(Integer id) {
		CouponHistory couponHistory = null;
		try {
			couponHistory = getActiveCouponService().getCouponTncByHistoryId(id);
		} catch (Exception e) {
			logger.error("Error in getCouponTncByHistoryId",e);
		} 
		return couponHistory;
	}
	
	@Override
	public List<Map<String, Object>> getAllAvailableCouponReissue(){
		List<Map<String, Object>> coupons = null;
		try {
			coupons = getActiveCouponService().getAllAvailableCouponReissue();
		} catch (Exception e) {
			logger.error("Error in getAllAvailableCouponReissue",e);
		}
		return coupons;
	}

	@Override
	public void saveOrderStatus(String orderId, boolean isSuccess, String reason, Integer couponId, boolean isRefunded) {
		try {
			getActiveCouponService().saveOrderStatus(orderId, isSuccess, reason, couponId, isRefunded);
		} catch (Exception e) {
			logger.error("Error in saveOrderStatus",e);
		}
	}

	@Override
	public void doCouponFulfillment(List<BlockCouponRequest> blockCouponList, Integer userId, Map<String, String> variableValues) {
		try {
			getActiveCouponService().doCouponFulfillment(blockCouponList, userId, variableValues);
		} catch (Exception e) {
			logger.error("Error in doCouponFulfillment",e);
		}
		
	}
	
	@Override
	public BlockedCouponResponse doAllTypeCouponFulfilment(List<CouponCartItem> coupons, String orderId,Integer userId, Map<String, String> variableValues) {
		try {
			return getActiveCouponService().doAllTypeCouponFulfilment(coupons, orderId, userId, variableValues);
		} catch (Exception e) {
			logger.error("Error in doCouponFulfillment",e);
		}
		return null;
    }

	@Override
	public CouponOrderIdTracker getCouponOrderIdTracker(String orderId) {
		try {
			return getActiveCouponService().getCouponOrderIdTracker(orderId);
		} catch (Exception e) {
			logger.error("Error while getting getCouponOrderIdTracker by orderId = " + orderId ,e);
		}
		return null;
    }
	
	
	@Override
	public Map<String, Object> getFilteredCouponsAndHCouponsForDisplay(String startStr, String countStr, String categoryIdsStr, String minValueStr,
            String maxValueStr, String minPriceStr, String maxPriceStr, int channel,
            CouponFraudCheckRequest fraudCheckRequestData, boolean includeTnC,
            boolean isPromoted, String city, int appVersion, String latStr, String longStr,boolean isInTxn, String orderId,
																	   Timestamp txnCreatedOn, int txnAmount) throws InvalidParameterException {

		Map<String, Object> couponDataApiResultMap = new HashMap<>();
		try {
			couponDataApiResultMap = getActiveCouponService().getFilteredCouponsAndHCouponsForDisplay(startStr,
					countStr, categoryIdsStr, minValueStr, maxValueStr, minPriceStr, maxPriceStr, channel,
					fraudCheckRequestData, includeTnC, isPromoted, city, appVersion, latStr, longStr, isInTxn, orderId,
					txnCreatedOn, txnAmount);
		} catch (Exception e) {
			logger.error("Error in getFilteredHCouponsForDisplay",e);
		}
		return couponDataApiResultMap;
	}
	@Override
	public String markCouponAsUsedInApiService(String couponCode, String orderId, boolean is_used) {
		return getActiveCouponService().markCouponAsUsedInApiService(couponCode, orderId,is_used);
	}
}

