package com.freecharge.coupon.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.api.coupon.service.web.model.Coupon;
import com.freecharge.api.coupon.service.web.model.CouponCategory;
import com.freecharge.api.coupon.service.web.model.CouponData;
import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.api.coupon.service.web.model.CouponsDataVO;
import com.freecharge.api.coupon.service.web.model.PageCouponCategory;
import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.mobile.web.view.MobileCouponData;

public class CouponDummyDataManager {
	
	private static boolean isDummyCouponMode = false;
	
	public static final CouponData getDummyCouponData(){
		CouponData couponData = new CouponData();
		couponData.setCoupons(getDummyCoupons());
		couponData.setCategories(getDummyCategories());
		return couponData;
	}
	
	public static final void setDummyCouponMode(boolean isDummyMode){
		isDummyCouponMode = isDummyMode;
	}
	
	public static final boolean isDummyCouponMode(){
		return isDummyCouponMode;
	}
	
	public static final List<Coupon> getDummyCoupons(){
		List<Coupon> coupons = new ArrayList<>();
		coupons.add(getMetroCoupon());
		return coupons;
	}
	
	public static final MobileCouponData getDummyOldMobileSiteCouponsData(){
		MobileCouponData mobileCouponData = new MobileCouponData();
		mobileCouponData.setCategories(getDummyVoucherCat());
		mobileCouponData.setCategoryMap(getDummyCouponsCategoryMap());
		return mobileCouponData;
	}
	
	private static Map<String, List<CouponVO>> getDummyCouponsCategoryMap() {
		Map<String, List<CouponVO>> couponsData = new HashMap<>();
		couponsData.put("Store", getDummyCouponVOList());
		return couponsData;
	}

	private static final List<CouponVO> getDummyCouponVOList(){
		List<CouponVO> couponVOList = new ArrayList<>();
		couponVOList.add(getDummyCouponVO());
		return couponVOList;
	}
	
	private static final CouponVO getDummyCouponVO() {
		CouponVO couponVo = new CouponVO();
		couponVo.setVoucherId(1311);
		couponVo.setVoucherName("MetroShoes.net - Rs.300 OFF");
		couponVo.setVoucherImgUrl("https://d32vr05tkg9faf.cloudfront.net/content/images/coupon/mobile/1311_1419494923583.png");
		couponVo.setVoucherImgUrlSW("https://d32vr05tkg9faf.cloudfront.net/content/images/coupon/web/1311_1419494857311.png");
		couponVo.setVoucherAmount(10);
		couponVo.setVoucherType("E");
		couponVo.setVoucherCat("Store");
		couponVo.setTermsConditions("Offer Highlights:<br />\nGet Rs.300 off on minimum purchase of Rs.1250 and above by shopping on MetroShoes.net.<br />\n<br />\nHow to redeem this Offer:<br />\nYou will receive an e-coupon code via e-mail as soon as your recharge/bill payment transaction on FreeCharge is successful .<br />\nYou need to apply the coupon code at the time of placing the order on www.metroShoes.net<br />\n<br />\nTerms of this Offer:<br />This offer is NOT valid at any of Metro Shoes retail outlets / showrooms across India.<br />\nThis offer applicable ONLY on Non Sale products at www.MetroShoes.net. <br />\nThis offer cannot be combined with any other offers.<br />\nMultiple coupons cannot be clubbed in a single order.<br />\nThe balance amount, after the discount has been availed, will have to be bared by the customer at the time of purchase <br />\nMetroShoes.net reserves the right to change the terms and conditions of this offer, including extending, withdrawing or discontinuing the same without notice, at its sole discretion.<br />\nAll orders would be subject to availability at the time of purchase and will be governed by the standard terms and conditions on MetroShoes.net");
		couponVo.setVoucherUrl("MetroShoes.net - Rs.300 OFF");
		couponVo.setCouponRemainingCount(0);
		couponVo.setCityNames("All");
		couponVo.setFaceValue(10);
		couponVo.setMinRedemptionAmount(1250);
		couponVo.setCouponShortDesc("Rs.300 OFF on Shoes!");
		couponVo.setValidityDate(new Date(1438281000000l));
		couponVo.setReadableValidityDate("31 Jul 2015");
		couponVo.setPrice(1);
		couponVo.setPriceLabel("paid");
		couponVo.setMerchantTitle("Metroshoes.net");
		couponVo.setPromotionLevel(0);
		couponVo.setNew(false);
		couponVo.setRecommended(false);
		couponVo.setHot(false);
		return couponVo;
	}
	
	private static List<VoucherCat> getDummyVoucherCat() {
		List<VoucherCat> voucherCategories = new ArrayList<>();
		VoucherCat cat = new VoucherCat();
		cat.setCatDetail("Online Store");
		cat.setCatFlag(true);
		cat.setCatName("Store");
		cat.setVoucherCatId(2);
		voucherCategories.add(cat);
		return voucherCategories;
	}

	public static final Map<String,Object> getDummyHCouponMap(){
		Map<String,Object> dummyHCouponMap = new HashMap<String, Object>();
		dummyHCouponMap.put("isHCouponVisible", false);
		dummyHCouponMap.put("coupons", new ArrayList<Coupon>());
		dummyHCouponMap.put("categories", new ArrayList<CouponCategory>());
		return dummyHCouponMap;
	}
	
	public static final List<CouponCategory> getDummyCategories(){
		List<CouponCategory> categories = new ArrayList<>();
		CouponCategory category = new CouponCategory();
		category.setCategoryId(2);
		category.setCategoryName("Store");
		List<Integer> couponIds = new ArrayList<>();
		couponIds.add(1311);
		category.setCouponIds(couponIds);
		category.setCategoryImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/category/null");
		category.setFreeCouponCount(0);
		category.setPaidCouponCount(1);
		categories.add(category);
		return categories;
	}
	
	public static final List<PageCouponCategory> getDummyPageCategories(){
		List<PageCouponCategory> categories = new ArrayList<>();
		PageCouponCategory category = new PageCouponCategory();
		category.setCategoryId(2);
		category.setCategoryName("Store");
		category.setCouponCount(1);
		category.setCategoryImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/category/null");
		categories.add(category);
		return categories;
	}
	
	public static final CouponsDataVO getDummyCouponDataVO(){
		CouponsDataVO vo = new CouponsDataVO();
		List<com.freecharge.api.coupon.service.web.model.VoucherCat> catList = new ArrayList<com.freecharge.api.coupon.service.web.model.VoucherCat>();
		vo.setCategories(catList);
		vo.setCategoryMap(getDummyCouponsCategoryMap());
		return vo;
	}
	
	private static final Coupon getMetroCoupon(){
		Coupon coupon = new Coupon();
		coupon.setCouponId(1311);
		coupon.setCategoryId(2);
		coupon.setCouponName("MetroShoes.net - Rs.300 OFF");
		coupon.setCouponValue(10);
		coupon.setCouponImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/coupon/mobile/1311_1419494923583.png");
		coupon.setBigCouponImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/coupon/web/1311_1419494857311.png");
		coupon.setCouponType("E");
		coupon.setValidityDate("31 Jul 2015");
		coupon.setPrice(1);
		coupon.setTermsConditions("https://www.freecharge.in/rest/coupon/tnc?couponIds=1311&ts=" + System.currentTimeMillis());
		coupon.setCouponShortDesc("Rs.300 OFF on Shoes!");
		coupon.setMerchantTitle("Metroshoes.net");
		coupon.setMaxOptinCount(2);
		coupon.setRank(94);
		coupon.setMinRedemptionAmount(1250);
		coupon.setAppCouponImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/coupon/app/1311_1419085648573.png");
		coupon.setBrandImagePath("https://d32vr05tkg9faf.cloudfront.net/content/images/brand/merchant_icon/53b28f9a26007_Metro.png");
		coupon.setCities("All");
		coupon.setPromotionLevel("1");
		coupon.setRating(0);
		coupon.setNoOfPick("0");
		coupon.setFaceValue(10);
		coupon.setLabel("99% off");
		coupon.setPrintable(false);
		coupon.setInTxn(false);
		coupon.setOutTxn(false);
		return coupon;
	}
}
