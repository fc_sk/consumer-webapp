package com.freecharge.coupon.service;

import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.coupon.dao.ICouponExperimentDao;
import com.freecharge.tracker.TrackerEventVo;
import com.freecharge.tracker.api.TrackerApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("couponExperimentService")
public class CouponExperimentService {

    @Autowired
    private TrackerApiService trackerApiService;

    @Autowired
    private ICouponExperimentDao couponExperimentDao;

    public void addCouponExperimentMeasure(String uid, String imei, Integer measureId){
        if (FCStringUtils.isBlank(imei) || measureId==null){
            return;
        }

        TrackerEventVo eventVo = new TrackerEventVo();
        eventVo.setEventName("app_cpn_exp");
        eventVo.setVal0(imei);
        eventVo.setVal1(String.valueOf(measureId));
        this.trackerApiService.trackEvent(eventVo, FCConstants.DUMMY_VISIT_ID);
        //this.couponExperimentDao.addCouponExperimentMeasure(uid, imei, measureId);
    }

    public void updateLookUpIdInCouponExperiment(String uid, String lookupId, String imei){
        if (FCStringUtils.isBlank(imei) || FCStringUtils.isBlank(lookupId)){
            return;
        }
        TrackerEventVo eventVo = new TrackerEventVo();
        eventVo.setEventName("app_cpn_exp_li");
        eventVo.setVal0(imei);
        eventVo.setVal1(lookupId);
        this.trackerApiService.trackEvent(eventVo, FCConstants.DUMMY_VISIT_ID);
        //this.couponExperimentDao.updateCouponExperimentMeasure(uid, lookupId, imei);
    }
}
