package com.freecharge.growthevent.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.freecharge.reward.models.*;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.reward.enums.RewardType;
import com.freecharge.reward.web.AbstractProcessResult;
import com.freecharge.reward.web.IRewardService;
import com.freecharge.reward.web.RewardEligibleResult;
import com.freecharge.reward.web.RewardProcessResult;

@Service
public class RewardServiceProxy implements IRewardService {

	private Logger logger = Logger.getLogger(getClass());
	
	private IRewardService remoteRewardService;
	
	@PostConstruct
	public void init(){
		try {
			ApplicationContext applicationContextClient = new ClassPathXmlApplicationContext("classpath:reward-client-webapp-beans.xml");
			remoteRewardService = (IRewardService) applicationContextClient.getBean("rewardServiceClient");
		} catch (Exception e) {
			logger.error("Error initializing local reward service context.", e);
		}
	}
	
	@Override
	public Integer createOrUpdateReward(Reward reward) {
		return null;
	}

	@Override
	public Reward getRewardById(Integer rewardId) {
		return null;
	}

	@Override
	public List<Reward> getAllReward() {
		return null;
	}

	@Override
	public Integer createOrUpdateCouponRewardDetail(
			CouponRewardDetail couponReward) {
		return null;
	}

	@Override
	public Integer createOrUpdateCreditRewardDetail(
			CreditRewardDetail creditReward) {
		return null;
	}

	@Override
	public Integer createOrUpdatePercentCreditRewardDetail(
			PercentCreditRewardDetail percentCreditReward) {
		return null;
	}

	@Override
	public void createOrUpdateGrouponRewardDetails(
			GroupRewardDetail grouponReward) {
	}

	@Override
	public Integer createOrUpdateAndGroupReward(
			AndGroupRewardDetail andGroupReward) {
		return null;
	}

	@Override
	public Integer createOrUpdateOrGroupReward(OrGroupRewardDetail orGroupReward) {
		return null;
	}

	@Override
	public RewardProcessResult processReward(Integer rewardId, Long userId,
			Integer callerId, String orderId, String refId) {
		return remoteRewardService.processReward(rewardId, userId, callerId, orderId, refId);
	}

	@Override
	public CreditRewardDetail getCreditRewardByRewardId(Integer rewardId) {
		return null;
	}

	@Override
	public RewardEligibleResult rewardEligiblityCheck(Integer rewardId,
			Long userId, Integer callerId, String orderId, String refId, Map<String, Object> prMap) {
		return this.remoteRewardService.rewardEligiblityCheck(rewardId, userId, callerId, orderId, refId, prMap);
	}

	@Override
	public AbstractProcessResult processEligibleReward(Integer rewardId,
			Long userId, Integer callerId, String orderId, String refId,
			Integer childRewardId, RewardType type, Map<String, Object> prMap) {
		return this.remoteRewardService.processEligibleReward(rewardId, userId, callerId, orderId, refId, childRewardId, type, prMap);
	}

	@Override
	public CreditRewardDetail getCreditRewardById(Integer paramInteger) {
		return null;
	}

	@Override
	public CouponRewardDetail getCouponRewardsByRewardId(Integer paramInteger) {
		return null;
	}

	@Override
	public CouponRewardDetail getCouponRewardById(Integer paramInteger) {
		return null;
	}

	@Override
	public List<AndGroupRewardDetail> getAndGroupRewardsByMasterRewardId(Integer paramInteger) {
		return null;
	}

	@Override
	public Boolean createOrUpdateAndGroupRewardList(Integer paramInteger, List<Integer> paramList) {
		return null;
	}

	@Override
	public void removeAndGroupRewardByMasterId(Integer paramInteger) {
	}

	@Override
	public List<AndGroupRewardDetail> getAndGroupRewardsByChildRewardId(Integer paramInteger) {
		return null;
	}

	@Override
	public AndGroupRewardDetail getAndGroupRewardsByChildRewardMasterId(Integer paramInteger) {
		return null;
	}

	@Override
	public void removeAndGroupRewardsForChildRewardMasterId(Integer paramInteger) {
	}

	@Override
	public RewardProcessResult processRewardWithAmount(Integer integer, Long aLong, Integer integer1, String s, String s1, Map<String, Object> map) {
		return null;
	}

	@Override
	public PercentCreditRewardDetail getPercentCreditRewardDetailById(Integer integer) {
		return null;
	}

	@Override
	public PercentCreditRewardDetail getPercentCreditRewardDetailByRewardId(Integer integer) {
		return null;
	}

	@Override
	public DataRewardDetail getDataRewardById(Integer integer) {
		return null;
	}

	@Override
	public DataRewardDetail getDataRewardDetailByRewardId(Integer integer) {
		return null;
	}

	@Override
	public Integer createOrUpdateDataRewardDetail(DataRewardDetail dataRewardDetail) {
		return null;
	}

	@Override
	public List<CompleteRewardDetails> getAllChildRewardsByRewardId(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer createOrUpdateTokenRewardDetail(TokenRewardDetail tokenrewarddetail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TokenRewardDetail getTokenRewardByRewardId(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TokenRewardDetail getTokenRewardById(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FactoredTokenRewardDetail getFactoredTokenRewardByRewardId(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FactoredTokenRewardDetail getFactoredTokenRewardById(Integer integer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer createOrUpdateFactoredTokenRewardDetail(FactoredTokenRewardDetail factoredtokenrewarddetail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DataUserReward> getEligibleDataReward(Integer integer, String s, Integer integer1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DataRewardAmountMapping> getDataRewardAmountDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
