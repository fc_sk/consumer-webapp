package com.freecharge.growthevent.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.user.RewardStatusCache;
import com.freecharge.reward.models.CreditRewardDetail;
import com.freecharge.reward.web.AbstractProcessResult;
import com.freecharge.reward.web.IRewardService;
import com.freecharge.reward.web.RewardEligibleResult;

@Service
public class VariableRewardService {

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private RewardStatusCache rewardCache;
	
	@Autowired
	@Qualifier("rewardServiceProxy")
	private IRewardService rewardServiceProxy;
	
	public boolean isNewRewardServiceConfigEnabled() {

		if (!appConfigService.isChildPropertyEnabled(
				AppConfigService.NEW_REWARD_SERVICE_CONFIG,
				AppConfigService.NEW_REWARD_SERVICE_ENABLED)) {
			logger.info("New Reward Service Disabled.");
			return false;
		}
		return true;
	}

	public boolean isEventEligibleForNewRewardService(long eventId) {
		String eventIds = appConfigService.getChildProperty(
				AppConfigService.NEW_REWARD_SERVICE_CONFIG,
				AppConfigService.NEW_REWARD_SERVICE_EVENT_IDS);
		if (FCUtil.isEmpty(eventIds)) {
			logger.info("No Event config for New Reward Service");
			return false;
		}
		List<String> items = Arrays.asList(eventIds.split("\\s*,\\s*"));
		for (String item : items) {
			if (eventId == Long.parseLong(item)) {
				return true;
			}
		}
		return false;
	}

	public Map<String, Object> processReward(String orderId) {
		if(orderId==null || orderId.isEmpty()){
			logger.info("orderId is null or empty");
			return null;
		}
		RewardEligibleResult eligibleResult = rewardCache.getRewardStatus(orderId);
		Map<String,Object> resultMap = new HashMap<String, Object>();
		if(eligibleResult!=null && eligibleResult.isEligible() && eligibleResult.getEligibleResults()!=null && eligibleResult.getEligibleResults().size()>0){
			AbstractProcessResult result = null;
			try {
				result = rewardServiceProxy.processEligibleReward(eligibleResult.getRewardId(), eligibleResult.getUserId(), getCallerId(eligibleResult.getRewardId()), 
					orderId, orderId,eligibleResult.getChildRewardId(),eligibleResult.getEligibleResults().get(0).getType(), null);
			} catch (Exception e) {
				logger.error("error in fetching process Reward",e);
			}
			if(result!=null) {
				resultMap.put("status",result.getStatus().getStatus()==1);
				if(result.getInfo()!=null) {
					resultMap.put("creditedAmount", result.getInfo().get(CreditRewardDetail.CREDITED_AMOUNT));
				}
			}
			
		}
		return resultMap;
	}
	
	public void setRewardEligiblityResult(String orderId,long userId,long eventId,String rechargeMobileNumber) {
		logger.info("enter into setRewardEligiblityResult for orderId: "+orderId+" userId: "+userId+
				" eventId:"+eventId+" rechargeMobileNumebr:"+rechargeMobileNumber);
		if(!FCUtil.isEmpty(orderId)) {
			Integer rewardId = getRewardId(eventId);
			if (rewardId!=null) {
				int callerId = getCallerId(rewardId);
				RewardEligibleResult result = null;
				try {
					result = rewardServiceProxy.rewardEligiblityCheck(rewardId, userId, callerId, orderId, orderId, null);
				} catch (Exception e) {
					logger.error("error in fetching reward eligiblity", e);
				}
				if (result!=null && result.isEligible()) {
					rewardCache.setRewardStatus(orderId, result);
					Map<String,Object> resultMap = new HashMap<String, Object>();
					resultMap.put(rewardCache.USER_ID,userId);
					resultMap.put(rewardCache.EVENT_ID, eventId);
					resultMap.put(rewardCache.ORDER_ID, orderId);
					if(result.getInfo()!=null){
						resultMap.put(rewardCache.REWARD_VALUE, "CachBack"+result.getInfo().get((CreditRewardDetail.CREDITED_AMOUNT)));
					} else {
						resultMap.put(rewardCache.REWARD_VALUE, "CachBack");
					}
					resultMap.put(rewardCache.RECHARGE_MOBILE_NUMBER, rechargeMobileNumber);
					rewardCache.setGrowthRewardHistory(orderId, resultMap);
				} 
			}
		}
	}
	
	private int getCallerId(Integer rewardId) {
		String rewardCallerIdMap = appConfigService.getChildProperty(
				AppConfigService.NEW_REWARD_SERVICE_CONFIG,
				AppConfigService.REWARD_CALLER_ID_MAP);
		int callerId = 1;
        if (!FCUtil.isEmpty(rewardCallerIdMap)) {
            JSONParser parser = new JSONParser();
            try {
				JSONObject jsonObj= (JSONObject) parser.parse(rewardCallerIdMap);
				 if(jsonObj.containsKey(rewardId+"")) {
			            callerId =Integer.parseInt((String)jsonObj.get(rewardId+""));
			        }
			} catch (ParseException e) {
				logger.error("callerId parse error");
			}
        }
		return callerId;
	}
	
	private Integer getRewardId(long eventId) {
		String eventCallerIdMap = appConfigService.getChildProperty(
				AppConfigService.NEW_REWARD_SERVICE_CONFIG,
				AppConfigService.EVENT_REWARD_ID_MAP);
		Integer rewardId = null;
        if (!FCUtil.isEmpty(eventCallerIdMap)) {
            JSONParser parser = new JSONParser();
            try {
				JSONObject jsonObj= (JSONObject) parser.parse(eventCallerIdMap);
				 if(jsonObj.containsKey(eventId+"")) {
			            rewardId =Integer.parseInt((String)jsonObj.get(eventId+""));
			     }
			} catch (ParseException e) {
				logger.error("rewardId parse error");
			}
        }
		return rewardId;
	}
	
	public Map<String,Object> getRewardStatus(String orderId) {
		if(orderId==null || orderId.isEmpty()){
			logger.info("orderId is null or empty");
			return null;
		}
		Map<String,Object> resultMap = new HashMap<String, Object>();
		RewardEligibleResult result = rewardCache.getRewardStatus(orderId);
		if(result!=null) {
			resultMap.put("eligible", result.isEligible());
			if(result.getInfo()!=null){
				resultMap.put("creditedAmount",result.getInfo().get(CreditRewardDetail.CREDITED_AMOUNT));
			}
		}
		return resultMap;
	}
	
	
}
