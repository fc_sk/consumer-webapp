package com.freecharge.growthevent.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.UserDetails;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUserUtil;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.ims.client.IUserMigrationServiceClient;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.enums.StatusEnum;
import com.snapdeal.ims.exception.HttpTransportException;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.ConfigureUserStateRequest;
import com.snapdeal.ims.request.GetUserByEmailRequest;
import com.snapdeal.ims.request.GetUserByIdRequest;
import com.snapdeal.ims.request.GetUserByMobileRequest;
import com.snapdeal.ims.request.UserUpgradeByEmailRequest;
import com.snapdeal.ims.response.ConfigureUserStateResponse;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.ims.response.UserUpgradationResponse;

@Service
public class UserServiceProxy {
	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private CartService cartService;

	@Autowired
	private IUserServiceClient userServiceClient;
	
	@Autowired
	private IUserMigrationServiceClient userMigrationServiceClient;

	@Autowired
	UserWriteDao userWriteDao;

	@Autowired
	OrderIdSlaveDAO orderIdSlaveDAO;

	public String getEmailIdFromUserId(Long userId) {
		Users user = getUserByUserId(userId.intValue());
		if (user != null) {
			return user.getEmail();
		}
		return null;
	}

	public Users getUserByEmailId(String emailId) {
		Users user = null;
		try {
			GetUserByEmailRequest request = new GetUserByEmailRequest();
			request.setEmailId(emailId);
			GetUserResponse response = userServiceClient.getUserByEmail(request);
			if (null != response) {
				UserDetailsDTO userDTO = response.getUserDetails();
				user = FCUserUtil.getUser(userDTO);
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;
	}

	public UserDetails getUserProfileByEmailId(String emailId) {
		logger.info("Enter into getUserProfileByEmailId for emailId :"+emailId);
		UserDetails user = null;
		try {
			GetUserByEmailRequest request = new GetUserByEmailRequest();
			request.setEmailId(emailId);
			GetUserResponse response = userServiceClient.getUserByEmail(request);
			if (null != response) {
				UserDetailsDTO userDTO = response.getUserDetails();
				user = FCUserUtil.getUserDetails(userDTO);
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;

	}
	
	public UserDetails getUserProfileByUserId(Integer userId) {
		logger.info("Enter into getUserProfileByUserId for userId :"+userId);
		UserDetails user = null;
		try {
			if (null != userId) {
				GetUserByIdRequest request = new GetUserByIdRequest();
				request.setUserId(userId.toString());
				GetUserResponse response = userServiceClient.getUserById(request);
				if (null != response) {
					UserDetailsDTO userDetailsDTO = response.getUserDetails();
					user = FCUserUtil.getUserDetails(userDetailsDTO);
				}
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;

	}

	public Users getUserByUserId(Integer userId) {
		Users user = null;
		try {
			if (null != userId) {
				GetUserByIdRequest request = new GetUserByIdRequest();
				request.setUserId(userId.toString());
				GetUserResponse response = userServiceClient.getUserById(request);
				if (null != response) {
					UserDetailsDTO userDetailsDTO = response.getUserDetails();
					user = FCUserUtil.getUser(userDetailsDTO);
				}
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;
	}

	public String getEmailIdByLookupId(String lookupId) {
		CartBusinessDo cart = cartService.getCart(lookupId);
		if (cart != null && cart.getUserId() != null) {
			return getEmailByUserId(cart.getUserId());
		}
		return null;
	}

	public Users getLoggedInUser() {
		if (FreechargeContextDirectory.get() != null
				&& FreechargeContextDirectory.get().getFreechargeSession() != null) {
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			Map<String, Object> map = fs.getSessionData();
			Boolean isloggedin = false;
			if (map != null) {
				String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
				if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
					isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
					if (isloggedin != null && !isloggedin) {
						logger.debug("No logged in user found for " + email);
						return null;
					} else {
						Users user = getUserByEmailId(email);
						if (user == null) {
							logger.debug("No user found in db for " + email);
						}
						return user;
					}
				} else {
					logger.debug("isLogin boolean not found in session data for " + email);
					return null;
				}
			} else {
				logger.debug("No session data found for user");
			}
		} else {
			logger.debug("No context found in freecharge context directory");
		}
		return null;
	}

	public UserProfile getUserProfile(Integer userId) {
		UserDetailsDTO userDetailsDTO = null;
		try {
			if (null != userId) {
				GetUserByIdRequest request = new GetUserByIdRequest();
				request.setUserId(userId.toString());
				GetUserResponse response = userServiceClient.getUserById(request);
				if (null != response) {
					userDetailsDTO = response.getUserDetails();
				}
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
			return null;
		}
		UserProfile userProfile = FCUserUtil.getUserProfile(userDetailsDTO);
		return userProfile;
	}

	public UserProfile getDefaultUserProfile(Integer userId) {
		UserDetailsDTO userDetailsDTO = null;
		try {
			if (null != userId) {
				GetUserByIdRequest request = new GetUserByIdRequest();
				request.setUserId(userId.toString());
				GetUserResponse response = userServiceClient.getUserById(request);
				if (null != response) {
					userDetailsDTO = response.getUserDetails();
				}
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
			return null;
		}
		UserProfile userProfile = FCUserUtil.getUserProfile(userDetailsDTO);
		return userProfile;
	}

	public List<com.freecharge.app.domain.entity.Users> getUsersByProfileMobileNo(String mobileNo) {
		List<com.freecharge.app.domain.entity.Users> usersList = new ArrayList<com.freecharge.app.domain.entity.Users>();
		try {
			GetUserByMobileRequest request = new GetUserByMobileRequest();
			request.setMobileNumber(mobileNo);
			GetUserResponse response =
				userServiceClient.getUserByVerifiedMobile(request);
			if (null != response) {
				com.freecharge.app.domain.entity.Users user =
					FCUserUtil.getUsers(response.getUserDetails());
				usersList.add(user);
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return usersList;
	}

	public int updateUserEmailId(Integer userId, String emailId) {
		return userService.updateUserEmailId(userId, emailId);
	}

	public UserProfile getUserProfile(String email) {
		UserDetailsDTO userDTO = null;
		try {
			GetUserByEmailRequest request = new GetUserByEmailRequest();
			request.setEmailId(email);
			GetUserResponse response = userServiceClient.getUserByEmail(request);
			if (null != response) {
				userDTO = response.getUserDetails();
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
			return null;
		}
		UserProfile userProfile = FCUserUtil.getUserProfile(userDTO);
		return userProfile;
	}

	public String getEmailByUserId(Integer userId) {
		Users user = getUserByUserId(userId);
		if (user != null) {
			return user.getEmail();
		}
		return null;
	}

	public long getUserIdByEmailId(String email) {
		Users user = getUserByEmailId(email);
		if (user != null) {
			return user.getUserId();
		}
		return 0;
	}

	public boolean isUserExists(String emailId) {
		Users user = getUserByEmailId(emailId);
		if (user != null) {
			return true;
		}
		return false;
	}

	public boolean deActivateUser(String email) {
		ConfigureUserStateResponse response = null;
		try {
			ConfigureUserStateRequest request = new ConfigureUserStateRequest();
			request.setEnable(false);
			response = userServiceClient.configureUserState(request);
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		if (response != null && response.getStatus() != null && response.getStatus().equals(StatusEnum.SUCCESS)) {
			return true;
		}
		return false;
	}

	public boolean activateUser(String email) {
		ConfigureUserStateResponse response = null;
		try {
			ConfigureUserStateRequest request = new ConfigureUserStateRequest();
			request.setEnable(true);
			response = userServiceClient.configureUserState(request);
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		if (response != null && response.getStatus() != null && response.getStatus().equals(StatusEnum.SUCCESS)) {
			return true;
		}
		return false;
	}

	public Users getUserFromOrderId(String orderId) {
		Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
		if (userId != null) {
			return getUserByUserId(userId);
		}
		return null;
	}

	public String getEmailIdFromOrderId(String orderId) {
		String email = null;
		Users user = getUserFromOrderId(orderId);
		if (user != null) {
			email = user.getEmail();
		}
		return email;
	}

	public String getUserContactNumberFromOrderId(String orderId) {
		String mobileNumber = "";
		Users user = getUserFromOrderId(orderId);
		if (user != null) {
			mobileNumber = user.getMobileNo();
		}
		return mobileNumber;
	}

	/**
	 * Validates if a given order Id is owned by the logged in user.
	 * 
	 * @param orderId
	 * @return <code>true</code> if valid.
	 */
	public boolean validateOrderOwnership(String orderId) {
		Integer loggedinUserId = this.getLoggedInUser().getUserId();
		Users orderIdOwner = this.getUserFromOrderId(orderId);

		if (orderIdOwner == null || (orderIdOwner.getUserId().intValue() != loggedinUserId.intValue())) {
			return false;
		}

		return true;
	}

	public Integer getUserIdByLookupId(String lookupId) {
		CartBusinessDo cart = cartService.getCart(lookupId);
		if (cart != null && cart.getUserId() != null) {
			return cart.getUserId();
		}
		return null;
	}

	public String getLoggedInUserEmailId() {
		String emailId = "";
		try {
			Users user = this.getLoggedInUser();
			emailId = user.getEmail().toLowerCase().trim();
		} catch (Exception ex) {
			logger.error("Error fetching current logged in user.");
		}
		return emailId;
	}
	
	public MigrationStatus getUserMigrationStatusByEmail(String email, String token)  {
        UserUpgradeByEmailRequest request = fetchUserUpgradeByEmailRequest(email, token);
        logger.info(String.format("Get user migration status by email - %s is started", email));
        MigrationStatus migrationStatus = null;
        UserUpgradationResponse response = null;
        try {
            response = userMigrationServiceClient.userUpgradeStatus(request);
            migrationStatus = FCUserUtil.getUserMigrationStatus(response);
        } catch (Exception e) {
        	logger.error("ims fetching getUserMigrationStatusByEmail error", e);
        }
        logger.info(String.format("Get user migration status by email - %s and response - %s is completed", email, response));
        return migrationStatus;
    }
	
	private UserUpgradeByEmailRequest fetchUserUpgradeByEmailRequest(String email, String token) {
        UserUpgradeByEmailRequest request = new UserUpgradeByEmailRequest();
        request.setEmailId(email);
        request.setToken(token);
        return request;
    }
	
	public UserDetailsDTO getUserDetailsByEmailId(final String emailId) {
		UserDetailsDTO user = null;
		try {
			GetUserByEmailRequest request = new GetUserByEmailRequest();
			request.setEmailId(emailId);
			GetUserResponse response = userServiceClient.getUserByEmail(request);
			if (null != response) {
				user = response.getUserDetails();
			}
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;
	}
	
	public UserDetailsDTO getUserDetailsByMobileNo(String mobileNo) {
		UserDetailsDTO user = null;
		try {
			GetUserByMobileRequest request = new GetUserByMobileRequest();
			request.setMobileNumber(mobileNo);
			GetUserResponse response = userServiceClient.getUserByVerifiedMobile(request);
			if (null != response) {
				user = response.getUserDetails();
			}
		} catch (HttpTransportException e) {
			logger.error("HttpTransportException while fetching user by mobile no", e);
		} catch (ServiceException e) {
			logger.error("ServiceException while fetching user by mobile no", e);
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;
	}

	public UserDetailsDTO getUserDetailsByUserId(String userId) {
		logger.info("Enter into getUserDetailsByUserId for userId :" + userId);
		UserDetailsDTO user = null;
		try {
			if (null != userId) {
				GetUserByIdRequest request = new GetUserByIdRequest();
				request.setUserId(userId);
				GetUserResponse response = userServiceClient.getUserById(request);
				if (null != response) {
					logger.info(String.format("Exiting UserServiceProxy.getUserDetailsByUserId (GetUserResponse=%s)",response));
					user = response.getUserDetails();
				}
			}
		} catch (HttpTransportException e) {
			logger.error("HttpTransportException while fetching user by user id", e);
		} catch (ServiceException e) {
			logger.error("ServiceException while fetching user by user id", e);
		} catch (Exception e) {
			logger.error("ims fetching data error", e);
		}
		return user;
	}

}