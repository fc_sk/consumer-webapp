package com.freecharge.growthevent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.mongo.repos.GrowthActivityFailureMsgRepository;
import com.mongodb.DBObject;

/**
 * This service has utilities to get entries from or insert entries to growthActivityFailureMsg repository
 **/
@Service
public class GrowthActivityFailureMsgService {

    @Autowired
    private GrowthActivityFailureMsgRepository growthActivityFailureMsgRepository;

    public void saveFailureReason(final long userId, final Long growthEventId, final String rewardFailureReason,
                                  final String orderId, final String rechargeMobileNumber) {
        if (null != growthEventId) {
            this.growthActivityFailureMsgRepository.insert(userId, growthEventId, rewardFailureReason,
                                                           orderId, rechargeMobileNumber);
        }
    }

    public List<DBObject> getFailureMsgs(final long growthEventId, final String key, final Object value) {
        return this.growthActivityFailureMsgRepository.getData(growthEventId, key, value);
    }
}
