package com.freecharge.growthevent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.mongo.repos.CouponRewardHistoryRepository;
import com.mongodb.DBObject;

/**
 * This service has utilities to get entries from or insert entries to couponRewardHistory repository
 **/
@Service
public class CouponRewardHistoryService {

    @Autowired
    private CouponRewardHistoryRepository couponRewardHistoryRepository;

    public void saveCouponRewardHistory(final long userId, final Long growthEventId, final String growthEventType,
                                        final String rechargeMobileNumber, final String promocode,
                                        final Long freefundClassId, final String orderId) {
        this.couponRewardHistoryRepository.insert(userId, growthEventId, growthEventType, rechargeMobileNumber, promocode, freefundClassId, orderId);
    }

    public List<DBObject> getRewardHistoryByEventAndMobileNumber(final long growthEventId, final String mobileNumber) {
        return this.couponRewardHistoryRepository.getCouponRewardHistoryByEventAndMobileNumber(growthEventId, mobileNumber);
    }

    public List<DBObject> getRewardHistoryByEventTypeAndMobileNumber(final String growthEventType, final String mobileNumber) {
        return this.couponRewardHistoryRepository.getCouponRewardHistoryByEventAndMobileNumber(growthEventType, mobileNumber);
    }

    public List<DBObject> getRewardHistoryByMobileNumber(final String mobileNumber) {
        return this.couponRewardHistoryRepository.getCouponRewardHistoryByMobileNumber(mobileNumber);
    }
}