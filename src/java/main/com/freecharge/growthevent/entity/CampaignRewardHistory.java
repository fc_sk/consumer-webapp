package com.freecharge.growthevent.entity;

import org.json.simple.JSONObject;

/**
 * Created by suryaprasanna on 06/08/15.
 */
public class CampaignRewardHistory {
    private long userId;
    private long campaignId;
    private String growthEventType;
    private long rewardId;
    private String rewardValue;
    private String orderId;
    private String serviceNumber;
    private String params;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    public String getGrowthEventType() {
        return growthEventType;
    }

    public void setGrowthEventType(String growthEventType) {
        this.growthEventType = growthEventType;
    }

    public long getRewardId() {
        return rewardId;
    }

    public void setRewardId(long rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardValue() {
        return rewardValue;
    }

    public void setRewardValue(String rewardValue) {
        this.rewardValue = rewardValue;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String asJsonString() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId", userId);
        jsonObject.put("campaignId",campaignId);
        jsonObject.put("growthEventType",growthEventType);
        jsonObject.put("rewardId",rewardId);
        jsonObject.put("rewardValue",rewardValue);
        jsonObject.put("orderId",orderId);
        jsonObject.put("serviceNumber",serviceNumber);
        jsonObject.put("params",params);
        return jsonObject.toString();
    }
}
