package com.freecharge.growthevent.entity;

import java.util.Date;

/**
 * This table stores data about the Growth Event Opt-ins by users.
 * We make entries here whenever a user opts-in.
 * We also make entries here when we send the email, sms and notifications to user
 */
public class GrowthActivityEntity {
    public static String ACTIVITY_OPTIN = "optin";
    public static String ACTIVITY_REWARDED = "rewarded";
    public static String ACTIVITY_EMAIL_SENT = "emailSent";
    public static String ACTIVITY_SMS_SENT = "smsSent";
    public static String ACTIVITY_APP_NOTIFICATION_SENT = "appNotificationSent";

    private Long id;
    private Long fkGrowthEventId;
    private String eventType;
    private Integer fkUserId;
    private String sessionId;
    private String lookupId;
    private Long threadId;
    private String activity;
    private Date createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFkGrowthEventId() {
        return fkGrowthEventId;
    }

    public void setFkGrowthEventId(Long fkGrowthEventId) {
        this.fkGrowthEventId = fkGrowthEventId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Integer getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(Integer fkUserId) {
        this.fkUserId = fkUserId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String toString() {
        return "[GrowthActivityEntity: id : " + id + ", fkGrowthEventId : " + fkGrowthEventId + ", eventType : " + eventType 
                + ", userId : " + fkUserId + ", sessionId : " + sessionId + ", lookupId : " + lookupId 
                + ", activity : " + activity + ", threadId : " + threadId + "]";
    }

}
