package com.freecharge.growthevent.entity;

import java.util.Date;

import com.freecharge.common.framework.basedo.AbstractDO;

/**
 * This entity has the details of a reward after it is successfully processed
 **/
public class GrowthRewardHistoryEntity extends AbstractDO {

    private long id;
    private long fkUserId;
    private long fkGrowthEventId;
    private long fkARRewardId;
    private long fkARRewardTypeId;
    private String rewardValue;
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public long getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(final long fkUserId) {
        this.fkUserId = fkUserId;
    }

    public long getFkGrowthEventId() {
        return fkGrowthEventId;
    }

    public void setFkGrowthEventId(final long growthEventId) {
        this.fkGrowthEventId = growthEventId;
    }

    public long getFkARRewardId() {
        return fkARRewardId;
    }

    public void setFkARRewardId(final long fkARRewardId) {
        this.fkARRewardId = fkARRewardId;
    }

    public long getFkARRewardTypeId() {
        return fkARRewardTypeId;
    }

    public void setFkARRewardTypeId(final long fkARRewardTypeId) {
        this.fkARRewardTypeId = fkARRewardTypeId;
    }

    public String getRewardValue() {
        return rewardValue;
    }

    public void setRewardValue(final String rewardValue) {
        this.rewardValue = rewardValue;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(final Date createdOn) {
        this.createdOn = createdOn;
    }
}
