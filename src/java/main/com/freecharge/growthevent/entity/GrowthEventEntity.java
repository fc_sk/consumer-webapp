package com.freecharge.growthevent.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.freecharge.common.framework.basedo.AbstractDO;

public class GrowthEventEntity extends AbstractDO implements java.io.Serializable {

    private static final long serialVersionUID = 827973193180467534L;

    private long id;
    @NotBlank(message = "Growth Event name is required.")
    private String name;
    @NotNull
    private String eventType;
    @NotNull
    private Long fkRewardId;
    @NotNull
    private Long fkConditionId;
    private Date validFrom;
    private Date validUpto;
    private Date createdOn;
    private Date updatedOn;
    @NotBlank(message = "Please select channels for the event.")
    private String channels;
    private boolean isPrivUserCond;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEventType() {
        return eventType;
    }
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
    public Long getFkRewardId() {
        return fkRewardId;
    }
    public void setFkRewardId(Long fkRewardId) {
        this.fkRewardId = fkRewardId;
    }
    public Long getFkConditionId() {
        return fkConditionId;
    }
    public void setFkConditionId(Long fkConditionId) {
        this.fkConditionId = fkConditionId;
    }
    public Date getValidFrom() {
        return validFrom;
    }
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }
    public Date getValidUpto() {
        return validUpto;
    }
    public void setValidUpto(Date validUpto) {
        this.validUpto = validUpto;
    }
    public Date getCreatedOn() {
        return createdOn;
    }
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    public Date getUpdatedOn() {
        return updatedOn;
    }
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
    public String getChannels() {
        return channels;
    }
    public void setChannels(String channels) {
        this.channels = channels;
    }
    public boolean isPrivUserCond() {
        return isPrivUserCond;
    }
    public void setPrivUserCond(boolean isPrivUserCond) {
        this.isPrivUserCond = isPrivUserCond;
    }
 
}
