package com.freecharge.useragent;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.eventprocessing.event.UserLoginEvent;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.util.RequestUtil;
import com.freecharge.web.util.WebConstants;

/**
 * User: abhi
 * Date: 8/11/13
 * Time: 2:07 PM
 */
@Component
public class UserLoginEventPublisherHelper {
    private Logger logger = LoggingFactory.getLogger(UserLoginEventPublisherHelper.class);

    @Autowired
    private FreechargeEventPublisher freechargeEventPublisher;

    /**
     * Publish the event.
     * @param request
     * @param source - Object from which this event originated.
     */
    public void publish(HttpServletRequest request, Object source) {
        if (MobileUtil.isMobile(request)) {
            Object userIdObject = FreechargeContextDirectory.get().getFreechargeSession().getSessionData().get(WebConstants.SESSION_USER_USERID);
            if (userIdObject != null) {
                UserLoginEvent userLoginEvent = new UserLoginEvent(source);
                userLoginEvent.setUserId((Integer)userIdObject);
                userLoginEvent.setUserAgent(RequestUtil.getUserAgent(request));
                freechargeEventPublisher.publish(userLoginEvent);
            } else {
                logger.error(UserLoginEvent.LOGGER_PREFIX + " - User id is null while trying to enqueue object for user agent processing");
            }
        }
    }
}
