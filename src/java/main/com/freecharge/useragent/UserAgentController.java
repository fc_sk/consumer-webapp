package com.freecharge.useragent;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.domain.dao.jdbc.UserSlaveDao;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCHttpUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.eventprocessing.event.UserLoginEvent;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.util.RequestUtil;
import com.google.common.io.ByteStreams;
import com.google.common.io.Resources;

/**
 * User: abhi
 * Date: 12/11/13
 * Time: 1:53 PM
 */
@Controller
@RequestMapping(UserAgentController.CONTROLLER_MAPPING)
/**
 * A controller to record user agents.
 */
public class UserAgentController {
    private Logger logger = LoggingFactory.getLogger(UserAgentController.class);

    public static final String CONTROLLER_MAPPING = "/useragent";

    public static final String METRIC_SERVICE_NAME = "userAgentProcessing";
    public static final String METRIC_NAME = "controllerHits";

    @Autowired
    private UserSlaveDao userSlaveDao;

    @Autowired
    private FreechargeEventPublisher freechargeEventPublisher;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private FCHttpUtil fcHttpUtil;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @NoLogin
    @Csrf(exclude=true)
    @RequestMapping(method = RequestMethod.GET)
    /**
     * Record user agent of the device which invoked this request. As a response, a transparent 1x1 png image is returned.
     */
    public void record(HttpServletRequest request, HttpServletResponse response) throws IOException {
        metricsClient.recordEvent(METRIC_SERVICE_NAME, METRIC_NAME, "hits");

        if (MobileUtil.isMobile(request)) {
            metricsClient.recordEvent(METRIC_SERVICE_NAME, METRIC_NAME, "mobileHits");
            UserLoginEvent userLoginEvent = new UserLoginEvent(this);
            userLoginEvent.setUserAgent(RequestUtil.getUserAgent(request));
            String email = request.getParameter("email");
            long userId = 0;
            try {
                userId = userServiceProxy.getUserIdByEmailId(email);
                userLoginEvent.setUserId((int)userId);
                freechargeEventPublisher.publish(userLoginEvent);
            } catch (EmptyResultDataAccessException e) {
                metricsClient.recordEvent(METRIC_SERVICE_NAME, METRIC_NAME, "emailNotPresentHits");
                logger.error(UserLoginEvent.LOGGER_PREFIX + " - Got an email address not present in db - " + email, e);
            }
        }

        //We do not want to cache these requests because our phone identification list keeps expanding
        fcHttpUtil.doNotCache(response);
        addNoCacheHeaders(response);

        response.setContentType("image/png");

        URL url = Resources.getResource("transparent.png");
        ByteStreams.copy(Resources.newInputStreamSupplier(url).getInput(), response.getOutputStream());
    }

    private String getRecordUrl() {
        return fcProperties.getFreechargeUrl() + UserAgentController.CONTROLLER_MAPPING + ".htm";
    }
    
    public String getRecordUrl(String email) {
        return this.getRecordUrl() + "?email=" + email;
    }

    private void addNoCacheHeaders(HttpServletResponse response) {
        //http://palizine.plynt.com/issues/2008Jul/cache-control-attributes/
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, private");
        response.setHeader("Pragma", "no-cache" );
    }

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserAgentController userAgentController = ctx.getBean(UserAgentController.class);
        System.out.println(userAgentController.getRecordUrl());
    }
}
