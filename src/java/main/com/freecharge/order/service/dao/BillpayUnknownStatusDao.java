package com.freecharge.order.service.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.BillpayUnknownStatus;

@Component
public class BillpayUnknownStatusDao {

	private static Logger              logger = LoggingFactory.getLogger(BillpayUnknownStatusDao.class);
    private NamedParameterJdbcTemplate jdbcTemplate;
//    private SimpleJdbcInsert           insertOrderData;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
//        insertOrderData = new SimpleJdbcInsert(dataSource).withTableName("billpay_unknown_status").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }
    
    public Integer insert(BillpayUnknownStatus object) {
    	
    	Integer count = -1;
		Integer id = new Integer(-1);
		try {
			final String queryString = "insert into billpay_unknown_status(bill_id,authenticator,amount,txn_date,is_active,status,order_id,agg_txn_id)"
					+ "values (:billId, :authenticator, :amount, :txnDate, :isActive, :status, :orderId, :agTxnId)";
			MapSqlParameterSource parameters = new MapSqlParameterSource()
					.addValue("billId", object.getBillId())
					.addValue("authenticator", object.getAuthenticator())
					.addValue("amount", object.getAmount())
					.addValue("txnDate", object.getTxnDate())
					.addValue("isActive", true)
					.addValue("status", "unknown")
					.addValue("orderId", object.getOrderId())
					.addValue("agTxnId", object.getAgTxnId());
			
			KeyHolder keyHolder = new GeneratedKeyHolder();
			
			count = jdbcTemplate.update(queryString, parameters, keyHolder, new String[]{"id"});
			id=new Integer(keyHolder.getKey().intValue());
		} catch (RuntimeException re) {
			logger.error("createEmail failed", re);
			return id;
		}
		return id;
    	
    }
    
    public boolean updateStatus(String billId, String status, Boolean active) {
		try {

			String queryString = "update billpay_unknown_status set status = :status, is_active= :isActive "
					+ " where bill_id=:billId";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("status", status);
			paramMap.put("isActive", active);
			paramMap.put("billId", billId);
			int result = this.jdbcTemplate.update(queryString, paramMap);
			if (result <= 0) {
				logger.error("No Rows affected on updating in_request for billId = "+billId);
				return false;
			}
			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			return false;
		}
	}
	public boolean updateStatus(String frgId) {
		try {

			String queryString = "update billpay_unknown_status set is_active= :isActive "
					+ " where agg_txn_id=:frgId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("isActive", 0);
			paramMap.put("frgId", frgId);
			int result = this.jdbcTemplate.update(queryString, paramMap);
			if (result <= 0) {
				logger.error("No Rows affected on updating in_request for frgId = "+frgId);
				return false;
			}
			return true;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			return false;
		}
	}

    public BillpayUnknownStatus getUnknownStatusTxn(String authenticator, String date) {
		try {
			final String queryString = "select * from billpay_unknown_status where authenticator = :authenticator and txn_date = :date and is_active = 1 and status = :status";
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("authenticator", authenticator);
			paramMap.put("date", date);
			paramMap.put("status", "unknown");

			List<BillpayUnknownStatus> codes = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(BillpayUnknownStatus.class));
			if(codes == null || codes.size() == 0){
				logger.error("no entries exist for authenticator : " + authenticator);
				return null;
			}
			if(codes.size() > 1){
				logger.info("more than one entry exist for authenticator : " + authenticator);
			}
			
			return codes.get(0);
		} catch (RuntimeException re) {
			logger.error("Exception in getUnknownStatusTxn by authenticator : " + authenticator + "for date: " + date, re);
			throw re;
		}
	}
    
    public List<BillpayUnknownStatus> getAllUnknownStatusTxn(String date) {
		try {
			final String queryString = "select * from billpay_unknown_status where txn_date = :date and is_active = 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("date", date);

			List<BillpayUnknownStatus> codes = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(BillpayUnknownStatus.class));
			if (codes == null || codes.size() == 0) {
				logger.error("no entries exist for date : " + date);
				return null;
			}
			if (codes.size() > 1) {
				logger.info("more than one entry exist for date : " + date);
			}

			return codes;
		} catch (RuntimeException re) {
			logger.error("Exception in getUnknownStatusTxn by date : " + date, re);
			throw re;
		}
	}
		public BillpayUnknownStatus getUnknownStatusTxn(String frgId) {
			try {
				final String queryString = "select * from billpay_unknown_status where agg_txn_id = :frgId and is_active = 1 and status = :status";

				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("frgId", frgId);
				paramMap.put("status", "unknown");

				List<BillpayUnknownStatus> codes = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(BillpayUnknownStatus.class));
				if(codes == null || codes.size() == 0){
					logger.error("no entries exist for agg_txn_id : " + frgId);
					return null;
				}
				if(codes.size() > 1){
					logger.info("more than one entry exist for agg_txn_id : " + frgId);
				}

				return codes.get(0);
			} catch (RuntimeException re) {
				logger.error("Exception in getUnknownStatusTxn by agg_txn_id : " + frgId, re);
				throw re;
			}
		}
	
}
