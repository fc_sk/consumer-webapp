package com.freecharge.order.service.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.order.service.model.OrderDataEntity;

@Component
public class OrderDataDao {
    private static Logger              logger = LoggingFactory.getLogger(OrderIdWriteDAO.class);
    private NamedParameterJdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert           insertOrderData;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        insertOrderData = new SimpleJdbcInsert(dataSource).withTableName("order_data").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public boolean create(OrderDataEntity orderDataEntity) {
//        try {
//            SqlParameterSource parameters = new BeanPropertySqlParameterSource(orderDataEntity);
//            insertOrderData.execute(parameters);
//            return true;
//        } catch (RuntimeException re) {
//            logger.error("Failed to save order_data for orderid : " + orderDataEntity.getOrderId(), re);
//            return false;
//        }
    	return true;
    }

    public String getMetaDataFromOrderDataTable(String orderId) {
        String metaData = null;
        try {
            String sql = "select meta_data from order_data where order_id= :order_id";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("order_id", orderId);
            List<Map<String, Object>> orderDataMapList = this.jdbcTemplate.queryForList(sql, paramMap);
            if (orderDataMapList != null && (!orderDataMapList.isEmpty())) {
                metaData = (String) orderDataMapList.get(0).get("meta_data");
            }
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting ipAddress for orderId from table txn_ipaddress : " + orderId,
                    e);
        }
        return metaData;
    }
}