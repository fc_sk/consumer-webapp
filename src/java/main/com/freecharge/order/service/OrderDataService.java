package com.freecharge.order.service;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.order.service.dao.OrderDataDao;
import com.freecharge.order.service.model.OrderData;
import com.freecharge.order.service.model.OrderDataEntity;
import com.freecharge.util.JsonUtil;

@Service
public class OrderDataService {
    private final Logger logger = LoggingFactory.getLogger(OrderDataService.class);

    @Autowired
    private OrderDataDao orderDataDao;

    public void insertIPInOrderDataTable(String orderId, String ip) {
        OrderData orderData = new OrderData();
        orderData.setIpAddress(ip);
        String metaJasonString = JsonUtil.getJson(orderData);
        OrderDataEntity orderDataEntity = new OrderDataEntity();
        orderDataEntity.setMetaData(metaJasonString);
        orderDataEntity.setOrderId(orderId);
        orderDataEntity.setCreatedAt(new Date());

        boolean result = orderDataDao.create(orderDataEntity);
        if (result) {
            logger.info("Successfully entered order_data table with orderid : " + orderId);
        }
    }

    public String getMetaDataFromOrderDataByOrderId(String orderId) {
        return orderDataDao.getMetaDataFromOrderDataTable(orderId);
    }

    public String getIpFromOrderDataByOrderId(String orderId) {
        String metaData = getMetaDataFromOrderDataByOrderId(orderId);
        if (metaData != null) {
            OrderData orderData = (OrderData) JsonUtil.getObject(metaData, OrderData.class);
            return orderData.getIpAddress();
        }
        return null;
    }
}