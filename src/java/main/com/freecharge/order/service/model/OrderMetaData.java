package com.freecharge.order.service.model;

import com.freecharge.common.util.FCUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OrderMetaData {
	private String orderId;
	private Date createdAt;
	private String userCookie;
	private String ipAddress;
	private String userEmail;
	private String serviceNo;
	private String profileNo;
	private String operatorName;
    private String opertorCircle;
	private String lookupId;
	private Integer userId;
	private Float rechargeAmount;
	private String productType;
	private String freefundCode;
    private String imeiNumber;
    private String advertisementId;
    private String deviceUniqueId;
    private String imsi;
    private Boolean isEmulator;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getUserCookie() {
		return userCookie;
	}

	public void setUserCookie(String userCookie) {
		this.userCookie = userCookie;
	}

	public String getLookupId() {
		return lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

    public String getOpertorCircle() {
        return opertorCircle;
    }

    public void setOpertorCircle(String opertorCircle) {
        this.opertorCircle = opertorCircle;
    }

    public Map<String, Object> asMap() {
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("userCookie", userCookie);
		dataMap.put("ipAddress", ipAddress);
		dataMap.put("createdAt", createdAt);
		dataMap.put("userEmail", userEmail);
		dataMap.put("serviceNo", serviceNo);
		dataMap.put("profileNo", profileNo);
		dataMap.put("lookupId", lookupId);
		dataMap.put("operatorName", operatorName);
        dataMap.put("operatorCircle", opertorCircle);
		dataMap.put("userId", userId);
		dataMap.put("rechargeAmount", rechargeAmount);
		dataMap.put("productType", productType);
		dataMap.put("freefundCode", freefundCode);
        dataMap.put("imeiNumber", imeiNumber);
        dataMap.put("advertisementId", advertisementId);
        dataMap.put("deviceUniqueId", deviceUniqueId);
        dataMap.put("imsi", imsi);
        dataMap.put("isEmulator", isEmulator);
		return dataMap;
	}

	public static OrderMetaData fromMap(Map<String, Object> dataMap) {
		OrderMetaData orderMetaData = new OrderMetaData();
		orderMetaData.setIpAddress(FCUtil.getStringValue(dataMap
				.get("ipAddress")));
		orderMetaData.setUserCookie(FCUtil.getStringValue(dataMap
				.get("userCookie")));
		orderMetaData.setCreatedAt((Date) dataMap.get("createdAt"));
		orderMetaData.setUserEmail(FCUtil.getStringValue(dataMap
				.get("userEmail")));
		orderMetaData.setServiceNo(FCUtil.getStringValue(dataMap
				.get("serviceNo")));
		orderMetaData.setProfileNo(FCUtil.getStringValue(dataMap
				.get("profileNo")));
		orderMetaData.setOperatorName(FCUtil.getStringValue(dataMap
				.get("operatorName")));
        orderMetaData.setOpertorCircle(FCUtil.getStringValue(dataMap
                .get("operatorCircle")));
		orderMetaData
				.setLookupId(FCUtil.getStringValue(dataMap.get("lookupId")));
		orderMetaData.setUserId(Integer.parseInt(FCUtil.getStringValue(dataMap
				.get("userId"))));
		orderMetaData.setRechargeAmount(Float.parseFloat(FCUtil
				.getStringValue(dataMap.get("rechargeAmount"))));
		orderMetaData.setProductType(FCUtil.getStringValue(dataMap
				.get("productType")));
		orderMetaData.setFreefundCode(FCUtil.getStringValue(dataMap
                .get("freefundCode")));
        orderMetaData.setImeiNumber(FCUtil.getStringValue(dataMap
                .get("imeiNumber")));
        orderMetaData.setAdvertisementId(FCUtil.getStringValue(dataMap
                .get("advertisementId")));
        orderMetaData.setDeviceUniqueId(FCUtil.getStringValue(dataMap
                .get("deviceUniqueId")));
        orderMetaData.setOrderId(FCUtil.getStringValue(dataMap
                .get("orderId")));
        orderMetaData.setImsi(FCUtil.getStringValue(dataMap
                .get("imsi")));
        orderMetaData.setIsEmulator((Boolean)(dataMap
                .get("isEmulator")));
        return orderMetaData;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getServiceNo() {
		return serviceNo;
	}

	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}

	public String getProfileNo() {
		return profileNo;
	}

	public void setProfileNo(String profileNo) {
		this.profileNo = profileNo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Float getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(Float rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public String getProductType() {
		return productType;
	}

	public String getFreefundCode() {
        	return freefundCode;
	}

	public void setFreefundCode(String freefundCode) {
        	this.freefundCode = freefundCode;
	}

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(String advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Boolean getIsEmulator() {
        return isEmulator;
    }

    public void setIsEmulator(Boolean isEmulator) {
        this.isEmulator = isEmulator;
    }
}
