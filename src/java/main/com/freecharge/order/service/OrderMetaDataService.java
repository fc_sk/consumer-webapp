package com.freecharge.order.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.mongo.repos.OrderMetaDataRepository;
import com.freecharge.order.service.model.OrderMetaData;

@Service
public class OrderMetaDataService implements IOrderMetaDataService {

	@Autowired
	private OrderMetaDataRepository orderMetaDataRepository;

	@Override
	public void saveOrderMetaData(OrderMetaData orderMetaData) {
		if (orderMetaData == null) {
			return;
		}
		Map<String, Object> dataMap = orderMetaData.asMap();
		orderMetaDataRepository.updateOrCreateOrderMetaData(orderMetaData.getOrderId(), dataMap);
		orderMetaDataRepository.updateOrCreateOrderMetaDataWithLookupId(orderMetaData.getLookupId(), dataMap);
	}

	@Override
	public OrderMetaData getOrderMetaData(String orderId) {
		Map<String, Object> dataMap = orderMetaDataRepository.getDataByOrderId(orderId);
		OrderMetaData orderMetaData = null;
		if (dataMap != null) {
			orderMetaData = OrderMetaData.fromMap(dataMap);
		}
		return orderMetaData;
	}
	
	@Override
	public OrderMetaData getOrderMetaDataUsingLookupId(String lookupId) {
		Map<String, Object> dataMap = orderMetaDataRepository.getDataByLookupId(lookupId);
		OrderMetaData orderMetaData = null;
		if (dataMap != null) {
			orderMetaData = OrderMetaData.fromMap(dataMap);
		}
		return orderMetaData;
	}

}
