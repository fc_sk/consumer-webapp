package com.freecharge.order.service;

import com.freecharge.order.service.model.OrderMetaData;

public interface IOrderMetaDataService {
	public void saveOrderMetaData(OrderMetaData orderMetaData);
	public OrderMetaData getOrderMetaData(String orderId);
	public OrderMetaData getOrderMetaDataUsingLookupId(String lookupId);
}
