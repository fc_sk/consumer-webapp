package com.freecharge.batcave.paymentsview.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.model.RewardHistoryWrapper;
import com.freecharge.batcave.mob.entities.Merchant;
import com.freecharge.batcave.paymentsview.service.PaymentsViewService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.customercare.service.CustomerTrailService;
import com.freecharge.tsm.service.TSMService;
import com.freecharge.views.GlobalTransactionView;
import com.freecharge.web.util.ViewConstants;
import com.snapdeal.payments.view.request.commons.response.GetRequestViewResponse;

@Controller("paymentsview")
@RequestMapping("/admin/customertrail/batcave/*")
public class PaymentsViewController extends BaseAdminController{

	private final Logger logger = LoggingFactory.getLogger(PaymentsViewController.class);
	
	@Autowired
	private PaymentsViewService paymentsViewService;
	
	@Autowired
	private TSMService tSMService;
	
	@Autowired
	private CustomerTrailService customerTrailService;

	
	@RequestMapping(value = "getusermerchanthistory/view", method = RequestMethod.GET)
	public String getUserMerchantHistory(@ModelAttribute("merchant") Merchant merchant, @RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
		
		logger.info("In Controller: Selected merchant id is -> " + merchant.getMerchantId());
		List<String> messages = new LinkedList<String>();
		String merchantId = "";
		if(merchant.getMerchantId()==null || "".equals(merchant.getMerchantId())) {
			messages.add("Please enter/select merchant id after entering merchant order id");
			model.addAttribute("messages", messages);
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		if(merchant.getMerchantId().indexOf('-') == -1){
			//no '-' found, this means user has entered the merchant id manually
			merchantId = merchant.getMerchantId();
		}else{
			merchantId = merchant.getMerchantId().split("-")[1];
		}
		
		String userMerchantHistoryResponse = paymentsViewService.getUserMerchantHistory(mapping.get("emailid"), merchantId);
		GetRequestViewResponse userMerchantHistoryResponseObject = null;
		//convert string to list of RVTransactionDto
		ObjectMapper mapper = new ObjectMapper();
		try {
			userMerchantHistoryResponseObject = mapper.readValue(userMerchantHistoryResponse, new TypeReference<GetRequestViewResponse>(){});
		} catch (IOException e) {
			logger.error("Exception occured while using object mapper in converting object string to object ", e);
		}
		model.addAttribute("userMerchantHistory", userMerchantHistoryResponseObject);
		model.addAttribute("emailid",mapping.get("emailid"));
		
		return ViewConstants.USER_HISTORY_BY_MERCHANT_VIEW;
	}
	
	@RequestMapping(value = "getdetailsbymercahntorderid/view", method = RequestMethod.GET)
	public String getDetailsByMerchantOrderId(@ModelAttribute("merchant") Merchant merchant, @RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
		
		logger.info("In Controller: Selected merchant id is -> " + merchant.getMerchantId() +  " and order id is -> " + mapping.get("orderid"));
		
		List<String> messages = new LinkedList<String>();
		String merchantId = "";
		if(merchant.getMerchantId()==null || "".equals(merchant.getMerchantId())) {
			messages.add("Please enter/select merchant id after entering merchant order id");
			model.addAttribute("messages", messages);
			return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		}
		if(merchant.getMerchantId().indexOf('-') == -1){
			//no '-' found, this means user has entered the merchant id manually
			merchantId = merchant.getMerchantId();
		}else{
			merchantId = merchant.getMerchantId().split("-")[1];
		}
		
		//get the global transaction id from order id using payments view
		String globalTxnId = paymentsViewService.getGlobalTransaction(mapping.get("orderid"), merchantId);
		logger.info("Global transaction received for the order id - " + mapping.get("orderid") + " is - " + globalTxnId);
		model.addAttribute("orderid",mapping.get("orderid"));
		model.addAttribute("globaltxnid",globalTxnId);
		
		if(globalTxnId!=null){
			//use global txn id to get the transaction details from TSM
			//call TSM Service
        	List<GlobalTransactionView> globalTransactionViewList = tSMService.getCompleteTransactionView(globalTxnId);
        	
        	if(globalTransactionViewList.size()!=0){
        		model.addAttribute("globalTransactionViewList", globalTransactionViewList);
        		//integrating campaign history with TSM
        		RewardHistoryWrapper rewardHistoryWrapper = customerTrailService.getRewardHistoryListFromOrderId(globalTxnId);
        		model.addAttribute("rewardHistoryWrapper", rewardHistoryWrapper);
        		logger.info("Reward History List received for TSM: " + rewardHistoryWrapper);
        		return ViewConstants.SD_CUSTOMER_TRAIL_LISTING;	
        	}    	
		}
		//Offline/Online transactions not found
    	//No transaction found anywhere corresponding to this global txn id => No transaction found for order id
		messages.add("Sorry, No transaction exists for the given orderid!");
		model.addAttribute("messages", messages);
		return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
	}
}


	
