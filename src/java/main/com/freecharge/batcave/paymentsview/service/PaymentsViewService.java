package com.freecharge.batcave.paymentsview.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.BatcaveConstants;
import com.snapdeal.payments.view.merchant.commons.response.GetTransactionsResponse;

@Service("paymentsViewService")
public class PaymentsViewService {

	private static Logger	logger	=	LoggingFactory.getLogger(PaymentsViewService.class);
	
	private static final String paymentsViewUserMerchantHistoryUri = "/paymentsview/paymentshistory";
	private static final String paymentsViewTransactionDetailsURI = "/paymentsview/transactiondetails";
	private static final String forwardSlash ="/";
	
	@Autowired
	protected FCProperties fcProperties;
	
	public String getUserMerchantHistory(String emailId, String merchantId){
		
		//2 arguments for this REST call
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		restArg.append(forwardSlash).append(emailId);
		restArg.append(forwardSlash).append(merchantId);
		restUri.append(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI)).append(paymentsViewUserMerchantHistoryUri).append(restArg);
		
		logger.info("Payments View get User Merchant History URI is : " + restUri);
		RestTemplate restTemplate = new RestTemplate();
		String getRequestViewResponseString = "";
		try {
			getRequestViewResponseString = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("getRequestViewResponseString: " + getRequestViewResponseString + " received from batcave Payments View service");	
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Exception while fetching user - merchant history: ",e);
		}
		return getRequestViewResponseString;	
	}
	
	public String getGlobalTransaction(String orderId, String merchantId){
		
		//2 arguments for this REST call
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		restArg.append(forwardSlash).append(orderId);
		restArg.append(forwardSlash).append(merchantId);
		restUri.append(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI)).append(paymentsViewTransactionDetailsURI).append(restArg);
		
		logger.info("Payments View get Transaction Details from Order Id URI is : " + restUri);
		RestTemplate restTemplate = new RestTemplate();
		String getMerchantViewResponseString = "";
		try {
			getMerchantViewResponseString = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("getMerchantViewResponseString: " + getMerchantViewResponseString + " received from batcave Payments View service");	
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Exception while fetching transaction details from order id: ",e);
		}
		 
		//Its now time to transform the object string into object
		GetTransactionsResponse transactionDetailsResponseObject= null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			transactionDetailsResponseObject = mapper.readValue(getMerchantViewResponseString, new TypeReference<GetTransactionsResponse>(){});
		} catch (IOException e) {
			logger.error("Exception occured while using object mapper in converting object string to object ", e);
		}
		
		logger.info("The transaction details after transformation into object is - " + transactionDetailsResponseObject);
		if(null!=transactionDetailsResponseObject && null!=transactionDetailsResponseObject.getMvTransactions() && transactionDetailsResponseObject.getMvTransactions().size()!=0){
			if(null!=transactionDetailsResponseObject.getMvTransactions().get(0)) return transactionDetailsResponseObject.getMvTransactions().get(0).getFcTxnId();
		}
		return null;	
	}
}
