package com.freecharge.batcave;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.batcave.entity.vcrecon.VCTransactionByARNWrapper;
import com.freecharge.batcave.entity.vcrecon.VCTransactionByCardHashIdWrapper;
import com.freecharge.batcave.entity.vcrecon.VCTransactionByRefNoWrapper;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.BatcaveConstants;


@Service("vcReconService")
public class VCReconService {

	@Autowired
	private FCProperties fcProperties;

	private static final Logger logger = LoggerFactory.getLogger(VCReconService.class);

	public VCTransactionByRefNoWrapper getTransactionDetailsByTransactionRefNo(String transactionRefNo) {

		StringBuilder restUri = new StringBuilder(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI));
		restUri.append(fcProperties.getProperty(BatcaveConstants.VCRECON_BY_TRANSCTION_REF_NO_URI));
		restUri.append(transactionRefNo);

		logger.info("Fetch vcTransaction by transactionRefNo over URI: " + restUri);

		VCTransactionByRefNoWrapper vcTransactionWrapper = new VCTransactionByRefNoWrapper();

		try {
			ObjectMapper mapper = new ObjectMapper();
			vcTransactionWrapper = mapper.readValue(new URL(restUri.toString()), VCTransactionByRefNoWrapper.class);

		} catch (Exception e) {
			vcTransactionWrapper.setFinalStatus(BatcaveConstants.FAILURE, "RUNTIME EXCEPTION", e.getMessage()); 
			logger.error("Exception thown while fetching vcTransaction by transactionRefNo over URI: " + restUri, e);
		}

		logger.info("VCTxnDetails : " + vcTransactionWrapper);
		return vcTransactionWrapper;
	}


	public VCTransactionByARNWrapper getTransactionDetailsByARN(String acquirerRefNo) {

		StringBuilder restUri = new StringBuilder(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI));
		restUri.append(fcProperties.getProperty(BatcaveConstants.VCRECON_BY_ARN_URI));
		restUri.append(acquirerRefNo);

		logger.info("Fetch vcTransaction by acquirerRefNo over URI: " + restUri);

		VCTransactionByARNWrapper vcTransactionWrapper = new VCTransactionByARNWrapper();

		try {
			ObjectMapper mapper = new ObjectMapper();
			vcTransactionWrapper = mapper.readValue(new URL(restUri.toString()), VCTransactionByARNWrapper.class);

		} catch (Exception e) {
			vcTransactionWrapper.setFinalStatus(BatcaveConstants.FAILURE, "RUNTIME EXCEPTION", e.getMessage()); 
			logger.error("Exception thown while fetching vcTransaction by acquirerRefNo over URI: " + restUri, e);
		}

		return vcTransactionWrapper;
	}

	public VCTransactionByCardHashIdWrapper getTransactionDetailsByCardHashId(String cardHashId, String from, String to) {

		StringBuilder restUri = new StringBuilder(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI));
		restUri.append(fcProperties.getProperty(BatcaveConstants.VCRECON_BY_CARD_HASH_ID_URI));
		restUri.append(cardHashId);

		logger.info("Fetch vcTransaction by cardHashId over URI: " + restUri);

		VCTransactionByCardHashIdWrapper vcTransactionWrapper = new VCTransactionByCardHashIdWrapper();

		try {

			URIBuilder uri = new URIBuilder(restUri.toString());
			uri.addParameter(BatcaveConstants.FROM_DATE, from);
			uri.addParameter(BatcaveConstants.TO_DATE, to);
			ObjectMapper mapper = new ObjectMapper();
			vcTransactionWrapper = mapper.readValue(uri.build().toURL(), VCTransactionByCardHashIdWrapper.class);

		} catch (Exception e) {
			vcTransactionWrapper.setFinalStatus(BatcaveConstants.FAILURE, "RUNTIME EXCEPTION", e.getMessage()); 
			logger.error("Exception thown while fetching vcTransaction by cardHashId  over URI: " + restUri, e);
		}

		return vcTransactionWrapper;

	}
}