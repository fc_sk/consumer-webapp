package com.freecharge.batcave.mob.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.batcave.mob.entities.Merchant;
import com.freecharge.batcave.mob.service.MOBService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;
import com.google.gson.Gson;

@Controller("mob")
@RequestMapping("/admin/customertrail/batcave/*")
public class MOBController extends BaseAdminController {
	
	private final Logger logger = LoggingFactory.getLogger(MOBController.class);
	
	@Autowired
	private MOBService mobService;
	
	@RequestMapping(value = "getallmerchants/view", method = RequestMethod.GET)
    public String getCustomerTrail(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
				
		List<String> merchantList = mobService.getAllMerchants();
		List<String> messages = new LinkedList<String>();
		
		logger.info("Successfully received merchant list from Mob Service using Batcave with size = " +  merchantList.size());
		//MOB Service always return non null wrapper
		//converting list to json
		String jsonMerchantList = new Gson().toJson(merchantList);
		model.addAttribute("jsonmerchantlist", jsonMerchantList);
		Merchant merchant = new Merchant();
		model.addAttribute(merchant);
		if(mapping.get("emailid")!=null){
			model.addAttribute("emailid",mapping.get("emailid"));
			return ViewConstants.USER_MERCHANT_DETAILS_INPUT_PAGE;
		}else if(mapping.get("orderid")!=null){
			model.addAttribute("orderid",mapping.get("orderid"));
			return ViewConstants.USER_MERCHANT_DETAILS_INPUT_PAGE;
		}
		messages.add("Please enter a value and try again.");
		model.addAttribute("messages", messages);
		return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;	
	}
}
