package com.freecharge.batcave.mob.entities;

public class MerchantDetails {
	private String email;
	private String merchantName;
	private String businessCategory;
	private String tin;
	private String businessType;
	private String merchantStatus;
	private String mobileNumber;
	private String merchantId;
	private String integrationMode;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getBusinessCategory() {
		return businessCategory;
	}
	public void setBusinessCategory(String businessCategory) {
		this.businessCategory = businessCategory;
	}
	public String getTin() {
		return tin;
	}
	public void setTin(String tin) {
		this.tin = tin;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getMerchantStatus() {
		return merchantStatus;
	}
	public void setMerchantStatus(String merchantStatus) {
		this.merchantStatus = merchantStatus;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getIntegrationMode() {
		return integrationMode;
	}
	public void setIntegrationMode(String integrationMode) {
		this.integrationMode = integrationMode;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((businessCategory == null) ? 0 : businessCategory.hashCode());
		result = prime * result + ((businessType == null) ? 0 : businessType.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((integrationMode == null) ? 0 : integrationMode.hashCode());
		result = prime * result + ((merchantId == null) ? 0 : merchantId.hashCode());
		result = prime * result + ((merchantName == null) ? 0 : merchantName.hashCode());
		result = prime * result + ((merchantStatus == null) ? 0 : merchantStatus.hashCode());
		result = prime * result + ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
		result = prime * result + ((tin == null) ? 0 : tin.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchantDetails other = (MerchantDetails) obj;
		if (businessCategory == null) {
			if (other.businessCategory != null)
				return false;
		} else if (!businessCategory.equals(other.businessCategory))
			return false;
		if (businessType == null) {
			if (other.businessType != null)
				return false;
		} else if (!businessType.equals(other.businessType))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (integrationMode == null) {
			if (other.integrationMode != null)
				return false;
		} else if (!integrationMode.equals(other.integrationMode))
			return false;
		if (merchantId == null) {
			if (other.merchantId != null)
				return false;
		} else if (!merchantId.equals(other.merchantId))
			return false;
		if (merchantName == null) {
			if (other.merchantName != null)
				return false;
		} else if (!merchantName.equals(other.merchantName))
			return false;
		if (merchantStatus == null) {
			if (other.merchantStatus != null)
				return false;
		} else if (!merchantStatus.equals(other.merchantStatus))
			return false;
		if (mobileNumber == null) {
			if (other.mobileNumber != null)
				return false;
		} else if (!mobileNumber.equals(other.mobileNumber))
			return false;
		if (tin == null) {
			if (other.tin != null)
				return false;
		} else if (!tin.equals(other.tin))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "MerchantDetails [email=" + email + ", merchantName=" + merchantName + ", businessCategory="
				+ businessCategory + ", tin=" + tin + ", businessType=" + businessType + ", merchantStatus="
				+ merchantStatus + ", mobileNumber=" + mobileNumber + ", merchantId=" + merchantId
				+ ", integrationMode=" + integrationMode + "]";
	}	
}
