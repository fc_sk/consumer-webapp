package com.freecharge.batcave.mob.entities;

import java.util.List;

public class MerchantListWrapper {
	
	private List<MerchantDetails> merchantList;

	public List<MerchantDetails> getMerchantList() {
		return merchantList;
	}

	public void setMerchantList(List<MerchantDetails> merchantList) {
		this.merchantList = merchantList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((merchantList == null) ? 0 : merchantList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchantListWrapper other = (MerchantListWrapper) obj;
		if (merchantList == null) {
			if (other.merchantList != null)
				return false;
		} else if (!merchantList.equals(other.merchantList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MerchantListWrapper [merchantList=" + merchantList + "]";
	}
	
	
	
}
