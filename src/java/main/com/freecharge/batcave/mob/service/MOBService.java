package com.freecharge.batcave.mob.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.batcave.mob.entities.MerchantDetails;
import com.freecharge.batcave.mob.entities.MerchantListWrapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.BatcaveConstants;

@Service("mobService")
public class MOBService {
	
	private static Logger	logger	=	LoggingFactory.getLogger(MOBService.class);
	
	private static final String mobAllMerchantsUri = "/mob/getallmerchants";
	
	@Autowired
	protected FCProperties fcProperties;
			
	public List<String> getAllMerchants(){
		
		logger.info("Fetching the List of All Merchants from Batcave");

		//No arguments for this REST call
		//StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		restUri.append(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI)).append(mobAllMerchantsUri);
		
		logger.info("MOB get all merchants URI: " + restUri);
		
		RestTemplate restTemplate = new RestTemplate();
		MerchantListWrapper merchantListWrapper = new MerchantListWrapper();
		List<MerchantDetails> tempMerchantList = null;
		String merchantListString = null;
		
		try {
			merchantListString = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("merchantListWrapperString: " + merchantListString + " received from batcave mob service");		
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Exception while fetching list of all merchants: ",e);
		} catch (Exception f){
			logger.error("Exception while fetching list of all merchants: ",f);
		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			tempMerchantList = mapper.readValue(merchantListString, new TypeReference<List<MerchantDetails>>(){});
		} catch (IOException e) {
			logger.error("Exception occured while using object mapper in converting object string to object ", e);
		}
		merchantListWrapper.setMerchantList(tempMerchantList);
		//covert list of merchant details into list of Merchant name; Merchant id
		List<String> customizedMerchantList = getCustomisedMerchantDetails(merchantListWrapper);
	
		//sort the list
		Collections.sort(customizedMerchantList);
		return customizedMerchantList; 
	}

	private List<String> getCustomisedMerchantDetails(MerchantListWrapper tempMerchantListWrapper) {
		List<String> modifiedMerchantDetailsList = new ArrayList<String>();
		if(null != tempMerchantListWrapper && null !=tempMerchantListWrapper.getMerchantList()){
		List<MerchantDetails> merchantDetailsList = (List<MerchantDetails>) tempMerchantListWrapper.getMerchantList();
			for(MerchantDetails merchantDetails: merchantDetailsList){
				modifiedMerchantDetailsList.add(merchantDetails.getMerchantName() + "-" + merchantDetails.getMerchantId());
			}
		}
		return modifiedMerchantDetailsList;
	}
}
