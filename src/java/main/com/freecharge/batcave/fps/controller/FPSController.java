package com.freecharge.batcave.fps.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.batcave.fps.service.FPSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.util.ViewConstants;
import com.snapdeal.payments.fps.aht.model.GetUserTransactionDetailsResponse;

@Controller("fps")
@RequestMapping("/admin/customertrail/batcave/*")
public class FPSController extends BaseAdminController{

	private final Logger logger = LoggingFactory.getLogger(FPSController.class);
	
	@Autowired
	private FPSService fpsService;
	
	@RequestMapping(value = "getusertransactiondetails", method = RequestMethod.GET)
	public String getUserTransactionDetails(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
		
		logger.info("In FPS Controller: Entered email id is -> " + mapping.get("emailid"));
		List<String> messages = new LinkedList<String>();
		String getUserTransactionDetailsResponse = fpsService.getUserTransactionDetails(mapping.get("emailid"));
		GetUserTransactionDetailsResponse getUserTransactionDetailsResponseObject = null;
		
		//convert string to list of RVTransactionDto
		ObjectMapper mapper = new ObjectMapper();
		try {
			getUserTransactionDetailsResponseObject = mapper.readValue(getUserTransactionDetailsResponse, new TypeReference<GetUserTransactionDetailsResponse>(){});
		} catch (IOException e) {
			logger.error("Exception occured while using object mapper in converting object string to object ", e);
		}
		if(null!=getUserTransactionDetailsResponseObject && null!=getUserTransactionDetailsResponseObject.getListOfTransactions() && getUserTransactionDetailsResponseObject.getListOfTransactions().size() > 0){
			model.addAttribute("usertransactiondetails", getUserTransactionDetailsResponseObject.getListOfTransactions());
			model.addAttribute("emailid",mapping.get("emailid"));
			return ViewConstants.FPS_USER_TRANSACTION_DETAILS;
		}
		model.addAttribute("emailid",mapping.get("emailid"));
		
        messages.add("No transactions found in FPS for the given user");
        model.addAttribute("messages", messages);
		return ViewConstants.CUSTOMER_TRAIL_HOME_VIEW;
		
	}
}


	
