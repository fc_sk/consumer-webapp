package com.freecharge.batcave.fps.service;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.BatcaveConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.snapdeal.ims.response.GetUserResponse;

@Service("fpsService")
public class FPSService {

	private static Logger	logger	=	LoggingFactory.getLogger(FPSService.class);
	private static final String fpsGetUserDetailsUri = "fps/getusertransactiondetails";
	private static final String forwardSlash ="/";
	
	@Autowired
	protected FCProperties fcProperties;
	
	@Autowired
    private OneCheckWalletService oneCheckWalletService;
	
	public String getUserTransactionDetails(String emailId) {
		
		//try getting email id from ims assuming email id is user id
		GetUserResponse userDetailsResponse = oneCheckWalletService
				.getUserByID(emailId, null, null);
		logger.info("User details are -> " + userDetailsResponse);
		if (userDetailsResponse != null) {
			if (userDetailsResponse.getUserDetails() != null) {
				emailId = userDetailsResponse.getUserDetails().getEmailId();
			}
		}
		
		//1 argument for this REST call
		StringBuilder restArg = new StringBuilder();
		StringBuilder restUri = new StringBuilder();
		restArg.append(forwardSlash).append(emailId);
		restArg.append(forwardSlash);
		restUri.append(fcProperties.getProperty(BatcaveConstants.BATCAVE_HOST_URI)).append(fpsGetUserDetailsUri).append(restArg);
		
		logger.info("FPS get User Transaction Details URI is : " + restUri);
		RestTemplate restTemplate = new RestTemplate();
		String getUserTransactionDetailsResponseString = "";
		try {
			getUserTransactionDetailsResponseString = restTemplate.getForObject(new URI(restUri.toString()), String.class);
			logger.info("getUserTransactionDetailsResponseString: " + getUserTransactionDetailsResponseString + " received from batcave FPS service");	
		} catch (RestClientException | URISyntaxException e) {
			logger.error("Exception while fetching fps user transaction details::",e);
		}
		return getUserTransactionDetailsResponseString;	
	}
}
