package com.freecharge.amazon.factory;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;

public class AwsFactory {
	
	public static AmazonDynamoDBAsyncClient getDDBClient(BasicAWSCredentials awsCredentials, String regionName) {
		AmazonDynamoDBAsyncClient dynamoDBClient = (AmazonDynamoDBAsyncClient) AmazonDynamoDBAsyncClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(regionName).build();
		return dynamoDBClient;
	}
	
	public static AmazonSQSAsyncClient getSQSClient(BasicAWSCredentials awsCredentials, String regionName) {
		AmazonSQSAsyncClient amazonSQSAsyncClient = (AmazonSQSAsyncClient) AmazonSQSAsyncClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(regionName).build();
		return amazonSQSAsyncClient;
	}
}
