package com.freecharge.mgage.service;

public class MgageResponse {

	private String requestId;
	private String dest;
	private String status;
	private String reason;
	private String homeCountry;
	private String homeOperator;
	private String homeCircle;
	private String portingStatus;
	private String roamingStatus;
	private String roamingCountry;
	private String roamingOperator;
	private String roamingCircle;
	private String ncprRegistered;
	private String ncprPreference;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getHomeCountry() {
		return homeCountry;
	}
	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}
	public String getHomeOperator() {
		return homeOperator;
	}
	public void setHomeOperator(String homeOperator) {
		this.homeOperator = homeOperator;
	}
	public String getHomeCircle() {
		return homeCircle;
	}
	public void setHomeCircle(String homeCircle) {
		this.homeCircle = homeCircle;
	}
	public String getPortingStatus() {
		return portingStatus;
	}
	public void setPortingStatus(String portingStatus) {
		this.portingStatus = portingStatus;
	}
	public String getRoamingStatus() {
		return roamingStatus;
	}
	public void setRoamingStatus(String roamingStatus) {
		this.roamingStatus = roamingStatus;
	}
	public String getRoamingCountry() {
		return roamingCountry;
	}
	public void setRoamingCountry(String roamingCountry) {
		this.roamingCountry = roamingCountry;
	}
	public String getRoamingOperator() {
		return roamingOperator;
	}
	public void setRoamingOperator(String roamingOperator) {
		this.roamingOperator = roamingOperator;
	}
	public String getRoamingCircle() {
		return roamingCircle;
	}
	public void setRoamingCircle(String roamingCircle) {
		this.roamingCircle = roamingCircle;
	}
	public String getNcprRegistered() {
		return ncprRegistered;
	}
	public void setNcprRegistered(String ncprRegistered) {
		this.ncprRegistered = ncprRegistered;
	}
	public String getNcprPreference() {
		return ncprPreference;
	}
	public void setNcprPreference(String ncprPreference) {
		this.ncprPreference = ncprPreference;
	}
	@Override
	public String toString() {
		return "MgageResponse [requestId=" + requestId + ", dest=" + dest + ", status=" + status + ", reason=" + reason
				+ ", homeCountry=" + homeCountry + ", homeOperator=" + homeOperator + ", homeCircle=" + homeCircle
				+ ", portingStatus=" + portingStatus + ", roamingStatus=" + roamingStatus + ", roamingCountry="
				+ roamingCountry + ", roamingOperator=" + roamingOperator + ", roamingCircle=" + roamingCircle
				+ ", ncprRegistered=" + ncprRegistered + ", ncprPreference=" + ncprPreference + "]";
	}
	
	
	
}
