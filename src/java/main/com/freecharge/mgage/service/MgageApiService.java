package com.freecharge.mgage.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.hlr.service.HLRResponse;
import com.freecharge.hlr.service.HLRService;
import com.freecharge.hlr.service.HLRService.HLR_SERVICE;
import com.freecharge.hlr.service.HLRService.STATUS;
import com.freecharge.hlr.service.HLRServiceInterface;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.fulfillment.db.HLRResponseDAO;
import com.google.common.collect.ImmutableMap;

@Service
public class MgageApiService implements HLRServiceInterface {
    private Logger              logger               = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private EmailService          emailService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private HLRResponseDAO hlrResponseDao;

    private Map<String, String> mgageCircleMapping   = ImmutableMap.<String, String> builder()
                                                             .put("AP", "Andhra Pradesh").put("Assam", "Assam")
                                                             .put("Bihar", "Bihar").put("Chennai", "Chennai")
                                                             .put("Delhi", "Delhi").put("Gujarat", "Gujarat")
                                                             .put("Haryana", "Haryana").put("HP", "Himachal Pradesh")
                                                             .put("J&K", "JK").put("Karnataka", "Karnataka")
                                                             .put("Kerala", "Kerala").put("Kolkata", "Kolkata")
                                                             .put("Maharashtra", "Maharashtra")
                                                             .put("MP", "Madhya Pradesh").put("Mumbai", "Mumbai")
                                                             .put("nil", "Nil").put("North East", "North East")
                                                             .put("Orissa", "Orissa").put("Punjab", "Punjab")
                                                             .put("Rajasthan", "Rajasthan")
                                                             .put("Tamilnadu", "Tamil Nadu")
                                                             .put("UP (East)", "Uttar Pradesh (E)")
                                                             .put("UP (West)", "Uttar Pradesh (W)")
                                                             .put("West Bengal", "West Bengal").build();

    private Map<String, String> mgageOperatorMapping = ImmutableMap.<String, String> builder()
                                                             .put("Vodafone", "Vodafone").put("Airtel", "Airtel")
                                                             .put("BSNL", "BSNL").put("TTSL", "Tata Docomo GSM")
                                                             .put("nil", "Nil").put("MTS", "MTS")
                                                             .put("Reliance", "Reliance-GSM").put("Aircel", "Aircel")
                                                             .put("Uninor", "Uninor").put("IDEA", "Idea")
                                                             .put("Videocon", "Videocon Mobile").put("MTNL","MTNL Delhi").put("Jio","JIO").build();

    public MgageResponse getMgageResponse(String serviceNumber) {

        String url = FCConstants.MGAGE_URL;
        HttpClient client = new HttpClient();

        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();

        connectionParams.setConnectionTimeout(2000);
        connectionParams.setSoTimeout(15000);
        client.getHttpConnectionManager().setParams(connectionParams);

        GetMethod getMethod = new GetMethod(url);

        String username = FCConstants.MGAGE_USERNAME;
        String pwd = FCConstants.MGAGE_PASSWORD;

        getMethod.setQueryString(new NameValuePair[] { new NameValuePair("uname", username),
                new NameValuePair("pass", pwd), new NameValuePair("dest", serviceNumber) });
        long start = System.currentTimeMillis();
        try {
            int response = client.executeMethod(getMethod);

            if (response != HttpStatus.SC_OK) {
                logger.error("Got Http status call " + response);
                return null;
            }

            String resp = getMethod.getResponseBodyAsString();
            logger.info("Got MGage Response for " + serviceNumber + " is " + resp);

            if(resp.contains("SMS Credits have Expired"))
            {
                sendEmailAlert(resp);
                return null;
            }
           
            if (resp == null) {
                return null;
            }

            String[] responseString = resp.split("\\n");
            int length = responseString.length;

            if (length < 3) {
                logger.error("Invalid response from mgage for " + serviceNumber);
                return null;
            }

            Map<String, String> keyvalueResponse = new HashMap<>();
            for (int i = 0; i < length; i++) {
                String[] keyValue = responseString[i].split(":");
                if (keyValue.length == 2) {
                    keyvalueResponse.put(keyValue[0], keyValue[1]);
                } else {
                    keyvalueResponse.put(keyValue[0], "");
                }
            }
            MgageResponse mgageResponse = new MgageResponse();
            if ("SUCCESS".equals(keyvalueResponse.get(MgageConstants.STATUS))) {
                mgageResponse.setRequestId(keyvalueResponse.get(MgageConstants.REQUEST_ID));
                mgageResponse.setDest(keyvalueResponse.get(MgageConstants.DEST));
                mgageResponse.setStatus(keyvalueResponse.get(MgageConstants.STATUS));
                mgageResponse.setReason(keyvalueResponse.get(MgageConstants.REASON));
                mgageResponse.setHomeCountry(keyvalueResponse.get(MgageConstants.HOME_COUNTRY));
                mgageResponse.setHomeOperator(keyvalueResponse.get(MgageConstants.HOME_OPERATOR));
                mgageResponse.setHomeCircle(keyvalueResponse.get(MgageConstants.HOME_CIRCLE));
                mgageResponse.setPortingStatus(keyvalueResponse.get(MgageConstants.PORTING_STATUS));
                mgageResponse.setRoamingStatus(keyvalueResponse.get(MgageConstants.ROAMING_STATUS));
                mgageResponse.setRoamingCountry(keyvalueResponse.get(MgageConstants.ROAMING_COUNTRY));
                mgageResponse.setRoamingOperator(keyvalueResponse.get(MgageConstants.ROAMING_OPERATOR));
                mgageResponse.setRoamingCircle(keyvalueResponse.get(MgageConstants.ROAMING_CIRCLE));

            } else {
                mgageResponse.setRequestId(keyvalueResponse.get(MgageConstants.REQUEST_ID));
                mgageResponse.setReason(keyvalueResponse.get(MgageConstants.REASON));
                mgageResponse.setStatus(keyvalueResponse.get(MgageConstants.STATUS));
            }
            return mgageResponse;
        } catch (IOException e) {
            logger.error("Caught exception on trying to get operator from Mgage for " + serviceNumber, e);
        } finally {
            logger.info("Total time for Mgage call is " + (System.currentTimeMillis() - start));
        }
        return null;
    }

    private void sendEmailAlert(String response) {
       EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
       emailBusinessDO.setSubject(MgageConstants.BALANCE_EXPIRED);
       emailBusinessDO.setTo(MgageConstants.EMAIL_TO);
       emailBusinessDO.setFrom(FCConstants.OPERATOR_ALERT_SUCCESS_FROM);
       emailBusinessDO.setCc(MgageConstants.EMAIL_CC);
       emailBusinessDO.setContent(response);
       
       emailService.sendEmail(emailBusinessDO);
        
    }

    public String getFCOperator(String operator) {
        if (mgageOperatorMapping.containsKey(operator))
            return mgageOperatorMapping.get(operator);
        else
            return null;
    }

    public String getFCCircle(String circle) {
        if (mgageCircleMapping.containsKey(circle))
            return mgageCircleMapping.get(circle);
        else
            return null;
    }

    public static void main(String args[]) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("commonApplicationContext.xml");
        MgageApiService service = (MgageApiService) ctx.getBean("mgageApiService");
        HLRResponse response = service.getHLRResponse("9051543117", "X", 10, 14);
        System.out.println(response);

    }

	@Override
	public HLRResponse getHLRResponse(String serviceNumber, String orderId, Integer operatorId, Integer circleId) {
		HLRResponse hlrResponse = null;

		metricsClient.recordCount("HLRService", "MgageAPIService.RequestCount", 1);
		MgageResponse mgageResponse = getMgageResponse(serviceNumber);
		logger.info("MgageResponse  = " + mgageResponse);
		if (mgageResponse != null && mgageResponse.getStatus() != null
				&& "SUCCESS".equalsIgnoreCase(mgageResponse.getStatus().trim())) {
			hlrResponse = new HLRResponse();
			hlrResponse.setOperator(getFCOperator(mgageResponse.getHomeOperator()));
			hlrResponse.setCircle(getFCCircle(mgageResponse.getHomeCircle()));
			hlrResponse.setErrorCode(HLRService.STATUS.SUCCESS.getStatusId());
			try {
				hlrResponseDao.logHLRSuccess(hlrResponse, mgageResponse.getRequestId(), serviceNumber, orderId,
						operatorId, circleId, STATUS.SUCCESS, HLR_SERVICE.MGAGE);
			} catch (Exception e) {
				logger.error("Error in logging MgageResponse for orderId:" + orderId, e);
			}
			return hlrResponse;
		}
		try{
		if(mgageResponse!=null){
			hlrResponse = new HLRResponse();
			hlrResponse.setErrorCode(HLRService.STATUS.FAILURE.getStatusId());
			hlrResponse.setErrorMsg(mgageResponse.getReason());
			hlrResponseDao.logHLRFailure(mgageResponse.getRequestId(),mgageResponse.getReason(),serviceNumber,orderId,operatorId,circleId,STATUS.FAILURE,HLR_SERVICE.MGAGE);
		}else{
			hlrResponseDao.logHLRTimeout(orderId,serviceNumber,operatorId,circleId,STATUS.TIMEOUT,HLR_SERVICE.MGAGE);
		}
		} catch (Exception e) {
			logger.error("Error in logging MgageResponse for orderId:" + orderId, e);
		}
		logger.error("Mgage response for " + serviceNumber + " and " + orderId + " is " + mgageResponse);
		metricsClient.recordCount("HLRService", "MgageAPIService.ErrorCount", 1);
		
		return hlrResponse;
	}
}
