package com.freecharge.mgage.service;

public class MgageConstants {

	public static final String REQUEST_ID = "Request ID";
	public static final String DEST = "Dest";
	public static final String STATUS = "Status";
	public static final String REASON = "Reason";
	public static final String HOME_COUNTRY = "HomeCountry";
	public static final String HOME_OPERATOR = "HomeOperator";
	public static final String HOME_CIRCLE = "HomeCircle";
	public static final String PORTING_STATUS = "PortingStatus";
	public static final String ROAMING_STATUS = "RoamingStatus";
	public static final String ROAMING_COUNTRY = "RoamingCountry";
	public static final String ROAMING_OPERATOR = "RoamingOperator";
	public static final String ROAMING_CIRCLE = "RoamingCircle";
	public static final String NCPR_REGISTERED = "NCPR-Registered";
	public static final String NCPR_PREFERENCE = "NCPR-Preference";
    public static final String BALANCE_EXPIRED = "Mgage Credits Expired";
    public static final String EMAIL_TO = "ravi.singla@freecharge.com";
    public static final String EMAIL_CC = "payments@freecharge.com";
    

}
