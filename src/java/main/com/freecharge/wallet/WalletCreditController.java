package com.freecharge.wallet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.rest.onecheck.model.WalletResponseDo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.util.StringUtils;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.FCCreditWalletLimitExceededException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.dos.CreditRewardSuccessDetailsSNSDo;
import com.freecharge.freefund.dos.business.WalletCreditStatus;
import com.freecharge.freefund.services.CreditRewardSuccessDetailsSNSService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.WalletCreditRequest;

@Controller
@RequestMapping("/")
public class WalletCreditController {

	@Autowired
	UserServiceProxy userServiceProxy;

	@Autowired
	private WalletService walletService;
	
	@Autowired
	protected FCProperties fcProps;
	
	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private CreditRewardSuccessDetailsSNSService creditRewardSuccessDetailsSNSService;
	
    @Autowired
    private CampaignCashRewardService campaignCashRewardService;

    
	@NoLogin
	@Csrf(exclude=true)
	@RequestMapping(method = RequestMethod.POST, value = "/protected/wallet/credits/amount/add")
	public @ResponseBody WalletCreditsResponse addCreditsToWalletById(@RequestParam Long userId,
			@RequestParam Double amount, @RequestParam String notes, @RequestParam(defaultValue = "") String orderId,
			@RequestParam String reference, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) {
		if(userId==null) {
			return null;
		}
		String emailId = userServiceProxy.getEmailIdFromUserId(userId);
		return addCreditsToWallet(emailId, amount, notes, orderId, reference, httpRequest, response, model);
	}
	
	/**
	 * Controller method to handle requests for adding credits to wallet.<br>
	 * User to whom credits should be added is identified by email id 
	 *  
	 * @param creditsInput
	 * @return
	 */
	@NoLogin
	@Csrf(exclude=true)
	@RequestMapping(method = RequestMethod.POST, value = "/protected/wallet/credits/add")
	public @ResponseBody WalletCreditsResponse addCreditsToWallet(@RequestParam String emailId,
			@RequestParam Double amount, @RequestParam String notes, @RequestParam(defaultValue = "") String orderId,
			@RequestParam String reference, 
            HttpServletRequest httpRequest, HttpServletResponse response, ModelMap model) {
		logger.info("Processing wallet add request for emailId:" + emailId + " amount:" +
            amount + " notes:" + notes + " orderId:" + orderId + " reference:" + reference);
		WalletCreditsInput creditsInput = new WalletCreditsInput();
		creditsInput.setEmailId(emailId);
		creditsInput.setAmount(amount);
		creditsInput.setNotes(notes);
		creditsInput.setOrderId(orderId==null?"":orderId);
		creditsInput.setReference(reference);
		WalletCreditsResponse creditsResponse = new WalletCreditsResponse(
				ErrorCode.WALLET_CREDIT_SUCCESS.getErrorNumber(), "Wallet credited successfully");
		validateAddCreditsRequest(creditsInput, creditsResponse);
		if(creditsResponse.getResponseCode() != ErrorCode.WALLET_CREDIT_SUCCESS.getErrorNumber()){
			return creditsResponse;
		}
		
		Users users = userServiceProxy.getUserByEmailId(creditsInput.getEmailId());
		if (users == null) {
			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_INVALID_EMAIL.getErrorNumber());
			creditsResponse.setResponseMessage("No user exists with given email id : " + creditsInput.getEmailId());
		}
		WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
		walletCreditRequest.setUserID(users.getUserId());
		walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
		walletCreditRequest.setTxnAmount(new Amount(creditsInput.getAmount()));
		walletCreditRequest.setMetadata(creditsInput.getNotes());
		walletCreditRequest.setMtxnId(creditsInput.getOrderId());
		walletCreditRequest.setFundSource("REWARD_SERVICE");
		walletCreditRequest.setCallerReference(creditsInput.getReference());
		try {
			WalletResponseDo addCreditResponse = campaignCashRewardService.credit(walletCreditRequest, OneCheckWalletService.OneCheckTxnType.CREDIT_REWARD, "", creditsInput.getOrderId(), fcProps.getProperty(FCConstants.PROMOTION_CORP_ID));
			if (addCreditResponse == null || !addCreditResponse.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {
				creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_FAILED.getErrorNumber());
				creditsResponse.setResponseMessage("Failed to add credits to wallet");
			}
		}catch(FCCreditWalletLimitExceededException exception){
			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_LIMIT_EXCEEDED.getErrorNumber());
			creditsResponse.setResponseMessage("Wallet limit exceeded");
		}
		if(creditsResponse.getResponseCode() == ErrorCode.WALLET_CREDIT_SUCCESS.getErrorNumber()) {
			CreditRewardSuccessDetailsSNSDo creditRewardSuccessDetailsSNSDo=new CreditRewardSuccessDetailsSNSDo(orderId,reference,FundSource.REWARD_SERVICE.name(),amount);
			try {
                logger.info("Publish credit reward success details notification " +
                        CreditRewardSuccessDetailsSNSDo.getJsonString(creditRewardSuccessDetailsSNSDo));
                creditRewardSuccessDetailsSNSService.publish(orderId, "{\"message\":" +
                		CreditRewardSuccessDetailsSNSDo.getJsonString(creditRewardSuccessDetailsSNSDo)+"}");
            } catch (IOException e) {
                logger.error("Error publishing credit reward success event to SNS ", e);
            } catch (Exception e) {
                logger.error("Error publishing credit reward success event to SNS ", e);
            }
			
		}
		
		return creditsResponse;
	}
	
	/**
	 * Validator method to validate input data for adding credits to wallet
	 * 
	 * @param creditsInput
	 * @param creditsResponse
	 */
	private void validateAddCreditsRequest(WalletCreditsInput creditsInput,
			WalletCreditsResponse creditsResponse) {
		if (StringUtils.isNullOrEmpty(creditsInput.getEmailId())) {
			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_INVALID_EMAIL.getErrorNumber());
			creditsResponse.setResponseMessage("Invalid email id");
		}

		if (creditsInput.getAmount() == null || creditsInput.getAmount() <= 0.0) {
			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_INVALID_AMOUNT.getErrorNumber());
			creditsResponse.setResponseMessage("Invalid amount");
		}

//		if (StringUtils.isNullOrEmpty(creditsInput.getOrderId())) {
//			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_INVALID_ORDER_ID.getErrorNumber());
//			creditsResponse.setResponseMessage("Invalid order id");
//		}

		if (StringUtils.isNullOrEmpty(creditsInput.getReference())) {
			creditsResponse.setResponseCode(ErrorCode.WALLET_CREDIT_INVALID_REFERENCE.getErrorNumber());
			creditsResponse.setResponseMessage("Invalid reference");
		}

	}
}
