package com.freecharge.wallet;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.model.WalletResponseDo;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.VoucherBalanceDetails;
import com.snapdeal.payments.sdmoney.service.model.type.VoucherStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

@Service
public class CampaignCashRewardService {
    private static Logger logger = LoggingFactory.getLogger(CampaignCashRewardService.class);

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private WalletService walletService;

    @Autowired
    private FreefundService freefundService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    @Resource(name="clientNameToCorpIdMap")
    private HashMap<String, String> 		 clientNameToCorpIdMap;

    @Autowired
    private FCProperties fcProperties;
    
    public WalletResponseDo voucherCredit(Long userId, Double amount, String orderId, String callerReference,
                                 String fundSource, Long itemId, String metaData,
                                 OneCheckWalletService.OneCheckTxnType oneChekTxnType, String corpId) {
        WalletResponseDo walletResponseDo = new WalletResponseDo();
        walletResponseDo.setStatusCode(FCConstants.SUCCESS_STATUS);
        String oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(
                userServiceProxy.getEmailByUserId(userId.intValue()));
        long startMillis = System.currentTimeMillis();
        if (FCUtil.isEmpty(oneCheckId)) {
            Map<String,Object> cashbackSuccessMap = walletService.doWalletCashBackWithResponse(userId, amount, orderId, callerReference, fundSource,
                    itemId, metaData);
            if(cashbackSuccessMap != null) {
                if(cashbackSuccessMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS)) {
                    metricSuccess(startMillis, "voucherCredit");
                    return walletResponseDo;
                }
                else {
                    metricFailure(startMillis, "voucherCredit");
                    walletResponseDo.setStatusCode(FCConstants.FAILURE);
                    walletResponseDo.setErrorMessage(cashbackSuccessMap.get(FCConstants.FC_GENERAL_ERROR_MESSAGE).toString());
                    return walletResponseDo;
                }
            } else {
                metricFailure(startMillis, "voucherCredit");
                walletResponseDo.setStatusCode(FCConstants.FAILURE);
                walletResponseDo.setErrorMessage("FC Wallet cashback failed.");
                return walletResponseDo;
            }
        } else {
            Map<String, Object> voucherRequest = new HashMap<>();
            voucherRequest.put(FCConstants.MTXN_ID, orderId);
            voucherRequest.put(FCConstants.SD_IDENTITY, oneCheckId);
            voucherRequest.put(FCConstants.EVENT_CONTEXT, metaData);
            voucherRequest.put(FCConstants.AMOUNT_WALLET, new BigDecimal(amount));
            voucherRequest.put(FCConstants.EXPIRY_DURATION, FCConstants.SIX_MONTHS_IN_SECONDS);
            voucherRequest.put(FCConstants.ONECHECK_TXN_TYPE, oneChekTxnType);
            voucherRequest.put(FCConstants.ORDER_ID, orderId);
            if (freefundService.getFreefundCoupon(itemId) != null) {
                voucherRequest.put(FCConstants.CODE, freefundService.getFreefundCoupon(itemId).getFreefundCode());
                metricSuccess(startMillis, "voucherCredit");
            }
            voucherRequest.put(FCConstants.CORP_ID, corpId);
            
            //this is a dirty hack. Need to resolve this ASAP
            if(clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.VIRTUALCARD_CLIENT_NAME)).equals(corpId) 
            		|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.TINYOWL_CLIENT_NAME)).equals(corpId)
            		|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.SHOPSTOP_CLIENT_NAME)).equals(corpId)
            		|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.MCD_CLIENT_NAME)).equals(corpId)
                    || clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.CROSSWORD_CLIENT_NAME)).equals(corpId)
                    || clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.HYPERCITY_CLIENT_NAME)).equals(corpId)
                    || clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.ZOOMIN_CLIENT_NAME)).equals(corpId)
                    || clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.DAILYHUNT_CLIENT_NAME)).equals(corpId)) {
            	voucherRequest.put(FCConstants.BUSINESS_ENTITY, FCConstants.KPAY_BUSINESS_ENTITY);
            }
            
            try {

                GetVoucherBalanceDetailsResponse voucherBalanceDetails =
                        oneCheckWalletService.getVoucherBalanceDetails(oneCheckId);
                if (!FCUtil.isEmpty(voucherBalanceDetails.getVoucherBalanceDetails())
                        && voucherBalanceDetails.getVoucherBalanceDetails().size() >= FCConstants.VOUCHER_SIZE_LIMIT) {
                    Integer activeVoucherCount = 0;
                    for(VoucherBalanceDetails voucherBalanceDetail: voucherBalanceDetails.getVoucherBalanceDetails()) {
                        GetVoucherDetailsResponse voucherDetailsResponse = oneCheckWalletService
                                .getVoucherDetails(voucherBalanceDetail.getVoucherId());
                        if(voucherDetailsResponse.getVoucherStatus() == VoucherStatus.ACTIVE) {
                            activeVoucherCount += 1;
                        }
                    }
                    if(activeVoucherCount >= FCConstants.VOUCHER_SIZE_LIMIT) {
                        walletResponseDo.setStatusCode(FCConstants.FAILURE);
                        walletResponseDo.setErrorMessage("Sorry, " +
                                "you can have only 25 active vouchers at one time. Please use up a few before adding more.");
                        metricSuccess(startMillis, "voucherCredit");
                        return walletResponseDo;
                    }
                }

                CreditVoucherBalanceResponse creditVoucherBalanceResponse =
                    oneCheckWalletService.creditVoucherBalance(voucherRequest);
                if (FCUtil.isEmpty(creditVoucherBalanceResponse.getVoucherId())) {
                    walletResponseDo.setStatusCode(FCConstants.FAILURE);
                    walletResponseDo.setErrorMessage("OC Wallet cashback failed.");
                    metricFailure(startMillis, "voucherCredit");
                    return walletResponseDo;
                }
                return walletResponseDo;
            } catch (Exception e) {
                logger.error("Error crediting gift voucher for " + userId + " with amount "  +
                        amount, e);
                walletResponseDo.setStatusCode(FCConstants.FAILURE);
                metricFailure(startMillis, "voucherCredit");
                walletResponseDo.setErrorMessage("Error in processing voucher credit to OC Wallet");
                return walletResponseDo;
            }
        }
    }
    
	public WalletResponseDo voucherCredit(Long userId, Double amount, String orderId, String callerReference,
			String fundSource, Long itemId, String metaData, OneCheckWalletService.OneCheckTxnType oneChekTxnType,
			String corpId, String emailId) {
		WalletResponseDo walletResponseDo = new WalletResponseDo();
		walletResponseDo.setStatusCode(FCConstants.SUCCESS_STATUS);
		
		String oneCheckId = oneCheckWalletService
				.getOneCheckIdIfOneCheckWalletEnabled(emailId);
		logger.info("One check id is -> " + oneCheckId);
//		if (oneCheckId == null || "".equals(oneCheckId)){
//			//fetch onecheck id from IMS
//			oneCheckId = oneCheckWalletService.getOneCheckIdentityFromIMS(emailId);
//		}
		
		long startMillis = System.currentTimeMillis();
		if (FCUtil.isEmpty(oneCheckId)) {
			Map<String, Object> cashbackSuccessMap = walletService.doWalletCashBackWithResponse(userId, amount, orderId,
					callerReference, fundSource, itemId, metaData);
			if (cashbackSuccessMap != null) {
				if (cashbackSuccessMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS)) {
					metricSuccess(startMillis, "voucherCredit");
					return walletResponseDo;
				} else {
					metricFailure(startMillis, "voucherCredit");
					walletResponseDo.setStatusCode(FCConstants.FAILURE);
					walletResponseDo
							.setErrorMessage(cashbackSuccessMap.get(FCConstants.FC_GENERAL_ERROR_MESSAGE).toString());
					return walletResponseDo;
				}
			} else {
				metricFailure(startMillis, "voucherCredit");
				walletResponseDo.setStatusCode(FCConstants.FAILURE);
				walletResponseDo.setErrorMessage("FC Wallet cashback failed.");
				return walletResponseDo;
			}
		} else {
			Map<String, Object> voucherRequest = new HashMap<>();
			voucherRequest.put(FCConstants.MTXN_ID, orderId);
			voucherRequest.put(FCConstants.SD_IDENTITY, oneCheckId);
			voucherRequest.put(FCConstants.EVENT_CONTEXT, metaData);
			voucherRequest.put(FCConstants.AMOUNT_WALLET, new BigDecimal(amount));
			voucherRequest.put(FCConstants.EXPIRY_DURATION, FCConstants.SIX_MONTHS_IN_SECONDS);
			voucherRequest.put(FCConstants.ONECHECK_TXN_TYPE, oneChekTxnType);
			voucherRequest.put(FCConstants.ORDER_ID, orderId);
			if (freefundService.getFreefundCoupon(itemId) != null) {
				voucherRequest.put(FCConstants.CODE, freefundService.getFreefundCoupon(itemId).getFreefundCode());
				metricSuccess(startMillis, "voucherCredit");
			}
			voucherRequest.put(FCConstants.CORP_ID, corpId);

			// this is a dirty hack. Need to resolve this ASAP
			if (clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.VIRTUALCARD_CLIENT_NAME)).equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.TINYOWL_CLIENT_NAME))
							.equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.SHOPSTOP_CLIENT_NAME))
							.equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.MCD_CLIENT_NAME)).equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.CROSSWORD_CLIENT_NAME))
							.equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.HYPERCITY_CLIENT_NAME))
							.equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.ZOOMIN_CLIENT_NAME))
							.equals(corpId)
					|| clientNameToCorpIdMap.get(fcProperties.getProperty(fcProperties.DAILYHUNT_CLIENT_NAME))
							.equals(corpId)) {
				voucherRequest.put(FCConstants.BUSINESS_ENTITY, FCConstants.KPAY_BUSINESS_ENTITY);
			}

			try {

				GetVoucherBalanceDetailsResponse voucherBalanceDetails = oneCheckWalletService
						.getVoucherBalanceDetails(oneCheckId);
				if (!FCUtil.isEmpty(voucherBalanceDetails.getVoucherBalanceDetails())
						&& voucherBalanceDetails.getVoucherBalanceDetails().size() >= FCConstants.VOUCHER_SIZE_LIMIT) {
					Integer activeVoucherCount = 0;
					for (VoucherBalanceDetails voucherBalanceDetail : voucherBalanceDetails
							.getVoucherBalanceDetails()) {
						GetVoucherDetailsResponse voucherDetailsResponse = oneCheckWalletService
								.getVoucherDetails(voucherBalanceDetail.getVoucherId());
						if (voucherDetailsResponse.getVoucherStatus() == VoucherStatus.ACTIVE) {
							activeVoucherCount += 1;
						}
					}
					if (activeVoucherCount >= FCConstants.VOUCHER_SIZE_LIMIT) {
						walletResponseDo.setStatusCode(FCConstants.FAILURE);
						walletResponseDo.setErrorMessage("Sorry, "
								+ "you can have only 25 active vouchers at one time. Please use up a few before adding more.");
						metricSuccess(startMillis, "voucherCredit");
						return walletResponseDo;
					}
				}

				CreditVoucherBalanceResponse creditVoucherBalanceResponse = oneCheckWalletService
						.creditVoucherBalance(voucherRequest);
				if (FCUtil.isEmpty(creditVoucherBalanceResponse.getVoucherId())) {
					walletResponseDo.setStatusCode(FCConstants.FAILURE);
					walletResponseDo.setErrorMessage("OC Wallet cashback failed.");
					metricFailure(startMillis, "voucherCredit");
					return walletResponseDo;
				}
				return walletResponseDo;
			} catch (Exception e) {
				logger.error("Error crediting gift voucher for " + userId + " with amount " + amount, e);
				walletResponseDo.setStatusCode(FCConstants.FAILURE);
				metricFailure(startMillis, "voucherCredit");
				walletResponseDo.setErrorMessage("Error in processing voucher credit to OC Wallet");
				return walletResponseDo;
			}
		}
	}

    public WalletResponseDo credit(WalletCreditRequest walletCreditRequest,
                                     OneCheckWalletService.OneCheckTxnType oneCheckTxnType,
                                     String freefundCode, String orderId, String corpId) {
        WalletResponseDo walletResponseDo = new WalletResponseDo();
        walletResponseDo.setStatusCode(FCConstants.SUCCESS_STATUS);
        long startMillis = System.currentTimeMillis();

        String oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(
                userServiceProxy.getEmailByUserId(new Long(walletCreditRequest.getUserID()).intValue()));
        if (FCUtil.isEmpty(oneCheckId)) {
            String walletTransactionId = walletService.credit(walletCreditRequest);
            walletResponseDo.settransactionId(walletTransactionId);
            metricSuccess(startMillis, "credit");
            return walletResponseDo;
        } else {
            Map<String, Object> voucherRequest = new HashMap<>();
            voucherRequest.put(FCConstants.MTXN_ID, walletCreditRequest.getMtxnId());
            voucherRequest.put(FCConstants.SD_IDENTITY, oneCheckId);
            voucherRequest.put(FCConstants.EVENT_CONTEXT, walletCreditRequest.getMetadata());
            voucherRequest.put(FCConstants.AMOUNT_WALLET, walletCreditRequest.getTxnAmount().getAmount());
            voucherRequest.put(FCConstants.EXPIRY_DURATION, FCConstants.SIX_MONTHS_IN_SECONDS);
            voucherRequest.put(FCConstants.ONECHECK_TXN_TYPE, oneCheckTxnType);
            voucherRequest.put(FCConstants.ORDER_ID, orderId);
            if (FCUtil.isNotEmpty(freefundCode)) {
                voucherRequest.put(FCConstants.CODE, freefundService.getFreefundCoupon(freefundCode).getFreefundCode());
                metricSuccess(startMillis, "credit");
            }
            voucherRequest.put(FCConstants.CORP_ID, corpId);
            try {

                GetVoucherBalanceDetailsResponse voucherBalanceDetails =
                        oneCheckWalletService.getVoucherBalanceDetails(oneCheckId);
                if (!FCUtil.isEmpty(voucherBalanceDetails.getVoucherBalanceDetails())
                        && voucherBalanceDetails.getVoucherBalanceDetails().size() >= FCConstants.VOUCHER_SIZE_LIMIT) {
                    walletResponseDo.setStatusCode(FCConstants.FAILURE);
                    walletResponseDo.setErrorMessage("Sorry, " +
                            "you can have only 25 active vouchers at one time. Please use up a few before adding more.");
                    metricSuccess(startMillis, "credit");
                    return walletResponseDo;
                }

                CreditVoucherBalanceResponse creditVoucherBalanceResponse =
                        oneCheckWalletService.creditVoucherBalance(voucherRequest);
                if (FCUtil.isEmpty(creditVoucherBalanceResponse.getVoucherId())) {
                    walletResponseDo.setStatusCode(FCConstants.FAILURE);
                    metricFailure(startMillis, "credit");
                    return walletResponseDo;
                }
                walletResponseDo.settransactionId(creditVoucherBalanceResponse.getVoucherId());
                return walletResponseDo;
            } catch (Exception e) {
                logger.error("Error crediting gift voucher for " + walletCreditRequest.getUserID() + " with amount "  +
                        walletCreditRequest.getTxnAmount().getAmount(), e);
                walletResponseDo.setStatusCode(FCConstants.FAILURE);
                metricFailure(startMillis, "credit");
                return walletResponseDo;
            }
        }
    }
    
    private void metricFailure(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Exception");
    }

    private void metricSuccess(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String methodName, final String eventName) {
        String serviceName = "OnecheckWallet";
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;
        
        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
    }
}
