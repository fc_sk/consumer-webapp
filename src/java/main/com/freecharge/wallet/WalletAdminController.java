package com.freecharge.wallet;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 21/12/12
 * Time: 11:50 AM
 * To change this template use File | Settings | File Templates.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.AdminUsers;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.StringUtils;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.web.util.ViewConstants;
import com.google.gson.Gson;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceResponse;

@Controller
@RequestMapping("/admin/*")
public class WalletAdminController extends BaseAdminController {
	
	public static final String REVOKE_HOME_LINK = "/admin/wallet/revokefcbalance.htm";
	
	@Autowired
	private WalletService walletService;

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private UsersDAO usersDAO;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private InService inService;
	
	@Autowired
	private CartService cartService;
    
	@Autowired
    private UserServiceProxy userServiceProxy;

	@Autowired
    private WalletWrapper walletWrapper;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    public static final String CUSTOMER_WALLET_HISTORY_URL = "/admin/wallethistory.htm";
    public static final String ADMIN_DEBIT_WALLET_LINK = "/admin/walletdebitapi.htm";
	public static final String DEBIT_REASON_STRING = "Reason";

	@RequestMapping(value = "wallethistory", method = RequestMethod.GET)
	@NoLogin
	public ModelAndView viewAdminLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.CUSTOMER_WALLET)){
            get403HtmlResponse(response);
            return null;
        }
		return new ModelAndView(ViewConstants.MIS_VIEW);
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@NoLogin
	public ModelAndView adminLogin(
			@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, Model model) {
		String view = "recharge/mis";
		if (adminUsers.getAdminUserEmail().trim() != null
				&& !adminUsers.getAdminUserEmail().trim().isEmpty()
				&& adminUsers.getAdminUserEmail().trim().contains("@")) {
			com.freecharge.app.domain.entity.jdbc.Users users = userServiceProxy.getUserByEmailId(adminUsers.getAdminUserEmail().trim());
			if (users != null) {
				int userID = users.getUserId();
				Amount balance = walletWrapper.getTotalBalance(userID);
				Map<String, Object> lastEntry = walletService
						.findLastEntry(userID);
				List<Map<String, Object>> rows = walletService.getBalanceRows(
						userID, lastEntry,false);
				model.addAttribute("balance", balance.getAmount());
				model.addAttribute("lastEntry", lastEntry);
				model.addAttribute("rows", rows);
				return new ModelAndView(ViewConstants.WALLET_HISTORY_VIEW);

			} else {
				ModelAndView mav = new ModelAndView();
				mav.setViewName(view);
				mav.addObject("message",
						"No Wallet history Available for this Email Address");
				return mav;
			}
		} else {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("recharge/mis");
			mav.addObject("message", "Please Enter correct Email Address");
			return mav;
		}
	}

	@RequestMapping(value = "walletdebitapi", method = RequestMethod.GET)
	@NoLogin
	public ModelAndView viewUserLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.ADMIN_DEBIT_WALLET)){
            get403HtmlResponse(response);
            return null;
        }
		return new ModelAndView(ViewConstants.WALLET_LOGIN_VIEW);
	}

	@RequestMapping(value = "debit", method = RequestMethod.POST)
	@NoLogin
	public ModelAndView walletDebitLogin(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		if (!hasAccess(request, AdminComponent.ADMIN_DEBIT_WALLET)){
            get403HtmlResponse(response);
            return null;
        }

		if (adminUsers.getAdminUserEmail() != null
				&& !adminUsers.getAdminUserEmail().isEmpty()
					&& adminUsers.getAdminUserEmail().contains("@")) {
			
				Amount balance;
				try {
					balance = walletWrapper.getGeneralBalance(adminUsers.getAdminUserEmail());
				} catch (Exception e) {
					ModelAndView mav = new ModelAndView();
					mav.setViewName(ViewConstants.WALLET_LOGIN_VIEW);
					mav.addObject("message", "No Wallet history Available for this Email Address|" + e.toString());
					return mav;
				}
				int balanceVal = balance.getAmount().intValue();
				if (balanceVal > 0) {
					model.addAttribute("balance", balance.getAmount());
					model.addAttribute("emailToDebit", adminUsers.getAdminUserEmail());
					return new ModelAndView(ViewConstants.WALLET_DEBIT_VIEW);
				}
				else {
					ModelAndView mav = new ModelAndView();
					mav.setViewName(ViewConstants.WALLET_DEBIT_STATUS_VIEW);
					mav.addObject("message", "Not sufficient Balance to make Transaction");
					return mav;
				}
		}
		else {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(ViewConstants.WALLET_LOGIN_VIEW);
			mav.addObject("message", "Please Enter correct Email Address");
			return mav;
		}
	}

	@RequestMapping(value = "withdrawal", method = RequestMethod.POST)
	@NoLogin
	public ModelAndView walletWithdrawal(@ModelAttribute("AdminUsers") AdminUsers adminUsers,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		if (!hasAccess(request, AdminComponent.ADMIN_DEBIT_WALLET)){
            get403HtmlResponse(response);
            return null;
        }
		
		Amount balance = null;
		StringBuilder msgString = new StringBuilder();
		ModelAndView mav = new ModelAndView();
		long userID = 0L;
		try {
			if (adminUsers.getDebitAmount() != null
					&& !adminUsers.getDebitAmount().isEmpty()
						&& (Integer.parseInt(adminUsers.getDebitAmount()) > 0))
			{
				
			    Map<String, Object> debitBalanceMap = new HashMap<>();
				if(adminUsers.getReason()!= null && !adminUsers.getReason().isEmpty()) {
					com.freecharge.app.domain.entity.jdbc.Users users = userServiceProxy.getUserByEmailId(adminUsers.getAdminUserEmail());
					
					String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(adminUsers.getAdminUserEmail());;
					if (users == null && oneCheckIdentity == null) {
							msgString.append("User Does Not Exist");
							mav.setViewName(ViewConstants.WALLET_DEBIT_STATUS_VIEW);
							mav.addObject("message", msgString.toString());
							return mav;
					}
					else if (users != null) {
						userID = users.getUserId();
					}
					
					Amount creditBal = walletWrapper.getGeneralBalance(adminUsers.getAdminUserEmail());
					int balanceCredit = creditBal.getAmount().intValue();
					if (balanceCredit <= 0) {
						msgString.append("Can Not Proceed, User Does Not Have A Positive Balance In His Wallet");
						model.addAttribute("balance", creditBal.getAmount());
						mav.setViewName(ViewConstants.WALLET_DEBIT_STATUS_VIEW);
						mav.addObject("message", msgString);
						return mav;
					}
					int amount = Integer.parseInt(adminUsers.getDebitAmount());
					String orderId = adminUsers.getOrderId();
					String callerReference = orderId + RechargeConstants.DEBIT_TYPE;
					WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
					walletDebitRequest.setUserID(userID);
					walletDebitRequest.setTxnAmount(new Amount(amount));
					walletDebitRequest.setMtxnId(orderId);
					walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
					walletDebitRequest.setCallerReference(callerReference);
					walletDebitRequest.setFundSource("");
					walletDebitRequest.setFundDestination("REVOKE_FUND");
					walletDebitRequest.setMtxnId(orderId);

					UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
					Map<String, Object> walletMetaDataMap = paymentTransactionService.getWalletMetaMap(userTransactionHistory, orderId);
					walletMetaDataMap.put(DEBIT_REASON_STRING, adminUsers.getReason());
					String metaData = new Gson().toJson(walletMetaDataMap);
					walletDebitRequest.setMetadata(metaData);

					logger.info("Started amount debit of user : " + adminUsers.getAdminUserEmail());
					/*Handling one-check wallet case */
					debitBalanceMap.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
					debitBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE,
							OneCheckWalletService.OneCheckTxnType.DEBIT_REVOKE_FUND);
					debitBalanceMap.put(FCConstants.ORDER_ID, orderId);
					
					if (FCUtil.isNotEmpty(oneCheckIdentity)) {
						debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
						debitBalanceMap.put(WalletWrapper.ONE_CHECK_IDENTITY, oneCheckIdentity);
					} else {
						debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
					}
					Map<String, Object> responseMap = walletWrapper.debitBalance(debitBalanceMap);
					String status = (String) responseMap.get(FCConstants.STATUS);
					if (!FCConstants.SUCCESS.equals(status)) {
						msgString.append("Wallet Debit Failure");
						mav.setViewName(ViewConstants.WALLET_DEBIT_VIEW);
						mav.addObject("message", msgString);
						return mav;
					}
					DebitBalanceResponse debitBalanceResponse = (DebitBalanceResponse)responseMap.get("walletTransactionResponse");
	                String debitTransactionId = debitBalanceResponse.getTransactionId();
					PaymentTransaction payTxn = paymentTransactionService.getSuccessfulPaymentTransactionByOrderId(orderId);
					walletService.publishRevokeAlert(debitBalanceMap, payTxn, debitTransactionId);
					/*End of Handling one-check wallet case*/
					logger.info("Finished amount debit of user : " + adminUsers.getAdminUserEmail());
					creditBal = walletWrapper.getGeneralBalance(adminUsers.getAdminUserEmail());
					model.addAttribute("balance", creditBal.getAmount());
					mav.setViewName(ViewConstants.WALLET_DEBIT_STATUS_VIEW);
					mav.addObject("message", "Transaction Successful");
					return mav;
					
				}
				else {
					msgString.append("Please Enter Reason");
					model.addAttribute("balance", balance.getAmount());
					mav.setViewName(ViewConstants.WALLET_DEBIT_VIEW);
					mav.addObject("message", msgString);
					return mav;
				}
			}
			else {
				msgString.append("Please Enter correct Amount");
				mav.setViewName(ViewConstants.WALLET_DEBIT_VIEW);
				mav.addObject("message", msgString);
				return mav;
			}
		}
		catch (Exception exception) {
		    logger.info("Exception while debitting amount of user :" + adminUsers.getAdminUserEmail(), exception);
		    msgString.append("Exception occured During debit process. Please try later");
		    mav.setViewName(ViewConstants.WALLET_DEBIT_STATUS_VIEW);
			mav.addObject("message", msgString);
			return mav;
		}
	}
	
	@RequestMapping(value = "wallet/revokefcbalance", method = RequestMethod.GET)
	public String revoke(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.REVOKE_FC_BALANCE)){
            return get403HtmlResponse(response);
        }
		return ViewConstants.ADMIN_WALLET_REVOKE_VIEW ;
	}

	@RequestMapping(value = "wallet/dorevokefcbalance", method = RequestMethod.POST)
	public String dorevoke(@RequestParam("orderids") MultipartFile file, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        if (!hasAccess(request, AdminComponent.REVOKE_FC_BALANCE)){
            return get403HtmlResponse(response);
        }
		int count = 0;
		String messages = "";
		try {
			BufferedReader br = null;
			List<String> revokedOrderIdList = new ArrayList<String>();
	        List<String> nonRevokedOrderIdList = new ArrayList<String>();
			try {
				br = new BufferedReader(new InputStreamReader(
						file.getInputStream()));
				String line = "";
				while ((line = br.readLine()) != null) {
					try {
						line = StringUtils.trim(line);
						if (line.isEmpty())
							continue;
						String[] array = line.split("[,]");
						if (array.length > 1) {
							String orderId = array[0];
							BigDecimal amountToDebit = new BigDecimal(
									Double.parseDouble(array[1]));
                            boolean isRevoke = walletService.dorevoke(orderId,amountToDebit);
                            if (isRevoke) {
                            	count++;
                            	revokedOrderIdList.add(orderId);
                            } else {
                                nonRevokedOrderIdList.add(orderId);
                            }
                            model.addAttribute("revokedOrderIdList", revokedOrderIdList);
                            model.addAttribute("nonRevokedOrderIdList", nonRevokedOrderIdList);
                            }
						} catch (Exception e) {
						    logger.error("Exception ", e);
					}
				}
			} catch (Exception e) {
				messages += "Something went wrong. Try again later " + ""
						+ e.getMessage();
				throw new Exception("Try again later: " + e.toString());
			} finally {
				if (br != null)
					br.close();
			}
			messages += "Revoke done successfully for " + count + " order ids.";
		} catch (Exception e) {
			logger.info("Exception " + messages, e);
			if (!messages.isEmpty())
				messages += "<br/>Try again later " + "" + e.getMessage();
		}

		model.addAttribute("messages",messages);
		return ViewConstants.ADMIN_WALLET_REVOKE_VIEW;

	}
}	
	

