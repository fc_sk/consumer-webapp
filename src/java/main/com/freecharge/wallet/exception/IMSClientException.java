package com.freecharge.wallet.exception;

public class IMSClientException extends RuntimeException{

    public IMSClientException(Exception e) {
        super(e);
    }
}
