package com.freecharge.wallet.exception;

public class OneCheckWalletException extends RuntimeException {
    public OneCheckWalletException(String errorMsg) {
        super(errorMsg);
    }
    public OneCheckWalletException(Exception e) {
        super(e);
    }
}
