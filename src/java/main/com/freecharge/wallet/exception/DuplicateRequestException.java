package com.freecharge.wallet.exception;

public class DuplicateRequestException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public DuplicateRequestException(String cause) {
        super(cause);
    }
}
