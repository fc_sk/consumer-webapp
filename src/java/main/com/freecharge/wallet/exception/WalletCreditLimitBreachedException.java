package com.freecharge.wallet.exception;

public class WalletCreditLimitBreachedException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public WalletCreditLimitBreachedException(String errorMsg) {
        super(errorMsg);
    }
    public WalletCreditLimitBreachedException(Exception e) {
        super(e);
    }
}
