package com.freecharge.wallet;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.freecharge.common.encryption.mdfive.MD5;
import com.freecharge.payment.util.PaymentConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 29/12/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class WalletUtil {
    @Autowired
    private WalletService walletService;

    public void addWalletSwitches(Model model) {
        model.addAttribute("isWalletPayEnabled", walletService.isWalletPayEnabled());
        model.addAttribute("isWalletDataDisplayEnabled", walletService.isWalletDataDisplayEnabled());
    }

    public static String createWalletResponseHash(Map<String, String> params) {
        String[] keys = {
                PaymentConstants.MERCHANT_ORDER_ID,
                PaymentConstants.AAMOUNT,
                PaymentConstants.RESULT,
                PaymentConstants.MESSAGE
        };

        String data = "";
        for (String key : keys) {
            if (params.containsKey(key)) {
                data = data + params.get(key);
            }
        }

        return MD5.hash(data);
    }

    public static boolean doesWalletResponseHashMatch(Map<String, String> params) {
        String hash = params.get(PaymentConstants.HASH);
        return createWalletResponseHash(params).equals(hash);
    }

    public static String createHash(String merchantOrderId, int userId) {
        String data = merchantOrderId + userId;
        return new String(MD5.hash(data));
    }

    public static boolean doesHashMatch(String merchantOrderId, int userId, String hash) {
        return createHash(merchantOrderId, userId).equals(hash);
    }
}
