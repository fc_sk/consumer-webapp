package com.freecharge.wallet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import com.snapdeal.payments.sdmoney.service.model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentTransactionService.PaymentPlanType;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.exception.OneCheckWalletException;
import com.freecharge.wallet.exception.WalletCreditLimitBreachedException;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletTransaction;
import com.freecharge.wallet.util.FCWalletUtil;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;

/**
 * Wrapper class to decide which wallet(fc wallet or one check wallet) to consider for credits
 */
@Service("walletWrapper")
public class WalletWrapper {

    public static final String ONE_CHECK_WALLET_TRANSACTION_RESPONSE = "walletTransactionResponse";
    public static final String FC_WALLET_TRANSACTION_ID = "walletTransactionId";
    public static final String WALLET_DEBIT_REQUEST = "walletDebitRequest";
    public static final String WALLET_CREDIT_REQUEST = "walletCreditRequest";
    public static final String ONE_CHECK_IDENTITY = "oneCheckIdentity";
    public static final String WALLET_TYPE = "walletType";
    public static final String PAYMENT_TYPE = "paymentType";
    public static final String  ONE_CHECK_TXN_TYPE = "oneCheckTxnType";
    public static final String DESTINATION_CORP_ID = "destinationCorpId";
    private static Logger logger = LoggingFactory.getLogger(WalletWrapper.class);

    @Autowired
    private WalletService walletService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private FCWalletUtil fcWalletUtil;
    
    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    @Qualifier("checkoutExecutor")
    ThreadPoolTaskExecutor checkoutExecutor;
    
    @Autowired
    private OrderIdSlaveDAO                       orderIdDAO;
    
    @Autowired
    private PaymentTransactionService paymentService;
    
    /**
     * Wrapper method which decides to which account should the money be credited
     * If the user is a one check customer the money is refunded to one check wallet else money is refunded to fc wallet
     * @param creditBalanceMap:should contain walletCreditRequest object, oneCheckTxnType,
     *                        promocode if credit is done for CASHBACK or FREEFUND codes
     * @return
     */
    public Map<String, Object> creditGeneralBalanceToUser(Map<String, Object> creditBalanceMap) {
        WalletCreditRequest walletCreditRequest = (WalletCreditRequest) creditBalanceMap.get(WALLET_CREDIT_REQUEST);
        int userId = (int) walletCreditRequest.getUserID();
        Users user = userServiceProxy.getUserByUserId(userId);
        String email = user.getEmail();
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        String oneCheckIdentity;
        if (null != (oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email))) {
            Map<String, Object> params = new HashMap<>();
            params.putAll(creditBalanceMap);
            params.put(FCConstants.EMAIL, email);
            params.put(ONE_CHECK_IDENTITY, oneCheckIdentity);
            CreditGeneralBalanceToUserResponse walletTransactionResponse = oneCheckWalletService
                    .creditGeneralBalanceToUser(
                            params);
            responseMap
                    .put(PaymentConstants.REFUND_TO_WALLET, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
            if (null != walletTransactionResponse) {
                responseMap.put(ONE_CHECK_WALLET_TRANSACTION_RESPONSE, walletTransactionResponse);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            }
        } else {
            String walletTransactionId = walletService.credit(walletCreditRequest);
            responseMap.put(PaymentConstants.REFUND_TO_WALLET, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
            if (FCUtil.isNotEmpty(walletTransactionId)) {
                responseMap.put(FC_WALLET_TRANSACTION_ID, walletTransactionId);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            }
        }
        return responseMap;
    }
    
    /**
     * Wrapper method which decides to which account should the money be refunded
     * If the user is a one check customer the money is refunded to one check wallet else money is refunded to fc wallet
     * @param creditBalanceMap:should contain walletCreditRequest object, oneCheckTxnType,
     *                        promocode if credit is done for CASHBACK or FREEFUND codes
     * @return
     * @throws PaymentPlanNotFoundException 
     */
    public Map<String, Object> refundBalanceToUser(Map<String, Object> creditBalanceMap) throws PaymentPlanNotFoundException {
        WalletCreditRequest walletCreditRequest = (WalletCreditRequest) creditBalanceMap.get(WALLET_CREDIT_REQUEST);
        int userId = (int) walletCreditRequest.getUserID();
        Users user = userServiceProxy.getUserByUserId(userId);
        String email = user.getEmail();
        String orderId = (String) creditBalanceMap.get(FCConstants.ORDER_ID);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        String oneCheckIdentity;
        if (null != (oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email))) {
            Map<String, Object> params = new HashMap<>();
            params.putAll(creditBalanceMap);
            params.put(FCConstants.EMAIL, email);
            params.put(ONE_CHECK_IDENTITY, oneCheckIdentity);
            
            CreditGeneralBalanceToUserResponse walletTransactionResponse = null;
            RefundBalancesResponse walletRefundBalanceResponse = null;
            OneCheckTxnType txnType = (OneCheckTxnType) params.get(OneCheckWalletService.ONE_CHECK_TXN_TYPE);
            boolean isCsCashback = false;
            if (txnType == OneCheckTxnType.CS_CASHBACK) {
                isCsCashback = true;
            }
            
            List<PaymentTransaction>  successPaymentTxn = paymentService.getSuccessfulPaymentTransactionByMerchantOrderId(walletCreditRequest.getMtxnId());
            
            /*
             * Check if it is a partial payment
             * If so, call the refund API for wallet amount and creditGeneral balance API for payment
             */
            
            boolean isfcCreditsPayment = isfcCreditsPayment((Integer) creditBalanceMap.get(WalletWrapper.PAYMENT_TYPE));
            
            if (!isfcCreditsPayment && !isCsCashback && paymentService.getPaymentPlanType(successPaymentTxn, true) == PaymentPlanType.PARTIAL_PAYMENT) {
                logger.info("Onecheck user + Partial payment. Need to call the refund API for the wallet amount for orderId "
                        + orderId);
                Amount maxRefundAmount = walletCreditRequest.getTxnAmount();
                
                Amount walletAmount = PaymentUtil.getNewFCWalletAmount(successPaymentTxn);
                Amount pgAmount = PaymentUtil.getPGAmount(successPaymentTxn);
                
                boolean shouldRefundWallet = shouldRefundWallet(walletCreditRequest.getMtxnId(), orderId, walletAmount, oneCheckIdentity, maxRefundAmount);
                logger.info("shouldRefundWallet for " + walletCreditRequest.getMtxnId() + " is " + shouldRefundWallet);
                if (shouldRefundWallet) {
                    logger.info("Trying to refund balance of " + walletAmount.getAmount()
                            + " for mtxnId " + walletCreditRequest.getMtxnId());
                    WalletCreditRequest walletRefundRequest = getWalletRefundRequest(walletCreditRequest,
                            walletAmount);
                    Map<String, Object> refundParams = getRefundParams(params, walletRefundRequest, OneCheckTxnType.CREDIT_REFUND_API);
                    logger.info("Calling wallet refund API for " + refundParams);
                    walletRefundBalanceResponse = oneCheckWalletService.refundBalanceToUser(refundParams);
                    maxRefundAmount = new Amount(maxRefundAmount.getAmount().subtract(
                            walletAmount.getAmount()));
                }
                
                boolean shouldCreditPgAmount = shouldCreditPgAmountToWallet(maxRefundAmount, pgAmount);
                logger.info("shouldCreditPgAmountToWallet for " + walletCreditRequest.getMtxnId() + " is " + shouldCreditPgAmount);
                logger.info("MaxRefund amount for " + orderId + " is " + maxRefundAmount.getAmount().doubleValue());
                if (shouldCreditPgAmount) {
                    if (maxRefundAmount.compareTo(new Amount("2000")) >= 0) {
                        logger.info("PG amount > 2000 for " + orderId
                                + " throwing wallet credit limit breached exception");
                        throw new WalletCreditLimitBreachedException("PG amount > 2000 for " + orderId
                                + " throwing wallet credit limit breached exception");
                    }
                    logger.info("Trying to credit general balance for " + maxRefundAmount.getAmount()
                            + " for mtxnId " + walletCreditRequest.getMtxnId());
                    params.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, txnType);
                    WalletCreditRequest walletCreditPgRequest = getWalletRefundRequest(walletCreditRequest,
                            maxRefundAmount);
                    Map<String, Object> walletCreditPgParams = getRefundParams(params, walletCreditPgRequest, OneCheckTxnType.CREDIT_REFUND);
                    logger.info("Calling wallet credit API for " + walletCreditPgParams);
                    walletTransactionResponse = oneCheckWalletService.creditGeneralBalanceToUser(walletCreditPgParams);
                }

                if (null != walletTransactionResponse && null != walletTransactionResponse.getTransactionId() && null != walletRefundBalanceResponse && null != walletRefundBalanceResponse.getTransactionId()) {
                    responseMap.put(ONE_CHECK_WALLET_TRANSACTION_RESPONSE, walletTransactionResponse);
                    responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                }
            } else {
                walletTransactionResponse = oneCheckWalletService.creditGeneralBalanceToUser(params);
                if (null != walletTransactionResponse && null != walletTransactionResponse.getTransactionId()) {
                    responseMap.put(ONE_CHECK_WALLET_TRANSACTION_RESPONSE, walletTransactionResponse);
                    responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                }
            }

            responseMap
                    .put(PaymentConstants.REFUND_TO_WALLET, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
        } else {
            String walletTransactionId = walletService.credit(walletCreditRequest);
            responseMap.put(PaymentConstants.REFUND_TO_WALLET, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
            if (FCUtil.isNotEmpty(walletTransactionId)) {
                responseMap.put(FC_WALLET_TRANSACTION_ID, walletTransactionId);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            }
        }
        return responseMap;
    }

    private boolean shouldCreditPgAmountToWallet(Amount maxRefundAmount, Amount pgAmount) {
        return maxRefundAmount.compareTo(pgAmount) <= 0 && maxRefundAmount.compareTo(Amount.ZERO) > 0;
    }

    private boolean shouldRefundWallet(String mtxnId, String orderId, Amount walletAmount, String oneCheckIdentity, Amount maxRefundAmount) {
        List<TransactionSummary> txns = oneCheckWalletService.findTransactionsByReference(orderId, oneCheckIdentity);
        boolean isWalletAlreadyRefunded = false;
        boolean isWalletDebited = false;
        
        int creditRefundCount = 0;
        int walletDebitCount = 0;
        
        if(txns!=null && !txns.isEmpty()){
            for(TransactionSummary summary : txns){
                if (isWalletRefunded(summary, mtxnId)) {
                    logger.info("Wallet refund idempotency " + summary.getIdempotencyId());
                    creditRefundCount++;
                } else if (isWalletRefundRevoked(summary, mtxnId)) {
                    logger.info("Wallet refund revoke idempotency " + summary.getIdempotencyId());
                    creditRefundCount--;
                } else if (isWalletDebitedForRecharge(summary, mtxnId)) {
                    logger.info("Wallet debit idempotency " + summary.getIdempotencyId());
                    walletDebitCount++;
                } else if (isWalletDebitRevokedForRecharge(summary, mtxnId)) {
                    logger.info("Wallet debit revoke idempotency " + summary.getIdempotencyId());
                    walletDebitCount--;
                }
            }
        }
        
        if (creditRefundCount > 0) {
            isWalletAlreadyRefunded = true;
        }
        
        if (walletDebitCount > 0) {
            isWalletDebited = true;
        }
        
        boolean isAmountAllowedForRefund = false;
        if (maxRefundAmount.compareTo(walletAmount) >=0 && walletAmount.compareTo(Amount.ZERO) >= 0) {
            isAmountAllowedForRefund = true;
        }
        
        logger.info("isWalletAlreadyRefunded = " + isWalletAlreadyRefunded + ", isWalletDebited=" + isWalletDebited
                + ", isAmountAllowedForRefund=" + isAmountAllowedForRefund + " for mtxnId " + mtxnId);
        
        return !isWalletAlreadyRefunded && isWalletDebited && isAmountAllowedForRefund;
    }
    
    private boolean isfcCreditsPayment(Integer paymentType) {
        logger.info("Payment type is " + paymentType);
        if (paymentType == null) {
            return false;
        }
        
        if (paymentType.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET))) {
            return true;
        }
        return false;
    }

    private Map<String, Object> getRefundParams(Map<String, Object> params, WalletCreditRequest walletRefundRequest, OneCheckTxnType oneCheckTxnType) {
        Map<String, Object> refundParams = new HashMap<>();
        refundParams = params;
        refundParams.put(WALLET_CREDIT_REQUEST, walletRefundRequest);
        if (oneCheckTxnType != null) {
            refundParams.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, oneCheckTxnType);
        }
        return refundParams;
    }

    private WalletCreditRequest getWalletRefundRequest(WalletCreditRequest walletCreditRequest, Amount walletAmount) {
        WalletCreditRequest walletRefundRequest = new WalletCreditRequest();
        walletRefundRequest = walletCreditRequest;
        walletRefundRequest.setTxnAmount(walletAmount);
        return walletRefundRequest;
    }

    /**
     * Wrapper method which decides from which account should the money be debited
     * If the user is a one check customer the money is debited from one check wallet else money is debited to fc wallet
     * @param debitBalanceMap:should contain walletDebitRequest object, oneCheckTxnType,
     * @return
     * @throws DuplicateRequestException
     */
    public Map<String, Object> debitBalance(Map<String, Object> debitBalanceMap)
            throws DuplicateRequestException {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
        WalletDebitRequest walletDebitRequest = (WalletDebitRequest) debitBalanceMap.get(WALLET_DEBIT_REQUEST);
        String walletType = (String) debitBalanceMap.get(WALLET_TYPE);
        String oneCheckIdentity = (String) debitBalanceMap.get(ONE_CHECK_IDENTITY);
        int userId = (int) walletDebitRequest.getUserID();
        Users user = userServiceProxy.getUserByUserId(userId);
        String email = (String) debitBalanceMap.get(FCConstants.EMAIL);
        
        if(null != user) {
        	email = user.getEmail();
        }
        
        if(FCUtil.isEmpty(walletType) || (PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(walletType) && FCUtil.isEmpty(oneCheckIdentity))) {
            if (null != (oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email))) {
                walletType = PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION;
            } else {
                walletType = PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION;
            }
        }
        if (PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(walletType)) {
            Map<String, Object> params = new HashMap<>();
            params.putAll(debitBalanceMap);
            params.put(ONE_CHECK_IDENTITY, oneCheckIdentity);
          
            DebitBalanceResponse debitBalanceResponse=null;
            try{
            debitBalanceResponse = oneCheckWalletService.debitBalance(params);
			} catch (OneCheckWalletException ocwe) {
				if (ocwe.getMessage().contains(FCConstants.OCW_INTERNAL_SERVER_EXCEPTION)) {
					logger.info("Caught internal Server Exception ", ocwe);
					logger.info("Doing status check");

					String mtxnId = walletDebitRequest.getMtxnId();

					boolean attemptRetry = true;
					int retryCount = 3;
					while (attemptRetry && retryCount > 0) {
						TransactionSummary transaction = oneCheckWalletService.findTransactionForStatusCheck(params,
								mtxnId);
						if (transaction != null) {
							debitBalanceResponse = new DebitBalanceResponse();
							debitBalanceResponse.setTransactionId(transaction.getTransactionId());
							debitBalanceResponse.setTransactionTimestamp(transaction.getTimestamp());
							attemptRetry = false;
						}

						if (attemptRetry) {
							// If transaction status is not known, attempting
							// retry with a sleep of 1 sec
							logger.info("Transaction Status not Know Attempting Again");
							try {
								Thread.sleep(1000);
								retryCount--;
							} catch (InterruptedException e) {
							
								retryCount--;
								logger.info("Exception while putting retry thread to sleep:", e);
							}

						}
					}
				}
			}
            if (null != debitBalanceResponse) {
                responseMap.put(ONE_CHECK_WALLET_TRANSACTION_RESPONSE, debitBalanceResponse);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            }
        } else {
            String walletTransactionId = walletService.debit(walletDebitRequest);
            if (FCUtil.isNotEmpty(walletTransactionId)) {
                responseMap.put(FC_WALLET_TRANSACTION_ID, walletTransactionId);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            }
        }
        responseMap.put(WALLET_TYPE, walletType);
        return responseMap;
    }

	public Amount getTotalBalance(long userId) {
		try {
			Users user = userServiceProxy.getUserByUserId((int) userId);
			String email = user.getEmail();
			final String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
			if (sdIdentity != null) {
				Future<Balance> balanceResult = checkoutExecutor.submit(new Callable<Balance>() {
					@Override
					public Balance call() throws Exception {
						return oneCheckWalletService.getFCWalletAccountBalance(sdIdentity);
					}
				});
				try {
					long timeOutInMilliseconds = 1000;
					Balance balance = balanceResult.get(timeOutInMilliseconds, TimeUnit.MILLISECONDS);
					return new Amount(balance.getTotalBalance());
				} catch (Exception e) {
					logger.error("Caught exception while fetching balance for OC User " + sdIdentity, e);
					return new Amount(BigDecimal.ZERO);
				} finally {
					balanceResult.cancel(true);
				}
			} else {
				return walletService.findBalance(userId);
			}
		} catch (Exception e) {
			logger.error("Caught Exception while getting total balance for user : " + userId);
			return new Amount(BigDecimal.ZERO);
		}

	}

    public Amount getDebitableBalance(long userId) {
        try {
            Users user = userServiceProxy.getUserByUserId((int) userId);
            String email = user.getEmail();
            final String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
            if (sdIdentity != null) {
                Future<Amount> amountResult = checkoutExecutor.submit(new Callable<Amount>() {
                    @Override
                    public Amount call() throws Exception {
                       return oneCheckWalletService.getWalletDebitableAmount(sdIdentity);
                    }
                });
                try {
                    long timeOutInMilliseconds = 1000;
                    Amount amount = amountResult.get(timeOutInMilliseconds, TimeUnit.MILLISECONDS);
                    logger.info("Debitable amount : "+amount.toString());
                    return amount;
                } catch (Exception e) {
                    logger.error("Caught exception while fetching debitable balance for OC User " + sdIdentity, e);
                    return new Amount(BigDecimal.ZERO);
                } finally {
                    amountResult.cancel(true);
                }
            } else {
                //need to check
                return walletService.findBalance(userId);
            }
        } catch (Exception e) {
            logger.error("Caught Exception while getting debitable balance for user : " + userId);
            return new Amount(BigDecimal.ZERO);
        }
    }
    /***
     * Do not use this method. It returns total balance only. 
     * This will be removed
     * @param email
     * @return
     * @throws Exception
     */
    public Amount getGeneralBalance(String email) throws Exception {
        final String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        if (sdIdentity != null) {
            Future<Balance> balanceResult = checkoutExecutor.submit(new Callable<Balance>() {
                @Override
                public Balance call() throws Exception {
                    return oneCheckWalletService.getFCWalletAccountBalance(sdIdentity);
                }
            });
            try {
                long timeOutInMilliseconds = 5000;
                Balance balance = balanceResult.get(timeOutInMilliseconds, TimeUnit.MILLISECONDS);
                return new Amount(balance.getTotalBalance());
            } catch (Exception e) {
                logger.error("Caught exception while fetching balance for OC User " + sdIdentity, e); 
                throw e;
            } finally{
                balanceResult.cancel(true);
            }
        } else {
        	Users user = userServiceProxy.getUserByEmailId(email);
        	if(user != null) {
        		return walletService.findBalance(user.getUserId());
        	}
        	else throw new Exception("No Such User Exists");
        }
    }
    
    public List<PaymentRefundTransaction> getRefundTxnFromWallet(PaymentTransaction successPayTxn) {
        List<PaymentRefundTransaction> paymentRefundTransactions = new ArrayList<>();
        
        logger.info("Checking old fcwallet for any refunds for orderID " + successPayTxn.getOrderId());
        List<WalletTransaction> walletRefunds = walletService.findTransactionTypeByOrderId(successPayTxn.getMerchantTxnId());
        if (walletRefunds != null && !walletRefunds.isEmpty()) {
            boolean isRefunded = false;
            boolean isDebited = false;
            boolean isCredited = false;
            WalletTransaction refundTxn = null;
            WalletTransaction creditTxn = null;
            for (WalletTransaction walletTransaction : walletRefunds) {
                if (!FCUtil.isEmpty(walletTransaction.getFundSource())
                        && FundSource.valueOf(walletTransaction.getFundSource()) == FundSource.REFUND) {
                    isRefunded = true;
                    refundTxn = walletTransaction;
                } else if (!FCUtil.isEmpty(walletTransaction.getFundSource())
                        && FundSource.valueOf(walletTransaction.getFundSource()) == FundSource.RECHARGE_FUND) {
                    isCredited = true;
                    creditTxn = walletTransaction;
                } else if (!FCUtil.isEmpty(walletTransaction.getFundDestination())
                        && FundDestination.valueOf(walletTransaction.getFundDestination()) == FundDestination.RECHARGE) {
                    isDebited = true;
                }
            }
            if (isRefunded) {
                logger.info("FCCredits Txn refunded for " + successPayTxn.getOrderId() + ", " + refundTxn);
                paymentRefundTransactions.add(createPaymentRefund(refundTxn, successPayTxn));
            } else if (isCredited && !isDebited) {
                logger.info("Payment credited to FCCredits " + successPayTxn.getOrderId() + ", " + creditTxn);
                paymentRefundTransactions.add(createPaymentRefund(creditTxn, successPayTxn));
            }
        }
        
        String email = userServiceProxy.getEmailIdFromOrderId(successPayTxn.getOrderId());
        String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        if (sdIdentity != null) {
            logger.info("Checking new fcwallet for any refunds for orderID " + successPayTxn.getOrderId());
            List<TransactionSummary> txnList = oneCheckWalletService.findTransactionsByReference(successPayTxn.getOrderId(), sdIdentity);
            if (txnList == null) {
                throw new RuntimeErrorException(null, "Breaking getRefundTxnFromWallet as wallet threw an exception "
                        + successPayTxn.getOrderId());
            }
            
            if (!txnList.isEmpty()) {
                for (TransactionSummary summary : txnList) {
                    if (isWalletCreditedForRefund(summary, successPayTxn.getMerchantTxnId())) {
                        logger.info("Wallet Txn refunded for " + successPayTxn.getOrderId() + ", " + summary);
                        paymentRefundTransactions.add(createPaymentRefund(summary, successPayTxn));
                    } else if (isWalletCreditRevoked(summary, successPayTxn.getMerchantTxnId())) {
                        logger.info("Wallet Txn refunded for " + successPayTxn.getOrderId() + ", " + summary);
                        summary.setTransactionAmount(summary.getTransactionAmount().negate());
                        paymentRefundTransactions.add(createPaymentRefund(summary, successPayTxn));
                    }
                }
            }
        }
        logger.info("Total wallet refunds for " + successPayTxn.getOrderId() + " is " + paymentRefundTransactions.size());
        return paymentRefundTransactions;
    }
    
    private PaymentRefundTransaction createPaymentRefund(WalletTransaction refund,
            PaymentTransaction successPayTxn) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        Amount refundedAmount = new Amount(refund.getTransactionAmount());
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(successPayTxn.getPaymentTxnId());
        paymentRefundTransaction.setRawPGRequest("Credits Refund");
        paymentRefundTransaction.setSentToPG(refund.getTransactionDate());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setPaymentTransaction(successPayTxn);
        paymentRefundTransaction.setPgTransactionReferenceNo(String.valueOf(refund.getWalletTransactionId()));
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setReceivedFromPG(refund.getTransactionDate());
        paymentRefundTransaction.setRawPGResponse("FCCredits refund");
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
        return paymentRefundTransaction;
    }

    private boolean isWalletCreditedForRefund(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return (oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND, null)
                || oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CS_REFUND, null) || oneCheckWalletService
                    .isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND_API, null))
                    && (txnType == TransactionType.CREDIT_DEFAULT || txnType == TransactionType.CREDIT_REFUND);
    }
    
    private boolean isWalletCreditRevoked(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return (oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND, null)
                || oneCheckWalletService
                        .isSameIdempotency(idempotency, summary.getTransactionReference(), OneCheckTxnType.DEBIT_REVOKE_FUND, null)
                || oneCheckWalletService
                        .isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND_API, null)
                || oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CS_REFUND, null) 
                || oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.DEBIT_REFUND, null))
                && (txnType == TransactionType.DEBIT_DEFAULT);
    }
    
    private boolean isWalletRefunded(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return (oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND_API, null))
                && (txnType == TransactionType.CREDIT_REFUND);
    }
    
    private boolean isWalletRefundRevoked(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return (oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.CREDIT_REFUND_API, null))
                && (txnType == TransactionType.DEBIT_DEFAULT);
    }
    
    private boolean isWalletDebitedForRecharge(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.DEBIT_RECHARGE, null)
                && (txnType == TransactionType.DEBIT_DEFAULT);
    }
    
    private boolean isWalletDebitRevokedForRecharge(TransactionSummary summary, String mtxnId) {
        String idempotency = summary.getIdempotencyId();
        TransactionType txnType = summary.getTransactionType();

        return oneCheckWalletService.isSameIdempotency(idempotency, mtxnId, OneCheckTxnType.DEBIT_RECHARGE, null)
                && (txnType == TransactionType.CREDIT_DEFAULT || txnType == TransactionType.CREDIT_REFUND);
    }
    
    public boolean checkWalletDebitedAndSetTransactionId(PaymentTransaction transactionToSync) {
        Users user = userServiceProxy.getUserFromOrderId(transactionToSync.getOrderId());
        String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
        if (sdIdentity != null) {
            Transaction txn = oneCheckWalletService.findTxnByIdempotencyId(transactionToSync.getMerchantTxnId(), OneCheckTxnType.DEBIT_RECHARGE, sdIdentity);
            if (txn != null) {
                transactionToSync.setTransactionId(txn.getTransactionId());
                return true;
            }
        } else {
            List<WalletTransaction> result = walletService.findTransaction(transactionToSync.getMerchantTxnId(),
                    com.freecharge.wallet.service.Wallet.TransactionType.WITHDRAWAL);
            if (!result.isEmpty()) {
                return true;
            }
        }
        return false;
    }
    
    private PaymentRefundTransaction createPaymentRefund(TransactionSummary summary, PaymentTransaction successPayTxn) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        Amount refundedAmount = new Amount(summary.getTransactionAmount());
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(successPayTxn.getPaymentTxnId());
        paymentRefundTransaction.setRawPGRequest("Wallet Refund");
        paymentRefundTransaction.setSentToPG(summary.getTimestamp());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setPaymentTransaction(successPayTxn);
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setPgTransactionReferenceNo(summary.getTransactionId());
        paymentRefundTransaction.setReceivedFromPG(summary.getTimestamp());
        paymentRefundTransaction.setRawPGResponse(summary.toString());
        paymentRefundTransaction.setRefundTo(PaymentConstants.ONE_CHECK_WALLET_CODE);
        return paymentRefundTransaction;
    }
}
