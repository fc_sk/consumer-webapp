package com.freecharge.wallet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.wallet.service.CreditsEncashDetails;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

@Service
public class CreditsEncashDetailsDAO {
    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertCreditsEncashDetails;
    protected final Logger logger = LoggingFactory.getLogger(getClass().getName());

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertCreditsEncashDetails = new SimpleJdbcInsert(dataSource).withTableName("fc_credit_encash_bank_detail").usingGeneratedKeyColumns("id");

    }

    public int insert(CreditsEncashDetails creditsEncashDetails) {
        SqlParameterSource parameters = creditsEncashDetails.getMapSqlParameterSource();
        Number returnParam = this.insertCreditsEncashDetails.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public boolean updateBankDetails(CreditsEncashDetails creditsEncashDetails) {
    	if (creditsEncashDetails == null){
    		return false;
    	}
    	int rowsUpdated = 0;
    	try {
            String sql = "update fc_credit_encash_bank_detail set bank_account_no = :bankAccountNo, ifsc_code = :ifscCode,"
            		+ " beneficiary_name = :beneficiaryName, email_id= :emailId "
            		+ " where user_id = :userId AND (transfer_status IS NULL OR transfer_status != 'success')";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("bankAccountNo", creditsEncashDetails.getBankAccountNo());
            paramMap.put("ifscCode", creditsEncashDetails.getIfscCode());
            paramMap.put("beneficiaryName", creditsEncashDetails.getBeneficiaryName());
            paramMap.put("emailId", creditsEncashDetails.getEmailId());
            paramMap.put("userId", creditsEncashDetails.getUserId());
            rowsUpdated = this.jt.update(sql, paramMap);
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting credit encash bank details for userId: " + creditsEncashDetails.getUserId() 
            		+ " for details " + creditsEncashDetails,
                    e);
        }
    	
    	boolean isSuccess = false;
    	if (rowsUpdated >= 1){
    		isSuccess = true;
    	}
        return isSuccess;
    }

    public boolean isCreditAlreadyEncashed(Integer userId) {
    	boolean isCreditAlreadyEncashed = false;
        try {
            String sql = "select * from fc_credit_encash_bank_detail where user_id = :userid and transfer_status = 'success'";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("userid", userId);
            List<Map<String, Object>> creditEncashMapList = this.jt.queryForList(sql, paramMap);
            if (creditEncashMapList != null && creditEncashMapList.size() > 0){
            	isCreditAlreadyEncashed = true;
            }
            
            // Added to handle anomalies if any :)
            if (creditEncashMapList.size() > 1){
            	isCreditAlreadyEncashed = true;
            	logger.error("How can there be 2 encash details for email: "  + userId);
            }
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting credit encash bank details : " + userId,
                    e);
        }
        return isCreditAlreadyEncashed;
    }
    
    public List<Map<String, Object>> getCreditEncashDetails(Integer userId) {
    	List<Map<String, Object>> creditEncashMapList = null;
        try {
            String sql = "select * from fc_credit_encash_bank_detail where user_id = :userid";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("userid", userId);
            creditEncashMapList = this.jt.queryForList(sql, paramMap);
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting credit encash bank details : " + userId,
                    e);
        }
        return creditEncashMapList;
    }

    public boolean updateTransactionStatus(CreditsEncashDetails creditsEncashDetails) {
    	if (creditsEncashDetails == null){
    		return false;
    	}
    	int rowsUpdated = 0;
    	try {
			String sql = "update fc_credit_encash_bank_detail set transfer_status = :transferStatus, transfer_date = :transferDate,"
					+ " failure_reason = :failureReason, utr_number = :utr "
					+ " where id = :primaryId AND user_id = :userId AND (transfer_status IS NULL OR transfer_status != 'success')";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("transferStatus", creditsEncashDetails.getTransferStatus());
            paramMap.put("transferDate", creditsEncashDetails.getTransferDate());
            paramMap.put("failureReason", creditsEncashDetails.getFailureReason());
            paramMap.put("utr", creditsEncashDetails.getUtrNumber());
            paramMap.put("userId", creditsEncashDetails.getUserId());
            paramMap.put("primaryId", creditsEncashDetails.getId());
            rowsUpdated = this.jt.update(sql, paramMap);
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting updating transferstatus for userId: " + creditsEncashDetails.getUserId() 
            		+ " for details " + creditsEncashDetails,
                    e);
        }
    	
    	boolean isSuccess = false;
    	if (rowsUpdated >= 1){
    		isSuccess = true;
    	}
        return isSuccess;
    }

    public List<Map<String, Object>> getCreditEncashDetailsNotSuccess() {
    	List<Map<String, Object>> creditEncashMapList = null;
        try {
            String sql = "select * from fc_credit_encash_bank_detail where transfer_status IS NULL OR transfer_status != :istransfered";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("istransfered", "success");
            creditEncashMapList = this.jt.queryForList(sql, paramMap);
        } catch (DataAccessException e) {
            logger.error("Exception thrown while downloading bulk credit encash bank details(not success) : ",
                    e);
        }
        return creditEncashMapList;
    }

    public List<Map<String, Object>> getEncashBanDataForEnteredEmail(String emailId) {
    	List<Map<String, Object>> creditEncashMapList = null;
        try {
            String sql = "select * from fc_credit_encash_bank_detail where email_id = :emailid";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("emailid", emailId);
            creditEncashMapList = this.jt.queryForList(sql, paramMap);
        } catch (DataAccessException e) {
            logger.error("Exception thrown while getting credit encash bank details : " + emailId,
                    e);
        }
        return creditEncashMapList;
    }
}
