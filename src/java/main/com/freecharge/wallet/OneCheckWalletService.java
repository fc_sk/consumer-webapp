package com.freecharge.wallet;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.api.error.ValidationErrorCode;
import com.freecharge.api.exception.InvalidParameterException;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.rest.wallet.InvalidIMSTokenException;
import com.freecharge.wallet.exception.IMSClientException;
import com.freecharge.wallet.exception.OneCheckWalletException;
import com.freecharge.wallet.exception.WalletCreditLimitBreachedException;
import com.freecharge.wallet.service.Wallet.OneCheckTransactionTypePrefix;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.util.FCWalletUtil;
import com.google.gson.GsonBuilder;
import com.snapdeal.ims.client.ILoginUserServiceClient;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.client.impl.LoginUserServiceClientImpl;
import com.snapdeal.ims.client.impl.UserMigrationServiceClientImpl;
import com.snapdeal.ims.client.impl.UserServiceClientImpl;
import com.snapdeal.ims.dto.ClientConfig;
import com.snapdeal.ims.dto.UpgradationInformationDTO;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.enums.Upgrade;
import com.snapdeal.ims.exception.HttpTransportException;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.*;
import com.snapdeal.ims.response.GetTransferTokenResponse;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.ims.response.LoginUserResponse;
import com.snapdeal.ims.response.UserUpgradationResponse;
import com.snapdeal.payments.sdmoney.client.SDMoneyClient;
import com.snapdeal.payments.sdmoney.exceptions.InsufficientFundsException;
import com.snapdeal.payments.sdmoney.exceptions.LimitBreachedException;
import com.snapdeal.payments.sdmoney.exceptions.SDMoneyException;
import com.snapdeal.payments.sdmoney.service.model.*;
import com.snapdeal.payments.sdmoney.service.model.type.AccountDetailsType;
import com.snapdeal.payments.sdmoney.service.model.type.BalanceType;
import com.snapdeal.payments.sdmoney.service.model.type.OrderingCriteria;
import com.snapdeal.payments.sdmoney.service.model.type.VoucherTransaction;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@Service("oneCheckWalletService")
public class OneCheckWalletService {

    public static final String             ONE_CHECK_TXN_TYPE  = "oneCheckTxnType";
    public static final String             CODE                = "code";
    public static final String             SD_IDENTITY         = "sdIdentity";
    public static final String             EVENT_CONTEXT       = "eventContext";
    public static final String             EXPIRY_DURATION     = "expiryDuration";
    public static final String             MTXN_ID             = "mtxnId";
    private static final String            UPGRADE_COMPLETED   = "UPGRADE_COMPLETED";
    private static final String            MONEY_TRANSFERRED   = "MONEY_TRANSFERRED";
    private static final int               MAX_RETRIES_ALLOWED = 2;
    private static final int MAX_CREDIT_RETRY_COUNT = 3;
    private static final long THREAD_SLEEP_MILLIS = 1000;

    private static Logger       logger             = LoggingFactory.getLogger(OneCheckWalletService.class);

    @Autowired
    private SDMoneyClient       sdMoneyClient;

    @Autowired
    private FCProperties        fcProperties;

    @Autowired
    private AmazonDynamoDBAsync dynamoClientMum;

    @Autowired
    private FCWalletUtil        fcWalletUtil;

    @Autowired
    private AppConfigService    appConfigService;

    @Autowired
    private WalletService       walletService;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private SDMoneyClientWrapperService sdmoneyClientWrapperService;


    private @Autowired AutowireCapableBeanFactory beanFactory;

    private AdminSDMoneyClientWrapperService adminSDmoneyClientWrapperService;

    @Autowired
    private ExternalClientCallService externalClientCallService;

    private IUserServiceClient userClient;
    private UserMigrationServiceClientImpl umsc;
    private ILoginUserServiceClient lc;

    @PostConstruct
    public void init() throws OneCheckWalletException {
        try {
            adminSDmoneyClientWrapperService = new AdminSDMoneyClientWrapperService();
            beanFactory.autowireBean(adminSDmoneyClientWrapperService);
            adminSDmoneyClientWrapperService.init();
        } catch (BeansException e1) {
            logger.error("Exception while initializing adminSDmoneyClientWrapperService", e1);
        }

        try {
            com.snapdeal.ims.utils.ClientDetails.init(fcProperties.getProperty(FCProperties.IMS_CLIENT_URL), 
            		fcProperties.getProperty(FCProperties.IMS_CLIENT_PORT),
            		fcProperties.getProperty(FCProperties.IMS_SLAVE_URL),
            		fcProperties.getProperty(FCProperties.IMS_SLAVE_PORT),
            		fcProperties.getProperty(FCProperties.IMS_CLIENT_KEY),
            		fcProperties.getProperty(FCProperties.IMS_CLIENT_ID),
            		fcProperties.getIntProperty(FCProperties.IMS_CLIENT_TIMEOUT));

            userClient = new UserServiceClientImpl();
            umsc = new UserMigrationServiceClientImpl();
            lc = new LoginUserServiceClientImpl();
        } catch (Exception e) {
            logger.error("Exception while initializing IMS Client Config", e);
        }

    }

    public String getUserEmailByTransferToken(String transferToken, String userAgent, String machineIp) throws HttpTransportException, ServiceException {
        LoginWithTransferTokenRequest lwttr = new LoginWithTransferTokenRequest();
        lwttr.setToken(transferToken);
        lwttr.setClientConfig(getClientConfigForIms());
        lwttr.setUserAgent(userAgent);
        lwttr.setUserMachineIdentifier(machineIp);

        LoginUserResponse loginUserWithTransferToken = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetLoginuserWithTransferCommand(lc, lwttr));
        if (loginUserWithTransferToken == null || loginUserWithTransferToken.getUserDetails() == null) {
            throw new IMSClientException(new RuntimeException("Exception while getting user from token: "
                    + transferToken + ",userAgent: " + userAgent + ",machineIp: " + machineIp));
        }
        return loginUserWithTransferToken.getUserDetails().getEmailId();
    }
    
    /***
     * Calls getUserByVerifiedMobile method of IMS client
     * fetches user details from mobile number
     * @param mobileNo
     * @return
     */
    public GetUserResponse getUserByMobile(String mobileNo) {
    	
    	//verify mobile number here
    	
    	GetUserByMobileRequest getUserByMobileRequest = new GetUserByMobileRequest();
    	getUserByMobileRequest.setMobileNumber(mobileNo);
    	getUserByMobileRequest.setClientConfig(getClientConfigForIms());
    	long startMillis = System.currentTimeMillis();
    	
    	try {
            logger.info("Request for fetching user from Mobile: "+mobileNo+" is: "+getUserByMobileRequest.toString());
			GetUserResponse userResponse = userClient.getUserByVerifiedMobile(getUserByMobileRequest);
            logger.info("Response for fetching user from Mobile: "+mobileNo+" is: "+userResponse.toString());
            metricSuccess(startMillis, "getUserByMobile");
            return userResponse;
		} catch (HttpTransportException e) {
			logger.error("Exception while fetching user from Mobile", e);
			recordTypeOfException(startMillis, "getUserByMobile", e);
		} catch (ServiceException e) {
			logger.error("Exception while fetching user from Mobile", e);
			recordTypeOfException(startMillis, "getUserByMobile", e);
		} catch (Exception e) {
			logger.error("Exception while fetching user from Mobile", e);
			recordTypeOfException(startMillis, "getUserByMobile", e);
		}
    	return null;
    }
    
    /***
     * Calls getUserById method of IMS client
     * fetches user details from SD ID
     * @param userId
     * @param userAgent
     * @param machineIp
     * @return
     */
    public GetUserResponse getUserByID(String userId, String userAgent, String machineIp) {

    	GetUserByIdRequest getuserbyidrequest = new GetUserByIdRequest();
    	getuserbyidrequest.setUserId(userId);
    	getuserbyidrequest.setUserAgent(userAgent);
    	getuserbyidrequest.setUserMachineIdentifier(machineIp);
    	getuserbyidrequest.setClientConfig(getClientConfigForIms());
    	long startMillis = System.currentTimeMillis();
    	
    	try {
            logger.info("Request for fetching user from SD ID: "+userId+" is: "+getuserbyidrequest.toString());
			GetUserResponse userResponse = userClient.getUserById(getuserbyidrequest);
            logger.info("Response for fetching user from SD ID: "+userId+" is: "+userResponse.toString());
            metricSuccess(startMillis, "getUserByID");
            return userResponse;
		} catch (HttpTransportException e) {
			logger.error("Exception while fetching user from SD ID", e);
			recordTypeOfException(startMillis, "getUserByID", e);
		} catch (ServiceException e) {
			logger.error("Exception while fetching user from SD ID", e);
			recordTypeOfException(startMillis, "getUserByID", e);
		} catch (Exception e) {
			logger.error("Exception while fetching user from SD ID", e);
			recordTypeOfException(startMillis, "getUserByID", e);
		}
    	return null;
    }
    /***
     * Calls getSDMoneyAccount() of SDMoney Client
     * fetches user's wallet account details from SD Money
     * @param sdIdentity
     * @return
     */
    public GetSDMoneyAccountResponse getSDMoneyAccountDetails(String sdIdentity) {
        GetSDMoneyAccountResponse getSDMoneyAccountResponse = new GetSDMoneyAccountResponse();
        GetSDMoneyAccountRequest getSDMoneyAccountRequest = new GetSDMoneyAccountRequest();
        getSDMoneyAccountRequest.setSdIdentity(sdIdentity);
        long startMillis = System.currentTimeMillis();
        try {
            logger.info("Request for fetching user's wallet account from SD ID: "+sdIdentity+" is: "
                    + getSDMoneyAccountResponse.toString());
            getSDMoneyAccountResponse = sdMoneyClient.getSDMoneyAccount(getSDMoneyAccountRequest);
            logger.info("Response for fetching user's wallet account from SD ID: "+sdIdentity+" is: "
                     + getSDMoneyAccountResponse.toString());
            metricSuccess(startMillis, "getSDMoneyAccountDetails");
            return getSDMoneyAccountResponse;
        } catch (Exception e) {
            logger.error("Exception while fetching user's wallet account from SD ID", e);
            recordTypeOfException(startMillis, "getSDMoneyAccountDetails", e);
        }
        return null;
    }
    /**
     * Calls SDMoneyServiceClient to Credit the general amount in the One Check
     * Wallet. This amount doesn’t expires and can be withdrawn to the Bank
     * account
     *
     * @param params
     * @return
     */
    public CreditGeneralBalanceToUserResponse creditGeneralBalanceToUser(Map<String, Object> params) {
        StringBuilder errorMsg = new StringBuilder();
        if (null != sdMoneyClient && FCUtil.isNotEmpty(params)) {
            WalletCreditRequest walletCreditRequest = (WalletCreditRequest) params
                    .get(WalletWrapper.WALLET_CREDIT_REQUEST);
            String email = (String) params.get(FCConstants.EMAIL);
            OneCheckTxnType oneCheckTxnType = (OneCheckTxnType) params.get(ONE_CHECK_TXN_TYPE);
            if (null == oneCheckTxnType) {
                logger.error("oneCheckTxnType cannot be null. Params map: " + params.toString());
                throw new OneCheckWalletException("oneCheckTxnType cannot be null.");
            }
            String code = null;
            if (null != params.get(CODE)) {
                code = (String) params.get(CODE);
            }
            if (null != walletCreditRequest && null != oneCheckTxnType) {
                String orderId = (String) params.get(FCConstants.ORDER_ID);
                CreditGeneralBalanceToUserRequest creditGeneralBalanceToUserRequest = getCreditBalanceToUserRequestObj(
                        walletCreditRequest, email, oneCheckTxnType, orderId, code);
                if (creditGeneralBalanceToUserRequest != null) {
                    logger.info("Parameters passed to sdmoneyclient for credit to general balance : "
                            + creditGeneralBalanceToUserRequest.toString());
                }
                int retryCount = 0;
                while (retryCount < MAX_CREDIT_RETRY_COUNT) {
                    long startMillis = System.currentTimeMillis();
                    try {
                        CreditGeneralBalanceToUserResponse creditResp = sdmoneyClientWrapperService
                                .creditGeneralBalanceToUser(creditGeneralBalanceToUserRequest);
                        metricSuccess(startMillis, "creditGeneralBalanceToUser");
                        return creditResp;
                    } catch (InterruptedException ie) {
                        recordTypeOfException(startMillis, "creditGeneralBalanceToUser", ie);
                        logger.error("Interrupted Exception while crediting balance to SDMoney:" , ie);
                    } catch (ExecutionException ee) {
                        logger.error("Execution Exception while crediting balance to SDMoney:" , ee);
                        recordTypeOfException(startMillis, "creditGeneralBalanceToUser", ee);
                        if (ExceptionUtils.indexOfThrowable(ee, LimitBreachedException.class) > 0) {
                            logger.error(
                                    "Failed to credit the amount to wallet as wallet limit exceeded for orderId " + orderId,
                                    ee);
                            throw new WalletCreditLimitBreachedException("Failed to credit the amount to wallet as "
                                    + "wallet limit exceeded for orderId " + orderId);
                        }
                    } catch (TimeoutException te) {
                        logger.error("Timeout Exception while crediting balance to SDMoney:", te);
                        recordTypeOfException(startMillis, "creditGeneralBalanceToUser", te);
                    }
                    sleep(THREAD_SLEEP_MILLIS);
                    retryCount++;
                    logger.info("Retry creditGeneralBalanceToUser for " + walletCreditRequest.getMtxnId() + ". Count - " + retryCount);
                }
            }
        }
        errorMsg.append(
                "Unable to call sdMoneyClient.CreditGeneralBalanceToUser to credit balance to user's one check account: ");
        errorMsg.append(params);
        logger.error(errorMsg.toString());
        throw new OneCheckWalletException(errorMsg.toString());
    }

    private void sleep(long sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
        }
    }

    /**
     * Calls SDMoneyServiceClient refund API to refund the debited amount.
     * Refund can happen only against a wallet debit
     *
     * @param params
     * @return
     */

    public RefundBalancesResponse refundBalanceToUser(Map<String, Object> params) {
        StringBuilder errorMsg = new StringBuilder();
        if (null != sdMoneyClient && FCUtil.isNotEmpty(params)) {
            WalletCreditRequest walletCreditRequest = (WalletCreditRequest) params
                    .get(WalletWrapper.WALLET_CREDIT_REQUEST);
            OneCheckTxnType oneCheckTxnType = (OneCheckTxnType) params.get(ONE_CHECK_TXN_TYPE);
            String sdIdentity = (String) params.get(WalletWrapper.ONE_CHECK_IDENTITY);
            if (null == oneCheckTxnType) {
                logger.error("oneCheckTxnType cannot be null. Params map: " + params.toString());
                throw new OneCheckWalletException("oneCheckTxnType cannot be null.");
            }
            String code = null;
            if (null != params.get(CODE)) {
                code = (String) params.get(CODE);
            }
            if (null != walletCreditRequest && null != oneCheckTxnType) {
                int retryCount = 0;
                while (retryCount < MAX_CREDIT_RETRY_COUNT) {
                    long startMillis = System.currentTimeMillis();
                    try {
                        String orderId = (String) params.get(FCConstants.ORDER_ID);
                        String mtxnId = walletCreditRequest.getMtxnId();
                        /*
                         * Getting previous debit txn for recharge
                         */
                        String previousTransactionId = getPreviousDebitTransactionId(orderId, sdIdentity, mtxnId, code,
                                OneCheckTxnType.DEBIT_RECHARGE);
                        if (previousTransactionId == null) {
                            logger.info("Failed to get the previous debit transaction for orderId " + orderId);
                            throw new OneCheckWalletException("Failed to get the previous debit transaction for orderId "
                                    + orderId);
                        }
    
                        RefundBalanceRequest refundBalanceRequest = getRefundBalanceRequest(walletCreditRequest,
                                previousTransactionId, sdIdentity, oneCheckTxnType, orderId, code);
                        if (refundBalanceRequest != null) {
                            logger.info("Parameters passed to sdmoneyclient for credit to general balance : "
                                    + refundBalanceRequest.toString());
                        }
                        try {
                            RefundBalancesResponse refundBalanceResp = sdmoneyClientWrapperService
                                    .refundBalanceToUser(refundBalanceRequest);
                            metricSuccess(startMillis, "refundBalanceToUser");
                            return refundBalanceResp;
                        } catch (InterruptedException ie) {
                            logger.error("Interrupted Exception while crediting balance to SDMoney to user:"
                                    + "userId:" + walletCreditRequest.getUserID(), ie);
                            recordTypeOfException(startMillis, "refundBalanceToUser", ie);
                        } catch (ExecutionException ee) {
                            logger.error("Execution Exception while crediting balance to SDMoney to user:" + "userId:"
                                    + walletCreditRequest.getUserID(), ee );
                            recordTypeOfException(startMillis, "refundBalanceToUser", ee);
                        } catch (TimeoutException te) {
                            logger.error("Timeout Exception while crediting balance to SDMoney to user:" + "userId:"
                                    + walletCreditRequest.getUserID(), te);
                            recordTypeOfException(startMillis, "refundBalanceToUser", te);}
                    } catch (RuntimeException e) {
                        logger.error("RuntimeException while crediting balance to SDMoney for userId: "
                                + walletCreditRequest.getUserID() + " amount: " + walletCreditRequest.getTxnAmount()
                                + " mtxnId: " + walletCreditRequest.getMtxnId() + " transactionType: "
                                + walletCreditRequest.getTransactionType() + " callerReference: "
                                + walletCreditRequest.getCallerReference() + " fundSource: "
                                + walletCreditRequest.getFundSource(), e);
                        recordTypeOfException(startMillis, "refundBalanceToUser", e);
                    }
                    sleep(THREAD_SLEEP_MILLIS);
                    retryCount++;
                    logger.info("Retry refundBalanceToUser for " + walletCreditRequest.getMtxnId() + ". Count - " + retryCount);
                }
            }
        }
        errorMsg.append(
                "Unable to call sdMoneyClient.CreditGeneralBalanceToUser to credit balance to user's one check account: ");
        errorMsg.append(params);
        logger.error(errorMsg.toString());
        throw new OneCheckWalletException(errorMsg.toString());
    }

    public ReverseLoadMoneyResponse reverseWalletLoadMoney(String previousTransactionId, String reason) {
        StringBuilder errorMsg = new StringBuilder();
        long startMillis = System.currentTimeMillis();
        if (null != sdMoneyClient ) {
            if (null != previousTransactionId) {
                ReverseLoadMoneyRequest reverseLoadMoneyRequest = getReverseLoadMoneyRequest(previousTransactionId,
                        reason);
                if (reverseLoadMoneyRequest != null) {
                    logger.info("Parameters passed to sdmoneyclient for reverseLoadMoneyRequest: "
                            + reverseLoadMoneyRequest.toString());
                }
                try {
                    ReverseLoadMoneyResponse reverseLoadMoneyResp = sdmoneyClientWrapperService
                            .reverseLoadMoney(reverseLoadMoneyRequest);
                    metricSuccess(startMillis, "reverseWalletLoadMoney");
                    return reverseLoadMoneyResp;
                } catch (InterruptedException ie) {
                    logger.error("Interrupted Exception while reversing wallet load for previousTransactionId: "
                            + previousTransactionId, ie);
                    recordTypeOfException(startMillis, "reverseWalletLoadMoney", ie);
                } catch (ExecutionException ee) {
                    logger.error("Execution Exception while reversing wallet load for previousTransactionId: "
                            + previousTransactionId, ee);
                    recordTypeOfException(startMillis, "reverseWalletLoadMoney", ee);
                } catch (TimeoutException te) {
                    logger.error("Timeout Exception while reversing wallet load for previousTransactionId: "
                            + previousTransactionId, te);
                    recordTypeOfException(startMillis, "reverseWalletLoadMoney", te);
                }
            }
        }
        errorMsg.append("Unable to call sdMoneyClient.ReverseLoadMoneyForUser from wallet ");
        errorMsg.append(String.format("Previous Transaction ID: %s", previousTransactionId));
        logger.error(errorMsg.toString());
        throw new OneCheckWalletException(errorMsg.toString());
    }
    
    public String getPreviousDebitTransactionId(String orderId, String sdIdentity, String mtxnId, String code,
                                                 OneCheckTxnType expectedTransactionType) {
        List<TransactionSummary> transactionList = findTransactionsByReference(orderId, sdIdentity);

        if (transactionList != null && !transactionList.isEmpty()) {
            for (TransactionSummary transactionSummary : transactionList) {
                if (transactionSummary.getTransactionType().name()
                        .startsWith(OneCheckTransactionTypePrefix.DEBIT.name())
                        && transactionSummary.getIdempotencyId().equals(
                        getIdempotencyId(mtxnId, expectedTransactionType, code))) {
                    return transactionSummary.getTransactionId();
                }
            }
        }
        return null;
    }

    private String getPreviousCreditWalletTransactionId(String orderId, String sdIdentity, String mtxnId, String code,
                                                        OneCheckTxnType expectedTransactionType) {
        List<TransactionSummary> transactionList = findTransactionsByReference(orderId, sdIdentity);

        if (transactionList != null && !transactionList.isEmpty()) {
            for (TransactionSummary transactionSummary : transactionList) {
                if (transactionSummary.getTransactionType().name()
                        .startsWith(OneCheckTransactionTypePrefix.CREDIT.name())
                        && transactionSummary.getIdempotencyId().equals(
                        getIdempotencyId(mtxnId, expectedTransactionType, code))) {
                    return transactionSummary.getTransactionId();
                }
            }
        }
        return null;
    }

    /**
     * Credit the Voucher amount in the One Check Wallet. The voucher amount has
     * a expiry date
     *
     * @param params
     * @return
     */
    public CreditVoucherBalanceResponse creditVoucherBalance(Map<String, Object> params) {
        long startMillis = System.currentTimeMillis();
        try {
            OneCheckTxnType oneCheckTxnType = (OneCheckTxnType) params.get(ONE_CHECK_TXN_TYPE);

            String orderId = (String) params.get(PaymentConstants.ORDER_ID_KEY);
            String code = (String) params.get(CODE);
            String mtxnId = (String) params.get(MTXN_ID);
            String idempotencyId = getIdempotencyId(mtxnId, oneCheckTxnType, code);

            CreditVoucherBalanceRequest creditVoucherBalanceRequest = new CreditVoucherBalanceRequest();
            creditVoucherBalanceRequest.setSdIdentity((String) params.get(SD_IDENTITY));
            creditVoucherBalanceRequest.setTransactionReference(orderId);
            creditVoucherBalanceRequest.setEventContext((String) params.get(EVENT_CONTEXT));
            creditVoucherBalanceRequest.setAmount((BigDecimal) params.get(PaymentConstants.AMOUNT));
            creditVoucherBalanceRequest.setExpiryDuration((Long) params.get(EXPIRY_DURATION));
            creditVoucherBalanceRequest.setIdempotencyId(idempotencyId);
            if (!params.containsKey(InTxnDataConstants.BUSINESS_ENTITY_REQUIRED)
                    || Boolean.parseBoolean(String.valueOf(params.get(InTxnDataConstants.BUSINESS_ENTITY_REQUIRED)))) {

                if(params.containsKey(FCConstants.BUSINESS_ENTITY)) {
                    creditVoucherBalanceRequest.setBusinessEntity((String) params.get(FCConstants.BUSINESS_ENTITY));
                }
                else creditVoucherBalanceRequest.setBusinessEntity(fcProperties.getProperty(FCProperties.ONE_CHECK_BUSINESS_ENTITY));
            }
            creditVoucherBalanceRequest.setSourceCorporateAccountId(String.valueOf(params.get(FCConstants.CORP_ID)));
            logger.info("Credit Voucher Balance request " + creditVoucherBalanceRequest);
            try {
                CreditVoucherBalanceResponse creditVoucherBalanceResp = adminSDmoneyClientWrapperService.creditVoucherBalance(creditVoucherBalanceRequest);
                metricSuccess(startMillis, "creditVoucherBalance");
                return creditVoucherBalanceResp;
            } catch (InterruptedException ie) {
                recordTypeOfException(startMillis, "creditVoucherBalance", ie);
                logger.error("Interrupted Exception while crediting voucher balance to user:",ie);
            } catch (ExecutionException ee) {
                recordTypeOfException(startMillis, "creditVoucherBalance", ee);
                logger.error("Execution Exception while crediting voucher balance to user:",ee);
            } catch (TimeoutException te)  {
                recordTypeOfException(startMillis, "creditVoucherBalance", te);
                logger.error("Timeout Exception while crediting voucher balance to user:",te);
            } catch (Exception e) {
                recordTypeOfException(startMillis, "creditVoucherBalance", e);
                logger.error("Exception in rewarding user with gift voucher for creditvoucherbalancerequest " + creditVoucherBalanceRequest, e);
            }
        } catch (SDMoneyException e) {
            recordTypeOfException(startMillis, "creditVoucherBalance", e);
            logger.error("Exception while crediting voucher balance: " + params.toString(), e);
            throw new OneCheckWalletException(e);
        }
        return null;
    }

    /**
     * Calls SDMoneyServiceClient to debit money from user's one check wallet
     *
     */
    public DebitBalanceResponse debitBalance(Map<String, Object> params) {
        StringBuilder errorMsg = new StringBuilder();
        long startMillis = System.currentTimeMillis();
        DebitBalanceRequest debitBalanceRequest = null;
        WalletDebitRequest walletDebitRequest = null;
        if (null != sdMoneyClient && FCUtil.isNotEmpty(params)) {
            walletDebitRequest = (WalletDebitRequest) params.get(WalletWrapper.WALLET_DEBIT_REQUEST);
            OneCheckTxnType oneCheckTxnType = (OneCheckTxnType) params.get(ONE_CHECK_TXN_TYPE);
            if (null == oneCheckTxnType) {
                logger.error("oneCheckTxnType cannot be null. Params map: " + params.toString());
                throw new OneCheckWalletException("oneCheckTxnType cannot be null.");
            }
            String orderId = (String) params.get(FCConstants.ORDER_ID);
            String sdIdentity = (String) params.get(WalletWrapper.ONE_CHECK_IDENTITY);
            String corpId = (String) params.get(WalletWrapper.DESTINATION_CORP_ID);
            debitBalanceRequest = getDebitBalanceRequest(walletDebitRequest, oneCheckTxnType,
                    sdIdentity, orderId, corpId);
            logger.info("debitBalanceRequest params " + debitBalanceRequest);
            try {
                DebitBalanceResponse debitBalaceResp = sdmoneyClientWrapperService.debitBalance(debitBalanceRequest);
                metricSuccess(startMillis, "debitBalance");
                return debitBalaceResp;
            } catch (InterruptedException ie) {
                logger.error("Interrupted Exception while debiting money from SDMoney:",ie);
                recordTypeOfException(startMillis, "debitBalance", ie);
            } catch (ExecutionException ee) {
                logger.error("Execution Exception while debiting money from SDMoney:",ee);
                recordTypeOfException(startMillis, "debitBalance", ee);
            } catch (TimeoutException te)  {
                logger.error("Timeout Exception while debiting money from SDMoney:",te);
                recordTypeOfException(startMillis, "debitBalance", te);
            }
        }

        errorMsg.append("Unable to call sdMoneyClient.debitBalance to debit money from user's one check account: ");
        errorMsg.append(params);
        logger.error(errorMsg.toString());
        throw new OneCheckWalletException(errorMsg.toString());
    }

    /**
     * Calls SDMoneyServiceClient to get TransactionByIdempotencyId
     * @param sdIdentity
     * @return
     */
    private Transaction findTransactionsByIdempotencyId(String idempotencyKey, String sdIdentity) {
        long startMillis= System.currentTimeMillis();
        try {
            GetTransactionByIdempotencyIdRequest getTransactionByIdempotencyIdRequest = new GetTransactionByIdempotencyIdRequest();
            getTransactionByIdempotencyIdRequest.setIdempotencyKey(idempotencyKey);
            getTransactionByIdempotencyIdRequest.setSdIdentity(sdIdentity);
            GetTransactionByIdempotencyIdResponse transactionByIdempotencyIdResponse = sdmoneyClientWrapperService
                    .findTransactionsByIdempotency(getTransactionByIdempotencyIdRequest);
            Transaction txn = transactionByIdempotencyIdResponse.getTransaction();
            metricSuccess(startMillis, "findTransactionsByIdempotencyId");
            return txn;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while findTransactionsByIdempotencyId:",ie);
            recordTypeOfException(startMillis, "findTransactionsByIdempotencyId", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while findTransactionsByIdempotencyId:",ee);
            recordTypeOfException(startMillis, "findTransactionsByIdempotencyId", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while findTransactionsByIdempotencyId:",te);
            recordTypeOfException(startMillis, "findTransactionsByIdempotencyId", te);
        }
        return null;
    }
    
    public Transaction findTxnByIdempotencyId(String mtxnId, OneCheckTxnType oneCheckTxnType, String sdIdentity) {
        String idempotencyKey = getIdempotencyId(mtxnId, oneCheckTxnType, null);
        return findTransactionsByIdempotencyId(idempotencyKey, sdIdentity);
    }
    
    public List<TransactionSummary> findTransactionsByReference(String orderId, String sdIdentity) {
        long startMillis= System.currentTimeMillis();
        try {
            GetTransactionsByReferenceRequest transactionByReferenceRequest = new GetTransactionsByReferenceRequest();
            transactionByReferenceRequest.setTransactionReference(orderId);
            transactionByReferenceRequest.setSdIdentity(sdIdentity);
            GetTransactionsByReferenceResponse transactionByReference = sdmoneyClientWrapperService
                    .findTransactionsByReference(transactionByReferenceRequest);
            List<TransactionSummary> txnSummaries = transactionByReference.getListTransaction();
            metricSuccess(startMillis, "findTransactionsByReference");
            return txnSummaries;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while finding transaction by reference:",ie);
            recordTypeOfException(startMillis, "findTransactionsByReference", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while finding transaction by reference:",ee);
            recordTypeOfException(startMillis, "findTransactionsByReference", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while finding transaction by reference:",te);
            recordTypeOfException(startMillis, "findTransactionsByReference", te);
        }
        return null;
    }

    public GetTransactionsResponse findTransactions(String sdIdentity, Date startDate, Date endDate, Integer pageSize, String evaulatedKey) {
        GetTransactionsRequest getTransactionsRequest = new GetTransactionsRequest();
        long startMillis = System.currentTimeMillis();
        if (startDate != null) {
            getTransactionsRequest.setStartTime(startDate);
        }

        if (endDate == null) {
            endDate = new Date();
        }

        getTransactionsRequest.setSdIdentity(sdIdentity);
        getTransactionsRequest.setEndTime(endDate);
        getTransactionsRequest.setPageSize(pageSize);
        getTransactionsRequest.setOrderingCriteria(OrderingCriteria.DESC);
        getTransactionsRequest.setLastEvaluatedKey(evaulatedKey);
        GetTransactionsResponse getTransactionsResponse = null;
        logger.info("Request for getting transaction : " + getTransactionsRequest.toString());
        try {
            getTransactionsResponse = sdmoneyClientWrapperService.findTransactions(getTransactionsRequest);
            logger.info("Response for getting transaction : " + getTransactionsResponse.toString());
            metricSuccess(startMillis, "findTransactions");
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting txn data:",ie);
            recordTypeOfException(startMillis, "findTransactions", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while getting txn data:",ee);
            recordTypeOfException(startMillis, "findTransactions", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while getting txn data:",te);
            recordTypeOfException(startMillis, "findTransactions", te);
        } catch (Exception e) {
            logger.error("Caught exception while getting txn data from API for " + sdIdentity, e);
            recordTypeOfException(startMillis, "findTransactions", e);
            return null;
        }
        return getTransactionsResponse;
    }

    /**
     * Returns a specific Transaction with idempotencyId = mtxnId + "_" +
     * oneCheckTxnType.toString() + "_" + code from the list of transactions
     * returned by List<com.snapdeal.payments.sdmoney.service.modelTransaction>
     * findTransactionsByReference(String orderId, String sdIdentity) method
     *
     * @param oneCheckTxnType
     * @param orderId
     * @param mtxnId
     * @param sdIdentity
     * @param code
     * @return
     */
    public TransactionSummary findTransactionIdempotencyId(OneCheckTxnType oneCheckTxnType, String orderId,
                                                           String mtxnId, String sdIdentity, String code) {
        List<TransactionSummary> transactionsByReference = findTransactionsByReference(orderId, sdIdentity);
        String idempotencyId = getIdempotencyId(mtxnId, oneCheckTxnType, code);
        if(transactionsByReference != null) {
            for (TransactionSummary transaction : transactionsByReference) {
                if (idempotencyId.equals(transaction.getIdempotencyId())) {
                    return transaction;
                }
            }
        }
        return null;
    }

    /**
     * @param transactionTypePrefix
     *            - CREDIT/DEBIT
     * @param orderId
     * @param sdIdentity
     * @return
     */
    public List<TransactionSummary> findTransactionTransactionType(OneCheckTransactionTypePrefix transactionTypePrefix,
                                                                   String orderId, String sdIdentity) {
        List<TransactionSummary> transactionsByReference = findTransactionsByReference(orderId, sdIdentity);

        List<TransactionSummary> transactionsForType = new ArrayList<>();

        if (transactionsByReference == null) {
            return transactionsForType;
        }

        for (TransactionSummary transaction : transactionsByReference) {
            if (transaction.getTransactionType().name().startsWith(transactionTypePrefix.name())) {
                transactionsForType.add(transaction);
            }
        }
        return transactionsForType;
    }

    /**
     * Returns the total balance in fc wallet
     *
     * @param sdIdentity
     * @return com.snapdeal.payments.sdmoney.service.model.Balance
     */
    public Balance getFCWalletAccountBalance(String sdIdentity) {
        logger.info("Getting balance for onecheck user id : " + sdIdentity);
        GetAccountBalanceRequest getAccountBalanceRequest = new GetAccountBalanceRequest();
        getAccountBalanceRequest.setSdIdentity(sdIdentity);

        for (int retryAttempt = 0; retryAttempt < MAX_RETRIES_ALLOWED; retryAttempt++) {
            long startMillis = System.currentTimeMillis();
            try {
                GetAccountBalanceResponse accountBalance = sdmoneyClientWrapperService
                        .getFCWalletAccountBalance(getAccountBalanceRequest);
                metricSuccess(startMillis, "getFCWalletAccountBalance");
                return accountBalance.getBalance();
            } catch (InterruptedException ie) {
                logger.error("Interrupted Exception while getting account fc wallet balance for one check user:" , ie);
                recordTypeOfException(startMillis, "getFCWalletAccountBalance", ie);
                break;
            } catch (ExecutionException ee) {
                logger.error("Execution Exception while getting account fc wallet balance for one check user:" , ee);
                recordTypeOfException(startMillis, "getFCWalletAccountBalance", ee);
                break;
            } catch (TimeoutException te) {
                logger.error("Timeout Exception while getting account fc wallet balance for one check user:" , te);
            }
        }
        return null;
    }

    public UserDetailsDTO getOneCheckUserByToken(String token) {
        GetUserByTokenRequest request = new GetUserByTokenRequest();
        request.setToken(token);
        GetUserResponse userByToken = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByTokenCommand(userClient, request));
        if (userByToken == null) {
            throw new IMSClientException(new RuntimeException("Exception while getting user from token: " + token));
        }
        return userByToken.getUserDetails();
    }

 
    /**
     * Given an email returns the wallet migration status ie whether FC wallet
     * exists for the user and one check id of the user
     *
     * @param email
     * @return com.freecharge.umsclient.response.vo.MigrationStatus
     */
    public MigrationStatus getFcWalletMigrationStatus(String email, String token) {
        /*
         *  Commented to restrict api call to IMS as All user migrated.
         *  Keeping this code commented. Would be handy if required to revert this change in anycase.
         * 
         * return externalClientCallService
                .executewithtimeout(umsimscommandbuilder.getgetfcwalletmigrationstatuscommand(userserviceproxy,email,token));*/
    	MigrationStatus migrationStatus = new MigrationStatus();
    	migrationStatus.setMigrationStatus(FCConstants.UPGRADE_COMPLETED);
    	return migrationStatus;
    }

    /**
     * Returns CreditGeneralBalanceToUserRequest object that is required by
     * creditGeneralBalanceToUser method of sdMoneyClient
     *
     * @param walletCreditRequest
     * @param email
     * @param orderId
     * @return
     */
    private CreditGeneralBalanceToUserRequest getCreditBalanceToUserRequestObj(WalletCreditRequest walletCreditRequest,
                                                                               String email, OneCheckTxnType oneCheckTxnType, String orderId, String code) {
        CreditGeneralBalanceToUserRequest creditGeneralBalanceToUserRequest = new CreditGeneralBalanceToUserRequest();
        creditGeneralBalanceToUserRequest.setBusinessEntity(fcProperties
                .getProperty(FCProperties.ONE_CHECK_BUSINESS_ENTITY));
        creditGeneralBalanceToUserRequest.setEmailId(email);
        creditGeneralBalanceToUserRequest.setAmount(walletCreditRequest.getTxnAmount().getAmount());
        creditGeneralBalanceToUserRequest.setIdempotencyId(getIdempotencyId(walletCreditRequest.getMtxnId(), oneCheckTxnType, code));
        if(walletCreditRequest.getAppendSufix() != null && !walletCreditRequest.getAppendSufix().trim().isEmpty())
        {
        	creditGeneralBalanceToUserRequest.setIdempotencyId(creditGeneralBalanceToUserRequest.getIdempotencyId() + walletCreditRequest.getAppendSufix());
        }
        creditGeneralBalanceToUserRequest.setTransactionReference(orderId);
        Map<String, Object> eventContextMap = new HashMap<>();
        eventContextMap.put("userId", walletCreditRequest.getUserID());
        eventContextMap.put("transactionType", walletCreditRequest.getTransactionType());
        eventContextMap.put("metaData", walletCreditRequest.getMetadata());
        String eventContext = new GsonBuilder().create().toJson(eventContextMap);
        creditGeneralBalanceToUserRequest.setEventContext(eventContext);
        return creditGeneralBalanceToUserRequest;
    }

    private RefundBalanceRequest getRefundBalanceRequest(WalletCreditRequest walletCreditRequest,
                                                         String prevTransactionId, String sdIdentity, OneCheckTxnType oneCheckTxnType, String orderId, String code) {
        RefundBalanceRequest refundBalanceRequest = new RefundBalanceRequest();
        refundBalanceRequest.setBusinessEntity(fcProperties.getProperty(FCProperties.ONE_CHECK_BUSINESS_ENTITY));
        refundBalanceRequest.setRefundAmount(walletCreditRequest.getTxnAmount().getAmount());
        refundBalanceRequest.setSdIdentity(sdIdentity);
        refundBalanceRequest.setIdempotencyKey(getIdempotencyId(walletCreditRequest.getMtxnId(), oneCheckTxnType, code));
        refundBalanceRequest.setTransactionReference(orderId);
        refundBalanceRequest.setPrevTransactionId(prevTransactionId);
        Map<String, Object> eventContextMap = new HashMap<>();
        eventContextMap.put("userId", walletCreditRequest.getUserID());
        eventContextMap.put("transactionType", walletCreditRequest.getTransactionType());
        eventContextMap.put("metaData", walletCreditRequest.getMetadata());
        String eventContext = new GsonBuilder().create().toJson(eventContextMap);
        refundBalanceRequest.setEventContext(eventContext);
        return refundBalanceRequest;
    }

    private ReverseLoadMoneyRequest getReverseLoadMoneyRequest(
            String prevTransactionId, String reason) {
        ReverseLoadMoneyRequest reverseLoadMoneyRequest = new ReverseLoadMoneyRequest();
        reverseLoadMoneyRequest.setPrevTransactionId(prevTransactionId);
        reverseLoadMoneyRequest.setReverseReason(reason);
        return reverseLoadMoneyRequest;
    }

    /**
     * Returns DebitBalanceRequest object that is required by debitBalance
     * method of sdMoneyClient
     *
     * @param walletDebitRequest
     * @param oneCheckTxnType
     * @param sdIdentity
     * @param orderId
     * @return
     */
	private DebitBalanceRequest getDebitBalanceRequest(WalletDebitRequest walletDebitRequest,
			OneCheckTxnType oneCheckTxnType, String sdIdentity, String orderId, String destCorpId) {
		DebitBalanceRequest debitBalanceRequest = new DebitBalanceRequest();
		debitBalanceRequest.setTransactionReference(walletDebitRequest.getMtxnId());
		debitBalanceRequest.setAmount(walletDebitRequest.getTxnAmount().getAmount());
		debitBalanceRequest.setSdIdentity(sdIdentity);
		debitBalanceRequest.setIdempotencyId(getIdempotencyId(walletDebitRequest.getMtxnId(), oneCheckTxnType, null));
		debitBalanceRequest.setEventContext(walletDebitRequest.getMetadata());
		debitBalanceRequest.setDestinationCorpAccountId(destCorpId);
		if (StringUtils.isEmpty(destCorpId)) {
			debitBalanceRequest.setBusinessEntity(fcProperties.getProperty(FCProperties.ONE_CHECK_BUSINESS_ENTITY));
		}
		return debitBalanceRequest;
	}

    /**
     * get idempotency id for one check wallet
     *
     * @param mtxnId
     *            : FC orderId
     * @param oneCheckTxnType
     * @param code
     *            : required in case cashback code or freefund code is redeemed
     *            else can be null
     * @return
     */
    private String getIdempotencyId(String mtxnId, OneCheckTxnType oneCheckTxnType, String code) {
        StringBuffer idempotencyId = new StringBuffer();
        idempotencyId = idempotencyId.append(mtxnId).append("_").append(oneCheckTxnType.toString());
        if (FCUtil.isNotEmpty(code)) {
            idempotencyId.append("_").append(code);
        }
        /*if(false)
        {
        	Date date = new Date();
        	idempotencyId.append("_").append(date.getYear()+"_"+date.getMonth()+"_"+date.getDay());
        }*/
        return idempotencyId.toString();
    }

    public boolean isSameIdempotency(String idempotency, String mtxnId, OneCheckTxnType oneCheckTxnType, String code) {
        return getIdempotencyId(mtxnId, oneCheckTxnType, code).equals(idempotency);
    }


    /**
     * Gets oneCheckIdentity from dynamodb, if it is not there makes a call to
     * IMS service to fetch it and then store it in dynamodb Returns null if
     * user is not a one check customer or one check wallet is disabled
     *
     * @param email
     * @return
     */

	public String getOneCheckIdIfOneCheckWalletEnabled(String email) {
		long startTime = System.currentTimeMillis();
		if (FCUtil.isEmpty(email)) {
			logger.error("Email is null for fetching one check id");
			return null;
		}
		if (!isOneCheckWalletEnabled()) {
			logger.error("One check wallet is disabled");
			// return null; //TODO: We are making sure this flag is always
			// enabled. it doesnt take one check wallet enable flag into
			// consideration.
		}
		String oneCheckIdentity = null;
		if (appConfigService.isIMSDirectHitEnabled()) {
			logger.info("checking for imsdirecthit in app config toggle");
			oneCheckIdentity = getOneCheckIdentityFromIMS(email);
			metricSuccess(startTime, "getOneCheckIdFromDirectIms");
		} else {
			logger.info("Fetching one check id from DynamoDB for email : " + email);
			oneCheckIdentity = fcWalletUtil.getOneCheckIdentityFromDynamo(email);
			if (FCUtil.isEmpty(oneCheckIdentity)) {
				logger.info("One check id not present in dynamo db");
				oneCheckIdentity = getOneCheckIdentityFromIMS(email);
				Map<String, AttributeValue> putItemData = new HashMap<>();
				putItemData.put(FCWalletUtil.EMAIL_ID_COL_NAME, new AttributeValue(email));
				putItemData.put(FCWalletUtil.ONE_CHECK_ID_COL_NAME, new AttributeValue(oneCheckIdentity));
				dynamoClientMum.putItem(new PutItemRequest(FCWalletUtil.FC_ONE_CHECK_USER_MAPPING_TABLE, putItemData));
				logger.info("Putting one check id in dynamo db");
			}
			metricSuccess(startTime, "getOneCheckIdFromDynamoDB");
		}
		return oneCheckIdentity;
	}
	
    private boolean isOneCheckWalletEnabled() {
        // Get this from appConfig
        return appConfigService.isOneCheckWalletEnabled();
    }

    /**
     * Returns true if user is a one check customer(ie one check wallet exists
     * for the user)
     *
     * @param emailId
     * @return
     */
    private boolean isOneCheckCustomer(String emailId) {
        UserUpgradeByEmailRequest upgradeRequest = new UserUpgradeByEmailRequest();
        upgradeRequest.setEmailId(emailId);
        UserUpgradationResponse userUpgradationResponse = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetUserUpgradeStatusCommand(umsc, upgradeRequest));
        if (userUpgradationResponse == null) {
            throw new IMSClientException(
                    new RuntimeException("Exception while checking is one check customer for user email: " + emailId));
        }
        UpgradationInformationDTO upgradationInformation = userUpgradationResponse.getUpgradationInformation();
        Upgrade upgrade = upgradationInformation.getUpgrade();
        if (UPGRADE_COMPLETED.equals(upgrade.toString())) {
            return true;
        }
        return false;
    }

    public String getOneCheckIdentityFromIMS(String emailId) {
        GetUserByEmailRequest userByEmailRequest = new GetUserByEmailRequest();
        userByEmailRequest.setEmailId(emailId);
        GetUserResponse user = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByEmailCommand(userClient, userByEmailRequest));
        if (user == null) {
            throw new IMSClientException(new RuntimeException("Exception while getting user from token: " + emailId));
        }
        UserDetailsDTO userDetails = user.getUserDetails();
        return userDetails.getUserId();
    }

    public UserDetailsDTO getOneCheckUserDetailsFromIMS(String emailId) {
        GetUserByEmailRequest userByEmailRequest = new GetUserByEmailRequest();
        userByEmailRequest.setEmailId(emailId);
        GetUserResponse user = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByEmailCommand(userClient, userByEmailRequest));
        if (user == null) {
            throw new IMSClientException(new RuntimeException("Exception while getting user from token: " + emailId));
        }
        UserDetailsDTO userDetails = user.getUserDetails();
        return userDetails;
    }

    public Map<String, String> getUserByToken(String token, String userAgent, String machineIdentifier) {
        GetUserByTokenRequest gtr = new GetUserByTokenRequest();
        gtr.setToken(token);
        gtr.setClientConfig(getClientConfigForIms());
        gtr.setUserAgent(userAgent);
        gtr.setUserMachineIdentifier(machineIdentifier);
        GetUserResponse response = null;
        try {
            response = externalClientCallService
                    .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByTokenCommand(userClient, gtr));

            if (response == null || response.getUserDetails() == null) {
                return null;
            }

            Map<String, String> userDetailsMap = new HashMap<>();
            userDetailsMap.put("emailId", response.getUserDetails().getEmailId());
            userDetailsMap.put("mobileNumber", response.getUserDetails().getMobileNumber());
            userDetailsMap.put("firstName", response.getUserDetails().getFirstName());
            userDetailsMap.put("middleName", response.getUserDetails().getMiddleName());
            userDetailsMap.put("lastName", response.getUserDetails().getLastName());
            userDetailsMap.put("displayName", response.getUserDetails().getDisplayName());

            if (response.getUserDetails().getGender() != null) {
                userDetailsMap.put("gender", response.getUserDetails().getGender().getValue());
            } else {
                userDetailsMap.put("gender", "NA");
            }

            userDetailsMap.put("dob", response.getUserDetails().getDob());

            if (response.getUserDetails().getLanguagePref() != null) {
                userDetailsMap.put("languagePref", response.getUserDetails().getLanguagePref().getValue());
            } else {
                userDetailsMap.put("languagePref", "NA");
            }

            userDetailsMap.put("token", token);

            MigrationStatus migrationStatus = getFcWalletMigrationStatus(response.getUserDetails().getEmailId(), token);
            Amount balance = walletService.findBalance(response.getUserDetails().getFcUserId());

            userDetailsMap.put("migrationStatus", migrationStatus.getMigrationStatus());
            userDetailsMap.put("balance", balance.getAmount().toString());
            return userDetailsMap;

        } catch (Exception e) {
            logger.error("Exception while getting user from token: " + token, e);
            throw new IMSClientException(e);
        }

    }

    public Map<String, String> getTransferToken(Map<String, String> headerMap) {
        LoginWithTransferTokenRequest lwttr = new LoginWithTransferTokenRequest();
        lwttr.setToken(headerMap.get(OneCheckConstants.TOKEN));
        lwttr.setClientConfig(getClientConfigForIms());
        lwttr.setUserAgent(headerMap.get(OneCheckConstants.USER_AGENT));
        lwttr.setUserMachineIdentifier(headerMap.get(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER));
        LoginUserResponse response = null;
        try {
            response = externalClientCallService
                    .executeWithTimeOut(UmsImsCommandBuilder.getGetLoginuserWithTransferCommand(lc, lwttr));
            if (response == null || response.getTokenInformation() == null) {
            	return null;
            }

            Map<String, String> userDetailsMap = new HashMap<>();
            userDetailsMap.put("emailId", response.getUserDetails().getEmailId());
            userDetailsMap.put("mobileNumber", response.getUserDetails().getMobileNumber());
            userDetailsMap.put("firstName", response.getUserDetails().getFirstName());
            userDetailsMap.put("middleName", response.getUserDetails().getMiddleName());
            userDetailsMap.put("lastName", response.getUserDetails().getLastName());
            userDetailsMap.put("displayName", response.getUserDetails().getDisplayName());
            userDetailsMap.put("fcUserId", String.valueOf(response.getUserDetails().getFcUserId()));

            if (response.getUserDetails().getGender() != null) {
                userDetailsMap.put("gender", response.getUserDetails().getGender().getValue());
            } else {
                userDetailsMap.put("gender", "NA");
            }

            userDetailsMap.put("dob", response.getUserDetails().getDob());

            if (response.getUserDetails().getLanguagePref() != null) {
                userDetailsMap.put("languagePref", response.getUserDetails().getLanguagePref().getValue());
            } else {
                userDetailsMap.put("languagePref", "NA");
            }

            userDetailsMap.put("token", response.getTokenInformation().getToken());

            MigrationStatus migrationStatus = externalClientCallService
                    .executeWithTimeOut(UmsImsCommandBuilder.getGetFcWalletMigrationStatusCommand(userServiceProxy, response.getUserDetails().getEmailId(),null));
            Amount balance = walletService.findBalance(response.getUserDetails().getFcUserId());

            userDetailsMap.put("migrationStatus", migrationStatus.getMigrationStatus());
            userDetailsMap.put("balance", balance.getAmount().toString());
            return userDetailsMap;

        } catch (Exception e) {
            logger.error(
                    "Caught Exception while getting transfer token for token " + headerMap.get(OneCheckConstants.TOKEN),
                    e);
            return new HashMap<>();
        }
    }

    public Map<String, String> generateTransferTOken(String authToken) {
        GetTransferTokenRequest gttr = new GetTransferTokenRequest();
        gttr.setToken(authToken);
        gttr.setTargetImsConsumer("FC_WALLET");
        gttr.setClientConfig(getClientConfigForIms());
        gttr.setUserAgent("FC-generic-UA");
        gttr.setUserMachineIdentifier(UUID.randomUUID().toString());

        try {
            GetTransferTokenResponse response= externalClientCallService
                    .executeWithTimeOut(UmsImsCommandBuilder.getGetTransferTokenCommand(lc, gttr));
            Map<String, String> tokenDetailsMap = new HashMap<>();
            tokenDetailsMap.put("transferToken", response.getTransferTokenDto().getTransferToken());
            tokenDetailsMap.put("tokenExpiry", response.getTransferTokenDto().getTransferTokenExpiry().toString());
            return tokenDetailsMap;
        } catch (Exception ex) {
            logger.error("Caught Exception while getting transfer token for token " + authToken, ex);
            throw new IMSClientException(ex);
        }

    }

    private ClientConfig getClientConfigForIms() {
        ClientConfig config = new ClientConfig();
        config.setApiTimeOut(fcProperties.getIntProperty(FCProperties.IMS_CLIENT_TIMEOUT));
        config.setClientId(fcProperties.getProperty(FCProperties.IMS_CLIENT_ID));
        config.setClientKey(fcProperties.getProperty(FCProperties.IMS_CLIENT_KEY));
        return config;
    }

    private ClientConfig getInterimClientConfigForIms() {
        ClientConfig config = new ClientConfig();
        config.setApiTimeOut(fcProperties.getIntProperty(FCProperties.IMS_CLIENT_TIMEOUT));
        config.setClientId(fcProperties.getProperty(FCProperties.INTERIM_CLIENT_ID));
        config.setClientKey(fcProperties.getProperty(FCProperties.INTERIM_CLIENT_KEY));
        return config;
    }

    public GetUserResponse getUserDetailsByToken(String token, String userAgent, String machineIdentifier) {
        GetUserByTokenRequest gtr = new GetUserByTokenRequest();
        gtr.setToken(token);
        gtr.setUserAgent(userAgent);
        gtr.setUserMachineIdentifier(machineIdentifier);
        gtr.setClientConfig(getClientConfigForIms());

        GetUserResponse response = externalClientCallService
                .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByTokenCommand(userClient, gtr));
        if (response == null) {
            logger.error("Exception while getting user from token: " + token);
            /*
             * this change is done for partially migrated user this does not
             * affect normal flow
             */
            gtr.setClientConfig(getInterimClientConfigForIms());
            response = externalClientCallService
                    .executeWithTimeOut(UmsImsCommandBuilder.getGetUserByTokenCommand(userClient, gtr));
        }
        if (response == null) {
            throw new IMSClientException(new RuntimeException("Exception while getting user from token: " + token));
        }
        return response;
    }

    public static enum OneCheckTxnType {
        CREDIT_RECHARGE, CREDIT_REFUND, CREDIT_REFUND_API, CREDIT_PROMOCODE, CREDIT_FREEFUND, CREDIT_ADDCASH,
        CREDIT_ONE_CHECK_MIGRATION, DEBIT_RECHARGE, DEBIT_REFUND, DEBIT_WALLET, CREDIT_REWARD, CS_CASHBACK, CS_REFUND,
        DEBIT_REVOKE_FUND, CREDIT_WALLET_LOAD, CREDIT_CAMPAIGN_REWARD, DEBIT_VC, CREDIT_VC_REVERSAL,
        DEBIT_FUNDPOST_CS_VC, DEBIT_FUNDPOST_MM_VC, DEBIT_FUNDPOST_BULKREFUND_VC, CREDIT_FUNDPOST_CS_VC,
        CREDIT_FUNDPOST_MM_VC, CREDIT_FUNDPOST_BULKREFUND_VC, DEBIT_FUNDPOST_VC, CREDIT_FUNDPOST_VC,
        DEBIT_FUNDPOST_BulkRefund_VC, CREDIT_FUNDPOST_BulkRefund_VC
    }

    public boolean isLoggedInOCUser(HttpServletRequest request) {
        String email = FCSessionUtil.getLoggedInEmailId();
        long startMillis = System.currentTimeMillis();
        if (email == null) {
            logger.info("Logged in email id in session is null.");
            return false;
        }
        if (null != getOneCheckIdIfOneCheckWalletEnabled(email)) {
            try {
                Integer userId = FCSessionUtil.getLoggedInUserId();
                if (userId == null) {
                    return false;
                }
                String userAgent = null;
                String ipAddress = null;
                if (request != null) {
                    userAgent = request.getHeader("User-Agent");
                    ipAddress = request.getHeader("X-FORWARDED-FOR");
                    if (StringUtils.isBlank(ipAddress)) {
                        ipAddress = request.getRemoteAddr();
                    }
                }
                if (StringUtils.isBlank(ipAddress)) {
                    ipAddress = "NA";
                }

                if (StringUtils.isBlank(userAgent)) {
                    userAgent = "Chrome";
                }

                String token = FCSessionUtil.getAppCookie(request);
                Map<String, String> userDetails = getUserByToken(token, userAgent, ipAddress);
                String imsEmailId = userDetails.get("emailId");
                if (imsEmailId == null || !imsEmailId.equalsIgnoreCase(email)) {
                    return false;
                }
                metricSuccess(startMillis, "isLoggedInOCUser");
            } catch (Exception e) {
                logger.error("Caught Exception on checking OCUser Logged in token validation ", e);
                recordTypeOfException(startMillis, "isLoggedInOCUser", e);
                return false;
            }
        }
        return true;
    }

    public GetVoucherDetailsResponse getVoucherDetailsForTransaction(TransactionSummary transactionSummary) {
        long startMillis = System.currentTimeMillis();
        try {
            logger.info("Getting voucher details for transaction : " + transactionSummary.getTransactionReference());
            GetVouchersForTransactionRequest request = new GetVouchersForTransactionRequest();
            request.setTransactionId(transactionSummary.getTransactionId());
            GetVouchersForTransactionResponse response = sdmoneyClientWrapperService.getVoucherDetailsForTransaction(request);
            if (response != null && !response.getVoucherTransactionList().isEmpty()) {
                for (VoucherTransaction voucherTransaction : response.getVoucherTransactionList()) {
                    GetVoucherDetailsResponse voucherDetailsResponse = getVoucherDetails(voucherTransaction
                            .getVoucherId());
                    if (voucherDetailsResponse != null) {
                        metricSuccess(startMillis, "getVoucherDetailsForTransaction");
                        return voucherDetailsResponse;
                    }
                }
            }
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting voucher details for transaction:",ie);
            recordTypeOfException(startMillis, "getVoucherDetailsForTransaction", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while getting voucher details for transaction:",ee);
            recordTypeOfException(startMillis, "getVoucherDetailsForTransaction", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while getting voucher details for transaction:",te);
            recordTypeOfException(startMillis, "getVoucherDetailsForTransaction", te);
        } catch (Exception exception) {
            logger.error("Exception getting voucher details for transaction : "
                    + transactionSummary.getTransactionReference());
            recordTypeOfException(startMillis, "getVoucherDetailsForTransaction", exception);
        }
        return null;
    }

    public GetExpiredVoucherDetailsResponse getExpiredVouchersForUser(String oneCheckUserId) {
        long startMillis = System.currentTimeMillis();
        GetExpiredVoucherDetailsRequest expiredVoucherDetailsRequest = new GetExpiredVoucherDetailsRequest();
        expiredVoucherDetailsRequest.setSdIdentity(oneCheckUserId);
        try {
            GetExpiredVoucherDetailsResponse getExpiredVoucherDetailaResp = sdmoneyClientWrapperService.getExpiredVouchersForUser(expiredVoucherDetailsRequest);
            metricSuccess(startMillis, "getExpiredVouchersForUser");
            return getExpiredVoucherDetailaResp;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting expired vouchers for user:" , ie);
            recordTypeOfException(startMillis, "getExpiredVouchersForUser", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while getting expired vouchers for user:" , ee);
            recordTypeOfException(startMillis, "getExpiredVouchersForUser", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while getting expired vouchers for user:" , te);
            recordTypeOfException(startMillis, "getExpiredVouchersForUser", te);
        }
        return null;
    }

    public GetVoucherDetailsResponse getVoucherDetails(String voucherId) {
        GetVoucherDetailsRequest voucherDetailsRequest = new GetVoucherDetailsRequest();
        voucherDetailsRequest.setVoucherId(voucherId);
        long startMillis = System.currentTimeMillis();
        try {
            GetVoucherDetailsResponse getVoucherDetailsResp = sdmoneyClientWrapperService.getVoucherDetails(voucherDetailsRequest);
            metricSuccess(startMillis, "getVoucherDetails");
            return getVoucherDetailsResp;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting voucherDetails:",ie);
            recordTypeOfException(startMillis, "getVoucherDetails", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while getting voucherDetails:",ee);
            recordTypeOfException(startMillis, "getVoucherDetails", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while  getting voucherDetails:",te);
            recordTypeOfException(startMillis, "getVoucherDetails", te);
        }
        return null;

    }

    public GetVoucherTransactionsResponse getTransactionsForVoucher(String voucherId) {
        GetVoucherTransactionsRequest voucherTransactionsRequest = new GetVoucherTransactionsRequest();
        voucherTransactionsRequest.setVoucherId(voucherId);
        long startMillis = System.currentTimeMillis();
        try {
            GetVoucherTransactionsResponse getVoucherResp = adminSDmoneyClientWrapperService.getTransactionsForVoucher(voucherTransactionsRequest);
            metricSuccess(startMillis, "getTransactionsForVoucher");
            return getVoucherResp;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting transactionsForVoucher:",ie);
            recordTypeOfException(startMillis, "getTransactionsForVoucher", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while  getting transactionsForVoucher:",ee);
            recordTypeOfException(startMillis, "getTransactionsForVoucher", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while  getting transactionsForVoucher:",te);
            recordTypeOfException(startMillis, "getTransactionsForVoucher", te);
        }
        return null;

    }

    public GetVoucherBalanceDetailsResponse getVoucherBalanceDetails(String oneCheckUserId) {
        GetVoucherBalanceDetailsRequest voucherBalanceDetailsRequest = new GetVoucherBalanceDetailsRequest();
        long startMillis = System.currentTimeMillis();
        voucherBalanceDetailsRequest.setSdIdentity(oneCheckUserId);
        try {
            GetVoucherBalanceDetailsResponse getVoucherBalanceResp = adminSDmoneyClientWrapperService.getVoucherBalanceDetails(voucherBalanceDetailsRequest);
            metricSuccess(startMillis, "getVoucherBalanceDetails");
            return getVoucherBalanceResp;
        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while getting voucherBalanceDetails:",ie);
            recordTypeOfException(startMillis, "getVoucherBalanceDetails", ie);
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while getting voucherBalanceDetails:",ee);
            recordTypeOfException(startMillis, "getVoucherBalanceDetails", ee);
        } catch (TimeoutException te)  {
            logger.error("Timeout Exception while getting voucherBalanceDetails:",te);
            recordTypeOfException(startMillis, "getVoucherBalanceDetails", te);
        }
        return null;

    }

    public GetAccountBalanceDetailsResponse getBalanceDetailsByToken(String imsToken, String userAgent, String machineIdentifier,boolean isWithdrawable, boolean isGVDetailsNeeded) throws InvalidIMSTokenException {
        GetUserResponse userByToken = getUserDetailsByToken(imsToken, userAgent, machineIdentifier);
        long startMillis = System.currentTimeMillis();
        GetAccountBalanceDetailsRequest gbdr = new GetAccountBalanceDetailsRequest();
        List<BalanceType> balanceTypes = new ArrayList<BalanceType>(Arrays.asList(BalanceType.CREDITABLE_BALANCE, BalanceType.GENERAL_BALANCE, BalanceType.VOUCHER_BALANCE, BalanceType.TOTAL_BLOCKEDIN_BALANCE));
        if(isWithdrawable) {
        	balanceTypes.add(BalanceType.WITHDRAWABLE_BALANCE);
        	boolean showWithdrawMessage = true;
        	gbdr.setShowWithdrawMessage(showWithdrawMessage);
        }
        if(isGVDetailsNeeded){
           balanceTypes.add(BalanceType.CREDITABLE_VOUCHER_BALANCE);
           balanceTypes.add(BalanceType.CREDITABLE_VOUCHER_AVAILABLE_COUNT);
           balanceTypes.add(BalanceType.DEBITABLE_BALANCE);
           gbdr.setShowAddMoneyMessage(true);
        }
        
        gbdr.setBalanceTypes(balanceTypes);
        gbdr.setSdIdentity(userByToken.getUserDetails().getUserId());
            try {
            	 GetAccountBalanceDetailsResponse balanceDetails = sdmoneyClientWrapperService.getAccountBalanceDetails(gbdr);
                 metricSuccess(startMillis, "getBalanceDetailsByToken");
                 logger.info("balanceDetails:" + balanceDetails.getBalanceDetails());
                 return balanceDetails;
            } catch (InterruptedException ie) {
    	        logger.error("Interrupted Exception while getting AccountBalanceDetails balance to user:",ie);
    	        recordTypeOfException(startMillis, "getBalanceDetailsByToken", ie);
            } catch (ExecutionException ee) {
    	        logger.error("Execution Exception while getting AccountBalanceDetails to user:",ee);
    	        recordTypeOfException(startMillis, "getBalanceDetailsByToken", ee);
            } catch (TimeoutException te) {
    	        logger.error("Timeout Exception while getting AccountBalanceDetails to user:", te);
    	        recordTypeOfException(startMillis, "getBalanceDetailsByToken", te);
            } 
            return null;
    }

    public GetWithdrawalFeeDetailsResponse getWithdrawFeeDetails(String imsToken, String userAgent, String machineIdentifier,BigDecimal amount) throws InvalidIMSTokenException {
        GetUserResponse userByToken = getUserDetailsByToken(imsToken, userAgent, machineIdentifier);
        long startMillis = System.currentTimeMillis();
        GetWithdrawalFeeDetailsRequest gbdr = new GetWithdrawalFeeDetailsRequest();
        gbdr.setSdIdentity(userByToken.getUserDetails().getUserId());
        gbdr.setAmount(amount);
        try {
        	 GetWithdrawalFeeDetailsResponse withdrawalFeeDetailsResponse = sdmoneyClientWrapperService.getWithdrawalFeeDetails(gbdr);
             metricSuccess(startMillis, "getWithdrawFeeDetails");
             logger.info("withdrawalFeeDetailsResponse:" + withdrawalFeeDetailsResponse);
             return withdrawalFeeDetailsResponse;
        } catch (InterruptedException ie) {
	        logger.error("Interrupted Exception while getting getWithdrawFeeDetails balance to user:",ie);
	        recordTypeOfException(startMillis, "getBalanceDetailsByToken", ie);
        } catch (ExecutionException ee) {
	        Throwable te = ee.getCause ();
	        if (te instanceof InsufficientFundsException)
	        {
	        	InsufficientFundsException ife = (InsufficientFundsException) te;
	        	logger.error("InsufficientFundsException while getting getWithdrawFeeDetails to user:", ife);
	        	recordTypeOfException(startMillis, "getWithdrawFeeDetails", ife);
	        	throw ife;
	        }
	        logger.error("Execution Exception while getting getWithdrawFeeDetails to user:",ee);
	        recordTypeOfException(startMillis, "getWithdrawFeeDetails", ee);
        } catch (TimeoutException te) {
	        logger.error("Timeout Exception while getting getWithdrawFeeDetails to user:", te);
	        recordTypeOfException(startMillis, "getWithdrawFeeDetails", te);
        }catch (InsufficientFundsException ife) {
        	logger.error("InsufficientFundsException while getting getWithdrawFeeDetails to user:", ife);
        	recordTypeOfException(startMillis, "getWithdrawFeeDetails", ife);
        	throw ife;
        }
        return null;
    }

    
    /**
     * Calls creditGeneralBalance() to credit money to user's one check wallet
     *
     * @param userId : userId to credit
     * @param amount : amount to credit
     * @param mtxnId : merchant transaction id for this credit transaction
     * @param email  : email id of user
     * @param fcWalletId : FC Wallet ID for the user
     * @return responseMap: Contains Status, Money transferred along with other parameters of the transaction
     */

    public Map<String,Object> creditToOneCheckWallet(long userId,double amount, String mtxnId, String email, String fcWalletId) {
        logger.info("Initiating refund to wallet as bank transfer has failed for userId: "+userId);
        Map<String,Object> responseMap = new HashMap<>();
        Map<String, Object> params = new HashMap<>();
        WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
        walletCreditRequest.setTxnAmount(new Amount(amount));
        walletCreditRequest.setCallerReference(mtxnId + "_" + PaymentConstants.OCW_MIGRATION);
        walletCreditRequest.setUserID(userId);
        walletCreditRequest.setMtxnId(mtxnId);
        params.put(WalletWrapper.WALLET_CREDIT_REQUEST, walletCreditRequest);
        params.put(ONE_CHECK_TXN_TYPE, OneCheckTxnType.CREDIT_ONE_CHECK_MIGRATION);
        params.put(FCConstants.EMAIL, email);
        params.put(WalletWrapper.ONE_CHECK_IDENTITY, fcWalletId);
        params.put(FCConstants.ORDER_ID, mtxnId);
        fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                PaymentConstants.OCW_MIGRATION);
        try {
            CreditGeneralBalanceToUserResponse creditBalanceResponse = creditGeneralBalanceToUser(params);
            logger.info("Credited One Check wallet for wallet migration for fcUserId: " + userId);
            responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            responseMap.put(MONEY_TRANSFERRED, amount);
            responseMap.put(FCConstants.REFERENCE_ID, creditBalanceResponse.getTransactionId());
            responseMap.put(FCConstants.IS_PARKED, creditBalanceResponse.isParked());
            fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                    PaymentConstants.OCW_MIGRATION_SUCCESS);
        } catch (RuntimeException e) {
            logger.error("Exception while crediting one check wallet for wallet migration for fcUserId: " + userId);
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
            fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                    PaymentConstants.OCW_MIGRATION_FAILED);
            throw new OneCheckWalletException(e);
        }
        return responseMap;
    }

    public List<Transaction> findTransactionById(String transactionReference) {
        GetTransactionByIdRequest getTransactionByIdRequest = new GetTransactionByIdRequest();
        getTransactionByIdRequest.setTransactionId(transactionReference);
        GetTransactionByIdResponse getTransactionByIdResponse = null;
        try {
            getTransactionByIdResponse = sdmoneyClientWrapperService.findTransactionsById(getTransactionByIdRequest);
            return getTransactionByIdResponse.getTransactions();
        }
        catch(InterruptedException ex) {
            logger.error("Interrupted exception in fetching transaction from OCW." +ex);
        }
        catch (ExecutionException ex) {
            logger.error("Execution exception in fetching transaction from OCW." +ex);
        }
        catch (TimeoutException ex) {
            logger.error("Timeout exception in fetching transaction from OCW." +ex);
        }
        return null;
    }

    public TransactionSummary findTransactionForStatusCheck(Map<String, Object> params, String mtxnId) {
        String orderId = (String) params.get(FCConstants.ORDER_ID);
        String sdIdentity = (String) params.get(WalletWrapper.ONE_CHECK_IDENTITY);
        OneCheckTxnType oneCheckTxnType = (OneCheckTxnType) params.get(ONE_CHECK_TXN_TYPE);
        String idempotencyId=getIdempotencyId(mtxnId, oneCheckTxnType, null);
        List<TransactionSummary> transactionsByReference = findTransactionsByReference(orderId, sdIdentity);
        if (transactionsByReference != null && !transactionsByReference.isEmpty()) {
            for (TransactionSummary transaction : transactionsByReference) {
                if (idempotencyId.equals(transaction.getIdempotencyId())) {
                    return transaction;
                }
            }
        }
        return null;
    }

    private void recordTypeOfException(long startMillis, String methodName, Exception exception) {
        Throwable throwableException = ExceptionUtils.getRootCause(exception);
        if (throwableException != null) {
            metricFailure(startMillis, methodName, throwableException.getClass().getSimpleName());
        } else {
            metricFailure(startMillis, methodName, exception.getClass().getSimpleName());
        }
    }


    private void metricFailure(final long startMillis, final String methodName, final String eventName) {
        this.recordMetric(startMillis, methodName, eventName);
    }

    private void metricSuccess(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String methodName, final String eventName) {
        String serviceName = "OnecheckWallet";
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;

        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
    }

    /**
     * Calls SDMoneyServiceClient to get all the wallet Integration transactions
     * for a particular order id for a user
     *
     * @param orderId
     * @return
     */
    public List<TransactionSummary> findTransactionsById(String orderId) {
        try {
            GetTransactionByIdRequest transactionByIdRequest = new GetTransactionByIdRequest();
            transactionByIdRequest.setTransactionId(orderId);
            logger.info("Request: SD money Client - find transaction by id -> " + transactionByIdRequest);
            GetTransactionByIdResponse transactionById = sdmoneyClientWrapperService
                    .findTransactionsById(transactionByIdRequest);
            logger.info("Response: SD money Client - find transaction by id -> " + transactionById);
            
            // transform transactions into transaction summary
            List<TransactionSummary> transactions = new ArrayList<TransactionSummary>();

            if (transactionById != null) {
                List<Transaction> transactionList = transactionById.getTransactions();
                TransactionSummary transactionSummary = null;

                // check if transactionList is null?
                if (transactionList.size() == 0) {
                    return transactions;
                }

                for (Transaction transaction : transactionList) {
                    transactionSummary = new TransactionSummary();
                    transactionSummary.setTransactionId(transaction.getTransactionId());
                    transactionSummary.setTransactionReference(transaction.getTransactionReference());
                    transactionSummary.setIdempotencyId(transaction.getIdempotencyId());
                    transactionSummary.setTransactionAmount(transaction.getTransactionAmount());
                    transactionSummary.setRunningBalance(transaction.getRunningBalance().getTotalBalance());
                    transactionSummary.setTimestamp(transaction.getTimestamp());
                    transactionSummary.setTransactionType(transaction.getType());
                    transactionSummary.setBusinessTransactionType(transaction.getBusinessTransactionType());
                    transactionSummary.setBussinessEntity(transaction.getBussinessEntity());
                    transactionSummary.setEventContext(transaction.getEventContext());
                    transactionSummary.setCreditType(transaction.getCreditType());
                    transactionSummary.setOrderDetails(transaction.getOrderDetails());

                    transactions.add(transactionSummary);

                }

            }
            return transactions;

        } catch (InterruptedException ie) {
            logger.error("Interrupted Exception while finding transaction by reference:" + ie);
            //ie.printStackTrace();
        } catch (ExecutionException ee) {
            logger.error("Execution Exception while finding transaction by reference:" + ee);
            //ee.printStackTrace();
        } catch (TimeoutException te) {
            logger.error("Timeout Exception while finding transaction by reference:" + te);
            //te.printStackTrace();
        }
        return null;
    }
    
	public GetUserAccountDetailsResponse getUserAccountDetails(String sdIdentity) {
		long startMillis = System.currentTimeMillis();
		try {
			GetUserAccountDetailsRequest getUserAccountDetailsRequest = new GetUserAccountDetailsRequest();
			List<BalanceType> balanceTypeList = new ArrayList<BalanceType>();
			List<AccountDetailsType> accountDetailsTypeList = new ArrayList<AccountDetailsType>();
			balanceTypeList.add(BalanceType.TOTAL_BALANCE);
			balanceTypeList.add(BalanceType.VOUCHER_BALANCE);
			balanceTypeList.add(BalanceType.GENERAL_BALANCE);
			balanceTypeList.add(BalanceType.CREDITABLE_BALANCE);
			
            balanceTypeList.add(BalanceType.DEBITABLE_BALANCE);
            balanceTypeList.add(BalanceType.CREDITABLE_VOUCHER_BALANCE);
            balanceTypeList.add(BalanceType.CREDITABLE_VOUCHER_AVAILABLE_COUNT);
			
			accountDetailsTypeList.add(AccountDetailsType.ACCOUNT_STATUS);
			accountDetailsTypeList.add(AccountDetailsType.KYC_STATUS);
			getUserAccountDetailsRequest.setSdIdentity(sdIdentity);
			getUserAccountDetailsRequest.setAccountDetailsType(accountDetailsTypeList);
			getUserAccountDetailsRequest.setBalanceTypes(balanceTypeList);
			GetUserAccountDetailsResponse getUserAccountDetailsResponse = sdmoneyClientWrapperService
					.getUserAccountDetails(getUserAccountDetailsRequest);
			return getUserAccountDetailsResponse;
		} catch (InterruptedException ie) {
			logger.error("InterruptedException in getUserAccountDetails:", ie);
			recordTypeOfException(startMillis, "getUserAccountDetails", ie);
		} catch (ExecutionException ee) {
			logger.error("ExecutionException in getUserAccountDetails:", ee);
			recordTypeOfException(startMillis, "getUserAccountDetails", ee);
		} catch (TimeoutException te) {
			logger.error("TimeoutException in getUserAccountDetails:", te);
			recordTypeOfException(startMillis, "getUserAccountDetails", te);
		}

		return null;
	}

	public Amount getWalletDebitableAmount(String sdIdentity) {
        logger.info("Getting debitable balance for onecheck user id : " + sdIdentity);
        try {
            GetUserAccountDetailsResponse getUserAccountDetailsResponse = getUserAccountDetails(sdIdentity);
            return  new Amount(getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.DEBITABLE_BALANCE));
        } catch(Exception e) {
            logger.error("Exception while getting user account details response",e);
        }
        return new Amount(BigDecimal.ZERO);
    }

    public static enum TSMClientGlobalTxnType {
        CANCELLATION_REFUND,CUSTOMER_PAYMENT, FC_WALLET_CASHBACK, OPS_WALLET_CREDIT, OPS_WALLET_CREDIT_REVERSE,
        OPS_WALLET_CREDIT_VOID, OPS_WALLET_DEBIT, OPS_WALLET_DEBIT_REVERSE, OPS_WALLET_DEBIT_VOID,
        OPS_WALLET_DEBIT_VOID_REVERSE, P2P_PAY_TO_MID, P2P_REQUEST_MONEY, P2P_SEND_MONEY, SPLIT_MONEY, PAYABLES_CHARGE,
        PAYABLES_DISBURSEMENT, PAYABLES_LOAD, PAYABLES_REFUND, WALLET_CORP_DISBURSEMENT, ESCROW_PAYMENT, ESCROW_CANCEL, ESCROW_FINALIZE
    }

}
