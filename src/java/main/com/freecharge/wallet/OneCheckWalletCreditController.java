package com.freecharge.wallet;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.freecharge.intxndata.common.InTxnDataConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceResponse;

@Controller
public class OneCheckWalletCreditController {

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private FCProperties fcproperties;

    @Autowired
    private MetricsClient metricsClient;
    
    private Logger logger = LoggingFactory.getLogger(getClass());
    

    @NoLogin
    @Csrf(exclude=true)
    @RequestMapping(method = RequestMethod.POST, value = "/protected/voucher/balance/add", produces = "application/json")
    public @ResponseBody CreditVoucherBalanceResponse addVoucherBalance(
        @RequestParam final Integer userId,
        @RequestParam(required = false) final String eventContext,
        @RequestParam final BigDecimal amount, @RequestParam final long expiryDuration,
        @RequestParam(required = false) final String corporateAccountId,
        @RequestParam(required = false) final String customerId,
        @RequestParam(required = false) final String customerName,
        @RequestParam(required = false) final OneCheckTxnType oneCheckTxnType, @RequestParam final String orderId,
        @RequestParam(required = false) final String code, @RequestParam final String mtxnId,
        @RequestParam(required = false) final String oneCheckWalletId,
        @RequestParam(required = false) final String businessEntityRequired) {

        String corpId;
        if (null != corporateAccountId && !corporateAccountId.isEmpty()) {
            corpId = corporateAccountId;
        } else {
            corpId = fcproperties.getProperty(FCConstants.PROMOTION_CORP_ID);
        }

        logger.info("Processing add voucher balance request for userId : " + userId +
                ", eventContext : " + eventContext + ", amount : " + amount +
                ", expiryDuration : " + expiryDuration + ", oneCheckTxnType : " + oneCheckTxnType +
                ", orderId : " + orderId + ", code : " + code + ", mtxnId : " + mtxnId + ", corpId : " + corpId);

        Map<String, Object> params = new HashMap<>();
        if (!StringUtils.isBlank(oneCheckWalletId)){
            params.put(FCConstants.SD_IDENTITY, oneCheckWalletId);
        }else {
            params.put(FCConstants.SD_IDENTITY, oneCheckWalletService.
                    getOneCheckIdIfOneCheckWalletEnabled(userServiceProxy.getEmailByUserId(userId)));
        }
        params.put(FCConstants.EVENT_CONTEXT, eventContext);
        params.put(FCConstants.AMOUNT_WALLET, amount);
        params.put(InTxnDataConstants.CUSTOMER_ID, customerId);
        params.put(InTxnDataConstants.CUSTOMER_NAME, customerName);
        if (null != businessEntityRequired && !businessEntityRequired.isEmpty()) {
            params.put(InTxnDataConstants.BUSINESS_ENTITY_REQUIRED, businessEntityRequired);
        }
        params.put(FCConstants.EXPIRY_DURATION, expiryDuration);
        params.put(FCConstants.ONECHECK_TXN_TYPE, (null == oneCheckTxnType) ? OneCheckTxnType.CREDIT_CAMPAIGN_REWARD : oneCheckTxnType);
        params.put(FCConstants.ORDER_ID, orderId);
        params.put(FCConstants.CODE, code);
        params.put(FCConstants.MTXN_ID, mtxnId);
        params.put(FCConstants.CORP_ID, corpId);

        CreditVoucherBalanceResponse creditVoucherBalanceResponse = null;
        long startMillis = System.currentTimeMillis();
        try {
            creditVoucherBalanceResponse = oneCheckWalletService.creditVoucherBalance(params);
            if (FCUtil.isEmpty(creditVoucherBalanceResponse.getVoucherId())) {
            	metricSuccess(startMillis, "addVoucherBalance");
                return creditVoucherBalanceResponse;
            }
        } catch (Exception e) {
            logger.error("Error crediting gift voucher for " + userId + " with amount " + amount, e);
             metricFailure(startMillis, "addVoucherBalance");
        }

        return creditVoucherBalanceResponse;
    }
    
    private void metricFailure(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Exception");
    }

    private void metricSuccess(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String methodName, final String eventName) {
        String serviceName = "OnecheckWallet";
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;
        
        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
    }
}