package com.freecharge.wallet;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.freecharge.common.framework.properties.FCProperties;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherTransactionsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherTransactionsResponse;

public class AdminSDMoneyClientWrapperService extends SDMoneyClientWrapperService {

    
	public void init() {
		this.connectionTimeOut = fcProperties.getLongProperty(FCProperties.ADMIN_WALLET_API_TIMEOUT);
	}
	
	public GetVoucherBalanceDetailsResponse getVoucherBalanceDetails(final GetVoucherBalanceDetailsRequest voucherBalanceDetailsRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		return super.getVoucherBalanceDetails(voucherBalanceDetailsRequest);
	}
	
	public CreditVoucherBalanceResponse creditVoucherBalance(final CreditVoucherBalanceRequest creditVoucherBalanceRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		return super.creditVoucherBalance(creditVoucherBalanceRequest);   
	}
	
	public GetVoucherTransactionsResponse getTransactionsForVoucher(final GetVoucherTransactionsRequest voucherTransactionsRequest)
			throws InterruptedException, ExecutionException, TimeoutException {
		 return super.getTransactionsForVoucher(voucherTransactionsRequest);   
	}
}
