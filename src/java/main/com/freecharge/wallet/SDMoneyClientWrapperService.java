package com.freecharge.wallet;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.properties.FCProperties;
import com.snapdeal.payments.authorize.core.exception.AuthorizationException;
import com.snapdeal.payments.authorize.core.exception.ValidationException;
import com.snapdeal.payments.sdmoney.client.SDMoneyClient;
import com.snapdeal.payments.sdmoney.exceptions.InsufficientFundsException;
import com.snapdeal.payments.sdmoney.exceptions.InternalServerException;
import com.snapdeal.payments.sdmoney.service.model.CreditGeneralBalanceToUserRequest;
import com.snapdeal.payments.sdmoney.service.model.CreditGeneralBalanceToUserResponse;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.CreditVoucherBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.GetAccountBalanceDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetAccountBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetAccountBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.GetAccountBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.GetExpiredVoucherDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetExpiredVoucherDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdRequest;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdempotencyIdRequest;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdempotencyIdResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionsByReferenceRequest;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionsByReferenceResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetUserAccountDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetUserAccountDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherTransactionsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherTransactionsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVouchersForTransactionRequest;
import com.snapdeal.payments.sdmoney.service.model.GetVouchersForTransactionResponse;
import com.snapdeal.payments.sdmoney.service.model.GetWithdrawalFeeDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetWithdrawalFeeDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.RefundBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.RefundBalancesResponse;
import com.snapdeal.payments.sdmoney.service.model.ReverseLoadMoneyRequest;
import com.snapdeal.payments.sdmoney.service.model.ReverseLoadMoneyResponse;
import com.tracelytics.api.ext.TraceContext;

@Service("sdmoneyClientWrapperService")
public class SDMoneyClientWrapperService {
	
	@Autowired
	private SDMoneyClient sdMoneyClient;
	
	@Autowired
	protected FCProperties fcProperties;
	
	private ExecutorService oneCheckExecutor = Executors.newCachedThreadPool();
	
	protected long connectionTimeOut;
	
	protected long getBalanceApiTimout;
	
	
	@PostConstruct
	public void setTimeOut(){
		this.connectionTimeOut = fcProperties.getLongProperty(FCProperties.WALLET_API_TIMEOUT);
		this.getBalanceApiTimout = fcProperties.getLongProperty(FCProperties.GET_BALANCE_API_TIMEOUT);
	}
	
	public CreditGeneralBalanceToUserResponse creditGeneralBalanceToUser(
			final CreditGeneralBalanceToUserRequest creditGeneralBalanceToUserRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		    CreditGeneralBalanceToUserResponse creditGeneralBalanceToUserResponse = null; 
		    Future<CreditGeneralBalanceToUserResponse> reponse = oneCheckExecutor.submit(new Callable<CreditGeneralBalanceToUserResponse>(){
			 public CreditGeneralBalanceToUserResponse call() {
				return sdMoneyClient.creditGeneralBalanceToUser(creditGeneralBalanceToUserRequest);	
			}
		});
	    try {
	    	 creditGeneralBalanceToUserResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
	    } finally {
	    	reponse.cancel(true);
	    }
	    return creditGeneralBalanceToUserResponse;	
	}
	
	public RefundBalancesResponse refundBalanceToUser(
			final RefundBalanceRequest refundBalanceRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		    RefundBalancesResponse refundBalancesResponse = null;
		    Future<RefundBalancesResponse> reponse = oneCheckExecutor.submit(new Callable<RefundBalancesResponse>(){
			 public RefundBalancesResponse call() {
				return sdMoneyClient.refundBalance(refundBalanceRequest);	
			}
		});
		try {    
		     refundBalancesResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		return refundBalancesResponse;
	}
	
	public ReverseLoadMoneyResponse reverseLoadMoney(
			final ReverseLoadMoneyRequest reverseLoadMoneyRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		    ReverseLoadMoneyResponse reverseLoadMoneyResponse = null;
		    Future<ReverseLoadMoneyResponse> reponse = oneCheckExecutor.submit(new Callable<ReverseLoadMoneyResponse>(){
			 public ReverseLoadMoneyResponse call() {
				return sdMoneyClient.reverseLoadMoney(reverseLoadMoneyRequest);	
			}
		});
		try {    
			reverseLoadMoneyResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		return reverseLoadMoneyResponse;
	}
	
	public CreditVoucherBalanceResponse creditVoucherBalance(
			final CreditVoucherBalanceRequest creditVoucherBalanceRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		CreditVoucherBalanceResponse creditVoucherBalance = null;
		    Future<CreditVoucherBalanceResponse> reponse = oneCheckExecutor.submit(new Callable<CreditVoucherBalanceResponse>(){
			 public CreditVoucherBalanceResponse call() {
				return sdMoneyClient.creditVoucherBalance(creditVoucherBalanceRequest);	
			}
		});
		try {
			creditVoucherBalance = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return creditVoucherBalance;   
	}
	
	public DebitBalanceResponse debitBalance(final DebitBalanceRequest debitBalanceRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		DebitBalanceResponse debitBalanceResponse = null;
		final TraceContext traceContext = TraceContext.getDefault();

		debitBalanceRequest.setNotificationNotDesired(true);
		Future<DebitBalanceResponse> reponse = oneCheckExecutor.submit(new Callable<DebitBalanceResponse>(){
			 public DebitBalanceResponse call() {
			     traceContext.setAsDefault();
				return sdMoneyClient.debitBalance(debitBalanceRequest);	
			}
		});
		try {
			debitBalanceResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return debitBalanceResponse;   
	}
	
	
	public GetVoucherDetailsResponse getVoucherDetails(final GetVoucherDetailsRequest voucherDetailsRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		GetVoucherDetailsResponse getVoucherDetailsResponse = null;
		Future<GetVoucherDetailsResponse> reponse = oneCheckExecutor.submit(new Callable<GetVoucherDetailsResponse>(){
			 public GetVoucherDetailsResponse call() {
				return sdMoneyClient.getVoucherDetails(voucherDetailsRequest);	
			}
		});
		try {
			getVoucherDetailsResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getVoucherDetailsResponse;   
	}
	
	
	public GetExpiredVoucherDetailsResponse getExpiredVouchersForUser(final GetExpiredVoucherDetailsRequest 
			expiredVoucherDetailsRequest) throws InterruptedException, ExecutionException, TimeoutException {
		GetExpiredVoucherDetailsResponse getExpiredVoucherDetailsResponse = null;
		Future<GetExpiredVoucherDetailsResponse> reponse = oneCheckExecutor.submit(new Callable<GetExpiredVoucherDetailsResponse>(){
			 public GetExpiredVoucherDetailsResponse call() {
				return sdMoneyClient.getExpiredVoucherDetails(expiredVoucherDetailsRequest);	
			}
		});
		try {
			getExpiredVoucherDetailsResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getExpiredVoucherDetailsResponse;   
	}
	
	
	public  GetVoucherTransactionsResponse getTransactionsForVoucher(final GetVoucherTransactionsRequest voucherTransactionsRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		GetVoucherTransactionsResponse getVoucherTransactionsResponse = null;
		Future<GetVoucherTransactionsResponse> reponse = oneCheckExecutor.submit(new Callable<GetVoucherTransactionsResponse>(){
			 public GetVoucherTransactionsResponse call() {
				return sdMoneyClient.getVoucherTransactions(voucherTransactionsRequest);
			}
		});
		try {
			getVoucherTransactionsResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getVoucherTransactionsResponse;   
	}
	
	public GetVoucherBalanceDetailsResponse getVoucherBalanceDetails(final GetVoucherBalanceDetailsRequest voucherBalanceDetailsRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		GetVoucherBalanceDetailsResponse getVoucherBalanceDetailsResponse = null;
		Future<GetVoucherBalanceDetailsResponse> reponse = oneCheckExecutor.submit(new Callable<GetVoucherBalanceDetailsResponse>(){
			 public GetVoucherBalanceDetailsResponse call() {
				return sdMoneyClient.getVoucherBalanceDetails(voucherBalanceDetailsRequest);	
			}
		});
		try {
			getVoucherBalanceDetailsResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getVoucherBalanceDetailsResponse;   
	}

	public GetTransactionsByReferenceResponse findTransactionsByReference (final GetTransactionsByReferenceRequest 
			transactionByReferenceRequest) throws InterruptedException, ExecutionException, TimeoutException {
		GetTransactionsByReferenceResponse getTransactionsByReferenceResponse = null;
		Future<GetTransactionsByReferenceResponse> reponse = oneCheckExecutor.submit(new Callable<GetTransactionsByReferenceResponse>(){
			 public GetTransactionsByReferenceResponse call() {
				return sdMoneyClient
		                .getTransactionByReference(transactionByReferenceRequest);	
			}
		});
		try {
			getTransactionsByReferenceResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getTransactionsByReferenceResponse;   
	}
	
    public GetTransactionByIdempotencyIdResponse findTransactionsByIdempotency(
            final GetTransactionByIdempotencyIdRequest transactionByIdempotencyIdRequest) throws InterruptedException,
            ExecutionException, TimeoutException {
        GetTransactionByIdempotencyIdResponse transactionByIdempotencyIdResponse = null;
        Future<GetTransactionByIdempotencyIdResponse> reponse = oneCheckExecutor
                .submit(new Callable<GetTransactionByIdempotencyIdResponse>() {
                    public GetTransactionByIdempotencyIdResponse call() {
                        return sdMoneyClient.getTransactionByIdempotencyId(transactionByIdempotencyIdRequest);
                    }
                });
        try {
            transactionByIdempotencyIdResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
        } finally {
            reponse.cancel(true);
        }
        return transactionByIdempotencyIdResponse;
    }
	
	//This method fetches the transactions by Id
	public GetTransactionByIdResponse findTransactionsById (final GetTransactionByIdRequest 
			transactionByIdRequest) throws InterruptedException, ExecutionException, TimeoutException {
		GetTransactionByIdResponse getTransactionByIdResponse = null;
		Future<GetTransactionByIdResponse> reponse = oneCheckExecutor.submit(new Callable<GetTransactionByIdResponse>(){
			 public GetTransactionByIdResponse call() {
				return sdMoneyClient
		                .getTransactionById(transactionByIdRequest);
			}
		});
		try {
			getTransactionByIdResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getTransactionByIdResponse;   
	}
	
	public GetAccountBalanceResponse getFCWalletAccountBalance(final GetAccountBalanceRequest getAccountBalanceRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		GetAccountBalanceResponse getAccountBalanceResponse = null;
		final TraceContext traceContext = TraceContext.getDefault();
		Future<GetAccountBalanceResponse> reponse = oneCheckExecutor.submit(new Callable<GetAccountBalanceResponse>(){
			 public GetAccountBalanceResponse call() {
			     traceContext.setAsDefault();
			     return sdMoneyClient.getAccountBalance(getAccountBalanceRequest);	
			}
		});
		try {
			getAccountBalanceResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getAccountBalanceResponse;   
	}
		
	public GetAccountBalanceDetailsResponse getAccountBalanceDetails(final GetAccountBalanceDetailsRequest 
			getAccountBalanceDetailsRequest) throws InterruptedException, ExecutionException, TimeoutException {
		GetAccountBalanceDetailsResponse getAccountBalanceDetailsResponse = null;
		Future<GetAccountBalanceDetailsResponse> reponse = oneCheckExecutor.submit(new Callable<GetAccountBalanceDetailsResponse>(){
			 public GetAccountBalanceDetailsResponse call() {
				return sdMoneyClient.getAccountBalanceDetails(getAccountBalanceDetailsRequest);
			}
		});
		try {
			getAccountBalanceDetailsResponse = reponse.get(getBalanceApiTimout, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getAccountBalanceDetailsResponse;   
	}
	
	public GetWithdrawalFeeDetailsResponse getWithdrawalFeeDetails(final GetWithdrawalFeeDetailsRequest 
			getWithdrawalFeeDetailsRequest) throws InterruptedException, ExecutionException, TimeoutException {
		GetWithdrawalFeeDetailsResponse getWithdrawalFeeDetailsResponse = null;
		Future<GetWithdrawalFeeDetailsResponse> reponse = oneCheckExecutor.submit(new Callable<GetWithdrawalFeeDetailsResponse>(){
			 public GetWithdrawalFeeDetailsResponse call() {
				return sdMoneyClient.getWithdrawalFeeDetails(getWithdrawalFeeDetailsRequest);
			}
		});
		try {
			getWithdrawalFeeDetailsResponse = reponse.get(getBalanceApiTimout, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getWithdrawalFeeDetailsResponse;   
	}
	
	public GetTransactionsResponse findTransactions(final GetTransactionsRequest getTransactionsRequest) 
			throws InterruptedException, ExecutionException, TimeoutException {
		GetTransactionsResponse getTransactionsResponse = null;
		Future<GetTransactionsResponse> reponse = oneCheckExecutor.submit(new Callable<GetTransactionsResponse>(){
			 public GetTransactionsResponse call() {
				return sdMoneyClient.getTransactions(getTransactionsRequest);
			}
		});
		try {
			getTransactionsResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			reponse.cancel(true);
		}
		 return getTransactionsResponse;   
	}
	
	 public GetVouchersForTransactionResponse getVoucherDetailsForTransaction(final GetVouchersForTransactionRequest getVouchersForTransactionRequest) 
			 throws InterruptedException, ExecutionException, TimeoutException {
		 GetVouchersForTransactionResponse getVouchersForTransactionResponse = null;
		 Future<GetVouchersForTransactionResponse> reponse = oneCheckExecutor.submit(new Callable<GetVouchersForTransactionResponse>(){
			 public GetVouchersForTransactionResponse call() {
				return sdMoneyClient.getVouchersForTransaction(getVouchersForTransactionRequest);
			}
		});
		 try {
			 getVouchersForTransactionResponse = reponse.get(connectionTimeOut, TimeUnit.MILLISECONDS);
			} finally {
				reponse.cancel(true);
			}
			 return getVouchersForTransactionResponse;   
		}

	public GetUserAccountDetailsResponse getUserAccountDetails(
			final GetUserAccountDetailsRequest getUserAccountDetailsRequest)
					throws InterruptedException,
					ExecutionException, TimeoutException {
		GetUserAccountDetailsResponse getUserAccountDetailsResponse = null;
		Future<GetUserAccountDetailsResponse> response = oneCheckExecutor
				.submit(new Callable<GetUserAccountDetailsResponse>() {
					public GetUserAccountDetailsResponse call() {
						return sdMoneyClient.getUserAccountDetails(getUserAccountDetailsRequest);
					}
				});
		try {
			getUserAccountDetailsResponse = response.get(connectionTimeOut, TimeUnit.MILLISECONDS);
		} finally {
			response.cancel(true);
		}
		return getUserAccountDetailsResponse;
	}
}
