package com.freecharge.wallet;

public class WalletCreditsResponse {

	private int responseCode;

	private String responseMessage;

	public WalletCreditsResponse() {
		super();
	}

	public WalletCreditsResponse(int responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

}
