package com.freecharge.wallet;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.addcash.service.AddCashService;
import com.freecharge.addcash.service.exception.AddCashProductToCartException;
import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.web.PaymentResponseWebDO;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletLimitRequest;
import com.freecharge.wallet.service.WalletLimitResponse;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.AddCashWebDO;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceResponse;

@Controller
@RequestMapping("/app/*")
public class WalletController {

    @Autowired
    private WalletService walletService;

    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private CartDAO cartDao;

    @Autowired
    private AddCashService addCashService;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private PaymentSuccessAction paymentSuccessAction;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;

    @Autowired
    private PaymentPlanService paymentPlanService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    OrderIdWriteDAO orderIdWriteDAO;

    @Autowired
    private PaymentSNSService paymentSNSService;
    
    @Autowired
    WalletWrapper walletWrapper;

    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    PaymentService paymentService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    FCProperties fcProperties;
    
    @Autowired
    private AppConfigService appConfig;
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    private Logger walletLogger = LoggingFactory.getLogger(LoggingFactory.getWalletLoggerName(WalletController.class.getName()));

    public static final String WALLET_PAY_ACTION = "/app/walletpay.htm";
    public static final String CASH_TO_FCBALANCE_PARAMETER = "cashtofcbalance";
    public static final String LOOKUP_ID_PARAMETER = "lookupID";

    private static interface JSPVariables {
        public final String WALLET_LIMIT_STATUS = "limitStatus";
        public final String WALLET_LIMIT_MESSAGE = "limitFailReason";
    }

    @RequestMapping(value = "walletpay")
    public String processPaymentRequest(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, String> requestParams, Model model) throws Exception {
        
		String returnTileDef = PaymentConstants.WAITPAGE_RESPONSE_VIEW;
		Integer userIdObject = userServiceProxy.getLoggedInUser().getUserId();
		
		logger.info("Starting processing of wallet pay - for user id"+ userIdObject);
		
		if (requestParams.isEmpty()) {
			logger.error("Request params is empty so mostly a double click or url reload issue for user id = "+ userIdObject);
			model.addAttribute("status", "error");
			model.addAttribute("error", "URL bad reload or multiple button clicks");
			return FCUtil.getRedirectAction("/");
		}

        Map<String, String> responseParams = new HashMap<String, String>();

        String merchantOrderId = "";

        try {
            if (requestParams.containsKey(PaymentConstants.HASH)) {
                merchantOrderId = requestParams.get(PaymentConstants.MERCHANT_ORDER_ID);
                FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
                String orderId = (String) fs.getSessionData().get(merchantOrderId + "_order_id");
                int userId = Integer.parseInt(requestParams.get(PaymentConstants.USER_ID));
                String hash = requestParams.get(PaymentConstants.HASH);
                if (WalletUtil.doesHashMatch(merchantOrderId, userId, hash)) {
                    if (userId != userIdObject) {
                        responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                        responseParams.put(PaymentConstants.MESSAGE, "user id passed is not the same as the logged in user");
                    } else {
                        String callerReference = orderId + RechargeConstants.RECHARGE_TYPE;

                        WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
                        walletDebitRequest.setUserID(userIdObject);
                        walletDebitRequest.setMtxnId(merchantOrderId);
                        walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
                        walletDebitRequest.setCallerReference(callerReference);
                        walletDebitRequest.setFundSource("");
                        walletDebitRequest.setFundDestination(Wallet.FundDestination.RECHARGE.toString());
                        walletDebitRequest.setMetadata("");

                        String transactionId = walletService.pay(walletDebitRequest);
                        
                        logger.info("wallet_txn ID " + transactionId + "merchantOrderId" + merchantOrderId);
                        
                        responseParams.put(PaymentConstants.TRANSACTION_ID, transactionId);
                        responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
                        responseParams.put(PaymentConstants.MESSAGE, "Pg Transaction Successful");
                        responseParams.put(PaymentConstants.USER_ID, userIdObject.toString());
                        PaymentTransaction paymentTransaction = getTransactionAmount(merchantOrderId);
                        
                        double transactionAmount = paymentTransaction.getAmount();
                        responseParams.put(PaymentConstants.AAMOUNT, String.valueOf(transactionAmount));

                        if (transactionId != null && transactionId.equals("1")) {
                            logger.info("Wallet pay success going ahead with recharge for order ID: [" + paymentTransaction.getOrderId() + "]");
                            paymentTransactionService.createSuccessfulWalletPaymentResponse(paymentTransaction);
                            onPaymentSuccess(paymentTransaction);
                        } else {
                            logger.info("Wallet pay failure not going ahead with recharge for order ID: [" + paymentTransaction.getOrderId() + "]");
                        }
                    }
                } else {
                    responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                    responseParams.put(PaymentConstants.MESSAGE, "hash mismatch");
                }
            } else {
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, "hash key not found");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            walletLogger.error(e.getMessage(), e);
            return "home/error";
        }

        responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);
        String hash = WalletUtil.createHash(merchantOrderId, userIdObject);

        responseParams.put(PaymentConstants.HASH, hash);
        responseParams.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());

        String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
        PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
        paymentResponseWebDO.setRequestMap(responseParams);
        paymentResponseWebDO.setPg(IGatewayHandler.PaymentGateways.FREECHARGE_WALLET.getName());
        paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_WALLET));

        try {
            DelegateHelper.processRequest(request, response, delegateName, model, paymentResponseWebDO);
            if ("true".equalsIgnoreCase(paymentResponseWebDO.getResultMap().get(PaymentConstants.IS_METHOD_SUCCESSFUL).toString())) {
                model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE, paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, paymentResponseWebDO.getResultMap().get(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL));
                returnTileDef = (String) paymentResponseWebDO.getResultMap().get(PaymentConstants.KEY_WAITPAGE);
                Map<String, Object> dataMap = (Map<String, Object>) paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
                String orderNo = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
                model.addAttribute("orderNo", orderNo);
            } else {
                returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
            }
        } catch (Exception fcExp) {
            throw new Exception(fcExp);
        }
        return returnTileDef;
    }

    @NoSessionWrite
    @RequestMapping(value = "mycredits", method = RequestMethod.GET)
    public ModelAndView showMyWallet(HttpServletRequest request, HttpServletResponse response, Model model) {
        return new ModelAndView(FCUtil.getRedirectAction("/#/credits"));
    }
    
    @NoSessionWrite
    @RequestMapping(value = "ajaxmybalance", method = RequestMethod.GET)
    @NoLogin
    public String ajaxMyBalance(HttpServletRequest request, HttpServletResponse response, Model model) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Object userIdObject = map.get(WebConstants.SESSION_USER_USERID);

        int userID = userIdObject == null ? -1 : (Integer) userIdObject;
        Amount balance = walletWrapper.getTotalBalance(userID);
        
        model.addAttribute("balance", balance.getAmount());

        return "jsonView";
    }

    @RequestMapping(value = "addcashproducttocart", method = RequestMethod.POST)
    public String addCash(@ModelAttribute(value = "addCashWebDo") AddCashWebDO addCashWebDO, HttpServletRequest request, HttpServletRequest response) {
        String lookupId = null;
        try {
            lookupId = addCashService.addProductToCartForAddCash(addCashWebDO.getAddCashAmount());
        } catch (AddCashProductToCartException e) {
            return "redirect:/app/mycredits.htm";
        }
        return "redirect:/app/checkout.htm?lid=" + lookupId;
    }

    @NoSessionWrite
    @RequestMapping(value = "app/walletfulfillment", method = RequestMethod.GET)
    public String handleFulFillment(HttpServletRequest request, HttpServletResponse response, Model model) {
        return FCUtil.getRedirectAction("/#/credits");
    }

    @NoSessionWrite
    @RequestMapping(value = "getcashlimit/{amountToValidate}", method = RequestMethod.GET)
    public String checkAddCashLimit(@PathVariable String amountToValidate, Model model) {
        boolean isValid = validateForAmount(amountToValidate, model);

        if (!isValid) {
            return "jsonView";
        }

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Integer userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);

        Amount amountToEvaluate = new Amount(amountToValidate);

        WalletLimitRequest limitRequest = new WalletLimitRequest();
        limitRequest.setAmountToTest(amountToEvaluate);
        limitRequest.setFundSource(Wallet.FundSource.CASH_FUND);
        limitRequest.setTransactionType(Wallet.TransactionType.DEPOSIT);
        limitRequest.setUserId(userId.longValue());

        WalletLimitResponse limitResponse = walletService.evaluateWalletLimit(limitRequest);

        if (Wallet.LimitStatus.Failure == limitResponse.getLimitStatus()) {
            model.addAttribute(JSPVariables.WALLET_LIMIT_STATUS, "fail");
            model.addAttribute(JSPVariables.WALLET_LIMIT_MESSAGE, limitResponse.getDescriptionString());
        } else {
            model.addAttribute(JSPVariables.WALLET_LIMIT_STATUS, "success");
        }

        return "jsonView";
    }

    private boolean validateForAmount(String amountString, Model model) {
        boolean isValid = false;

        if (StringUtils.isEmpty(amountString)) {
            model.addAttribute(JSPVariables.WALLET_LIMIT_STATUS, "fail");
            model.addAttribute(JSPVariables.WALLET_LIMIT_MESSAGE, "Empty Amount value");
        } else if (!StringUtils.isNumeric(amountString)) {
            model.addAttribute(JSPVariables.WALLET_LIMIT_STATUS, "fail");
            model.addAttribute(JSPVariables.WALLET_LIMIT_MESSAGE, "%$#&! Numbers only, please.");
        } else {
            isValid = true;
        }

        return isValid;
    }
    /**
     * Gets transactions by merchant id
     * @param merchantOrderId
     * @return
     */
    private PaymentTransaction getTransactionAmount(String merchantOrderId) {
        List<PaymentTransaction> paymentTransactions = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId);
        return paymentTransactions.get(0);
    }
    
    /**
     * Completes the direct wallet pay - without UI intervention [different than other payment options]
     * @param request
     * @param response
     * @param userId
     * @param orderId
     * @param productType 
     * @return
     * @throws Exception
     */
    public Map<String, String> processWalletPaymentRequest(HttpServletRequest request, HttpServletResponse response,
            Integer userId, String orderId, String mtxnId, String productType) {
        logger.debug("Starting processing of wallet pay for user: " + userId);
        Map<String, String> responseParams = new HashMap<String, String>();

        try {
            
            String callerReference = orderId + RechargeConstants.RECHARGE_TYPE;
            String fundDestination = Wallet.FundDestination.RECHARGE.toString();
            if (ProductName.fromPrimaryProductType(productType) == ProductName.HCoupons) {
                callerReference = orderId + RechargeConstants.HCOUPON_TYPE;
                fundDestination = Wallet.FundDestination.HCOUPON.toString();
            }

            WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
            walletDebitRequest.setUserID(((Integer) userId));
            walletDebitRequest.setMtxnId(mtxnId);
            walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
            walletDebitRequest.setCallerReference(callerReference);
            walletDebitRequest.setFundSource("");
            walletDebitRequest.setFundDestination(fundDestination);
            walletDebitRequest.setMetadata("");

            String transactionId = walletService.pay(walletDebitRequest);
            boolean isPaymentSuccess =false;

            logger.info("wallet_txn ID " + transactionId + " merchantOrderId " + mtxnId);

            responseParams.put(PaymentConstants.TRANSACTION_ID, transactionId);
            responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
            responseParams.put(PaymentConstants.MESSAGE, "Pg Transaction Successful");
            responseParams.put(PaymentConstants.HASH, WalletUtil.createHash(orderId, userId));
            responseParams.put(PaymentConstants.USER_ID, userId.toString());
            PaymentTransaction paymentTransaction = getTransactionAmount(mtxnId);

            double transactionAmount = paymentTransaction.getAmount();
            responseParams.put(PaymentConstants.AAMOUNT, String.valueOf(transactionAmount));

            if (transactionId != null && transactionId.equals("1")) {
                logger.info("Wallet pay success going ahead with recharge for order ID: ["
                        + paymentTransaction.getOrderId() + "]");
                paymentTransactionService.createSuccessfulWalletPaymentResponse(paymentTransaction);
                onPaymentSuccess(paymentTransaction);
                isPaymentSuccess = true;
            } else {
                logger.info("Wallet pay failure not going ahead with recharge for order ID: ["
                        + paymentTransaction.getOrderId() + "]");
            }

            walletService.notifyWalletPayment(paymentTransaction, "fw", isPaymentSuccess);

            responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, mtxnId);

            responseParams.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());

            String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
            PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
            paymentResponseWebDO.setRequestMap(responseParams);
            paymentResponseWebDO.setPg(IGatewayHandler.PaymentGateways.FREECHARGE_WALLET.getName());
            paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_WALLET));

            DelegateHelper.processRequest(request, response, delegateName, null, paymentResponseWebDO);
            if (!"true".equalsIgnoreCase(paymentResponseWebDO.getResultMap().get(PaymentConstants.IS_METHOD_SUCCESSFUL)
                    .toString())) {
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, "Wallet payment encountered error");
                return responseParams;
            }
        } catch (Exception fcExp) {
            responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
            responseParams.put(PaymentConstants.MESSAGE, "Wallet payment error: " + fcExp.getLocalizedMessage());
            return responseParams;
        }
        responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
        responseParams.put(PaymentConstants.ORDER_ID, orderId);
        logger.debug("Completed processing of wallet pay for user: " + userId);
        return responseParams;
    }

    private void onPaymentSuccess(PaymentTransaction paymentTransaction) throws DuplicateRequestException {
        if (kestrelWrapper.isEnqueuedForFulfilment(paymentTransaction.getOrderId())) {
            kestrelWrapper.enqueuePaymentRefund(paymentTransaction.getMerchantTxnId(), paymentTransaction.getAmount());
        } else {
            paymentSuccessAction.onPaymentSuccess(paymentTransaction.getOrderId(), paymentTransaction.getMerchantTxnId());
        }
    }
    
    @SuppressWarnings("unchecked")
    public Map<String, String> processOneCheckWalletPaymentRequest(HttpServletRequest request,
            HttpServletResponse response, Integer userId, String orderId, String mtxnId, String productType,
            HashMap<String, Object> responseMsg, String lookupID) {
        logger.debug("Starting processing of onecheck wallet pay for user: " + userId);
        Map<String, String> responseParams = new HashMap<String, String>();
        try {
            try {
                PaymentTransaction paymentTransaction = getTransactionAmount(mtxnId);
                double transactionAmount = paymentTransaction.getAmount();
                
                UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                        .findUserTransactionHistoryByOrderId(orderId);
                Map<String, Object> walletMetaDataMap = paymentTransactionService.getWalletMetaMap(userTransactionHistory,
                        orderId);
                String eventContext = getEventContext(walletMetaDataMap);
                Map<String, Object> debitBalanceRequest = new HashMap<String, Object>();
                WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
                walletDebitRequest.setUserID(userId);
                walletDebitRequest.setTxnAmount(new Amount(transactionAmount));
                walletDebitRequest.setMtxnId(mtxnId);
                walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
                walletDebitRequest.setCallerReference(mtxnId+RechargeConstants.RECHARGE_TYPE);
                walletDebitRequest.setFundDestination(FundDestination.RECHARGE.toString());
                walletDebitRequest.setMetadata("Wallet Debit for " + mtxnId + " " +eventContext);
                
                debitBalanceRequest.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
                debitBalanceRequest.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.DEBIT_RECHARGE);
                debitBalanceRequest.put(FCConstants.ORDER_ID, orderId);
                debitBalanceRequest.put(WalletWrapper.WALLET_TYPE, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
                debitBalanceRequest.put(WalletWrapper.DESTINATION_CORP_ID, getCorpId(orderId));
                String email = FCSessionUtil.getLoggedInEmailId();
                String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
                debitBalanceRequest.put(WalletWrapper.ONE_CHECK_IDENTITY, oneCheckIdentity);
                
                Map<String, Object> debitBalanceResponse = walletWrapper.debitBalance(debitBalanceRequest);
                
                String transactionId = "1";
                if (!FCConstants.SUCCESS.equals(debitBalanceResponse.get(FCConstants.STATUS))) {
                    logger.error("One check wallet payment failed for orderId :" + orderId + " KlickPay Response : "
                            + debitBalanceResponse.toString());
                    transactionId = "0";
                    responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                    responseParams.put(PaymentConstants.MESSAGE, "Wallet payment encountered error");
                }
                else{
                    DebitBalanceResponse balanceResponse = (DebitBalanceResponse) debitBalanceResponse.get(WalletWrapper.ONE_CHECK_WALLET_TRANSACTION_RESPONSE);
                    transactionId = balanceResponse.getTransactionId();
                    responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
                    responseParams.put(PaymentConstants.MESSAGE, "Wallet Transaction Successful");
                }

                responseParams.put(PaymentConstants.TRANSACTION_ID, transactionId);
                responseParams.put(PaymentConstants.AAMOUNT, String.valueOf(transactionAmount));
                responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, mtxnId);
                responseParams.put(PaymentConstants.ORDER_ID, orderId);
                responseParams.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());
                responseParams.put(PaymentConstants.USER_ID, String.valueOf(userId));
                responseParams.put(PaymentConstants.HASH, WalletUtil.createHash(mtxnId, userId));
                
                boolean isPaymentSuccess = false;
                if (transactionId != null && !transactionId.equals("0")) {
                    logger.info("OneCheck wallet pay success going ahead with recharge for order ID: ["
                            + paymentTransaction.getOrderId() + "]");
                    paymentTransaction.setTransactionId(transactionId);
                    paymentTransactionService.createSuccessfulWalletPaymentResponse(paymentTransaction);
                    paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), userId, PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS);

                    // save in-txn-data
                    HashMap<String, Object> paymentInfo = new HashMap<String, Object>();
                    paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, paymentTransaction.getPaymentType());
                    String paymentOptionStr = (paymentTransaction.getPaymentOption() == null)? "" : paymentTransaction.getPaymentOption(); 
                    if("UPI".equalsIgnoreCase(paymentOptionStr))
                    {
                    	paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, com.freecharge.payment.services.PaymentType.PaymentTypeName.UPI.getPaymentTypeCode());
                    }
                    paymentInfo.put(InTxnDataConstants.PG_AMOUNT, 0d);
                    paymentInfo.put(InTxnDataConstants.WALLET_AMOUNT, transactionAmount);
                    paymentInfo.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, new Date());
                    try{
                        logger.info("Populating wallet payment data to intxndata " + paymentInfo + " for lookupid " + lookupID);
                        campaignServiceClient.saveInTxnData1(lookupID, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, paymentInfo);
                    }catch (Exception e){
                        logger.error("Error saving intxndata in wallet controller for uniqueId " + lookupID + ", orderId "
                                + orderId + " and data " + paymentInfo, e);
                    }

                    onPaymentSuccess(paymentTransaction);
                    
                    isPaymentSuccess = true;
                } else {
                    logger.info("OneCheck wallet pay failure not going ahead with recharge for order ID: ["
                            + paymentTransaction.getOrderId() + "]");
                    paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), userId, PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
                }

                walletService.notifyWalletPayment(paymentTransaction, "ocw", isPaymentSuccess);

                String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
                PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
                paymentResponseWebDO.setRequestMap(responseParams);
                paymentResponseWebDO.setPg(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
                paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET));

                DelegateHelper.processRequest(request, response, delegateName, null, paymentResponseWebDO);
                String isMethodSuccessful = paymentResponseWebDO.getResultMap()
                        .get(PaymentConstants.IS_METHOD_SUCCESSFUL).toString();
                Map<String, String> resultMap = (Map<String, String>) paymentResponseWebDO.getResultMap().get(
                        PaymentConstants.DATA_MAP_FOR_WAITPAGE);
                String isPaySuccess = resultMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL);

                if (!"true".equalsIgnoreCase(isMethodSuccessful) || !"true".equalsIgnoreCase(isPaySuccess)) {
                    responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                    responseParams.put(PaymentConstants.MESSAGE, "Wallet payment encountered error");
                    return responseParams;
                }
            } catch (Exception e) {
                logger.error("Caught exception on doing OCW payment ", e);
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, "FC Cash payment error: " + e.getLocalizedMessage());
                return responseParams;
            }
        } catch (Exception fcExp) {
            logger.error("Caught exception on doing OCW payment ", fcExp);
            responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
            responseParams.put(PaymentConstants.MESSAGE, "FC Cash payment error: " + fcExp.getLocalizedMessage());
            return responseParams;
        }
        responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
        logger.debug("Completed processing of wallet pay for user: " + userId);
        return responseParams;
    }
    
    public String getCorpId(String orderId) {
    	
    	JSONObject jsonObject = appConfig.getCustomProp(PaymentConstants.WALLET_CORP_ID_FLAG);
		if(jsonObject != null && jsonObject.get("enabled") != null) {
			Boolean enabled = Boolean.valueOf((String)(jsonObject.get("enabled")));
			logger.info("Flag Received from AppConfig:" + enabled);
			if (enabled) {
				String productType = orderIdWriteDAO.getProductName(orderId).getProductType();
		    	String corpIdHandle = FCConstants.productTypeToCorpID.get(productType);
		    	if(corpIdHandle != null) {
		    		return fcProperties.getProperty(corpIdHandle);
		    	}
			}
		} 
		return null;
    }
    
    private String getEventContext(Map<String, Object> walletMetaDataMap) {

        String eventContext = "serviceNumber:" + walletMetaDataMap.get("serviceNumber");
        eventContext += " && productType:" + walletMetaDataMap.get("product");
        eventContext += " && operator:" + walletMetaDataMap.get("operatorName");

        return eventContext;
    }
    
    private List<NameValuePair> convert(Map<String, String> pgRequestData) {
        if (pgRequestData == null) {
            return null;
        }
        List<NameValuePair> requestData = new ArrayList<>();
        for (String key : pgRequestData.keySet()) {
            requestData.add(new BasicNameValuePair(key, String.valueOf(pgRequestData.get(key))));
        }
        return requestData;
    }

}