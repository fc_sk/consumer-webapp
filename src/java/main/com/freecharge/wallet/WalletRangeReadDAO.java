package com.freecharge.wallet;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.mappers.WalletServiceRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.WalletTransactionRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletTransaction;

/**
 * DAO for doing range queries. This uses different connection pool with 
 * possibily different set of properties and slave DB.
 * @author arun
 *
 */
public class WalletRangeReadDAO {
    protected final Logger logger = LoggingFactory.getLogger(getClass().getName());

    private NamedParameterJdbcTemplate jdbcTemplate;

    private final String WALLET_TRANSACTION_COLUMNS_STRING = "WALLET_TRANSACTION_ID, fk_wallet_id, transaction_date, "+
            "caller_refrence, TXN_AMOUNT, transaction_type, fund_source, fund_destination, metadata, order_id, " +
            "NEW_BALANCE, FK_USER_ID";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Map<String, Object>> findTransactions(long userID, TransactionType txnType, int walletHistoryLimit) {
        String clause = null;
        switch (txnType) {
        case DEPOSIT:
            clause = "transaction_type = 'deposit' ";
            break;
        case WITHDRAWAL:
            clause = "transaction_type = 'withdrawal' ";
            break;
        default:
            break;
        }

        String sql = "select wt.fk_wallet_id, wt.fk_user_id, wt.caller_refrence, wt.is_debit, wt.order_id, wt.fund_source, wt.fund_destination, wt.metadata, wt.TXN_AMOUNT, wt.transaction_type, wt.NEW_BALANCE, wt.created_ts "
                + " from wallet_txn wt where  wt.fk_user_id = :user_id " + (clause == null ? "" : ("AND " + clause + " ")) + "order by wt.CREATED_TS desc, wt.WALLET_TRANSACTION_ID desc LIMIT " + walletHistoryLimit;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", userID);
        LinkedList<Map<String, Object>> returnTxnList = new LinkedList(jdbcTemplate.queryForList(sql, params));

        if (returnTxnList.size() == walletHistoryLimit)
        	return returnTxnList;

        int oldWalletHistoryLimit = walletHistoryLimit - returnTxnList.size();

        String sqlOldTable = "select wt.fk_wallet_id, wt.fk_user_id, wt.caller_refrence, wt.is_debit, wt.order_id, wt.fund_source, wt.fund_destination, wt.metadata, wt.TXN_AMOUNT, wt.transaction_type, wt.NEW_BALANCE, wt.created_ts "
                + " from wallet_transaction wt where  wt.fk_user_id = :user_id " + (clause == null ? "" : ("AND " + clause + " ")) + "order by wt.CREATED_TS desc, wt.WALLET_TRANSACTION_ID desc LIMIT " + oldWalletHistoryLimit;
        LinkedList<Map<String, Object>> oldTxnList = new LinkedList(this.jdbcTemplate.queryForList(sqlOldTable, params));
        returnTxnList.addAll(oldTxnList);
        return returnTxnList;
    }

    public List<Map<String, Object>> findAllTransactions(long userID) {
        String sql = "select distinct(wt.fk_wallet_id), wt.fk_user_id, wt.caller_refrence, wt.is_debit, wt.order_id, wt.fund_source, wt.fund_destination, wt.metadata, wt.TXN_AMOUNT, wt.transaction_type,wt.NEW_BALANCE, created_ts  "
                + "from wallet_txn wt where "
                + "wt.fk_user_id = :user_id order by CREATED_TS desc, wallet_transaction_id desc";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", userID);
        LinkedList<Map<String, Object>> returnTxnList = new LinkedList(this.jdbcTemplate.queryForList(sql, params));

        String sqlOldTable = "select distinct(wt.fk_wallet_id), wt.fk_user_id, wt.caller_refrence, wt.is_debit, wt.order_id, wt.fund_source, wt.fund_destination, wt.metadata, wt.TXN_AMOUNT, wt.transaction_type,wt.NEW_BALANCE, created_ts  "
                + "from wallet_transaction wt where "
                + "wt.fk_user_id = :user_id order by CREATED_TS desc, wallet_transaction_id desc";

        LinkedList<Map<String, Object>> oldTxnList = new LinkedList(this.jdbcTemplate.queryForList(sqlOldTable, params));

        // Note that wt.fk_wallet_id may not be unique for very few cases.
        returnTxnList.addAll(oldTxnList);
        return returnTxnList;

    }
    
    public List<Map<String, Object>> findAllTransactionsWithLimit(long userID, int walletHistoryLimit) {
    	return this.findTransactions(userID, TransactionType.ANY, walletHistoryLimit);
    }

    public List<WalletTransaction> findTransactionTypeByOrderId(String orderId) {
        String sql = "(select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_transaction where order_id = :orderId) UNION (select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_txn where order_id = :orderId)" ;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", orderId);
        return jdbcTemplate.query(sql, params, new WalletTransactionRowMapper());  
    }
    
    public double getCumulativeAmountForDeposit(Long userId, Timestamp startTime, Timestamp endTime, List<FundSource> fundSources) {
        String sql = "select COALESCE(sum(txn_amount), 0) as totalAmount from wallet_txn where " +
                "FK_USER_ID = :userId and " +
                "transaction_type = 'deposit' and " +
                "CREATED_TS >= :startTime and CREATED_TS <= :endTime and " +
                "fund_source in (:fundSources);";

        List<String> fundSourcesString = new ArrayList<String>();

        for (FundSource fundSource : fundSources) {
            fundSourcesString.add(fundSource.toString());
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userId", userId);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        params.put("fundSources", fundSourcesString);
        
        List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(sql, params);
        
        BigDecimal cumulativeAmount = (BigDecimal) queryResult.get(0).get("totalAmount");
        
        String sqlOldTable = "select COALESCE(sum(txn_amount), 0) as totalAmount from wallet_transaction where " +
                "FK_USER_ID = :userId and " +
                "transaction_type = 'deposit' and " +
                "CREATED_TS >= :startTime and CREATED_TS <= :endTime and " +
                "fund_source in (:fundSources);";

        List<Map<String, Object>> oldQueryResult = jdbcTemplate.queryForList(sqlOldTable, params);
        final BigDecimal oldCumulativeAmount = (BigDecimal) oldQueryResult.get(0).get("totalAmount");
        
        final double finalcumulativeAmount = cumulativeAmount.doubleValue() + oldCumulativeAmount.doubleValue();
        return finalcumulativeAmount;
    }
    
    public long getTransactionCount(Long userId, Timestamp startTime, Timestamp endTime, List<FundSource> fundSources, List<TransactionType> transactionTypes) {
        String sql = "select count(txn_amount) as transactionCount from wallet_txn where " +
                "FK_USER_ID = :userId and " +
                "CREATED_TS >= :startTime and CREATED_TS <= :endTime and " +
                "fund_source in (:fundSources) and " +
                "transaction_type in (:transactionTypes);";
        
        List<String> fundSourcesString = new ArrayList<String>();
        
        for (FundSource fundSource : fundSources) {
            fundSourcesString.add(fundSource.toString());
        }
        
        List<String> transactionTypesString = new ArrayList<String>();
        
        for (TransactionType transactionType : transactionTypes) {
            transactionTypesString.add(transactionType.toString()); 
        }
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userId", userId);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        params.put("fundSources", fundSourcesString);
        params.put("transactionTypes", transactionTypesString);
        
        List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(sql, params);
        
        long transactionCount = (Long) queryResult.get(0).get("transactionCount");
        
        // Now look in the older table.
        String sqlOldTable = "select count(txn_amount) as transactionCount from wallet_transaction where " +
                "FK_USER_ID = :userId and " +
                "CREATED_TS >= :startTime and CREATED_TS <= :endTime and " +
                "fund_source in (:fundSources) and " +
                "transaction_type in (:transactionTypes);";
        
        List<Map<String, Object>> oldQueryResult = jdbcTemplate.queryForList(sqlOldTable, params);
        final long oldtransactionCount = (Long) oldQueryResult.get(0).get("transactionCount");

        final long finalTransactionCount = transactionCount + oldtransactionCount;
        return finalTransactionCount;
    }

    public List<WalletTransaction> getAddCashTransactionByUserId(long userId) {
        logger.info("get the orderId whose fundSource is addCash by userId"+userId);
        try {
            final String sql = "(select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_transaction where fk_user_id = :userId and fund_source='CASH_FUND') UNION " +
                    "(select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_txn where fk_user_id = :userId and fund_source='CASH_FUND')";
             Map<String, Object> params = new HashMap<String, Object>();
             params.put("userId", userId);
             return jdbcTemplate.query(sql, params, new WalletTransactionRowMapper());       
        } catch (RuntimeException re) {
            throw re;
        }   
    }

    public List<WalletTransaction> getWalletTxn(String orderId, String fundSource) {
        logger.info("get the walletTxn by fundsource and orderId" + orderId);
        try {
            final String sql = "(select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_transaction where order_id = :orderId and fund_source = :fundSource) UNION " +
            		"(select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_txn where order_id = :orderId and fund_source = :fundSource)";
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("orderId", orderId);
            params.put("fundSource", fundSource);
            
            return jdbcTemplate.query(sql, params, new WalletTransactionRowMapper());
        } catch (RuntimeException re) {
            throw re;
        }
    }
    public com.freecharge.wallet.service.Wallet findBalanceByUserID(long userID) {
        String sql = "SELECT * FROM wallet_status WHERE fk_user_id = :fk_user_id";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("fk_user_id", userID);
        List<com.freecharge.wallet.service.Wallet> rows = jdbcTemplate.query(sql, params, new WalletServiceRowMapper());
        if (rows.size() == 0) {
            return null;
        }
        return rows.get(0);
    }
    
    public List<WalletTransaction> getWalletTxnByDateRange(Date start, Date end) {
        String sql = "(SELECT "+WALLET_TRANSACTION_COLUMNS_STRING+" FROM wallet_transaction WHERE CREATED_TS >= :start and CREATED_TS <= :end) UNION " +
        		"(SELECT "+WALLET_TRANSACTION_COLUMNS_STRING+" FROM wallet_txn WHERE CREATED_TS >= :start and CREATED_TS <= :end)";
          		
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("start", start);
        params.put("end", end);

        List<WalletTransaction> rows = jdbcTemplate.query(sql, params, new WalletTransactionRowMapper());

        return rows;
    }

    public List<WalletTransaction> getAddCashTransactionByUserIdBetweenDays(long userId, Timestamp fromDateTime,
            Timestamp toDateTime) {
        logger.info("get the orderId whose fundSource is addCash by userId" + userId);
        try {
            final String sql = "select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_transaction where fk_user_id = :userId and fund_source='CASH_FUND' and created_ts >= :start and created_ts <= :end";
            final String sqlNew = "select "+WALLET_TRANSACTION_COLUMNS_STRING+" from wallet_txn where fk_user_id = :userId and fund_source='CASH_FUND' and created_ts >= :start and created_ts <= :end";

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userId", userId);
            params.put("start", fromDateTime);
            params.put("end", toDateTime);

            final String finalSql = sql + " UNION " + sqlNew;
            return jdbcTemplate.query(finalSql, params, new WalletTransactionRowMapper());
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
