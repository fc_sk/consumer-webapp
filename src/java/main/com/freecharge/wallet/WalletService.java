package com.freecharge.wallet;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.LockAcquisitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.antifraud.FraudDetection;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.exception.FCCreditWalletLimitExceededException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.GSTService;
import com.freecharge.common.util.TimeBarrierService;
import com.freecharge.freebill.common.BillPaymentCommonValidationApiResponse;
import com.freecharge.freebill.dao.BillReadDAO;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.domain.BillTxnHomePage;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freebill.service.BillPostpaidValidation;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.mobile.web.util.ConvenienceFeeUtil;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.services.PaymentType.PaymentTypeName;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.RefundSNSService;
import com.freecharge.sns.RevokeAlertSNSService;
import com.freecharge.sns.WalletTxnSNSService;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.sns.bean.RefundAlertBean;
import com.freecharge.sns.bean.RefundAlertBean.RefundStatus;
import com.freecharge.sns.bean.WalletTxnNotificationBean;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.exception.OneCheckWalletException;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.RefundResponse;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.Wallet.OneCheckTransactionTypePrefix;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletEligibility;
import com.freecharge.wallet.service.WalletItems;
import com.freecharge.wallet.service.WalletLimitRequest;
import com.freecharge.wallet.service.WalletLimitResponse;
import com.freecharge.wallet.service.WalletTransaction;
import com.freecharge.wallet.service.limitengine.WalletRuleEvaluator;
import com.freecharge.wallet.util.FCWalletUtil;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.FeeCharge;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.snapdeal.payments.sdmoney.service.model.CreditGeneralBalanceToUserResponse;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceResponse;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;

@Component
public class WalletService {
    private Logger                    logger = LoggingFactory.getLogger(getClass());

    private final static String       PRIMARY_KEY_STRING = "primary key";

    @Autowired
    private FCProperties              fcProperties;

    @Autowired
    private CartService               cartService;

    @Autowired
    private FreefundService           freefundService;

    @Autowired
    private WalletDAO                 walletDAO;

    @Autowired
    private WalletRangeReadDAO        walletRangeReadDAO;

    @Autowired
    private WalletItemsDAO            walletItemsDAO;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private WalletRuleEvaluator       ruleEvaluator;

    @Autowired
    private InService                 inService;

    @Autowired
    PaymentRefund                     paymentRefund;

    @Autowired
    PgReadDAO                         pgRadDao;

    @Autowired
    OrderIdSlaveDAO                   orderIdDAO;

    @Autowired
    BillReadDAO                       billReadDAO;

    @Autowired
    TxnHomePageDAO                    txnHomePageDAO;

    @Autowired
    private FraudDetection            fraudDetection;

    @Autowired
    private GSTService 		      gstService;

    @Autowired
    TimeBarrierService                timeBarrierService;

    @Autowired
    MetricsClient                     metricsClient;
    
    @Autowired
    BillPaymentService               billPaymentService;
    
    @Autowired
    BillPostpaidValidation               billPostpaidValidation;
    
    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO; 
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @Autowired
    private RefundSNSService refundSNSService;
    
    @Autowired
    private WalletTxnSNSService walletTxnSNSService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    UserServiceProxy userServiceProxy;
    
    @Autowired
    WalletWrapper walletWrapper;

    @Autowired
    private FCWalletUtil fcWalletUtil;
    
    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    private PaymentSNSService paymentSNSService;
    
    @Autowired
    private ConvenienceFeeUtil convenienceFeeUtil;

    private static final String ORDER_ID_NOT_PRESENT = "Not Present";
    private static final String MONEY_TRANSFERRED = "MONEY_TRANSFERRED";

    public boolean isUnderMaintenance() {
        return fcProperties.isWalletUnderMaintenance();
    }

    public boolean isWalletPayEnabled() {
        return fcProperties.isWalletPayEnabled();
    }

    public boolean isWalletDataDisplayEnabled() {
        return fcProperties.isWalletDataDisplayEnabled();
    }

    public Amount findBalance(long userID) {
        com.freecharge.wallet.service.Wallet walletService = walletRangeReadDAO.findBalanceByUserID(userID);
        Amount userBalance = Amount.ZERO;

        if (walletService != null) {
            userBalance = new Amount(walletService.getCurrentBalance());
        }

        return userBalance;
    }

    public Wallet findUserWallet(long usreId) {
        return walletRangeReadDAO.findBalanceByUserID(usreId);
    }

    // call this method in a transaction only
    public String saveWalletStatus(long userID, Amount oldBalance, Amount newBalance) {
        String walletId = null;
        if (oldBalance == null) {
            walletId = Long.toString(walletDAO.insertStatus(userID, newBalance));
        } else {
            walletDAO.updateStatus(userID, newBalance, oldBalance);
        }

        return walletId;
    }

    public Long insertWalletItem(long fkWalletTransactionId, long fkItemId, String entityId) {
        return walletItemsDAO.insert(fkWalletTransactionId, fkItemId, entityId);
    }

    /**
     * Utility method to calculate eligibility for payment using wallet.
     * 
     * @param userId
     * @param totalAmount
     * @param cartBusinessDo
     * @return
     */
    public WalletEligibility calculateEligibility(Integer userId, Amount totalAmount, CartBusinessDo cartBusinessDo, Amount walletBalance) {
        WalletEligibility eligibility = new WalletEligibility();
        if (freefundService.containsFreeFund(cartBusinessDo)
                && !freefundService.allPaymentOptionsAllowed(cartBusinessDo)) {
            return eligibility;
        }
        if (this.isWalletDataDisplayEnabled()) {
            if (walletBalance.getAmount().doubleValue() > 0) {
                if (totalAmount.getAmount().doubleValue() > walletBalance.getAmount().doubleValue()) {
                    eligibility.setEligibleForPartialPayment(true);
                } else {
                    eligibility.setEligibleForFullWalletPayment(true);
                }
            }
        }
        return eligibility;
    }
    
    /*
     * This method just checks the balance criteria and returns the wallet eligibility.
     * This will not have any dependency on the applied promo/freefund code.
     */
    public WalletEligibility calculateWalletEligibility(Integer userId, Amount totalAmount,
            CartBusinessDo cartBusinessDo, Amount walletBalance) {
        WalletEligibility eligibility = new WalletEligibility();
        if (this.isWalletDataDisplayEnabled()) {
            if (walletBalance.getAmount().doubleValue() > 0) {
                if (totalAmount.getAmount().doubleValue() > walletBalance.getAmount().doubleValue()) {
                    eligibility.setEligibleForPartialPayment(true);
                } else {
                    eligibility.setEligibleForFullWalletPayment(true);
                }
            }
        }
        return eligibility;
    }

    /**
     * Utility method to calculate payment breakup
     * 
     * @param cartBusinessDo
     * @return
     */
    public PaymentBreakup calculatePaymentBreakup(Integer userId, Amount totalPayAmount, CartBusinessDo cartBusinessDo, WalletEligibility walletEligibility, Amount walletBalance) {
        PaymentBreakup paymentBreakup = new PaymentBreakup();
        paymentBreakup.setPayableTotalAmount(totalPayAmount);
        paymentBreakup.setPayablePGAmount(totalPayAmount);
        paymentBreakup.setPayableWalletAmount(Amount.ZERO);
        paymentBreakup.setWalletBalance(Amount.ZERO);

        if (this.isWalletDataDisplayEnabled()) {
            paymentBreakup.setWalletBalance(walletBalance);

            if (walletEligibility.getEligibleForPartialPayment()) {
                Amount payablePgAmount = new Amount(totalPayAmount.getAmount().doubleValue()
                        - walletBalance.getAmount().doubleValue());

                paymentBreakup.setPayablePGAmount(payablePgAmount);
                paymentBreakup.setPayableWalletAmount(walletBalance);
            } else if (walletEligibility.getEligibleForFullWalletPayment()) {
                paymentBreakup.setPayablePGAmount(Amount.ZERO);
                paymentBreakup.setPayableWalletAmount(totalPayAmount);
            } else {
                paymentBreakup.setPayablePGAmount(totalPayAmount);
                paymentBreakup.setPayableWalletAmount(Amount.ZERO);
            }
        }

        return paymentBreakup;
    }

    @Transactional
    public String credit(WalletCreditRequest walletCreditRequest) throws LockAcquisitionException {
        Map<String,Object> resultMap = null;
        resultMap = creditWithResponse(walletCreditRequest);
        if(resultMap!=null)
        {
            if(resultMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS)) {
                return resultMap.get(PRIMARY_KEY_STRING).toString();
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    public Map<String,Object> creditWithResponse(WalletCreditRequest walletCreditRequest) throws LockAcquisitionException {
        Map<String,Object> resultMap = new HashMap<>();
        Amount userBalance = null;
        String walletIdToUpdate = null;
        walletCreditRequest.getTxnAmount().negativeCheck();
        Preconditions.checkArgument(walletCreditRequest.getMtxnId() != null, "orderId cannot be null");
        com.freecharge.wallet.service.Wallet walletService = walletDAO.findAndLockBalanceByUserID(walletCreditRequest
                .getUserID());

        List<WalletTransaction> walletTransactions = walletDAO.walletTxnForCallerReference(walletCreditRequest
                .getCallerReference());
        if (walletTransactions != null && !walletTransactions.isEmpty()) {
            logger.info(String.format("wallet is already credited for this caller reference %s and mtxn id %s",
                    walletCreditRequest.getCallerReference(), walletCreditRequest.getMtxnId()));
            resultMap.put(FCConstants.STATUS, FCConstants.FAILURE);
            resultMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE, "wallet is already credited for this caller reference and mtxn id");
            return resultMap;
        }

        if (walletService != null) {
            userBalance = new Amount(walletService.getCurrentBalance());
        }
        Amount oldBalance = userBalance == null ? Amount.ZERO : userBalance;
        BigDecimal newBalance = oldBalance.getAmount().add(walletCreditRequest.getTxnAmount().getAmount());

        FundSource fundSource = Wallet.FundSource.valueOf(walletCreditRequest.getFundSource());
        if (FundSource.RECHARGE_FUND != fundSource) {
            assertBalanceUpperLimit(walletCreditRequest, newBalance);
        }
        if (FundSource.REFUND == fundSource && walletCreditRequest.getMetadata() != null
                && !walletCreditRequest.getMetadata().equals(PaymentConstants.PAYMENT_TYPE_WALLET)) {
            assertNotFraudNewUser(walletCreditRequest, newBalance);
        }

        String walletId = saveWalletStatus(walletCreditRequest.getUserID(), userBalance, new Amount(newBalance));

        if (walletId != null) {
            walletIdToUpdate = walletId;
        } else {
            walletIdToUpdate = Long.toString(walletService.getWalletStatusId());
        }

        String primaryKey = walletDAO
                .addCreditTransaction(walletIdToUpdate, walletCreditRequest, new Amount(newBalance), oldBalance);

        if (fundSource == FundSource.REFUND) {
            publishWalletRefund(walletCreditRequest, true);
        }

        try {
            WalletTxnNotificationBean walletTxnNotificationBean = generateWalletTxnAlertBean(walletCreditRequest, oldBalance.getAmount(), newBalance);
            walletTxnSNSService.publish(WalletTxnNotificationBean.getJsonString(walletTxnNotificationBean));
        } catch (IOException e) {
            metricsClient.recordEvent("Payment", "notification.credit", "failure");
            logger.error("Failed to push wallet txn notification for mtxnId " + walletCreditRequest.getMtxnId());
        }

        if(primaryKey!=null) {
            resultMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            resultMap.put(PRIMARY_KEY_STRING, primaryKey);
        }
        else {
            resultMap.put(FCConstants.STATUS, FCConstants.FAILURE);
            resultMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE, "Failed to perform credit transaction.");
        }
        return resultMap;
    }

    private WalletTxnNotificationBean generateWalletTxnAlertBean(WalletCreditRequest walletCreditRequest, BigDecimal oldBalance,
            BigDecimal newBalance) {
        WalletTxnNotificationBean wtab = new WalletTxnNotificationBean();
        wtab.setCallerRefrence(walletCreditRequest.getCallerReference());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sDate= sdf.format(new Date());
        wtab.setCreatedTs(sDate);
        wtab.setTransactionDate(sDate);
        wtab.setFundDestination(walletCreditRequest.getFundDestination());
        wtab.setFundSource(walletCreditRequest.getFundSource());
        wtab.setMtxnId(walletCreditRequest.getMtxnId());
        wtab.setNewBalance(newBalance);
        wtab.setOldBalance(oldBalance);
        wtab.setTxnAmount(walletCreditRequest.getTxnAmount().getAmount());
        wtab.setTxnType(walletCreditRequest.getTransactionType());
        wtab.setUserId(walletCreditRequest.getUserID());
        return wtab;
    }

    private void publishWalletRefund(WalletCreditRequest walletCreditRequest, boolean isSuccess) {

        try {
            RefundAlertBean refundAlertBean = new RefundAlertBean();
            refundAlertBean.setUserId(walletCreditRequest.getUserID());
            refundAlertBean.setOrderId(walletCreditRequest.getMtxnId());
            refundAlertBean.setAmount(walletCreditRequest.getTxnAmount().getAmount().doubleValue());
            String status = isSuccess ? RefundStatus.REFUND_SUCCESS.getStatus() : RefundStatus.REFUND_FAILURE.getStatus();
            refundAlertBean.setStatusCode(status);

            refundSNSService.publish(refundAlertBean.getOrderId(), "{\"message\" : " + RefundAlertBean.getJsonString(refundAlertBean) + "}");
        } catch (Exception e) {
            logger.error(" Exception while updating the Refund alert for order id " + walletCreditRequest.getMtxnId(), e);
        }
    }
    
    public String debit(WalletDebitRequest walletDebitRequest, Amount minBalance) throws DuplicateRequestException {
    	return debit(walletDebitRequest, minBalance, false);
    }

    @Transactional
    public String debit(WalletDebitRequest walletDebitRequest, Amount minBalance, boolean isMigration) throws DuplicateRequestException {
        if (!timeBarrierService.putTimeBarrier(walletDebitRequest.getCallerReference(), 5000)) {
            String errorString = "wallet debit in progress for this caller reference "
                    + walletDebitRequest.getCallerReference();
            logger.info(errorString);
            metricsClient.recordEvent("Wallet", "Debit", "DuplicateRequest");
            throw new DuplicateRequestException(errorString);
        }

        walletDebitRequest.getTxnAmount().negativeCheck();
        Preconditions.checkArgument(walletDebitRequest.getMtxnId() != null, "orderId cannot be null");
        Preconditions.checkArgument(minBalance != null, "minBalance cannot be null");
        com.freecharge.wallet.service.Wallet walletService = walletDAO.findAndLockBalanceByUserID(walletDebitRequest
                .getUserID());

        
        List<WalletTransaction> walletTransactions = null;
        if(isMigration)
        {
        	Date date = new Date();
        	walletTransactions = walletDAO.walletTxnForCallerReference(walletDebitRequest
                    .getCallerReference()+"_"+date.getYear()+"_"+date.getMonth()+"_"+date.getDay());
        } else
        {
        	walletTransactions = walletDAO.walletTxnForCallerReference(walletDebitRequest
                    .getCallerReference());
        }

        if (walletTransactions != null && !walletTransactions.isEmpty()) {
            String errorString = "wallet is already debited for this caller reference "
                    + walletDebitRequest.getCallerReference();
            logger.info(errorString);
            metricsClient.recordEvent("Wallet", "Debit", "DuplicateRequest");
            throw new DuplicateRequestException(errorString);
        }

        Amount dbBalance = new Amount(walletService.getCurrentBalance());
        Amount oldBalance = dbBalance == null ? Amount.ZERO : dbBalance;
        BigDecimal newBalance = oldBalance.getAmount().subtract(walletDebitRequest.getTxnAmount().getAmount());
        if (newBalance.compareTo(minBalance.getAmount()) < 0) {
            throw new IllegalStateException("Debit cannot allow balance to go under limit [" + minBalance.getAmount()
                    + "] for mtxnID: [" + walletDebitRequest.getMtxnId() + "]");
        }
        saveWalletStatus(walletDebitRequest.getUserID(), dbBalance, new Amount(newBalance));
        try {
            WalletTxnNotificationBean walletTxnAlertBean = generateWalletTxnAlertBean(walletDebitRequest, oldBalance.getAmount(), newBalance);
            walletTxnSNSService.publish(WalletTxnNotificationBean.getJsonString(walletTxnAlertBean));
        } catch (IOException e) {
            metricsClient.recordEvent("Payment", "notification.debit", "failure");
            logger.error("Failed to push wallet txn notification for mtxnId " + walletDebitRequest.getMtxnId());
        }
        return walletDAO.addDebitTransactionToWallet(walletService.getWalletStatusId(), walletDebitRequest, new Amount(
                newBalance), oldBalance);

    }

    private WalletTxnNotificationBean generateWalletTxnAlertBean(WalletDebitRequest walletDebitRequest, BigDecimal oldBalance,
            BigDecimal newBalance) {
        WalletTxnNotificationBean wtab = new WalletTxnNotificationBean();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sDate= sdf.format(new Date());
        wtab.setCallerRefrence(walletDebitRequest.getCallerReference());
        wtab.setCreatedTs(sDate);
        wtab.setTransactionDate(sDate);
        wtab.setFundDestination(walletDebitRequest.getFundDestination());
        wtab.setFundSource(walletDebitRequest.getFundSource());
        wtab.setMtxnId(walletDebitRequest.getMtxnId());
        wtab.setNewBalance(newBalance);
        wtab.setOldBalance(oldBalance);
        wtab.setTxnAmount(walletDebitRequest.getTxnAmount().getAmount());
        wtab.setTxnType(walletDebitRequest.getTransactionType());
        wtab.setUserId(walletDebitRequest.getUserID());
        return wtab;
    }

    public boolean dorevoke(String orderId, BigDecimal amountToDebit) {
        try {
            CartBusinessDo cart = cartService.getCartByOrderId(orderId);
            Amount fcAmount = walletWrapper.getTotalBalance(cart.getUserId());
            
            if (fcAmount.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                logger.info("Failed to Revoke (Wallet balance is ZERO) for orderId " + orderId + " Amount : " + amountToDebit);
                return false;
            }
            
            if (fcAmount.getAmount().compareTo(amountToDebit) < 0) {
                amountToDebit = fcAmount.getAmount();
            }
            WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
            walletDebitRequest.setUserID(cart.getUserId());
            walletDebitRequest.setTxnAmount(new Amount(amountToDebit));
            walletDebitRequest.setMtxnId(orderId);
            walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
            walletDebitRequest.setCallerReference("Payment deducted for " + orderId);
            walletDebitRequest.setFundSource("");
            walletDebitRequest.setFundDestination(Wallet.FundDestination.REVOKE_FUND.toString());
            walletDebitRequest.setMtxnId(orderId);
            
            UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId);
            Map<String, Object> walletMetaDataMap = paymentTransactionService.getWalletMetaMap(userTransactionHistory, orderId);
            String metaData = new Gson().toJson(walletMetaDataMap);
            walletDebitRequest.setMetadata(metaData);

            Map<String, Object> debitBalanceMap = new HashMap<>();
            debitBalanceMap.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
            debitBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE,
                    OneCheckWalletService.OneCheckTxnType.DEBIT_REVOKE_FUND);
            debitBalanceMap.put(FCConstants.ORDER_ID, orderId);
            
            Users user = userServiceProxy.getUserByUserId(cart.getUserId());
            String email = user.getEmail();
            String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);

            if (FCUtil.isNotEmpty(oneCheckIdentity)) {
                debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
                debitBalanceMap.put(WalletWrapper.ONE_CHECK_IDENTITY, oneCheckIdentity);
            } else {
                debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
            }
            Map<String, Object> responseMap = walletWrapper.debitBalance(debitBalanceMap);

            String status = (String) responseMap.get(FCConstants.STATUS);
            if (FCConstants.SUCCESS.equals(status)) {
                DebitBalanceResponse debitBalanceResponse = (DebitBalanceResponse)responseMap.get("walletTransactionResponse");
                String debitTransactionId = debitBalanceResponse.getTransactionId();
                logger.info("Succesfully Revoked for orderId " + orderId + " Amount : " + amountToDebit + " respone : " + responseMap);
                PaymentTransaction payTxn = paymentTransactionService.getSuccessfulPaymentTransactionByOrderId(orderId);
                createPaymentRefundTxnForRevokeFund(payTxn, amountToDebit, oneCheckIdentity);
                this.publishRevokeAlert(debitBalanceMap, payTxn, debitTransactionId);
                return true;
            } else {
                logger.info("Failed to Revoke for orderId " + orderId + " Amount : " + amountToDebit);
                return false;
            }
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
        return false;
    }
    
    private void createPaymentRefundTxnForRevokeFund(PaymentTransaction paymentTransaction, BigDecimal revokedAmount, String oneCheckId) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        paymentRefundTransaction.setRefundedAmount(new Amount(revokedAmount.negate()));
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setReceivedFromPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setSentToPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setPaymentTransactionId(paymentTransaction.getPaymentTxnId());
        paymentRefundTransaction.setRawPGRequest("DEBIT_REVOKE_FUND");
        if (oneCheckId == null) {
            paymentRefundTransaction.setRawPGResponse("DEBIT_REVOKE_FUND");
            paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
        } else {
            paymentRefundTransaction.setRawPGResponse("DEBIT_REVOKE_FUND");
            paymentRefundTransaction.setRefundTo(PaymentConstants.ONE_CHECK_WALLET_CODE);
        }
        paymentTransactionService.create(paymentRefundTransaction);
    }

    @Transactional
    public String debit(WalletDebitRequest walletDebitRequest ,boolean isMigration) throws DuplicateRequestException {
        return debit(walletDebitRequest, Amount.ZERO , isMigration);
    }
    
    public String debit(WalletDebitRequest walletDebitRequest) throws DuplicateRequestException {
        return debit(walletDebitRequest, false);
    }

    @Transactional
    public String pay(WalletDebitRequest walletDebitRequest) throws DuplicateRequestException {
        List<PaymentTransaction> paymentTransactions = paymentTransactionService
                .getPaymentTransactionByMerchantOrderId(walletDebitRequest.getMtxnId());

        if (paymentTransactions.isEmpty()) {
            throw new RuntimeException("No payment transaction found for merchant order id:"
                    + walletDebitRequest.getMtxnId());
        }

        if (paymentTransactions.size() > 1) {
            logger.error("More than one transaction found for merchant order id:" + walletDebitRequest.getMtxnId());
        }

        PaymentTransaction paymentTransaction = paymentTransactions.get(0);

        walletDebitRequest.setTxnAmount(new Amount(paymentTransaction.getAmount()));
        String transactionId = debit(walletDebitRequest);
        if (transactionId != null) {
            paymentTransactionService.publishPaymentSuccessEvent(paymentTransaction);
        }
        return transactionId;
    }

    public List<Map<String, Object>> findAllWalletTxn(long userID) {
        com.freecharge.wallet.service.Wallet walletService = walletRangeReadDAO.findBalanceByUserID(userID);
        if (walletService != null) {
            return walletRangeReadDAO.findAllTransactions(walletService.getFkUserId());
        } else {
            return null;
        }
    }

    public List<Map<String, Object>> findAllWalletTxnWithLimit(long userID) {
        com.freecharge.wallet.service.Wallet walletService = walletRangeReadDAO.findBalanceByUserID(userID);
        if (walletService != null) {
            return walletRangeReadDAO.findAllTransactionsWithLimit(walletService.getFkUserId(),
                    WebConstants.WALLET_HISTORY_LIMIT);
        } else {
            return null;
        }
    }

    public List<Map<String, Object>> getBalanceRows(int userID, Map<String, Object> lastEntry, boolean isLimit) {
        PaymentTransaction lastPaymentTransaction = null;
        DateFormat walletDisplayDateFormat = new SimpleDateFormat("d MMM yy hh:mm a");
        Timestamp lastEntryTime = (Timestamp) lastEntry.get("created_ts");
        List<Map<String, Object>> rows;
        if (lastEntryTime != null) {
            String formattedLastEntryTime = walletDisplayDateFormat.format(lastEntryTime);

            lastEntry.put("created_ts", formattedLastEntryTime);
        }

        String lastEntryMtxnId = (String) lastEntry.get("order_id");

        if (lastEntryMtxnId != null && !lastEntryMtxnId.isEmpty()) {
            if (lastEntryMtxnId.contains("_")) {
                int index = lastEntryMtxnId.indexOf("_");
                lastPaymentTransaction = paymentTransactionService
                        .getUniquePaymentTransactionByMerchantOrderId(lastEntryMtxnId.substring(0, index));
            } else {
                lastPaymentTransaction = paymentTransactionService
                        .getUniquePaymentTransactionByMerchantOrderId(lastEntryMtxnId);
            }

            lastEntry.put("order_id", (lastPaymentTransaction != null) ? lastPaymentTransaction.getOrderId() : "-");
        }

        if (isLimit == true) {
            rows = findAllWalletTxnWithLimit(userID);
        } else {
            rows = findAllWalletTxn(userID);
        }

        if (rows != null && rows.size() > 0) {
            for (Map<String, Object> displayRow : rows) {
                Timestamp transactionTime = (Timestamp) displayRow.get("created_ts");

                String formattedTransactionTime = walletDisplayDateFormat.format(transactionTime);

                displayRow.put("created_ts", formattedTransactionTime);
                FundSource fundSource = null;
                FundDestination fundDestination = null;
                String orderId = null;

                if (displayRow.get("fund_source") != null && !((String) displayRow.get("fund_source")).isEmpty()) {
                    fundSource = Wallet.FundSource.valueOf((String) displayRow.get("fund_source"));
                }
                if (displayRow.get("fund_destination") != null
                        && !((String) displayRow.get("fund_destination")).isEmpty()) {
                    fundDestination = Wallet.FundDestination.valueOf((String) displayRow.get("fund_destination"));
                }
                List<PaymentTransaction> paymentTransactionList = null;
                if (FundSource.REDEEM != fundSource) {
                    String mtxnId = (String) displayRow.get("order_id");

                    paymentTransactionList = paymentTransactionService.getPaymentTransactionByMerchantOrderId(mtxnId);
                    if (paymentTransactionList != null) {
                        orderId = paymentTransactionList.get(0).getOrderId();
                    } else {
                        if (mtxnId.contains("_")) {
                            int index = mtxnId.indexOf("_");
                            orderId = mtxnId.substring(0, index);
                        } else {
                            if (mtxnId.equals("")) {
                                orderId = ORDER_ID_NOT_PRESENT;
                            } else {
                                orderId = mtxnId;
                            }

                        }
                    }
                    /*
                     * If this was for recharge then get recharge transaction
                     * data; just to see if it failed or not.
                     */

                    if (displayRow.get("fund_destination") != null
                            && !((String) displayRow.get("fund_destination")).isEmpty()) {
                        FundDestination fundDest = Wallet.FundDestination.valueOf((String) displayRow
                                .get("fund_destination"));

                        if (Wallet.FundDestination.RECHARGE == fundDest) {
                        	InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId);
                            if (inTransactionData != null) {
                                if (!inTransactionData.isTransactionSuccessOrPending()) {
                                    displayRow.put("recharge_failed", true);
                                }
                            }
                        }
                    }

                    displayRow.put("order_id", orderId);
                }

                if (fundSource != null && FundSource.REDEEM == fundSource) {
                    displayRow.put("description", displayRow.get("caller_refrence"));
                } else if (fundDestination != null && FundDestination.REVOKE_FUND == fundDestination) {
                    displayRow.put("description", displayRow.get("caller_refrence"));
                } else {
                    if (displayRow.get("transaction_type") != null) {
                        if (displayRow.get("transaction_type").toString()
                                .equalsIgnoreCase(TransactionType.WITHDRAWAL.toString())) {
                            displayRow.put("description", Wallet.FundDestination.getFundDestinationValue(displayRow
                                    .get("fund_destination").toString()));
                        } else {
                            displayRow.put("description",
                                    Wallet.FundSource.getFundSourceValue(displayRow.get("fund_source").toString()));
                        }

                        /* below code change to handle retry cases */

                        UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                                .findUserTransactionHistoryByOrderId(orderId);
                        BillPaymentStatus billPaymentStatus = null;
                        if (userTransactionHistory != null) {
                            if (FCUtil.isBillPostpaidType(userTransactionHistory.getProductType())) {
                                billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
                                if (billPaymentStatus != null) {
                                    try {
                                        String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
                                        if (lookupId != null && !lookupId.isEmpty()) {
                                            List<TxnHomePage> homePages = txnHomePageDAO.findByLookupId(lookupId);
                                            if (homePages != null) {
                                                BillTxnHomePage billTxnHomePage = homePages.get(0).getBillTxnHomePage();
                                                if (billTxnHomePage != null) {
                                                    displayRow.put("operator",
                                                            billTxnHomePage.getPostpaidMerchantName());
                                                    displayRow.put("circle", "ALL");
                                                    displayRow.put("subscriber_number",
                                                            billTxnHomePage.getPostpaidNumber());
                                                } else {
                                                    BillPaymentCommonValidationApiResponse billResponse = billPostpaidValidation
                                                            .validate(String.valueOf(billPaymentStatus
                                                                    .getFk_operator_master_id()), billPaymentStatus
                                                                    .getPostpaidNumber(), null, billPaymentStatus
                                                                    .getAmount());
                                                    billTxnHomePage = BillTxnHomePage.createForMobilePostpaid(
                                                            billPaymentStatus.getPostpaidNumber(), billResponse,
                                                            billPaymentStatus.getOperatorName());
                                                    displayRow.put("operator",
                                                            billTxnHomePage.getPostpaidMerchantName());
                                                    displayRow.put("circle", "ALL");
                                                    displayRow.put("subscriber_number",
                                                            billTxnHomePage.getPostpaidNumber());
                                                    displayRow.put("postpaid_retry", true);
                                                }
                                            }
                                            if (!billPaymentStatus.getSuccess()) {
                                                displayRow.put("recharge_failed", true);
                                            } else {
                                                displayRow.put("recharge_failed", false);
                                            }
                                        }
                                    } catch (Exception exception) {
                                        if (!orderId.equals(ORDER_ID_NOT_PRESENT)) {
                                            logger.info(
                                                    "Exception raised while fetching bill payment txn for waller credit history",
                                                    exception);
                                        }
                                    }
                                }
                            } else if (FCUtil.isUtilityPaymentType(userTransactionHistory.getProductType())) {
                                String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
                                List<TxnHomePage> homePages = txnHomePageDAO.findByLookupId(lookupId);
                                String billId = homePages.get(0).getServiceNumber();
                                BillWithSubscriberNumber bill = null;
                                try {
                                    bill = billPayServiceRemote.getBillDetails(billId);
                                } catch (Exception e) {
                                    logger.error("Caught exception on fetching bill for order " + orderId, e);
                                }
                                if (bill != null) {
                                    displayRow.put("operator", homePages.get(0).getOperatorName());
                                    displayRow.put("circle", bill.getServiceRegionForOxigen());
                                    displayRow
                                            .put("subscriber_number", bill.getAdditionalInfo1());
                                }
                            } else {
                                InTransactionData inTransactionData = inService.getLatestInTransactionData(orderId);
                                if (inTransactionData != null) {
                                    if (inTransactionData.getInRequest() != null) {
                                        displayRow.put("operator", inTransactionData.getInRequest().getOperator());
                                        displayRow.put("circle", inTransactionData.getInRequest().getCircle());
                                        displayRow.put("subscriber_number", inTransactionData.getInRequest()
                                                .getSubscriberNumber());
                                    }
                                    if (!inTransactionData.isTransactionSuccessOrPending()) {
                                        displayRow.put("recharge_failed", true);
                                    }
                                }
                            }
                        }
                    } else {
                        if ((Boolean) displayRow.get("is_debit")) {
                            displayRow.put("description", "Recharge for");
                        } else {
                            displayRow.put("description", "Recharge failed for");
                        }
                        displayRow.put("fund_source", "REFUND");
                    }
                }

                if (displayRow.get("operator") == null && displayRow.get("transaction_type") == null
                        && displayRow.get("order_id") != null) {
                    if ((Boolean) displayRow.get("is_debit") == false) {
                        displayRow.put("description", "Payment received towards Order Id "
                                + displayRow.get("order_id").toString());
                    } else {
                        displayRow.put("description", "Payment made towards Order Id "
                                + displayRow.get("order_id").toString());
                    }
                }

                if (displayRow.get("fund_source") != null && !((String) displayRow.get("fund_source")).isEmpty()) {
                    fundSource = Wallet.FundSource.valueOf((String) displayRow.get("fund_source"));

                    if (paymentTransactionList != null
                            && (FundSource.RECHARGE_FUND == fundSource || FundSource.CASH_FUND == fundSource)) {
                        for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                            if (paymentTransaction.getIsSuccessful()
                                    && !PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentTransaction
                                            .getPaymentGateway())) {

                                Integer paymentTypeCode = paymentTransaction.getPaymentType();
                                String paymentTypeString = PaymentTypeName.fromPaymentTypeCode(paymentTypeCode)
                                        .toString();

                                displayRow.put("description", fundSource.getDescriptionString() + " through "
                                        + paymentTypeString);
                            }
                        }
                    }

                    if (FundSource.CASHBACK == fundSource || FundSource.ARREWARD == fundSource || FundSource.REWARD_SERVICE == fundSource) {
                        String metaData = (String) displayRow.get("metadata");
                        if (StringUtils.isNotEmpty(metaData)) {
                            displayRow.put("description", metaData);
                        }
                    }
                }
            }
        }
        return rows;
    }

    public Map<String, Object> findLastEntry(long userID) {
        List<Map<String, Object>> rows = walletRangeReadDAO.findTransactions(userID, TransactionType.ANY, 1);
        return (rows == null || rows.size() == 0) ? new HashMap<String, Object>() : rows.get(0);
    }

    public List<WalletTransaction> findTransaction(String merchantOrderId, TransactionType transactionType) {
        return walletDAO.findTransaction(merchantOrderId, transactionType);
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(WalletService.class.getSimpleName());
    }

    public WalletLimitResponse evaluateWalletLimit(WalletLimitRequest limitRequest) {
        return ruleEvaluator.evaluateLimit(limitRequest);
    }

    private void assertBalanceUpperLimit(WalletCreditRequest walletCreditRequest, BigDecimal newBalance) {
        BigDecimal creditBalanceLimit = new BigDecimal(
                fcProperties.getProperty(FCConstants.WALLET_CREDIT_BALANCE_LIMIT));
        if (newBalance.compareTo(creditBalanceLimit) > 0) {
            throw new FCCreditWalletLimitExceededException("Credit cannot allow balance to exceed limit " + creditBalanceLimit
                    + "for fund source" + walletCreditRequest.getFundSource());
        }
    }

    private void assertNotFraudNewUser(WalletCreditRequest walletCreditRequest, BigDecimal newBalance) {
        long userId = walletCreditRequest.getUserID();
        BigDecimal creditAmt = walletCreditRequest.getTxnAmount().getAmount();
        String rechargeAmount = walletCreditRequest.getTxnAmount().getAmount().toString();
        String totalAmount = walletCreditRequest.getTxnAmount().getAmount().toString();
        String lookupId = walletCreditRequest.getMtxnId();
        String fundSource = walletCreditRequest.getFundSource();
        fraudDetection.checkFraudNewUser(userId, creditAmt, rechargeAmount, totalAmount, lookupId, fundSource);
    }

    @Transactional
    public RefundResponse refundToPgForPaymentMxtnId(PaymentTransaction paymentTransaction, Double refundToBankAmount, String refundType)
            throws Exception {
        RefundResponse response = null;
        String orderId = paymentTransaction.getOrderId();
        int userId = orderIdDAO.getUserIdFromOrderId(orderId);
        Users user = userServiceProxy.getUserByUserId(userId);
        String oneCheckIdentity;
        String email = user.getEmail();
        int creditTransactionSize = 0;
        if (null != (oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email))) {
            List<TransactionSummary> creditTransaction = oneCheckWalletService
                    .findTransactionTransactionType(OneCheckTransactionTypePrefix.CREDIT, orderId, oneCheckIdentity);
            if(null != creditTransaction) {
                creditTransactionSize = creditTransaction.size();
            }
        } else {
            List<WalletTransaction> transactionResult = findTransaction(
                    paymentTransaction.getMerchantTxnId(), TransactionType.DEPOSIT);
            creditTransactionSize = transactionResult.size();
        }

        int debitTransactionListSize = 0;
        if(FCUtil.isNotEmpty(oneCheckIdentity)) {
            List<TransactionSummary> debitTransaction = oneCheckWalletService
                    .findTransactionTransactionType(OneCheckTransactionTypePrefix.DEBIT, orderId, oneCheckIdentity);
            if(null != debitTransaction) {
                debitTransactionListSize = debitTransaction.size();
            }
        } else {
            List<WalletTransaction> debitTransactionList = findTransaction(
                    paymentTransaction.getMerchantTxnId(), TransactionType.WITHDRAWAL);

            debitTransactionListSize = debitTransactionList.size();
        }
        
        if (FCUtil.isNotEmpty(oneCheckIdentity) && creditTransactionSize == 1 && debitTransactionListSize == 0) {
            logger.info("This is a oneCheck user. Order : " + orderId + ". Debit count : " + debitTransactionListSize
                    + ". Credit count: " + creditTransactionSize);
            logger.info("Order : " + orderId + ". This is the case where recharge failed ");
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
            if (userTransactionHistory != null && inService.isFailure(userTransactionHistory.getTransactionStatus())) {
                response = initiateRefundToPg(paymentTransaction, refundToBankAmount, orderId, userId, oneCheckIdentity, refundType);
            }
        } else if (creditTransactionSize == 1 && debitTransactionListSize == 0) {
            List<InRequest> inRequestList = pgRadDao.getMissedRechargeStatus(orderId);
            if (inRequestList == null || inRequestList.isEmpty()) {
                response = initiateRefundToPg(paymentTransaction, refundToBankAmount, orderId, userId, oneCheckIdentity, refundType);
            }
        } else if (creditTransactionSize >= 2 && debitTransactionListSize == 1) {
            List<InRequest> inRequestList = pgRadDao.getMissedRechargeStatus(orderId);
            if (inRequestList == null || inRequestList.isEmpty()) {
                response = initiateRefundToPg(paymentTransaction, refundToBankAmount, orderId, userId, oneCheckIdentity, refundType);
            } else {
                if (inRequestList.size() > 0) {
                    List<InResponse> inResponse = pgRadDao.isRechargeSuccess(orderId);
                    if (inResponse != null && !inResponse.isEmpty()) {
                        String responseCode = inResponse.get(0).getInRespCode();
                        if (responseCode != null && !responseCode.isEmpty()
                                && !AggregatorInterface.RECHARGE_PENDING_AGGREGATOR_RESPONSE.contains(responseCode)) {
                            response = initiateRefundToPg(paymentTransaction, refundToBankAmount, orderId, userId, oneCheckIdentity, refundType);
                            metricsClient.recordEvent("WalletService", "refundToPgForPaymentMxtnId", "Success");
                        } else {
                            response = new RefundResponse();
                            response.setSuccess(false);
                            response.setResponseMessage("Sorry Recharge Status is either Success  or Underprocess");
                            metricsClient.recordEvent("WalletService", "refundToPgForPaymentMxtnId", "Failed");
                        }
                    } else {
                        response = new RefundResponse();
                        response.setSuccess(false);
                        response.setResponseMessage("Sorry Database Table empty for given Order ID ");
                        metricsClient.recordEvent("WalletService", "refundToPgForPaymentMxtnId", "Failed");
                    }
                }
            }
         }else {
            response = new RefundResponse();
            response.setSuccess(false);
            response.setResponseMessage("Sorry Something Went wrong");
            metricsClient.recordEvent("WalletService", "refundToPgForPaymentMxtnId", "Failed");
        }
        return response;
    }

    private String debitFromWalletForPaymentMtxnId(String paymentMtxnId, String orderId, String refundAmount, int userId, String oneCheckId,
                                                   OneCheckWalletService.OneCheckTxnType oneCheckTxnType) throws Exception {
        logger.info("request for debit amount for order id " + orderId);
        WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
        walletDebitRequest.setUserID(userId);
        walletDebitRequest.setTxnAmount(new Amount(refundAmount));
        walletDebitRequest.setMtxnId(paymentMtxnId);
        walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
        walletDebitRequest.setCallerReference(orderId + RechargeConstants.TRANSACTION_TYPE_DEBIT);
        walletDebitRequest.setFundDestination(PaymentConstants.REFUND_TO_BANK);
        String debitTransactionId =null;
        Map<String, Object> params = new HashMap<>();
        if(FCUtil.isEmpty(oneCheckId)) {
            params.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
            params.put(WalletWrapper.WALLET_TYPE, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
            params.put(FCConstants.ORDER_ID, orderId);
            debitTransactionId = this.debit(walletDebitRequest);
            logger.info("Debited FC wallet for debitTransactionId " + debitTransactionId);
            metricsClient.recordEvent("WalletService", "debitFromWalletForPaymentMtxnId", "Success");
        } else {
            
            params.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
            params.put(WalletWrapper.ONE_CHECK_IDENTITY, oneCheckId);
            params.put(WalletWrapper.WALLET_TYPE, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
            params.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, oneCheckTxnType);
            params.put(FCConstants.ORDER_ID, orderId);
            DebitBalanceResponse debitBalanceResponse =null;
            try{
            debitBalanceResponse = oneCheckWalletService.debitBalance(params);
            }catch (OneCheckWalletException ocwe) {
				if (ocwe.getMessage().contains(FCConstants.OCW_INTERNAL_SERVER_EXCEPTION)) {
					logger.info("Caught internal Server Exception " ,ocwe);
					logger.info("Doing status check");

					String mtxnId = walletDebitRequest.getMtxnId();

					boolean attemptRetry = true;
					int retryCount = 3;
					while (attemptRetry && retryCount > 0) {
						TransactionSummary transaction = oneCheckWalletService.findTransactionForStatusCheck(params,
								mtxnId);
						if (transaction != null) {
							debitBalanceResponse = new DebitBalanceResponse();
							debitBalanceResponse.setTransactionId(transaction.getTransactionId());
							debitBalanceResponse.setTransactionTimestamp(transaction.getTimestamp());
							attemptRetry = false;
						}

						if (attemptRetry) {
							// If transaction status is not known, attempting
							// retry with a sleep of 1 sec
							logger.info("Transaction Status not Know Attempting Again");
							try {
								Thread.sleep(1000);
								retryCount--;
							} catch (InterruptedException e) {
							
								retryCount--;
								logger.info("Exception while putting retry thread to sleep:", e);
							}

						}
					}
				}
			}
            if(debitBalanceResponse!=null){
            debitTransactionId = debitBalanceResponse.getTransactionId();
            logger.info("Debited one check wallet: " + debitBalanceResponse.toString());
            }
            metricsClient.recordEvent("WalletService", "debitFromWalletForPaymentMtxnId", "Success");
        }
        PaymentTransaction payTxn = paymentTransactionService.getSuccessfulPaymentTransactionByOrderId(orderId);
        this.publishRevokeAlert(params, payTxn, debitTransactionId);
        return debitTransactionId;
    }

    @Transactional
    public RefundResponse initiateRefundToPg(PaymentTransaction paymentTransaction,
                                             Double refundToBankAmount, String orderId, int userId, String oneCheckId, String refundType) throws Exception {
        RefundResponse response = new RefundResponse();
        logger.info("request for debit amount for order id " + orderId);
        Amount currentBalance = walletWrapper.getTotalBalance(userId);

        /*
         * If Partial refund mode-then edited amount we refund else we take
         * amount from PaymentTransaction object, which is PG amount
         */
        String refundAmount = null;
        if (refundToBankAmount != null) {
            refundAmount = String.valueOf(refundToBankAmount);
        } else {
            refundAmount = String.valueOf(paymentTransaction.getAmount());
        }

        if (currentBalance.getAmount().doubleValue() >= Double.parseDouble(refundAmount)) {
            PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doRefundToBank(paymentTransaction,
                    Double.parseDouble(refundAmount), true, refundType);
            if (paymentRefundBusinessDO.getStatus()) {
                response.setSuccess(true);
                try {
                    String txnId = this.debitFromWalletForPaymentMtxnId(paymentTransaction.getMerchantTxnId(), orderId, refundAmount, userId, oneCheckId, OneCheckWalletService.OneCheckTxnType.DEBIT_REFUND);
                    if (txnId != null) {
                        createPaymentRefundTxnForBankRefund(paymentTransaction, new BigDecimal(refundAmount), txnId, oneCheckId);
                    }
                    response.setResponseMessage("Payment refund via PG is successfull. Bank Message = "
                            + paymentRefundBusinessDO.getMsg());
                } catch (DuplicateRequestException e) {
                    logger.warn(e);
                    response.setResponseMessage("PG refund is successfull, but wallet debit failed. Bank Message = "
                            + paymentRefundBusinessDO.getMsg());

                }
            } else {
                response.setSuccess(false);
                response.setResponseMessage("Payment refund via PG Failed. Bank Message = "
                        + paymentRefundBusinessDO.getMsg());
            }
        } else {
            response.setSuccess(false);
            response.setResponseMessage("Sorry Wallet Amount less then Refund Amount");
        }
        return response;
    }

    private void createPaymentRefundTxnForBankRefund(PaymentTransaction paymentTransaction, BigDecimal refundAmount, String txnId,
            String oneCheckId) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        paymentRefundTransaction.setRefundedAmount(new Amount(refundAmount.negate()));
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setReceivedFromPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setSentToPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setPaymentTransactionId(paymentTransaction.getPaymentTxnId());
        paymentRefundTransaction.setRawPGRequest("Wallet debit request for refund to Source PG");
        if (oneCheckId == null) {
            paymentRefundTransaction.setRawPGResponse("Wallet debit response for refund to Source PG");
            paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
        } else {
            paymentRefundTransaction.setRawPGResponse("Wallet Debit TxnID : " + txnId);
            paymentRefundTransaction.setRefundTo(PaymentConstants.ONE_CHECK_WALLET_CODE);
        }
        paymentTransactionService.create(paymentRefundTransaction);
    }

    public List<WalletTransaction> getWalletTxn(String orderId, String fundSource) {
        return walletRangeReadDAO.getWalletTxn(orderId, fundSource);
    }

    public WalletItems getWalletItems(Integer walletTransactionId) {
        return walletItemsDAO.getWalletItems(walletTransactionId);
    }

    public List<WalletTransaction> findTransactionTypeByOrderId(String orderId) {
        return walletRangeReadDAO.findTransactionTypeByOrderId(orderId);
    }

    public List<WalletTransaction> getAddCashTransactionByUserId(long userId) {
        return walletRangeReadDAO.getAddCashTransactionByUserId(userId);
    }

    public double getCumulativeAmountForDeposit(Long userId, Timestamp startTime, Timestamp endTime,
            List<FundSource> fundSources) {
        return walletRangeReadDAO.getCumulativeAmountForDeposit(userId, startTime, endTime, fundSources);
    }

    public com.freecharge.wallet.service.Wallet findBalanceByUserID(long userID) {
        return walletRangeReadDAO.findBalanceByUserID(userID);
    }

    public long getTransactionCount(Long userId, Timestamp startTime, Timestamp endTime, List<FundSource> fundSources,
            List<TransactionType> transactionTypes) {
        return walletRangeReadDAO.getTransactionCount(userId, startTime, endTime, fundSources, transactionTypes);
    }

    public List<WalletTransaction> getWalletTxnByDateRange(Date start, Date end) {
        return walletRangeReadDAO.getWalletTxnByDateRange(start, end);
    }

    /**
     * This function adds freecharge credits to a users wallet.
     * 
     * @param userId
     * @param amount
     * @param orderId
     *            This need not be FreeCharge order ID but any string will do.
     *            Also, this needn't be unique.
     * @param callerReference
     *            This should be unique across all transactions.
     * @param fundSource
     * @param itemId
     *            If you do not know what to pass here; pass in 0L value here.
     * @param metaData
     *            This could be any string upto 256char length. In case of
     *            {@link FundSource} being CASHBACK this appears as description
     *            in user's wallet page.
     * @return
     */
    public Boolean doWalletCashBack(Long userId, Double amount, String orderId, String callerReference,
            String fundSource, Long itemId, String metaData) {
        Map<String,Object> resultMap;
        resultMap = doWalletCashBackWithResponse(userId, amount, orderId, callerReference,
                 fundSource, itemId, metaData);
        if(resultMap!=null)
        {
            if(resultMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public Map<String,Object> doWalletCashBackWithResponse(Long userId, Double amount, String orderId, String callerReference,
                                                           String fundSource, Long itemId, String metaData) {
        Map<String,Object> resultMap = new HashMap<>();
        FundSource fs = FundSource.valueOf(fundSource);
        logger.info(String.format("Processing wallet cashback for user: %d amount: %f and callerReference: %s and "
                + "orderId: %s", userId, amount, callerReference, orderId));
        if (userId == null || amount == null || FCStringUtils.isBlank(callerReference)
                || FCStringUtils.isBlank(fundSource)) {
            resultMap.put(FCConstants.STATUS, FCConstants.FAILURE);
            resultMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE,"One or more of params are null or blank");
            throw new IllegalArgumentException("One or more of params are null or blank");
        }
        WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
        walletCreditRequest.setUserID(userId);

        walletCreditRequest.setMtxnId(orderId);
        walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
        walletCreditRequest.setCallerReference(callerReference);

        walletCreditRequest.setFundSource(fs.toString());
        walletCreditRequest.setTxnAmount(new Amount(amount));
        walletCreditRequest.setMetadata(metaData);

        Map<String,Object> creditResponseMap = this.creditWithResponse(walletCreditRequest);
        if(creditResponseMap!= null) {
            if (creditResponseMap.get(FCConstants.STATUS).equals(FCConstants.SUCCESS)) {
                Long fkWalletTransactionId = Long.parseLong(creditResponseMap.get(PRIMARY_KEY_STRING).toString());
                this.insertWalletItem(fkWalletTransactionId, itemId, fundSource);
                logger.info(String.format("WalletCashbackCredited for callerReference: %s and user: %d and fundSource: %s",
                        callerReference, userId, fundSource));
                resultMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            } else {
                logger.info(String.format("WalletCashbackFailed for callerReference: %s and user: %d and fundSource: %s",
                        callerReference, userId, fundSource));
                resultMap.put(FCConstants.STATUS, FCConstants.FAILURE);
                resultMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE, creditResponseMap.get(FCConstants.FC_GENERAL_ERROR_MESSAGE));
            }
        }
        return resultMap;
    }
    
    public List<WalletTransaction> getAddCashTransactionByUserIdBetweenDays(long userId, Timestamp fromDateTime,
            Timestamp toDateTime) {
        return walletRangeReadDAO.getAddCashTransactionByUserIdBetweenDays(userId, fromDateTime, toDateTime);
    }
    
    public Double findWalletBalanceByOrderId(String orderId) {
    	Integer userId = orderIdDAO.getUserIdFromOrderId(orderId);
    	if (userId != null) {
    		Amount amount = walletWrapper.getTotalBalance(userId);
    		if (amount != null) {
    			return amount.getAmount().doubleValue();
    		}
    	}
    	return null;
    }

    /**
     * Transfers FC credits to one check wallet
     * @param email
     * @return
     * @throws DuplicateRequestException
     */
    /*public Map<String, Object> fcCreditsToFcWallet(String email) throws DuplicateRequestException {
        IUms ums = new Ums();
        ums.init(umsConfig);
        MigrationStatus fcWalletMigrationStatus = ums.getFcWalletMigrationStatus(email);
        if(null == fcWalletMigrationStatus || !"UPGRADE_COMPLETED".equals(fcWalletMigrationStatus.getMigrationStatus())) {
            Map<String, Object> responseMap = new HashMap<>();
            responseMap.put(FCConstants.STATUS, FCConstants.FAILURE);
            responseMap.put(FCConstants.FC_GENERAL_ERROR_MESSAGE, "Wallet not fully migrated");
            return responseMap;
        }
        String fcWalletId = fcWalletMigrationStatus.getFcWalletId();
        User user = ums.getUser(email);
        int fcUserId = user.getUserId();
        return fcCreditsToFcWallet(fcWalletId, fcUserId);
    }*/
    
    public com.freecharge.wallet.service.Wallet findAndLockBalanceByUserID(long userID) {
        return walletDAO.findAndLockBalanceByUserID(userID);
    }
    
    /**
     * Transfers FC credits to one check wallet
     * @param fcWalletId
     * @param fcUserId
     * @return
     * @throws DuplicateRequestException
     */
    public Map<String, Object> fcCreditsToFcWallet(String fcWalletId, int fcUserId) throws DuplicateRequestException {
    	return fcCreditsToFcWallet ( fcWalletId, fcUserId, false);
    }

    /**
     * Transfers FC credits to one check wallet
     * @param fcWalletId
     * @param fcUserId
     * @return
     * @throws DuplicateRequestException
     */
    @Transactional
    public Map<String, Object> fcCreditsToFcWallet(String fcWalletId, int fcUserId, boolean appendDate) throws DuplicateRequestException {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put(FCConstants.IS_PARKED, false);
        Wallet walletBalance = findAndLockBalanceByUserID(fcUserId);
        String email = userServiceProxy.getEmailByUserId(fcUserId);
        if(null != walletBalance) {
            double currentBalance = walletBalance.getCurrentBalance();
            if(currentBalance > 0) {
                WalletDebitRequest walletDebitRequest = getWalletDebitRequestForWalletMigration(walletBalance, FundDestination.ONE_CHECK_WALLET, fcUserId, email);
                debit(walletDebitRequest , appendDate);
                logger.info("Debited FC wallet for wallet migration for fcUserId: " + fcUserId);
                Map<String, Object> params = new HashMap<>();
                WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
                walletCreditRequest.setTxnAmount(new Amount(walletBalance.getCurrentBalance()));
                String mtxnId = walletDebitRequest.getMtxnId();
                walletCreditRequest.setCallerReference(mtxnId + "_" + PaymentConstants.OCW_MIGRATION);
                walletCreditRequest.setUserID(fcUserId);
                walletCreditRequest.setMtxnId(mtxnId);
                walletCreditRequest.setAppendSufix("");
                if(appendDate)
                {
                	Date date = new Date();
                	walletCreditRequest.setAppendSufix("_"+date.getYear()+"_"+date.getMonth()+"_"+date.getDay());
                }
                params.put(WalletWrapper.WALLET_CREDIT_REQUEST, walletCreditRequest);
                params.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.CREDIT_ONE_CHECK_MIGRATION);
                params.put(FCConstants.EMAIL, email);
                params.put(WalletWrapper.ONE_CHECK_IDENTITY, fcWalletId);
                params.put(FCConstants.ORDER_ID, mtxnId);
                fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                        PaymentConstants.OCW_MIGRATION);
                try {
                    CreditGeneralBalanceToUserResponse creditBalanceResponse = oneCheckWalletService.creditGeneralBalanceToUser(params);
                    logger.info("Credited One Check wallet for wallet migration for fcUserId: " + fcUserId);
                    logger.info("CreditGeneralBalanceToUserResponse: " + creditBalanceResponse.toString());
                    responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                    responseMap.put(MONEY_TRANSFERRED, currentBalance);
                    responseMap.put(FCConstants.REFERENCE_ID, creditBalanceResponse.getTransactionId());
                    responseMap.put(FCConstants.IS_PARKED, creditBalanceResponse.isParked());
                    fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                            PaymentConstants.OCW_MIGRATION_SUCCESS);
                    metricsClient.recordEvent("WalletService", "fcCreditsToFcWallet", "Success");   
                } catch (RuntimeException e) {
                    logger.error("Exception while crediting one check wallet for wallet migration for fcUserId: " + fcUserId);
                    walletCreditRequest.setFundSource(FundSource.WALLET_MIGRATN_FAIL.toString());
                    walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
                    walletCreditRequest.setCallerReference(mtxnId + "_" + PaymentConstants.OCW_MIGRATION_FAILED);
                    credit(walletCreditRequest);
                    fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                            PaymentConstants.OCW_MIGRATION_FAILED);
                    metricsClient.recordEvent("WalletService", "fcCreditsToFcWallet", "Failed"); 
                    throw new OneCheckWalletException(e);
                }
            } else {
                fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                        PaymentConstants.OCW_MIGRATION_SUCCESS);
                logger.info("No FC credits for user: " + fcUserId);
                responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
                responseMap.put(MONEY_TRANSFERRED, currentBalance);
                metricsClient.recordEvent("WalletService", "fcCreditsToFcWallet", "Success"); 
            }
        } else {
            fcWalletUtil.storeWalletMigrationStatusInDynamo(email, fcWalletId,
                    PaymentConstants.OCW_MIGRATION_SUCCESS);
            logger.info("No wallet_status for user: " + fcUserId);
            responseMap.put(FCConstants.STATUS, FCConstants.SUCCESS);
            responseMap.put(MONEY_TRANSFERRED, 0d);
            metricsClient.recordEvent("WalletService", "fcCreditsToFcWallet", "Success");
        }
        return responseMap;
    }
    
    public WalletDebitRequest getWalletDebitRequestForWalletMigration(Wallet walletBalance,
            FundDestination fundDestination, int userId, String uniqueId) {
    	return getWalletDebitRequestForWalletMigration(walletBalance, fundDestination, userId, uniqueId, false);
    }

    public WalletDebitRequest getWalletDebitRequestForWalletMigration(Wallet walletBalance,
                                                                       FundDestination fundDestination, int userId, String uniqueId, boolean appendDate) {
        WalletDebitRequest walletDebitRequest =  new WalletDebitRequest();
        walletDebitRequest.setUserID(userId);
        walletDebitRequest.setTxnAmount(new Amount(walletBalance.getCurrentBalance()));
        walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
        walletDebitRequest.setMtxnId(uniqueId);
        walletDebitRequest.setCallerReference(uniqueId + "_" + PaymentConstants.OCW_MIGRATION);
        walletDebitRequest.setFundDestination(fundDestination.toString());
        walletDebitRequest.setMetadata("");
        return walletDebitRequest;
    }
    
    public void publishRevokeAlert(Map<String, Object> debitBalanceMap, PaymentTransaction paymentTransaction, String debitTransactionId) {
        logger.info("publishing Revoke Alert");
        try {
            PaymentAlertBean alertBean = new PaymentAlertBean();
            WalletDebitRequest walletDebitRequest = (WalletDebitRequest) debitBalanceMap
                    .get(WalletWrapper.WALLET_DEBIT_REQUEST);
            String orderId = (String) debitBalanceMap.get(FCConstants.ORDER_ID);
            try {
                alertBean.setUserId(walletDebitRequest.getUserID());
                alertBean.setOrderId(orderId);
                alertBean.setMtxnId(walletDebitRequest.getMtxnId());
                alertBean.setAmount(walletDebitRequest.getTxnAmount().getAmount().doubleValue());
                alertBean.setTransactionId(debitTransactionId);
                alertBean.setPaymentGateway(PaymentConstants.ONE_CHECK_WALLET_CODE);
                alertBean.setChannel(FCUtil.getChannelFromOrderId(orderId));
                alertBean.setMessageType(PaymentStatus.WALLET_REVOKE.getStatus());
                //alertBean.setTransactionId(paymentTransaction.getTransactionId());
                PaymentPlan paymentPlan = new PaymentPlan();
                paymentPlan.setOcw(walletDebitRequest.getTxnAmount().getAmount().doubleValue());
                alertBean.setPaymentPlan(paymentPlan);
                alertBean.setPaymentType(PaymentType.PaymentTypeName.ONECHECK_WALLET.toString());
                alertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()).longValue());
                CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(paymentTransaction.getOrderId());
                
                convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, alertBean);
                
                ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
                alertBean.setProduct(primaryProduct.name());
                
                UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
                String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
                String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
                String operator = userTransactionHistory.getServiceProvider();
                alertBean.setImsId(oneCheckIdentity);
                //Set GST
                gstService.populateGST(alertBean, primaryProduct,operator);
            } catch (Exception e) {
                logger.error("Error while publishing the revoke alert method to SNS for order id " + orderId, e);
            } finally {
                alertBean.setReconMetaData(new ReconMetaData(walletDebitRequest.getMtxnId(), debitTransactionId,
                    "WALLET", "WALLET", null));
                logger.info("Publishing revoke alert SNS");
                paymentSNSService.publish(orderId,
                        "{\"message\" : " + PaymentAlertBean.getJsonString(alertBean) + "}");
            }
        } catch (Exception e) {
            logger.error("Error thrown inside publishRevokeAlert method");
        }

    }
    
    public void notifyWalletPayment(PaymentTransaction paymentTransaction, String pg, boolean isPaymentSuccess) {
    	
    	PaymentAlertBean alertBean = new PaymentAlertBean();
    	String jsonAlert = null;
        try {
            logger.info("Wallet paymentSNSService publish triggered for orderId : " + paymentTransaction.getOrderId());
            alertBean.setMtxnId(paymentTransaction.getMerchantTxnId());
            alertBean.setChannel(FCUtil.getChannelFromOrderId(paymentTransaction.getOrderId()));
            alertBean.setMessageType(isPaymentSuccess? PaymentStatus.PAYMENT_SUCCESS.getStatus() : PaymentStatus.PAYMENT_FAILURE.getStatus());
            alertBean.setOrderId(paymentTransaction.getOrderId());
            alertBean.setTransactionId(paymentTransaction.getTransactionId());
            
            String paymentTypeName;
            com.freecharge.payment.dos.entity.PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlan(paymentTransaction.getOrderId());
            alertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
            alertBean.setPaymentGateway(pg);
            
            PaymentPlan paymentPlan = new PaymentPlan();
            if (fcPaymentPlan.getTotalAmount().getAmount().doubleValue() != paymentTransaction.getAmount()) {
        		paymentPlan.setPg(fcPaymentPlan.getTotalAmount().getAmount().doubleValue() - paymentTransaction.getAmount());
        		paymentPlan.setOcw(paymentTransaction.getAmount());
        		paymentTypeName = "Partial Payment";
            } else {
            	paymentPlan.setOcw(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
            	paymentTypeName = PaymentType.PaymentTypeName.ONECHECK_WALLET.toString();
            }
            
            alertBean.setPaymentPlan(paymentPlan);
            alertBean.setPaymentType(paymentTypeName);

            CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(paymentTransaction.getOrderId());
            
            convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, alertBean);
            
            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
            alertBean.setProduct(primaryProduct.name());
            alertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()).longValue());
            
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
            String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
            String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
            String operator = userTransactionHistory.getServiceProvider();
            alertBean.setImsId(oneCheckIdentity);
            
            //Set GST
            gstService.populateGST(alertBean, primaryProduct,operator);
        } catch (Exception e) {
            logger.error("Exception while creating wallet alert bean : " + paymentTransaction.getOrderId(), e);
        } finally {
        	alertBean.setReconMetaData(new ReconMetaData(paymentTransaction.getMerchantTxnId(), paymentTransaction.getTransactionId(),
            		"WALLET", "WALLET", null));
        	try {
        		jsonAlert = PaymentAlertBean.getJsonString(alertBean);
            	logger.info("Wallet Alert :"+ jsonAlert);
        		paymentSNSService.publish(paymentTransaction.getOrderId(), "{\"message\" : " + PaymentAlertBean.getJsonString(alertBean) + "}");
        	} catch (Exception e) {
				logger.error("Exception while publishing notification for alert bean:" + paymentTransaction.getOrderId(), e);
			}
        }
    }
    
}


