package com.freecharge.wallet;


import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.wallet.service.CreditsEncashDetails;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditsEncashDetailsService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private CreditsEncashDetailsDAO creditsEncashDetailsDAO;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    public int insertBankDetails(CreditsEncashDetails creditsEncashDetails) {
        return creditsEncashDetailsDAO.insert(creditsEncashDetails);
    }
    
    public boolean updateBankDetails(CreditsEncashDetails creditsEncashDetails) {
        return creditsEncashDetailsDAO.updateBankDetails(creditsEncashDetails);
    }
    
    public List<Map<String, Object>> getCreditEncashDetails(String emailId) {
    	logger.info("Bank details for encashing credits, user email :" + emailId);
    	Users users = userServiceProxy.getUserByEmailId(emailId);
    	if (users == null || users.getUserId() == null) {
    		return null;
    	}
    	logger.info("Bank details for encashing credits, user email :" + emailId +" ,"+"fcuserId :" + users.getUserId());
    	return creditsEncashDetailsDAO.getCreditEncashDetails(users.getUserId());
    }
    
    public boolean isCreditAlreadyEncashed(String emailId) {
    	Users users = userServiceProxy.getUserByEmailId(emailId);
    	if (users == null || users.getUserId() == null) {
    		return false;
    	}
    	return creditsEncashDetailsDAO.isCreditAlreadyEncashed(users.getUserId());
    }
    
    public boolean updateTransactionStatus(CreditsEncashDetails creditsEncashDetails) {
    	return creditsEncashDetailsDAO.updateTransactionStatus(creditsEncashDetails);
    }
    
    public List<Map<String, Object>> getCreditEncashDetailsNotSuccess() {
    	return creditsEncashDetailsDAO.getCreditEncashDetailsNotSuccess();
    }
    
    public List<Map<String, Object>> getEncashBanDataForEnteredEmail(String emailId) {
    	logger.info("Bank details for encashing credits, user entered email :" + emailId);
    	return creditsEncashDetailsDAO.getEncashBanDataForEnteredEmail(emailId);
    }
}
