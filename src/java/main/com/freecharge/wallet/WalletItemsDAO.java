package com.freecharge.wallet;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.mappers.WalletItemsRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.wallet.service.WalletItems;
@Service
public class WalletItemsDAO {

    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertWalletItems;
    private static Logger logger = LoggingFactory.getLogger(WalletItemsDAO.class);
   
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertWalletItems = new SimpleJdbcInsert(dataSource).withTableName("wallet_items").usingGeneratedKeyColumns("wallet_items_id", "n_last_updated", "n_created");
    }

    public long insert(long fkWalletTransactionId, long fkItemId, String entityId) {
    	
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("fk_wallet_transaction_id", fkWalletTransactionId);
        insertParams.put("fk_item_id", fkItemId);
        insertParams.put("entity_id", entityId);

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        insertParams.put("added_on", timestamp);

        Number primaryKey = insertWalletItems.executeAndReturnKey(insertParams);

        return primaryKey.longValue();
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(WalletItemsDAO.class.getSimpleName());
    }
    
    public WalletItems getwalletTransationIdByfreefundCouponId(long freefundCouponId) {
    	logger.debug("request goes inside getwalletTransationIdByfreefundCouponId ");
    	try {
			String queryString = "select * from wallet_items where fk_item_id = :fkItemId";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("fkItemId", freefundCouponId);
			List<WalletItems> list = this.jt.query(queryString,
					paramMap, new WalletItemsRowMapper());
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			
		} catch (RuntimeException re) {
			logger.error("getwalletTransactionid ", re);
			throw re;
          }
		return  null; 
    }
    
    //For admin
    public WalletItems getWalletItems(Integer walletTransactionId) {
    	try {
    		String queryString = "select * from wallet_items where fk_wallet_transaction_id = :fkWalletTxnId";
    		Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("fkWalletTxnId", walletTransactionId);
			
			List<WalletItems> list = this.jt.query(queryString, paramMap, new WalletItemsRowMapper());
			if (list != null && list.size() > 0) {
				return list.get(0);
			}	
    	} catch (RuntimeException re) {
			logger.error("getwalletTransactionid ", re);
			throw re;
        }
    return null;
   }
}
