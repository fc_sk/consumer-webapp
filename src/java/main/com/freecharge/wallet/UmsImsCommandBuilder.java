package com.freecharge.wallet;

import java.util.concurrent.Callable;

import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.common.timeout.ExternalClientCallCommand;
import com.freecharge.common.timeout.TimeoutKey;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.snapdeal.ims.client.ILoginUserServiceClient;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.client.impl.UserMigrationServiceClientImpl;
import com.snapdeal.ims.request.GetTransferTokenRequest;
import com.snapdeal.ims.request.GetUserByEmailRequest;
import com.snapdeal.ims.request.GetUserByTokenRequest;
import com.snapdeal.ims.request.LoginWithTransferTokenRequest;
import com.snapdeal.ims.request.UserUpgradeByEmailRequest;
import com.snapdeal.ims.response.GetTransferTokenResponse;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.ims.response.LoginUserResponse;
import com.snapdeal.ims.response.UserUpgradationResponse;

public final class UmsImsCommandBuilder {

    public static ExternalClientCallCommand<GetUserResponse> getGetUserByTokenCommand(
            final IUserServiceClient userClient, final GetUserByTokenRequest request) {
        return new ExternalClientCallCommand<GetUserResponse>("OnecheckWallet", "getInterimUserDetailsByToken",
                new Callable<GetUserResponse>() {
                    @Override
                    public GetUserResponse call() throws Exception {
                        return userClient.getUserByToken(request);
                    }
                }, TimeoutKey.UMS_GET_USER_BY_TOKEN);
    }

    public static ExternalClientCallCommand<GetUserResponse> getGetUserByEmailCommand(
            final IUserServiceClient userClient, final GetUserByEmailRequest request) {
        return new ExternalClientCallCommand<GetUserResponse>("OnecheckWallet", "getOneCheckIdIfOneCheckWalletEnabled",
                new Callable<GetUserResponse>() {
                    @Override
                    public GetUserResponse call() throws Exception {
                        return userClient.getUserByEmail(request);
                    }
                }, TimeoutKey.UMS_GET_USER_BY_EMAIL);
    }

    public static ExternalClientCallCommand<UserUpgradationResponse> getGetUserUpgradeStatusCommand(
            final UserMigrationServiceClientImpl userMigrationService, final UserUpgradeByEmailRequest request) {
        return new ExternalClientCallCommand<UserUpgradationResponse>("OnecheckWallet", "isOneCheckCustomer",
                new Callable<UserUpgradationResponse>() {
                    @Override
                    public UserUpgradationResponse call() throws Exception {
                        return userMigrationService.userUpgradeStatus(request);
                    }
                }, TimeoutKey.UMS_GET_UPGRADE_STATUS);
    }

    public static ExternalClientCallCommand<LoginUserResponse> getGetLoginuserWithTransferCommand(
            final ILoginUserServiceClient loginUserService, final LoginWithTransferTokenRequest request) {
        return new ExternalClientCallCommand<LoginUserResponse>("OnecheckWallet", "getUserByTransferToken",
                new Callable<LoginUserResponse>() {
                    @Override
                    public LoginUserResponse call() throws Exception {
                        return loginUserService.loginUserWithTransferToken(request);
                    }
                }, TimeoutKey.UMS_GET_USER_BY_TRANSFER_TOKEN);
    }

    public static ExternalClientCallCommand<GetTransferTokenResponse> getGetTransferTokenCommand(
            final ILoginUserServiceClient loginUserService, final GetTransferTokenRequest request) {
        return new ExternalClientCallCommand<GetTransferTokenResponse>("OnecheckWallet", "getUserDetailsByToken",
                new Callable<GetTransferTokenResponse>() {
                    @Override
                    public GetTransferTokenResponse call() throws Exception {
                        return loginUserService.getTransferToken(request);
                    }
                }, TimeoutKey.UMS_GET_TRANSFER_TOKEN);
    }

    public static ExternalClientCallCommand<MigrationStatus> getGetFcWalletMigrationStatusCommand(final UserServiceProxy ims,
            final String email,final String token) {
        return new ExternalClientCallCommand<MigrationStatus>("OnecheckWallet", "getFcWalletMigrationStatus",
                new Callable<MigrationStatus>() {
                    @Override
                    public MigrationStatus call() throws Exception {
                        return ims.getUserMigrationStatusByEmail(email, token);
                    }
                }, TimeoutKey.UMS_GET_FC_WALLET_MIGRATION_STATUS);
    }

}
