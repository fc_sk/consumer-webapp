package com.freecharge.wallet;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.mappers.WalletServiceRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.WalletTransactionRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCUtil;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletTransaction;
import com.google.common.base.Preconditions;

public class WalletDAO {

    private NamedParameterJdbcTemplate jt;
    private SimpleJdbcInsert insertWalletStatus;
    private SimpleJdbcInsert insertWalletTxn;
    protected final Logger logger = LoggingFactory.getLogger(getClass().getName());
    
    private final String WALLET_TRANSACTION_COLUMNS_STRING = "WALLET_TRANSACTION_ID, fk_wallet_id, transaction_date, "+
            "caller_refrence, TXN_AMOUNT, transaction_type, fund_source, fund_destination, metadata, order_id, " +
            "NEW_BALANCE, FK_USER_ID";
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
        insertWalletStatus = new SimpleJdbcInsert(dataSource).withTableName("wallet_status").usingGeneratedKeyColumns("wallet_status_id", "n_last_updated", "n_created");
        insertWalletTxn = new SimpleJdbcInsert(dataSource).withTableName("wallet_txn").usingGeneratedKeyColumns("wallet_transaction_id");
    }

    public com.freecharge.wallet.service.Wallet findAndLockBalanceByUserID(long userID) {
        String sql = "SELECT * FROM wallet_status WHERE fk_user_id = :fk_user_id for update";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("fk_user_id", userID);
        List<com.freecharge.wallet.service.Wallet> rows = jt.query(sql, params, new WalletServiceRowMapper());
        if (rows.size() == 0) {
            return null;
        }
        return rows.get(0);
    }

    public long insertStatus(long userID, Amount amount) {
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("fk_user_id", userID);
        insertParams.put("BALANCE", amount.getAmount());

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        insertParams.put("created_ts", timestamp);
        insertParams.put("updated_ts", timestamp);

        Number primaryKey = insertWalletStatus.executeAndReturnKey(insertParams);

        return primaryKey.longValue();
    }
 
    public void updateStatus(long userID, Amount newBalance, Amount balanceLock) {
        String updateSQL = "UPDATE wallet_status SET BALANCE = :balance, updated_ts = NOW() " +
        		"WHERE fk_user_id = :fk_user_id";
        Map<String, Object> updateParams = new HashMap<String, Object>();
        updateParams.put("fk_user_id", userID);
        updateParams.put("balance", newBalance.getAmount());

        Preconditions.checkState(jt.update(updateSQL, updateParams) == 1, "Attempt to UPDATE wallet status failed for userID=" + userID);
    }

    /**
     * insert debit and credit into table only when there is no existing debit
     * or credit record in the database for that order id
     * _
     * @param userID
     * @param orderID
     * @param txnAmount
     * @param isDebit
     * @param oldBalance
     * @param newBalance
     * @return
     */
    public String addCreditTransaction(String walletId, WalletCreditRequest walletCreditRequest, Amount runningBalance, Amount oldBalance) {
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("fk_wallet_id", walletId);
        insertParams.put("fk_user_id", walletCreditRequest.getUserID());
        insertParams.put("caller_refrence", walletCreditRequest.getCallerReference());
        insertParams.put("TXN_AMOUNT", walletCreditRequest.getTxnAmount().getAmount());
        insertParams.put("transaction_type", walletCreditRequest.getTransactionType());
        insertParams.put("fund_source", walletCreditRequest.getFundSource());
        insertParams.put("fund_destination", walletCreditRequest.getFundDestination());
        insertParams.put("metadata", walletCreditRequest.getMetadata());
        insertParams.put("order_id", walletCreditRequest.getMtxnId());
        insertParams.put("new_balance", runningBalance.getAmount());
        insertParams.put("old_balance", oldBalance.getAmount());
        insertParams.put("transaction_date", new Timestamp(System.currentTimeMillis()));
        insertParams.put("created_ts", new Timestamp(System.currentTimeMillis()));
        insertParams.put("is_debit", false);
        Number primaryKey = insertWalletTxn.executeAndReturnKey(insertParams);
       // Number primaryKey = this.jt.update(insertSql, insertParams);
        
        if(primaryKey.intValue()==0) {
            throw new IllegalStateException(
                    "try to attempt transaction more then once for order id " + walletCreditRequest.getMtxnId() + " transaction type " + walletCreditRequest.getTransactionType());
        }

        return primaryKey.toString();
    }

    
    public String addDebitTransactionToWallet(int walletId, WalletDebitRequest walletDebitRequest, Amount runningBalance, Amount oldBalance) {
        String insertSql = "INSERT INTO wallet_txn " +
                "(fk_wallet_id, fk_user_id, caller_refrence, TXN_AMOUNT, " +
                "transaction_type, fund_source, fund_destination, metadata, " +
                "order_id, new_balance, old_balance, " +
                "transaction_date, is_debit, created_ts) " +
                "values (:fk_wallet_id, :fk_user_id, :caller_refrence, :TXN_AMOUNT, " +
                ":transaction_type, :fund_source, :fund_destination, :metadata, " +
                ":order_id, :new_balance, :old_balance, :transaction_date, :is_debit, :created_ts)";
        
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("fk_wallet_id", walletId);
        insertParams.put("fk_user_id", walletDebitRequest.getUserID());
        insertParams.put("caller_refrence", walletDebitRequest.getCallerReference());
        insertParams.put("TXN_AMOUNT", walletDebitRequest.getTxnAmount().getAmount());
        insertParams.put("transaction_type", walletDebitRequest.getTransactionType());
        insertParams.put("fund_source", walletDebitRequest.getFundSource());
        insertParams.put("fund_destination", walletDebitRequest.getFundDestination());
        insertParams.put("metadata", walletDebitRequest.getMetadata());
        insertParams.put("order_id", walletDebitRequest.getMtxnId());
        insertParams.put("new_balance", runningBalance.getAmount());
        insertParams.put("old_balance", oldBalance.getAmount());
        insertParams.put("transaction_date", new Timestamp(System.currentTimeMillis()));
        insertParams.put("created_ts", new Timestamp(System.currentTimeMillis()));
        insertParams.put("is_debit", true);

        Number primaryKey = this.jt.update(insertSql, insertParams);
        
        if(primaryKey.intValue()==0) {
            throw new IllegalStateException(
                    "try to attempt transaction more then once for order id " + walletDebitRequest.getMtxnId() + " transaction type " + walletDebitRequest.getTransactionType());
        }

        return primaryKey.toString();
    }

    public List<WalletTransaction> walletTxnForCallerReference(String callerReference) {

        String sql = "(select " + WALLET_TRANSACTION_COLUMNS_STRING + " from wallet_transaction where caller_refrence = :callerRef) UNION (select "+ WALLET_TRANSACTION_COLUMNS_STRING + " from wallet_txn where caller_refrence = :callerRef)";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("callerRef", callerReference);

        return jt.query(sql, params, new WalletTransactionRowMapper());
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(WalletDAO.class.getSimpleName());
    }
    
    public List<WalletTransaction> findTransaction(String merchantOrderId, TransactionType transactionType) {
        Preconditions.checkArgument(transactionType != null, "Transaction type cannot be null");

        String sql = "select " + WALLET_TRANSACTION_COLUMNS_STRING + " from wallet_transaction where order_id = :orderId";
        String sqlNew = "select " + WALLET_TRANSACTION_COLUMNS_STRING + " from wallet_txn where order_id = :orderId";
        String txnTypeSql = "";
        switch (transactionType) {
        case DEPOSIT:
        	txnTypeSql = " and transaction_type = 'deposit'";
            break;
        case WITHDRAWAL:
        	txnTypeSql = " and transaction_type = 'withdrawal'";
            break;
        default:
            break;
        }

        // Also search in the new table.
        sql += txnTypeSql;
        sqlNew += txnTypeSql;
        String finalSql = sql + " UNION " + sqlNew;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orderId", merchantOrderId);
        return jt.query(finalSql, params, new WalletTransactionRowMapper());
    }

    public List<WalletTransaction> findFailedWalletMigrationTxn(long userId) {
        List<WalletTransaction> walletTransactions = new ArrayList<>();
        String sql = "select * from wallet_txn  where fk_user_id = :userId and fund_source = 'WALLET_MIGRATION_FAILED'";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        return jt.query(sql, params, new WalletTransactionRowMapper());
    }
}
