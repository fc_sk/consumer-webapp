package com.freecharge.wallet.service;

public class WalletLimitResponse {
    private Wallet.LimitStatus limitStatus;
    private String descriptionString;
    
    public String getDescriptionString() {
        return descriptionString;
    }
    
    public void setDescriptionString(String descriptionString) {
        this.descriptionString = descriptionString;
    }
    
    public Wallet.LimitStatus getLimitStatus() {
        return limitStatus;
    }
    
    public void setLimitStatus(Wallet.LimitStatus limitStatus) {
        this.limitStatus = limitStatus;
    }
}
