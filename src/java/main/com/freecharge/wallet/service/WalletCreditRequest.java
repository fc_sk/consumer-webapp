package com.freecharge.wallet.service;

import com.freecharge.common.util.Amount;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

public class WalletCreditRequest {

    private long userID;
    private Amount txnAmount;
    private String mtxnId;
    private String transactionType;
    private String callerReference;
    private String fundSource;
    private String fundDestination;
    private String appendSufix;

	public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public Amount getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Amount txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCallerReference() {
        return callerReference;
    }

    public void setCallerReference(String callerReference) {
        this.callerReference = callerReference;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getFundDestination() {
        return fundDestination;
    }

    public void setFundDestination(String fundDestination) {
        this.fundDestination = fundDestination;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    private String metadata;

	public Map<String, Object> toMap() {
        Field[] fields = getClass().getDeclaredFields();
        Map<String, Object> result = new LinkedHashMap<>();
        try {
            for (Field each : fields) {
                each.setAccessible(true);
                Object value = each.get(this);
                if (null != value) {
                    result.put(each.getName(), value.toString());
                }
            }
            return result;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * @return the appendSufix
	 */
	public String getAppendSufix() {
		return appendSufix;
	}

	/**
	 * @param appendSufix the appendSufix to set
	 */
	public void setAppendSufix(String appendSufix) {
		this.appendSufix = appendSufix;
	}
}
