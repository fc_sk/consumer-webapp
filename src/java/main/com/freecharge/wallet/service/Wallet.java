package com.freecharge.wallet.service;

import java.util.Date;

public class Wallet {
    public static enum LimitStatus {
        Success, Failure
    }
    
    public static enum TransactionType {
        DEPOSIT, WITHDRAWAL, ANY
    }
    
    public static enum OneCheckTransactionTypePrefix {
        CREDIT, DEBIT
    }

    private int walletStatusId;
    private int fkUserId;
    private double currentBalance;
    private Date createdOn;
    private Date updatedOn;

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public int getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(int fkUserId) {
        this.fkUserId = fkUserId;
    }

    public int getWalletStatusId() {
        return walletStatusId;
    }

    public void setWalletStatusId(int walletStatusId) {
        this.walletStatusId = walletStatusId;
    }

    public static enum FundSource {
        RECHARGE_FUND, REFUND, CASHBACK, MISSSED_PAYMENT_FUND, CASH_FUND, REDEEM, ARREWARD, EXCESSCREDIT, CS_CASHBACK,
        TVC_CAMPAIGN,REWARD_SERVICE, HCOUPON_REFUND, REWARDS_CASHBACK, WALLET_MIGRATN_FAIL;

        public static String getFundSourceValue(String val) {
            try {
                return FundSource.valueOf(val).getDescriptionString();
            } catch (Exception e) {
                return "Payment received";
            }
        }
        
        public String getDescriptionString() {
            switch (this) {
            case MISSSED_PAYMENT_FUND:
            case RECHARGE_FUND:
                return "Payment received ";
            case REFUND:
                return "Refund for recharge failure";
            case CASHBACK:
                return "Cash-back offer by FreeCharge";
            case REDEEM:
                return "Redeem freefund";        
            case CASH_FUND:
                return "Credits top-up";
            case ARREWARD:
                return "ARReward Fund";
            case EXCESSCREDIT:
                return "Excess Credit";
            case CS_CASHBACK:
                return "Cashback processed by FreeCharge Customer Support";
            case TVC_CAMPAIGN:
                return "Rs. 10 Surprise Cashback from FreeCharge";
            case REWARD_SERVICE : 
            	return "Reward Service";
            case HCOUPON_REFUND:
            	return "Refund for FreeCharge Deals";
            case REWARDS_CASHBACK:
                return "Cashback reward by FreeCharge";
            case WALLET_MIGRATN_FAIL:
                return "Wallet migration failed";
            default:
                return "Payment received";
            }
        }
    }

    public static enum FundDestination {
        RECHARGE, REVOKE_FUND, REFUND_TO_BANK, HCOUPON, ONE_CHECK_WALLET;

        public static String getFundDestinationValue(String val) {
            String name;
            try {
                name = FundDestination.valueOf(val).getDescriptionString();
            } catch (Exception e) {
                name = "Recharge for";
            }
            return name;
        }
        
        public String getDescriptionString() {
            switch (this) {
                case RECHARGE: return "Recharge for";
                case REVOKE_FUND: return "Revoke FC Balance amount";
                case REFUND_TO_BANK: return "Refund To Bank";
                case HCOUPON: return "FreeCharge Deals";
                case ONE_CHECK_WALLET: return "Credits transferred to FC Wallet";
                default: return "Recharge for";
            }
        }
    }
}
