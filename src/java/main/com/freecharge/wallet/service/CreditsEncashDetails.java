package com.freecharge.wallet.service;


import java.util.Date;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CreditsEncashDetails {

    private int id;
    private int userId;
    private String bankAccountNo;
    private String ifscCode;
    private String beneficiaryName;
    private String emailId;
    private String referenceId;
    private double amount;
    private String transferStatus;
    private Date transferDate;
    private String failureReason;
    private String utrNumber;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(String transferStatus) {
		this.transferStatus = transferStatus;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getFailureReason() {
		return failureReason;
	}

	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	public String getUtrNumber() {
		return utrNumber;
	}

	public void setUtrNumber(String utrNumber) {
		this.utrNumber = utrNumber;
	}

	public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("user_id", this.userId);
        mapSqlParameterSource.addValue("bank_account_no", this.bankAccountNo);
        mapSqlParameterSource.addValue("ifsc_code", this.ifscCode);
        mapSqlParameterSource.addValue("beneficiary_name", this.beneficiaryName);
        mapSqlParameterSource.addValue("email_id", this.emailId);
        mapSqlParameterSource.addValue("reference_id", this.referenceId);
        mapSqlParameterSource.addValue("amount", this.amount);
        mapSqlParameterSource.addValue("transfer_status", this.transferStatus);
        mapSqlParameterSource.addValue("transfer_date", this.transferDate);
        mapSqlParameterSource.addValue("failure_reason", this.failureReason);
        return mapSqlParameterSource;
    }

}
