package com.freecharge.wallet.service;

/**
 * This class encapsulates wallet payment eligibility.
 * Currently there are two.
 * <ul>
 * <li> Pay full amount using wallet </li>
 * <li> Pay partial amount using wallet </li>
 * </ul>
 * @author arun
 *
 */
public class WalletEligibility {
    private boolean eligibleForPartialPayment = false;
    private boolean eligibleForFullWalletPayment = false;
    
    public void setEligibleForFullWalletPayment(boolean eligibleForFullWalletPayment) {
        this.eligibleForFullWalletPayment = eligibleForFullWalletPayment;
    }
    
    public boolean getEligibleForFullWalletPayment() {
        return this.eligibleForFullWalletPayment;
    }

    public void setEligibleForPartialPayment(boolean eligibleForPartialPayment) {
        this.eligibleForPartialPayment = eligibleForPartialPayment;
    }
    
    public boolean getEligibleForPartialPayment() {
        return this.eligibleForPartialPayment;
    }
}
