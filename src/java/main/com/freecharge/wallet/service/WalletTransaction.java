package com.freecharge.wallet.service;

import java.util.Date;

public class WalletTransaction {

    private int walletTransactionId;
    private int fkWalletId;
    private String callerRefrence;
    private Date transactionDate;
    private double transactionAmount;
    private String transactionType;
    private String fundSource;
    private String fundDestination;
    private String metadata;
    private String orderId;
    private double runningBalance;
    private int fkUserId; 

	public double getRunningBalance() {
        return runningBalance;
    }

    public void setRunningBalance(double runningBalance) {
        this.runningBalance = runningBalance;
    }

    public int getWalletTransactionId() {
        return walletTransactionId;
    }

    public void setWalletTransactionId(int walletTransactionId) {
        this.walletTransactionId = walletTransactionId;
    }

    public int getFkWalletId() {
        return fkWalletId;
    }

    public void setFkWalletId(int fkWalletId) {
        this.fkWalletId = fkWalletId;
    }

    public String getCallerRefrence() {
        return callerRefrence;
    }

    public void setCallerRefrence(String callerRefrence) {
        this.callerRefrence = callerRefrence;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getFundDestination() {
        return fundDestination;
    }

    public void setFundDestination(String fundDestination) {
        this.fundDestination = fundDestination;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public int getFkUserId() {
		return fkUserId;
	}

	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}

}
