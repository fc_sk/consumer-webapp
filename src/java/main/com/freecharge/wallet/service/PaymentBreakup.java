package com.freecharge.wallet.service;

import com.freecharge.common.util.Amount;

/**
 * Class to encapsulate the potential payment
 * breakup between payment gateway and wallet.
 * @author arun
 *
 */
public class PaymentBreakup {
    private Amount walletBalance;
    private Amount payableTotalAmount;
    private Amount payablePGAmount;
    private Amount payableWalletAmount;
    
    public Amount getWalletBalance() {
        return walletBalance;
    }
    
    public void setWalletBalance(Amount walletBalance) {
        this.walletBalance = walletBalance;
    }
    
    public Amount getPayableTotalAmount() {
        return payableTotalAmount;
    }
    
    public void setPayableTotalAmount(Amount payableTotalAmount) {
        this.payableTotalAmount = payableTotalAmount;
    }
    
    public Amount getPayablePGAmount() {
        return payablePGAmount;
    }
    
    public void setPayablePGAmount(Amount payablePGAmount) {
        this.payablePGAmount = payablePGAmount;
    }
    
    public Amount getPayableWalletAmount() {
        return payableWalletAmount;
    }
    
    public void setPayableWalletAmount(Amount payableWalletAmount) {
        this.payableWalletAmount = payableWalletAmount;
    }
}
