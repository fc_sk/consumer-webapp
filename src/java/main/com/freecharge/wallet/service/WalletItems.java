package com.freecharge.wallet.service;

import java.util.Date;

public class WalletItems {
	
	private int  walletItemsId;
	private int  fkwalletTransactionId;
	private int  fkItemId;
	private String entityId; 
	private Date addedOn;
	
	public int getWalletItemsId() {
		return walletItemsId;
	}
    
	public void setWalletItemsId(int walletItemsId) {
		this.walletItemsId = walletItemsId;
	}
	
	public int getfkwalletTransationId() {
		return fkwalletTransactionId;
	}
	
	public void setfkwalletTransactionId(int fkwalletTransactionId) {
		this.fkwalletTransactionId = fkwalletTransactionId;
	}
	
	public int getfkItemId() {
		return fkItemId;
	}
	
	public void setfkItemId(int fkItemId) {
		this.fkItemId = fkItemId;
	}
	
	public String getentityId() {
		return entityId;
	}
	
	public void setentityId(String entityId) {
		this.entityId = entityId;
	}
	
	public Date getAddedOn() {
		return addedOn;
	}
	
	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}
}
