package com.freecharge.wallet.service.limitengine;

import org.springframework.stereotype.Component;

import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletLimitRequest;

/**
 * Rule that evaluates if after carrying out the given
 * transaction the wallet balance exceeds a given limit.
 * @author arun
 *
 */
@Component
public class NetBalanceRule extends BaseWalletRule {
    private double balanceUpperLimit;
    private double balanceLowerLimit;
    
    public void setAmountUpperLimit(double amountUpperLimit) {
        this.balanceUpperLimit = amountUpperLimit;
    }
    
    public void setAmountLowerLimit(double balanceLowerLimit) {
        this.balanceLowerLimit = balanceLowerLimit;
    }

    @Override
    public boolean eval(WalletLimitRequest limitRequest) {
        Wallet userWallet = walletService.findBalanceByUserID(limitRequest.getUserId());
        
        if (userWallet == null) {
            // new user who has not done any wallet transaction.
            return true;
        }
        double currentBalance = userWallet.getCurrentBalance();
        double transactionAmount = limitRequest.getAmountToTest().getAmount().doubleValue();
        
        double finalBalance = 0;
        
        switch(limitRequest.getTransactionType()) {
            case DEPOSIT: finalBalance = currentBalance + transactionAmount; break;
            case WITHDRAWAL: finalBalance = currentBalance - transactionAmount; break;
            case ANY:
                throw new IllegalArgumentException(
                        "Invalid transaction type ANY in NetBalanceRule for user ID: "
                                + limitRequest.getUserId());
        }
        
        if (finalBalance < balanceLowerLimit || finalBalance > balanceUpperLimit) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getFailureDescription() {
        return "We know you are rich, but you have exceeded the Balance limit!";
    }
}
