package com.freecharge.wallet.service.limitengine;

import com.freecharge.wallet.service.WalletLimitRequest;

public interface WalletRule {
    public boolean eval(WalletLimitRequest limitRequest);
    public String getFailureDescription();
}
