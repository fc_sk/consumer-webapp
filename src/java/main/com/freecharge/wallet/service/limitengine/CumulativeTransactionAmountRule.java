package com.freecharge.wallet.service.limitengine;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.WalletLimitRequest;

/**
 * Rule that evaluates if the sum total of amounts for 
 * transactions done in the last specified time duration 
 * exceeds a given limit 
 * @author arun
 *
 */
@Component
public class CumulativeTransactionAmountRule extends BaseWalletRule {
    private Integer limitWindowHours;
    private double cumulativeAmountUpperLimit;
    
    public void setLimitWindowHours(Integer limitWindowHours) {
        this.limitWindowHours = limitWindowHours;
    }
    
    public void setCumulativeAmountUpperLimit(double cumulativeAmountUpperLimit) {
        this.cumulativeAmountUpperLimit = cumulativeAmountUpperLimit;
    }

    @Override
    public boolean eval(WalletLimitRequest limitRequest) {
        Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
        
        Date startDate = DateUtils.addHours(new Date(endTimestamp.getTime()), -limitWindowHours);
        
        Timestamp startTimestamp = new Timestamp(startDate.getTime());

        double amountSoFar = walletService.getCumulativeAmountForDeposit(limitRequest.getUserId(), startTimestamp, endTimestamp, Arrays.asList(FundSource.CASH_FUND));
        
        if (amountSoFar + limitRequest.getAmountToTest().getAmount().doubleValue() > cumulativeAmountUpperLimit) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getFailureDescription() {
        return "Amount limit exceeded";
    }
}
