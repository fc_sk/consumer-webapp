package com.freecharge.wallet.service.limitengine;

import com.freecharge.wallet.service.WalletLimitRequest;

/**
 * Rule that evaluates if the transaction amount is
 * within a specified range.
 * @author arun
 *
 */
public class TransactionAmountRule extends BaseWalletRule {
    private Integer minAmount;
    private Integer maxAmount;
    
    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }
    
    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    @Override
    public boolean eval(WalletLimitRequest limitRequest) {
        double amountDoubleValue = limitRequest.getAmountToTest().getAmount().doubleValue();
        if (amountDoubleValue < minAmount || amountDoubleValue > maxAmount) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getFailureDescription() {
        return "We have a situation! Your Balance top-up amount <br> must be between Rs." + minAmount + " and Rs." + maxAmount +".";
    }

}
