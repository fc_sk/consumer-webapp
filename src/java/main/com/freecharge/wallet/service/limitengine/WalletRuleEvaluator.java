package com.freecharge.wallet.service.limitengine;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.freecharge.wallet.service.Wallet.LimitStatus;
import com.freecharge.wallet.service.WalletLimitRequest;
import com.freecharge.wallet.service.WalletLimitResponse;

@Component
public class WalletRuleEvaluator {
    private Map<Integer, WalletRule> prioritizedRuleMap = new HashMap<Integer, WalletRule>();
    
    public void setPrioritizedRuleMap(Map<Integer, WalletRule> prioritizedRuleMap) {
        this.prioritizedRuleMap = prioritizedRuleMap;
    }
    
    public WalletLimitResponse evaluateLimit(WalletLimitRequest limitEvalRequest) {
        final int highestPriority = prioritizedRuleMap.size();
        final int leastPriority = 1;
        
        WalletLimitResponse evalResult = new WalletLimitResponse();
        evalResult.setLimitStatus(LimitStatus.Success);
        
        for(int rulePriority = highestPriority; rulePriority >= leastPriority; rulePriority--) {
            WalletRule ruleToEvaluate = prioritizedRuleMap.get(rulePriority);
            
            if (!ruleToEvaluate.eval(limitEvalRequest)) {
                evalResult.setLimitStatus(LimitStatus.Failure);
                evalResult.setDescriptionString(ruleToEvaluate.getFailureDescription());
                break;
            }
            
        }
        
        return evalResult;
    }
}
