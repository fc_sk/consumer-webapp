package com.freecharge.wallet.service.limitengine;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import com.freecharge.wallet.service.Wallet.FundSource;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletLimitRequest;
/**
 * Rule that evaluates if the count of wallet transactions
 * done in the last specified time duration exceeds a given
 * limit 
 * @author arun
 *
 */
@Component
public class TransactionCountRule extends BaseWalletRule {
    private Integer limitWindowHours;
    private long countUpperLimit;
    
    public void setCountUpperLimit(long countUpperLimit) {
        this.countUpperLimit = countUpperLimit;
    }
    
    public void setLimitWindowHours(Integer limitWindowHours) {
        this.limitWindowHours = limitWindowHours;
    }
    
    @Override
    public boolean eval(WalletLimitRequest limitRequest) {
        Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
        
        Date startDate = DateUtils.addHours(new Date(endTimestamp.getTime()), -limitWindowHours);
        
        Timestamp startTimestamp = new Timestamp(startDate.getTime());

        long countSoFar = walletService.getTransactionCount(limitRequest.getUserId(), startTimestamp, endTimestamp, Arrays.asList(FundSource.CASH_FUND), Arrays.asList(TransactionType.DEPOSIT));
        
        if (countSoFar + 1 > countUpperLimit) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String getFailureDescription() {
        return "Count limit exceeded";
    }
}
