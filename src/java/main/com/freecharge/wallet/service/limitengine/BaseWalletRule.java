package com.freecharge.wallet.service.limitengine;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.wallet.WalletService;


/**
 * Base class for wallet rules. Provides scaffolding on which
 * rules can build. E.g., DAOs and so on.
 * @author arun
 *
 */
public abstract class BaseWalletRule implements WalletRule {
    @Autowired
    protected WalletService walletService; 
}
