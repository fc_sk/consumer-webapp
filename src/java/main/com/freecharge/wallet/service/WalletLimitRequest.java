package com.freecharge.wallet.service;

import com.freecharge.common.util.Amount;

/**
 * A request object that encapsulates
 * request to wallet service to evaluate
 * limit. A typical request could 
 * "Can I add 200Rs to wallet where fund source is add_cash". 
 * @author arun
 *
 */
public class WalletLimitRequest {
    private Amount amountToTest;
    private Wallet.FundSource fundSource;
    private Wallet.FundDestination fundDestination;
    private Wallet.TransactionType transactionType;
    private Long userId;
    
    public Long getUserId() {
        return userId;
    }
    
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    public Amount getAmountToTest() {
        return amountToTest;
    }
    
    public void setAmountToTest(Amount amountToTest) {
        this.amountToTest = amountToTest;
    }
    
    public Wallet.FundDestination getFundDestination() {
        return fundDestination;
    }
    
    public void setFundDestination(Wallet.FundDestination fundDestination) {
        this.fundDestination = fundDestination;
    }
    
    public Wallet.FundSource getFundSource() {
        return fundSource;
    }
    
    public void setFundSource(Wallet.FundSource fundSource) {
        this.fundSource = fundSource;
    }
    
    public Wallet.TransactionType getTransactionType() {
        return transactionType;
    }
    
    public void setTransactionType(Wallet.TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
