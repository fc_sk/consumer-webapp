package com.freecharge.wallet.service;

import java.io.IOException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.apache.http.client.methods.HttpPost;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.apache.http.config.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.KlickPayConstants;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.services.IGatewayHandler.PaymentGateways;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.rest.user.FCWalletTransactionHistoryRequest;
import com.freecharge.rest.user.OneCheckTransactionSummary;
import com.freecharge.rest.user.VoucherDetails;
import com.freecharge.reward.util.StringUtil;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.snapdeal.fcpt.txnhistoryview.client.impl.TxnHistoryViewClientImpl;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetInvoiceDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetInvoiceDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.InvoiceInfo;
import com.snapdeal.fcpt.txnhistoryview.exceptions.HttpTransportException;
import com.snapdeal.fcpt.txnhistoryview.exceptions.ServiceException;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.response.GetUserResponse;
import com.snapdeal.payments.PaymentsClientAuthorizationLib.exception.InvalidRequestException;
import com.snapdeal.payments.sdmoney.client.BankDetailsStoreClient;
import com.snapdeal.payments.sdmoney.client.SDMoneyClient;
import com.snapdeal.payments.sdmoney.service.model.BankAccountDetails;
import com.snapdeal.payments.sdmoney.service.model.GetBankDetailsRequest;
import com.snapdeal.payments.sdmoney.service.model.GetBankDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetExpiredVoucherDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetMoneyOutStatusRequest;
import com.snapdeal.payments.sdmoney.service.model.GetMoneyOutStatusResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherBalanceDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.GetVoucherTransactionsResponse;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import com.snapdeal.payments.sdmoney.service.model.VoucherBalanceDetails;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;

import flexjson.JSONDeserializer;

@Service
public class OneCheckUserService {
    public static final Integer DEFAULT_FCWALLET_TXN_COUNT = 50;

    private static final Set<String> walletGateways = ImmutableSet.<String>of(PaymentGateways.FREECHARGE_WALLET.getName(), PaymentGateways.ONE_CHECK_WALLET.getName());
   
    private static Logger                         logger       = LoggingFactory.getLogger(OneCheckUserService.class);

    private static final Set<String> vcIdempotencyIds = ImmutableSet.<String>of(OneCheckTxnType.DEBIT_VC.name(),
            OneCheckTxnType.CREDIT_VC_REVERSAL.name(), OneCheckTxnType.CREDIT_FUNDPOST_CS_VC.name(),
            OneCheckTxnType.CREDIT_FUNDPOST_MM_VC.name(), OneCheckTxnType.CREDIT_FUNDPOST_BULKREFUND_VC.name(),
            OneCheckTxnType.DEBIT_FUNDPOST_BULKREFUND_VC.name(), OneCheckTxnType.DEBIT_FUNDPOST_CS_VC.name(),
            OneCheckTxnType.DEBIT_FUNDPOST_MM_VC.name(), OneCheckTxnType.CREDIT_FUNDPOST_VC.name(),
            OneCheckTxnType.DEBIT_FUNDPOST_VC.name(), OneCheckTxnType.CREDIT_FUNDPOST_BulkRefund_VC.name(),
            OneCheckTxnType.DEBIT_FUNDPOST_BulkRefund_VC.name());

    SimpleDateFormat                              formatter    = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private JSONDeserializer<Map<String, Object>> deserializer = new JSONDeserializer<Map<String, Object>>();

    @Autowired
    @Qualifier("klickPayConnectionManager")
    private MultiThreadedHttpConnectionManager    klickPayConnectionManager;

    @Autowired
    private SDMoneyClient sdMoneyClient;

    @Autowired
    private BankDetailsStoreClient bankDetailsStoreClient;

    @Autowired
    OneCheckWalletService oneCheckWalletService;

    @Autowired
    FCProperties fcProperties;
   
    @Autowired
    PgReadDAO pgReadDAO;
   
    @Autowired
    KpayPaymentMetaDAO kpayPaymentMetaDAO;

    @Autowired
    private MetricsClient metricsClient;
   
    private TxnHistoryViewClientImpl txnHistoryViewClientImpl;
   
    @PostConstruct
    public void init() {
        txnHistoryViewClientImpl = new TxnHistoryViewClientImpl();
    }
   
    //Hack
    private static Map<String, String> MERCHANT_START_WITH_OC = new HashMap<>();

    private UserDetailsDTO validateRequestAndGetUser(String authToken, String userAgent, String machineIdentifier) {
        if (FCUtil.isEmpty(authToken)) {
            logger.error("Auth token is null");
            throw new InvalidRequestException();
        }
        try {
            GetUserResponse getUserResponse = oneCheckWalletService.getUserDetailsByToken(authToken, userAgent, machineIdentifier);
            UserDetailsDTO userDetailsDTO = getUserResponse.getUserDetails();
            return userDetailsDTO;
        } catch (Exception e) {
            logger.error("Failed to get user for auth token " + authToken, e);
        }
        throw new InvalidRequestException();
    }

    public List<OneCheckTransactionSummary> findTransactions(FCWalletTransactionHistoryRequest historyRequest,
            HttpServletRequest request) throws Exception {
        List<OneCheckTransactionSummary> oneCheckTransactionSummaries = new ArrayList<>();
        String token = request.getHeader("token");
        String userAgent =  request.getHeader("userAgent");
        String machineIdentifier = request.getHeader("userMachineIdentifier");
        UserDetailsDTO userDetails = null;
        try {
            userDetails = validateRequestAndGetUser(token, userAgent, machineIdentifier);
        } catch (InvalidRequestException e) {
            logger.error("Invalid request for auth token : " + token, e);
            return oneCheckTransactionSummaries;
        }
        String userId = userDetails.getUserId();

        Date startDate = null, endDate = null;
       
        try {
            startDate = formatter.parse(historyRequest.getStartDate());
        } catch (Exception e) {
        }
        try {
            endDate = formatter.parse(historyRequest.getEndDate());
        } catch (Exception e) {
        }
       
        return getAllTypesOneCheckTxn(userId, startDate, endDate);
    }

    /*Used for CS tools*/
    @SuppressWarnings("unchecked")
    public Map<String, Object> findTransactionsFromUserId(String userId, Date startDate, Date endDate, String evaluatedKey) throws Exception {
        Map<String, Object> allTypeTransaction =  new HashMap<>();
        Set<String> fcWalletIdempotencyIds = new HashSet<>();
        GetTransactionsResponse getTransactionsResponse = oneCheckWalletService.findTransactions(userId, startDate,
                endDate, DEFAULT_FCWALLET_TXN_COUNT, evaluatedKey);
        List<TransactionSummary> fcWalletTransactions = getTransactionsResponse.getTransactions();
        List<OneCheckTransactionSummary> fcWalletTxnSummaries = new ArrayList<>();
        if (!FCUtil.isEmpty(fcWalletTransactions)) {   
            for (TransactionSummary fcWalletSummary : fcWalletTransactions) {
                OneCheckTransactionSummary transactionSummary = new OneCheckTransactionSummary();
                transactionSummary.setTxnId(this.getTxnId(fcWalletSummary.getTransactionReference()));
                transactionSummary.setTransactionDate(formatter.format(fcWalletSummary.getTimestamp()));
                transactionSummary.setTxnType(fcWalletSummary.getTransactionType() != null ? getTransactionType(fcWalletSummary) : null);
                double amount = fcWalletSummary.getTransactionAmount().doubleValue();
                transactionSummary.setAmount((double) Math.round(amount * 100) / 100);
                if(transactionSummary.getTxnType() != null  && transactionSummary.getTxnType().startsWith("DEBIT")){
                    transactionSummary.setWithdrawalFee(this.getWithdrawalFeeForTransaction(userId, fcWalletSummary));
                }
                setMetadataForTransaction(fcWalletSummary, transactionSummary);
                transactionSummary.setMerchantName(getMerchantNameFromWallet(fcWalletSummary));
                setMetadataForTransaction(fcWalletSummary, transactionSummary);
                transactionSummary.setRunningBalance(fcWalletSummary.getRunningBalance());
                transactionSummary.setIdempotencyId(fcWalletSummary.getIdempotencyId());
                if(fcWalletSummary.getTransactionType() == TransactionType.DEBIT_WITHDRAW){
                    setMetaDataForWithdrawalTxn(transactionSummary, userId);
                }
                fcWalletTxnSummaries.add(transactionSummary);
                fcWalletIdempotencyIds.add(fcWalletSummary.getTransactionReference());
            }
        }
        Collections.sort(fcWalletTxnSummaries);
        allTypeTransaction.put("fcwallet", fcWalletTxnSummaries);
        allTypeTransaction.put("fcwalletlastkey",getTransactionsResponse.getLastEvaluatedKey());
        // Get the transaction history from klickpay
        Map<String, Object> klickPayTransactions = getUserTransactionFromKlickPay(userId, startDate, endDate);
        String statusCode = (String) klickPayTransactions.get("status");
        if (statusCode == null || !statusCode.equals(KlickPayConstants.SUCCESS_CODE)) {
            logger.info("Unable to fetch transaction details from klickpay for userId " + userId);
            return allTypeTransaction;
        }
        List<Map<String, Object>> kpayTransactions = (ArrayList<Map<String, Object>>) klickPayTransactions
                .get("transactions");
        List<OneCheckTransactionSummary> klickPayTxnSummaries = new ArrayList<>();
        if (!FCUtil.isEmpty(kpayTransactions)) {
            for (Map<String, Object> kpayTxn : kpayTransactions) {
                String kpayMetaData = (String) kpayTxn.get("metadata");
                String mtxnId = (String) kpayTxn.get("merchant_transaction_id");
                Boolean isPaySuccess = kpayTxn.get("status") == null ? null : Boolean.parseBoolean((String) kpayTxn.get("status"));
                String pgUsed = (String) kpayTxn.get("pg_underlier");
                if (StringUtils.isEmpty(pgUsed) || !pgUsed.equals("ocw")) {
                    Double amount = null;
                    try {
                        amount = (Double) kpayTxn.get("transaction_amount");
                    } catch (ClassCastException e) {
                        continue;
                    }
                    String merchantName = (String) kpayTxn.get("merchant_name");
                    String txnType = (String) kpayTxn.get("transaction_type");
                    OneCheckTransactionSummary transaction = new OneCheckTransactionSummary();
                    transaction.setTxnId(mtxnId);
                    transaction.setTransactionDate((String) kpayTxn.get("transaction_date"));
                    transaction.setTxnType(convertTxnType(txnType, isPaySuccess));
                    transaction.setAmount(amount);
                    transaction.setMetaData(getKeyFromMetaData(kpayMetaData, "event_context"));
                    transaction.setMerchantName(merchantName);
                    transaction.setPgTransactionId((String) kpayTxn.get("pg_transaction_id"));
                    transaction.setPgUsed((String) kpayTxn.get("pg_used"));
                    transaction.setPgUnderlier((String) kpayTxn.get("pg_underlier"));
                    klickPayTxnSummaries.add(transaction);
                }
            }
        }
        Collections.sort(klickPayTxnSummaries);
        allTypeTransaction.put("klickpay", klickPayTxnSummaries);
     return allTypeTransaction;
    }

    private double getWithdrawalFeeForTransaction(String userId, TransactionSummary fcWalletSummary) throws ServiceException {
        double withdrawalFee = 0;
        try {
            GetInvoiceDetailsRequest invoiceDetailsRequest = new GetInvoiceDetailsRequest();
            invoiceDetailsRequest.setUserId(userId);
            invoiceDetailsRequest.setTxnId(getGlobalTxnIdFromTransactionId(fcWalletSummary.getIdempotencyId()));
            invoiceDetailsRequest.setTxnType("MoneyOutSync");
            logger.info("GetInvoiceDetailsRequest for userId: " + userId + " is " + invoiceDetailsRequest.toString());
            GetInvoiceDetailsResponse invoiceDetailsResponse = txnHistoryViewClientImpl.getInvoiceDetails(invoiceDetailsRequest);
            if (invoiceDetailsResponse != null) {
                logger.info("GetInvoiceDetailsResponse for userId: " + userId + " is " + invoiceDetailsResponse.toString());
                if (invoiceDetailsResponse.getInvoiceDetails() != null) {
                    withdrawalFee = calculateWithdrawalFee(invoiceDetailsResponse.getInvoiceDetails());
                }else{
                    logger.info("InvoiceDetails for userId: " + userId + " is null");
                }
            } else {
                logger.info("GetInvoiceDetailsResponse for userId: " + userId + " is null");
            }
        } catch (ServiceException e) {
            logger.error("ServiceException while getting getInvoiceDetails to user: " + userId+" Exception "+e.getMessage());
        } catch (Exception e) {
            logger.error("Exception while getting getInvoiceDetails to user: "  + userId+" Exception "+e.getMessage());
        }
        return ((double) Math.round(withdrawalFee * 100) / 100);
    }

    private double calculateWithdrawalFee(
            InvoiceInfo invoiceInfo) {
        double withdrawalFee = 0;
        if(invoiceInfo.getHandlingCharges() != null){
            withdrawalFee += invoiceInfo.getHandlingCharges().doubleValue();
        }
        if(invoiceInfo.getIgst() != null){
            withdrawalFee += invoiceInfo.getIgst().doubleValue();
        }
        if(invoiceInfo.getCgst() != null){
            withdrawalFee += invoiceInfo.getCgst().doubleValue();
        }
        if(invoiceInfo.getSgst() != null){
            withdrawalFee += invoiceInfo.getSgst().doubleValue();
        }
        return withdrawalFee;
    }

    private String getGlobalTxnIdFromTransactionId(String idempotencyId) {
        if (StringUtils.isNotBlank(idempotencyId) && idempotencyId.contains("WITHDRAW")) {
            idempotencyId = idempotencyId.replace("WITHDRAW", "");
        }
        return idempotencyId;
    }

    public boolean isVCardTxn(OneCheckTransactionSummary oneCheckTransactionSummary)
    {
        boolean result = false;
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> metaData = new HashMap<String, Object>();
        try {
            if(oneCheckTransactionSummary.getMetaData()!=null) {
                metaData = mapper.readValue(oneCheckTransactionSummary.getMetaData(), new TypeReference<Map<String, String>>() {
                });
                if (metaData.containsKey("transactionType")) {
                    if(vcIdempotencyIds.contains(metaData.get("transactionType"))) {
                        result = true;
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error parsing Meta-Data to JSON");
            result = false;
        } finally {
            for(String idempotencyVC : vcIdempotencyIds) {
                if(oneCheckTransactionSummary.getIdempotencyId().contains(idempotencyVC)) {
                    result = true;
                }
            }
        }
        return result;
    }
   
    public boolean isP2PTxn(OneCheckTransactionSummary oneCheckTransactionSummary)
    {
        boolean result = false;
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> metaData = new HashMap<String, Object>();
        try {
            if(oneCheckTransactionSummary.getMetaData()!=null) {
                metaData = mapper.readValue(oneCheckTransactionSummary.getMetaData(), new TypeReference<Map<String, String>>() {
                });
                if (metaData.containsKey("displayInfo") || metaData.containsKey("payHandleType") || metaData.containsKey("payHandle")) {
                        result = true;
                }
            }
        } catch (IOException e) {
            logger.error("Error parsing Meta-Data to JSON");
            result = false;
        } finally {
                if(metaData.containsKey("displayInfo")) {
                    result = true;
                }
        }   
        return result;
    }
   

    public List<VoucherDetails> getUsersExpiredVouchers(HttpServletRequest request) {
        String token = request.getHeader("token");
        String userAgent = request.getHeader("userAgent");
        String machineIdentifier = request.getHeader("userMachineIdentifier");
        UserDetailsDTO userDetails = null;
        List<VoucherDetails> vouchersList = new ArrayList<VoucherDetails>();
        try {
            userDetails = validateRequestAndGetUser(token, userAgent, machineIdentifier);
        } catch (InvalidRequestException e) {
            logger.error("Invalid request for auth token : " + token, e);
            return vouchersList;
        } catch (Exception exception) {
            logger.error("Exception getting user details from token : " + token, exception);
            return vouchersList;
        }
        String oneCheckUserId = userDetails.getUserId();
        vouchersList.addAll(getVouchersExpiredForUser(oneCheckUserId));
        return vouchersList;
    }
   
    public List<VoucherDetails> getUsersVouchers(HttpServletRequest request) {
        String token = request.getHeader("token");
        String userAgent = request.getHeader("userAgent");
        String machineIdentifier = request.getHeader("userMachineIdentifier");
        UserDetailsDTO userDetails = null;
        List<VoucherDetails> vouchersList = new ArrayList<VoucherDetails>();
        try {
            userDetails = validateRequestAndGetUser(token, userAgent, machineIdentifier);
        } catch (InvalidRequestException e) {
            logger.error("Invalid request for auth token : " + token, e);
            return vouchersList;
        } catch (Exception exception) {
            logger.error("Exception getting user details from token : " + token, exception);
            return vouchersList;
        }
        String oneCheckUserId = userDetails.getUserId();
        vouchersList.addAll(getUsersVouchers(oneCheckUserId));
        return vouchersList;
    }
   
    public List<OneCheckTransactionSummary> getVoucherTransactionsForUser(HttpServletRequest request){
        String token = request.getHeader("token");
        String userAgent = request.getHeader("userAgent");
        String machineIdentifier = request.getHeader("userMachineIdentifier");
        List<OneCheckTransactionSummary> transactionsList = new ArrayList<OneCheckTransactionSummary>();
        try {
            validateRequestAndGetUser(token, userAgent, machineIdentifier);
        } catch (InvalidRequestException e) {
            logger.error("Invalid request for auth token : " + token, e);
            return transactionsList;
        } catch (Exception exception) {
            logger.error("Exception getting user details from token : " + token, exception);
            return transactionsList;
        }
        String voucherId = request.getParameter("voucher_id");
        transactionsList.addAll(getTransactionsForVoucher(voucherId));
        return transactionsList;
    }
   
    public List<VoucherDetails> getVouchersExpiredForUser(String oneCheckUserId){
        List<VoucherDetails> vouchersList = new ArrayList<VoucherDetails>();
        GetExpiredVoucherDetailsResponse expiredVouchersResponse = null;
        long startMillis = System.currentTimeMillis();
        try {
            expiredVouchersResponse = oneCheckWalletService.getExpiredVouchersForUser(oneCheckUserId);
            metricSuccess(startMillis, "getVouchersExpiredForUser");
        } catch (Exception exception) {
            logger.error("Exception getting expired voucher details for user : " + oneCheckUserId, exception);
            metricFailure(startMillis, "getVouchersExpiredForUser");
            return vouchersList;
        }
        if (expiredVouchersResponse != null && expiredVouchersResponse.getVoucherBalanceDetails() != null
                && !expiredVouchersResponse.getVoucherBalanceDetails().isEmpty()) {
            for (VoucherBalanceDetails voucherBalanceDetails : expiredVouchersResponse.getVoucherBalanceDetails()) {
                GetVoucherDetailsResponse voucherDetailsResponse = oneCheckWalletService
                        .getVoucherDetails(voucherBalanceDetails.getVoucherId());
                if (voucherDetailsResponse != null) {
                    VoucherDetails voucherDetails = convertIntoVoucherDetails(voucherDetailsResponse);
                    voucherDetails.setVoucherId(voucherBalanceDetails.getVoucherId());
                    voucherDetails.setVoucherBalance(voucherBalanceDetails.getVoucherAmount().doubleValue());
                    vouchersList.add(voucherDetails);
                }
            }
            metricSuccess(startMillis, "getVouchersExpiredForUser");
        }
        return vouchersList;
    }

    public List<VoucherDetails> getUsersVouchers(String oneCheckUserId){
        List<VoucherDetails> vouchersList = new ArrayList<VoucherDetails>();
        GetVoucherBalanceDetailsResponse voucherBalanceResponse = null;
        long startMillis = System.currentTimeMillis();
        try {
            voucherBalanceResponse = oneCheckWalletService.getVoucherBalanceDetails(oneCheckUserId);
            metricSuccess(startMillis, "getUsersVouchers");
        } catch (Exception exception) {
            logger.error("Exception getting expired voucher details for user : " + oneCheckUserId, exception);
            metricFailure(startMillis, "getUsersVouchers");
            return vouchersList;
        }
        if (voucherBalanceResponse != null && voucherBalanceResponse.getVoucherBalanceDetails() != null
                && !voucherBalanceResponse.getVoucherBalanceDetails().isEmpty()) {
            for (VoucherBalanceDetails voucherBalanceDetails : voucherBalanceResponse.getVoucherBalanceDetails()) {
                GetVoucherDetailsResponse voucherDetailsResponse = oneCheckWalletService
                        .getVoucherDetails(voucherBalanceDetails.getVoucherId());
                if (voucherDetailsResponse != null) {
                    VoucherDetails voucherDetails = convertIntoVoucherDetails(voucherDetailsResponse);
                    voucherDetails.setVoucherId(voucherBalanceDetails.getVoucherId());
                    voucherDetails.setVoucherBalance(voucherBalanceDetails.getVoucherAmount().doubleValue());
                    vouchersList.add(voucherDetails);
                }
            }
        }
        metricSuccess(startMillis, "getUsersVouchers");
        return vouchersList;
    }
   
    public List<OneCheckTransactionSummary> getTransactionsForVoucher(String voucherId) {
        List<OneCheckTransactionSummary> transactionsList = new ArrayList<OneCheckTransactionSummary>();
        GetVoucherTransactionsResponse voucherTransactionsResponse = null;
        try {
            voucherTransactionsResponse = oneCheckWalletService.getTransactionsForVoucher(voucherId);
        } catch (Exception exception) {
            logger.error("Exception getting transactions for voucher : " + voucherId, exception);
            return transactionsList;
        }
        if (voucherTransactionsResponse != null) {
            List<TransactionSummary> transactions = voucherTransactionsResponse.getTransactions();
            if (transactions != null && !transactions.isEmpty()) {
                for (TransactionSummary fcWalletSummary : transactions) {
                    OneCheckTransactionSummary transactionSummary = new OneCheckTransactionSummary();
                    transactionSummary.setTxnId(fcWalletSummary.getTransactionReference());
                    transactionSummary.setTransactionDate(formatter.format(fcWalletSummary.getTimestamp()));
                    transactionSummary.setTxnType(fcWalletSummary.getTransactionType() != null ? fcWalletSummary
                            .getTransactionType().name() : null);
                    transactionSummary.setAmount(fcWalletSummary.getTransactionAmount().doubleValue());
                    transactionSummary.setMetaData(fcWalletSummary.getEventContext());
                    transactionSummary.setMerchantName(getMerchantNameFromWallet(fcWalletSummary));
                    transactionSummary.setRunningBalance(fcWalletSummary.getRunningBalance());
                    transactionSummary.setIdempotencyId(fcWalletSummary.getIdempotencyId());
                    transactionsList.add(transactionSummary);
                }
            }
        }
        return transactionsList;
    }
   
    private VoucherDetails convertIntoVoucherDetails(GetVoucherDetailsResponse voucherDetailsResponse) {
        VoucherDetails voucherDetails = new VoucherDetails();
        voucherDetails.setBusinessEntity(voucherDetailsResponse.getBussinessEntity());
        voucherDetails.setVoucherAmount(voucherDetailsResponse.getVoucherAmount().doubleValue());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        voucherDetails.setVoucherCreatedOn(dateFormat.format(voucherDetailsResponse.getCreatedOn()));
        if (voucherDetailsResponse.getExpiredOn() != null) {
            voucherDetails.setVoucherExpiredOn(dateFormat.format(voucherDetailsResponse.getExpiredOn()));
        }
        voucherDetails.setVoucherExpiryDate(dateFormat.format(voucherDetailsResponse.getExpiryDate()));
        voucherDetails.setVoucherStatus(voucherDetailsResponse.getVoucherStatus().name());
        return voucherDetails;
    }

    private void setMetadataForTransaction(TransactionSummary fcWalletSummary,
            OneCheckTransactionSummary transactionSummary) {
        String metadata = fcWalletSummary.getEventContext();
        if (transactionSummary.getTxnType() != null
                && transactionSummary.getTxnType().equals(OneCheckConstants.ONE_CHECK_CASHBACK_TRANSACTION_TYPE)) {
            GetVoucherDetailsResponse voucherDetailsResponse = oneCheckWalletService
                    .getVoucherDetailsForTransaction(fcWalletSummary);
            String voucherExpiryDate = null;
            if (voucherDetailsResponse != null) {
                Date expiryDate = voucherDetailsResponse.getExpiryDate();
                if (expiryDate != null) {
                    voucherExpiryDate = formatter.format(expiryDate);
                }
            }

            if (!StringUtils.isEmpty(voucherExpiryDate)) {
                if (!StringUtils.isEmpty(metadata)) {
                    metadata = metadata + " (Expires on " + voucherExpiryDate + ")";
                } else {
                    metadata = "Expires on " + voucherExpiryDate;
                }
            }
        }
        else if(OneCheckConstants.ONE_CHECK_REFUND_TRANSACTION_TYPE.equals(transactionSummary.getTxnType())
                && OneCheckConstants.ONE_CHECK_FREECHARGE_MERCHANT.equals(transactionSummary.getMerchantName()))
        {
            List<PaymentRefundTransaction> refundTransactions = pgReadDAO.getSuccessfulRefundsForOrderId(transactionSummary.getTxnId());
            if(!FCUtil.isEmpty(refundTransactions))
            {
                PaymentRefundTransaction paymentRefundTransaction = refundTransactions.get(0);
                String refundMsg = getRefundMessage(transactionSummary.getTxnId(), paymentRefundTransaction.getRefundTo());
                if (!StringUtils.isEmpty(metadata))
                {
                    metadata = metadata + " ("+refundMsg+")";
                }
                else if(!StringUtils.isEmpty(paymentRefundTransaction.getPgTransactionReferenceNo()))
                {
                    metadata = refundMsg;
                }
            }
        }
       
        transactionSummary.setMetaData(metadata);
    }
   
    private String getRefundMessage(String txnId, String refundTo)
    {
        String refundedGateway = refundTo;
       
        if(PaymentGateways.KLICK_PAY.getName().equals(refundTo))
        {
            List<KlickpayPaymentMeta> paymentMetas = kpayPaymentMetaDAO.getKpayPaymentMeta(txnId);
            if(!FCUtil.isEmpty(paymentMetas))
            {
                KlickpayPaymentMeta kpayPaymentMeta = paymentMetas.get(0);
                refundedGateway = kpayPaymentMeta.getPgUsed();
            }
        }
       
        if(walletGateways.contains(refundedGateway))
        {
            return "Refunded to FC wallet";
        }
        else
        {
            return "Refunded to Bank";
        }
    }
   
    private String getTransactionType(TransactionSummary transactionSummary) {
        String idempotencyId = transactionSummary.getIdempotencyId();
        String transactionType = null;
        if (!StringUtils.isEmpty(idempotencyId)) {
            String[] idempotencyIdTokens = idempotencyId.split("_");
            if (idempotencyIdTokens.length >= 3) {
                String dataToMatch = idempotencyIdTokens[1] + "_" + idempotencyIdTokens[2];
                if (Arrays.asList(OneCheckTxnType.CREDIT_FREEFUND.toString(), OneCheckTxnType.CREDIT_REWARD.toString(),
                        OneCheckTxnType.CREDIT_PROMOCODE.toString(), OneCheckTxnType.CS_CASHBACK.toString()).contains(
                        dataToMatch)) {
                    transactionType = OneCheckConstants.ONE_CHECK_CASHBACK_TRANSACTION_TYPE;
                } else if (Arrays.asList(OneCheckTxnType.CS_REFUND.toString(),
                        OneCheckWalletService.OneCheckTxnType.CREDIT_REFUND.toString()).contains(dataToMatch)) {
                    transactionType = OneCheckConstants.ONE_CHECK_REFUND_TRANSACTION_TYPE;
                }
            }
        }
        if (StringUtils.isEmpty(transactionType)) {
            transactionType = transactionSummary.getTransactionType().name();
        }
        return transactionType;
    }

    private void setMetaDataForWithdrawalTxn(OneCheckTransactionSummary transactionSummary, String userId)
    {
        try {
            GetMoneyOutStatusRequest getMoneyOutStatusRequest = new GetMoneyOutStatusRequest();
            getMoneyOutStatusRequest.setTransactionRef(transactionSummary.getTxnId());
            getMoneyOutStatusRequest.setSdIdentity(userId);
            GetMoneyOutStatusResponse getMoneyOutStatusResponse = sdMoneyClient.getMoneyOutStatus(getMoneyOutStatusRequest);
            GetBankDetailsRequest getBankDetailsRequest = new GetBankDetailsRequest();
            getBankDetailsRequest.setAccountToken(getMoneyOutStatusResponse.getAccountToken());
            GetBankDetailsResponse getBankDetailsResponse = bankDetailsStoreClient.getBankDetails(getBankDetailsRequest);
            BankAccountDetails bankAccountDetails = getBankDetailsResponse.getAccountDetails();
            String metadata = "Account Number: " + bankAccountDetails.getAccountNumber() +
                    ", Bank Name: " + bankAccountDetails.getBankName() + ", IFSC Code: " + bankAccountDetails.getIfsc() + ", Status: " + bankAccountDetails.getStatus().toString() +
                    ", Verfication Ref. ID: " + bankAccountDetails.getVrfnRefId();
            transactionSummary.setMetaData(metadata);
        }
        catch (Exception e) {
            logger.error("Exception in fetchin bank details for transaction id: "+transactionSummary.getTxnId() + e);
            transactionSummary.setMetaData("");
        }
    }

    @SuppressWarnings("unchecked")
    private final List<OneCheckTransactionSummary> getAllTypesOneCheckTxn(
            String userId, Date startDate, Date endDate) throws Exception {
        // Get all the transactions for user from FC Wallet
        Set<String> fcWalletIdempotencyIds = new HashSet<>();
        List<OneCheckTransactionSummary> oneCheckTransactionSummaries = new ArrayList<>();
        GetTransactionsResponse getTransactionsResponse = oneCheckWalletService.findTransactions(userId, startDate,
                endDate, DEFAULT_FCWALLET_TXN_COUNT,null);
        List<TransactionSummary> fcWalletTransactions = getTransactionsResponse.getTransactions();

        if (!FCUtil.isEmpty(fcWalletTransactions)) {
            for (TransactionSummary fcWalletSummary : fcWalletTransactions) {
                OneCheckTransactionSummary transactionSummary = new OneCheckTransactionSummary();
                transactionSummary.setTxnId(fcWalletSummary.getTransactionReference());
                transactionSummary.setTransactionDate(formatter.format(fcWalletSummary.getTimestamp()));
                transactionSummary.setTxnType(fcWalletSummary.getTransactionType() != null ? fcWalletSummary.getTransactionType().name() : null);
                transactionSummary.setAmount(fcWalletSummary.getTransactionAmount().doubleValue());
                transactionSummary.setMetaData(fcWalletSummary.getEventContext());
                transactionSummary.setMerchantName(getMerchantNameFromWallet(fcWalletSummary));
                transactionSummary.setRunningBalance(fcWalletSummary.getRunningBalance());
                transactionSummary.setIdempotencyId(fcWalletSummary.getIdempotencyId());
                transactionSummary.setBusinssTxnType(fcWalletSummary.getBusinessTransactionType() != null ? fcWalletSummary.getBusinessTransactionType().toString() : null);
                transactionSummary.setContextMap(getContextMap(fcWalletSummary));
                oneCheckTransactionSummaries.add(transactionSummary);
                fcWalletIdempotencyIds.add(fcWalletSummary.getTransactionReference());
            }
        }
        // Get the transaction history from klickpay
        Map<String, Object> klickPayTransactions = getUserTransactionFromKlickPay(userId, startDate, endDate);
        String statusCode = (String) klickPayTransactions.get("status");
        if (statusCode == null || !statusCode.equals(KlickPayConstants.SUCCESS_CODE)) {
            logger.info("Unable to fetch transaction details from klickpay for userId " + userId);
            return oneCheckTransactionSummaries;
        }
        List<Map<String, Object>> kpayTransactions = (ArrayList<Map<String, Object>>) klickPayTransactions
                .get("transactions");
       
        if (!FCUtil.isEmpty(kpayTransactions)) {
            for (Map<String, Object> kpayTxn : kpayTransactions) {
                String kpayMetaData = (String) kpayTxn.get("metadata");
                String mtxnId = (String) kpayTxn.get("merchant_transaction_id");
                Boolean isPaySuccess = kpayTxn.get("status") == null ? null : Boolean.parseBoolean((String) kpayTxn.get("status"));
               
                String pgUsed = kpayTxn.get("pg_underlier") == null ? null : (String) kpayTxn.get("pg_underlier");
                if ((mtxnId != null && !mtxnId.startsWith("OC")) && (StringUtils.isEmpty(pgUsed) || !pgUsed.equals("ocw"))) {
                    Double amount = null;
                    try {
                        amount = (Double) kpayTxn.get("transaction_amount");
                    } catch (ClassCastException e) {
                        continue;
                    }
                   
                    String merchantName = (String) kpayTxn.get("merchant_name");
                    String txnType = (String) kpayTxn.get("transaction_type");
                    OneCheckTransactionSummary transactionSummary = new OneCheckTransactionSummary();
                    transactionSummary.setTxnId(mtxnId);
                    transactionSummary.setTransactionDate((String) kpayTxn.get("transaction_date"));
                   
                    if (txnType != null && txnType.equals("REFUND")) {
                        continue;
                    }
                   
                    transactionSummary.setTxnType(convertTxnType(txnType, isPaySuccess));
                    transactionSummary.setAmount(amount);
                    transactionSummary.setMetaData(getKeyFromMetaData(kpayMetaData, "event_context"));
                    transactionSummary.setMerchantName(merchantName);
                    oneCheckTransactionSummaries.add(transactionSummary);
                    fcWalletIdempotencyIds.add(mtxnId);
                }
            }
        }
        // Sort in descending order of TransactionDate
        Collections.sort(oneCheckTransactionSummaries);
        return oneCheckTransactionSummaries;
    }

    private Map<String, Object> getContextMap(TransactionSummary fcWalletSummary) {
        Map<String, Object> contextMap = new HashMap<>();
        constructP2PContextMap(fcWalletSummary, contextMap);
        return contextMap;
    }

    @SuppressWarnings({ "serial", "unchecked" })
    private void constructP2PContextMap(TransactionSummary fcWalletSummary, Map<String, Object> contextMap) {
        try {
            Type type = new TypeToken<Map<String, Object>>() {}.getType();
            String eventContext = fcWalletSummary.getEventContext();
            Map<String, Object> p2pContextMap = null;

            if (!(eventContext == null || eventContext.isEmpty())) {
                try {
                    p2pContextMap = new Gson().fromJson(eventContext, type);
                } catch (JsonSyntaxException e) {
                    return;
                }

                logger.info(String.format("EventContext details - %s", fcWalletSummary.getEventContext()));
                logger.info(String.format("p2pContextMap details - %s", p2pContextMap));

                Map<String, Object> displayInfoWrapper = null;
                Map<String, String> displayInfo = null;
                String transactionType = null;

                if (p2pContextMap != null) {
                    contextMap.put(OneCheckConstants.P2P_COMMENT, p2pContextMap.get("comment"));
                    contextMap.put(OneCheckConstants.P2P_REQUEST_TIMESTAMP, p2pContextMap.get("requestTimestamp"));
                    transactionType = (String) p2pContextMap.get("transactionType");

                    for (Map.Entry<String, Object> entry : p2pContextMap.entrySet()) {
                        String key = entry.getKey();
                        Object value = entry.getValue();
                        if (key.equals("displayInfo")) {
                            try {
                                displayInfoWrapper = new Gson().fromJson((String) value, type);
                            } catch (JsonSyntaxException e) {
                                return;
                            }
                        }
                    }
                }

                boolean isCredit = isCreditTransaction(fcWalletSummary.getTransactionType(), transactionType);
                if (displayInfoWrapper != null) {
                    for (Map.Entry<String, Object> entry : displayInfoWrapper.entrySet()) {
                        String key = entry.getKey();
                        Object value = entry.getValue();
                        String displayInfoKey = TransactionType.CREDIT_DEFAULT.equals(fcWalletSummary.getTransactionType()) ? "sourceDisplayInfo" : "destDisplayInfo";
                        if (key.equals(displayInfoKey)) {
                            displayInfo = (Map<String, String>) value;
                        }
                    }
                }

                if (displayInfo != null) {
                    String contactNameKey = TransactionType.CREDIT_DEFAULT.equals(fcWalletSummary.getTransactionType()) ? "profileName" : "contactName";
                    contextMap.put(OneCheckConstants.P2P_OTHER_PARTY_CONTACT_NAME, displayInfo.get(contactNameKey));
                    contextMap.put(OneCheckConstants.P2P_OTHER_PARTY_MOBILE, displayInfo.get("mobile"));
                    contextMap.put(OneCheckConstants.P2P_OTHER_PARTY_TAG, displayInfo.get("tag"));
                    contextMap.put(OneCheckConstants.IS_MONEY_RECEIVED, isCredit);
                }
            }
        } catch (Exception e) {
            return;
        }
    }

    private boolean isCreditTransaction(final TransactionType transactionType, final String eventContextTransaction) {
        boolean isCredit = false;
        if (eventContextTransaction == null && TransactionType.CREDIT_DEFAULT.equals(transactionType)) {
            isCredit = true;
        } else if (OneCheckConstants.EVENT_CONTEXT_TRANSACTION_P2P_REQUEST_MONEY.equals(eventContextTransaction) && TransactionType.CREDIT_DEFAULT.equals(transactionType)
                || OneCheckConstants.EVENT_CONTEXT_TRANSACTION_P2P_SEND_MONEY.equals(eventContextTransaction) && TransactionType.CREDIT_DEFAULT.equals(transactionType)) {
            isCredit = true;
        }

        return isCredit;
    }

    public String getMerchantNameFromWallet(TransactionSummary transactionSummary) {
       
        String businessEntity = transactionSummary.getBussinessEntity();

        boolean isVCardTxn = isVCardTxn(transactionSummary.getIdempotencyId());
        if (isVCardTxn) {
            String merchantName = getMerchantNameForVCard(transactionSummary.getEventContext());
            if (!FCUtil.isEmpty(merchantName)) {
                return merchantName;
            }
        }
       
        if (businessEntity == null) {
            return null;
        }
        if (businessEntity.equalsIgnoreCase(fcProperties.getProperty(FCProperties.ONE_CHECK_BUSINESS_ENTITY))
                || businessEntity.equalsIgnoreCase(OneCheckConstants.CLICKPAY_MERCHANT)) {
            return "FreeCharge";
        }
       
        return businessEntity;
    }

    private static String getMerchantNameForVCard(String eventContext) {
        try {
            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            Map<String, Object> metaDataMap = gson.fromJson(eventContext, HashMap.class);
            if (metaDataMap != null && metaDataMap.containsKey("merchantName")) {
                return (String) metaDataMap.get("merchantName");
            }
        } catch (Exception e) {
            logger.error("Could not parse vcard merchant from event context " + eventContext);
        }
        return null;
    }

    private boolean isVCardTxn(String idempotencyId) {
        if (FCUtil.isEmpty(idempotencyId)) {
            return false;
        }
        for(String idempotencyVC : vcIdempotencyIds) {
            if(idempotencyId.endsWith(idempotencyVC)){
                return true;
            }
        }
        return false;
    }

    private String convertTxnType(String kpayTxnType, Boolean isTxnSuccess) {
        if (kpayTxnType == null) {
            return null;
        }
       
        switch (kpayTxnType) {
            case "PAYMENT":
                if (isTxnSuccess == null) {
                    return KlickPayTransactionHistoryType.PAY_PENDING.name();
                } else if (isTxnSuccess) {
                    return KlickPayTransactionHistoryType.PAY_SUCCESS.name();
                } else {
                    return KlickPayTransactionHistoryType.PAY_FAILURE.name();
                }
            case "REFUND":
                if (isTxnSuccess == null) {
                    return null;
                } else if (isTxnSuccess) {
                    return KlickPayTransactionHistoryType.REFUND_SUCCESS.name();
                } else {
                    return KlickPayTransactionHistoryType.REFUND_FAILED.name();
                }
            default:
                return kpayTxnType;
        }
    }
   
    public enum KlickPayTransactionHistoryType {
        PAY_SUCCESS, PAY_PENDING, PAY_FAILURE, REFUND_SUCCESS, REFUND_FAILED
    }

    private String getKeyFromMetaData(String kpayMetaData, String metaKey) {
        if (kpayMetaData == null) {
            return null;
        }
        String[] metaDataArray = kpayMetaData.split("\\|");
        if (metaDataArray == null) {
            return null;
        }
        for (String data : metaDataArray) {
            String key = data.split(":")[0];
            if (key != null && key.equalsIgnoreCase(metaKey)) {
                return data.split(":")[1];
            }
        }
        return null;
    }

    private Map<String, Object> getUserTransactionFromKlickPay(String userId, Date startDate, Date endDate)
            throws Exception {
        String start = null, end = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            start = dateFormat.format(startDate);
        } catch (Exception e) {
        }
        try {
            end = dateFormat.format(endDate);
        } catch (Exception e) {
        }
        logger.info("Get klickpay txns userid : " + userId + "," + "startdate :"+ start + "," + "endDate : "+ end);
        String requestData = new JSONObject().put("user_id", userId).put("start_date", start).put("end_date", end)
                .toString();
        String url = fcProperties.getProperty(PaymentConstants.KLICKPAY_USER_TRANSACTION_URL);
        Map<String, Object> kpayResponse = new HashMap<String, Object>();
        try {
            kpayResponse = makeWSCall(requestData, url);
        } catch (Exception e) {
            logger.error("Caught exception on fetching the transaction from klickpay for " + userId+" Exception "+e.getMessage());
        }
        return kpayResponse;
    }

    @SuppressWarnings("deprecation")
    private Map<String, Object> makeWSCall(String submitData, String requestURL) {
        HttpClient client = new HttpClient();
        client.setHttpConnectionManager(klickPayConnectionManager);
        PostMethod postMethod = new PostMethod(requestURL);
        postMethod.setRequestHeader("Content-Type", "application/json");

        try {
            postMethod.setRequestBody(submitData);

            int postStatus;
            String paymentResponse;
            try {
                logger.info("Posting request to KlickPay");
                postStatus = client.executeMethod(postMethod);
                paymentResponse = postMethod.getResponseBodyAsString();
                logger.info("Response Received from KlickPay :"+paymentResponse);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            logger.info("Web service response from Klickpay : " + paymentResponse);

            if (HttpStatus.SC_OK == postStatus) {
                Map<String, Object> wsResponse = deserializer.deserialize(paymentResponse);
                logger.info("Http response from klickpay successful");
                return wsResponse;
            } else {
                throw new RuntimeException("Error while calling Klickpay WS end point : [" + requestURL + "]"
                        + "Got HTTP failure HTTP Code [" + postStatus + "]. Post Response [" + paymentResponse + "]");
            }
        } finally {
            postMethod.releaseConnection();
        }
        /*// HttpClient client = new HttpClient();
        int postStatus = 0;
        String paymentResponse=null;
        try {
            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(
                    SSLContexts.createDefault(), new String[] { "TLSv1.2" }, null,
                    SSLConnectionSocketFactory.getDefaultHostnameVerifier());
            PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager(
                    RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("http", PlainConnectionSocketFactory.getSocketFactory())
                            .register("https", sslConnectionSocketFactory).build());

            // Customize the connection pool
            CloseableHttpClient client = HttpClientBuilder.create()
                    .setConnectionManager(poolingHttpClientConnectionManager).build();
            // client.setHttpConnectionManager(klickPayConnectionManager);
            // PostMethod postMethod = new PostMethod(requestURL);
            HttpPost post = new HttpPost(requestURL);
            StringEntity entity = new StringEntity(submitData);
            post.setEntity(entity);
            post.addHeader("Content-Type", "application/json");

            // postMethod.setRequestBody(submitData);

            try {
                CloseableHttpResponse response = client.execute(post);
                HttpEntity responseEntity = response.getEntity();
                paymentResponse = EntityUtils.toString(responseEntity, "UTF-8");
                postStatus = response.getStatusLine().getStatusCode();
                // paymentResponse = postMethod.getResponseBodyAsString();
            } catch (Exception e) {
                logger.error("Exception occured while sending request to Klickpay exception trace : " + e.getMessage());
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            logger.error(
                    "Exception occured while sending request to Klickpay exception trace : " + e.getMessage());
        }
        logger.info("Web service response from Klickpay : " + paymentResponse);

        if (HttpStatus.SC_OK == postStatus) {
            Map<String, Object> wsResponse = deserializer.deserialize(paymentResponse);
            return wsResponse;
        } else {
            throw new RuntimeException("Error while calling Klickpay WS end point : [" + requestURL + "]"
                    + "Got HTTP failure HTTP Code [" + postStatus + "]. Post Response [" + paymentResponse + "]");
        }*/
    }
   
    private void metricFailure(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Exception");
    }

    private void metricSuccess(final long startMillis, final String methodName) {
        this.recordMetric(startMillis, methodName, "Success");
    }

    private void recordMetric(final long startMillis, final String methodName, final String eventName) {
        String serviceName = "OnecheckWallet";
        String finalMethodName = methodName + ".Latency";
        long latency = System.currentTimeMillis() - startMillis;
       
        metricsClient.recordLatency(serviceName, finalMethodName, latency);
        metricsClient.recordEvent(serviceName, methodName, eventName);
    }
   
    //Hack for paid coupons
    private String getTxnId(String txnId) {
        String txnIdModified = txnId;
        if (StringUtils.isNotBlank(txnId) && txnId.startsWith("OC") && MERCHANT_START_WITH_OC.get(txnId) == null && !txnId.startsWith("OC-") && txnId.length() > 6) {
            txnIdModified = txnId.substring(0, txnId.length() - 6);
        }
        return txnIdModified;
    }
   
    static {
        MERCHANT_START_WITH_OC.put("OC7bYWgUQbDGuT", "OC7bYWgUQbDGuT");
        MERCHANT_START_WITH_OC.put("OCTgyZPfd8ftnX", "OCTgyZPfd8ftnX");
        MERCHANT_START_WITH_OC.put("OCL3UPoEsoD54o", "OCL3UPoEsoD54o");
        MERCHANT_START_WITH_OC.put("OCaJxtVvDFgfpU", "OCaJxtVvDFgfpU");
        MERCHANT_START_WITH_OC.put("OCZXw8sLjx53cP", "OCZXw8sLjx53cP");
        MERCHANT_START_WITH_OC.put("OCQXbjo6byaVGf", "OCQXbjo6byaVGf");
        MERCHANT_START_WITH_OC.put("OC4CMZKNF9Hu8G", "OC4CMZKNF9Hu8G");
        MERCHANT_START_WITH_OC.put("OCrEbt0pgIzqSK", "OCrEbt0pgIzqSK");
        MERCHANT_START_WITH_OC.put("OCbWWTP55VyTVf", "OCbWWTP55VyTVf");
        MERCHANT_START_WITH_OC.put("OCd6sfFh4832SH", "OCd6sfFh4832SH");
        MERCHANT_START_WITH_OC.put("OCX4bacvgCYgIy", "OCX4bacvgCYgIy");
        MERCHANT_START_WITH_OC.put("OCF1b398PZTDBI", "OCF1b398PZTDBI");
        MERCHANT_START_WITH_OC.put("OCXFiqzBaaZksf", "OCXFiqzBaaZksf");
        MERCHANT_START_WITH_OC.put("OCVaPnS3ixHq7S", "OCVaPnS3ixHq7S");
    }
   
}