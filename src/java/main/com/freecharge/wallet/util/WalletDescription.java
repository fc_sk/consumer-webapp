package com.freecharge.wallet.util;

public class WalletDescription {

    public static String getValue(String val) {

        try {
            switch (GuideView.valueOf(val)) {

            case RECHARGE_FUND:
                return "Deposit into wallet for payment made towards recharge of ";
            case REFUND:
                return "Recharge failed for ";
            case CASHBACK:
                return "Cash-back offer by FreeCharge ";
            case RECHARGE:
                return "Payment made towards recharge of ";
            default:
                return "Transaction made towards ";
            }
        } catch (Exception e) {
            return "Transaction made towards ";
        }
    }

}

enum GuideView {
    RECHARGE_FUND, CASHBACK, REFUND, RECHARGE;

}
