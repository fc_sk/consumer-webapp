package com.freecharge.wallet.util;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.util.PaymentConstants;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FCWalletUtil {


    @Autowired
    private AmazonDynamoDBAsync dynamoClientMum;

    private Logger logger = LoggingFactory.getLogger(getClass());

    public static final String FC_ONE_CHECK_USER_MAPPING_TABLE = "fc_one_check_user_mapping";
    public static final String EMAIL_ID_COL_NAME = "email";
    public static final String ONE_CHECK_ID_COL_NAME = "one_check_id";
    public static final String ONE_CHECK_MIGRATION_STATUS_COL_NAME = "migration_status";

    public void storeWalletMigrationStatusInDynamo(String email, String oneCheckIdentity, String ocwMigrationStatus) {
        Map<String, AttributeValue> putItemData = new HashMap<>();
        putItemData.put(EMAIL_ID_COL_NAME, new AttributeValue(email));
        putItemData.put(ONE_CHECK_ID_COL_NAME, new AttributeValue(oneCheckIdentity));
        putItemData.put(ONE_CHECK_MIGRATION_STATUS_COL_NAME, new AttributeValue(ocwMigrationStatus));
        dynamoClientMum.putItem(new PutItemRequest(FC_ONE_CHECK_USER_MAPPING_TABLE, putItemData));
        logger.info("Putting wallet migration status in dynamo db");
    }
    public boolean ifFcCreditsToFcWalletMigrationStatusSuccess(String email) {

        GetItemRequest getItemRequest = new GetItemRequest().withTableName(FC_ONE_CHECK_USER_MAPPING_TABLE).withKey(
                ImmutableMap.of(FCWalletUtil.EMAIL_ID_COL_NAME, new AttributeValue().withS(email)));
        GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);
        if(null != getItemResult && null != getItemResult.getItem()) {
            Map<String, AttributeValue> item = getItemResult.getItem();
            AttributeValue attributeValue = item.get(ONE_CHECK_MIGRATION_STATUS_COL_NAME);
            if(null != attributeValue) {
                String creditsMigrationStatus = attributeValue.getS();
                if(PaymentConstants.OCW_MIGRATION_SUCCESS.equals(creditsMigrationStatus) ||
                        PaymentConstants.ENCASH_CREDIT.equals(creditsMigrationStatus)) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getOneCheckIdentityFromDynamo(String email) {
        String oneCheckIdentity = null;
        GetItemRequest getItemRequest = new GetItemRequest().withTableName(FC_ONE_CHECK_USER_MAPPING_TABLE).withKey(
                ImmutableMap.of(EMAIL_ID_COL_NAME, new AttributeValue().withS(email)));
        GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);
        if(null != getItemResult && null != getItemResult.getItem()) {
            Map<String, AttributeValue> item = getItemResult.getItem();
            oneCheckIdentity = item.get(ONE_CHECK_ID_COL_NAME).getS();
        }
        return oneCheckIdentity;
    }
}
