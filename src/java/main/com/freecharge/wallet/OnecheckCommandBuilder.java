package com.freecharge.wallet;

import java.util.concurrent.Callable;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

import com.freecharge.common.timeout.ExternalClientCallCommand;
import com.freecharge.common.timeout.TimeoutKey;


public final class OnecheckCommandBuilder {
	
	public static ExternalClientCallCommand<Integer> getSDResponseCommand(
            final HttpClient httpClient, final HttpMethod method, final String serviceName, final String mtricName) {
        return new ExternalClientCallCommand<Integer>(serviceName, mtricName,
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        return httpClient.executeMethod(method);
                    }
                }, TimeoutKey.ONE_CHECK_OTP_TIME_OUT);
    }


}
