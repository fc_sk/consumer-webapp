package com.freecharge.sns;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by shwetanka on 8/5/15.
 */
@Service("campaignSNSService")
public class CampaignSNSService extends AmazonSNSService {
  @Autowired
  public CampaignSNSService(@Qualifier("campaignSNSProp") AmazonSNSProp campaignSNSProp) {
    super(campaignSNSProp);
  }
}
