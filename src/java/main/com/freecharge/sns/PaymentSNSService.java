package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("paymentSnsService")
public class PaymentSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + PaymentSNSService.class.getName());

    @Autowired
    public PaymentSNSService(@Qualifier("paymentSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }

}
