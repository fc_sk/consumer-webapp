package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("refundSNSService")
public class RefundSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RefundSNSService.class.getName());

    @Autowired
    public RefundSNSService(@Qualifier("refundSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }

}
