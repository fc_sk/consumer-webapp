package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("revokeAlertSNSService")
public class RevokeAlertSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RevokeAlertSNSService.class.getName());

    @Autowired
    public RevokeAlertSNSService(@Qualifier("revokeAlertSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
      
    }
}