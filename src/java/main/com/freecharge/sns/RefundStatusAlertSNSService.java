package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("refundStatusAlertSNSService")
public class RefundStatusAlertSNSService extends AmazonSNSService{

	private Logger logger = LoggingFactory.getLogger("amazon.sns." + RefundStatusAlertSNSService.class.getName());

    @Autowired
    public RefundStatusAlertSNSService(@Qualifier("refundStatusAlertSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
        logger.info("Props for Refund attempt alert : " + amazonSNSProp.getAwsAccessKey() + " | "
                + amazonSNSProp.getAwsSecretKey() + " | " + amazonSNSProp.getAwsRegion());
    }
}
