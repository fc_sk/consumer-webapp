package com.freecharge.sns;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 1/13/15
 * Time: 11:23 AM
 * To change this template use File | Settings | File Templates.
 */
@Service("locationSNSService")
public class LocationSNSService extends AmazonSNSService {

    @Autowired

    public LocationSNSService(@Qualifier("locationSNSProp") AmazonSNSProp locationSNSProp) {
        super(locationSNSProp);
    }
}
