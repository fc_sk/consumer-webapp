package com.freecharge.sns;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Class for Amazon SNS publish functionality
 */
public class AmazonSNSService {

    private Logger          logger = LoggingFactory.getLogger(getClass());

    private AmazonSNSProp   amazonsnsProps;
    private AmazonSNSClient snsClient;

    public AmazonSNSService(final AmazonSNSProp amazonsns) {
        this.amazonsnsProps = amazonsns;
        snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(amazonsns.getAwsAccessKey(),
                amazonsns.getAwsSecretKey()))).withRegion(amazonsns.getAwsRegion()).build();
    }
    
    public AmazonSNSService(final AmazonSNSProp amazonsns, boolean isDefault) {
        this.amazonsnsProps = amazonsns;
		if (isDefault) {
			logger.info("creating default client");
/*			snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.defaultClient();
*/			
			snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
					.withRegion(amazonsns.getAwsRegion()).build();
			logger.info("created default client");
			
			/*snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
					.withRegion(amazonsns.getAwsRegion()).build();*/

		} else {
			snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(
							new BasicAWSCredentials(amazonsns.getAwsAccessKey(), amazonsns.getAwsSecretKey())))
					.withRegion(amazonsns.getAwsRegion()).build();
		}
       
    }

    public String publish(final String orderId, final String msg) {
        PublishRequest publishReq = new PublishRequest(amazonsnsProps.getTopicArn(), msg);
        PublishResult publishResult = snsClient.publish(publishReq);
        logger.info("Published notification for orderId : " + orderId + " with ack : " + publishResult.getMessageId());
        return publishResult.getMessageId();
    }

    public String publish(final String msg){
        PublishRequest publishReq = new PublishRequest(amazonsnsProps.getTopicArn(), msg);
        PublishResult publishResult = snsClient.publish(publishReq);
        logger.info("Published notification msg : "+ msg+" with ack : " + publishResult.getMessageId());
        logger.info("msg published to topic: " + amazonsnsProps.getTopicArn());
        return publishResult.getMessageId();
    }
}
