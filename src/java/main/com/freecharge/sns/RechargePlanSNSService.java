package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("rechargePlanSnsService")
public class RechargePlanSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RechargePlanSNSService.class.getName());

    @Autowired
    public RechargePlanSNSService(@Qualifier("rechargePlanSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }
}
