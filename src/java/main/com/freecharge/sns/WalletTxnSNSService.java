package com.freecharge.sns;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("walletTxnSNSService")
public class WalletTxnSNSService extends AmazonSNSService {

    @Autowired
    public WalletTxnSNSService(@Qualifier("walletTxnSNSProp") AmazonSNSProp amazonsns) {
        super(amazonsns);
    }

}
