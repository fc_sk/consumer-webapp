package com.freecharge.sns;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;

public class AmazonSNSTest {
    private static Logger logger = LoggingFactory.getLogger(AmazonSNSTest.class.getName());

    public static void main(String[] args) throws JSONException, IOException {
        ClassPathXmlApplicationContext ctx = null;
        try {
            logger.info("Starting to run the Job....");
            ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
            logger.info("Spring context initialization done");
            RechargeSNSService rechargeSNSService = (RechargeSNSService) ctx.getBean("rechargeSnsService");
            rechargeSNSService.publish("testorderid", "Publish message from sns service");
            System.out.println("Spring context initialization done");
        } catch (Exception e) {
            logger.error("Exception running the batch job ", e);
        } finally {
            if (ctx != null) {
                ctx.close();
            }
        }
    }
}
