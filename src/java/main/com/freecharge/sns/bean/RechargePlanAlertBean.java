package com.freecharge.sns.bean;

public class RechargePlanAlertBean implements AlertBean {

    private int     operatorId;
    private int     circleId;
    private String  productType;
    private Double  rechargeAmount;
    private String  planType;
    private String  actionType;
    private boolean isRecommended;
    private Float   sucessRate;
    private Long    txnCount;

    public enum PlanAction {

        // Mark the plan as not active and also do not allow
        // the user to recharge with this plan.
        INVALID("InvalidPlan"),
        // Add the plan to the DB if it is not already there.
        ADD("AddPlan");

        private final String action;

        private PlanAction(String action) {
            this.action = action;
        }

        public String getActionName() {
            return this.action;
        }
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public int getCircleId() {
        return circleId;
    }

    public void setCircleId(int circleId) {
        this.circleId = circleId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Double getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(Double rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public boolean isRecommended() {
        return isRecommended;
    }

    public Float getSucessRate() {
        return sucessRate;
    }

    public void setSucessRate(Float sucessRate) {
        this.sucessRate = sucessRate;
    }

    public Long getTxnCount() {
        return txnCount;
    }

    public void setTxnCount(Long txnCount) {
        this.txnCount = txnCount;
    }

    public void setRecommended(boolean isRecommended) {
        this.isRecommended = isRecommended;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof RechargePlanAlertBean)) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        RechargePlanAlertBean bean = (RechargePlanAlertBean) obj;

        if (this.operatorId == bean.operatorId && this.circleId == bean.circleId
                && this.rechargeAmount == bean.rechargeAmount && this.productType == bean.productType
                && this.planType == bean.planType && this.txnCount == bean.txnCount) {
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "RechargePlanAlertBean [operatorId=" + operatorId + ", circleId=" + circleId + ", productType="
                + productType + ", rechargeAmount=" + rechargeAmount + ", planType=" + planType + ", actionType="
                + actionType + ", isRecommended=" + isRecommended + ", sucessRate=" + sucessRate + ", txnCount="
                + txnCount + "]";
    }
}
