package com.freecharge.sns.bean;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public class RefundAttemptBean implements AlertBean {

    private Calendar timeStamp;
    private Long     userId;
    private String   orderId;
    private String   mtxnId;
    private String   callerReference;
    private Double   amount;
    private String   paymentGateway;
    private Boolean  status;
    private String   reason;

    public RefundAttemptBean() {

        this.status=null;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public String getCallerReference() {
        return callerReference;
    }

    public void setCallerReference(String callerReference) {
        this.callerReference = callerReference;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public static String getJsonString(RefundAttemptBean refundAttemptAlertBean)
            throws JsonGenerationException, JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(refundAttemptAlertBean);
        return json;
    }

}
