package com.freecharge.sns.bean;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public class RechargeAttemptBean implements AlertBean {
    private String   messageVersion;
    private Calendar timeStamp;
    private String   ag;
    private String   orderId;
    private String   product;
    private String   agRequestId;
    private Float    amount;
    private String   serviceNumber;

    public RechargeAttemptBean() {
        this.messageVersion = VERSION10;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getAg() {
        return ag;
    }

    public void setAg(String ag) {
        this.ag = ag;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAgRequestId() {
        return agRequestId;
    }

    public void setAgRequestId(String agRequestId) {
        this.agRequestId = agRequestId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public static String getJsonString(RechargeAttemptBean rechargeAttemptBean) throws JsonGenerationException,
            JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(rechargeAttemptBean);
        return json;
    }

}
