package com.freecharge.sns.bean;

public class GSTFee {

	private Double cgst;
	private Double sgst;
	private Double originalAmount;
	private Double igst;
	
	/**
	 * @return the cgst
	 */
	public Double getCgst() {
		return cgst;
	}
	/**
	 * @param cgst the cgst to set
	 */
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	/**
	 * @return the sgst
	 */
	public Double getSgst() {
		return sgst;
	}
	/**
	 * @param sgst the sgst to set
	 */
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	/**
	 * @return the originalAmount
	 */
	public Double getOriginalAmount() {
		return originalAmount;
	}
	/**
	 * @param originalAmount the originalAmount to set
	 */
	public void setOriginalAmount(Double originalAmount) {
		this.originalAmount = originalAmount;
	}
	/**
	 * @return the igst
	 */
	public Double getIgst() {
		return igst;
	}
	/**
	 * @param igst the igst to set
	 */
	public void setIgst(Double igst) {
		this.igst = igst;
	}
}
