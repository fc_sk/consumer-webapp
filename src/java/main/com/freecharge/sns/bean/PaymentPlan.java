package com.freecharge.sns.bean;

public class PaymentPlan {

    private Double pg;
    private Double credits;
    private Double ocw;

    public Double getPg() {
        return pg;
    }

    public void setPg(Double pg) {
        this.pg = pg;
    }

    public Double getCredits() {
        return credits;
    }

    public void setCredits(Double credits) {
        this.credits = credits;
    }

    public Double getOcw() {
        return ocw;
    }

    public void setOcw(Double ocw) {
        this.ocw = ocw;
    }

}
