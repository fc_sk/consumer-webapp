package com.freecharge.sns.bean;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.freecharge.web.webdo.BBPSTransactionDetails;

public class RechargeAlertBean implements AlertBean {

    private String       messageVersion;
    
	private Calendar     timeStamp;
    private String       messageType;
    private Long         userId;
    private String       orderId;
    private String       channel;
    private String       product;
    private String       serviceNumber;
    private String       serviceProvider;
    private String       serviceRegion;
    private Double       amount;
    private String       isMNP;
    private String       planType;
    private String       statusCode;
    private String       ag;
    private String       agRequestId;
    private String 		 oneCheckId;
    private String       emailId;
    private boolean      isRechargeReversal = false;
    private String		 lob;
    private String		 mob_merchant_id;
    private ReconMetaData reconMetaData;
    private BBPSTransactionDetails bbpsDetails;
    private String fcBillerId;
    private String operatorTxnId;
    
	public BBPSTransactionDetails getBbpsDetails() {
		return bbpsDetails;
	}

	public void setBbpsDetails(BBPSTransactionDetails bbpsDetails) {
		this.bbpsDetails = bbpsDetails;
	}

	public ReconMetaData getReconMetaData() {
		return reconMetaData;
	}

	public void setReconMetaData(ReconMetaData reconMetaData) {
		this.reconMetaData = reconMetaData;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getMobMerchantId() {
		return mob_merchant_id;
	}
	
	@JsonProperty("mob_merchant_id")
	public void setMobMerchantId(String mob_merchant_id) {
		this.mob_merchant_id = mob_merchant_id;
	}


	public enum RechargeStatus {
        RECHARGE_SUCCESS("RechargeSuccess"), RECHARGE_FAILURE("RechargeFailure"), RECHARGE_UNDER_PROCESS(
                "RechargeUnderProcess");
        private final String status;

        private RechargeStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }

    public RechargeAlertBean() {
        this.messageVersion = VERSION11;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getServiceRegion() {
        return serviceRegion;
    }

    public void setServiceRegion(String serviceRegion) {
        this.serviceRegion = serviceRegion;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getIsMNP() {
        return isMNP;
    }

    public void setIsMNP(String isMNP) {
        this.isMNP = isMNP;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getAg() {
        return ag;
    }

    public void setAg(String ag) {
        this.ag = ag;
    }

    public String getAgRequestId() {
        return agRequestId;
    }

    public void setAgRequestId(String agRequestId) {
        this.agRequestId = agRequestId;
    }    

    public String getOneCheckId() {
		return oneCheckId;
	}

	public void setOneCheckId(String oneCheckId) {
		this.oneCheckId = oneCheckId;
	}	

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public static String getJsonString(RechargeAlertBean rechargeAlertBean) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(rechargeAlertBean);
        return json;
    }

    public boolean isRechargeReversal() {
        return isRechargeReversal;
    }

    public void setRechargeReversal(boolean isRechargeReversal) {
        this.isRechargeReversal = isRechargeReversal;
    }

	public String getFcBillerId() {
		return fcBillerId;
	}

	public void setFcBillerId(String fcBillerId) {
		this.fcBillerId = fcBillerId;
	}
	public String getOperatorTxnId() {
		return operatorTxnId;
	}

	public void setOperatorTxnId(String operatorTxnId) {
		this.operatorTxnId = operatorTxnId;
	}
	
	@Override
	public String toString() {
		return "RechargeAlertBean [messageVersion=" + messageVersion + ", timeStamp=" + timeStamp + ", messageType="
				+ messageType + ", userId=" + userId + ", orderId=" + orderId + ", channel=" + channel + ", product="
				+ product + ", serviceNumber=" + serviceNumber + ", serviceProvider=" + serviceProvider
				+ ", serviceRegion=" + serviceRegion + ", amount=" + amount + ", isMNP=" + isMNP + ", planType="
				+ planType + ", statusCode=" + statusCode + ", ag=" + ag + ", agRequestId=" + agRequestId
				+ ", oneCheckId=" + oneCheckId + ", emailId=" + emailId + ", isRechargeReversal=" + isRechargeReversal
				+ ", lob=" + lob + ", mob_merchant_id=" + mob_merchant_id + ", reconMetaData=" + reconMetaData
				+ ", fcBillerId=" + fcBillerId + ", operatorTxnId=" + operatorTxnId + "]";
	}

}
