package com.freecharge.sns.bean;

public class ReconMetaData {
	
	private String reconId;
	private String referenceId;
	private String categoryType;
	private String categorySubtype;
	private String categoryId;
	 private String serviceName;
	    private String serviceReferenceNumber;
	
	public ReconMetaData() {
		super();
	}
	
	public ReconMetaData(String reconId, String referenceId, String categoryType, String categorySubtype,
			String categoryId) {
		super();
		this.reconId = reconId;
		this.referenceId = referenceId;
		this.categoryType = categoryType;
		this.categorySubtype = categorySubtype;
		this.categoryId = categoryId;
	}

	public String getReconId() {
		return reconId;
	}
	public void setReconId(String reconId) {
		this.reconId = reconId;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String productType) {
		this.categoryType = productType;
	}
	public String getCategorySubtype() {
		return categorySubtype;
	}
	public void setCategorySubtype(String categorySubtype) {
		this.categorySubtype = categorySubtype;
	}
	
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceReferenceNumber() {
		return serviceReferenceNumber;
	}

	public void setServiceReferenceNumber(String serviceReferenceNumber) {
		this.serviceReferenceNumber = serviceReferenceNumber;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReconMetaData [reconId=").append(reconId).append(", referenceId=").append(referenceId)
				.append(", categoryType=").append(categoryType).append(", categorySubtype=").append(categorySubtype)
				.append(", categoryId=").append(categoryId).append(", serviceName=").append(serviceName)
				.append(", serviceReferenceNumber=").append(serviceReferenceNumber).append("]");
		return builder.toString();
	}
	
}
