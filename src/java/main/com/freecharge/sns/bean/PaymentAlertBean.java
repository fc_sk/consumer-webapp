package com.freecharge.sns.bean;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.freecharge.web.webdo.FeeCharge;
import com.freecharge.web.webdo.UPIAccountDetails;

public class PaymentAlertBean implements AlertBean {

    private String      messageVersion;
    private Calendar    timeStamp;
    private String      messageType;
    private String      mtxnId;
    private Long        userId;
    private String      orderId;
    private String      channel;
    private String      product;
    private Double      amount;
    private String      paymentType;
    private String      cardNature;
    private String      cardType;
    private String      cardBin;
    private String      cardIssuingBank;
    private PaymentPlan paymentPlan;
    private String      netBankingName;
    private String      paymentGateway;
	private Boolean     manualStatusCheck;
	private String 		transactionId;
	private String 		imsId;
	private FeeCharge   feeCharge;
	private Boolean 	isGSTEnabled;
	private GSTFee		gstAmount;
	private ReconMetaData	reconMetaData;
	private UPIAccountDetails upiAccountDetails;
    private String            pgMessage;
    private String rrn;
	
    public String getPgMessage() {
        return this.pgMessage;
    }

    public void setPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
    }

    public enum PaymentStatus {

        PAYMENT_SUCCESS("PaymentSuccess"), PAYMENT_FAILURE("PaymentFailure"), WALLET_REVOKE("WalletRevoke");
        private final String status;

        private PaymentStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }

    public PaymentAlertBean() {
        this.messageVersion = VERSION10;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = timeStamp;
    }

    public UPIAccountDetails getUpiAccountDetails() {
        return this.upiAccountDetails;
    }

    public void setUpiAccountDetails(UPIAccountDetails upiAccountDetails) {
        this.upiAccountDetails = upiAccountDetails;
    }

    public String getRrn() {
        return this.rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardNature() {
        return cardNature;
    }

    public void setCardNature(String cardNature) {
        this.cardNature = cardNature;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardBin() {
        return cardBin;
    }

    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }

    public String getCardIssuingBank() {
        return cardIssuingBank;
    }

    public void setCardIssuingBank(String cardIssuingBank) {
        this.cardIssuingBank = cardIssuingBank;
    }

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public String getNetBankingName() {
        return netBankingName;
    }

    public void setNetBankingName(String netBankingName) {
        this.netBankingName = netBankingName;
    }

	
    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
    
	public Boolean getManualStatusCheck() {
		return manualStatusCheck;
	}

	public void setManualStatusCheck(Boolean manualStatusCheck) {
		this.manualStatusCheck = manualStatusCheck;
	}

	public static String getJsonString(PaymentAlertBean paymentAlertBean) throws JsonGenerationException,
            JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(paymentAlertBean);
        return json;
    }

	public String getImsId() {
		return imsId;
	}

	public void setImsId(String imsId) {
		this.imsId = imsId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the feeCharge
	 */
	public FeeCharge getFeeCharge() {
		return feeCharge;
	}

	/**
	 * @param feeCharge the feeCharge to set
	 */
	public void setFeeCharge(FeeCharge feeCharge) {
		this.feeCharge = feeCharge;
	}

	/**
	 * @return the isGSTEnabled
	 */
	public Boolean getIsGSTEnabled() {
		return isGSTEnabled;
	}

	/**
	 * @param isGSTEnabled the isGSTEnabled to set
	 */
	public void setIsGSTEnabled(Boolean isGSTEnabled) {
		this.isGSTEnabled = isGSTEnabled;
	}

	/**
	 * @return the gstAmount
	 */
	public GSTFee getGstAmount() {
		return gstAmount;
	}

	/**
	 * @param gstAmount the gstAmount to set
	 */
	public void setGstAmount(GSTFee gstAmount) {
		this.gstAmount = gstAmount;
	}

	public ReconMetaData getReconMetaData() {
		return reconMetaData;
	}

	public void setReconMetaData(ReconMetaData reconMetaData) {
		this.reconMetaData = reconMetaData;
	}
	
}
