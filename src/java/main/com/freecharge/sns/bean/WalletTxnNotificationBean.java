package com.freecharge.sns.bean;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public class WalletTxnNotificationBean implements AlertBean {
    private String     messageVersion;
    private Calendar   timeStamp;
    private Long       userId;
    private String     mtxnId;
    private BigDecimal txnAmount;
    private String     txnType;
    private BigDecimal oldBalance;
    private BigDecimal newBalance;
    private String     fundSource;
    private String     fundDestination;
    private String     createdTs;
    private String     callerRefrence;
    private String     transactionDate;

    public WalletTxnNotificationBean() {
        this.messageVersion = VERSION10;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public BigDecimal getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(BigDecimal txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public BigDecimal getOldBalance() {
        return oldBalance;
    }

    public void setOldBalance(BigDecimal oldBalance) {
        this.oldBalance = oldBalance;
    }

    public BigDecimal getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(BigDecimal newBalance) {
        this.newBalance = newBalance;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getFundDestination() {
        return fundDestination;
    }

    public void setFundDestination(String fundDestination) {
        this.fundDestination = fundDestination;
    }

    public String getCallerRefrence() {
        return callerRefrence;
    }

    public void setCallerRefrence(String callerRefrence) {
        this.callerRefrence = callerRefrence;
    }

    public String getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public static String getJsonString(WalletTxnNotificationBean walletTxnAlertBean) throws JsonGenerationException,
            JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(walletTxnAlertBean);
        return json;
    }

}
