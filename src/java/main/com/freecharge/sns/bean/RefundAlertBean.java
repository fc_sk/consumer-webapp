package com.freecharge.sns.bean;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public class RefundAlertBean implements AlertBean {
    private String       messageVersion;
    private Calendar     timeStamp;
    private Long         userId;
    private String       orderId;
    private Double       amount;
    private String       statusCode;

    public enum RefundStatus {

        REFUND_SUCCESS("RefundSuccess"), REFUND_FAILURE("RefundFailure"), REFUND_UNDER_PROCESS(
                "RefundUnderProcess");
        private final String status;

        private RefundStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return this.status;
        }
    }
    
    public RefundAlertBean() {
        this.messageVersion = VERSION10;
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public Calendar getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public static String getJsonString(RefundAlertBean RefundAlertBean) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(RefundAlertBean);
        return json;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
