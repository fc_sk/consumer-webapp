package com.freecharge.sns;

import java.util.Date;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

// Example SNS Sender
public class AmazonSNSSenderTest {
    // AWS credentials -- replace with your credentials
    static String ACCESS_KEY = "AKIAI7EYMVEPVZBCC5XQ";
    static String SECRET_KEY = "qvNHKnjF1YAQwTqrvTD7L78ArKAhtA2pl/rRn8MT";

    // Sender loop
    public static void main(String... args) throws Exception {
        // Create a client
        AmazonSNSClient service = new AmazonSNSClient(new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY));
        service.setRegion(Region.getRegion(Regions.fromName("ap-southeast-1")));
        // Create a topic
        /*CreateTopicRequest createReq = new CreateTopicRequest().withName("RechargeAlerts");
        CreateTopicResult createRes = service.createTopic(createReq);*/
        
        String topicArn = "arn:aws:sns:ap-southeast-1:695210568016:RechargeAlerts";
        //System.out.println(createRes.getTopicArn());
        /*PublishRequest publishReq = new PublishRequest().withTopicArn(createRes.getTopicArn()).withMessage(
                "Example notification sent at " + new Date());*/
        String msg = "My text published to SNS topic with email endpoint";
        PublishRequest publishReq = new PublishRequest(topicArn, msg);
        PublishResult publishResult = service.publish(publishReq);
        System.out.println("Published " + publishResult.getMessageId());
    }
}
