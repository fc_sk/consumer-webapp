package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("rechargeSnsService")
public class RechargeSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RechargeSNSService.class.getName());

    @Autowired
    public RechargeSNSService(@Qualifier("rechargeSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }

}
