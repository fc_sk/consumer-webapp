package com.freecharge.sns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.json.JsonSerializer;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.uuip.UserDataPointType;

/**
 * Unique User Identification: This is a User Data Point Publish
 * service, which will publish relevant Data Point needed to identify users as
 * unique.
 */
@Service
public class UserDataPointSNSService {

    private static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + UserDataPointSNSService.class.getName());

    private JsonSerializer jsonSerializer  = new JsonSerializer();

    private AmazonSNSClient snsClient;
    
    private Map<UserDataPointType, String> topicMap;

    @Autowired
    private FCProperties fcProperties;

    @PostConstruct
    public void init() {
        snsClient = new AmazonSNSClient(new BasicAWSCredentials(fcProperties.getAwsSnsAccessKey(),
                fcProperties.getAwsSnsSecretKey()));
        snsClient.setRegion(Region.getRegion(Regions.fromName(fcProperties.getAwsSnsRegion())));
        topicMap = new HashMap<>();
        topicMap.put(UserDataPointType.EMAIL_ID, fcProperties.getAwsSnsTopicArnEmail());
        topicMap.put(UserDataPointType.PROFILE_NO, fcProperties.getAwsSnsTopicArnProfileNo());
        topicMap.put(UserDataPointType.RECHARGE_NO, fcProperties.getAwsSnsTopicArnRechargeNo());
        topicMap.put(UserDataPointType.IMEI, fcProperties.getAwsSnsTopicArnIMEI());
        topicMap.put(UserDataPointType.WINDOWS_UNIQUE_DEVICE_ID, fcProperties.getAwsSnsTopicArnDeviceId());
    }

    public String publish(final int userId, final String msg, final UserDataPointType udType) {
        PublishRequest publishReq = new PublishRequest(topicMap.get(udType), msg);
        PublishResult publishResult = snsClient.publish(publishReq);
        logger.info("Published notification for userId : " + userId + " with ack : " + publishResult.getMessageId());
        return publishResult.getMessageId();
    }

    /**
     * Publishes User Data Point for use by Unique User Identification Service.
     * @param userId
     *            the user for which to publish the data point
     * @param dataPoint
     *            the value of data point
     * @param type
     *            the type of data point
     * @param creationDate
     *            time instant when it is published
     */
    public void publishUserDataPoint(final int userId, final String dataPoint, final UserDataPointType type,
            final Date creationDate) {
        try {
            logger.info("Publishing User Data Point for user id: " + userId + " of type: " + type);
            // Return if dataPoint to be published is EMPTY or Profile no. is dummy (as in case of social sign-up)
            if (FCUtil.isEmpty(dataPoint)
                    || (FCConstants.DUMMY_MOBILE_NO.equals(dataPoint) && type == UserDataPointType.PROFILE_NO)) {
                logger.info("EMPTY User Data Point for user id: " + userId + " of type: " + type);
                return;
            }
            // Converting date into ISO formatted string
            SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String isoDateStr = sdf.format(creationDate);

            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("userId", Integer.valueOf(userId));
            dataMap.put("dataPoint", dataPoint);
            dataMap.put("type", type.getName());
            dataMap.put("createdTimestamp", isoDateStr);

            publish(userId, jsonSerializer.serialize(dataMap), type);
        } catch (RuntimeException e) {
            logger.error("Error while publishing User data points for userId: " + userId, e);
        }
    }
}
