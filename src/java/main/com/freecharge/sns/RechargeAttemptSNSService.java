package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("rechargeAttemptSNSService")
public class RechargeAttemptSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RechargeAttemptSNSService.class.getName());
    
    @Autowired
    public RechargeAttemptSNSService(@Qualifier("rechargeAttemptSNSProp") AmazonSNSProp amazonsns) {
        super(amazonsns);
    }

}
