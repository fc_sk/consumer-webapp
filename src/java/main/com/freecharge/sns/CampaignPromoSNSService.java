package com.freecharge.sns;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("campaignPromoSNSService")
public class CampaignPromoSNSService extends AmazonSNSService {
    @Autowired
    public CampaignPromoSNSService(@Qualifier("campaignPromoSNSProp") AmazonSNSProp campaignPromoSNSProp) {
        super(campaignPromoSNSProp,true);
    }
}
