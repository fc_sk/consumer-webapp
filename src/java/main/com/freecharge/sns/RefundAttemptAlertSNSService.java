package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("refundAttemptAlertSNSService")
public class RefundAttemptAlertSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + RefundAttemptAlertSNSService.class.getName());

    @Autowired
    public RefundAttemptAlertSNSService(@Qualifier("refundAttemptAlertSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
        logger.info("Props for Refund attempt alert : " + amazonSNSProp.getAwsAccessKey() + " | "
                + amazonSNSProp.getAwsSecretKey() + " | " + amazonSNSProp.getAwsRegion());
    }

}
