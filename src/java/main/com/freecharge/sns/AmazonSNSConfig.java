package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

@Configuration
public class AmazonSNSConfig {

    private Logger       logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;

    @Bean
    public AmazonSNSProp rechargeSNSProp() {

        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRecharge());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp rechargeAttemptSNSProp() {

        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRechargeAttempt());
        return amazonSNSProp;
    }    
    
    @Bean
    public AmazonSNSProp paymentSNSProp() {

        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsNewRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnPayment());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp locationSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnLocation());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp paymentAttemptSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnPaymentAttempt());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp refundSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRefund());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp rechargePlanSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRechargePlan());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp walletTxnSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnWalletTxn());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp promocodeRedeemDetailsSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnPromocodeRedeemDetails());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp campaignSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsProdAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsProdSecret());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsProdQaRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnCampaign());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp campaignPromoSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
       // amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsProdAccessKey());
      //  amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsProdSecret());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsProdQaRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnCampaignPromo());
        return amazonSNSProp;
    }

    @Bean
    public AmazonSNSProp creditRewardSuccessDetailsSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnCreditRewardSuccessDetails());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp refundAttemptAlertSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRefundAttemptAlert());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp refundStatusAlertSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRefundStatusAlert());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp revokeAlertSNSProp() {
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicArnRevokeAlert());
        return amazonSNSProp;
    }
    
    @Bean
    public AmazonSNSProp invalidPlanDenominationSNSProp(){
        AmazonSNSProp amazonSNSProp = new AmazonSNSProp();
        amazonSNSProp.setAwsAccessKey(fcProperties.getAwsSnsAccessKey());
        amazonSNSProp.setAwsSecretKey(fcProperties.getAwsSnsSecretKey());
        amazonSNSProp.setAwsRegion(fcProperties.getAwsSnsRegion());
        amazonSNSProp.setTopicArn(fcProperties.getAwsSnsTopicInvalidPlanDenominations());
        return amazonSNSProp;
    }
}
