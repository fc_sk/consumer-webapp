package com.freecharge.sns;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("paymentAttemptSNSService")
public class PaymentAttemptSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + PaymentAttemptSNSService.class.getName());

    @Autowired
    public PaymentAttemptSNSService(@Qualifier("paymentAttemptSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }
}
