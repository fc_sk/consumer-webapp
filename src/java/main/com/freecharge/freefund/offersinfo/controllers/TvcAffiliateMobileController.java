package com.freecharge.freefund.offersinfo.controllers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.offersinfo.services.TvcAffiliateMobileService;
import com.freecharge.mongo.repos.TvcAffiliateMobileRepository;
import com.freecharge.util.TimeUtil;
import com.mongodb.DBObject;

/**
 * This api saves the mobile number entered by user on any offer landing page and sends app download SMS to that number
 **/
@Controller
public class TvcAffiliateMobileController {

    private static final String MOBILE_NUMBER = "mobileNumber";
    private static final String SMS_SENDER = "smsgupshup";
    private static final int DEFAULT_SMS_INTERVAL_IN_MINUTES = 60;

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private TvcAffiliateMobileService tvcAffiliateMobileService;

    @Autowired
    private TvcAffiliateMobileRepository tvcAffiliateMobileRepo;

    @Autowired
    private SMSService smsService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private AppConfigService appConfigService;

    @Csrf (exclude = true)
    @NoLogin
    @RequestMapping(value = "/tvc/saveMobileNumber", method = RequestMethod.GET, produces = "application/json")
    public String saveMobileNumber(final HttpServletRequest request, final HttpServletResponse response,
                                   final Model model) throws IOException {
        boolean smsSent;
        Integer userId = FCSessionUtil.getLoggedInUserId();

        String mobileNumber = request.getParameter(MOBILE_NUMBER);
        if (FCUtil.isEmpty(mobileNumber) || !FCUtil.isMobileNoFormat(mobileNumber)) {
            model.addAttribute("status", "failure");
            model.addAttribute("message", "Invalid Mobile Number. Please try again.");
            logger.error("Mobile Number " + mobileNumber + " is not valid for userId " + userId);
            return "jsonView";
        }

        int minRepeatInterval = getAppDownloadConfigProperty(AppConfigService.MIN_SMS_REPEAT_INTERVAL);
        DBObject dbEntry = tvcAffiliateMobileRepo.getData(mobileNumber);
        if (null == dbEntry) {
            logger.debug("No entry found in DB for mobileNumber " + mobileNumber);
            smsSent = sendSms(mobileNumber, userId);
            tvcAffiliateMobileRepo.insert(mobileNumber, userId, new Date(), smsSent);
            createResponse(smsSent, model);
        } else {
            logger.debug("Entry found in DB for mobileNumber " + mobileNumber);
            smsSent = (boolean) dbEntry.get(TvcAffiliateMobileRepository.SMS_SENT);

            if (smsSent) {
                logger.debug("SMS already sent for mobileNumber " + mobileNumber + ". Checking the time difference");
                Date smsSentDate = (Date) dbEntry.get(TvcAffiliateMobileRepository.SMS_SENT_DATE);
                long diffMinutes;

                try {
                    diffMinutes = TimeUtil.getDiffInMinutesFromCurrentTime(smsSentDate);
                } catch (IllegalArgumentException e) {
                    model.addAttribute("status", "failure");
                    model.addAttribute("message", "Some technical error has occured");
                    return "jsonView";
                }
                logger.debug("Diff In Minutes : " + diffMinutes);

                if (diffMinutes <= minRepeatInterval) {
                    logger.debug("SMS sent in last " + minRepeatInterval + " minutes");
                    model.addAttribute("status", "success");
                    model.addAttribute("message", "Message already sent to your Mobile.");
                } else {
                    logger.debug("SMS not sent for mobileNumber " + mobileNumber + " in the last "
                                 + minRepeatInterval + " minutes");
                    smsSent = sendSms(mobileNumber, userId);
                    tvcAffiliateMobileService.updateData(mobileNumber, new Date(), smsSent, new Date());
                    createResponse(smsSent, model);
                }

            } else {
                smsSent = sendSms(mobileNumber, userId);
                tvcAffiliateMobileService.updateData(mobileNumber, new Date(), smsSent, new Date());
                createResponse(smsSent, model);
            }
        }
        return "jsonView";
    }

    public Integer getAppDownloadConfigProperty(final String property) {
        JSONObject config = appConfigService.getCustomProp(AppConfigService.APP_DOWNLOAD_SMS_CONFIG);
        int propteryValue = DEFAULT_SMS_INTERVAL_IN_MINUTES;

        if (config.containsKey(property)) {
            propteryValue = Integer.parseInt(String.valueOf(config.get(property)));
        }

        return propteryValue;
    }

    public boolean sendSms(final String mobileNumber, final Integer userId) {
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        smsBusinessDO.setText(fcProperties.getProperty(FCProperties.MSG_APP_DOWNLOAD));
        smsBusinessDO.setReceiverNumber(mobileNumber);
        smsBusinessDO.setRegisteredUserEmail(FCSessionUtil.getLoggedInEmailId());

        logger.info("Sending TVC affiliate cashback SMS to mobile number : " + mobileNumber + ", userId : "
                    + userId + ", emailId : " + FCSessionUtil.getLoggedInEmailId());
        return smsService.sendSMSText(smsBusinessDO, SMS_SENDER);
    }

    private void createResponse(final boolean smsSent, final Model model) {
        if (smsSent) {
            model.addAttribute("status", "success");
            model.addAttribute("message", "Message sent successfully to your Mobile.");
        } else {
            model.addAttribute("status", "failure");
            model.addAttribute("message", "Attempt unsuccessful. Please try again.");
        }
    }
}
