package com.freecharge.freefund.offersinfo.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.mongo.repos.TvcAffiliateMobileRepository;
import com.mongodb.DBObject;

/**
 * This api saves unique email addresses of users to tvcAffiliateMobileRepository
 **/
@Controller
public class UserInputController {
    private static final String EMAIL_ID = "emailId";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private TvcAffiliateMobileRepository tvcAffiliateMobileRepo;

    @Csrf(exclude = true)
    @NoLogin
    @RequestMapping(value = "/userInput/email", method = RequestMethod.GET, produces = "application/json")
    public String saveMobileNumber(final HttpServletRequest request, final HttpServletResponse response,
                                   final Model model) throws IOException {
        String emailId = request.getParameter(EMAIL_ID);
        if (FCUtil.isEmpty(emailId) || !FCUtil.isEmailIdFormat(emailId)) {
            model.addAttribute("status", "failure");
            model.addAttribute("message", "Invalid Email Id. Please try again.");
            logger.error("Email Id " + emailId + " is not valid");
            return "jsonView";
        }

        DBObject dbEntry = tvcAffiliateMobileRepo.getDataByEmailId(emailId);
        if (null == dbEntry) {
            logger.debug("No entry found in DB for emailId " + emailId);
            tvcAffiliateMobileRepo.insertEmailId(emailId);
        }

        model.addAttribute("status", "success");
        model.addAttribute("message", "Thanks. We will keep you posted. You can always "
                           + "come back & check this space on Dec 10, 2014.");
        return "jsonView";
    }
}
