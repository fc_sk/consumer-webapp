package com.freecharge.freefund.offersinfo.services;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.mongo.repos.TvcAffiliateMobileRepository;

/**
 * This class contains utilities to get/insert entries from/to tvcAffiliateMobileRepository
 **/
@Service
public class TvcAffiliateMobileService {

    @Autowired
    private TvcAffiliateMobileRepository tvcRepository;

    public void saveMobileNumber(final String mobileNumber, final Integer userId,
                                 final Date smsSentDate, final Boolean smsSent) {
        this.tvcRepository.insert(mobileNumber, userId, smsSentDate, smsSent);
    }

    public void updateData(final String mobileNumber, final Date smsSentDate,
                           final Boolean smsSent, final Date updateAt) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(TvcAffiliateMobileRepository.SMS_SENT_DATE, smsSentDate);
        data.put(TvcAffiliateMobileRepository.SMS_SENT, smsSent);
        data.put(TvcAffiliateMobileRepository.UPDATED_AT, updateAt);
        tvcRepository.updateData(mobileNumber, data);
    }
}
