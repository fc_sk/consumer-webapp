package com.freecharge.freefund.services;

public class FreefundUsageHistoryServiceRequest {

    private Integer userId;
    private String  profileNumber;
    private String  serviceNumber;
    private Integer freefundClassId;
    private Integer inHours;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getProfileNumber() {
        return profileNumber;
    }

    public void setProfileNumber(String profileNumber) {
        this.profileNumber = profileNumber;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getFreefundClassId() {
        return freefundClassId;
    }

    public void setFreefundClassId(Integer freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public Integer getInHours() {
        return inHours;
    }

    public void setInHours(Integer inHours) {
        this.inHours = inHours;
    }
}
