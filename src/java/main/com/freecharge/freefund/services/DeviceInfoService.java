package com.freecharge.freefund.services;


import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CityMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.mongo.entity.DeviceInfo;
import com.freecharge.mongo.repos.DeviceInfoRepository;
import com.mongodb.DBObject;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 12/16/14
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("deviceInfoService")
public class DeviceInfoService {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private DeviceInfoRepository deviceInfoRepository;
    
    @Autowired
    @Qualifier("couponServiceProxy")
    private ICouponService couponServiceProxy;

    public void addDeviceInfo(String uid, String imei, String imsi, String deviceId, String advertiserId, String rawAddress){
        if (FCStringUtils.isBlank(rawAddress)){
            return;
        }
        //Parse raw address and get address object
        JSONObject addressObject = null;
        try {
            addressObject = FCUtil.getAddressFromAppLocation(rawAddress);
            logger.debug(String.format("address parsing successful for raw address: %s", rawAddress));
        }catch (Exception e){
            logger.error(String.format("app_address_parse_error: uid: %s, imei: %s and address: %s", uid, imei, rawAddress), e);
            return;
        }
        //Successfully parsed the address
        //Try to get our cityId for city name deduced from raw address
        Integer cityId = null;
        try {
        	Map<String, CityMaster> cityNameMap = this.couponServiceProxy.getAllCitiesNameMasterMap();
        	if(cityNameMap!=null){
        		cityId = cityNameMap.get((String)addressObject.get("city")).getCityMasterId();
        	}
        }catch (Exception e){
            logger.warn(String.format("Error getting cityId for name: %s", addressObject.get("city")));
        }

        //Get existing device info document
        DBObject infoObject = this.deviceInfoRepository.getDeviceInfo(uid, imei);

        //Prepare addressMap with time
        Map<String, Object> addressesMap = new HashMap<>();
        addressesMap.put("address", (String) addressObject.get("address"));
        addressesMap.put("time", new Date());

        if (infoObject!=null){
            logger.info(String.format("device_info found for UID: %s and imei: %s", uid, imei));
            this.deviceInfoRepository.updateCurrentCity(uid, imei, (String) addressObject.get("city"),
                    addressesMap, cityId, rawAddress);
        }else {
            logger.info(String.format("No device_info found for UID: %s and imei: %s. Hence creating.", uid, imei));
            DeviceInfo deviceInfo = new DeviceInfo(imei, imsi, deviceId, advertiserId, uid);
            deviceInfo.setCityId(cityId);
            deviceInfo.setCurAddress((String) addressObject.get("address"));
            deviceInfo.setCurCity((String) addressObject.get("city"));

            List<Map<String, Object>> addresses = new ArrayList<>();
            addresses.add(addressesMap);

            List<String> cities = new ArrayList<>();
            cities.add((String) addressObject.get("city"));

            List<String> rawAddresses = new ArrayList<>();
            rawAddresses.add(rawAddress);

            deviceInfo.setCities(cities);
            deviceInfo.setAddresses(addresses);
            deviceInfo.setRawAddresses(rawAddresses);

            //Add this to repo
            this.deviceInfoRepository.addDeviceInfo(deviceInfo);
        }
    }

    public String getCityFromDeviceInfo(String uid, String imei){
        DBObject deviceInfo = this.deviceInfoRepository.getDeviceInfo(uid, imei);

        if (deviceInfo!=null){
            return deviceInfo.get("cur_city")!=null?(String) deviceInfo.get("cur_city"):null;
        }
        logger.info("No device info found for imei: "+imei);
        return null;
    }
}
