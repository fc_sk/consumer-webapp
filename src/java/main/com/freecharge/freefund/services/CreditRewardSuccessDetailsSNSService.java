package com.freecharge.freefund.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.sns.AmazonSNSProp;
import com.freecharge.sns.AmazonSNSService;

@Service
public class CreditRewardSuccessDetailsSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + CreditRewardSuccessDetailsSNSService.class.getName());

    @Autowired
    public CreditRewardSuccessDetailsSNSService(@Qualifier("creditRewardSuccessDetailsSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }

}
