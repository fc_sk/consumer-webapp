package com.freecharge.freefund.services;


import com.freecharge.sns.AmazonSNSProp;
import com.freecharge.sns.AmazonSNSService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class PromocodeRedeemDetailsSNSService extends AmazonSNSService {

    private Logger logger = LoggingFactory.getLogger("amazon.sns." + PromocodeRedeemDetailsSNSService.class.getName());

    @Autowired
    public PromocodeRedeemDetailsSNSService(@Qualifier("promocodeRedeemDetailsSNSProp") AmazonSNSProp amazonSNSProp) {
        super(amazonSNSProp);
    }

}