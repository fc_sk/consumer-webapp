package com.freecharge.freefund.services;

import com.freecharge.freefund.dao.ReIssuedPromocodesReadDao;
import com.freecharge.freefund.dao.ReIssuedPromocodesWriteDao;
import com.freecharge.freefund.dos.entity.ReIssuedPromocode;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReIssuedPromocodesService {

    @Autowired
    private ReIssuedPromocodesWriteDao reIssuedPromocodeWriteDao;

    @Autowired
    private ReIssuedPromocodesReadDao reIssuedPromocodesReadDao;

    public void insertIntoReIssuedPromocodes(String orderId, long parentPromocodeId,
                                             long reIssuedPromocodeFreefundCouponId, int parentFreefundClassId) {
        ReIssuedPromocode reIssuedPromocode = new ReIssuedPromocode();
        reIssuedPromocode.setOrderId(orderId);
        reIssuedPromocode.setParentFreefundClassId(parentFreefundClassId);
        reIssuedPromocode.setParentPromocodeId(parentPromocodeId);
        reIssuedPromocode.setReIssuedPromocodeId(reIssuedPromocodeFreefundCouponId);
        reIssuedPromocodeWriteDao.insert(reIssuedPromocode);
    }

    public List<ReIssuedPromocode> getReIssuedPromocodeForOrderId(String orderId) {
        return reIssuedPromocodesReadDao.getReIssuedPromocodeForOrderId(orderId);
    }

    public List<ReIssuedPromocode> getReIssuedPromocodeForParentPromocodeId(Long parentPromocodeId) {
        return reIssuedPromocodesReadDao.getReIssuedPromocodeForParentPromocodeId(parentPromocodeId);
    }
}
