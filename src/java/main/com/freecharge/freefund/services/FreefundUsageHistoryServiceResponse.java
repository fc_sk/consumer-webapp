package com.freecharge.freefund.services;

public class FreefundUsageHistoryServiceResponse {

    private int applies;
    private int blocks;
    private int redeems;
    
    public int getApplies() {
        return applies;
    }
    public void setApplies(int applies) {
        this.applies = applies;
    }
    public int getBlocks() {
        return blocks;
    }
    public void setBlocks(int blocks) {
        this.blocks = blocks;
    }
    public int getRedeems() {
        return redeems;
    }
    public void setRedeems(int redeems) {
        this.redeems = redeems;
    }

}
