package com.freecharge.freefund.services;


import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.HomeService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dao.PromocodeRedeemDetailsReadDao;
import com.freecharge.freefund.dao.PromocodeRedeemDetailsWriteDao;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.dos.entity.PromocodeRedeemedDetails;

@Service("promocodeRedeemDetailsService")
public class PromocodeRedeemDetailsService {

    private static Logger logger = LoggingFactory.getLogger(PromocodeRedeemDetailsService.class);

    @Autowired
    private OrderIdWriteDAO orderIdDAO;

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private PromocodeRedeemDetailsWriteDao promocodeRedeemDetailsWriteDao;

    @Autowired
    private CartService cartService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private PromocodeRedeemDetailsReadDao promocodeRedeemDetailsReadDao;

    public int  insertIntoPromocodeRedeemDetails(FreefundCoupon freefundCoupon, String orderId, Integer userId,
                                                 String imeiNumber, String advertisementId, String deviceUniqueId,
                                                 String imsi, Boolean isEmulator) {
        try {

            Map<String, Object> rechargeDetails = homeService.getUserRechargeDetailsFromOrderId(orderId);
            List<OrderId> orderIdObjectList = orderIdDAO.getByOrderId(orderId);
            OrderId orderIdObject;
            Integer channelId = null;
            if (FCUtil.isEmpty(orderIdObjectList)) {
                return 0;
            } else {
                orderIdObject = FCUtil.getFirstElement(orderIdObjectList);
                if (orderIdObject != null) {
                    channelId = orderIdObject.getChannelId();
                } else {
                    return 0;
                }
            }
            int isCouponOpted = 0;
            if (cartService.hasCoupons(orderId)) {
                isCouponOpted = 1;
            }
            Long freefundClassId = freefundCoupon.getFreefundClassId();
            FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
            Double discountValue = 0.0;
            if (freefundCoupon.getFreefundValue() <= 0.0) {
                discountValue = freefundCoupon.getDiscountValue(
                        Double.parseDouble(String.valueOf(rechargeDetails.get("amount"))),
                        freefundClass.getDiscountType(), freefundClass.getMaxDiscount());
            } else {
                discountValue = freefundCoupon.getFreefundValue().doubleValue();
            }

            PromocodeRedeemedDetails promocodeRedeemedDetails = new PromocodeRedeemedDetails();
            promocodeRedeemedDetails.setName(freefundClass.getName());
            promocodeRedeemedDetails.setFreefundClassId(freefundClassId);
            promocodeRedeemedDetails.setDiscountType(freefundClass.getDiscountType());
            promocodeRedeemedDetails.setPromoEntity(freefundClass.getPromoEntity());
            promocodeRedeemedDetails.setFreefundValue(freefundClass.getFreefundValue());
            promocodeRedeemedDetails.setFreefundCode(freefundCoupon.getFreefundCode());
            promocodeRedeemedDetails.setUserId(userId);
            promocodeRedeemedDetails.setOrderId(orderId);
            promocodeRedeemedDetails.setDiscountValue(discountValue);
            promocodeRedeemedDetails.setOperatorName(String.valueOf(rechargeDetails.get("operatorName")));
            promocodeRedeemedDetails.setCircleName(String.valueOf(rechargeDetails.get("circleName")));
            promocodeRedeemedDetails.setAmount(String.valueOf(rechargeDetails.get("amount")));
            promocodeRedeemedDetails.setServiceNumber(String.valueOf(rechargeDetails.get("mobileno")));
            promocodeRedeemedDetails.setChannelId(channelId);
            promocodeRedeemedDetails.setProductName(String.valueOf(rechargeDetails.get("productType")));
            promocodeRedeemedDetails.setCouponOpted(isCouponOpted);
            promocodeRedeemedDetails.setImeiNumber(imeiNumber);
            promocodeRedeemedDetails.setAdvertisementId(advertisementId);
            promocodeRedeemedDetails.setDeviceUniqueId(deviceUniqueId);
            promocodeRedeemedDetails.setImsi(imsi);
            promocodeRedeemedDetails.setIsEmulator(isEmulator);

            return promocodeRedeemDetailsWriteDao.insert(promocodeRedeemedDetails);
        } catch (Exception e) {
            logger.error("Error inserting into promocodeRedeemDetails table for order id " + orderId, e);
        }
        return 0;
    }

    public int insertIntoPromocodeRedeemDetailsForAddCash(FreefundCoupon freefundCoupon, String orderId, Integer userId, String amount) {
        try {
            Long freefundClassId = freefundCoupon.getFreefundClassId();
            FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
            Double discountValue = 0.0;
            if (freefundCoupon.getFreefundValue() <= 0.0) {
                discountValue = freefundCoupon.getDiscountValue(
                        Double.parseDouble(amount),
                        freefundClass.getDiscountType(), freefundClass.getMaxDiscount());
            } else {
                discountValue = freefundCoupon.getFreefundValue().doubleValue();
            }

            PromocodeRedeemedDetails promocodeRedeemedDetails = new PromocodeRedeemedDetails();
            promocodeRedeemedDetails.setName(freefundClass.getName());
            promocodeRedeemedDetails.setFreefundClassId(freefundClassId);
            promocodeRedeemedDetails.setDiscountType(freefundClass.getDiscountType());
            promocodeRedeemedDetails.setPromoEntity(freefundClass.getPromoEntity());
            promocodeRedeemedDetails.setFreefundValue(freefundClass.getFreefundValue());
            promocodeRedeemedDetails.setFreefundCode(freefundCoupon.getFreefundCode());
            promocodeRedeemedDetails.setUserId(userId);
            promocodeRedeemedDetails.setOrderId(orderId);
            promocodeRedeemedDetails.setDiscountValue(discountValue);
            promocodeRedeemedDetails.setOperatorName(null);
            promocodeRedeemedDetails.setCircleName(null);
            promocodeRedeemedDetails.setAmount(null);
            promocodeRedeemedDetails.setServiceNumber(null);
            promocodeRedeemedDetails.setChannelId(null);
            promocodeRedeemedDetails.setProductName(null);
            promocodeRedeemedDetails.setCouponOpted(0);
            promocodeRedeemedDetails.setImeiNumber(null);
            promocodeRedeemedDetails.setAdvertisementId(null);
            promocodeRedeemedDetails.setDeviceUniqueId(null);
            promocodeRedeemedDetails.setImsi(null);
            promocodeRedeemedDetails.setIsEmulator(null);

            return promocodeRedeemDetailsWriteDao.insert(promocodeRedeemedDetails);

        } catch (Exception e) {
            logger.error("Error inserting into promocodeRedeemDetails table for order id " + orderId, e);
        }
        return 0;

    }

    public List<PromocodeRedeemedDetails> getPromocodeRedeemDetailsForOrderId(String orderId) {
        return promocodeRedeemDetailsReadDao.getPromocodeRedeemDetailsForOrderId(orderId);
    }

    public List<PromocodeRedeemedDetails> getPromocodeRedeemDetailsByUserIdAndCampaign(long userId, Integer campaignId) {
        return promocodeRedeemDetailsReadDao.getPromocodeRedeemDetails(userId, campaignId);
    }
}
