package com.freecharge.freefund.services;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.freecharge.admin.controller.FreefundLargeGenerateUploadController;
import com.freecharge.common.amazon.s3.service.AmazonS3Service;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.mongo.repos.FreefundTemporaryFilesStatusRepository;
import com.freecharge.mongo.service.FreefundTemporaryFilesStatusService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.promo.util.PromocodeConstants;
import com.google.common.base.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static com.freecharge.common.util.FCUtil.isEmpty;

@Service
public class UploadCodesService {
    private static Logger logger = LoggingFactory.getLogger(UploadCodesService.class);

    @Autowired
    private FreefundTemporaryFilesStatusService freefundTemporaryFilesStatusService;

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private EmailService emailService;

    @Autowired
    @Qualifier("amazonS3Service")
    private AmazonS3Service amazons3Service;

    @Autowired
    private MetricsClient metricsClient;

    public void uploadCodes() throws InterruptedException {
        List<Map<String, String>> pendingJobsList = freefundTemporaryFilesStatusService.getDataByStatus(PromocodeConstants.STATUS_PENDING);
        for (Map<String, String> pendingJob : pendingJobsList) {
            String freefundClassId =  pendingJob.get(FreefundTemporaryFilesStatusRepository.CAMPAIGN_ID);
            String codePrefix = pendingJob.get(FreefundTemporaryFilesStatusRepository.PREFIX);
            String codeLength = pendingJob.get(FreefundTemporaryFilesStatusRepository.CODE_LENGTH);
            String time = pendingJob.get(FreefundTemporaryFilesStatusRepository.FILE_CREATION_TIMESTAMP);
            String numberOfFiles = pendingJob.get(FreefundTemporaryFilesStatusRepository.NUMBER_OF_FILES);
            String email = pendingJob.get(FreefundTemporaryFilesStatusRepository.CREATED_BY_EMAILID);
            String fileCount = pendingJob.get(FreefundTemporaryFilesStatusRepository.FILE_COUNT);

            uploadCodes(codePrefix, Integer.parseInt(codeLength), Integer.parseInt(numberOfFiles),
                    Long.parseLong(time), Long.parseLong(freefundClassId), email, fileCount);
        }
    }

    private void sendEmail(List<FreefundCoupon> duplicateFreefundCoupons, List<FreefundCoupon> replacementCoupons,
                           String freefundClassId, String email, int uploadCodesCount) {
        String name = freefundService.getFreefundClass(Long.parseLong(freefundClassId)).getName();
        if (!isEmpty(replacementCoupons) && replacementCoupons.size() > 0) {
            emailService.sendFreeFundCodeUploadNotificationEmailForLargeUploads(uploadCodesCount + " number of codes " +
                    "uploaded. " + replacementCoupons.size() +  " number of duplicate codes found. PFA duplicate codes " +
                    "with their replacements already uploaded in the system.", name, email, duplicateFreefundCoupons,
                    replacementCoupons);

        } else {
            emailService.sendFreeFundCodeUploadNotificationEmail(uploadCodesCount + " number of codes uploaded.", name,
                    email, null, null);
        }
    }

    private List<FreefundCoupon> generateReplacementCodes(long classId, String codePrefix, int codeLength, int size) {
        int numCodesToBeGenerated = size * 2; // Generate extra to check for duplicates!
        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        randomCodeGenerator.setPrefix(codePrefix);
        randomCodeGenerator.setNoOfCodes(numCodesToBeGenerated);
        randomCodeGenerator.setLength(codeLength);
        randomCodeGenerator.setType(RandomCodeGenerator.TYPE.ALPHANUMERIC);
        randomCodeGenerator.setCustomChar(true);

        List<FreefundCoupon> freefundCouponList = new ArrayList<>();
        List<String> freefundCodeList = new ArrayList<>();

        for (int count = 0 ; count < numCodesToBeGenerated ; count++) {
            String code = randomCodeGenerator.getCodeWithPrefix();
            FreefundCoupon freefundCoupon = new FreefundCoupon();
            freefundCoupon.setFreefundCode(code);
            freefundCoupon.setFreefundClassId(classId);
            Calendar calendar = Calendar.getInstance();
            freefundCoupon.setGeneratedDate(calendar.getTime());
            freefundCouponList.add(freefundCoupon);
            freefundCodeList.add(code);
        }
        List<FreefundCoupon> duplicateCoupons = getDuplicateFreefundCoupons(freefundCouponList, freefundCodeList);
        logger.info("Number of duplicate codes found in replacement codes : " + duplicateCoupons.size());
        freefundCouponList.removeAll(duplicateCoupons);
        logger.info("Number of replacement coupons post duplicate removal " + freefundCouponList.size());
        return freefundCouponList;
    }

    private List<FreefundCoupon> getDuplicateFreefundCoupons(List<FreefundCoupon> freefundCouponList,
                                                             List<String> freefundCodeList) {
        List<String> duplicateCodes = getDuplicateFreefundCodes(freefundCodeList);
        List<FreefundCoupon> duplicateFreefundCoupons = new ArrayList<>();
        for (FreefundCoupon freefundCoupon : freefundCouponList) {
            if (duplicateCodes.contains(freefundCoupon.getFreefundCode())) {
                duplicateFreefundCoupons.add(freefundCoupon);
            }
        }
        return duplicateFreefundCoupons;
    }

    private List<String> getDuplicateFreefundCodes(List<String> freefundCodeList) {
        return freefundService.getFreefundCodes(freefundCodeList);
    }

    public void uploadCodes(String codePrefix, int codeLength, int numberOfFiles, long time, long classId,
                            String email, String processFilesCount) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        try {
            freefundTemporaryFilesStatusService.updateStatus(time, PromocodeConstants.STATUS_UNDER_PROCESS);

            int fileCount = 0;

            if (FCUtil.isNotEmpty(processFilesCount) && FCUtil.isInteger(processFilesCount)) {
                fileCount = Integer.parseInt(processFilesCount);
            }

            while (fileCount < numberOfFiles) {
                String fileName = FreefundLargeGenerateUploadController.getFileName(codePrefix,
                        codeLength, fileCount + 1, time);
                String currentLine;

                Optional s3ObjectInputStreamOptional = amazons3Service.getFileFromS3(fileName, FCConstants.AWS_FREEFUND_BUCKET);
                if (s3ObjectInputStreamOptional.isPresent()) {
                    List<FreefundCoupon> freefundCouponList = new ArrayList<>();
                    List<String> freefundCodeList = new ArrayList<>();
                    S3ObjectInputStream s3ObjectInputStream = (S3ObjectInputStream) s3ObjectInputStreamOptional.get();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(s3ObjectInputStream));
                    while ((currentLine = reader.readLine()) != null) {
                        FreefundCoupon freefundCoupon = new FreefundCoupon();
                        freefundCoupon.setFreefundClassId(classId);
                        freefundCoupon.setFreefundCode(currentLine);
                        Calendar calendar = Calendar.getInstance();
                        freefundCoupon.setGeneratedDate(calendar.getTime());
                        freefundCouponList.add(freefundCoupon);
                        freefundCodeList.add(currentLine);
                    }
                    List<FreefundCoupon> duplicateFreefundCoupons =
                            getDuplicateFreefundCoupons(freefundCouponList, freefundCodeList);
                    logger.info("Number of duplicate codes found: " + duplicateFreefundCoupons.size() + " for "
                            + fileName + " of campaign " + classId);
                    freefundCouponList.removeAll(duplicateFreefundCoupons);
                    List<FreefundCoupon> replacementCoupons = generateReplacementCodes(classId, codePrefix, codeLength,
                            duplicateFreefundCoupons.size());
                    logger.info("Replacement coupons size : " + replacementCoupons.size() + " for "
                            + fileName + " of campaign " + classId);
                    List<FreefundCoupon> replacementCouponSubList;
                    if(replacementCoupons.size() > duplicateFreefundCoupons.size()){
                        replacementCouponSubList = replacementCoupons.subList(0, duplicateFreefundCoupons.size());
                    } else {
                        replacementCouponSubList = replacementCoupons;
                    }
                    freefundCouponList.addAll(replacementCouponSubList);
                    logger.info("Inserting codes of size " + freefundCodeList.size() + " for freefund class" + classId);
                    int uploadCodesCount = freefundService.uploadCodes(freefundCouponList);
                    sendEmail(duplicateFreefundCoupons, replacementCouponSubList, String.valueOf(classId), email,
                            uploadCodesCount);
                    reader.close();
                }
                freefundTemporaryFilesStatusService.updateFileCounter(time, String.valueOf(fileCount + 1 ));
                fileCount++;
            }
            freefundTemporaryFilesStatusService.updateStatus(time, PromocodeConstants.STATUS_COMPLETE);
        } catch (IOException e) {
            logger.error("Error running InsertCodes Job " + e);
            freefundTemporaryFilesStatusService.updateStatus(time, PromocodeConstants.STATUS_PENDING);
        }
        long executionTime = System.currentTimeMillis() - startTime;
        metricsClient.recordLatency("FreefundCodeUpload", numberOfFiles + ".ExecutionTime", executionTime);
    }
}
