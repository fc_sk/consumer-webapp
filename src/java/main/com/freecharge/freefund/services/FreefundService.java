package com.freecharge.freefund.services;


import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.CartItemsDAO;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.FreefundReadDAO;
import com.freecharge.app.domain.dao.jdbc.FreefundWriteDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeCampaignReadDAO;
import com.freecharge.app.domain.dao.jdbc.PromocodeCampaignWriteDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.CartItems;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponCartService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.SimpleReturn;
import com.freecharge.freefund.dos.PromocodeRedeemDetailsSNSDo;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.rulesengine.FreeFundRule;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mongo.repos.IMEIRepository;
import com.freecharge.mongo.repos.PromocodeMobileRepository;
import com.freecharge.mongo.repos.PromocodeRewardRepository;
import com.freecharge.mongo.repos.PromocodeUsageRepository;
import com.freecharge.mongo.repos.UserReferralPromocodeRepository;
import com.freecharge.mongo.repos.WindowsDeviceUniqueIdRepository;
import com.freecharge.mongo.service.UserReferralPromocodeService;
import com.freecharge.order.service.OrderMetaDataService;
import com.freecharge.order.service.model.OrderMetaData;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.promo.dao.PromocodeReadDao;
import com.freecharge.promo.entity.Promocode;
import com.freecharge.promo.util.PromocodeConstants;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.onecheck.model.WalletResponseDo;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.util.RandomCodeGenerator.TYPE;
import com.freecharge.wallet.CampaignCashRewardService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.web.apiController.CampaignFrameworkService;
import com.freecharge.web.service.PromocodeCampaignService;
import com.google.common.base.Optional;

@Service
public class FreefundService {

    public static final int             OFFER_TYPE_BIN        = 1;

    public static final String          PAYMENT_OPTION_ALL       = "all-payment-options";
    public static final String          PAYMENT_OPTION_ICICI     = "icici-netbanking";
    public static final String          PAYMENT_OPTION_HDFD      = "hdfc-debitcard";
    public static final String          METRIC_SERVICE_PROMOCODE = "promocode";
    public static final String          METRIC_SERVICE_FREEFUND  = "freefund";
    public static final String          METRIC_NAME_REDEEM       = "Redeem";
    public static final String          METRIC_NAME_BLOCK        = "Block";
    public static final String          METRIC_NAME_UNBLOCK      = "Unblock";
    public static final String          METRIC_NAME_APPLY        = "Apply";
    public static final String          METRIC_NAME_REMOVE       = "Remove";

    public static final String          METRIC_SERVICE_PROMOREWARD       = "promoreward";
    public static final String          METRIC_NAME_PROMOREWARD_GENERATE = "generate";
    public static final String          METRIC_NAME_PROMOREWARD_REDEEM   = "redeem";
    public static final String          METRIC_NAME_BLOCK_FAILURE = "BlockFailure";
    public static final String          METRIC_NAME_REDEEM_FAILED = "RedeemFailure";
    private static final String         METRIC_NAME_UNBLOCK_FAILURE = "UnblockFailure";

    // todo: move to config
    private List<String>                doubleCouponOfferList = Arrays.asList(PAYMENT_OPTION_HDFD, PAYMENT_OPTION_ICICI);

    @Autowired
    private CartItemsDAO                cartItemsDAO;

    @Autowired
    private CartService                 cartService;

    @Autowired
    private WalletService               walletService;

    @Autowired
    private FreeFundRule                freeFundRule;

    @Autowired
    private PgReadDAO                   pgDao;

    @Autowired
    protected OrderIdSlaveDAO              orderIdDAO;

    @Autowired
    private FreefundWriteDAO            freefundWriteDao;

    @Autowired
    private FreefundReadDAO             freefundReadDao;

    @Autowired
    protected MetricsClient             metricsClient;

    private final Logger                logger                = LoggingFactory.getLogger(getClass());

    @Autowired
    private UserServiceProxy 			userServiceProxy;

    @Autowired
    private FreefundUsageCounterService freefundUsageCounterService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private PromocodeRewardRepository promocodeRewardRepository;

    @Autowired
    private PromocodeMobileRepository promocodeMobileRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AppConfigService            appConfigService;

    @Autowired
    private PricingService pricingService;

    @Autowired
    private PromocodeCampaignWriteDAO promocodeCampaignWriteDAO;


    @Autowired
    private PromocodeCampaignService promocodeCampaignService;

    @Autowired
    private PromocodeRedeemDetailsService promocodeRedeemDetailsService;

    @Autowired
    private UserReferralPromocodeService userReferralPromocodeService;

    @Autowired
    private IMEIRepository imeiRepository;

    @Autowired
    private OrderMetaDataService orderMetaDataService;

    @Autowired
    private PromocodeCampaignReadDAO promocodeCampaignReadDAO;

    @Autowired
    private WindowsDeviceUniqueIdRepository deviceUniqueIdRepository;

    @Autowired
    private IPService ipService;

    @Autowired
    private PromocodeUsageRepository promocodeUsageRepository;

    @Autowired
    private PromocodeReadDao promocodeReadDao;

    @Autowired
    private OrderIdReadDAO orderIdReadDAO;

    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Autowired
    private PromocodeRedeemDetailsSNSService promocodeRedeemDetailsSNSService;

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    @Autowired
    private CampaignCashRewardService campaignCashRewardService;

    @Autowired
    @Qualifier("couponServiceProxy")
    private ICouponService couponServiceProxy;

    @Autowired
    private CouponCartService couponCartService;


    @Autowired
    private CartDAO cartDao;


    @Autowired
    private CampaignFrameworkService campaignFrameworkService;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    public ResponseStatus redeemToWallet(FreefundRequestBusinessDO freeFundBusinessDO,
            com.freecharge.app.domain.entity.jdbc.Users users) {
        FreefundCoupon freefundCoupon = freeFundBusinessDO.getFreefundCoupon();
        ResponseStatus responseStatus = new ResponseStatus();
        boolean blockSuccess = blockCouponCode(freeFundBusinessDO);
        logger.info(freefundCoupon.getFreefundCode() + " [ " + freefundCoupon.getFreefundClassId() + " ] " + " Redeeming coupon. blockSuccess : " + blockSuccess);
        if (blockSuccess) {
            WalletCreditRequest walletCreditRequest = new WalletCreditRequest();

            walletCreditRequest.setUserID(users.getUserId());

            walletCreditRequest.setMtxnId(UUID.randomUUID().toString() + "_" + freefundCoupon.getFreefundCode());
            walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);

            walletCreditRequest.setCallerReference(" [ " + freeFundBusinessDO.getCouponId() + " ] " + " Redeem "
                    + freeFundBusinessDO.getCouponCode());

            walletCreditRequest.setFundSource(Wallet.FundSource.REDEEM.toString());
            walletCreditRequest.setTxnAmount(new Amount(freeFundBusinessDO.getFreefundCoupon().getFreefundValue()));
            walletCreditRequest.setMetadata(getFreefundCoupon(freefundCoupon.getFreefundCode()).getFreefundCode());
            String walletTransactionId = null;
            String voucherId = null;
            WalletResponseDo walletResponseDo = null;
            try {
                walletResponseDo =  campaignCashRewardService.credit(walletCreditRequest,
                        OneCheckWalletService.OneCheckTxnType.CREDIT_FREEFUND, freefundCoupon.getFreefundCode(), "DUMMY",
                        fcProperties.getProperty(FCConstants.PROMOTION_CORP_ID));
                walletTransactionId = walletResponseDo.gettransactionId();
            } catch (Exception e) {
                logger.error(freefundCoupon.getFreefundCode() + " [ "+freefundCoupon.getFreefundClassId()+" ] "+" Wallet balance exceeded limit, can not redeem coupon" + e, e);
            }

            if (walletResponseDo == null || !walletResponseDo.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {
                logger.info(freefundCoupon.getFreefundCode() + " [ "+freefundCoupon.getFreefundClassId()+" ] "+" Wallet credit error. ");
                responseStatus.setResult("Credit to wallet failed");
                responseStatus.setStatus("failure");
                releaseCouponCode(freeFundBusinessDO.getFreefundCoupon(), null, false,
                        "User wallet credit of freefund failed. Hence unblocking code.", false, null);
            } else {
                if (!FCUtil.isEmpty(walletTransactionId) && FCUtil.isLong(walletTransactionId)) {
                    Long fkWalletTransactionId = Long.parseLong(walletTransactionId);
                    walletService.insertWalletItem(fkWalletTransactionId, freeFundBusinessDO.getCouponId(),
                            Wallet.FundSource.REDEEM.toString());
                }
                PromocodeRedeemDetailsSNSDo promocodeRedeemDetailsSNSDo = new PromocodeRedeemDetailsSNSDo();
                promocodeRedeemDetailsSNSDo.setOrderId("");
                promocodeRedeemDetailsSNSDo.setDiscountValue(String.valueOf(walletCreditRequest.getTxnAmount().getAmount()));
                promocodeRedeemDetailsSNSDo.setPromocode(freefundCoupon.getFreefundCode());
                promocodeRedeemDetailsSNSDo.setCallerReference(walletCreditRequest.getCallerReference());
                promocodeRedeemDetailsSNSDo.setPromoEntity(
                        getFreefundClass(freefundCoupon.getFreefundClassId()).getPromoEntity());
                try {
                    logger.info("Publish promocode redeem details notification " +
                            PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo));
                    promocodeRedeemDetailsSNSService.publish(walletCreditRequest.getMtxnId(), "{\"message\":" +
                            PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo) + "}");
                } catch (IOException e) {
                    logger.error("Error publishing freefund success event to SNS ", e);
                }
                logger.info(freefundCoupon.getFreefundCode() +
                        " ["+freefundCoupon.getFreefundClassId()+"] "+" Wallet credit done. ");
                markFreefundRedeemedForCoupon(freeFundBusinessDO.getFreefundCoupon(), users.getUserId());
                responseStatus.setStatus("redeemed");
            }

        } else {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "+" Blocking failed for redemption : " + blockSuccess);
            responseStatus.setStatus("failure");
            responseStatus.setResult("Code blocking was unsuccessfull");
        }
        return responseStatus;
    }

    public List<String> getAllowablePGList(CartBusinessDo cartBusinessDo, String pg) {
        logger.info("Request received for lookupId : " + cartBusinessDo.getLookupID() + ", pg : " + pg);
        List<String> allowablePGList = new ArrayList<String>();
        if (pg != null && pg.equals(PaymentConstants.ALL)) {
            allowablePGList.add(PaymentConstants.ALL);
        } else {
            if (pricingService.isTotalPayableamountIsZero(cartBusinessDo)) {
                allowablePGList.add(PaymentConstants.ZEROPAY_PAYMENT_OPTION);
            } else if (this.hasICICINetBankFreefund(cartBusinessDo)) {
                allowablePGList.add(PaymentConstants.ICICNETBANKING_PAYMENT_OPTION);
            } else if (this.hasHDFCDebitCardFreefund(cartBusinessDo)) {
                allowablePGList.add(PaymentConstants.HDFCDEBITCARD_PAYMENT_OPTION);
            } else {
                allowablePGList.add(PaymentConstants.ALL);
            }
        }
        return allowablePGList;
    }

    /*
     * This method is used to get allowed PG list depends upon Payment amount / Freefund Payment Condition etc.
     * */
    public List<String> getAllowablePGList(CartBusinessDo cartBusinessDo, int channel, String appVersion) {

        final int VALID_APP_VERSION = 47;
        List<String> allowablePGList = new ArrayList<String>();

        if (pricingService.isTotalPayableamountIsZero(cartBusinessDo)) {

            allowablePGList.add(PaymentConstants.ZEROPAY_PAYMENT_OPTION);

        } else if (channel == FCConstants.ANDROID_APP_CHANNEL_ID && !FCUtil.isEmpty(appVersion)) {

            int intAppVersion = Integer.parseInt(appVersion);

            if(intAppVersion >= VALID_APP_VERSION && this.containsFreeFundItem(cartBusinessDo.getLookupID())) {

                allowablePGList = getPaymentTypes(cartBusinessDo);
            }
        } else if (channel != FCConstants.ANDROID_APP_CHANNEL_ID && this.containsFreeFundItem(cartBusinessDo.getLookupID())){
            allowablePGList = getPaymentTypes(cartBusinessDo);
        }

        if(FCUtil.isEmpty(allowablePGList)) {
            allowablePGList.add(PaymentConstants.ALL);
        }

        logger.info("Allowable PG List for lookupId : " + cartBusinessDo.getLookupID() + ", fcChannel : " + channel
                + ", appVersion : " + appVersion + " is " + allowablePGList);

        return allowablePGList;
    }
    
    public List<String> getAllowablePGList(String lookUpId, int channel, String appVersion) {
    	logger.info("input params , lookUpId :  "+lookUpId+", channel : "+channel+", appVersion : "+appVersion);
        final int VALID_APP_VERSION = 47;
        List<String> allowablePGList = new ArrayList<String>();

        if (channel == FCConstants.ANDROID_APP_CHANNEL_ID && !FCUtil.isEmpty(appVersion)) {
        	logger.info("inside if");
            int intAppVersion = Integer.parseInt(appVersion);
        	logger.info("intAppVersion : "+intAppVersion);
            if(intAppVersion >= VALID_APP_VERSION && this.containsFreeFundItem(lookUpId)) {
            	logger.info("inside if2");
                allowablePGList = getPaymentTypes(lookUpId);
            	logger.info((allowablePGList));
            }
        } else if (channel != FCConstants.ANDROID_APP_CHANNEL_ID && this.containsFreeFundItem(lookUpId)){
        	logger.info("inside elseif");
            allowablePGList = getPaymentTypes(lookUpId);
        }
        logger.info("allowablePGList : "+allowablePGList);
        if(FCUtil.isEmpty(allowablePGList)) {
            allowablePGList.add(PaymentConstants.ALL);
        }

        logger.info("Allowable PG List for lookupId : " + lookUpId + ", fcChannel : " + channel
                + ", appVersion : " + appVersion + " is " + allowablePGList);

        return allowablePGList;
    }

    public List<String> getPaymentTypes(CartBusinessDo cartBusinessDo) {
        List<String> allowablePGList = new ArrayList<>();
        List<String> promocodeSpecificPGList = null;
        try {
            Long freefundCouponId = this.getFreefundCouponIdInCartForLookupId(cartBusinessDo.getLookupID());
            FreefundCoupon freefundCoupon = this.getFreefundCoupon(freefundCouponId);
            FreefundClass freefundClass = this.getFreefundClass(freefundCoupon.getFreefundClassId());

            promocodeSpecificPGList = campaignFrameworkService.getAllowedPGListFromNewCF(freefundClass.getId());
        } catch (Exception e) {
            logger.info("Error getting PGList for uniqueId " + cartBusinessDo.getLookupID());
        }

        if(promocodeSpecificPGList != null && promocodeSpecificPGList.size() > 0) {
            allowablePGList.addAll(promocodeSpecificPGList);
        }
        return allowablePGList;
    }
    
    public List<String> getPaymentTypes(String lookUpId) {
        List<String> allowablePGList = new ArrayList<>();
        String all = new String("ALL");
        List<String> promocodeSpecificPGList = null;
        try {
        	Map<String, Object> result = campaignServiceClient.getIntxnData(lookUpId, 1);
        	String couponCode;
        	if(result.get("PROMOCODE") == null){
        		allowablePGList.add(all);
        		return allowablePGList;
        	}else{
        		couponCode = (String) result.get("PROMOCODE");
        	}
            FreefundCoupon freefundCoupon = this.getFreefundCoupon(couponCode);
            FreefundClass freefundClass;
            if(freefundCoupon != null){
            	freefundClass = this.getFreefundClass(freefundCoupon.getFreefundClassId());
            }else{
            	allowablePGList.add(all);
            	return allowablePGList;
            }

            promocodeSpecificPGList = campaignFrameworkService.getAllowedPGListFromNewCF(freefundClass.getId());
        } catch (Exception e) {
            logger.info("Error getting PGList for uniqueId " + lookUpId);
        }

        if(promocodeSpecificPGList != null && promocodeSpecificPGList.size() > 0) {
            allowablePGList.addAll(promocodeSpecificPGList);
        }
        return allowablePGList;
    }

    @Transactional
    public void saveCartWithFreeFund(FreefundRequestBusinessDO freeFundBusinessDO) {// used
        logger.info("[saveCartWithFreeFund] Saving freefund " + freeFundBusinessDO.getCouponCode());
        CartItems cartItems = new CartItems();
        Cart cart = new Cart();
        int cartId = 0;
        if (freeFundBusinessDO.getCartBusinessDo() == null) {
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            cart.setLookupId(freeFundBusinessDO.getLookupID());
            cart.setCreatedOn(currentTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentTimestamp);
            calendar.add(Calendar.DATE, 1);
            cart.setExpiresOn(new Timestamp(calendar.getTime().getTime()));
            cartId = cartDao.saveCart(cart);
        } else {
            cartId = freeFundBusinessDO.getCartBusinessDo().getId().intValue();
        }

        cart.setCartId(cartId);
        cartItems.setCart(cart);
        cartItems.setFkItemId(freeFundBusinessDO.getCouponId().intValue());
        cartItems.setEntityId(FCConstants.ENTITY_NAME_FOR_FREEFUND);

        FreefundCoupon freefundCoupon = freeFundBusinessDO.getFreefundCoupon();
        FreefundClass freefundClass = this.getFreefundClass(freefundCoupon.getFreefundCode());

        Double rechargeAmount = cartService.getRechargeAmount(freeFundBusinessDO.getCartBusinessDo());
        String discountType = freefundClass.getDiscountType();
        Float maxDiscount = freefundClass.getMaxDiscount();
        cartItems.setPrice(freefundCoupon.getDiscountValue(rechargeAmount, discountType, maxDiscount));
        cartItems.setQuantity(1);
        if(FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundClass.getPromoEntity()) || FreefundClass.PROMO_ENTITY_CASHBACK.equals(freefundClass.getPromoEntity())) {
            cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_FREEFUND_CASHBACK);
            cartItems.setDisplayLabel(freeFundBusinessDO.getFreefundCoupon().getFreefundCode()+"(CASHBACK)");
        } else {
            cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_FREEFUND_DISCOUNT);
            cartItems.setDisplayLabel(freeFundBusinessDO.getFreefundCoupon().getFreefundCode());
        }

        cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
        cartItems.setIsDeleted(false);

        if (!cartService.isFreeFundPresent(cartItems)) {
            int status = cartService.saveCartItems(cartItems);
            if (status != 1) {
                logger.error("Failed to save cart items " + freeFundBusinessDO.getFreefundCoupon() + " for " +
                        freeFundBusinessDO.getEmail());
            }
        } else {
            logger.info("[saveCartWithFreeFund] freefund save failed, cart already has a freefund. Removing previous freefund entry..");
            removeFreefundFromCart(freeFundBusinessDO);
            cartService.saveCartItems(cartItems);
        }
    }

    @Transactional
    public void saveCartWithFreeFundForAddCash(FreefundRequestBusinessDO freeFundBusinessDO, Integer userId) {// used
        logger.info("[saveCartWithFreeFund] Saving freefund " + freeFundBusinessDO.getCouponCode());
        CartItems cartItems = new CartItems();
        Cart cart = new Cart();
        int cartId = 0;
        if (freeFundBusinessDO.getCartBusinessDo() == null) {
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            cart.setLookupId(freeFundBusinessDO.getLookupID());
            cart.setFkUserId(userId);
            cart.setCreatedOn(currentTimestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentTimestamp);
            calendar.add(Calendar.DATE, 1);
            cart.setExpiresOn(new Timestamp(calendar.getTime().getTime()));
            cartId = cartDao.saveCart(cart);
        } else {
            cartId = freeFundBusinessDO.getCartBusinessDo().getId().intValue();
        }

        cart.setCartId(cartId);
        cartItems.setCart(cart);
        cartItems.setFkItemId(freeFundBusinessDO.getCouponId().intValue());
        cartItems.setEntityId(FCConstants.ENTITY_NAME_FOR_FREEFUND);

        FreefundCoupon freefundCoupon = freeFundBusinessDO.getFreefundCoupon();
        FreefundClass freefundClass = this.getFreefundClass(freefundCoupon.getFreefundCode());

        cartItems.setPrice(freefundCoupon.getFreefundValue().doubleValue());
        cartItems.setQuantity(1);
        if(FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundClass.getPromoEntity()) || FreefundClass.PROMO_ENTITY_CASHBACK.equals(freefundClass.getPromoEntity())) {
            cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_FREEFUND_CASHBACK);
            cartItems.setDisplayLabel(freeFundBusinessDO.getFreefundCoupon().getFreefundCode()+"(CASHBACK)");
        } else {
            cartItems.setFlProductMasterId(FCConstants.PRODUCT_ID_FREEFUND_DISCOUNT);
            cartItems.setDisplayLabel(freeFundBusinessDO.getFreefundCoupon().getFreefundCode());
        }

        cartItems.setAddedOn(new Timestamp(System.currentTimeMillis()));
        cartItems.setIsDeleted(false);

        if (!cartService.isFreeFundPresent(cartItems)) {
            cartService.saveCartItems(cartItems);
        } else {
            logger.info("[saveCartWithFreeFund] freefund save failed, cart already has a freefund : "
                    + freeFundBusinessDO.getCouponCode());
        }
    }

    @Transactional
    public void removeFreefundFromCart(FreefundRequestBusinessDO freeFundBusinessDO) {

        /*
         * used only in CheckoutSrvice, if cart has a freefund, and invalid
         * payment options Else, never modify cart by deleting cart item
         */
        logger.info("[removeFreefundFromCart] Removing FreeFund from cart associated with lookup Id : "
                + freeFundBusinessDO.getLookupID());
        Cart cart = new Cart();
        if (freeFundBusinessDO.getCartBusinessDo() != null) {
            cart.setCartId(freeFundBusinessDO.getCartBusinessDo().getId().intValue());

            List<CartItems> cartItemsToUpdate = cartItemsDAO.findByFkCart(cart);

            if (cartItemsToUpdate != null && !cartItemsToUpdate.isEmpty()) {
                for (CartItems cartItemToUpdate : cartItemsToUpdate) {
                    if (FCConstants.FFREEFUND_PRODUCT_TYPE.equals(cartItemToUpdate.getEntityId())) {
                        cartItemToUpdate.setIsDeleted(true);
                        cartItemToUpdate.setDeletedOn(new Timestamp(System.currentTimeMillis()));
                        cartItemsDAO.save(cartItemToUpdate);
                    }
                }
            }
            logger.info("Successfully removed FreeFund from cart associated with lookup Id : "
                    + freeFundBusinessDO.getLookupID());
        } else {
            logger.info("Cart not found, hence not removing.");
        }
    }

    @Transactional
    public void removeFreefundFromCart(String lookupId) {

        logger.info("[removeFreefundFromCart] Removing FreeFund from cart associated with lookup Id : "
                + lookupId);
        Cart cart = new Cart();
        CartBusinessDo cartBusinessDo = cartService.getCart(lookupId);
        if (cartBusinessDo != null) {
            cart.setCartId(cartBusinessDo.getId().intValue());

            List<CartItems> cartItemsToUpdate = cartItemsDAO.findByFkCart(cart);

            if (cartItemsToUpdate != null && !cartItemsToUpdate.isEmpty()) {
                for (CartItems cartItemToUpdate : cartItemsToUpdate) {
                    if (FCConstants.FFREEFUND_PRODUCT_TYPE.equals(cartItemToUpdate.getEntityId())) {
                        cartItemToUpdate.setIsDeleted(true);
                        cartItemToUpdate.setDeletedOn(new Timestamp(System.currentTimeMillis()));
                        cartItemsDAO.save(cartItemToUpdate);
                    }
                }
            }
            logger.info("Successfully removed FreeFund from cart associated with lookup Id : "
                    + lookupId);
        } else {
            logger.info("Cart not found, hence not removing.");
        }
    }


    public int getBlockedCountEmail(String emailId, Long freefundClassId) {
        return freefundUsageCounterService.getBlockedCountForEmail(emailId, freefundClassId);
    }

    public int getReedeemedCountEmail(String emailId, Long freefundClassId) {
        return freefundUsageCounterService.getRedeemedCountForEmail(emailId, freefundClassId);
    }

    public int getBlockedCountMobile(String mobileNo, Long freefundClassId) {
        return freefundUsageCounterService.getBlockedCountForNumber(mobileNo, freefundClassId);
    }

    public int getReedeemedCountMobile(String mobileNo, Long freefundClassId) {
        return freefundUsageCounterService.getRedeemedCountForNumber(mobileNo, freefundClassId);
    }

    public int getBlockedCountForProfileNumber(String mobileNo, Long freefundClassId) {
        return freefundUsageCounterService.getBlockedCountForProfileNumber(mobileNo, freefundClassId);
    }

    public int getReedeemedCountForProfileNumber(String mobileNo, Long freefundClassId) {
        return freefundUsageCounterService.getRedeemedCountForProfileNumber(mobileNo, freefundClassId);
    }


    public boolean blockCouponCode(FreefundRequestBusinessDO freeFundBusinessDO) { // used
        String couponCode = freeFundBusinessDO.getCouponCode();
        logger.info("[blockCouponCode] Blocking couponCode : " + couponCode);
        String promoEntity = freeFundBusinessDO.getFreefundCoupon().getPromoEntity();
        Long freefundClassId = freeFundBusinessDO.getFreefundCoupon().getFreefundClassId();
        boolean blockSuccess = false;
        String email = freeFundBusinessDO.getEmail();
        Integer userId = oneCheckWalletService.getOneCheckUserDetailsFromIMS(email).getFcUserId();
        String lookupId = freeFundBusinessDO.getLookupID();
        String orderId = orderIdDAO.getOrderIdForLookUpId(lookupId);
        String phoneNo = freeFundBusinessDO.getServiceNumber();

        if (FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(promoEntity)
                || FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(promoEntity)) {
            PromocodeCampaign promocodeCampaign = new PromocodeCampaign();
            promocodeCampaign.setUserId(userId);
            promocodeCampaign.setFkPromocodeId(freeFundBusinessDO.getFreefundCoupon().getId().intValue());
            promocodeCampaign.setPhoneNo(phoneNo);
            promocodeCampaign.setOrderId(orderId != null?orderId:lookupId);
            promocodeCampaign.setStatus(FCConstants.STATUS_BLOCKED);
            promocodeCampaign.setCreatedDate(FCUtil.currentDate());
            Integer returnVal = -1;
            try {
                returnVal = promocodeCampaignService.insertPromocodeCampaign(promocodeCampaign);
            } catch (Exception e) {
                logger.error("Promocode campaign entry insert failed for orderId " + orderId, e);
            }
            blockSuccess = (returnVal != -1);
        } else {
            blockSuccess = freefundWriteDao.blockCouponCode(couponCode,
                        freeFundBusinessDO.getFreefundCoupon().getId(), freeFundBusinessDO.getEmail(), phoneNo);
        }

        // Increment usage count.
        if (blockSuccess) {
            if (promoEntity != null && FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService.METRIC_NAME_BLOCK, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            } else {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_BLOCK, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            }
        } else {
            if (promoEntity != null && FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND,
                        FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            } else {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                        FreefundService.METRIC_NAME_BLOCK_FAILURE, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            }
        }
        // End increment usage count.

        logger.info("[blockCouponCode]  couponCode blocked : " + couponCode);
        return blockSuccess;
    }

    /*
     * DO NOT MAKE THIS FUNCTION PUBLIC. INSTEAD WRITE A INFORMATIVE FUNCTION TO CALL THIS
     * Generally in all the cases the shouldForce is false, But when the function releaseCouponCode
     * is called from  resetCouponFromCSTool() method the shouldForce will be true.
     * Cases to look into
     * 1. How Freefund code released from user account.
     * 2. How Freefund code released from admin tool.
     * 3. How Promocode code released from user account.
     * 4. How Promocode code released from admin tool.
     * 5. How to realse Generic promocode from user account and admin tool
     */
    @Transactional
    protected void releaseCouponCode(FreefundCoupon coupon, String orderId, boolean deleteCartItem, String why, boolean shouldForce,
                                     OrderMetaData orderMetaData) {
        Long couponCodeId = coupon.getId();
        coupon = getFreefundCoupon(couponCodeId);
        String emailId = coupon.getUsedEmailId();
        //Users user = userService.getUser(emailId);
        Users user = null;
        if (FCUtil.isNotEmpty(orderId)) {
            user = userServiceProxy.getUserByUserId(orderIdDAO.getUserIdFromOrderId(orderId));
        }
        String freefundCode = coupon.getFreefundCode();
        logger.info(freefundCode + " [" + coupon.getFreefundClassId() + "] " + " Releasing couponCode, why: " + why);
        String promoEntity = coupon.getPromoEntity();
        Long freefundClassId = coupon.getFreefundClassId();

        CartBusinessDo cartBusinessDo = null;
        String servicedNumber = null;

        boolean releaseSuccess = false;
        if (promoEntity != null && !FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
            cartBusinessDo = cartService.getCartByOrderId(orderId);
            servicedNumber = cartService.getRechargedNumber(cartBusinessDo.getLookupID());
        }

        unblockPromocode(coupon, cartBusinessDo != null?cartBusinessDo.getUserId():null, orderId);

        if (releaseSuccess) {
            if (promoEntity != null && FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService.METRIC_NAME_UNBLOCK, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            } else {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_UNBLOCK, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            }

            if (deleteCartItem && !getFreefundClass(coupon.getFreefundClassId()).getIsNewCampaign()) {
                //String lookupId = orderIdReadDAO.getLookupIdForOrderId(orderId);
                //CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
                FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
                freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
                freeFundRequestBusinessDO.setLookupID(cartBusinessDo.getLookupID());
                removeFreefundFromCart(freeFundRequestBusinessDO);
            }

            // Reset usage count.
            /*String email = coupon.getUsedEmailId();
            if (email == null) {
                logger.error("Email can not be null for freefund coupon code id: " + coupon.getFreefundCode());
                throw new IllegalStateException("Email can not be null for freefund coupon code id: " + coupon.getFreefundCode());
            }*/

            //Users user = userService.getUserById(cartBusinessDo.getUserId());
            if (user != null && !getFreefundClass(coupon.getFreefundClassId()).getIsNewCampaign()) {
                freefundUsageCounterService.decrementUseCountForEmail(Long.valueOf(user.getUserId()), coupon
                        .getFreefundClassId().intValue());
                freefundUsageCounterService.decrementUseCountForProfileNumber(user.getMobileNo(), coupon
                        .getFreefundClassId().intValue());
                if (promoEntity != null && !FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                    freefundUsageCounterService.decrementUseCountForNumber(servicedNumber, coupon
                            .getFreefundClassId().intValue());
                }

//                freefundUsageHistoryService.recordHistory(FreefundAction.UNBLOCK, user.getUserId()+"", coupon.getUsedServiceNumber(), user.getMobileNo(), Integer.valueOf(coupon.getFreefundClassId()+""),
//                        freefundCode);
//                if (orderMetaData != null) {
//                    imeiRepository.insert(orderMetaData.getImeiNumber(), orderMetaData.getAdvertisementId(),
//                            freefundClassId, user.getUserId(),
//                            freefundCode, FCConstants.UNBLOCKED);
//                    //deviceUniqueIdRepository.insert(FCConstants.FREEFUND_CLASS_ID, freefundClassId,
//                      //      orderMetaData.getDeviceUniqueId(), user.getUserId(), freefundCode, FCConstants.UNBLOCKED);
//                }
            }
        } else {
            if (promoEntity != null && FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService. METRIC_NAME_UNBLOCK_FAILURE, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            } else {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_UNBLOCK_FAILURE, "ffclass-"
                        + (freefundClassId != null ? freefundClassId : ""));
            }
        }
        logger.info(freefundCode + " [" + coupon.getFreefundClassId() + "] " + " Releasing couponCode done");
    }

    public boolean unblockPromocode(FreefundCoupon freefundCoupon, Integer userId, String orderId) {
        boolean releaseSuccess = false;
        if (FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon.getPromoEntity())
                || FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(freefundCoupon.getPromoEntity())) {
            releaseSuccess = promocodeCampaignWriteDAO.unblockCode(freefundCoupon.getId(), userId.longValue(), orderId);
        } else {
            releaseSuccess = freefundWriteDao.releaseCouponCode(freefundCoupon.getId(), true);
        }
        return releaseSuccess;
    }

    /**
     * Method for removing all traces of promocode from cart & mongo repos
     * The successful apply attempts corresponding to the user & promocode should be cleared up.
     *
     * @param freefundClassId
     * @param freefundCode
     * @param cartBusinessDo
     * @param applyCookie
     * @param ip
     * @param fingerPrint
     * @param userId
     * @param email
     * @return boolean
     */
    @Transactional
    public void removeFreefundCode(Long freefundClassId,String freefundCode,
                                    CartBusinessDo cartBusinessDo,
                                    String applyCookie,
                                    String ip,
                                    String fingerPrint,
                                    Integer userId, String email){
    	logger.info("Removing FreefunCode : " + freefundCode + " FF Class Id: " + freefundClassId
    	            + "LookupId: " + cartBusinessDo.getLookupID()
    	            + " Cookie: " + applyCookie
    	            + " IP: " + ip
    	            + "DeviceFP: " + fingerPrint);

        FreefundRequestBusinessDO freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
        freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
        freeFundRequestBusinessDO.setLookupID(cartBusinessDo.getLookupID());

        // 1. Remove freefund from cart
        removeFreefundFromCart(freeFundRequestBusinessDO);

        // 2. Decrement Apply count for all redemption repos
//        decremenetApplyCounts(userId,
//                freefundCode, freefundClassId,
//                cartService.getMobileNumber(cartBusinessDo),
//                applyCookie, ip,
//                fingerPrint, email);
        return;
    }

    /*
     * Method for decrementing Apply Counts from all Mongo Repos
     */

    private void decremenetApplyCounts(Integer userId,
                                       String freefundCode,
                                       Long freefundClassId,
                                       String serviceNumber,
                                       String applyCookie,
                                       String ip,
                                       String fingerPrint,
                                       String email) {

        logger.info("Decrementing Apply Counts : " + freefundCode + " FF Class Id: " + freefundClassId
                + "UserId: " + userId
                + " Cookie: " + applyCookie
                + " IP: " + ip
                + "DeviceFP: " + fingerPrint);


        // decrement Cookie Apply Count
        int decrementCount = -1;
        freefundUsageCounterService.updateApplyCountForCookie(email,
                                                        applyCookie,
                                                        freefundClassId.intValue(),
                                                        decrementCount);

        // decrement Device FP Apply Count
        freefundUsageCounterService.decrementApplyCountForFingerPrint(fingerPrint,userId, freefundCode, freefundClassId);

        // decrement IP Apply Count
        ipService.removeIP(ip, userId, freefundCode, freefundClassId);
    }

    /*
     * DO NOT CALL ANYWHERE ELSE EXCEPT IN /customertrail LOGIC
     */
    @Transactional
    public boolean resetCouponFromCSTool(FreefundCoupon freefundCoupon) {

        List<String> orderIdlist = orderIdReadDAO.getOrderIdListWithUniqueCodesUsed(freefundCoupon.getFreefundCode());
        if (orderIdlist != null && orderIdlist.size() >= 1) {
            String orderId = orderIdlist.get(0);
            releaseCouponCode(freefundCoupon, orderId, false, "Unblocking coupon code from CSTool", true, null);
            return true;
        }

        return false;
    }

    public boolean validateAndSetCouponId(FreefundRequestBusinessDO freefundRequestBusinessDO) {
        logger.info("Servicing freefund for coupon code " + freefundRequestBusinessDO.getCouponCode() + " email Id "
                + freefundRequestBusinessDO.getEmail());
        return freeFundRule.evaluate(freefundRequestBusinessDO);
    }

    @Transactional
    public Long getFreefundCouponIdForLookupId(String lookupId) {
        logger.info("get coupon associated with lookup Id : " + lookupId);
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(lookupId);
        if (cartItemsBusinessDO != null) {
            return cartItemsBusinessDO.getItemId().longValue();
        }
        return null;
    }

    @Transactional
    public boolean containsFreeFundItem(String lookupId) {
        CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
        return cartService.containsFreeFund(cartBusinessDo.getCartItems());
    }

    @Transactional
    public Double getFreeFundItemPriceByOrderId(String orderId) {
        if (orderId == null || orderId.trim().isEmpty()) {
            return 0.0;
        }
        String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
        return getFreeFundItemPrice(lookupId);
    }

    public String getLookupIdByOrderId(String orderId) {
        String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);
        return lookupId;
    }

    @Transactional
    public CartItemsBusinessDO getFreeFundItem(String lookupId) {
        CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
        return getFreeFundItem(cartBusinessDo);
    }

    @Transactional
    public CartItemsBusinessDO getFreeFundItem(CartBusinessDo cartBusinessDo) {

        List<CartItemsBusinessDO> cartItems = cartBusinessDo.getCartItems();

        for (CartItemsBusinessDO cartItem : cartItems) {
            if (FCConstants.FFREEFUND_PRODUCT_TYPE.equals(cartItem.getEntityId()) && !cartItem.getDeleted()) {
                return cartItem;
            }
        }

        return null;
    }

    @Transactional
    public Long getFreefundCouponIdInCartForLookupId(String lookupId) {
        logger.info("get coupon id associated with lookup Id : " + lookupId);

        return couponCartService.getFreefundCouponIdInCartForLookupId(lookupId);
    }

    @Transactional
    public Double getFreeFundItemPrice(String lookupId) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(lookupId);
        if (cartItemsBusinessDO != null) {
            return cartItemsBusinessDO.getPrice();
        }
        return 0.0;
    }

    @Transactional
    public String getFreeFundDisplayLabelByLookUpId(String lookupId) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(lookupId);
        if (cartItemsBusinessDO != null) {
            return cartItemsBusinessDO.getDisplayLabel();
        }
        return null;
    }

    @Transactional
    public boolean doesOrderContainFreeFund(String orderId) {
        logger.debug("Checking FreeFund coupon associated with Order Id : " + orderId);

        if (orderId == null || orderId.trim().isEmpty()) {
            // nothing to be done;
            return false;
        }

        String lookupId = orderIdDAO.getLookupIdForOrderId(orderId);

        return containsFreeFundItem(lookupId);
    }

    @Transactional
    public boolean isCancelledPaymentForOrderIdList(FreefundRequestBusinessDO freefundRequestBusinessDO,
            List<String> orderIdList) {
        if (orderIdList == null || orderIdList.isEmpty()) {
            return false;
        }

        boolean isPaymentCancelled = false;

        for (String orderId : orderIdList) {
            if (orderId == null) {
                return false;
            }

            if (pgDao.isCancelledPayment(orderId)) {
                isPaymentCancelled = true;
                break;
            }
        }

        return isPaymentCancelled;
    }

    public int uploadCodes(List<FreefundCoupon> freefundCodes) throws InterruptedException {
        int totalCouponsUploaded = 0;
        int count = 0;
        while(count < freefundCodes.size()) {
            int prevCount = count;
            JSONObject versionObject = appConfigService.getCustomProp(FCConstants.UPLOAD_CODE_CONFIG);
            int batchSize = Integer.parseInt(String.valueOf(versionObject.get(
                    FCConstants.BATCH_SIZE_FREEFUND_CODE)));
            if (count + batchSize < freefundCodes.size()) {
                count += batchSize;
            } else {
                count = freefundCodes.size();
            }
            Map<String, Object>[] batchValues = new Map[count - prevCount];
            int batchValuesCounter = 0;
            FreefundCoupon freefundCode = null;
            for (int i = prevCount; i < count; i++) {
               freefundCode = freefundCodes.get(i);
                Map<String, Object> paramMap = new HashMap<String, Object>();
                paramMap.put("freefundClassId", freefundCode.getFreefundClassId());
                paramMap.put("freefundCode", freefundCode.getFreefundCode());
                paramMap.put("freefundValue", freefundCode.getFreefundValue());
                paramMap.put("minRechargeValue", freefundCode.getMinRechargeValue());
                paramMap.put("promoEntity", freefundCode.getPromoEntity());
                batchValues[batchValuesCounter++] = paramMap;
            }
            totalCouponsUploaded += uploadCodes(batchValues);
            Thread.sleep(Integer.parseInt(String.valueOf(versionObject.get(FCConstants.SLEEP_TIME_UPLOAD_CODES))));
        }
        return totalCouponsUploaded;
    }

    @Transactional
    public int uploadCodes(Map<String, Object>[] freeFundCodeMapArray) {
        return freefundWriteDao.upload(freeFundCodeMapArray);
    }

    @Transactional
    public FreefundClass createFreefundClass(FreefundClass freefundClass) {
        return freefundWriteDao.createFreefundClass(freefundClass);
    }

    public boolean saveFreefundClass(FreefundClass freefundClass) {
        return freefundWriteDao.saveFreefundClass(freefundClass);
    }

    @Transactional
    public boolean insert(FreefundCoupon freefundCode) {
        return freefundWriteDao.insert(freefundCode);
    }

    public List<FreefundClass> getFreefundClasses() {
        return freefundReadDao.getFreefundClasses();
    }

    @Transactional
    public boolean allPaymentOptionsAllowed(Integer cartFkItemId) {
        FreefundCoupon freefundCoupon = freefundReadDao.getFreefundCoupon((long) cartFkItemId);
        if (PAYMENT_OPTION_ALL.equals(freefundCoupon.getPromoEntity())) {
            return true;
        }
        return false;
    }

    public boolean allPaymentOptionsAllowed(CartBusinessDo cartBusinessDo) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(cartBusinessDo);
        if (cartItemsBusinessDO != null) {
            return allPaymentOptionsAllowed(cartItemsBusinessDO.getItemId());
        }
        return false;
    }

    public FreefundCoupon getFreefundCoupon(Long couponId) {
        return freefundReadDao.getFreefundCoupon(couponId);
    }

    public FreefundCoupon getFreefundCoupon(String couponCode) {
        return freefundReadDao.getFreefundCoupon(couponCode);
    }

    /**
     * Returns extra factor of coupons to be given away in case of a freefund.
     *
     * @param freeFundCoupon
     * @return
     */
    public Integer getCouponFactor(Integer freeFundCouponId) {
        int couponFactor = 1;

        FreefundCoupon freeFundCoupon = this.getFreefundCoupon((long) freeFundCouponId);

        if (doubleCouponOfferList.contains(freeFundCoupon.getPromoEntity())) {
            couponFactor = 2;
        }

        return couponFactor;
    }
    public FreefundClass getFreefundClassForName(String freefundClassName) {
        return freefundReadDao.getFreefundClass(freefundClassName);
    }

    public FreefundClass getFreefundClass(long freefundClassId) {
        return freefundReadDao.getFreefundClass(freefundClassId);
    }

    public FreefundClass getFreefundClass(String code) {
        FreefundCoupon freefundCoupon = freefundReadDao.getFreefundCoupon(code);
        FreefundClass freefundClass = freefundReadDao.getFreefundClass(freefundCoupon.getFreefundClassId());
        return freefundClass;
    }

    public Map<String, String> setDatamapForFreefundClass(FreefundClass freefundClass, Promocode promocode) {
        String dataMapStr = freefundClass.getDatamap();
        Map<String, String> freefundDataMap;
        if(FCUtil.isEmpty(dataMapStr)) {
            freefundDataMap = new HashMap<>();
        } else {
            freefundDataMap = FCStringUtils.jsonToMap(dataMapStr);
        }
        int codeLength = promocode.getCodeLength();
        if(codeLength > 0 ) {
            freefundDataMap.put(FCConstants.LENGTH, String.valueOf(codeLength));
            freefundClass.setCodeLength(String.valueOf(codeLength));
        }
        String prefix = promocode.getPrefix();
        if(FCUtil.isNotEmpty(prefix)) {
            freefundDataMap.put(PromocodeConstants.PREFIX, prefix);
            freefundClass.setPrefix(prefix);
        }
        long redeemConditionId = promocode.getRedeemConditionId();
        if (redeemConditionId > PromocodeConstants.NO_CONDITION_SET) {
            freefundDataMap.put(PromocodeConstants.REDEEM_CONDITON_ID, String.valueOf(redeemConditionId));
            freefundClass.setRedeemConditionId(String.valueOf(redeemConditionId));
        }
        String successMessageApply = promocode.getSuccessMessageApply();
        if(FCUtil.isNotEmpty(successMessageApply)) {
            freefundDataMap.put(FCConstants.SUCCESS_MESSAGE_PARAM_NAME, successMessageApply);
            freefundClass.setSuccessMessage(successMessageApply);
        }
        String successMessageRecharge = promocode.getSuccessMessageRecharge();
        if(FCUtil.isNotEmpty(successMessageRecharge)) {
            freefundDataMap.put(FCConstants.RECHARGE_SUCCESS_MESSAGE_PARAM_NAME, successMessageRecharge);
            freefundClass.setRechargeSuccessMessage(successMessageRecharge);
        }
        return freefundDataMap;
    }

    public FreefundCoupon getAvailableCoupon(final String couponCode) {
        return freefundReadDao.getAvailableCoupon(couponCode);
    }

    public FreefundCoupon getDetailsbyCoupnCode(String couponCode) {
        return freefundReadDao.getAvailableCoupon(couponCode);
    }

    public boolean hasICICINetBankFreefund(CartBusinessDo cartBusinessDo) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(cartBusinessDo);
        if (cartItemsBusinessDO == null)
            return false;
        FreefundCoupon freefundCoupon = getFreefundCoupon((long) cartItemsBusinessDO.getItemId());
        if (freefundCoupon == null)
            return false;
        if (FreefundService.PAYMENT_OPTION_ICICI.equals(freefundCoupon.getPromoEntity())) {
            return true;
        }
        return false;
    }

    public boolean hasHDFCDebitCardFreefund(CartBusinessDo cartBusinessDo) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(cartBusinessDo);
        if (cartItemsBusinessDO == null)
            return false;
        FreefundCoupon freefundCoupon = getFreefundCoupon((long) cartItemsBusinessDO.getItemId());
        if (freefundCoupon == null)
            return false;
        if (FreefundService.PAYMENT_OPTION_HDFD.equals(freefundCoupon.getPromoEntity())) {
            return true;
        }
        return false;
    }

    public boolean containsFreeFund(CartBusinessDo cartBusinessDo) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(cartBusinessDo);
        if (cartItemsBusinessDO != null)
            return true;
        return false;
    }

    public boolean containsFreeFund(String lookupId) {
        CartItemsBusinessDO cartItemsBusinessDO = getFreeFundItem(lookupId);
        if (cartItemsBusinessDO != null)
            return true;
        return false;
    }

    public boolean  markFreefundRedeemedForLookupId(FreefundCoupon freefundCoupon, String lookupId, Integer userId,
                                                    String orderId, String imeiNumber, String advertisementId,
                                                    String deviceUniqueId, String imsi, Boolean isEmulator) {
        boolean isSuccess = false;
        if (freefundCoupon != null) {
            String freefundCode = freefundCoupon.getFreefundCode();
            String promoEntity = freefundCoupon.getPromoEntity();
            FreefundClass freefundClass = getFreefundClass(freefundCode);
            isSuccess = markPromocodeRedeemed(freefundCoupon, userId, orderId);

            if(!promoEntity.equals(FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE)){
                int count = promocodeRedeemDetailsService.insertIntoPromocodeRedeemDetails(freefundCoupon, orderId,
                        userId, imeiNumber, advertisementId, deviceUniqueId, imsi, isEmulator);
                if (count != 1) {
                    logger.error("Couldnt insert into PromocodeRedeemDetails for: " + orderId + " .Got count: " + count);
                }
            }

//            if (freefundClass != null) {
//                //captureRedemptionData(freefundClass, freefundCoupon, userId, orderId, lookupId, imeiNumber,
//                  //      advertisementId, deviceUniqueId);
//            } else {
//                logger.info("Freefund class not found for " + orderId + "and coupon " + freefundCode);
//            }
            Long freefundClassId = freefundCoupon.getFreefundClassId();
            if (isSuccess) {
                if (!FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_REDEEM, "ffclass-"
                            + freefundClassId);
                } else {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService.METRIC_NAME_REDEEM, "ffclass-"
                            + freefundClassId);
                }

                logger.info("Incrementing promocode redemption count for campaign " + freefundClassId);
            } else {
                if (!FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity)) {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE,
                            FreefundService.METRIC_NAME_REDEEM_FAILED, "ffclass-" + freefundClassId);
                } else {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND,
                            FreefundService.METRIC_NAME_REDEEM_FAILED, "ffclass-" + freefundClassId);
                }
            }
        }

        return isSuccess;
    }

    public boolean markPromocodeRedeemed(FreefundCoupon freefundCoupon, Integer userId,
                                         String orderId) {
        boolean isSuccess = false;
        if (FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon.getPromoEntity())
                || FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(freefundCoupon.getPromoEntity())) {
            isSuccess = promocodeCampaignWriteDAO.redeemCode(freefundCoupon.getId(), userId.longValue(), orderId);
        } else {            
            isSuccess = freefundWriteDao.markFreefundRedeemed(freefundCoupon.getId());
        }
        return isSuccess;
    }

    public void captureRedemptionData(FreefundClass freefundClass, FreefundCoupon freefundCoupon, int userId,
                                       String orderId, String lookupId,  String imeiNumber, String advertisementId, String deviceUniqueId) {
        Users user = userServiceProxy.getUserByUserId(userId);
        logger.info("Starting to save freefund count for email save." + user.getEmail() + "order id" + orderId);
        freefundUsageCounterService.incrementRedeemCountForEmail(new Long(user.getUserId()),
                freefundClass.getId());
        String number = cartService.getRechargedNumber(lookupId);
        freefundUsageCounterService.incrementReedemCountForNumber(number, freefundClass.getId());
        freefundUsageCounterService.incrementReedemCountForProfileNumber(user.getMobileNo(),
                freefundClass.getId());
        String freefundCode = freefundCoupon.getFreefundCode();
        if (!FCUtil.isEmpty(imeiNumber) || !FCUtil.isEmpty(advertisementId)) {
            imeiRepository.insert(imeiNumber, advertisementId, freefundCoupon.getFreefundClassId(), userId,
                    freefundCode, FCConstants.REDEEMED);
        }
        if (!FCUtil.isEmpty(deviceUniqueId)) {
            deviceUniqueIdRepository.insert(FCConstants.FREEFUND_CLASS_ID, freefundCoupon.getFreefundClassId(),
                    deviceUniqueId, userId, freefundCode, FCConstants.REDEEMED);
        }
        logger.info("Finished saving freefund count for email save." + user.getEmail() + "order id" + orderId);
    }

    private void markFreefundRedeemedForCoupon(FreefundCoupon freefundCoupon, Integer userId) {
        freefundWriteDao.markFreefundRedeemed(freefundCoupon.getId());
        FreefundClass freefundClass = getFreefundClass( freefundCoupon.getFreefundCode());
        if (freefundClass != null) {
            freefundUsageCounterService.incrementRedeemCountForEmail(new Long(userId), freefundClass.getId());

            Users user = userServiceProxy.getUserByUserId(userId);
            //freefundUsageHistoryService.recordHistory(FreefundAction.REDEEM, userId + "", null, user.getMobileNo(), freefundClass.getId(), freefundCoupon.getFreefundCode());
        }
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService.METRIC_NAME_REDEEM, "ffclass-"
                + (freefundClass != null ? freefundClass.getId() : ""));

        FreefundClass rewardfreefundClass = getFreefundClass(freefundCoupon.getFreefundCode());
        if(rewardfreefundClass != null) {
            Long configuredFreefundClassId = Long.parseLong(rewardfreefundClass.getId() + "");

            if (freefundCoupon.getFreefundClassId() == configuredFreefundClassId) {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOREWARD, FreefundService.METRIC_NAME_PROMOREWARD_REDEEM, "ffclass-"
                        + configuredFreefundClassId);
            }
        }

    }

    public SimpleReturn validateClass(Users user, CartBusinessDo cartBusinessDo, FreefundCoupon freefundCoupon, String txnChannel) {
        boolean isFreefundCode = FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(freefundCoupon.getPromoEntity());
        String mobileNumber = null;
        Double rechargeAmount = null;
        ProductName productName = null;
        if(!isFreefundCode && cartBusinessDo != null){
            mobileNumber = cartService.getRechargedNumber(cartBusinessDo.getLookupID());
            rechargeAmount = cartService.getRechargeAmount(cartBusinessDo);
            productName = cartService.getPrimaryProduct(cartBusinessDo);

            if(txnChannel == null || txnChannel.length() == 0)
                txnChannel =  orderIdDAO.getChannelId(cartBusinessDo.getLookupID());
        }

        SimpleReturn simpleReturn = new SimpleReturn();

        if (freefundCoupon.getFreefundClassId() != null && freefundCoupon.getFreefundClassId() != 0){
            String orderId = orderIdDAO.getOrderIdForLookUpId(cartBusinessDo.getLookupID());
            OrderMetaData orderMetaData = orderMetaDataService.getOrderMetaData(orderId);

            String imeiNumber = null, advertisementId = null, deviceUniqueId = null;
            if (orderMetaData != null) {
                imeiNumber = orderMetaData.getImeiNumber();
                advertisementId = orderMetaData.getAdvertisementId();
                deviceUniqueId = orderMetaData.getDeviceUniqueId();
            } else {
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, "OrderMetaData", "EmptyRead");
            }

			if (!FCUtil.isEmpty(imeiNumber) || !FCUtil.isEmpty(advertisementId)) {
				logger.info("Incrementing block count for imei " + imeiNumber + " for orderId "
						+ orderIdDAO.getOrderIdForLookUpId(cartBusinessDo.getLookupID()));
				imeiRepository.insert(imeiNumber, advertisementId, freefundCoupon.getFreefundClassId(),
						user.getUserId(), freefundCoupon.getFreefundCode(), FCConstants.BLOCKED);
			} else if (!FCUtil.isEmpty(deviceUniqueId)) {
				deviceUniqueIdRepository.insert(FCConstants.FREEFUND_CLASS_ID, freefundCoupon.getFreefundClassId(),
						deviceUniqueId, user.getUserId(), freefundCoupon.getFreefundCode(), FCConstants.BLOCKED);
			}
		}

        Calendar todayCal = Calendar.getInstance();
        //todayCal.add(Calendar.DATE, -1);
        FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundCode());
        if (freefundClass != null && todayCal.after(freefundClass.getValidUpto())) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "+ "Attempt to use expired coupon ");
            simpleReturn.setErrorCode(ErrorCode.CODE_EXPIRED);
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "This code has expired.");
            return simpleReturn;
        }

        if(freefundClass != null && todayCal.before(freefundClass.getValidFrom())) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "+ "Attempt to use coupon before activation ");
            simpleReturn.setErrorCode(ErrorCode.CODE_INACTIVE_ERROR);
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "This code has not been activated yet.");
            return simpleReturn;

        }


        if (!isFreefundCode && rechargeAmount != null && rechargeAmount < new Double(freefundCoupon.getMinRechargeValue())) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "+ "Minimum recharge value failure ");
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "To avail this Promo code, you should have minimum recharge value of Rs."
                    + freefundCoupon.getMinRechargeValue());
            simpleReturn.setErrorCode(ErrorCode.MINRECHARGE_CHECK_FAIL);
            return simpleReturn;
        }

        simpleReturn.setValid(true);
        return simpleReturn;
    }

    //TODO: This code is common for validateClass. Need to work and avoid code repetition.
    public SimpleReturn validateClassOnApply(CartBusinessDo cartBusinessDo, FreefundCoupon freefundCoupon) {
        SimpleReturn simpleReturn = new SimpleReturn();
        FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundCode());
        Double rechargeAmount = null;
        Calendar todayCal = Calendar.getInstance();
        Calendar validFrom = freefundClass.getValidFrom();
        Calendar validUpto = freefundClass.getValidUpto();
        String promoEntity = freefundCoupon.getPromoEntity();
        boolean isFreefundCode = FCConstants.REDEEM_FREEFUND_TO_FC_BALANCE.equals(promoEntity);

        if (freefundClass != null && todayCal.after(validUpto)) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "
                    + "Attempt to use expired coupon ");
            simpleReturn.setErrorCode(ErrorCode.CODE_EXPIRED);
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "This code has expired.");
            return simpleReturn;
        }

        if(freefundClass != null && todayCal.before(validFrom)) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "
                    + "Attempt to use coupon before activation ");
            simpleReturn.setErrorCode(ErrorCode.CODE_INACTIVE_ERROR);
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "This code has not been activated yet.");
            return simpleReturn;

        }

        if(!isFreefundCode && cartBusinessDo != null){
            rechargeAmount = cartService.getRechargeAmount(cartBusinessDo);
        }

        if (!isFreefundCode && rechargeAmount != null && rechargeAmount < new Double(freefundCoupon.getMinRechargeValue())) {
            logger.info(freefundCoupon.getFreefundCode() + " ["+freefundCoupon.getFreefundClassId()+"] "+ "Minimum recharge value failure ");
            simpleReturn.setValid(false);
            simpleReturn.getObjMap().put(SimpleReturn.MESSAGE_KEY, "To avail this Promo code, you should have minimum recharge value of Rs."
                    + freefundCoupon.getMinRechargeValue());
            simpleReturn.setErrorCode(ErrorCode.MINRECHARGE_CHECK_FAIL);
            return simpleReturn;
        }

        simpleReturn.setValid(true);
        return simpleReturn;
    }

    public Map<String, Object> issueRewardCode(Integer userId, String orderId) {
        logger.info("Request promocode reward for orderId " + orderId + " , userId " + userId);
        Map<String, Object> rewardInRepo =  promocodeRewardRepository.doesRewardExist(userId);
        if(rewardInRepo == null) {
            logger.info("Generating reward for o" +
                    "rderId " + orderId + " , userId " + userId);
            FreefundCoupon freefundCoupon  = generateRewardCode();

            if(freefundCoupon != null) {
                FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundCode());
                rewardInRepo = new HashMap<String, Object>();
                rewardInRepo.put(PromocodeRewardRepository.ORDERID, orderId);
                rewardInRepo.put(PromocodeRewardRepository.PROMOCODE, freefundCoupon.getFreefundCode());
                rewardInRepo.put(PromocodeRewardRepository.VALUE, freefundClass.getFreefundValue());
                rewardInRepo.put(PromocodeRewardRepository.FFCLASS, freefundCoupon.getFreefundClassId());
                Calendar today = Calendar.getInstance();
                rewardInRepo.put(PromocodeRewardRepository.CREATEDAT, today.getTime());
                Calendar validUpto = Calendar.getInstance();
                validUpto.add(Calendar.DATE, 30);
                rewardInRepo.put(PromocodeRewardRepository.VALIDUPTO, validUpto.getTime());
                promocodeRewardRepository.updateOrCreateUserIdData(userId, rewardInRepo);
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOREWARD, FreefundService.METRIC_NAME_PROMOREWARD_GENERATE, "ffclass-"
                        + freefundCoupon.getFreefundClassId());
                Users user = userServiceProxy.getUserByUserId(userId);
                UserProfile userProfile = userServiceProxy.getUserProfile(userId);

                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

                String validUptoStr = sdf.format(rewardInRepo.get(PromocodeRewardRepository.VALIDUPTO));
                emailService.sendPromocodeRewardMail(userProfile.getFirstName(), user.getEmail(), freefundCoupon.getFreefundCode(), Math.round(freefundClass.getFreefundValue()), validUptoStr);
            }
        } else {
            logger.info("Serving existing reward for orderId " + orderId + " , userId " + userId);
            FreefundCoupon freefundCoupon = getFreefundCoupon((String) rewardInRepo.get(PromocodeRewardRepository.PROMOCODE));
            if(freefundCoupon != null && FCConstants.STATUS_REDEEMED.equals(freefundCoupon.getStatus())) {
                rewardInRepo.put(PromocodeRewardRepository.HASUSED, true);
            }
        }

        reformatValidUptoDate(rewardInRepo);

        return rewardInRepo;
    }

    //maxLimit : number of promocodes allowed per mobile
    public String issuePromocodeToMobile(String mobile, String className, Integer maxLimit) {

        logger.debug("Request promocode for mobile " + mobile);

        List<Map<String, Object>> promoEntries =  promocodeMobileRepository.query(mobile);

        if(promoEntries == null) {
            promoEntries = new ArrayList<Map<String, Object>>();
        }

        logger.debug("promoEntries size for mobile " + mobile +" is : " + promoEntries.size());

        if((promoEntries.size()+1) > maxLimit) {
            logger.info("Promocode maxLimit of "+maxLimit+" reached. Can not issue promocode for : " + mobile);
            return null;
        }

        if(codeIssuedInThisWeek(promoEntries)) {
            logger.info("Promocode issued for this week already. Can not issue promocode for : " + mobile);
            return null;
        }
        FreefundCoupon freefundCoupon  = generatePromocode(className);

        if(freefundCoupon == null) {
            logger.info("Promocode generation failed for : " + mobile);
            return null;
        }

        Map<String, Object>  promocodeMap = new HashMap<String, Object>();
        //promocodeMap.put(PromocodeMobileRepository.MOBILE, mobile);
        promocodeMap.put(PromocodeMobileRepository.PROMOCODE, freefundCoupon.getFreefundCode());
        promocodeMap.put(PromocodeMobileRepository.FFCLASS, freefundCoupon.getFreefundClassId());
        //promocodeMap.put(PromocodeMobileRepository.VALUE, freefundCoupon.getFreefundValue());
        promocodeMap.put(PromocodeMobileRepository.CREATEDAT, freefundCoupon.getGeneratedDate());
        promoEntries.add(promocodeMap);

        Map<String, Object>  dbEntry = new HashMap<String, Object>();
        dbEntry.put(PromocodeMobileRepository.PROMOCODELIST, promoEntries);
        dbEntry.put(PromocodeMobileRepository.CREATEDAT, freefundCoupon.getGeneratedDate());
        promocodeMobileRepository.updateOrCreateEntry(mobile, dbEntry);
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOREWARD, FreefundService.METRIC_NAME_PROMOREWARD_GENERATE, "ffclass-"
                + freefundCoupon.getFreefundClassId());
        return freefundCoupon.getFreefundCode();
    }

    private boolean codeIssuedInThisWeek(List<Map<String, Object>> promoEntries) {
        if(promoEntries == null || promoEntries.size() == 0) {
            return false;
        }
        for(Map<String, Object> entry : promoEntries) {
            Calendar issuedCal = Calendar.getInstance();
            issuedCal.setTime((Date)entry.get(PromocodeMobileRepository.CREATEDAT));

            if(codeIssuedThisWeek(issuedCal)) {

                return true;
            }
            }
        return false;
    }

    private boolean codeIssuedThisWeek(Calendar issuedCal) {

        Calendar today = Calendar.getInstance();
        int weekDayOfToday = today.get(Calendar.DAY_OF_WEEK);
        if((weekDayOfToday+1) > 7 ) {
            weekDayOfToday = 0;
        }
        Calendar lastFriDay = Calendar.getInstance();
        lastFriDay.add(Calendar.DAY_OF_MONTH, -(weekDayOfToday + 1));
        lastFriDay.set(Calendar.HOUR, 0);
        lastFriDay.set(Calendar.MINUTE, 0);
        lastFriDay.set(Calendar.SECOND, 0);

        if(issuedCal.after(lastFriDay)) {
            return true;
        } else {
            return false;
        }
    }

    private void reformatValidUptoDate(Map<String, Object> rewardInRepo) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date validUpto = (Date) rewardInRepo.get(PromocodeRewardRepository.VALIDUPTO);
        rewardInRepo.put(PromocodeRewardRepository.VALIDUPTO, sdf.format(validUpto.getTime()));
    }

    /*
     * Generate a random code, check if the generated code already in freefund_coupon table, if yes, retry to generate another code.
     * Store the generated code in the table and return the same code
     */
    private FreefundCoupon generateRewardCode() {

        FreefundCoupon freefundCoupon = null;
        int retry = 5;
        String rewardCode = null;
        Calendar cal = Calendar.getInstance();

        String configuredRewardClass = fcProperties.getPromoCodeRewardClassName();
        FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundCode());

        if(freefundClass == null || freefundClass.getIsActive() == 0 || !(cal.after(freefundClass.getValidFrom()) && cal.before(freefundClass.getValidUpto()))) {
            return null;
        }
        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        randomCodeGenerator.setPrefix("PR");
        randomCodeGenerator.setNoOfCodes(1);
        randomCodeGenerator.setLength(8);
        randomCodeGenerator.setType(TYPE.ALPHANUMERIC);
        randomCodeGenerator.setCustomChar(true);

        int i = 0;
        while(i < retry) {
            i++;
            String code = randomCodeGenerator.getCodeWithPrefix();
            freefundCoupon = getFreefundCoupon(code);
            if(freefundCoupon == null) {
                rewardCode = code;
                break;
            } else {
                freefundCoupon = null;
            }
        }

        if(freefundCoupon == null) {
            freefundCoupon = new FreefundCoupon();
            freefundCoupon.setFreefundClassId(Long.valueOf(freefundClass.getId()));
            freefundCoupon.setFreefundCode(rewardCode);
            freefundCoupon.setGeneratedDate(cal.getTime());
            boolean isInserted = insert(freefundCoupon);
            if(isInserted) {
                //Reward code generated, log, metric log
                return freefundCoupon;
            }
        }

        return null;
    }

    private FreefundCoupon generatePromocode(String className) {

        FreefundCoupon freefundCoupon = null;
        int retry = 5;
        String rewardCode = null;
        Calendar cal = Calendar.getInstance();

        FreefundClass freefundClass = getFreefundClassForName(className);

        if(freefundClass == null || freefundClass.getIsActive() == 0 || !(cal.after(freefundClass.getValidFrom()) && cal.before(freefundClass.getValidUpto()))) {
            logger.error("Promocode generate failed for " + className);
            return null;
        }
        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        randomCodeGenerator.setPrefix("DI"); //freefundClass.getPrefix()
        randomCodeGenerator.setNoOfCodes(1);
        randomCodeGenerator.setLength(10); //freefundClass.getCodeLength()
        randomCodeGenerator.setType(TYPE.ALPHANUMERIC);
        randomCodeGenerator.setCustomChar(true);

        int i = 0;
        while(i < retry) {
            i++;
            String code = randomCodeGenerator.getCodeWithPrefix();
            freefundCoupon = getFreefundCoupon(code);
            if(freefundCoupon == null) {
                rewardCode = code;
                break;
            } else {
                freefundCoupon = null;
            }
        }

        if(freefundCoupon == null) {
            freefundCoupon = new FreefundCoupon();
            freefundCoupon.setFreefundClassId(Long.valueOf(freefundClass.getId()));
            freefundCoupon.setFreefundCode(rewardCode);
            freefundCoupon.setGeneratedDate(cal.getTime());
            freefundCoupon.setFreefundValue(freefundClass.getFreefundValue());
            boolean isInserted = insert(freefundCoupon);
            if(isInserted) {
                //Reward code generated, log, metric log
                return freefundCoupon;
            }
        }
        logger.error("Promocodes are not generating for " + className + " ");
        return null;
    }

    public List<String> getFreefundCodes(List<String> freefundCodes) {
        return freefundReadDao.getFreefundCoupons(freefundCodes);
    }

    public List<String> getFreefundCodesForFreefundClass(List<String> freefundCodes, long freefundClassId) {
        return freefundReadDao.getFreefundCoupons(freefundCodes, freefundClassId);
    }

    public List<PromocodeCampaign> getAllPromocodeCampaignFromFreeFund(Long promocodeId) {
        return promocodeCampaignReadDAO.getAllPromocodeCampaignFromFreeFund(promocodeId);
    }

    public List<String> getRedeemedFreefundCouponForServiceNumber(String serviceNumber, Long freefundClassId) {
        return freefundReadDao.getRedeemedFreefundCouponForServiceNumber(serviceNumber, freefundClassId);
    }

    public int getRedeemedCountForServiceNumber(String serviceNumber, Long freefundClassId) {
        logger.debug("Freefund Coupon Redeemed Count retrieval starts for ServiceNumber : " + serviceNumber + ", freefundClassId : " + freefundClassId);
        List<String> list = freefundReadDao.getRedeemedFreefundCouponForServiceNumber(serviceNumber, freefundClassId);
        if (list != null) {
            logger.info("RedeemedCount for serviceNumber : " + serviceNumber + " and class : " + freefundClassId +" is " + list.size());
            return list.size();
        }
        return 0;
    }

    public List<String> getRedeemedFreefundCouponForEmail(String emailId, Long freefundClassId) {
        return freefundReadDao.getRedeemedFreefundCouponForEmail(emailId, freefundClassId);
    }

    public int getRedeemedCountEmail(String emailId, Long freefundClassId) {
        logger.debug("Freefund Coupon Redeemed Count retrieval starts for emailId : " + emailId + ", freefundClassId : " + freefundClassId);
        List<String> list = freefundReadDao.getRedeemedFreefundCouponForEmail(emailId, freefundClassId);
        if (list != null) {
            logger.info("RedeemedCount for email : " + emailId + ", class : " + freefundClassId +" is " + list.size());
            return list.size();
        }
        return 0;
    }

    public boolean isPromoCampaignCode(String promoEntity){
        if(promoEntity.equalsIgnoreCase(FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK)
                || promoEntity.equalsIgnoreCase(FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT)) {
            return true;
        }
        return false;
    }

    public String generatePromoCode(Users user, String campaignId, String codePrefix, String codeType, String codeLength) {
        String code = null;
        List<Map<String, String>> userReferralMapList = userReferralPromocodeService.
                getMappingByUserIdAndCampaign(String.valueOf(user.getUserId()), campaignId);
        Map<String, String> userReferralMap = FCUtil.getFirstElement(userReferralMapList);
        if (userReferralMap != null && !userReferralMap.isEmpty()) {
            String promocodeId = userReferralMap.get(UserReferralPromocodeRepository.PROMOCODE_ID);
            if (!FCStringUtils.isBlank(promocodeId)) {
                FreefundCoupon coupon  = this.getFreefundCoupon(Long.parseLong(promocodeId));
                code = coupon.getFreefundCode();
                logger.info("Code already exists for email id  " + user.getEmail() + ". Code is " + code+" returning same.");
                return code;
            }
        }
        RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
        do {
            String prefix = getCodePrefix(codePrefix);
            String length = getCodeLength(codeLength);
            RandomCodeGenerator.TYPE type = getCodeType(codeType);

            randomCodeGenerator.setParams(prefix, FCConstants.NUMBER_OF_CODES, length, type);
        } while (checkIfCodeExists(randomCodeGenerator.getCodeWithPrefix()));

        code = randomCodeGenerator.getCodeWithPrefix();

        if (!insertIntoFreefundCouponClass(campaignId, code)){
            logger.info("Couldnt create coupon for email " + user.getEmail());
            throw new IllegalStateException("insertIntoFreefundCouponClass: Unable to create coupon for email: "+user.getEmail());
        }

        FreefundCoupon freefundCoupon = this.getFreefundCoupon(code);
        Long promocodeId = freefundCoupon.getId();
        if(!userReferralPromocodeService.storeUserReferrelPromocodeMapping(String.valueOf(user.getUserId()),
                String.valueOf(promocodeId), String.valueOf(campaignId),String.valueOf(0))) {
            logger.info("Couldnt create coupon for email " + user.getEmail());
            throw new IllegalStateException("storeUserReferrelPromocodeMapping: Unable to create coupon for email: "+user.getEmail());
        }
        return code;
    }

    public RandomCodeGenerator.TYPE getCodeType(String codeType) {
        if ("1".equals(codeType)) {
            return RandomCodeGenerator.TYPE.NUMERIC;
        } else {
            return RandomCodeGenerator.TYPE.ALPHANUMERIC;
        }
    }

    public String getCodeLength(String codeLength) {
        if (FCUtil.isInteger(codeLength)) {
            return codeLength;
        } else {
            return FCConstants.LENGTH_OF_CODES;
        }
    }

    public String getCodePrefix(String codePrefix) {
        if(!FCStringUtils.isBlank(codePrefix)) {
            return codePrefix;
        } else {
            return FCConstants.PREFIX;
        }
    }

    public boolean insertIntoFreefundCouponClass(String campaignId, String code) {
        try{
            FreefundClass freefundClass = this.getFreefundClass(Long.parseLong(campaignId));
            if (freefundClass == null) {
                logger.error("Couldnt find freefund class with id: " + campaignId);
                return false;
            }
            Float freefundValue = freefundClass.getFreefundValue();
            Float minRechargeValue = freefundClass.getMinRechargeValue();
            String promoEntity = freefundClass.getPromoEntity();
            return this.insert(new FreefundCoupon(Long.parseLong(campaignId), code,
                    freefundValue, minRechargeValue, promoEntity));
        } catch (Exception e) {
            logger.error("Error inserting into freefund coupon class " + campaignId + " for code " + code, e);
            return false;
        }
    }

    public boolean checkIfCodeExists(String codeWithPrefix) {
        FreefundCoupon freefundCoupon = this.getFreefundCoupon(codeWithPrefix);
        if(freefundCoupon != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine if a freefund has been redeemed for the given order.
     * @param invoiceBusinessDO
     */
    public boolean isFreefundRedeemedForOrder(String orderId) {
        boolean isRedeemed = false;
        CartBusinessDo businessDo = cartService.getCartByOrderId(orderId);
        for (CartItemsBusinessDO cartItem : businessDo.getCartItems()) {
            if (!cartItem.getDeleted()) {
                if (ProductName.fromProductId(cartItem.getProductMasterId()) == ProductName.FreeFund) {
                    FreefundCoupon freefundCoupon = getFreefundCoupon(cartItem.getItemId().longValue());
                    FreefundClass freefundClass = getFreefundClass(freefundCoupon.getFreefundCode());
                    if (freefundClass != null && freefundCoupon != null
                            && StringUtils.isNotEmpty(freefundClass.getPromoEntity())) {

                        if (!PaymentConstants.DISABLED_FREEFUND_CLASS_FOR_INVOICE.contains(freefundClass
                                .getPromoEntity()) && "REDEEMED".equals(freefundCoupon.getStatus())) {
                            isRedeemed = true;
                        }

                        /**
                         * For generic discount codes, we are looking for status
                         * in promocode_campaign table as the status is not
                         * updated to REDEEMED in freefund_coupon table
                         */
                        if (!isRedeemed
                                && freefundClass.getPromoEntity().equalsIgnoreCase(
                                        FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT)) {
                            PromocodeCampaign promocodeCampaign = promocodeCampaignReadDAO
                                    .getPromocodeCampaignFromFreeFund(freefundCoupon.getId());
                            if (promocodeCampaign != null && StringUtils.isNotEmpty(promocodeCampaign.getStatus())
                                    && promocodeCampaign.getStatus().equals("REDEEMED")) {
                                isRedeemed = true;
                            }
                        }
                    }


                }
            }
        }
        return isRedeemed;
    }

    public Optional<Map<String, Object>> getAffiliateIdForFreefundClass(long id) {
        return freefundReadDao.getAffiliateIdForFreefundClass(id);
    }

    public FreefundCoupon getFreefundCouponObjectByOrderId(String orderId) {
		CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
		FreefundCoupon freefundCoupon = null;
		if (cartBusinessDo != null) {
			freefundCoupon = cartService.getFreefundCoupon(cartBusinessDo);
		}
		return freefundCoupon;
	}

    public void blockFreefundCodes(List<String> freefundCoupons, long freefundClassId) {
        freefundWriteDao.blockCodes(freefundCoupons, freefundClassId);
    }

    public List<String> getRedeemedFreefundCoupons(List<String> freefundCodes, long freefundClassId) {
        return freefundReadDao.getRedeemedFreefundCoupons(freefundCodes, freefundClassId);
    }

    public void deleteFreefundCodes(List<String> couponcodes, long freefundClassId) {
        freefundWriteDao.deleteCodes(couponcodes, freefundClassId);
    }

    public int getFreefundCodesForFreefundClassId(long freefundClassId) {
        return freefundReadDao.getCodeCountForFreefundClassId(freefundClassId);
    }

    public FreefundCoupon getFreefundCouponObjectByAddCashOrderId(String orderId) {
        PromocodeCampaign promocodeCampaign =
                promocodeCampaignReadDAO.getPromocodeCampaignByOrderId(orderId);
        if (promocodeCampaign == null) {
            return getFreefundCouponByUsedEmailId(orderId);
        } else {
            return getFreefundCoupon((Integer.valueOf(promocodeCampaign.getFkPromocodeId())).longValue());
        }
    }

    public FreefundCoupon getFreefundCouponByUsedEmailId(String orderId) {
        Map<String, Object> intxnMap = campaignServiceClient.getIntxnData(orderId, FCConstants.ADD_CASH_CAMPAIGN_MERCHANT_ID);
        String promocode = (String) intxnMap.get(InTxnDataConstants.PROMOCODE);
        if (FCUtil.isEmpty(promocode)) {
            intxnMap = campaignServiceClient.getIntxnData(orderId, 3);
            promocode = (String) intxnMap.get(InTxnDataConstants.PROMOCODE);
        }
        return getFreefundCoupon(promocode);
    }

    public void updatePromocodeCampaignOrderId(String lookupId, String orderId) {
        promocodeCampaignWriteDAO.updateOrderId(lookupId
        , orderId);
    }

    public void markFreefundRedeemed(FreefundRequestBusinessDO freeFundBusinessDO, FreefundCoupon freefundCoupon, Users user) {
        PromocodeRedeemDetailsSNSDo promocodeRedeemDetailsSNSDo = new PromocodeRedeemDetailsSNSDo();
        promocodeRedeemDetailsSNSDo.setOrderId("");
        promocodeRedeemDetailsSNSDo.setDiscountValue(String.valueOf(freefundCoupon.getFreefundValue()));
        promocodeRedeemDetailsSNSDo.setPromocode(freefundCoupon.getFreefundCode());
        promocodeRedeemDetailsSNSDo.setCallerReference(" [ " + freeFundBusinessDO.getCouponId() + " ] " + " Redeem "
                + freeFundBusinessDO.getCouponCode());
        promocodeRedeemDetailsSNSDo.setPromoEntity(
                getFreefundClass(freefundCoupon.getFreefundClassId()).getPromoEntity());
        try {
            logger.info("Publish promocode redeem details notification " +
                    PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo));
            promocodeRedeemDetailsSNSService.publish(UUID.randomUUID().toString(), "{\"message\":" +
                    PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo) + "}");
        } catch (Exception e) {
            logger.error("Error publishing freefund success event to SNS ", e);
        }
        logger.info(freefundCoupon.getFreefundCode() +
                " [" + freefundCoupon.getFreefundClassId() + "] " + " Wallet credit done. ");
        markFreefundRedeemedForCoupon(freeFundBusinessDO.getFreefundCoupon(), user.getUserId());

    }
}
