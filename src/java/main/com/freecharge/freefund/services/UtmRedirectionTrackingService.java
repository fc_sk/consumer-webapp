package com.freecharge.freefund.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.UtmRedirectionTrackingReadDao;
import com.freecharge.app.domain.dao.jdbc.UtmRedirectionTrackingWriteDao;
import com.freecharge.app.domain.entity.jdbc.UtmRedirectionTrackingDetails;

@Service
public class UtmRedirectionTrackingService {
    @Autowired
    private UtmRedirectionTrackingWriteDao utmRedirectionTrackingWriteDao;

    @Autowired
    private UtmRedirectionTrackingReadDao utmRedirectionTrackingReadDao;

    public void insert(String shortCode, int userId, int channelId, String redirectedUrl) {
        UtmRedirectionTrackingDetails utmRedirectionTrackingDetails = new UtmRedirectionTrackingDetails();
        utmRedirectionTrackingDetails.setShortCode(shortCode);
        utmRedirectionTrackingDetails.setUserId(userId);
        utmRedirectionTrackingDetails.setChannelId(channelId);
        utmRedirectionTrackingDetails.setRedirectedUrl(redirectedUrl);
        utmRedirectionTrackingWriteDao.insert(utmRedirectionTrackingDetails);
    }

    public List<Map<String, Object>> getShortCodeData(String shortCode) {
        return utmRedirectionTrackingReadDao.getShortCodeData(shortCode);
    }
}
