package com.freecharge.freefund.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.BinofferRepository;
import com.freecharge.mongo.repos.CookieRepository;
import com.freecharge.mongo.repos.DeviceFingerPrintUsageRepository;
import com.freecharge.mongo.repos.NumberRepository;
import com.freecharge.mongo.repos.ProfileNumberRepository;
import com.freecharge.mongo.repos.UserRepository;
import com.freecharge.payment.services.PaymentCard;
import com.freecharge.payment.services.PaymentTransactionService;
import com.mongodb.DBObject;

/**
 * This class provides for recording the freefund usage information, which can be later used for fraud checks.
 * @author shirish
 *
 */
@Service
public class FreefundUsageCounterService {
	public static final String FREEFUND_CLASS_BLOCK_PREFIX = "FREEFUND_CLASS_BLOCK";
	public static final String FREEFUND_CLASS_PREFIX = "FREEFUND_CLASS:";
	public static final String OFFER_DATA_KEY = "offers";
	public static final String FREEFUND_CLASS_APPLY_PREFIX = "FREEFUND_CLASS_APPLY_";
	public static final String EMAIL_KEY = "email";

	private final Logger logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CookieRepository cookieRepository;
    @Autowired
    private NumberRepository numberRepository;
    @Autowired
    private ProfileNumberRepository profileNumberRepository;
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private BinofferRepository binofferRepository;

    @Autowired
    private BinOfferService binOfferService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private DeviceFingerPrintUsageRepository deviceFingerPrintUsageRepository;

    public int getCookieApplyCountInRepository(String cookie, Integer freefundClassId) {
		int freefundClassApplyCount = 0;
		DBObject cookieObject = cookieRepository.getDataByCookie(cookie);
		String freefundClassApplyKey = FREEFUND_CLASS_APPLY_PREFIX + freefundClassId;
		Object objCount = null;
		if (cookieObject != null){
			objCount = cookieObject.get(freefundClassApplyKey);
		}
		if (objCount == null){
			freefundClassApplyCount = 0;
		} else{
			String objCountStr = String.valueOf(objCount);
			if (FCUtil.isInteger(objCountStr)){
				freefundClassApplyCount = Integer.parseInt(objCountStr);
			} else{
				logger.error("Could not get apply freefund class count. Invalid apply freefund class count in Mongo : " + objCount);
				throw new IllegalStateException("Could not get apply freefund class count. Invalid apply freefund class count in " +
						"Mongo: " + objCount);
			}
		}
		logger.info("Current apply count: " + freefundClassApplyCount);
		return freefundClassApplyCount;
	}

    public void incrementReedemCountForNumber(String number, Integer freefundClassId) {
    	logger.debug("Saving freefund class id " + freefundClassId + " in Mongo for number = " + number);
    	if (number == null){
        	logger.error("Could not save freefund class usage count for number: " + number);
        	throw new IllegalStateException("Could not save freefund class usage count for number: " + number);
        }
    	DBObject numberObject = numberRepository.getDataByNumber(number);
		Map<String, Object> numberData = incrementReedemCountInMap(freefundClassId,
				numberObject);
		numberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished saving freefund class usage count for number in Mongo.");

	}

    public void incrementReedemCountForProfileNumber(String number, Integer freefundClassId) {
    	logger.debug("Saving freefund class id " + freefundClassId + " in Mongo for profile number = " + number);
    	if (number == null || number.trim().isEmpty()){
        	logger.warn("Could not save freefund class usage count for profile number: " + number + " as it is blank or null.");
        	return;
        }
    	DBObject numberObject = profileNumberRepository.getDataByProfileNumber(number);
		Map<String, Object> numberData = incrementReedemCountInMap(freefundClassId,
				numberObject);
		profileNumberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished saving freefund class usage count for profile number in Mongo.");

	}

    public void incrementRedeemCountForEmail(Long userId, Integer freefundClassId) {
		logger.info("Saving freefund class id " + freefundClassId + " in Mongo for userId = " + userId);
		DBObject userObject = userRepository.getUserByUserId(new Long(userId));
		Map<String, Object> userData = incrementReedemCountInMap(freefundClassId,
				userObject);
		userRepository.updateOrCreateUser(userId, userData);
		logger.info("Finished saving freefund class in Mongo for " + freefundClassId + "and user " + userId);
	}

	private Map<String, Object> incrementReedemCountInMap(Integer freefundClassId,
			DBObject numberObject) {
		int freefundClassCount = 1;
		Map<String, Object> numberData = new HashMap<String, Object>();
		Map<String, Object> offersData = null;
		if (numberObject == null){
			offersData = new HashMap<String, Object>();
			offersData.put(FREEFUND_CLASS_PREFIX + freefundClassId, freefundClassCount);
		} else{
			offersData = (Map<String, Object>) numberObject.get(OFFER_DATA_KEY);
			if (offersData == null){
				offersData = new HashMap<String, Object>();
			}
			Object objCount = offersData.get(FREEFUND_CLASS_PREFIX + freefundClassId);
			if (objCount == null){
                logger.info("Incremented count to " + freefundClassCount + " for " + numberObject.get(FCConstants.EMAIL));
				offersData.put(FREEFUND_CLASS_PREFIX + freefundClassId, freefundClassCount);
			} else{
				String objCountStr = String.valueOf(objCount);
				if (FCUtil.isInteger(objCountStr)){
					freefundClassCount = Integer.parseInt(objCountStr);
					freefundClassCount = freefundClassCount + 1;
                    logger.info("Incremented count to " + freefundClassCount + " for " + numberObject.get(FCConstants.EMAIL));
                    offersData.put(FREEFUND_CLASS_PREFIX + freefundClassId, freefundClassCount);
				} else{
					logger.error("Could not update freefund class count. Invalid freefund class count in Mongo : " + objCount);
					throw new IllegalStateException("Could not update freefund class count. Invalid freefund class count in Mongo: " + objCount);
				}
			}
		}
		logger.info("New count: " + freefundClassCount);
		numberData.put(OFFER_DATA_KEY, offersData);
		return numberData;
	}

    public void setUseCountForNumber(String number, Integer freefundClassId, int count) {
    	logger.debug("Changing freefund block count for class id " + freefundClassId + " in Mongo for number = " + number + " setting use count = " + count);
    	if (number == null){
        	logger.error("Could not save freefund class usage count for number: " + number);
        	throw new IllegalStateException("Could not save freefund class usage count for number: " + number);
        }
    	DBObject numberObject = numberRepository.getDataByNumber(number);
    	Map<String, Object> numberData = setUseCountInMap(freefundClassId, count,
    			numberObject);
		numberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished changing freefund block count for number in Mongo.");
	}

    public void setUseCountForProfileNumber(String number, Integer freefundClassId, int count) {
    	logger.debug("Changing freefund block count for class id " + freefundClassId + " in Mongo for profile number = " + number + " setting use count = " + count);
    	if (number == null || number.trim().isEmpty()){
        	logger.warn("Could not save freefund class usage count for blank or null profile number: " + number);
        	return;
        }
    	DBObject numberObject = profileNumberRepository.getDataByProfileNumber(number);
    	Map<String, Object> numberData = setUseCountInMap(freefundClassId, count,
    			numberObject);
    	profileNumberRepository	.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished changing freefund block count for profile number in Mongo.");
	}

	public void setUseCountForEmail(Long userId, Integer freefundClassId, int count) {
		logger.debug("Changing freefund class id " + freefundClassId + " in Mongo for userId = " + userId + " setting count = " + count);
		DBObject userObject = userRepository.getUserByUserId(new Long(userId));
		Map<String, Object> userData = setUseCountInMap(freefundClassId, count,
				userObject);
		userRepository.updateOrCreateUser(userId, userData);
		logger.debug("Finished changing freefund class in Mongo.");
	}

	private Map<String, Object> setUseCountInMap(Integer freefundClassId,
			int count, DBObject userObject) {
		int freefundClassCount = count;
		Map<String, Object> userData = new HashMap<String, Object>();
		Map<String, Object> offersData = null;
		if (userObject == null){
			offersData = new HashMap<String, Object>();
			offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
		} else{
			offersData = (Map<String, Object>) userObject.get(OFFER_DATA_KEY);
			if (offersData == null){
				offersData = new HashMap<String, Object>();
			}
			offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
		}
		userData.put(OFFER_DATA_KEY, offersData);
		return userData;
	}

    public void incrementUseCountForEmail(Long userId, Integer freefundClassId) {
		logger.debug("Saving freefund class id " + freefundClassId + " in Mongo for userId = " + userId);
		DBObject userObject = userRepository.getUserByUserId(new Long(userId));
		Map<String, Object> userData = incrementCountInMap(freefundClassId,
				userObject);
		userRepository.updateOrCreateUser(userId, userData);
		logger.debug("Finished saving freefund class in Mongo.");
	}

    public void incrementUseCountForProfileNumber(String number, Integer freefundClassId) {
    	logger.debug("Saving freefund block count for class id " + freefundClassId + " in Mongo for profile number = " + number);
    	if (number == null || number.trim().isEmpty()){
        	logger.warn("User has null or blank number : " + number);
        	//Do nothing as in few users before Nov 2012 have blank numbers or null numbers in users table.
        	return;
        }
    	DBObject numberObject = profileNumberRepository.getDataByProfileNumber(number);
    	Map<String, Object> numberData = incrementCountInMap(freefundClassId,
    			numberObject);
    	profileNumberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished saving freefund block count for profile number in Mongo.");

	}

    public void incrementUseCountForNumber(String number, Integer freefundClassId) {
    	logger.debug("Saving freefund block count for class id " + freefundClassId + " in Mongo for number = " + number);
    	if (number == null){
        	logger.error("Could not save freefund class usage count for number: " + number);
        	throw new IllegalStateException("Could not save freefund class usage count for number: " + number);
        }
    	DBObject numberObject = numberRepository.getDataByNumber(number);
    	Map<String, Object> numberData = incrementCountInMap(freefundClassId,
    			numberObject);
		numberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished saving freefund block count for number in Mongo.");

	}

    public void decrementUseCountForEmail(Long userId, Integer freefundClassId) {
		logger.debug("Decrementing freefund class id " + freefundClassId + " in Mongo for userId = " + userId);
		DBObject userObject = userRepository.getUserByUserId(new Long(userId));
		Map<String, Object> userData = decrementCountInMap(freefundClassId,
				userObject);
		userRepository.updateOrCreateUser(userId, userData);
		logger.debug("Finished Decrementing freefund class in Mongo.");
	}

    public void decrementUseCountForProfileNumber(String number, Integer freefundClassId) {
    	logger.debug("Decrementing freefund block count for class id " + freefundClassId + " in Mongo for profile number = " + number);
    	if (number == null || number.trim().isEmpty()){
        	logger.warn("User has null or blank number : " + number);
        	//Do nothing as in few users before Nov 2012 have blank numbers or null numbers in users table.
        	return;
        }
    	DBObject numberObject = profileNumberRepository.getDataByProfileNumber(number);
    	Map<String, Object> numberData = decrementCountInMap(freefundClassId,
    			numberObject);
    	profileNumberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished Decrementing freefund block count for profile number in Mongo.");

	}

    public void decrementUseCountForNumber(String number, Integer freefundClassId) {
    	logger.debug("Decrementing freefund block count for class id " + freefundClassId + " in Mongo for number = " + number);
    	if (number == null){
        	logger.error("Could not decrement freefund class usage count for number: " + number);
        	throw new IllegalStateException("Could not decrement freefund class usage count for number: " + number);
        }
    	DBObject numberObject = numberRepository.getDataByNumber(number);
    	Map<String, Object> numberData = decrementCountInMap(freefundClassId,
    			numberObject);
		numberRepository.updateOrCreateNumberData(number, numberData);
		logger.debug("Finished decrementing freefund block count for number in Mongo.");

	}

    private Map<String, Object> decrementCountInMap(Integer freefundClassId,
			DBObject userObject) {
		int freefundClassCount = 0;
		Map<String, Object> userData = new HashMap<String, Object>();
		Map<String, Object> offersData = null;
		if (userObject == null){
			offersData = new HashMap<String, Object>();
			offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
		} else{
			offersData = (Map<String, Object>) userObject.get(OFFER_DATA_KEY);
			if (offersData == null){
				offersData = new HashMap<String, Object>();
			}
			Object objCount = offersData.get(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId);
			if (objCount == null){
				offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
			} else{
				String objCountStr = String.valueOf(objCount);
				if (FCUtil.isInteger(objCountStr)){
					freefundClassCount = Integer.parseInt(objCountStr);
					if (freefundClassCount < 0 ){
						logger.error("freefundClassCount already negative - check the issue. For now setting to zero");
						freefundClassCount = 0;
					} else{
						freefundClassCount = freefundClassCount - 1;
					}
					offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
				} else{
					logger.error("Could not decrement freefund class count. Invalid freefund class count in Mongo : " + objCount);
					throw new IllegalStateException("Could not decrement freefund class count. Invalid freefund class count in Mongo: " + objCount);
				}
			}
		}
		logger.info("New count: " + freefundClassCount);
		userData.put(OFFER_DATA_KEY, offersData);
		return userData;
	}

	private Map<String, Object> incrementCountInMap(Integer freefundClassId,
			DBObject userObject) {
		int freefundClassCount = 1;
		Map<String, Object> userData = new HashMap<String, Object>();
		Map<String, Object> offersData = null;
		if (userObject == null){
			offersData = new HashMap<String, Object>();
			offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
		} else{
			offersData = (Map<String, Object>) userObject.get(OFFER_DATA_KEY);
			if (offersData == null){
				offersData = new HashMap<String, Object>();
			}
			Object objCount = offersData.get(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId);
			if (objCount == null){
				offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
			} else{
				String objCountStr = String.valueOf(objCount);
				if (FCUtil.isInteger(objCountStr)){
					freefundClassCount = Integer.parseInt(objCountStr);
					freefundClassCount = freefundClassCount + 1;
					offersData.put(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId, freefundClassCount);
				} else{
					logger.error("Could not update freefund class count. Invalid freefund class count in Mongo : " + objCount);
					throw new IllegalStateException("Could not update freefund class count. Invalid freefund class count in Mongo: " + objCount);
				}
			}
		}
		userData.put(OFFER_DATA_KEY, offersData);
		return userData;
	}

    public int getBlockedCountForEmail(String emailId, Long freefundClassId) {
    	int redeemedCount = 0;
    	try{
    		Users user = userServiceProxy.getUserByEmailId(emailId);
        	DBObject userObject = userRepository.getUserByUserId(new Long(user.getUserId()));
        	redeemedCount = getBlockedCountFromMap(freefundClassId,
					redeemedCount, userObject);
    	} catch (Exception e) {
    		logger.error("Error occured while getting redeem count for email: " + emailId + " freefundClassId: " + freefundClassId, e);
		}
    	return redeemedCount;
    }

    public int getBlockedCountForNumber(String mobileNo, Long freefundClassId) {
    	int redeemedCount = 0;
    	try{
    		DBObject numberObject = numberRepository.getDataByNumber(mobileNo);
        	redeemedCount = getBlockedCountFromMap(freefundClassId,
					redeemedCount, numberObject);

    	} catch (Exception e) {
    		logger.error("Error occured while getting redeem count for number: " + mobileNo + " freefundClassId: " + freefundClassId, e);
		}
    	return redeemedCount;
    }

    public int getBlockedCountForProfileNumber(String mobileNo, Long freefundClassId) {
    	int redeemedCount = 0;
    	try{
    		DBObject numberObject = profileNumberRepository.getDataByProfileNumber(mobileNo);
        	redeemedCount = getBlockedCountFromMap(freefundClassId,
					redeemedCount, numberObject);

    	} catch (Exception e) {
    		logger.error("Error occured while getting redeem count for number: " + mobileNo + " freefundClassId: " + freefundClassId, e);
		}
    	return redeemedCount;
    }

	private int getBlockedCountFromMap(Long freefundClassId,
			int redeemedCount, DBObject numberObject) {
		if (numberObject != null){
			Map<String, Object> existingOffersData = (Map<String, Object>) numberObject.get(OFFER_DATA_KEY);
			if (existingOffersData != null){
				Object countObject = existingOffersData.get(FREEFUND_CLASS_BLOCK_PREFIX + freefundClassId);
				String countStr = String.valueOf(countObject);
		    	if (countObject != null && FCUtil.isInteger(countStr)){
		    		redeemedCount = Integer.parseInt(countStr);
		    	}
			}
		} // end if
		return redeemedCount;
	}

	public int getRedeemedCountForEmail(String emailId, Long freefundClassId) {
    	int redeemedCount = 0;
    	try{
    		Users user = userServiceProxy.getUserByEmailId(emailId);
    		logger.info("user id of  " + emailId + " : " + new Long(user.getUserId()));
        	DBObject userObject = userRepository.getUserByUserId(new Long(user.getUserId()));
        	if (userObject != null){
        		Map<String, Object> existingOffersData = (Map<String, Object>) userObject.get(OFFER_DATA_KEY);
        		if (existingOffersData != null){
        			Object countObject = existingOffersData.get(FREEFUND_CLASS_PREFIX + freefundClassId);
        			String countStr = String.valueOf(countObject);
        			logger.info("countStr is "+countStr+" for " + emailId + " : " + new Long(user.getUserId()));
                	if (countObject != null && FCUtil.isInteger(countStr)){
                		redeemedCount = Integer.parseInt(countStr);
                		logger.info("redeemedCount is "+redeemedCount+" for " + emailId + " : " + new Long(user.getUserId()));
                	}
        		}  else {
                    logger.info("existingOffersData is null  " + emailId + " : " + new Long(user.getUserId()));
                }
            } else {
                logger.info("userObject is null  " + emailId + " : " + new Long(user.getUserId()));
            }
    	} catch (Exception e) {
    		logger.error("Error occured while getting redeem count for email: " + emailId + " freefundClassId: " + freefundClassId, e);
		}
    	return redeemedCount;
    }

	 public int getRedeemedCountForNumber(String mobileNo, Long freefundClassId) {
	    	int redeemedCount = 0;
	    	try{
	    		DBObject numberObject = numberRepository.getDataByNumber(mobileNo);
	        	if (numberObject != null){
	        		Map<String, Object> existingOffersData = (Map<String, Object>) numberObject.get(OFFER_DATA_KEY);
	        		if (existingOffersData != null){
	        			Object countObject = existingOffersData.get(FREEFUND_CLASS_PREFIX + freefundClassId);
	        			String countStr = String.valueOf(countObject);
	                	if (countObject != null && FCUtil.isInteger(countStr)){
	                		redeemedCount = Integer.parseInt(countStr);
	                	}
	        		}
	            } // end if

	    	} catch (Exception e) {
	    		logger.error("Error occured while getting redeem count for number: " + mobileNo + " freefundClassId: " + freefundClassId, e);
			}
	    	return redeemedCount;
	    }

	 public int getRedeemedCountForProfileNumber(String mobileNo, Long freefundClassId) {
	    	int redeemedCount = 0;
	    	try{
	    		DBObject numberObject = profileNumberRepository.getDataByProfileNumber(mobileNo);
	        	if (numberObject != null){
	        		Map<String, Object> existingOffersData = (Map<String, Object>) numberObject.get(OFFER_DATA_KEY);
	        		if (existingOffersData != null){
	        			Object countObject = existingOffersData.get(FREEFUND_CLASS_PREFIX + freefundClassId);
	        			String countStr = String.valueOf(countObject);
	                	if (countObject != null && FCUtil.isInteger(countStr)){
	                		redeemedCount = Integer.parseInt(countStr);
	                	}
	        		}
	            } // end if

	    	} catch (Exception e) {
	    		logger.error("Error occured while getting redeem count for number: " + mobileNo + " freefundClassId: " + freefundClassId, e);
			}
	    	return redeemedCount;
	    }

	 public void storeCardbinOfferAvail(String campaignKey, Long campaignId, String orderId) {
	     try {
	         List<PaymentCard> paymentCards = paymentTransactionService.getSuccessfulPaymentCards(orderId);
	         if(!FCUtil.isEmpty(paymentCards)){
	             PaymentCard paymentCard = FCUtil.getFirstElement(paymentCards);
	             if (paymentCard != null && FCUtil.isNotEmpty(paymentCard.getCardHash())) {

	                 logger.debug("Order ID: " + orderId + " cardHash : " + paymentCard.getCardHash());
	                 binofferRepository.insert(paymentCard.getCardHash(), orderId, campaignKey, campaignId);

	             } else {
	                 logger.error("Payment cards list from payu found empty or cardhash is null for " + orderId);
	             }
	         } else {
	             logger.info("Error occured while storing card usage. Payment may not be via a card. For " 
	                     + campaignKey + " offer : " + campaignId + " cardbin is null , orderId : " + orderId);
	         }
	     } catch (Exception e) {
	         logger.error("Exception occured while storing card usage for " + campaignKey + " offer : " + campaignId +
	                 " cardbin is null , orderId : " + orderId, e);
	     }
	 }

    public void incrementApplyCountForFingerPrint(String fingerPrint, Integer userId, String promocode, Integer freefundClassId) {
        logger.debug("Saving User : " + userId + ", promocode : " + promocode + ", freefundClassId : " + freefundClassId + " in Mongo for FingerPrint : " + fingerPrint);

        try {
            if (fingerPrint == null || userId == null || promocode == null || freefundClassId == null){
                logger.error("Could not save DeviceFingerPrintsUsage for FingerPrint : " + fingerPrint + ", userId : " + userId 
                        + ", promocode : " + promocode + ", freefundClassId : " + freefundClassId);
                return;
            }

            deviceFingerPrintUsageRepository.insert(fingerPrint, userId, promocode, DeviceFingerPrintUsageRepository.FREEFUND_CLASS_ID, (long) freefundClassId);
            logger.info("FingerPrint : " + fingerPrint + ", userId : " + userId + ", promocode : " + promocode + ", freefundClassId : " 
                    + freefundClassId + " saved into DeviceFingerPrintsUsageRepository");

        } catch (Exception e) {
            logger.error("Exception occured while storing DeviceFingerPrintsUsage for FingerPrint : " + fingerPrint + ", userId : " + userId 
                    + ", promocode : " + promocode + ", freefundClassId : " + freefundClassId, e);
        }
    }
    
    public void decrementApplyCountForFingerPrint(String fingerPrint, 
                                                  Integer userId, 
                                                  String promocode, 
                                                  Long freefundClassId) {
        logger.debug("Deleting entry- User : " + userId + ", promocode : " + promocode 
                     + ", freefundClassId : " + freefundClassId 
                     + " in Mongo for FingerPrint : " + fingerPrint);

        try {
            if (fingerPrint == null || userId == null || promocode == null || freefundClassId == null){
                logger.error("Could not delete DeviceFingerPrintsUsage for FingerPrint : " + fingerPrint 
                        + ", userId : " + userId 
                        + ", promocode : " + promocode 
                        + ", freefundClassId : " + freefundClassId);
                return;
            }

            deviceFingerPrintUsageRepository.deleteOneRecord(fingerPrint, 
                                                            userId, 
                                                            promocode, 
                                                            DeviceFingerPrintUsageRepository.FREEFUND_CLASS_ID, 
                                                            (long) freefundClassId);
            logger.info("FingerPrint : " + fingerPrint + ", userId : " + userId 
                        + ", promocode : " + promocode + ", freefundClassId : " 
                        + freefundClassId + " deleted from DeviceFingerPrintsUsageRepository");

        } catch (Exception e) {
            logger.error("Exception occured while deleting DeviceFingerPrintsUsage for FingerPrint : " + fingerPrint 
                    + ", userId : " + userId 
                    + ", promocode : " + promocode 
                    + ", freefundClassId : " + freefundClassId, e);
        }
    }
    
    public int updateApplyCountForCookie(String email, String cookie, Integer freefundClassId, int updateCount) {
        
        logger.debug("Update ApplyCount - FreefundClassId: " + freefundClassId + " Cookie: " + cookie);
        if (updateCount == 0) {
            logger.debug("Nothing to update in ApplyCounts for FreefundClassId: " + freefundClassId + " Cookie: " + cookie);
            return 1;
        }
        if (cookie == null || freefundClassId == null){
            logger.error("Could not update apply count for cookie: " + cookie + " and freefundClassId: " + freefundClassId);
            return 0;
        }
        
        DBObject cookieObject = cookieRepository.getDataByCookie(cookie);
        
        int freefundClassApplyCount = 1;
        String freefundClassApplyKey = FREEFUND_CLASS_APPLY_PREFIX + freefundClassId;
        Map<String, Object> cookieData = new HashMap<String, Object>();
        Object objCount = null;
        if (cookieObject != null){
            objCount = cookieObject.get(freefundClassApplyKey);
        }
        if (objCount == null){
            cookieData.put(freefundClassApplyKey, freefundClassApplyCount);
        } else{
            String objCountStr = String.valueOf(objCount);
            if (FCUtil.isInteger(objCountStr)){
                freefundClassApplyCount = Integer.parseInt(objCountStr);
                freefundClassApplyCount = freefundClassApplyCount + updateCount;
                cookieData.put(freefundClassApplyKey, freefundClassApplyCount);
            } else{
                logger.error("Could not update CookieApplyCount. Invalid apply count : " + objCount);
                throw new IllegalStateException("Could not update CookieApplyCount. Invalid apply count : " + objCount);
            }
        }
        logger.debug("Updated Apply Count: " + freefundClassApplyCount 
                     + " Cookie: " + cookie 
                     + " FreefundClassId:" + freefundClassId);
        cookieData.put(EMAIL_KEY, email);
        cookieRepository.updateOrCreateCookieData(cookie, cookieData);
        return freefundClassApplyCount;
    }
}


