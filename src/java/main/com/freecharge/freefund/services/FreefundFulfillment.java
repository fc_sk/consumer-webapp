package com.freecharge.freefund.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.campaign.web.RewardResult;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.fulfillment.CashbackService;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.PromocodeRedeemDetailsSNSDo;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.order.service.OrderMetaDataService;
import com.freecharge.order.service.model.OrderMetaData;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.promo.util.PromocodeConstants;
import com.freecharge.recharge.businessdao.InExcelReader;
import com.freecharge.recharge.businessdao.ReportData;
import com.freecharge.reward.web.AbstractProcessResult;
import com.freecharge.reward.web.RewardProcessResult;
import com.freecharge.sqs.reward.RewardServiceSQSService;
import com.freecharge.util.DelimeterSeparatedFileParserUtil;
import com.google.common.util.concurrent.RateLimiter;

/**
 * This class provides methods to enqueue and process FreeFund Fulfillment
 *
 */
@Service
public class FreefundFulfillment {

    @Autowired
    protected OrderIdSlaveDAO orderIdDAO;

    @Autowired
    private FreefundService   freefundService;

    @Autowired
    private CashbackService   cashbackService;

    @Autowired
    private FreefundUsageCounterService freefundUsageCounterService;

    private final Logger      logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private OrderMetaDataService orderMetaDataService;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private KestrelWrapper kestrelWrapper;

    @Autowired
    private InService inService;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private SMSService smsService;

    public final static String ORDER_ID = "orderId";
    public final static String LOOKUP_ID = "lookupId";
    public final static String IN_RESPONSE = "inResponse";

    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
//    @Autowired
    private InExcelReader 				  inExcelReader = new InExcelReader();

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    @Autowired
    private PromocodeRedeemDetailsSNSService promocodeRedeemDetailsSNSService;

    @Autowired
    private RewardServiceSQSService rewardServiceSQSService;
    
    @Autowired
    private HomeService                       homeService;
    
    @Autowired
    private FulfillmentService fulfillmentService;

	
	public void campaignBackfill() {
		try {
			String filePath = "/home/fc-web/hangar/promocode_data.xls";
			int count= 1;
			ReportData reportData = inExcelReader.read(filePath);
			List<List<String>> allOrderDetails = reportData.getData();

			for(List<String> orderData : allOrderDetails) {
//				if(count == 3) break;
				String lookupId = orderData.get(1);
				String freefundClassStr = orderData.get(3);
				Double freefundClassDouble = Double.parseDouble(freefundClassStr);
				Integer freefundClassId = freefundClassDouble.intValue();
				String orderId = orderData.get(0);
				logger.info("PromocodeBackFill lookupId: " + lookupId + " ,freefundClass: " + freefundClassId + " ,orderId: " +orderId);
				Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
				logger.info("[PromocodeBackFill] after getting userDetails" );
				UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
				Map<String, Object> payLoad = new HashMap<>();
				payLoad.put("ffClassId", freefundClassId);
				fulfillmentService.storeInTxnData(orderId, userDetailsObj, payLoad);
				logger.info("[PromocodeBackFill] after storing intxnData" );
				CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(lookupId, freefundClassId,
                        "RECHARGE_SUCCESS", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
				logger.info("[PromocodeBackfill]Result of campaign call for orderId: "+orderId+"," + lookupId +"," +freefundClassId+","+campaignResult.getStatus());
				count++;
				if(count%100 ==0) Thread.sleep(5000);

			}

			logger.info("[PromocodeBackFill]Total lines in file : " + count);
		}catch (Exception e) {
			logger.error("[PromocodeBackFill]Exception caught : ",e);
		} 
	}
	
	public void nonPromocodeCampaignBackfill() {
		
		
		logger.info("[NonPromocodeBackFill] Reached inside nonPromocodeCampaignBackfill" );
		String filePath = "/home/fc-web/hangar/promocode_data.xls";
		BufferedReader br = null;
		String line = "";

		String comma = ",";



		try {
			int count= 1;
			ReportData reportData = inExcelReader.read(filePath);
			List<List<String>> allOrderDetails = reportData.getData();

			for(List<String> orderData : allOrderDetails) {
//				if(count == 3) break;
				//	use comma as separator
				String lookupId = orderData.get(1);
				String freefundClass = orderData.get(3);
				String orderId = orderData.get(0);
				logger.info("[NonPromocodeBackFill] lookupId: " + lookupId + " ,freefundClass : " + freefundClass + " ,orderId: " +orderId);
				Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
				UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
				fulfillmentService.storeInTxnData(orderId, userDetailsObj);
				logger.info("[NonPromocodeBackFill] after storing intxnData" );
				fulfillmentService.publishCampaignCheckMessage(lookupId);
				logger.info("[NonPromocodeBackFill]published data async call for orderId: "+orderId);
				count++;
				if(count%100 ==0) Thread.sleep(5000);

			}

			logger.info("[NonPromocodeBackFill]Total lines in file : " + count);
		}catch (Exception e) {
			logger.error("[NonPromocodeBackFill] Exception caught : ",e);
		}
	}
	
	public void campaignBackfill(final List<Map<String, Object>> data) {
		final String threadName = "PromoCodeBackfillBulkData";
		Thread thread = new Thread(new Runnable() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				logger.info("Started Thread : " + threadName);
				Integer itemIndex = 0;
				ObjectMapper objectMapper = new ObjectMapper();
				for(Map<String, Object> txnPayload : data) {
					String lookupId = (String) txnPayload.get("lookupId");
					if(txnPayload.get("txnData") != null) {
						if((itemIndex + 1)%100 == 0) {
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								logger.error("Interrupted exception : ", e);
							}
						}
						try {
							logger.info("itemIndex : " + itemIndex + " , data : " + objectMapper.writeValueAsString(data));
						} catch (JsonProcessingException jpe) {
							logger.error("Unable to log data for itemIndex : " + itemIndex + " because of exception : ", jpe);
						}
						Map<String, Object> txnData = (Map<String, Object>) txnPayload.get("txnData");
						String orderId = (String) txnData.get("orderId");
						logger.info("itemIndex : " + itemIndex + " ,lookupId: " + lookupId + " ,orderId: " + orderId);
						try {
							Integer freefundClassId = (Integer) txnData.get("ffClassId");
							logger.info("orderId : " + orderId + ",freefundClassId : " + freefundClassId);
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							if(txnData.get(InTxnDataConstants.PAYMENT_SUCCESS_ON) != null) {
								txnData.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, simpleDateFormat.parse((String)(txnData.get(InTxnDataConstants.PAYMENT_SUCCESS_ON))));
							}
							if(txnData.get(InTxnDataConstants.ORDER_ID_GENERATED_ON) != null) {
								txnData.put(InTxnDataConstants.ORDER_ID_GENERATED_ON, simpleDateFormat.parse((String)(txnData.get(InTxnDataConstants.ORDER_ID_GENERATED_ON))));
							}
							Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
							UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
							fulfillmentService.storeInTxnData(orderId, userDetailsObj, txnData);
							CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(lookupId, freefundClassId,
			                        "RECHARGE_SUCCESS", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
							logger.info("Result of campaign call for orderId: " + orderId + ",lookupId : " + lookupId + ",freefundClassId : " + freefundClassId + ",campaignResultStatus : " + campaignResult.getStatus());
						} catch (Exception e) {
							logger.error(String.format("Unable to process promocode cash back for itemIndex %d lookupId %s orderId %s because of exception : ", itemIndex, lookupId, orderId), e);
						}
						itemIndex++;
					}else {
						logger.error("No txnData for itemIndex : " + itemIndex + " ,lookupId : " + lookupId);
					}
				}
				logger.info("Finished Thread : " + threadName);
			}
		});
		try {
			thread.setName(threadName);
			logger.info("Starting thread : " + threadName);
			thread.start();
		} catch (IllegalThreadStateException e) {
			logger.error("Unable to start thread "+ threadName + " because of exception : ", e);
		} catch (Exception e) {
			logger.error("Unknown exception in thread " + threadName + " : ", e);
		}
	}
	
	public void nonPromocodeCampaignBackfill (final List<Map<String, Object>> data) {
		final String threadName = "NonPromoCodeBackfillBulkData";
		Thread thread = new Thread(new Runnable() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				logger.info("Started Thread : " + threadName);
				Integer itemIndex = 0;
				ObjectMapper objectMapper = new ObjectMapper();
				for(Map<String, Object> txnPayload : data) {
					String lookupId = (String) txnPayload.get("lookupId");
					if(txnPayload.get("txnData") != null) {
						if((itemIndex + 1)%100 == 0) {
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								logger.error("Interrupted excetion : ", e);
							}
						}
						try {
							logger.info("itemIndex : " + itemIndex + " , data : " + objectMapper.writeValueAsString(data));
						} catch (JsonProcessingException jpe) {
							logger.error("Unable to log data for itemIndex : " + itemIndex + " because of exception : ", jpe);
						}
						Map<String, Object> txnData = (Map<String, Object>) txnPayload.get("txnData");
						String orderId = (String) txnData.get("orderId");
						logger.info("itemIndex : " + itemIndex + " ,lookupId: " + lookupId + " ,orderId: " + orderId);
						try {
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							if(txnData.get(InTxnDataConstants.PAYMENT_SUCCESS_ON) != null) {
								txnData.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, simpleDateFormat.parse((String)(txnData.get(InTxnDataConstants.PAYMENT_SUCCESS_ON))));
							}
							if(txnData.get(InTxnDataConstants.ORDER_ID_GENERATED_ON) != null) {
								txnData.put(InTxnDataConstants.ORDER_ID_GENERATED_ON, simpleDateFormat.parse((String)(txnData.get(InTxnDataConstants.ORDER_ID_GENERATED_ON))));
							}
							Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
							UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
							fulfillmentService.storeInTxnData(orderId, userDetailsObj);
							fulfillmentService.publishCampaignCheckMessage(lookupId);
							logger.info("Published data async call for orderId:  " + orderId);
						} catch (Exception e) {
							logger.error(String.format("Unable to process non-promocode cash back for itemIndex %d lookupId %s orderId %s because of exception : ", itemIndex, lookupId, orderId), e);
						}
						itemIndex++;
					}else {
						logger.error("No txnData for itemIndex : " + itemIndex + " ,lookupId : " + lookupId);
					}
				}
				logger.info("Finished Thread : " + threadName);
			}
		});
		try {
			thread.setName(threadName);
			logger.info("Starting thread : " + threadName);
			thread.start();
		} catch (IllegalThreadStateException e) {
			logger.error("Unable to start thread " + threadName + " because of exception : ", e);
		} catch (Exception e) {
			logger.error("Unknown exception in thread " + threadName + " : ", e);
		}
	}
	
	public void campaignBackfillFromFile() {
		final String threadName = "PromoCodeBackfillBulkDataFromFile";
		final String filePath = "/tmp/camapign_data.csv";
		final Integer rowsToReadPerExecution = 10000;
		final Integer totalTps = 10;
		Thread thread = new Thread(new Runnable() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				logger.info("Started Thread : " + threadName);
				final RateLimiter rateLimiter = RateLimiter.create(totalTps);
				final ObjectMapper objectMapper = new ObjectMapper();
				final int[] itemIndexArr = new int[1];
				itemIndexArr[0] = -1;
				DelimeterSeparatedFileParserUtil.ExecuterFunction executerFunction = new DelimeterSeparatedFileParserUtil.ExecuterFunction() {
					
					@Override
					public void execute(List<String> columnHeaders, List<List<String>> rowRecords) {
						List<Map<String, Object>> data = getParsedData(columnHeaders, rowRecords);
						for(Map<String, Object> txnPayload : data) {
							String lookupId = (String) txnPayload.get(InTxnDataConstants.LOOKUP_ID);
							if(txnPayload.get("txnData") != null) {
								Map<String, Object> txnData = (Map<String, Object>) txnPayload.get("txnData");
								String orderId = (String) txnData.get(InTxnDataConstants.ORDER_ID);
								logger.info("lookupId : " + lookupId + " , orderId : " + orderId);
								try {
									Integer freefundClassId = (Integer) txnData.get(InTxnDataConstants.FF_CLASS_ID);
									//logger.info("orderId : " + orderId + ", freefundClassId : " + freefundClassId);
									rateLimiter.acquire();
									Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
									UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
									fulfillmentService.storeInTxnData(orderId, userDetailsObj, txnData);
									CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(lookupId, freefundClassId,
					                        "RECHARGE_SUCCESS", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
									logger.info("Result of campaign call cashback file came as " + campaignResult.getStatus() + " for orderId : " + orderId + " , lookupId : " + lookupId + " , freefundClassId : " + freefundClassId + " , itemIndex : " + txnPayload.get("itemIndex"));
								} catch (Exception e) {
									logger.error(String.format("Unable to process promocode cash back from file for lookupId %s orderId %s because of exception : ", lookupId, orderId), e);
								}
							}else {
								logger.error("No txnData for promocode cashback file for lookupId : " + lookupId);
							}
							/*try {
								if(counter % 100 == 0) {
									int time = 3000;
									logger.info("Sleeping for " + time +" ms.");
									Thread.sleep(time);
								}
							} catch (InterruptedException e) {
								logger.error("Got Interrupted exception for : " + threadName);
							}*/
						}
					}
					
					private List<Map<String, Object>> getParsedData(List<String> columnHeaders, List<List<String>> rowRecords){
						Integer itemIndex = itemIndexArr[0] + 1;
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						List<Map<String, Object>> allPayloadData = new LinkedList<>(); 
						for(List<String> rowRecord : rowRecords) {
							Map<String, Object> payLoadData = new HashMap<>();
							Map<String, Object> txnData = new HashMap<>();
							payLoadData.put("txnData", txnData);
							try {
								if(rowRecord.size() != columnHeaders.size()) {
									logger.error("Total items in rows  : " + rowRecord.size() + " mismatches total items in column : " + columnHeaders.size() + " ,itemIndex : " + itemIndex + " with rowData : " + objectMapper.writeValueAsString(rowRecord));
								}else {
									//logger.info("itemIndex : " + itemIndex + " , rowData : " + objectMapper.writeValueAsString(rowRecord));
									for (int i = 0; i < columnHeaders.size(); i++) {
										String key = columnHeaders.get(i).trim();
										String value = rowRecord.get(i).trim();
										switch (key) {
										case InTxnDataConstants.LOOKUP_ID:
											payLoadData.put(key, value);
											break;
										case InTxnDataConstants.CHANNEL:
										case InTxnDataConstants.PAYMENT_TYPE:
										case InTxnDataConstants.FF_CLASS_ID:
											Integer intValue = ((Double)Double.parseDouble(value)).intValue();
											txnData.put(key, intValue);
											break;
										case InTxnDataConstants.IS_EMULATOR:
											txnData.put(key, Boolean.parseBoolean(value));
											break;
										case InTxnDataConstants.PAYMENT_SUCCESS_ON:
										case InTxnDataConstants.ORDER_ID_GENERATED_ON:
											txnData.put(key, simpleDateFormat.parse(value));
											break;
										case InTxnDataConstants.PG_AMOUNT:
										case InTxnDataConstants.WALLET_AMOUNT:
										case InTxnDataConstants.RECHARGE_AMOUNT:
											Double doubleValue = Double.parseDouble(value);
											txnData.put(key, doubleValue);
											break;
										default:
											if(!StringUtils.isNullOrEmpty(value)) {
												txnData.put(key, value);
											}
											break;
										}
									}
								}
								/*try {
									logger.info("Promocode cashback file itemIndex : " + itemIndex + " , data : " + objectMapper.writeValueAsString(txnData));
								} catch (JsonProcessingException jpe) {
									logger.error("Unable to log data for promocode cashback file itemIndex : " + itemIndex + " because of exception : ", jpe);
								}*/
								allPayloadData.add(payLoadData);
							} catch (Exception e) {
								logger.error("Skipping the rowItem : " + itemIndex + " because of exception : ", e);
							}
							payLoadData.put("itemIndex", itemIndex);
							itemIndex++;
						}
						itemIndexArr[0] += itemIndex - 1;
						return allPayloadData;
					}
				};
				try {
					DelimeterSeparatedFileParserUtil.readDataFromLinesAndExecute(new File(filePath), rowsToReadPerExecution, DelimeterSeparatedFileParserUtil.COMMA_DELIMETER, executerFunction);
				} catch (Exception e) {
					logger.error("Unable to process the file because of exception : ", e);
				}
				logger.info("Finished Thread : " + threadName);
			}
		});
		try {
			thread.setName(threadName);
			logger.info("Starting thread : " + threadName);
			thread.start();
		} catch (IllegalThreadStateException e) {
			logger.error("Unable to start thread "+ threadName + " because of exception : ", e);
		} catch (Exception e) {
			logger.error("Unknown exception in thread " + threadName + " : ", e);
		}
	}
    
    private void onRechargeSuccess(String orderId, String lookupId, Float amount) {
        try {
            FreefundCoupon freefundCoupon = null;
            String freefundClassId = "Unknown";
            long startTime = System.currentTimeMillis();
            if (orderId == null) {
                logger.error("RechargeSuccessEvent fired without orderId.");
                return;
            }

            logger.info("RechargeSuccessEvent received for " + orderId);

            if (lookupId == null) {
                logger.error("RechargeSuccessEvent fired without lookupid.");
                return;
            }

            CartItemsBusinessDO freefundCartItem = freefundService.getFreeFundItem(lookupId);
            if (freefundCartItem != null) {
                Long freefundCouponId = freefundCartItem.getItemId().longValue();
                freefundCoupon = freefundService.getFreefundCoupon(freefundCouponId);
                logger.info(" RechargeSuccessEvent received " + freefundCoupon.getFreefundCode() + " ["
                        + freefundCoupon.getFreefundClassId() + "] " + orderId);

                Integer userId = orderIdDAO.getUserIdFromOrderId(orderId);
                FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
                if (freefundClass != null) {
                    freefundClassId = String.valueOf(freefundClass.getId());
                }
                logger.debug("Product master id: " + freefundCartItem.getProductMasterId());
                OrderMetaData orderMetaData = orderMetaDataService.getOrderMetaData(orderId);
                String imeiNumber = null;
                String advertisementId = null;
                String deviceUniqueId = null;
                String imsi = null;
                Boolean isEmulator = null;
                if(orderMetaData != null){
                    imeiNumber = orderMetaData.getImeiNumber();
                    advertisementId = orderMetaData.getAdvertisementId();
                    deviceUniqueId = orderMetaData.getDeviceUniqueId();
                    imsi = orderMetaData.getImsi();
                    isEmulator = orderMetaData.getIsEmulator();
                }
                if (FCConstants.PRODUCT_ID_FREEFUND_CASHBACK == freefundCartItem.getProductMasterId()) {
                        logger.info("Promocode " + freefundCoupon.getFreefundCode() + " is for new campaign framework.");
                        try {
                            CampaignResult campaignResult = campaignServiceClient.checkAndProcessCampaign1(lookupId, freefundClass.getId(),
                                    "RECHARGE_SUCCESS", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
                            if (campaignResult.getStatus()) {
                                logger.info("Campaign Reward successfully processed for " + orderId + ", userId " + userId);
                                RewardResult rewardResult = campaignResult.getRewardResult();
                                if(rewardResult != null && rewardResult.getStatus()) {
                                    Double cashbackAmount = 0.0;
                                    for (RewardProcessResult rewardProcessResult : rewardResult.getRewardProcessResultMap().values()){
                                        if (rewardProcessResult != null && rewardProcessResult.getProcessResults() != null
                                                && rewardProcessResult.getProcessResults().size() > 0) {
                                            for (AbstractProcessResult abstractProcessResult : rewardProcessResult.getProcessResults()) {
                                                if (abstractProcessResult != null && abstractProcessResult.getInfo() != null
                                                        &&
                                                        (FCConstants.GIFTVOUCHER.equalsIgnoreCase(abstractProcessResult.getType().name()) ||
                                                                FCConstants.PERCENTCREDITS.equalsIgnoreCase(abstractProcessResult.getType().name()))) {
                                                    cashbackAmount = Double.parseDouble(String.valueOf(abstractProcessResult.getInfo().get(FCConstants.CREDITED_AMOUNT)));
                                                    freefundCoupon.setFreefundValue(freefundCoupon.getFreefundValue() +
                                                            cashbackAmount.floatValue());
                                                }
                                            }
                                        } else {
                                            if (rewardProcessResult != null && rewardProcessResult.getInfo() != null
                                                    && (FCConstants.GIFTVOUCHER.equalsIgnoreCase(rewardProcessResult.getType().name()) ||
                                                    FCConstants.PERCENTCREDITS.equalsIgnoreCase(rewardProcessResult.getType().name()))) {
                                                cashbackAmount = Double.parseDouble(String.valueOf(rewardProcessResult.getInfo().get(FCConstants.CREDITED_AMOUNT)));
                                                freefundCoupon.setFreefundValue(freefundCoupon.getFreefundValue() + cashbackAmount.floatValue());
                                            }
                                        }
                                    }
                                }
                                boolean freefundRedeemSuccess = freefundService.markFreefundRedeemedForLookupId(freefundCoupon, lookupId,
                                        userId, orderId, imeiNumber, advertisementId, deviceUniqueId, imsi, isEmulator);
                                if (freefundRedeemSuccess) {
                                    logger.info("Marking freefund redeemed for orderId " + orderId + ", userId " + userId + " : successful");
                                } else {
                                    logger.info("Marking freefund redeemed for orderId " + orderId + ", userId " + userId + " : failed");
                                }
                            } else {
                                logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId()
                                        + "] " + campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                                logger.error("Campaign Reward processing failed for " + orderId + ", userId " + userId + ". Unblocking freefund coupon " + freefundCoupon.getFreefundCode());
                                freefundService.releaseCouponCode(freefundCoupon, orderId, false, campaignResult.getConditionResult().getFailedRule().getFailureMessage(), false, orderMetaData);
                            }
                        } catch (Exception e) {
                            logger.error("Error while processing new framework campaign for " + orderId + ", userId " + userId + ", freefundcode " + freefundCoupon.getFreefundCode(), e);
                        }
                } else {
                    boolean isSuccess = freefundService.markFreefundRedeemedForLookupId(freefundCoupon, lookupId,
                            userId, orderId, imeiNumber, advertisementId, deviceUniqueId, imsi, isEmulator);
                    if(isSuccess) {
                        PromocodeRedeemDetailsSNSDo promocodeRedeemDetailsSNSDo = new PromocodeRedeemDetailsSNSDo();
                        promocodeRedeemDetailsSNSDo.setOrderId(orderId);
                        Map<String, Object> rechargeDetails = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
                        Double discountValue = 0.0;
                        if (freefundCoupon.getFreefundValue() <= 0.0) {
                            discountValue = freefundCoupon.getDiscountValue(
                                    Double.parseDouble(String.valueOf(rechargeDetails.get("amount"))),
                                    freefundClass.getDiscountType(), freefundClass.getMaxDiscount());
                        } else {
                            discountValue = freefundCoupon.getFreefundValue().doubleValue();
                        }
                        promocodeRedeemDetailsSNSDo.setDiscountValue(String.valueOf(discountValue));
                        promocodeRedeemDetailsSNSDo.setPromocode(freefundCoupon.getFreefundCode());
                        promocodeRedeemDetailsSNSDo.setCallerReference("");
                        promocodeRedeemDetailsSNSDo.setPromoEntity(freefundClass.getPromoEntity());
                        try {
                            logger.info("Publish promocode redeem details notification " +
                                    PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo));
                            promocodeRedeemDetailsSNSService.publish(orderId, "{\"message\":" +
                                    PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo) + "}");
                        } catch (IOException e) {
                            logger.error("Error publishing promocode discount success event to SNS ", e);
                        }
                    }
                    if(appConfigService.isIVRSEnabled()) {
                        String dataMapString = freefundClass.getDatamap();
                        String ivrId = null;
                        String ivrsClient = null;
                        if (dataMapString != null) {
                            Map dataMap = FCStringUtils.jsonToMap(dataMapString);
                            ivrId = String.valueOf(dataMap.get(FCConstants.IVRID));
                            ivrsClient = String.valueOf(dataMap.get(FCConstants.IVRSClient));
                            logger.debug("IVR id: " + ivrId);
                            logger.debug("ivrsClient: " + ivrsClient);
                        }
                        logger.debug("datamap: " + dataMapString);
                        if(FCUtil.isNotEmpty(ivrId) && FCUtil.isNotEmpty(ivrsClient)) {
                            Map<String, String> ivrsDataMap = new HashMap();
                            ivrsDataMap.put(FCConstants.PHONE_NUMBER, orderMetaData.getServiceNo());
                            ivrsDataMap.put(FCConstants.ORDER_ID, orderId);
                            ivrsDataMap.put(FCConstants.PROMOCODE, freefundCoupon.getFreefundCode());
                            ivrsDataMap.put(FCConstants.FREEFUND_CLASS_ID, String.valueOf(freefundClass.getId()));
                            ivrsDataMap.put(FCConstants.TIMESTAMP, Long.toString(System.currentTimeMillis()));
                            ivrsDataMap.put(FCConstants.IVRSClient, ivrsClient);
                            ivrsDataMap.put(FCConstants.IVRID, ivrId);
                            kestrelWrapper.enqueueIVRSCall(ivrsDataMap);
                            logger.info("Asynchronous IVR call enqueued  for mobile no: " + orderMetaData.getServiceNo() + ", orderId: " + orderId);
                        }
                    }
                }
            }
            long executionTime = System.currentTimeMillis() - startTime;
            metricsClient.recordLatency("CashbackProcessing", "TotalTime." + freefundClassId + ".ExecutionTime", executionTime);
        } catch (Exception e) {
            logger.error("Success Recharge - Freefund Fulfillment failed for OrderId : " + orderId + ", lookupId : " + lookupId, e);
        }
    }


    /**
     * Issues a new promocode of instant discount type for cashback type promocodes. Keeps original code in blocked state and
     * increments count in redemption repository. User must have satisfied all redeem conditions to avail new code, otherwise
     * the flow will be as usual : Old code will be unblocked and block counts decremented.
     */
    public void onRechargeFailure(String orderId, String lookupId, Float amount, String subscriberNumber) {
        logger.info("RechargeFailureEvent received for " + orderId);
        try {
            if (lookupId == null || orderId == null) {
                logger.error("RechargeSuccessEvent fired without lookupid/orderId.");
                return;
            }

            OrderMetaData orderMetaData = orderMetaDataService.getOrderMetaData(orderId);
            String imeiNumber = null;
            String advertisementId = null;
            String deviceUniqueId = null;
            if(orderMetaData != null){
                imeiNumber = orderMetaData.getImeiNumber();
                advertisementId = orderMetaData.getAdvertisementId();
                deviceUniqueId = orderMetaData.getDeviceUniqueId();
            }
            CartItemsBusinessDO freefundCartItem = freefundService.getFreeFundItem(lookupId);
            if (freefundCartItem != null) {
                Long freefundCouponId = freefundCartItem.getItemId().longValue();
                FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(freefundCouponId);
                FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
                Integer userId = orderIdDAO.getUserIdFromOrderId(orderId);

                //Only cashback type codes get a reIssuable promocode
                if(!(FCConstants.PRODUCT_ID_FREEFUND_CASHBACK == freefundCartItem.getProductMasterId())) {
                    handleRechargeFailureWithoutReIssuingPromocode(freefundCartItem, freefundClass, freefundCoupon,
                            orderId, orderMetaData, userId);
                } else { // Cashback promocode used + redeem conditions passed
                    try {
                        JSONObject versionObject = appConfigService.getCustomProp(FCConstants.PROMO_HANDLING_KEY);
                        String isPromoHandlingEnabled = String.valueOf(versionObject.get(FCConstants.IS_PROMO_HANDLING_ENABLED));
                        if ("true".equals(isPromoHandlingEnabled)) { // Controlling via config
                        } else {
                            handleRechargeFailureWithoutReIssuingPromocode(freefundCartItem, freefundClass,
                                    freefundCoupon, orderId, orderMetaData, userId);
                        }
                    } catch (RuntimeException e) {
                        logger.error("Exception reIssuing promocode on recharge failure ", e);
                        handleRechargeFailureWithoutReIssuingPromocode(freefundCartItem, freefundClass, freefundCoupon,
                                orderId, orderMetaData, userId);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Failed Recharge - Freefund Fulfillment failed for OrderId : " + orderId, e);
        }
    }

    private void setPropertiesOfReIssueCampaignToRequestMap(Map<String, Object> promocodeRequestMap,
                                                            FreefundClass freefundClassForReissuePromocodeCampaign,
                                                            FreefundClass freefundClass, Float discountValue) throws ParseException {
        String dataMapStr = freefundClassForReissuePromocodeCampaign.getDatamap();
        Map<String, String> dataMap = FCUtil.deserialize(dataMapStr);
        promocodeRequestMap.put(PromocodeConstants.CAMPAIGN_ID, String.valueOf(
                freefundClassForReissuePromocodeCampaign.getId()));
        promocodeRequestMap.put(PromocodeConstants.CODE_LENGTH, dataMap.get("length"));
        promocodeRequestMap.put(PromocodeConstants.PREFIX, dataMap.get("prefix"));
        promocodeRequestMap.put(PromocodeConstants.REDEEM_CONDITON_ID, String.valueOf(0));
        promocodeRequestMap.put(PromocodeConstants.PROMO_TYPE, FreefundClass.PROMO_ENTITY_ALL);
        promocodeRequestMap.put(PromocodeConstants.DISCOUNT_TYPE, FCConstants.DISCOUNT_TYPE_FLAT);
        promocodeRequestMap.put(PromocodeConstants.DISCOUNT_VALUE, String.valueOf(discountValue));
        Calendar validUptoCalender = freefundClass.getValidUpto();
        validUptoCalender.add(Calendar.DATE, getExtendedByDaysFromConfig());
        promocodeRequestMap.put(PromocodeConstants.VALID_UPTO,
                FCUtil.getFormattedDateFromCalender(validUptoCalender));
    }

    private void sendSmsToProfileNumberAndRechargeNumber(String orderId,
                                                         Users user, String subscriberNumber) {
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        smsBusinessDO.setReceiverNumber(user.getMobileNo());
        smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_REISSUED_PROMOCODE_HANDLE_ON_RECHARGE_FAILURE);
        Map<String, String> variableValuesMap = new HashMap<>();
        variableValuesMap.put("orderId", orderId);
        smsBusinessDO.setVariableValues(variableValuesMap);
        smsService.sendSMS(smsBusinessDO);
        smsBusinessDO.setReceiverNumber(subscriberNumber);
        smsService.sendSMS(smsBusinessDO);
    }

    private void handleRechargeFailureWithoutReIssuingPromocode(CartItemsBusinessDO freefundCartItem,
                                                                FreefundClass freefundClass,
                                                                FreefundCoupon freefundCoupon,
                                                                String orderId,
                                                                OrderMetaData orderMetaData,
                                                                int userId) {
        String failureReason = ErrorCode.RECHARGE_FAILED_NO_CASHBACK + ": Recharge has failed, hence the cashback is not done.";
        String detailedReasons = "Your transaction failed and as per the T&C of this offer, cashback is processed "
                + "only on successful transactions.";
        if (FCConstants.PRODUCT_ID_FREEFUND_CASHBACK == freefundCartItem.getProductMasterId() && !freefundClass.getIsNewCampaign()) {
            cashbackService.notifyPromocodeCashbackFailure(orderId, userId, freefundClass.getName(),
                    freefundCoupon.getFreefundCode(), failureReason, detailedReasons);
        }
        logger.info(" RechargeFailureEvent received " + freefundCoupon.getFreefundCode() + " ["
                + freefundCoupon.getFreefundClassId() + "] " + orderId);
        freefundService.releaseCouponCode(freefundCoupon, orderId, false,
                "Recharge failed event received. Hence unblocking", false, orderMetaData);
    }

    public int getExtendedByDaysFromConfig() {
        int extendBy = 1; // default
        String extendBydays = fcProperties.getProperty(PromocodeConstants.EXTEND_VALID_UPTO_BY_DAYS);
        if (FCUtil.isNotEmpty(extendBydays) && FCUtil.isInteger(extendBydays)) {
            extendBy = Integer.parseInt(extendBydays);
        }
        return extendBy;
    }

    public void handleFreefundFulfillment(String orderId, String lookupId, String inResponse) {

        if (fcProperties.isDevMode() || isSyncFreefundFulfillmentEnabled()) {
            logger.info("Sync frefund processing initiated for orderId : " + orderId + ", lookupId : " + lookupId
                    + ", inResponse : " + inResponse);
            proessFreefund(orderId, lookupId, inResponse);
        } else {

            try {
                Map<String,String> postData = new HashMap<String, String>();
                postData.put(ORDER_ID, orderId);
                postData.put(LOOKUP_ID, lookupId);
                postData.put(IN_RESPONSE, inResponse);

                kestrelWrapper.enqueueToMumbaiSQS(postData, fcProperties.getFreefundQueue(), fcProperties.getKestrelTimeOut());
                logger.info("Async Frefund Fulfillment item enqueued to " + fcProperties.getFreefundQueue()
                        + " for orderId : " + orderId + ", lookupId : " + lookupId + ", inResponse : " + inResponse);

            } catch(Exception e) {
                logger.error("Error queueing Frefund Fulfillment itme to " + fcProperties.getFreefundQueue()
                        + " for orderId : " + orderId + ", lookupId : " + lookupId + ", inResponse : " + inResponse
                        + ". Going ahead with sync frefund processing");
                proessFreefund(orderId, lookupId, inResponse);
            }
        }
    }

    @Transactional
    public void proessFreefund(String orderId, String lookupId, String inResponse) {
        InRequest inRequest = inService.getInRequest(orderId);
        if (inService.isSuccessfulStatusCode(inResponse)) {
            onRechargeSuccess(orderId, lookupId,
                    inRequest == null?
                            null:
                            inRequest.getAmount());
        } else if (inService.isFailure(inResponse)) {
            onRechargeFailure(orderId, lookupId, inRequest == null? null:inRequest.getAmount(),
                    inRequest == null ? null :inRequest.getSubscriberNumber());
        } else {
            logger.info("Transaction is under process for orderId : " + orderId + ", lookupId : " + lookupId
                    + ", inResponse : " + inResponse);
        }
    }

    private boolean isSyncFreefundFulfillmentEnabled() {

        if(!appConfigService.isChildPropertyEnabled(AppConfigService.FREEFUND_FULFILLMENT_CONFIG,
                AppConfigService.SYNC_FREEFUND_FULFILLMENT_ENABLED)) {
            return false;
        }

        return true;
    }

}
