package com.freecharge.freefund.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.freefund.dao.ApsalarEventDataWriteDao;
import com.freecharge.freefund.dos.entity.ApsalarEventData;

/**
 * This service has utilities to get/insert entries to apsalar_event_data table
 **/
@Service
public class ApsalarEventDataService {

    @Autowired
    private ApsalarEventDataWriteDao apsalarEvenDataWriteDao;

    public int insert(final ApsalarEventData apsalarEventData) throws IllegalAccessException {
        return apsalarEvenDataWriteDao.insert(apsalarEventData);
    }
}
