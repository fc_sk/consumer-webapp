package com.freecharge.freefund.services;

import static com.freecharge.admin.controller.UtmTrackerAdminController.FC_MASTER;
import static com.freecharge.admin.controller.UtmTrackerAdminController.FORWARD_URL;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_CAMPAIGN_ID;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_CAMPAIGN_NAME;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_CONTENT;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_KEYWORD;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_MEDIUM;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_SOURCE;
import static com.freecharge.admin.controller.UtmTrackerAdminController.UTM_URL;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.app.domain.dao.jdbc.UtmTrackerReadDao;
import com.freecharge.app.domain.dao.jdbc.UtmTrackerWriteDao;
import com.freecharge.app.domain.entity.jdbc.UtmTracker;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.reward.util.StringUtil;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Service
public class UtmTrackerService {
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    @Autowired
    private UtmTrackerWriteDao utmTrackerWriteDao;

    @Autowired
    private UtmTrackerReadDao utmTrackerReadDao;
    
    @Autowired
    @Qualifier("dynamoDBClient")
    private AmazonDynamoDBAsync dynamoDBClient;
    
    @Value("${dynamodb.region}")
    private String dynamoDbRegion;
    
    @PostConstruct
    void init() {
    	dynamoDBClient = AmazonDynamoDBAsyncClientBuilder.standard().withRegion(dynamoDbRegion).build();
    }


    public int createCampaign(String fcMaster, String campaignName, String medium, String source, String keyword,
                              String url, String content, String forwardUrl, String curUserEmail) {
        UtmTracker utmTracker = new UtmTracker();
        utmTracker.setCampaignName(campaignName);
        utmTracker.setFcMaster(fcMaster);
        utmTracker.setMedium(medium);
        utmTracker.setSource(source);
        utmTracker.setKeyword(keyword);
        utmTracker.setUrl(url);
        utmTracker.setContent(content);
        utmTracker.setForwardUrl(forwardUrl);
        utmTracker.setCreated_by(curUserEmail);

       return utmTrackerWriteDao.insert(utmTracker);
    }

    public void editCampaign(int campaignId, String fcMaster, String campaignName, String medium, String source,
                             String keyword, String url, String content, String forwardUrl, String curUserEmail) {
        UtmTracker utmTracker = new UtmTracker();
        utmTracker.setCampaignId(campaignId);
        utmTracker.setCampaignName(campaignName);
        utmTracker.setFcMaster(fcMaster);
        utmTracker.setMedium(medium);
        utmTracker.setSource(source);
        utmTracker.setKeyword(keyword);
        utmTracker.setUrl(url);
        utmTracker.setContent(content);
        utmTracker.setForwardUrl(forwardUrl);
        utmTracker.setUpdated_by(curUserEmail);
        utmTrackerWriteDao.editCampaign(utmTracker);
    }

    public List<UtmTracker> getUtmCampaign(String key, String value) {
        return utmTrackerReadDao.getUtmCampaign(key, value);
    }

    public List<UtmTracker> getAllCampaigns() {
       return utmTrackerReadDao.getAllCampaigns();
    }

    public String getDBColumnNameForKey(String key) {
        switch (key){
            case UTM_CAMPAIGN_NAME : return "utm_campaign_name";
            case UTM_CAMPAIGN_ID : return "utm_campaign_id";
            case UTM_CONTENT : return "utm_content";
            case UTM_SOURCE : return "utm_source";
            case UTM_MEDIUM : return "utm_medium";
            case UTM_URL : return "utm_url";
            case FC_MASTER: return "fc_master";
            case UTM_KEYWORD : return "utm_keyword";
            case FORWARD_URL : return "forward_url";
        }
        return key;
    }

    public List<UtmTracker> getUtmCampaignForMaster(String fcMaster, String campaignName) {
        return utmTrackerReadDao.getUtmCampaignforMaster(fcMaster, campaignName);
    }
    
    public void storeUtmDetailsInDynamo(String utmDetails,String channelId,String orderId) {
        logger.info("Putting utm details in dynamo db for order: "+orderId+" utmdetails: "+utmDetails);
        if(utmDetails==null || utmDetails.isEmpty()) {
            return;
        }
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<Map<String,String>>(){}.getType();
            Map<String, String>data=gson.fromJson(utmDetails, type);
            logger.info("utmDetails Map:- "+data);
            Map<String, AttributeValue> putItemData = new HashMap<>();
            if(data!=null) {
                if(!StringUtil.isBlank(data.get("utm_source"))) {
                    putItemData.put("utm_source", new AttributeValue(data.get("utm_source")));
                }
                if(!StringUtil.isBlank(data.get("utm_medium"))) {
                    putItemData.put("utm_medium", new AttributeValue(data.get("utm_medium")));
                }
                if(!StringUtil.isBlank(data.get("utm_term"))) {
                    putItemData.put("utm_term", new AttributeValue(data.get("utm_term")));
                }
                if(!StringUtil.isBlank(data.get("utm_content"))) {
                        putItemData.put("utm_content", new AttributeValue(data.get("utm_content")));
                }
                if(!StringUtil.isBlank(data.get("utm_campaign"))) {
                    putItemData.put("utm_campaign", new AttributeValue(data.get("utm_campaign")));
                }
                if(!StringUtil.isBlank(channelId)) {
                    putItemData.put("channel_id", new AttributeValue(channelId));
                }
            }
            putItemData.put("order_id", new AttributeValue(orderId));
            Long timeStamp = System.currentTimeMillis();
            putItemData.put("timestamp_in_sec", new AttributeValue().withN(Long.toString(timeStamp/1000L)));
            putItemData.put("timestamp_in_millisec", new AttributeValue().withN(Long.toString(timeStamp)));
            dynamoDBClient.putItem(new PutItemRequest("utm_details_order_mapping", putItemData));
        } catch (Exception e) {
            logger.error("Error in storing utmDetails in dynamo ",e);
        }
    }
}
