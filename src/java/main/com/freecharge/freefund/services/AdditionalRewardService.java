package com.freecharge.freefund.services;

import com.freecharge.freefund.dao.AdditionalRewardReadDao;
import com.freecharge.freefund.dao.AdditionalRewardWriteDao;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditionalRewardService {

    @Autowired
    private AdditionalRewardWriteDao additionalRewardWriteDao;

    @Autowired
    private AdditionalRewardReadDao additionalrewardReadDao;

    public int create(AdditionalReward additionalReward) {
        return additionalRewardWriteDao.create(additionalReward);
    }

    public List<AdditionalReward> getByRewardId(int rewardId) {
        return additionalrewardReadDao.getById(rewardId);
    }

    public int edit(AdditionalReward additionalReward) {
        return additionalRewardWriteDao.edit(additionalReward);
    }

    public List<AdditionalReward> getByRewardName(String rewardName) {
        return additionalrewardReadDao.getByRewardName(rewardName);
    }

    public List<AdditionalReward> getAllActiveAdditionalRewards() {
        return additionalrewardReadDao.getAllActiveAdditionalRewards();
    }

    public boolean disableAdditionalReward(int rewardId) {
        return additionalRewardWriteDao.disableAdditionalReward(rewardId);
    }
}
