package com.freecharge.freefund.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.mongo.repos.IPRepository;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 10/12/13
 * Time: 9:19 AM
 * To change this template use File | Settings | File Templates.
 */
@Service("ipService")
public class IPService {
    
    private Logger logger = LoggingFactory.getLogger(IPService.class);
    
    @Autowired
    private IPRepository ipRepository;

    public void recordIP(String ip, Integer userId, String promocode, Long freefundClassId) {
     // IP will be blank for App requests, and we do not want to register blank IP records 
        if (FCStringUtils.isBlank(ip) || (userId != null && userId <=0)) {  
            logger.error("Not recording IP Details for ip : " + ip + " , userId : " + String.valueOf(userId));
            return;
        }
        this.ipRepository.insert(ip, userId, promocode, freefundClassId);
    }

    public Integer getIpCount(String ip, Long freefundClassId){
        return this.ipRepository.getIPCount(ip, freefundClassId);
    }
    
    public void removeIP(String ip, Integer userId, String promocode, Long freefundClassId) {
      
        if (FCStringUtils.isBlank(ip) || (userId != null && userId <=0)) {  
            logger.error("Not removing IP Details for ip : " + ip + " , userId : " + String.valueOf(userId));
            return;
        }
        this.ipRepository.deleteOneRecord(ip, userId, promocode, freefundClassId);
    }
}
