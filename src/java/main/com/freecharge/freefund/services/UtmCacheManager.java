package com.freecharge.freefund.services;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.jdbc.UtmShortCode;
import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class UtmCacheManager extends RedisCacheManager {

		private Logger logger = LoggingFactory.getLogger(getClass());

	    @Autowired
	    private RedisTemplate<String, Object> utmCacheTemplate;
	    
	    private static final String UTM_SHORT_CODE = "utm_short_code_";
	    
	    @Override
	    protected RedisTemplate<String, Object> getCacheTemplate() {
	        return this.utmCacheTemplate;
	    }
	    
	    public List<UtmShortCode> getShortCodeDetails(String shortCode) {
	        try {
	            return (List<UtmShortCode>) get(UTM_SHORT_CODE+shortCode);
	        } catch (Exception e) {
	            logger.error("Error in getting cache data for shortCode "+shortCode, e);
	            return null;
	        }
	    }

	    public void setShortCodeDetails(List<UtmShortCode> utmShortCodes,String shortCode) {
	        if(utmShortCodes==null || utmShortCodes.size()==0){
	            return;
	        }
	        try {
	            set(UTM_SHORT_CODE+shortCode, utmShortCodes, 6, TimeUnit.HOURS);
	        } catch (Exception e) {
	            logger.error("Error in setting cache data for shortCode "+shortCode, e);
	        }
	    }
	    
}
