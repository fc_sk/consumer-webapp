package com.freecharge.freefund.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.UtmShortCodeReadDao;
import com.freecharge.app.domain.dao.jdbc.UtmShortCodeWriteDao;
import com.freecharge.app.domain.entity.jdbc.UtmShortCode;

@Service
public class UtmShortCodeService {

    @Autowired
    private UtmShortCodeReadDao utmShortCodeReadDao;

    @Autowired
    private UtmShortCodeWriteDao utmShortCodeWriteDao;
    
    @Autowired
    private UtmCacheManager utmCacheManager;

    public List<UtmShortCode> getShortCodeDetails(String shortCode) {
    	List<UtmShortCode>  utmShortCodes = utmCacheManager.getShortCodeDetails(shortCode);
    	if(utmShortCodes!=null) {
    		return utmShortCodes;
    	}
    	utmShortCodes = utmShortCodeReadDao.getShortCodeDetails(shortCode);
    	utmCacheManager.setShortCodeDetails(utmShortCodes, shortCode);
        return utmShortCodes;
    }

    public int createShortCode(String shortCode, int campaignId, String webUrl, String mobileWebUrl,
                                String androidAppUrl, String iosAppUrl, String windowsAppUrl, Date startDate,
                                Date endDate, String createdBy) {
        UtmShortCode utmShortCode = new UtmShortCode();
        utmShortCode.setShortCode(shortCode);
        utmShortCode.setCampaignId(campaignId);
        utmShortCode.setWebUrl(webUrl);
        utmShortCode.setMobileWebUrl(mobileWebUrl);
        utmShortCode.setAndroidAppUrl(androidAppUrl);
        utmShortCode.setIosAppUrl(iosAppUrl);
        utmShortCode.setWindowsAppUrl(windowsAppUrl);
        utmShortCode.setCreatedBy(createdBy);
        utmShortCode.setStartDate(startDate);
        utmShortCode.setEndDate(endDate);

        return utmShortCodeWriteDao.insert(utmShortCode);
    }

    public void editShortCode(int shortCodeId, String shortCode, int campaignId, String webUrl, String mobileWebUrl,
                              String androidAppUrl, String iosAppUrl, String windowsAppUrl, Date startDate,
                              Date endDate, String createdBy) {
        UtmShortCode utmShortCode = new UtmShortCode();
        utmShortCode.setShortCodeId(shortCodeId);
        utmShortCode.setShortCode(shortCode);
        utmShortCode.setCampaignId(campaignId);
        utmShortCode.setWebUrl(webUrl);
        utmShortCode.setMobileWebUrl(mobileWebUrl);
        utmShortCode.setAndroidAppUrl(androidAppUrl);
        utmShortCode.setIosAppUrl(iosAppUrl);
        utmShortCode.setWindowsAppUrl(windowsAppUrl);
        utmShortCode.setCreatedBy(createdBy);
        utmShortCode.setStartDate(startDate);
        utmShortCode.setEndDate(endDate);
        utmShortCodeWriteDao.editShortCode(utmShortCode);
    }

    public List<UtmShortCode> getAllShortCodeDetails() {
        return utmShortCodeReadDao.getAllShortCodeDetails();
    }
}
