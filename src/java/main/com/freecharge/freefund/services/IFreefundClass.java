package com.freecharge.freefund.services;

import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;

public interface IFreefundClass {
    
    public void postValidateAction(FreefundRequestBusinessDO request, Object obj);

}
