package com.freecharge.freefund.services;


import com.freecharge.freefund.constants.PhonebookDump;
import com.freecharge.freefund.dao.PhonebookDumpWriteDao;
import com.freecharge.freefund.dos.entity.PhonebookDumpDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhonebookDumpService {

    @Autowired
    private PhonebookDumpWriteDao phonebookDumpWriteDao;

    private void dumpPhonebook(PhonebookDump status, String body) {
        PhonebookDumpDO dump = new PhonebookDumpDO();
        dump.setBody(body);
        dump.setStatus(status.toString());
        phonebookDumpWriteDao.insert(dump);    	
    }
    public void dumpPhonebook(String body) {
    	dumpPhonebook(PhonebookDump.UNPROCESSED,body);
    }
    public void dumpZIPPhonebook(String body) {
    	dumpPhonebook(PhonebookDump.UNPROCESSED_ZIP,body);
    }
}
