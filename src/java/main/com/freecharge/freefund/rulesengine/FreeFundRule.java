package com.freecharge.freefund.rulesengine;

import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;

public interface FreeFundRule {
    public boolean evaluate(FreefundRequestBusinessDO requestDO);
}
