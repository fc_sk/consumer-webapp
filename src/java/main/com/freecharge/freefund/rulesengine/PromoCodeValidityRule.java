package com.freecharge.freefund.rulesengine;

import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;

/**
 * This is a promotion code validity rule that validates the following.
 * <ul>
 * <li> Given promo code is valid.
 * <li> Given promo code has not expired.
 * </ul>
 * 
 * @author arun
 *
 */
public class PromoCodeValidityRule implements FreeFundRule {
	
	@Autowired
	private FreefundService freeFundService; 

	@Override
	public boolean evaluate(FreefundRequestBusinessDO requestDO) {
		if(requestDO.getCouponCode() == null || requestDO.getCouponCode().isEmpty()) {
			return false;
		}
		
		FreefundCoupon freefundCoupon = null;
		freefundCoupon = freeFundService.getAvailableCoupon(requestDO.getCouponCode());
		if(freefundCoupon == null) {
			return false;
		}
		
		requestDO.setFreefundCoupon(freefundCoupon);
		requestDO.setCouponId(freefundCoupon.getId());
		
		return true;
	}
}
