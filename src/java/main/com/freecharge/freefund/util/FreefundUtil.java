package com.freecharge.freefund.util;

import java.util.HashMap;
import java.util.Map;

public class FreefundUtil {

    public static final Map<Integer, String> returnType = new HashMap<Integer,String>();

    static{
        returnType.put(1,"java.lang.Integer");
        returnType.put(2,"java.lang.Double");
        returnType.put(3,"java.lang.String");
        returnType.put(4,"java.lang.Boolean");
        returnType.put(5,"java.lang.Object");
    }
    
    public static enum CampaignSponsorshipEnum {  
        CLIENT_SPONSORED, FREECHARGE_SPONSORED, CO_PAID;       
    }
}
