package com.freecharge.freefund.constants;

/**
 * Constants for postback data
 **/

public final class PostbackConstants {

    public static final String DATETIME = "conversion_datetime";
    public static final String APP_NAME = "app_name";
    public static final String CAMPAIGN_NAME = "campaign_name";
    public static final String PUBLISHER = "publisher_name";
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_IP = "device_ip";
    public static final String DEVICE_OS = "device_os";
    public static final String OS_ID = "os_id";
    public static final String REVENUE_USD = "revenue_usd";
    public static final String EVENT_NAME = "event_name";
    public static final String PUBLISHER_SUB_PUBLISHER_NAME = "publisher_sub_publisher_name";
    public static final String PUBLISHER_SUB_ADGROUP_NAME = "publisher_sub_adgroup_name";
    public static final String COUNTRY_NAME = "country_name";
    public static final String BUNDLE_ID = "bundle_id";
    public static final String CREATIVE_ID = "creative_id";
    public static final String GOOGLE_AID = "google_aid";
    public static final String SHA1_ANDROID_ID = "sha1_android_id";
    public static final String APSALAR_INTERNAL_DEVICE_ID = "apsalar_internal_device_id";
    public static final String FB_CAMPAIGN_ID = "fb_campaign_id";
    public static final String FB_CAMPAIGN_NAME = "fb_campaign_name";
    public static final String FB_AD_SET_ID = "fb_ad_set_id";
    public static final String FB_AD_SET_NAME = "fb_ad_set_name";
    public static final String FB_AD_ID = "fb_ad_id";
    public static final String FB_AD_NAME = "fb_ad_name";
    public static final String TWITTER_CAMPAIGN_NAME = "twitter_campaign_name";
    public static final String TWITTER_CAMPAIGN_ID = "twitter_campaign_id";
    public static final String TWITTER_LINE_ITEM_ID = "twitter_line_item_id";
    public static final String TWEET_ID = "tweet_id";

    private PostbackConstants() { }
}
