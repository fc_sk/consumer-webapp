package com.freecharge.freefund.web.controller;


import com.freecharge.admin.controller.UtmTrackerAdminController;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.domain.entity.jdbc.UtmShortCode;
import com.freecharge.app.domain.entity.jdbc.UtmTracker;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.UtmRedirectionTrackingService;
import com.freecharge.freefund.services.UtmShortCodeService;
import com.freecharge.freefund.services.UtmTrackerService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import net.sf.uadetector.DeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/")
public class UtmTrackerController {

    private static final String DEFAULT_URL = "https://www.freecharge.in";

    private static Logger logger = LoggingFactory.getLogger(UtmTrackerController.class);

    @Autowired
    private UtmShortCodeService utmShortCodeService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private UtmRedirectionTrackingService utmRedirectionTrackingService;

    @Autowired
    private UtmTrackerService utmTrackerService;

    @Autowired
    private MetricsClient metricsClient;

    @NoLogin
    @RequestMapping(value = "get-app", method = RequestMethod.GET)
    public void getApp(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long startTime = System.currentTimeMillis();
        List<UtmShortCode> utmShortCodes = utmShortCodeService.getShortCodeDetails("get-app");
        UtmShortCode utmShortCode = FCUtil.getFirstElement(utmShortCodes);
        Users user= userServiceProxy.getLoggedInUser();
        int userId = 0;
        if (user != null) {
            userId = user.getUserId();
        }
        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        ReadableUserAgent agent = parser.parse(request.getHeader("User-Agent"));
        if (utmShortCode != null && agent != null) {
            String forwardUrl = getForwardUrlAndTrack(agent, utmShortCode, userId, "get-app");
            if (!FCUtil.isEmpty(forwardUrl)) {
                response.sendRedirect(forwardUrl);
            }
        } else {
            response.sendRedirect(DEFAULT_URL);
        }
        long executionTime = System.currentTimeMillis() - startTime;
        metricsClient.recordLatency("UrlRedirection", "get-app" + ".ExecutionTime", executionTime);
    }

    @NoLogin
    @RequestMapping(value = "fc/*", method = RequestMethod.GET)
    public void sendRedirectForShortCode(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        long startTime = System.currentTimeMillis();
        String shortCode = null;
        String requestUri = request.getRequestURI();
        String[] requestUriParts = requestUri.split("/");
        if (requestUriParts != null && requestUriParts.length == 3) {
            shortCode = requestUriParts[2];
            List<UtmShortCode> utmShortCodes = utmShortCodeService.getShortCodeDetails(shortCode);
            UtmShortCode utmShortCode = FCUtil.getFirstElement(utmShortCodes);
            Users user= userServiceProxy.getLoggedInUser();
            int userId = 0;
            if (user != null) {
                userId = user.getUserId();
            }
            UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
            ReadableUserAgent agent = parser.parse(request.getHeader("User-Agent"));
            if (utmShortCode != null && agent != null) {
                String forwardUrl = getForwardUrlAndTrack(agent, utmShortCode, userId, shortCode);
                if (!FCUtil.isEmpty(forwardUrl)) {
                    if (forwardUrl.contains("?")) {
                        response.sendRedirect(forwardUrl + request.getQueryString());
                    } else {
                        response.sendRedirect(forwardUrl + "?" + request.getQueryString());
                    }
                } else {
                    response.sendRedirect(DEFAULT_URL);
                }
            } else {
                logger.info("Values of shortcodeObject " + utmShortCode + " agent " + agent);
                request.getRequestDispatcher("/get-app").forward(request, response);
            }
        } else {
          request.getRequestDispatcher("/get-app").forward(request, response);
        }
        long executionTime = System.currentTimeMillis() - startTime;
        metricsClient.recordLatency("UrlRedirection", shortCode + ".ExecutionTime", executionTime);
    }

    @NoLogin
    @RequestMapping(value = "trackio", method = RequestMethod.GET)
    public void trackerUrl(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fcMaster = request.getParameter(UtmTrackerAdminController.FC_MASTER);
        String campaignName = request.getParameter(UtmTrackerAdminController.UTM_CAMPAIGN_NAME);

        List<UtmTracker> utmTrackerList = utmTrackerService.getUtmCampaignForMaster(fcMaster, campaignName);
        if (!FCUtil.isEmpty(utmTrackerList)) {
            UtmTracker utmTracker = FCUtil.getFirstElement(utmTrackerList);
            String forwardUrl = utmTracker.getForwardUrl();
            if (!FCUtil.isEmpty(forwardUrl)) {
                response.sendRedirect(forwardUrl);
                return;
            } else {
                response.sendRedirect(DEFAULT_URL);
            }
        } else {
            response.sendRedirect(DEFAULT_URL);
        }
    }

    public String getForwardUrlAndTrack(ReadableUserAgent agent, UtmShortCode utmShortCode, int userId, String shortCode) {
        String forwardUrl = null;
        int channelId = 0;
        switch (agent.getOperatingSystem().getFamily()) {
            case ANDROID: if (DeviceCategory.Category.SMARTPHONE.equals(agent.getDeviceCategory().getCategory())) {
                forwardUrl = utmShortCode.getAndroidAppUrl();
                channelId = 3;
            }
                break;
            case IOS: if (DeviceCategory.Category.SMARTPHONE.equals(agent.getDeviceCategory().getCategory())) {
                forwardUrl = utmShortCode.getIosAppUrl();
                channelId = 5;
            }
                break;
            case WINDOWS: if (DeviceCategory.Category.SMARTPHONE.equals(agent.getDeviceCategory().getCategory())) {
                forwardUrl = utmShortCode.getWindowsAppUrl();
                channelId = 6;
            } else if (DeviceCategory.Category.PERSONAL_COMPUTER.equals(agent.getDeviceCategory().getCategory())){
                forwardUrl = utmShortCode.getWebUrl();
                channelId = 1;
            }
                break;
            default: if (DeviceCategory.Category.SMARTPHONE.equals(agent.getDeviceCategory().getCategory())) {
                forwardUrl = utmShortCode.getMobileWebUrl();
                channelId = 2;
            } else if (DeviceCategory.Category.PERSONAL_COMPUTER.equals(agent.getDeviceCategory().getCategory()) ||
                       DeviceCategory.Category.TABLET.equals(agent.getDeviceCategory().getCategory())) {
                forwardUrl = utmShortCode.getWebUrl();
                channelId = 1;
            }
        }

        utmRedirectionTrackingService.insert(shortCode, userId, channelId, forwardUrl);
        logger.info(agent.getName() + " on " + agent.getOperatingSystem().getName() + " redirected to " + forwardUrl
                + " os family " + agent.getOperatingSystem().getFamily() + " and device category " +
                agent.getDeviceCategory().getCategory() + " for short code " + shortCode);
        return forwardUrl;
    }
}
