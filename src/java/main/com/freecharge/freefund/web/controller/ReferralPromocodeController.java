package com.freecharge.freefund.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.PromocodeCampaign;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.util.RandomCodeGenerator;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.dos.RefferalResponse;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.UserReferralPromocodeRepository;
import com.freecharge.mongo.service.UserReferralPromocodeService;
import com.mongodb.DBObject;


@Controller
public class ReferralPromocodeController {

	public static final String          REWARD_EARNED          = "rewardEarned";

	@Autowired
	private FreefundService             freefundService;

	@Autowired
	private UserServiceProxy                 userServiceProxy;

	@Autowired
	private UserReferralPromocodeService userReferralPromocodeService;

	private final Logger                logger                     = LoggingFactory.getLogger(getClass());

	/*  Api to generate/retrieve promocode unique to a user. If promocode already exists for a
	 *  email id, it is fetched, if not a new one is generated and stored in DB. CampaignID acts
	 *  as freefund class id and appropriate entries are made based on the same. The user with
	 *  the emailid can distribute the code among his friends and have himself and his friends
	 *  rewarded.
	 *  Input Params :
	 *      email - email id of the user for whom code is to be generated.
	 *      campaignId - Freefund class id for the receivers. The generated promocdoe belongs to this class.
	 *      secret - shared between client and server for auth. Find referral.api.secret in any .properties file
	 *      codePrefix - Prefix for the generated code. (default REF)
	 *      codeType - 1 for numeric only, 2 for alpha numeric  (default alphanumeric)
	 *      codeLength - Length of the generated code (default 10)
	 *  Output Params:
	 *      status -  success or failure
	 *      message - cause for failure, if any
	 *      code - the generated code, if successful
	 *  */
	@RequestMapping(value = "/promocode/getcode", method = RequestMethod.POST, produces = "application/json")
	@NoLogin
	@Csrf(exclude = true)
	public String getPromoCode(HttpServletRequest request, Model model) {
		String apiSecret = request.getHeader("secret");
		String email = request.getParameter(FCConstants.EMAIL);
		String campaignId = request.getParameter(FCConstants.CAMPAIGN_ID);
		String codePrefix = request.getParameter(FCConstants.CODE_PREFIX);
		String codeType = request.getParameter(FCConstants.CODE_TYPE);
		String codeLength = request.getParameter(FCConstants.CODE_LENGTH);
		String code = null;

		Map<String, String> validationMap = userReferralPromocodeService.validateRequest(email, campaignId, apiSecret);
		if (validationMap != null && "failure".equals(validationMap.get("status"))) {
			model.addAttribute("status", validationMap.get("status"));
			model.addAttribute("message", validationMap.get("message"));
			return "jsonView";
		}

		Users user = userServiceProxy.getUserByEmailId(email);

		String existingPromocodeId = userReferralPromocodeService.getUserDetailByPropertyName(String.valueOf(user.getUserId()),
				campaignId, UserReferralPromocodeRepository.PROMOCODE_ID);
		if (!FCStringUtils.isBlank(existingPromocodeId)) {
			FreefundCoupon coupon  = freefundService.getFreefundCoupon(Long.parseLong(existingPromocodeId));
			code = coupon.getFreefundCode();
		}

		if (!FCStringUtils.isBlank(code)) {
			logger.info("Code already exists for email id  " + email + ". Code is " + code);
			model.addAttribute("status", "success");
			model.addAttribute("code", code);
			return "jsonView";
		}
		RandomCodeGenerator randomCodeGenerator = new RandomCodeGenerator();
		do {
			String prefix = freefundService.getCodePrefix(codePrefix);
			String length = freefundService.getCodeLength(codeLength);
			RandomCodeGenerator.TYPE type = freefundService.getCodeType(codeType);

			randomCodeGenerator.setParams(prefix, FCConstants.NUMBER_OF_CODES, length, type);
		} while (freefundService.checkIfCodeExists(randomCodeGenerator.getCodeWithPrefix()));

		code = randomCodeGenerator.getCodeWithPrefix();

		if (!freefundService.insertIntoFreefundCouponClass(campaignId, code)){
			logger.info("Couldnt create coupon for email " + email);
			model.addAttribute("status", "failure");
			model.addAttribute("message", "Couldnt create coupon for email " + email);
			return "jsonView";
		}

		FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(code);
		Long promocodeId = freefundCoupon.getId();
		if(!userReferralPromocodeService.storeUserReferrelPromocodeMapping(String.valueOf(user.getUserId()),
				String.valueOf(promocodeId), String.valueOf(campaignId),String.valueOf(0))) {
			logger.info("Couldnt create coupon for email " + email);
			model.addAttribute("status", "failure");
			model.addAttribute("message", "Couldnt create coupon for email " + email);
			return "jsonView";
		}

		logger.info("Code for email " + email + " is " + code);
		model.addAttribute("status", "success");
		model.addAttribute("code", code);
		return "jsonView";
	}


	@RequestMapping(value = "/protected/promocode/getreferraldetailsbyemailid", method = RequestMethod.GET, produces = "application/json")
	@NoLogin
	@Csrf(exclude = true)
	public @ResponseBody List<RefferalResponse> getReferralDetailsbyEmailid(HttpServletRequest request, Model model) {
		String emailId = request.getParameter(FCConstants.EMAIL);
		List<RefferalResponse> result = new ArrayList();
		try {
			List<Map<String, String>> userReferralData = null;
			Users referrerUser = userServiceProxy.getUserByEmailId(emailId);
			if(null == referrerUser || null == referrerUser.getUserId()) {
				logger.error("Unable to find user for emailId: " + emailId);
				RefferalResponse response = new RefferalResponse();
				response.setStatus("Failure, Unable to find user for emailId");
				result.add(response);
			}
			Map<String, Double> totalReferralRewardMap = new HashMap();
			userReferralData = userReferralPromocodeService.getMappingByUserId(Integer.toString(referrerUser.getUserId()));
			if(userReferralData != null && userReferralData.size() > 0) {
				for(Map<String, String> singleEntry : userReferralData) {
					if(totalReferralRewardMap.containsKey(singleEntry.get("promocodeId"))) {
						totalReferralRewardMap.put(singleEntry.get("promocodeId"), totalReferralRewardMap.get(singleEntry.get("promocodeId")) + Double.parseDouble(singleEntry.get("amountRewarded")));
					}else {
						totalReferralRewardMap.put(singleEntry.get("promocodeId"), Double.parseDouble(singleEntry.get("amountRewarded")));
					}

				}
			}else{
				logger.info("userReferralData is null for referrerUser.getUserId(): " + referrerUser.getUserId());
			}
			if(totalReferralRewardMap == null || totalReferralRewardMap.size() < 1) {
				logger.info("totalReferralRewardMap is null emailId: " + emailId +" can;t proceed furthur");
			}else{
				for(Map.Entry<String,Double> entity : totalReferralRewardMap.entrySet()) {
					RefferalResponse response = new RefferalResponse();
					FreefundCoupon freeFundCoupon = freefundService.getFreefundCoupon(Long.valueOf(entity.getKey()));
					if(null == freeFundCoupon) {
						logger.error("could not found freefundcoupon for promocodeId: " + entity.getKey());
						continue;
					}
					response.setReferralCode(freeFundCoupon.getFreefundCode());
					response.setCashbackEarned(entity.getValue());
					List<PromocodeCampaign> promocodeCampaignList = freefundService.getAllPromocodeCampaignFromFreeFund(freeFundCoupon.getId());
					List<String> orderIdList = new ArrayList();
					if(promocodeCampaignList != null) {
						for(PromocodeCampaign promocodeCampaign : promocodeCampaignList ) {
							orderIdList.add(promocodeCampaign.getOrderId());
						}
						response.setOrderList(orderIdList);
						result.add(response);
					}else {
						String s = "No orders";
						orderIdList.add(s);
						response.setOrderList(orderIdList);
						result.add(response);
					}

				}
			}
		} catch (Exception e) {
			logger.error("Exception caught while getreferraldetailsbyemailid for emailId: "+ emailId,e);
			model.addAttribute("status", "failure");
		}

		return result;
	}

	@RequestMapping(value = "/protected/promocode/getreferralcodeapplydetails", method = RequestMethod.GET, produces = "application/json")
	@NoLogin
	@Csrf(exclude = true)
	public String getReferralCodeApplyDetails(HttpServletRequest request, Model model) {
		String referralcode = request.getParameter("referralcode");

		try {
			List<Map<String, String>> userReferralData = null;
			FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(referralcode);
			if(null == freefundCoupon || null == freefundCoupon.getFreefundCode()) {
				logger.error("Unable to find freefundCoupon for referralcode: " + referralcode);
				model.addAttribute("status", "failure");
				model.addAttribute("message", "Couldnt find referral details for referralcode " + referralcode);
				return "jsonView";
			}
			userReferralData = userReferralPromocodeService.getMappingByPromocodeId(String.valueOf(freefundCoupon.getId()));
			Map<String, String> singleMap = FCUtil.getFirstElement(userReferralData);
			if(null == singleMap || null == singleMap.get("promocodeId") || null == singleMap.get("userId")) {
				logger.error("Unable to find userReferralData for promocodeId: " + freefundCoupon.getId());
				model.addAttribute("status", "failure");
				model.addAttribute("message", "Couldnt find referral details for referralcode " + referralcode);
				return "jsonView";
			}
			Users referrerUser = userServiceProxy.getUserByUserId(Integer.valueOf(singleMap.get("userId")));
			if(null == referrerUser || null == referrerUser.getEmail()) {
				logger.error("Unable to find referrerUser for userId: " + singleMap.get("userId"));
				model.addAttribute("status", "failure");
				model.addAttribute("message", "Couldnt find referral details for referralcode " + referralcode);
				return "jsonView";
			}
			Double totalRewardValue = 0D;
			if(userReferralData != null && userReferralData.size() > 0) {
				for(Map<String, String> singleEntry : userReferralData) {
					totalRewardValue += Double.parseDouble(singleEntry.get("amountRewarded"));
				}
			}else{
				logger.info("userReferralData is zero for freefundcoupon: " + freefundCoupon.getId());
			}

			model.addAttribute("Email Id",referrerUser.getEmail());
			model.addAttribute("Total cashback earned",totalRewardValue);
			List<PromocodeCampaign> promocodeCampaignList = freefundService.getAllPromocodeCampaignFromFreeFund(freefundCoupon.getId());
			List<String> orderIdList = new ArrayList();
			if(promocodeCampaignList != null) {
				for(PromocodeCampaign promocodeCampaign : promocodeCampaignList ) {
					orderIdList.add(promocodeCampaign.getOrderId());
				}
				model.addAttribute("OrderId List",orderIdList);
			}else {
				model.addAttribute("OrderId List","No orders");
			}
		} catch (Exception e) {
			logger.error("Exception caught white fetching referralcodeapplydetails for refferal code: " + referralcode, e);
			model.addAttribute("status", "failure");
		}
		return "jsonView";
	}

	/*  Api to retrieve referral reward earned by user against a CampaignID (unique promocode).
	 *  Input Params :
	 *      email - email id of the user
	 *      campaignId - FreeFund class id against which reward earned to be retrieved
	 *      secret - shared between client and server for auth. Find referral.api.secret in any .properties file
	 *  Output Params:
	 *      status -  success or failure
	 *      message - cause for failure, if any
	 *      email - email id of the user
	 *      campaignId - FreeFund class id against which reward earned to be retrieved
	 *      rewardEarned - amount rewarded to user till date, if any
	 *  */


	@RequestMapping(value = "/promocode/getRewardEarned", method = RequestMethod.POST, produces = "application/json")
	@NoLogin
	@Csrf(exclude = true)
	public String getRewardEarned(HttpServletRequest request, Model model) {

		try {
			String apiSecret = request.getHeader("secret");
			String email = request.getParameter(FCConstants.EMAIL);
			String campaignId = request.getParameter(FCConstants.CAMPAIGN_ID);

			model.addAttribute(FCConstants.EMAIL, email);
			model.addAttribute(FCConstants.CAMPAIGN_ID, campaignId);

			Map<String, String> validationMap = userReferralPromocodeService.validateRequest(email, campaignId, apiSecret);
			if (validationMap != null && "failure".equals(validationMap.get("status"))) {
				model.addAttribute("status", validationMap.get("status"));
				model.addAttribute("message", validationMap.get("message"));
				return "jsonView";
			}

			Users user = userServiceProxy.getUserByEmailId(email);
			Double rewardEarned = userReferralPromocodeService.getRewardEarned(String.valueOf(user.getUserId()),
					campaignId, UserReferralPromocodeRepository.AMOUNT_REWARDED);

			model.addAttribute("status", FCConstants.SUCCESS);
			model.addAttribute(REWARD_EARNED, rewardEarned.intValue());
			logger.info("Reward Earned [" + rewardEarned + "] details retrieved successfully for email : " + email
					+ ", campaignId : " + campaignId);

		} catch (Exception e) {
			logger.error(ErrorCode.REFERRAL_API_ERROR +  " - Reward Earned retrieval failed for email : "
					+ request.getParameter(FCConstants.EMAIL) + ", campaignId : "
					+ request.getParameter(FCConstants.CAMPAIGN_ID), e);
		}
		return "jsonView";
	}
}
