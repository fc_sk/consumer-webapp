package com.freecharge.freefund.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.CaptchaService;

@Controller
@RequestMapping("/captcha/*")
public class CaptchaController {

    private static final long serialVersionUID = 1490947492185481844L;

    @Autowired
    CaptchaService            captchaService;

    @RequestMapping(value = "draw", method = RequestMethod.GET)
    @NoLogin
    public void draw(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest req,
            HttpServletResponse resp) throws IOException {
        FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();

        // HttpSession session = req.getSession(false);

        captchaService.generateToken(fcSession);
        String token = fcSession != null ? captchaService.getToken(fcSession) : null;
        if (token == null || captchaService.isTokenUsed(fcSession)) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Captcha not found.");

            return;
        }

        captchaService.setResponseHeaders(resp);
        // captchaService.markTokenUsed(session, true);
        captchaService.draw(token, resp.getOutputStream());
        return;
    }

    @RequestMapping(value = "regenerate")
    @NoLogin
    public String regenerate(@RequestParam Map<String, String> mapping, Model model, HttpServletRequest req,
            HttpServletResponse resp) {
        return "captcha-draw";
    }

}
