package com.freecharge.freefund.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.services.BinOfferCache;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;

@Controller
public class BinOfferController extends BaseAdminController {
	public static final String ADMIN_HOME_LINK = "/admin/binoffer/home.htm";
	public static final String BINOFFER_CHECK_LINK = "/app/binoffer/check.htm";

	private final Logger LOGGER = LoggingFactory.getLogger(getClass());

	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

	@Autowired
	BinOfferService binOfferService;

	@Autowired
	FreefundService freefundService;

	@Autowired
	CartService cartService;

	@Autowired
	private BinOfferCache binOfferCache;

	@RequestMapping(value = "/app/binoffer/list", method = RequestMethod.GET)
	public String homeList(HttpServletRequest request, HttpServletResponse response, Model model) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Integer userId = -1;
        try {
            Map map = fs.getSessionData();
            Boolean isloggedin = false;
            if (map != null) {
                String email = (String) map.get(WebConstants.SESSION_USER_EMAIL_ID);
                try {
                	userId = Integer.parseInt((String) map.get(WebConstants.SESSION_USER_USERID));
				}
				catch (Exception e) {
					LOGGER.error("Error parsing session useri Id " + (String) map.get(WebConstants.SESSION_USER_USERID));
				}
            }
        } catch (Exception e) {
			// TODO: handle exception
		}
		Map<String, BinOffer> binOffers = binOfferService.getActiveOffers(userId);
		model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO_MAP, binOffers);
		return ViewConstants.BINOFFER_HOME_VIEW_LIST;
	}

	@RequestMapping(value = "/app/binoffer/gettnc/{id}", method = RequestMethod.GET)
	public String getTnC(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response, Model model) {
		BinOffer binOffer = binOfferService.getBinOfferById(id);
		model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO, binOffer);
		return ViewConstants.BINOFFER_HOME_VIEW_TNC;
	}

	@RequestMapping(value = "/app/binoffer/check", method = RequestMethod.GET)
	public String check(HttpServletRequest request, HttpServletResponse response, Model model) {
		String cardNumber = request.getParameter("card");
		String lid = request.getParameter("lid");
		if (!checkCouponDataSanity(cardNumber)) {
			model.addAttribute(WebConstants.STATUS, "failure");
			model.addAttribute("message", "Please enter a valid card number");
			return "jsonView";
		}
		List<BinOffer> binOffers = binOfferService.getAllActive();
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Integer userId = ((Integer) fs.getSessionData().get(WebConstants.SESSION_USER_USERID));
		binOffers = binOfferService.validateForUser(binOffers, userId);
		BinOffer binOffer = binOfferService.getValidBinOffer(binOffers, cardNumber);
		if (binOffer != null) {
			boolean isValidAmount = binOfferService.isValidAmount(binOffer, lid);
	        if(!isValidAmount) {
	        	model.addAttribute(WebConstants.STATUS, "failure");
	    		model.addAttribute("message",  "So the good news is your card is valid. But to avail the discount, you need a minimum recharge value of <span class='webRupee'>Rs.</span>"
						+ binOffer.getMinRechargeAmount());
	    		return "jsonView";
	        }
	        CartBusinessDo cartBusinessDo = cartService.getCart(lid);
	        boolean isCartHasFreefund =  freefundService.containsFreeFund(cartBusinessDo);
	        if(isCartHasFreefund) {
	        	model.addAttribute(WebConstants.STATUS, "failure");
	        	model.addAttribute("message",  "Whoa, that's OD-ing! Since you already used up a FreeFund code, you can't avail this offer.");
	    		return "jsonView";
	        }
	        Double offerDiscountAmount = binOfferService.calculateDiscount(binOffer, cartBusinessDo);
			model.addAttribute(WebConstants.STATUS, "success");
			model.addAttribute("binOfferId", binOffer.getId());
			if (BinOfferService.OFFER_TYPE_INSTANT.equals(binOffer.getOfferType())) {
				model.addAttribute(
						"message",
							"Yay! Your "+binOffer.getBank()+" card is eligible for instant rebate. " +
						    "Check your rebate on the 3D secure page. Want more details? <a class='bin-offer-tnc' href='/app/binoffer/gettnc/"
										+ binOffer.getId()
										+ "' >Click here.</a>");
			}
			else {
				model.addAttribute(
						"message",
							"Yay! Your "+binOffer.getBank()+" card is eligible for cashback. " +
							"Check your Balance after the transaction is complete. " +
						    "Want more details? <a class='bin-offer-tnc' href='/app/binoffer/gettnc/" + binOffer.getId() + "' >Click here</a>.");
			}
			return "jsonView";
		}
		model.addAttribute(WebConstants.STATUS, "failure");
		model.addAttribute("message", "Damn! Your card is not eligible for an offer. Please enter other details and proceed to pay. P.S.: Sorry! ");
		return "jsonView";
	}

	@RequestMapping(value = "/admin/binoffer/home", method = RequestMethod.GET)
	public String adminHome(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        if (!hasAccess(request, AdminComponent.BIN_OFFERS)){
            return get403HtmlResponse(response);
        }
		List<BinOffer> binOffers = binOfferService.getAll();
		model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO_LIST, binOffers);

		Map<String, BinOffer> binOffersMap = new HashMap<String, BinOffer>();
		for(BinOffer binOffer : binOffers) {
			if(binOffersMap.get(binOffer.getOfferKey()) == null) {
				binOffersMap.put(binOffer.getOfferKey(), binOffer);
			}
		}
		model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO_MAP, binOffersMap);

		model.addAttribute("BANKS", BinOfferService.banks);
		model.addAttribute("offerTypes", BinOfferService.offerTypes);
		model.addAttribute("discountTypes", BinOfferService.discountTypes);

		Integer id = -1;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		if(id != -1) {
			BinOffer binOffer = binOfferService.getBinOffer(id);
			model.addAttribute(WebConstants.ATTR_BINOFFER_BUSSINESS_DO, binOffer);
			List<BinOffer> allOffersWithKey = binOfferService.getAllWithKey(binOffer.getOfferKey());
			StringBuffer sbuf = new StringBuffer("");
			for(BinOffer binOffer2 : allOffersWithKey) {
				sbuf.append(binOffer2.getBin()+",");
			}
			model.addAttribute("binText", sbuf.toString());
			model.addAttribute("validFrom", sdf.format(binOffer.getValidFrom().getTime()));
			model.addAttribute("validUpto", sdf.format(binOffer.getValidUpto().getTime()));
		}
		return ViewConstants.BINOFFER_ADMIN_HOME_VIEW;
	}

	@RequestMapping(value = "/admin/binoffer/updatestatus", method = RequestMethod.POST)
	public String updateStatus(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request) {
		Integer id = -1;
		int status = 0;
		try {
			id = Integer.parseInt(request.getParameter("id"));
			status =  Integer.parseInt(request.getParameter("status"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		if(id != -1) {
			BinOffer existingOffer = binOfferService.getBinOffer(id);
			if(existingOffer != null) {
				binOfferService.updateStatus(existingOffer.getOfferKey(), status);
				model.addAttribute("status", "success");
			} else {
				model.addAttribute("status", "error");
			}
		} else {
			model.addAttribute("status", "error");
		}

		return "jsonView";
	}

	@RequestMapping(value = "/admin/binoffer/create", method = RequestMethod.POST)
	public String create(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request) {
		String messages = "";

		BinOffer binOffer = new BinOffer();
		try {
			binOffer.setName(request.getParameter("name").trim());
			binOffer.setDescription(request.getParameter("description").trim());
			binOffer.setShortDescription(request.getParameter("short_description").trim());
			binOffer.setConditions(request.getParameter("conditions").trim());
			binOffer.setBank(request.getParameter("bank"));
			binOffer.setImageUrl(request.getParameter("image_url").trim());
			binOffer.setOfferKey(request.getParameter("offer_key").trim());
			binOffer.setOfferType(request.getParameter("offer_type"));
			binOffer.setDatamap(request.getParameter("datamap").trim());
			binOffer.setIsActive(Integer.parseInt(request.getParameter("is_active").trim()));
			binOffer.setCouponFactor(Integer.parseInt(request.getParameter("coupon_factor").trim()));
			binOffer.setDiscountType(request.getParameter("discount_type"));
			binOffer.setDiscount(Double.parseDouble(request.getParameter("discount").trim()));
			binOffer.setMinRechargeAmount(Double.parseDouble(request.getParameter("min_recharge_amount").trim()));

			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(sdf.parse(request.getParameter("valid_from")));
			binOffer.setValidFrom(cal1);

			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(sdf.parse(request.getParameter("valid_upto")));
			binOffer.setValidUpto(cal2);
			binOffer.setCreatedAt(cal2);

			String binText = request.getParameter("bin");
			createBins(binText, binOffer);
		}
		catch (Exception e) {
			messages = "Try again : " + e.getMessage();
			LOGGER.error("Create bin offer failed", e);
		}

		if (binOffer.getId() != null) {
			messages = "Binbased offer created successfully";
		}
		return String.format("redirect:%s?message=%s", ADMIN_HOME_LINK, messages);
	}

	@RequestMapping(value = "/admin/binoffer/update", method = RequestMethod.POST)
	public String update(@RequestParam Map<String, String> mapping, Model model,
			HttpServletRequest request) {
		String messages = "";
		Integer id = -1;
		try {
			id = Integer.parseInt(request.getParameter("id"));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		List<BinOffer>  existingBinOffers = null;
		if(id != -1) {
			BinOffer existingOffer = binOfferService.getBinOffer(id);
			if(existingOffer != null) {
				existingBinOffers =	binOfferService.getAllWithKey(existingOffer.getOfferKey());
			}
		}

        boolean newBinsCreated = false;
		for(BinOffer binOffer : existingBinOffers) {
			try {
				binOffer.setName(request.getParameter("name").trim());
				binOffer.setDescription(request.getParameter("description").trim());
				binOffer.setShortDescription(request.getParameter("short_description").trim());
				binOffer.setConditions(request.getParameter("conditions").trim());
				binOffer.setBank(request.getParameter("bank"));
				binOffer.setImageUrl(request.getParameter("image_url"));
				binOffer.setOfferKey(request.getParameter("offer_key").trim());
				binOffer.setOfferType(request.getParameter("offer_type"));
				binOffer.setDatamap(request.getParameter("datamap").trim());
				binOffer.setIsActive(Integer.parseInt(request.getParameter("is_active").trim()));
				binOffer.setCouponFactor(Integer.parseInt(request.getParameter("coupon_factor").trim()));
				binOffer.setDiscountType(request.getParameter("discount_type"));
				binOffer.setDiscount(Double.parseDouble(request.getParameter("discount").trim()));
				binOffer.setMinRechargeAmount(Double.parseDouble(request.getParameter("min_recharge_amount").trim()));

				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(sdf.parse(request.getParameter("valid_from")));
				binOffer.setValidFrom(cal1);

				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(sdf.parse(request.getParameter("valid_upto")));
				binOffer.setValidUpto(cal2);
				binOffer.setCreatedAt(cal2);

				binOfferService.update(binOffer);
                if (!newBinsCreated) {
                    newBinsCreated = true;
                    String binText = request.getParameter("add_bins");
                    createBins(binText, binOffer);
                }
			}
			catch (Exception e) {
				messages = "Try again : " + e.getMessage();
				LOGGER.error("Create bin offer failed", e);
			}
		}
		messages = "Binbased offer updated successfully";
		return String.format("redirect:%s?message=%s", ADMIN_HOME_LINK, messages);
	}

    private void createBins(String binText, BinOffer binOffer) {
        String bins[] = binText.split("[,]");
        for(String bin : bins) {
            bin = bin.trim();
            if(!bin.isEmpty()) {
                binOffer.setBin(bin);
                binOffer.setId(0);
                binOfferService.create(binOffer);
            }
        }

		if (bins != null && bins.length > 0) {
			List<String> binRangeList = binOfferCache.get(binOffer.getOfferKey());
			if (FCUtil.isEmpty(binRangeList)) {
				binRangeList = new ArrayList<>();
			}
			List<String> binList = Arrays.asList(bins);
			binList.remove("");
			binRangeList.addAll(binList);
			binOfferCache.setBinOfferDataInCache(binOffer.getOfferKey(), binRangeList, 365);
		}
    }

	private boolean checkCouponDataSanity(String cardNumber) {
		if (cardNumber == null || cardNumber.isEmpty()) {
			return false;
		}

		return true;
	}
}