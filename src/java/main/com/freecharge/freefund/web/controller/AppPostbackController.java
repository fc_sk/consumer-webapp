package com.freecharge.freefund.web.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.encoding.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.constants.PostbackConstants;
import com.freecharge.freefund.dos.entity.ApsalarEventData;
import com.freecharge.freefund.services.ApsalarEventDataService;
import com.freecharge.freefund.services.PhonebookDumpService;
import com.freecharge.util.TimeUtil;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;

/**
 * This class has apis related to postbacks
 **/
@Controller
public class AppPostbackController {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private ApsalarEventDataService apsalarEventDataService;

    @Autowired
    private PhonebookDumpService phonebookDumpService;

    @NoSessionWrite
    @Csrf (exclude = true)
    @NoLogin
    @RequestMapping(value = "/apsalar/postback", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody Map<String, Object> listenToEvent(final HttpServletRequest request, final HttpServletResponse response,
                                                           @RequestParam Map<String, String> map) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        if (null == map.get(PostbackConstants.DATETIME)) {
            logger.error("Required Parameter " + PostbackConstants.DATETIME + " to save apsalar event data");
            responseMap.put("error", "Required Parameter " + PostbackConstants.DATETIME + " to save apsalar event data");
            responseMap.put(FCConstants.STATUS, "failure");
        } else {
            String time = map.get(PostbackConstants.DATETIME);
            String timeInIST = TimeUtil.convertTimeZoneWithPattern("UTC", "Asia/Kolkata", time,
                                                                   "yyyy-MM-dd_HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            if (null == timeInIST) {
                logger.error("Error converting UTC date " +  time + " to IST in yyyy-MM-dd HH:mm:ss pattern");
                responseMap.put("error", "Error converting UTC date " +  time + " to IST in yyyy-MM-dd HH:mm:ss pattern");
                responseMap.put(FCConstants.STATUS, "failure");
                return responseMap;
            } else {
                map.put(PostbackConstants.DATETIME, timeInIST);
                ApsalarEventData eventData = new ApsalarEventData(map);
                try {
                    int status = apsalarEventDataService.insert(eventData);
                    if (status <= 0) {
                        logger.error("Couldnt insert data into apsalar_event_data table for campaign "
                                + eventData.getCampaignName() + " deviceId " + eventData.getDeviceId());
                    } else {
                        logger.info("Sucessfully captured Apsalar event data for campaign : "
                                + eventData.getCampaignName() + ", deviceId : " + eventData.getDeviceId()
                                + ", app : " + eventData.getAppName() + ", partner : " + eventData.getPartner()
                                + ", eventName " + eventData.getEventName());
                        responseMap.put(FCConstants.STATUS, "success");
                    }
                } catch (Exception e) {
                    logger.error("Error inserting data " + eventData + " into apsalar_event_data table", e);
                    responseMap.put("error", "Error capturing data for campaign : " + eventData.getCampaignName() +
                                    ", partner : " +  eventData.getPartner());
                    responseMap.put(FCConstants.STATUS, "failure");
                }
            }
        }
        return responseMap;
    }
    
    @NoSessionWrite
    @Csrf (exclude = true)
    @NoLogin
    @RequestMapping(value = "/apppost/pb", method = {RequestMethod.POST })
    public @ResponseBody Map<String, Object> save(final HttpServletRequest request, final HttpServletResponse response,
                                                           @RequestParam Map<String, String> map, @RequestBody String body) throws IOException {
    	try {
            //String decodedBody = decodeBase64(body);
    		//phonebookDumpService.dumpPhonebook(decodedBody);
            phonebookDumpService.dumpZIPPhonebook(body);
    	}catch (Exception e) {
            logger.info("Fail to dump request body: ["+body + "]  , Error message: " +e.getMessage());
		}
    	return doDummy(request, body);
    }

    private String decodeBase64(String body) throws IOException {
        byte[] decodedZip = Base64.decode(body);
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(decodedZip));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
            outStr += line;
        }
        return outStr;
    }


    @NoSessionWrite
    @Csrf (exclude = true)
    @NoLogin
    @RequestMapping(value = "/apppost/gn", method = {RequestMethod.POST })
    public @ResponseBody Map<String, Object> saveGoogleNowToken(final HttpServletRequest request, final HttpServletResponse response,
                                                           @RequestParam Map<String, String> map, @RequestBody String body) {
        return doDummy(request, body);
    }

	private Map<String, Object> doDummy(final HttpServletRequest request, String body) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
        responseMap.put("result", "success");
      //logRequest(request); 
     //   logger.info(body);
        return responseMap;
	}
    
//	private void logRequest(HttpServletRequest request) {
//		logger.info(request.getParameterMap());
//	}

	@NoLogin
    @Csrf(exclude=true)
	@NoSessionWrite
    @RequestMapping(value = "/apppost/atrfos", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> appTrackFirstOpenNew(HttpServletRequest request, HttpServletResponse response, ModelMap model, @RequestBody String body){
		return doDummy(request, body);
	}
	
	@NoLogin
    @Csrf(exclude=true)
	@NoSessionWrite
    @RequestMapping(value = "/apppost/aattrs", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> appTrackAttributionNew(HttpServletRequest request, HttpServletResponse response, ModelMap model, @RequestBody String body){
		return doDummy(request, body);
	}
	
}
