package com.freecharge.freefund.web.controller;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.jdbc.FreefundReadDAO;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.campaign.web.CampaignResult;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.CaptchaService;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.SimpleReturn;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.dos.CampaignPromocodeValidateResponse;
import com.freecharge.freefund.dos.business.FreefundRequestBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.dos.web.FreefundRequestWebDO;
import com.freecharge.freefund.fraud.FreefundFraud;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.freefund.services.FreefundUsageCounterService;
import com.freecharge.freefund.services.IPService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.promo.rest.error.ErrorCode.FreefundErrorCode;
import com.freecharge.promo.util.RequestIdGenerator;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.UserDataPointSNSService;
import com.freecharge.tracker.TrackerContextDictionary;
import com.freecharge.util.CookieUtil;
import com.freecharge.uuip.UserDataPointType;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.exception.IMSClientException;
import com.freecharge.web.util.WebConstants;
import com.google.common.primitives.Ints;
import com.google.gson.Gson;

/**
 * Controller for processing Freefunds
 */
@Controller
public class FreeFundController {

    public static final String          FREEFUND_APPLY_LINK           = "/freefund/apply.htm";
    public static final String          PROMOCODE_APPLY_LINK          = "/promocode/apply.htm";
    public static final String          FREEFUND_RESET_LINK           = "/promotion/reset.htm";
    public static final String          FREEFUND_REDEEM_LINK          = "/freefund/redeem.htm";
    public static final String          METRIC_CAPTCHA_SERVICE        = "captcha";
    public static final String          METRIC_CAPTCHA_ENTERED        = "entered";
    public static final String          METRIC_CAPTCHA_SUCCESS        = "success";
    public static final String          METRIC_CAPTCHA_FAIL           = "fail";
    public static final String          METRIC_FREEFUND_APPLY_FAIL    = "ApplyFailure";
    public static final String          METRIC_FREEFUND_REMOVE_FAIL   = "RemoveFailure";
    public static final String          METRIC_FREEFUND_VALIDATE      = "Validate";
    public static final String          METRIC_FREEFUND_VALIDATE_FAIL = "ValidateFailure";
    public static final String          PREFIX                        = "REF";
    public static final String          NUMBER_OF_CODES               = "1";
    public static final String          LENGTH_OF_CODES               = "10";
    public static final String          FREEFUND_APPLY_FAIL           = "applyFailed";
    public static final String          METRIC_FREEFUND_ADDITIONAL_REWARD_VALIDATE_FAIL = "AdditionalRewardValidateFailure";

    public static final String          TRACKER_IP_LOG_PREFIX         = "(Tracker ip) - ";

    private static final String         APPLY_TRACKING_COOKIE_NAME    = "fc.ff";

    @Autowired
    private FreefundService             freefundService;

    @Autowired
    private CartService                 cartService;

    @Autowired
    private UserServiceProxy                 userServiceProxy;

    @Autowired
    private AppConfigService            appConfigService;

    @Autowired
    private CaptchaService              captchaService;

    @Autowired
    private MetricsClient             metricsClient;

    private final Logger                logger                        = LoggingFactory.getLogger(getClass());

    @Autowired
    private FreefundUsageCounterService freefundUsageCounterService;

    @Autowired
    private IPService                   ipService;

    @Autowired
    private FCProperties                fcProperties;

    @Autowired
    private UserDataPointSNSService     userDataPointSNSService;

    @Autowired
    FreefundReadDAO freefundReadDAO;

    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private RequestIdGenerator requestIdGenerator;

    public String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    @RequestMapping(value = "/promocode/new/apply", method = RequestMethod.POST, produces = "application/json",
            headers = "Accept=application/json")
    public String validateAndApplyPromocodeforNewVersions(final  HttpServletRequest request, final HttpServletResponse response, final Model model,
            @RequestBody String requestBody) {
    
    	long startTime = System.currentTimeMillis();
        FreefundRequestBusinessDO freeFundRequestBusinessDO = null;
        FreefundRequestWebDO freeFundRequestWebDO = null;
        boolean isFreefundCode = false;
        try {        	
        	
            FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();

            if (FCUtil.isEmpty(requestBody)) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.EMPTY_REQUEST_DATA.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.EMPTY_REQUEST_DATA.toString());
                return "jsonView";
            }

            freeFundRequestWebDO = new Gson().fromJson(requestBody, FreefundRequestWebDO.class);

            if (!appConfigService.isPromocodeEnabled()) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.PROMOCODE_FEATURE_DISABLED.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_PROMOCODE_DOWN));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.PROMOCODE_FEATURE_DISABLED.toString());
                return "jsonView";
            }

            com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getLoggedInUser();
            if (user == null) {
                logger.info("User not found in session data for applied promocode "
                        + freeFundRequestWebDO.getCouponCode());
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.DEFAULT_ERROR.toString());
                return "jsonView";
            }

            String txnChannel = freeFundRequestWebDO.getTxnChannel();
            JSONObject hashCheckObj = appConfigService.getCustomProp(FCConstants.PROMOCODE_HASH_CHECK);
            Object isCheckEnabled = hashCheckObj.get(FCConstants.IS_CHECK_ENABLED);
           
            if (isCheckEnabled != null && !FCUtil.isEmpty(String.valueOf(isCheckEnabled))
                    && "true".equalsIgnoreCase(String.valueOf(isCheckEnabled))) {            	
            	
                String channels = String.valueOf(hashCheckObj.get(FCConstants.ENABLED_CHANNELS));               
                if (!FCUtil.isEmpty(channels) && channels.contains(txnChannel)) {
                    String requestBodyHash = request.getHeader("ftoken");                  
                    
                    if (FCUtil.isEmpty(requestBodyHash) || FCUtil.isEmpty(requestBody)) {
                        model.addAttribute("status", "failure");
                        model.addAttribute("errorCode", ErrorCode.MAX_REDEMPTION_CONDITION_FAIL.getErrorNumber());
                        model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                                ErrorCode.DEFAULT_ERROR.toString());
                        return "jsonView";
                    } else {
                        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                        String onecheckId = null;
                        try {
                            logger.info("Fetching migration status for user " + user.getUserId());
                            onecheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());                   
                        } catch (Exception e) {                        
                            logger.error("error fetching wallet migration status for email " + user.getEmail(), e);
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                            model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                            return "jsonView";
                        }
                        String hashable = requestBody + onecheckId;
                        messageDigest.update(hashable.getBytes());
                        byte[] digest = messageDigest.digest();
                        String hashed = DatatypeConverter.printHexBinary(digest);
                        if (FCUtil.isEmpty(hashed) ||  !hashed.equalsIgnoreCase(requestBodyHash)) {
                            logger.info("Hash mismatch " + freeFundRequestWebDO.getCouponCode() + " Got hash: " +
                                    requestBodyHash + " Calculated: " + hashed + " request body " + requestBody
                                    + " and wallet id " + onecheckId);
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.MAX_REDEMPTION_CONDITION_FAIL.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                                    ErrorCode.DEFAULT_ERROR.toString());
                            return "jsonView";
                        }

                    }
                } else {
                    logger.info("Code " + freeFundRequestWebDO.getCouponCode() + "applied from channel " + txnChannel
                            + " not applicable for hash fraud check.");
                }
            }

            String imeiNumber = freeFundRequestWebDO.getImeiNumber();
            String advertisementId = freeFundRequestWebDO.getAdvertisementId();

            String deviceUniqueId = freeFundRequestWebDO.getDeviceUniqueId();

            String imsi = freeFundRequestWebDO.getImsi();
            Boolean isEmulator = "TRUE".equalsIgnoreCase(freeFundRequestWebDO.getIsEmulator());

            if (isEmulator) {
                logger.info("isEmulator found to be true for " + user.getEmail() + " while applying code " +
                        freeFundRequestWebDO.getCouponCode());
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.MAX_REDEMPTION_CONDITION_FAIL.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.DEFAULT_ERROR.toString());
                return "jsonView";
            }
            String appVersion = null;
            if(txnChannel!=null && txnChannel.equals(String.valueOf(FCConstants.CHANNEL_ID_IOS_APP))) {
            	appVersion = request.getParameter("fcversion");
            } else {
            	appVersion = freeFundRequestWebDO.getAppFcVersion();
            }
            logger.debug("channel id: " + txnChannel + " App version: " + appVersion);
            if (!FCUtil.isEmpty(txnChannel) && "APP".equals(FCUtil.getChannelTypeAsDescriptor(txnChannel))) {
                int minVersion = getMinVersion();
                logger.debug("Minimum version set " + minVersion + " for channel " + txnChannel + " ,received app "
                        + "version " + appVersion + " for " + freeFundRequestWebDO.getCouponCode() + " applied by"
                        + " user " + user.getEmail());
                if (!FCUtil.isEmpty(appVersion)) {
                    int reqAppVersion = Integer.parseInt(appVersion);
                    // Temporary hack - Allow user to move ahead if version is
                    // comes as -1.
                    // Bug to be fixed by android app team.
                    if (reqAppVersion != -1 && reqAppVersion < minVersion) {
                        return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                    }
                } else {
                    return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                }
            }

            if (!checkCouponDataSanity(freeFundRequestWebDO.getCouponCode())) {
                logger.info("coupon validation failure");
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.EMPTY_CODE_ERROR.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.EMPTY_CODE_ERROR.toString());
                return "jsonView";
            }

            logger.info("IMEI : " + imeiNumber + " AdvId " + advertisementId + "deviceUniqueId" + deviceUniqueId
                    + " IMSI : " + imsi + " IsEmulator : " + isEmulator + " for "
                    + freeFundRequestWebDO.getCouponCode() + " and user id " + user.getUserId());

            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            fs.setIMEINumber(imeiNumber);
            fs.setAdvertisementId(advertisementId);
            fs.setDeviceUniqueId(deviceUniqueId);
            fs.setImsi(imsi);
            fs.setIsEmulator(isEmulator);

            freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
            freeFundRequestBusinessDO.setEmail(user.getEmail());
            freeFundRequestBusinessDO.setCouponCode(freeFundRequestWebDO.getCouponCode().trim());

            FreefundCoupon freefundCoupon = freefundService
                    .getFreefundCoupon(freeFundRequestBusinessDO.getCouponCode());

            // Need to get coupon before captcha check so that we can update
            // captcha fraudcount
            if (appConfigService.isRecaptchaEnabled()) {
                boolean shouldCheckCaptcha = captchaService.shouldCheckCaptchaForCampaign(fcSession);
                SimpleReturn simpleReturn = captchaService.isCaptchValid(freeFundRequestWebDO.getCaptcha());
                recordCaptcha(shouldCheckCaptcha, simpleReturn.isValid());
                if (shouldCheckCaptcha && !simpleReturn.isValid()) {
                    if (freefundCoupon != null) {
                        logger.info(freefundCoupon.getFreefundCode() + " [ " + freefundCoupon.getFreefundClassId()
                                + " ] " + " reCaptchaService validation failure " + " Reason : "
                                + simpleReturn.getObjMap());
                    } else {
                        logger.info(" reCaptchaService validation failure. Reason : " + simpleReturn.getObjMap());
                    }
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.CAPTCHA_VALIDATION_FAIL.getErrorNumber());
                    model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INCORRECT_CAPTCHA));
                    model.addAttribute(WebConstants.FORCE_CAPTCHA, true);
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.CAPTCHA_VALIDATION_FAIL.toString());
                    return "jsonView";
                }
                /*
                 * Reset captcha only if its a valid code and captcha validated
                 * first time
                 */
                boolean resetCaptcha = (freefundCoupon != null) && simpleReturn.isValid();
                model.addAttribute(WebConstants.FORCE_CAPTCHA, captchaService.forceCaptchaForCampaign(fcSession, resetCaptcha));
                captchaService.markTokenUsed(fcSession, true);
            }
            // Captcha check should happend before coupon validity check
            if (freefundCoupon == null) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.INVALID_CODE.toString());
                return "jsonView";
            } else {
                model.addAttribute("codeType", freefundCoupon.getPromoEntity());
            }


            JSONObject versionObject = appConfigService.getCustomProp(FCConstants.REFERRAL_CAMPAIGN_KEY);
            if (FCUtil.isNotEmpty(versionObject)) {
                String referralCampaignIdString = String.valueOf(versionObject.get(FCConstants.CAMPAIGN_ID));
                String isReferralEnabled = String.valueOf(versionObject.get(FCConstants.IS_REFERRAL_ENABLED));
                long referralCampId = Long.parseLong(referralCampaignIdString);
                if ("false".equals(isReferralEnabled) && freefundCoupon.getFreefundClassId() == referralCampId) {
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", FreefundErrorCode.REFERRAL_SHUT_DOWN.getErrorNumber());
                    model.addAttribute("errors",
                            "The referral program is under maintenance and being revamped...we will be back shortly. " +
                                    "We apologise for the inconvenience. ");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            FreefundErrorCode.REFERRAL_SHUT_DOWN.toString());
                    return "jsonView";
                }
            }

            isFreefundCode = "redeem-to-fcbalance".equals(freefundCoupon.getPromoEntity());
            CartBusinessDo cartBusinessDo = null;
            ProductName productName = null;
            if (!isFreefundCode) {
                cartBusinessDo = cartService.getCart(freeFundRequestWebDO.getLookupID());
                if (cartBusinessDo == null) {
                    // transactional discount avail, but no cart found. User
                    // tried redeeming promocode from balance page
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
                    model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                    logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                            + " promocode apply from credits page ");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.INVALID_CODE.toString());
                    return "jsonView";
                } else {
                    try {
                        productName = cartService.getPrimaryProduct(cartBusinessDo);
                    } catch (Exception e) {
                        logger.error("error getting productName for email: " + user.getEmail(), e);
                    }
                }
            }
            // If this code us already redeemed, no need to do any other checks.
            // this will reduce db load
            if ("REDEEMED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_REDEEMED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                }
                return "jsonView";
            }

            if ("BLOCKED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                }
                return "jsonView";
            }

            SimpleReturn simpleReturn = freefundService.validateClassOnApply(cartBusinessDo, freefundCoupon);
            if (!simpleReturn.isValid()) {
                logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                        + " class validtion failed." + simpleReturn.getObjMap().get("message"));
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", simpleReturn.getErrorCode().getErrorNumber());
                model.addAttribute("errors", simpleReturn.getObjMap().get(SimpleReturn.MESSAGE_KEY));
                if (isFreefundCode) {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, METRIC_FREEFUND_VALIDATE_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + simpleReturn.getErrorCode().toString());
                } else {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + simpleReturn.getErrorCode().toString());
                }
                return "jsonView";
            }
            boolean isNewCampaign = false;
            if (freefundCoupon.getFreefundClassId() != null && freefundCoupon.getFreefundClassId() != 0) {
                FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
                String ip = MobileUtil.getRealClientIpAddress(request);
                if (freefundClass != null && !freefundClass.getIsNewCampaign()) {

                    String applyCookie = getApplyCookie(request, response, freefundClass.getId(), user.getEmail());
                    String number = cartService.getRechargedNumber(freeFundRequestWebDO.getLookupID());

                    String fingerPrint = null;

                    if (fcSession.getSessionData().get(WebConstants.SESSION_FINGER_PRINT) != null) {
                        fingerPrint = String.valueOf(fcSession.getSessionData().get(WebConstants.SESSION_FINGER_PRINT));
                    }
                    logger.debug(TRACKER_IP_LOG_PREFIX + "Email: " + user.getEmail() + " FF ID:"
                            + freefundClass.getId() + " FF Code:" + freefundCoupon.getFreefundCode() + " Channel:"
                            + txnChannel + " Cookie :" + applyCookie + " ip :" + ip + " fingerPrint: " + fingerPrint
                            + " tracker visit id: " + TrackerContextDictionary.get().getVisitId());
                } else {
                    isNewCampaign = true;
                    HashMap<String, Object> transactionInfo = new HashMap<>();
                    String clientId = null;
                    String visitId = null;
                    Cookie[] cookies = request.getCookies();
                    if (cookies != null && cookies.length > 0) {
                        for (Cookie cookie : cookies) {
                            if ("fc.tc3".equalsIgnoreCase(cookie.getName())) {
                                String clientCookie = cookie.getValue();
                                if (!FCUtil.isEmpty(clientCookie)) {
                                    String[] clientCookieSplit = clientCookie.split(";");
                                    if (clientCookieSplit.length > 0) {
                                        clientId = clientCookieSplit[0];
                                    }
                                }
                            } else if ("fc.tv".equalsIgnoreCase(cookie.getName())) {
                                visitId = cookie.getValue();
                            }
                        }
                    }

                    transactionInfo.put(InTxnDataConstants.TRACKER_VISIT_ID, visitId);
                    transactionInfo.put(InTxnDataConstants.TRACKER_CLIENT_ID, clientId);
                    transactionInfo.put(InTxnDataConstants.PROMOCODE, freefundCoupon.getFreefundCode());
                    transactionInfo.put(InTxnDataConstants.FF_CLASS_ID, freefundCoupon.getFreefundClassId().intValue());
                    Integer userId=user.getUserId();
                    transactionInfo.put(InTxnDataConstants.USER_ID, userId );
                    //new user condition
                    List<String> orders= userTransactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 1);
                    if (null == orders || orders.isEmpty()) {
                        transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
                    } else {
                        transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
                    }

                    transactionInfo.put(InTxnDataConstants.USER_EMAIL, user.getEmail());

                    MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(user.getEmail(), null);
                    String oneCheckWalletId = null;
                    try {
                        logger.info("Fetching oneCheckWalletId for user " + userId);
                        oneCheckWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
                    } catch (Exception e) {
                        logger.error("error fetching oneCheckWalletId for email " + user.getEmail(), e);
                        model.addAttribute("status", "failure");
                        model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                        model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                        return "jsonView";
                    }
                    if (null != migrationStatus) {
                        transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
                    }
                    if (null != oneCheckWalletId) {
                        transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
                    }

                    if (null != txnChannel) {
                        transactionInfo.put(InTxnDataConstants.CHANNEL, Ints.tryParse(txnChannel));
                    }
                    transactionInfo.put(InTxnDataConstants.IP_ADDRESS, ip);
                    transactionInfo.put(InTxnDataConstants.ADVERTISEMENT_ID, advertisementId);
                    if (null != appVersion) {
                        transactionInfo.put(InTxnDataConstants.APP_VERSION, Ints.tryParse(appVersion));
                    }
                    transactionInfo.put(InTxnDataConstants.DEVICE_UNIQUE_ID, deviceUniqueId);
                    transactionInfo.put(InTxnDataConstants.IMEI_NUMBER, imeiNumber);
                    transactionInfo.put(InTxnDataConstants.IMSI, imsi);
                    transactionInfo.put(InTxnDataConstants.IS_EMULATOR, isEmulator);

                    if(isFreefundCode) {
                        transactionInfo.put(InTxnDataConstants.ORDER_ID, "OD" + requestIdGenerator.nextRequestId() + "_" +
                                freefundCoupon.getFreefundCode());
                    }

                    String uniqueId = FCUtil.isNotEmpty(freeFundRequestWebDO.getLookupID()) ?
                            freeFundRequestWebDO.getLookupID() : "ff_" + UUID.randomUUID() + "_" +
                            freefundCoupon.getFreefundCode() ;

                    try {
                        logger.info("LookUpId for campaign_service: " + freeFundRequestWebDO.getLookupID());
                        if (!isFreefundCode) {
                            campaignServiceClient.saveInTxnData1(uniqueId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
                        } else {
                            campaignServiceClient.saveInTxnData1(uniqueId, FCConstants.FREEFUND_MERCHANT_ID, transactionInfo);
                        }
                    }catch (Exception e){
                        logger.error("error saving intxndata in freefund service: ", e);
                    }


                    CampaignResult campaignResult = null;
                    if (!isFreefundCode) {
                        campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                                uniqueId, freefundClass.getId(),
                                "APPLY_PROMOCODE", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
                    } else {
                        freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
                        freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
                        freeFundRequestBusinessDO.setLookupID(uniqueId);
                        boolean isBlockSuccess = freefundService.blockCouponCode(freeFundRequestBusinessDO);
                        if (!isBlockSuccess){
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                            if (isFreefundCode) {
                                metricsClient.recordEvent(
                                        FreefundService.METRIC_SERVICE_FREEFUND,
                                        METRIC_FREEFUND_APPLY_FAIL,
                                        "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                                + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                            } else {
                                metricsClient.recordEvent(
                                        FreefundService.METRIC_SERVICE_PROMOCODE,
                                        METRIC_FREEFUND_APPLY_FAIL,
                                        "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                                + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                            }
                            return "jsonView";
                        }
                        try {
                            campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                                    uniqueId, freefundClass.getId(),
                                    "TXN_SUCCESS", FCConstants.FREEFUND_MERCHANT_ID);
                        } catch (Exception e) {
                            logger.error("Error calling campaign service to process freefund "
                                    + freefundCoupon.getFreefundCode(), e);
                            freefundService.unblockPromocode(freefundCoupon, userId, uniqueId);
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
                            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                                    ErrorCode.UNKNOWN_ERROR.toString());
                            return "jsonView";
                        }
                    }

                    if (!campaignResult.getStatus()) {
                        if (isFreefundCode) {
                            freefundService.unblockPromocode(freefundCoupon, userId, uniqueId);
                        }
                        model.addAttribute("status", "failure");
                        // Intentionally confusing message to user
                        model.addAttribute("errorCode", ErrorCode.APPLY_FRAUD_DETECTED.getErrorNumber());
                        model.addAttribute("errors", campaignResult.getConditionResult().getFailedRule().getDetailedFailureMessage());
                        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId()
                                + "] " + campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                        logger.info("Time duration for apply code = " + freefundCoupon.getFreefundCode() + ", by user email = "+ user.getEmail() + " is = " + (System.currentTimeMillis()-startTime));
                        return "jsonView";
                    }
                }
            }

            freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
            freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
            boolean validCoupon = (isNewCampaign && isFreefundCode)
                    ?true:freefundService.validateAndSetCouponId(freeFundRequestBusinessDO);
            freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
            model.addAttribute("freefundCoupon", freefundCoupon);

            // Publishing user data points for Mystique (Unique User Identification Service)
            Date date = new Date();
            userDataPointSNSService.publishUserDataPoint(user.getUserId(), imeiNumber, UserDataPointType.IMEI, date);
            userDataPointSNSService.publishUserDataPoint(user.getUserId(), deviceUniqueId,
                    UserDataPointType.WINDOWS_UNIQUE_DEVICE_ID, date);

            Integer[] paymentOptions = null;
            JSONObject paymentOptionsObject = appConfigService.getCustomProp(FCConstants.PAYMENT_OPTIONS_KEY);
            Object allEnabledStr = paymentOptionsObject.get(FCConstants.ALL_ENABLED);
            if (allEnabledStr != null && !FCUtil.isEmpty(String.valueOf(allEnabledStr))) {
                String allEnabled = String.valueOf(allEnabledStr);
                if(PaymentConstants.ALL.equalsIgnoreCase(allEnabled)) {
                    paymentOptions = new Integer[]{0};
                } else {
                    List<PaymentTypeMaster> paymentTypeMasters = paymentTransactionService.getPaymentOptions();
                    List<Integer> idList = new ArrayList<>();
                    for (PaymentTypeMaster paymentOption : paymentTypeMasters) {
                        idList.add(paymentOption.getPaymentTypeMasterId());
                    }

                    if (!FCUtil.isEmpty(idList)) {
                        paymentOptions = idList.toArray(new Integer[idList.size()]);
                    }
                }
            }

            if (isFreefundCode) {
                return processFreefundRedeem(validCoupon, freeFundRequestBusinessDO, model, user, paymentOptions,
                        isNewCampaign);
            } else {

                freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
                freeFundRequestBusinessDO.setLookupID(freeFundRequestWebDO.getLookupID());
                for (Entry<Integer, String> productMap : FCConstants.productMasterMap.entrySet()) {
                    if (productMap.getValue().equals(freeFundRequestWebDO.getProductType())) {
                        freeFundRequestBusinessDO.setProductType(productMap.getKey());
                    }
                }

                for (CartItemsBusinessDO cartItemsDO : cartBusinessDo.getCartItems()) {
                    if (FCConstants.productMasterMap.containsKey(cartItemsDO.getProductMasterId())) {
                        freeFundRequestBusinessDO.setServiceNumber(cartItemsDO.getDisplayLabel());
                    }
                }
                freeFundRequestBusinessDO.setServiceNumber(cartService.getRechargedNumber(freeFundRequestWebDO
                        .getLookupID()));
                /*
                 * ==========================GENERIC
                 * PROMOCODE================================== This is the right
                 * place to handle generic promocode logic. Do not move this.
                 */
                boolean isGenericCashbackPromocode = FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon
                        .getPromoEntity());
                boolean isGenericDiscountPromocode = FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(freefundCoupon
                        .getPromoEntity());

                logger.info("Time duration for apply code = " + freefundCoupon.getFreefundCode() + ", by user email = "+ user.getEmail() + " is = " + (System.currentTimeMillis()-startTime));
                
                if (isGenericCashbackPromocode) {
                    return processGenericCashbackPromocode(validCoupon, freeFundRequestBusinessDO,
                            freeFundRequestWebDO, model, user, paymentOptions);
                } else if (isGenericDiscountPromocode) {
                    return processGenericDiscountPromocode(validCoupon, freeFundRequestBusinessDO,
                            freeFundRequestWebDO, model, user, paymentOptions);
                }
                /*
                 * =========================GENERIC
                 * PROMOCODE===================================
                 */

                return processPromocodeAvail(validCoupon, freeFundRequestBusinessDO, freeFundRequestWebDO, model, user,
                        paymentOptions);
            }

        } catch (Exception e) {
            logger.error("Exception in promocode/freefund code path : ", e);
            model.addAttribute("status", "failure");
            model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                    ErrorCode.UNKNOWN_ERROR.toString());
            return "jsonView";
        } finally {
            if (model != null && model.asMap() != null
                    && "failure".equalsIgnoreCase(String.valueOf(model.asMap().get("status")))
                    && freeFundRequestWebDO != null) {
                freefundService.removeFreefundFromCart(freeFundRequestWebDO.getLookupID());
            }
        }

    }

    @RequestMapping(value = "/promocode/apply", method = RequestMethod.POST, produces = "application/json")
    public String validateAndApplyPromocode(
            @ModelAttribute(value = "freefundRequestWebDO") final FreefundRequestWebDO freeFundRequestWebDO,
            final HttpServletRequest request, final HttpServletResponse response, final Model model) {
    	long startTime = System.currentTimeMillis();
        FreefundRequestBusinessDO freeFundRequestBusinessDO = null;
        boolean isFreefundCode = false;
        try {

            FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();

            if (!appConfigService.isPromocodeEnabled()) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.PROMOCODE_FEATURE_DISABLED.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_PROMOCODE_DOWN));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.PROMOCODE_FEATURE_DISABLED.toString());
                return "jsonView";
            }

            com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getLoggedInUser();
            if (user == null) {
                logger.info("User not found in session data for applied promocode "
                        + freeFundRequestWebDO.getCouponCode());
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.DEFAULT_ERROR.toString());
                return "jsonView";
            }

            String txnChannel = request.getParameter(FCConstants.FC_CHANNEL);

            JSONObject hashCheckObj = appConfigService.getCustomProp(FCConstants.PROMOCODE_HASH_CHECK);
            Object isCheckEnabled = hashCheckObj.get(FCConstants.IS_CHECK_ENABLED);
            if (isCheckEnabled != null && !FCUtil.isEmpty(String.valueOf(isCheckEnabled))
                    && "true".equalsIgnoreCase(String.valueOf(isCheckEnabled))) {
                String channels = String.valueOf(hashCheckObj.get(FCConstants.ENABLED_CHANNELS));

                if (!FCUtil.isEmpty(channels)) {
                    for (String channel : channels.split(",")) {
                        if (channel.equalsIgnoreCase(txnChannel)) {
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.CHANNEL_CONDITION_FAIL.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_CHANNEL_BLOCKED_FOR_OLD_API));
                            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                                    ErrorCode.DEFAULT_ERROR.toString());
                            return "jsonView";
                        }
                    }
                }
            }

            String imeiNumber = request.getParameter(FCConstants.IMEI_NUMBER);
            String advertisementId = request.getParameter(FCConstants.ADVERTISEMENT_ID);

            String deviceUniqueId = request.getParameter(FCConstants.DEVICE_UNIQUE_ID);

            String imsi = request.getParameter(FCConstants.IMSI);
            Boolean isEmulator = "TRUE".equalsIgnoreCase(request.getParameter(FCConstants.IS_EMULATOR));

            if (isEmulator) {
                logger.info("isEmulator found to be true for " + user.getEmail() + " while applying code " +
                        freeFundRequestWebDO.getCouponCode());
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.MAX_REDEMPTION_CONDITION_FAIL.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.DEFAULT_ERROR.toString());
                return "jsonView";
            }

            String appVersion = request.getParameter(FCConstants.FC_ANDROID_APP_VERSION);
            JSONObject appVersionObj = appConfigService.getCustomProp(FCConstants.CUT_OFF_VERSION_CHECK);
            Object cutOffCheckEnabled = appVersionObj.get(FCConstants.CUT_OFF_CHECK_ENABLED);
            if (cutOffCheckEnabled != null && !FCUtil.isEmpty(String.valueOf(cutOffCheckEnabled)) &&
                    "true".equalsIgnoreCase(String.valueOf(cutOffCheckEnabled))) {
                Integer cutOffVersion = Integer.parseInt(String.valueOf(appVersionObj.get(FCConstants.CUT_OFF_VERSION)));
                if (Integer.parseInt(appVersion) > cutOffVersion) {
                    logger.info("Got request from a newer version for old api " + appVersion + " for "
                            + freeFundRequestWebDO.getCouponCode());
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.MAX_REDEMPTION_CONDITION_FAIL.getErrorNumber());
                    model.addAttribute("errors", fcProperties.getProperty(FCConstants.MSG_MAX_REDEMPTION_REACHED));
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.DEFAULT_ERROR.toString());
                    return "jsonView";
                }
            }

            logger.debug("channel id: " + txnChannel + " App version: " + appVersion);
            if (!FCUtil.isEmpty(txnChannel) && "APP".equals(FCUtil.getChannelTypeAsDescriptor(txnChannel))) {
                int minVersion = getMinVersion();
                logger.debug("Minimum version set " + minVersion + " for channel " + txnChannel + " ,received app "
                        + "version " + appVersion + " for " + freeFundRequestWebDO.getCouponCode() + " applied by"
                        + " user " + user.getEmail());
                if (!FCUtil.isEmpty(appVersion)) {
                    int reqAppVersion = Integer.parseInt(appVersion);
                    // Temporary hack - Allow user to move ahead if version is
                    // comes as -1.
                    // Bug to be fixed by android app team.
                    if (reqAppVersion != -1 && reqAppVersion < minVersion) {
                        return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                    }
                } else {
                    return getAndroidVersionInvalidErrorObjectAndLogMetrics(model);
                }
            }

            if (!checkCouponDataSanity(freeFundRequestWebDO.getCouponCode())) {
                logger.info("coupon validation failure");
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.EMPTY_CODE_ERROR.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.EMPTY_CODE_ERROR.toString());
                return "jsonView";
            }

            logger.info("IMEI : " + imeiNumber + " AdvId " + advertisementId + "deviceUniqueId" + deviceUniqueId
                    + " IMSI : " + imsi + " IsEmulator : " + isEmulator + " for "
                    + freeFundRequestWebDO.getCouponCode() + " and user id " + user.getUserId());

            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            fs.setIMEINumber(imeiNumber);
            fs.setAdvertisementId(advertisementId);
            fs.setDeviceUniqueId(deviceUniqueId);
            fs.setImsi(imsi);
            fs.setIsEmulator(isEmulator);

            freeFundRequestBusinessDO = new FreefundRequestBusinessDO();
            freeFundRequestBusinessDO.setEmail(user.getEmail());
            freeFundRequestBusinessDO.setCouponCode(freeFundRequestWebDO.getCouponCode().trim());

            FreefundCoupon freefundCoupon = freefundService
                    .getFreefundCoupon(freeFundRequestBusinessDO.getCouponCode());

            // Need to get coupon before captcha check so that we can update
            // captcha fraudcount
            if (appConfigService.isRecaptchaEnabled()) {
                boolean shouldCheckCaptcha = captchaService.shouldCheckCaptchaForCampaign(fcSession);
                SimpleReturn simpleReturn = captchaService.isCaptchValid(request);
                recordCaptcha(shouldCheckCaptcha, simpleReturn.isValid());
                if (shouldCheckCaptcha && !simpleReturn.isValid()) {
                    if (freefundCoupon != null) {
                        logger.info(freefundCoupon.getFreefundCode() + " [ " + freefundCoupon.getFreefundClassId()
                                + " ] " + " reCaptchaService validation failure " + " Reason : "
                                + simpleReturn.getObjMap());
                    } else {
                        logger.info(" reCaptchaService validation failure. Reason : " + simpleReturn.getObjMap());
                    }
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.CAPTCHA_VALIDATION_FAIL.getErrorNumber());
                    model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INCORRECT_CAPTCHA));
                    model.addAttribute(WebConstants.FORCE_CAPTCHA, true);
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.CAPTCHA_VALIDATION_FAIL.toString());
                    return "jsonView";
                }
                /*
                 * Reset captcha only if its a valid code and captcha validated
                 * first time
                 */
                boolean resetCaptcha = (freefundCoupon != null) && simpleReturn.isValid();
                model.addAttribute(WebConstants.FORCE_CAPTCHA, captchaService.forceCaptchaForCampaign(fcSession, resetCaptcha));
                captchaService.markTokenUsed(fcSession, true);
            }
            // Captcha check should happend before coupon validity check
            if (freefundCoupon == null) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        ErrorCode.INVALID_CODE.toString());
                return "jsonView";
            } else {
                model.addAttribute("codeType", freefundCoupon.getPromoEntity());
            }


            JSONObject versionObject = appConfigService.getCustomProp(FCConstants.REFERRAL_CAMPAIGN_KEY);
            if (FCUtil.isNotEmpty(versionObject)) {
                String referralCampaignIdString = String.valueOf(versionObject.get(FCConstants.CAMPAIGN_ID));
                String isReferralEnabled = String.valueOf(versionObject.get(FCConstants.IS_REFERRAL_ENABLED));
                long referralCampId = Long.parseLong(referralCampaignIdString);
                if ("false".equals(isReferralEnabled) && freefundCoupon.getFreefundClassId() == referralCampId) {
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", FreefundErrorCode.REFERRAL_SHUT_DOWN.getErrorNumber());
                    model.addAttribute("errors",
                            "The referral program is under maintenance and being revamped...we will be back shortly. " +
                                    "We apologise for the inconvenience. ");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            FreefundErrorCode.REFERRAL_SHUT_DOWN.toString());
                    return "jsonView";
                }
            }

            isFreefundCode = "redeem-to-fcbalance".equals(freefundCoupon.getPromoEntity());
            CartBusinessDo cartBusinessDo = null;
            ProductName productName = null;
            if (!isFreefundCode) {
                cartBusinessDo = cartService.getCart(freeFundRequestWebDO.getLookupID());
                if (cartBusinessDo == null) {
                    // transactional discount avail, but no cart found. User
                    // tried redeeming promocode from balance page
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
                    model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
                    logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                            + " promocode apply from credits page ");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            ErrorCode.INVALID_CODE.toString());
                    return "jsonView";
                } else {
                    try {
                        productName = cartService.getPrimaryProduct(cartBusinessDo);
                    } catch (Exception e) {
                        logger.error("error getting productName for email: " + user.getEmail(), e);
                    }
                }
            }
            // If this code us already redeemed, no need to do any other checks.
            // this will reduce db load
            if ("REDEEMED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_REDEEMED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
                }
                return "jsonView";
            }

            if ("BLOCKED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                if (isFreefundCode) {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_FREEFUND,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                } else {
                    metricsClient.recordEvent(
                            FreefundService.METRIC_SERVICE_PROMOCODE,
                            METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                }
                return "jsonView";
            }

            SimpleReturn simpleReturn = freefundService.validateClassOnApply(cartBusinessDo, freefundCoupon);
            if (!simpleReturn.isValid()) {
                logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                        + " class validtion failed." + simpleReturn.getObjMap().get("message"));
                model.addAttribute("status", "failure");
                model.addAttribute("errorCode", simpleReturn.getErrorCode().getErrorNumber());
                model.addAttribute("errors", simpleReturn.getObjMap().get(SimpleReturn.MESSAGE_KEY));
                if (isFreefundCode) {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, METRIC_FREEFUND_VALIDATE_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + simpleReturn.getErrorCode().toString());
                } else {
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                            "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                    + simpleReturn.getErrorCode().toString());
                }
                return "jsonView";
            }
            boolean isNewCampaign = false;
            if (freefundCoupon.getFreefundClassId() != null && freefundCoupon.getFreefundClassId() != 0) {
                FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
                String ip = MobileUtil.getRealClientIpAddress(request);
                if (freefundClass != null && !freefundClass.getIsNewCampaign()) {
                } else {
                    isNewCampaign = true;
                    HashMap<String, Object> transactionInfo = new HashMap<>();
                    transactionInfo.put(InTxnDataConstants.PROMOCODE, freefundCoupon.getFreefundCode());
                    transactionInfo.put(InTxnDataConstants.FF_CLASS_ID, freefundCoupon.getFreefundClassId().intValue());
                    Integer userId=user.getUserId();
                    transactionInfo.put(InTxnDataConstants.USER_ID, userId );
                    //new user condition
                    List<String> orders= userTransactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 1);
                    if (null == orders || orders.isEmpty()) {
                        transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
                    } else {
                        transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
                    }

                    transactionInfo.put(InTxnDataConstants.USER_EMAIL, user.getEmail());

                    MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(user.getEmail(), null);;
                    String oneCheckWalletId = null;
                    try {
                        logger.info("Fetching oneCheckWalletId for user " + userId);
                        oneCheckWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());                      
                        
                    } catch (Exception e) {
                        logger.error("error fetching oneCheckWalletId for email " + user.getEmail(), e);
                        model.addAttribute("status", "failure");
                        model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
                        model.addAttribute("errors", "Sorry, something went wrong. Please try again.");
                        return "jsonView";
                    }
                    if (null != migrationStatus) {
                        transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
                    }
                    if (null != oneCheckWalletId) {
                        transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
                    }
                    if (null != txnChannel) {
                        transactionInfo.put(InTxnDataConstants.CHANNEL, Ints.tryParse(txnChannel));
                    }
                    transactionInfo.put(InTxnDataConstants.IP_ADDRESS, ip);
                    transactionInfo.put(InTxnDataConstants.ADVERTISEMENT_ID, advertisementId);
                    if (null != appVersion) {
                        transactionInfo.put(InTxnDataConstants.APP_VERSION, Ints.tryParse(appVersion));
                    }
                    transactionInfo.put(InTxnDataConstants.DEVICE_UNIQUE_ID, deviceUniqueId);
                    transactionInfo.put(InTxnDataConstants.IMEI_NUMBER, imeiNumber);
                    transactionInfo.put(InTxnDataConstants.IMSI, imsi);
                    transactionInfo.put(InTxnDataConstants.IS_EMULATOR, isEmulator);

                    if(isFreefundCode) {
                        transactionInfo.put(InTxnDataConstants.ORDER_ID, "OD" + requestIdGenerator.nextRequestId() +
                         "_" + freefundCoupon.getFreefundCode());
                    }

                    String uniqueId = FCUtil.isNotEmpty(freeFundRequestWebDO.getLookupID()) ?
                            freeFundRequestWebDO.getLookupID() : "ff_" + UUID.randomUUID();

                    try {
                        logger.info("LookUpId for campaign_service: "+freeFundRequestWebDO.getLookupID());
                        if(!isFreefundCode) {
                            campaignServiceClient.saveInTxnData1(uniqueId,
                                    FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
                        } else {
                            campaignServiceClient.saveInTxnData1(uniqueId,
                                    FCConstants.FREEFUND_MERCHANT_ID, transactionInfo);
                        }
                    }catch (Exception e){
                        logger.error("error saving intxndata in freefund service: ", e);
                    }
                    CampaignResult campaignResult = null;
                    if (!isFreefundCode) {
                        campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                                uniqueId, freefundClass.getId(),
                                "APPLY_PROMOCODE", FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
                    } else {
                        freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
                        freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
                        freeFundRequestBusinessDO.setLookupID(uniqueId);
                        boolean isBlockSuccess = freefundService.blockCouponCode(freeFundRequestBusinessDO);
                        if (!isBlockSuccess){
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                            if (isFreefundCode) {
                                metricsClient.recordEvent(
                                        FreefundService.METRIC_SERVICE_FREEFUND,
                                        METRIC_FREEFUND_APPLY_FAIL,
                                        "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                                + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                            } else {
                                metricsClient.recordEvent(
                                        FreefundService.METRIC_SERVICE_PROMOCODE,
                                        METRIC_FREEFUND_APPLY_FAIL,
                                        "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                                + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                            }
                            return "jsonView";
                        }
                        try {
                            campaignResult = campaignServiceClient.checkAndProcessCampaign1(
                                    uniqueId, freefundClass.getId(),
                                    "TXN_SUCCESS", FCConstants.FREEFUND_MERCHANT_ID);
                        } catch (Exception e) {
                            logger.error("Error calling campaign service to process freefund "
                                + freefundCoupon.getFreefundCode(), e);
                            freefundService.unblockPromocode(freefundCoupon, userId, uniqueId);
                            model.addAttribute("status", "failure");
                            model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
                            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
                            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                                    ErrorCode.UNKNOWN_ERROR.toString());
                            return "jsonView";
                        }
                    }
                    if (!campaignResult.getStatus()) {
                        if (isFreefundCode) {
                            freefundService.unblockPromocode(freefundCoupon, userId, uniqueId);
                        }
                        model.addAttribute("status", "failure");
                        // Intentionally confusing message to user
                        model.addAttribute("errorCode", ErrorCode.APPLY_FRAUD_DETECTED.getErrorNumber());
                        model.addAttribute("errors", campaignResult.getConditionResult().getFailedRule().getDetailedFailureMessage());
                        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId()
                                + "] " + campaignResult.getConditionResult().getFailedRule().getFailureMessage());
                        logger.info("Time duration for apply code = " + freefundCoupon.getFreefundCode() + ", by user email = "+ user.getEmail() + " is = " + (System.currentTimeMillis()-startTime));
                        return "jsonView";
                    }
                }
            }

            freeFundRequestBusinessDO.setFreefundCoupon(freefundCoupon);
            freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
            boolean validCoupon = (isNewCampaign && isFreefundCode)?true:freefundService.validateAndSetCouponId(freeFundRequestBusinessDO);
            freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
            model.addAttribute("freefundCoupon", freefundCoupon);


            // Publishing user data points for Mystique (Unique User Identification Service)
            Date date = new Date();
            userDataPointSNSService.publishUserDataPoint(user.getUserId(), imeiNumber, UserDataPointType.IMEI, date);
            userDataPointSNSService.publishUserDataPoint(user.getUserId(), deviceUniqueId,
                    UserDataPointType.WINDOWS_UNIQUE_DEVICE_ID, date);

            Integer[] paymentOptions = null;
            JSONObject paymentOptionsObject = appConfigService.getCustomProp(FCConstants.PAYMENT_OPTIONS_KEY);
            Object allEnabledStr = paymentOptionsObject.get(FCConstants.ALL_ENABLED);
            if (allEnabledStr != null && !FCUtil.isEmpty(String.valueOf(allEnabledStr))) {
                String allEnabled = String.valueOf(allEnabledStr);
                if(PaymentConstants.ALL.equalsIgnoreCase(allEnabled)) {
                    paymentOptions = new Integer[]{0};
                } else {
                    List<PaymentTypeMaster> paymentTypeMasters = paymentTransactionService.getPaymentOptions();
                    List<Integer> idList = new ArrayList<>();
                    for (PaymentTypeMaster paymentOption : paymentTypeMasters) {
                        idList.add(paymentOption.getPaymentTypeMasterId());
                    }

                    if (!FCUtil.isEmpty(idList)) {
                        paymentOptions = idList.toArray(new Integer[idList.size()]);
                    }
                }
             }

            if (isFreefundCode) {
                return processFreefundRedeem(validCoupon, freeFundRequestBusinessDO, model, user, paymentOptions,
                        isNewCampaign);
            } else {

                freeFundRequestBusinessDO.setCartBusinessDo(cartBusinessDo);
                freeFundRequestBusinessDO.setLookupID(freeFundRequestWebDO.getLookupID());
                for (Entry<Integer, String> productMap : FCConstants.productMasterMap.entrySet()) {
                    if (productMap.getValue().equals(freeFundRequestWebDO.getProductType())) {
                        freeFundRequestBusinessDO.setProductType(productMap.getKey());
                    }
                }

                for (CartItemsBusinessDO cartItemsDO : cartBusinessDo.getCartItems()) {
                    if (FCConstants.productMasterMap.containsKey(cartItemsDO.getProductMasterId())) {
                        freeFundRequestBusinessDO.setServiceNumber(cartItemsDO.getDisplayLabel());
                    }
                }
                freeFundRequestBusinessDO.setServiceNumber(cartService.getRechargedNumber(freeFundRequestWebDO
                        .getLookupID()));
                /*
                 * ==========================GENERIC
                 * PROMOCODE================================== This is the right
                 * place to handle generic promocode logic. Do not move this.
                 */
                boolean isGenericCashbackPromocode = FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon
                        .getPromoEntity());
                boolean isGenericDiscountPromocode = FreefundClass.PROMO_ENTITY_GENERIC_DISCOUNT.equals(freefundCoupon
                        .getPromoEntity());
                logger.info("Time duration for apply code = " + freefundCoupon.getFreefundCode() + ", by user email = "+ user.getEmail() + " is = " + (System.currentTimeMillis()-startTime));
                if (isGenericCashbackPromocode) {
                    return processGenericCashbackPromocode(validCoupon, freeFundRequestBusinessDO,
                            freeFundRequestWebDO, model, user, paymentOptions);
                } else if (isGenericDiscountPromocode) {
                    return processGenericDiscountPromocode(validCoupon, freeFundRequestBusinessDO,
                            freeFundRequestWebDO, model, user, paymentOptions);
                }
                /*
                 * =========================GENERIC
                 * PROMOCODE===================================
                 */

                return processPromocodeAvail(validCoupon, freeFundRequestBusinessDO, freeFundRequestWebDO, model, user,
                        paymentOptions);
            }

        } catch (Exception e) {
            logger.error("Exception in promocode/freefund code path : ", e);
            model.addAttribute("status", "failure");
            model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                    ErrorCode.UNKNOWN_ERROR.toString());
            return "jsonView";
        } finally {
            if (model != null && model.asMap() != null
                    && "failure".equalsIgnoreCase(String.valueOf(model.asMap().get("status")))
                    && freeFundRequestWebDO != null) {
                freefundService.removeFreefundFromCart(freeFundRequestWebDO.getLookupID());
            }
        }

    }

    private boolean shouldForceSocialConnect(final FreefundFraud applyFraud) {
        if (applyFraud.getErrorCode() != null) {
            String errorCode = applyFraud.getErrorCode();
            if (!errorCode.isEmpty()
                    && (errorCode.equals(ErrorCode.DEVICE_FP_FRAUD_ERROR.getErrorNumberString())
                            || errorCode.equals(ErrorCode.IP_ADDRESS_FRAUD_ERROR.getErrorNumberString()) || errorCode
                                .equals(ErrorCode.COOKIE_CONDITION_FAIL.getErrorNumberString()))) {
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value = "/rest/promocode/validate", method = RequestMethod.GET)
    @NoLogin
    @ResponseBody
    public Map<String, Object> promocodeValidate(final HttpServletRequest request, final HttpServletResponse response) {
        String couponCode = String.valueOf(request.getParameter("promocode"));
        Map<String, Object> responseData = new HashMap<String, Object>();
        FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();

        // check if promocode is enabled
        if (!appConfigService.isPromocodeEnabled()) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.PROMOCODE_FEATURE_DISABLED.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_PROMOCODE_DOWN));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                    ErrorCode.PROMOCODE_FEATURE_DISABLED.toString());
            return responseData;
        }

        // check if code is blank
        if (!checkCouponDataSanity(couponCode)) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.EMPTY_CODE_ERROR.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                    ErrorCode.EMPTY_CODE_ERROR.toString());
            return responseData;
        }

        FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(couponCode);

        // if Captcha enabled check its validity
        if (appConfigService.isRecaptchaEnabled()) {
            boolean shouldCheckCaptcha = captchaService.shouldCheckCaptchaForCampaign(fcSession);
            SimpleReturn simpleReturn = captchaService.isCaptchValid(request);
            recordCaptcha(shouldCheckCaptcha, simpleReturn.isValid());
            if (shouldCheckCaptcha && !simpleReturn.isValid()) {
                if (freefundCoupon != null) {
                    logger.info(freefundCoupon.getFreefundCode() + " [ " + freefundCoupon.getFreefundClassId() + " ] "
                            + " reCaptchaService validation failure " + " Reason : " + simpleReturn.getObjMap());
                } else {
                    logger.info("reCaptchaService validation failure. Reason : " + simpleReturn.getObjMap());
                }
                responseData.put("result", FCConstants.FAIL);
                responseData.put("errorCode", ErrorCode.CAPTCHA_VALIDATION_FAIL.getErrorNumber());
                responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_INCORRECT_CAPTCHA));
                responseData.put(WebConstants.FORCE_CAPTCHA, true);
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                        ErrorCode.CAPTCHA_VALIDATION_FAIL.toString());
                return responseData;
            }
            /*
             * Reset captcha only if its a valid code and captcha validated
             * first time
             */
            boolean resetCaptcha = (freefundCoupon != null) && simpleReturn.isValid();
            responseData.put(WebConstants.FORCE_CAPTCHA, captchaService.forceCaptchaForCampaign(fcSession, resetCaptcha));
            captchaService.markTokenUsed(fcSession, true);
        }

        // check if code exists
        if (freefundCoupon == null) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.INVALID_CODE.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_INVALID_PROMOCDOE));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                    ErrorCode.INVALID_CODE.toString());
            return responseData;
        }

        // check if code is expired
        Calendar todayCal = Calendar.getInstance();
        todayCal.add(Calendar.DATE, -1);
        FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
        if (freefundClass != null && todayCal.after(freefundClass.getValidUpto())) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.CODE_EXPIRED.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_CODE_EXPIRED));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL,
                    "ffclass-" + freefundCoupon.getFreefundClassId() + "." + ErrorCode.CODE_EXPIRED.toString());
            return responseData;
        }

        // check if code is already redeemed
        if ("REDEEMED".equals(freefundCoupon.getStatus())) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.CODE_IN_REDEEMED_STATE.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
            metricsClient.recordEvent(
                    FreefundService.METRIC_SERVICE_PROMOCODE,
                    METRIC_FREEFUND_VALIDATE_FAIL,
                    "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                            + ErrorCode.CODE_IN_REDEEMED_STATE.toString());
            return responseData;
        }

        // check if code is blocked
        if ("BLOCKED".equals(freefundCoupon.getStatus())) {
            responseData.put("result", FCConstants.FAIL);
            responseData.put("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
            responseData.put("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
            metricsClient
                    .recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE_FAIL, "ffclass-"
                            + freefundCoupon.getFreefundClassId() + "." + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
            return responseData;
        }

        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_VALIDATE, "ffclass-"
                + freefundCoupon.getFreefundClassId());
        responseData.put("freefundCoupon", freefundCoupon);
        responseData.put("result", FCConstants.SUCCESS);
        return responseData;
    }

    private String getApplyCookie(final HttpServletRequest request, final HttpServletResponse response,
            final Integer freefundClassId, final String email) {
        String applyTrackingCookieValue = CookieUtil.getCookieValue(request, APPLY_TRACKING_COOKIE_NAME);
        if (applyTrackingCookieValue == null) {
            applyTrackingCookieValue = UUID.randomUUID().toString();
            CookieUtil.setCookie(response, APPLY_TRACKING_COOKIE_NAME, applyTrackingCookieValue, Integer.MAX_VALUE,
                    false);
        }
        return applyTrackingCookieValue;
    }

    public void recordCaptcha(final boolean shouldCheckCaptcha, final boolean valid) {
        if (shouldCheckCaptcha) {
            metricsClient.recordEvent(METRIC_CAPTCHA_SERVICE, METRIC_CAPTCHA_ENTERED, "entered");
            if (valid) {
                metricsClient.recordEvent(METRIC_CAPTCHA_SERVICE, METRIC_CAPTCHA_SUCCESS, "success");
            } else {
                metricsClient.recordEvent(METRIC_CAPTCHA_SERVICE, METRIC_CAPTCHA_FAIL, "fail");
            }
        }
    }

    private String processGenericDiscountPromocode(final boolean validCoupon,
                                                   final FreefundRequestBusinessDO freeFundRequestBusinessDO, final FreefundRequestWebDO freeFundRequestWebDO,
                                                   final Model model, final Users user, Integer[] paymentOptions) {

        FreefundCoupon freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
        FreefundClass freefundClass = freefundService.getFreefundClass(freeFundRequestBusinessDO.getCouponCode());
        String status = freefundCoupon.getStatus();
        if (validCoupon) {
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " promocode apply success. " + freefundCoupon.getStatus());
            Double rechargeAmount = cartService.getRechargeAmount(freeFundRequestBusinessDO.getCartBusinessDo());
            freefundService.saveCartWithFreeFund(freeFundRequestBusinessDO);
            model.addAttribute("status", "success");
            Map dataMap = setSuccessMessageForCode(
                    freefundClass,
                    "You've applied the code successfully! Your payment "
                            + "has been discounted by Rs."
                            + freefundCoupon.getDiscountValue(rechargeAmount, freefundClass.getDiscountType(),
                                    freefundClass.getMaxDiscount()) + ".");
            logger.info("SuccessMessage:" + dataMap.get("successMessage"));
            model.addAttribute("successMsg", dataMap.get("successMessage"));
            model.addAttribute("paymentOptions", paymentOptions);
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_APPLY,
                    "ffclass-" + (freefundClass != null ? freefundClass.getId() : ""));
            String number = cartService.getRechargedNumber(freeFundRequestWebDO.getLookupID());
            return "jsonView";
        } else {

            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " coupon validation failure. " + freefundCoupon.getStatus());
            model.addAttribute("status", "failure");
            if ("BLOCKED".equals(status)) {
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        "ffclass-" + freefundClass.getId() + "." + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                return "jsonView";
            }
        }
        model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
        model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                + " coupon validation failure. " + freefundCoupon.getStatus());
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL, "ffclass-"
                + freefundClass.getId() + "." + ErrorCode.UNKNOWN_ERROR.toString());
        return "jsonView";

    }

    private Map setSuccessMessageForCode(final FreefundClass freefundClass, final String defaultMessage) {
        Map dataMap = new HashMap();
        String datamapjson = freefundClass.getDatamap();
        if (datamapjson != null && datamapjson.length() != 0) {
            dataMap = FCStringUtils.jsonToMap(datamapjson);
        }
        if (dataMap.get("successMessage") == null || "".equals(dataMap.get("successMessage").toString().trim())) {

            dataMap.put("successMessage", defaultMessage);
        }
        return dataMap;
    }

    private String processGenericCashbackPromocode(final boolean validCoupon,
                                                   final FreefundRequestBusinessDO freeFundRequestBusinessDO, final FreefundRequestWebDO freeFundRequestWebDO,
                                                   final Model model, final Users user, Integer[] paymentOptions) {

        FreefundCoupon freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
        FreefundClass freefundClass = freefundService.getFreefundClass(freeFundRequestBusinessDO.getCouponCode());
        String status = freefundCoupon.getStatus();
        if (validCoupon) {
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " promocode apply success. " + freefundCoupon.getStatus());
            freefundService.saveCartWithFreeFund(freeFundRequestBusinessDO);
            model.addAttribute("status", "success");
            Map dataMap = setSuccessMessageForCode(freefundClass,
                    "You have successfully applied" + freefundCoupon.getFreefundCode()
                            + ", your cash back will be processed within 24hrs .");
            logger.info("SuccessMessage:" + dataMap.get("successMessage"));
            model.addAttribute("successMsg", dataMap.get("successMessage"));
            model.addAttribute("paymentOptions", paymentOptions);
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_APPLY,
                    "ffclass-" + (freefundClass != null ? freefundClass.getId() : ""));
            String number = cartService.getRechargedNumber(freeFundRequestWebDO.getLookupID());
            return "jsonView";
        } else {

            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " coupon validation failure. " + freefundCoupon.getStatus());
            model.addAttribute("status", "failure");
            if ("BLOCKED".equals(status)) {
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        "ffclass-" + freefundClass.getId() + "." + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                return "jsonView";
            }
        }
        model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
        model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                + " coupon validation failure. " + freefundCoupon.getStatus());
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL, "ffclass-"
                + freefundClass.getId() + "." + ErrorCode.UNKNOWN_ERROR.toString());
        return "jsonView";

    }

    private String processPromocodeAvail(final boolean validCoupon,
                                         final FreefundRequestBusinessDO freeFundRequestBusinessDO, final FreefundRequestWebDO freeFundRequestWebDO,
                                         final Model model, final Users user, Integer[] paymentOptions) {

        FreefundCoupon freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
        FreefundClass freefundClass = freefundService.getFreefundClass(freeFundRequestBusinessDO.getCouponCode());
        if (validCoupon) {
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " promocode apply success. " + freefundCoupon.getStatus());
            freefundService.saveCartWithFreeFund(freeFundRequestBusinessDO);
            Double rechargeAmount = cartService.getRechargeAmount(freeFundRequestBusinessDO.getCartBusinessDo());
            model.addAttribute("status", "success");
            Map dataMap = setSuccessMessageForCode(
                    freefundClass,
                    "You've applied the code successfully! "
                            + "Your payment has been discounted by Rs."
                            + freefundCoupon.getDiscountValue(rechargeAmount, freefundClass.getDiscountType(),
                            freefundClass.getMaxDiscount()) + ".");
            logger.info("SuccessMessage:" + dataMap.get("successMessage"));
            model.addAttribute("successMsg", dataMap.get("successMessage"));
            model.addAttribute("paymentOptions", paymentOptions);
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_APPLY,
                    "ffclass-" + (freefundClass != null ? freefundClass.getId() : ""));
            String number = cartService.getRechargedNumber(freeFundRequestWebDO.getLookupID());
            return "jsonView";
        } else {

            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " coupon validation failure. " + freefundCoupon.getStatus());
            model.addAttribute("status", "failure");
            if ("BLOCKED".equals(freefundCoupon.getStatus())) {
                model.addAttribute("errorCode", ErrorCode.CODE_IN_BLOCKED_STATE.getErrorNumber());
                model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_BLOCKED));
                metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                        "ffclass-" + freefundClass.getId() + "." + ErrorCode.CODE_IN_BLOCKED_STATE.toString());
                return "jsonView";
            }
        }
        model.addAttribute("errorCode", ErrorCode.UNKNOWN_ERROR.getErrorNumber());
        model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
        logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                + " coupon validation failure. " + freefundCoupon.getStatus());
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL, "ffclass-"
                + freefundClass.getId() + "." + ErrorCode.UNKNOWN_ERROR.toString());
        return "jsonView";

    }

    private String processFreefundRedeem(final boolean validCoupon,
                                         final FreefundRequestBusinessDO freeFundRequestBusinessDO, final Model model,
                                         final Users user, Integer[] paymentOptions, boolean isNewCampaign) {
        FreefundCoupon freefundCoupon = freeFundRequestBusinessDO.getFreefundCoupon();
        if (validCoupon) {
            FreefundClass freefundClass = freefundService.getFreefundClass(freeFundRequestBusinessDO.getCouponCode());
            model.addAttribute("freefundCoupon", freefundCoupon);
            freeFundRequestBusinessDO.setCouponId(freefundCoupon.getId());
            if (!isNewCampaign) {
                com.freecharge.rest.model.ResponseStatus responseStatus = freefundService.redeemToWallet(freeFundRequestBusinessDO, user);
                model.addAttribute("status", responseStatus.getStatus());
                if ("failure".equals(responseStatus.getStatus())) {
                    logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                            + " Wallet credit failure. ");
                    model.addAttribute("status", "failure");
                    model.addAttribute("errorCode", ErrorCode.WALLET_CREDIT_FAIL.getErrorNumber());
                    model.addAttribute("errors", "Wallet limit exceeded. Can not redeem coupon");
                    metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                            "ffclass-" + freefundClass.getId() + "." + ErrorCode.WALLET_CREDIT_FAIL.toString());
                    return "jsonView";
                }
            } else {
                model.addAttribute("status", "success");
                freefundService.markFreefundRedeemed(freeFundRequestBusinessDO, freefundCoupon, user);
            }
            Map dataMap = setSuccessMessageForCode(freefundClass, "You've applied the code successfully! Your balance "
                    + "has been credited with Rs. " + freefundCoupon.getFreefundValue() + ".");
            
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " freefund redeem success.");
            logger.info("SuccessMessage:" + dataMap.get("successMessage"));
            model.addAttribute("successMsg", dataMap.get("successMessage"));
            model.addAttribute("paymentOptions", paymentOptions);
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, FreefundService.METRIC_NAME_APPLY,
                    "ffclass-" + (freefundClass != null ? freefundClass.getId() : ""));
            return "jsonView";
        } else {
            logger.info(freefundCoupon.getFreefundCode() + " [" + freefundCoupon.getFreefundClassId() + "] "
                    + " Coupon validation failure. ");
            model.addAttribute("status", "failure");
            model.addAttribute("errorCode", ErrorCode.INVALID_FREEFUNDCODE.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_CODE_ALREADY_USED));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_FREEFUND, METRIC_FREEFUND_APPLY_FAIL, "ffclass-"
                    + freefundCoupon.getFreefundClassId() + "." + ErrorCode.INVALID_FREEFUNDCODE.toString());
            return "jsonView";
        }
    }

    private boolean checkCouponDataSanity(final String couponCode) {
        if (couponCode == null || couponCode.isEmpty()) {
            return false;
        }

        return true;
    }

    public int getMinVersion() {
        JSONObject versionObject = appConfigService.getCustomProp(FCConstants.PROMOCODE_APP_VERSION);
        Object minVersionStr = versionObject.get(FCConstants.ANDROID_VERSION_KEY);
        if (minVersionStr != null && !FCUtil.isEmpty(String.valueOf(minVersionStr))) {
            return Integer.parseInt(String.valueOf(minVersionStr));
        } else {
            return -1;
        }
    }

    public String getAndroidVersionInvalidErrorObjectAndLogMetrics(final Model model) {
        model.addAttribute("status", "failure");
        model.addAttribute("errorCode", ErrorCode.ANDROID_APP_VERSION_INVALID.getErrorNumber());
        model.addAttribute("errors", fcProperties.getProperty(FCProperties.INVALID_ANDROID_APP_VERSION));
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_APPLY_FAIL,
                ErrorCode.ANDROID_APP_VERSION_INVALID.toString());
        return "jsonView";
    }
    
    @RequestMapping(value = "/promocode/remove", method = RequestMethod.POST, produces = "application/json")
    public String removePromoCode(
            @ModelAttribute(value = "freefundRequestWebDO") FreefundRequestWebDO freeFundRequestWebDO,
            HttpServletRequest request, HttpServletResponse response, Model model){
    	logger.info("Processing request to remove freefund code : " + freeFundRequestWebDO.getCouponCode() + " for lookup id : " + freeFundRequestWebDO.getLookupID());
    	FreefundCoupon freefundCoupon = freefundService
                .getFreefundCoupon(freeFundRequestWebDO.getCouponCode());
    	com.freecharge.app.domain.entity.jdbc.Users user = userServiceProxy.getLoggedInUser();
    	if(user == null){
    		 logger.info("User not found in session data for applied promocode " +
                     freeFundRequestWebDO.getCouponCode());
             model.addAttribute("status", FCConstants.ERROR);
             model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
             model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
             metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_REMOVE_FAIL,
                     ErrorCode.DEFAULT_ERROR.toString());
             return "jsonView";
    	}
    	CartBusinessDo cartBusinessDo = cartService.getCart(freeFundRequestWebDO.getLookupID());
    	if(cartBusinessDo == null){
    		logger.info("No cart found for given lookup id " +
                    freeFundRequestWebDO.getLookupID());
            model.addAttribute("status", FCConstants.ERROR);
            model.addAttribute("errorCode", ErrorCode.DEFAULT_ERROR.getErrorNumber());
            model.addAttribute("errors", fcProperties.getProperty(FCProperties.MSG_UNKNOWN_ERROR));
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_REMOVE_FAIL,
                    ErrorCode.DEFAULT_ERROR.toString());
            return "jsonView";
    	}
    	
    	if(freefundCoupon == null){ // Even if the code is not present in cart, still return SUCCESS
    		logger.info("No promocode found for given code " +
                    freeFundRequestWebDO.getCouponCode() +
                        "Return Success, as this might be an unwanted promocode remove call");
            model.addAttribute("status", FCConstants.SUCCESS);
            model.addAttribute("message", fcProperties.getProperty(FCProperties.MSG_NO_CODE_TO_REMOVE));
            return "jsonView";
    	}
  
    	// If the code is in 'Redeemed/Blocked' state, it should NOT be removed from cart.
  
        if ((FCConstants.BLOCKED.equalsIgnoreCase(freefundCoupon.getStatus())) || FCConstants.REDEEMED.equalsIgnoreCase(freefundCoupon.getStatus())) {
            model.addAttribute("status", FCConstants.ERROR);
            model.addAttribute("errorCode", ErrorCode.CODE_NOT_REMOVED_ERROR.getErrorNumber());
            model.addAttribute("errors", freefundCoupon.getFreefundCode() + " "
                    + fcProperties.getProperty(FCProperties.MSG_CODE_NOT_REMOVED));
            
            metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, METRIC_FREEFUND_REMOVE_FAIL,
                        "ffclass-" + freefundCoupon.getFreefundClassId() + "."
                                + ErrorCode.CODE_NOT_REMOVED_ERROR.toString());
            
            return "jsonView";
        }
               
        logger.info("Releasing coupon " + freeFundRequestWebDO.getCouponCode() + " from cart : " + cartBusinessDo.getId());

        // Need ip/cookie/deviceFP info to decrement promocode apply counts
        
        FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();
        String applyCookie = getApplyCookie(request, response, freefundCoupon.getFreefundClassId().intValue(), user.getEmail());  
        String ip = MobileUtil.getRealClientIpAddress(request);
        String fingerPrint = null;
       
        if (fcSession.getSessionData().get(WebConstants.SESSION_FINGER_PRINT) != null) {
              fingerPrint = String.valueOf(fcSession.getSessionData().get(WebConstants.SESSION_FINGER_PRINT));
        }
          
        freefundService.removeFreefundCode(freefundCoupon.getFreefundClassId(), 
                                           freefundCoupon.getFreefundCode(), 
                                           cartBusinessDo, applyCookie, 
                                           ip,fingerPrint, 
                                           user.getUserId(), user.getEmail());
        model.addAttribute("status", FCConstants.SUCCESS);
        model.addAttribute("message", fcProperties.getProperty(FCProperties.MSG_CODE_REMOVED));
        metricsClient.recordEvent(FreefundService.METRIC_SERVICE_PROMOCODE, FreefundService.METRIC_NAME_REMOVE,
                "ffclass-" + (freefundCoupon.getFreefundClassId() != null ? freefundCoupon.getFreefundClassId(): ""));

        return "jsonView";

    }

    @NoLogin
    @Csrf(exclude = true)
    @RequestMapping(value = "/promocode/validateAndGetFreefundClass", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    CampaignPromocodeValidateResponse removePromoCode(
            @RequestParam("promocode") String promocode,
            HttpServletRequest request, HttpServletResponse response, Model model) {
        CampaignPromocodeValidateResponse campaignPromocodeValidateResponse = new CampaignPromocodeValidateResponse();
        FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(promocode);
        if (freefundCoupon != null) {
            campaignPromocodeValidateResponse.setFreefundClass(freefundService.getFreefundClass(freefundCoupon.getFreefundClassId()));
            campaignPromocodeValidateResponse.setFreefundCoupon(freefundCoupon);
            campaignPromocodeValidateResponse.setStatus(true);
        } else {
            campaignPromocodeValidateResponse.setStatus(false);
        }
        return campaignPromocodeValidateResponse;
    }
}
