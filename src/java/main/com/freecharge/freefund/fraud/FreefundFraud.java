package com.freecharge.freefund.fraud;

public class FreefundFraud {
	private boolean isFraud;
	private String message; // For debugging and CS team 
	private String detailedMessage; // Visible for customers via e-mail or any other source 
	private String errorCode;
	
	public boolean isFraud() {
		return isFraud;
	}

	public void setFraud(boolean isFraud) {
		this.isFraud = isFraud;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

	public String getErrorCode() {
	    return errorCode;
	}
	public void setErrorCode(String errorCode) {
	    this.errorCode = errorCode;
	}

    public void setFreefundFraud(boolean isFraud, String message, String detailedMessage, String errorCode) {
        this.isFraud = isFraud;
        this.message = message;
        this.detailedMessage = detailedMessage;
		this.errorCode = errorCode;
    }

}
