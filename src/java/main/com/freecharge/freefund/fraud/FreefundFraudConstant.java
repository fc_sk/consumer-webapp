package com.freecharge.freefund.fraud;

public final class FreefundFraudConstant {
    public static final String APPLY_COUNT                    = "applyCount";

    public static final String REEDEEMED_COUNT_EMAIL          = "redeemedCountEmail";
    public static final String BLOCKED_COUNT_EMAIL            = "blockedCountEmail";

    public static final String REEDEEMED_COUNT_NUMBER         = "redeemedCountNUMBER";
    public static final String BLOCKED_COUNT_NUMBER           = "blockedCountNUMBER";

    public static final String REEDEEMED_COUNT_PROFILE_NUMBER = "redeemedCountProfileNumber";
    public static final String BLOCKED_COUNT_PROFILE_NUMBER   = "blockedCountProfileNumber";

    public static final String USER_ID                        = "userId";
    public static final String PROFILE_ID                     = "profileId";
    public static final String SERVICE_NUMBER                 = "serviceNumber";
    public static final String FREEFUND_CLASS                 = "freefundClassId";
    public static final String PROMOCODE                      = "promocode";
    public static final String PRODUCT_TYPE                   = "productType";
    public static final String TXN_CHANNEL                    = "channelType";
    public static final String CARDHASH                       = "cardHash";
    public static final String ORDERID                        = "orderId";
    public static final String NUMTIMES                       = "numberOfTimes";
    public static final String DATE_ADDED                     = "addedDate";
    public static final String CARDBIN                        = "cardBin";
    public static final String FINGER_PRINT                   = "fingerPrint";
    public static final String IP                             = "ip";
    public static final String IMEI_NUMBER_COUNT              = "imeiNumberCount";
    public static final String ADVERTISEMENT_ID_COUNT         = "advertisementIdCount";
    public static final String EMAIL = "email";
    public static final String ALLOWED_TACS = "allowedTacs";
}
