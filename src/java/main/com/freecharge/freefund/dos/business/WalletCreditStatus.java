package com.freecharge.freefund.dos.business;


public class WalletCreditStatus {
    private boolean walletCredited;
    private String walletTransactionId;
    private String voucherId;

    public String getWalletTransactionId() {
        return walletTransactionId;
    }

    public void setWalletTransactionId(String walletTransactionId) {
        this.walletTransactionId = walletTransactionId;
    }

    public boolean isWalletCredited() {
        return walletCredited;
    }

    public void setWalletCredited(boolean walletCredited) {
        this.walletCredited = walletCredited;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }
}

