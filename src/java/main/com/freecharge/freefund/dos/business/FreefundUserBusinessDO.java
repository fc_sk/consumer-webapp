package com.freecharge.freefund.dos.business;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class FreefundUserBusinessDO extends BaseBusinessDO {

    private Long userid;
    private String email;
    private String serviceNumber;
    private CartBusinessDo cartBusinessDo;
    private Integer primaryProduct;
    private Double totalAmount;
    private Boolean fistTimeUser;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CartBusinessDo getCartBusinessDo() {
        return cartBusinessDo;
    }

    public void setCartBusinessDo(CartBusinessDo cartBusinessDo) {
        this.cartBusinessDo = cartBusinessDo;
    }

    public Integer getPrimaryProduct() {
        return primaryProduct;
    }

    public void setPrimaryProduct(Integer primaryProduct) {
        this.primaryProduct = primaryProduct;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Boolean getFistTimeUser() {
        return fistTimeUser;
    }

    public void setFistTimeUser(Boolean fistTimeUser) {
        this.fistTimeUser = fistTimeUser;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }
}
