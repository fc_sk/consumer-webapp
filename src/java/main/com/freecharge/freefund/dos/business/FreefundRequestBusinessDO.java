package com.freecharge.freefund.dos.business;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.freefund.dos.entity.FreefundCoupon;

public class FreefundRequestBusinessDO extends BaseBusinessDO {

    private String couponCode;
    private Integer productType;
    private Long couponId;
    private String email;
    private String serviceNumber;
    private CartBusinessDo cartBusinessDo;
    private FreefundPaymentBusinessDO freefundPaymentBusinessDO;
    private FreefundUserBusinessDO freefundUserBusinessDO;
    private FreefundCoupon freefundCoupon;
    private int offerType;
    
    public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}
    
    public String getServiceNumber() {
		return serviceNumber;
	}
    
    public Integer getProductType() {
		return productType;
	}
    
    public void setProductType(Integer productType) {
		this.productType = productType;
	}

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CartBusinessDo getCartBusinessDo() {
        return cartBusinessDo;
    }

    public void setCartBusinessDo(CartBusinessDo cartBusinessDo) {
        this.cartBusinessDo = cartBusinessDo;
    }

    public FreefundPaymentBusinessDO getFreefundPaymentBusinessDO() {
        return freefundPaymentBusinessDO;
    }

    public void setFreefundPaymentBusinessDO(FreefundPaymentBusinessDO freefundPaymentBusinessDO) {
        this.freefundPaymentBusinessDO = freefundPaymentBusinessDO;
    }

    public FreefundUserBusinessDO getFreefundUserBusinessDO() {
        return freefundUserBusinessDO;
    }

    public void setFreefundUserBusinessDO(FreefundUserBusinessDO freefundUserBusinessDO) {
        this.freefundUserBusinessDO = freefundUserBusinessDO;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

	public FreefundCoupon getFreefundCoupon() {
		return freefundCoupon;
	}

	public void setFreefundCoupon(FreefundCoupon freefundCoupon) {
		this.freefundCoupon = freefundCoupon;
	}

	public int getOfferType() {
		return offerType;
	}

	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}

}
