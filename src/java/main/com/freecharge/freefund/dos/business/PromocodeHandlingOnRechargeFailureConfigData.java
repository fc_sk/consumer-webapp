package com.freecharge.freefund.dos.business;


import com.freecharge.common.framework.basedo.AbstractDO;

public class PromocodeHandlingOnRechargeFailureConfigData extends AbstractDO {
    private String campaignId;

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }
}
