package com.freecharge.freefund.dos;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.freecharge.common.framework.basedo.AbstractDO;

public class CreditRewardSuccessDetailsSNSDo extends AbstractDO {
	
    private String orderId;
    private Double amount;
    private String callerReference;
    private Calendar timeStamp;
    private String fundSource;

    public CreditRewardSuccessDetailsSNSDo() {
        this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }
    
    public CreditRewardSuccessDetailsSNSDo(String orderId,String callerReference,String fundSource,Double amount) {
    	this.orderId = orderId;
    	this.amount = amount;
    	this.callerReference = callerReference;
    	this.fundSource =fundSource;
    	this.timeStamp = Calendar.getInstance(TimeZone.getTimeZone("IST"));
    }
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public static String getJsonString(CreditRewardSuccessDetailsSNSDo creditRewardSuccessDetailsSNSDo) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(creditRewardSuccessDetailsSNSDo);
        return json;
    }

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCallerReference() {
		return callerReference;
	}

	public void setCallerReference(String callerReference) {
		this.callerReference = callerReference;
	}

	public Calendar getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Calendar timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getFundSource() {
		return fundSource;
	}

	public void setFundSource(String fundSource) {
		this.fundSource = fundSource;
	}
}
