package com.freecharge.freefund.dos.web;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class FreeFundCartWebDO extends BaseWebDO {
    private String status;
    private Integer txnHomePageId;
    private Integer cartId;
    private Integer couponId;
    private Integer productTypeId;
    private Integer cartItemsId;
    private Double totalPrice;
    private Double freeCouponAmount;
    private String promoEntity;
    
    public Double getTotalPrice() {
		return totalPrice;
	}
    
    public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTxnHomePageId() {
		return txnHomePageId;
	}

	public void setTxnHomePageId(Integer txnHomePageId) {
		this.txnHomePageId = txnHomePageId;
	}

	public Integer getCartId() {
		return cartId;
	}

	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public Integer getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(Integer productTypeId) {
		this.productTypeId = productTypeId;
	}

	public Integer getCartItemsId() {
		return cartItemsId;
	}

	public void setCartItemsId(Integer cartItemsId) {
		this.cartItemsId = cartItemsId;
	}

	public Double getFreeCouponAmount() {
		return freeCouponAmount;
	}

	public void setFreeCouponAmount(Double freeCouponAmount) {
		this.freeCouponAmount = freeCouponAmount;
	}

	public String getPromoEntity() {
		return promoEntity;
	}

	public void setPromoEntity(String promoEntity) {
		this.promoEntity = promoEntity;
	}
}
