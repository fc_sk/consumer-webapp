package com.freecharge.freefund.dos.web;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class FreefundRequestWebDO extends BaseWebDO {
    private String couponCode;
    private String productType;
    private String lid;
    private String promoId;
    private String amount;
    private String fcChannel;
    private String imeiNumber;
    private String advertisementId;
    private String deviceUniqueId;
    private String imsi;
    private String isEmulator;
    private String fcversion;
    private String captcha;

    public String getCouponCode() {
		return couponCode;
	}
    
    public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
    
    public String getProductType() {
		return productType;
	}
    
    public void setProductType(String productType) {
		this.productType = productType;
	}

    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxnChannel() {
        return fcChannel;
    }

    public String getFcChannel() {
        return fcChannel;
    }

    public void setFcChannel(String fcChannel) {
        this.fcChannel = fcChannel;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(String advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getIsEmulator() {
        return isEmulator;
    }

    public void setIsEmulator(String isEmulator) {
        this.isEmulator = isEmulator;
    }

    public String getAppFcVersion() {
        return fcversion;
    }

    public String getFcversion() {
        return fcversion;
    }

    public void setFcversion(String fcversion) {
        this.fcversion = fcversion;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
