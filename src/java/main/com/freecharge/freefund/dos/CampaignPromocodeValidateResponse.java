package com.freecharge.freefund.dos;


import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.freefund.dos.entity.FreefundCoupon;

public class CampaignPromocodeValidateResponse {
    private boolean status;
    private FreefundClass freefundClass;
    private FreefundCoupon freefundCoupon;

    public FreefundClass getFreefundClass() {
        return freefundClass;
    }

    public void setFreefundClass(FreefundClass freefundClass) {
        this.freefundClass = freefundClass;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public FreefundCoupon getFreefundCoupon() {
        return freefundCoupon;
    }

    public void setFreefundCoupon(FreefundCoupon freefundCoupon) {
        this.freefundCoupon = freefundCoupon;
    }
}
