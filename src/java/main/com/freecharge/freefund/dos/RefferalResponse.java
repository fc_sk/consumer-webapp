package com.freecharge.freefund.dos;

import java.io.Serializable;
import java.util.List;

public class RefferalResponse implements Serializable{
	private Double cashbackEarned;
	public Double getCashbackEarned() {
		return cashbackEarned;
	}
	public void setCashbackEarned(Double cashbackEarned) {
		this.cashbackEarned = cashbackEarned;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public List<String> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<String> orderList) {
		this.orderList = orderList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	private String referralCode;
	private List<String> orderList;
	private String status;
}