package com.freecharge.freefund.dos;


import com.freecharge.common.framework.basedo.AbstractDO;
import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public class PromocodeRedeemDetailsSNSDo extends AbstractDO {
    private String orderId;
    private String discountValue;
    private String promocode;
    private String callerReference;
    private String promoEntity;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public static String getJsonString(PromocodeRedeemDetailsSNSDo promocodeRedeemDetailsSNSDo) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(promocodeRedeemDetailsSNSDo);
        return json;
    }

    public String getCallerReference() {
        return callerReference;
    }

    public void setCallerReference(String callerReference) {
        this.callerReference = callerReference;
    }

    public String getPromoEntity() {
        return promoEntity;
    }

    public void setPromoEntity(String promoEntity) {
        this.promoEntity = promoEntity;
    }
}
