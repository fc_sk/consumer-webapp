package com.freecharge.freefund.dos.entity;


import com.freecharge.admin.helper.AdditionalRewardAdminHelper;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.HashMap;
import java.util.Map;

public class AdditionalReward extends AbstractDO {
    private int id;
    private String rewardName;
    private Float rewardValue;
    private String rewardDiscountType;
    private Float rewardMaxDiscount;
    private long rewardConditionId;
    private boolean isEnabled;

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public Float getRewardValue() {
        return rewardValue;
    }

    public void setRewardValue(Float rewardValue) {
        this.rewardValue = rewardValue;
    }

    public String getRewardDiscountType() {
        return rewardDiscountType;
    }

    public void setRewardDiscountType(String rewardDiscountType) {
        this.rewardDiscountType = rewardDiscountType;
    }

    public Float getRewardMaxDiscount() {
        return rewardMaxDiscount;
    }

    public void setRewardMaxDiscount(Float rewardMaxDiscount) {
        this.rewardMaxDiscount = rewardMaxDiscount;
    }

    public long getRewardConditionId() {
        return rewardConditionId;
    }

    public void setRewardConditionId(long rewardConditionId) {
        this.rewardConditionId = rewardConditionId;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.REWARD_NAME, this.rewardName);
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.REWARD_VALUE, this.rewardValue);
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.REWARD_DISCOUNT_TYPE, this.rewardDiscountType);
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.REWARD_MAX_DISCOUNT, this.rewardMaxDiscount);
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.REWARD_CONDITION_ID, this.rewardConditionId);
        mapSqlParameterSource.addValue(AdditionalRewardAdminHelper.IS_ENABLED, true);
        return mapSqlParameterSource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Map<String, Object> validate(String rewardName, 
                                        String rewardValue,
                                        String rewardDiscountType, 
                                        String rewardMaxDiscount,
                                        String rewardConditionId) {

        Map<String, Object> resultMap = new HashMap<>();

        if (FCUtil.isEmpty(rewardName)) {
            resultMap.put(AdditionalRewardAdminHelper.MESSAGE, "Reward Name Invalid");
        }

        if (!FCUtil.isFloat(rewardValue)) {
            resultMap.put(AdditionalRewardAdminHelper.MESSAGE, 
                    resultMap.get(AdditionalRewardAdminHelper.MESSAGE) + " Invalid reward value");
        }

        if (!FCUtil.isFloat(rewardMaxDiscount)) {
            resultMap.put(AdditionalRewardAdminHelper.MESSAGE,
                    resultMap.get(AdditionalRewardAdminHelper.MESSAGE) + " Invalid max discount");
        }

        if (FCUtil.isEmpty(rewardDiscountType)) {
            resultMap.put(AdditionalRewardAdminHelper.MESSAGE,
                    resultMap.get(AdditionalRewardAdminHelper.MESSAGE) + " Invalid discount type");
        }

        if (!FCUtil.isInteger(rewardConditionId)) {
            resultMap.put(AdditionalRewardAdminHelper.MESSAGE,
                    resultMap.get(AdditionalRewardAdminHelper.MESSAGE) + " Invalid reward condition id");
        }
        
        return resultMap;
    }

    public Float getDiscountAmount(Float rechargeAmount) {
        if(FCConstants.DISCOUNT_TYPE_PERCENT.equals(rewardDiscountType)) {
            Float discountValue = (float) Math.round(rechargeAmount * rewardValue / 100);
            if(rewardMaxDiscount != null && discountValue > rewardMaxDiscount) {
                return rewardMaxDiscount;
            }
            return discountValue;
        }
        return rewardValue;
    }
}
