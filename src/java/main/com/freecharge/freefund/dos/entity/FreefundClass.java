package com.freecharge.freefund.dos.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "freefund_class")
public class FreefundClass implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String ruleExpression;
    private String javaClass;
    private Boolean isActive;
    private Boolean before;
    private Boolean after;
    private Boolean oneTimeUse;
    private Integer returnType;
    private Date startDate;
    private Date endDate;
    private Date createdOn;
    private Date modifiedOn;

    public FreefundClass() {
    }

    public FreefundClass(Long id, String name, String description, String ruleExpression,
                         String javaClass, Boolean active, Boolean before, Boolean after,
                         Boolean oneTimeUse, Integer returnType, Date startDate, Date endDate,
                         Date createdOn, Date modifiedOn) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ruleExpression = ruleExpression;
        this.javaClass = javaClass;
        isActive = active;
        this.before = before;
        this.after = after;
        this.oneTimeUse = oneTimeUse;
        this.returnType = returnType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdOn = createdOn;
        this.modifiedOn = modifiedOn;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "freefund_class_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }

    @Column(name="rule_expression")
    public String getRuleExpression() {
        return ruleExpression;
    }

    @Column(name="freefund_class")
    public String getJavaClass() {
        return javaClass;
    }

    @Column(name="is_active")
    public Boolean getActive() {
        return isActive;
    }

    @Column(name="before")
    public Boolean getBefore() {
        return before;
    }

    @Column(name="after")
    public Boolean getAfter() {
        return after;
    }

    @Column(name="one_time_use")
    public Boolean getOneTimeUse() {
        return oneTimeUse;
    }

    @Column(name="return_type")
    public Integer getReturnType() {
        return returnType;
    }

    @Column(name="effective_start_date")
    public Date getStartDate() {
        return startDate;
    }

    @Column(name="effective_end_date")
    public Date getEndDate() {
        return endDate;
    }

    @Column(name="create_date")
    public Date getCreatedOn() {
        return createdOn;
    }

    @Column(name="modified_date")
    public Date getModifiedOn() {
        return modifiedOn;
    }


  // setters .......................

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRuleExpression(String ruleExpression) {
        this.ruleExpression = ruleExpression;
    }

    public void setJavaClass(String javaClass) {
        this.javaClass = javaClass;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setBefore(Boolean before) {
        this.before = before;
    }

    public void setAfter(Boolean after) {
        this.after = after;
    }

    public void setOneTimeUse(Boolean oneTimeUse) {
        this.oneTimeUse = oneTimeUse;
    }

    public void setReturnType(Integer returnType) {
        this.returnType = returnType;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }
}
