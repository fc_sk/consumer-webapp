package com.freecharge.freefund.dos.entity;

import com.freecharge.promo.util.PromocodeConstants;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class ReIssuedPromocode {
    private int id;
    private String orderId;
    private long reIssuedPromocodeId;
    private long parentPromocodeId;
    private int parentFreefundClassId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public long getReIssuedPromocodeId() {
        return reIssuedPromocodeId;
    }

    public void setReIssuedPromocodeId(long reIssuedPromocodeId) {
        this.reIssuedPromocodeId = reIssuedPromocodeId;
    }

    public long getParentPromocodeId() {
        return parentPromocodeId;
    }

    public void setParentPromocodeId(long parentPromocodeId) {
        this.parentPromocodeId = parentPromocodeId;
    }

    public int getParentFreefundClassId() {
        return parentFreefundClassId;
    }

    public void setParentFreefundClassId(int parentFreefundClassId) {
        this.parentFreefundClassId = parentFreefundClassId;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(PromocodeConstants.ORDER_ID, this.orderId);
        mapSqlParameterSource.addValue(PromocodeConstants.REISSUED_PROMOCODE_ID, this.reIssuedPromocodeId);
        mapSqlParameterSource.addValue(PromocodeConstants.PARENT_PROMOCODE_ID, this.parentPromocodeId);
        mapSqlParameterSource.addValue(PromocodeConstants.PARENT_FREEFUND_CLASS_ID, this.parentFreefundClassId);
        return mapSqlParameterSource;
    }
}
