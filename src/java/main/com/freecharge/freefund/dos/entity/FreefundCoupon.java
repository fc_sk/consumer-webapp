package com.freecharge.freefund.dos.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.freecharge.common.util.FCConstants;

@Entity
@Table(name = "freefund_coupon")
public class FreefundCoupon  implements Serializable {

    private Long id;
    private Long freefundClassId;
    private String freefundCode;
    private Date generatedDate;
    private Date usedDate;
    private String usedEmailId;
    private String usedServiceNumber;
    private Float freefundValue;
    private Float minRechargeValue;
    private String promoEntity;
    private String status;
    
    public FreefundCoupon() {
    }

    public FreefundCoupon(Long id, Long freefundClassId, String freefundCode,
                          Date generatedDate, Date usedDate, String usedEmailId,
                          String usedServiceNumber, String status) {
        this.id = id;
        this.freefundClassId = freefundClassId;
        this.freefundCode = freefundCode;
        this.generatedDate = generatedDate;
        this.usedDate = usedDate;
        this.usedEmailId = usedEmailId;
        this.usedServiceNumber = usedServiceNumber;
        this.status = status;
    }

    public FreefundCoupon(Long freefundClassId, String code, Float freefundValue, Float minRechargeValue, String promoEntity) {
        this.freefundClassId = freefundClassId;
        this.freefundCode = code;
        this.freefundValue = freefundValue;
        this.minRechargeValue = minRechargeValue;
        this.promoEntity = promoEntity;
    }

    public Double getDiscountValue(Double rechargAmount, String discountType, Float maxDiscount) {
        if(FCConstants.DISCOUNT_TYPE_PERCENT.equals(discountType)) {    
            Double discountValue = new Double(Math.round(rechargAmount * freefundValue / 100));
            if(maxDiscount != null && discountValue > maxDiscount) {
                    return new Double(maxDiscount);
            }
            return discountValue;
        }
        return new Double(freefundValue.toString());
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "freefund_coupon_id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    @Column(name="freefund_class_id")
    public Long getFreefundClassId() {
        return freefundClassId;
    }

    @Column(name="freefund_code")
    public String getFreefundCode() {
        return freefundCode;
    }

    @Column(name="generated_date")
    public Date getGeneratedDate() {
        return generatedDate;
    }

    @Column(name="used_date")
    public Date getUsedDate() {
        return usedDate;
    }

    @Column(name="used_email_id")
    public String getUsedEmailId() {
        return usedEmailId;
    }

    @Column(name="used_service_number")
    public String getUsedServiceNumber() {
        return usedServiceNumber;
    }
    
    @Column(name="freefund_value")
    public Float getFreefundValue() {
		return freefundValue;
	}
    
    @Column(name="min_recharge_value")
	public Float getMinRechargeValue() {
		return minRechargeValue;
	}
    
    @Column(name="promo_entity")
	public String getPromoEntity() {
		return promoEntity;
	}
	
    // setters ...................

    public void setId(Long id) {
        this.id = id;
    }

    public void setFreefundClassId(Long freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public void setFreefundCode(String freefundCode) {
        this.freefundCode = freefundCode;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    public void setUsedDate(Date usedDate) {
        this.usedDate = usedDate;
    }

    public void setUsedEmailId(String usedEmailId) {
        this.usedEmailId = usedEmailId;
    }

    public void setUsedServiceNumber(String usedServiceNumber) {
        this.usedServiceNumber = usedServiceNumber;
    }

	public void setFreefundValue(Float freefundValue) {
		this.freefundValue = freefundValue;
	}

	public void setMinRechargeValue(Float minRechargeValue) {
		this.minRechargeValue = minRechargeValue;
	}

	public void setPromoEntity(String promoEntity) {
		this.promoEntity = promoEntity;
	}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
