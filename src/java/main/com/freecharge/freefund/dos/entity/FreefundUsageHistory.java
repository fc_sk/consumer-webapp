package com.freecharge.freefund.dos.entity;

import java.util.Comparator;
import java.util.Date;
/*
 * 
 * An instance of this class represents one Freefund usage item. A list of these is stored to help us make fraud checks.
 * 
 * @author raviande
 */
public class FreefundUsageHistory implements Comparator<FreefundUsageHistory> {

    private Integer           freefundClassId;
    private String            code;
    private String            action;
    private Date              timestamp;

    public Integer getFreefundClassId() {
        return this.freefundClassId;
    }

    public void setFreefundClassId(Integer freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int compare(FreefundUsageHistory o1, FreefundUsageHistory o2) {
        if ((o1.freefundClassId == o2.freefundClassId) && (o1.getCode().equals(o2.getCode()))) {
            return 0;
        } else {
            return -1;
        }
    }
}