package com.freecharge.freefund.dos.entity;

public enum FreefundAction {
    REDEEM, BLOCK, APPLY, UNBLOCK
}