package com.freecharge.freefund.dos.entity;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.freefund.constants.PostbackConstants;

/**
 * This entity has the details of a Apsalar event
 **/
public class ApsalarEventData extends AbstractDO {
    private String partner;
    private String eventName;
    private String time;
    private String appName;
    private String deviceId;
    private String deviceIp;
    private String deviceOS;
    private String oSId;
    private String revenueUSD;
    private String publisherSubPublisherName;
    private String campaignName;
    private String publisherSubAdgroupName;
    private String countryName;
    private String bundleId;
    private String creativeId;
    private String googleAid;
    private String sha1AndroidId;
    private String apsalarInternalDeviceId;
    private String fbCampaignId;
    private String fbCampaignName;
    private String fbAdSetId;
    private String fbAdSetName;
    private String fbAdId;
    private String fbAdName;
    private String twitterCampaignName;
    private String twitterCampaignId;
    private String twitterLineItemId;
    private String tweetId;

    public ApsalarEventData(Map<String, String> map) {
        this.time = map.get(PostbackConstants.DATETIME);
        this.appName = map.get(PostbackConstants.APP_NAME);
        this.partner = map.get(PostbackConstants.PUBLISHER);
        this.deviceId = map.get(PostbackConstants.DEVICE_ID);
        this.deviceIp = map.get(PostbackConstants.DEVICE_IP);
        this.deviceOS = map.get(PostbackConstants.DEVICE_OS);
        this.oSId = map.get(PostbackConstants.OS_ID);
        this.campaignName = map.get(PostbackConstants.CAMPAIGN_NAME);
        this.revenueUSD = map.get(PostbackConstants.REVENUE_USD);
        this.eventName = map.get(PostbackConstants.EVENT_NAME);
        this.publisherSubPublisherName = map.get(PostbackConstants.PUBLISHER_SUB_PUBLISHER_NAME);
        this.publisherSubAdgroupName = map.get(PostbackConstants.PUBLISHER_SUB_ADGROUP_NAME);
        this.countryName = map.get(PostbackConstants.COUNTRY_NAME);
        this.bundleId = map.get(PostbackConstants.BUNDLE_ID);
        this.creativeId = map.get(PostbackConstants.CREATIVE_ID);
        this.googleAid = map.get(PostbackConstants.GOOGLE_AID);
        this.sha1AndroidId = map.get(PostbackConstants.SHA1_ANDROID_ID);
        this.apsalarInternalDeviceId = map.get(PostbackConstants.APSALAR_INTERNAL_DEVICE_ID);
        this.fbCampaignId = map.get(PostbackConstants.FB_CAMPAIGN_ID);;
        this.fbCampaignName = map.get(PostbackConstants.FB_CAMPAIGN_NAME);
        this.fbAdSetId = map.get(PostbackConstants.FB_AD_SET_ID);
        this.fbAdSetName = map.get(PostbackConstants.FB_AD_SET_NAME);
        this.fbAdId = map.get(PostbackConstants.FB_AD_ID);
        this.fbAdName = map.get(PostbackConstants.FB_AD_NAME);
        this.twitterCampaignName = map.get(PostbackConstants.TWITTER_CAMPAIGN_NAME);
        this.twitterCampaignId = map.get(PostbackConstants.TWITTER_CAMPAIGN_ID);
        this.twitterLineItemId = map.get(PostbackConstants.TWITTER_LINE_ITEM_ID);
        this.tweetId = map.get(PostbackConstants.TWEET_ID);
    }

    public String getTime() {
        return time;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(final String appName) {
        this.appName = appName;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(final String partner) {
        this.partner = partner;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceIp() {
        return deviceIp;
    }

    public void setDeviceIp(final String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getOsId() {
        return oSId;
    }

    public void setOsId(final String oSId) {
        this.oSId = oSId;
    }

    public String getPublisherSubPublisherName() {
        return publisherSubPublisherName;
    }

    public void setPublisherSubPublisherName(final String publisherSubPublisherName) {
        this.publisherSubPublisherName = publisherSubPublisherName;
    }

    public String getPublisherSubAdgroupName() {
        return publisherSubAdgroupName;
    }

    public void setPublisherSubAdgroupName(final String publisherSubAdgroupName) {
        this.publisherSubAdgroupName = publisherSubAdgroupName;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(final String campaignName) {
        this.campaignName = campaignName;
    }

    public String getRevenueUSD() {
        return revenueUSD;
    }

    public void setRevenueUSD(final String revenueUSD) {
        this.revenueUSD = revenueUSD;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(final String eventName) {
        this.eventName = eventName;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(final String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(final String countryName) {
        this.countryName = countryName;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(String creativeId) {
        this.creativeId = creativeId;
    }

    public String getGoogleAid() {
        return googleAid;
    }

    public void setGoogleAid(String googleAid) {
        this.googleAid = googleAid;
    }

    public String getSha1AndroidId() {
        return sha1AndroidId;
    }

    public void setSha1AndroidId(String sha1AndroidId) {
        this.sha1AndroidId = sha1AndroidId;
    }

    public String getApsalarInternalDeviceId() {
        return apsalarInternalDeviceId;
    }

    public void setApsalarInternalDeviceId(String apsalarInternalDeviceId) {
        this.apsalarInternalDeviceId = apsalarInternalDeviceId;
    }

    public String getFbCampaignId() {
        return fbCampaignId;
    }

    public void setFbCampaignId(String fbCampaignId) {
        this.fbCampaignId = fbCampaignId;
    }

    public String getFbCampaignName() {
        return fbCampaignName;
    }

    public void setFbCampaignName(String fbCampaignName) {
        this.fbCampaignName = fbCampaignName;
    }

    public String getFbAdSetId() {
        return fbAdSetId;
    }

    public void setFbAdSetId(String fbAdSetId) {
        this.fbAdSetId = fbAdSetId;
    }

    public String getFbAdSetName() {
        return fbAdSetName;
    }

    public void setFbAdSetName(String fbAdSetName) {
        this.fbAdSetName = fbAdSetName;
    }

    public String getFbAdId() {
        return fbAdId;
    }

    public void setFbAdId(String fbAdId) {
        this.fbAdId = fbAdId;
    }

    public String getFbAdName() {
        return fbAdName;
    }

    public void setFbAdName(String fbAdName) {
        this.fbAdName = fbAdName;
    }

    public String getTwitterCampaignName() {
        return twitterCampaignName;
    }

    public void setTwitterCampaignName(String twitterCampaignName) {
        this.twitterCampaignName = twitterCampaignName;
    }

    public String getTwitterCampaignId() {
        return twitterCampaignId;
    }

    public void setTwitterCampaignId(String twitterCampaignId) {
        this.twitterCampaignId = twitterCampaignId;
    }

    public String getTwitterLineItemId() {
        return twitterLineItemId;
    }

    public void setTwitterLineItemId(String twitterLineItemId) {
        this.twitterLineItemId = twitterLineItemId;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("app_name", this.appName);
        mapSqlParameterSource.addValue("partner", this.partner);
        mapSqlParameterSource.addValue("device_id", this.deviceId);
        mapSqlParameterSource.addValue("device_ip", this.deviceIp);
        mapSqlParameterSource.addValue("device_os", this.deviceOS);
        mapSqlParameterSource.addValue("os_id", this.oSId);
        mapSqlParameterSource.addValue("campaign_name", this.campaignName);
        mapSqlParameterSource.addValue("revenue_usd", this.revenueUSD);
        mapSqlParameterSource.addValue("event_name", this.eventName);
        mapSqlParameterSource.addValue("event_time", this.time);
        mapSqlParameterSource.addValue("publisher_sub_publisher_name", this.publisherSubPublisherName);
        mapSqlParameterSource.addValue("publisher_sub_adgroup_name", this.publisherSubAdgroupName);
        mapSqlParameterSource.addValue("country_name", this.countryName);
        mapSqlParameterSource.addValue("bundle_id", this.bundleId);
        mapSqlParameterSource.addValue("creative_id", this.creativeId);
        mapSqlParameterSource.addValue("google_aid", this.googleAid);
        mapSqlParameterSource.addValue("sha1_android_id", this.sha1AndroidId);
        mapSqlParameterSource.addValue("apsalar_internal_device_id", this.apsalarInternalDeviceId);
        mapSqlParameterSource.addValue("fb_campaign_id", this.fbCampaignId);
        mapSqlParameterSource.addValue("fb_campaign_name", this.fbCampaignName);
        mapSqlParameterSource.addValue("fb_ad_set_id", this.fbAdSetId);
        mapSqlParameterSource.addValue("fb_ad_set_name", this.fbAdSetName);
        mapSqlParameterSource.addValue("fb_ad_id", this.fbAdId);
        mapSqlParameterSource.addValue("fb_ad_name", this.fbAdName);
        mapSqlParameterSource.addValue("twitter_campaign_name", this.twitterCampaignName);
        mapSqlParameterSource.addValue("twitter_campaign_id", this.twitterCampaignId);
        mapSqlParameterSource.addValue("twitter_line_item_id", this.twitterLineItemId);
        mapSqlParameterSource.addValue("tweet_id", this.tweetId);

        return mapSqlParameterSource;
     }
}
