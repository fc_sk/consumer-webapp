package com.freecharge.freefund.dos.entity;


import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class PromocodeRedeemedDetails {

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("order_id", this.orderId);
        mapSqlParameterSource.addValue("campaign_name", this.name);
        mapSqlParameterSource.addValue("campaign_id", this.freefundClassId);
        mapSqlParameterSource.addValue("discount_type", this.discountType);
        mapSqlParameterSource.addValue("discount_value", this.discountValue);
        mapSqlParameterSource.addValue("promo_entity", this.promoEntity);
        mapSqlParameterSource.addValue("freefund_value", this.freefundValue);
        mapSqlParameterSource.addValue("freefund_code", this.freefundCode);
        mapSqlParameterSource.addValue("user_id", this.userId);
        mapSqlParameterSource.addValue("operator_name", this.operatorName);
        mapSqlParameterSource.addValue("circle_name", this.circleName);
        mapSqlParameterSource.addValue("recharge_amount", this.amount);
        mapSqlParameterSource.addValue("service_number", this.serviceNumber);
        mapSqlParameterSource.addValue("channel_id", this.channelId);
        mapSqlParameterSource.addValue("product_type", this.productType);
        mapSqlParameterSource.addValue("is_coupon_opted", this.isCouponOpted);
        mapSqlParameterSource.addValue("imei_number", this.imeiNumber);
        mapSqlParameterSource.addValue("advertisement_id", this.advertisementId);
        mapSqlParameterSource.addValue("imsi", this.imsi);
        mapSqlParameterSource.addValue("is_emulator", this.isEmulator);
        return mapSqlParameterSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Long getFreefundClassId() {
        return freefundClassId;
    }

    public void setFreefundClassId(Long freefundClassId) {
        this.freefundClassId = freefundClassId;
    }

    public String getPromoEntity() {
        return promoEntity;
    }

    public void setPromoEntity(String promoEntity) {
        this.promoEntity = promoEntity;
    }

    public Float getFreefundValue() {
        return freefundValue;
    }

    public void setFreefundValue(Float freefundValue) {
        this.freefundValue = freefundValue;
    }

    public String getFreefundCode() {
        return freefundCode;
    }

    public void setFreefundCode(String freefundCode) {
        this.freefundCode = freefundCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Double discountValue) {
        this.discountValue = discountValue;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getProductName() {
        return productType;
    }

    public void setProductName(String productName) {
        this.productType = productName;
    }

    public int isCouponOpted() {
        return isCouponOpted;
    }

    public void setCouponOpted(int isCouponOpted) {
        this.isCouponOpted = isCouponOpted;
    }

    public String getImeiNumber (){
        return imeiNumber;
    }

    public void setImeiNumber (String imeiNumber){
        this.imeiNumber = imeiNumber;
    }

    public String getAdvertisementId () {
        return advertisementId;
    }

    public void setAdvertisementId (String advertisementId) {
        this.advertisementId = advertisementId;
    }

    public String getImsi (){
        return imsi;
    }

    public void setImsi (String imsi){
        this.imsi = imsi;
    }

    public Boolean getIsEmulator (){
        return isEmulator;
    }

    public void setIsEmulator (Boolean isEmulator){
        this.isEmulator = isEmulator;
    }

    private String name;
    private Long freefundClassId;
    private String discountType;
    private String promoEntity;
    private Float freefundValue;
    private String freefundCode;
    private Integer userId;
    private String orderId;
    private Double discountValue;
    private String operatorName;
    private String circleName;
    private String amount;
    private String serviceNumber;
    private Integer channelId;
    private String productType;
    private int isCouponOpted;
    private String imeiNumber;
    private String advertisementId;
    private String deviceUniqueId;
    private String imsi;
    private Boolean isEmulator;

    @Override
    public String toString() {
        return "PromocodeRedeemDetails {" +
                "CampaignName=" + name +
                ", FreefundclassId=" + freefundClassId +
                ", DiscountType=" + discountType +
                ", PromoEntity=" + promoEntity +
                ", FreefundValue=" + freefundValue +
                ", FreefundCode=" + freefundCode +
                ", UserId=" + userId +
                ", orderId=" + orderId +
                ", discountValue=" + discountValue +
                ", operatorName=" + operatorName +
                ", circleName=" + circleName +
                ", amount=" + amount +
                ", serviceNumber=" + serviceNumber +
                ", channelId=" + channelId +
                ", productType=" + productType +
                ", isCouponOpted=" + isCouponOpted +
                ", IMSI=" + imsi;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }
}
