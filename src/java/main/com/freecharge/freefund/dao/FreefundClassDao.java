package com.freecharge.freefund.dao;

import java.util.List;

import org.hibernate.Query;

import com.freecharge.app.domain.dao.CommonDao;
import com.freecharge.freefund.dos.entity.FreefundClass;

public class FreefundClassDao extends CommonDao<FreefundClass> implements IFreefundClassDao {

    protected Class<FreefundClass> getDomainClass() {
        return FreefundClass.class;
    }

    // this should return available coupon and not used one.
    public FreefundClass getClass(Long classId) {
        return this.read(classId);
    }

//    private Long id;
//    private String name;
//    private String description;
//    private String ruleExpression;
//    private String javaClass;
//    private Boolean isActive;
//    private Boolean before;
//    private Boolean after;
//    private Boolean oneTimeUse;
//    private Integer returnType;
//    private Date startDate;
//    private Date endDate;
//    private Date createdOn;
//    private Date modifiedOn;
    
    public FreefundClass getValidFreefundClass(Long freefundClassId){
        logger.info("finding all Crosssell instances",null);
            try {
                final String queryString = "select model from FreefundClass model " +
                        "where model.isActive = :active and model.id = :id " +
                        "and model.startDate <= current_date() " +
                        "and model.endDate >= current_date() ";
                Query query = getSession().createQuery(queryString);
                query.setParameter("id", freefundClassId);
                query.setParameter("active", true);
                List list = query.list();
                if (list != null && list.size() >0){
                    return (FreefundClass)list.get(0);
                }
                return null;
            } catch (RuntimeException re) {
                logger.info("find all failed",re);
                throw re;
            }
    }
}
