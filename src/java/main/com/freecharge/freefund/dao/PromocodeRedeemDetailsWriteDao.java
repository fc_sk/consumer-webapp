package com.freecharge.freefund.dao;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.PromocodeRedeemedDetails;

@Service
public class PromocodeRedeemDetailsWriteDao {

    private static Logger logger = LoggingFactory.getLogger(PromocodeRedeemDetailsWriteDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertPromocodeRedeemDetails;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPromocodeRedeemDetails = new SimpleJdbcInsert(dataSource).withTableName(
                "promocode_redeem_details").usingColumns("order_id", "campaign_name", "campaign_id","discount_type",
                "discount_value", "promo_entity", "freefund_value", "freefund_code",
                "user_id", "operator_name", "circle_name", "recharge_amount", "service_number",
                "channel_id", "product_type", "is_coupon_opted", "imei_number", "advertisement_id", "windows_device_unique_id",
                "imsi", "is_emulator");
    }

    public int insert(PromocodeRedeemedDetails promocodeRedeemedDetailsDO) {
        logger.debug("Inserting into PromocodeRedeemDetails table for " + promocodeRedeemedDetailsDO.getOrderId());
        SqlParameterSource parameters = promocodeRedeemedDetailsDO.getMapSqlParameterSource();
        Number returnParam = this.insertPromocodeRedeemDetails.execute(parameters);
        return returnParam.intValue();
    }
}
