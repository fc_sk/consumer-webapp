package com.freecharge.freefund.dao;

import com.freecharge.admin.helper.AdditionalRewardAdminHelper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Service
public class AdditionalRewardWriteDao {

    private Logger logger = LoggingFactory.getLogger(AdditionalRewardWriteDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertAdditionalReward;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertAdditionalReward = new SimpleJdbcInsert(dataSource).
                withTableName("additional_reward").usingGeneratedKeyColumns("id");
    }

    public int create(AdditionalReward doubleReward) {
        SqlParameterSource parameters = doubleReward.getMapSqlParameterSource();
        Number returnParam = this.insertAdditionalReward.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }

    public int edit(AdditionalReward additionalReward) {
        try {
            String sql = "update additional_reward set rewardName=:rewardName, rewardValue=:rewardValue, rewardDiscountType" +
                    "=:rewardDiscountType, rewardMaxDiscount=:rewardMaxDiscount," +
                    "rewardConditionId=:rewardConditionId where id=:id";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put(AdditionalRewardAdminHelper.REWARD_NAME, additionalReward.getRewardName());
            paramMap.put(AdditionalRewardAdminHelper.REWARD_VALUE, additionalReward.getRewardValue());
            paramMap.put(AdditionalRewardAdminHelper.REWARD_DISCOUNT_TYPE, additionalReward.getRewardDiscountType());
            paramMap.put(AdditionalRewardAdminHelper.REWARD_MAX_DISCOUNT, additionalReward.getRewardMaxDiscount());
            paramMap.put(AdditionalRewardAdminHelper.REWARD_CONDITION_ID, additionalReward.getRewardConditionId());
            paramMap.put("id", additionalReward.getId());

            return this.jdbcTemplate.update(sql, paramMap);
        } catch (RuntimeException e) {
            logger.error("Error updating additional reward for " + additionalReward.getRewardName(), e);
            throw e;
        }
    }

    public boolean disableAdditionalReward(int rewardId) {
        try {
            String sql = "update additional_reward set isEnabled=false where id=:id";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("id", rewardId);
            int count = this.jdbcTemplate.update(sql, paramMap);
            if (count == 1) {
                return true;
            }
        } catch (RuntimeException e) {
            logger.error("Error disabling additional reward for " + rewardId, e);
            throw e;
        }
        return false;
    }
}
