package com.freecharge.freefund.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.freefund.dos.entity.ApsalarEventData;

/**
 * DAO for ApsalarEventData
 **/
@Service
public class ApsalarEventDataWriteDao {

    private SimpleJdbcInsert insertApsalarEventData;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.insertApsalarEventData =
            new SimpleJdbcInsert(dataSource).withTableName("apsalar_event_data").
                                             usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public int insert(final ApsalarEventData apsalarEventData) {
        SqlParameterSource parameters = apsalarEventData.getMapSqlParameterSource();
        Number returnParam = this.insertApsalarEventData.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }
}
