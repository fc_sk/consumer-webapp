package com.freecharge.freefund.dao;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.ReIssuedPromocode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class ReIssuedPromocodesWriteDao {
    private static Logger logger = LoggingFactory.getLogger(ReIssuedPromocodesWriteDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertReIssuedPromocodeDetails;

    @Autowired
    @Qualifier("promocodeJdbcWriteDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertReIssuedPromocodeDetails = new SimpleJdbcInsert(dataSource).withTableName(
                "reissued_promocodes").usingGeneratedKeyColumns("id" ,"n_last_updated", "n_created");
    }

    public int insert(ReIssuedPromocode reIssuedPromocode) {
        logger.debug("Inserting into ReIssuedPromocodes table for " + reIssuedPromocode.getOrderId());
        SqlParameterSource parameters = reIssuedPromocode.getMapSqlParameterSource();
        Number returnParam = this.insertReIssuedPromocodeDetails.execute(parameters);
        return returnParam.intValue();
    }
}
