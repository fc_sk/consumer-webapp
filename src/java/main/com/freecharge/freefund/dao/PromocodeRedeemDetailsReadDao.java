package com.freecharge.freefund.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.PromocodeRedeemDetailsRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.PromocodeRedeemedDetails;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PromocodeRedeemDetailsReadDao {
    private static Logger logger = LoggingFactory.getLogger(PromocodeRedeemDetailsReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


    public List<PromocodeRedeemedDetails> getPromocodeRedeemDetailsForOrderId(String orderId) {
        List<PromocodeRedeemedDetails> diwaliOfferDOList = new ArrayList<>();
        try{
            String queryString = "select * from promocode_redeem_details where order_id=:orderId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("orderId", orderId);

            diwaliOfferDOList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRedeemDetailsRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch promocode redeem details data for order id " + orderId, e);
        }
        return diwaliOfferDOList;
    }
    
    /*Getting promocode redeem details*/
    public List<PromocodeRedeemedDetails> getPromocodeRedeemDetails(long userId, int campaignId) {
        List<PromocodeRedeemedDetails> promocodeRedeemedList = new ArrayList<>();
        try {
        	String queryString = "select * from promocode_redeem_details where user_id=:userid and campaign_id=:campaignid order by created_at DESC limit 10";
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("userid", userId);
            paramMap.put("campaignid", campaignId);   
            promocodeRedeemedList = this.jdbcTemplate.query(queryString, paramMap, new PromocodeRedeemDetailsRowMapper());
        } catch (RuntimeException re) {
        	logger.error("Failed to fetch promocode redeem details data for email and campaignid : " + userId + "," + campaignId, re);
        } 
        return promocodeRedeemedList;
    }
}
