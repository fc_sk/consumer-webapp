package com.freecharge.freefund.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.ReIssuedPromocodesRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.ReIssuedPromocode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReIssuedPromocodesReadDao {
    private static Logger logger = LoggingFactory.getLogger(ReIssuedPromocodesReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("promocodeJdbcReadDataSource")
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<ReIssuedPromocode> getReIssuedPromocodeForOrderId(String orderId) {
        List<ReIssuedPromocode> reIssuedPromocodeList = new ArrayList<>();
        try{
            String queryString = "select id,order_id,reissued_promocode_id,parent_promocode_id," +
                    "parent_freefund_class_id from reissued_promocodes where order_id=:orderId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("orderId", orderId);

            reIssuedPromocodeList = this.jdbcTemplate.query(queryString, paramMap, new ReIssuedPromocodesRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch reIssued promocode details for order id " + orderId, e);
        }
        return reIssuedPromocodeList;
    }

    public List<ReIssuedPromocode> getReIssuedPromocodeForParentPromocodeId(Long parentPromocodeId) {
        List<ReIssuedPromocode> reIssuedPromocodeList = new ArrayList<>();
        try{
            String queryString = "select id,order_id,reissued_promocode_id,parent_promocode_id,parent_freefund_class_id " +
                    " from reissued_promocodes where parent_promocode_id=:parentPromocodeId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("orderId", parentPromocodeId);

            reIssuedPromocodeList = this.jdbcTemplate.query(queryString, paramMap, new ReIssuedPromocodesRowMapper());

        } catch (DataAccessException e){
            logger.error("Failed to fetch reIssued promocode details for parent promocode id " + parentPromocodeId, e);
        }
        return reIssuedPromocodeList;
    }
}
