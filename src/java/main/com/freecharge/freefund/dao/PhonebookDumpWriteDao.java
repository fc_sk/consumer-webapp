package com.freecharge.freefund.dao;

import com.freecharge.freefund.dos.entity.PhonebookDumpDO;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;


public class PhonebookDumpWriteDao {
    private SimpleJdbcInsert insertPhonebookDumpDO;

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        this.insertPhonebookDumpDO =
                new SimpleJdbcInsert(dataSource).withTableName("phonebook_dump").
                        usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public int insert(final PhonebookDumpDO phonebookDumpDO) {
        SqlParameterSource parameters = phonebookDumpDO.getMapSqlParameterSource();
        Number returnParam = this.insertPhonebookDumpDO.executeAndReturnKey(parameters);
        return returnParam.intValue();
    }
}
