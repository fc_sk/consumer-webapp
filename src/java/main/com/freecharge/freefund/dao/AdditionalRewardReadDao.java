package com.freecharge.freefund.dao;

import com.freecharge.app.domain.entity.jdbc.mappers.AdditionalRewardRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdditionalRewardReadDao {
    private static Logger logger = LoggingFactory.getLogger(AdditionalRewardReadDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<AdditionalReward> getById(int rewardId) {
        List<AdditionalReward> additionalRewardList = new ArrayList<>();
        try{
            String queryString = "select id,rewardName,rewardConditionId,rewardValue,rewardMaxDiscount," +
                    "rewardDiscountType,isEnabled from additional_reward where id=:rewardId";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("rewardId", rewardId);

            additionalRewardList = this.jdbcTemplate.query(queryString, paramMap, new AdditionalRewardRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch additional rewards data for id " + rewardId, e);
        }
        return additionalRewardList;
    }

    public List<AdditionalReward> getByRewardName(String rewardName) {
        List<AdditionalReward> additionalRewardList = new ArrayList<>();
        try{
            String queryString = "select id,rewardName,rewardConditionId,rewardValue,rewardMaxDiscount," +
                    "rewardDiscountType,isEnabled from additional_reward where rewardName=:rewardName";

            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("rewardName", rewardName);

            additionalRewardList = this.jdbcTemplate.query(queryString, paramMap, new AdditionalRewardRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch additional rewards data for id " + rewardName, e);
        }
        return additionalRewardList;
    }

    public List<AdditionalReward> getAllActiveAdditionalRewards() {
        List<AdditionalReward> additionalRewardList = new ArrayList<>();
        try{
            String queryString = "select id,rewardName,rewardConditionId,rewardValue,rewardMaxDiscount," +
                    "rewardDiscountType,isEnabled from additional_reward where isEnabled=1";

            Map<String, Object> paramMap = new HashMap<>();

            additionalRewardList = this.jdbcTemplate.query(queryString, paramMap, new AdditionalRewardRowMapper());

        } catch (Exception e){
            logger.error("Failed to fetch additional rewards data " , e);
        }
        return additionalRewardList;
    }
}
