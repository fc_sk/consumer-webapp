package com.freecharge.freefund.dao;

import com.freecharge.app.domain.dao.ICommonDao;
import com.freecharge.freefund.dos.entity.FreefundClass;

public interface IFreefundClassDao extends ICommonDao<FreefundClass> {

    // this should return available coupon and not used one.
    public FreefundClass getValidFreefundClass(Long freefundClassId);


}
