package com.freecharge.autopay.clientdo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutoPaySchedule {

    private long            scheduleId;
    private int             userId;
    private int             frequencyInDays;
    private boolean         active;
    private String          nextTriggerDate;
    private String          productType;
    private RechargeDetails rechargeDetails;
    private BillPayDetails  billPayDetails;
    private boolean         notificationEnabled;
    private String          creationDate;
    private List<String>    timeLine;
    private String          orderId;

    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFrequencyInDays() {
        return frequencyInDays;
    }

    public void setFrequencyInDays(int frequencyInDays) {
        this.frequencyInDays = frequencyInDays;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean isActive) {
        this.active = isActive;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public RechargeDetails getRechargeDetails() {
        return rechargeDetails;
    }

    public void setRechargeDetails(RechargeDetails rechargeDetails) {
        this.rechargeDetails = rechargeDetails;
    }

    public BillPayDetails getBillPayDetails() {
        return billPayDetails;
    }

    public void setBillPayDetails(BillPayDetails billPayDetails) {
        this.billPayDetails = billPayDetails;
    }

    public boolean isNotificationEnabled() {
        return notificationEnabled;
    }

    public void setNotificationEnabled(boolean notificationEnabled) {
        this.notificationEnabled = notificationEnabled;
    }

    public ProductName getProductName() {
        return ProductName.fromPrimaryProductType(productType);
    }

    public String getNextTriggerDate() {
        return nextTriggerDate;
    }

    public void setNextTriggerDate(String nextTriggerDate) {
        this.nextTriggerDate = nextTriggerDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public List<String> getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(List<String> timeLine) {
        this.timeLine = timeLine;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
