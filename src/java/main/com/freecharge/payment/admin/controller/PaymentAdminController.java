package com.freecharge.payment.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.ActivityLogger;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.RefundResponse;

@Controller
@RequestMapping("/admin/paymentAdminController/*")
public class PaymentAdminController extends BaseAdminController {
	
	private static final String  PAYMENT_MTXN_ID = "paymentTxnId";
	private static final String  RESULT = "result";
    private static final String  AMOUNT_TO_BANK   = "amountToBank";
    private static final String  BANK_REFUND_MODE = "bankRefundMode";
	
	@Autowired
	private PaymentTransactionService paymentTransactionService;
		
	@Autowired
    PaymentRefund paymentRefund;
	
	@Autowired
	WalletService walletService;
	
    @Autowired
    private AdminHelper              adminHelper;
    
    @Autowired
    private CTActionControlService ctActionControlService;
	
	private final Logger logger = Logger.getLogger(PaymentAdminController.class);
	
	@RequestMapping(value = "statusRequest", method = RequestMethod.GET)
	public String getRequestForPaymentStatus(@RequestParam Map<String,String> mapping, Model model, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");
		String paymentTxnId = mapping.get(PaymentAdminController.PAYMENT_MTXN_ID);
		logger.info(ActivityLogger.PGStatus + "initiated by "+ getCurrentAdminUser(request));
		try {
		PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(Integer.parseInt(paymentTxnId));
		if(paymentTransaction!=null) {
			PaymentTransaction paymentTxnResponse =  paymentTransactionService.getPaymentStatus(paymentTransaction);
			if(paymentTxnResponse!=null) {
				if(paymentTxnResponse.getIsSuccessful()==true) {
					model.addAttribute("PAYMENT_STATUS", "Payment status is Successful");
				} else {
					model.addAttribute("PAYMENT_STATUS", "Payment status is Fail");
				}
			
			} else {
				model.addAttribute("PAYMENT_STATUS", "Sorry Payment Txn not found");
			}
		}
		} catch(Exception exception) {
			logger.error(" exception occured for payment status check for customer trail for paymentTxnId " +  paymentTxnId, exception);
			model.addAttribute("PAYMENT_STATUS", "Exception Occured");
		}
		return "jsonView";
		
	}
	
	@RequestMapping(value="refundToWallet", method=RequestMethod.POST)
	public String requestForRefundtTowallet(@RequestParam Map<String, String> requestMap, Model model, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");
		String paymentTxnId = requestMap.get(PAYMENT_MTXN_ID);
	try {
		if (paymentTxnId!=null && !paymentTxnId.isEmpty()) {
			PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(Integer.parseInt(paymentTxnId));
				if (paymentTransaction!=null) {
				if (paymentTransaction.getIsSuccessful()==true) {
					PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doManualrefund(paymentTransaction, PaymentConstants.MANUAL_REFUND);
					if(paymentRefundBusinessDO.getStatus()==true) {
						model.addAttribute(RESULT, "Refunded successfully for " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
					} else {
						model.addAttribute(RESULT, "Refund Unsuccessful for " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
					}
				} else {
				 PaymentTransaction paymentTxnResponse =  paymentTransactionService.getPaymentStatus(paymentTransaction);
				 if (paymentTxnResponse!=null) {
						if (paymentTxnResponse.getIsSuccessful()==true) {
						PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doManualrefund(paymentTransaction, PaymentConstants.MANUAL_REFUND);
						if(paymentRefundBusinessDO.getStatus()==true) {
							model.addAttribute(RESULT, "Refunded successfully for " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
						} else {
							model.addAttribute(RESULT, "Refund Unsuccessful for " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
						}
					} else {
						model.addAttribute(RESULT, "PG status is Unsuccessful for Order Id");	
					}
				} else {
					model.addAttribute(RESULT, "Payment status is unsuccessful in Database and PG status check response is null");
				}
			} 
		} 		else {
					model.addAttribute(RESULT, "Payment transaction not found for Txn id");
				}
		} else {
			model.addAttribute(RESULT, "Please Enter correct Payment txn Id");
		}
	} catch (Exception exception) {
			logger.error(" Exception occured please try after some time ", exception);
			model.addAttribute(RESULT, "Exception ocurred Please Try after some Time for paymentTxnId " + paymentTxnId);
		}
		return "jsonView";
	}

    @RequestMapping(value = "refundToPgViaWallet", method = RequestMethod.GET)
    public String refundToPgViaWallet(@RequestParam Map<String, String> requestMap, Model model,
            HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        if (!isAuthorizedToUpdateCt(request)) {
        	logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel for bankrefund " + requestMap.get(PAYMENT_MTXN_ID));
        	return null;
        }

        if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)){
            get403HtmlResponse(response);
            return null;
        }
        
       String paymentTxnId = requestMap.get(PAYMENT_MTXN_ID);
        String amount = requestMap.get(AMOUNT_TO_BANK);
        String bankRefundMode = requestMap.get(BANK_REFUND_MODE);

        Double amountToBank = null;
        if ("partial".equals(bankRefundMode)) {
            if (amount == null || amount.isEmpty() || amount.trim().isEmpty()) {
                model.addAttribute(RESULT, "Amount required.");
                return "jsonView";
            }
            try {
                amountToBank = Double.parseDouble(amount);
            } catch (NumberFormatException e) {
                model.addAttribute(RESULT, "Amount format is wrong.");
                return "jsonView";
            }

            if (amountToBank <= 0) {
                model.addAttribute(RESULT, "Amount should not be negative or zero.");
                return "jsonView";
            }
        }
        
      /*Checking avail-credits of cs-agent to do refund */
        if (!ctActionControlService.hasSufficientCreditToDoCashBackOrRefund(getCurrentAdminUser(request), Double.parseDouble(amount))) {
            model.addAttribute(RESULT, getCurrentAdminUser(request) + " : cs-agent does not have sufficient credits to do any refund(or exception while checking available credits), pls contact manager.");
            return "jsonView";
        }
        
       Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
        logger.info("request for refundToPgViaWallet for payment request id " + paymentTxnId);
        if (paymentTxnId != null && !paymentTxnId.isEmpty()) {
            try {
                PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(Integer
                        .parseInt(paymentTxnId));
                if (paymentTransaction != null) {
                    if ("partial".equals(bankRefundMode)) {
                        if (amountToBank > paymentTransaction.getAmount()) {
                            model.addAttribute(RESULT, "Amount should not be greater than PG amount.");
                            return "jsonView";
                        }
                    }
                    if (paymentTransaction.getIsSuccessful() == true) {
                        if (!paymentTransaction.getPaymentGateway().equals(
                                PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                        	logger.info(ActivityLogger.ReversePGtoSource+" initiated by "+ getCurrentAdminUser(request));
                            RefundResponse refundResponse = walletService.refundToPgForPaymentMxtnId(paymentTransaction, amountToBank, PaymentConstants.CS_BANK_REFUND);
                            model.addAttribute(RESULT, refundResponse.getResponseMessage());
                            if (refundResponse.isSuccess()) {
                                logger.info("Bankrefund success from CT for orderId " + paymentTransaction.getOrderId() + "and ptxnId "+ paymentTransaction.getPaymentTxnId() + "by cs agent "+ getCurrentAdminUser(request));
                            	resultObjectMap.put(paymentTransaction.getMerchantTxnId(), adminHelper.getResultObjectListForEmail(paymentTransaction.getMerchantTxnId(), String.valueOf(amountToBank)));
                                /*Deduct available credits of cs-agent after successful refund*/
                                if (ctActionControlService.deductAvailableCreditLimits(getCurrentAdminUser(request), amountToBank)) {
                                    model.addAttribute(RESULT, refundResponse.getResponseMessage());
                                } else {
                                    logger.error("Exception while deducting cs-agent"+ getCurrentAdminUser(request) + "available-credits after bank-refund");
                                    model.addAttribute(RESULT, refundResponse.getResponseMessage()+ ": But Exception while deducting cs-agent's avail-credits, pls contact manager");
                                }
                                
                                /*Send a mail if cs-agent has crossed 90% of his max credits limit */
                                if (ctActionControlService.hasCrossedNinetyPercentOfCredits(getCurrentAdminUser(request))) {
                                    ctActionControlService.emailOnNinetyPercentCreditsReached(getCurrentAdminUser(request));
                                }
                            }
                        } else {
                            model.addAttribute(RESULT, "Sorry Payment Gateway is FCBalance We can not initiate Refund");
                        }
                    } else {
                        model.addAttribute(RESULT, "Sorry PG status is Unsuccessful");
                    }
                } else {
                    model.addAttribute(RESULT, "Sorry Payment Transaction not found");
                }
            } catch (Exception exception) {
                logger.error(" Exception occured please try after some time for paymentTxnId " + paymentTxnId,
                        exception);
                model.addAttribute(RESULT, "Exception occured please try after some time");
            }
        }
        if (resultObjectMap != null && resultObjectMap.size() > 0) {
            adminHelper.sendAlertEmail(resultObjectMap, "Refund to Bank from customertrail", getCurrentAdminUser(request));
        }
        return "jsonView";
    }
}








