package com.freecharge.payment.autorefund;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.hcoupon.service.HCouponFulfilmentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;

/**
 * A simple wrapper that sweeps through pending and failed payments
 * double checking if any of them succeeded.
 * @author arun
 *
 */
@Component
public class AutoRefundPaymentGatewayFailures {
    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(AutoRefundPaymentGatewayFailures.class.getName()));
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private AppConfigService appConfigService;
    
    @Autowired
    private PaymentRefund paymentRefund;

    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private AppConfigService appConfig;
    
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private PaymentTransactionService paymentService;
    
    @Autowired
    private HCouponFulfilmentService hCouponFulfilmentService;
    
    @Autowired
    UserTransactionHistoryService transactionHistoryService;
    
    public void sweep(Date startDate, Date endDate) throws Exception {
        //Get candidates which are in an unknown payment state
        List<PaymentTransaction> paymentTransactions =  paymentTransactionService.getPaymentTxnWithResNullWithTimeLimit(startDate, endDate);

        if (paymentTransactions == null) {
            return;
        }

        logger.info("Working on transaction for which we did not receive a response from PG - start");
        logger.info("Size of payment transactions:" + paymentTransactions.size());

        /*
         * Just sync the payment transaction; this is sufficient
         * since as part of syncing money will be moved to wallet so
         * there's no need to refund the transaction.
         */
        
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            try {
                paymentSyncTxn(paymentTransaction,false);
            } catch (Exception e) {
                logger.info("Caught exception on doing sweep for " + paymentTransaction, e);
            }
        }
    }
    
    public PaymentTransaction paymentSyncTxn(PaymentTransaction paymentTransaction,Boolean isFromAdminPanel) {
        try {
            logger.info("payment transaction " + paymentTransaction.toString());
            paymentTransactionService.syncfromPGData(paymentTransaction,isFromAdminPanel);

            PaymentTransaction updatedTransaction = paymentTransactionService.getPaymentTransactionById(paymentTransaction.getPaymentTxnId());
            logger.info("Updated payment transaction " + updatedTransaction.toString());
            
            logger.info("PG: " + updatedTransaction.getPaymentGateway());
            
            if (!PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(updatedTransaction.getPaymentGateway())
                    && updatedTransaction.getIsSuccessful() != null && updatedTransaction.getIsSuccessful()) {
                boolean enqueued = false;
                boolean alwaysRefundOnDelayedPaymentSuccess = appConfigService.isAlwaysRefundOnDelayedPaymentSuccess();
                if (!alwaysRefundOnDelayedPaymentSuccess && paymentTransactionService.canBeEnqueuedForRecharge(updatedTransaction)) {
                    logger.info("Potential order ID for recharging: [" + updatedTransaction.getOrderId() + "]. Enqueueing for recharge as transaction with mtxn id "+updatedTransaction.getMerchantTxnId()+" is successful.");
                    kestrelWrapper.enqueueRechargeAsync(updatedTransaction.getOrderId());
                    enqueued = true;
                } else if (!alwaysRefundOnDelayedPaymentSuccess && paymentService.canBeEnqueuedForHCouponFulfilment(updatedTransaction)) {
                    logger.info("Enqueueing for fulfilment as transaction with mtxn id "+updatedTransaction.getMerchantTxnId()+" is successful.");
                    hCouponFulfilmentService.enqueueForfulfilment(updatedTransaction.getOrderId());
                    enqueued = true;
                }
                
                if (!enqueued || alwaysRefundOnDelayedPaymentSuccess) {
                    logger.info("OrderID " + updatedTransaction.getOrderId() + " not enqueued for recharge. Enqueueing "+updatedTransaction.getMerchantTxnId()+" for refund.");
                    kestrelWrapper.enqueuePaymentRefund(updatedTransaction.getMerchantTxnId(), updatedTransaction.getAmount());
                    // Commenting old communication of refund
                    // New communication stating 'Payment Received by FC' can be added here
                    // paymentRefund.sendPaymentSuccessfulNotifications(updatedTransaction);
                }
            }
            
            return updatedTransaction;
        } catch (Exception e) {
            logger.error("something bad happened with order ID :[" + paymentTransaction.getOrderId() + "]. Going ahead with rest of the orders", e);
            throw e;
        }
    }
    
    public void sweepAsync(Date startDate, Date endDate) throws Exception {
        //Get candidates which are in an unknown payment state
        List<Integer> paymentIds =  paymentTransactionService.getPaymentTxnIdsWithResNullWithTimeLimit(startDate, endDate);

        if (paymentIds == null) {
            return;
        }

        logger.info("Working on transaction for which we did not receive a response from PG - start");
        logger.info("Size of payment transactions:" + paymentIds.size());

        for (Integer paymentTransactionId : paymentIds) {
            kestrelWrapper.enqueueForSyncPayment(paymentTransactionId.toString(), PaymentConstants.IS_SWEEP);
        }
    }
    
    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(AutoRefundPaymentGatewayFailures.class.getSimpleName());
    }
    
    private Boolean isRechargeTypeTopup(PaymentTransaction updatedTransaction) {
    	 List<CartItemsBusinessDO> cartItemsBusinessDOs = orderIdSlaveDAO.getCartItemsByOrderId(updatedTransaction.getOrderId());
         
         	for (CartItemsBusinessDO cartItemsBusinessDO : cartItemsBusinessDOs) {
         		if (ProductName.fromProductId(cartItemsBusinessDO.getProductMasterId()) == ProductName.Mobile || ProductName.fromProductId(cartItemsBusinessDO.getProductMasterId()) == ProductName.DTH) {
         		 if (StringUtils.isEmpty(cartItemsBusinessDO.getEntityId()) || "topup".equals(cartItemsBusinessDO.getEntityId())) {
         			return true;
         		}
         	  }
         	}
         return false;
    }
}
