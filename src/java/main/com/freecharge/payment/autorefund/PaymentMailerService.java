package com.freecharge.payment.autorefund;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentType.PaymentTypeName;
import com.freecharge.payment.util.PaymentConstants;
import com.google.common.base.CaseFormat;

@Component
public class PaymentMailerService {
    private static final Logger logger = LoggingFactory.getLogger(PaymentMailerService.class);

    @Autowired
    private FreefundService freeFundService;

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private EmailService emailService;

    @Autowired
    private CartService cartService;
    
    @Autowired
    private PricingService pricingService;
    
    private DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public void sendRefundMail(PaymentRefundBusinessDO refundBusinessDO) throws Exception {
        logger.info("About to enqueue in refund mail queue for order ID : " + refundBusinessDO.getOrderId());

        Map<String, Object> userDetail = null;
        userDetail = homeBusinessDao.getUserRechargeDetailsFromOrderId(refundBusinessDO.getOrderId());
        if (userDetail == null || userDetail.isEmpty()) {
            logger.error("Something is wrong here no user details found not sending any e-mail for Order ID : " + refundBusinessDO.getOrderId());

            return;
        }

        String userEmail = (String) userDetail.get("email");

        String userFirstName = (String) userDetail.get("firstname");
        String userRechargeAmount = userDetail.get("amount").toString();
        String operatorName = (String) userDetail.get("operatorName");
        String lookupId = (String) userDetail.get("lookupId");
        String createdAt = dateFormatter.format(new Date(((Timestamp) userDetail.get("createdAt")).getTime()));
        String userMobileNo = (String) userDetail.get("mobileno");
        String refundDate = dateFormatter.format(new Date());

        String imagePrefix = fcProperties.getProperty("imgprefix1").trim();
        String hostPrefix = fcProperties.getProperty("hostprefix").trim();

        Boolean hasHandlingCharges = Boolean.FALSE;

        

        Map<String, String> mailDataMap = new HashMap<String, String>();
        mailDataMap.put("imageprefix", imagePrefix);
        mailDataMap.put("hostprefix", hostPrefix);

        mailDataMap.put("orderid", refundBusinessDO.getOrderId());
        mailDataMap.put("rechargedate", createdAt);
        mailDataMap.put("refunddate", refundDate);
        mailDataMap.put("operator", operatorName);
        mailDataMap.put("servicenumber", userMobileNo);
        mailDataMap.put("userfirstname", userFirstName);

        mailDataMap.put("rechargeamount", userRechargeAmount);
        mailDataMap.put("hasCoupons", String.valueOf(false));
        mailDataMap.put("hasPaidCoupons", String.valueOf(false));
        
        if (pricingService.hasPaidCoupons(refundBusinessDO.getOrderId())){
        	mailDataMap.put("hasPaidCoupons", String.valueOf(true));
        	mailDataMap.put("paidCouponAmount", String.valueOf(pricingService.getCouponCharges(refundBusinessDO.getOrderId())));
        	String productType = (String) userDetail.get("productType");
        	int handlingCharge = pricingService.getHandlingCharge(productType);
        	mailDataMap.put("couponHandlingCharge", String.valueOf(handlingCharge));
        } else{
        	if (cartService.hasCoupons(refundBusinessDO.getOrderId())) {
        	    mailDataMap.put("hasCoupons", String.valueOf(true));
        	    hasHandlingCharges = Boolean.TRUE;
                String productType = (String) userDetail.get("productType");
            	int handlingCharge = pricingService.getHandlingCharge(productType);
            	mailDataMap.put("couponHandlingCharge", String.valueOf(handlingCharge));
            } 
        }
        
        
        
        mailDataMap.put("hashandlingcharges", hasHandlingCharges.toString());
        
        if (freeFundService.containsFreeFundItem(lookupId)) {
            mailDataMap.put("promodiscount", String.valueOf(freeFundService.getFreeFundItemPrice(lookupId)));
            mailDataMap.put("promodiscountavailed", String.valueOf(true));
        } else {
            mailDataMap.put("promodiscount", "0");
            mailDataMap.put("promodiscountavailed", String.valueOf(false));
        }

        mailDataMap.put("totalpaid", String.valueOf(refundBusinessDO.getAmount()));
        mailDataMap.put("totalrefund", String.valueOf(refundBusinessDO.getRefundAmount()));
        
        String basePath = fcProperties.getCCSBasePathSurveyUrl();
        String urlKey = fcProperties.getSurveyUrlKey();
        String [] urlList = FCUtil.getCCSValues(urlKey, basePath, fcProperties.getCCSHost());
        mailDataMap.put("improveUrl",urlList[0]);
        mailDataMap.put("serveBetterUrl",urlList[1]);
        
        logger.info("UrlList:"+urlList+"\nimproveUrl:"+urlList[0]+"\nserveBetterurl"+urlList[1]);

        String mailTemplate = "";

        if (refundBusinessDO.isRefundToWallet()) {
            mailTemplate = "refundToWalletTrans.vm";
        } else {
            mailTemplate = "refundTrans.vm";
        }

        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setTo(userEmail);
        emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_FROM));
        emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_REPLYTO));
        if (refundBusinessDO.isRefundToWallet()) {
            emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_SUBJECT) + refundBusinessDO.getOrderId());
        } else  {
            emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.BANK_REFUND_SUCCESS_SUBJECT) + refundBusinessDO.getOrderId());
        }
        emailBusinessDO.setVariableValues(mailDataMap);
        emailBusinessDO.setTemplateName(mailTemplate);

        emailService.sendEmail(emailBusinessDO);
    }

    public void paymentSuccessful(PaymentTransaction paymentTransaction) {

        logger.info("About to enqueue in refund mail queue for order ID : " + paymentTransaction.getOrderId());

        Map<String, Object> userDetail = null;
        userDetail = homeBusinessDao.getUserRechargeDetailsFromOrderId(paymentTransaction.getOrderId());
        if (userDetail == null || userDetail.isEmpty()) {
            logger.error("Something is wrong here no user details found not sending any e-mail for Order ID : " + paymentTransaction.getOrderId());

            return;
        }

        String userEmail = (String) userDetail.get("email");
        String userFirstName = (String) userDetail.get("firstname");
        String userRechargeAmount = userDetail.get("amount").toString();
        String operatorName = (String) userDetail.get("operatorName");
        String lookupId = (String) userDetail.get("lookupId");
        String createdAt = dateFormatter.format(new Date(((Timestamp) userDetail.get("createdAt")).getTime()));
        String userMobileNo = (String) userDetail.get("mobileno");
        String refundDate = dateFormatter.format(new Date());

        String imagePrefix = fcProperties.getProperty("imgprefix1").trim();
        String hostPrefix = fcProperties.getProperty("hostprefix").trim();

        Boolean hasHandlingCharges = Boolean.FALSE;

        if (cartService.hasCoupons(paymentTransaction.getOrderId())) {
            hasHandlingCharges = Boolean.TRUE;
        }

        Map<String, String> mailDataMap = new HashMap<String, String>();
        mailDataMap.put("imageprefix", imagePrefix);
        mailDataMap.put("hostprefix", hostPrefix);

        mailDataMap.put("orderid", paymentTransaction.getOrderId());
        mailDataMap.put("rechargedate", createdAt);
        mailDataMap.put("refunddate", refundDate);
        mailDataMap.put("operator", operatorName);
        mailDataMap.put("servicenumber", userMobileNo);
        mailDataMap.put("userfirstname", userFirstName);

        mailDataMap.put("rechargeamount", userRechargeAmount);
        mailDataMap.put("hashandlingcharges", hasHandlingCharges.toString());
        
        mailDataMap.put("hasCoupons", String.valueOf(false));
        mailDataMap.put("hasPaidCoupons", String.valueOf(false));

        if (paymentTransaction.getIsSuccessful() && !PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentTransaction.getPaymentGateway())) {
            Integer paymentTypeCode = paymentTransaction.getPaymentType();
            String paymentTypeString = PaymentTypeName.fromPaymentTypeCode(paymentTypeCode).toString();
            mailDataMap.put("paymentSource", "Payment received through " + paymentTypeString);
        } else {
            mailDataMap.put("paymentSource", "Payment Successful");
        }

        if (freeFundService.containsFreeFundItem(lookupId)) {
            mailDataMap.put("promodiscount", String.valueOf(freeFundService.getFreeFundItemPrice(lookupId)));
            mailDataMap.put("promodiscountavailed", String.valueOf(true));
        } else {
            mailDataMap.put("promodiscount", "0");
            mailDataMap.put("promodiscountavailed", String.valueOf(false));
        }

        mailDataMap.put("totalpaid", String.valueOf(paymentTransaction.getAmount()));
        mailDataMap.put("totalrefund", String.valueOf(paymentTransaction.getAmount()));
        String mailTemplate = "";

        mailTemplate = "refundToWalletTrans.vm";

        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setTo(userEmail);
        emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_FROM));
        emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_REPLYTO));
        emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_SUBJECT) + paymentTransaction.getOrderId());
        emailBusinessDO.setVariableValues(mailDataMap);
        emailBusinessDO.setTemplateName(mailTemplate);

        emailService.sendEmail(emailBusinessDO);

    }

    public static String getBeanName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, PaymentMailerService.class.getSimpleName());
    }
}
