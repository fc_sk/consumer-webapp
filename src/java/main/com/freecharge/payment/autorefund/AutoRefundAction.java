package com.freecharge.payment.autorefund;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Hours;
import org.joda.time.Interval;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This is a wrapper around {@link AutoRefund} that makes use of {@link ForkJoinPool} to parallelize
 * auto refunds. This makes sense when the auto refund period is more than a few hours. Time periods are
 * split into two hour windows and each of them are executed in parallel.
 * 
 * Note: This has been *specifically* not been "springified" since I'm not sure how fork-join works with
 * spring eco-system; neither is there any clarity in spring community itself. So till then this class
 * shall be hand created and managed.
 * @author arun
 *
 */
public class AutoRefundAction extends RecursiveAction {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    private static final Logger logger = LoggingFactory.getLogger(AutoRefundAction.class);

    private Date startTime;
    private Date endTime;
    private AutoRefund delegate;
    private Boolean async;

    public AutoRefundAction(Date startTime, Date endTime, AutoRefund delegate, Boolean async) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.delegate = delegate;
        this.async = async;
    }
    
    @Override
    protected void compute() {
        if (!inForkJoinPool()) {
            throw new IllegalStateException(
                    "Attempting to compute AutoRefundAction from outside of ForkJoin computation");
        }
        
        if (getHoursBetween(this.startTime, this.endTime) <= 2) {
            logger.info("Refund is now between [" + this.startTime + "] and [" + this.endTime + "]");
            try {
                
                delegate.doRefund(this.startTime, this.endTime, this.async);
            } catch (Exception e) {
                logger.error("Caught while auto refunding between [" + this.startTime + "] and [" + this.endTime + "]", e);
            }
        } else {
            List<Interval> intervalList = splitDuration(this.startTime, this.endTime);
            
            List<AutoRefundAction> refundActionList = new ArrayList<>();
            
            for (Interval interval : intervalList) {
                refundActionList.add(new AutoRefundAction(new Date(interval.getStartMillis()), new Date(interval
                        .getEndMillis()), this.delegate, this.async));
            }
            
            invokeAll(refundActionList);
        }
    }
    
    private int getHoursBetween(Date startDate, Date endDate) {
        DateTime st = new DateTime(startDate.getTime());
        DateTime et = new DateTime(endDate.getTime());

        final int hours = Hours.hoursBetween(st, et).getHours();
        
        logger.info("Hours between [" + startDate + "] and [" + endDate + "] is " + hours);

        return hours;
    }
    
    private List<Interval> splitDuration(Date startDate, Date endDate) {
        DateTime start = new DateTime(startDate.getTime());
        DateTime end = new DateTime(endDate.getTime());
        
        if (end.isBefore(start)) {
            throw new IllegalArgumentException("End is before start. Start [" + start + "]. End [" + end + "]");
        }
        
        long startMillis = end.getMillis();
        long endMillis = end.getMillis();
        
        List<Interval> list = new ArrayList<Interval>();

        while(true) {
            startMillis = endMillis - DateTimeConstants.MILLIS_PER_HOUR * 2;
            
            final Interval currentInterval = new Interval(startMillis, endMillis);
            
            if (currentInterval.getEnd().isBefore(start)) {
                break;
            }
            
            if (currentInterval.getStart().isBefore(start)) {
                if (endMillis > start.getMillis()) {
                    list.add(new Interval(start.getMillis(), endMillis));
                }
                break;
            } else {
                list.add(currentInterval);
            }
            endMillis = startMillis;
        }
        return list;
    }
    
    public static void main(String[] args) throws Exception {
        ForkJoinPool fjPool = new ForkJoinPool();
        
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        AutoRefund autoRefund = ((AutoRefund) ctx.getBean("autoRefund"));

        Date start = RefundUtil.getDate(args[0]);
        Date end = RefundUtil.getDate(args[1]);
        Boolean async = false;
        if (args.length >= 3) {
            if (args[2].equals("async")) {
                async = true;
            }
        }
        
        logger.info(String.format("Doing efund for candidates from %s to %s", start, end));

        try {
            fjPool.submit(new AutoRefundAction(start, end, autoRefund, async)).get();
        } catch (Exception e) {
            logger.error("Exception while doing autorefund", e);
        } finally {
            fjPool.shutdown();
            fjPool.awaitTermination(1, TimeUnit.MINUTES);
            
            ctx.close();
            System.exit(0);
        }
    }

}