package com.freecharge.payment.autorefund;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCFileUtil;

/**
 * This class handles re-triggering coupons for a list of order ids.
 * @author Shirish Surti
 *
 */
public class CouponEmailRetrigger {
	private static Logger logger = LoggingFactory.getLogger(CouponEmailRetrigger.class);
	
    public static void main(String[] args) throws Exception {
    	Thread.currentThread().setName("CouponEmailRetrigger");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        try {
        	String csvFilePath = args[0];
            List<String[]> data = FCFileUtil.parseCSVFile(csvFilePath);
            if (data == null){
            	logger.error("No data could be read from the CSV file.");
            	return;
            }
            List<String> orderIdList = new ArrayList<String>();
            FulfillmentService fulfillmentService = (FulfillmentService) ctx.getBean("fulfillmentService");
            for (String[] dataRow : data){
            	String orderId = dataRow[0];
            	orderIdList.add(orderId);
            }
            fulfillmentService.reTriggerTxnAndCouponEmail(orderIdList);
            logger.info("Finished retriggering coupons for order ids.");
		} catch (Exception e) {
			logger.error("ERROR occured in executing CouponEmailRetrigger.");
			logger.error(e.getCause(), e);
		} finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
    }

}
