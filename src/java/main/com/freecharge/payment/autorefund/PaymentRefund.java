package com.freecharge.payment.autorefund;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.TransactionHomePage;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.properties.FCProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.jdbc.UserSlaveDao;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.services.KpayPaymentMetaService;
import com.freecharge.payment.services.PaymentGatewayHandlerFactory;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentRefundService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.RefundStatus;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.exception.WalletCreditLimitBreachedException;
import com.google.common.collect.ImmutableList;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;
import org.springframework.web.client.RestTemplate;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 28/9/12 Time: 10:08 AM To change
 * this template use File | Settings | File Templates.
 */
@Component
public class PaymentRefund {
    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory
            .getRefundLoggerName(PaymentRefund.class.getName()));

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private PaymentMailerService paymentMailerService;

    @Autowired
    private CartService cartService;

    @Autowired
    private PricingService pricingService;
    
    @Autowired
    private PgReadDAO pgReadDAO;

    @Autowired
    private SMSService smsService;

    @Autowired
    private PaymentGatewayHandlerFactory pgFactory;

    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private UserSlaveDao userSlaveDao;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    OneCheckWalletService oneCheckWalletService;
    
    @Autowired
    WalletWrapper walletWrapper;
    
    @Autowired
    PaymentRefundService paymentRefundService;
    
    @Autowired
    private KpayPaymentMetaService                kpayPaymentMetaService;

    @Autowired
    protected FCProperties fcProperties;
    
    private List<String> idempotencyExclusion = ImmutableList.of(PaymentConstants.ADMIN_CASH_BACK,
            PaymentConstants.EXCESS_CREDIT, PaymentConstants.CS_CASHBACK);

    private final String TRANSACTION_NOT_FOUND = "Transaction not found";
    private final String INVALID_DATE = "Invalid transaction date";
    private final Integer OCW_REFUND_ITERATIONS = 3;
    private static final String REFUND_STATUS_URI = "/refundStatus?";
    private static final String fetchFromMtxnArg = "MTxnId=";
    private static final String REFUND_NOT_ATTEMPTED = "not_attempted";
    private static final String ERROR = "error";

    // ---------- DEFAULT Shortcuts ----------

    public List<PaymentRefundBusinessDO> doRefund(Set<String> merchantOrderIds, String refundType) {
        return doRefund(merchantOrderIds, refundType, WALLET_REFUND_ACTION);
    }

    public List<PaymentRefundBusinessDO> doBillPaymentRefund(Set<String> merchantOrderIds, String refundType)
            throws Exception {
        return doRefund(merchantOrderIds, refundType, WALLET_REFUND_ACTION);
    }

    public PaymentRefundBusinessDO doManualrefund(PaymentTransaction paymentTransaction, String refundType)
            throws Exception {
        return doManualrefund(paymentTransaction, refundType, WALLET_REFUND_ACTION);
    }

    public PaymentRefundBusinessDO doRefund(String merchantOrderIds) throws Exception {
        logger.info("In PaymentRefundBusinessDO doRefund PaymentRefund:111 ");
        logger.info("About to invoke doRefund(merchantOrderIds, WALLET_REFUND_ACTION):114");
        return doRefund(merchantOrderIds, WALLET_REFUND_ACTION);
    }

    public PaymentRefundBusinessDO doRefund(PaymentTransaction paymentTransaction, Double refundAmount,
            boolean enableRefundMail, String refundType) throws Exception {
        return doRefund(paymentTransaction, refundAmount, enableRefundMail, refundType, WALLET_REFUND_ACTION);
    }

    // ---------- Implementation ----------

    public List<PaymentRefundBusinessDO> doRefund(Set<String> merchantOrderIds, String refundType,
            RefundAction refundAction) {
        logger.info("In List<PaymentRefundBusinessDO> doRefund:124");
        
        List<PaymentRefundBusinessDO> paymentRefundBusinessDOs = new LinkedList<PaymentRefundBusinessDO>();
        for (String orderId : merchantOrderIds) {
            logger.info("Refunding the following order ids:  :" + orderId);

            List<PaymentTransaction> paymentTransactions = this.paymentTransactionService
                    .getPaymentTransactionByMerchantOrderId(orderId);
            
            List<String> processedMtxnIds = new ArrayList<>();
            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                logger.info("Refunding the following mtxnid :  :" + paymentTransaction.getMerchantTxnId());
                try {
                    if (processedMtxnIds.contains(paymentTransaction.getMerchantTxnId())) {
                        logger.info("MtxnID : " + paymentTransaction.getMerchantTxnId() + " already processed. Skipping.");
                        continue;
                    }
                    if (paymentTransaction.getIsSuccessful()) {
                        /*if(paymentTransaction.isWalletPayment()){
                            List<PaymentRefundTransaction> walletRefunds = walletWrapper.getRefundTxnFromWallet(paymentTransaction);
                            if (!walletRefunds.isEmpty()) {
                                logger.info("Wallet payment already refunded. Not attempting refund for mtxnID " + orderId);
                                continue;
                            }
                        }*/
                        PaymentRefundBusinessDO paymentRefundBusinessDO = doRefund(paymentTransaction, refundType,
                                refundAction);

                        /*
                         * Handling BD case where 'coz of various reasons we
                         * could get transaction not found or invalid
                         * transaction date. In such cases refresh the DB state
                         * by querying PG and attempt refund again.
                         */
                        if (paymentRefundBusinessDO.getRawPGResponse() != null
                                && !paymentRefundBusinessDO.getRawPGResponse().trim().isEmpty()) {
                            if (paymentRefundBusinessDO.getRawPGResponse().contains(TRANSACTION_NOT_FOUND)
                                    || paymentRefundBusinessDO.getRawPGResponse().contains(INVALID_DATE)) {
                                logger.info(
                                        "Got retriable error while refunding trying again after syncing with PG for Order ID : "
                                                + paymentTransaction.getOrderId());
                                paymentTransactionService.syncfromPGData(paymentTransaction, false);
                                logger.info(
                                        "About to invoke doRefund(paymentTransaction, refundType, refundAction):156");
                                paymentRefundBusinessDO = doRefund(paymentTransaction, refundType, refundAction);

                            }
                        }

                        paymentRefundBusinessDOs.add(paymentRefundBusinessDO);

                    }
                } catch (Exception e) {
                    logger.error("Something terrible happened while refunding Order ID :["
                            + paymentTransaction.getOrderId() + "]. Moving on with rest of the order IDs", e);
                }
            }
        }
        return paymentRefundBusinessDOs;
    }

    public PaymentRefundBusinessDO doManualrefund(PaymentTransaction paymentTransaction, String refundType,
            RefundAction refundAction) throws Exception {
        logger.info("Refunding the following mtxnIds ids:" + paymentTransaction.getMerchantTxnId());
        Double refundAmount = Double.parseDouble(this.calculateRefundAmountForPaymentTxn(paymentTransaction));
        PaymentRefundBusinessDO paymentRefundBusinessDO = doRefund(paymentTransaction, refundAmount, true, refundType,
                refundAction);
        return paymentRefundBusinessDO;
    }

    public PaymentRefundBusinessDO doRefund(String merchantOrderIds, RefundAction refundAction) throws Exception {
        logger.info("In PaymentRefundBusinessDO doRefund(String merchantOrderIds, RefundAction refundAction):182");
        Set<String> orderIds = new HashSet<String>();
        orderIds.add(merchantOrderIds);
        logger.info("About to invoke doRefund(orderIds, PaymentConstants.MANUAL_REFUND, refundAction):187");
        List<PaymentRefundBusinessDO> dos = doRefund(orderIds, PaymentConstants.MANUAL_REFUND, refundAction);
        return dos == null ? null : dos.size() > 0 ? dos.get(0) : null;
    }

    public static interface RefundAction {
        public void refund(IGatewayHandler paymentGatewayHandler, String paymentGateway,
                PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType,
                PaymentTransaction paymentTransaction, Double refundAmount) throws Exception;
    }

    public final RefundAction PG_REFUND_ACTION = new RefundAction() {
        @Override
        public void refund(IGatewayHandler paymentGatewayHandler, String paymentGateway,
                PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType,
                PaymentTransaction paymentTransaction, Double refundAmount) throws Exception {
            /*
             * In case the payment gateway is wallet then find the originating
             * payment gateway if any and refund to it instead of refunding to
             * wallet.
             */
            paymentRefundBusinessDO.setRefundToWallet(false);
            logger.info("Inside refund of RefundAction PG_REFUND_ACTION :200");
            if (PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentGateway)) {
                List<PaymentTransaction> paymentTransactionList = paymentTransactionService
                        .getPaymentTransactionsByOrderId(paymentTransaction.getOrderId());

                for (PaymentTransaction paymentTransactionToInspect : paymentTransactionList) {
                    if (paymentTransactionToInspect.getIsSuccessful()) {

                        // need to create this afresh since pg txn ID etc., are
                        // different for wallet vs PG.
                        PaymentRefundBusinessDO pgRefundBusinessDO = paymentTransactionService
                                .createPaymentRefundBusinessDO(paymentTransactionToInspect);
                        pgRefundBusinessDO.setRefundAmount(refundAmount);

                        String pgToCheck = paymentTransactionToInspect.getPaymentGateway();
                        if (!PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(pgToCheck)) {
                            logger.info(String.format(
                                    "Found non-wallet payment gateway for order ID: [%s]. Refunding using that PG",
                                    paymentTransaction.getOrderId()));
                            IGatewayHandler originatingPGHandler = pgFactory.getPaymentHandler(pgToCheck);
                            logger.info("About to invoke refundPayment of originatingPGHandler:230");
                            originatingPGHandler.refundPayment(pgRefundBusinessDO, refundType);
                            logger.info("Returning from refundPayment of originatingPGHandler:230");
                        }
                    }
                }
            } else {
                logger.info("About to invoke refundPayment of paymentGatewayHandler:236");
                paymentGatewayHandler.refundPayment(paymentRefundBusinessDO, refundType);
                logger.info("Returning from refundPayment of originatingPGHandler:230");
            }
        }
    };

    public final RefundAction WALLET_REFUND_ACTION = new RefundAction() {
        @Override
        public void refund(IGatewayHandler paymentGatewayHandler, String paymentGateway,
                PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType,
                PaymentTransaction paymentTransaction, Double refundAmount) throws Exception {
            paymentRefundBusinessDO.setRefundToWallet(true);
            try {
                logger.info("Trying to do a wallet refund for mtxnID " + paymentTransaction.getMerchantTxnId());
                paymentGatewayHandler.refundToWallet(paymentRefundBusinessDO, refundType);
            } catch (WalletCreditLimitBreachedException e) {
                logger.error("WalletCreditLimitBreachedException occured while refunding to wallet. " + paymentRefundBusinessDO.getOrderId(), e);
                refundPgAmountToPaymentGateway(paymentRefundBusinessDO, paymentTransaction, refundType);
            } catch (Exception e) {
                logger.error("Error occured while refunding to wallet. Cron should pick up these cases for refund " + paymentRefundBusinessDO.getOrderId(), e);
            }
        }

        private void refundPgAmountToPaymentGateway(PaymentRefundBusinessDO pgRefundBusinessDO, PaymentTransaction txn, String refundType) throws Exception {
            PaymentTransaction paymentTransaction = getPgTxnToBeRefunded(txn);
            if (paymentTransaction != null) {
                logger.info("Trying to refund " + paymentTransaction.getAmount() + " via PG " + paymentTransaction.getPaymentGateway() + " for orderID " + paymentTransaction.getOrderId());
                IGatewayHandler paymentGatewayHandler = pgFactory.getPaymentHandler(paymentTransaction.getPaymentGateway());
                pgRefundBusinessDO.setRefundAmount(paymentTransaction.getAmount());
                PG_REFUND_ACTION.refund(paymentGatewayHandler, paymentTransaction.getPaymentGateway(), pgRefundBusinessDO, refundType,
                        paymentTransaction, paymentTransaction.getAmount());
                logger.info("PG Refund success for " + paymentTransaction.getAmount() + " via PG " + paymentTransaction.getPaymentGateway() + " for orderID " + paymentTransaction.getOrderId());
            }
        }
    };

    public PaymentRefundBusinessDO doRefund(PaymentTransaction paymentTransaction, Double refundAmount,
            boolean enableRefundMail, String refundType, RefundAction refundAction) throws Exception {
        // Insert or update refund scheduler
        try {
            paymentRefundService.createOrUpdateStatus(paymentTransaction.getMerchantTxnId(), RefundStatus.IN_PROGRESS);
            return processRefundForMtxnId(paymentTransaction, refundAmount, enableRefundMail, refundType, refundAction);
        } finally {
            logger.info("Check and update the status of refund_scheduler table for " + paymentTransaction.getMerchantTxnId());
            List<PaymentTransaction> successfulPaymentTxn = paymentTransactionService
                    .getSuccessfulPaymentTransactionByMerchantOrderId(paymentTransaction.getMerchantTxnId());
            boolean refundDone = isRefundDone(successfulPaymentTxn);
            if (refundDone) {
                paymentRefundService.createOrUpdateStatus(paymentTransaction.getMerchantTxnId(), RefundStatus.COMPLETED);
            } else {
                paymentRefundService.scheduleOrMarkFailed(paymentTransaction.getMerchantTxnId());
            }
        }
    }

    private PaymentRefundBusinessDO processRefundForMtxnId(PaymentTransaction paymentTransaction, Double refundAmount,
            boolean enableRefundMail, String refundType, RefundAction refundAction) throws Exception {
        String paymentGateway = paymentTransaction.getPaymentGateway();
        IGatewayHandler paymentGatewayHandler = pgFactory.getPaymentHandler(paymentGateway);

        PaymentRefundBusinessDO paymentRefundBusinessDO = paymentTransactionService
                .createPaymentRefundBusinessDO(paymentTransaction);

        RefundAction refundDestination = determineRefundDestination(paymentTransaction, refundAmount);
        if(refundDestination != null){
            refundAction = refundDestination;
        }

        if (!idempotencyExclusion.contains(refundType)) {
            /*
             * Idempotency check. Exclude it for cash back refund for now.
             */
            Double totalOrderAmount = Double.parseDouble(calculateRefundAmountForPaymentTxn(paymentTransaction));
            
            List<PaymentRefundTransaction> refundedTransaction = getAllRefundsDoneForPayment(paymentTransaction);
            Double refundedAmount = FCUtil.calculateTotalRefunds(refundedTransaction);

            logger.info("Total amount for transaction " + paymentTransaction.getMerchantTxnId() + " : " + totalOrderAmount + " and Refunded amount is : " + refundedAmount);

            // Round to 2 decimal places
            Double maxRefundable = new BigDecimal(totalOrderAmount).subtract(new BigDecimal(refundedAmount))
                    .setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
            
            boolean isOrderRefunded = false;
            if (totalOrderAmount.compareTo(refundedAmount) == 0) {
                isOrderRefunded = true;
            } else {
                if (refundAmount.compareTo(maxRefundable) > 0) {
                    logger.error(String.format(
                            "Refund amount (%s) is greater than the maximum amount (%s) that can be refunded ",
                            refundAmount, maxRefundable));
                    refundAmount = maxRefundable;
                }
            }
            if (isOrderRefunded) {
                logger.info("Payment order already successfully refunded,"
                        + " returning from PaymentRefund.doRefund for order ID " + paymentRefundBusinessDO.getOrderId());
                paymentRefundBusinessDO.setStatus(true);
                paymentRefundBusinessDO.setRefundAmount(refundedTransaction.get(0).getRefundedAmount().getAmount()
                        .doubleValue());
                return paymentRefundBusinessDO;
            }
        }
        paymentRefundBusinessDO.setRefundAmount(refundAmount);
        if (refundAmount <= 0) {
            logger.warn("Refund amount is less than or zero for Order ID :[" + paymentTransaction.getOrderId()
                    + "]. Refund Amount[" + refundAmount + "]. Not going ahead with refund");
            return paymentRefundBusinessDO;
        }

        logger.info("Attempting refund of " + paymentTransaction.getMerchantTxnId() + " for amount : " + refundAmount);
        refundAction.refund(paymentGatewayHandler, paymentGateway, paymentRefundBusinessDO, refundType,
                paymentTransaction, refundAmount);

        sendNotifications(paymentRefundBusinessDO, enableRefundMail);
        return paymentRefundBusinessDO;
    }

    private boolean isFullyFCWalletPayment(PaymentTransaction paymentTransaction) {
        return paymentTransaction.getPaymentType() != null
                && paymentTransaction.getPaymentType().equals(
                        Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET))
                && PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(paymentTransaction.getPaymentOption());
    }
    /*
     *  Method to determine the refund action (WALLET/PG) to be used for the refund request
     *  1. If the transaction is worth more than 2000, the refund should always go to source.
     *  2. For FSS transactions and NB transactions via ICICI, refund should always go to source.
     *  
     */
    public RefundAction determineRefundDestination(PaymentTransaction paymentTransaction, Double refundAmount) {
        if (isFullyFCWalletPayment(paymentTransaction)) {
            logger.info("Trying to refund to wallet via kpay for mtxnId " + paymentTransaction.getMerchantTxnId());
            return PG_REFUND_ACTION;
        }
        
        // To prevent wallet frauds
        if (refundAmount >= 2000) {
            if (paymentTransaction.isWalletPayment()) {
                return WALLET_REFUND_ACTION;
            } else {
                return PG_REFUND_ACTION;
            }
        }

        String pgUsed = kpayPaymentMetaService.getPgUsed(paymentTransaction.getMerchantTxnId());
        if (!StringUtils.isEmpty(pgUsed) && (pgUsed.equals("fss")
                || (paymentTransaction.getPaymentType().equals(3) && pgUsed.equals("icici")))) {
            logger.info("RefundAction is pg");
            return PG_REFUND_ACTION;
        }
        return null;
    }

    private Double getOCWRefundedAmountForOrder(PaymentTransaction paymentTransaction) {
        Double refundedAmount = 0.0;
        Users users = userServiceProxy.getUserFromOrderId(paymentTransaction.getOrderId());
        String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(users.getEmail());
        if (!StringUtils.isEmpty(sdIdentity)) {
                logger.info("Checking for wallet refund for one check transaction "
                                + paymentTransaction.getMerchantTxnId());
            for(int sleepCounter = 0; sleepCounter < OCW_REFUND_ITERATIONS; sleepCounter++) {
                    refundedAmount = 0.0;
                    TransactionSummary txnSummary = oneCheckWalletService.findTransactionIdempotencyId(
                            OneCheckTxnType.CREDIT_REFUND, paymentTransaction.getOrderId(),
                            paymentTransaction.getMerchantTxnId(), sdIdentity, null);
                    if (txnSummary != null) {
                        logger.info("Amount refunded to wallet for " + paymentTransaction.getMerchantTxnId() + " : "
                                + txnSummary.getTransactionAmount().doubleValue());
                        refundedAmount += txnSummary.getTransactionAmount().doubleValue();
                    }
                    txnSummary = oneCheckWalletService.findTransactionIdempotencyId(OneCheckTxnType.CS_REFUND,
                            paymentTransaction.getOrderId(), paymentTransaction.getMerchantTxnId(), sdIdentity, null);
                    if (txnSummary != null) {
                        logger.info("Amount refunded to wallet by CS for " + paymentTransaction.getMerchantTxnId() + " : "
                                + txnSummary.getTransactionAmount().doubleValue());
                        refundedAmount += txnSummary.getTransactionAmount().doubleValue();
                    }
                    txnSummary = oneCheckWalletService.findTransactionIdempotencyId(OneCheckTxnType.CREDIT_REFUND_API,
                            paymentTransaction.getOrderId(), paymentTransaction.getMerchantTxnId(), sdIdentity, null);
                    if (txnSummary != null) {
                        logger.info("Amount refunded to wallet by CREDIT_REFUND_API for " + paymentTransaction.getMerchantTxnId() + " : "
                                + txnSummary.getTransactionAmount().doubleValue());
                        refundedAmount += txnSummary.getTransactionAmount().doubleValue();
                    }
                    if (refundedAmount == 0) {
                        try {
                            logger.info("Sleeping for 1000ms before checking again for wallet refunds for mtxnid: " + paymentTransaction.getMerchantTxnId());
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            logger.error("Sleep interrupted for mtxnid: " + paymentTransaction.getMerchantTxnId());
                        }
                    }
                }
            }
        return refundedAmount;
    }

    private PaymentRefundBusinessDO doRefund(PaymentTransaction paymentTransaction, String refundType,
            RefundAction refundAction) throws Exception {
        logger.info("In PaymentRefundBusinessDO doRefund for mtxn " + paymentTransaction.getMerchantTxnId());
        Double refundAmount = Double.parseDouble(calculateRefundAmountForPaymentTxn(paymentTransaction));
        logger.info("Refund amount for payment transaction "+ paymentTransaction.getMerchantTxnId()  + " is " + refundAmount);
        return doRefund(paymentTransaction, refundAmount, true, refundType, refundAction);
    }

    private void sendRefundSms(String orderId, String amount, String no) {
        if (no == null) {
            logger.info("Mobile no is null so not sending sms for order id: " + orderId);
            return;
        }
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        Map<String, String> params = new HashMap<String, String>();
        params.put("orderId", orderId);
        params.put("refundAmount", amount);
        params.put("receiverNumber", no);
        Users user = userServiceProxy.getUserFromOrderId(orderId);
        smsBusinessDO.setReceiverNumber(no);
        smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_RECHARGE_REFUND);
        smsBusinessDO.setVariableValues(params);
        if (user != null) {
            smsBusinessDO.setRegisteredUserEmail(user.getEmail());
        }
        smsService.sendSMS(smsBusinessDO);
    }

    public PaymentTransactionService getPaymentTransactionService() {
        return paymentTransactionService;
    }

    public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
        this.paymentTransactionService = paymentTransactionService;
    }

    private void sendNotifications(PaymentRefundBusinessDO paymentRefundBusinessDO, Boolean enableRefundNotification) {

        if (paymentRefundBusinessDO.getStatus() !=null && paymentRefundBusinessDO.getStatus() && enableRefundNotification) {

            sendRefundMail(paymentRefundBusinessDO);
            sendRefundSms(paymentRefundBusinessDO);
        }
    }

    public void sendPaymentSuccessfulNotifications(PaymentTransaction paymentTransaction) {
        if (paymentTransaction.getIsSuccessful()) { 
            paymentMailerService.paymentSuccessful(paymentTransaction);
        }
    }

    @Transactional
    public String calculateRefundAmountForPaymentTxn(PaymentTransaction paymentTransaction) throws Exception {
        logger.info("Calculating the total amount to be refunded for mtxnId " + paymentTransaction.getMerchantTxnId());
        Double refundAmount = pricingService.getRefundAmountForOrderId(paymentTransaction.getOrderId());

        logger.info("Refund Amount calculated from cart for " + paymentTransaction.getMerchantTxnId() + " is "
                + refundAmount);
        if (refundAmount.compareTo(paymentTransaction.getAmount()) > 0) {
            List<PaymentTransaction> successPayments = paymentTransactionService
                    .getSuccessfulPaymentTransactionByMerchantOrderId(paymentTransaction.getMerchantTxnId());

            Double totalPaidAmount = getTotalPaidAmount(successPayments);

            if (refundAmount.compareTo(totalPaidAmount) > 0) {
                return totalPaidAmount.toString();
            }
        }
        return refundAmount.toString();
    }

    private Double getTotalPaidAmount(List<PaymentTransaction> successPayments) {
        Double paidAmount = new Double("0");
        for (PaymentTransaction paymentTransaction : successPayments) {
            if (paymentTransaction.getIsSuccessful() == null || !paymentTransaction.getIsSuccessful()) {
                continue;
            }
            if (PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(paymentTransaction.getPaymentGateway())) {
                return paymentTransaction.getAmount();
            }
            paidAmount += paymentTransaction.getAmount();
        }
        return paidAmount;
    }

    @Transactional
    public PaymentTransaction getPgTxnToBeRefunded(PaymentTransaction paymentTransaction) throws Exception {
        List<PaymentTransaction> payList = paymentTransactionService.getSuccessfulPaymentTransactionsByOrderId(paymentTransaction.getOrderId());
        if (!FCUtil.isEmpty(payList)) {
            for (PaymentTransaction payTransaction : payList) {
                if (!paymentTransactionService.isTransactionThroughWallet(payTransaction.getPaymentType())) {
                    PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(payTransaction.getOrderId());
                    if (paymentPlan.getPgAmount().getAmount().doubleValue() != payTransaction.getAmount().doubleValue()) {
                        logger.error("PG amount does not match with payment plan for order ID :["
                                + payTransaction.getOrderId() + "]. Not refunding anything");
                        throw new IllegalStateException();
                    }
                    return payTransaction;
                }
            }
        }
        return null;
    }

    @Transactional
    public PaymentRefundBusinessDO doRefundToBank(PaymentTransaction paymentTransaction, Double refundAmount,
            boolean enableRefundMail, String refundType) throws Exception {
        logger.info("request for debit amount for order id " + paymentTransaction.getOrderId());
        
        String paymentGateway = paymentTransaction.getPaymentGateway();
        IGatewayHandler paymentGatewayHandler = pgFactory.getPaymentHandler(paymentGateway);
        PaymentRefundBusinessDO paymentRefundBusinessDO = paymentTransactionService
                .createPaymentRefundBusinessDO(paymentTransaction);
        paymentRefundBusinessDO.setRefundAmount(refundAmount);
        if (refundAmount <= 0) {
            logger.info("Refund amount is less than or zero for Order ID :[" + paymentTransaction.getOrderId()
                    + "]. Refund Amount[" + refundAmount + "]. Not going ahead with refund");
            return paymentRefundBusinessDO;
        }
        List<PaymentRefundTransaction> refundedTransaction = pgReadDAO.getSuccessfulPgRefundsForOrderId(
                paymentRefundBusinessDO.getOrderId(), paymentGateway);
        Double totalRefundedAmount = FCUtil.calculateTotalRefunds(refundedTransaction);
        if (refundedTransaction != null && !refundedTransaction.isEmpty() &&
                totalRefundedAmount.equals(paymentTransaction.getAmount())) {
            logger.info("Payment order in question has already been successfully refunded returning from PaymentRefund.doRefund for order ID "
                    + paymentRefundBusinessDO.getOrderId());
            //paymentRefundBusinessDO.setStatus(true);
            paymentRefundBusinessDO.setRefundAmount(refundedTransaction.get(0).getRefundedAmount().getAmount()
                    .doubleValue());

            return paymentRefundBusinessDO;
        }
        if (refundAmount > paymentTransaction.getAmount()) {
            logger.error("Refund amount should be less than or equal to PG amount for orderId: " + paymentTransaction.getOrderId());
            return paymentRefundBusinessDO;
        }
        paymentGatewayHandler.refundPayment(paymentRefundBusinessDO, refundType);
        notificationsForRefundToBank(paymentRefundBusinessDO, enableRefundMail);
        return paymentRefundBusinessDO;
    }
    
    private void sendRefundMail(PaymentRefundBusinessDO paymentRefundBusinessDO) {

        try {
            paymentMailerService.sendRefundMail(paymentRefundBusinessDO);
            logger.debug("[NOTIFICATIONS:" + paymentRefundBusinessDO.getOrderId() + "] Email sent for Order ID: "
                    + paymentRefundBusinessDO.getOrderId());
        } catch (Exception e) {
            logger.error("Error in sending email notification for Order ID: " + paymentRefundBusinessDO.getOrderId()
                    + " Exception : " + e.getMessage());
        }

    }

    private void sendRefundSms(PaymentRefundBusinessDO paymentRefundBusinessDO) {

        String mobileNumber = findRefundSmsMobileNumber(paymentRefundBusinessDO);

        try {
            String refundAmount = String.valueOf(paymentRefundBusinessDO.getRefundAmount());
            
            sendRefundSms(paymentRefundBusinessDO.getOrderId(), refundAmount, mobileNumber);
            
            logger.debug("[NOTIFICATIONS:" + paymentRefundBusinessDO.getOrderId() + "] Sms sent for Order ID: "
                    + paymentRefundBusinessDO.getOrderId() + "  to Mobile Number: " + mobileNumber);
        } catch (Exception e) {
            logger.error("Error in sending SMS notification for Order ID: " + paymentRefundBusinessDO.getOrderId()
                    + " Exception : " + e.getMessage());
        }

    }

    private String findRefundSmsMobileNumber(PaymentRefundBusinessDO paymentRefundBusinessDO) {
        
        String mobileNumber = smsService.getProfileNumber(paymentRefundBusinessDO.getLookupID());
        
        try {
            if (mobileNumber == null || mobileNumber.isEmpty() || mobileNumber.equals("null")) {
                String mobileServiceNumber = cartService.getMobileNumber(paymentRefundBusinessDO.getOrderId());
                logger.info("mobileServiceNumber value : " + mobileServiceNumber);
                mobileNumber = mobileServiceNumber;
            }
        } catch (Exception exception) {
            logger.error("Error while fetching mobile number from cartService :", exception);
        }
        return mobileNumber;
    }

    /* For refund-to-bank notification */
    private void notificationsForRefundToBank(PaymentRefundBusinessDO paymentRefundBusinessDO,
            boolean enableRefundNotification) {
        logger.info("Started sending notification for refund-to-bank " + paymentRefundBusinessDO.getOrderId());
        try {
            if (paymentRefundBusinessDO.getStatus() && enableRefundNotification) {
                try {
                    paymentMailerService.sendRefundMail(paymentRefundBusinessDO);
                    logger.debug("[NOTIFICATIONS:" + paymentRefundBusinessDO.getOrderId()
                            + "] Email sent for Order ID: " + paymentRefundBusinessDO.getOrderId());
                } catch (Exception e) {
                    logger.error("Error in sending email notification for Order ID: "
                            + paymentRefundBusinessDO.getOrderId() + " Exception : " + e.getMessage());
                }
                String refundAmount = String.valueOf(paymentRefundBusinessDO.getRefundAmount());
                Integer userId = userServiceProxy.getUserFromOrderId(paymentRefundBusinessDO.getOrderId()).getUserId();
                Users user = userServiceProxy.getUserByUserId(userId);
                try {
                    logger.info("Refund-to-bank : Started sending sms for " + paymentRefundBusinessDO.getOrderId()
                            + "to Mobile number : " + user.getMobileNo());
                    sendBankRefundSms(paymentRefundBusinessDO.getOrderId(), refundAmount, user.getMobileNo(), user.getEmail());
                    logger.debug("[NOTIFICATIONS:" + paymentRefundBusinessDO.getOrderId() + "] Sms sent for Order ID: "
                            + paymentRefundBusinessDO.getOrderId() + "  to Mobile Number: " + user.getMobileNo());
                } catch (Exception e) {
                    logger.error("Error in sending SMS notification for Order ID: " + paymentRefundBusinessDO.getOrderId()
                            + " Exception : ",e);
                }
            }
        } catch (Exception e) {
            logger.error("Error in sending Refund-to-bank notifiacation for order id : " + paymentRefundBusinessDO.getOrderId(), e);
        }
    }

    private void sendBankRefundSms(String orderId, String amount, String no, String email) {
        if (no == null || no.isEmpty()) {
            logger.info("Mobile no is null so not sending sms for order id: " + orderId);
            return;
        }
        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        Map<String, String> params = new HashMap<String, String>();
        params.put("orderId", orderId);
        params.put("refundAmount", amount);
        params.put("receiverNumber", no);
        smsBusinessDO.setReceiverNumber(no);
        smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_BANK_REFUND_SUCCESS);
        smsBusinessDO.setVariableValues(params);
        smsBusinessDO.setRegisteredUserEmail(email);
        smsService.sendSMS(smsBusinessDO);
    }
    
    public List<PaymentRefundTransaction> getAllRefundsDoneForPayment(PaymentTransaction paymentTransaction) {
        List<PaymentRefundTransaction> refundList = new ArrayList<>();

        logger.info("Checking if any wallet refund happened for OrderId " + paymentTransaction.getOrderId());
        refundList.addAll(walletWrapper.getRefundTxnFromWallet(paymentTransaction));

        if (!paymentTransaction.isFullFCWalletPayment()) {
            logger.info("Checking if any kpay refund happened for OrderId " + paymentTransaction.getOrderId());
            refundList.addAll(paymentTransactionService.getRefundTxnForKPay(paymentTransaction));
        }
        
        if (refundList.isEmpty() && paymentTransaction.isPGPayment()
                && !PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE.equals(paymentTransaction.getPaymentGateway())) {
            logger.info("Getting the data that is recorded in the payment_refund_transactions table for "
                    + paymentTransaction.getMerchantTxnId());
            refundList.addAll(pgReadDAO.getSuccessfulRefundsForMtxnId(paymentTransaction.getMerchantTxnId()));
        }
        
        return refundList;
    }

    public Double getTotalAmountToBeRefunded(List<PaymentTransaction> successPaymentTxns) throws NumberFormatException, Exception {
        Double amountToBeRefunded = new Double("0");
        Set<String> processedMtxnIds = new HashSet<>();
        for (PaymentTransaction paymentTransaction : successPaymentTxns) {
            if (!processedMtxnIds.contains(paymentTransaction.getMerchantTxnId())) {
                amountToBeRefunded += Double.parseDouble(calculateRefundAmountForPaymentTxn(paymentTransaction));
                processedMtxnIds.add(paymentTransaction.getMerchantTxnId());
            }
        }
        return amountToBeRefunded;
    }
    
    public List<PaymentRefundTransaction> fetchAndUpdateRefunds(List<PaymentTransaction> successPaymentTxns) {
        List<PaymentRefundTransaction> wholeRefundList = new ArrayList<>();
        Set<String> processedMtxn = new HashSet<>();
        for (PaymentTransaction successPaymentTxn : successPaymentTxns) {
            if (!processedMtxn.contains(successPaymentTxn.getMerchantTxnId())) {
                List<PaymentRefundTransaction> refundList = new ArrayList<>();
                refundList.addAll(getAllRefundsDoneForPayment(successPaymentTxn));
                processedMtxn.add(successPaymentTxn.getMerchantTxnId());
                wholeRefundList.addAll(refundList);
                logger.info("Updating the payment_refund_transactions table for mtxnId "
                        + successPaymentTxn.getMerchantTxnId());
                paymentTransactionService.updateFreshPRT(refundList, successPaymentTxn.getMerchantTxnId());
            }
        }
        return wholeRefundList;
    }
    
    public boolean isRefundDone(List<PaymentTransaction> successPaymentTxns) {
        try {
            List<PaymentRefundTransaction> refundList = fetchAndUpdateRefunds(successPaymentTxns);
            Double amountToBeRefunded;
            amountToBeRefunded = getTotalAmountToBeRefunded(successPaymentTxns);
            Double totalRefundedAmount = FCUtil.calculateTotalRefunds(refundList);
            logger.info("AmountToBeRefunded = " + amountToBeRefunded + ", ActualAmountRefunded : "
                    + totalRefundedAmount + " for orderId " + successPaymentTxns.get(0).getOrderId());
            return amountToBeRefunded.compareTo(totalRefundedAmount) == 0;
        } catch (Exception e) {
            logger.error("Failed to check if refund happend or not.", e);
        }
        return false;
    }

    public Double calculateRefundAmountForMtxnId(String mtxnId) {
        List<PaymentTransaction> successfulPaymentTransactionList =
                paymentTransactionService.getSuccessfulPaymentTransactionByMerchantOrderId(mtxnId);
        Double totalPaidAmount = FCUtil.calculateTotalPaid(successfulPaymentTransactionList);
        Double totalRefundedAmount = paymentTransactionService.getAlreadyRefundedAmountForMtxnId(mtxnId);
        Double totalRefundAmount = totalPaidAmount - totalRefundedAmount;
        return totalRefundAmount;
    }

    public Boolean isEligibleForNewRefund(String mtxnId) {
        List<PaymentTransaction> paymentTransactionList =
                paymentTransactionService.getSuccessfulPaymentTransactionByMerchantOrderId(mtxnId);
        if(!FCUtil.isEmpty(paymentTransactionList)) {
            for(PaymentTransaction paymentTransaction : paymentTransactionList) {
                if(paymentTransaction.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                    return false;
                }
            }
            String orderIdString = paymentTransactionList.get(0).getOrderId();
            CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderIdString);
            if(null != cartToInspect) {
                UserProfile userProfile = userServiceProxy.getUserProfile(cartToInspect.getUserId());
                Users user = null;
                if ((userProfile != null) && (userProfile.getFkUserId() != null)) {
                    user = userServiceProxy.getUserByUserId(userProfile.getFkUserId());
                } else {
                    user = userServiceProxy.getUserByUserId(cartToInspect.getUserId());
                }
                if (user != null) {
                    String fcWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
                    if (fcWalletId != null) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Boolean isOrderRefundedByNewRefundFlow(String orderId, List<PaymentTransaction> successPaymentTransactionList) {
        List<String> processedMtxnIds = new ArrayList<String>();
        for(PaymentTransaction paymentTransaction: successPaymentTransactionList) {
            if(!processedMtxnIds.contains(paymentTransaction.getMerchantTxnId())) {
                if(isMtxnRefundedByNewRefundFlow(paymentTransaction.getMerchantTxnId())) {
                    return true;
                }
                processedMtxnIds.add(paymentTransaction.getMerchantTxnId());
            }
        }
        return false;
    }

    public Boolean isMtxnRefundedByNewRefundFlow(String mtxnId) {
        logger.info("Fetching New Refund Status for mtxnId: " + mtxnId);
        StringBuilder restArg = new StringBuilder();
        StringBuilder restUri = new StringBuilder();
        String result = null;
        Boolean response = false;
        restArg.append(fetchFromMtxnArg).append(mtxnId);
        restUri.append(fcProperties.getProperty(FCProperties.NEW_REFUND_URI)).append(REFUND_STATUS_URI).append(restArg);
        logger.info("Refund status fetch URI: " + restUri);
        try {
            RestTemplate restTemplate = new RestTemplate();
            String stringObj = restTemplate.getForObject(new URI(restUri.toString()), String.class);
            logger.info("stringObj: " + stringObj + " received from refund for mtxn id: " + mtxnId);
            result = stringObj;
        } catch (URISyntaxException e) {
            String error = "Service threw an exception while forming URI for fetching refund status results via " +
                    "rest call for mtxn id:" + mtxnId;
            logger.error(error, e);
            result = ERROR;
        } catch (Exception e) {
            String error = "Service threw an exception when fetching refund status results via " +
                    "rest call for mtxn id:" + mtxnId;
            logger.error(error, e);
            result = ERROR;
        }
        finally {
            if(FCUtil.isEmpty(result) || result.equals(REFUND_NOT_ATTEMPTED)) {
                response = false;
            } else {
                response = true;
            }
        }
        return response;
    }
}
