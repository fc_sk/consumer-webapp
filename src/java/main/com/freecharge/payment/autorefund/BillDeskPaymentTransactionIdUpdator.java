package com.freecharge.payment.autorefund;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.recharge.businessdao.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 23/10/12
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class BillDeskPaymentTransactionIdUpdator {
    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    @Autowired
    private EasyDBWriteDAO easyDBWriteDAO;

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        BillDeskPaymentTransactionIdUpdator billDeskPaymentTransactionIdUpdator = ((BillDeskPaymentTransactionIdUpdator) ctx.getBean("billDeskPaymentTransactionIdUpdator"));
        File csvFile = new File(args[0]);
        Result result = billDeskPaymentTransactionIdUpdator.update(csvFile);
        System.out.println(result);
        ctx.close();
    }

    public Result update(File csvFile) throws Exception {
        Collection<MerchantOrderIdPaymentTransactionId> merchantOrderIdPaymentTransactionIds = this.getOrderIdPaymentTransactionIds(csvFile);
        return this.update(merchantOrderIdPaymentTransactionIds);
    }

    public Result update(String csvString) throws Exception {
        Collection<MerchantOrderIdPaymentTransactionId> merchantOrderIdPaymentTransactionIds = this.getOrderIdPaymentTransactionIds(csvString);
        return  this.update(merchantOrderIdPaymentTransactionIds);
    }

    private Collection<MerchantOrderIdPaymentTransactionId> getOrderIdPaymentTransactionIds(String csvData) throws Exception {
        String[] lines = csvData.split(FCUtil.getNewLineCharacter());

        List<MerchantOrderIdPaymentTransactionId> merchantOrderIdPaymentTransactionIds = new LinkedList<MerchantOrderIdPaymentTransactionId>();

        for (String line : lines) {
            String[] orderIdMerchantTransactionId = line.split(",");
            String merchantOrderId = StringUtils.trim(orderIdMerchantTransactionId[1]);
            String paymentTransactionId = StringUtils.trim(orderIdMerchantTransactionId[0]);

            MerchantOrderIdPaymentTransactionId merchantOrderIdPaymentTransactionId = new MerchantOrderIdPaymentTransactionId();
            merchantOrderIdPaymentTransactionId.orderId = merchantOrderId;
            merchantOrderIdPaymentTransactionId.paymentTransactionId = paymentTransactionId;

            merchantOrderIdPaymentTransactionIds.add(merchantOrderIdPaymentTransactionId);
        }

        return merchantOrderIdPaymentTransactionIds;
    }

    private Collection<MerchantOrderIdPaymentTransactionId> getOrderIdPaymentTransactionIds(File csvFile) throws Exception {
        String csvString = FileUtils.readFileToString(csvFile);
        return this.getOrderIdPaymentTransactionIds(csvString);
    }

    private Result update(Collection<MerchantOrderIdPaymentTransactionId> merchantOrderIdPaymentTransactionIds) {
        Map<String, String> merchantOrderIdPaymentTransactionIdMap = MerchantOrderIdPaymentTransactionId.convertToOrderIdMerchantOrderIdMap(merchantOrderIdPaymentTransactionIds);

        Map<String, Integer> results = new HashMap<String, Integer>();
        Map<String, Integer> duplicates = new HashMap<String, Integer>();

        for (String merchantOrderId : merchantOrderIdPaymentTransactionIdMap.keySet()) {
            PaymentTransaction paymentTransaction = new PaymentTransaction();
            paymentTransaction.setMerchantTxnId(merchantOrderId);

            List<PaymentTransaction> paymentTransactions = easyDBWriteDAO.get(paymentTransaction);

            if (paymentTransactions.size() > 1) {
                logger.fatal(String.format("More than one payment transaction found for %s merchant order id. Has %d transactions.", merchantOrderId, paymentTransactions.size()));
                duplicates.put(merchantOrderId, paymentTransactions.size());
                continue;
            }

            paymentTransaction.addToWhereFields("merchantTxnId");
            paymentTransaction.setTransactionId(merchantOrderIdPaymentTransactionIdMap.get(merchantOrderId));

            int count = easyDBWriteDAO.update(paymentTransaction);

            results.put(merchantOrderId, count);
        }

        Result result = this.convertToResult(results);
        result.duplicateMerchantTransactionIds = duplicates;

        return result;
    }

    private Result convertToResult(Map<String, Integer> updationResult) {
        List<String> successes = new LinkedList<String>();
        List<String> failures = new LinkedList<String>();

        for (Map.Entry<String, Integer> entry : updationResult.entrySet()) {
            if (entry.getValue() > 0) {
                successes.add(entry.getKey());
            } else {
                failures.add(entry.getKey());
            }
        }

        Result result = new Result();
        result.successOrderIds = successes;
        result.failedOrderIds = failures;

        return result;
    }

    private static class MerchantOrderIdPaymentTransactionId {
        String orderId;
        String paymentTransactionId;

        @Override
        public String toString() {
            return "MerchantOrderIdPaymentTransactionId{" +
                    "orderId='" + orderId + '\'' +
                    ", paymentTransactionId='" + paymentTransactionId + '\'' +
                    '}';
        }

        private static Map<String, String> convertToOrderIdMerchantOrderIdMap(Collection<MerchantOrderIdPaymentTransactionId> merchantOrderIdPaymentTransactionIds) {
            Map<String, String> orderIdMerchantOrderId = new HashMap<String, String>();

            for (MerchantOrderIdPaymentTransactionId idMerchantMerchantOrderIdPaymentTransactionId : merchantOrderIdPaymentTransactionIds) {
                orderIdMerchantOrderId.put(idMerchantMerchantOrderIdPaymentTransactionId.orderId, idMerchantMerchantOrderIdPaymentTransactionId.paymentTransactionId);
            }

            return orderIdMerchantOrderId;
        }

    }

    public static class Result {
        private Map<String, Integer> duplicateMerchantTransactionIds = new HashMap<String, Integer>();
        private List<String> successOrderIds = new LinkedList<String>();
        private List<String> failedOrderIds = new LinkedList<String>();

        public Map<String, Integer> getDuplicateMerchantTransactionIds() {
            return duplicateMerchantTransactionIds;
        }

        public void setDuplicateMerchantTransactionIds(Map<String, Integer> duplicateMerchantTransactionIds) {
            this.duplicateMerchantTransactionIds = duplicateMerchantTransactionIds;
        }

        public List<String> getSuccessOrderIds() {
            return successOrderIds;
        }

        public void setSuccessOrderIds(List<String> successOrderIds) {
            this.successOrderIds = successOrderIds;
        }

        public List<String> getFailedOrderIds() {
            return failedOrderIds;
        }

        public void setFailedOrderIds(List<String> failedOrderIds) {
            this.failedOrderIds = failedOrderIds;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "duplicateMerchantTransactionIds=" + duplicateMerchantTransactionIds +
                    ", successOrderIds=" + successOrderIds +
                    ", failedOrderIds=" + failedOrderIds +
                    '}';
        }
    }

    public PaymentTransactionService getPaymentTransactionService() {
        return paymentTransactionService;
    }

    public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
        this.paymentTransactionService = paymentTransactionService;
    }

    public EasyDBWriteDAO getEasyDBWriteDAO() {
        return easyDBWriteDAO;
    }

    public void setEasyDBWriteDAO(EasyDBWriteDAO easyDBWriteDAO) {
        this.easyDBWriteDAO = easyDBWriteDAO;
    }
}
