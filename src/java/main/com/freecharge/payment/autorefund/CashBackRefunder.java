package com.freecharge.payment.autorefund;

import java.io.BufferedReader;
import java.io.FileReader;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.StringUtils;

/**
 * This class handles refunds that are part of cashback offers.
 * Given an excel sheet as input with order ID and amount in each
 * line needful is done. Note that caller is assumed to have done
 * due validation on payment being successful.
 * @author arun
 *
 */
public class CashBackRefunder {
    private static final boolean DISABLE_REFUND_MAIL = false;

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentRefund paymentRefund = ((PaymentRefund) ctx.getBean("paymentRefund"));
        PaymentTransactionService paymentTransactionService = ctx.getBean(PaymentTransactionService.class);

        String csv = args[0];

        BufferedReader bufferedReader = new BufferedReader(new FileReader(csv));
        String line;

        while ((line = bufferedReader.readLine()) != null) {
            String refundData = StringUtils.trim(line);
            
            String[] split = refundData.split(",");
            
            String mtxnId = split[0];
            Double refundAmount = Double.parseDouble(split[1]);
            
            try {
                System.out.println("Refunding [" + mtxnId + "," + refundAmount + "]");
                PaymentTransaction uniquePaymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(mtxnId);
                paymentRefund.doRefund(uniquePaymentTransaction, refundAmount, DISABLE_REFUND_MAIL, PaymentConstants.ADMIN_CASH_BACK);
                
            } catch (Exception e) {
                System.out.println("Something BAD happened while refunding [" + mtxnId + "," + refundAmount + "]. Going ahead with rest of the orders");
            }
        }
        
        bufferedReader.close();
        ctx.close();
    }

}
