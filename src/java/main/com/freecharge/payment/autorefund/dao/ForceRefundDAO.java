package com.freecharge.payment.autorefund.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.recharge.exception.NetworkConnectTimeoutException;

@Component
public class ForceRefundDAO {

	private NamedParameterJdbcTemplate jt;
	Logger logger = Logger.getLogger(ForceRefundDAO.class);

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jt = new NamedParameterJdbcTemplate(dataSource);
	}

	private static final String INSERT_REFUND_ATTEMPT = "INSERT INTO force_refund_attempts"
			+ "(order_id,amount,created_on)" + "VALUES(:order_id,:amount,:created_on)";

	public void recordForceRefundAttempt(String orderId, Double transactionAmount) {
		try {
			Map<String, Object> insertParams = new HashMap<String, Object>();
			insertParams.put("order_id", orderId);
			insertParams.put("amount", transactionAmount);
			insertParams.put("created_on", new Timestamp(new Date().getTime()));
			jt.update(INSERT_REFUND_ATTEMPT, insertParams);
		} catch (Exception e) {
			logger.error("Caught exception in recording Refund Attempt", e);
		}

	}

}
