package com.freecharge.payment.autorefund;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;

@Component
public class ForceRefundConfigCache extends RedisCacheManager {

    private String getAmountRefundedKey() {
        String key = new SimpleDateFormat("yyyy_MM_dd").format(new Date()).toString() + "_FORCE_REFUND_AMOUNT";
        return key;
    }

    public Long getAmountRefundedTillNow() {
        String key = getAmountRefundedKey();
        return (Long) this.get(key);
    }

    public void incrementAmountRefundedTillNow(Long amountRefunded) {
        String key = getAmountRefundedKey();
        this.increment(key, amountRefunded);
        this.expireAt(key, new DateTime().plusHours(24).toDate());
    }

}
