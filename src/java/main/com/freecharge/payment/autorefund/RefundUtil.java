package com.freecharge.payment.autorefund;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.PaymentTransaction;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 6/10/12
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundUtil {
    private static final Logger logger = LoggingFactory.getLogger(RefundUtil.class);
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date getDate(String dateString) throws ParseException {
        return new SimpleDateFormat(DATE_FORMAT).parse(dateString);
    }

    public static String getDate(Date date) throws ParseException {
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    public static void printOrderList(List<PaymentTransaction> successfulTransactionList, String gatewayName) {
        logger.info(String.format("------------------------------<%s>------------------------------", gatewayName));

        for (PaymentTransaction paymentTransaction : successfulTransactionList) {
            logger.info(String.format("Order id:%s, Merchant transaction id:%s, Pg transaction id:%s",
                    paymentTransaction.getOrderId(), paymentTransaction.getMerchantTxnId(), paymentTransaction.getTransactionId()) );
        }

        logger.info(String.format("------------------------------<%s> Finished------------------------------", gatewayName));
    }
}
