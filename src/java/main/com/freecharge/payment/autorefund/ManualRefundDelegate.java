package com.freecharge.payment.autorefund;

import java.io.File;
import java.io.FileReader;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.bytecode.opencsv.CSVReader;

import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;

import flexjson.JSONSerializer;

public class ManualRefundDelegate implements WebDelegate {

    @Autowired
    private FCProperties properties;

    @Autowired
    private PaymentRefund paymentRefund;

    JSONSerializer serializer = new JSONSerializer();

    @Autowired
    private EmailService emailService;

    private static Logger logger = LoggingFactory.getLogger(ManualRefundDelegate.class);

    public void delegate(WebContext webContext) throws Exception {
        String email = "";
        Calendar cals = Calendar.getInstance();
        long timeValue = cals.getTimeInMillis();
        logger.info("In delegate for refund...");
       // final List<String> results = new LinkedList<String>();
        Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
        String refundId = "Refunded for Id(Time in MilliSeconds) : " + timeValue;

      //  RefundThreadPoolExecutor executor = new RefundThreadPoolExecutor();
        HashMap<String, Object> resultMap = handleSubmit(webContext, email, timeValue);
        Set<String> orderIds = (Set<String>) resultMap.get("orderids");
        email = (String) resultMap.get("email");

        if (orderIds == null || orderIds.size() == 0) {
            webContext.getModel().addAttribute("results", "No orderId present in the request for refund");
            return;
        }

        Iterator<String> itr = orderIds.iterator();
        while (itr.hasNext()) {
            final String orderIdForReund = itr.next();
            List<String> results = new LinkedList<String>();
            logger.info("Refunding thread for order id : " + orderIdForReund);
            try {
                PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doRefund(orderIdForReund);
                if (paymentRefundBusinessDO != null) {
                    if (paymentRefundBusinessDO.getStatus()) {
                        results.add("Refunded successfully For : " + orderIdForReund);
                        results.add("Total Transaction Amount: "+paymentRefundBusinessDO.getAmount());
                        results.add("Refunded Amount: " +paymentRefundBusinessDO.getRefundAmount());
                        results.add("Retained Amount:(crossell): "+(paymentRefundBusinessDO.getAmount()-paymentRefundBusinessDO.getRefundAmount()));
                    } else {
                        results.add("refund unsuccessful. Message from PG" + orderIdForReund + " " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPgMessage());
                    }
                } else {
                    results.add("Transaction reference for orderid : " + orderIdForReund + " not found");
                }
            } catch (Exception exp) {
                logger.error("Exception raised in refunding for order id : " + orderIdForReund, exp);
            }
            resultObjectMap.put(orderIdForReund, results);
            
        }

        
        webContext.getModel().addAttribute("refundId", refundId);
        webContext.getModel().addAttribute("resultObjectMap",resultObjectMap);
        sendEmail(resultObjectMap, email, timeValue);
    }

    private Set<String> getOrderIdsFromCSV(String location) {
        String columnName = properties.getProperty("refund.upload.file.columnName");
        Set<String> orderIds = new HashSet<String>();
        try {
            CSVReader reader = new CSVReader(new FileReader(location));
            // this is the first line, may be of header
            Integer columnIndex = null;
            String[] nextLine = reader.readNext();
            if (nextLine == null) {
                logger.error("File is empty or top line is not there");
                return orderIds;
            }

            for (int i = 0; i < nextLine.length; i++) {
                if (columnName != null && (columnName.equalsIgnoreCase(StringUtils.trim(nextLine[i])) || columnName.equalsIgnoreCase(nextLine[i]))) {
                    columnIndex = i;
                    break;
                }
            }

            while ((nextLine = reader.readNext()) != null) {
                if (StringUtils.isNotBlank(nextLine[columnIndex]))
                    orderIds.add(StringUtils.trim(nextLine[columnIndex]));
            }
        } catch (Exception exp) {
            logger.error("Exception is reading from a csv file", exp);
        }

        return orderIds;
    }

    public static Set<String> getOrderIds(String orderIdsString) {

        String orderIds[] = orderIdsString.split(",");

        Set<String> orderIdsCollection = new HashSet<String>();
        for (String orderId : orderIds) {
            orderId = StringUtils.trim(orderId);
            if (StringUtils.isNotBlank(orderId)) {
                orderIdsCollection.add(orderId);
            }
        }

        return orderIdsCollection;
    }

    public HashMap<String, Object> handleSubmit(WebContext webContext, String email, long timeValue) {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        Set<String> setOrderIds = null;
        String orderIds = null;
        String fileDirectory = properties.getProperty("refund.upload.file.dir");

        String location = "";
        File uploadFile;

        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload fileUpload = new ServletFileUpload(factory);
            List items = fileUpload.parseRequest(webContext.getRequest());
            Iterator ir = items.iterator();
            while (ir.hasNext()) {
                FileItem item = (FileItem) ir.next();
                String name = item.getFieldName();
                if (item.isFormField()) {
                    if (name != null) {
                        if (name.equals("orderIds")) {
                            orderIds = item.getString();
                            setOrderIds = getOrderIds(orderIds);
                        }
                        if (name.equals("email")) {
                            email = item.getString();
                        }
                    }
                } else {
                    String fileParamter = item.getFieldName();
                    String fileName = item.getName();
                    location = fileDirectory + File.separator + timeValue + "_" + fileName;
                    uploadFile = new File(location);
                    long size = item.getSize();
                    if (size <= 1024 * 1024 * 1024) {
                        item.write(uploadFile);
                    } else {
                        logger.error("Your File is not uploaded.File size should be less than 1GB, " + location);
                        throw new Exception("Your File is not uploaded.File size should be less than 1GB, " + location);
                    }
                    setOrderIds = getOrderIdsFromCSV(location);
                }
            }
        } catch (Exception e) {
            logger.error("Exception in refund ", e);
        }
        webContext.getModel().addAttribute("email", email);
        resultMap.put("orderids", setOrderIds);
        resultMap.put("email", email);
        return resultMap;
    }

    public void sendEmail(List<String> results, String email, long timeValue) {
        logger.info("In sending mail for id: " + timeValue);
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setSubject(properties.getProperty(FCConstants.REFUND_SUCCESS_SUBJECT) + timeValue);
        emailBusinessDO.setTo(email);
        emailBusinessDO.setFrom(properties.getProperty(FCConstants.REFUND_SUCCESS_FROM));
        emailBusinessDO.setReplyTo(properties.getProperty(FCConstants.REFUND_SUCCESS_REPLYTO));
        emailBusinessDO.setBcc(properties.getProperty(FCConstants.REFUND_SUCCESS_CC));
        emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
        if (results != null && results.size() > 0)
            emailBusinessDO.setContent(StringUtils.join(results, FCUtil.getNewLineCharacter()));
        emailService.sendNonVelocityEmail(emailBusinessDO);
    }
    public void sendEmail(Map<String, List<String>> resultObjectMap, String email, long timeValue) {
        logger.info("In sending mail for id: " + timeValue);
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setSubject(properties.getProperty(FCConstants.REFUND_SUCCESS_SUBJECT) + timeValue);
        emailBusinessDO.setTo(email);
        emailBusinessDO.setFrom(properties.getProperty(FCConstants.REFUND_SUCCESS_FROM));
        emailBusinessDO.setReplyTo(properties.getProperty(FCConstants.REFUND_SUCCESS_REPLYTO));
        emailBusinessDO.setBcc(properties.getProperty(FCConstants.REFUND_SUCCESS_CC));
        emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
        
        StringBuffer sbf = new StringBuffer("");
        if ( resultObjectMap != null &&  resultObjectMap.size() > 0) {
        	for(String key : resultObjectMap.keySet()) {
        		List<String> results = resultObjectMap.get(key);
        		sbf.append("================"+key+"==============");
        		sbf.append(StringUtils.join(results, FCUtil.getNewLineCharacter()));
        		sbf.append("==============================");
        	}
        	 emailBusinessDO.setContent(sbf.toString());
        }
            
        emailService.sendNonVelocityEmail(emailBusinessDO);
    }

}
