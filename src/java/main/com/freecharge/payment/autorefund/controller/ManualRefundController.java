package com.freecharge.payment.autorefund.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freecharge.virtualcard.VirtualCardService;
import com.freecharge.wallet.service.OneCheckUserService;
import com.freecharge.wallet.WalletWrapper;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.payments.sdmoney.service.model.Transaction;
import com.snapdeal.payments.sdmoney.service.model.type.TransactionType;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.admin.controller.BaseAdminController;
import com.freecharge.admin.enums.AdminComponent;
import com.freecharge.admin.helper.AdminHelper;
import com.freecharge.admin.service.CTActionControlService;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.freefund.dos.PromocodeRedeemDetailsSNSDo;
import com.freecharge.freefund.services.PromocodeRedeemDetailsSNSService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.autorefund.AutoRefundPaymentGatewayFailures;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.autorefund.form.ManualRefundForm;
import com.freecharge.payment.autorefund.form.ManualRefundFormValidator;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dao.jdbc.PgWriteDAO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction.Status;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.ActivityLogger;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.onecheck.model.WalletResponseDo;
import com.freecharge.wallet.CampaignCashRewardService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.web.util.ViewConstants;
import com.snapdeal.payments.sdmoney.service.model.TransactionSummary;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 26/10/12 Time: 11:08 AM To
 * change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/refund/manual/*")
public class ManualRefundController extends BaseAdminController {

	@Autowired
	AutoRefundPaymentGatewayFailures autoRefundPaymentGatewayFailures;

	@Autowired
	VirtualCardService virtualCardService;

	@Autowired
	OneCheckUserService oneCheckUserService;

	@Autowired
	private PaymentRefund paymentRefund;

	@Autowired
	private AppConfigService appConfig;

	@Autowired
	private EmailService emailService;

	@Autowired
	private InReadDAO inReadDao;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private MetricsClient metricsClient;

	@Autowired
	TxnHomePageDAO txnHomePageDAO;

	@Autowired
	private FreechargeEventPublisher freechargeEventPublisher;

	@Autowired
	private AdminHelper adminHelper;

	@Autowired
	private CTActionControlService ctActionControlService;

	@Autowired
	UserTransactionHistoryService userTransactionHistoryService;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private CampaignCashRewardService campaignCashRewardService;

	@Autowired
	private OrderIdSlaveDAO orderIdDAO;

	@Autowired
	PgReadDAO pgReadDAO;

	@Autowired
	PgWriteDAO pgWriteDAO;

	@Autowired
	OneCheckWalletService oneCheckWalletService;

	@Autowired
	PaymentPlanService paymentPlanService;

	@Resource(name = "clientNameToCorpIdMap")
	private HashMap<String, String> clientNameToCorpIdMap;

	private static Logger logger = LoggingFactory.getLogger(ManualRefundController.class);

	public static final String MANUAL_REFUND_TOOL = "/admin/refund/manual/doNewRefund.htm";

	private static final boolean DISABLE_REFUND_MAIL = false;

	public static final String CASHBACK_TOOL = "/admin/refund/manual/doCashbackRefund.htm";

	public static final String METRIC_SERVICE_R = "Refund";

	private static final String METRIC_NAME = "ManualRefund";

	public static final String CASH_BACK = "/admin/refund/manual/doCashback.htm";
	public static final String CUSTOMER_TRAIL_LINK = "/admin/customertrail/getcustomertrail.htm";

	private static final int ARRAY_SIZE_ONE = 1;
	private static final int ARRAY_SIZE_TWO = 2;
	private static final int ARRAY_SIZE_THREE = 3;
	private static final int ARRAY_INDEX_ZERO = 0;
	private static final int ARRAY_INDEX_ONE = 1;
	private static final int ARRAY_INDEX_TWO = 2;
	private static final int ARRAY_INDEX_THREE = 3;

	private static final String ACTION_FROM_CUSTOMERTRAIL = "customertrail";

	private static final int ORDER_STATUS_SUCCESS = 1;
	private static final int ORDER_STATUS_ENQUEUED = 0;

	@Autowired
	private FCProperties fcproperties;

	@Autowired
	private PromocodeRedeemDetailsSNSService promocodeRedeemDetailsSNSService;

	@Autowired
	private KestrelWrapper kestrelWrapper;

	@RequestMapping(method = RequestMethod.GET)
	public final ModelAndView home(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (!hasAccess(request, AdminComponent.MANUAL_REFUND)) {
			get403HtmlResponse(response);
			return null;
		}
		return new ModelAndView(ViewConstants.MANUAL_REFUND_HOME, "command", new ManualRefundForm());
	}

	@RequestMapping(value = "doRefund", method = RequestMethod.POST)
	public final ModelAndView doRefund(@ModelAttribute("command") final ManualRefundForm manualRefundForm,
			final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (!hasAccess(request, AdminComponent.MANUAL_REFUND)) {
			get403HtmlResponse(response);
			return null;
		}
		ManualRefundFormValidator manualRefundFormValidator = new ManualRefundFormValidator();
		manualRefundFormValidator.validate(manualRefundForm, bindingResult);

		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView(ViewConstants.MANUAL_REFUND_HOME, "command", manualRefundForm);
			return modelAndView;
		}

		Set<String> orderIds = getOrderIdsFromRefundForm(manualRefundForm);
		for (String orderId : orderIds) {
			logger.info("Starting refund for order : " + orderId);
		}
		PaymentRefund paymentRefundObject = new PaymentRefund();
		List<PaymentRefundBusinessDO> paymentRefundBusinessDOs = paymentRefundObject.doRefund(orderIds,
				PaymentConstants.MANUAL_REFUND);
		List<String> results = new LinkedList<String>();
		for (PaymentRefundBusinessDO paymentRefundBusinessDO : paymentRefundBusinessDOs) {
			if (paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getSuccessful()) {
				results.add(String.format("%s refunded successfully.",
						paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getOrderId()));
			} else {
				results.add(String.format("%s refund unsuccessful. Message from PG %s.",
						paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getOrderId(),
						paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPgMessage()));
			}
		}

		ModelAndView modelAndView = new ModelAndView(ViewConstants.MANUAL_REFUND_RESULT);
		modelAndView.addObject("results", results);
		return modelAndView;
	}

	@RequestMapping(value = "doNewRefund", method = RequestMethod.POST)
	public final String doNewRefund(@RequestParam("refundMtxnids") final MultipartFile file,
			@RequestParam(required = false, value = "orderIds") final String orderIdsorRequestIdsStr,
			@RequestParam(required = false, value = "email") final String email,
			@RequestParam(required = false, value = "radio_names") final String fileType, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.MANUAL_REFUND)) {
			get403HtmlResponse(response);
			return null;
		}
		String returnView = ViewConstants.MANUAL_REFUND_NEW;
		String refundId = null;

		try {
			Calendar cals = Calendar.getInstance();
			long timeValue = cals.getTimeInMillis();
			Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
			refundId = "Refunded for Id(Time in MilliSeconds) : " + timeValue;
			Set<String> mtxnIds = new HashSet<String>();
			InRequest inRequest = new InRequest();
			List<PaymentTransaction> paymentTransaction = new ArrayList<PaymentTransaction>();
			Set<String> orderIdsorRequestIds = getOrderIds(orderIdsorRequestIdsStr);
			if (orderIdsorRequestIds.isEmpty()) {
				BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
				String line = "";
				while ((line = br.readLine()) != null) {
					orderIdsorRequestIds.add(line);
				}
			}

			if (fileType.equalsIgnoreCase("request_id")) {
				for (String inRequests : orderIdsorRequestIds) {
					inRequest = inReadDao.getInRequest(Long.parseLong(inRequests));
					paymentTransaction = paymentTransactionService
							.getSuccessfulPaymentTransactionsByOrderId(inRequest.getAffiliateTransId());
					if (paymentTransaction != null) {
						for (PaymentTransaction paymentTransactions : paymentTransaction) {
							mtxnIds.add(paymentTransactions.getMerchantTxnId());
						}
					}
				}
				orderIdsorRequestIds = mtxnIds;
			}

			resultObjectMap = processManualRefund(orderIdsorRequestIds);

			model.addAttribute("refundId", refundId);
			model.addAttribute("resultObjectMap", resultObjectMap);
			sendEmail(resultObjectMap, email.trim(), timeValue);

			if (resultObjectMap != null && resultObjectMap.size() == 1) {
				adminHelper.sendAlertEmail(resultObjectMap, "Manual refund From CustomerTrail",
						getCurrentAdminUser(request));
			}
			returnView = ViewConstants.MANUAL_REFUND_NEW_RESULT;
		} catch (Exception exp) {
			logger.error("Exception in refunding - Ids" + refundId + ":", exp);
			returnView = ViewConstants.MANUAL_REFUND_NEW;
		}

		return returnView;
	}

	@RequestMapping(value = "bulk/statuscheck/view", method = RequestMethod.GET)
	public final String getBulkStatusCheckView(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		String view = "admin/tool/bulkpg/statuscheck/viewpage";
		if (!hasAccess(request, AdminComponent.BULK_PG_STATUS_CHECK)) {
			return get403HtmlResponse(response);
		}
		return view;
	}

	@RequestMapping(value = "bulk/statuscheck/result", method = RequestMethod.POST)
	public final String doBulkStatusCheck(@RequestParam("fileInputIds") final MultipartFile file,
			@RequestParam(required = false, value = "inputIds") final String inputIdsString, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		String view = "admin/tool/bulkpg/statuscheck/viewpage";
		if (!isAuthorizedToUpdateCt(request)) {
			logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel for paystatuscheck");
			return null;
		}
		Set<String> inputIdSet = FCUtil.spiltStringOnNewLine(inputIdsString);
		if (FCUtil.isEmpty(inputIdSet)) {
			inputIdSet = FCUtil.getStringSetFromFile(file);
		}

		if (FCUtil.isEmpty(inputIdSet)) {
			model.addAttribute("messages", "Please enter Order ID either in TextArea box or upload a requestIds File.");
			return view;
		}

		List<String> successProcessList = new ArrayList<>();
		List<String> enqueuedProcessList = new ArrayList<>();
		for (String inputId : inputIdSet) {
			Map<Integer, Map<String, List<String>>> pgStatusResults = doPGStatusCheck(inputId);
			if (null != pgStatusResults && !pgStatusResults.isEmpty()
					&& pgStatusResults.containsKey(ORDER_STATUS_SUCCESS)) {
				successProcessList.add(inputId);
			} else {
				enqueuedProcessList.add(inputId);
			}
		}
		model.addAttribute("successlist", successProcessList);
		model.addAttribute("enqueuedlist", enqueuedProcessList);

		return view;
	}

	public final Map<Integer, Map<String, List<String>>> doPGStatusCheck(String merchantOrderId) {

		Map<Integer, Map<String, List<String>>> orderResult = new HashMap<Integer, Map<String, List<String>>>();
		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		boolean orderStatus = false;
		Boolean isManualStatusCheck = true;
		try {
			List<PaymentTransaction> paymentTransactions = paymentTransactionService
					.getPaymentTransactionByMerchantOrderId(merchantOrderId);
			if (paymentTransactions == null) {
				return null;
			}

			logger.info("Working on transaction for which we did not receive a response from PG - start");
			logger.info("Size of payment transactions:" + paymentTransactions.size());

			/*
			 * Just sync the payment transaction; this is sufficient since as part of
			 * syncing money will be moved to wallet so there's no need to refund the
			 * transaction.
			 */

			for (PaymentTransaction paymentTransaction : paymentTransactions) {
				List<String> result = new LinkedList<>();
				try {
					logger.info("payment transaction " + paymentTransaction.toString());
					if (paymentTransaction.getIsSuccessful()) {
						logger.info("Payment transaction already successful for mtxn id : "
								+ paymentTransaction.getMerchantTxnId());
						result.add("Payment transaction is already successful for merchant transaction id "
								+ paymentTransaction.getMerchantTxnId());
						orderStatus = true;
						// TODO : Check recharge status and send an alert to
						// sanity-alert@ in case it is a SUCCESS
					} else {
						PaymentTransaction updatedTransaction = null;
						boolean walletTransactionException = false;
						boolean ocwWalletTransaction = paymentTransaction.getPaymentType()
								.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET));
						try {
							updatedTransaction = autoRefundPaymentGatewayFailures.paymentSyncTxn(paymentTransaction,
									false);
						} catch (IllegalArgumentException e) {

							/**
							 * IllegalArgumentException is thrown only in case of wallet payments from
							 * {@link com.freecharge.payment.services.PaymentTransactionService#creditPGTransactionToWallet}
							 * method. In that case, we need to check the update transaction status and
							 * check if the transaction should be refunded or not.
							 */
							logger.error("Exception performing payment status check for "
									+ paymentTransaction.getMerchantTxnId(), e);
							if (paymentTransaction != null && paymentTransaction.getPaymentType()
									.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET))) {
								walletTransactionException = true;
							}

						}
						if (updatedTransaction != null && updatedTransaction.getIsSuccessful()) {
							orderStatus = true;
							result.add("Payment transaction is successful for mtxn id: "
									+ paymentTransaction.getMerchantTxnId());
							boolean alwaysRefundOnDelayedPaymentSuccess = appConfig
									.isAlwaysRefundOnDelayedPaymentSuccess();
							if (!alwaysRefundOnDelayedPaymentSuccess
									&& paymentTransactionService.canBeEnqueuedForRecharge(updatedTransaction)) {
								result.add("Payment transaction " + paymentTransaction.getMerchantTxnId()
										+ " is enqueued for recharge");
							} else {
								result.add("Payment transaction " + paymentTransaction.getMerchantTxnId()
										+ " is enqueued for refund");
							}
						} else {
							result.add("Payment transaction has failed for mtxn id: "
									+ paymentTransaction.getMerchantTxnId());
						}
					}
				} catch (Exception e) {
					logger.error("something bad happened with order ID :[" + paymentTransaction.getOrderId() + "].", e);
					result.add("Something bad happened with merchant transaction ID :["
							+ paymentTransaction.getMerchantTxnId() + "]");
				}
				resultObjectMap.put(paymentTransaction.getMerchantTxnId(), result);
			}

		} catch (Exception exp) {
			logger.error("Exception in refunding -:", exp);
		}
		if (orderStatus) {
			orderResult.put(ORDER_STATUS_SUCCESS, resultObjectMap);
		} else {
			orderResult.put(ORDER_STATUS_ENQUEUED, resultObjectMap);
		}
		return orderResult;
	}

	@RequestMapping(value = "doPayStatusCheckAndRefund", method = RequestMethod.POST)
	public final String doPayStatusCheckAndRefund(@RequestParam("merchantOrderId") final String merchantOrderId,
			final Model model, final HttpServletRequest request, final HttpServletRequest response) {
		String returnView = ViewConstants.PAY_STATUS_CHECK_NEW;
		Map<Integer, Map<String, List<String>>> pgStatusResult = doPGStatusCheck(merchantOrderId);
		Map<String, List<String>> resultObjectMap;
		if (pgStatusResult.containsKey(ORDER_STATUS_SUCCESS)) {
			resultObjectMap = pgStatusResult.get(ORDER_STATUS_SUCCESS);
		} else {
			resultObjectMap = pgStatusResult.get(ORDER_STATUS_ENQUEUED);
		}
		model.addAttribute("resultObjectMap", resultObjectMap);
		returnView = ViewConstants.PAY_STATUS_CHECK_NEW_RESULT;
		return returnView;
	}

	@RequestMapping(value = "doPayStatusCheckAndRefund", method = RequestMethod.GET)
	public final ModelAndView doPayStatusCheckAndRefundHome(final Model model, final HttpServletRequest request,
			final HttpServletRequest response) {
		model.addAttribute("orderIds", request.getParameter("orderIds"));
		return new ModelAndView(ViewConstants.PAY_STATUS_CHECK_NEW);
	}

	/* Start refund from cash back tool */
	@RequestMapping(value = "doCashbackRefund", method = RequestMethod.POST)
	public final String doCashBackRefund(@RequestParam("refundMtxnids") final MultipartFile file,
			@RequestParam(required = false, value = "orderIds") final String orderIdsorRequestIdsStr, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		if (!hasAccess(request, AdminComponent.CASH_BACK_TOOL)) {
			get403HtmlResponse(response);
			return null;
		}

		/* Picking Order Id,amount lists entered in TextArea box */
		Set<String> orderIdsorRequestIds = getOrderIds(orderIdsorRequestIdsStr);

		/*
		 * If No orderid's entered in TextArea then pick OrderId,amount from uploaded
		 * CSV file.
		 */
		if (orderIdsorRequestIds == null || orderIdsorRequestIds.isEmpty()) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				String refundData = StringUtils.trim(line);
				orderIdsorRequestIds.add(refundData);
			}
		}

		String messages = "";
		List<String> cashBackOrderIdList = new ArrayList<String>();

		/* If No orderid's in TextArea or uploaded csv file */
		if (orderIdsorRequestIds == null || orderIdsorRequestIds.isEmpty()) {
			messages += "No file or Order Id's for Cash back ";
			model.addAttribute("messages", messages);
			return ViewConstants.CASH_BACK_REFUND;
		}

		/* Iterate over oredrId-amount pair lists */
		for (String orderIdAmount : orderIdsorRequestIds) {
			String[] split = orderIdAmount.split(",");
			String mtxnId = split[0];
			Double refundAmount = Double.parseDouble(split[1]);
			try {
				logger.info("Starting cash back for : [" + mtxnId + "," + refundAmount + "].");
				PaymentTransaction uniquePaymentTransaction = paymentTransactionService
						.getUniquePaymentTransactionByMerchantOrderId(mtxnId);
				if (adminHelper.isExceedCSCashBackLimit(uniquePaymentTransaction, refundAmount)) {
					logger.info("Refundamount exceeds CSCASHBACK limit for " + mtxnId + "," + refundAmount);
					continue;
				}
				paymentRefund.doRefund(uniquePaymentTransaction, refundAmount, DISABLE_REFUND_MAIL,
						PaymentConstants.ADMIN_CASH_BACK);
			} catch (Exception e) {
				logger.error("Something BAD happened while refunding [" + mtxnId + "," + refundAmount
						+ "]. Going ahead with rest of the orders");
				e.printStackTrace();
			}
			cashBackOrderIdList.add("Done refunding [" + mtxnId + "," + refundAmount + "]");
			model.addAttribute("mtxnId", mtxnId);
			model.addAttribute("refundAmount", refundAmount);
		}

		messages += "Cash Back Refund Done successfully. Click on Show Status to view the details ";
		model.addAttribute("cashBackorderIdList", cashBackOrderIdList);
		model.addAttribute("messages", messages);
		return ViewConstants.CASH_BACK_REFUND;
	}

	/* End of Cash back tool */

	public final Map<String, List<String>> processManualRefund(final Set<String> orderIds) {
		logger.info("Inside processManualRefund:414");
		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		Iterator<String> itr = orderIds.iterator();
		int count = 0;
		Set<String> oldMtxnIds = new HashSet<>();
		while (itr.hasNext()) {
			final String orderIdForReund = itr.next();
			if (paymentRefund.isEligibleForNewRefund(orderIdForReund)) {
				Double refundAmount = paymentRefund.calculateRefundAmountForMtxnId(orderIdForReund);
				kestrelWrapper.enqueueNewRefundWithParams(orderIdForReund, PaymentConstants.MANUAL_REFUND,
						refundAmount);
				logger.info("Successfully enqueued mtxnId: " + orderIdForReund + " for new refund.");
				List<String> results = new LinkedList<String>();
				results.add("success");
				results.add("Refunded enqueued For : " + orderIdForReund);
				results.add("Refunded Amount: " + refundAmount);
				resultObjectMap.put(orderIdForReund, results);
				metricsClient.recordEvent(METRIC_SERVICE_R, METRIC_NAME, "success");
				count++;
			} else {
				oldMtxnIds.add(orderIdForReund);
			}
		}
		if (count > 0) {
			metricsClient.recordCount(METRIC_SERVICE_R, METRIC_NAME, count);
		}
		if (!FCUtil.isEmpty(oldMtxnIds)) {
			processManualRefundOld(oldMtxnIds);
		}
		return resultObjectMap;
	}

	public final Map<String, List<String>> processManualRefundOld(final Set<String> orderIds) {
		logger.info("Inside processManualRefund:414");
		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		Iterator<String> itr = orderIds.iterator();
		int count = 0;
		while (itr.hasNext()) {
			final String orderIdForReund = itr.next();
			if (isTransactionSuccessful(orderIdForReund)) {
				continue;
			}
			List<String> results = new LinkedList<String>();
			logger.info("Refunding thread for order id : " + orderIdForReund);
			try {
				logger.info("About to invoke paymentRefund.doRefund(orderIdForReund) for " + orderIdForReund);
				PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doRefund(orderIdForReund);
				if (paymentRefundBusinessDO != null) {

					logger.info("Refund Status for orderid : " + orderIdForReund + " status: "
							+ paymentRefundBusinessDO.getStatus());
					if (paymentRefundBusinessDO.getStatus()) {
						logger.info("Manul refund successful for " + orderIdForReund);
						String mainOrderId = paymentTransactionService.getMainOrderIdForMerchantId(orderIdForReund);
						if (mainOrderId != null) {
							freechargeEventPublisher.publishRechargeFailureEvent(orderIdForReund,
									txnHomePageDAO.findTxnHomePageByOrderID(mainOrderId).getServiceNumber());
						}
						results.add("success");
						results.add("Refunded successfully For : " + orderIdForReund);
						results.add("Total Transaction Amount: " + paymentRefundBusinessDO.getAmount());
						results.add("Refunded Amount: " + paymentRefundBusinessDO.getRefundAmount());
						results.add("Retained Amount:(crossell): "
								+ (paymentRefundBusinessDO.getAmount() - paymentRefundBusinessDO.getRefundAmount()));
						count++;
						metricsClient.recordEvent(METRIC_SERVICE_R, METRIC_NAME, "success");

					} else {
						results.add("refund unsuccessful. Message from PG" + orderIdForReund + " "
								+ paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPgMessage());
						metricsClient.recordEvent(METRIC_SERVICE_R, METRIC_NAME, "fail");
					}
				} else {
					results.add("Transaction reference for orderid : " + orderIdForReund + " not found");
					metricsClient.recordEvent(METRIC_SERVICE_R, METRIC_NAME, "fail-noreferenceinpaymenttxn");
				}
			} catch (Exception exp) {
				metricsClient.recordEvent(METRIC_SERVICE_R, METRIC_NAME, "fail-exception");
				logger.error("Exception raised in refunding for order id : " + orderIdForReund, exp);
			}
			resultObjectMap.put(orderIdForReund, results);
		}
		if (count > 0) {
			metricsClient.recordCount(METRIC_SERVICE_R, METRIC_NAME, count);
		}
		return resultObjectMap;
	}

	public static Set<String> getOrderIds(final String orderIdsString) {
		String[] orderIds = orderIdsString.split("\n");
		Set<String> orderIdsCollection = new HashSet<String>();
		for (String orderId : orderIds) {
			orderId = StringUtils.trim(orderId);
			if (StringUtils.isNotBlank(orderId)) {
				orderIdsCollection.add(orderId);
			}
		}

		return orderIdsCollection;
	}

	public final void sendEmail(final Map<String, List<String>> resultObjectMap, final String email,
			final long timeValue) {
		logger.info("In sending mail for id: " + timeValue);
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setSubject(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_SUBJECT) + timeValue);
		emailBusinessDO.setTo(email);
		emailBusinessDO.setFrom(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_FROM));
		emailBusinessDO.setReplyTo(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_REPLYTO));
		emailBusinessDO.setBcc(fcProperties.getProperty(FCConstants.REFUND_SUCCESS_CC));
		emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());

		StringBuffer sbf = new StringBuffer("");
		if (resultObjectMap != null && resultObjectMap.size() > 0) {
			for (String key : resultObjectMap.keySet()) {
				List<String> results = resultObjectMap.get(key);
				sbf.append("================" + key + "==============");
				sbf.append(StringUtils.join(results, FCUtil.getNewLineCharacter()));
				sbf.append("==============================");
			}
			emailBusinessDO.setContent(sbf.toString());
		}

		emailService.sendNonVelocityEmail(emailBusinessDO);
	}

	@RequestMapping(value = "doNewRefund", method = RequestMethod.GET)
	public final ModelAndView doNewRefundHome(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.MANUAL_REFUND)) {
			get403HtmlResponse(response);
			return null;
		}
		model.addAttribute("orderIds", request.getParameter("orderIds"));
		return new ModelAndView(ViewConstants.MANUAL_REFUND_NEW);
	}

	/* Manual refund from Customertrail */
	@RequestMapping(value = "doCtManualRefund", method = RequestMethod.POST)
	public final String manualRefundFromCustomertrail(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		logger.info("Now in ManualRefundController:516");
		if (!isAuthorizedToUpdateCt(request)) {
			logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel to do manualrefund : "
					+ request.getParameter("orderIds"));
			return null;
		}

		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}
		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		String orderid = request.getParameter("orderIds");
		double refundAmount = 0.0;
		if (FCUtil.isEmpty(orderid)) {
			return String.format("redirect:%s?orderId=%s&manualRefundStatus=%s&calledFrom=%s", CUSTOMER_TRAIL_LINK,
					orderid, "OrderId is empty", "customerTrail");
		}
		String orderId = orderid.trim();

		

		try {
			refundAmount = adminHelper.getManualRefundAmount(orderId);
			logger.info(ActivityLogger.ManualRefund + "initiated by " + getCurrentAdminUser(request) + "for orderId "
					+ orderId + "on " + System.currentTimeMillis()+" for Amount "+refundAmount );
		} catch (Exception e) {
			logger.error(
					"Exception while getting refund amount for manual refund process before checking sufficient avail-credits : "
							+ orderId);
			return String.format("redirect:%s?orderId=%s&manualRefundStatus=%s&calledFrom=%s", CUSTOMER_TRAIL_LINK,
					orderId, "Exception on getting refund amount to check sufficient avail-credits of cs-agent ",
					"customerTrail");
		}

		if (!ctActionControlService.hasSufficientCreditToDoCashBackOrRefund(getCurrentAdminUser(request),
				refundAmount)) {
			String errorMessage = getCurrentAdminUser(request)
					+ " : cs-agent does not have sufficient credits to do manualRefund(or exception while checking available credits), pls contact manager";
			return String.format("redirect:%s?orderId=%s&manualRefundStatus=%s&calledFrom=%s", CUSTOMER_TRAIL_LINK,
					orderId, errorMessage, "customerTrail");
		}

		String manualRefundStatus = null;
		Set<String> orderIdColl = new HashSet<String>();
		if (orderId != null) {
			orderIdColl.add(orderId);
			logger.info("About to do processManualRefund :549");
			resultObjectMap = processManualRefund(orderIdColl);
			logger.info("Returned from processManualRefund :551");
			if (resultObjectMap != null) {
				List<String> resultString = resultObjectMap.get(orderId);
				if (resultString != null && resultString.contains("success")) {
					logger.info("Manual refund from ct success for orderId " + orderId + "by cs agent "
							+ getCurrentAdminUser(request));
					adminHelper.sendAlertEmail(resultObjectMap, "Manual refund From CustomerTrail",
							getCurrentAdminUser(request));
					/*
					 * Deducting cs-agents aval-credits after successful manualrefund.
					 */
					if (ctActionControlService.deductAvailableCreditLimits(getCurrentAdminUser(request),
							refundAmount)) {
						manualRefundStatus = "Manual Refund is success ";
					} else {
						manualRefundStatus = "Manual Refund success but exception while deducting avail-credits of cs-agent, inform manager.";
					}
					/*
					 * Send a mail if cs-agent has crossed 90% of his max credits limit
					 */
					if (ctActionControlService.hasCrossedNinetyPercentOfCredits(getCurrentAdminUser(request))) {
						ctActionControlService.emailOnNinetyPercentCreditsReached(getCurrentAdminUser(request));
					}
					return String.format("redirect:%s?orderId=%s&manualRefundStatus=%s&calledFrom=%s",
							CUSTOMER_TRAIL_LINK, orderId, manualRefundStatus, "customerTrail");
				}
			}
		}
		manualRefundStatus = "Manual Refund is failed ";
		return String.format("redirect:%s?orderId=%s&manualRefundStatus=%s&calledFrom=%s", CUSTOMER_TRAIL_LINK, orderId,
				manualRefundStatus, "customerTrail");
	}

	@RequestMapping(value = "doCashbackRefund", method = RequestMethod.GET)
	public final String doCashBackRefund(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CASH_BACK_TOOL)) {
			get403HtmlResponse(response);
			return null;
		}
		return ViewConstants.CASH_BACK_REFUND;
	}

	public Set<String> getOrderIdsFromRefundForm(final ManualRefundForm manualRefundForm) {
		String orderIdsString = manualRefundForm.getOrderIds();
		String[] orderIds = orderIdsString.split(",");

		Set<String> orderIdsCollection = new HashSet<String>();
		for (String orderId : orderIds) {
			orderId = StringUtils.trim(orderId);
			if (StringUtils.isNotBlank(orderId)) {
				if (isTransactionSuccessful(orderId)) {
					continue;
				}
				orderIdsCollection.add(orderId);
			}
		}

		return orderIdsCollection;
	}

	public static Set<String> getOrderIds(final ManualRefundForm manualRefundForm) {
		String orderIdsString = manualRefundForm.getOrderIds();
		String[] orderIds = orderIdsString.split(",");

		Set<String> orderIdsCollection = new HashSet<String>();
		for (String orderId : orderIds) {
			orderId = StringUtils.trim(orderId);
			if (StringUtils.isNotBlank(orderId)) {
				orderIdsCollection.add(orderId);
			}
		}

		return orderIdsCollection;
	}

	private boolean isTransactionSuccessful(String merchantTxnId) {
		List<PaymentTransaction> paymentTxns = paymentTransactionService
				.getPaymentTransactionByMerchantOrderId(merchantTxnId);
		if (paymentTxns != null && !paymentTxns.isEmpty()) {
			String orderId = paymentTxns.get(0).getOrderId();
			List<PaymentTransaction> paymentsForOrder = paymentTransactionService
					.getAllPaymentTransactionByAcsOrder(orderId);
			Set<String> unrefundedMerchantTxnIds = new HashSet<String>();
			Set<String> refundedMerchantTxnIds = new HashSet<String>();
			for (PaymentTransaction transaction : paymentsForOrder) {
				if (transaction.getSuccessful()
						&& !transaction.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
					unrefundedMerchantTxnIds.add(transaction.getMerchantTxnId());
					List<PaymentRefundTransaction> refundedTransactions = pgReadDAO
							.getSuccessfulRefundsForMtxnId(transaction.getMerchantTxnId());
					if (refundedTransactions != null && !refundedTransactions.isEmpty()) {
						for (PaymentRefundTransaction refundTransaction : refundedTransactions) {
							if (refundTransaction.getStatus() != null
									&& !refundTransaction.getStatus().equals(Status.FAILURE)) {
								if (refundTransaction.getRefundTo().equals("ocw")) {
									Users users = userServiceProxy.getUserFromOrderId(transaction.getOrderId());
									String sdIdentity = oneCheckWalletService
											.getOneCheckIdIfOneCheckWalletEnabled(users.getEmail());
									if (!StringUtils.isEmpty(sdIdentity)) {
										logger.info("Checking for wallet refund for one check transaction "
												+ transaction.getMerchantTxnId());
										TransactionSummary txnSummary = oneCheckWalletService
												.findTransactionIdempotencyId(OneCheckTxnType.CREDIT_REFUND,
														transaction.getOrderId(), transaction.getMerchantTxnId(),
														sdIdentity, null);
										if (txnSummary != null) {
											logger.info("Wallet refund for " + transaction.getOrderId()
													+ " done with txn type : " + OneCheckTxnType.CREDIT_REFUND);
											refundedMerchantTxnIds.add(transaction.getMerchantTxnId());
											if (!txnSummary.getTransactionAmount()
													.equals(refundTransaction.getRefundedAmount().getAmount())) {
												refundTransaction.setRefundedAmount(
														new Amount(txnSummary.getTransactionAmount()));
												pgWriteDAO.updateRefundedAmountOnRefundTransaction(refundTransaction);
											}
										} else {
											txnSummary = oneCheckWalletService.findTransactionIdempotencyId(
													OneCheckTxnType.CS_REFUND, transaction.getOrderId(),
													transaction.getMerchantTxnId(), sdIdentity, null);
											if (txnSummary != null) {
												logger.info("Wallet refund for " + transaction.getOrderId()
														+ " done with txn type : " + OneCheckTxnType.CS_REFUND);
												refundedMerchantTxnIds.add(transaction.getMerchantTxnId());
												if (!txnSummary.getTransactionAmount()
														.equals(refundTransaction.getRefundedAmount().getAmount())) {
													refundTransaction.setRefundedAmount(
															new Amount(txnSummary.getTransactionAmount()));
													pgWriteDAO
															.updateRefundedAmountOnRefundTransaction(refundTransaction);
												}
											} else {
												txnSummary = oneCheckWalletService.findTransactionIdempotencyId(
														OneCheckTxnType.CREDIT_REFUND_API, transaction.getOrderId(),
														transaction.getMerchantTxnId(), sdIdentity, null);
												if (txnSummary != null) {
													logger.info("Wallet refund for " + transaction.getOrderId()
															+ " done with txn type : "
															+ OneCheckTxnType.CREDIT_REFUND_API);
													refundedMerchantTxnIds.add(transaction.getMerchantTxnId());
													if (!txnSummary.getTransactionAmount().equals(
															refundTransaction.getRefundedAmount().getAmount())) {
														refundTransaction.setRefundedAmount(
																new Amount(txnSummary.getTransactionAmount()));
														pgWriteDAO.updateRefundedAmountOnRefundTransaction(
																refundTransaction);
													}
												}
											}
										}
									}
								}
								if (transaction.getAmount()
										.equals(refundTransaction.getRefundedAmount().getAmount().doubleValue())) {
									logger.info("MTxnId refunded.");
									refundedMerchantTxnIds.add(transaction.getMerchantTxnId());
									break;
								}
							}

						}
					}
				}
			}
			unrefundedMerchantTxnIds.removeAll(refundedMerchantTxnIds);
			if (unrefundedMerchantTxnIds.size() > 1 && unrefundedMerchantTxnIds.contains(merchantTxnId)) {
				return false;
			}
			UserTransactionHistory transactionHistory = userTransactionHistoryService
					.findUserTransactionHistoryByOrderId(paymentTxns.get(0).getOrderId());
			if (transactionHistory != null && transactionHistory.getTransactionStatus().equals("00")) {
				logger.info("Transaction : " + merchantTxnId + " is already successful. Hence not refunding");
				return true;
			}
		}
		return false;
	}

	/* DO cashback from customer trail for a orderId */
	@RequestMapping(value = "cashBack-customerTrail", method = RequestMethod.POST)
	@ResponseBody
	public final Map<String, String> doCashBackCustomerTrail(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (!isAuthorizedToUpdateCt(request)) {
			logger.info(getCurrentAdminUser(request) + " doesn't have write access to admin panel to do cashback : "
					+ request.getParameter("orderIdCashBack"));
			return null;
		}
		if (!hasAccess(request, AdminComponent.CUSTOMER_TRAIL)) {
			get403HtmlResponse(response);
			return null;
		}
		
		Map<String, String> cashBackResponse = new HashMap<String, String>();
		String orderId = request.getParameter("orderIdCashBack");
		String amount = request.getParameter("cashBackAmount");
		logger.info(ActivityLogger.ManualCashBack+" initiated by "+getCurrentAdminUser(request)+" for Order Id "+ orderId+" for Amount "+amount);
		

		if (amount == null || amount.isEmpty()) {
			cashBackResponse.put("message", "Cash back amount required.");
			cashBackResponse.put("status", "failed");
			return cashBackResponse;
		} else if (!isNumeric(amount)) {
			cashBackResponse.put("message", "Cash back amount should be numeric.");
			cashBackResponse.put("status", "failed");
			return cashBackResponse;
		}
		/* Checking whether CS-agent has avail-credits to do any cashback */
		if (!ctActionControlService.hasSufficientCreditToDoCashBackOrRefund(getCurrentAdminUser(request),
				Double.parseDouble(amount))) {
			cashBackResponse.put("message", getCurrentAdminUser(request)
					+ " : cs-agent does not have sufficient credits to do cashback(or exception while checking available credits), pls contact manager");
			cashBackResponse.put("status", "failed");
			return cashBackResponse;
		}

		Double refundAmount = Double.parseDouble(amount);
		logger.info("Entered manual refund for mtxn(orderId) :" + orderId);

		Map<String, List<String>> resultObjectMap = new HashMap<String, List<String>>();
		WalletResponseDo status = null;
		try {
			PaymentTransaction uniquePaymentTransaction = paymentTransactionService
					.getUniquePaymentTransactionByMerchantOrderId(orderId);
			if (adminHelper.isExceedCSCashBackLimit(uniquePaymentTransaction, refundAmount)) {
				cashBackResponse.put("message",
						"Admin cashback exceeds the CSCASHBACK limit [" + orderId + "," + refundAmount + "].");
				cashBackResponse.put("mtxnId", orderId);
				cashBackResponse.put("refundAmount", String.valueOf(refundAmount));
				cashBackResponse.put("status", "failed");
				return cashBackResponse;
			}

			Integer userId = orderIdDAO.getUserIdFromOrderId(orderId);

			String callerReference = orderId + "_" + "CS_CASHBACK";
			String foundSource = "CS_CASHBACK";
			String metaData = "Cashback by CS";

			status = campaignCashRewardService.voucherCredit((long) userId, refundAmount, orderId, callerReference,
					foundSource, 0L, metaData, OneCheckWalletService.OneCheckTxnType.CS_CASHBACK,
					fcproperties.getProperty(FCConstants.COMPENSATION_CORP_ID));
			if (status.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {
				resultObjectMap.put(orderId,
						adminHelper.getResultObjectListForEmail(orderId, String.valueOf(refundAmount)));
			}
		} catch (Exception e) {
			logger.error("Something BAD happened while refunding [" + orderId + "," + refundAmount + "].", e);
			cashBackResponse.put("message",
					"Something BAD happened while refunding [" + orderId + "," + refundAmount + "].");
			cashBackResponse.put("mtxnId", orderId);
			cashBackResponse.put("refundAmount", String.valueOf(refundAmount));
			cashBackResponse.put("status", "failed");
			return cashBackResponse;
		}

		if (!status.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {
			logger.error("CS Cashback failed [" + orderId + "," + refundAmount + "] - " + status.getErrorMessage());
			cashBackResponse.put("message",
					"Cashback failed [" + orderId + "," + refundAmount + "] - " + status.getErrorMessage());
			cashBackResponse.put("mtxnId", orderId);
			cashBackResponse.put("refundAmount", String.valueOf(refundAmount));
			cashBackResponse.put("status", "failed");
			return cashBackResponse;
		}

		logger.info("Done cashback refund for mtxn(orderId) :" + orderId + "by " + getCurrentAdminUser(request));
		if (resultObjectMap != null && resultObjectMap.size() > 0) {
			adminHelper.sendAlertEmail(resultObjectMap, "CashBack From CustomerTrail", getCurrentAdminUser(request));
		}
		/* Deducting cs-agent's aval-credits after successful cashback. */
		if (!ctActionControlService.deductAvailableCreditLimits(getCurrentAdminUser(request), refundAmount)) {
			cashBackResponse.put("message",
					"Cashback success but exception on deducting avail-credits of cs-agent after cashback, Contact manager");
			cashBackResponse.put("status", "success");
		} else {
			cashBackResponse.put("message", "Done refunding [" + orderId + "," + refundAmount + "]");
			cashBackResponse.put("mtxnId", orderId);
			cashBackResponse.put("refundAmount", String.valueOf(refundAmount));
			cashBackResponse.put("status", "success");
		}

		/* Send a mail if cs-agent has crossed 90% of his max credits limit */
		if (ctActionControlService.hasCrossedNinetyPercentOfCredits(getCurrentAdminUser(request))) {
			ctActionControlService.emailOnNinetyPercentCreditsReached(getCurrentAdminUser(request));
		}

		return cashBackResponse;
	}

	// Validation for isNumeric
	private Boolean isNumeric(final String amount) {
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(amount.trim());
		if (!(m.matches())) {
			return false;
		}
		return true;
	}

	/* CashBack view for Different Fund_sources */
	@RequestMapping(value = "doCashback", method = RequestMethod.GET)
	public final ModelAndView doCashBack(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (!hasAccess(request, AdminComponent.CASHBACK_FOR_FUNDSOURCE)) {
			get403HtmlResponse(response);
			return null;
		}
		List<String> cashBackNameList = getCashbackFundSourceList();
		model.addAttribute("cashBackNameList", cashBackNameList);
		return new ModelAndView(ViewConstants.CASH_BACK);
	}

	/* CashBack back end code for Different Fund_sources */
	@RequestMapping(value = "doCashback", method = RequestMethod.POST)
	public final ModelAndView doCashBack(@RequestParam("cashbackemailids") final MultipartFile file,
			@RequestParam(required = false, value = "emailIds") final String emailIdAmountFullStr,
			@RequestParam(required = false, value = "fundSource") final String fundSource, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		if (!hasAccess(request, AdminComponent.CASHBACK_FOR_FUNDSOURCE)) {
			get403HtmlResponse(response);
			return null;
		}

		/*
		 * Get inputdata(Email Id,amount,callerRef,MetaData) set from TextArea
		 */
		Set<String> emailIdAmountSet = getEmailIdStringSet(emailIdAmountFullStr);

		/* If TextArea empty pick input data set from uploaded CSV file. */
		if (emailIdAmountSet == null || emailIdAmountSet.isEmpty()) {
			emailIdAmountSet = getDataSetFromFile(file);
		}

		String messages = "";
		Map<String, Object> validationMap = validateCashBackInput(emailIdAmountSet, fundSource);
		if (!(Boolean) validationMap.get("valid")) {
			model.addAttribute("messages", validationMap.get("messages"));
			List<String> cashBackNameList = getCashbackFundSourceList();
			model.addAttribute("cashBackNameList", cashBackNameList);
			return new ModelAndView(ViewConstants.CASH_BACK);
		}

		/* Check email Id's are exists with us */
		List<String> nonExistEmails = validateCashBackEmailData(emailIdAmountSet);
		if (nonExistEmails != null && !(nonExistEmails.isEmpty())) {
			messages += "Email id is not exist or not active : " + nonExistEmails;
			model.addAttribute("messages", messages);
			List<String> cashBackNameList = getCashbackFundSourceList();
			model.addAttribute("cashBackNameList", cashBackNameList);
			return new ModelAndView(ViewConstants.CASH_BACK);
		}

		List<String> cashBackSuccessEmailIdList = null;
		List<String> cashBackFailureEmailIdList = null;
		Map<String, List<String>> cashBackResultMap = doCashBackForFundSource(fundSource, emailIdAmountSet);
		cashBackSuccessEmailIdList = cashBackResultMap.get("cashBackSuccessEmailIdList");
		cashBackFailureEmailIdList = cashBackResultMap.get("cashBackFailureEmailIdList");

		messages += "Cash Back Process Done successfully. Click on Show Status to view the details ";
		model.addAttribute("cashBackSuccessEmailIdList", cashBackSuccessEmailIdList);
		model.addAttribute("cashBackFailureEmailIdList", cashBackFailureEmailIdList);
		model.addAttribute("messages", messages);
		model.addAttribute("successCount", cashBackSuccessEmailIdList.size());
		model.addAttribute("failureCount", cashBackFailureEmailIdList.size());
		List<String> cashBackNameList = getCashbackFundSourceList();
		model.addAttribute("cashBackNameList", cashBackNameList);
		return new ModelAndView(ViewConstants.CASH_BACK);
	}

	/* Get Fund Source enum constants */
	private List<String> getCashbackFundSourceList() {
		List<String> cashBackNameList = new ArrayList<>();
		Class cls = Wallet.FundSource.class;
		for (Object obj : cls.getEnumConstants()) {
			cashBackNameList.add(obj.toString());
		}
		cashBackNameList.remove("RECHARGE_FUND");
		cashBackNameList.remove("REFUND");
		cashBackNameList.add("Select FundSource");
		return cashBackNameList;
	}

	private OneCheckWalletService.OneCheckTxnType getOncheckTypeForFundSourceForCredit(String fundSource) {
		if (fundSource.equals("REDEEM")) {
			return OneCheckWalletService.OneCheckTxnType.CREDIT_FREEFUND;
		} else if (fundSource.equals("REWARDS_CASHBACK") || fundSource.equals("ARREWARD")) {
			return OneCheckWalletService.OneCheckTxnType.CREDIT_REWARD;
		} else {
			return OneCheckWalletService.OneCheckTxnType.CREDIT_REWARD;
		}
	}

	public static Set<String> getEmailIdStringSet(final String emailIdAmountFullStr) {
		String[] orderIds = emailIdAmountFullStr.split("\n");
		Set<String> emailIdAmountSet = new HashSet<>();
		for (String orderId : orderIds) {
			orderId = StringUtils.trim(orderId);
			if (StringUtils.isNotBlank(orderId)) {
				emailIdAmountSet.add(orderId);
			}
		}
		return emailIdAmountSet;
	}

	private Set<String> getDataSetFromFile(final MultipartFile file) throws IOException {
		Set<String> emailIdAmountSetFromFile = new HashSet<>();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			String cashBackData = StringUtils.trim(line);
			emailIdAmountSetFromFile.add(cashBackData);
		}
		return emailIdAmountSetFromFile;
	}

	private Map<String, Object> validateCashBackInput(final Set<String> emailIdAmountSet, final String fundSource) {
		String messages = "";
		Boolean valid = true;
		Map<String, Object> validationMap = new HashMap<>();

		if (emailIdAmountSet == null || emailIdAmountSet.isEmpty()) {
			messages += "No file or EmailId string's for Cash back ";
			valid = false;
		} else if (fundSource == null || fundSource.isEmpty()) {
			messages += "Please select Fund Source";
			valid = false;
		}
		validationMap.put("valid", valid);
		validationMap.put("messages", messages);
		return validationMap;
	}

	private List<String> validateCashBackEmailData(final Set<String> emailIdAmountSet) {
		List<String> nonExistEmails = new ArrayList<>();

		for (String emailIdAmount : emailIdAmountSet) {
			if (emailIdAmount == null || emailIdAmount.isEmpty()) {
				continue;
			}

			String[] split = emailIdAmount.split(",");
			String emailId = split[0];

			Users users = userServiceProxy.getUserByEmailId(emailId);
			if ((users == null) || (!users.getIsActive())) {
				logger.info("The user is either inactive or does not exist in UMS. Checking IMS next.");
				// let us check if the user exists in IMS instead
				UserDetailsDTO userDetails = oneCheckWalletService.getOneCheckUserDetailsFromIMS(emailId);
				if (userDetails != null || userDetails.getAccountState().equals("registered")) {
					logger.info("The user either exist or is in registered State in IMS.");
				} else {
					logger.info(
							"Either user does not exist or is not active! Adding him/her to non existant email list");
					nonExistEmails.add(emailId);
				}
			}
		}
		return nonExistEmails;
	}

	private Map<String, List<String>> doCashBackForFundSource(final String fundSource,
			final Set<String> emailIdAmountSet) {
		List<String> cashBackSuccessEmailIdList = new ArrayList<>();
		List<String> cashBackFailureEmailIdList = new ArrayList<>();
		Map<String, List<String>> cashBackResultMap = new HashMap<>();
		String corpId = fcproperties.getProperty(FCConstants.COMPENSATION_CORP_ID);

		for (String emailIdAmount : emailIdAmountSet) {
			if (emailIdAmount.isEmpty()) {
				continue;
			}

			Map<String, Object> splitData = getFirstThreeSplitData(emailIdAmount);
			String emailId = (String) splitData.get("emailId");
			Double cashBackAmount = (Double) splitData.get("cashBackAmount");
			String callerReference = (String) splitData.get("callerReference");
			String metaData = (String) splitData.get("metaData");

			Long userId = userServiceProxy.getUserIdByEmailId(emailId);
			logger.info("User id fetched from UMS is : " + userId);

			if (userId == 0) {
				logger.info("User is not present in UMS, trying IMS : " + userId);
				UserDetailsDTO userDetails = oneCheckWalletService.getOneCheckUserDetailsFromIMS(emailId);
				if (userDetails != null) {
					userId = (long) userDetails.getFcUserId();
					logger.info("FC User id fetched from IMS is : " + userId);
				} else {
					logger.info("User is not present IMS either : " + userId);
				}
			} else {
				logger.info("User id exists in UMS, no need to check in IMS");
			}

			WalletResponseDo status = new WalletResponseDo();
			try {
				logger.info(String.format("Started cash back for : [ %s , %.2f, %s ]", emailId, cashBackAmount,
						callerReference));
				if ((emailId != null && !(emailId.isEmpty()))
						&& (cashBackAmount != null && !(cashBackAmount.toString().isEmpty()))
						&& (callerReference != null && !(callerReference.isEmpty()))) {
					String orderIdRef = callerReference;
					// Needs to display proper text while fetching idempotencyId
					// for wallet txn details in cs tool
					// String orderId = orderIdRef.replace("_", "-");
					String orderId = orderIdRef;
					String oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
					String nonFcCorpId = getCorpIdForNonFCOrderId(orderId, oneCheckId);
					if (nonFcCorpId != null) {
						corpId = nonFcCorpId;
					}
					if (!FCUtil.isEmpty(corpId)) {
						status = campaignCashRewardService.voucherCredit(userId, cashBackAmount, orderId,
								callerReference, fundSource, 0L, metaData,
								getOncheckTypeForFundSourceForCredit(fundSource), corpId, emailId);
						PromocodeRedeemDetailsSNSDo promocodeRedeemDetailsSNSDo = new PromocodeRedeemDetailsSNSDo();
						promocodeRedeemDetailsSNSDo.setOrderId(orderId);
						promocodeRedeemDetailsSNSDo.setDiscountValue(String.valueOf(cashBackAmount));
						promocodeRedeemDetailsSNSDo.setPromocode("");
						promocodeRedeemDetailsSNSDo.setCallerReference(callerReference);
						promocodeRedeemDetailsSNSDo.setPromoEntity(null);
						try {
							logger.info("Publish promocode redeem details notification "
									+ PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo));
							promocodeRedeemDetailsSNSService.publish(orderId, "{\"message\":"
									+ PromocodeRedeemDetailsSNSDo.getJsonString(promocodeRedeemDetailsSNSDo) + "}");
						} catch (IOException e) {
							logger.error("Error publishing freefund success event to SNS ", e);
						}
					} else {
						status.setStatusCode(FCConstants.FAILURE);
					}
				} else {
					status.setStatusCode(FCConstants.FAILURE);
				}
			} catch (RuntimeException re) {
				status.setStatusCode(FCConstants.FAILURE);
				logger.error(String.format(
						"Something BAD happened while doing cashback [ %s , %.2f, %s ]. Going ahead with rest of the email id's",
						emailId, cashBackAmount, callerReference), re);
			}

			if (status.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {
				cashBackSuccessEmailIdList.add(String.format("Done cash back [ %s, %.2f, %s, %s(optional)]", emailId,
						cashBackAmount, callerReference, metaData));
			} else {
				cashBackFailureEmailIdList.add(String.format("Failed Cash Back process [ %s, %.2f, %s, %s(optional)]",
						emailId, cashBackAmount, callerReference, metaData));
			}
		}
		cashBackResultMap.put("cashBackSuccessEmailIdList", cashBackSuccessEmailIdList);
		cashBackResultMap.put("cashBackFailureEmailIdList", cashBackFailureEmailIdList);
		return cashBackResultMap;
	}

	private Map<String, Object> getFirstThreeSplitData(final String emailIdAmount) {
		String[] split = emailIdAmount.split(",");
		int size = split.length;
		Map<String, Object> splitDataMap = new HashMap<>();
		splitDataMap.put("emailId", split[ARRAY_INDEX_ZERO]);

		if (size > ARRAY_SIZE_ONE) {
			splitDataMap.put("cashBackAmount", Double.parseDouble(split[ARRAY_INDEX_ONE]));
		}
		if (size > ARRAY_SIZE_TWO) {
			splitDataMap.put("callerReference", split[ARRAY_INDEX_TWO]);
		}
		if (size > ARRAY_SIZE_THREE) {
			splitDataMap.put("metaData", split[ARRAY_INDEX_THREE]);
		}
		return splitDataMap;
	}

	/** Keeping this unused method for now, will remove if no impact of it */
	private void syncPaymentPlanWithPaymentTransaction(String orderIdForReund) {
		List<PaymentTransaction> paymentTransactions = paymentTransactionService
				.getPaymentTransactionByMerchantOrderId(orderIdForReund);
		Double walletAmount = 0.0;
		Double pgAmount = 0.0;
		Boolean userUpgraded = true;
		if (paymentTransactions != null && !paymentTransactions.isEmpty()) {
			paymentTransactions = paymentTransactionService
					.getAllPaymentTransactionSentToPG(paymentTransactions.get(0).getOrderId());
			if (paymentTransactions != null && !paymentTransactions.isEmpty()) {
				for (PaymentTransaction paymentTransaction : paymentTransactions) {
					if (paymentTransaction.getIsSuccessful()) {
						if ((paymentTransaction.getPaymentType() == 6 || paymentTransaction.getPaymentType() == 9)) {
							if (paymentTransaction.getPaymentType() == 6) {
								userUpgraded = false;
							}
							walletAmount += paymentTransaction.getAmount();
							break;
						}
					}
				}

				for (PaymentTransaction paymentTransaction : paymentTransactions) {
					if (paymentTransaction.getIsSuccessful()) {
						if (paymentTransaction.getPaymentType() != 6 && paymentTransaction.getPaymentType() != 9) {
							pgAmount += paymentTransaction.getAmount();
							break;
						}
					}
				}
			}
		}
		if (userUpgraded) {
			try {
				PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(paymentTransactions.get(0).getOrderId());
				logger.info(
						"Wallet amount on payment plan : " + paymentPlan.getWalletAmount().getAmount().doubleValue());
				logger.info("Wallet amount from payment txn : " + walletAmount.doubleValue());

				logger.info("PG amount on payment plan : " + paymentPlan.getPgAmount().getAmount().doubleValue());
				logger.info("PG amount from payment txn : " + pgAmount.doubleValue());
				if ((paymentPlan.getWalletAmount() != null
						&& paymentPlan.getWalletAmount().getAmount().doubleValue() != walletAmount.doubleValue())
						|| (paymentPlan.getPgAmount() != null
								&& paymentPlan.getPgAmount().getAmount().doubleValue() != pgAmount.doubleValue())) {
					paymentPlan.setWalletAmount(new Amount(walletAmount));
					paymentPlan.setPgAmount(new Amount(pgAmount));
					paymentPlanService.updatePaymentPlan(paymentPlan);
				}
			} catch (PaymentPlanNotFoundException e) {
				logger.error("Exception fetching payment plan for " + orderIdForReund, e);
			}
		}

	}

	private String getCorpIdForNonFCOrderId(String orderId, String sdIdentity) {
		String corpId = null;
		Map<String, Object> responseMap = virtualCardService.getTransactionByTxnRefId(orderId);
		List<Map<String, Object>> vCardTxns = null;
		if (null != responseMap && !responseMap.isEmpty()) {
			vCardTxns = (List<Map<String, Object>>) responseMap.get(VirtualCardService.TXN_LIST_STRING);
		}
		if (null != vCardTxns && !vCardTxns.isEmpty()) {
			corpId = clientNameToCorpIdMap.get("VirtualCard");
		} else {
			List<TransactionSummary> oneCheckTransactions = oneCheckWalletService.findTransactionsByReference(orderId,
					sdIdentity);
			for (TransactionSummary transactionSummary : oneCheckTransactions) {
				String merchantName = oneCheckUserService.getMerchantNameFromWallet(transactionSummary);
				if (null != merchantName && clientNameToCorpIdMap.containsKey(merchantName)) {
					corpId = clientNameToCorpIdMap.get(merchantName);
					logger.debug("CorpId for MerchantName : " + merchantName + " is : " + corpId);
				}
			}
		}
		return corpId;
	}

	private String getSDIdentityIfWalletExistsFromOrderId(String orderId) {
		Users users = userServiceProxy.getUserFromOrderId(orderId);
		String sdIdentity = null;
		sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(users.getEmail());
		return sdIdentity;
	}
}
