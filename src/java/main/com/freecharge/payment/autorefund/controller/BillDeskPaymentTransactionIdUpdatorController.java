package com.freecharge.payment.autorefund.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.payment.autorefund.BillDeskPaymentTransactionIdUpdator;
import com.freecharge.payment.autorefund.form.BillDeskPaymentTransactionIdUpdatorForm;
import com.freecharge.payment.autorefund.form.BillDeskPaymentTransactionIdUpdatorFormValidator;
import com.freecharge.web.util.ViewConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/10/12
 * Time: 2:13 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/refund/billDeskUpdator/*")
public class BillDeskPaymentTransactionIdUpdatorController {

    @Autowired
    private BillDeskPaymentTransactionIdUpdator billDeskPaymentTransactionIdUpdator;

    @RequestMapping(method = RequestMethod.GET)
    @NoLogin
    public ModelAndView home() {
        return new ModelAndView(ViewConstants.BILL_DESK_UPDATOR_HOME, "command", new BillDeskPaymentTransactionIdUpdatorForm());
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @NoLogin
    public ModelAndView doRefund(@ModelAttribute("command")BillDeskPaymentTransactionIdUpdatorForm billDeskPaymentTransactionIdUpdatorForm, BindingResult bindingResult) throws Exception {
        BillDeskPaymentTransactionIdUpdatorFormValidator billDeskPaymentTransactionIdUpdatorFormValidator = new BillDeskPaymentTransactionIdUpdatorFormValidator();
        billDeskPaymentTransactionIdUpdatorFormValidator.validate(billDeskPaymentTransactionIdUpdatorForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return new ModelAndView(ViewConstants.BILL_DESK_UPDATOR_HOME, "command", billDeskPaymentTransactionIdUpdatorForm);
        }

        String csv = billDeskPaymentTransactionIdUpdatorForm.getCsv();

        BillDeskPaymentTransactionIdUpdator.Result result = this.billDeskPaymentTransactionIdUpdator.update(csv);

        ModelAndView modelAndView = new ModelAndView(ViewConstants.BILL_DESK_UPDATOR_RESULT);
        modelAndView.addObject("result", result);

        return modelAndView;
    }
}
