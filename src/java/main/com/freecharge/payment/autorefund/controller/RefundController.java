package com.freecharge.payment.autorefund.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.payment.autorefund.AutoRefund;
import com.freecharge.payment.autorefund.AutoRefundRechargeFailures;
import com.freecharge.payment.autorefund.RefundUtil;
import com.freecharge.payment.autorefund.form.RefundForm;
import com.freecharge.payment.autorefund.form.RefundFormValidator;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.web.util.ViewConstants;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 27/10/12
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/refund/*")
public class RefundController {
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    @Autowired
    private AutoRefund autoRefund;
    @Autowired
    private AutoRefundRechargeFailures autoRefundRechargeFailures;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(RefundUtil.DATE_FORMAT);
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView historyHome() {
        ModelAndView modelAndView = new ModelAndView(ViewConstants.REFUND_HOME, "command", new RefundForm());
        modelAndView.addObject("postAction", "/admin/refund/history/list");
        modelAndView.addObject("submitButtonName", "List Refunds");
        return modelAndView;
    }

    @RequestMapping(value = "/doRefund", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView doRefundHome() {
        ModelAndView modelAndView = new ModelAndView(ViewConstants.REFUND_HOME, "command", new RefundForm());
        modelAndView.addObject("postAction", "/admin/refund/doRefund/do");
        modelAndView.addObject("submitButtonName", "Do Refund");
        return modelAndView;
    }

    @RequestMapping(value = "/failedOxigenTransactions", method = RequestMethod.GET)
    @NoLogin
    public ModelAndView failedOxigenTransactionsHome() {
        ModelAndView modelAndView = new ModelAndView(ViewConstants.REFUND_HOME, "command", new RefundForm());
        modelAndView.addObject("postAction", "/admin/refund/failedOxigenTransactions/get");
        modelAndView.addObject("submitButtonName", "Get Failed Oxigen Transactions");
        return modelAndView;
    }

    @RequestMapping(value = "/history/list", method = RequestMethod.POST)
    @NoLogin
    public ModelAndView list(@ModelAttribute("command") RefundForm refundForm, BindingResult bindingResult) {
        RefundFormValidator refundFormValidator = new RefundFormValidator();
        refundFormValidator.validate(refundForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return new ModelAndView(ViewConstants.REFUND_HOME, "command", refundForm);
        }

        List<PaymentRefundTransaction> paymentRefundTransactions = this.paymentTransactionService.getPaymentRefundTransactions(refundForm.getFrom(), refundForm.getTo(), false);

        ModelAndView modelAndView = new ModelAndView(ViewConstants.REFUND_HISTORY_LIST);
        modelAndView.addObject("refunds", paymentRefundTransactions);

        return modelAndView;
    }

    @RequestMapping(value = "/doRefund/do", method = RequestMethod.POST)
    @NoLogin
    public ModelAndView doRefund(@ModelAttribute("command") RefundForm refundForm, BindingResult bindingResult) throws Exception {
        RefundFormValidator refundFormValidator = new RefundFormValidator();
        refundFormValidator.validate(refundForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return new ModelAndView(ViewConstants.REFUND_HOME, "command", refundForm);
        }

        Date from = refundForm.getFrom();
        Date to = refundForm.getTo();

        this.autoRefund.doRefund(from, to, false);
        List<PaymentRefundTransaction> paymentRefundTransactions = this.paymentTransactionService.getPaymentRefundTransactions(from, to, false);

        ModelAndView modelAndView = new ModelAndView(ViewConstants.REFUND_HISTORY_LIST);
        modelAndView.addObject("refunds", paymentRefundTransactions);

        return modelAndView;
    }

    public PaymentTransactionService getPaymentTransactionService() {
        return paymentTransactionService;
    }

    public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
        this.paymentTransactionService = paymentTransactionService;
    }

    public AutoRefund getAutoRefund() {
        return autoRefund;
    }

    public void setAutoRefund(AutoRefund autoRefund) {
        this.autoRefund = autoRefund;
    }
}
