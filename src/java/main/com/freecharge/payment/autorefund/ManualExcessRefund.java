package com.freecharge.payment.autorefund;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.StringUtils;

/**
 * @param args
 * User: Vipin Goyal
 */

public class ManualExcessRefund {


    public static void main(String[] args) throws Exception {
        String csv = args[0];
        BufferedReader bufferedReader = new BufferedReader(new FileReader(csv));
        String line;

        Set<String> merchantOrderIds = new HashSet<String>();
        while ((line = bufferedReader.readLine()) != null) {
            String orderId = StringUtils.trim(line);
            merchantOrderIds.add(orderId);
        }

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentRefund paymentRefund = ((PaymentRefund) ctx.getBean("paymentRefund"));

        paymentRefund.doRefund(merchantOrderIds, PaymentConstants.EXCESS_CREDIT);
        ctx.close();
    }
 
   
}
