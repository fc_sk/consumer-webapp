package com.freecharge.payment.autorefund;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.recharge.businessdao.StringUtils;

/**
 * This is a wrapper class, meant to be run from ant task, over
 * {@link PaymentTransactionService#syncfromPGData(PaymentTransaction)} method.
 * @author arun
 *
 */
public class PaymentTransactionUpdator {

    public static void main(String[] args) throws Exception  {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        PaymentTransactionService paymentService = ctx.getBean(PaymentTransactionService.class);

        String csv = args[0];

        BufferedReader bufferedReader = new BufferedReader(new FileReader(csv));
        String line;

        Set<String> orderIdList = new HashSet<String>();
        while ((line = bufferedReader.readLine()) != null) {
            String orderId = StringUtils.trim(line);
            orderIdList.add(orderId);
        }

        for (String orderId : orderIdList) {
            List<PaymentTransaction> paymentTransactionList = paymentService.getPaymentTransactionsByOrderId(orderId);
            
            for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                System.out.println("Syncing [" + orderId + ", " + paymentTransaction.getMerchantTxnId() + "]");
                paymentService.syncfromPGData(paymentTransaction,false);
                System.out.println("Done syncing [" + orderId + ", " + paymentTransaction.getMerchantTxnId() + "]");
            }
            
        }

        ctx.close();
    }

}
