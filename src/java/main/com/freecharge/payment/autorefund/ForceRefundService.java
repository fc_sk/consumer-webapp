package com.freecharge.payment.autorefund;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.freecharge.admin.service.RefundService;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.payment.autorefund.dao.ForceRefundDAO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.common.collect.ImmutableMap;

@Component
public class ForceRefundService {

    private static final String DD_MM_YYYY = "ddMMyyyy";

    private static final String TOTAL_AMOUNT = "TotalAmount";

    private static final String REFUND_DATE = "RefundDate";

    private static final String TIME_BOUND_REFUND_TABLE = "TimeBoundRefundSummary";

    private static final boolean REFUND_FAILURE = false;

    private static final boolean REFUND_SUCCESS = true;

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private InService inService;

    @Autowired
    private BillPaymentService billPaymentService;

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private AmazonDynamoDBAsync dynamoClient;

    @Autowired
    private RefundService refundService;
    
    @Autowired
    private ForceRefundDAO forceRefundDAO;

    public boolean forceRefundUnderProcess(UserTransactionHistory uth) {        
    	try {
            if (!appConfigService.isTimeBasedRefundEnabled()
                    || !RechargeConstants.IN_UNDER_PROCESS_CODE.equals(uth.getTransactionStatus())
                    || !isEligibleForTimeboundRefund(uth)) {
                return REFUND_FAILURE;
            }
            String orderId = uth.getOrderId();
            String basePath = fcProperties.getCCSBasePathRefund();
            String aggregatorListKey = fcProperties.getRefundAggregatorListKey();
            String[] allowedAggregators = FCUtil.getCCSValues(aggregatorListKey, basePath, fcProperties.getCCSHost());
            InResponse inResponse = inService.getInResponseByOrderID(orderId);
            TransactionStatus transactionStatus = new TransactionStatus();
            transactionStatus.setTransactionStatus(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND);
            if (inResponse != null) {
                String agResp = inResponse.getInRespCode();
                if (agResp != null && agResp.equals(RechargeConstants.IN_UNDER_PROCESS_CODE)
                        && FCUtil.containsCaseInsensitive(inResponse.getAggrName(), allowedAggregators)) {
                    logger.info("Force refund under process prepaid OrderId - " + orderId);
                    incrementAmountRefundedTillNow(uth.getTransactionAmount().longValue());
                    userTransactionHistoryService.updateStatus(orderId,
                            PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND);
                    forceRefundDAO.recordForceRefundAttempt(orderId,uth.getTransactionAmount());
                    refundService.initiateRechargeRefund(orderId, transactionStatus ,PaymentConstants.FORCED_REFUND);
                    return REFUND_SUCCESS;
                }
            }

            BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
            if (billPaymentStatus != null && billPaymentStatus.getSuccess() == null
                    && FCUtil.containsPartialMatchCaseInsensitive(billPaymentStatus.getBillPaymentGateway(),
                            allowedAggregators)) {
                logger.info("Force refund under process postpaid OrderId - " + orderId);
                incrementAmountRefundedTillNow(uth.getTransactionAmount().longValue());
                userTransactionHistoryService.updateStatus(orderId,
                        PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND);
                forceRefundDAO.recordForceRefundAttempt(orderId,uth.getTransactionAmount());
                refundService.initiateBillPayRefund(orderId, transactionStatus ,PaymentConstants.FORCED_REFUND);
                return REFUND_SUCCESS;
            }
        } catch (Exception ex) {
            logger.error("Error while trying to do a time based refund.", ex);
        }
        return REFUND_FAILURE;
    }

    private boolean isEligibleForTimeboundRefund(UserTransactionHistory uth) {
        boolean VALIDATION_FAIL = false;
        boolean VALIDATION_SUCCESS = true;
        String basePath = fcProperties.getCCSBasePathRefund();
        // order age check
        String orderAgeMinKey = fcProperties.getRefundOrderAgeMinKey();
        String[] orderAgeMinLimit = FCUtil.getCCSValues(orderAgeMinKey, basePath, fcProperties.getCCSHost());
        
        String orderAgeMaxKey = fcProperties.getRefundOrderAgeMaxKey();
        String[] orderAgeMaxLimit = FCUtil.getCCSValues(orderAgeMaxKey, basePath, fcProperties.getCCSHost());
		if (!(uth.getLastUpdated().before(new DateTime().minusHours(Integer.parseInt(orderAgeMinLimit[0])).toDate())
				&& (uth.getLastUpdated()
						.after(new DateTime().minusHours(Integer.parseInt(orderAgeMaxLimit[0])).toDate())))) {
			logger.info(uth.getOrderId() + " - failed time limit validation.");
			return VALIDATION_FAIL;
		}
        // daily limit check
        String[] maxAmountPerDay = FCUtil.getCCSValues(fcProperties.getForceRefundMaxAmountPerDayKey(), basePath,
                fcProperties.getCCSHost());
        if (Long.valueOf(maxAmountPerDay[0]) <= (getAmountRefundedTillNow() + uth.getTransactionAmount())) {
            logger.info(uth.getOrderId() + " - Max refund amount limit for the day validation failed.");
            return VALIDATION_FAIL;
        }
        // allowed products check
        String productListKey = fcProperties.getRefundProductListKey();
        String[] allowedProducts = FCUtil.getCCSValues(productListKey, basePath, fcProperties.getCCSHost());
        if (!FCUtil.containsCaseInsensitive(uth.getProductType(), allowedProducts)) {
            logger.info(uth.getOrderId() + " - Product type validation failed.");
            return VALIDATION_FAIL;
        }
        // allowed channels check
        String channelListKey = fcProperties.getRefundChannelListKey();
        String[] allowedChannels = FCUtil.getCCSValues(channelListKey, basePath, fcProperties.getCCSHost());
        if (!ArrayUtils.isEmpty(allowedChannels)
                && !FCUtil.containsCaseInsensitive(FCUtil.getChannelFromOrderId(uth.getOrderId()), allowedChannels)) {
            logger.info(uth.getOrderId() + " - Channel type validation failed.");
            return VALIDATION_FAIL;
        }
        // allowed operators check
        String operatorListKey = fcProperties.getRefundOperatorListKey();
        String[] allowedOperators = FCUtil.getCCSValues(operatorListKey, basePath, fcProperties.getCCSHost());
        if (!ArrayUtils.isEmpty(allowedOperators)
                && !FCUtil.containsCaseInsensitive(uth.getServiceProvider(), allowedOperators)) {
            logger.info(uth.getOrderId() + " - Operator validation failed.");
            return VALIDATION_FAIL;
        }
        logger.info("Sending validation Success");
        return VALIDATION_SUCCESS;
    }

    private Long getAmountRefundedTillNow() {
        GetItemRequest getItemRequest = new GetItemRequest().withTableName(TIME_BOUND_REFUND_TABLE).withKey(ImmutableMap
                .of(REFUND_DATE, new AttributeValue(new SimpleDateFormat(DD_MM_YYYY).format(new Date()).toString())));

        GetItemResult getItemResult = dynamoClient.getItem(getItemRequest);
        Map<String, AttributeValue> item = getItemResult.getItem();
        return item != null && item.get(TOTAL_AMOUNT) != null ? Long.valueOf(item.get(TOTAL_AMOUNT).getN()) : 0;
    }

    private void incrementAmountRefundedTillNow(Long amount) {
        Map<String, AttributeValue> updateItemKey = new HashMap<String, AttributeValue>();
        updateItemKey.put(REFUND_DATE,
                new AttributeValue(new SimpleDateFormat(DD_MM_YYYY).format(new Date()).toString()));

        Map<String, AttributeValueUpdate> updateItemAttrs = new HashMap<String, AttributeValueUpdate>();
        updateItemAttrs.put(TOTAL_AMOUNT,
                new AttributeValueUpdate(new AttributeValue().withN(String.valueOf(amount)), AttributeAction.ADD));

        UpdateItemRequest updateItemRequest = new UpdateItemRequest(TIME_BOUND_REFUND_TABLE, updateItemKey,
                updateItemAttrs);
        dynamoClient.updateItem(updateItemRequest);
    }
    
    public void adjustAmountRefunded(UserTransactionHistory uth){
    	if(uth!=null && uth.getTransactionStatus().equals(PaymentConstants.TRANSACTION_STATUS_UP_FORCE_REFUND)){
    		decrementAmountRefundedTillNow(uth.getTransactionAmount().longValue());
    		logger.info("Total Burned Amount decremented by "+ uth.getTransactionAmount().longValue());
    	}
    }

	private void decrementAmountRefundedTillNow(long amount) {
		  Map<String, AttributeValue> updateItemKey = new HashMap<String, AttributeValue>();
	        updateItemKey.put(REFUND_DATE,
	                new AttributeValue(new SimpleDateFormat(DD_MM_YYYY).format(new Date()).toString()));

	        Map<String, AttributeValueUpdate> updateItemAttrs = new HashMap<String, AttributeValueUpdate>();
	        updateItemAttrs.put(TOTAL_AMOUNT,
	                new AttributeValueUpdate(new AttributeValue().withN(String.valueOf(amount*(-1))), AttributeAction.ADD));

	        UpdateItemRequest updateItemRequest = new UpdateItemRequest(TIME_BOUND_REFUND_TABLE, updateItemKey,
	                updateItemAttrs);
	        dynamoClient.updateItem(updateItemRequest);
		
	}

}