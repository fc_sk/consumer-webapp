package com.freecharge.payment.autorefund;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.InService.INRESPONSE;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AggregatorFactory;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.RechargeStatus;
import com.freecharge.recharge.RechargeStatusService;
import com.freecharge.recharge.businessdao.AggregatorInterface;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/9/12
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AutoRefundRechargeFailures {
    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(AutoRefundRechargeFailures.class.getName()));

    @Autowired
    private InService inService;
    @Autowired
    private AggregatorFactory aggregatorFactory;
    @Autowired
    private OperatorCircleService operatorCircleService;
    @Autowired
    private PaymentRefund paymentRefund;
    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private InReadDAO inReadDAO;
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
	@Autowired
    private RechargeStatusService reshargeStatusService;

    public Set<String> getMerchantOrderIds(Collection<String> orderIds) {
        Set<String> merchantOrderIds = new HashSet<String>();

        for (String orderId : orderIds) {
            List<PaymentTransaction> paymentTransactions = this.paymentTransactionService.getPaymentTransactionsByOrderId(orderId);

            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                merchantOrderIds.add(paymentTransaction.getMerchantTxnId());
            }

        }

        return merchantOrderIds;
    }

    public InService getInService() {
        return inService;
    }

    public void setInService(InService inService) {
        this.inService = inService;
    }

    public AggregatorFactory getAggregatorFactory() {
        return aggregatorFactory;
    }

    public void setAggregatorFactory(AggregatorFactory aggregatorFactory) {
        this.aggregatorFactory = aggregatorFactory;
    }

    public OperatorCircleService getOperatorCircleService() {
        return operatorCircleService;
    }

    public void setOperatorCircleService(OperatorCircleService operatorCircleService) {
        this.operatorCircleService = operatorCircleService;
    }

    public PaymentRefund getPaymentRefund() {
        return paymentRefund;
    }

    public void setPaymentRefund(PaymentRefund paymentRefund) {
        this.paymentRefund = paymentRefund;
    }
}
