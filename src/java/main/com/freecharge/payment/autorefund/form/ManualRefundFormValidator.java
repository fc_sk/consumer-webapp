package com.freecharge.payment.autorefund.form;

import java.util.Set;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.payment.autorefund.controller.ManualRefundController;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/10/12
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManualRefundFormValidator implements Validator {
    public static final int ORDER_ID_LIMIT = 100;

    @Override
    public boolean supports(Class<?> aClass) {
        return ManualRefundForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //todo : User message properties
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orderIds", "Order ids cannot be blank.", "Order ids cannot be blank.");

        Set<String> orderIds = ManualRefundController.getOrderIds((ManualRefundForm)o);

        if (orderIds.size() > ORDER_ID_LIMIT) {
            errors.rejectValue("orderIds", "error.invalid.orderIds", String.format("Cannot refund more than %d order ids at a time.", ORDER_ID_LIMIT));
        }
    }
}
