package com.freecharge.payment.autorefund.form;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/10/12
 * Time: 5:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class BillDeskPaymentTransactionIdUpdatorForm {
    //comma separated values of <merchant transaction id>, <bill desk payment transaction id>
    private String csv;

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }
}
