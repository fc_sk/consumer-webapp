package com.freecharge.payment.autorefund.form;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.common.util.FCUtil;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/10/12
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class BillDeskPaymentTransactionIdUpdatorFormValidator implements Validator {
    public static final int LINE_LIMIT = 100;

    @Override
    public boolean supports(Class<?> aClass) {
        return BillDeskPaymentTransactionIdUpdatorForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //todo : use message bundle for this
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "csv", "Order ids cannot be blank.", "Order ids cannot be blank.");

        String[] lines = ((BillDeskPaymentTransactionIdUpdatorForm)o).getCsv().split(FCUtil.getNewLineCharacter());

        if (lines.length > LINE_LIMIT) {
            errors.rejectValue("csv", "error.invalid.csv", String.format("Cannot update more than %d transactions at a time.", LINE_LIMIT));
        }
    }
}
