package com.freecharge.payment.autorefund.form;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.freecharge.common.util.FCUtil;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 27/10/12
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return RefundForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //todo : User message properties
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "from", "From date field cannot be blank.", "From date field cannot be blank.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "to", "From date field cannot be blank.", "From date field cannot be blank.");

        RefundForm refundForm = (RefundForm)o;

        int days = FCUtil.noOfDays(refundForm.getFrom(), refundForm.getTo());
        if (days > 1) {
            errors.rejectValue("to", "error.invalid.dateInterval", String.format("No of day between start and end time should not be greater than 1. Currently it is %s", days));
        }
    }
}
