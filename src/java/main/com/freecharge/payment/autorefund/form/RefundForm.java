package com.freecharge.payment.autorefund.form;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 27/10/12
 * Time: 2:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class RefundForm {
    public Date from;
    public Date to;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
