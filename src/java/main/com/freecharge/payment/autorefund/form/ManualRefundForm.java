package com.freecharge.payment.autorefund.form;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/10/12
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class ManualRefundForm {
    private String orderIds;

    public String getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String orderIds) {
        this.orderIds = orderIds;
    }
}
