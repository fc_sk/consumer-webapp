package com.freecharge.payment.autorefund;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;

public class RefundThreadPoolExecutor {

    private static Logger logger = LoggingFactory.getLogger(RefundThreadPoolExecutor.class);


    int poolSize = 10;

    int maxPoolSize = 100;

    long keepAliveTime = 100000;

    ThreadPoolExecutor threadPool = null;

    final ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(
            100);

    RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();


    public RefundThreadPoolExecutor()
    {
        threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize,
                keepAliveTime, TimeUnit.SECONDS, queue,rejectedExecutionHandler);

    }

    public Integer getActiveCount(){
        return threadPool.getActiveCount();
    }

    public void runTask(Runnable task)
    {
    	threadPool.execute(task);
    }

    public void shutDown()
    {
        threadPool.shutdown();
        try {
			threadPool.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
            logger.error("in shutting down the executor thread pool");
		}
    }

}
