package com.freecharge.payment.autorefund;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 9/10/12
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class AutoRefund {
    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(AutoRefund.class.getName()));

    @Autowired
    private AutoRefundPaymentGatewayFailures autoRefundPaymentGatewayFailures;

    @Autowired
    private AutoRefundRechargeFailures autoRefundRechargeFailures;

    public AutoRefundPaymentGatewayFailures getAutoRefundPaymentGatewayFailures() {
        return autoRefundPaymentGatewayFailures;
    }

    public void setAutoRefundPaymentGatewayFailures(AutoRefundPaymentGatewayFailures autoRefundPaymentGatewayFailures) {
        this.autoRefundPaymentGatewayFailures = autoRefundPaymentGatewayFailures;
    }

    public AutoRefundRechargeFailures getAutoRefundRechargeFailures() {
        return autoRefundRechargeFailures;
    }

    public void setAutoRefundRechargeFailures(AutoRefundRechargeFailures autoRefundRechargeFailures) {
        this.autoRefundRechargeFailures = autoRefundRechargeFailures;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        AutoRefund autoRefund = ((AutoRefund) ctx.getBean("autoRefund"));

        Date start = RefundUtil.getDate(args[0]);
        Date end = RefundUtil.getDate(args[1]);

        logger.info(String.format("Doing auto refund for candidates from %s to %s", start, end));

        try {
            autoRefund.doRefund(start, end, false);
        } catch (Exception e) {
            logger.error("Exception while doing autorefund", e);
        } finally {
            ctx.close();
            System.exit(0);
        }
    }

    public void doRefund(Date from, Date to, Boolean async) throws Exception {
        if (async) {
            sweepExcludeFailures(from, to);
        } else {
            this.autoRefundPaymentGatewayFailures.sweep(from, to);
        }
    }

    public void sweepExcludeFailures(Date from, Date to) throws Exception {
        this.autoRefundPaymentGatewayFailures.sweepAsync(from, to);
    }
}