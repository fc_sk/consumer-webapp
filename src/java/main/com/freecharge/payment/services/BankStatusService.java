package com.freecharge.payment.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.web.PaymentHealthRequest;
import com.freecharge.payment.dos.web.PaymentHealthResponse;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;

@Service
public class BankStatusService {

    @Autowired
    private MetricsClient                    metricsClient;

    @Autowired
    UserServiceProxy                         userServiceProxy;

    @Autowired
    @Qualifier("payUGatewayHandler")
    private CardGatewayHandler               payUGatewayHandler;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private  CardGatewayHandler              klickpayGatewayhandler;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private  UPIPaymentStatusHandler         klickpayUPIPaymentStatusGatewayhandler;

    @Autowired
    FCProperties                             fcProperties;

    @Autowired
    AppConfigService                         appConfigService;

    @Autowired
    PgReadDAO                                pgReadDAO;

    @Autowired
    PaymentsCache                            paymentsCache;
    
    private static final String              BANK_STATUS_UP_CODE     = "1";
    private static final String              BANK_STATUS             = "up_status";
    private static final String              ISSUING_BANK_NETBANKING = "title";
    private static final String              ISSUING_BANK_CARDS      = "issuing_bank";
    private static final String              IS_BLOCK                = "isBlock";
    private static final String              IS_BANK_DOWN            = "is.bank.down";
    private static final String              IBIB0_CODE              = "ibibo_code";
    private static final String              IS_ALLOWED_FOR_PURPOSE   = "is_allowed_for_purpose";
    private static final String              NOT_ALLOWED_REASON      = "not_allowed_reason";
    private static final String              UPI                     = "UPI";
    private static final String              CARD                    = "CARD";
    private static final String              NB                      = "NB";
    private static final String              SUCCESS_STATUS_CODE     = "200";
    private static final String              FAILURE_STATUS_CODE     = "400";
    private static final String              SUCCESS_STATUS_MSG     = "SUCCESS";

    private final Logger                     logger                  = LoggingFactory.getLogger(getClass());
    
    private static final Map<String, String> BANK_CODE_MAPPING       = new HashMap<>();

    static {
        BANK_CODE_MAPPING.put("ABN", "ABN AMRO");
        BANK_CODE_MAPPING.put("ALLBD", "ALLAHABAD BANK");
        BANK_CODE_MAPPING.put("ANDHRA", "ANDHRA BANK");
        BANK_CODE_MAPPING.put("AXIS", "AXIS BANK LIMITED");
        BANK_CODE_MAPPING.put("BOB", "BANK OF BARODA");
        BANK_CODE_MAPPING.put("BOI", "BANK OF INDIA");
        BANK_CODE_MAPPING.put("BOM", "BANK OF MAHARASHTRA");
        BANK_CODE_MAPPING.put("BARC", "BARCLAYS BANK");
        BANK_CODE_MAPPING.put("CANA", "CANARA BANK");
        BANK_CODE_MAPPING.put("CBI", "CENTRAL BANK OF INDIA");
        BANK_CODE_MAPPING.put("CITI", "CITI");
        BANK_CODE_MAPPING.put("CUB", "CITY UNION BANK LTD");
        BANK_CODE_MAPPING.put("CORP", "CORPORATION BANK");
        BANK_CODE_MAPPING.put("DENA", "Dena Bank");
        BANK_CODE_MAPPING.put("DEUT", "DEUTSCHE BANK");
        BANK_CODE_MAPPING.put("DCB", "DEVELOPMENT CREDIT BANK LTD");
        BANK_CODE_MAPPING.put("GMFS", "GE MONEY FINANCIAL SERVICES LTD");
        BANK_CODE_MAPPING.put("HDFC", "HDFC");
        BANK_CODE_MAPPING.put("ICICI", "ICICI");
        BANK_CODE_MAPPING.put("IDBI", "IDBI BANK LTD");
        BANK_CODE_MAPPING.put("INDB", "INDIAN BANK");
        BANK_CODE_MAPPING.put("IOB", "INDIAN OVERSEAS BANK");
        BANK_CODE_MAPPING.put("INDUSIND", "INDUSIND BANK LIMITED");
        BANK_CODE_MAPPING.put("INGV", "ING VYSYA BANK_LIMITED");
        BANK_CODE_MAPPING.put("JKB", "JAMMU & KASHMIR BANK LTD.");
        BANK_CODE_MAPPING.put("KBL", "KARNATAKA BANK LIMITED");
        BANK_CODE_MAPPING.put("KOTAK", "KOTAK MAHINDRA BANK LTD");
        BANK_CODE_MAPPING.put("OBC", "ORIENTAL BANK OF COMMERCE");
        BANK_CODE_MAPPING.put("PNB", "PUNJAB NATIONAL BANK");
        BANK_CODE_MAPPING.put("SBIDC", "SBI");
        BANK_CODE_MAPPING.put("SYND", "SYNDICATE BANK");
        BANK_CODE_MAPPING.put("TNB", "Tamilnad Mercantile Bank Limited");
        BANK_CODE_MAPPING.put("TBR", "THE BANK OF RAJASTHAN LIMITED");
        BANK_CODE_MAPPING.put("TCSB", "THE CATHOLIC SYRIAN BANK LIMITED");
        BANK_CODE_MAPPING.put("TCCB", "THE COSMOS CO-OP BANK LIMITED");
        BANK_CODE_MAPPING.put("DBL", "The Dhanalakshmi Bank Limited");
        BANK_CODE_MAPPING.put("FEDE", "THE FEDERAL BANK");
        BANK_CODE_MAPPING.put("HKS", "THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMI");
        BANK_CODE_MAPPING.put("KARUR", "THE KARUR VYSYA BANK LTD");
        BANK_CODE_MAPPING.put("SCB", "STANDARD CHARTERED BANK");
        BANK_CODE_MAPPING.put("UCO", "UCO BANK");
        BANK_CODE_MAPPING.put("UBI", "UNION BANK OF INDIA");
        BANK_CODE_MAPPING.put("UNB", "United Bank of India");
        BANK_CODE_MAPPING.put("VIJY", "VIJAYA BANK");
        BANK_CODE_MAPPING.put("YES", "YES BANK LIMITED");
        BANK_CODE_MAPPING.put("SBICC", "SBI");
        BANK_CODE_MAPPING.put("AMEX", "AMERICAN EXPRESS");
        BANK_CODE_MAPPING.put("RBS", "ROYAL BANK OF SCOTLAND");
        BANK_CODE_MAPPING.put("SIB", "SOUTH INDIAN BANK");
        BANK_CODE_MAPPING.put("DBS", "DBS BANK");
        BANK_CODE_MAPPING.put("RBL", "RATNAKAR BANK LIMITED");
        BANK_CODE_MAPPING.put("RATNAKAR", "RATNAKAR BANK LIMITED");
        BANK_CODE_MAPPING.put("HSBC", "HSBC");
        BANK_CODE_MAPPING.put("LVB", "Lakshmi Vilas Bank");
        BANK_CODE_MAPPING.put("ACB", "ABHYUDAYA COOPERATIVE BANK");
        BANK_CODE_MAPPING.put("ADCB", "Adarsh Co-operative Bank Ltd., Hyderabad");
        BANK_CODE_MAPPING.put("AMCUBL", "AP MAHESH COOPERATIVE URBAN BANK LIMITED");
        BANK_CODE_MAPPING.put("CLB", "Capital Local Bank");
        BANK_CODE_MAPPING.put("DNSBL", "Dombivli Nagari Sahakari Bank Limited ");
        BANK_CODE_MAPPING.put("GPPJSB", "GP PARSIKÂ  PATIL JANATA SAHAKARI BANK");
        BANK_CODE_MAPPING.put("GSCB", "Gujarat State co-op Bank");
        BANK_CODE_MAPPING.put("HCB", "Hasti Co-operative Bank");
        BANK_CODE_MAPPING.put("JSBLP", "JANATA SAHAKARI BANK LTD PUNE");
        BANK_CODE_MAPPING.put("JSBL", "Janseva Sahakari Bank Ltd (Borivili)");
        BANK_CODE_MAPPING.put("KGB", "Kerala Gramin Bank");
        BANK_CODE_MAPPING.put("MGB", "MAHARASHTRA GRAMIN BANK");
        BANK_CODE_MAPPING.put("MS", "Maharashtra SCB");
        BANK_CODE_MAPPING.put("MUDB", "MUDRA BIN");
        BANK_CODE_MAPPING.put("MUCBM", "MUNICIPAL CO-OPERATIVE BANK MUMBAI");
        BANK_CODE_MAPPING.put("NNSBL", "Nutan Nagarik Sahakari Bank Ltd");
        BANK_CODE_MAPPING.put("PUCB", "Pochampally Urban Co-op Bank");
        BANK_CODE_MAPPING.put("PKGB", "Pragati Krishna Gramin Bank");
        BANK_CODE_MAPPING.put("PASB", "Punjab and Sindh Bank");
        BANK_CODE_MAPPING.put("SCBL", "SARASWAT CO-OPERATIVE BANK LTD");
        BANK_CODE_MAPPING.put("SWSB", "Shree Warana Sahakari Bank");
        BANK_CODE_MAPPING.put("SACB", "Shri Arihant Co-operative Bank");
        BANK_CODE_MAPPING.put("SBBJ", "State Bank of Bikaner & Jaipur");
        BANK_CODE_MAPPING.put("SBH", "State Bank of Hyderabad");
        BANK_CODE_MAPPING.put("SBM", "State Bank of Mysore");
        BANK_CODE_MAPPING.put("SBP", "State Bank of Patiala");
        BANK_CODE_MAPPING.put("SBT", "State Bank of Travancore");
        BANK_CODE_MAPPING.put("SUSOB", "SUCO Souharda Bank");
        BANK_CODE_MAPPING.put("SUPCOB", "Surat People's Co-Op Bank");
        BANK_CODE_MAPPING.put("TADCB", "THE AHMEDABAD DISTRICT CO-OPERATIVE BANK");
        BANK_CODE_MAPPING.put("ACCB", "The Amritsar Central Co-op Bank (CCB)");
        BANK_CODE_MAPPING.put("TBCCB", "The Bathinda CCB");
        BANK_CODE_MAPPING.put("TBCOBLM", "THE BHARAT CO-OPERATIVE BANK LTD. MUMBAI");
        BANK_CODE_MAPPING.put("TFCCB", "The Faridkot CCB");
        BANK_CODE_MAPPING.put("TFSCCB", "The Fatehgarh Sahib CCB");
        BANK_CODE_MAPPING.put("TFACCB", "The Fazilka CCB");
        BANK_CODE_MAPPING.put("TFECCB", "The Ferozepur CCB");
        BANK_CODE_MAPPING.put("TGUCCB", "The Gurdaspur CCB");
        BANK_CODE_MAPPING.put("THOCCB", "The Hoshiarpur CCB");
        BANK_CODE_MAPPING.put("TGPCOB", "The Jalgaon Peoples Co-op Bank");
        BANK_CODE_MAPPING.put("TJUCCB", "The Jullundur CCB");
        BANK_CODE_MAPPING.put("TKCCOBL", "THE KALUPUR COMMERCIAL CO-OPERATIVE BANK LTD");
        BANK_CODE_MAPPING.put("TKJSB", "The Kalyan Janata Sahakari Bank");
        BANK_CODE_MAPPING.put("TKCCB", "The Kapurthala CCB");
        BANK_CODE_MAPPING.put("TLCCB", "The Ludhiana CCB");
        BANK_CODE_MAPPING.put("TMACOB ", "The Mahanagar Co-operative Bank");
        BANK_CODE_MAPPING.put("TMSBL", "The Malad Sahakari Bank Ltd");
        BANK_CODE_MAPPING.put("TMCCB", "The Mansa CCB");
        BANK_CODE_MAPPING.put("TMOCCB", "The Moga CCB");
        BANK_CODE_MAPPING.put("TMUCCB", "The Muktsar CCB");
        BANK_CODE_MAPPING.put("TNACCB", "The Nawanshahr CCB");
        BANK_CODE_MAPPING.put("TOSCOB", "The Odisha State co-op Bank");
        BANK_CODE_MAPPING.put("TPACCB", "The Patiala CCB");
        BANK_CODE_MAPPING.put("TPRCOBL", "THE PRIME CO-OPERATIVE BANK LTD");
        BANK_CODE_MAPPING.put("TPUSCOB", "The Punjab State Co-op Bank");
        BANK_CODE_MAPPING.put("TROCCB", "The Ropar CCB");
        BANK_CODE_MAPPING.put("TSASNCCB", "The S.A.S. Nagar CCB");
        BANK_CODE_MAPPING.put("TSACCB", "The Sangrur CCB");
        BANK_CODE_MAPPING.put("TSHVCOB", "The Shamrao Vithal co-op Bank");
        BANK_CODE_MAPPING.put("TTTCCB", "The Tarn Taran CCB");
        BANK_CODE_MAPPING.put("TZCOBL", "The Zoroastrian Co-Operative Bank Ltd.");
        BANK_CODE_MAPPING.put("TDCB", "THRISSUR DISTRICT COOPERATIVE BANK");
        BANK_CODE_MAPPING.put("TJSBSB", "TJSB Sahakari Bank");
        BANK_CODE_MAPPING.put("VDCCB", "Valsad DCCB");
        BANK_CODE_MAPPING.put("VVSB", "VASAI VIKAS SAHAKARI BANK");
    }

    
	public PaymentHealthResponse cardPaymentHelathStatus(PaymentHealthRequest input ,PaymentHealthResponse paymentHealthResponse) {

		Map<String, Object> respMap = new HashMap<>();
		final String cardDataParam = "cardData";
		Map<String, String> paymentDetails=input.getPaymentHealthRequestDetails();
		String cardData = paymentDetails.get(cardDataParam);
		String paymentPurpose = paymentDetails.get("paymentPurpose");
		String GVLoad = paymentDetails.get("GVLoad");
		
		Integer userId = userServiceProxy.getLoggedInUser().getUserId();
		if (StringUtils.isBlank(cardData) || !cardData.matches("[0-9]+") || cardData.length() < 6) {
			paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Invalid card details");
			return paymentHealthResponse;
		}
		cardData = cardData.substring(0, 6);
		try {
			logger.info("Getting status from Klickpay");
			respMap = klickpayGatewayhandler.getIssuingBankStatus(cardData);
		} catch (Exception e) {
			paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Caught exception on checking issuing bank status.");
			logger.error("Caught exception on checking issuing bank status.", e);
			return paymentHealthResponse;
		}
		if (null == respMap || respMap.isEmpty()) {
			paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Caught exception on checking issuing bank status.");
			return paymentHealthResponse;
		}
		String upStatus = (String) respMap.get(BANK_STATUS);
		String issuingBank = (String) respMap.get(ISSUING_BANK_CARDS);
		respMap.put(IS_BLOCK, getValueFromCCS(IS_BANK_DOWN));
		if (!StringUtils.isEmpty(upStatus) && upStatus.equals("0") && respMap.containsKey("block")) {
			respMap.put(IS_BLOCK, respMap.remove("block"));
		}
		
		if (BANK_CODE_MAPPING.containsKey(issuingBank)) {
			respMap.put(ISSUING_BANK_CARDS, BANK_CODE_MAPPING.get(issuingBank));
		} else {
			respMap.put(ISSUING_BANK_CARDS, "");
		}
		respMap.put(IBIB0_CODE, cardData);
		
		JSONObject ppiSwitches = appConfigService.getCustomProp(FCConstants.PPI_SWITCHES);
		Boolean isCreditCardsEnabledForGVLoad = Boolean.valueOf((String) ppiSwitches.get(FCConstants.PPI_GVLOAD_IS_CREDIT_CARD_ALLOWED));

		respMap.put(IS_ALLOWED_FOR_PURPOSE, true);
		// If request contains txnPurpose, then check for card_type which is received from klickpay
		if (paymentPurpose!=null) {
			if (PaymentConstants.PAYMENT_PAYMENT_PURPOSE_GIFT_VOUCHER_LOAD.equals(GVLoad)) {
				respMap.put(IS_ALLOWED_FOR_PURPOSE, true);
				if (!isCreditCardsEnabledForGVLoad && FCConstants.CARD_TYPE_CREDIT_CARD.equals(respMap.get("card_type"))) {
					respMap.put(IS_ALLOWED_FOR_PURPOSE, false);
					respMap.put(NOT_ALLOWED_REASON, "Credit cards are not applicable on adding voucher.");
				} else if (null == respMap.get("card_type") || respMap.get("card_type").toString().length() == 0) {
					respMap.put(IS_ALLOWED_FOR_PURPOSE, false);
					respMap.put(NOT_ALLOWED_REASON, "Invalid Card");
				}
			}
		}
		String status=(String) respMap.get(BANK_STATUS);
		respMap.remove(BANK_STATUS);
		respMap.remove("mssg");
		paymentHealthResponse.setStatusCode(SUCCESS_STATUS_CODE);
		paymentHealthResponse.setStatusMsg(SUCCESS_STATUS_MSG);
		paymentHealthResponse.setPaymentHealthStatus(status);
		paymentHealthResponse.setPaymentHealthResponseDetails(respMap);
		return paymentHealthResponse;
	}

	public PaymentHealthResponse upiPaymentHealthStatus(PaymentHealthResponse paymentHealthResponse) {
		try {
			logger.info("Inside UPI payment health status");
			paymentHealthResponse = klickpayUPIPaymentStatusGatewayhandler.checkUPIPaymentHealthStatus(paymentHealthResponse);
		} catch (Exception e) {
		    logger.error("Caught exception on checking UPI status.", e);
		    paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Caught exception on checking UPI status.");
			return paymentHealthResponse;
		}
		return paymentHealthResponse;
	}
	
	public PaymentHealthResponse NBHealthStatus(PaymentHealthRequest input, PaymentHealthResponse paymentHealthResponse) {
		
		logger.info("Inside NB payment health status");
		
		final String bankCodeParam = "bankCode";
		Map<String, String> paymentDetails=input.getPaymentHealthRequestDetails();
		String bankCode = paymentDetails.get(bankCodeParam);
		
        Map<String, Object> respMap = new HashMap<>();
        if (bankCode==null || bankCode.isEmpty()) {
        	paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Bank code for NB is null");
			return paymentHealthResponse;
        }
        try {
            logger.info("Getting status from Klickpay");
            respMap = klickpayGatewayhandler.getNetBankingStatus(bankCode);
        } catch (Exception e) {
        	logger.error("Caught exception on checking NB status from klickpay", e);
		    paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Caught exception on checking NB status from klickpay.");
			return paymentHealthResponse;
        }
        logger.info("Net bank status for  bank code =" + bankCode + " is = " + respMap);

        if (null == respMap || respMap.isEmpty()) {
        	paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
			paymentHealthResponse.setStatusMsg("Response from klickpay is null");
			return paymentHealthResponse;
        }
       
        String upStatus = respMap.get(BANK_STATUS).toString();
        
        respMap.put(IS_BLOCK, getValueFromCCS(IS_BANK_DOWN));
        if (!StringUtils.isEmpty(upStatus) && upStatus.equals("0") && respMap.containsKey("block")) {
            respMap.put(IS_BLOCK, respMap.remove("block"));
        }
        respMap.put(IBIB0_CODE, bankCode);
        if (!respMap.containsKey("mssg")) {
            respMap.put("mssg", "");
        }
        
        String status=(String) respMap.get(BANK_STATUS);
		respMap.remove(BANK_STATUS);
		respMap.remove("mssg");
		paymentHealthResponse.setPaymentHealthStatus(status);
		paymentHealthResponse.setStatusCode(SUCCESS_STATUS_CODE);
		paymentHealthResponse.setStatusMsg(SUCCESS_STATUS_MSG);
		paymentHealthResponse.setPaymentHealthResponseDetails(respMap);
		return paymentHealthResponse;
		
	}

	public PaymentHealthResponse paymentHealthStatus(PaymentHealthRequest input) {
        PaymentHealthResponse paymentHealthResponse= new PaymentHealthResponse();
		String paymentType= input.getPaymentType();
		switch (paymentType) { 
	        case UPI: 
	        	return upiPaymentHealthStatus(paymentHealthResponse);
		    case CARD: 
	        	return cardPaymentHelathStatus(input, paymentHealthResponse);
		    case NB:
		    	return NBHealthStatus(input, paymentHealthResponse);
		    default: 
	        	logger.error("Payment type in request map is not correct");
				paymentHealthResponse.setStatusCode(FAILURE_STATUS_CODE);
				paymentHealthResponse.setStatusMsg("PaymentType is not valid");
				return paymentHealthResponse;  
		 }	
	}
	
    public String getValueFromCCS(String key) {
        ICCS ccs;
        ICCSConfig ccsConfig = new AbstractCCSConfig();

        ccsConfig.setCcsHost(fcProperties.getCCSHost());
        ccsConfig.setDefaultAttachPath(fcProperties.getCCSBasePathPayment());

        ccs = new CCS();
        ccs.init(ccsConfig);
        String value = null;
        try {
            value = ccs.getConf(key);
            if (!FCUtil.isEmpty(value)) {
                return value;
            }
        } catch (CCSException e) {
            logger.error("Caught Exception is ", e);
        } catch (Exception e) {
            logger.error("Caught Exception is ", e);
        }

        return null;
    }


	
}
