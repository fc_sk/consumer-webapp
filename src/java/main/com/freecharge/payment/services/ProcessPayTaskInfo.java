package com.freecharge.payment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.payment.dos.entity.ProcessPayTaskRequest;
import com.freecharge.payment.util.PaymentConstants;
import com.snapdeal.payments.ts.registration.TaskExecutor;
import com.snapdeal.payments.ts.registration.TaskRegistrationInfo;
import com.snapdeal.payments.ts.registration.TaskSerializer;

@Component
public class ProcessPayTaskInfo implements TaskRegistrationInfo {
	
	@Override
	public TaskSerializer<ProcessPayTaskRequest> getTaskSerializer() {
		return serializer;
	}
	
	@Autowired
	private ProcessPayTaskExecutor executor;
	
	@Autowired
	private ProcessPayTaskSerializer serializer;

	@Override
	public TaskExecutor getTaskExecutor() {
		return executor;
	}

	@Override
	public String getTaskType() {
		return PaymentConstants.PROCESS_PAY_TASK_TYPE;
	}

	@Override
	public long getRetryLimit() {
		return PaymentConstants.DEFAULT_PROCESS_PAY_RETRY_LIMIT;
	}

	@Override
	public long getExecutionTime() {
		return 30 * 1000;
	}

	@Override
	public boolean isRecurring() {
		return false;
	}

}
