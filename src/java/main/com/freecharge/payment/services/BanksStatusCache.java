package com.freecharge.payment.services;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class BanksStatusCache extends RedisCacheManager {
	
	private final long  TIME_TO_CACHE = 5;
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
    @Autowired
    private RedisTemplate<String, Object> banksCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.banksCacheTemplate;
    }
    
	public Map<String, Object> getIssuingBankStatusByCardNo(
			String cardFirstSixDigit) {
		try {
			return (HashMap<String, Object>) get(cardFirstSixDigit);
		} catch (Exception e) {
			logger.error("Error in getting cache data for issuing bank status", e);
			return null;
		}
	}

	public void setIssuingBankStatus(String cardFirstSixDigit,
			Map<String, Object> respMap) {
		try {
			set(cardFirstSixDigit, respMap, TIME_TO_CACHE, TimeUnit.MINUTES);
		} catch (Exception e) {
			logger.error("Error in setting cache data for issuing bank status", e);
		}
	}
	
	public Map<String, Object> getNetBankingStatusByBankCode(String bankCode) {
		try {
			return (HashMap<String, Object>) get(bankCode);
		} catch (Exception e) {
			logger.error("Error in getting cache data for net bank status", e);
			return null;
		}
	}
	
	public void setNetBankingStatus(String bankCode, Map<String, Object> respMap) {
		try {
			set(bankCode, respMap, TIME_TO_CACHE, TimeUnit.MINUTES);
		} catch (Exception e) {
			logger.error("Error in setting cache data for net bank status", e);
		}
	}
}