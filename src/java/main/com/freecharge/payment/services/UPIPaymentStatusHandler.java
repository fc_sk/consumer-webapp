package com.freecharge.payment.services;

import com.freecharge.payment.dos.web.PaymentHealthResponse;

public interface UPIPaymentStatusHandler {
	
    public PaymentHealthResponse checkUPIPaymentHealthStatus(PaymentHealthResponse paymentHealthResponse);
}
