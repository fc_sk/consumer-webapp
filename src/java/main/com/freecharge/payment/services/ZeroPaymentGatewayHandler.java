package com.freecharge.payment.services;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.freecharge.common.framework.util.CSRFTokenManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.freecharge.zeropayment.ZeroPaymentController;
import com.freecharge.zeropayment.ZeroPaymentUtil;

@Component("zeroPaymentGatewayHandler")
public class ZeroPaymentGatewayHandler implements IGatewayHandler {

    private static Logger logger = LoggingFactory.getLogger(ZeroPaymentGatewayHandler.class);

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private PaymentsCache paymentsCache;
    
    @Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws Exception {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String freeChargeOrderId = paymentRequestBusinessDO.getOrderId();
        String merchantOrderId = paymentRequestBusinessDO.getMtxnId();
        merchantOrderId = StringUtils.trim(merchantOrderId);
        
        String pgAmount = "";
        String amount = paymentRequestBusinessDO.getAmount();
        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }
        
        Map<String, String> retMap = new LinkedHashMap<String, String>();

        retMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);

        Map<String, Object> map = fs.getSessionData();
        int userId = (Integer)map.get(WebConstants.SESSION_USER_USERID);

        retMap.put(PaymentConstants.USER_ID, String.valueOf(userId));
        retMap.put(PaymentConstants.HASH, ZeroPaymentUtil.createHash(merchantOrderId, userId));
        retMap.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());

        fs.getSessionData().put(merchantOrderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantOrderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantOrderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, retMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, ZeroPaymentController.ZERO_PAY_ACTION);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_ZEROPG_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);
        

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(pgAmount));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantOrderId);
        paymentRequest.setOrderId(freeChargeOrderId);
        paymentRequest.setPaymentGateway(PaymentGateways.FREECHARGE_ZEROPAY.getName());
        paymentRequest.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(retMap));

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(amount));
        pt.setMerchantTxnId(merchantOrderId);
        pt.setOrderId(freeChargeOrderId);
        pt.setPaymentGateway(PaymentGateways.FREECHARGE_ZEROPAY.getName());
        pt.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        pt = createPaymentTransaction(pt);

        paymentRequestBusinessDO.setResultMap(resultMap);

        fs.getSessionData().put(merchantOrderId + "_txn_id", pt.getPaymentTxnId());

        return resultMap;
    }

    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        Boolean isSuccessful = true;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String merchantOrderId = requestMap.get(PaymentConstants.MERCHANT_ORDER_ID);
        logger.info("merchantOrderId in zeroPayment gateway" + merchantOrderId);
        String transactionId = requestMap.get(PaymentConstants.TRANSACTION_ID);
        String amount = requestMap.get(PaymentConstants.AAMOUNT);
        String message = requestMap.get(PaymentConstants.MESSAGE);

        /*if (ZeroPaymentUtil.doesZeroPaymentResponseHashMatch(requestMap)) {
            if (Boolean.valueOf(result)) {
                isSuccessful = true;
            } else {
                isSuccessful = false;
            }
        }*/

        String mode = null;

        Integer id = (Integer)fs.getSessionData().get(merchantOrderId + "_txn_id");
        String productOrderId = FCSessionUtil.getProductOrderId(merchantOrderId, fs);
        //If id is not found in session get it from DB
        if (id == null|| productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId).get(0);
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        String redirectUrl = FCSessionUtil.getSuccessUrl(merchantOrderId, fs);
        String errorUrl = FCSessionUtil.getErrorUrl(merchantOrderId, fs) + "&payment=fail";
        if (productOrderId == null || redirectUrl == null || errorUrl == null) {
            PaymentTransaction uniquePaymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(merchantOrderId);
            productOrderId = uniquePaymentTransaction.getOrderId();
            String lookUpId = orderIdSlaveDAO.getLookupIdForOrderId(productOrderId);
            redirectUrl = ViewConstants.REDIRECT_URL;
            errorUrl = ViewConstants.ERROR_URL+"?lid="+lookUpId+"&payment=fail";
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String pg = PaymentGateways.FREECHARGE_ZEROPAY.getName();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, isSuccessful.toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, productOrderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, pg);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, mode);


        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);

        if (isSuccessful) {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);
        } else {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, errorUrl);
        }

        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(merchantOrderId);
        logger.info("amount value " + amount);
        PaymentResponse paymentResponse = new PaymentResponse(
                null,
                isSuccessful,
                transactionId,
                productOrderId,
                merchantOrderId,
                PaymentUtil.getMapAsString(requestMap),
                new Double(amount),
                pg,
                message,
                null,
                null,
                null,
                null,
                null,
                PaymentUtil.getMapAsString(resultMap),
                null,
                null
        );

        paymentResponse = dumpResponseAudit(paymentResponse);

        PaymentTransaction pt = new PaymentTransaction(
                id,
                transactionId,
                null,
                null,
                Calendar.getInstance().getTime(),
                null,
                null,
                isSuccessful,
                null,
                null,
                null, //response code
                null,
                paymentResponse.getPaymentRsId(),
                null,
                null,
                null
        );

        updatePaymentTransaction(pt);

        return finalMap;
    }

    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
    	/*
		 * Refresh payment transaction. 
		 */
		PaymentTransaction refreshedPaymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentQueryBusinessDO.getPaymentTxnId());
		
		if(refreshedPaymentTransaction.getIsSuccessful()) {
			paymentQueryBusinessDO.setPgTransactionTime(new Timestamp(refreshedPaymentTransaction.getSetToPg().getTime()));
		}
		
		paymentQueryBusinessDO.setPgTransactionId("NA");
		paymentQueryBusinessDO.setPaymentStatus(refreshedPaymentTransaction.getIsSuccessful());
		paymentQueryBusinessDO.setResponseMsg("NA");

        logger.debug("Status check complete for ZEROPAY : " + paymentQueryBusinessDO.getOrderId());

		return paymentQueryBusinessDO;
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {

        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }

    private PaymentResponse dumpResponseAudit(PaymentResponse paymentResponse) {
        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;

    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
            pt = paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
        return pt;
    }

    private void updatePaymentTransaction(PaymentTransaction pt) {
        try {
            paymentTransactionService.updatePaymentTransaction(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
    }

	@Override
	public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO,
			String refundType) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
			throws Exception {
		// TODO Auto-generated method stub
	}

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        // TODO Auto-generated method stub
        return null;
    }

}
