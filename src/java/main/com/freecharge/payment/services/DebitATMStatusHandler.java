package com.freecharge.payment.services;

import java.util.List;
import java.util.Map;

import com.freecharge.cardstorage.CardDetailsDo;

public interface DebitATMStatusHandler {

    public List<CardDetailsDo> isDebitATMEnabled(String userId, List<CardDetailsDo> cardDetails);
}
