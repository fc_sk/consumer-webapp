package com.freecharge.payment.services;

import java.util.Map;

public interface CardStatusHandler {

    public Map<String, Object> isCardValid(String cardNumber);

}
