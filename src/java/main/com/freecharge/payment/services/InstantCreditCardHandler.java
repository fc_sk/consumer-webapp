package com.freecharge.payment.services;

import com.freecharge.cardstorage.CardStorageService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.ChecksumCalculator;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.exception.HTTPAbortException;
import com.freecharge.payment.exception.HashValidationException;
import com.freecharge.payment.exception.VirtualCardStorageServiceException;
import com.freecharge.payment.model.InstantCreditCardApiErrorCodes;
import com.freecharge.payment.util.EncryptionUtil;
import com.freecharge.web.webdo.*;
import com.google.gson.Gson;
import net.logstash.logback.encoder.org.apache.commons.lang.RandomStringUtils;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component("instantCreditCardHandler")
public class InstantCreditCardHandler {

    @Autowired
    ApiHandler vccApiHandler;

    @Autowired
    FCProperties fcProperties;

    @Autowired
    private CardStorageService cardService;

    Logger LOGGER = Logger.getLogger(InstantCreditCardHandler.class);


    public InstantCreditCardResponse fetchInstantCreditCardDetail(VccCardStorageRequest vccCardStorageRequest) throws VirtualCardStorageServiceException{
        LOGGER.info("Request to fetch card initiated for user_id :"+vccCardStorageRequest.getUserId()+" application serial no is :"+vccCardStorageRequest.getApplicationSerNo());
        Map<String,String> header=new HashMap<>();
        createRequestHeader(header);
        FetchCardDetailsRequest fetchCardDetailsRequest=createFetchCardDetailRequest(vccCardStorageRequest);
        InstantCreditCardRequest instantCreditCardRequest=new InstantCreditCardRequest();
        instantCreditCardRequest.setFetchCardDetailsRequest(fetchCardDetailsRequest);

        InstantCreditCardResponse instantCreditCardResponse=new InstantCreditCardResponse();


        StringBuilder statusIdentifier = new StringBuilder();
        try {
                instantCreditCardResponse = vccApiHandler.executeHTTPPost(fcProperties.getProperty(FCProperties.FETCH_CARD_DETAIL_URL), instantCreditCardRequest,
                        header, 3000, Integer.parseInt(fcProperties.getProperty(FCProperties.VCC_FETCH_API_TIMEOUT)), InstantCreditCardResponse.class,
                        statusIdentifier);
                LOGGER.info("fetch credit card successful : user_id ="+vccCardStorageRequest.getUserId()+" serial no. ="+vccCardStorageRequest.getUserId());
            } catch (Exception e) {
                LOGGER.error("Error in processing the card fetch request" + e);
                throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.CARD_FETCH_ERROR.getErrorCode(),
                        statusIdentifier.toString(), e);
            }

        return instantCreditCardResponse;
    }

    public PaymentRequestCardDetails convertCardDetailsToPaymentRequestCardDetails(InstantCreditCardResponse instantCreditCardResponse,VccCardStorageResponse vccCardStorageResponse){
        if(instantCreditCardResponse==null){
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorCode()
                    ,InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorMessage());
        }
        LOGGER.info("axis card request decryption started ");
        FetchCardDetailsResponse fetchCardDetailsResponse=new FetchCardDetailsResponse();
        fetchCardDetailsResponse=instantCreditCardResponse.getFetchCardDetailsResponse();
        String decryptedFinalBody=decryptCardResponseBody(fetchCardDetailsResponse.getFetchCardDetailsResponseBodyEncrypted());
        InstantCreditCardResponseBody instantCreditCardResponseBody=new InstantCreditCardResponseBody();
        InstantCreditCardDetail instantCreditCardDetail=new InstantCreditCardDetail();
        PaymentRequestCardDetails paymentRequestCardDetails = new PaymentRequestCardDetails();
        try {

            org.codehaus.jackson.map.ObjectMapper mapper = new ObjectMapper();
            instantCreditCardResponseBody = mapper.readValue(decryptedFinalBody, InstantCreditCardResponseBody.class);
        }catch (Exception e){
            LOGGER.error("Exception in parsing card details from request"+e);
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorCode()
                    ,InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorMessage(),e);
        }
        if(instantCreditCardResponseBody.getIsCardDetailsFetched()==null || !Boolean.valueOf(instantCreditCardResponseBody.getIsCardDetailsFetched())){
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.CARD_FETCH_ERROR.getErrorCode(),
                    instantCreditCardResponseBody.getFailureReason());
        }
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse(instantCreditCardResponseBody.getCardDetails().getCardExpiryDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            instantCreditCardDetail = instantCreditCardResponseBody.getCardDetails();

            paymentRequestCardDetails.setDebitCard(false);
            paymentRequestCardDetails.setCardCvv(instantCreditCardDetail.getCardCVV2());
            paymentRequestCardDetails.setCardExpMonth(Integer.toString(cal.get(Calendar.MONTH)+1));
            paymentRequestCardDetails.setCardExpYear(Integer.toString(cal.get(Calendar.YEAR)));
            paymentRequestCardDetails.setSaveCard(true);
            paymentRequestCardDetails.setCardNumber(instantCreditCardDetail.getCardNumber());
            paymentRequestCardDetails.setCardHolderName(instantCreditCardDetail.getCardEmbossingDetails());
            paymentRequestCardDetails.setSaveCardName(instantCreditCardDetail.getCardEmbossingDetails());
            vccCardStorageResponse.setCardSerialNo(instantCreditCardDetail.getCardSerNo());
            LOGGER.info("axis card request decryption completed ");
        }catch(Exception e){
            LOGGER.error("Exception in parsing card details from request"+e);
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorCode()
                    ,InstantCreditCardApiErrorCodes.INVALID_CARD_RESPONSE.getErrorMessage(),e);
        }
        return paymentRequestCardDetails;

    }

    public void saveInstantCreditCard(PaymentRequestCardDetails paymentRequestCardDetails,String userId){
        try {
                LOGGER.info("card save processing started for user_id = "+userId);
                cardService.addCard(paymentRequestCardDetails, userId, null, null);
                LOGGER.info("card save processing completed successfully for user_id = "+userId);
            } catch (Exception e) {
                LOGGER.error("Error saving card to forknox" + e);
                throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.FORTNOX_CARD_SAVE_FAILED.getErrorCode(),
                        InstantCreditCardApiErrorCodes.FORTNOX_CARD_SAVE_FAILED.getErrorMessage(), e);
            }

    }

    public void validateChecksum(VccCardStorageRequest vccCardStorageRequest){
        LOGGER.info("caluclate checksum key :"+fcProperties.CHECKSUM_KEY+"check");
        LOGGER.info("userid :"+vccCardStorageRequest.getUserId());
        LOGGER.info("serial num :"+vccCardStorageRequest.getApplicationSerNo());
        String checksum=ChecksumCalculator.calculateRequestChecksum(fcProperties.getProperty(fcProperties.CHECKSUM_KEY),vccCardStorageRequest.getUserId(),vccCardStorageRequest.getApplicationSerNo());
        LOGGER.info("caluclate checksum is : "+checksum);
        LOGGER.info("received checksum is : "+vccCardStorageRequest.getChecksum());
        if(!checksum.equals(vccCardStorageRequest.getChecksum())){
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.INVALID_CHECKSUM.getErrorCode()
                    ,InstantCreditCardApiErrorCodes.INVALID_CHECKSUM.getErrorMessage());
        }
    }

    private void createRequestHeader(Map<String,String > header){
        header.put("X-IBM-Client-Id",fcProperties.getProperty(FCProperties.CLIENT_USER_ID));
        header.put("X-IBM-Client-Secret",fcProperties.getProperty(FCProperties.CLIENT_PASS));
    }

    private FetchCardDetailsRequest createFetchCardDetailRequest(VccCardStorageRequest vccCardStorageRequest) throws VirtualCardStorageServiceException{
        FetchCardDetailsRequest fetchCardDetailsRequest=new FetchCardDetailsRequest();
        InstantCreditCardHeader instantCreditCardHeader=createFetchCardDetailSubHeader(vccCardStorageRequest);
        String encryptedBodyResult=createFetchCardDetailBodyEncrypted(vccCardStorageRequest);

        fetchCardDetailsRequest.setSubHeader(instantCreditCardHeader);
        fetchCardDetailsRequest.setFetchCardDetailsRequestBodyEncrypted(encryptedBodyResult);
        return fetchCardDetailsRequest;
    }

    private InstantCreditCardHeader createFetchCardDetailSubHeader(VccCardStorageRequest vccCardStorageRequest){
        InstantCreditCardHeader instantCreditCardHeader=new InstantCreditCardHeader();
        instantCreditCardHeader.setRequestUUID(createRandomRequestUIID(vccCardStorageRequest.getApplicationSerNo()));
        instantCreditCardHeader.setServiceRequestId(InstantCreditCardHeaderParams.SERVICE_REQUEST_ID);
        instantCreditCardHeader.setServiceRequestVersion(InstantCreditCardHeaderParams.SERVICE_REQUEST_VERSION);
        instantCreditCardHeader.setChannelId(InstantCreditCardHeaderParams.CHANNEL_ID);
        return instantCreditCardHeader;
    }

    private String createFetchCardDetailBodyEncrypted(VccCardStorageRequest vccCardStorageRequest){
        String encryptedResult="";
        InstantCreditCardRequestBody instantCreditCardRequestBody=new InstantCreditCardRequestBody();
        instantCreditCardRequestBody.setApplicationSerNo(vccCardStorageRequest.getApplicationSerNo());
        try {
            encryptedResult = EncryptionUtil.encrypt(new Gson().toJson(instantCreditCardRequestBody),
                    fcProperties.getProperty(FCProperties.ENCRYPTION_KEY),
                    "AES", "AES/CBC/PKCS5PADDING");
        }catch(Exception e){
            LOGGER.error("Error in Calculation Hash for the Request"+e);
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.HASH_VALIDATION_ERROR.getErrorCode(),
                    InstantCreditCardApiErrorCodes.HASH_VALIDATION_ERROR.getErrorMessage(),e);
        }
        return encryptedResult;
    }

    private String decryptCardResponseBody(String encryptedBody){
        String decryptedVal="";
        try {
            decryptedVal=EncryptionUtil.decrypt(encryptedBody,fcProperties.getProperty(FCProperties.ENCRYPTION_KEY),
                    "AES", "AES/CBC/PKCS5PADDING");
        }catch(Exception e){
            LOGGER.error("Error in decrypting encrypted request body"+e);
            throw new VirtualCardStorageServiceException(InstantCreditCardApiErrorCodes.HASH_VALIDATION_ERROR.getErrorCode(),
                    InstantCreditCardApiErrorCodes.HASH_VALIDATION_ERROR.getErrorMessage(),e);
        }
        return decryptedVal;
    }

    private String createRandomRequestUIID(String applicationSerNo){
        StringBuffer requestUIID=new StringBuffer();
        requestUIID.append("FC");
        requestUIID.append(applicationSerNo);
        requestUIID.append(RandomStringUtils.random(10,true,true));
        return requestUIID.toString();
    }


    private final class InstantCreditCardHeaderParams {

        public static final String SERVICE_REQUEST_ID = "FetchCardDetails";

        public static final String SERVICE_REQUEST_VERSION = "1.0";

        public static final String CHANNEL_ID = "FCHRG";


    }
}
