package com.freecharge.payment.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.DeviceFingerPrint;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.collect.ImmutableMap;

@Service
public class DeviceFingerPrintManager {
    private final Logger        logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private MetricsClient       metricsClient;

    @Autowired
    AmazonDynamoDBAsync dynamoClient;
    
    private final String TABLE = "deviceIdMap";
    private final String HASH_KEY = "deviceId";
    private final String USER_ID_ATTRIBUTE = "userIdList";
    
    public void saveUsersForFingerPrint(DeviceFingerPrint fingerPrint, List<String> userIdList) { 
        if (!userIdList.contains(fingerPrint.getUserId())) {
            if (!isValidFingerPrintData(fingerPrint)) {
                return;
            }
            Map<String, AttributeValue> item = new HashMap<>();
            item.put(HASH_KEY, new AttributeValue().withS(fingerPrint.getDeviceFingerPrint()));

            userIdList.add(fingerPrint.getUserId());
            item.put(USER_ID_ATTRIBUTE, new AttributeValue().withNS(userIdList));
            PutItemRequest itemRequest = new PutItemRequest().withTableName(TABLE).withItem(item);
            final long startTime = System.currentTimeMillis();
            try {
                dynamoClient.putItem(itemRequest);
                metricsClient.recordEvent("DynamoDB", "PutItem", "Success");
                logger.info("Done recording deviceIdMap [deviceId:" + fingerPrint.getDeviceFingerPrint()
                        + ", userIdList: " + userIdList.toString() + "]");
            } catch (AmazonServiceException e) {
                logger.error("AmazonServiceException while recording deviceIdMap", e);
                metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonServiceException");
                userIdList.remove(fingerPrint.getUserId());
            } catch (AmazonClientException e) {
                logger.error("AmazonClientException while recording deviceIdMap", e);
                metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonClientException");
                userIdList.remove(fingerPrint.getUserId());
            } catch (Exception e) {
                metricsClient.recordEvent("DynamoDB", "PutItem", "Exception");
                userIdList.remove(fingerPrint.getUserId());
            } finally {
                metricsClient.recordLatency("DynamoDB", "PutItem", System.currentTimeMillis() - startTime);
            }
        }
    }

    public List<String> getUserIdsForDeviceFingerPrint(DeviceFingerPrint fingerPrint) {
        List<String> userIdInDynamo = new ArrayList<>();
        if (!isValidFingerPrintData(fingerPrint)) {
            return userIdInDynamo;
        }
        GetItemRequest getItemRequest = new GetItemRequest().withTableName(TABLE).withKey(
                ImmutableMap.of(HASH_KEY, new AttributeValue().withS(fingerPrint.getDeviceFingerPrint())));
        getItemRequest.setConsistentRead(false);
        final long startTime = System.currentTimeMillis();
        try {
            GetItemResult getItemResult = dynamoClient.getItem(getItemRequest);
            metricsClient.recordEvent("DynamoDB", "GetItem", "Success");
            if (getItemResult != null && getItemResult.getItem() != null) {
                Map<String, AttributeValue> deviceIdItem = getItemResult.getItem();
                userIdInDynamo = deviceIdItem.get(USER_ID_ATTRIBUTE).getNS();
            }
        } catch (AmazonServiceException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "AmazonServiceException");
            logger.error("AmazonServiceException while fetching deviceIdMap", e);
        } catch (AmazonClientException e) {
            metricsClient.recordEvent("DynamoDB", "GetItem", "AmazonClientException");
            logger.error("AmazonClientException while fetching deviceIdMap", e);
        } finally {
            metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
        }
        return userIdInDynamo;
    }
    
    private boolean isValidFingerPrintData(DeviceFingerPrint fingerPrint) {
        if (FCUtil.isEmpty(fingerPrint.getDeviceFingerPrint()) || FCUtil.isEmpty(fingerPrint.getUserId())) {
            logger.error("Got empty finger print data : " + fingerPrint);
            return false;
        }
        return true;
    }
}
