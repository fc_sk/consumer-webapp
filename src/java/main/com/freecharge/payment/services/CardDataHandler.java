package com.freecharge.payment.services;

import java.util.Map;

public interface CardDataHandler {
	public Map<String, Object> makeWSCall(String submitData, String requestURL, int connectionTimeout,
            int readTimeout, boolean isJsonResponse);

}
