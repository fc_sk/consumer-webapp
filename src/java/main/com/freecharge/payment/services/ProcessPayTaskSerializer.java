package com.freecharge.payment.services;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.ProcessPayTaskRequest;
import com.snapdeal.payments.ts.registration.TaskSerializer;

@Component
public class ProcessPayTaskSerializer implements TaskSerializer<ProcessPayTaskRequest>{

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Override
	public ProcessPayTaskRequest fromString(String request) {
		ProcessPayTaskRequest target = null;
		try {
			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (request != null) {
				target = mapper.readValue(request, ProcessPayTaskRequest.class);
			}
		} catch (Exception exp) {
			logger.info("task serilalizer failed", exp);
		}
		return target;
	}

	@Override
	public String toString(ProcessPayTaskRequest request) {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		if (request != null) {
			try{
			json = mapper.writeValueAsString(request);
			} catch(Exception e) {
				logger.error("Not able to convert object to string", e);
			}
		}
		return json;
	}

}
