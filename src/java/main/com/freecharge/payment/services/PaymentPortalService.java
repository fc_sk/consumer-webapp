package com.freecharge.payment.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpUtils;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.cardstorage.FortKnoxCardStorageService;
import com.freecharge.cardstorage.ICardStorageService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentTransactionBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.KlickPayGatewayHandler.RequestConstants;
import com.freecharge.payment.services.PaymentGatewayHandler.Constants;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.rest.onecheck.model.DeviceInformation;
import com.freecharge.rest.onecheck.model.PaymentDetails;
import com.freecharge.rest.onecheck.model.PaymentType;
import com.freecharge.rest.onecheck.model.WalletCreditInitReqDo;
import com.freecharge.rest.onecheck.util.OneCheckConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.utils.HttpUtil;
import com.snapdeal.payments.aggregator.enums.AggregatorPaymentMethods;
import com.snapdeal.payments.aggregator.model.CardDetails;
import com.snapdeal.payments.aggregator.model.CustomerDetails;
import com.snapdeal.payments.aggregator.model.PaymentAmount;
import com.snapdeal.payments.aggregator.request.AddCashRequest;
import com.snapdeal.payments.aggregator.response.AddCashResponse;

@Service("paymentPortalService")
public class PaymentPortalService {

	private final Logger logger = LoggingFactory.getLogger(getClass());

    public static final int MIN_PERC = 0;
    public static final int MAX_PERC = 100;
    private static final String      HASH_ALGO = "HmacSHA256";
    private static final String key="c0e80614-d9a0-44e4-a5cf-51419f37af1e";
	private PaymentTypeFactory paymentTypeFactory;
	private FCProperties properties;
	private PaymentGatewayHandlerFactory gatewayHandlerFactory;
	
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	
	@Autowired
    private AppConfigService            appConfigService;
	
	@Autowired
    private OneCheckWalletService      walletServiceOneCheck;
	
	@Autowired
	@Qualifier("klickPayGatewayHandler")
	private CardDataHandler klickPayGatewayHandler;
	
	@Autowired
    private FCProperties               fcProperties;

	@Autowired
	@Qualifier("fk-card-storage")
	private FortKnoxCardStorageService fortKnoxCardStorageService;
	
	private RechargeInitiateService rechargeInitiateService;
	
	@Autowired
    private AppConfigService           appConfig;

	@Autowired
	public void setRechargeInitiateService(RechargeInitiateService rechargeInitiateService) {
		this.rechargeInitiateService = rechargeInitiateService;
	}

	@Autowired
	public void setPaymentTypeFactory(PaymentTypeFactory paymentTypeFactory) {
		this.paymentTypeFactory = paymentTypeFactory;
	}

	@Autowired
	public void setProperties(FCProperties properties) {
		this.properties = properties;
	}

	public PaymentRefundBusinessDO doRefund(String orderId, Double amount, String refundType) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		PaymentRefundBusinessDO pr = new PaymentRefundBusinessDO();
		if (orderId == null || amount == null) {
			pr.setStatus(false);
			pr.setMsg("Order Id or Amount cannot be null");
			logger.error("Order Id or Amount cannot be null. Session Id: " + fs.getUuid());
			return pr;
		}

		PaymentTransactionBusinessDO pt = getSuccessMerchantTxnIdForOrderId(orderId);
		if (pt == null) {
			pr.setStatus(false);
			pr.setMsg("No successful txn for this order id. OrderId : " + orderId);
			logger.error("Order Id or Amount cannot be null. Session Id: " + fs.getUuid());
			return pr;
		}

		IPaymentType paymentType = paymentTypeFactory.getPaymentType(pt.getPaymentType());
		pr.setAmount(amount);
		pr.setOrderId(orderId);
		pr.setPaymentTransactionBusinessDO(pt);
		try {
			pr = paymentType.doRefund(pr,refundType);
		} catch (Exception e) {
			logger.error("Exception in executing refund");
			pr.setStatus(false);
			pr.setMsg("Exception in executing refund");
			return pr;
		}
		return pr;
	}

	private PaymentTransactionBusinessDO getSuccessMerchantTxnIdForOrderId(String orderId) {
		PaymentTransaction paymentTransaction = paymentTransactionService.getSuccessfulPaymentTransactionByOrderId(orderId);
        if (paymentTransaction == null) {
            return null;
        }
		return getBusinessPTxnDO(paymentTransaction);
	}

	private PaymentTransactionBusinessDO getBusinessPTxnDO(PaymentTransaction paymentTransaction) {
		PaymentTransactionBusinessDO paymentTransactionBusinessDO = new PaymentTransactionBusinessDO();
		paymentTransactionBusinessDO.setAmount(paymentTransaction.getAmount());
		paymentTransactionBusinessDO.setId(new Long(paymentTransaction.getPaymentTxnId()));
		paymentTransactionBusinessDO.setMerchantTxnId(paymentTransaction.getMerchantTxnId());
		paymentTransactionBusinessDO.setOrderId(paymentTransaction.getOrderId());
		paymentTransactionBusinessDO.setPaymentGateway(paymentTransaction.getPaymentGateway());
		paymentTransactionBusinessDO.setPaymentMode(paymentTransaction.getPaymentMode());
		paymentTransactionBusinessDO.setPaymentOption(paymentTransaction.getPaymentOption());
		paymentTransactionBusinessDO.setPaymentRequestId(paymentTransaction.getPaymentRequestId());
		paymentTransactionBusinessDO.setPaymentResponseId(paymentTransaction.getPaymentResponseId());
		paymentTransactionBusinessDO.setPaymentType(paymentTransaction.getPaymentType());
		paymentTransactionBusinessDO.setSetToPg(paymentTransaction.getSetToPg());
		paymentTransactionBusinessDO.setSuccessful(paymentTransaction.getSuccessful());
		paymentTransactionBusinessDO.setTransactionId(paymentTransaction.getTransactionId());
		return paymentTransactionBusinessDO;
	}
	
	public Map<String, Object> getCardInformation(Map<String, String> paramMap, HttpServletRequest request) {

        Map<String, Object> dataMap = new HashMap<String, Object>();
        String requestURL = fcProperties.getProperty(FCProperties.KLICK_PAY_REQUEST_URL);
        String merchantCode = fcProperties.getProperty(FCProperties.KLICK_PAY_MERCHANT_CODE);
        String cardToken = paramMap.get("saved_card_token");
        String cardNumber = paramMap.get("card_num");
        String fcUserId = paramMap.get("userId");
        String cardReference = paramMap.get("card_reference");
        
        dataMap.put("merchantCode", merchantCode);
        dataMap.put("cardToken", cardToken);
        dataMap.put("cardNumber", cardNumber);
        dataMap.put("isOneCheckUser", true);

		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			if (!Strings.isNullOrEmpty(cardToken) || !Strings.isNullOrEmpty(cardNumber)) {
				resultMap = klickPayGatewayHandler.makeWSCall(new Gson().toJson(dataMap), requestURL, 1000, 2000, false);
				if (!Strings.isNullOrEmpty(cardNumber)) {
					String cardBin = cardNumber.substring(0, 6);
					resultMap.put("card_is_in", cardBin);
				} else if (cardReference != null) {
					Map<String, Object> listCardMap = fortKnoxCardStorageService.list(Integer.parseInt(fcUserId));
					logger.info("Listcardmap:"+listCardMap.isEmpty());
					@SuppressWarnings("unchecked")
					List<Map<String, String>> cards = (List<Map<String, String>>) listCardMap.get("cards");
					logger.info("cards:"+cards.isEmpty());
					for (Map<String, String> map : cards) {
						logger.info("card_reference:"+map.get(CardStorageKey.CARD_REFERENCE));
						if (cardReference.equals(map.get(CardStorageKey.CARD_REFERENCE))) {
							logger.info("card reference:" + map.get(CardStorageKey.CARD_REFERENCE));
							resultMap.put("card_is_in", map.get(CardStorageKey.CARD_IS_IN));
						}
					}
				} else {
                    logger.info("card reference is null");
                    resultMap.put("card_is_in", "");
				}
			}
			logger.info("Card_is_in:" + resultMap.get("card_is_in") + "\n" + "card finger print:"
					+ resultMap.get("fingerPrint"));
			logger.info("isCardnumberPresent:"+Strings.isNullOrEmpty(cardNumber)+"\tcardToken"+cardToken);
		} catch (Exception e) {
			logger.error("Exception:", e);
		}
		return resultMap;
	}
    
	public DeviceInformation initDeviceInformation(HttpServletRequest request) {
		DeviceInformation deviceInfo = new DeviceInformation();
		String imei = request.getParameter("imei");
		String advertiseId = request.getParameter("advertise_id");
		deviceInfo.setHost(request.getRemoteHost());
		deviceInfo.setOsName(request.getHeader(OneCheckConstants.USER_MACHINE_REQUEST_IDENTIFIER));
		deviceInfo.setReferer(request.getHeader("Referer"));
		deviceInfo.setiMei(imei);
		deviceInfo.setAdvertisementId(advertiseId);
		String ipAddress = FCSessionUtil.getIpAddress(request);
		deviceInfo.setIpAddress(ipAddress);
		deviceInfo.setClientId(request.getParameter("client_id"));
		return deviceInfo;
	}
    
	public PaymentDetails initPaymentDetails(Map<String, String> paramMap, HttpServletRequest request) {
		PaymentDetails paymentDetails = new PaymentDetails();
		String bankName = paramMap.get("payment_option");
    	Map<String, Object> resultMap = getCardInformation(paramMap, request);
    	String cardBin = (resultMap.get("card_is_in") == null)? null : String.valueOf(resultMap.get("card_is_in"));
    	String fingerPrint = (resultMap.get("fingerPrint") == null)? null : String.valueOf(resultMap.get("fingerPrint"));
		if (!Strings.isNullOrEmpty(cardBin) || !Strings.isNullOrEmpty(fingerPrint)) {
			paymentDetails.setCardBin(cardBin);
			paymentDetails.setCardHash(fingerPrint);
			paymentDetails.setName("com.snapdeal.onecheck.cards.entity.CardDetails");
		} else {
			paymentDetails.setPaymentMode("netBanking");
			paymentDetails.setPaymentIdentifier(bankName);
			paymentDetails.setName("com.snapdeal.onecheck.cards.entity.OtherBankDetails");
		}
		return paymentDetails;
	}
	

    public AddCashRequest createAddCashRequestForAggregator(WalletCreditInitReqDo creditInitReqDo, UserDetailsDTO oneCheckUser, String promoId, String originEndPoint, PaymentDetails paymentDetails){
        AddCashRequest addCashRequest = new AddCashRequest();
        addCashRequest.setMerchantId(fcProperties.getAggregatorMerchantId());
        addCashRequest.setChannel(creditInitReqDo.getReqChannel());
        addCashRequest.setSourceSystem(creditInitReqDo.getSource());
        addCashRequest.setPromoCode(promoId);
        addCashRequest.setCallbackUrl("http://www.test.com/callbackUrl");
        
        PaymentAmount paymentType1 = new PaymentAmount(BigDecimal.valueOf(creditInitReqDo.getTxnAmount()), AggregatorPaymentMethods.PG);
        PaymentAmount paymentType2 = new PaymentAmount(BigDecimal.valueOf(creditInitReqDo.getTxnAmount()), AggregatorPaymentMethods.GENERALBALANCE);
        if(creditInitReqDo.isGvLoadMoneyRequest()){
           paymentType2 = new PaymentAmount(BigDecimal.valueOf(creditInitReqDo.getTxnAmount()), AggregatorPaymentMethods.GIFTVOUCHER);
        }
        
        List<PaymentAmount> txnAmountList = new ArrayList<PaymentAmount>();
        txnAmountList.add(paymentType1);
        txnAmountList.add(paymentType2);
        addCashRequest.setTxnAmountList(txnAmountList); 
        
        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setEmail(oneCheckUser.getEmailId());
        customerDetails.setName(oneCheckUser.getDisplayName());
        customerDetails.setUserId(oneCheckUser.getUserId());
        customerDetails.setMobileNumber(oneCheckUser.getMobileNumber());
        addCashRequest.setCustomerDetails(customerDetails);
        
        setCardDetailsForAggregatorFPS(creditInitReqDo, oneCheckUser, addCashRequest,paymentDetails);
        
        if (StringUtils.isNotBlank(originEndPoint)) {
            addCashRequest.setMerchantRedirectSuccessUrl(originEndPoint + "/app/walletsuccess");
            addCashRequest.setMerchantRedirectFailureUrl(originEndPoint + "/app/walletfailure");
        } else {
            addCashRequest.setMerchantRedirectSuccessUrl(fcProperties.getAppRootUrl() + "/app/walletsuccess");
            addCashRequest.setMerchantRedirectFailureUrl(fcProperties.getAppRootUrl() + "/app/walletfailure");
        }
        
        return addCashRequest;
    }

    private void setCardDetailsForAggregatorFPS(WalletCreditInitReqDo creditInitReqDo, UserDetailsDTO oneCheckUser,
            AddCashRequest addCashRequest, PaymentDetails paymentDetails) {
        CardDetails cardDetails = new CardDetails();
        
        JSONObject fpsFlagObject = appConfig.getCustomProp(FCConstants.FPS_FLAG);
        Object fpsEnabled = fpsFlagObject.get(FCConstants.FPS_ENABLED);
        logger.info("FPS enabled: "+String.valueOf(fpsEnabled));
        //Send card details only if fps property is enabled
        
        
        if (Boolean.parseBoolean(String.valueOf(fpsEnabled))) {
            
            if("netBanking".equalsIgnoreCase(paymentDetails.getPaymentMode())){
                addCashRequest.setNetBankingBankCode(paymentDetails.getPaymentIdentifier());
            }
            // For saved cards
            else if (creditInitReqDo.getSavedCardToken() != null) {
                logger.info("Saved card txn: "+creditInitReqDo.getSavedCardToken());
                Map<String, String> card = fortKnoxCardStorageService.get(oneCheckUser.getFcUserId(),
                        creditInitReqDo.getSavedCardToken(), "random");
                cardDetails.setCardBin(card.get(CardStorageKey.CARD_IS_IN));
                cardDetails.setCardExpiry(
                        card.get(CardStorageKey.CARD_EXPIRY_MONTH) + "/" + card.get(CardStorageKey.CARD_EXPIRY_YEAR).substring(2,4));
                cardDetails.setCardHash(card.get(CardStorageKey.CARD_FINGERPRINT));
                addCashRequest.setCardDetails(cardDetails);
            }
            
            // New cards
            else if (StringUtils.isNotEmpty(creditInitReqDo.getCardNumber())){
                logger.info("New card txn:"+creditInitReqDo.getNameOnCard());
                cardDetails.setCardBin(creditInitReqDo.getCardNumber().substring(0, 6));
                cardDetails.setCardExpiry(
                        creditInitReqDo.getCardExpiryMonth() + "/" + creditInitReqDo.getCardExpiryYear().substring(2,4));
                cardDetails.setCardHash(paymentDetails.getCardHash());
                addCashRequest.setCardDetails(cardDetails);
            } 
            
        }
    }
    
    public Map<String, Object> createPaymentRequestForKlickpay(WalletCreditInitReqDo creditInitReqDo, AddCashResponse addCashResponse, List<String> payUExcludedNBList){
        Map<String, Object> paymentRequestMap = new HashMap<String, Object>();
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_ID, addCashResponse.getPgRequest().getMerchant_id());
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_TXN_ID, addCashResponse.getPgRequest().getMerchant_txn_id());
        paymentRequestMap.put(RequestConstants.PARAM_TXN_AMOUNT, addCashResponse.getPgRequest().getTxn_amount());
        paymentRequestMap.put(RequestConstants.PARAM_PRODUCT_INFO, addCashResponse.getPgRequest().getProduct_info()==null ? "" : addCashResponse.getPgRequest().getProduct_info());
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_NAME, addCashResponse.getPgRequest().getCustomer_name());
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_EMAIL, addCashResponse.getPgRequest().getCustomer_email());
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_CONTACT, addCashResponse.getPgRequest().getCustomer_contact());
        paymentRequestMap.put(RequestConstants.PARAM_BLACK_PAYMENTS, Constants.EXCLUSION_LIST);
        paymentRequestMap.put(RequestConstants.PARAM_CARD_TOKEN, "");
        paymentRequestMap.put(RequestConstants.PARAM_CHANNEL, creditInitReqDo.getReqChannel());
        paymentRequestMap.put(RequestConstants.PARAM_CURRENCY, "INR");
        paymentRequestMap.put(RequestConstants.PARAM_SAVE_CARD_NAME, "");
        paymentRequestMap.put(RequestConstants.PARAM_SAVE_CARD, "true");
        
        if("2".equals(creditInitReqDo.getPaymentType()))
            paymentRequestMap.put(RequestConstants.PARAM_SAVE_CARD, "false");
        
        paymentRequestMap.put(RequestConstants.PARAM_GATEWAY_TO_USE, "payu");
        if (payUExcludedNBList.contains(creditInitReqDo.getPaymentOption())) {
            // some fessed up special case.
            paymentRequestMap.put(RequestConstants.PARAM_GATEWAY_TO_USE, "cca");
        }
        
        String metaData = addCashResponse.getPgRequest().getMetadata();
        
        //TODO: Remove hardcoded merchantKey
        paymentRequestMap.put(RequestConstants.PARAM_CHECKSUM,addCashResponse.getPgRequest().getChecksum());
        
        String cardHolderName = creditInitReqDo.getNameOnCard();
        if (StringUtils.isNotEmpty(cardHolderName)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_HOLDER_NAME, cardHolderName);
        } else {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_HOLDER_NAME, "UNKNOWN");
        }
        if (StringUtils.isNotEmpty(creditInitReqDo.getCardNumber())) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_NUMBER, creditInitReqDo.getCardNumber());
        }
        if (StringUtils.isNotEmpty(creditInitReqDo.getCardExpiryMonth())) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_EXPIRY_MONTH,
                    PaymentUtil.getExpMonthInReqFormat(creditInitReqDo.getCardExpiryMonth()));
        }
        if (StringUtils.isNotEmpty(creditInitReqDo.getCardExpiryYear())) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_EXPIRY_YEAR, creditInitReqDo.getCardExpiryYear());
        }
        if (StringUtils.isNotEmpty(creditInitReqDo.getCardCvv())) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_CVV, creditInitReqDo.getCardCvv());
        }
        
        String responseUrl = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME)
                + RequestConstants.PG_RESPONSE_URL;
        //TODO: Check if old mobile payments need to be taken care: KPGH MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO?
        paymentRequestMap.put(RequestConstants.PARAM_CALLBACK_URL, responseUrl);
        //TODO: Check what to do if not onecheckuser: KPGH sessionData.get(WebConstants.SESSION_USER_USERID)?
        String oneCheckUserId = walletServiceOneCheck.getOneCheckIdIfOneCheckWalletEnabled(addCashResponse.getPgRequest().getCustomer_email());
        if (oneCheckUserId != null && !oneCheckUserId.trim().isEmpty()) {
            paymentRequestMap.put(RequestConstants.PARAM_USER_ID, oneCheckUserId);
        }
        
        PaymentType ptTemp = PaymentType.fromCode(creditInitReqDo.getPaymentType());
        switch (ptTemp) {
            case Card:
                paymentRequestMap.put("payment_type_code", "CC");
                paymentRequestMap.put("card_type", "VI");
                paymentRequestMap.put("card_num", creditInitReqDo.getCardNumber());
                paymentRequestMap.put("card_cvv", creditInitReqDo.getCardCvv());
                paymentRequestMap.put("card_exp_mon", creditInitReqDo.getCardExpiryMonth());
                paymentRequestMap.put("card_exp_yr", creditInitReqDo.getCardExpiryYear());
                paymentRequestMap.put("save_card", "TRUE");

//                metaData += "|is_onecheck_user=true";
                break;

            case SavedCard:
                paymentRequestMap.put("payment_type_code", "SC");
                paymentRequestMap.put("card_token", creditInitReqDo.getSavedCardToken());
                paymentRequestMap.put("card_cvv", creditInitReqDo.getCardCvv());
                paymentRequestMap.put("card_type", "VI");

//                metaData += "|is_onecheck_user=true";

                break;

            case NetBanking:
                paymentRequestMap.put("payment_type_code", "NB");
                if (payUExcludedNBList.contains(creditInitReqDo.getPaymentOption())) {
                    // some fessed up special case.
                    paymentRequestMap.put("pg", "cca");
                }
                paymentRequestMap.put("card_type", creditInitReqDo.getPaymentOption());
                break;
            default:
                logger.info("Unknown payment type: "+ptTemp);
                break;
            }
        Map<String, String> metadataMap = new HashMap<>();
        if (!StringUtils.isEmpty(metaData)) {
            String[] metadataPairs = metaData.split("\\|");
            for (String pair : metadataPairs) {
                String[] keyValue = pair.split("=");
                metadataMap.put(keyValue[0], keyValue[1]);
            }
        }
        paymentRequestMap.put(RequestConstants.PARAM_METADATA, metadataMap);
        return paymentRequestMap;
    }
    /**
     * This method is used to find the percentage based on roll out plan for load money movement from
     * onecheck to s2s (aggregator)
     * **/
    public boolean isS2SLoadMoneyAllowed(){
        JSONObject walletS2SPercObject = appConfigService.getCustomProp(FCConstants.WALLET_S2S_PERC_KEY);
        int randomNum = ThreadLocalRandom.current().nextInt(MIN_PERC, MAX_PERC + 1);
        logger.info("RandomNumber: "+randomNum);
        Object walletS2SPerc = walletS2SPercObject.get(FCConstants.WALLET_S2S_PERC);
        
        if (walletS2SPerc != null && !FCUtil.isEmpty(String.valueOf(walletS2SPerc))) {
            if(randomNum<=Integer.parseInt(String.valueOf(walletS2SPerc)))
                return true;
        }
        return false;
    }
    
    public static String getHash(String text, byte[] key, String algorithm) throws Exception {
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        byte[] sign = mac.doFinal(text.getBytes("UTF8"));
        return DatatypeConverter.printHexBinary(sign);
    }
}
