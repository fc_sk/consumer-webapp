package com.freecharge.payment.services;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.billdesk.pgidsk.PGIUtil;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.UserAuthService;
import com.freecharge.common.businessdo.LoginBusinessDO;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.model.PaymentType;
import com.freecharge.sns.bean.ReconMetaData;

@Component(value = "billDeskGatewayHandler")
public class BillDeskGatewayHandler extends PaymentGatewayHandler {

    public static final String                   BILL_DESK_DATE_FORMAT                  = "yyyyMMddHHmmss";
    public static final String                   REFUND_DATE_FORMAT                     = "yyyyMMdd";
    public static final String                   MESSAGE_PARAM_SEPARATOR                = "|";
    public static final String                   MESSAGE_PARAM_NAME                     = "msg";
    public static final String                   REFUND_SUCCESS_RESPONSE_STRING         = "Y";
    public static final String                   CANCELLATION_CODE                      = "0699";
    public static final String                   REFUND_CODE                            = "0799";

    private final Logger                         logger                                 = LoggingFactory
            .getLogger(getClass());
    private static final Logger                  refundLogger                           = LoggingFactory
            .getLogger(LoggingFactory.getRefundLoggerName(BillDeskGatewayHandler.class.getName()));

    private static String                        BILLDESK_Success_Return_Value          = "0300";
    private static String                        BILLDESK_URL                           = "https://www.billdesk.com/pgidsk/pgmerc/FREECHARGERedirect.jsp";
    public static String                         BILLDESK_FREECHARGE_RETUERN_URL        = "/payment/handlbdkeresposne.htm";
    public static String                         MOBILE_BILLDESK_FREECHARGE_RETUERN_URL = "/m/payment/response/handlbdkeresposne.htm";
    private static String                        BILLDESK_CHECK_STATUS_URL              = "https://www.billdesk.com/pgidsk/PGIQueryController";
    private static String                        BILLDESK_REFUND_URL                    = "https://www.billdesk.com/pgidsk/PGIRefundController";
    private static String                        BILLDESK_PAYMENT_AUTHORIZATION_URL     = "https://www.billdesk.com/pgidsk/PGIDirectGateway?reqid=BDRD0002";
    private static String                        BILLDESK_PAYMENT_INITIATION_URL        = "https://www.billdesk.com/pgidsk/PGIDirectGateway";
    public static final String                   PAYDATA_PARAM_NAME                     = "paydata";
    public static final String                   IP_ADDRESS_PARAM_NAME                  = "ipaddress";
    private final static String                  DEFAULT_DETAILS                        = "NA";
    private static String                        WORKING_KEY                            = "ifrmYy4rzIl7";
    private static String                        MECODE                                 = "FREECHARGE";
    private static String                        TRANSACTION_STATUS_REQUEST_TYPE        = "0121";
    private static String                        REFUND_REQUEST_TYPE                    = "0400";

    private static HashMap<String, String>       paymentTypeMap                         = new HashMap<String, String>();
    private static HashMap<String, BankItemCode> billdeskNetBankCodesFromUi             = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskCreditCardCodesFromUI          = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskCashCardCodesFromUi            = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskATMCardCodesFromUI             = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskVisaDebitCardCodesFromUI       = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskMasterDebitCardCodesFromUI     = new HashMap<String, BankItemCode>();
    private static HashMap<String, BankItemCode> billdeskOtherDebitCardCodesdFromUI     = new HashMap<String, BankItemCode>();
    private static List<String>                  cardRelatedFields                      = Arrays.asList("ccNum",
            "ccExpYear", "ccExpMon", "ccHolderName", "ccCvv", "dbNum", "dbExpYear", "dbExpMon", "dbCvv", "dbHolderName",
            "cardNumber", "cardExpYear", "cardCvv", "cardExpMonth", "cardHolderName");

    private static final String                  BILLDESK_PAY_DATA_KEY                  = "PAYMENT_REQ_PAY_DATA";

    @Autowired
    private MetricsClient                        metricsClient;

    @Autowired
    private FCProperties                         fcProperties;

    @Autowired
    @Qualifier(value = "walletGatewayHandler")
    private IGatewayHandler                      walletGatewayHandler;

    @Autowired
    UserServiceProxy                             userServiceProxy;

    @Autowired
    UserAuthService                              userAuthService;

    @Autowired
    OrderService                                 orderService;

    @Autowired
    OrderIdSlaveDAO                              orderIdSlaveDAO;

    private static final EPinEncrypt             ENCRYPT                                = new EPinEncrypt();

    static {

        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_CREDITCARD, "NonMoto");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_DEBITCARD, "");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_CASHCARD, "CCRD");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_NETBANKING, "netBanking");

        billdeskCreditCardCodesFromUI.put("CC", new BankItemCode("HCC", "DIRECT"));
        billdeskCreditCardCodesFromUI.put("MC", new BankItemCode("HCC", "DIRECT"));
        billdeskCreditCardCodesFromUI.put("DN", new BankItemCode("CIT", "DCD"));

        billdeskCashCardCodesFromUi.put("ITZ", new BankItemCode("HCC", "DIRECT"));
        billdeskCashCardCodesFromUi.put("DON", new BankItemCode("HCC", "DIRECT"));
        billdeskCashCardCodesFromUi.put("OXI", new BankItemCode("HCC", "DIRECT"));

        billdeskATMCardCodesFromUI.put("ABOI", new BankItemCode("CTR", "DIRECT"));
        billdeskATMCardCodesFromUI.put("AIOS", new BankItemCode("IOD", "DIRECT"));
        billdeskATMCardCodesFromUI.put("ALVB", new BankItemCode("LVD", "DIRECT"));
        billdeskATMCardCodesFromUI.put("APNB", new BankItemCode("PDC", "DIRECT"));
        billdeskATMCardCodesFromUI.put("ASBI", new BankItemCode("SPG", "DIRECT"));
        billdeskATMCardCodesFromUI.put("AUBI", new BankItemCode("UDC", "DIRECT"));
        billdeskATMCardCodesFromUI.put("ACAN", new BankItemCode("HCC", "DIRECT"));
        billdeskATMCardCodesFromUI.put("ACIT", new BankItemCode("CIT", "DIRECTD"));

        billdeskVisaDebitCardCodesFromUI.put("VXXX", new BankItemCode("HCC", "DDV"));
        billdeskVisaDebitCardCodesFromUI.put("VIOB", new BankItemCode("IOD", "DIRECT"));
        billdeskVisaDebitCardCodesFromUI.put("VSBI", new BankItemCode("SPG", "DIRECT"));
        billdeskVisaDebitCardCodesFromUI.put("VUBI", new BankItemCode("UDC", "DIRECT"));

        billdeskMasterDebitCardCodesFromUI.put("MXXX", new BankItemCode("CTR", "DIRECT"));
        billdeskMasterDebitCardCodesFromUI.put("MBOI", new BankItemCode("CTR", "DIRECT"));
        billdeskMasterDebitCardCodesFromUI.put("MPNB", new BankItemCode("PDC", "DIRECT"));
        billdeskMasterDebitCardCodesFromUI.put("MSBI", new BankItemCode("SPG", "DIRECT"));
        billdeskMasterDebitCardCodesFromUI.put("MUBI", new BankItemCode("UDC", "DIRECT"));

        billdeskOtherDebitCardCodesdFromUI.put("DSME", new BankItemCode("SMP", "DIRECT"));
        billdeskOtherDebitCardCodesdFromUI.put("DCME", new BankItemCode("CMP", "DIRECTD"));
        billdeskOtherDebitCardCodesdFromUI.put("HDFD", new BankItemCode("HCC", "DDV-HDF"));

        billdeskNetBankCodesFromUi.put("ANDB", new BankItemCode("ADB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("AXIS", new BankItemCode("UTI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("ALLB", new BankItemCode("ALB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("BAHK", new BankItemCode("BBK", "DIRECT"));
        billdeskNetBankCodesFromUi.put("BOBC", new BankItemCode("BBC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("BOBR", new BankItemCode("BBR", "DIRECT"));
        billdeskNetBankCodesFromUi.put("BOI", new BankItemCode("BOI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("MAHB", new BankItemCode("BOM", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CITA", new BankItemCode("HCC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CANB", new BankItemCode("CNB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CENB", new BankItemCode("CBI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CITU", new BankItemCode("CUB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CORB", new BankItemCode("CRP", "DIRECT"));
        billdeskNetBankCodesFromUi.put("CSYB", new BankItemCode("CSB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("DENB", new BankItemCode("DEN", "DIRECT"));
        billdeskNetBankCodesFromUi.put("DECB", new BankItemCode("DCB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("DEUB", new BankItemCode("DBK", "DIRECT"));
        billdeskNetBankCodesFromUi.put("DHLB", new BankItemCode("DLB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("FEDB", new BankItemCode("FBK", "DIRECT"));
        billdeskNetBankCodesFromUi.put("IDBI", new BankItemCode("IDB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("IOB", new BankItemCode("IOB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("INDB", new BankItemCode("INB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("INIB", new BankItemCode("IDS", "DIRECT"));
        billdeskNetBankCodesFromUi.put("INVB", new BankItemCode("ING", "DIRECT"));
        billdeskNetBankCodesFromUi.put("JNKB", new BankItemCode("JKB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("KRTB", new BankItemCode("KBL", "DIRECT"));
        billdeskNetBankCodesFromUi.put("KARB", new BankItemCode("KVB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("LXCB", new BankItemCode("LVC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("LXRB", new BankItemCode("LVR", "DIRECT"));
        billdeskNetBankCodesFromUi.put("KOBK", new BankItemCode("162", "DIRECT"));
        billdeskNetBankCodesFromUi.put("ORTB", new BankItemCode("OBC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("PJSB", new BankItemCode("PSB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("PJCB", new BankItemCode("CPN", "DIRECT"));
        billdeskNetBankCodesFromUi.put("PJRB", new BankItemCode("PNB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("RATB", new BankItemCode("RTN", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SHVB", new BankItemCode("SVC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SINB", new BankItemCode("SIB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("STCB", new BankItemCode("HCC", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SBJB", new BankItemCode("SBJ", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SHYB", new BankItemCode("SBH", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SBIN", new BankItemCode("SBI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SMYB", new BankItemCode("SBM", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SPTB", new BankItemCode("SBP", "DIRECT"));
        billdeskNetBankCodesFromUi.put("STVB", new BankItemCode("SBT", "DIRECT"));
        billdeskNetBankCodesFromUi.put("SYNB", new BankItemCode("SYD", "DIRECT"));
        billdeskNetBankCodesFromUi.put("TMEB", new BankItemCode("TMB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("UCOB", new BankItemCode("UCO", "DIRECT"));
        billdeskNetBankCodesFromUi.put("UBI", new BankItemCode("UBI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("UNIB", new BankItemCode("UNI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("VIJB", new BankItemCode("VJB", "DIRECT"));
        billdeskNetBankCodesFromUi.put("YEBK", new BankItemCode("YBK", "DIRECT"));
        billdeskNetBankCodesFromUi.put("ICIC", new BankItemCode("ICI", "DIRECT"));
        billdeskNetBankCodesFromUi.put("HDFC", new BankItemCode("HDF", "DIRECT"));
        // Adding Amex Card ezeClick under netbanking.
        billdeskNetBankCodesFromUi.put("AX", new BankItemCode("AEC", "DIRECT"));

        // done till above
        // billdeskNetBankCodesFromUi.put("SIBK", "SIB_N");
        // billdeskNetBankCodesFromUi.put("SCBK", "SCB_N");
        // billdeskNetBankCodesFromUi.put("SBKH", "SBH_N");
        // billdeskNetBankCodesFromUi.put("SBIN", "SBI_N");
        // billdeskNetBankCodesFromUi.put("SBMK", "SBM_N");
        // billdeskNetBankCodesFromUi.put("SBTK", "SBT_N");
        // billdeskNetBankCodesFromUi.put("SYND", "SYNBK_N");
        // billdeskNetBankCodesFromUi.put("TMBK", "TNMB_N");
        // billdeskNetBankCodesFromUi.put("UBIK", "UNI_N");
        // billdeskNetBankCodesFromUi.put("UNBI", "UBI_N");
        // billdeskNetBankCodesFromUi.put("VJBK", "VJYA_N");
        // billdeskNetBankCodesFromUi.put("YEBK", "YES_N");
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws Exception {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String pOrderId = paymentRequestBusinessDO.getOrderId();
        Integer count = createOrderIdForPG(pOrderId);
        String orderId = paymentRequestBusinessDO.getOrderId();
        if (count > 0)
            orderId = orderId + count;
        paymentRequestBusinessDO.setMtxnId(orderId);
        logger.debug("Billdesk PG payment starts for order id: " + orderId + " and session id: " + fs.getUuid());
        String host = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME);

        String responseUrl = host + BILLDESK_FREECHARGE_RETUERN_URL;
        if (MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO)) {
            responseUrl = host + MOBILE_BILLDESK_FREECHARGE_RETUERN_URL;
        }
        String txtAdditionalInfo1 = DEFAULT_DETAILS;
        String txtAdditionalInfo2 = DEFAULT_DETAILS;
        String txtAdditionalInfo3 = DEFAULT_DETAILS;
        String txtAdditionalInfo4 = DEFAULT_DETAILS;
        String txtAdditionalInfo5 = DEFAULT_DETAILS;
        String txtAdditionalInfo6 = DEFAULT_DETAILS;
        String txtAdditionalInfo7 = DEFAULT_DETAILS;

        Map<String, String> billDeskMap = new HashMap<String, String>();

        String pgAmount = "";
        String amount = paymentRequestBusinessDO.getAmount();
        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }

        String bankId = "HCC";
        String itemCode = "DIRECT";
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentRequestBusinessDO.getPaymentType()) {
            BankItemCode bic = billdeskCreditCardCodesFromUI.get(paymentRequestBusinessDO.getPaymentOption());
            if (bic != null) {
                bankId = bic.getBankCode();
                itemCode = bic.getItemCode();
            }
        }
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentRequestBusinessDO.getPaymentType()) {
            BankItemCode bic = billdeskNetBankCodesFromUi.get(paymentRequestBusinessDO.getPaymentOption());
            if (bic != null) {
                bankId = bic.getBankCode();
                itemCode = bic.getItemCode();
            }
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CASHCARD) == paymentRequestBusinessDO.getPaymentType()) {
            BankItemCode bic = billdeskCreditCardCodesFromUI.get(paymentRequestBusinessDO.getPaymentOption());
            if (bic != null) {
                bankId = bic.getBankCode();
                itemCode = bic.getItemCode();
            }
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ATMCARD) == paymentRequestBusinessDO.getPaymentType()) {
            BankItemCode bic = billdeskATMCardCodesFromUI.get(paymentRequestBusinessDO.getPaymentOption());
            if (bic != null) {
                bankId = bic.getBankCode();
                itemCode = bic.getItemCode();
            }
        }
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD) == paymentRequestBusinessDO
                .getPaymentType()) {
            BankItemCode bic = billdeskOtherDebitCardCodesdFromUI.get(paymentRequestBusinessDO.getPaymentOption());
            if (bic != null) {
                bankId = bic.getBankCode();
                itemCode = bic.getItemCode();
            } else {
                bankId = "HCC";
                itemCode = "DIRECT";
            }
        }
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentRequestBusinessDO.getPaymentType()) {
            if (paymentRequestBusinessDO.getPaymentOption() != null
                    && paymentRequestBusinessDO.getPaymentOption().equalsIgnoreCase("DVI")) {
                BankItemCode bic = billdeskVisaDebitCardCodesFromUI.get(paymentRequestBusinessDO.getPaymentOption());
                if (bic != null) {
                    bankId = bic.getBankCode();
                    itemCode = bic.getItemCode();
                } else {
                    bankId = "CMP";
                    itemCode = "DDV";
                }

            } else if (paymentRequestBusinessDO.getPaymentOption() != null
                    && paymentRequestBusinessDO.getPaymentOption().equalsIgnoreCase("DMC")) {
                BankItemCode bic = billdeskMasterDebitCardCodesFromUI.get(paymentRequestBusinessDO.getPaymentOption());
                if (bic != null) {
                    bankId = bic.getBankCode();
                    itemCode = bic.getItemCode();
                } else {
                    bankId = "CMP";
                    itemCode = "DIRECT";
                }
            } else if (paymentRequestBusinessDO.getPaymentOption() != null
                    && paymentRequestBusinessDO.getPaymentOption().equalsIgnoreCase("HDFD")) {
                BankItemCode bic = billdeskOtherDebitCardCodesdFromUI.get(paymentRequestBusinessDO.getPaymentOption());
                if (bic != null) {
                    bankId = bic.getBankCode();
                    itemCode = bic.getItemCode();
                } else {
                    bankId = "HCC";
                    itemCode = "DIRECT";
                }
            } else if (paymentRequestBusinessDO.getPaymentOption() != null
                    && paymentRequestBusinessDO.getPaymentOption().equalsIgnoreCase(PaymentConstants.MAESTRO_SBI)) {
                BankItemCode bic = billdeskOtherDebitCardCodesdFromUI.get(paymentRequestBusinessDO.getPaymentOption());
                if (bic != null) {
                    bankId = bic.getBankCode();
                    itemCode = bic.getItemCode();
                } else {
                    bankId = "HCC";
                    itemCode = "DIRECT";
                }

                String cardExpMon = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpMonth();
                String cardExpYear = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpYear();
                // TODO: Find default Cvv value for PayU.
                String cardCvv = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardCvv();

                // Set the default values for the MEASTORO_SBI debit card.
                if (StringUtils.isEmpty(cardCvv) && StringUtils.isEmpty(cardExpMon)
                        && StringUtils.isEmpty(cardExpYear)) {
                    paymentRequestBusinessDO.getPaymentRequestCardDetails()
                            .setCardExpMonth(PaymentConstants.MAESTRO_BD_DEFAULT_EXPIRY_MONTH);
                    paymentRequestBusinessDO.getPaymentRequestCardDetails()
                            .setCardExpYear(PaymentConstants.MAESTRO_BD_DEFAULT_EXPIRY_YEAR);
                    paymentRequestBusinessDO.getPaymentRequestCardDetails()
                            .setCardCvv(PaymentConstants.MAESTRO_BD_DEFAULT_CVV);
                }

            } else {
                BankItemCode bic = billdeskOtherDebitCardCodesdFromUI.get(paymentRequestBusinessDO.getPaymentOption());
                if (bic != null) {
                    bankId = bic.getBankCode();
                    itemCode = bic.getItemCode();
                } else {
                    bankId = "HCC";
                    itemCode = "DIRECT";
                }

            }

        }

        boolean isNetBanking = paymentRequestBusinessDO.getPaymentRequestCardDetails() != null
                && (paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardNumber() == null
                        || paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardNumber().isEmpty());
        String responseMssg = null;
        String responseRdata = null;

        if (!isNetBanking) {
            itemCode = "DIRECT";
        }

        StringBuilder msg = new StringBuilder();
        msg.append(MECODE).append(MESSAGE_PARAM_SEPARATOR).append(orderId).append(MESSAGE_PARAM_SEPARATOR).append("NA")
                .append(MESSAGE_PARAM_SEPARATOR).append(pgAmount).append(MESSAGE_PARAM_SEPARATOR).append(bankId)
                .append(MESSAGE_PARAM_SEPARATOR).append("NA").append(MESSAGE_PARAM_SEPARATOR).append("NA")
                .append(MESSAGE_PARAM_SEPARATOR).append("INR").append(MESSAGE_PARAM_SEPARATOR).append(itemCode)
                .append(MESSAGE_PARAM_SEPARATOR).append("R").append(MESSAGE_PARAM_SEPARATOR).append("freecharge")
                .append(MESSAGE_PARAM_SEPARATOR).append("NA").append(MESSAGE_PARAM_SEPARATOR).append("NA")
                .append(MESSAGE_PARAM_SEPARATOR).append("F").append(MESSAGE_PARAM_SEPARATOR).append(txtAdditionalInfo1)
                .append(MESSAGE_PARAM_SEPARATOR).append(txtAdditionalInfo2).append(MESSAGE_PARAM_SEPARATOR)
                .append(txtAdditionalInfo3).append(MESSAGE_PARAM_SEPARATOR).append(txtAdditionalInfo4)
                .append(MESSAGE_PARAM_SEPARATOR).append(txtAdditionalInfo5).append(MESSAGE_PARAM_SEPARATOR)
                .append(txtAdditionalInfo6).append(MESSAGE_PARAM_SEPARATOR).append(txtAdditionalInfo7)
                .append(MESSAGE_PARAM_SEPARATOR).append(responseUrl);

        String checkSum = createChecksum(msg.toString(), WORKING_KEY);
        msg.append(MESSAGE_PARAM_SEPARATOR).append(checkSum);

        billDeskMap.put(MESSAGE_PARAM_NAME, msg.toString());

        /*
         * Handling new API integration for bill desk. The new integration is
         * supported only for CC/DC. Netbanking will continue to get processed
         * in the old fashion
         */
        if (!isNetBanking && !fcProperties.shouldMockPG()) {
            addCardDetailsToDataMap(paymentRequestBusinessDO, billDeskMap);

            /*
             * Adding client ipaddress details for direct integration with
             * billdesk
             */
            if (paymentRequestBusinessDO.getRemoteIp() == null || paymentRequestBusinessDO.getRemoteIp().isEmpty()) {
                paymentRequestBusinessDO.setRemoteIp("1.1.1.1");
            }
            String ipAddress = paymentRequestBusinessDO.getRemoteIp();
            ipAddress = ipAddress.split(",")[0];
            billDeskMap.put(IP_ADDRESS_PARAM_NAME, ipAddress);

            String pgInititionRequestData = PaymentUtil.createSubmitData(billDeskMap, PaymentConstants.UTF_8_ENCODING);
            logger.info("[Order : " + orderId + "] Request data for Billdesk initiation request : "
                    + getPaymentRequestDataToLog(billDeskMap));
            metricsClient.recordEvent("Payment", "Billdesk.New.Initiation", "Request");
            Long startTime = System.currentTimeMillis();
            String initationResponse = PaymentUtil.submitPost(BILLDESK_PAYMENT_INITIATION_URL, pgInititionRequestData,
                    false, fcProperties.getProperty(PaymentConstants.PAYMENT_PROXY_HOST),
                    fcProperties.getIntProperty(PaymentConstants.PAYMENT_PROXY_PORT));
            metricsClient.recordEvent("Payment", "Billdesk.New.Initiation", "Response");
            metricsClient.recordLatency("Payment", "Billdesk.New.Initiation.Latency",
                    System.currentTimeMillis() - startTime);
            logger.info("[Order : " + orderId + "] Billdesk initiation response : " + initationResponse);
            int indexOfSeparator = initationResponse.indexOf("&");
            responseMssg = initationResponse.substring(0, indexOfSeparator);
            responseRdata = initationResponse.substring(indexOfSeparator + 1);
        }

        fs.getSessionData().put(orderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(orderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(orderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, orderId);
        if (isNetBanking) {
            resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
            resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
            resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, billDeskMap);
            if (fcProperties.shouldMockPG()) {
                logger.info("[MOCK] Putting /dummypg/billdesk/pay as the payment gateway submit url");
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                        fcProperties.getProperty("hostprefix") + "/dummypg/billdesk/pay");
            } else {
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, BILLDESK_URL);
            }
        } else {
            resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
            resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
            if (fcProperties.shouldMockPG()) {
                resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, billDeskMap);
                logger.info("[MOCK] Putting /dummypg/billdesk/pay as the payment gateway submit url");
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                        fcProperties.getProperty("hostprefix") + "/dummypg/billdesk/pay");
            } else if (!fcProperties.shouldMockPG() && responseMssg != null
                    && responseMssg.split("=")[1].equals("NA")) {
                metricsClient.recordEvent("Payment", "Billdesk.New.Initiation", "Success");
                resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                        getResponseDataMap(responseRdata.substring(6), paymentRequestBusinessDO.getChannel()));
                logRequestParameters((Map<String, String>) resultMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                        ((Map<String, String>) resultMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE)).get("url"));
                ((Map<String, String>) resultMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE)).remove("url");
            } else {
                metricsClient.recordEvent("Payment", "Billdesk.New.Initiation", "Failure");
                resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
            }
        }

        if (isNetBanking
                || (!isNetBanking && ((String) resultMap.get(PaymentConstants.IS_METHOD_SUCCESSFUL)).equals("true"))) {
            PaymentRequest paymentRequest = new PaymentRequest();
            paymentRequest.setAmount(new Double(pgAmount));
            paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
            paymentRequest.setMtxnId(orderId);
            paymentRequest.setOrderId(pOrderId);
            paymentRequest.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
            paymentRequest.setPaymentMode("pur");
            paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
            removeCardRelatedData(paymentRequestBusinessDO.getRequestMap());
            paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
            if (billDeskMap.containsKey(PAYDATA_PARAM_NAME)) {
                billDeskMap.remove(PAYDATA_PARAM_NAME);
            }
            paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(billDeskMap));

            paymentRequest = dumpRequestAudit(paymentRequest);

            PaymentTransaction pt = new PaymentTransaction();
            pt.setAmount(new Double(amount));
            pt.setMerchantTxnId(orderId);
            pt.setOrderId(pOrderId);
            pt.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
            pt.setPaymentMode("pur");
            pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
            pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
            pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
            pt.setSetToPg(Calendar.getInstance().getTime());

            pt = createPaymentTransaction(pt);

            fs.getSessionData().put(orderId + "_txn_id", pt.getPaymentTxnId());

            logger.debug("Billdesk handler execution completes. Session Id : " + fs.getUuid());
        }
        paymentRequestBusinessDO.setResultMap(resultMap);
        return resultMap;
    }

    private void logRequestParameters(Map<String, String> dataMap) {
        logger.info("Params being sent to issuer bank");
        for (String key : dataMap.keySet()) {
            logger.info(key + " = " + dataMap.get(key));
        }
    }

    @SuppressWarnings("rawtypes")
    private void removeCardRelatedData(Map requestMap) {
        for (String cardField : cardRelatedFields) {
            if (requestMap.containsKey(cardField)) {
                requestMap.remove(cardField);
            }
        }
    }

    /*
     * Adding card details for direct integration with billdesk
     */
    private void addCardDetailsToDataMap(PaymentRequestBusinessDO paymentRequestBusinessDO,
            Map<String, String> billDeskMap) {

        StringBuffer payDataBuffer = new StringBuffer();
        PaymentRequestCardDetails cardDetails = paymentRequestBusinessDO.getPaymentRequestCardDetails();
        String cardNo = cardDetails.getCardNumber();
        String expMon = getExpMonthInReqFormat(cardDetails.getCardExpMonth());
        String expYear = cardDetails.getCardExpYear();
        String cvv = cardDetails.getCardCvv();
        String cardType = cardDetails.getCardType();
        String cardHoldersName = cardDetails.getCardHolderName();
        if (StringUtils.isEmpty(cardHoldersName)) {
            Users user = userServiceProxy.getLoggedInUser();
            LoginBusinessDO businessDO = new LoginBusinessDO();
            businessDO.setUserId(user.getUserId().toString());
            userAuthService.doUserProfile(businessDO);
            cardHoldersName = businessDO.getFirstName();
        }

        payDataBuffer.append(cardNo).append(MESSAGE_PARAM_SEPARATOR).append(expMon).append(MESSAGE_PARAM_SEPARATOR)
                .append(expYear).append(MESSAGE_PARAM_SEPARATOR).append(cvv).append(MESSAGE_PARAM_SEPARATOR)
                .append(cardType).append(MESSAGE_PARAM_SEPARATOR).append(cardHoldersName);
        billDeskMap.put(PAYDATA_PARAM_NAME, payDataBuffer.toString());
        String encryptedData = null;
        try {
            encryptedData = ENCRYPT.getEncryptedPin(payDataBuffer.toString());
        } catch (Exception exception) {
            logger.error("Error encrypting card details for order : " + paymentRequestBusinessDO.getOrderId());
        }
        FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
        freechargeSession.getSessionData().put(BILLDESK_PAY_DATA_KEY, encryptedData);
    }

    @SuppressWarnings("deprecation")
    private Map<String, String> getResponseDataMap(String responseRdata, int channel) {
        String[] responseDataTokens = responseRdata.split("\\|");
        Map<String, String> dataMap = new HashMap<String, String>();
        for (String token : responseDataTokens) {
            if (!token.isEmpty()) {
                int indexOfSeparator = token.indexOf("=");
                String key = token.substring(0, indexOfSeparator);
                String value = token.substring(indexOfSeparator + 1);
                if (key.equals("PaReq") && (channel != FCConstants.CHANNEL_ID_WEB
                        && channel != FCConstants.CHANNEL_ID_MOBILE && channel != FCConstants.CHANNEL_ID_TIZEN_PHONE)) {
                    value = URLEncoder.encode(value);
                }
                if (!key.equals("paramcount")) {
                    dataMap.put(key, value);
                }
            }
        }
        return dataMap;
    }

    private void validatePaymentResponseMap(String responseMessage, Map<String, String> responseMap) {
        responseMap.put(PAYMENT_STATUS, String.valueOf(false));

        Boolean isChecksumEqual = isChecksumEqual(responseMessage, responseMap.get(CHECKSUM_PAYMENT_RESPONSE_MAP_KEY),
                WORKING_KEY);

        if (!isChecksumEqual) {
            responseMap.put(RESPONSE_CODE_PAYMENT_RESPONSE_MAP_KEY, "Checksum not matching, possible hacking case.");
            return;
        }

        try {
            new Double(responseMap.get(AMOUNT_PAYMENT_RESPONSE_MAP_KEY));
        } catch (NumberFormatException exp) {
            logger.error("Number format exception " + responseMap.get(MERCHANT_ID_PAYMENT_RESPONSE_MAP_KEY)
                    + responseMap.get(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY));
            responseMap.put(AMOUNT_PAYMENT_RESPONSE_MAP_KEY, String.valueOf("-1"));
            return;
        }

        if (StringUtils.equalsIgnoreCase(responseMap.get(PAYMENT_RESULT_PAYMENT_RESPONSE_MAP_KEY),
                BILLDESK_Success_Return_Value)
                && MECODE.equalsIgnoreCase(responseMap.get(MERCHANT_ID_PAYMENT_RESPONSE_MAP_KEY))) {
            responseMap.put(PAYMENT_STATUS, String.valueOf(true));
        }
    }

    private static final String MERCHANT_ID_PAYMENT_RESPONSE_MAP_KEY    = "merchantId";
    private static final String CHECKSUM_PAYMENT_RESPONSE_MAP_KEY       = "checksum";
    private static final String ORDER_ID_PAYMENT_RESPONSE_MAP_KEY       = "orderId";
    private static final String TRANSACTION_ID_PAYMENT_RESPONSE_MAP_KEY = "transactionId";
    public static final String  PAYMENT_RESULT_PAYMENT_RESPONSE_MAP_KEY = "paymentResult";
    public static final String  RESPONSE_CODE_PAYMENT_RESPONSE_MAP_KEY  = "responseCode";
    public static final String  AMOUNT_PAYMENT_RESPONSE_MAP_KEY         = "amount";
    private static final String PAYMENT_STATUS                          = "paymentStatus";
    private static final String PG_TRANSACTION_TIME_KEY                 = "pgTransactionTime";

    private void populateResponseMapWithDefaultValues(Map<String, String> responseMap) {
        responseMap.put(MERCHANT_ID_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(CHECKSUM_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(TRANSACTION_ID_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(PAYMENT_RESULT_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(RESPONSE_CODE_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(AMOUNT_PAYMENT_RESPONSE_MAP_KEY, "");
        responseMap.put(PG_TRANSACTION_TIME_KEY, "");
    }

    private Map<String, String> convertPaymentResponseIntoMap(String responseMessage) {
        Map<String, String> responseMap = new HashMap<String, String>();
        populateResponseMapWithDefaultValues(responseMap);

        String[] params = StringUtils.splitPreserveAllTokens(responseMessage, MESSAGE_PARAM_SEPARATOR);

        if (params != null) {
            try {
                responseMap.put(MERCHANT_ID_PAYMENT_RESPONSE_MAP_KEY, params[0]);
                responseMap.put(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY, params[1]);
                responseMap.put(TRANSACTION_ID_PAYMENT_RESPONSE_MAP_KEY, params[2]);
                responseMap.put(AMOUNT_PAYMENT_RESPONSE_MAP_KEY, params[4]);
                responseMap.put(PG_TRANSACTION_TIME_KEY, params[13]);
                responseMap.put(PAYMENT_RESULT_PAYMENT_RESPONSE_MAP_KEY, params[14]);
                responseMap.put(RESPONSE_CODE_PAYMENT_RESPONSE_MAP_KEY, params[23]);
                responseMap.put(CHECKSUM_PAYMENT_RESPONSE_MAP_KEY, params[25]);
            } catch (ArrayIndexOutOfBoundsException arrExp) {
                logger.error("Error in data received: " + responseMessage, arrExp);
            }
        }

        return responseMap;
    }

    @Transactional
    public Map<String, Object> handlePaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO,
            String orderId, Integer paymentTxnId, String successUrl, String errorUrl,
            Boolean isPaymentStatusForAdminPanel) {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();

        final String msg = requestMap.get(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY);

        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTxnId);
        paymentResponseBusinessDO.setPaymentType(paymentTransaction.getPaymentType());
        Map<String, String> responseMap = convertPaymentResponseIntoMap(msg);
        validatePaymentResponseMap(msg, responseMap);
        String redirectUrl = null;

        String mtxnId = responseMap.get(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY);
        paymentResponseBusinessDO.setmTxnId(mtxnId);
        boolean isSuccessful = isPaymentSuccessful(responseMap);

        if (isSuccessful) {
            redirectUrl = successUrl;
        } else {
            redirectUrl = errorUrl;
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String transactionId = responseMap.get(TRANSACTION_ID_PAYMENT_RESPONSE_MAP_KEY);
        String responseCode = responseMap.get(RESPONSE_CODE_PAYMENT_RESPONSE_MAP_KEY);
        String amount = responseMap.get(AMOUNT_PAYMENT_RESPONSE_MAP_KEY);
        String pgTxnTimeStr = responseMap.get(PG_TRANSACTION_TIME_KEY);

        Timestamp pgTxnTime = FCDateUtil.convertStringToTimestamp(pgTxnTimeStr, "dd-MM-yyyy HH:mm:ss");

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, responseMap.get(PAYMENT_STATUS));
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, null);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, MECODE);
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(false));        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(false));

        resultMap.put(PaymentConstants.MERCHANT_TXN_ID, mtxnId);

        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);
        finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);

        paymentResponseBusinessDO.setResultMap(finalMap);

        if (requestMap.containsKey(BILLDESK_PAY_DATA_KEY)
                && !StringUtils.isEmpty(requestMap.get(BILLDESK_PAY_DATA_KEY))) {
            String cardBin = PaymentUtil
                    .getCardNumberInBinFormat(requestMap.get(BILLDESK_PAY_DATA_KEY).split("\\|")[0]);
            paymentResponseBusinessDO.setCardBinHead(StringUtils.left(cardBin, 6));
            paymentResponseBusinessDO.setCardBinTail(StringUtils.right(cardBin, 4));
            if (isSuccessful) {
                metricsClient.recordEvent("Payment", "Billdesk.New", "Success");
            } else {
                metricsClient.recordEvent("Payment", "Billdesk.New", "Failure");
            }
        }

        PaymentResponse paymentResponse = new PaymentResponse(null, isSuccessful, transactionId, orderId, mtxnId,
                PaymentUtil.getMapAsString(requestMap), new Double(amount),
                PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE, responseCode, null, null, null, null, null,
                PaymentUtil.getMapAsString(resultMap), pgTxnTime, null);
        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
        paymentResponse = dumpResponsetAudit(paymentResponse);
        paymentResponse.setReconMetaData(new ReconMetaData(mtxnId, transactionId, "NA", PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE, transactionId));
        logger.debug("updating payment txn for id : " + paymentTxnId);

        boolean paymentAlreadyAck = acknoledgePaymentTransactionUpdate(paymentTransaction, orderId, transactionId,
                paymentTxnId, paymentResponse, paymentResponseBusinessDO, isPaymentStatusForAdminPanel);
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(paymentAlreadyAck));

        if (!paymentAlreadyAck) {
            logPaymentStatusMetric(paymentResponseBusinessDO);
            Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId());
            Users users = userServiceProxy.getUserByUserId(userId);
            paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), users.getUserId(),
                    isSuccessful ? PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS
                            : PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, isPaymentStatusForAdminPanel);
            if (paymentTransaction.getIsSuccessful()) {
                storeCardHashForSuccessfulPayment(paymentResponseBusinessDO);
            }
        }

        logger.info("Billdesk handling response ends here." + mtxnId);
        return finalMap;
    }

    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        logger.info("BillDesk response handling starts, parameters received are: " + requestMap);

        String msg = null;
        String pgPayData = null;
        if (fs.getSessionData().containsKey(BILLDESK_PAY_DATA_KEY)) {
            pgPayData = (String) fs.getSessionData().get(BILLDESK_PAY_DATA_KEY);
            pgPayData = EPinEncrypt.getDecryptedPin(pgPayData);
        }
        logger.info("Found Pay data for Billdesk initiation request : " + (pgPayData == null));
        /*
         * Handling both new API integration and existing redirection
         * integration for bill desk gateway. For the new API, paydata will be
         * stored in cache and hence the null check. If paydata is not found
         * then the processing should happen through old integration
         */
        if (pgPayData != null && !pgPayData.isEmpty()) {
            requestMap.put(PAYDATA_PARAM_NAME, pgPayData);
            String authorizationSubmitData = PaymentUtil.createSubmitData(requestMap, PaymentConstants.UTF_8_ENCODING);
            logger.info("[LookupId : " + paymentResponseBusinessDO.getLookupID()
                    + "] Request data for Billdesk authorization request : " + getPaymentRequestDataToLog(requestMap));
            metricsClient.recordEvent("Payment", "Billdesk.New.Authorization", "Request");
            Long startTime = System.currentTimeMillis();
            String authorizationResponse = PaymentUtil.submitPost(BILLDESK_PAYMENT_AUTHORIZATION_URL,
                    authorizationSubmitData, false, fcProperties.getProperty(PaymentConstants.PAYMENT_PROXY_HOST),
                    fcProperties.getIntProperty(PaymentConstants.PAYMENT_PROXY_PORT));
            metricsClient.recordEvent("Payment", "Billdesk.New.Authorization", "Response");
            metricsClient.recordLatency("Payment", "Billdesk.New.Authorization.Latency",
                    System.currentTimeMillis() - startTime);
            logger.info("[LookupId : " + paymentResponseBusinessDO.getLookupID()
                    + "] Billdesk authorization response : " + authorizationResponse);

            msg = authorizationResponse.split("=")[1];
            if (paymentResponseBusinessDO.getRequestMap() == null) {
                Map<String, String> map = new HashMap<>();
                paymentResponseBusinessDO.setRequestMap(map);
            }
            paymentResponseBusinessDO.getRequestMap().put(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY, msg);
            paymentResponseBusinessDO.getRequestMap().put(BILLDESK_PAY_DATA_KEY, pgPayData);
        } else {
            msg = requestMap.get(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY);
        }

        Map<String, String> responseMap = convertPaymentResponseIntoMap(msg);

        String productOrderId = null;
        String successUrl = null;
        String errorUrl = null;

        String mtxnId = responseMap.get(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY);
        Integer id = (Integer) fs.getSessionData().get(mtxnId + "_txn_id");

        productOrderId = FCSessionUtil.getProductOrderId(mtxnId, fs);

        // If id is not found in session get it from DB
        if (id == null || productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderIdAndPG(
                    mtxnId, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        successUrl = FCSessionUtil.getSuccessUrl(mtxnId, fs);
        errorUrl = FCSessionUtil.getErrorUrl(mtxnId, fs) + "&payment=fail";
        return handlePaymentResponse(paymentResponseBusinessDO, productOrderId, id, successUrl, errorUrl, false);
    }

    private Boolean isPaymentSuccessful(Map<String, String> responseMap) {
        return Boolean.valueOf(responseMap.get(PAYMENT_STATUS));
    }

    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws UnsupportedEncodingException {
        refundLogger.info(String.format("End - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(),
                PaymentGateways.BILLDESK));

        Map<String, String> parameterMap = new HashMap<String, String>();

        String refundParam = createRefundParam(paymentRefundBusinessDO);

        parameterMap.put(MESSAGE_PARAM_NAME, refundParam);

        String response = "";
        String submitData = PaymentUtil.createSubmitData(parameterMap, PaymentConstants.UTF_8_ENCODING);

        logger.info("BillDesk refund submit data:" + submitData);
        refundLogger.info("BillDesk refund submit data:" + submitData);

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(
                paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest(submitData);
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.REQUESTED);

        paymentTransactionService.create(paymentRefundTransaction);

        String orderId = paymentRefundBusinessDO.getOrderId();

        try {

            this.publishRefundAttempt(paymentRefundBusinessDO, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);

            response = PaymentUtil.submitPost(BILLDESK_REFUND_URL, submitData, false, "192.168.1.2", 80);

            paymentRefundBusinessDO.setRawPGResponse(response);
            logger.info("Refund response from BillDesk:" + response);
            refundLogger.info("Refund response from BillDesk:" + response);
        } catch (Exception ex) {
            logger.error("Exception in submitting request to url while doing refund for order id : " + orderId, ex);
            refundLogger.error("Exception in submitting request to url while doing refund for order id : " + orderId,
                    ex);
            paymentRefundBusinessDO.setStatus(false);
            paymentRefundBusinessDO.setMsg("No Reference Id passed");

            this.publishRefundAttemptStatus(paymentRefundBusinessDO, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
            return paymentRefundBusinessDO;
        }

        Boolean isSuccessful = false;
        // 0410|FREECHARGE|MCIT2817282715|20120927|FCVW1209270000413|10.00|10.00|20120929113918|0699|MCIT28172827151|NA|NA|Y|633408286
        // 0410|FREECHARGE|MCIT2818072498|20120928|FCVW1209280000045|10.00|10.00|20120928180459|0699|MCIT28180724981|NA|NA|Y|3813850974
        // - response format
        // 0410|FREECHARGE|MHCC2823459048|NA|FCVW1210050003587|NA|NA|NA|NA|NA|ERR_REF009|Cancel
        // request already received|N|296744785

        String[] splittedParams = StringUtils.splitPreserveAllTokens(response, MESSAGE_PARAM_SEPARATOR);

        String merchantIdResponse = "";
        String referenceStatusResponse = "";
        String transactionReferenceNo = "";
        String processStatusResponse = "";
        String checkSumResponse = "";

        if (splittedParams != null) {
            try {
                merchantIdResponse = splittedParams[1];
                transactionReferenceNo = splittedParams[2];
                referenceStatusResponse = this.getRefundResponseCode(splittedParams);
                processStatusResponse = splittedParams[12];
                checkSumResponse = splittedParams[13];

                logger.info(String.format(
                        "Response -  Merchant id: %s, Reference status: %s, Process status: %s, Checksum: %s",
                        merchantIdResponse, referenceStatusResponse, processStatusResponse, checkSumResponse));

            } catch (ArrayIndexOutOfBoundsException arrExp) {
                logger.error("Error in refund data received: " + refundParam, arrExp);
                refundLogger.error("Error in refund data received: " + refundParam, arrExp);
            }
        }

        if (REFUND_SUCCESS_RESPONSE_STRING.equalsIgnoreCase(processStatusResponse)) {
            isSuccessful = true;
        } else {
            isSuccessful = false;
        }

        if (!MECODE.equalsIgnoreCase(merchantIdResponse)) {
            isSuccessful = false;
        }

        Boolean isChecksumEqual = isChecksumEqual(response, checkSumResponse, WORKING_KEY);
        String refundResponseDescription = this.getRefundDescription(referenceStatusResponse);

        if (!isChecksumEqual) {
            isSuccessful = false;
            refundResponseDescription = refundResponseDescription + "| checksum mismatch. Possible hacking case";
        }

        paymentRefundBusinessDO.setStatus(isSuccessful);
        this.publishRefundAttemptStatus(paymentRefundBusinessDO, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);

        paymentRefundBusinessDO.setMsg(refundResponseDescription);
        paymentRefundTransaction.setPgResponseCode(referenceStatusResponse);
        paymentRefundTransaction.setPgResponseDescription(refundResponseDescription);
        paymentRefundTransaction.setPgStatus(processStatusResponse);
        paymentRefundTransaction.setPgTransactionReferenceNo(transactionReferenceNo);
        paymentRefundTransaction.setRawPGResponse(response);
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);

        if (isSuccessful) {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.INITIATED);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refunded successfully");
        } else {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refund failed");
        }

        paymentRefundTransaction.addToWhereFields("paymentRefundTransactionId");

        paymentTransactionService.update(paymentRefundTransaction);

        refundLogger.info(String.format("Start - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(),
                PaymentGateways.BILLDESK));

        return paymentRefundBusinessDO;
    }

    private String getRefundResponseCode(String[] responseParams) {
        // 0410|FREECHARGE|MHCC2823459048|NA|FCVW1210050003587|NA|NA|NA|NA|NA|ERR_REF009|Cancel
        // request already received|N|296744785
        String refundResponseCode = responseParams[8];
        if ("NA".equals(refundResponseCode)) {
            return responseParams[10];
        }

        return refundResponseCode;
    }

    private String createRefundParam(PaymentRefundBusinessDO paymentRefundBusinessDO) {
        String merchantOrderId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
        // todo - what about dates at the end of day
        Date sentToPG = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getSetToPg();
        if (sentToPG == null) {
            List<PaymentTransaction> txnList = paymentTransactionService
                    .getPaymentTransactionByMerchantOrderId(merchantOrderId);
            if (txnList != null && !txnList.isEmpty()) {
                for (PaymentTransaction paymentTransaction : txnList) {
                    if (!paymentTransaction.getPaymentGateway()
                            .equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                        sentToPG = txnList.get(0).getSetToPg();
                    }
                }
            }
        }
        String transactionDate = formatDateForRefund(sentToPG);

        String paymentGatewayTransactionId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO()
                .getTransactionId();

        // Do not change the order of the params, the order matters.
        List<String> params = new LinkedList<String>();
        params.add(REFUND_REQUEST_TYPE);
        params.add(MECODE);
        params.add(paymentGatewayTransactionId);
        params.add(transactionDate);
        params.add(merchantOrderId);

        // todo : check this method - PaymentUtil.getDoubleTillSpecificPlaces
        double transactionAmount = PaymentUtil
                .getDoubleTillSpecificPlaces(new Double(paymentRefundBusinessDO.getAmount()), 2);
        params.add(formatCurrency(transactionAmount));

        double refundAmount = PaymentUtil
                .getDoubleTillSpecificPlaces(new Double(paymentRefundBusinessDO.getRefundAmount()), 2);
        params.add(formatCurrency(refundAmount));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(BILL_DESK_DATE_FORMAT);
        String currentDate = simpleDateFormat.format(new Date());
        params.add(currentDate);

        String toSendParam = StringUtils.join(params, MESSAGE_PARAM_SEPARATOR);

        String checksum = createChecksum(toSendParam, WORKING_KEY);
        params.add(checksum);

        toSendParam = StringUtils.join(params, MESSAGE_PARAM_SEPARATOR);
        return toSendParam;
    }

    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO,
            Boolean isPaymentStatusForAdminPanel) {
        logger.debug("Initiating status check for BD : " + paymentQueryBusinessDO.getOrderId());

        String txnRefId = paymentQueryBusinessDO.getMerchantTxnId();
        String orderId = paymentQueryBusinessDO.getOrderId();

        String submitData = createPaymentStatusSubmitString(txnRefId);

        Map<String, String> paramterMap = new HashMap<String, String>();
        paramterMap.put(MESSAGE_PARAM_NAME, submitData);

        logger.debug("Transaction status submit data for BD is:" + submitData);

        String response = "";

        try {
            submitData = PaymentUtil.createSubmitData(paramterMap, PaymentConstants.UTF_8_ENCODING);
            response = PaymentUtil.submitPost(BILLDESK_CHECK_STATUS_URL, submitData, false, "192.168.1.2", 80);
            logger.debug("Transaction status response from bill desk is:" + response);
        } catch (Exception ex) {
            logger.error("Exception in submitting request to url for order id : " + orderId, ex);
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
            return paymentQueryBusinessDO;
        }

        if (response == null || response.trim().isEmpty()) {
            logger.error("null or empty response for payment status check for order id : " + orderId);
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
            return paymentQueryBusinessDO;
        }

        Map<String, String> responseMapValue = convertPaymentResponseIntoMap(response);
        boolean isSuccessful = isPaymentSuccessful(responseMapValue);

        if (isSuccessful) {
            paymentQueryBusinessDO.setPaymentStatus(true);
        } else {
            paymentQueryBusinessDO.setPaymentStatus(false);
        }

        Map<String, String> responseMap = new HashMap<String, String>();

        responseMap.put(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY, response);

        PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
        paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
        paymentResponseDO.setRequestMap(responseMap);
        paymentResponseDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));

        Map<String, String> billDeskS2SMap = convertPaymentResponseIntoMap(response);

        String mtxnId = billDeskS2SMap.get(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY);

        PaymentTransaction uniquePaymentTransaction = paymentTransactionService
                .getUniquePaymentTransactionByMerchantOrderId(mtxnId);
        /*
         * Don't care about success and error URLs here so null is good enough.
         */
        handlePaymentResponse(paymentResponseDO, uniquePaymentTransaction.getOrderId(),
                uniquePaymentTransaction.getPaymentTxnId(), null, null, isPaymentStatusForAdminPanel);

        /*
         * Refresh payment transaction.
         */
        PaymentTransaction refreshedPaymentTransaction = paymentTransactionService
                .getPaymentTransactionById(uniquePaymentTransaction.getPaymentTxnId());

        if (refreshedPaymentTransaction.getIsSuccessful()) {
            paymentQueryBusinessDO
                    .setPgTransactionTime(new Timestamp(refreshedPaymentTransaction.getPgTransactionTime().getTime()));
        }

        paymentQueryBusinessDO.setPgTransactionId(refreshedPaymentTransaction.getTransactionId());
        paymentQueryBusinessDO.setResponseMsg("NA");

        logger.debug("Status check complete for BD : " + paymentQueryBusinessDO.getOrderId());

        return paymentQueryBusinessDO;
    }

    private String createPaymentStatusSubmitString(String txnRefId) {
        DateFormat df = new SimpleDateFormat(BILL_DESK_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        String formattedDate = df.format(currentDate);

        StringBuilder msg = new StringBuilder();
        msg.append(TRANSACTION_STATUS_REQUEST_TYPE).append(MESSAGE_PARAM_SEPARATOR).append(MECODE)
                .append(MESSAGE_PARAM_SEPARATOR).append(txnRefId).append(MESSAGE_PARAM_SEPARATOR).append(formattedDate);

        String checksum = createChecksum(msg.toString(), WORKING_KEY);
        msg.append(MESSAGE_PARAM_SEPARATOR).append(checksum);

        String submitData = msg.toString();
        return submitData;
    }

    @PostConstruct
    public void loadConfigurations() {

        BILLDESK_Success_Return_Value = properties.getProperty(PaymentConstants.KEY_BILLDESK_SUCCESS_DECISION);
        BILLDESK_URL = properties.getProperty(PaymentConstants.KEY_BILLDESK_URL);
        BILLDESK_FREECHARGE_RETUERN_URL = properties.getProperty(PaymentConstants.KEY_BILLDESK_RETURN_URL);
        BILLDESK_CHECK_STATUS_URL = properties.getProperty(PaymentConstants.KEY_BILLDESK_CHECK_STATUS_URL);
        BILLDESK_REFUND_URL = properties.getProperty(PaymentConstants.KEY_BILLDESK_REFUND_URL);
        WORKING_KEY = properties.getProperty(PaymentConstants.KEY_BILLDESK_WORKING_KEY);
        MECODE = properties.getProperty(PaymentConstants.KEY_BILLDESK_MECODE);
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {

        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }

    private PaymentResponse dumpResponsetAudit(PaymentResponse paymentResponse) {

        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;

    }

    private Integer createOrderIdForPG(String productOrderId) {
        try {
            return paymentTransactionService.countPGRequestForOrderId(productOrderId);
        } catch (Exception exp) {
            logger.error("Count for orderid failed.. setting count of order id to 0");
            return 0;
        }
    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
            pt = paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
        return pt;
    }

    private String getRefundDescription(String refundStatus) {
        if (CANCELLATION_CODE.equals(refundStatus)) {
            return "Cancellation";
        }

        if (REFUND_CODE.equals(refundStatus)) {
            return "Refund";
        }

        return refundStatus;
    }

    private Boolean isChecksumEqual(String message, final String checkSumToCheck, final String checkSumKey) {
        logger.info("BillDesk is going to validate the checksum. Checksum received is : " + checkSumToCheck);

        // TBD: Remove this by generating proper checksum in DummyPGController
        if (fcProperties.shouldMockPG()) {
            return true;
        }

        message = StringUtils.chomp(message, MESSAGE_PARAM_SEPARATOR); // Remove
                                                                       // the
                                                                       // extra
                                                                       // pipe
                                                                       // at end
                                                                       // if
                                                                       // present.

        String[] parameterArray = StringUtils.splitPreserveAllTokens(message, MESSAGE_PARAM_SEPARATOR);

        List<String> parameterListForChecksum;

        if (parameterArray != null) {
            parameterListForChecksum = new ArrayList<String>(Arrays.asList(parameterArray));
            parameterListForChecksum.remove(parameterListForChecksum.size() - 1);

            String stringToCheck = StringUtils.join(parameterListForChecksum, MESSAGE_PARAM_SEPARATOR);
            String calculatedCheckSum = PGIUtil.doDigest(stringToCheck, checkSumKey);

            logger.info("Calculated checksum is : " + calculatedCheckSum + " for order id: " + message);

            if (StringUtils.equalsIgnoreCase(checkSumToCheck, calculatedCheckSum)) {
                logger.info("Checksums Matched here");
                return true;
            }
        }

        logger.info("Sorry Checksums don't Match");
        return false;
    }

    private String createChecksum(String msg, final String key) {
        return PGIUtil.doDigest(msg, key);
    }

    public static class BankItemCode {
        private String bankCode;
        private String itemCode;

        public BankItemCode(String bankCode, String itemCode) {
            super();
            this.bankCode = bankCode;
            this.itemCode = itemCode;
        }

        public String getBankCode() {
            return bankCode;
        }

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }
    }

    public static String formatDateForRefund(Date date) {
        return new SimpleDateFormat(REFUND_DATE_FORMAT).format(date);
    }

    public static String formatCurrency(double currency) {
        DecimalFormat formatter = new DecimalFormat("0.00");
        return formatter.format(currency);
    }

    private FCProperties              properties;
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    public void setProperties(FCProperties properties) {
        this.properties = properties;
    }

    /**
     * Sample S2S response -This method is not getting used in web flow
     * 
     * msg=FREECHARGE|FCVW14011622769193|MSBI3227468430|IGP9859464
     * |00000100.00|SBI
     * |FREECHARGE|NA|INR|DIRECT|NA|NA|NA|16-01-2014%2012:24:31|0300
     * |NA|NA|NA|NA|NA|NA|NA|NA|NA|Completed%20successfully.|1324308708
     * 
     * Persist the response and if success then initiate recharge.
     */
    @Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws Exception {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        logger.info("BillDesk response handling starts, parameters received are: " + requestMap);

        final String msg = requestMap.get(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY);

        Map<String, String> responseMap = convertPaymentResponseIntoMap(msg);

        String mtxnId = responseMap.get(ORDER_ID_PAYMENT_RESPONSE_MAP_KEY);

        responseMap.put(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY, msg);

        PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
        paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
        paymentResponseDO.setRequestMap(responseMap);
        paymentResponseDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));

        PaymentTransaction uniquePaymentTransaction = paymentTransactionService
                .getPaymentTransactionByMerchantOrderIdAndPG(mtxnId, PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
        metricsClient.recordLatency("Payment", "S2SLatency." + uniquePaymentTransaction.getPaymentGateway(),
                System.currentTimeMillis() - uniquePaymentTransaction.getSetToPg().getTime());
        /*
         * Don't care about success and error URLs here so null is good enough.
         */
        return handlePaymentResponse(paymentResponseDO, uniquePaymentTransaction.getOrderId(),
                uniquePaymentTransaction.getPaymentTxnId(), null, null, false);
    }

    private String getPaymentRequestDataToLog(Map<String, String> dataMap) {
        StringBuffer buffer = new StringBuffer();
        for (String key : dataMap.keySet()) {
            if (buffer.length() != 0) {
                buffer.append("&");
            }
            String value = dataMap.get(key);
            if (key.equals(PAYDATA_PARAM_NAME)) {
                String[] payDataValues = value.split("\\|");
                payDataValues[0] = "X";
                payDataValues[3] = "X";
                value = StringUtils.join(payDataValues, "|");
            }
            buffer.append(key).append("=").append(value);
        }
        return buffer.toString();
    }

    @Override
    @Transactional
    public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception {
        walletGatewayHandler.refundToWallet(paymentRefundBusinessDO, refundType);
    }

    @Override
    protected void validatePaymentResponse(Map<String, String> payResponseMap) {
        // TODO Auto-generated method stub

    }

    @Override
    protected PaymentResponse createPaymentResponse(Map<String, String> payResponseMap) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getPaymentPortalMerchantId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getMerchantTransactionId(Map<String, String> payResponseMap) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        return null;
    }

    /*
     * Method for formatting expiry month to 2 digits. Billdesk has the
     * requirement for the expiry month to always be as 2
     * digits("01","02"..."12")
     */
    private String getExpMonthInReqFormat(String expMonth) {
        if (expMonth.length() == 2) {
            return expMonth;
        } else {
            return "0" + expMonth;
        }
    }
}
