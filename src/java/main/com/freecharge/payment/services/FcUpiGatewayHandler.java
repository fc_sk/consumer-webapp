package com.freecharge.payment.services;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.KlickPayConstants;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.web.PaymentResponseWebDO;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.IGatewayHandler.PaymentGateways;
import com.freecharge.payment.services.KlickPayGatewayHandler.RequestConstants;
import com.freecharge.payment.services.KlickPayGatewayHandler.ResponseConstants;
import com.freecharge.payment.services.PaymentGatewayHandler.Constants;
import com.freecharge.payment.services.PaymentGatewayHandler.RequestParameters;
import com.freecharge.payment.util.KlickPayUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletUtil;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.klickpay.payment.FCPayUtil;

@Component("fcUpiGatewayHandler")
public class FcUpiGatewayHandler extends PaymentGatewayHandler{
    
    @Autowired
    KlickPayUtil                                  klickPayUtil;
    
    @Autowired
    OrderIdWriteDAO                               orderIdWriteDAO;
    
    @Autowired
    OneCheckWalletService                         oneCheckWalletService;
    
    @Autowired
    PaymentTransactionService paymentTransactionService;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private CardDataHandler            klickPayGatewayHandler;
    
    @Autowired
    private PaymentSuccessAction paymentSuccessAction;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    private KpayPaymentMetaService                kpayPaymentMetaService;
    
    @Autowired
    private PaymentPlanService            paymentPlanService;
    
    @Autowired
    private PricingService                pricingService;
    
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    public Map<String, String> processUpiPaymentRequest(HttpServletRequest request, HttpServletResponse response, Users user, ProductPaymentBusinessDO productPaymentBusinessDO, String mtxnId){
        Map<String, String> responseParams = new HashMap<String, String>();
        Map<String, Object> paymentRequestMap = new HashMap<String, Object>();
        
        PaymentTransaction paymentTransaction=null;
        try {
            String userId = createKlickpayPaymentRequest(user, productPaymentBusinessDO, mtxnId, paymentRequestMap);
            
            createResponseMap(productPaymentBusinessDO, mtxnId, responseParams, userId);
            //Klickpay Call for FCUPI
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String submitData = ow.writeValueAsString(paymentRequestMap);
            logger.info("UPIDebug: Submit Data to klickpay: "+submitData);
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Request");
            
            Map<String, Object> wsResponse = klickPayGatewayHandler.makeWSCall(submitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_INIT_URL), 1000, 90000, true);
            ObjectMapper mapper = new ObjectMapper();
            logger.info("Klickpay response: " + mapper.writeValueAsString(wsResponse));
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Response");
            ////////////////
            
             paymentTransaction= getPaymentTransaction(mtxnId);
            if (!wsResponse.containsKey(ResponseConstants.PARAM_STATUS)
                    || !wsResponse.get(ResponseConstants.PARAM_STATUS).equals("E000")) {
                metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Fail");
                paymentTransaction.setIsSuccessful(false);
                logger.error("Error in KP authorization API");
            } else if (!wsResponse.isEmpty()) {
                metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Success");
            }
            
            Map<String, String> payResponseMap = new HashMap<>();
            //Map<String, Object> responseData = (Map<String, Object>) wsResponse.get("request_data");
            for (String key : wsResponse.keySet()) {
                Object value = wsResponse.get(key);
                if (value != null) {
                    payResponseMap.put(key, value.toString());
                }
            }
            logger.info("After converting response: " + mapper.writeValueAsString(payResponseMap));
            validatePaymentResponse(payResponseMap);
            
            String pgTxnId = payResponseMap.get(ResponseConstants.PARAM_PG_REF);
            responseParams.put(PaymentConstants.TRANSACTION_ID, pgTxnId);
            responseParams.put(PaymentConstants.AAMOUNT, payResponseMap.get(KlickPayGatewayHandler.ResponseConstants.PARAM_AMOUNT));
            
            
            PaymentResponse paymentResponse = createPaymentResponse(payResponseMap);
            paymentResponse.setAmount(paymentTransaction.getAmount());
            paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
            
            paymentTransaction.setTransactionId(pgTxnId);
            
            //Setting up response message
            String message;
            if(payResponseMap.containsKey(ResponseConstants.PARAM_PG_MSSG) && StringUtils.isNotEmpty(payResponseMap.get(ResponseConstants.PARAM_PG_MSSG))){
                message = payResponseMap.get(ResponseConstants.PARAM_PG_MSSG);
            }
            else{
                message = "UPI Service Error. Please try after some time.";
            }
            
            /// If payment status is not true or checksum is not matching, return false
            if(!Boolean.parseBoolean(payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS)) || !Boolean.parseBoolean(payResponseMap.get(RequestParameters.CHECKSUM_RESULT))){
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, message);
                paymentResponse.setPgMessage(message);
                logger.info("UPIDebug: payment failure for mtxnId: "+mtxnId);
                paymentTransaction.setIsSuccessful(false);
                notifyPayment(paymentTransaction, paymentResponse, null, false, pgTxnId );
                paymentTransactionService.updateUPIPaymentDetailsInDb(paymentTransaction, mapper.writeValueAsString(payResponseMap));
                paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), Integer.parseInt(userId), PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
                metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(paymentTransaction.getOrderId()) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Failure");
                metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Failure");
                return responseParams;
            }
            logger.info("UPIDebug: payment success for mtxnId: "+mtxnId+" Performing post success payment tasks now");
            paymentTransaction.setIsSuccessful(true);
            notifyPayment(paymentTransaction, paymentResponse, null, false, pgTxnId );
            paymentTransactionService.updateUPIPaymentDetailsInDb(paymentTransaction,mapper.writeValueAsString(payResponseMap));
            paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), Integer.parseInt(userId), PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS);
            //Adding campaign info only when payment is success
            HashMap<String, Object> paymentInfo = new HashMap<String, Object>();
            paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, paymentTransaction.getPaymentType());
            paymentInfo.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, new Date());
            try{
                logger.info("Populating payment data to intxndata " + paymentInfo + " for lookupid " + productPaymentBusinessDO.getLookupID());
                campaignServiceClient.saveInTxnData1(productPaymentBusinessDO.getLookupID(), FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, paymentInfo);
            }catch (Exception e){
                logger.error("Error saving intxndata in wallet controller for uniqueId " + productPaymentBusinessDO.getLookupID() + ", orderId "
                        + productPaymentBusinessDO.getOrderId() + " and data " + paymentInfo, e);
            }
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, false);
            onPaymentSuccess(paymentTransaction);
            if (!FCUtil.isEmpty(paymentResponse.getStatus())
                    && paymentResponse.getStatus().equals(KlickPayConstants.SUCCESS_CODE)) {
                kpayPaymentMetaService.insertKpayPaymentMeta(paymentResponse.getMtxnId(), paymentResponse.getEci(),
                        ResponseConstants.PARAM_REFERENCE_ID, (String)paymentRequestMap.get(RequestConstants.PARAM_MERCHANT_ID));
            }
            
            String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
            PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
            paymentResponseWebDO.setRequestMap(responseParams);
            paymentResponseWebDO.setPg(PaymentGateways.FC_UPI.getName());
            paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_FCUPI));

            DelegateHelper.processRequest(request, response, delegateName, null, paymentResponseWebDO);
            String isMethodSuccessful = paymentResponseWebDO.getResultMap()
                    .get(PaymentConstants.IS_METHOD_SUCCESSFUL).toString();
            Map<String, String> resultMap = (Map<String, String>) paymentResponseWebDO.getResultMap().get(
                    PaymentConstants.DATA_MAP_FOR_WAITPAGE);
            String isPaySuccess = resultMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL);
            
            if (!"true".equalsIgnoreCase(isMethodSuccessful) || !"true".equalsIgnoreCase(isPaySuccess)) {
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, "Your payment failed due to some techinal difficulty. Please try after some time.");
                metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(paymentTransaction.getOrderId()) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Failure");
                metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Failure");
                return responseParams;
            }else {
            	metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(paymentTransaction.getOrderId()) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Success");
                metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Success");
            }
            
        } catch (Exception e) {
            logger.error("Exception occurred in UPI payment: ",e);
            metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(paymentTransaction.getOrderId()) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Failure");
            metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Failure");
            responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
            responseParams.put(PaymentConstants.MESSAGE, "Your payment failed due to techinal difficulty. Please try after some time.");
            return responseParams;
        } 
        //////////////////////////////////////////////
        return responseParams;
    }

    private void createResponseMap(ProductPaymentBusinessDO productPaymentBusinessDO, String mtxnId,
            Map<String, String> responseParams, String userId) {
        responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
        responseParams.put(PaymentConstants.MESSAGE, "UPI Transaction Successful");
        responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, mtxnId);
        responseParams.put(PaymentConstants.ORDER_ID, productPaymentBusinessDO.getOrderId());
        responseParams.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());
        responseParams.put(PaymentConstants.USER_ID, String.valueOf(userId));
    }

    private String createKlickpayPaymentRequest(Users user, ProductPaymentBusinessDO productPaymentBusinessDO,
            String mtxnId, Map<String, Object> paymentRequestMap) {
        String[] credentials = klickPayUtil.getKlickPayCredentials(
                orderIdWriteDAO.getProductName(productPaymentBusinessDO.getOrderId()).getProductType());
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> sessionData = fs.getSessionData();
        String type = FCSessionUtil.getRechargeType(productPaymentBusinessDO.getLookupID(), fs);
        Integer productId = FCConstants.productMasterMapReverse.get(type);
        String userId = ((Integer) sessionData.get(WebConstants.SESSION_USER_USERID)).toString();
        String clientRealIp = (String)sessionData.get(WebConstants.CLIENT_REAL_IP);
        logger.info("Debug client ip: "+clientRealIp);
        String contactNo = null;
        String customerEmail = null;
        if (user != null) {
            contactNo = user.getMobileNo();
            customerEmail = user.getEmail();
        }
        if (StringUtils.isEmpty(contactNo)) {
            contactNo = "6666666666";
        }
        if (StringUtils.isEmpty(customerEmail)) {
            customerEmail = "a@b.com";
        }
        String email = userServiceProxy.getEmailByUserId(Integer.parseInt(userId));
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        String metaData = "";
        
        Double pgAmountToSet = productPaymentBusinessDO.getAmount();
        Double totalAmountToPay = productPaymentBusinessDO.getAmount();
        try {
            CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(productPaymentBusinessDO.getOrderId());
            totalAmountToPay = pricingService.getPayableAmount(cartBusinessDo);
            PaymentPlan paymentPlan = paymentPlanService.getPaymentPlan(productPaymentBusinessDO.getOrderId());
            if (totalAmountToPay.doubleValue() != paymentPlan.getTotalAmount().getAmount().doubleValue()) {
                String errorString = "Total amount to pay does not match with payment plan. Expected amount: "
                        + totalAmountToPay.doubleValue() + ". Payment plan : " + paymentPlan;

                throw new IllegalStateException(errorString);
            }

            pgAmountToSet = paymentPlan.getPgAmount().getAmount().doubleValue();
        } catch (PaymentPlanNotFoundException ppnfe) {
            logger.error("Payment plan not found for order ID: [" + productPaymentBusinessDO.getOrderId() + "]");

            pgAmountToSet = totalAmountToPay;
        }

        logger.info("UPIDebug: paymentExecution Setting pg amount for order ID : [" + productPaymentBusinessDO.getOrderId() + "] as "
                + pgAmountToSet);
        productPaymentBusinessDO.setAmount(pgAmountToSet);
        
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_ID, merchantId);
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_TXN_ID, mtxnId);
        paymentRequestMap.put(RequestConstants.PARAM_TXN_AMOUNT, productPaymentBusinessDO.getAmount());
        paymentRequestMap.put(RequestConstants.PARAM_PRODUCT_INFO, productId.toString());
        paymentRequestMap.put(RequestConstants.PARAM_UPI_INFO, productPaymentBusinessDO.getUpiInfo());
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_NAME, "NA");
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_EMAIL, customerEmail);
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_CONTACT, contactNo);
        paymentRequestMap.put(RequestConstants.PARAM_BLACK_PAYMENTS, Constants.EXCLUSION_LIST);
        paymentRequestMap.put(RequestConstants.PARAM_CURRENCY, "INR");
        paymentRequestMap.put(RequestConstants.PARAM_PAYMENT_TYPE, PaymentConstants.FC_UPI_PAYMENT_OPTION);
        paymentRequestMap.put(RequestConstants.PARAM_CARD_TYPE, PaymentConstants.FC_UPI_PAYMENT_OPTION);
        paymentRequestMap.put(RequestConstants.PARAM_CALLBACK_URL, "not/needed");
        paymentRequestMap.put(RequestConstants.PARAM_GATEWAY_TO_USE, "fcupi");
        
        if (userId != null && !userId.trim().isEmpty()) {
            String cardStorageKey = userId;
            paymentRequestMap.put(RequestConstants.PARAM_USER_ID, fcProperties.getMode() + "-" + cardStorageKey);
        }
        
        if(StringUtils.isNotEmpty(clientRealIp))
            paymentRequestMap.put(RequestConstants.PARAM_CLIENT_REAL_IP, clientRealIp);
        
        if (oneCheckUserId != null && !oneCheckUserId.trim().isEmpty()) {
            paymentRequestMap.put(RequestConstants.PARAM_USER_ID, oneCheckUserId);
            metaData += "is_onecheck_user=true";
        }
        String checksum = FCPayUtil.calculateRequestChecksum(merchantKey, mtxnId,
                productPaymentBusinessDO.getAmount(),
                (String) paymentRequestMap.get(RequestConstants.PARAM_USER_ID));
        
        paymentRequestMap.put(RequestConstants.PARAM_CHECKSUM, checksum);
        return userId;
    }

    private PaymentTransaction getPaymentTransaction(String mtxnId) {
        List<PaymentTransaction> paymentTransactions = paymentTransactionService.getPaymentTransactionByMerchantOrderId(mtxnId);
        return paymentTransactions.get(0);
    }

    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws Exception {
        logger.info("UPIDebug: into FCUPIGatewayHandler");
        String freeChargeOrderId = paymentRequestBusinessDO.getOrderId();
        String merchantOrderId = paymentRequestBusinessDO.getMtxnId();
        merchantOrderId = StringUtils.trim(merchantOrderId);
        
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        int userId = (Integer)map.get(WebConstants.SESSION_USER_USERID);
        
        Map<String, String> retMap = new LinkedHashMap<String, String>();
        retMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);
        retMap.put(PaymentConstants.USER_ID, String.valueOf(userId));
        retMap.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());
        
        fs.getSessionData().put(merchantOrderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantOrderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantOrderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, retMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_FCUPI_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);
        
        paymentRequestBusinessDO.setResultMap(resultMap);
        
        String amount = paymentRequestBusinessDO.getAmount();
        String pgAmount = "";
        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }
        PaymentRequest paymentRequest = dumpPaymentRequest(paymentRequestBusinessDO, pgAmount, freeChargeOrderId,
                merchantOrderId, retMap);
        
        PaymentTransaction pt = dumpPaymentTransaction(paymentRequestBusinessDO, amount, freeChargeOrderId,
                merchantOrderId, paymentRequest);
        
        fs.getSessionData().put(merchantOrderId + "_txn_id", pt.getPaymentTxnId());
        
        return resultMap;
    }
    
    private PaymentTransaction dumpPaymentTransaction(PaymentRequestBusinessDO paymentRequestBusinessDO, String amount,
            String freeChargeOrderId, String merchantOrderId, PaymentRequest paymentRequest) {
        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(amount));
        pt.setMerchantTxnId(merchantOrderId);
        pt.setOrderId(freeChargeOrderId);
        pt.setPaymentGateway(PaymentGateways.KLICK_PAY.getName());
        pt.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());
        
        pt = paymentTransactionService.create(pt);
        return pt;
    }

    private PaymentRequest dumpPaymentRequest(PaymentRequestBusinessDO paymentRequestBusinessDO, String pgAmount,
            String freeChargeOrderId, String merchantOrderId, Map<String, String> retMap) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(pgAmount));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantOrderId);
        paymentRequest.setOrderId(freeChargeOrderId);
        paymentRequest.setPaymentGateway(PaymentGateways.KLICK_PAY.getName());
        paymentRequest.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(retMap));

        paymentRequest = paymentTransactionService.create(paymentRequest);
        return paymentRequest;
    }
    
    private void onPaymentSuccess(PaymentTransaction paymentTransaction) throws DuplicateRequestException {
        if (kestrelWrapper.isEnqueuedForFulfilment(paymentTransaction.getOrderId())) {
            kestrelWrapper.enqueuePaymentRefund(paymentTransaction.getMerchantTxnId(), paymentTransaction.getAmount());
        } else {
            paymentSuccessAction.onPaymentSuccess(paymentTransaction.getOrderId(), paymentTransaction.getMerchantTxnId());
        }
    }

    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        Map<String, Object> finalMap = new HashMap<String, Object>();
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        //NPE if request map is empty
        if(requestMap.isEmpty()){
            return finalMap;
        }
        Boolean isSuccessful = true;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String merchantOrderId = requestMap.get(PaymentConstants.MERCHANT_ORDER_ID);
        logger.info("UPIDebug: merchantOrderId in FCUPI gateway" + merchantOrderId);
        String transactionId = requestMap.get(PaymentConstants.TRANSACTION_ID);
        String amount = requestMap.get(PaymentConstants.AAMOUNT);
        String message = requestMap.get(PaymentConstants.MESSAGE);

        String mode = null;
        Integer id = (Integer)fs.getSessionData().get(merchantOrderId + "_txn_id");
        String productOrderId = FCSessionUtil.getProductOrderId(merchantOrderId, fs);
        //If id is not found in session get it from DB
        if (id == null|| productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId).get(0);
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        String redirectUrl = FCSessionUtil.getSuccessUrl(merchantOrderId, fs);
        String errorUrl = FCSessionUtil.getErrorUrl(merchantOrderId, fs) + "&payment=fail";
        if (productOrderId == null || redirectUrl == null || errorUrl == null) {
            PaymentTransaction uniquePaymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(merchantOrderId);
            productOrderId = uniquePaymentTransaction.getOrderId();
            String lookUpId = orderIdSlaveDAO.getLookupIdForOrderId(productOrderId);
            redirectUrl = ViewConstants.REDIRECT_URL;
            errorUrl = ViewConstants.ERROR_URL+"?lid="+lookUpId+"&payment=fail";
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String pg = PaymentGateways.KLICK_PAY.getName();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, isSuccessful.toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, productOrderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, pg);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, mode);


        
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);

        if (isSuccessful) {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);
        } else {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, errorUrl);
        }

        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(merchantOrderId);
        logger.info("amount value " + amount);
        return finalMap;
    }

    @Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO,
            Boolean isPaymentStatusForAdminPanel) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception {
        // TODO Auto-generated method stub
        
    }

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void validatePaymentResponse(Map<String, String> payResponseMap) {
        payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "true");

        com.klickpay.payment.model.PaymentResponse response = new com.klickpay.payment.model.PaymentResponse();
        response.setAmount(Double.parseDouble(payResponseMap.get(ResponseConstants.PARAM_AMOUNT)));
        response.setMerchantTransactionId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
        response.setReferenceId(payResponseMap.get(ResponseConstants.PARAM_REFERENCE_ID));
        response.setStatus(payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS));
        response.setTransactionTime(payResponseMap.get(ResponseConstants.PARAM_TRANSACTION_TIME));
        String orderId = paymentTransactionService
                .getMainOrderIdForMerchantId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
        String[] credentials = klickPayUtil.getKlickPayCredentials(orderIdWriteDAO.getProductName(orderId).getProductType());
        String expectedChecksum = FCPayUtil.calculateResponseChecksum(credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY], response);
        String responseChecksum = payResponseMap.get(ResponseConstants.PARAM_CHECKSUM);
        if (!expectedChecksum.equals(responseChecksum)) {
            logger.info("Invalid checksum for mtxnId: "+response.getMerchantTransactionId());
            payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "false");
        }
    }

    @Override
    protected PaymentResponse createPaymentResponse(Map<String, String> payResponseMap) {
            PaymentResponse paymentResponse = new PaymentResponse();

            if (payResponseMap.get(ResponseConstants.PARAM_AMOUNT) != null) {
                paymentResponse.setAmount(new Double(payResponseMap.get(RequestParameters.AMOUNT)));
            }
            
            if(payResponseMap.get(ResponseConstants.PARAM_RRN)!=null)
                paymentResponse.setRrnNo(payResponseMap.get(ResponseConstants.PARAM_RRN));
            
            paymentResponse.setIsSuccessful(isPaymentSuccessful(payResponseMap));
            paymentResponse.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
            paymentResponse.setPgTransactionId(payResponseMap.get(ResponseConstants.PARAM_REFERENCE_ID));
            paymentResponse.setMtxnId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
            paymentResponse.setUpiAccountDetails(payResponseMap.get(ResponseConstants.PARAM_FCUPI_ACC_DETAILS));

            String orderId = null;
            PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(
                    payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));

            if (paymentTransaction != null) {
                orderId = paymentTransaction.getOrderId();
                paymentResponse.setOrderId(orderId);
            }

            // The end bank reference code.
            paymentResponse.setAuthCodeNo(payResponseMap.get(RequestParameters.BANK_REFERENCE));

            // The bank used.
            paymentResponse.setEci(payResponseMap.get(ResponseConstants.PARAM_PG_USED));
            paymentsCache.setOrderPaymentDataInCache(orderId + PaymentConstants.FAILURE_REASON_CACHE_KEY,
                    payResponseMap.get(ResponseConstants.PARAM_USR_MSSG), 1);

            String paymentResponseCode = payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS);

            paymentResponse.setAvsCode(payResponseMap.get(ResponseConstants.PARAM_PG_UNDERLIER));
            paymentResponse.setResponseFromPp(PaymentUtil.getMapAsString(payResponseMap));
            paymentResponse.setPgMessage(paymentResponseCode);
            paymentResponse.setResponseData(PaymentUtil.serializedResponse(payResponseMap));
            paymentResponse.setStatus(payResponseMap.get(ResponseConstants.PARAM_STATUS));
            populateReportingData(paymentResponse, payResponseMap);
            
            logger.info("Status code from Klickpay : " + paymentResponse.getStatus());
            if (paymentResponseCode != null && !paymentResponseCode.isEmpty()) {
                metricsClient.recordEvent("Payment", "Klickpay.ResponseCode", paymentResponseCode);
            }

            return paymentResponse;
        }
private void populateReportingData(PaymentResponse paymentResponse, Map<String, String> payResponseMap) {
        
        ReconMetaData reconMetaData = new ReconMetaData();
        
        reconMetaData.setCategoryType(payResponseMap.get(ResponseConstants.PARAM_CARD_TYPE));
        reconMetaData.setReconId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
        reconMetaData.setCategorySubtype((null == payResponseMap.get(ResponseConstants.PARAM_PG_USED) && !("E000".equals(paymentResponse.getStatus()))) ? "NA" : payResponseMap.get(ResponseConstants.PARAM_PG_USED));
        reconMetaData.setReferenceId(payResponseMap.get(ResponseConstants.PARAM_PG_REF));
        paymentResponse.setReconMetaData(reconMetaData);
    }
    
    private Boolean isPaymentSuccessful(Map<String, String> payResponseMap) {
        String checkSumResult = payResponseMap.get(RequestParameters.CHECKSUM_RESULT);
        if (checkSumResult == null || checkSumResult.trim().isEmpty() || !Boolean.parseBoolean(checkSumResult)) {
            return false;
        }

        if (Boolean.parseBoolean(payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS))) {
            return true;
        }
        return false;
    }

    @Override
    protected String getPaymentPortalMerchantId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getMerchantTransactionId(Map<String, String> payResponseMap) {
        // TODO Auto-generated method stub
        return null;
    }

}
