package com.freecharge.payment.services;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class BinOfferCache extends RedisCacheManager {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RedisTemplate<String, Object> binOfferCacheTemplate;

    public void setBinOfferDataInCache(String binOfferKey, List<String> binRangeList, long cacheDurationInDays) {
        set(binOfferKey, binRangeList, cacheDurationInDays, TimeUnit.DAYS);
    }

    @Override
    public List<String> get(String key) {
        return (List<String>) super.get(key);
    }
}
