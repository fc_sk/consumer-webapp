package com.freecharge.payment.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.payment.util.PaymentConstants;

@Component
public class PaymentTypeFactory {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    private IPaymentType creditCardPaymentType;
    private IPaymentType debitCardPaymentType;
    private IPaymentType netbankingPaymentType;
    private IPaymentType cashCardPaymentType;
    private IPaymentType hdfcDebitCardPaymentType;
    private IPaymentType walletPaymentType;
    private IPaymentType zeroPaymentType;
    private IPaymentType oneCheckWalletPaymentType;
    private IPaymentType fcUpiPaymentType;

    @Autowired
    @Qualifier(value = "creditCardPaymentType")
    public void setCreditCardPaymentType(IPaymentType creditCardPaymentType) {
        this.creditCardPaymentType = creditCardPaymentType;
    }

    @Autowired
    @Qualifier(value = "debitCardPaymentType")
    public void setDebitCardPaymentType(IPaymentType debitCardPaymentType) {
        this.debitCardPaymentType = debitCardPaymentType;
    }

    @Autowired
    @Qualifier(value = "netbankingPaymentType")
    public void setNetbankingPaymentType(IPaymentType netbankingPaymentType) {
        this.netbankingPaymentType = netbankingPaymentType;
    }

    @Autowired
    @Qualifier(value = "cashCardPaymentType")
    public void setCashCardPaymentType(IPaymentType cashCardPaymentType) {
        this.cashCardPaymentType = cashCardPaymentType;
    }

    @Autowired
    @Qualifier(value = "hdfcDebitCardPaymentType")
    public void setHdfcDebigCardPaymentType(IPaymentType hdfcDebitCardPaymentType) {
        this.hdfcDebitCardPaymentType = hdfcDebitCardPaymentType;
    }

    @Autowired
    @Qualifier(value = "walletPaymentType")
    public void setWalletPaymentType(IPaymentType walletPaymentType) {
        this.walletPaymentType = walletPaymentType;
    }
    
    @Autowired
    @Qualifier(value = "zeroPaymentType")
    public void setZeroPaymentType(IPaymentType zeroPaymentType) {
        this.zeroPaymentType = zeroPaymentType;
    }
    
    @Autowired
    @Qualifier(value = "oneCheckWalletPaymentType")
    public void setOneCheckWalletPaymentType(IPaymentType oneCheckWalletPaymentType) {
        this.oneCheckWalletPaymentType = oneCheckWalletPaymentType;
    }

    @Autowired
    @Qualifier(value = "fcUpiPaymentType")
    public void setFcUpiPaymentType(IPaymentType fcUpiPaymentType) {
        this.fcUpiPaymentType = fcUpiPaymentType;
    }

    public IPaymentType getPaymentType(Integer paymentType) {
        if (paymentType == null) {
            String sessionId = FreechargeContextDirectory.get().getFreechargeSession().getUuid();
            logger.error("No payment type found for session id : " + sessionId);
            return null;
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentType.intValue()) {
            return creditCardPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentType.intValue()) {
            return debitCardPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentType.intValue()) {
            return netbankingPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CASHCARD) == paymentType.intValue()) {
            return cashCardPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ATMCARD) == paymentType.intValue()) {
            return debitCardPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD) == paymentType.intValue()) {
            return hdfcDebitCardPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET) == paymentType.intValue()) {
            return walletPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ZEROPAY) == paymentType.intValue()) {
            return zeroPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType.intValue()) {
            return oneCheckWalletPaymentType;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_FCUPI) == paymentType.intValue()) {
            return fcUpiPaymentType;
        }

        return null;
    }
}
