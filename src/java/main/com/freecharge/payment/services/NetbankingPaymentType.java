package com.freecharge.payment.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;

@Component
public class NetbankingPaymentType  extends PaymentType {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    AppConfigService configService;
    
    public Boolean doPayment(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {

        Map<String, String> validations = validateRequest(paymentRequestBusinessDO);
        Boolean valid = new Boolean(validations.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE));
        if(valid){
            IGatewayHandler gatewayHandler = getPaymentGateway(paymentRequestBusinessDO);
            if(doPaymentThroughKlickpay(paymentRequestBusinessDO)){
                logger.info("Routing netbanking payment through Klickpay for order : " + paymentRequestBusinessDO.getOrderId());
                gatewayHandler = pgFactory.getPaymentHandler(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
            }
            gatewayHandler.paymentRequestSales(paymentRequestBusinessDO);
            this.recordPaymentTypeMetric("netbanking");
        }

        return valid;
    }
    
    private boolean doPaymentThroughKlickpay(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        JSONObject klickpayConfig = configService.getCustomProp(PaymentConstants.KLICKPAY);
        if (klickpayConfig != null) {
            String klickPayCreditEnabled = (String) klickpayConfig.get(PaymentConstants.KLICKPAY_NB_ENABLED);
            if (StringUtils.isNotEmpty(klickPayCreditEnabled) && klickPayCreditEnabled.equals("true")) {
                String klickpayCreditPercentage = (String) klickpayConfig.get(PaymentConstants.KLICKPAY_NB_PERCENTAGE);
                if (StringUtils.isNotEmpty(klickpayCreditPercentage)) {
                    Integer condition = Integer.parseInt(klickpayCreditPercentage);
                    final int randomSampleInt = RandomUtils.nextInt(9999);
                    if (randomSampleInt % condition == 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public PaymentResponseBusinessDO checkStatus(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public PaymentRefundBusinessDO doRefund(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private Map<String, String> validateRequest(PaymentRequestBusinessDO paymentRequestBusinessDO)
    {

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

		logger.debug("Validation in Credit type payment starts for session id : "+fs.getUuid());

        Map<String, String> resultMap = new HashMap<String, String>();

		Boolean valid = true;
		List<String> errorList  = new ArrayList();

		if (StringUtils.isBlank(paymentRequestBusinessDO.getOrderId())) {
			valid = false;
			errorList.add("payment.orderid.error");
		}

		Map<String, String> amountValidationMap = PaymentUtil.validateAmount(paymentRequestBusinessDO.getAmount());

		if(valid)
		{
			valid = StringUtils.equalsIgnoreCase(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE), "true");
		}

		errorList.add(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION));
        Integer numTxnx = this.successfulTransaction(paymentRequestBusinessDO.getOrderId());
        if (numTxnx > 0){
            errorList.add("Already a successful Txn");
            valid = false;
        }


        String hashKey = paymentRequestBusinessDO.getHashKey();

        String f_string = paymentRequestBusinessDO.getAmount()+paymentRequestBusinessDO.getOrderId()+"freecharge"+properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);

		if (StringUtils.isNotBlank(hashKey) && !isAmountOverriden())
		{

			String outputString = PaymentUtil.getHashKey(f_string);

			if(!StringUtils.equalsIgnoreCase(hashKey, outputString))
			{
				valid = false;
				errorList.add("Url Tampered");
			}
		}

		resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_STATE, valid.toString());
		resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION, errorList.toString());
		resultMap.put(PaymentConstants.ORDER_ID, paymentRequestBusinessDO.getOrderId());

        if (!valid) {
            paymentRequestBusinessDO.setResultMap(resultMap);
        }

		logger.info(errorList);
		logger.info("Validation Ends here");

		return resultMap;
	}

    private Boolean isAmountOverriden()
    {
        String overrideAmount = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_AMOUNT_OVERRIDE);

        if(StringUtils.isNotBlank(overrideAmount) && !StringUtils.equalsIgnoreCase(overrideAmount, "-1"))
        {
        	return true;
        }
        return false;
    }


    private Integer successfulTransaction(String orderId){
        return paymentTransactionService.getSuccessFullTxnForOrderId(orderId);
    }

}
