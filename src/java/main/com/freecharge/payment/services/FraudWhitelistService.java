package com.freecharge.payment.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.service.UserService;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class FraudWhitelistService {
    private Logger              logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private FraudWhitelistCache fraudWhitelistCache;
    @Autowired
    private UserService         userService;

    public Boolean whiteListUser(Integer userId) {
        Boolean isAlreadyWhitelisted = fraudWhitelistCache.isWhitelisted(userId);
        if (isAlreadyWhitelisted == null || !isAlreadyWhitelisted) {
            try {
                fraudWhitelistCache.noteWhitelistedEmail(userId);
                int count = userService.whitelistUserInDatabase(userId);
                return count >= 1 ? true : false;
            } catch (Exception e) {
                logger.error("Failed to whitelist user id : " + userId, e);
                return false;
            }
        }
        return true;
    }

    public Boolean blacklistUser(Integer userId) {
        try {
            fraudWhitelistCache.removeWhitelistedEmail(userId);
            int count = userService.blacklistUserInDatabase(userId);
            return count >= 1 ? true : false;
        } catch (Exception e) {
            logger.error("Failed to blacklist user id : " + userId, e);
            return false;
        }
    }

    public Boolean isWhitelisted(Integer userId) {
        Boolean isWhitelisted = true;
        try {
            isWhitelisted = fraudWhitelistCache.isWhitelisted(userId);
            if (isWhitelisted == null) {
                if (!userService.isWhitelistedInDatabase(userId)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception e) {
            logger.error("Failed to check  isWhitelisted for user id : " + userId, e);
            return false;
        }
        return isWhitelisted;
    }
}
