package com.freecharge.payment.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.services.IPService;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.social.config.FacebookManager;
import com.freecharge.social.db.SocialUser;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 10/6/13
 * Time: 1:02 AM
 * To change this template use File | Settings | File Templates.
 */
@Service("fraudCheckService")
public class FraudCheckService {

    @Autowired
    FacebookManager facebookManager;

    @Autowired
    private WalletService walletService;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private IPService ipService;
    
    @Autowired
    WalletWrapper walletWrapper;

    /**
     * This method checks for fraud based on wallet amount, last successful recharges, date of registration
     *
     *
     * @param curUser
     * @param freefundClassId
     * @param request
     * @return
     */
    public Boolean shouldShowFbConnect(Users curUser, Long freefundClassId, HttpServletRequest request){
        Boolean result = false;
        if (appConfig.isFbFraudCheckEnabled()){
            logger.debug("Facebook fraud check enabled: Checking user: "+curUser.getUserId());

            //Using 1 september 2013 as the date to check social account is connected with new app.
            //Not adding to properties as this should be removed when all existing connected users
            //are connected with the new fb app
            Calendar calendar = Calendar.getInstance();
            calendar.set(2013, Calendar.SEPTEMBER, 1);

            //Making 30 days as check for new user sign up.
            Calendar backDate = Calendar.getInstance();
            backDate.add(Calendar.DATE, -30);
            SocialUser socialUser = this.facebookManager.getExistingFbUserWithLastUpdatedGtThan(curUser.getUserId().
                    longValue(), calendar.getTime());
            Amount amount = walletWrapper.getTotalBalance(curUser.getUserId().longValue());
            List<String> rechargeList = userTransactionHistoryService.findSuccessfulOrderIdByUserEmail(curUser.getEmail(), 20);

            if ((socialUser==null || !socialUser.getVerified()) && amount.getAmount().equals(BigDecimal.ZERO) &&
                    curUser.getDateAdded().after(new Timestamp(backDate.getTimeInMillis())) &&
                    (rechargeList ==null || rechargeList.size()<1)){
                result = true;
            }
            if (appConfig.isIpCheckEnabled()){
                boolean ipCheckStatus = false;
                JSONObject conditions = appConfig.getIpCheckConditions();
                Integer hours = 1;
                Long maxCount = 5l;
                if (conditions.get("count") != null){
                    try {
                        maxCount = Long.parseLong((String) conditions.get("count"));
                    }catch (Exception e){
                        //Just log the error do nothing
                        logger.error("Error reading count condition for ip check", e);
                    }
                }
                if (conditions.get("since") !=null){
                    try{
                        hours = Integer.parseInt((String)conditions.get("since"));
                    }catch (Exception e){
                        //Just log the error do nothing
                        logger.error("Error reading since condition for ip check", e);
                    }
                }
                Calendar ipCal = Calendar.getInstance();
                ipCal.add(Calendar.HOUR, -hours);
                Integer count = this.ipService.getIpCount(MobileUtil.getClientIpAddr(request), freefundClassId);
                if (count>maxCount){
                    logger.info("showip_for_user: "+curUser.getUserId());
                    this.metricsClient.recordEvent(FCConstants.FRAUD_CHECK_METRIC_SERVICE, "ipcheck", "show");
                    ipCheckStatus = true;
                }
                result = result && ipCheckStatus;
            }
        }
        if (result){
            logger.info("showFbConnect = true for user: "+curUser.getUserId());
            this.metricsClient.recordEvent(FCConstants.FRAUD_CHECK_METRIC_SERVICE, "showfbconnect", "yes");
        }else {
            logger.debug("showFbConnect = false for user: "+curUser.getUserId());
            this.metricsClient.recordEvent(FCConstants.FRAUD_CHECK_METRIC_SERVICE, "showfbconnect", "no");
        }
        return result;
    }

    private static final Logger logger = LoggingFactory.getLogger(FraudCheckService.class);
}
