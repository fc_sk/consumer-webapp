package com.freecharge.payment.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.infrastructure.billpay.util.BillPayUtil;
import com.freecharge.payment.dao.PaymentRetryNotificationDAO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;

@Service
public class PaymentRetryService {

private static final Logger logger = LoggingFactory.getLogger(PaymentRetryService.class);

@Autowired
PaymentTransactionService paymentTransactionService;

@Autowired
UserTransactionHistoryService userTransactionHistoryService;

@Autowired
OrderIdSlaveDAO orderIdSlaveDAO;

@Autowired
OrderIdReadDAO orderIdReadDAO;

@Autowired
private FCProperties fcProperties;

@Autowired
private PaymentOptionService paymentOptionService;

@Autowired
private OperatorCircleService operatorCircleService;

@Autowired
private UserServiceProxy userServiceProxy;

@Autowired
private PaymentRetryNotificationDAO paymentRetryNotificationDAO;	

@Autowired
@Qualifier("notificationConnectionManager")
private MultiThreadedHttpConnectionManager notificationConnectionManager;

@Autowired
private HomeService homeService;

@Autowired
@Qualifier("billPayServiceProxy")
private IBillPayService billPayServiceRemote;

private static String URL_SHORTEN_ENDPOINT = "url.shorten.endpoint";

private static final String TITLE = "Complete your payment now.";

@Autowired
PaymentsCache paymentsCache;

public void process(Integer m1, Integer m2) {
	Date now = new Date();
	Date to = DateUtils.addMinutes(now, -m1);
	Date from = DateUtils.addMinutes(to, -m2);
	logger.info("Checking failed payments from " + from + " to " + to);
	processFailedPayments(from, to);

}

@Async
public void updateOrderForRetry(String oldOrderId, String newOrderId){
    paymentRetryNotificationDAO.updateNewOrderIdOnNotificationRetry(oldOrderId, newOrderId);
}

private void processFailedPayments(Date from, Date to) {
	try {
			List<PaymentTransaction> failedPayments = paymentTransactionService.getFailedPaymentsWithTimeLimit(from, to);
			if (failedPayments == null) {
				logger.info("Could not find any failed orders between " + from + " and " + to);
				return;
			} else {
				logger.info("Processing failed orders. Count = " + failedPayments.size());
			}
		
			String[] eligibleProductList = paymentOptionService.getProductListFromCCS(fcProperties.getPaymentRetryProductsList());

			String orderId = null;
			for (PaymentTransaction failedPayment : failedPayments) {
				orderId = failedPayment.getOrderId();
				logger.info("Checking If Retriable for orderId "+failedPayment.getOrderId());
				if (paymentTransactionService.isOrderSuccessful(orderId) == Boolean.FALSE || paymentTransactionService.isOrderSuccessful(orderId) == null) {
					TxnHomePageBusinessDO txnHomePageBusinessDO = orderIdSlaveDAO.getTxnHomePageByOrderId(orderId);
					Integer userId = orderIdReadDAO.getUserIdFromOrderId(orderId);
					String serviceNumber = txnHomePageBusinessDO.getServiceNumber();
					Float amount = txnHomePageBusinessDO.getAmount();
					List<UserTransactionHistory> uth = userTransactionHistoryService.getSuccessfulTransactionHistoryByCombination(userId, serviceNumber, from, to, amount);
					if (uth == null || uth.isEmpty()){
						String redisKey = String.valueOf(userId).concat(serviceNumber).concat(String.valueOf(amount));
						logger.info("redis key : " +redisKey);
						String ifNotified = paymentsCache.get(redisKey);
						logger.info("ifNotified : " +ifNotified);
						if (ifNotified == null || ifNotified.isEmpty()) {
							logger.info("Notifying to retry for "+txnHomePageBusinessDO.getServiceNumber()+" and userId:"+userId);
							notifyPaymentRetry(failedPayment, txnHomePageBusinessDO, eligibleProductList);
							paymentsCache.setDataInCacheMinutes(String.valueOf(userId).concat(serviceNumber).concat(String.valueOf(amount)), "retry_notified", 30);
						}
					}
				}
			}
	} catch (Exception e) {
		logger.error("Caught exception while processing Failed Payments ", e);
	}
}

/*private String getFirstOrderIdOfSimilarPaymentAttempts(Integer userId, String serviceNumber, Date from, Date to, Float amount) {
	 UserTransactionHistory ut =  userTransactionHistoryService.getFirstOfSimilarPaymentAttempts(userId, serviceNumber, from, to, amount);
	 return ut.getOrderId();
}*/

// Send Push Notifications for the filtered orders
private void notifyPaymentRetry(PaymentTransaction failedPayment, TxnHomePageBusinessDO txnHomePageBusinessDO ,String[] eligibleProductList) {
	try {		
			logger.info("Notifying Payment Retry for orderID:"+failedPayment.getOrderId());
			
			if (!isRetriable(txnHomePageBusinessDO,eligibleProductList)) {
				logger.info("Product Type:" + txnHomePageBusinessDO.getProductType() + " is not an allowable productType");
				return;
			}
			
			String pushUrl = fcProperties.getPushNotificationUrl();
			PostMethod postMethod = new PostMethod(pushUrl);
			postMethod.setRequestHeader("Content-Type", "application/json");
			Users user = userServiceProxy.getUserFromOrderId(failedPayment.getOrderId());
			String emailId = user.getEmail();
			String contactNumber = user.getMobileNo();
			String firstName = userServiceProxy.getUserProfile(emailId).getFirstName();
			String productType = txnHomePageBusinessDO.getProductType();
			String amount = String.valueOf(txnHomePageBusinessDO.getAmount());
			String operatorName = null;
			String imgUrl = null;
			String serviceNumber;
			if (FCUtil.isUtilityPaymentType(productType)) {
				BillPayOperatorMaster billPayOperatorMaster = billPayServiceRemote.getBillPayOperatorFromName(txnHomePageBusinessDO.getOpName());
				operatorName = billPayOperatorMaster.getName();
				imgUrl = billPayOperatorMaster.getImgUrl();
				Map<String, Object> detailsMap = homeService.getUserRechargeDetailsFromOrderId(failedPayment.getOrderId());
				String billId = !FCUtil.isEmpty((String)detailsMap.get("billId")) ? (String)detailsMap.get("billId"):null; 
				BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
				serviceNumber = billDetails.getAdditionalInfo1();
			} else {
				OperatorMaster operatorMaster = operatorCircleService.getOperator(Integer.valueOf(txnHomePageBusinessDO.getOperatorName()));
				operatorName = operatorMaster.getName();
				imgUrl = operatorMaster.getImgUrl();
				serviceNumber = txnHomePageBusinessDO.getServiceNumber();
			}
	
			char platform = failedPayment.getOrderId().charAt(3);
			String urlParams = createUrlParams(productType, failedPayment.getOrderId(), firstName, amount, operatorName, txnHomePageBusinessDO);
			String key = null;
			switch(platform) {
				case 'W':
					key = FCProperties.CCS_PAYMENT_RETRY_CHANNELS_WEB;
					break;
					
				case 'M':
					key = FCProperties.CCS_PAYMENT_RETRY_CHANNELS_MOBILE;
					break;
					
				case 'A':
					key = FCProperties.CCS_PAYMENT_RETRY_CHANNELS_ANDROID;
					break;
					
				case 'I':
					key = FCProperties.CCS_PAYMENT_RETRY_CHANNELS_IOS;
					break;
			}
			
			String[] channelsList = paymentOptionService.getProductListFromCCS(key);
			
			if(channelsList == null) {
				channelsList = new String[1];
				channelsList[0] = "push";
			}
			
			/*for(String channel : channelsList) {
				if (channel.equals("email") || channel.equals("sms")) {
					String deeplinkUrl = "https://freecharge.com/fc/app/?action=recharge&" + urlParams;
					shortenedUrl = getShortUrl(deeplinkUrl);
					logger.info("shortened url for email/sms : " +shortenedUrl);
					break;
				}
			}*/
			
			for(String channel : channelsList) {
				if (channel.equals("email")) {
					String deepLinkEmail = "";
					String deeplinkUrl = "https://freecharge.com/fc/app/?action=recharge&" + urlParams;
					switch(platform) {
						case 'W' :
							deepLinkEmail = deeplinkUrl + "&UTM_source=pay_retry_notif_web_emai";
							break;
	
						case 'A':
							deepLinkEmail = deeplinkUrl + "&UTM_source=pay_retry_notif_android_email";
							break;
	
						case 'M':
							deepLinkEmail = deeplinkUrl + "&UTM_source=pay_retry_notif_wap_email";
							break;
					}
					String shortenedUrlEmailDeepLink = getShortUrl(deepLinkEmail);
					logger.info("shortened url for email: " +shortenedUrlEmailDeepLink);
					sendRetryMail(shortenedUrlEmailDeepLink, productType, emailId, firstName, amount, operatorName, failedPayment.getOrderId(), txnHomePageBusinessDO.getCirName(), imgUrl, serviceNumber);
					continue;
				}
				
				if (channel.equals("sms")) {
					String deepLinkSMS = "";
					String deeplinkUrlSMS = "https://freecharge.com/fc/app/?action=recharge&" + urlParams;
						switch(platform) {
						case 'W' :
							deepLinkSMS = deeplinkUrlSMS + "&UTM_source=pay_retry_notif_web_sms";
							break;
						
						case 'A':
							deepLinkSMS = deeplinkUrlSMS + "&UTM_source=pay_retry_notif_android_sms";
							break;
							
						case 'M':
							deepLinkSMS = deeplinkUrlSMS + "&UTM_source=pay_retry_notif_wap_sms";
							break;
					}
					String shortenedUrlSMSDeepLink = getShortUrl(deepLinkSMS);
					logger.info("shortened url for sms: " +shortenedUrlSMSDeepLink);
					sendRetrySMS(shortenedUrlSMSDeepLink, productType, contactNumber, firstName, amount, operatorName, failedPayment.getOrderId(), emailId);
					continue;
				}
				
				if (channel.equals("push")) {
					String deepLinkPush = "";
						switch(platform) {
						case 'W' :
							deepLinkPush = urlParams + "&UTM_source=pay_retry_notif_web_push";
							break;
						
						case 'A':
							deepLinkPush = urlParams + "&UTM_source=pay_retry_notif_android_push";
							break;
							
						case 'M':
							deepLinkPush = urlParams + "&UTM_source=pay_retry_notif_wap_push";
							break;
					}
					String timePeriod = paymentOptionService.getProductListFromCCS("failure.retry.inapp.timeperiod")[0];
					sendInAppAndPush(userServiceProxy.getUserIdByEmailId(emailId), serviceNumber, amount, timePeriod, imgUrl, deepLinkPush, operatorName, failedPayment.getOrderId(), emailId, firstName, productType);
					//sendRetryPush(urlParams, productType, emailId, firstName, amount, operatorName, failedPayment.getOrderId());
					continue;
				}
				
				if (channel.equals("ios")) {
					String iosUrlParams = createIosUrlParams(productType, failedPayment.getOrderId(), firstName, amount, operatorName, txnHomePageBusinessDO);
					String timePeriod = paymentOptionService.getProductListFromCCS("failure.retry.inapp.timeperiod")[0];
					sendInAppAndPush(userServiceProxy.getUserIdByEmailId(emailId), serviceNumber, amount, timePeriod, imgUrl, iosUrlParams.concat("&UTM_source=pay_retry_notif_iOS_push"), operatorName, failedPayment.getOrderId(), emailId, firstName, productType);
					//sendRetryIOSPush(iosUrlParams, productType, emailId, firstName, amount, operatorName, failedPayment.getOrderId());
				}
			}
	} catch (Exception e) {
		logger.error("Exception publishing Payment Retry Notification for OrderID: " + failedPayment.getOrderId(), e);
	}
}

private String createUrlParams(String productType, String orderId, String firstName, String amount, String operatorName, TxnHomePageBusinessDO txnHomePageBusinessDO) {
	StringBuilder urlParams = new StringBuilder();
	if (FCUtil.isUtilityPaymentType(productType)) {
		Map<String, Object> detailsMap = homeService.getUserRechargeDetailsFromOrderId(orderId);
		String billId = !FCUtil.isEmpty((String)detailsMap.get("billId")) ? (String)detailsMap.get("billId"):null; 
		BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
		urlParams.append("type=billpay").append("&serviceProvider=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getOperatorId())))
					.append("&serviceRegion=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getCircleId())))
					.append("&additionalInfo1=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo1()))
					.append("&additionalInfo2=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo2()))
					.append("&additionalInfo3=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo3()))
					.append("&additionalInfo4=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo4()))
					.append("&additionalInfo5=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo5()))
					.append("&amount=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getBillAmount())))
					.append("&fcBillerId=").append(BillPayUtil.sanitize(billDetails.getFcBillerId()))
					.append("&circleName=").append(BillPayUtil.sanitize(billDetails.getCircleName()))
					.append("&operatorName=").append(BillPayUtil.sanitize(operatorName))
					.append("&operatorLogo=")
					.append("&agBillId=").append(BillPayUtil.sanitize(billDetails.getAgBillId()))
					.append("&billDate=").append(BillPayUtil.sanitize(billDetails.getBillDate()))
					.append("&dueDate=").append(BillPayUtil.sanitize(billDetails.getDueDate()))
					.append("&billNumber=").append(BillPayUtil.sanitize(billDetails.getBillNumber()))
					.append("&isPartial=").append("false").append("&orderid=").append(orderId);
	} else {
		String type = "";
		switch(productType) {
			case "V":
				type = "prepaid";
				break;
				
			case "M":
				type = "postpaid";
				break;
				
			case "D":
				type = "dth";
				break;
				
			case "C":
			case "F":
				type = "datacard";
				break;
				
		}
		urlParams.append("type=").append(type).append("&number=").append(txnHomePageBusinessDO.getServiceNumber())
			.append("&amount=").append(amount).append("&operator=").append(txnHomePageBusinessDO.getOpName()).append("&operatorId=")
			.append(txnHomePageBusinessDO.getOperatorName()).append("&circleId=").append(txnHomePageBusinessDO.getCircleName()).append("&orderid=").append(orderId);;
	}
	
	return urlParams.toString();
}

private String createIosUrlParams(String productType, String orderId, String firstName, String amount, String operatorName, TxnHomePageBusinessDO txnHomePageBusinessDO) {
	StringBuilder urlParams = new StringBuilder();
	if (FCUtil.isUtilityPaymentType(productType)) {
		String billerType;
		try {
			billerType = String.valueOf(billPayServiceRemote.getBillPayValidatorForOperator(Integer.valueOf(txnHomePageBusinessDO.getOperatorName()) 
					,Integer.valueOf(txnHomePageBusinessDO.getCircleName())).getBillerType());
		} catch(Exception e){
			billerType = "";
		}
		Map<String, Object> detailsMap = homeService.getUserRechargeDetailsFromOrderId(orderId);
		String billId = !FCUtil.isEmpty((String)detailsMap.get("billId")) ? (String)detailsMap.get("billId"):null; 
		BillWithSubscriberNumber billDetails = billPayServiceRemote.getBillDetails(billId);
		urlParams.append("&serviceProvider=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getOperatorId())))
					.append("&productType=").append(productType)
					.append("&billerType=").append(billerType)
					.append("&serviceRegion=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getCircleId())))
					.append("&additionalInfo1=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo1()))
					.append("&additionalInfo2=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo2()))
					.append("&additionalInfo3=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo3()))
					.append("&additionalInfo4=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo4()))
					.append("&additionalInfo5=").append(BillPayUtil.sanitize(billDetails.getAdditionalInfo5()))
					.append("&amount=").append(BillPayUtil.sanitize(String.valueOf(billDetails.getBillAmount())))
					.append("&fcBillerId=").append(BillPayUtil.sanitize(billDetails.getFcBillerId()))
					.append("&circleName=").append(BillPayUtil.sanitize(billDetails.getCircleName()))
					.append("&operator=").append(BillPayUtil.sanitize(operatorName))
					.append("&operatorLogo=")
					.append("&agBillId=").append(BillPayUtil.sanitize(billDetails.getAgBillId()))
					.append("&billDate=").append(BillPayUtil.sanitize(billDetails.getBillDate()))
					.append("&dueDate=").append(BillPayUtil.sanitize(billDetails.getDueDate()))
					.append("&billNumber=").append(BillPayUtil.sanitize(billDetails.getBillNumber()))
					.append("&isPartial=").append("false").append("&orderid=").append(orderId);
	} else {
		urlParams.append("&number=").append(txnHomePageBusinessDO.getServiceNumber()).append("&productType=").append(productType)
			.append("&amount=").append(amount).append("&operator=").append(txnHomePageBusinessDO.getOpName()).append("&circle=")
			.append(txnHomePageBusinessDO.getCirName()).append("&operatorId=").append(txnHomePageBusinessDO.getOperatorName()).append("&circleId=")
			.append(txnHomePageBusinessDO.getCircleName()).append("&orderid=").append(orderId);
	}
	
	return urlParams.toString();
}

private void sendRetryMail(String shortenedUrlEmailDeepLink, String productType, String emailId, String firstName, String amount, String operatorName, String orderId, String circleName, String imgUrl, String serviceNumber) {
	try {
			Map<String, Object> subjectTemplateDataMap = new HashMap<String, Object>();
			Map<String, Object> contentTemplateDataMap = new HashMap<String, Object>();
			contentTemplateDataMap.put("name", firstName);
			contentTemplateDataMap.put("amount", amount);
			contentTemplateDataMap.put("operatorName", operatorName);
			contentTemplateDataMap.put("link", shortenedUrlEmailDeepLink);
			contentTemplateDataMap.put("orderId", orderId);
			if (FCUtil.isUtilityPaymentType(productType)) {
				if (FCUtil.isMetroPaymentType(productType)) {
					contentTemplateDataMap.put("isMetroPay", "true");
				}
				contentTemplateDataMap.put("isBillPay", "true");
			}
			if (!FCUtil.isEmpty(circleName)) {
				contentTemplateDataMap.put("circleName", circleName);
			}
			contentTemplateDataMap.put("imgPrefix", fcProperties.getProperty("akamai.img.prefix"));
			contentTemplateDataMap.put("operatorIcon", imgUrl);
			contentTemplateDataMap.put("serviceNumber", serviceNumber);
			contentTemplateDataMap.put("productType", productType);
			
			
			List<String> tags = new ArrayList<String>();
			tags.add(orderId);
			
			Map<String, Object> reqMap = new HashMap<String, Object>();
			reqMap.put("emailId", Integer.valueOf(fcProperties.getProperty(FCProperties.RETRY_CAMPAIGN_TOOL_EMAILID)));
			reqMap.put("subjectTemplateDataMap", subjectTemplateDataMap);
			reqMap.put("contentTemplateDataMap", contentTemplateDataMap);
			reqMap.put("toEmail", emailId);
			reqMap.put("tags", tags);

			String postUrl = fcProperties.getProperty(FCProperties.RETRY_EMAIL_END_POINT);
			postHttpMessage(postUrl, reqMap, orderId, emailId);
	} catch (Exception e) {
		logger.error("Exception while sending payment retry mail for oderId: " +orderId, e);
	}
}

private void sendRetrySMS(String shortenedUrlSMSDeepLink, String productType, String contactNumber, String firstName, String amount, String operatorName, String orderId, String emailId) {
	try {
			Map<String, String> templateDataMap = new HashMap<String, String>();
			templateDataMap.put("name", firstName);
			templateDataMap.put("amount", amount);
			templateDataMap.put("operatorName", operatorName);
			templateDataMap.put("link", shortenedUrlSMSDeepLink);
			
			List<String> tags = new ArrayList<String>();
			tags.add(orderId);
			
			Map<String, Object> reqMap = new HashMap<String, Object>();
			reqMap.put("smsId", Integer.valueOf(fcProperties.getProperty(FCProperties.RETRY_CAMPAIGN_TOOL_SMSID)));
			reqMap.put("templateDataMap", templateDataMap);
			reqMap.put("contactNumber", contactNumber);
			reqMap.put("tags", tags);
			
			String postUrl = fcProperties.getProperty(FCProperties.RETRY_SMS_END_POINT);
			postHttpMessage(postUrl, reqMap, orderId, emailId);
	} catch (Exception e) {
		logger.info("Exception while sending payment retry SMS for orderId: " +orderId, e);
	}
}

private String getRetryMessageForProductType(String productType, String firstName, String amount, String operatorName) {
	String message = null;
	switch(productType) {
		case "V":
		case "M":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_RECHARGE_RETRY),firstName, amount, operatorName);
			break;
			
		case "E":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_ELECTRICITY_RETRY), firstName, amount, operatorName);
			break;
			
		case "D":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_DTH_RETRY), firstName, amount, operatorName);
			break;
			
		case "Z":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_METRO_RETRY), firstName, amount, operatorName);
			break;
			
		case "C":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_DATACARD_RETRY), firstName, amount, operatorName);
			break;
			
		case "G":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_GAS_RETRY), firstName, amount, operatorName);
			break;
			
		case "L":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_LANDLINE_RETRY), firstName, amount, operatorName);
			break;
		case "Y":
			message = String.format(fcProperties.getProperty(FCProperties.MESSAGE_LANDLINE_RETRY), firstName, amount, operatorName);
			break;
	}
	return message;
}

private void recordPaymentRetryNotification(String orderId, String emailId) {
	try {
		paymentRetryNotificationDAO.recordPaymentRetryNotification(orderId, emailId);
	} catch (Exception e) {
		logger.error("Exception caught while logging payment Retry Notification for orderId:" + orderId);
	}

}

private boolean isRetriable(TxnHomePageBusinessDO txnHomePageBusinessDO,String[] eligibleProductList) {
	boolean isRetriable = false;
	for (String product : eligibleProductList) {
		if (product.equals(txnHomePageBusinessDO.getProductType())) {
			isRetriable = true;
			break;
		}
	}
	return isRetriable;
}

@SuppressWarnings("deprecation")
private void postHttpMessage(String postUrl, Map<String, Object> reqMap, String orderId, String emailId) {
	HttpClient client = new HttpClient();
	client.setHttpConnectionManager(notificationConnectionManager);
	PostMethod postMethod = new PostMethod(postUrl);
	postMethod.setRequestHeader("Content-Type", "application/json");
	Gson gson = new Gson();
	String postData = gson.toJson(reqMap);
	logger.info("[pushNotification data]: " + postData);
	postMethod.setRequestEntity(new StringRequestEntity(postData));
	int postStatus;
	String postResponse;
	try {
		postStatus = client.executeMethod(postMethod);
		postResponse = IOUtils.toString(postMethod.getResponseBodyAsStream());
	} catch (IOException e) {
		logger.error("Could not post http message to : " +postUrl);
		throw new RuntimeException(e);
	} finally {
		postMethod.releaseConnection();
	}
	
	if (HttpStatus.SC_OK == postStatus){
		recordPaymentRetryNotification(orderId, emailId);
	} else {
		throw new RuntimeException("Error sending Notification with status: " + postStatus + " with response: " + postResponse);
	}
}

@SuppressWarnings("deprecation")
private String getShortUrl(String longUrl) {
	HttpClient client = new HttpClient();
	client.setHttpConnectionManager(notificationConnectionManager);
	PostMethod postMethod = new PostMethod(fcProperties.getProperty(URL_SHORTEN_ENDPOINT));
	postMethod.setRequestHeader("Content-Type", "text/plain");
	postMethod.setRequestEntity(new StringRequestEntity(longUrl));
	int postStatus;
	String postResponse;
	try {
		postStatus = client.executeMethod(postMethod);
		postResponse = IOUtils.toString(postMethod.getResponseBodyAsStream());
	} catch (IOException e) {
			throw new RuntimeException(e);
	} finally {
		postMethod.releaseConnection();
	}

	if (HttpStatus.SC_OK != postStatus) {
		throw new RuntimeException("Error creating short url with status: " + postStatus + " with response: " + postResponse);
	}

	return postResponse;
 
}

private static final List<Integer> CHANNEL_ID_LIST = ImmutableList.of(3,4,5,6);

private static final String RETRY_INAPP_TEMPLATEID = "retry.inapp.templateid";

private static final String INAPP_POST_URL = "inapp.post.url";

public void sendInAppAndPush(Long userId, String serviceNumber, String amount, String timePeriod, String imageUrl, String finalUrl, String operatorName, String orderId, String emailId, String name, String productType) {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		DateTimeFormatter dtf2 = DateTimeFormat.forPattern("HH:mm:ss");
		int endTimeDuration = Integer.valueOf(timePeriod);
		String startTime = dtf.print(DateTime.now());
		String endTime = dtf.print(DateTime.now().plusHours(endTimeDuration));
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("userId", userId);
		reqMap.put("channelIdList", CHANNEL_ID_LIST);
		reqMap.put("templateId", Long.valueOf(fcProperties.getProperty(RETRY_INAPP_TEMPLATEID)));
		reqMap.put("startTime", startTime);
		reqMap.put("endTime", endTime);
		reqMap.put("isPushEnabled", 1);
		reqMap.put("source", "payment_retry");
		reqMap.put("fieldMap", createDescMap(serviceNumber, amount, dtf2.print(DateTime.now().plusHours(endTimeDuration)), imageUrl, finalUrl, operatorName, name, productType));
		postHttpMessage(fcProperties.getProperty(INAPP_POST_URL), reqMap, orderId, emailId);
}

 private Map<String, Object> createDescMap(String serviceNumber, String amount, String endTime, String imageUrl, String finalUrl, String operatorName, String name, String productType) {
	 	Map<String, Object> descMap = new HashMap<>();
        descMap.put("title", "Retry your Payment");
        descMap.put("field1", serviceNumber);
        descMap.put("field3", "Rs." +amount);
        descMap.put("field1_color", "#878787");
        descMap.put("field3_color", "#E3714D");
        descMap.put("cta1", "PAY NOW");
        descMap.put("cta2", "DISMISS");
        descMap.put("justification_text", "Complete your previous payment attempt");
        descMap.put("cta1_action_type", "DEEPLINK");
        descMap.put("cta1_action_url", "freechargeapp://freecharge?action=recharge&" + finalUrl.replace(" ", "%20"));
        descMap.put("info1", "Expires at " + endTime + " hrs");
        descMap.put("image_url", imageUrl);
        descMap.put("meta_data_use_case", "Payment Retry");
        descMap.put("meta_data_category", "Payment Retry Reminders");
        descMap.put("meta_data_tag", "Payment Retry");
        descMap.put("android_push_notification_template_id", fcProperties.getProperty(FCProperties.RETRY_CAMPAIGN_TOOL_ANDROIDTEMPLATEID));
        descMap.put("apple_push_notification_template_id", fcProperties.getProperty(FCProperties.RETRY_CAMPAIGN_TOOL_IOSTEMPLATEID));
        descMap.put("messageUrl", "recharge:" + finalUrl);
        descMap.put("Operator", operatorName);
        descMap.put("Amount", amount);
        descMap.put("url", "freechargeapp://freecharge?action=recharge&" + finalUrl.replace(" ", "%20"));
        descMap.put("name", name);
        descMap.put("amount", amount);
        descMap.put("operatorName", operatorName);
        descMap.put("message", getRetryMessageForProductType(productType, name, amount, operatorName));
        descMap.put("deeplink", "freechargeapp://freecharge?action=recharge&" + finalUrl);

        return descMap;

}

@SuppressWarnings("unused")
public static void main(String args[]) {
	TxnHomePageBusinessDO txnHomePageBusinessDO = new TxnHomePageBusinessDO();
	txnHomePageBusinessDO.setAmount((float) 100);
	txnHomePageBusinessDO.setOperatorName("Airtel ");
	txnHomePageBusinessDO.setCircleName("Karnataka");
	UserContactDetail contactDetails = new UserContactDetail();
	txnHomePageBusinessDO.setContactDetails(contactDetails);
	txnHomePageBusinessDO.setServiceNumber("9650800434");

    PaymentTransaction payTxn = new PaymentTransaction();
    payTxn.setOrderId("FCVW160222324772220");

	ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("commonApplicationContext.xml");
	PaymentRetryService paymentRetryService = (PaymentRetryService) ctx.getBean("paymentRetryService");
	
	//paymentRetryService.notifyPaymentRetry(payTxn, txnHomePageBusinessDO);


    // paymentRetryService.notifyPaymentRetry(payTxn,
    // txnHomePageBusinessDO);

    System.out.println("Notification Sent for service Number : " + txnHomePageBusinessDO.getServiceNumber());

    }
	

}