package com.freecharge.payment.services;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This cache holds, with timeout, various
 * user ids that have been caught in
 * the fraud checks and are not frauds.
 * The timeout is 30 days.
 * @author korath
 *
 */
@Component
public class FraudWhitelistCache extends RedisCacheManager {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private RedisTemplate<String, Object> whitelistedEmailCacheTemplate;
    private static final int TIMEOUT_DAYS = 30;

    private static final String keySuffix = "_whitelisted";
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        this.whitelistedEmailCacheTemplate.setValueSerializer(new GenericToStringSerializer<>(Boolean.class));
        return this.whitelistedEmailCacheTemplate;
    }
    
    public void noteWhitelistedEmail(final Integer userId) {
        putAndSetExpiry(userId, keySuffix, true);
    }
    
    public void removeWhitelistedEmail(final Integer userId) {
        putAndSetExpiry(userId, keySuffix, false);
    }
    
    public Boolean isWhitelisted(final Integer userId) {
        return getKey(userId, keySuffix);
    }
    
    private Boolean getKey(final Integer userId, final String keySuffix) {
        final String key = userId + keySuffix;
        Boolean isWhitelisted = false;
        try {
            isWhitelisted = (Boolean) this.get(key);
        } catch (Exception e) {
            logger.info("Exception while fetching whitelisted email id : " + userId, e);
        }
        return isWhitelisted;
    }
    
    private void putAndSetExpiry(final Integer userId, final String keySuffix, final Boolean isWhitelisted) {
        final DateTime today = new DateTime();
        final String key = userId + keySuffix;
        try {
            this.set(key, isWhitelisted);
            this.expireAt(key, today.plusDays(TIMEOUT_DAYS).toDate());
        } catch (Exception e) {
            logger.error("Exception while whitelisting emaiil id : " + userId, e);
        }
    }
}
