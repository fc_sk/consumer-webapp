package com.freecharge.payment.services;

import org.apache.commons.lang.StringUtils;

/**
 * Used to encapsulate a "card" payment type. It could 
 * either be credit card or debit card.
 * @author arun
 *
 */
public class PaymentCard {
    private String cardHash;
    private String cardBin;
    
    public PaymentCard() {
    }

    public PaymentCard(String cardHash, String cardBin) {
        this.cardHash = cardHash;
        this.cardBin = cardBin;
    }
    public String getCardHash() {
        return cardHash;
    }
    public void setCardHash(String cardHash) {
        this.cardHash = cardHash;
    }
    
    public String getCardBin() {
        return cardBin;
    }
    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }
    
    public boolean isEmpty() {
        return StringUtils.isBlank(cardBin) && StringUtils.isBlank(cardHash);
    }
}
