package com.freecharge.payment.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.dao.IPaymentGatewayManagementDAO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.web.webdo.PaymentGatewayManagementWebDO;

@Service
public class PaymentGatewayManagementService {

    @Autowired
    private IPaymentGatewayManagementDAO paymentGatewayManagementDAO;

    @Autowired
    private FCProperties properties;

    @Autowired
    private EmailService emailService;


    public Map<Integer, String> getPaymentGateways(){

        List<Map<String,Object>>  pgs = paymentGatewayManagementDAO.getActivePaymentGateways();

        Map<Integer, String> pg = new HashMap<Integer, String>();
        for(Map row : pgs){
            Integer id  = (Integer)row.get("id");
            String pgname = (String)row.get("name");
            pg.put(id,pgname);
        }
        return pg;

    }

    public Map<Integer, String> getBanksGrpMaster(){

        List<Map<String,Object>>  bks = paymentGatewayManagementDAO.getActiveBankGroup();
        Map<Integer, String> bk = new HashMap<Integer, String>();
        for(Map row : bks){
            Integer id  = (Integer)row.get("id");
            String pgname = (String)row.get("name");
            bk.put(id,pgname);
        }
        return bk;

    }
    
    public Map<Integer, String> getBanksGrpMasterForPgManage(){

        List<Map<String,Object>>  bks = paymentGatewayManagementDAO.getActiveBankGroup();
        Map<Integer, String> bk = new HashMap<Integer, String>();
        for(Map row : bks){
            Integer id  = (Integer)row.get("id");
            String pgDisplayName = (String)row.get("display_name");
            bk.put(id,pgDisplayName);
        }
        return bk;

    }

    public List<Map<String,Object>> getDefaultData(){
        return paymentGatewayManagementDAO.getDefaultData();    
    }

    public List<Map<String,Object>> getData(PaymentGatewayManagementWebDO managementWebDO){

        StringBuffer whereClause = new StringBuffer();
        if (!StringUtils.isBlank(managementWebDO.getProduct()))
            whereClause.append(" and pr.fk_product_master_id in ( ").append(managementWebDO.getProduct()).append(" ) ");

        if (!StringUtils.isBlank(managementWebDO.getPaymentGateway()))
            whereClause.append(" and pr.primary_gateway in ( ").append(managementWebDO.getPaymentGateway()).append(" ) ");

        if (!StringUtils.isBlank(managementWebDO.getBankGrp()))
            whereClause.append(" and pr.fk_banks_grp_master_id in ( ").append(managementWebDO.getBankGrp()).append(" ) ");


        return paymentGatewayManagementDAO.getData(whereClause.toString());
    }

    public void updatePG(Integer pgRelationId, Integer gatewayId){
        paymentGatewayManagementDAO.updatePG(pgRelationId,gatewayId);
    }


    public void createAndSendAlertEmail(String productName, String bankName, String oldValue, String newValue,
            String ipaddress, String currentAdminUser) {
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setSubject(properties.getProperty(PaymentConstants.KEY_PG_CHANGE_MAIL_SUBJECT));
        emailBusinessDO.setTo(properties.getProperty(PaymentConstants.KEY_PG_CHANGE_MAIL_RECEIVER));
        emailBusinessDO.setFrom(properties.getProperty(PaymentConstants.KEY_PG_CHANGE_MAIL_FROM));
        emailBusinessDO.setReplyTo(properties.getProperty(PaymentConstants.KEY_PG_CHANGE_MAIL_FROM));
        emailBusinessDO.setId(Calendar.getInstance().getTimeInMillis());
        emailBusinessDO.setContent(createPGChangeUpdateMsg(productName, bankName, oldValue, newValue, ipaddress,
                currentAdminUser));
        emailService.sendNonVelocityEmail(emailBusinessDO);
    }

    private String createPGChangeUpdateMsg(String productName, String bankName, String oldValue, String newValue,
            String ipaddress, String currentAdminUser) {
        String msg = "Payment Gateway changed from " + oldValue + " to " + newValue + " against bank " + bankName
                + " for product " + productName +" logged in from IP: " + ipaddress
                + ".   Created By: " + currentAdminUser;
        return msg;
    }
}
