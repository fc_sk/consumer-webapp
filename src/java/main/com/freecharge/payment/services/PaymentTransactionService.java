package com.freecharge.payment.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.exception.LockAcquisitionException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.PgMemcachedService;
import com.freecharge.app.service.PromocodeService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.easydb.EasyDBReadDAO;
import com.freecharge.common.easydb.EasyDBWriteDAO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.exception.FCCreditWalletLimitExceededException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.eventprocessing.event.PaymentFailureEvent;
import com.freecharge.eventprocessing.event.PaymentSuccessEvent;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dao.jdbc.PGRangeReadDAO;
import com.freecharge.payment.dao.jdbc.PaymentFulfilmentMapDAO;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dao.jdbc.PgWriteDAO;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentTransactionBusinessDO;
import com.freecharge.payment.dos.entity.BankMaster;
import com.freecharge.payment.dos.entity.PaymentGatewayMaster;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction.Status;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.entity.PgMisUser;
import com.freecharge.payment.dos.entity.PgRelation;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.services.IGatewayHandler.PaymentGateways;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.cron.RechargeStatusIdempotencyCache;
import com.freecharge.recharge.fulfillment.FulfillmentScheduleService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletController;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.wallet.exception.OneCheckWalletException;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.Wallet.FundDestination;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.wallet.service.WalletDebitRequest;
import com.freecharge.wallet.service.WalletTransaction;
import com.google.common.base.CaseFormat;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.snapdeal.payments.sdmoney.service.model.DebitBalanceResponse;

@Service
public class PaymentTransactionService {

    private final Logger                 logger                  = LoggingFactory
                                                                         .getLogger(PaymentTransactionService.class);

    private final List<ProductName>      productListForRecharge  = ImmutableList
                                                                         .of(ProductName.DTH, ProductName.Mobile, ProductName.DataCard);

    private final int                    RECHARGE_ATTEMPT_WINDOW = 3;
    
    private final int                    HCOUPON_ATTEMPT_WINDOW = 1;
    
    @Autowired
    private MetricsClient                metricsClient;

    @Autowired
    private WalletService                walletService;

    @Autowired
    protected UsersDAO                   usersDAO;

    @Autowired
    private PgReadDAO                    pgReadDAO;
    
    @Autowired
    private PaymentFulfilmentMapDAO        paymentFulfilmentMapDAO;

    @Autowired
    private PGRangeReadDAO               pgRangeReadDAO;

    @Autowired
    private CartService                  cartService;

    @Autowired
    private PgWriteDAO                   pgWriteDAO;

    @Autowired
    private PgMemcachedService           pgMemcachedService;

    @Autowired
    private EasyDBWriteDAO               easyDBWriteDAO;

    @Autowired
    private EasyDBReadDAO                easyDBReadDAO;

    @Autowired
    private PaymentGatewayHandlerFactory pgFactory;

    @Autowired
    private OrderIdWriteDAO              orderIdWriteDAO;

    @Autowired
    private FreechargeEventPublisher     freechargeEventPublisher;

    @Autowired
    private PaymentPlanService           paymentPlanService;

    @Autowired
    private InService                    inService;

    @Autowired
    private PaymentRefund                paymentRefund;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    public static final String ONE_CHECK_WALLET_TRANSACTION_RESPONSE = "walletTransactionResponse";
    
    @Autowired
    @Qualifier("payUGatewayHandler")
    private IGatewayHandler payUGatewayHandler;
    
    @Autowired
    @Qualifier("billDeskGatewayHandler")
    IGatewayHandler bdkGatewayHandler;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    CardGatewayHandler klickPayGatewayHandler;

    @Autowired
    private PricingService pricingService;
    
    @Autowired
    private FulfillmentScheduleService scheduleService;

    @Autowired
    private RechargeStatusIdempotencyCache rechargeStatusIdempotencyCache;

    @Autowired
    private WalletWrapper walletWrapper;

    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private PromocodeService promocodeService;
    
    @Autowired
    UserServiceProxy userServiceProxy;
    
    @Autowired
    private KpayPaymentMetaService kpayPaymentMetaService;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    AppConfigService appConfig;
    
    @Autowired
    private WalletController walletController;

    @Autowired
    PaymentRefundService paymentRefundService;
    
    private static final String DESTINATION_CORP_ID = "destinationCorpId";
    
    public PaymentStatusDetails isPaymentSuccessful(String lookupId) {
        return orderIdWriteDAO.isPaymentSuccessful(lookupId);
    }
    
    public Boolean isOrderSuccessful(String orderId){
        return orderIdWriteDAO.isOrderSuccessful(orderId);
    }

    @Transactional
    public List<PaymentCard> getSuccessfulPaymentCards(String orderId) {
        List<PaymentCard> cardList = new ArrayList<>();
        logger.info("Card hash and number in the database is not updated for orderId " + orderId);
        List<PaymentTransaction> paymentTransactionList = getSuccessfulPaymentTransactionsByOrderId(orderId);
        if (paymentTransactionList != null && !paymentTransactionList.isEmpty()) {
            String merchantTxnId = paymentTransactionList.get(0).getMerchantTxnId();
            logger.info("Fetching card data for order : " + orderId);
            final PaymentCard card = payUGatewayHandler.getPaymentCardData(merchantTxnId);
            if(card != null){
            	cardList.add(card);
            }
        }
        return cardList;
    }

    @Transactional
    public Integer countPGRequestForOrderId(String orderId) {
        return pgReadDAO.countPGRequestForOrderId(orderId);
    }

    @Autowired
    private FCProperties fcProperties;

    // Payment Transaction cache

    @Transactional
    public Integer getPaymentRequestCount(String orderId) {
        return pgReadDAO.getPaymentRequestCount(orderId);
    }

    @Transactional
    public Integer getSuccessFullTxnForOrderId(String orderId) {
        return pgReadDAO.getSuccessFullTxnForOrderId(orderId);
    }

    @Transactional
    public PaymentTransaction getSuccessfulPaymentTransactionByOrderId(String orderId) {
        return pgReadDAO.getSuccessfulPaymentTransactionByOrderId(orderId);
    }

    @Transactional
    public List<PaymentTransaction> getPaymentTxnDetails(String orderId) {
        return pgReadDAO.getPaymentTxnDetails(orderId);
    }

    @Transactional
    public PaymentTransaction getPaymentTransactionById(Integer txnId) {
        return pgReadDAO.getPaymentTransactionById(txnId);
    }

    @Transactional
    public PaymentGatewayMaster getPaymentGatewayMasterForCode(String gatewaycode) {
        return pgReadDAO.getPaymentGatewayMasterForCode(gatewaycode);
    }

    // Bank Master Cache
    @Transactional
    public BankMaster getBankMasterForRequest(String uiCode, Integer paymentOption) {

        try {
            BankMaster bankMaster = pgMemcachedService.getBankMaster(uiCode, paymentOption);
            if (bankMaster != null) {
                return bankMaster;
            } else {
                pgMemcachedService.setBankMaster(pgReadDAO.getBankMaster());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return pgReadDAO.getBankMasterForRequest(uiCode, paymentOption);
    }

    // Payment Gateway Master

    @Transactional
    public PaymentGatewayMaster getPaymentGateway(Integer gatewayId) {

        try {
            PaymentGatewayMaster paymentGatewayMaster = pgMemcachedService.getPaymentGatewayMaster(gatewayId);
            if (paymentGatewayMaster != null)
                return paymentGatewayMaster;
            else {
                pgMemcachedService.setPaymentGatewayMaster(pgReadDAO.getPaymentGatewayList());
            }
        } catch (Exception ex) {
        }

        return pgReadDAO.getPaymentGateway(gatewayId);
    }

    @Transactional
    public PgRelation getPaymentGateway(Integer productId, Integer affiliateId, Integer bankMasterId) {
        try {
            Map<String, PgRelation> map = (HashMap<String, PgRelation>) pgMemcachedService.getPgRelation();
            if (map != null && map.size() > 0) {
                return map.get(productId.toString() + affiliateId + bankMasterId);
            } else {
                pgMemcachedService.setPgRelation(pgReadDAO.getPgRelation());
            }
        } catch (Exception ex) {
        }

        return pgReadDAO.getPaymentGateway(productId, affiliateId, bankMasterId);
    }

    // Payment Type Option Cache

    @Transactional
    public List<PaymentTypeOptions> findAll() {
        try {
            List<PaymentTypeOptions> paymentTypeOptions = pgMemcachedService.getPaymentTypeOptions();
            if (paymentTypeOptions != null && paymentTypeOptions.size() > 0) {
                return paymentTypeOptions;
            } else {
                pgMemcachedService.setPaymentTypeOptions(pgReadDAO.findAll());
            }
        } catch (Exception ex) {
        }
        return pgReadDAO.findAll();
    }

    // Payment Type Master

    @Transactional
    public List<PaymentTypeMaster> getPaymentOptions() {
        try {
            List<PaymentTypeMaster> paymentTypeMasters = pgMemcachedService.getPaymentOptions();
            if (paymentTypeMasters != null && paymentTypeMasters.size() > 0) {
                return paymentTypeMasters;
            } else {
                pgMemcachedService.setPaymentOptions(pgReadDAO.getPaymentTypeMaster());
            }
        } catch (Exception ex) {
        }
        return pgReadDAO.getPaymentTypeMaster();
    }

    public List<Map<String, Object>> getPaymentOptionsList(BaseBusinessDO baseBusinessDO) {
        return pgReadDAO.getPaymentOptionsList(baseBusinessDO);
    }

    public List<PaymentTypeMaster> getPaymentType(Integer productId, Integer affiliateId) {
        return pgReadDAO.getPaymentType(productId, affiliateId);
    }

    public List<PaymentTypeOptions> getPaymentType(Integer paymentTypeRelationId) {
        return pgReadDAO.getPaymentType(paymentTypeRelationId);
    }

    @Transactional
    public boolean isMisUserValid(PgMisUser misUsers) {
        return pgReadDAO.isMisUserValid(misUsers);
    }

    @Transactional
    public int updatePaymentTransaction(PaymentTransaction pt) {
        return pgWriteDAO.updatePaymentTransaction(pt);
    }
    
    @Transactional
    public int updatePaymentTransactionDetails(PaymentTransaction pt) {
        return pgWriteDAO.updatePaymentTransactionDetails(pt);
    }
    
    @Transactional
    public Boolean getPaymentStatus(Integer paymentTxnId) {
        return pgReadDAO.getPaymentStatus(paymentTxnId);
    }

    @Transactional
    public void updatePaymentTransactionAmount(PaymentTransaction pt) {
        pgWriteDAO.updatePaymentTransactionAmount(pt);
    }

    @Transactional
    public PaymentTransaction create(PaymentTransaction pt) {
        return pgWriteDAO.insertPaymentTransaction(pt);
    }

    @Transactional
    public PaymentRefundTransaction update(PaymentRefundTransaction paymentRefundTransaction) {
        // todo - check whether this is ok
        easyDBWriteDAO.update(paymentRefundTransaction);
        return paymentRefundTransaction;
    }

    @Transactional
    public PaymentRefundTransaction create(PaymentRefundTransaction paymentRefundTransaction) {
        return pgWriteDAO.insertPaymentRefundTransaction(paymentRefundTransaction);
    }
    
    public void updateFreshPRT(List<PaymentRefundTransaction> refundList, String mtxnId) {
        logger.info("PRT size for mtxn : " + mtxnId + " is " + refundList.size());
        List<PaymentRefundTransaction> refundsForOrder = pgReadDAO.getSuccessfulRefundsForMtxnId(mtxnId);
        
        if (refundsForOrder.size() > 0 && refundsForOrder.size() != refundList.size()) {
            logger.info("Inconsistency in PRT table. Marking all refunds as failure and updating the correct status for " + mtxnId);
            updatePRTStatus(mtxnId, Status.FAILURE);
            refundsForOrder = pgReadDAO.getSuccessfulRefundsForMtxnId(mtxnId);
        }
        Set<Integer> processedRefundTxnIds = new HashSet<>();
        for (PaymentRefundTransaction refund : refundList) {
            if (PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE.equals(refund.getRefundTo())) {
                continue;
            }
            Integer refundTxnIdToBeProcessed = getPaymentRefundTransactionId(refundsForOrder, processedRefundTxnIds);
            logger.info("PRT txnId : " + refundTxnIdToBeProcessed + ". PRT : " + refund);
            if (refundTxnIdToBeProcessed != null) {
                updatePRT(refund, refundTxnIdToBeProcessed);
                processedRefundTxnIds.add(refundTxnIdToBeProcessed);
            } else {
                PaymentRefundTransaction insertedRefund = create(refund);
                processedRefundTxnIds.add(insertedRefund.getPaymentRefundTransactionId());
            }
        }
    }
    
    @Transactional
    private void updatePRT(PaymentRefundTransaction refund, Integer refundTxnIdToBeProcessed) {
        pgWriteDAO.updateRefundTransaction(refund, refundTxnIdToBeProcessed);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    private void updatePRTStatus(String mtxnId, Status status) {
        pgWriteDAO.updatePRTForMtxnId(mtxnId, Status.FAILURE);
    }

    private Integer getPaymentRefundTransactionId(List<PaymentRefundTransaction> refundTxnEntries,
            Set<Integer> processedRefundTxnIds) {
        for (PaymentRefundTransaction refundTxn : refundTxnEntries) {
            if (!processedRefundTxnIds.contains(refundTxn.getPaymentRefundTransactionId())) {
                return refundTxn.getPaymentRefundTransactionId();
            }
        }
        return null;
    }

    @Transactional
    public PaymentRequest create(PaymentRequest paymentRequest) {
        return pgWriteDAO.insertPaymentRequest(paymentRequest);
    }

    @Transactional
    public PaymentResponse create(PaymentResponse paymentResponse) {
        return pgWriteDAO.insertPaymentResponse(paymentResponse);
    }
    
    @Transactional
    public List<PaymentTransaction> getPaymentTxnWithResNull() {
        return pgReadDAO.getPaymentTxnWithResNull();
    }

    @Transactional
    public List<PaymentTypeOptions> getPaymentOptionTypeList(BaseBusinessDO baseBusinessDO) {
        return pgReadDAO.getPaymentOptionTypeList(baseBusinessDO);
    }

    @Transactional
    public List<PaymentTransaction> getPaymentTxnWithResNullWithTimeLimit(Date rangeFrom, Date rangeTo) {
        return pgRangeReadDAO.getPaymentTxnWithResNullWithTimeLimit(rangeFrom, rangeTo);
    }

    @Transactional
    public List<PaymentTransaction> getAllPaymentTxnWithTimeLimit(Date rangeFrom, Date rangeTo) {
        return pgRangeReadDAO.getAllPaymentTxnWithTimeLimit(rangeFrom, rangeTo);
    }

    @Transactional
    public List<Integer> getPaymentTxnIdsWithResNullWithTimeLimit(Date rangeFrom, Date rangeTo) {
        return pgRangeReadDAO.getPaymentTxnIdsWithResNullWithTimeLimit(rangeFrom, rangeTo);
    }

    @Transactional
    public List<PaymentTransaction> getPendingPaymentsInLastHourWindow() {
        return pgRangeReadDAO.getPendingPaymentsInLastHourWindow();
    }
    
    @Transactional
    public List<PaymentTransaction> getFailedPaymentsWithTimeLimit(Date rangeFromDate ,Date rangeToDate){
        return pgRangeReadDAO.getFailedPaymentsWithTimeLimit(rangeFromDate, rangeToDate);
    }

    @Transactional
    public List<PaymentTransaction> getPaymentTransactionByMerchantOrderId(String merchantOrderId) {
        return pgReadDAO.getPaymentTransactionByMerchantOrderId(merchantOrderId);
    }
    
    public PaymentTransaction getPaymentTransactionByMerchantOrderIdAndPG(String mtxnId, String pgUsed)
    {
        List<PaymentTransaction> paymentTransactions = getPaymentTransactionByMerchantOrderId(mtxnId);
        PaymentTransaction paymentTxn = null;
        if(paymentTransactions != null && !paymentTransactions.isEmpty()){
            for(int i=0; i<paymentTransactions.size(); i++){
                paymentTxn = paymentTransactions.get(i);
                if(paymentTxn.getPaymentGateway().equalsIgnoreCase(pgUsed)){
                    break;
                }
            }
        }
        return paymentTxn;
    }

    @Transactional
    public List<PaymentTransaction> getSuccessfulPaymentTransactionsByOrderIdforGivenPG(String orderId,
            String paymentGateway) {
        List<PaymentTransaction> paymentTransactionResultList = new ArrayList<PaymentTransaction>();
        List<PaymentTransaction> paymentTransactionsForOrder = getSuccessfulPaymentTransactionsByOrderId(orderId);
        for(PaymentTransaction paymentTransaction: paymentTransactionsForOrder) {
            if(paymentTransaction.getPaymentGateway().equals(paymentGateway)) {
                paymentTransactionResultList.add(paymentTransaction);
            }
        }
        return paymentTransactionResultList;
    }

    @Transactional
    public PaymentTransaction getSuccessfulPaymentTransactionByMerchantOrderIdforGivenPG(String merchantOrderId,
            String paymentGateway) {
        return pgReadDAO.getSuccessfulPaymentTransactionByMerchantOrderIdforGivenPG(merchantOrderId, paymentGateway);
    }
    
    @Transactional
    public List<PaymentTransaction> getSuccessfulPaymentTransactionByMerchantOrderId(String merchantOrderId) {
        return pgReadDAO.getSuccessfulPaymentTransactionByMerchantOrderId(merchantOrderId);
    }
    
    public PaymentPlanType getPaymentPlanType(String merchantOrderId, boolean isNewWalletUser) {
        List<PaymentTransaction> paymentTransactions = getSuccessfulPaymentTransactionByMerchantOrderId(merchantOrderId);
        return getPaymentPlanType(paymentTransactions, isNewWalletUser);
    }
    
    public PaymentPlanType getPaymentPlanType(List<PaymentTransaction> paymentTransactions, boolean isNewWalletUser) {
        if (isNewWalletUser) {
            boolean pgTxn = false;
            boolean walletTxn = false;
            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                if (paymentTransaction.isPGPayment()) {
                    pgTxn = true;
                } else if (paymentTransaction.isNewFCWalletPayment()) {
                    walletTxn = true;
                }
            }
            
            if (walletTxn && pgTxn) {
                return PaymentPlanType.PARTIAL_PAYMENT;
            } else if (walletTxn) {
                return PaymentPlanType.WALLET_PAYMENT;
            } else {
                return PaymentPlanType.FULL_PG_PAYMENT;
            }
        } else {
            Double pgAmount = new Double("0");
            Double walletAmount = new Double("0");
            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                if (paymentTransaction.isPGPayment()) {
                    pgAmount = paymentTransaction.getAmount();
                } else if (paymentTransaction.isFCCreditsPaymentTxn()) {
                    walletAmount = paymentTransaction.getAmount();
                }
            }
            
            if (pgAmount.compareTo(new Double("0")) == 0 && walletAmount.compareTo(new Double("0")) > 0) {
                return PaymentPlanType.WALLET_PAYMENT;
            } else if (pgAmount.compareTo(new Double("0")) >= 0 && walletAmount.compareTo(pgAmount) > 0) {
                return PaymentPlanType.PARTIAL_PAYMENT;
            } else {
                return PaymentPlanType.FULL_PG_PAYMENT;
            }
        }
    }
    
    

    public enum PaymentPlanType {
        FULL_PG_PAYMENT, PARTIAL_PAYMENT, WALLET_PAYMENT
    }
    
    public List<PaymentTransaction> getSuccessfulPaymentTransactionThroughPGByOrderId(String orderId) {
    	return pgReadDAO.getSuccessfulPaymentTransactionThroughPGByOrderId(orderId);
    }

    public PaymentTransaction getLatestPaymentTransactionThroughPGByOrderId(String orderId) {
    	return pgReadDAO.getLatestPaymentTransactionThroughPGByOrderId(orderId);  
    }

    @Transactional
    public PaymentTransaction getUniquePaymentTransactionByMerchantOrderId(String mtxnId) {
        List<PaymentTransaction> paymentTransaction = this.getPaymentTransactionByMerchantOrderId(mtxnId);

        if (paymentTransaction == null || paymentTransaction.isEmpty()) {
            logger.error("Something has gone wrong empty payment transaction for Order ID: " + mtxnId);
            return null;
        }

        PaymentTransaction uniquePaymentTransaction = paymentTransaction.get(0);
        return uniquePaymentTransaction;
    }
    
    @Transactional
    public PaymentTransaction getLastSuccessfulTransactionForUser(Integer userId) {
        return pgReadDAO.getLastSuccessfulPGTransactionForUser(userId);
    }

    public List<PaymentTransaction> getPaymentTransactionsByOrderId(String orderId) {
        return pgReadDAO.getPaymentTransactionsByOrderId(orderId);
    }

    public List<PaymentTransaction> getSuccessfulPaymentTransactionsByOrderId(String orderId) {
        return pgReadDAO.getSuccessfulPaymentTransactionsByOrderId(orderId);
    }

    public List<PaymentTransaction> getMasterSuccessfulPaymentTransactionsByOrderId(String orderId) {
        return pgWriteDAO.getSuccessfulPaymentTransactionsByOrderId(orderId);
    }

    public List<PaymentResponse> getPaymentResponseByResponseId(Integer responseId) {
        return pgReadDAO.getPaymentResponseByResponseId(responseId);
    }

    public List<PaymentTransaction> getSuccessfulPaymentTransactionsSentToPG(Collection<String> orderIds) {
        List<PaymentTransaction> paymentTransactions = new LinkedList<PaymentTransaction>();
        for (String orderId : orderIds) {
            paymentTransactions.add(this.getSuccessfulPaymentTransactionByOrderId(orderId));
        }

        return paymentTransactions;
    }

    public PaymentTransaction getLastPaymentTransactionSentToPG(String orderId) {
        return this.pgReadDAO.getLastPaymentTransactionSentToPG(orderId);
    }

    public List<PaymentTransaction> getAllPaymentTransactionSentToPG(String orderId) {
        return this.pgReadDAO.getAllPaymentTransactionSentToPG(orderId);
    }

    public List<PaymentTransaction> getAllPaymentTxSentToPGForEmail(String email) {
        return this.pgReadDAO.getAllPaymentTxSentToPGForEmail(email);
    }
    
    public List<PaymentTransaction> getAllPaymentTxSentToPGForEmailFromDate(String email, Timestamp fromDate, Timestamp toDate) {
        return this.pgReadDAO.getAllPaymentTxSentToPGForEmailFromDate(email, fromDate, toDate);
    }
    
    public List<PaymentTransaction> getAllPaymentTransactionByAcsOrder(String orderId) {
        return this.pgReadDAO.getAllPaymentTransactionByAcsOrder(orderId);
    }

    public List<PaymentTransaction> getLastPaymentTransactionsSentTogPG(Collection<String> orderIds) {
        List<PaymentTransaction> paymentTransactions = new LinkedList<PaymentTransaction>();

        for (String orderId : orderIds) {
            PaymentTransaction lastPaymentTransactionSentToPG = this.getLastPaymentTransactionSentToPG(orderId);
            if (lastPaymentTransactionSentToPG != null) {
                paymentTransactions.add(lastPaymentTransactionSentToPG);
            }
        }

        return paymentTransactions;
    }

    public List<PaymentTransaction> getLastPaymentTransactionsByMerchantOrderIds(Collection<String> merchantOrderIds) {
        List<PaymentTransaction> paymentTransactions = new LinkedList<PaymentTransaction>();
        for (String merchantOrderId : merchantOrderIds) {
            List<PaymentTransaction> paymentTransactionsLoc = this
                    .getPaymentTransactionByMerchantOrderId(merchantOrderId);
            if (paymentTransactionsLoc == null) {
                logger.error("No payment transaction found for merchant order id:" + merchantOrderId);
                continue;
            }
            for (int count = 0; count < paymentTransactionsLoc.size(); count++) {
                PaymentTransaction paymentTransaction = paymentTransactionsLoc.get(count);
                if (paymentTransaction.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)
                        && !paymentTransaction.getIsSuccessful()) {
                    continue;
                }
                if (paymentTransaction.getPaymentGateway().equals(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME)
                        && !paymentTransaction.getIsSuccessful()) {
                    continue;
                }
                paymentTransactions.add(paymentTransaction);
                break;
            }
        }
        return paymentTransactions;
    }

    public boolean isPaymentTransactionSuccessful(String orderId) {
        PaymentTransaction paymentTransactions = pgReadDAO.getSuccessfulPaymentTransactionByOrderId(orderId);
        if (paymentTransactions != null)
            return paymentTransactions.getSuccessful();

        return false;
    }

    /**
     * Given a {@link PaymentTransaction} sync it with data from PG. This is helpful in cases where date is out of sync
     * and PG txn ID is not to be found.
     * 
     * @param transactionToSync
     * @return
     * @throws Exception
     */
    public PaymentTransaction syncfromPGData(PaymentTransaction transactionToSync ,Boolean isFromAdminPanel) {
        String paymentGateway = transactionToSync.getPaymentGateway();
        IGatewayHandler paymentGatewayHandlerFactory = pgFactory.getPaymentHandler(paymentGateway);

        PaymentQueryBusinessDO paymentQueryBusinessDO = PaymentQueryBusinessDO.create(transactionToSync);
        
        try {
            long pgSyncStartTime = System.currentTimeMillis();
            paymentGatewayHandlerFactory.paymentStatus(paymentQueryBusinessDO, isFromAdminPanel);
            metricsClient.recordLatency("Payment", "PGSync."+paymentGateway, System.currentTimeMillis() - pgSyncStartTime);
        } catch (FCCreditWalletLimitExceededException e) {
            
            logger.error("Failed to credit to wallet as wallet credit limit exceeded for order id : "
                    + transactionToSync.getOrderId());
            try {
                ProductName productName = orderIdWriteDAO.getProductName(transactionToSync.getOrderId());
                if (productName.equals(ProductName.WalletCash)) {
                    metricsClient.recordEvent("Payment", "WalletCredit.AddCash", "FCCreditWalletLimitExceededException");
                    logger.info("Trying to do a refund to bank for add cash transaction where wallet limit reached. "
                            + transactionToSync.getOrderId());
                    PaymentRefundBusinessDO paymentRefundBusinessDO = paymentRefund.doRefundToBank(transactionToSync,
                            transactionToSync.getAmount(), true, "");
                    if (paymentRefundBusinessDO.getStatus()) {
                        metricsClient.recordEvent("Payment", "WalletCredit.AddCash.FCCreditWalletLimitExceededException", "BankRefundSuccess");
                        logger.info("Successfully processed refund to bank. " + transactionToSync.getOrderId());
                    } else {
                        metricsClient.recordEvent("Payment", "WalletCredit.AddCash.FCCreditWalletLimitExceededException", "BankRefundFailed");
                        logger.warn("Failed to refund to bank. " + transactionToSync.getOrderId());
                    }
                }
            } catch (Exception e1) {
                logger.error("Exception occured while trying to refund to bank. " + transactionToSync.getOrderId(), e1);
                throw e;
            }
        }
        

        transactionToSync.setIsSuccessful(paymentQueryBusinessDO.getPaymentStatus());
        Date currentDate = new Date();

        if (paymentQueryBusinessDO.getPgTransactionTime() == null) {
            transactionToSync.setRecievedToPg(currentDate);
            transactionToSync.setPgTransactionTime(currentDate);
        } else {
            transactionToSync.setRecievedToPg(paymentQueryBusinessDO.getPgTransactionTime());
            transactionToSync.setPgTransactionTime(paymentQueryBusinessDO.getPgTransactionTime());
        }

        if (paymentQueryBusinessDO.getPgTransactionId() != null
                && !paymentQueryBusinessDO.getPgTransactionId().trim().isEmpty()) {
            transactionToSync.setTransactionId(paymentQueryBusinessDO.getPgTransactionId());
        }

        return transactionToSync;
    }

    @Transactional
    public void syncfromPGData(String orderId) {
        List<PaymentTransaction> paymentTransactions = this.getAllPaymentTransactionByAcsOrder(orderId);

        if (paymentTransactions != null) {
            for (PaymentTransaction transactionToSync : paymentTransactions) {
                this.syncfromPGData(transactionToSync,false);
            }
        }
    }

    public PaymentTransaction getPaymentStatus(PaymentTransaction paymentTxnObj) throws Exception {
        String paymentGateway = paymentTxnObj.getPaymentGateway();
        IGatewayHandler paymentGatewayHandlerFactory = pgFactory.getPaymentHandler(paymentGateway);

        PaymentQueryBusinessDO paymentQueryBusinessDO = PaymentQueryBusinessDO.create(paymentTxnObj);

        long pgSyncStartTime = System.currentTimeMillis();
        paymentGatewayHandlerFactory.paymentStatus(paymentQueryBusinessDO, true);
        metricsClient.recordLatency("Payment", "PGSync."+paymentGateway, System.currentTimeMillis() - pgSyncStartTime);

        paymentTxnObj.setIsSuccessful(paymentQueryBusinessDO.getPaymentStatus());

        return paymentTxnObj;
    }

    public List<PaymentRefundTransaction> getPaymentRefundTransactions(Date from, Date to, boolean onlySuccesses) {
        List<PaymentRefundTransaction> paymentRefundTransactions = this.pgReadDAO.getPaymentRefundTransactions(from,
                to, onlySuccesses);

        for (PaymentRefundTransaction paymentRefundTransaction : paymentRefundTransactions) {
            PaymentTransaction paymentTransaction = new PaymentTransaction();
            paymentTransaction.setPaymentTxnId(paymentRefundTransaction.getPaymentTransactionId());
            List<PaymentTransaction> paymentTransactions = this.easyDBWriteDAO.get(paymentTransaction);

            paymentRefundTransaction.setPaymentTransaction(FCUtil.getFirstElement(paymentTransactions));
        }

        return paymentRefundTransactions;
    }

    public List<PaymentRefundTransaction> getPaymentRefundTransactions(String orderId) {
        PaymentTransaction paymentTransaction = new PaymentTransaction();
        paymentTransaction.setOrderId(orderId);

        List<PaymentTransaction> paymentTransactions = this.easyDBReadDAO.get(paymentTransaction);

        List<PaymentRefundTransaction> paymentRefundTransactions = new LinkedList<PaymentRefundTransaction>();
        for (PaymentTransaction transaction : paymentTransactions) {
            PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
            paymentRefundTransaction.setPaymentTransactionId(transaction.getPaymentTxnId());
            paymentRefundTransaction.setPaymentTransaction(transaction);

            List<PaymentRefundTransaction> paymentRefundTransactionsLocal = this.easyDBReadDAO
                    .get(paymentRefundTransaction);

            for (PaymentRefundTransaction refundTransaction : paymentRefundTransactionsLocal) {
                refundTransaction.setPaymentTransaction(transaction);
                paymentRefundTransactions.add(refundTransaction);
            }
        }

        return paymentRefundTransactions;
    }

    /**
     * This is essentially wrapper around pay using wallet. This API updates/creates appropriate data
     * (payment_txn/payment_rs/payment_rq). This method is meant to be used by those who want to debit from wallet but
     * don't want to go through all the redirection and related complexities.
     * 
     * @throws DuplicateRequestException
     */
    @Transactional(rollbackFor = DuplicateRequestException.class)
    private boolean makeWalletPayment(PaymentRequestBusinessDO paymentRequestBusinessDO,
            String merchantTransactionId, FundDestination fundDestination, String callerRef, String oneCheckIdentity) throws DuplicateRequestException {
        String paymentType;
        String paymentGateway;
        if(FCUtil.isNotEmpty(oneCheckIdentity)) {
            paymentType = PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET;
            paymentGateway = PaymentConstants.ONECHECK_FC_WALLET_PG_NAME;
        } else {
            paymentType = PaymentConstants.PAYMENT_TYPE_WALLET;
            paymentGateway = IGatewayHandler.PaymentGateways.FREECHARGE_WALLET.getName();
        }
        PaymentTransaction paymentTransaction = createPaymentRequestTRansaction(paymentRequestBusinessDO,
                merchantTransactionId, paymentType, paymentGateway);

        Users user = getUserForOrderID(paymentTransaction.getOrderId());

        String callerReference = paymentTransaction.getMerchantTxnId() + callerRef;

        WalletDebitRequest walletDebitRequest = new WalletDebitRequest();
        walletDebitRequest.setUserID(user.getUserId());
        walletDebitRequest.setTxnAmount(new Amount(paymentTransaction.getAmount()));
        walletDebitRequest.setMtxnId(merchantTransactionId);
        walletDebitRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_DEBIT);
        walletDebitRequest.setCallerReference(callerReference);
        walletDebitRequest.setFundDestination(fundDestination.toString());
        
        UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
        Map<String, Object> walletMetaDataMap = getWalletMetaMap(userTransactionHistory, paymentTransaction.getOrderId());
        String metaData = new Gson().toJson(walletMetaDataMap);
        walletDebitRequest.setMetadata(metaData);
        
        Map<String, Object> debitBalanceMap = new HashMap<>();
        debitBalanceMap.put(WalletWrapper.WALLET_DEBIT_REQUEST, walletDebitRequest);
        debitBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE,
                OneCheckWalletService.OneCheckTxnType.DEBIT_RECHARGE);
        debitBalanceMap.put(FCConstants.ORDER_ID, paymentRequestBusinessDO.getOrderId());
        String corpId = walletController.getCorpId(paymentRequestBusinessDO.getOrderId());
        if(corpId != null) {
        	debitBalanceMap.put(DESTINATION_CORP_ID, corpId);
        }
        if(FCUtil.isNotEmpty(oneCheckIdentity)) {
            debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
            debitBalanceMap.put(WalletWrapper.ONE_CHECK_IDENTITY, oneCheckIdentity);
        } else {
            debitBalanceMap.put(WalletWrapper.WALLET_TYPE, PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION);
        }
        Map<String, Object> responseMap = null;
        try {
            responseMap = walletWrapper.debitBalance(debitBalanceMap);
        } catch (OneCheckWalletException e) {
            logger.error("OneCheckWalletException : Failed to debit wallet for orderId " + paymentTransaction.getOrderId(), e);
        } catch (RuntimeException e) {
            logger.error("Caught exception on debiting wallet for orderId " + paymentTransaction.getOrderId(), e);
        }
        if (responseMap == null || !responseMap.containsKey(FCConstants.STATUS)) {
            logger.error("Failed to debit the wallet for order id " + paymentTransaction.getOrderId());
            return false;
        }
        
        String status = (String) responseMap.get(FCConstants.STATUS);
        DebitBalanceResponse debitBalanceResponse = (DebitBalanceResponse)responseMap.get(ONE_CHECK_WALLET_TRANSACTION_RESPONSE);
        if (debitBalanceResponse != null) {
        	paymentTransaction.setTransactionId(debitBalanceResponse.getTransactionId());
        }
        if(FCConstants.SUCCESS.equals(status)) {
            createSuccessfulWalletPaymentResponse(paymentTransaction, paymentType);
            walletService.notifyWalletPayment(paymentTransaction, "ocw", true);
            return true;
        } else {
            walletService.notifyWalletPayment(paymentTransaction, "ocw", false);
            logger.error("Failed to debit the wallet for order id " + paymentTransaction.getOrderId());
            return false;
        }
    }

    protected Users getUserForOrderID(String orderId) {
        CartBusinessDo cart = cartService.getCartByOrderId(orderId);
        com.freecharge.app.domain.entity.jdbc.Users users = userServiceProxy.getUserByUserId(cart.getUserId());
        Users user = new Users();
        user.setDateAdded(users.getDateAdded());
        user.setDob(users.getDob());
        user.setEmail(users.getEmail());
        user.setFkAffiliateProfileId(users.getFkAffiliateProfileId());
        user.setFkRefferredByUserId(users.getFkRefferredByUserId());
        user.setForgotPasswordExpiry(users.getForgotPasswordExpiry());
        user.setForgotPasswordKey(users.getForgotPasswordKey());
        user.setIsActive(users.getIsActive());
        user.setIsLoggedin(users.getIsLoggedin());
        user.setLastLoggedin(users.getLastLoggedin());
        user.setLastUpdate(users.getLastUpdate());
        user.setMobileNo(users.getMobileNo());
        user.setMorf(users.getMorf());
        user.setPassword(users.getPassword());
        user.setType(users.getType());
        user.setUserId(users.getUserId());
        return user;
    }

    public void createSuccessfulWalletPaymentResponse(PaymentTransaction paymentTransaction) {
        createSuccessfulWalletPaymentResponse(paymentTransaction, null);
    }
    
    public void updateUPIPaymentDetailsInDb(PaymentTransaction paymentTransaction, String response) {
        PaymentResponse paymentResponse = new PaymentResponse();

        paymentResponse.setAmount(paymentTransaction.getAmount());
        paymentResponse.setIsSuccessful(paymentTransaction.getIsSuccessful());
        paymentResponse.setOrderId(paymentTransaction.getOrderId());
        paymentResponse.setMtxnId(paymentTransaction.getMerchantTxnId());
        paymentResponse.setResponseData(response);
        paymentResponse.setPaymentGateway(PaymentGateways.KLICK_PAY.getName());
        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));

        paymentResponse = this.create(paymentResponse);

        paymentTransaction.setRecievedToPg(Calendar.getInstance().getTime());
        paymentTransaction.setPgTransactionTime(new Date());

        updatePaymentTransaction(paymentTransaction);
        
    }

    @Transactional
    public void createSuccessfulWalletPaymentResponse(PaymentTransaction paymentTransaction, String paymentGateway) {
        PaymentResponse paymentResponse = new PaymentResponse();

        paymentResponse.setAmount(paymentTransaction.getAmount());
        paymentResponse.setIsSuccessful(true);
        paymentResponse.setOrderId(paymentTransaction.getOrderId());
        paymentResponse.setMtxnId(paymentTransaction.getMerchantTxnId());
        if(FCUtil.isEmpty(paymentGateway)) {
            paymentGateway = PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE;
        }
        paymentResponse.setPaymentGateway(paymentGateway);
        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));

        paymentResponse = this.create(paymentResponse);

        paymentTransaction.setIsSuccessful(true);
        paymentTransaction.setRecievedToPg(Calendar.getInstance().getTime());
        paymentTransaction.setPgTransactionTime(new Date());

        updatePaymentTransaction(paymentTransaction);
    }

    public PaymentTransaction createPaymentRequestTRansaction(PaymentRequestBusinessDO paymentRequestBusinessDO,
                                                              String walletTransactionReference, String paymentType,
                                                              String paymentGateway) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        paymentRequest.setMtxnId(walletTransactionReference);
        paymentRequest.setOrderId(paymentRequestBusinessDO.getOrderId());
        paymentRequest.setPaymentGateway(paymentGateway);
        paymentRequest.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        paymentRequest.setPaymentType(paymentType);
        paymentRequest.setRequestToPp("WalletRequest");
        paymentRequest.setRequestToGateway("WalletRequest");

        paymentRequest = this.create(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        pt.setMerchantTxnId(walletTransactionReference);
        pt.setOrderId(paymentRequestBusinessDO.getOrderId());
        pt.setPaymentGateway(paymentGateway);
        pt.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(Integer.parseInt(paymentType));
        pt.setSetToPg(Calendar.getInstance().getTime());

        return this.create(pt);
    }

    public PaymentRefundBusinessDO createPaymentRefundBusinessDO(PaymentTransaction paymentTransaction) {
        PaymentRefundBusinessDO paymentRefundBusinessDO = new PaymentRefundBusinessDO();
        PaymentTransactionBusinessDO paymentTransactionBusinessDO = new PaymentTransactionBusinessDO();
        paymentTransactionBusinessDO.setMerchantTxnId(paymentTransaction.getMerchantTxnId());
        paymentTransactionBusinessDO.setPaymentOption(paymentTransaction.getPaymentOption());
        paymentTransactionBusinessDO.setPaymentType(paymentTransaction.getPaymentType());
        paymentTransactionBusinessDO.setTransactionId(paymentTransaction.getTransactionId());
        paymentTransactionBusinessDO.setPaymentTransactionId(paymentTransaction.getPaymentTxnId());
        paymentTransactionBusinessDO.setPaymentGateway(paymentTransaction.getPaymentGateway());

        paymentRefundBusinessDO.setLookupID(orderIdWriteDAO.getLookupIdForOrderId(paymentTransaction.getOrderId()));

        Date receivedToPg = getTransactionDate(paymentTransaction);

        paymentTransactionBusinessDO.setRecievedToPg(receivedToPg);
        paymentRefundBusinessDO.setPaymentTransactionBusinessDO(paymentTransactionBusinessDO);

        paymentRefundBusinessDO.setOrderId(paymentTransaction.getOrderId());

        Double amount = paymentTransaction.getAmount();
        paymentRefundBusinessDO.setAmount(amount);

        return paymentRefundBusinessDO;
    }

    private Date getTransactionDate(PaymentTransaction paymentTransaction) {
        Date recievedToPg;

        if (paymentTransaction.getPgTransactionTime() != null) {
            recievedToPg = paymentTransaction.getPgTransactionTime();
        } else if (paymentTransaction.getRecievedToPg() != null) {
            recievedToPg = paymentTransaction.getRecievedToPg();
        } else {
            recievedToPg = paymentTransaction.getSetToPg();
        }

        if (recievedToPg == null) {
            recievedToPg = paymentTransaction.getSetToPg();
        }
        return recievedToPg;
    }

    /**
     * Handle various post payment success tasks. This could be better handled but for now it's just a series of method
     * calls.
     * 
     * @param paymentTransaction
     */
    public void handlePostPaymentSuccessTasks(PaymentTransaction paymentTransaction,
            Boolean isPaymentStatusForAdminPanel) {
        if (paymentTransaction.getIsSuccessful()) {
            // Adding payment gateway to the record latency
            metricsClient.recordLatency("UserSession", "PaySuccess."+paymentTransaction.getPaymentGateway(), paymentTransaction.getOrderId(),
                    MetricsClient.EventName.PayAttempt);
            metricsClient.timeStampEvent(paymentTransaction.getOrderId(), MetricsClient.EventName.PaySuccess);
            if (!isPaymentStatusForAdminPanel) {
                creditPGTransactionToWallet(paymentTransaction);
                publishPaymentSuccessEvent(paymentTransaction);
            }
        } else {
            publishPaymentFailureEvent(paymentTransaction);
            logger.info("Payment has failed so no wallet credit for Merchant Txn ID: ["
                    + paymentTransaction.getMerchantTxnId() + "]");
        }
    }

    private void publishPaymentFailureEvent(PaymentTransaction paymentTransaction) {
        PaymentFailureEvent pfe = new PaymentFailureEvent(this);
        pfe.setOrderId(paymentTransaction.getOrderId());
        logger.debug("Publishing payment failed event for order Id : " + paymentTransaction.getOrderId());
        freechargeEventPublisher.publish(pfe);
    }

    public void publishPaymentSuccessEvent(PaymentTransaction paymentTransaction) {
        /* Publish a payment success event */
        PaymentSuccessEvent paymentSuccessEvent = new PaymentSuccessEvent(this);
        String orderId = paymentTransaction.getOrderId();
        String sessionId;
        List<OrderId> orderIdList = orderIdWriteDAO.getByOrderId(orderId);
        if (orderIdList != null && !orderIdList.isEmpty()) {
            sessionId = orderIdList.get(0).getSessionId();
            paymentSuccessEvent.setSessionId(sessionId);
        }
        paymentSuccessEvent.setOrderId(orderId);
        logger.debug("Publishing payment success event for order Id : " + paymentTransaction.getOrderId());
        freechargeEventPublisher.publish(paymentSuccessEvent);
        /* Done publish of payment success event */
    }

    /**
     * Given a {@link PaymentTransaction} credit the amount to the customer's {@link Wallet}, precondition being that
     * the original payment has succeeded. This method also ensures that wallet is credited with right fund source. For
     * now there are two fund sources
     * <ul>
     * <li><b> Recharge Fund: </b> When user is recharging his mobile.</li>
     * <li><b> Wallet Fund: </b> When user is funding his wallet.</li>
     * </ul>
     * 
     * @param paymentTransaction
     */
    private void creditPGTransactionToWallet(PaymentTransaction paymentTransaction) {
        Integer paymentType = paymentTransaction.getPaymentType();
        String orderId = paymentTransaction.getOrderId();
        if (isTransactionThroughWallet(paymentType)) {
            String errorString = "Trying to credit wallet using wallet payment gateway for orderID: ["
                    + orderId + "]";

            logger.error(errorString);
            throw new IllegalArgumentException(errorString);
        }

        if (paymentTransaction.getIsSuccessful()) {
            logger.info("Payment succeeded for Merchant Txn ID: [" + paymentTransaction.getMerchantTxnId()
                    + "]. Going ahead with wallet credit");

            CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderId);
            ProductName primaryProduct = cartService.getPrimaryProduct(cartToInspect);

            String callerReference;
            String fundSource;

            Users user = getUserForOrderID(orderId);
            if (primaryProduct == ProductMaster.ProductName.WalletCash) {
                logger.info("Going ahead with wallet credit for mtxnId : " + paymentTransaction.getMerchantTxnId());
                callerReference = paymentTransaction.getMerchantTxnId() + PaymentConstants.WALLET_FUND;
                fundSource = Wallet.FundSource.CASH_FUND.toString();
            } else {
                if (user != null && oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail()) != null) {
                    logger.info("This is a oneCheck customer. So not going ahead with wallet credit for orderId " + orderId);
                    return;
                }
                callerReference = paymentTransaction.getMerchantTxnId()
                        + PaymentConstants.PAYMENT_SUCCESS_CREDIT_TO_WALLET;
                fundSource = Wallet.FundSource.RECHARGE_FUND.toString();
            }

            WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
            walletCreditRequest.setUserID(user.getUserId());
            walletCreditRequest.setTxnAmount(new Amount(paymentTransaction.getAmount()));
            walletCreditRequest.setMtxnId(paymentTransaction.getMerchantTxnId());
            walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
            walletCreditRequest.setCallerReference(callerReference);
            walletCreditRequest.setFundSource(fundSource);
            
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(orderId);
            Map<String, Object> walletMetaDataMap = getWalletMetaMap(userTransactionHistory, orderId);
            String metaData = new Gson().toJson(walletMetaDataMap);
            walletCreditRequest.setMetadata(metaData);
            
            // Adding a wallet credit retry logic in case of a LockAcquisitionException
            try {
                Map<String, Object> creditBalanceMap = new HashMap<>();
                creditBalanceMap.put(WalletWrapper.WALLET_CREDIT_REQUEST, walletCreditRequest);
                creditBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.CREDIT_RECHARGE);
                if (primaryProduct == ProductMaster.ProductName.WalletCash) {
                    creditBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.CREDIT_WALLET_LOAD);
                }
                creditBalanceMap.put(FCConstants.ORDER_ID, orderId);
                Map<String, Object> responseMap = walletWrapper
                        .creditGeneralBalanceToUser(creditBalanceMap);
            } catch (LockAcquisitionException e) {
                metricsClient.recordEvent("Payment", "WalletPGCredit", "LockAcquisitionException");
                logger.error("LockAcquisitionException on trying to credit to wallet. Retrying wallet credit...", e);
                walletService.credit(walletCreditRequest);
            } catch (Exception e) {
            	logger.error("Exception while crediting user wallet for mtxnid : " + paymentTransaction.getMerchantTxnId(), e);
            	throw e;
            }
            logger.info("Successfully credited wallet for Merchant Txn ID: [" + paymentTransaction.getMerchantTxnId()
                    + "].");
            
        } else {
            logger.info("Payment has failed so no wallet credit for Merchant Txn ID: ["
                    + paymentTransaction.getMerchantTxnId() + "]");
        }
    }

    public Map<String, Object> getWalletMetaMap(UserTransactionHistory userTransactionHistory, String orderId) {
        Map<String, Object> walletMetaMap = new HashMap<>();
        if (userTransactionHistory != null) {
            walletMetaMap.put("operatorName", userTransactionHistory.getServiceProvider());
            if (!FCUtil.isEmpty(userTransactionHistory.getProductType())) {
                walletMetaMap.put("product",
                        ProductName.fromPrimaryProductType(userTransactionHistory.getProductType()).name());
            }
            walletMetaMap.put("serviceNumber", userTransactionHistory.getSubscriberIdentificationNumber());
        } else {
            logger.error("UTH is null for orderId, " + orderId);
        }
        return walletMetaMap;
    }

    public boolean isTransactionThroughWallet(int paymentType) {
        return Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET) == paymentType ||
                Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET)== paymentType;
    }

    @Transactional
    public void cashbackToWallet(PaymentTransaction paymentTransaction, Double amount, String reference,
            String cashBackSourceId) {
        logger.info("Cashback succeeded for Merchant Txn ID: [" + paymentTransaction.getMerchantTxnId()
                + "]. Going ahead with wallet credit");

        WalletCreditRequest walletCreditRequest = new WalletCreditRequest();

        Users user = getUserForOrderID(paymentTransaction.getOrderId());
        walletCreditRequest.setUserID(user.getUserId());

        walletCreditRequest.setMtxnId(paymentTransaction.getMerchantTxnId());
        walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);

        walletCreditRequest.setCallerReference(reference);

        walletCreditRequest.setFundSource(Wallet.FundSource.CASHBACK.toString());
        walletCreditRequest.setTxnAmount(new Amount(amount));

        String walletTransactionId = walletService.credit(walletCreditRequest);
        Long fkWalletTransactionId = Long.parseLong(walletTransactionId);
        Long fkItemId = Long.parseLong(cashBackSourceId);
        walletService.insertWalletItem(fkWalletTransactionId, fkItemId, Wallet.FundSource.CASHBACK.toString());

        logger.info("Successfully done cashback to wallet for Merchant Txn ID: ["
                + paymentTransaction.getMerchantTxnId() + "].");
    }

    public static String getBeanName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, PaymentTransactionService.class.getSimpleName());
    }

    /**
     * Returns true if *at least* one payment transaction has succeeded for a given order ID.
     * 
     * @param orderId
     */
    @Transactional
    public boolean hasPaymentSucceeded(String orderId) {
        List<PaymentTransaction> paymentTransactionList = this.getMasterSuccessfulPaymentTransactionsByOrderId(orderId);

        boolean hasPaymentSucceeded = false;

        if (paymentTransactionList != null) {
            for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                if (paymentTransaction.getIsSuccessful()) {
                    hasPaymentSucceeded = true;
                    break;
                }
            }
        }

        return hasPaymentSucceeded;
    }

    public void assertPaymentSuccessAndDebitWallet(String orderId, FundDestination hcoupon, String callerReference) throws DuplicateRequestException {
        String lookupId = orderIdWriteDAO.getLookupIdForOrderId(orderId);

        CartBusinessDo cartBusinessDo = cartService.getCartWithNoExpiryCheck(lookupId);
        Double expectedPayAmount = pricingService.getPayableAmount(cartBusinessDo);

        if (!this.hasPaymentSucceeded(orderId)) {
            logger.info("Looks like payment has failed; trying to sync with PG for orderID: [" + orderId + "]");
            this.syncfromPGData(orderId);
        }

        if (!this.hasPaymentSucceeded(orderId)) {
            String errorMessage = String.format("User for order id %s is trying to bypass pg", orderId);
            logger.info(errorMessage);
            throw new IllegalStateException(errorMessage);
        }

        PaymentTransaction paymentTransaction = this.getSuccessfulPaymentTransactionByOrderId(orderId);
        
        PaymentPlan paymentPlan = null;
        try {
            paymentPlan = paymentPlanService.getPaymentPlan(orderId);

            logger.info("Payment plan: " + paymentPlan);

            Amount totalPaidAmount = new Amount(paymentPlan.getPgAmount().getAmount()
                    .add(paymentPlan.getWalletAmount().getAmount()));

            if (expectedPayAmount.doubleValue() != totalPaidAmount.getAmount().doubleValue()) {
                String errorMessage = "Payment plan amount does not match with expected payable amount. Payment Plan : "
                        + paymentPlan + ". Payable amount : " + expectedPayAmount;
                logger.info(errorMessage);
                throw new IllegalStateException(errorMessage);
            }

            if (paymentPlan.getPgAmount().getAmount().doubleValue() != paymentTransaction.getAmount().doubleValue()) {
                String errorMessage = String.format(
                        "User for order id %s is trying to bypass pg using different amount ", orderId
                                + " expected amount " + paymentPlan.getPgAmount().getAmount() + "Payment Txn amount "
                                + paymentTransaction.getAmount());
                logger.info(errorMessage);
                throw new IllegalStateException(errorMessage);
            }
            
        } catch (PaymentPlanNotFoundException ppnfe) {
            logger.info("Payment plan not found for order ID : [" + orderId + "]");

            if (expectedPayAmount.doubleValue() != paymentTransaction.getAmount().doubleValue()) {
                String errorMessage = String.format(
                        "User for order id %s is trying to bypass pg using different amount ", orderId
                                + " expected amount " + expectedPayAmount.doubleValue() + "Payment Txn amount "
                                + paymentTransaction.getAmount());
                logger.info(errorMessage);
                throw new IllegalStateException(errorMessage);
            }
        }

        Users user = getUserForOrderID(orderId);
        String email = user.getEmail();
        int transactionSize = 0;
        String oneCheckIdentity = null;
        if (null != (oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email))) {
            // For oneCheck customer we assume that the wallet is not credited for a successful payment
            transactionSize = 0;
        } else {
            List<WalletTransaction> transactionResult = walletService.findTransaction(
                    paymentTransaction.getMerchantTxnId(), TransactionType.DEPOSIT);

            transactionSize = transactionResult.size();
        }
        
        if (!(transactionSize > 0)) {
            logger.info("Wallet was *not* credited for order ID : [ " + orderId
                    + " ]. Possible cases : 1) Wallet/Zero Payment, 2) OneCheck Customer");

            /*
             * Double check to make sure that wallet has been debited for this
             * request.
             */
            if (!(paymentTransaction.getPaymentGateway().equalsIgnoreCase(PaymentConstants.ZEROPAY_PAYMENT_OPTION) && expectedPayAmount
                    .doubleValue() == 0.0)) {

                int debitTransactionListSize = 0;
                int paymentType = paymentTransaction.getPaymentType();
                if (isTransactionThroughWallet(paymentType)) {
                    if (FCUtil.isNotEmpty(oneCheckIdentity)) {
                        logger.info("Wallet Payment is successful (as per payment_txn) for orderId " + orderId);
                        debitTransactionListSize = 1;
                    } else {
                        List<WalletTransaction> debitTransactionList = walletService.findTransaction(
                                paymentTransaction.getMerchantTxnId(), TransactionType.WITHDRAWAL);

                        debitTransactionListSize = debitTransactionList.size();
                    }
                    if (!(debitTransactionListSize > 0)) {
                        String errorMessage = "Wallet not debited even though payment gateway is wallet for order ID: "
                                + orderId + "]";
                        logger.error(errorMessage);
                        throw new IllegalStateException(errorMessage);
                    }
                } else {
                    if (oneCheckIdentity != null) {
                        if (paymentPlan.getWalletAmount().compareTo(Amount.ZERO) > 0) {
                            logger.info("Seems like this is a partial payment for onecheck user " + oneCheckIdentity
                                    + " and orderId " + orderId + ". So debiting the amount ("
                                    + paymentPlan.getWalletAmount() + ") from wallet");
                            List<PaymentTransaction> walletPaymentList =
                                    getSuccessfulPaymentTransactionsByOrderIdforGivenPG(
                                            orderId, PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
                            PaymentRequestBusinessDO walletPayDO = new PaymentRequestBusinessDO();
                            walletPayDO.setAmount(Double.toString(paymentPlan.getWalletAmount().getAmount()
                                    .doubleValue()));
                            walletPayDO.setOrderId(paymentTransaction.getOrderId());
                            if (!scheduleService.hasScheduleExecuted(paymentTransaction.getOrderId())) {
                                boolean isWalletDebited = false;
                                if( null != walletPaymentList && !walletPaymentList.isEmpty()) {
                                    for(PaymentTransaction walletPayment : walletPaymentList) {
                                        if ( null != walletPayment && walletPayment.getAmount().doubleValue() ==
                                                paymentPlan.getWalletAmount().getAmount().doubleValue())
                                            isWalletDebited = true;
                                            break;
                                    }
                                }
                                if(!isWalletDebited) {
                                    isWalletDebited = this.makeWalletPayment(walletPayDO,
                                            paymentTransaction.getMerchantTxnId(), hcoupon, callerReference,
                                            oneCheckIdentity);
                                }
                                if (!isWalletDebited) {
                                    logger.info("Need to refund the PG paid amount for order " + orderId);
                                    kestrelWrapper.enqueueUPUpdate(orderId);
                                    String errorMessage = "Wallet debit failed for partial payment (onecheck user) " + orderId;
                                    logger.error(errorMessage);
                                    throw new RuntimeException(errorMessage);
                                }
                            } else {
                                logger.info("Part of schedule so going ahead with rest of recharge flow for orderID: ["
                                        + paymentTransaction.getOrderId() + "]");
                            }
                        } else {
                            logger.info("Wallet has not been credited for successful payment. So, safe to go ahead with *recharge/fulfilment* without wallet debit for orderID "
                                    + orderId);
                        }
                    } else {
                        String errorMessage = "Non oneCheck user is having no wallet credited for order " + orderId;
                        logger.error(errorMessage);
                        throw new IllegalStateException(errorMessage);
                    }
                }
            }
        } else {
            logger.info("Debiting amount : [" + expectedPayAmount + "] from wallet for Order ID : [ " + orderId
                    + "]");

            PaymentRequestBusinessDO walletPayDO = new PaymentRequestBusinessDO();
            walletPayDO.setAmount(Double.toString(expectedPayAmount));
            walletPayDO.setOrderId(paymentTransaction.getOrderId());

            if (!scheduleService.hasScheduleExecuted(paymentTransaction.getOrderId())) {
                boolean isWalletDebited = this.makeWalletPayment(walletPayDO, paymentTransaction.getMerchantTxnId(), hcoupon,
                        callerReference, oneCheckIdentity);
                if (!isWalletDebited) {
                    logger.info("Need to refund the PG paid amount for order " + orderId);
                    kestrelWrapper.enqueueUPUpdate(orderId);
                    String errorMessage = "Wallet debit failed for partial payment (non onecheck user) " + orderId;
                    logger.error(errorMessage);
                    throw new RuntimeException(errorMessage);
                }
            } else {
                logger.info("Part of schedule so going ahead with rest of recharge flow for orderID: ["
                        + paymentTransaction.getOrderId() + "]");
            }
        }
        //calling doPromocodeFullfilment here so that promocode fulfillment happens only in case the user's wallet gets debited successfully.
        PaymentTransaction walletTransaction = getLatestSuccessfulPayment(orderId);
        promocodeService.doPromocodeFullfilment(orderId,walletTransaction, paymentPlan, paymentTransaction);
    }

    /**
     * Checks whether an order ID can be recharged. It depends on the product type when was the payment attempted etc.,
     * 
     * @param orderIdo
     * @param originalPaymentState
     * @param payAttemptTime
     * @return
     */
    public Boolean canBeEnqueuedForRecharge(PaymentTransaction updatedTransaction) {
        ProductName productName = orderIdWriteDAO.getProductName(updatedTransaction.getOrderId());
        
        if (!appConfig.isMissedRechargeAttemptEnabled()) {
            return false;
        }
        
        if (productName == ProductName.HCoupons) {
        	return false;
        }
        

        if (!updatedTransaction.getIsSuccessful()) {
            return false;
        }

        /*
         * For now enqueue only prepaid, dth and data card.
         */
        if (productListForRecharge.contains(productName)) {
            DateTime now = new DateTime();
            DateTime payAttemptTime = new DateTime(updatedTransaction.getSetToPg());
            DateTime rechargeAttemptExpiryTime = payAttemptTime.plusHours(RECHARGE_ATTEMPT_WINDOW);

            logger.info("Attempt Time: [" + payAttemptTime + "] Attemnpt Expiry: [" + rechargeAttemptExpiryTime + "]");

            /*
             * We'll attempt recharge only if pay was attempted in last 3hrs time.
             */
            if (now.isBefore(rechargeAttemptExpiryTime)) {

                InTransactionData inTransaction = inService.getInTransactionData(updatedTransaction.getOrderId());

                if (inTransaction == null) {
                    List<PaymentRefundTransaction> refunds = this.getPaymentRefundTransactions(updatedTransaction
                            .getOrderId());
                    if (refunds != null && refunds.size() > 0) {
                        logger.info("Refund done already. So not going ahead with recharge for " + updatedTransaction.getOrderId());
                        return false;
                    }
                    logger.info("No recharge attempt yet for Order ID: [" + updatedTransaction.getOrderId() + "]");
                    return true;
                }
            }
        }

        return false;
    }
    
    
    /**
     * Checks whether an order ID can be recharged. It depends on the product type when was the payment attempted etc.,
     * 
     * @param orderId
     * @param originalPaymentState
     * @param payAttemptTime
     * @return
     */
    public Boolean canBeEnqueuedForHCouponFulfilment(PaymentTransaction updatedTransaction) {
        ProductName productName = orderIdWriteDAO.getProductName(updatedTransaction.getOrderId());
        
        if (productName != ProductName.HCoupons) {
        	return false;
        }

        if (!updatedTransaction.getIsSuccessful()) {
            return false;
        }

        /*
         * For now enqueue only prepaid, dth and data card.
         */
        if (productName == ProductName.HCoupons) {
            DateTime now = new DateTime();
            DateTime payAttemptTime = new DateTime(updatedTransaction.getSetToPg());
            DateTime hcouponAttemptExpiryTime = payAttemptTime.plusHours(RECHARGE_ATTEMPT_WINDOW);

            logger.info("Attempt Time: [" + payAttemptTime + "] Attemnpt Expiry: [" + hcouponAttemptExpiryTime + "]");

            /*
             * We'll attempt recharge only if pay was attempted in last 6hrs time.
             */
            if (now.isBefore(hcouponAttemptExpiryTime)) {

            	UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(updatedTransaction.getOrderId());

                if (userTransactionHistory == null) {
                    logger.info("No Hcoupon fulfilment yet for Order ID: ["
                            + String.valueOf(updatedTransaction.getOrderId()) + "]");
                    return true;
                }
            }
        }

        return false;
    }

    public PaymentTransaction getLatestSuccessfulPayment(String orderId) {
        List<PaymentTransaction> successfulPtList = this.getSuccessfulPaymentTransactionsByOrderId(orderId);
        
        if (successfulPtList == null || successfulPtList.isEmpty()) {
            return null;
        }
        return successfulPtList.get(0);
    }
    
    /**
     * Returns the correct mtxn ID that is to be refunded in case
     * of recharge/bill-pay failures. This function is needed since
     * for every payment has two entries in payment_txn table and 
     * getting the right mtxn ID is tricky.
     * @param orderId
     * @return
     */
    public String getMtxnIdToRefund(String orderId) {
        List<PaymentTransaction> paymentTransactions = this.getSuccessfulPaymentTransactionsByOrderId(orderId);
        
        if (paymentTransactions != null) {
            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                if (paymentTransaction.getIsSuccessful()) {
                    return paymentTransaction.getMerchantTxnId();
                }
            }
        }
        
        throw new IllegalStateException("Could not find eligible mtxn ID to refund against Order ID: [" + orderId + "]");
    }
    
    public String getChannelFromOrderId(String orderId) {
        if (orderId != null && orderId.length() >= 4) {
            return String.valueOf(orderId.charAt(3));
        }
        return null;
    }

	public String getMainOrderIdForMerchantId(String merchantTxnId) {
		if (merchantTxnId != null) {
			List<PaymentTransaction> paymentTxnList = this
					.getPaymentTransactionByMerchantOrderId(merchantTxnId);
			if (paymentTxnList != null && paymentTxnList.size() > 0) {
				return paymentTxnList.get(0).getOrderId();
			}
		}
		return null;
	}
	
	/*Getting PG type*/
	public Map<String, String> getMtxnIdPGTypeMap(String orderId) {
		Map<String, String> mtxnPgTypeMap = new HashMap<>();
		List<PaymentTransaction> allPaymentTransaction = getAllPaymentTransactionSentToPG(orderId);
		if(!FCUtil.isEmpty(allPaymentTransaction)) {
			for (PaymentTransaction paymentTransaction : allPaymentTransaction) {
				if (mtxnPgTypeMap.containsKey(paymentTransaction.getMerchantTxnId()) || "fw".equals(paymentTransaction.getPaymentGateway())) {
					continue;
				}
				String mtxnId = paymentTransaction.getMerchantTxnId();
				mtxnPgTypeMap.put(mtxnId, kpayPaymentMetaService.getPgUsed(mtxnId));
			}
		}
		return mtxnPgTypeMap;
	}

    public List<PaymentRefundTransaction> getRefundTxnForKPay(PaymentTransaction paymentTransaction) {
        List<PaymentRefundTransaction> paymentRefundTransactions = new ArrayList<>();
        PaymentRefundBusinessDO paymentRefundBusinessDO = createPaymentRefundBusinessDO(paymentTransaction);
        Map<String, Object> refundStatusMap = klickPayGatewayHandler.getSuccessfulPgRefundData(paymentRefundBusinessDO);
        if (!refundStatusMap.containsKey("refundAmount")) {
            return paymentRefundTransactions;
        }
        Double totalPgRefundAmount = (Double) refundStatusMap.get("refundAmount");
        Date refundedOn = (Date) refundStatusMap.get("refundedOn");
        if (totalPgRefundAmount > 0) {
            paymentRefundTransactions.add(createPaymentPGRefund(paymentTransaction, totalPgRefundAmount, refundedOn));
        }
        return paymentRefundTransactions;
    }
    
    private PaymentRefundTransaction createPaymentPGRefund(PaymentTransaction successPayTxn, Double totalPgRefundAmount, Date refundedOn) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        Amount refundedAmount = new Amount(totalPgRefundAmount);
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(successPayTxn.getPaymentTxnId());
        paymentRefundTransaction.setRawPGRequest("PG Refund");
        paymentRefundTransaction.setSentToPG(refundedOn);
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setPaymentTransaction(successPayTxn);
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setReceivedFromPG(refundedOn);
        paymentRefundTransaction.setRawPGResponse("KPay PG Refund Success");
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        return paymentRefundTransaction;
    }
    
    public void insertPaymentFulfilmentMap(String orderId, String mtxnId) throws DuplicateRequestException {
        PaymentFulfilmentMapDO paymentFulfilmentMapDO = new PaymentFulfilmentMapDO();
        paymentFulfilmentMapDO.setOrderId(orderId);
        paymentFulfilmentMapDO.setMtxnId(mtxnId);
        int updatedRows = 0;
        try {
            updatedRows = paymentFulfilmentMapDAO.insertPaymentFulfilmentMap(paymentFulfilmentMapDO);
        } catch (Exception e) {
            logger.info("Failed to insert data to payment_fulfilment_map table for " + orderId, e);
        }
        
        if (updatedRows == 0) {
            logger.warn("Throwing DuplicateRechargeRequestException for " + orderId);
            throw new DuplicateRequestException(orderId);
        }
    }

    /* This is a temporary implementation, this will call refundStatus() API of new refund tool, eventually*/
    public Boolean isMtxnRefunded(String mtxnId) {
        Double totalRefundedAmount = new Double(FCConstants.ZERO);
        List<PaymentTransaction> paymentTransactionList = getSuccessfulPaymentTransactionByMerchantOrderId(mtxnId);
        Double totalPaidAmount = new Double(FCConstants.ZERO);
        if( null != paymentTransactionList && !paymentTransactionList.isEmpty()) {
            totalPaidAmount = FCUtil.calculateTotalPaid(paymentTransactionList);
            if(totalPaidAmount <= FCConstants.ZERO) {
                logger.info("Paid amount is 0 for mtxnId: "+mtxnId);
                return false;
            }
        }
        totalRefundedAmount = getAlreadyRefundedAmountForMtxnId(mtxnId);
        logger.info("Total paid amount = "+totalPaidAmount+", total refunded amount = "+totalRefundedAmount+
                "for mtxnId: "+mtxnId);
        if(totalPaidAmount.equals(totalRefundedAmount) || totalRefundedAmount > 0) {
            return true;
        }
        return false;
    }

    public Double getAlreadyRefundedAmountForMtxnId(String mtxnId) {
        Double totalRefundedAmount = new Double(FCConstants.ZERO);
        List<PaymentRefundTransaction> refundTransactionList = pgReadDAO.getSuccessfulRefundsForMtxnId(mtxnId);
        if(null != refundTransactionList && !refundTransactionList.isEmpty()) {
            totalRefundedAmount = FCUtil.calculateTotalRefunds(refundTransactionList);
        }
        return totalRefundedAmount;
    }
    
    public String getPaymentFulfilmentMtxnFromOrder(String orderId) {
        try {
            return paymentFulfilmentMapDAO.getMtxnFromOrder(orderId);
        } catch (Exception e) {
            logger.info("Failed to get payment_fulfilment_map table for for order " + orderId, e);
        }
        return null;
    }

    public String getPaymentFulfilmentOrderFromMtxn(String mtxnId) {
        try {
            return paymentFulfilmentMapDAO.getOrderFromMtxn(mtxnId);
        } catch (Exception e) {
            logger.info("Failed to get payment_fulfilment_map table for mtxnId " + mtxnId, e);
        }
        return null;
    }
    
    @Transactional(readOnly = true)
    public String getOrderIdFromMtxn(String mtxnId) {
        List<PaymentTransaction> paymentTransactions = getPaymentTransactionByMerchantOrderId(mtxnId);

        if (paymentTransactions != null && !paymentTransactions.isEmpty()) {
            return paymentTransactions.get(0).getOrderId();
        }
        return null;
    }
}
