package com.freecharge.payment.services;

import java.util.Map;

import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;

public interface IGatewayHandler {

    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO)
            throws Exception;

    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws Exception;
    
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) 
            throws Exception;

    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws Exception;

    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel);

    public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception;


    public enum PaymentGateways {
        CCAVENUE("cca"), BILLDESK("bdk"), FREECHARGE_WALLET("fw"), ONE_CHECK_WALLET("ocw"), KLICK_PAY("kpay"), ICIC("ics"), FREECHARGE_ZEROPAY("zpg"), FC_UPI("fcupi");

        private String name;

        private PaymentGateways(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
    public PaymentCard getPaymentCardData(String merchantTxnId);
}
