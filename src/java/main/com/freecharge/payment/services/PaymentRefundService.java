package com.freecharge.payment.services;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.notification.email.SendEmailRequest;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.notification.sms.SendSmsRequest;
import com.freecharge.payment.dao.jdbc.PaymentRefundDAO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;

@Service
public class PaymentRefundService {
    
    public enum PaymentStatusTransition{
        NULL_TO_SUCCESS, NULL_TO_FAILURE, SUCCESS_TO_FAILURE, FAILURE_TO_SUCCESS
    }
    
    private static final int SCHEDULE_INTERVAL_MINUTES = 15;

    private static final int MAX_SCHEDULE_COUNT        = 10;

    private final Logger     logger                    = LoggingFactory.getLogger(PaymentRefundService.class);

    @Autowired
    PaymentRefundDAO         paymentRefundDAO;

    @Autowired
    KestrelWrapper           kestrelWrapper;

    @Autowired
    PaymentTransactionService paymentTransactionService;

    @Autowired
    PaymentRefund paymentRefund;

    @Autowired
    HomeService homeService;

    @Autowired
    UserTransactionHistoryService transactionHistoryService;

    @Autowired
    @Qualifier("billPayServiceProxy")
    IBillPayService billPayServiceRemote;

    @Autowired
    OperatorCircleService operatorCircleService;
    
    @Autowired
    @Qualifier("notificationApiServiceClient")
    INotificationToolApiService notificationService;

    @Autowired
    FCProperties fcProperties;

    @Autowired
    InService inService;

    @Autowired
    BillPaymentService billPaymentService;
    
    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    OrderIdReadDAO orderIdReadDAO;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    public void createOrUpdateStatus(String mtxnId, RefundStatus refundStatus) {
        RefundSchedulerDO existingScheduler = paymentRefundDAO.getRefundScheduleForMtxn(mtxnId);
        if (existingScheduler != null) {
            paymentRefundDAO.updateRefundScheduleStatusForMtxn(mtxnId, refundStatus);
        } else {
            createAndInsertInitialSchedule(mtxnId, refundStatus);
        }
    }

    private void createAndInsertInitialSchedule(String mtxnId, RefundStatus refundStatus) {
        RefundSchedulerDO refundSchedulerDO = createInitialRefundSchedulerDO(mtxnId, refundStatus);
        paymentRefundDAO.insertRefundSchedule(refundSchedulerDO);
    }

    private RefundSchedulerDO createInitialRefundSchedulerDO(String mtxnId, RefundStatus refundStatus) {
        RefundSchedulerDO refundSchedulerDO = new RefundSchedulerDO();
        Calendar calendar = Calendar.getInstance();
        refundSchedulerDO.setExpiryTime(new Timestamp(calendar.getTimeInMillis()));
        refundSchedulerDO.setMtxnId(mtxnId);
        refundSchedulerDO.setScheduleCount(1);
        refundSchedulerDO.setStatus(refundStatus);
        return refundSchedulerDO;
    }

    public void scheduleOrMarkFailed(String merchantTxnId) {
        RefundSchedulerDO existingScheduler = paymentRefundDAO.getRefundScheduleForMtxn(merchantTxnId);
        if (existingScheduler != null) {
            if (existingScheduler.getScheduleCount() < MAX_SCHEDULE_COUNT) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MINUTE, SCHEDULE_INTERVAL_MINUTES);
                existingScheduler.setExpiryTime(new Timestamp(calendar.getTimeInMillis()));
                existingScheduler.setScheduleCount(existingScheduler.getScheduleCount() + 1);
                existingScheduler.setStatus(RefundStatus.SCHEDULED);
            } else {
                existingScheduler.setStatus(RefundStatus.FAILED);
            }
            paymentRefundDAO.updateRefundScheduleForMtxn(existingScheduler);
        } else {
            createAndInsertInitialSchedule(merchantTxnId, RefundStatus.SCHEDULED);
        }
    }

    public void triggerScheduledRefunds(DateTime createdFrom, DateTime createdTo) {
        List<RefundSchedulerDO> schedules = paymentRefundDAO.getScheduledRefunds(createdFrom, createdTo);
        logger.info("Processing refunds for " + schedules.size() + " mtxnIDs");

        for (RefundSchedulerDO schedule : schedules) {
            Map<String, String> queueItem = new HashMap<String, String>();
            queueItem.put(PaymentConstants.ORDER_ID_KEY, schedule.getMtxnId());
            queueItem.put(PaymentConstants.REFUND_TYPE_KEY, PaymentConstants.RECHARGE_FAILURE);
            kestrelWrapper.enqueueRefund(queueItem);
        }
    }
    
    public void sendPaymentRefundNotifications(PaymentStatusTransition paymentStatusTransition, String mtxnId) {
        try{
            if((!appConfigService.isNullToFailureRefundCommunicationEnabled() && PaymentStatusTransition.NULL_TO_FAILURE.equals(paymentStatusTransition))
                    || (!appConfigService.isNullToSuccessRefundCommunicationEnabled() && PaymentStatusTransition.NULL_TO_SUCCESS.equals(paymentStatusTransition))){
                logger.info("Refund notification is disabled for the transaction "+paymentStatusTransition);
                return;
            }
            logger.info("Sending Refund notification for mtxnId "+mtxnId);
            String orderId = paymentTransactionService.getMainOrderIdForMerchantId(mtxnId);
            String contactNumber = userServiceProxy.getUserContactNumberFromOrderId(orderId);
            Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
            UserTransactionHistory userTransactionHistory = transactionHistoryService
                    .findUserTransactionHistoryByOrderId(orderId);
            if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
                userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
                userDetail.put("circleName", userTransactionHistory.getServiceRegion());
                userDetail.put("productType", userTransactionHistory.getProductType());
                try {
                    Integer operatorMasterId = null;
                    Integer circleMasterId = -1;
                    String operatorUIName = null;
                    String circleUIName = null;
                    
                    if (FCUtil.isUtilityPaymentType(userTransactionHistory.getProductType())) {
                        BillPayOperatorMaster operator = billPayServiceRemote.getBillPayOperatorFromName(userTransactionHistory.getServiceProvider());
                        operatorMasterId = operator.getOperatorMasterId();
                        operatorUIName = operator.getName();
                    } else {
                        OperatorMaster operator = operatorCircleService.getOperator(userTransactionHistory.getServiceProvider());
                        CircleMaster circle = operatorCircleService.getCircle(userTransactionHistory.getServiceRegion());
                        operatorMasterId = operator.getOperatorMasterId();
                        operatorUIName = operator.getName();
                        if (circle != null) {
                            circleMasterId = circle.getCircleMasterId();
                            circleUIName = circle.getName();
                        }
                    }
                    
                    userDetail.put("operatorMasterId", operatorMasterId);
                    userDetail.put("circleMasterId", circleMasterId);
                    userDetail.put("operatorUIName", operatorUIName);
                    userDetail.put("circleUIName", circleUIName);
                    logger.info("userDetails set operator:" + operatorMasterId + ", circle: " + circleMasterId);
                } catch (Exception e) {
                    logger.error(" Error while converting the operator/circle id  " + orderId, e);
                }
            }
            UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
            TransactionStatus transactionStatus = getTransactionStatus(userDetailsObj, orderId);
            sendRefundCommunicationMail(paymentStatusTransition, userDetail, userDetailsObj, transactionStatus, mtxnId);
            sendRefundCommunicationSms(paymentStatusTransition, contactNumber, userDetail, userDetailsObj, transactionStatus, mtxnId);
            logger.info("Completed Sending Refund notification for mtxnId "+mtxnId);
        }catch(Exception e){
            logger.error("Could not send refund communication notification.", e);
        }
    }

    private void sendRefundCommunicationSms(PaymentStatusTransition paymentStatusTransition,
            String contactNumber, Map<String, Object> userDetail, UserDetails userDetailsObj, TransactionStatus transactionStatus, String mtxnId) {
        SendSmsRequest sendSmsRequest = new SendSmsRequest();
        sendSmsRequest.setContactNumber(contactNumber);
        Integer refundCommunicationSmsId = getRefundCommunicationSmsId(paymentStatusTransition);
        sendSmsRequest.setSmsId(refundCommunicationSmsId);
        Map<String, String> templateDataMap = new HashMap<String, String>();
        //Use o for operator and a for amount due to sms notification service length restrictions.
        templateDataMap.put("o", sanitizeOperator(userDetailsObj.getOperatorName())); //Adding Operator
        templateDataMap.put("a", userDetailsObj.getAmount());//Adding Amount
        sendSmsRequest.setTemplateDataMap(templateDataMap);
        sendSmsRequest.setTags(Arrays.asList(mtxnId));
        notificationService.sendSms(sendSmsRequest);
    }

    private String sanitizeOperator(String operatorName) {
        if(operatorName!=null){
            return operatorName.replaceAll(" ", "");
        }
        return operatorName;
    }

    private Integer getRefundCommunicationSmsId(PaymentStatusTransition paymentStatusTransition) {
        if(PaymentStatusTransition.NULL_TO_SUCCESS.equals(paymentStatusTransition)){
            return Integer.valueOf(fcProperties.getProperty("refund.sms.communication.null.to.success"));
        }else{
            return Integer.valueOf(fcProperties.getProperty("refund.sms.communication.null.to.failure"));
        }
    }

    private void sendRefundCommunicationMail(PaymentStatusTransition paymentStatusTransition,
            Map<String, Object> userDetail, UserDetails userDetailsObj, TransactionStatus transactionStatus, String mtxnId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        SendEmailRequest emailRequest = new SendEmailRequest();
        Integer refundCommunicationEmailId = getRefundCommunicationEmailId(paymentStatusTransition);
        emailRequest.setEmailId(refundCommunicationEmailId);
        emailRequest.setToEmail(userDetailsObj.getUserEmail());
        Map<String, Object> contentTemplateDataMap = new HashMap<String, Object>();
        contentTemplateDataMap.put("operator", userDetailsObj.getOperatorName());
        contentTemplateDataMap.put("circleName", userDetailsObj.getCircleName());
        contentTemplateDataMap.put("amount", userDetailsObj.getAmount());
        contentTemplateDataMap.put("orderId", userDetailsObj.getOrderId());
        contentTemplateDataMap.put("mobileno", transactionStatus!=null?transactionStatus.getSubscriberIdentificationNumber(): userDetail.get("mobileno"));
        String productName = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType()).getLabel();
        contentTemplateDataMap.put("productName", productName);
        contentTemplateDataMap.put("userFirstName", userDetailsObj.getUserFirstName());
        contentTemplateDataMap.put("isBillPay", FCUtil.isUtilityPaymentType(userDetailsObj.getProductType()));
        String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();
        contentTemplateDataMap.put("imgPrefix", imgPrefix);
        if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
            try {
                BillPayOperatorMaster bopm = billPayServiceRemote.getBillPayOperatorFromName(userDetailsObj.getOperatorName());
                if (bopm != null) {
                    contentTemplateDataMap.put("operatorIcon", bopm.getImgUrl());
                } else {
                    contentTemplateDataMap.put("operatorIcon", "");
                }
            } catch (Exception e) {
                contentTemplateDataMap.put("operatorIcon", "");
            }
        } else {
            OperatorMaster operatorMaster = operatorCircleService.getOperator(userDetailsObj.getOperatorName());
            if (operatorMaster != null) {
                contentTemplateDataMap.put("operatorIcon", operatorMaster.getImgUrl());
            } else {
                contentTemplateDataMap.put("operatorIcon", "");
            }
        }
        contentTemplateDataMap.put("communicationDate", dateFormat.format(new Date())); 
        emailRequest.setContentTemplateDataMap(contentTemplateDataMap);
        emailRequest.setSubjectTemplateDataMap(Collections.<String, Object>emptyMap());
        emailRequest.setTags(Arrays.asList(mtxnId));
        notificationService.sendEmail(emailRequest);
        
    }
    
    private Integer getRefundCommunicationEmailId(PaymentStatusTransition paymentStatusTransition) {
        if(PaymentStatusTransition.NULL_TO_SUCCESS.equals(paymentStatusTransition)){
            return Integer.valueOf(fcProperties.getProperty("refund.mail.communication.null.to.success"));
        }else{
            return Integer.valueOf(fcProperties.getProperty("refund.mail.communication.null.to.failure"));
        }
    }

    public TransactionStatus getTransactionStatus(final UserDetails userDetailsObj, final String orderId) {
        TransactionStatus transactionStatus = null;
        TransactionStatus retryTransactionStatus = null;
        if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
            transactionStatus = inService.getRechargeStatus(orderId);
            if (transactionStatus != null) {
                if (inService.isFailure(transactionStatus.getTransactionStatus())) {
                    retryTransactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
                    if (retryTransactionStatus != null) {
                        return retryTransactionStatus;
                    }
                }
            }
        }

        if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
            transactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
            if (transactionStatus != null) {
                if (inService.isFailure(transactionStatus.getTransactionStatus())) {
                    retryTransactionStatus = inService.getRechargeStatus(orderId);
                    if (retryTransactionStatus != null) {
                        return retryTransactionStatus;
                    }
                }
            }
        }
        return transactionStatus;
    }
}
