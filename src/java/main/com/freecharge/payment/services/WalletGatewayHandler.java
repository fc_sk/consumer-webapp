package com.freecharge.payment.services;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dao.jdbc.PgWriteDAO;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.business.PaymentTransactionBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.RefundAttemptAlertSNSService;
import com.freecharge.sns.RefundStatusAlertSNSService;
import com.freecharge.sns.bean.RefundAttemptBean;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.freecharge.wallet.SDMoneyClientWrapperService;
import com.freecharge.wallet.WalletController;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.WalletUtil;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.WalletCreditRequest;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.util.WebConstants;
import com.google.gson.Gson;
import com.snapdeal.payments.sdmoney.service.model.CreditGeneralBalanceToUserResponse;
import com.snapdeal.payments.sdmoney.service.model.RefundBalanceRequest;
import com.snapdeal.payments.sdmoney.service.model.RefundBalancesResponse;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdempotencyIdRequest;
import com.snapdeal.payments.sdmoney.service.model.GetTransactionByIdempotencyIdResponse;

@Component("walletGatewayHandler")
public class WalletGatewayHandler implements IGatewayHandler {

    private static Logger logger = LoggingFactory.getLogger(WalletGatewayHandler.class);

    private static final Logger refundLogger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(WalletGatewayHandler.class.getName()));

    @Autowired
    private WalletService walletService;

    @Autowired
    private CartService cartService;
    
    @Autowired
    PaymentsCache paymentsCache;

    @Autowired
    private PgWriteDAO pgWriteDAO;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private WalletWrapper walletWrapper;
    
    @Autowired
    UserTransactionHistoryDAO userTransactionHistoryDAO;
    
    @Autowired
    private RefundAttemptAlertSNSService refundAttemptAlertSNSService;
    
    @Autowired
    private RefundStatusAlertSNSService refundSatusAlertSNSService;

    @Autowired
    private UserServiceProxy userServiceProxy;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;
   
    @Autowired
    private KpayPaymentMetaDAO kpayPaymentMetaDAO;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private SDMoneyClientWrapperService sdmoneyClientWrapperService;
    
    @Override
    @Transactional
    public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception {
        String orderId = paymentRefundBusinessDO.getOrderId();
        logger.info("start refunding to wallet for order ID" + orderId);
        refundLogger.info("start refunding to wallet for order ID" + orderId);
        this.publishRefundAttempt(paymentRefundBusinessDO);

        if (paymentRefundBusinessDO != null) {
            String lookupID = paymentRefundBusinessDO.getLookupID();
            if (lookupID != null && !lookupID.isEmpty()) {
                Cart cart = cartService.getUserIdFromCartWithNoExpiryCheck(lookupID);
                if (cart != null) {
                    Integer userID = cart.getFkUserId();
                    if (userID == null) {
                        throw new Exception("User id is null while doing a refund into wallet");
                    }
                    String mtxnId=paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
                    String callerReference = mtxnId + refundType;
                    
                    
                    WalletCreditRequest walletCreditRequest = new WalletCreditRequest();
                    walletCreditRequest.setUserID(Long.parseLong(String.valueOf(userID)));
                    walletCreditRequest.setTxnAmount(new Amount(paymentRefundBusinessDO.getRefundAmount().toString()));
                    walletCreditRequest.setMtxnId(mtxnId);
                    walletCreditRequest.setTransactionType(RechargeConstants.TRANSACTION_TYPE_CREDIT);
                    walletCreditRequest.setCallerReference(callerReference);
                    
                    if (PaymentConstants.CS_CASHBACK.equals(refundType)) {
                        walletCreditRequest.setFundSource(Wallet.FundSource.CS_CASHBACK.toString());
                        
                    } else if (PaymentConstants.HCOUPON_REFUND.equals(refundType)) {
                    	walletCreditRequest.setFundSource(Wallet.FundSource.HCOUPON_REFUND.toString());
                    } else {
                        walletCreditRequest.setFundSource(Wallet.FundSource.REFUND.toString());
                    }
                    
                    UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO.findUserTransactionHistoryByOrderId(orderId);
                    Map<String, Object> walletMetaDataMap = paymentTransactionService.getWalletMetaMap(userTransactionHistory, orderId);
                    Map<String, Object> creditBalanceMap = new HashMap<>();
                    if (PaymentConstants.MANUAL_REFUND.equalsIgnoreCase(refundType)) {
                        walletMetaDataMap.put("meta", "CS manual refund");
                        creditBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.CS_REFUND);
                    } else {
                        walletMetaDataMap.put("meta", "");
                        creditBalanceMap.put(OneCheckWalletService.ONE_CHECK_TXN_TYPE, OneCheckWalletService.OneCheckTxnType.CREDIT_REFUND);
                    }
                    String metaData = new Gson().toJson(walletMetaDataMap);
                    walletCreditRequest.setMetadata(metaData);
                    
                    
                    if (paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentType() == Integer
                            .parseInt(PaymentConstants.PAYMENT_TYPE_WALLET)) {
                        walletCreditRequest.setMetadata(PaymentConstants.PAYMENT_TYPE_WALLET);
                    }
                    creditBalanceMap.put(WalletWrapper.WALLET_CREDIT_REQUEST, walletCreditRequest);
                    
                    creditBalanceMap.put(FCConstants.ORDER_ID, orderId);
                    // TODO: For debugging
                    logger.info("Payment txn for order Id " + orderId + " is " + paymentRefundBusinessDO.getPaymentTransactionBusinessDO());
                    creditBalanceMap.put(WalletWrapper.PAYMENT_TYPE, paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentType());
                    
                    Map<String, Object> responseMap = walletWrapper.refundBalanceToUser(creditBalanceMap);
                    
                    if(FCUtil.isNotEmpty(responseMap) && FCConstants.SUCCESS.equals(responseMap.get(FCConstants.STATUS))) {
                        paymentRefundBusinessDO.setStatus(true);                    	
                        try {
                            PaymentRefundTransaction paymentRefundTransaction = getPaymentRefundTransaction(
                                    paymentRefundBusinessDO, responseMap);
                            paymentTransactionService.create(paymentRefundTransaction);
                        } catch (Exception e) {
                        	paymentRefundBusinessDO.setStatus(false);
                            publishRefundAttemptStatus(paymentRefundBusinessDO);
                            logger.info("Exception occur due to save paymentRefundTransaction into database for orderId" + orderId);
                            refundLogger.info("Exception occur due to save paymentRefundTransaction into database for orderId" + orderId);
                        }
                    }
                }
            }
            publishRefundAttemptStatus(paymentRefundBusinessDO);
        }
    }

	@Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        throw new UnsupportedOperationException();
    }
	
    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws Exception {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String freeChargeOrderId = paymentRequestBusinessDO.getOrderId();
        String merchantOrderId = paymentRequestBusinessDO.getMtxnId();
        merchantOrderId = StringUtils.trim(merchantOrderId);

        String pgAmount = "";
        String amount = paymentRequestBusinessDO.getAmount();
        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }

        Map<String, String> retMap = new LinkedHashMap<String, String>();

        retMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);

        Map<String, Object> map = fs.getSessionData();
        int userId = (Integer)map.get(WebConstants.SESSION_USER_USERID);

        retMap.put(PaymentConstants.USER_ID, String.valueOf(userId));
        retMap.put(PaymentConstants.HASH, WalletUtil.createHash(merchantOrderId, userId));
        retMap.put(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER, CSRFTokenManager.getTokenFromSession());

        fs.getSessionData().put(merchantOrderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantOrderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantOrderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, retMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);

        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, WalletController.WALLET_PAY_ACTION);
        
        String pgUsed = PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(paymentRequestBusinessDO.getPaymentOption())?
                PaymentGateways.ONE_CHECK_WALLET.getName() : PaymentGateways.FREECHARGE_WALLET.getName();
                
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(pgAmount));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantOrderId);
        paymentRequest.setOrderId(freeChargeOrderId);
        paymentRequest.setPaymentGateway(pgUsed);
        paymentRequest.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(retMap));

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(amount));
        pt.setMerchantTxnId(merchantOrderId);
        pt.setOrderId(freeChargeOrderId);
        pt.setPaymentGateway(pgUsed);
        pt.setPaymentMode(PaymentConstants.PURCHASE_MODE);
        pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        pt = createPaymentTransaction(pt);

        paymentRequestBusinessDO.setResultMap(resultMap);

        fs.getSessionData().put(merchantOrderId + "_txn_id", pt.getPaymentTxnId());

        return resultMap;
    }

    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws Exception {
        Map<String, Object> finalMap = new HashMap<String, Object>();
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        //NPE if request map is empty
        if(requestMap.isEmpty()){
            return finalMap;
        }
        Boolean isSuccessful = false;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String merchantOrderId = requestMap.get(PaymentConstants.MERCHANT_ORDER_ID);
        logger.info("merchantOrderId in wallet gateway" + merchantOrderId);
        String transactionId = requestMap.get(PaymentConstants.TRANSACTION_ID);
        String amount = requestMap.get(PaymentConstants.AAMOUNT);
        String result = requestMap.get(PaymentConstants.RESULT);
        String message = requestMap.get(PaymentConstants.MESSAGE);
        String userId = requestMap.get(PaymentConstants.USER_ID);
        String hash = requestMap.get(PaymentConstants.HASH);

        if (WalletUtil.doesHashMatch(merchantOrderId, Integer.parseInt(userId), hash)) {
            if (Boolean.valueOf(result)) {
                metricsClient.recordEvent("Payment", "Wallet", "success");
                isSuccessful = true;
            } else {
                metricsClient.recordEvent("Payment", "Wallet", "failure");
                isSuccessful = false;
            }
        }

        String mode = null;
        Integer id = (Integer)fs.getSessionData().get(merchantOrderId + "_txn_id");
        String productOrderId = FCSessionUtil.getProductOrderId(merchantOrderId, fs);
        //If id is not found in session get it from DB
        if (id == null|| productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId).get(0);
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        String redirectUrl = FCSessionUtil.getSuccessUrl(merchantOrderId, fs);
        String errorUrl = FCSessionUtil.getErrorUrl(merchantOrderId, fs) + "&payment=fail";
        if (productOrderId == null || redirectUrl == null || errorUrl == null) {
            PaymentTransaction uniquePaymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(merchantOrderId);
            productOrderId = uniquePaymentTransaction.getOrderId();
            String lookUpId = orderIdSlaveDAO.getLookupIdForOrderId(productOrderId);
            redirectUrl = ViewConstants.REDIRECT_URL;
            errorUrl = ViewConstants.ERROR_URL+"?lid="+lookUpId+"&payment=fail";
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String pg = PaymentConstants.ONECHECK_FC_WALLET_PG_NAME.equals(paymentResponseBusinessDO.getPg()) ? PaymentGateways.ONE_CHECK_WALLET.getName() : PaymentGateways.FREECHARGE_WALLET.getName();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, isSuccessful.toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, productOrderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, pg);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, mode);


        
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);

        if (isSuccessful) {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);
        } else {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, errorUrl);
        }

        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(merchantOrderId);
        logger.info("amount value " + amount);
        PaymentResponse paymentResponse = new PaymentResponse(
                null,
                isSuccessful,
                transactionId,
                productOrderId,
                merchantOrderId,
                PaymentUtil.getMapAsString(requestMap),
                new Double(amount),
                pg,
                message,
                null,
                null,
                null,
                null,
                null,
                PaymentUtil.getMapAsString(resultMap),
                null,
                null
        );

        paymentResponse = dumpResponseAudit(paymentResponse);

        PaymentTransaction pt = new PaymentTransaction(
                id,
                transactionId,
                null,
                null,
                Calendar.getInstance().getTime(),
                null,
                null,
                isSuccessful,
                null,
                null,
                null, //response code
                null,
                paymentResponse.getPaymentRsId(),
                null,
                null,
                null
        );
        pt.setRecievedToPg(Calendar.getInstance().getTime());

        updatePaymentTransaction(pt);

        return finalMap;
    }

    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
        PaymentTransaction transactionToSync = paymentTransactionService.getPaymentTransactionById(paymentQueryBusinessDO.getPaymentTxnId());
        boolean originalPaymentStatus = transactionToSync.getIsSuccessful();
        boolean isWalletDebitedForPayment = walletWrapper.checkWalletDebitedAndSetTransactionId(transactionToSync);
        paymentQueryBusinessDO.setPaymentStatus(isWalletDebitedForPayment);
        if(originalPaymentStatus!=isWalletDebitedForPayment)
            walletService.notifyWalletPayment(transactionToSync, "ocw", isWalletDebitedForPayment);
        paymentQueryBusinessDO.setReferenceId(transactionToSync.getTransactionId());
        if (isWalletDebitedForPayment) {
            logger.info("Wallet Txn updated success for orderId " + transactionToSync.getOrderId());
            transactionToSync.setIsSuccessful(isWalletDebitedForPayment);
            transactionToSync.setRecievedToPg(new Date());
            updatePaymentTransaction(transactionToSync);
        }
        return paymentQueryBusinessDO;
    }

    private String getPrevTransactionId(String oneCheckIdentity, PaymentRefundBusinessDO paymentRefundBusinessDO) {
        String pgTransactionId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getTransactionId();
        if (null == pgTransactionId || pgTransactionId.equals("0")) {
            String orderId = paymentRefundBusinessDO.getOrderId();
            String mtxnId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
            String idempotencyId = mtxnId + "_" + OneCheckTxnType.DEBIT_RECHARGE;
            pgTransactionId = oneCheckWalletService.getPreviousDebitTransactionId(orderId, oneCheckIdentity, mtxnId,
                    null, OneCheckTxnType.DEBIT_RECHARGE);
        }
        return pgTransactionId;
    }

    @Override
    @Transactional
    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception {
        String walletReferenceId = paymentRefundBusinessDO.getOrderId();

        String oneCheckIdentity;
        try{
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentRefundBusinessDO.getOrderId());
            String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
            oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
            if(FCUtil.isEmpty(oneCheckIdentity))
            {
                logger.info(email+" is not a onecheck user.");
                refundToWallet(paymentRefundBusinessDO, refundType);
                return paymentRefundBusinessDO;
            }
            logger.info("Found the user id for the transaction "+paymentRefundBusinessDO.getOrderId());
        }
        catch(Exception e){
            logger.error("Failed to find the owner of the transaction "+walletReferenceId, e);
            throw e;
        }

        String pgTransactionId = getPrevTransactionId(oneCheckIdentity, paymentRefundBusinessDO);

        logger.info("Refunding transaction "+walletReferenceId);
        
        PaymentTransactionBusinessDO paymentTransactionBusinessDO = paymentRefundBusinessDO.getPaymentTransactionBusinessDO();
        String idempotencyId = paymentTransactionBusinessDO.getMerchantTxnId() + "_" + OneCheckTxnType.CREDIT_REFUND;

        logger.info("refund type is " + refundType + "for mtxnId " + paymentTransactionBusinessDO.getMerchantTxnId());

        String eventContext;
        if (FCUtil.isEmpty(refundType)) {
            eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId();
        } else {
            if (PaymentConstants.MANUAL_REFUND.equalsIgnoreCase(refundType)) {
                idempotencyId = paymentTransactionBusinessDO.getMerchantTxnId() + "_" + OneCheckTxnType.CS_REFUND;
                eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId() + " CS Manual refund";
            } else if (PaymentConstants.CS_BANK_REFUND.equalsIgnoreCase(refundType)) {
                eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId() + " CS Bank refund";
            } else {
                eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId();
            }
        }
        
        
        RefundBalanceRequest refundBalanceRequest = new RefundBalanceRequest();
        refundBalanceRequest.setBusinessEntity("FREECHARGE");
        refundBalanceRequest.setEventContext(eventContext);
        refundBalanceRequest.setIdempotencyKey(idempotencyId);
        refundBalanceRequest.setPrevTransactionId(pgTransactionId);
        refundBalanceRequest.setRefundAmount(new BigDecimal(paymentRefundBusinessDO.getAmount()));
        refundBalanceRequest.setSdIdentity(oneCheckIdentity);
        refundBalanceRequest.setTransactionReference(walletReferenceId);
        logger.info("Refund request : " + refundBalanceRequest.toString());

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        this.publishRefundAttempt(paymentRefundBusinessDO);
        try{
            RefundBalancesResponse response = sdmoneyClientWrapperService.refundBalanceToUser(refundBalanceRequest); 
            logger.info("Refund response : " + response.toString());
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
            paymentRefundBusinessDO.setStatus(true);
            paymentRefundTransaction.setRawPGResponse(response.toString());
            paymentRefundTransaction.setPgTransactionReferenceNo(response.getTransactionId());
            paymentRefundTransaction.setPgStatus("true");
        }
        catch(Exception e){
            logger.error("Refund failed!", e);
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            paymentRefundBusinessDO.setStatus(false);
            paymentRefundTransaction.setPgStatus("false");
        }
        this.publishRefundAttemptStatus(paymentRefundBusinessDO);
        
        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(
                paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest(refundBalanceRequest.toString());
        paymentRefundTransaction.setSentToPG(new Date());

        paymentRefundTransaction.setRefundTo(PaymentConstants.ONE_CHECK_WALLET_CODE);
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentTransactionService.create(paymentRefundTransaction);
        
        logger.info("Saved paymentRefundTransaction for "+walletReferenceId);
        
        paymentRefundBusinessDO.setMsg(paymentRefundTransaction.getPgResponseDescription());
        paymentRefundBusinessDO.setRawPGResponse(paymentRefundTransaction.getRawPGResponse());
        String paymentOption = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentOption();
        if (PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(paymentOption)) {
            paymentRefundBusinessDO.setRefundToWallet(true);
        }
        return paymentRefundBusinessDO;
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {

        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }

    private PaymentResponse dumpResponseAudit(PaymentResponse paymentResponse) {
        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;

    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
            pt = paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
        return pt;
    }

    private void updatePaymentTransaction(PaymentTransaction pt) {
        try {
            paymentTransactionService.updatePaymentTransaction(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
    }

    private PaymentRefundTransaction getPaymentRefundTransaction(PaymentRefundBusinessDO paymentRefundBusinessDO, Map<String, Object> responseMap) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        paymentRefundTransaction.setRefundedAmount(new Amount(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setPgStatus("true");
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);
        paymentRefundTransaction.setReceivedFromPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setSentToPG(Calendar.getInstance().getTime());
        paymentRefundTransaction.setPaymentTransactionId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest("wallet request");
        paymentRefundBusinessDO.setRefundToWallet(true);
        String refundToWallet = (String) responseMap.get(PaymentConstants.REFUND_TO_WALLET);
        switch (refundToWallet) {
            case PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION:
                paymentRefundTransaction.setRawPGResponse("wallet response");
                paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
                break;
            case PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION:
                CreditGeneralBalanceToUserResponse walletCreditResponse = (CreditGeneralBalanceToUserResponse) responseMap.get(
                        WalletWrapper.ONE_CHECK_WALLET_TRANSACTION_RESPONSE);
                paymentRefundTransaction.setRawPGResponse(walletCreditResponse.toString());
                paymentRefundTransaction.setRefundTo(PaymentConstants.ONE_CHECK_WALLET_CODE);
                break;
        }
        metricsClient.recordEvent("Payment", "getPaymentRefundTransaction", "success");
        return paymentRefundTransaction;
    }

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        // TODO Auto-generated method stub
        return null;
    }

    public void publishRefundAttempt(PaymentRefundBusinessDO paymentRefundBusinessDO) {
        try {
            RefundAttemptBean alertBean = new RefundAttemptBean();
            UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                    .findUserTransactionHistoryByOrderId(paymentRefundBusinessDO.getOrderId());
            String paymentGateway = this.getRefundAlertPaymentGateway(paymentRefundBusinessDO);
            alertBean.setUserId((long) userTransactionHistory.getFkUserId());
            alertBean.setOrderId(paymentRefundBusinessDO.getOrderId());
            alertBean.setAmount(paymentRefundBusinessDO.getAmount());
            alertBean.setMtxnId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
            alertBean.setPaymentGateway(paymentGateway);
            alertBean.setCallerReference(
                    paymentRefundBusinessDO.getOrderId() + RechargeConstants.TRANSACTION_TYPE_DEBIT);
            logger.info("Publishing refund attempt SNS");
            refundAttemptAlertSNSService.publish(paymentRefundBusinessDO.getOrderId(),
                    "{\"message\" : " + RefundAttemptBean.getJsonString(alertBean) + "}");
        } catch (Exception e) {
            logger.error("Error while publishing the payment attempt method to SNS for order id "
                    + paymentRefundBusinessDO.getOrderId(), e);
        }
    }

    public void publishRefundAttemptStatus(PaymentRefundBusinessDO paymentRefundBusinessDO) {
        try {
            RefundAttemptBean alertBean = new RefundAttemptBean();
            UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                    .findUserTransactionHistoryByOrderId(paymentRefundBusinessDO.getOrderId());
            String paymentGateway = this.getRefundAlertPaymentGateway(paymentRefundBusinessDO);
            alertBean.setUserId((long) userTransactionHistory.getFkUserId());
            alertBean.setOrderId(paymentRefundBusinessDO.getOrderId());
            alertBean.setAmount(paymentRefundBusinessDO.getAmount());
            alertBean.setMtxnId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
            alertBean.setPaymentGateway(paymentGateway);
            alertBean.setStatus(paymentRefundBusinessDO.getStatus());
            logger.info("Publishing refund attempt status SNS");
            refundSatusAlertSNSService.publish(paymentRefundBusinessDO.getOrderId(),
                    "{\"message\" : " + RefundAttemptBean.getJsonString(alertBean) + "}");
        } catch (Exception e) {
            logger.error("Error while publishing the payment attempt method to SNS for order id "
                    + paymentRefundBusinessDO.getOrderId(), e);
        }
    }

	private String getRefundAlertPaymentGateway(PaymentRefundBusinessDO paymentRefundBusinessDO) {
		String mtxnId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
		String paymentGateway = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentGateway();
		if ((paymentGateway != null) && (paymentGateway.equals("kpay"))) {
			try {
				Users user = userServiceProxy.getUserFromOrderId(paymentRefundBusinessDO.getOrderId());
				String sdIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(user.getEmail());
				if (sdIdentity != null) {
					paymentGateway = PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION;
				} else {
					paymentGateway = PaymentConstants.FREECHARGE_WALLET_PAYMENT_OPTION;
				}
			} catch (Exception e) {
				logger.info("Error fetching PaymentGateway for mtxnid:" + mtxnId + " ", e);
			}
		}
		return paymentGateway;
	}

}
