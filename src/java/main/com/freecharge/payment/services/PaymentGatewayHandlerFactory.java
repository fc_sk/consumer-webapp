package com.freecharge.payment.services;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.util.PaymentConstants;

@Component
public class PaymentGatewayHandlerFactory {
    @Autowired
    FCProperties fcProperties;
    
    @Autowired
    @Qualifier("ccAvenueGatewayHandler")
    private IGatewayHandler ccAvenueGatewayHandler;

    @Autowired
    @Qualifier("billDeskGatewayHandler")
    private IGatewayHandler billDeskGatewayHandler;
    
    @Autowired
    @Qualifier("iciciSslGatewayHandler")
    private IGatewayHandler iciciSslGatewayHandler;
    
    @Autowired
    @Qualifier("walletGatewayHandler")
    private IGatewayHandler walletGatewayHandler;
    
    @Autowired
    @Qualifier("fcUpiGatewayHandler")
    private IGatewayHandler fcUpiGatewayHandler;
    
    @Autowired
    @Qualifier("zeroPaymentGatewayHandler")
    private IGatewayHandler zeroPaymentGatewayHandler;

    @Autowired
    @Qualifier("payUGatewayHandler")
    private IGatewayHandler payUGatewayHandler;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private IGatewayHandler klickPayGatewayHandler;

    @Autowired
    @Qualifier("fcMockGatewayHandler")
    private IGatewayHandler fcMockGatewayHandler;

    public IGatewayHandler getPaymentHandler(String paymentGateway) {
        
        if (StringUtils.isNotBlank(paymentGateway)) {
            if (paymentGateway.equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE)) {
            	return ccAvenueGatewayHandler;
            } else if (paymentGateway.equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE)) {
                return billDeskGatewayHandler;
            } else if (paymentGateway.equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE)) {
            	if (fcProperties.shouldMockPG()) {
            		// We have not mocked the ICICI GW yet. Just use the BillDesk GW instead for mocking.
            		return billDeskGatewayHandler;
            	} else {
            		return iciciSslGatewayHandler;
            	}              
            } else if(paymentGateway.equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE)) {
                return payUGatewayHandler;
            } else if(paymentGateway.equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE)) {
                return klickPayGatewayHandler;
            } else if (IGatewayHandler.PaymentGateways.FREECHARGE_WALLET.getName().equals(paymentGateway)
                    || PaymentConstants.ONECHECK_FC_WALLET_PG_NAME.equals(paymentGateway) || IGatewayHandler.PaymentGateways.ONE_CHECK_WALLET.getName().equals(paymentGateway)) {
                return walletGatewayHandler;
            } else if(PaymentConstants.PAYMENT_GATEWAY_FCUPI_CODE.equals(paymentGateway)){
                return fcUpiGatewayHandler;
            } else if (IGatewayHandler.PaymentGateways.FREECHARGE_ZEROPAY.getName().equals(paymentGateway)) {
                return zeroPaymentGatewayHandler;
            } else if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_DUMMY_FREECHARGE_CODE)) {
                /*
                 * Being extra cautious.
                 */
                if(fcProperties.shouldMockPG()) {
                    return fcMockGatewayHandler;
                }
            }
        }
        
        throw new RuntimeException(String.format("Payment gateway corresponding to %s not found.", paymentGateway));
    }
}
