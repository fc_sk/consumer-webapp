package com.freecharge.payment.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.payment.dao.PaymentPlanDAO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.util.PaymentConstants;

/**
 * This class exposes, albeit rudimentary, "Payment Plan API".
 * Payment Plan can be loosely defined as a ordered list of
 * payment methods which need to be charged in that order.
 * Take the following payment plan for example.
 * "Wallet: 50, CreditCard: 20". This means that a customer's
 * wallet has to be charged Rs 50 and then his credit card Rs 20.
 * Note that this class in itself is *not* responsible for charging
 * appropriate payment methods.
 * <br>
 * There could be one and only payment plan for a given
 * plan reference ID. It is upto the consumers of {@link PaymentPlanService}
 * to use appropriate plan reference ID, could be order ID, merchant id or 
 * whatever makes sense.
 * @author arun
 *
 */
@Service
public class PaymentPlanService {
    private Logger         logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private PaymentPlanDAO ppDAO;
    
    
    public void createPaymentPlan(PaymentPlan paymentPlan) {
        ppDAO.insertPaymentPlan(paymentPlan);
    }
    
    public PaymentPlan getPaymentPlan(String planReferenceId) throws PaymentPlanNotFoundException {
        return ppDAO.getPaymentPlan(planReferenceId);
    }
    
    public PaymentPlan getPaymentPlanFromMtxnId(String mtxnId, String orderId) throws PaymentPlanNotFoundException {
        return ppDAO.getPaymentPlanFromMtxnId(mtxnId, orderId);
    }
    
    public boolean updatePaymentPlan(PaymentPlan paymentPlan) {
        return ppDAO.updatePaymentPlan(paymentPlan);
    }
    
    public Amount getTotalAmount(String planReferenceId) throws PaymentPlanNotFoundException {
        PaymentPlan paymentPlan = getPaymentPlan(planReferenceId);
        
        return new Amount(paymentPlan.getPgAmount().getAmount().add(paymentPlan.getWalletAmount().getAmount()));
    }

    public boolean isPaymentPlanMismatch(String orderId, List<PaymentTransaction> successPaymentTxn) {
        PaymentPlan paymentPlan = null;
        try {
            paymentPlan = getPaymentPlan(orderId);
        } catch (PaymentPlanNotFoundException e) {
            logger.error("Payment plan not found for order " + orderId);
            return true;
        }
        Amount totalPaidAmount = getTotalPaidAmount(successPaymentTxn);
        if (paymentPlan.getTotalAmount().compareTo(new Amount(0.0)) == 0
                && !successPaymentTxn.get(0).getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_ZEROPG_CODE)) {
            logger.info(String.format("Payment Plan amount %s, Paid Amount %s for order %s", paymentPlan
                    .getTotalAmount().getAmount(), totalPaidAmount.getAmount(), orderId));
            return true;
        }
        
        Amount pgPlan = paymentPlan.getPgAmount();
        Amount walletPlan = paymentPlan.getWalletAmount();
        Amount totalPlan = paymentPlan.getTotalAmount();
        return isPaymentPlanOutOfSync(pgPlan, walletPlan, totalPlan, successPaymentTxn);
    }

    private boolean isPaymentPlanOutOfSync(Amount pgPlan, Amount walletPlan, Amount totalPlan, List<PaymentTransaction> successPaymentTxn) {
        for (PaymentTransaction paymentTransaction : successPaymentTxn) {
            logger.info("PaymentTxn Amount = " + paymentTransaction.getAmount() + ", PG : " + pgPlan.getAmount() + " Wallet = " + walletPlan.getAmount());
            if (paymentTransaction.isFullFCWalletPayment()
                    && totalPlan.compareTo(new Amount(paymentTransaction.getAmount())) != 0) {
                return true;
            } else if (paymentTransaction.isNewFCWalletPayment() && !paymentTransaction.isFullFCWalletPayment()
                    && walletPlan.compareTo(new Amount(paymentTransaction.getAmount())) != 0) {
                return true;
            } else if (paymentTransaction.isPGPayment()
                    && pgPlan.compareTo(new Amount(paymentTransaction.getAmount())) != 0) {
                return true;
            }
        }
        return false;
    }

    private Amount getTotalPaidAmount(List<PaymentTransaction> successPaymentTxn) {
        Double totalAmount = 0.0;
        for (PaymentTransaction pt : successPaymentTxn) {
            if (!pt.getPaymentGateway().equalsIgnoreCase(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                totalAmount += pt.getAmount();
            }
        }
        return new Amount(totalAmount);
    }
}
