package com.freecharge.payment.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.TransactionInProgressException;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dao.jdbc.PgReadDAO;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.web.PaymentTxn;
import com.freecharge.payment.util.KlickPayUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.google.gson.Gson;
import com.snapdeal.payments.sdmoney.exceptions.TransactionFailedException;
import com.snapdeal.payments.sdmoney.exceptions.TransactionNotFoundException;

@Service("paymentDetailService")
public class PaymentDetailService {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private PgReadDAO          pgReadDAO;

    @Autowired
    private KpayPaymentMetaDAO kpayPaymentMetaDAO;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private PaymentGatewayHandlerFactory pgFactory;

    @Autowired
    private KlickPayUtil klickPayUtil;

    @Autowired
    private OrderIdWriteDAO orderIdWriteDAO;

    public List<PaymentTxn> getPaymentTxnByMtxnId(String mtxnId) {
        logger.info("Fetching payment details for mtxnId: " + mtxnId);
        List<PaymentTransaction> paymentTransactions = pgReadDAO.getPaymentTxnByMtxnId(mtxnId);
        logger.info("Found paymentTransactions: " + paymentTransactions);
        String orderId = paymentTransactions.get(0).getOrderId();
        List<PaymentTransaction> updatedPaymentTransactions = new ArrayList<PaymentTransaction>();
        Map<Integer, String> referenceIdMap = new HashMap<Integer, String>();
        Map<String, String> kpayPaymentMetaMap = getKpayMetaMapFromMtxnId(mtxnId);
        logger.info("Fetched kpay payment meta map: "+kpayPaymentMetaMap+ " for mtxnId: " + mtxnId);
        String kpayMerchantId = null;
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            String referenceId = null;
            PaymentTransaction updatedPaymentTransaction = null;
            if (PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE.equals(paymentTransaction.getPaymentGateway())) {
                if (!FCUtil.isEmpty(kpayPaymentMetaMap) &&
                        kpayPaymentMetaMap.containsKey(PaymentConstants.REFERENCE_ID_KEY_STRING)) {
                    logger.info("Found data from klickpay_payment_meta, not doing status check.");
                    referenceId = kpayPaymentMetaMap.get(PaymentConstants.REFERENCE_ID_KEY_STRING);
                    kpayMerchantId = kpayPaymentMetaMap.get(PaymentConstants.KLICKPAY_MID_KEY_STRING);
                    updatedPaymentTransaction = paymentTransaction;
                } else {
                    IGatewayHandler gatewayHandler = getPaymentGatewayHandler(paymentTransaction.getPaymentGateway());
                    logger.info("Querying pg(" + gatewayHandler.getClass().getSimpleName() + ") for status of payment with payment_txn_id " + paymentTransaction.getPaymentTxnId());
                    try {
                        PaymentQueryBusinessDO paymentQueryBusinessDO = gatewayHandler.paymentStatus(PaymentQueryBusinessDO.create(paymentTransaction), false);
                        referenceId = paymentQueryBusinessDO.getReferenceId();
                        updatedPaymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTransaction.getPaymentTxnId());
                    } catch (TransactionFailedException | TransactionInProgressException | TransactionNotFoundException e) {
                        logger.info("Received Transaction exception. Hence marking the transaction as failure." +e);
                        referenceId = null;
                        updatedPaymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTransaction.getPaymentTxnId());
                        updatedPaymentTransaction.setIsSuccessful(false);
                    }
                }
            } else {
                updatedPaymentTransaction = paymentTransaction;
                referenceId = paymentTransaction.getTransactionId();
            }
            updatedPaymentTransactions.add(updatedPaymentTransaction);
            referenceIdMap.put(updatedPaymentTransaction.getPaymentTxnId(), referenceId);
        }
        if(FCUtil.isEmpty(kpayMerchantId)) {
            logger.info("Could not get merchantId from table, calling CCS for mtxnId: "+mtxnId);
            String[] credentials = klickPayUtil.getKlickPayCredentials(
                    orderIdWriteDAO.getProductName(orderId).getProductType());
            kpayMerchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        }
        List<PaymentTxn> paymentTxns = convertToPaymentTxns(updatedPaymentTransactions, kpayPaymentMetaMap,
                referenceIdMap, kpayMerchantId);
        logger.info("PaymentTxns formed for mtxnId: "+mtxnId+" are: "+paymentTxns);
        return paymentTxns;
    }

    private Map<String, String> getKpayPaymentMetaMap(KlickpayPaymentMeta kpayPaymentMeta) {
        Map<String, String> kpayPaymentMetaMap = new HashMap<>();
        Map<String, String> metaData = new HashMap<>();
        if (null != kpayPaymentMeta) {
            kpayPaymentMetaMap.put(PaymentConstants.PG_USED_KEY_STRING, kpayPaymentMeta.getPgUsed());
            if(!FCUtil.isEmpty(kpayPaymentMeta.getReferenceId()) && !FCUtil.isEmpty(kpayPaymentMeta.getMetadata())) {
                metaData = new Gson().fromJson(kpayPaymentMeta.getMetadata(), metaData.getClass());
                kpayPaymentMetaMap.put(PaymentConstants.REFERENCE_ID_KEY_STRING, kpayPaymentMeta.getReferenceId());
                kpayPaymentMetaMap.put(PaymentConstants.KLICKPAY_MID_KEY_STRING,
                        metaData.get(PaymentConstants.KLICKPAY_MID_KEY_STRING));
            }
        }
        return kpayPaymentMetaMap;
    }

    private List<PaymentTxn> convertToPaymentTxns(List<PaymentTransaction> paymentTransactions,
            Map<String, String> kpayPaymentMetaMap, Map<Integer, String> referenceIdMap, String merchantId) {

        List<PaymentTxn> paymentTxns = new ArrayList<>();

        if (!FCUtil.isEmpty(paymentTransactions)) {
            for (PaymentTransaction paymentTransaction : paymentTransactions) {

                PaymentTxn paymentTxn = convertToPaymentTxn(paymentTransaction);
                if(!FCUtil.isEmpty(kpayPaymentMetaMap) && PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE.equals(paymentTransaction.getPaymentGateway())){
                    paymentTxn.setUnderlyingPgUsed(kpayPaymentMetaMap.get(PaymentConstants.PG_USED_KEY_STRING));
                    if(!FCUtil.isEmpty(merchantId)) {
                        paymentTxn.setMerchantId(merchantId);
                    }
                }

                if(!FCUtil.isEmpty(referenceIdMap)){
                    paymentTxn.setReferenceId(referenceIdMap.get(paymentTransaction.getPaymentTxnId()));
                }

                paymentTxns.add(paymentTxn);
            }
        }

        return paymentTxns;
    }

    private Map<String, String> getKpayMetaMapFromMtxnId(String mtxnId) {
        List<KlickpayPaymentMeta> kpayPaymentMetas = kpayPaymentMetaDAO.getKpayPaymentMeta(mtxnId);
        KlickpayPaymentMeta klickpayPaymentMeta = null;
        Map<String, String> kpayPaymentMetaMap = null;
        if(!FCUtil.isEmpty(kpayPaymentMetas)) {
            klickpayPaymentMeta = kpayPaymentMetas.get(0);
            kpayPaymentMetaMap = getKpayPaymentMetaMap(klickpayPaymentMeta);
        }
        return kpayPaymentMetaMap;
    }

    private PaymentTxn convertToPaymentTxn(PaymentTransaction paymentTransaction) {
        PaymentTxn paymentTxn = new PaymentTxn();

        paymentTxn.setPaymentTxnId(paymentTransaction.getPaymentTxnId());
        paymentTxn.setOrderId(paymentTransaction.getOrderId());
        paymentTxn.setSentToPg(paymentTransaction.getSetToPg());
        paymentTxn.setRecievedFromPg(paymentTransaction.getRecievedToPg());
        paymentTxn.setAmount(paymentTransaction.getAmount());
        paymentTxn.setMerchantTxnId(paymentTransaction.getMerchantTxnId());
        paymentTxn.setPgTransactionId(paymentTransaction.getTransactionId());
        paymentTxn.setIsSuccessful(paymentTransaction.getIsSuccessful());
        paymentTxn.setPaymentGateway(paymentTransaction.getPaymentGateway());
        paymentTxn.setPaymentMode(paymentTransaction.getPaymentMode());
        paymentTxn.setPaymentType(paymentTransaction.getPaymentType());
        paymentTxn.setPaymentOption(paymentTransaction.getPaymentOption());
        paymentTxn.setPgMessage(paymentTransaction.getPgMessage());
        paymentTxn.setPgTransactionTime(paymentTransaction.getPgTransactionTime());

        return paymentTxn;
    }

    private IGatewayHandler getPaymentGatewayHandler(String pgUsed) {
        return pgFactory.getPaymentHandler(pgUsed);
    }

}
