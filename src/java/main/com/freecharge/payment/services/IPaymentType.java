package com.freecharge.payment.services;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;

public interface IPaymentType {
    
    public Boolean doPayment(PaymentRequestBusinessDO paymentRequestBusinessDO)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception;

    public PaymentResponseBusinessDO handleResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception;

    public PaymentResponseBusinessDO checkStatus(PaymentRequestBusinessDO paymentRequestBusinessDO) 
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception;

    public PaymentRefundBusinessDO doRefund(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception;

}
