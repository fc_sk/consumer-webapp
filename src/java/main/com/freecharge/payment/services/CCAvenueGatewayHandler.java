package com.freecharge.payment.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Adler32;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.business.PaymentTransactionBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.sns.bean.ReconMetaData;


@Component("ccAvenueGatewayHandler")
public class CCAvenueGatewayHandler extends PaymentGatewayHandler {
    private static Logger logger = LoggingFactory.getLogger(CCAvenueGatewayHandler.class);
    private static final Logger refundLogger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(CCAvenueGatewayHandler.class.getName()));

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    @Qualifier(value = "walletGatewayHandler")
    private IGatewayHandler walletGatewayHandler;
    
    @Autowired
    OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    UserServiceProxy userServiceProxy;

    
    private final static String DEFAULT_DETAILS = "Freecharge Default Details";
    private static String CCAvenue_Success_Return_Value = "Y";
    private static String WORKING_KEY = "";
    private static String MECODE = "";
    private static String ICICINETBANKING_WORKING_KEY = "";
    private static String ICICINETBANKING_MECODE = "";
    private static String CCAVENUE_URL = "https://www.ccavenue.com/shopzone/cc_details.jsp";
    private static String CCAVENUE_URL_OLD = "https://www.ccavenue.com/shopzone/cc_details.jsp";
    private static String CCAVENUE_FREECHARGE_RETUERN_URL = "/payment/handleccaresposne";
    private static String MOBILE_CCAVENUE_FREECHARGE_RETUERN_URL = "/m/payment/response/handleccaresposne";
    private static String CCAVENUE_CHECK_STATUS_URL = "https://www.ccavenue.com/servlet/new_txn.OrderStatusTracker";
    private static String CCAVENUE_REFUND_URL = "https://www.ccavenue.com/servlet/ccav.Refund_NetBnk";
    private static HashMap<String, String> paymentTypeMap = new HashMap<String, String>();
    private static HashMap<String, String> ccAvenueBankCodesFromUi = new HashMap<String, String>();
    private static HashMap<String, String> ccAvenueATMBankCodesFromUi = new HashMap<String, String>();
    private static HashMap<String, String> ccAvenueCashCardCodesFromUi = new HashMap<String, String>();
    public static final String REFUND_SUCCESS_RESPONSE = "0";
    public static final String ORDER_NO_REFUND_RESPONSE_PARAM = "order_no";
    public static final String REFUND_STATUS_RESPOND_PARAM = "refStat";


    static {
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_CREDITCARD, "NonMoto");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_DEBITCARD, "");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_CASHCARD, "CCRD");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_NETBANKING, "netBanking");
        paymentTypeMap.put(PaymentConstants.PAYMENT_TYPE_ATMCARD, "NonMoto");
        ccAvenueCashCardCodesFromUi.put("ITZ","ITZ_N");
        ccAvenueCashCardCodesFromUi.put("DON","DONO_N");
        ccAvenueCashCardCodesFromUi.put("OXI","OXIG_N");

        ccAvenueBankCodesFromUi.put("ANDB", "AND_N");
        ccAvenueBankCodesFromUi.put("AXIS", "UTI_N");
        ccAvenueBankCodesFromUi.put("ALLB", "XXXX");
        ccAvenueBankCodesFromUi.put("BAHK", "BBK_N");
        ccAvenueBankCodesFromUi.put("BOBC", "BOBCO_N");
        ccAvenueBankCodesFromUi.put("BOBR", "BOB_N");
        ccAvenueBankCodesFromUi.put("BOI", "BOI_N");
        ccAvenueBankCodesFromUi.put("MAHB", "BOM_N");
        ccAvenueBankCodesFromUi.put("CITA", "CBIBAN_N");
        ccAvenueBankCodesFromUi.put("CANB", "CAN_N");
        ccAvenueBankCodesFromUi.put("CENB", "CEN_N");
        ccAvenueBankCodesFromUi.put("CITU", "CITIUB_N");
        ccAvenueBankCodesFromUi.put("CORB", "COP_N");
        ccAvenueBankCodesFromUi.put("CSYB", "XXXX");
        ccAvenueBankCodesFromUi.put("DENB", "DEUNB_N");
        ccAvenueBankCodesFromUi.put("DEUB", "DEUNB_N");
        ccAvenueBankCodesFromUi.put("DECB", "DCB_N");
        ccAvenueBankCodesFromUi.put("DHLB", "DLB_N");
        ccAvenueBankCodesFromUi.put("FEDB", "FDEB_N");
        ccAvenueBankCodesFromUi.put("HDFC", "HDFCM_N");
        ccAvenueBankCodesFromUi.put("ICIC", "ICPRF_N");
        ccAvenueBankCodesFromUi.put("IDBI", "IDBI_N");
        ccAvenueBankCodesFromUi.put("IOB", "IOB_N");
        ccAvenueBankCodesFromUi.put("INIB", "NIIB_N");
        ccAvenueBankCodesFromUi.put("INVB", "ING_N");
        ccAvenueBankCodesFromUi.put("JNKB", "JKB_N");
        ccAvenueBankCodesFromUi.put("KRTB", "KTKB_N");
        ccAvenueBankCodesFromUi.put("KARB", "KVB_N");
        ccAvenueBankCodesFromUi.put("LXCB", "LVB_N");
        ccAvenueBankCodesFromUi.put("LXRB", "LVB_N");
        ccAvenueBankCodesFromUi.put("KOBK", "NKMB_N");
        ccAvenueBankCodesFromUi.put("ORTB", "OBPRF_N");
        ccAvenueBankCodesFromUi.put("PJCB", "PNBCO_N");
        ccAvenueBankCodesFromUi.put("PJRB", "NPNB_N");
        ccAvenueBankCodesFromUi.put("PJSB", "NPNB_N");
        ccAvenueBankCodesFromUi.put("SINB", "SIB_N");
        ccAvenueBankCodesFromUi.put("STCB", "SCB_N");
        ccAvenueBankCodesFromUi.put("SBJB", "SBJ_N");
        ccAvenueBankCodesFromUi.put("SHYB", "SBH_N");
        ccAvenueBankCodesFromUi.put("SBIN", "SBI_N");
        ccAvenueBankCodesFromUi.put("SMYB", "SBM_N");
        ccAvenueBankCodesFromUi.put("SPTB", "SBP_N");
        ccAvenueBankCodesFromUi.put("STVB", "SBT_N");
        ccAvenueBankCodesFromUi.put("SYNB", "SYNBK_N");
        ccAvenueBankCodesFromUi.put("TMEB", "TNMB_N");
        ccAvenueBankCodesFromUi.put("UCOB", "UCO_N");
        ccAvenueBankCodesFromUi.put("UBI", "UNI_N");
        ccAvenueBankCodesFromUi.put("UNIB", "UBI_N");
        ccAvenueBankCodesFromUi.put("VIJB", "VJYA_N");
        ccAvenueBankCodesFromUi.put("YEBK", "YES_N");

        ccAvenueATMBankCodesFromUi.put("ACAN", "CANVIS_N");
        ccAvenueATMBankCodesFromUi.put("ACIT", "CBIDEB_N");
        ccAvenueATMBankCodesFromUi.put("AIOS", "IOBDB_N");
        ccAvenueATMBankCodesFromUi.put("APNB", "PNBVIS_N");
        ccAvenueATMBankCodesFromUi.put("ASBI", "SBDB_N");
        ccAvenueATMBankCodesFromUi.put("AUBI", "UNIDB_N");
        ccAvenueATMBankCodesFromUi.put("ALVB", "XXXX");
        ccAvenueATMBankCodesFromUi.put("ABOI", "XXXX");

    }

    public CCAvenueGatewayHandler() {
    }

    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws  Exception {
        String responseUrl = null;
        String cardOption = "";
        String netBankingCCACodeForBank = null;
        String nonMotoCardType = null;

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String freeChargeOrderId = paymentRequestBusinessDO.getOrderId();
        String merchantOrderId = PaymentUtil.getPaymentGatewayOrderId(paymentRequestBusinessDO, paymentTransactionService.countPGRequestForOrderId(freeChargeOrderId));

        logger.debug("CCAvenue PG payment starts for order id: " + merchantOrderId + " and session id: " + fs.getUuid());
        String host = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME);
        responseUrl = host + CCAVENUE_FREECHARGE_RETUERN_URL;
        if (MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO)){
        	responseUrl = host + MOBILE_CCAVENUE_FREECHARGE_RETUERN_URL;
        }
        String pgAmount = "";
        String amount = paymentRequestBusinessDO.getAmount();
        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }

        Map<String, String> retMap = new LinkedHashMap<String, String>();
        cardOption = paymentTypeMap.get(paymentRequestBusinessDO.getPaymentType().toString());

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentRequestBusinessDO.getPaymentType()) {
            retMap.put("cardOption", cardOption);

        }
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentRequestBusinessDO.getPaymentType()) {
            netBankingCCACodeForBank = ccAvenueBankCodesFromUi.get(paymentRequestBusinessDO.getPaymentOption());
            retMap.put("netBankingCards", netBankingCCACodeForBank);
            retMap.put("cardOption", cardOption);
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CASHCARD) == paymentRequestBusinessDO.getPaymentType()) {
            netBankingCCACodeForBank = ccAvenueCashCardCodesFromUi.get(paymentRequestBusinessDO.getPaymentOption());
            retMap.put("CCRDType", netBankingCCACodeForBank);
            retMap.put("cardOption", cardOption);
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentRequestBusinessDO.getPaymentType()) {
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ATMCARD) == paymentRequestBusinessDO.getPaymentType()) {
            String atmcode = ccAvenueATMBankCodesFromUi.get(paymentRequestBusinessDO.getPaymentOption());
            if (atmcode != null) {
                nonMotoCardType = atmcode;
                retMap.put("cardOption", cardOption);
            }
        }

        Boolean isICICINetbanking = false;
        if (paymentRequestBusinessDO.getPaymentOption() == null)
            isICICINetbanking = false;
        else
            isICICINetbanking = PaymentConstants.ICICNETBANKING_PAYMENT_OPTION.equalsIgnoreCase(paymentRequestBusinessDO.getPaymentOption()) ? true : false;

        String checkSum = this.createAvenueChecksum(merchantOrderId, pgAmount, responseUrl,isICICINetbanking);


        retMap.put("Merchant_Id", isICICINetbanking ? ICICINETBANKING_MECODE : MECODE);
        retMap.put("Amount", pgAmount);
        retMap.put("Order_Id", merchantOrderId);
        retMap.put("Redirect_Url", responseUrl);
        retMap.put("Checksum", checkSum);
        retMap.put("NonMotoCardType", nonMotoCardType);
        retMap.put("billing_cust_name", "CustomerName");
        retMap.put("billing_cust_address", "BillingAddress");
        retMap.put("billing_cust_city", "BillingCity");
        retMap.put("billing_cust_state", "BillingState");
        retMap.put("billing_zip_code", "560001");
        retMap.put("billing_cust_country", "India");
        retMap.put("billing_cust_tel", PaymentConstants.NA);
        retMap.put("billing_cust_email", PaymentConstants.NA);
        retMap.put("billing_cust_notes", DEFAULT_DETAILS);
        retMap.put("delivery_cust_tel", PaymentConstants.NA);
        retMap.put("delivery_cust_name", "CustomerName");
        retMap.put("delivery_cust_address", "BillingDeliveryAddress");

        fs.getSessionData().put(merchantOrderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantOrderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantOrderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());
        fs.getSessionData().put(merchantOrderId + "_payment_option", paymentRequestBusinessDO.getPaymentOption());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, retMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);

        if (fcProperties.shouldMockPG()) {
        	logger.info("[MOCK] Putting /dummypg/ccavenue/pay as the payment gateway submit url");
			resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, host + "/dummypg/ccavenue/pay");
        } else {
        	if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentRequestBusinessDO.getPaymentType()
        			|| Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentRequestBusinessDO.getPaymentType()) {
        		resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, CCAVENUE_URL_OLD);
        	} else {
        		resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, CCAVENUE_URL);
        	}
        }

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(pgAmount));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantOrderId);
        paymentRequest.setOrderId(freeChargeOrderId);
        paymentRequest.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        paymentRequest.setPaymentMode("pur");
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(retMap));

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(amount));
        pt.setMerchantTxnId(merchantOrderId);
        pt.setOrderId(freeChargeOrderId);
        pt.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        pt.setPaymentMode("pur");
        pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        pt = createPaymentTransaction(pt);

        paymentRequestBusinessDO.setResultMap(resultMap);

        logger.debug("putting txn id in session : key : "+merchantOrderId+"_txn_id"+" Value : "+pt.getPaymentTxnId());
        fs.getSessionData().put(merchantOrderId+"_txn_id",pt.getPaymentTxnId());
        logger.debug("CCAvenue handler execution completes. Session Id : " + fs.getUuid());

        return resultMap;
    }

    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws  Exception {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        logger.info("CCAvenue response handling starts, parameters recieved are: " + requestMap);
        Boolean isSuccessful = false;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String mtxnId = requestMap.get("Order_Id");
        String transactionId = requestMap.get("nb_order_no");
        String amount = requestMap.get("Amount");
        String paymentResult = requestMap.get("AuthDesc");
        String checkSum = requestMap.get("Checksum");
        String responseCode = "";

        if (StringUtils.equalsIgnoreCase(paymentResult, CCAvenue_Success_Return_Value)) {
            isSuccessful = true;
            responseCode = "Transaction is Successful";
        } else {
            isSuccessful = false;
            responseCode = "Transaction failed";
        }

        String redirectUrl = null;
        String errorUrl = null;
        String mode = null;
        String productOrderId = null;
        Boolean errorExists = false;

        Boolean isICICINetbanking = false;
        Integer id = (Integer)fs.getSessionData().get(mtxnId+"_txn_id");
        productOrderId = FCSessionUtil.getProductOrderId(mtxnId, fs);
        //If id is not found in session get it from DB
        if (id == null || productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderIdAndPG(mtxnId, PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
            
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        redirectUrl = FCSessionUtil.getSuccessUrl(mtxnId, fs);
        errorUrl = FCSessionUtil.getErrorUrl(mtxnId, fs) + "&payment=fail";

        isICICINetbanking = FCSessionUtil.getPaymentOption(mtxnId, fs) == null ? false
                : PaymentConstants.ICICNETBANKING_PAYMENT_OPTION.equalsIgnoreCase(FCSessionUtil.getPaymentOption(
                        mtxnId, fs)) ? true : false;
        
        Boolean isChecksumEqual = createAvenueChecksum(mtxnId, amount, paymentResult, checkSum,isICICINetbanking);

        // MOCK PG: TBD: Remove this by calculating checksum in dummyPG
        if (fcProperties.shouldMockPG()) {
        	isChecksumEqual = true;
        }
        
        if (!isChecksumEqual) {
            errorExists = true;
            responseCode = "Checksum not matching, possible hacking case.";
        }

        if (errorExists) {
            isSuccessful = false;
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, isSuccessful.toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, productOrderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, mode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, MECODE);


        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);
        if (isSuccessful)
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);
        else
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, errorUrl);



        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(mtxnId);


        PaymentResponse paymentResponse = new PaymentResponse(null,isSuccessful,transactionId,productOrderId,mtxnId,PaymentUtil.getMapAsString(requestMap),
                new Double(amount),PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE,responseCode,null,null,null,null,null,PaymentUtil.getMapAsString(resultMap), null, null);

        paymentResponse = dumpResponsetAudit(paymentResponse);
        paymentResponse.setReconMetaData(new ReconMetaData(mtxnId, transactionId, "NA", PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE, transactionId));
        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(id);
        
        boolean paymentAlreadyAck= acknoledgePaymentTransactionUpdate(paymentTransaction, mtxnId, transactionId, id, paymentResponse,paymentResponseBusinessDO, false);
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED,
                Boolean.toString(paymentAlreadyAck));
        if (!paymentAlreadyAck) {
            Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId());
            Users users = userServiceProxy.getUserByUserId(userId);
        	paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), users.getUserId(), isSuccessful ? PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS : PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, false);
        }

        logger.info("CCAvenue handling response ends here." + mtxnId);

        return finalMap;

    }

    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws  Exception {
        refundLogger.info(String.format("Start - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(), PaymentGateways.CCAVENUE.getName()));
        Map<String, String> valuesMap = new HashMap<String, String>();
        PaymentTransactionBusinessDO pt = paymentRefundBusinessDO.getPaymentTransactionBusinessDO();
        String txnRefId = pt.getMerchantTxnId();

        String orderId = paymentRefundBusinessDO.getOrderId();
        Double amount = paymentRefundBusinessDO.getRefundAmount();

        Boolean isICICINetbanking = false;

        if (pt.getPaymentOption() == null)
            isICICINetbanking = false;
        else
            isICICINetbanking = PaymentConstants.ICICNETBANKING_PAYMENT_OPTION.equalsIgnoreCase(pt.getPaymentOption()) ? true : false;

        String checkSum = createAvenueChecksum(amount.toString(), txnRefId,isICICINetbanking);

        valuesMap.put("user_id", isICICINetbanking?ICICINETBANKING_MECODE : MECODE);
        valuesMap.put("order_no", txnRefId);
        valuesMap.put("amount", amount.toString());
        valuesMap.put("chk_sum", checkSum);

        String postData = PaymentUtil.createSubmitData(valuesMap, PaymentConstants.UTF_8_ENCODING);

        logger.info("Refund data posted to CCAvenue:" + postData);
        refundLogger.info("Refund data posted to CCAvenue:" + postData);

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest(postData);
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.REQUESTED);

        paymentTransactionService.create(paymentRefundTransaction);

        String postResponse = null;

        try {
            this.publishRefundAttempt(paymentRefundBusinessDO,PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
            // create a URL connection to the Virtual Payment Client
            postResponse = PaymentUtil.submitPost(CCAVENUE_REFUND_URL, postData, false, "192.168.1.1", 80);
                   
            paymentRefundBusinessDO.setRawPGResponse(postResponse);
            logger.info("Refund response from CCAvenue:" + postResponse);
            refundLogger.info("Refund response from CCAvenue:" + postResponse);
        } catch (Exception ex) {
            logger.error("Error in posting refund data to ccavenue for order id : " + orderId, ex);
            refundLogger.error("Error in posting refund data to ccavenue for order id : " + orderId, ex);
            paymentRefundBusinessDO.setStatus(false);

            this.publishRefundAttemptStatus(paymentRefundBusinessDO,PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
            paymentRefundBusinessDO.setMsg("No Reference Id passed");
            
            return paymentRefundBusinessDO;
        }

        Boolean isSuccessful = false;
        String refStatus = null;
        String refundString = null;
        Map<String, String> responseMap = this.convertRefundResponseToMap(postResponse);
        
        if (FCUtil.isEmpty(responseMap)) {
            refundLogger.error("Got an empty response from CCAvenue for order id " + paymentRefundBusinessDO.getOrderId());
        } else {
            refStatus = responseMap.get(REFUND_STATUS_RESPOND_PARAM);
            if (refStatus != null && refStatus.equals(REFUND_SUCCESS_RESPONSE)) {
                isSuccessful = true;
            }
            if (refStatus!=null) {
                refundString = this.getCCAvenueRefundStatusDesc(Integer.parseInt(refStatus));
            }
        }
        paymentRefundBusinessDO.setStatus(isSuccessful);
        paymentRefundBusinessDO.setMsg(refundString);
        this.publishRefundAttemptStatus(paymentRefundBusinessDO,PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);

        paymentRefundTransaction.setPgResponseCode(String.valueOf(refStatus));
        paymentRefundTransaction.setPgResponseDescription(refundString);
        paymentRefundTransaction.setPgStatus(String.valueOf(refStatus));
        //todo - fix this
        paymentRefundTransaction.setPgTransactionReferenceNo("");
        paymentRefundTransaction.setRawPGResponse(postResponse);
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        
        if (isSuccessful) {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.INITIATED);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refunded successfully");
        } else {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refund failed");
        }

        paymentRefundTransaction.addToWhereFields("paymentRefundTransactionId");
        paymentTransactionService.update(paymentRefundTransaction);

        refundLogger.info(String.format("End - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(), PaymentGateways.CCAVENUE.getName()));

        return paymentRefundBusinessDO;
    }
    
    
    public Map<String, String> convertRefundResponseToMap(String refundResponse) {
        // create a hash map for the response data
        refundResponse = refundResponse.trim();
        //todo - check why this method is used
        String[] splittedParams = StringUtils.splitPreserveAllTokens(refundResponse, "|");
        Map<String, String> map = new HashMap<String, String>(splittedParams.length);

        for (String splittedParam : splittedParams) {
            String[] keyValue = splittedParam.split("=");
            map.put(keyValue[0], keyValue[1]);
        }

        return map;
    }


    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
        logger.info("Initiating status check for CCA : " + paymentQueryBusinessDO.getOrderId());

        PaymentTransactionBusinessDO pt = paymentQueryBusinessDO.getPaymentTransactionBusinessDO();

        Boolean isICICINetbanking = PaymentConstants.ICICNETBANKING_PAYMENT_OPTION.equalsIgnoreCase(pt.getPaymentOption()) ? true : false;

        String txnRefId = paymentQueryBusinessDO.getMerchantTxnId();
        String orderId = paymentQueryBusinessDO.getOrderId();

        Map<String, String> paramterMap = new HashMap<String, String>();
        paramterMap.put("Order_Id", txnRefId);
        paramterMap.put("Merchant_Id", isICICINetbanking?ICICINETBANKING_MECODE : MECODE);

        String response = "";

        String submitData = PaymentUtil.createSubmitData(paramterMap, PaymentConstants.UTF_8_ENCODING);

        logger.info("Transaction status submit data for CCA is:" + submitData);

        try {
            // create a URL connection to the Virtual Payment Client
            response = PaymentUtil.submitPost(CCAVENUE_CHECK_STATUS_URL, submitData, false, "192.168.1.2", 80);
            logger.info("Transaction status response from CCA is:" + response);
        } catch (Exception ex) {
            logger.error("Exception in submitting request to url for order id : "+orderId,ex);
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
            return paymentQueryBusinessDO;
        }

        // create a hash map for the response data
        Map<String, String> responseFieldMap = PaymentUtil.createMapFromResponse(response, PaymentConstants.UTF_8_ENCODING);
        Boolean isSuccessful = false;
        String pgTxnId = responseFieldMap.get("nb_order_no");
        String status = responseFieldMap.get("AuthDesc");

        if (status == null){
            status = responseFieldMap.get("error");
        }
        String responseCode = "";
        if(CCAvenue_Success_Return_Value.equalsIgnoreCase(status))
        {
            isSuccessful = true;
            responseCode = "Transaction is Successful";
        } else {
            isSuccessful = false;
            responseCode = "Transaction failed";
        }

        paymentQueryBusinessDO.setPaymentStatus(isSuccessful);
        paymentQueryBusinessDO.setResponseMsg(status);
        paymentQueryBusinessDO.setPgTransactionId(pgTxnId);
        // CCA doesn't return any time so use current date.
        paymentQueryBusinessDO.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
        pt.setTransactionId(pgTxnId);

        PaymentResponse paymentResponse = new PaymentResponse(null,isSuccessful,pgTxnId,txnRefId,orderId,PaymentUtil.getMapAsString(responseFieldMap),
                paymentQueryBusinessDO.getAmount(),PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE,responseCode,null,null,null,null,null,PaymentUtil.getMapAsString(responseFieldMap), null, null);
        
        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
        paymentResponse.setReconMetaData(new ReconMetaData(txnRefId, pgTxnId, "NA", PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE, pgTxnId));
        PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(txnRefId);
        boolean paymentAlreadyAck= acknoledgePaymentTransactionUpdate(paymentTransaction, orderId, pgTxnId, paymentTransaction.getPaymentTxnId(), paymentResponse,null, isPaymentStatusForAdminPanel);

        if (!paymentAlreadyAck) {
            paymentResponse = dumpResponsetAudit(paymentResponse);
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, isPaymentStatusForAdminPanel);
        }

        logger.info("Status check complete for CCA : " + paymentQueryBusinessDO.getOrderId());
        return paymentQueryBusinessDO;
    }

    @PostConstruct
    public void loadConfigurations() {

        CCAvenue_Success_Return_Value = properties.getProperty(PaymentConstants.KEY_CCAVENUE_SUCCESS_DECISION);
        MECODE = properties.getProperty(PaymentConstants.KEY_CCAVENUE_MECODE);
        WORKING_KEY = properties.getProperty(PaymentConstants.KEY_CCAVENUE_WORKING_KEY);
        CCAVENUE_URL = properties.getProperty(PaymentConstants.KEY_CCAVENUE_URL);
        CCAVENUE_URL_OLD = properties.getProperty(PaymentConstants.KEY_CCAVENUE_URL_OLD);
        CCAVENUE_FREECHARGE_RETUERN_URL = properties.getProperty(PaymentConstants.KEY_CCAVENUE_RETURN_URL);
        CCAVENUE_REFUND_URL = properties.getProperty(PaymentConstants.KEY_CCAVENUE_REFUND_URL);
        CCAVENUE_CHECK_STATUS_URL = properties.getProperty(PaymentConstants.KEY_CCAVENUE_CHECK_STATUS_URL);
        ICICINETBANKING_WORKING_KEY = properties.getProperty(PaymentConstants.KEY_CCAVENUE_ICICINETBANKING_WORKING_KEY);
        ICICINETBANKING_MECODE = properties.getProperty(PaymentConstants.KEY_CCAVENUE_ICICINETBANKING_MECODE);
    }

    private String createAvenueChecksum(String amount, String merchantTxRefId, Boolean isICICNetbanking) {
        String str = "";
        if (isICICNetbanking == null || !isICICNetbanking)
            str = MECODE + "|" + amount + "|" + merchantTxRefId + "|" + WORKING_KEY;
        else
            str = ICICINETBANKING_MECODE + "|" + amount + "|" + merchantTxRefId + "|" + ICICINETBANKING_WORKING_KEY;

        Adler32 adl = new Adler32();
        adl.update(str.getBytes());
        return String.valueOf(adl.getValue());
    }

    private String createAvenueChecksum(String merchantTxRefId, String amount, String redirectURL,Boolean isICICNetbanking) {
        String str = "";
        if (isICICNetbanking == null || !isICICNetbanking)
            str = MECODE + "|" + merchantTxRefId + "|" + amount + "|" + redirectURL + "|" + WORKING_KEY;
        else
            str = ICICINETBANKING_MECODE + "|" + merchantTxRefId + "|" + amount + "|" + redirectURL + "|" + ICICINETBANKING_WORKING_KEY;

        Adler32 adl = new Adler32();
        adl.update(str.getBytes());
        return String.valueOf(adl.getValue());
    }

    private boolean createAvenueChecksum(String merchantTxRefId, String amount, String authDesc, String checkSum,Boolean isICICNetbanking) {
        String str = "";
        if (isICICNetbanking == null || !isICICNetbanking)
            str = MECODE + "|" + merchantTxRefId + "|" + amount + "|" + authDesc + "|" + WORKING_KEY;
        else
            str = ICICINETBANKING_MECODE + "|" + merchantTxRefId + "|" + amount + "|" + authDesc + "|" + ICICINETBANKING_WORKING_KEY;

        Adler32 adl = new Adler32();
        adl.update(str.getBytes());

        String newChecksum = String.valueOf(adl.getValue());
        return newChecksum.equals(checkSum);
    }


    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {

        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }

    private PaymentResponse dumpResponsetAudit(PaymentResponse paymentResponse) {

        try {
           paymentResponse =  paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;

    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
           pt =  paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :"+pt.getOrderId(),exp);
        }
        return pt;
    }

    private String getCCAvenueRefundStatusDesc(int status) {
        switch (status) {
            case -2:
                return "Invalid Parameters";
            case -1:
                return "Order not found";
            case 0:
                return "Refunded Successfully";
            case 1:
                return "Duplicate request";
            case 2:
                return "Error/declined transaction.";
            case 3:
                return "Refund amount is greater than transaction amount";
            case 4:
                return "Transaction older than 90 days";
            case 5:
                return "Transaction not captured by merchant";
            case 6:
                return "Temporary Issue in Refund Please contact CCAvenue";
            case 100:
                return "Internal Error";
            case 101:
                return "Checksum mismatch";
            case 102:
                return "Invalid user_id";
            default:
                throw new AssertionError("New status code found in response status");

        }

    }

    private FCProperties properties;
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    public void setProperties(FCProperties properties) {
        this.properties = properties;
    }

	@Override
	public Map<String, Object> handleS2SPaymentResponse(
			PaymentResponseBusinessDO paymentResponseBusinessDO)
			throws Exception {
		throw new RuntimeException("Unimplemented method");
	}

	@Override
    @Transactional
	public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws  Exception {
        refundLogger.debug("Checking whether transaction is on:" + TransactionSynchronizationManager.isActualTransactionActive());
        //SNS alert for Refund Attempt
    	walletGatewayHandler.refundToWallet(paymentRefundBusinessDO, refundType);
	}

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        return null;
    }

    @Override
    protected void validatePaymentResponse(Map<String, String> payResponseMap) {
        
    }

    @Override
    protected PaymentResponse createPaymentResponse(Map<String, String> payResponseMap) {
        return null;
    }

    @Override
    protected String getPaymentPortalMerchantId() {
        return null;
    }

    @Override
    protected String getMerchantTransactionId(Map<String, String> payResponseMap) {
        return null;
    }
}
