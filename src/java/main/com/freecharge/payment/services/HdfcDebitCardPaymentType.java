package com.freecharge.payment.services;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 26/12/12
 * Time: 6:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class HdfcDebitCardPaymentType extends DebitCardPaymentType {
    //A new payment gateway has been configured using BillDesk for HDFC promo code that taken in only HDFC debit card
    @Transactional(readOnly = true)
    public IGatewayHandler getPaymentGateway(PaymentRequestBusinessDO obj){
        return pgFactory.getPaymentHandler(IGatewayHandler.PaymentGateways.BILLDESK.getName());
    }
}
