package com.freecharge.payment.services;

public enum RefundStatus {
    SCHEDULED(1), IN_PROGRESS(2), FAILED(3), COMPLETED(0);

    private int status;

    private RefundStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
    
    public static RefundStatus fromCode(int status) {
        switch(status) {
            case 1:
                return SCHEDULED;
            case 2:
                return IN_PROGRESS;
            case 3:
                return FAILED;
            case 0:
                return COMPLETED;
            default:
                throw new IllegalStateException("Unknown Refund state " + status);
        }
    }
}