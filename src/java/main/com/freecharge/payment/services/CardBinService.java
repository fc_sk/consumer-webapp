package com.freecharge.payment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.payment.dao.CardBinDao;

@Service
public class CardBinService {

    @Autowired
    CardBinDao cardBinDao;
    
    public boolean saveCardDetailsForOrder(String mtxnId, PaymentCard paymentCard){    
        return cardBinDao.saveCardDataForPaymentTxn(mtxnId, paymentCard);
    }
    
    public PaymentCard getPaymentCardForOrderId(String mtxnId){
        return cardBinDao.getCardRelatedDataForPaymentTxn(mtxnId);
    }
}
