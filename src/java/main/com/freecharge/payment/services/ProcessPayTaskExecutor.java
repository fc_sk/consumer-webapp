package com.freecharge.payment.services;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.cartcheckout.CartCheckoutClientService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.order.checkout.exception.ServiceException;
import com.freecharge.order.checkout.request.OndeckProcessOrderRequest;
import com.freecharge.order.checkout.response.ProcessOrderResponse;
import com.freecharge.order.checkout.service.IOrderCheckoutService;
import com.freecharge.payment.dos.entity.ProcessPayTaskRequest;
import com.freecharge.payment.util.PaymentConstants;
import com.snapdeal.payments.ts.registration.TaskExecutor;
import com.snapdeal.payments.ts.registration.TaskRequest;
import com.snapdeal.payments.ts.response.ExecutorResponse;
import com.snapdeal.payments.ts.response.RetryInfo;

@Component
public class ProcessPayTaskExecutor implements TaskExecutor<TaskRequest> {

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private IOrderCheckoutService orderCheckoutService;

	@Autowired
	private CartCheckoutClientService service;

	public ExecutorResponse execute(TaskRequest taskRequest) {
		ExecutorResponse execResponse = new ExecutorResponse();
		ProcessPayTaskRequest processPayTaskRequest = (ProcessPayTaskRequest) taskRequest;
		OndeckProcessOrderRequest processOrderRequest = processPayTaskRequest
				.getOndeckProcessOrderRequest();
		try {
			logger.info("Executing ProcessPayTaskExecutor for mtxnId "
					+ processOrderRequest.getMtxnId() + " lookup id "
					+ processPayTaskRequest.getLookupId());
			String checkoutId = service
					.getCheckoutIdFromLookupId(processPayTaskRequest
							.getLookupId());
			processOrderRequest.setCheckoutId(checkoutId);
			ProcessOrderResponse response = orderCheckoutService
					.processOnDeckOrder(processOrderRequest);
			execResponse.setCompletionLog(response.getOrderId());
			execResponse.setStatus(ExecutorResponse.Status.valueOf(response
					.getStatus()));
			logger.info("ProcessPayTaskExecutor created processOnDeckOrder successfully for mtxnId" + processOrderRequest.getMtxnId());
			return execResponse;
		} catch (ServiceException e) {
			logger.error(
					"processOnDeckOrder throwing ServiceException "
							+ processOrderRequest.toString(), e);
			logger.info("error code for execute processOnDeckOrder " + e.getErrCode() + " error message " + e.getErrMsg() +  " for mtxn id " + processOrderRequest.getMtxnId());
			if (!StringUtils.isEmpty(e.getErrCode())
					&& e.getErrCode().equalsIgnoreCase(
							PaymentConstants.TASK_ALREADY_CREATED)) {
				execResponse.setCompletionLog(e.getMessage());
				execResponse.setStatus(ExecutorResponse.Status.SUCCESS);
				return execResponse;
			}
		} catch (Exception e) {
			logger.error("processOnDeckOrder throwing Exception for mtxnId"
					+ processOrderRequest.getMtxnId(), e);
		}
		RetryInfo retryInfo = new RetryInfo();
		retryInfo.setType(RetryInfo.RetryType.EXPONENTIAL);
		retryInfo.setExpBase(PaymentConstants.TASK_EXPONENTIAL_BASE);
		retryInfo
				.setWaitTime(PaymentConstants.PROCESS_PAY_TASK_FAILURE_REEXECUTION_WAIT_TIME);
		execResponse.setAction(retryInfo);
		execResponse.setCompletionLog("Exception");
		execResponse.setStatus(ExecutorResponse.Status.RETRY);
		return execResponse;
	}
}
