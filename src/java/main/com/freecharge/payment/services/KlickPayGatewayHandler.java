package com.freecharge.payment.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import com.freecharge.util.CustomHttpClientBuilder;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.cardstorage.CardDetailsDo;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.KlickPayConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.business.PaymentTransactionBusinessDO;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.web.PaymentHealthResponse;
import com.freecharge.payment.util.KlickPayUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.model.BaseFPSParams;
import com.freecharge.rest.model.FPSParams;
import com.freecharge.rest.model.UserAgentEntity;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.OneCheckWalletService.OneCheckTxnType;
import com.freecharge.web.util.WebConstants;
import com.klickpay.payment.FCPayUtil;
import com.klickpay.payment.model.PaymentRefundInput;
import com.klickpay.payment.model.PaymentStatusRequest;
import com.klickpay.payment.model.PaymentStatusResponse;

import flexjson.JSONDeserializer;
import nl.bitwalker.useragentutils.UserAgent;

@Component("klickPayGatewayHandler")
public class KlickPayGatewayHandler extends PaymentGatewayHandler
        implements CardGatewayHandler, QuickCheckoutHandler, DebitATMStatusHandler, CardStatusHandler, CardDataHandler, ITTPHandler,TransactionTypeHandler,UPIPaymentStatusHandler {

    public static final String                   KP_COOKIE_NAME            = "pay_fc";
    SimpleDateFormat                              formatter                 = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    private static final String                   KLICKPAY_SBI_MAESTRO_CODE = "MAES_S";

    private static final String                   KLICKPAY_RUPAY_CODE       = "RPAY";

    private static final String                   KLICKPAY_MAESTRO_CODE     = "MAES";

    private static final String                   KLICKPAY_MASTERCARD_CODE  = "MC";

    private static final String                   KLICKPAY_VISA_CODE        = "VI";

    private static final String                   KLICKPAY_DEBIT_CARD_CODE  = "DC";

    private static final String                   KLICKPAY_CREDIT_CARD_CODE = "CC";

    public static final String                   KP_COOKIE_KEY             = "kp_cookie";

    public static final String                   KP_COOKIE_DOMAIN_KEY      = "kp_cookie_domain";

    public static final String                   KP_S2S_KEY                = "kp_s2s";

    public static final String                   KP_REFERENCE_ID_KEY       = "kp_ref_id";

    public static final String 					 TLS_V_1_2 				   = "TLSv1.2";
    
    public static final String 					 STATUS 				   = "status";
    
    public static final String 					 UPSTATUS 				   = "up_status";
    
    public static final String 					 MSSG 				   = "mssg";

    private static final Logger                   LOGGER                    = LoggerFactory
            .getLogger(KlickPayGatewayHandler.class);

    private JSONDeserializer<Map<String, Object>> deserializer              = new JSONDeserializer<Map<String, Object>>();

    @Autowired
    @Qualifier("klickPayConnectionManager")
    private MultiThreadedHttpConnectionManager    klickPayConnectionManager;

    @Autowired
    AppConfigService                              configService;

    @Autowired
    OrderIdSlaveDAO                               orderIdSlaveDAO;
    
    @Autowired
    OrderIdWriteDAO                               orderIdWriteDAO;

    @Autowired
    UserServiceProxy                              userServiceProxy;

    @Autowired
    OneCheckWalletService                         oneCheckWalletService;

    @Autowired
    private KpayPaymentMetaService                kpayPaymentMetaService;

    @Autowired
    private KpayPaymentMetaDAO                    kpayPaymentMetaDAO;

    @Autowired
    private MetricsClient                         metricsClient;

    @Autowired
    private PaymentOptionService                  paymentOptionService;

    @Autowired
    PaymentsCache                                 paymentsCache;

    @Autowired
    @Qualifier("paymentsExecutor")
    ThreadPoolTaskExecutor                        paymentsExecutor;

    @Autowired
    KlickPayUtil                                  klickPayUtil;

    private static final List<String>             BLACKLISTED_KEYS          = new ArrayList<String>(
            Arrays.asList(RequestConstants.PARAM_CARD_NUMBER, RequestConstants.PARAM_CARD_CVV,
                    RequestConstants.PARAM_CARD_EXPIRY_MONTH, RequestConstants.PARAM_CARD_EXPIRY_YEAR));

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws Exception {
        LOGGER.debug("Constructing request data for Klickpay PG");
        boolean isITTPTxn = false;
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String,String> finalTransactionStatusmap = new HashMap<>();
        String[] credentials = klickPayUtil.getKlickPayCredentials(
                orderIdWriteDAO.getProductName(paymentRequestBusinessDO.getOrderId()).getProductType());
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        Map<String, Object> sessionData = fs.getSessionData();

        String userId = ((Integer) sessionData.get(WebConstants.SESSION_USER_USERID)).toString();
        String orderId = paymentRequestBusinessDO.getOrderId();
        Integer paymentType = paymentRequestBusinessDO.getPaymentType();
        String merchantTransactionId = paymentRequestBusinessDO.getMtxnId();
        paymentRequestBusinessDO.setMtxnId(merchantTransactionId);

        // Saving the merchant id (MID) used in the payment call, in redis.
        // This will be used when the response is obtained from Kpay to store in klickpay_payment_meta.
        logger.info("Storing merchantId: "+merchantId+", for mtxnId: "+merchantTransactionId);
        paymentsCache.setMerchantIdForKpayPayment(merchantTransactionId, merchantId,
                Integer.parseInt(fcProperties.getProperty(FCProperties.KPAY_MERCHANT_ID_REDIS_TIMEOUT_DAYS)));
        LOGGER.info("About to process payments for Klickpay for order ID : [" + orderId + "], MtxnId: ["
                + merchantTransactionId + "]");

        String responseUrl = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME)
                + RequestConstants.RESPONSE_URL;
        if (MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO)) {
            responseUrl = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME)
                    + RequestConstants.MOBILE_PAYMENT_RESPONSE_URL;
        }
        Map<String, Object> paymentRequestMap = new HashMap<String, Object>();
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_ID, merchantId);
        paymentRequestMap.put(RequestConstants.PARAM_MERCHANT_TXN_ID, merchantTransactionId);
        Double chargeAmount = Double.parseDouble(paymentRequestBusinessDO.getAmount());
        paymentRequestMap.put(RequestConstants.PARAM_TXN_AMOUNT, Double.toString(chargeAmount));
        paymentRequestMap.put(RequestConstants.PARAM_PRODUCT_INFO, paymentRequestBusinessDO.getProductId() == null ? ""
                : paymentRequestBusinessDO.getProductId().toString());
        String contactNo = null;
        String customerEmail = null;
        Users user = paymentRequestBusinessDO.getUser();
        if (user != null) {
            contactNo = user.getMobileNo();
            customerEmail = user.getEmail();
        }
        if (StringUtils.isEmpty(contactNo)) {
            contactNo = "6666666666";
        }
        if (StringUtils.isEmpty(customerEmail)) {
            customerEmail = "a@b.com";
        }
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_NAME, "NA");
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_EMAIL, customerEmail);
        paymentRequestMap.put(RequestConstants.PARAM_CUSTOMER_CONTACT, contactNo);
        paymentRequestMap.put(RequestConstants.PARAM_CALLBACK_URL, responseUrl);
        paymentRequestMap.put(RequestConstants.PARAM_BLACK_PAYMENTS, Constants.EXCLUSION_LIST);

        if (userId != null && !userId.trim().isEmpty()) {
            String cardStorageKey = userId;
            paymentRequestMap.put(RequestConstants.PARAM_USER_ID, fcProperties.getMode() + "-" + cardStorageKey);
        }
        
        String cardHolderName = null;
        String cardNumber = null;
        String cardExpMon = null;
        String cardExpYear = null;
        String cardCvv = null;
        String paymentTypeCode = null;
        String cardType = null;

        if (StringUtils.isEmpty(paymentRequestBusinessDO.getPaymentOption())) {
            paymentRequestBusinessDO.setPaymentOption(PaymentConstants.VISA_DC);
        }
        final String paymentOption = paymentRequestBusinessDO.getPaymentOption();
        String metaData = "";
        StringBuilder transactionStatusString=new StringBuilder();
        String email = userServiceProxy.getEmailByUserId(Integer.parseInt(userId));
        String oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
        UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                .findUserTransactionHistoryByOrderId(orderId);
        Map<String, Object> walletMetaDataMap = paymentTransactionService.getWalletMetaMap(userTransactionHistory,
                orderId);
        if (oneCheckUserId != null && !oneCheckUserId.trim().isEmpty()) {
            paymentRequestMap.put(RequestConstants.PARAM_USER_ID, oneCheckUserId);
            metaData += "is_onecheck_user=true";
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentType) {
            paymentTypeCode = Constants.NETBANKING_PG;
            cardType = paymentOption;

            if (paymentOption.equals(PaymentConstants.AMEX_PAYMENT_OPTION)) {
                paymentTypeCode = "EZEC";
                cardType = "EZEC";
            }

            // For ICICI NB transactions, set QuickCheckout flag
            if (paymentOption.equals("ICIC")) {
                LOGGER.info("QC option : "
                        + paymentRequestBusinessDO.getRequestMap().containsKey(RequestConstants.IS_ICICI_QC_TXN));
                if (paymentRequestBusinessDO.getRequestMap() != null
                        && paymentRequestBusinessDO.getRequestMap().containsKey(RequestConstants.IS_ICICI_QC_TXN)
                        && Boolean.parseBoolean((String) paymentRequestBusinessDO.getRequestMap()
                                .get(RequestConstants.IS_ICICI_QC_TXN))) {
                    if (!StringUtils.isEmpty(metaData)) {
                        metaData += "|";
                    }
                    metaData += RequestConstants.IS_ICICI_QC_TXN + "=true";
                }
            }

            // For UPI payments
            if (paymentOption.equals("UPI")) {
                paymentTypeCode = "UPI";
                cardType = "UPI";
            }
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ATMCARD) == paymentType) {
            paymentTypeCode = Constants.ATM_PG;
            cardType = paymentOption;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CASHCARD) == paymentType) {
            paymentTypeCode = Constants.CASHCARD_PG;
            cardType = paymentOption;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType) {
            String eventContext = this.getEventContext(walletMetaDataMap);
            metaData += "|reference_id=" + orderId;
            metaData += "|idempotency_id=" + merchantTransactionId + "_" + OneCheckTxnType.DEBIT_RECHARGE;
            metaData += "|event_context=Wallet Debit for " + merchantTransactionId + " " + eventContext;
            metaData += "|bussiness_entity=FREECHARGE";
            if (oneCheckUserId != null && !oneCheckUserId.trim().isEmpty()) {
                paymentRequestMap.put(RequestConstants.PARAM_USER_ID, oneCheckUserId);
            }
            paymentTypeCode = Constants.ONE_CHECK_WALLET_PG;
            cardType = Constants.ONE_CHECK_WALLET_PG;
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentType
                || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentType) {
            cardHolderName = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardHolderName();
            cardNumber = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardNumber();
            cardExpMon = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpMonth();
            cardExpYear = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpYear();
            cardCvv = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardCvv();


            if(null!=paymentRequestBusinessDO.getTransactionTypeStatus()){
                for(String key :paymentRequestBusinessDO.getTransactionTypeStatus().keySet()){
                    transactionStatusString.append(key+"="+paymentRequestBusinessDO.getTransactionTypeStatus().get(key));
                    transactionStatusString.append("|");
                    finalTransactionStatusmap.put(key,paymentRequestBusinessDO.getTransactionTypeStatus().get(key));
                    }
                if(transactionStatusString.length()>0)
                    transactionStatusString.deleteCharAt(transactionStatusString.length()-1);
            }

            logger.info("finalTransactionStatusmap  is:"+finalTransactionStatusmap);
            logger.info("transactionStatusString is :"+transactionStatusString.toString());



            //Very hacky fix
            //Ideally frontend should call creditCard API for AMEX cards
            if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentType || (cardNumber.startsWith("34") || cardNumber.startsWith("37"))) {
                if (cardNumber.startsWith("34") || cardNumber.startsWith("37")) {
                    // Amex cards
                    paymentTypeCode = Constants.AMEX_CARD_PG;
                    cardType = Constants.AMEX_CARD_PG;
                } else if (cardNumber.startsWith("2") || cardNumber.startsWith("3")) {
                    // DINERS cards
                    paymentTypeCode = Constants.DINERS_CARD_PG;
                    cardType = Constants.DINERS_CARD_PG;
                } else {
                    paymentTypeCode = Constants.CREDIT_CARD_PG;
                    cardType = paymentOption;
                }
                if (paymentRequestBusinessDO.getRequestMap().containsKey(PaymentConstants.IS_ITTP_TXN) && Boolean
                        .parseBoolean((String) paymentRequestBusinessDO.getRequestMap().get(PaymentConstants.IS_ITTP_TXN))) {
                    if (!StringUtils.isEmpty(metaData)) {
                        metaData += "|";
                    }
                    metaData += PaymentConstants.IS_ITTP_TXN+"=true";
                    isITTPTxn = true;
                }
            } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentType) {
                paymentTypeCode = Constants.DEBIT_CARD_PG;
                if (paymentOption.equals(PaymentConstants.VISA_DC)) {
                    cardType = KLICKPAY_VISA_CODE;
                } else if (paymentOption.equals(PaymentConstants.MASTER_DC)) {
                    cardType = KLICKPAY_MASTERCARD_CODE;
                }

                if (PaymentConstants.MAESTRO_SBI.equals(paymentOption)
                        || PaymentConstants.MAESTRO_CITI.equals(paymentOption)) {
                    if (StringUtils.isBlank(cardExpYear) || StringUtils.isBlank(cardExpMon)) {
                        cardExpYear = PaymentConstants.MAESTRO_DEFAULT_EXPIRY_YEAR;
                        cardExpMon = PaymentConstants.MAESTRO_DEFAULT_EXPIRY_MONTH;
                    }

                    if (!finalTransactionStatusmap.containsKey(WebConstants.CVVLESS_TXN_TYPE) && !Boolean.valueOf(finalTransactionStatusmap.get(WebConstants.CVVLESS_TXN_TYPE))
                            && StringUtils.isBlank(cardCvv)) {
                        cardCvv = PaymentConstants.MAESTRO_BD_DEFAULT_CVV;
                    }

                    if (PaymentConstants.MAESTRO_SBI.equals(paymentOption)) {
                        cardType = KLICKPAY_SBI_MAESTRO_CODE;
                    } else {
                        cardType = KLICKPAY_MAESTRO_CODE;
                    }
                }

                // If the user has opted for Debit Card + ATM Pin transaction,
                // we need to pass "is_dc_pin" flag as true in metadata to
                // Klickpay
                if (paymentRequestBusinessDO.getRequestMap().containsKey("is_debit_atm_pin") && Boolean
                        .parseBoolean((String) paymentRequestBusinessDO.getRequestMap().get("is_debit_atm_pin"))) {
                    cardCvv = "111"; // Default CVV as this is not required for
                                     // Debit + ATM Pin payment
                    if (!StringUtils.isEmpty(metaData)) {
                        metaData += "|";
                    }
                    metaData += "is_dc_pin=true";

                }
                
                // If the user has opted for ITTP transaction,
                // we need to pass "is_ittp" flag as true in metadata to
                // Klickpay
                if (paymentRequestBusinessDO.getRequestMap().containsKey(PaymentConstants.IS_ITTP_TXN) && Boolean
                        .parseBoolean((String) paymentRequestBusinessDO.getRequestMap().get(PaymentConstants.IS_ITTP_TXN))) {
                    if (!StringUtils.isEmpty(metaData)) {
                        metaData += "|";
                    }
                    metaData += PaymentConstants.IS_ITTP_TXN+"=true";
                    isITTPTxn = true;
                }
            }
        }
        if (StringUtils.isEmpty(cardType)) {
            cardType = KLICKPAY_VISA_CODE;
        }
        if (StringUtils.isEmpty(paymentTypeCode)) {
            paymentTypeCode = Constants.DEBIT_CARD_PG;
        }

        String gatewayToUse = "payu";
        if (!(paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_ANDROID_APP
                && paymentRequestBusinessDO.getAppVersion() < 33)) {
            JSONObject jsonObject = configService.getCustomProp(PaymentConstants.KLICKPAY);
            if (jsonObject != null) {
                String billdeskCondition = (String) jsonObject.get(PaymentConstants.KLICKPAY_BDK);
                if (billdeskCondition != null) {
                    Integer billdeskPercentage = Integer.parseInt(billdeskCondition);
                    final int randomSampleInt = RandomUtils.nextInt(9999);
                    if (randomSampleInt % billdeskPercentage == 0) {
                        gatewayToUse = "bdk";
                    }
                }
            }
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentType
                || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CASHCARD) == paymentType) {
            if (!StringUtils.isEmpty(paymentRequestBusinessDO.getGateWayToUse())) {
                gatewayToUse = paymentRequestBusinessDO.getGateWayToUse();
            }
        }

        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ATMCARD) == paymentType)
            gatewayToUse = "bdk";

        paymentRequestMap.put(RequestConstants.PARAM_GATEWAY_TO_USE, gatewayToUse);
        if (StringUtils.isEmpty(paymentTypeCode)) {
            paymentTypeCode = Constants.DEBIT_CARD_PG;
        }
        paymentRequestMap.put(RequestConstants.PARAM_PAYMENT_TYPE, paymentTypeCode);
        if (StringUtils.isEmpty(cardType)) {
            paymentTypeCode = KLICKPAY_VISA_CODE;
        }
        paymentRequestMap.put(RequestConstants.PARAM_CARD_TYPE, cardType);

        if (StringUtils.isNotEmpty(cardHolderName)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_HOLDER_NAME, cardHolderName);
        } else {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_HOLDER_NAME, "UNKNOWN");
        }
        if (StringUtils.isNotEmpty(cardNumber)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_NUMBER, cardNumber);
        }
        if (StringUtils.isNotEmpty(cardExpMon)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_EXPIRY_MONTH,
                    PaymentUtil.getExpMonthInReqFormat(cardExpMon));
        }
        if (StringUtils.isNotEmpty(cardExpYear)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_EXPIRY_YEAR, cardExpYear);
        }
        if (StringUtils.isNotEmpty(cardCvv)) {
            paymentRequestMap.put(RequestConstants.PARAM_CARD_CVV, cardCvv);
        }

        paymentRequestMap.put(RequestConstants.PARAM_CARD_TOKEN, "");
        paymentRequestMap.put(RequestConstants.PARAM_CHECKSUM, getRequestChecksum(paymentRequestMap, merchantKey));

        paymentRequestMap.put(RequestConstants.PARAM_CHANNEL, getChannelName(paymentRequestBusinessDO));
        String s2sResponseUrl = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME)
                + RequestConstants.S2S_RESPONSE_URL;
        paymentRequestMap.put(RequestConstants.PARAM_CALLBACK_URL1, s2sResponseUrl);
        paymentRequestMap.put(RequestConstants.PARAM_CURRENCY, "INR");
        paymentRequestMap.put(RequestConstants.PARAM_METADATA, metaData);
        paymentRequestMap.put(RequestConstants.TRANSACTION_TYPE_MAP,transactionStatusString.toString());
        paymentRequestMap.put(RequestConstants.PARAM_SAVE_CARD,
                Boolean.toString(paymentRequestBusinessDO.getPaymentRequestCardDetails().isSaveCard()));
        String savedCardName = paymentRequestBusinessDO.getPaymentRequestCardDetails().getSaveCardName();
        if (savedCardName != null && !savedCardName.isEmpty()) {
            // Handling case for iOS payments where saved_card_name comes as
            // <null>
            savedCardName = savedCardName.replace("<", "").replace(">", "");
        }
        if (StringUtils.isEmpty(savedCardName)) {
            savedCardName = "";
        }
        paymentRequestMap.put(RequestConstants.PARAM_SAVE_CARD_NAME, savedCardName);

        LOGGER.info("Payment request map is : " + serializePayRequest(paymentRequestMap));
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, gatewayToUse);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantTransactionId);
        boolean useS2s = transactUsingS2S(paymentRequestBusinessDO);
        
        createPaymentRequestAndPaymentTransaction(paymentRequestBusinessDO, merchantTransactionId, orderId,
            paymentOption, paymentRequestMap, fs, isITTPTxn, useS2s, resultMap, finalTransactionStatusmap);
        
        //if it is an ittp request then even if it is non-s2s, we should make an init call to Kpay to get ITTP response.
        if (!useS2s && !isITTPTxn) {
            // MOCK PG
            resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, paymentRequestMap);
            if (fcProperties.shouldMockPG()) {
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                        fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME) + "/dummypg/payu/pay");
            } else {
                resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
                        fcProperties.getProperty(PaymentConstants.KLICKPAY_SUBMIT_URL));
            }
            resultMap.put(PaymentConstants.UPI_PAYMENT_FLAG, "false");
            if (paymentTypeCode.equals("UPI")) {
                resultMap.put(PaymentConstants.UPI_PAYMENT_FLAG, "true");
            }
        } else {
            LOGGER.info("Performing Klickpay initiation S2S call for + " + paymentRequestBusinessDO.getPaymentType());
            //Map<String, String> metadataMap = getPaymentRequestMetadata(paymentRequestMap);
            //paymentRequestMap.put(RequestConstants.PARAM_METADATA, metadataMap);
            //paymentRequestMap.put(RequestConstants.TRANSACTION_TYPE_MAP,finalTransactionStatusmap);
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String submitData = ow.writeValueAsString(paymentRequestMap);
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Request");
            Map<String, Object> wsResponse = makeWSCall(submitData,fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_INIT_URL), 1000, 90000, true);
            LOGGER.info("Klickpay Init response is : " + wsResponse);
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Response");
            if (wsResponse != null) {
                wsResponse.put(PaymentConstants.MERCHANT_ORDER_ID, orderId);
                String status = (String) wsResponse.get(ResponseConstants.PARAM_STATUS);
                String kpReferenceId = (String) wsResponse.get(ResponseConstants.PARAM_REFERENCE_ID);
                fs.getSessionData().put(KP_REFERENCE_ID_KEY, kpReferenceId);
                if (!StringUtils.isEmpty(status) && status.equals("E000") && wsResponse.containsKey("pg_url")) {
                    metricsClient.recordEvent("Payment", "KP.S2S.Init", "Success");
                    resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                            getResponseDataMap((Map<String, String>) wsResponse.get("request_data"),
                                    paymentRequestBusinessDO.getChannel()));
                    resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, wsResponse.get("pg_url"));
                    resultMap.put(PaymentConstants.UPI_PAYMENT_FLAG, "false");
                    if (paymentTypeCode.equals("UPI")) {
                        resultMap.put(PaymentConstants.UPI_PAYMENT_FLAG, "true");
                    }
                } else {
                    metricsClient.recordEvent("Payment", "KP.S2S.Init", "Fail");
                    resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
                }
            }
        }
        paymentRequestBusinessDO.setResultMap(resultMap);
        LOGGER.info("Done processing payments for Klickpay for order ID : " + orderId);
        return resultMap;
    }
    
    private void createPaymentRequestAndPaymentTransaction(PaymentRequestBusinessDO paymentRequestBusinessDO,
        String merchantTransactionId, String orderId, String paymentOption, Map<String, Object> paymentRequestMap,
        FreechargeSession fs, boolean isITTPTxn, boolean useS2s, Map<String, Object> resultMap, Map<String,String> finalTransactionStatusmap) throws Exception {
        PaymentRequest paymentRequest = new PaymentRequest();
        try{
        paymentRequest.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantTransactionId);
        paymentRequest.setOrderId(orderId);
        paymentRequest.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        paymentRequest.setPaymentMode("pur");
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(serializePayRequest(paymentRequestBusinessDO.getRequestMap()));
        if (useS2s || isITTPTxn) {
            Map<String, String> metadataMap = getPaymentRequestMetadata(paymentRequestMap);
            paymentRequestMap.put(RequestConstants.PARAM_METADATA, metadataMap);
            paymentRequestMap.put(RequestConstants.TRANSACTION_TYPE_MAP,finalTransactionStatusmap);
        }
        paymentRequest.setRequestToGateway(serializePayRequest(paymentRequestMap));

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        pt.setMerchantTxnId(merchantTransactionId);
        pt.setOrderId(orderId);
        pt.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        pt.setPaymentMode("pur");
        pt.setPaymentOption(paymentOption);
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        pt = createPaymentTransaction(pt);

        fs.getSessionData().put(merchantTransactionId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantTransactionId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantTransactionId + "_error_url", paymentRequestBusinessDO.getErrorUrl());
        fs.getSessionData().put(merchantTransactionId + "_txn_id", pt.getPaymentTxnId());
        fs.getSessionData().put(KP_S2S_KEY, Boolean.toString((isITTPTxn)?isITTPTxn:useS2s));
        } catch(Exception e){
            LOGGER.error("payment request data not inserted in db " + paymentRequest.getOrderId(), e);
            throw e;
        }
    }
    
    private Map<String, String> getPaymentRequestMetadata(Map<String, Object> paymentRequestMap) {
        String metadata = (String) paymentRequestMap.remove(RequestConstants.PARAM_METADATA);
        paymentRequestMap.remove(RequestConstants.TRANSACTION_TYPE_MAP);
        Map<String, String> metadataMap = new HashMap<>();
        if (!StringUtils.isEmpty(metadata)) {
            String[] metadataPairs = metadata.split("\\|");
            for (String pair : metadataPairs) {
                String[] keyValue = pair.split("=");
                metadataMap.put(keyValue[0], keyValue[1]);
            }
        }
        return metadataMap;
    }

    private boolean transactUsingS2S(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        boolean useS2s = false;
        if (paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_WINDOWS_APP
                || paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_IOS_APP
                || paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_UWP_APP) {
            return useS2s;
        }

        if (paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_ANDROID_APP
                && paymentRequestBusinessDO.getAppVersion() < 33) {
            return useS2s;
        }
        JSONObject s2sProp = configService.getCustomProp("kp_s2s");
        if (s2sProp != null) {
            String enabledKey = (String) s2sProp.get("is_enabled");
            if (!StringUtils.isEmpty(enabledKey) && Boolean.parseBoolean(enabledKey)) {
                int paymentType = paymentRequestBusinessDO.getPaymentType();
                String s2sPerc = (String) s2sProp.get("perc");
                if (paymentType == Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD)
                        || paymentType == Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD)) {
                    s2sPerc = (String) s2sProp.get("cards.perc");
                }
                System.out.println("s2sperc: "+s2sPerc);
                if (!StringUtils.isEmpty(s2sPerc)) {
                    Integer condition = Integer.parseInt(s2sPerc);
                    final int randomSampleInt = RandomUtils.nextInt(9999);
                    if (randomSampleInt % condition == 0) {
                        useS2s = true;
                    }
                }
            }
        }
        return useS2s;
    }

    private String getChannelName(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        String channelCode = FCConstants.channelMasterMap.get(paymentRequestBusinessDO.getChannel());
        return StringUtils.isEmpty(channelCode) ? String.valueOf(paymentRequestBusinessDO.getChannel())
                : FCConstants.channelNameMasterMap.get(channelCode);

    }

    private String getEventContext(Map<String, Object> walletMetaDataMap) {

        String eventContext = "serviceNumber:" + walletMetaDataMap.get("serviceNumber");
        eventContext += " && productType:" + walletMetaDataMap.get("product");
        eventContext += " && operator:" + walletMetaDataMap.get("operatorName");

        return eventContext;
    }

    @Override
    public String getQCStatusForUser(String userId) {
        String qcStatus = null;
        JSONObject configObject = configService.getCustomProp("icici.qc");
        if (configObject == null) {
            return qcStatus;
        }
        String isEnabled = (String) configObject.get("enabled");
        String perc = (String) configObject.get("perc");
        if (StringUtils.isEmpty(isEnabled) || !Boolean.parseBoolean(isEnabled)) {
            return qcStatus;
        }

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("user_id", userId);
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestData);
            long startTime = System.currentTimeMillis();
            Map<String, Object> response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_QC_STATUS_URL), 500, 2000);
            long endTime = System.currentTimeMillis();
            LOGGER.info("Done with API call to KPay for QC status. Response : " + response == null ? "null"
                    : response.toString() + "(" + (endTime - startTime) + " msecs)");
            if (response != null && response.containsKey(ResponseConstants.ICICI_USER_QC_STATUS)
                    && Boolean.parseBoolean((String) response.get(ResponseConstants.ICICI_USER_QC_STATUS))) {
                return ResponseConstants.ICICI_USER_QC_STATUS;
            }

            if (response != null && response.containsKey(ResponseConstants.ICICI_MERCHANT_QC_STATUS)
                    && Boolean.parseBoolean((String) response.get(ResponseConstants.ICICI_MERCHANT_QC_STATUS))) {
                qcStatus = ResponseConstants.ICICI_MERCHANT_QC_STATUS;
            }
        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for QC status", e);
        }

        if (!StringUtils.isEmpty(qcStatus) && qcStatus.equals(ResponseConstants.ICICI_MERCHANT_QC_STATUS)) {
            if (StringUtils.isEmpty(perc)) {
                qcStatus = null;
            }
            Integer routePerc = Integer.parseInt(perc);
            int randomNumber = RandomUtils.nextInt(101);
            if (randomNumber == 0 && routePerc != 1) {
                qcStatus = null;
            }
            if (randomNumber % routePerc != 0) {
                qcStatus = null;
            }
        }
        return qcStatus;
    }

    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws FCPaymentGatewayException, Exception {
        Map<String, String> payResponseMap = paymentResponseBusinessDO.getRequestMap();
        LOGGER.info("Payment response handling starts for Klickpay. RequestMap: "
                + PaymentUtil.getResponseDataWithoutCardData(payResponseMap));

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String s2sKey = (String) fs.getSessionData().get(KP_S2S_KEY);
        String referenceId =
                PaymentUtil.getResponseDataWithoutCardData(payResponseMap).get(ResponseConstants.PARAM_REFERENCE_ID);
        if (!StringUtils.isEmpty(s2sKey) && Boolean.parseBoolean(s2sKey)) {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            if(FCUtil.isEmpty(referenceId)) {
                referenceId = (String) fs.getSessionData().get(KP_REFERENCE_ID_KEY);
            }
            payResponseMap.put(ResponseConstants.PARAM_REFERENCE_ID, referenceId);
            final String submitData = ow.writeValueAsString(payResponseMap);
            metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Request");
            LOGGER.info("Performing KP S2S Auth call for " + referenceId);
            Map<String, Object> authResponse = makeWSCall(submitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_AUTH_URL), 1000, 120000, true);
            LOGGER.info("KP Auth response : " + authResponse);
            payResponseMap.clear();
            for (String key : authResponse.keySet()) {
                Object value = authResponse.get(key);
                if (value != null) {
                    payResponseMap.put(key, value.toString());
                }
            }
            if (!payResponseMap.containsKey(ResponseConstants.PARAM_STATUS)
                    || !payResponseMap.get(ResponseConstants.PARAM_STATUS).equals("E000")) {
                metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Fail");
                throw new FCPaymentGatewayException("Error in KP authorization API");
            } else if (!payResponseMap.isEmpty()) {
                metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Success");
            }
        }
        String productOrderId = null;
        String successUrl = null;
        String errorUrl = null;

        String mtxnId = getMerchantTransactionId(payResponseMap);
        if (mtxnId == null) {
            metricsClient.recordEvent("Payment", "KP.Response", "InvalidMtxn");
            throw new FCPaymentGatewayException("Empty merchant transaction id in response");
        }
        Integer id = (Integer) fs.getSessionData().get(mtxnId + "_txn_id");
        productOrderId = FCSessionUtil.getProductOrderId(mtxnId, fs);
        // If id is not found in session get it from DB
        if (id == null || productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderIdAndPG(
                    mtxnId, PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);

            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        successUrl = FCSessionUtil.getSuccessUrl(mtxnId, fs);
        errorUrl = FCSessionUtil.getErrorUrl(mtxnId, fs) + "&payment=fail";

        validatePaymentResponse(paymentResponseBusinessDO.getRequestMap());
        String merchantId = paymentsCache.getMerchantIdForMtxnId(mtxnId);
        logger.info("Fetched merchantId: "+merchantId+", for mtxnId: "+mtxnId);
        Map<String, Object> resultMap = paymentResponseBusinessDO.getResultMap();
        if(FCUtil.isEmpty(resultMap)) {
            resultMap = new HashMap<String, Object>();
        }
        resultMap.put(PaymentConstants.KLICKPAY_MID_KEY_STRING, merchantId);
        resultMap.put(PaymentConstants.REFERENCE_ID_KEY_STRING, referenceId);
        paymentResponseBusinessDO.setResultMap(resultMap);
        return handlePaymentResponse(paymentResponseBusinessDO, productOrderId, id, successUrl, errorUrl, false);
    }

    @Override
    protected Map<String, Object> handlePaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO,
            String orderId, Integer paymentTxnId, String successUrl, String errorUrl,
            Boolean isPaymentStatusForAdminPanel) {
        Map<String, Object> tempResultMap = paymentResponseBusinessDO.getResultMap();
        String merchantId = (String) tempResultMap.get(PaymentConstants.KLICKPAY_MID_KEY_STRING);
        String referenceId = (String) tempResultMap.get(PaymentConstants.REFERENCE_ID_KEY_STRING);
        PaymentResponse paymentResponse = createPaymentResponse(paymentResponseBusinessDO.getRequestMap());
        paymentResponseBusinessDO.setmTxnId(paymentResponse.getMtxnId());
        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTxnId);
        paymentResponseBusinessDO.setPaymentType(paymentTransaction.getPaymentType());
        paymentResponse.setAmount(paymentTransaction.getAmount());

        String redirectUrl = null;

        if (paymentResponse.getIsSuccessful()) {
            redirectUrl = successUrl;
        } else {
            redirectUrl = errorUrl;
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String transactionId = paymentResponse.getPgTransactionId();
        String responseCode = paymentResponse.getPgMessage();

        String amount = paymentTransaction.getAmount().toString();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, paymentResponse.getIsSuccessful().toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, null);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, getPaymentPortalMerchantId());
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(false));
        resultMap.put(PaymentConstants.MERCHANT_TXN_ID, paymentResponse.getMtxnId());

        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);
        finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);

        paymentResponseBusinessDO.setResultMap(finalMap);

        String binNumber = paymentResponseBusinessDO.getRequestMap().get(ResponseConstants.PARAM_CARD_BIN);
        if (binNumber != null && !binNumber.isEmpty()) {
            paymentResponseBusinessDO.setCardBinHead(StringUtils.left(binNumber, 6));
            paymentResponseBusinessDO.setCardBinTail(StringUtils.right(binNumber, 4));
        }

        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
        paymentResponse = dumpResponsetAudit(paymentResponse);

        LOGGER.info("updating payment txn for id : " + paymentTxnId);

        boolean paymentAlreadyAck = acknoledgePaymentTransactionUpdate(paymentTransaction, orderId, transactionId,
                paymentTxnId, paymentResponse, paymentResponseBusinessDO, isPaymentStatusForAdminPanel);
        LOGGER.info("Payment already acknoledged? " + paymentAlreadyAck);
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(paymentAlreadyAck));
        if (!paymentAlreadyAck) {
            Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId());
            paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), userId,
                    paymentTransaction.getIsSuccessful() ? PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS
                            : PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, isPaymentStatusForAdminPanel);
            logPaymentStatusMetric(paymentResponseBusinessDO);
            if (paymentTransaction.getIsSuccessful()) {
                storeCardHashForSuccessfulPayment(paymentResponseBusinessDO);
            }
        }
        if (!FCUtil.isEmpty(paymentResponse.getStatus())
                && paymentResponse.getStatus().equals(KlickPayConstants.SUCCESS_CODE)) {
            kpayPaymentMetaService.insertKpayPaymentMeta(paymentResponse.getMtxnId(), paymentResponse.getEci(),
                    referenceId, merchantId);
        }
        validatePaymentTypeFromResponse(paymentResponseBusinessDO.getRequestMap(), paymentTransaction);
        LOGGER.info("Handling Klickpay payment response ends here." + paymentResponse.getMtxnId());
        return finalMap;

    }

    private void validatePaymentTypeFromResponse(Map<String, String> paymentResponseMap,
            PaymentTransaction paymentTransaction) {
        try {
            String paymentType = paymentResponseMap.get(ResponseConstants.PARAM_CARD_TYPE);
            String paymentOption = paymentResponseMap.get(ResponseConstants.PARAM_CARD_SUB_TYPE);
            boolean updatePaymentTxn = false;
            if (!StringUtils.isEmpty(paymentType) && !StringUtils.isEmpty(paymentOption)
                    && Arrays.asList(KLICKPAY_CREDIT_CARD_CODE, KLICKPAY_DEBIT_CARD_CODE).contains(paymentType)
                    && !StringUtils.isEmpty(paymentTransaction.getPaymentOption())
                    && paymentTransaction.getPaymentType() != null) {
                if (paymentType.equals(KLICKPAY_CREDIT_CARD_CODE)) {
                    if (paymentTransaction.getPaymentType() != Integer
                            .parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentType(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD));
                    }

                    if (paymentOption.equals(KLICKPAY_VISA_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.VISA_CC)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.VISA_CC);
                    } else if (paymentOption.equals(KLICKPAY_MASTERCARD_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.MASTER_CC)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.MASTER_CC);
                    }
                } else if (paymentType.equals(KLICKPAY_DEBIT_CARD_CODE)) {
                    if (paymentTransaction.getPaymentType() != Integer
                            .parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentType(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD));
                    }
                    if (paymentOption.equals(KLICKPAY_VISA_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.VISA_DC)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.VISA_DC);
                    } else if (paymentOption.equals(KLICKPAY_MASTERCARD_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.MASTER_DC)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.MASTER_DC);
                    } else if (paymentOption.equals(KLICKPAY_MAESTRO_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.MAESTRO_CITI)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.MAESTRO_CITI);
                    } else if (paymentOption.equals(KLICKPAY_SBI_MAESTRO_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.MAESTRO_SBI)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.MAESTRO_SBI);
                    } else if (paymentOption.equals(KLICKPAY_RUPAY_CODE)
                            && !paymentTransaction.getPaymentOption().equals(PaymentConstants.RUPAY_DC)) {
                        updatePaymentTxn = true;
                        paymentTransaction.setPaymentOption(PaymentConstants.RUPAY_DC);
                    }
                }

            }

            if (updatePaymentTxn) {
                paymentTransactionService.updatePaymentTransactionDetails(paymentTransaction);
            }
        } catch (Exception exception) {
            LOGGER.error("Exception updating payment transaction based on payment response", exception);
        }
    }

    @Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws Exception {

        return paymentResponse(paymentResponseBusinessDO);
    }

    @Override
    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws Exception {
        String pgTransactionId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getTransactionId();

        LOGGER.info("About to refund for PG Transaction ID: [" + pgTransactionId + "]");

        String refundSubmitData = createRefundSubmitData(paymentRefundBusinessDO, refundType);
        LOGGER.info(String.format("Klickpay refund submit data for order id %s: %s",
                paymentRefundBusinessDO.getOrderId(), refundSubmitData.toString()));
        // SNS alert
        String pgUsed = getRefundAlertPaymentGateway(paymentRefundBusinessDO);
        this.publishRefundAttempt(paymentRefundBusinessDO, pgUsed);

        Map<String, Object> refundResponse = makeWSCall(refundSubmitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_REFUND_URL), 1000, 2000);
        if (refundResponse == null && paymentRefundBusinessDO != null) {
            throw new Exception("Got NULL status response from Klickpay not doing refund for order id: "
                    + paymentRefundBusinessDO.getOrderId());
        }
        LOGGER.info(String.format("Klickpay refund response data for order id %s: %s",
                paymentRefundBusinessDO.getOrderId(), refundResponse));

        Boolean refundStatus = Boolean
                .parseBoolean((String) refundResponse.get(RefundResponseConstants.PARAM_REFUND_MSSG));

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();

        if (refundStatus) {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.INITIATED);
            paymentRefundBusinessDO.setStatus(true);
        } else {
            LOGGER.error("WS call failed, doing nothing returning!");
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            paymentRefundBusinessDO.setStatus(false);
        }
        // SNS alert
        pgUsed = getRefundAlertPaymentGateway(paymentRefundBusinessDO);
        this.publishRefundAttemptStatus(paymentRefundBusinessDO, pgUsed);

        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(
                paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest(refundSubmitData);
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setRawPGResponse(PaymentUtil.serializedResponse(refundResponse));
        paymentRefundTransaction
                .setPgTransactionReferenceNo((String) refundResponse.get(RefundResponseConstants.PARAM_REFUND_TXN_ID));

        paymentRefundTransaction
                .setPgResponseDescription((String) refundResponse.get(RefundResponseConstants.PARAM_REFUND_MSSG));
        paymentRefundTransaction
                .setPgResponseCode(refundResponse.get(RefundResponseConstants.PARAM_REFUND_STATUS).toString());
        paymentRefundTransaction.setPgStatus(refundResponse.get(RefundResponseConstants.PARAM_REFUND_MSSG).toString());
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentTransactionService.create(paymentRefundTransaction);

        paymentRefundBusinessDO.setMsg(paymentRefundTransaction.getPgResponseDescription());
        paymentRefundBusinessDO.setRawPGResponse(paymentRefundTransaction.getRawPGResponse());
        String paymentOption = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentOption();
        if (PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equals(paymentOption)) {
            paymentRefundBusinessDO.setRefundToWallet(true);
        }
        return paymentRefundBusinessDO;

    }

    private String getRefundAlertPaymentGateway(PaymentRefundBusinessDO paymentRefundBusinessDO) {
        String mtxnId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
        List<KlickpayPaymentMeta> kPayList = kpayPaymentMetaDAO.getKpayPaymentMeta(mtxnId);
        String paymentGateway = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentGateway();
        if (paymentGateway != null && paymentGateway.equals("kpay") && kPayList != null && !kPayList.isEmpty()) {
            try {
                paymentGateway = kPayList.get(0).getPgUsed();
                LOGGER.info("Pg_used for mtxnId:" + mtxnId + " is " + paymentGateway);
            } catch (Exception e) {
                LOGGER.info("Error while fetching paymentGateway for mtxnId:" + mtxnId + " " + e);
            }
        }
        if (kPayList == null || kPayList.isEmpty()) {
            LOGGER.info("About to query Klickpay for the status of MtxnID: [" + mtxnId + "]");

            String statusCheckSubmitData = createStatusCheckSubmitData(mtxnId);

            Map<String, Object> statusCheckResponse = makeWSCall(statusCheckSubmitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
            /* fix for orders with old MIDs */
            if (statusCheckResponse.containsKey(ResponseConstants.PARAM_STATUS)
                    && statusCheckResponse.get(ResponseConstants.PARAM_STATUS).equals("E003")) {
                statusCheckSubmitData = createStatusCheckSubmitDataWithOldMID(mtxnId);
                statusCheckResponse = makeWSCall(statusCheckSubmitData,
                        fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
            }
            if (statusCheckResponse != null && statusCheckResponse.containsKey(ResponseConstants.PARAM_PG_USED)) {
                paymentGateway = (String) statusCheckResponse.get(ResponseConstants.PARAM_PG_USED);
                LOGGER.info("Pg_used for mtxnId:" + mtxnId + " is " + paymentGateway);
            }
        }
        return paymentGateway;
    }

    @SuppressWarnings("unchecked")
    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO,
            Boolean isPaymentStatusForAdminPanel) {
        String merchantTransactionId = paymentQueryBusinessDO.getMerchantTxnId();

        LOGGER.info("About to query Klickpay for the status of MtxnID: [" + merchantTransactionId + "]");

        String statusCheckSubmitData = createStatusCheckSubmitData(merchantTransactionId);
        
        LOGGER.info("The status Check Submit data is: " + statusCheckSubmitData);


        Map<String, Object> statusCheckResponse = makeWSCall(statusCheckSubmitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        
        LOGGER.info("The response received from pg status check is " + statusCheckResponse.toString());
        
        /* fix for orders with old MIDs */
        String orderId = paymentTransactionService.getMainOrderIdForMerchantId(merchantTransactionId);
        String[] credentials = klickPayUtil.getKlickPayCredentials(orderIdWriteDAO.getProductName(orderId).getProductType());
        if (statusCheckResponse.containsKey(ResponseConstants.PARAM_STATUS)
                && statusCheckResponse.get(ResponseConstants.PARAM_STATUS).equals("E003")) {
            credentials = klickPayUtil.getKlickPayCredentials("V");
            statusCheckSubmitData = createStatusCheckSubmitDataWithOldMID(merchantTransactionId);
            statusCheckResponse = makeWSCall(statusCheckSubmitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        }
        if (statusCheckResponse == null && paymentQueryBusinessDO != null) {
            throw new RuntimeException("Got NULL response from Klickpay not doing refund for order id: "
                    + paymentQueryBusinessDO.getOrderId());
        }

        if(statusCheckResponse.get(StatusResponseConstants.PARAM_PAY_STATUS)==null){
            LOGGER.error("WS call returned nothing, doing nothing returning!");
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
            return paymentQueryBusinessDO;
        }

        Boolean wsCallStatus = Boolean
                .parseBoolean((String) statusCheckResponse.get(StatusResponseConstants.PARAM_PAY_STATUS));

        if (!wsCallStatus) {
            LOGGER.error("WS call failed, doing nothing returning!");
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
        }

        String referenceId = (String) statusCheckResponse.get(ResponseConstants.PARAM_REFERENCE_ID);
        paymentQueryBusinessDO.setReferenceId(referenceId);

        Map<String, String> paymentResponse = new HashMap<String, String>();
        for (String responseKey : statusCheckResponse.keySet()) {
            paymentResponse.put(responseKey, statusCheckResponse.get(responseKey) == null ? ""
                    : statusCheckResponse.get(responseKey).toString());
        }

        /*
         * A few quirky fixes
         */
        paymentResponse.put(RequestParameters.AMOUNT, paymentResponse.get("amount"));
        paymentResponse.put(RequestParameters.PAYMENT_REFERENCE_ID, merchantTransactionId);

        List<PaymentTransaction> paymentTxns = paymentTransactionService
                .getPaymentTransactionByMerchantOrderId(merchantTransactionId);
        PaymentTransaction paymentTransaction = null;
        for (PaymentTransaction transaction : paymentTxns) {
            if (transaction.getPaymentTxnId().equals(paymentQueryBusinessDO.getPaymentTxnId())) {
                paymentTransaction = transaction;
                break;
            }
        }

        PaymentResponseBusinessDO payResponseDO = new PaymentResponseBusinessDO();
        payResponseDO.setRequestMap(paymentResponse);
        payResponseDO.setStatusCheck(true);
        Map<String, Object> tempResultMap = new HashMap<String, Object>();
        tempResultMap.put(PaymentConstants.KLICKPAY_MID_KEY_STRING, credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID]);
        tempResultMap.put(PaymentConstants.REFERENCE_ID_KEY_STRING, referenceId);
        payResponseDO.setResultMap(tempResultMap);
        validateStatusCheckResponse(paymentResponse, credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY]);
        Map<String, Object> finalmap = handlePaymentResponse(payResponseDO, paymentTransaction.getOrderId(),
                paymentTransaction.getPaymentTxnId(), null, null, isPaymentStatusForAdminPanel);

        Map<String, String> resultMap = (Map<String, String>) finalmap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
        String paymentStatus = resultMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL);
        if (paymentStatus.equals("true")) {
            paymentQueryBusinessDO.setPaymentStatus(true);
        } else {
            paymentQueryBusinessDO.setPaymentStatus(false);
        }
        /*
         * Refresh payment transaction.
         */
        PaymentTransaction refreshedPaymentTransaction = paymentTransactionService
                .getPaymentTransactionById(paymentTransaction.getPaymentTxnId());

        if (refreshedPaymentTransaction.getIsSuccessful()) {
            paymentQueryBusinessDO.setPgTransactionId(refreshedPaymentTransaction.getTransactionId());
        }
        paymentQueryBusinessDO.setResponseMsg("NA");

        LOGGER.info("Status check complete for Klickpay : " + paymentQueryBusinessDO.getOrderId());
        return paymentQueryBusinessDO;
    }

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        return null;
    }

    @Override
    public HashMap<String, Object> getIssuingBankStatus(String cardFirstSixDigit) {
        HashMap<String, Object> returnMap = new HashMap<>();
        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("card_bin", cardFirstSixDigit);
        requestData.put("payment_type", "CARD");
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(requestData);
            Map<String, Object> wsResult = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_DOWNTIME_URL), 1000, 1000);
            LOGGER.info("WS response from Klickpay : " + wsResult);
            String wsStatus = (String) wsResult.get(STATUS);
            if (!StringUtils.isEmpty(wsStatus) && wsStatus.equals("1")) {
                String bankStatus = (String) wsResult.get(UPSTATUS);
                if (!StringUtils.isEmpty(bankStatus)) {
                    returnMap.put(UPSTATUS, "0");
                    returnMap.put("issuing_bank", (String) wsResult.get("bank_name"));
                    if (bankStatus.equals("0")) {
                        String uiMssg = getValueFromCCS("card." + (String) wsResult.get("bank_code") + ".0.mssg");
                        if (StringUtils.isEmpty(uiMssg)) {
                            uiMssg = getValueFromCCS("card.0.mssg");
                        }
                        if (!StringUtils.isEmpty(uiMssg)) {
                            returnMap.put(MSSG, uiMssg);
                        }
                        returnMap.put("block", "1");
                    } else if (bankStatus.equals("1")) {
                        String uiMssg = getValueFromCCS("card." + (String) wsResult.get("bank_code") + ".1.mssg");
                        if (StringUtils.isEmpty(uiMssg)) {
                            uiMssg = getValueFromCCS("card.1.mssg");
                        }
                        if (!StringUtils.isEmpty(uiMssg)) {
                            returnMap.put(MSSG, uiMssg);
                        }
                        returnMap.put("block", "0");
                    } else {
                        returnMap.put(UPSTATUS, bankStatus);
                    }
                    
                    // ppi changes onwards klickpay will give card_type also in
                    // response of card check by bin number
                    if((!wsResult.containsKey("card_type")) || wsResult.get("card_type")==null){
                       returnMap.put("card_type","UNKNOWN");
                    }else{
                       returnMap.put("card_type",wsResult.get("card_type"));
                    }
                    return returnMap;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception making call to Klickpay for CARD status");
        }
        returnMap.put(UPSTATUS, "1");
        returnMap.put("issuing_bank", "");
        returnMap.put(MSSG, "");
        return returnMap;
    }
    
	public PaymentHealthResponse checkUPIPaymentHealthStatus(PaymentHealthResponse paymentHealthResponse) {    	
        HashMap<String, Object> returnMap = new HashMap<>();
        Map<String, String> requestData = new HashMap<String, String>();        
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("payment_type", "UPI");
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(requestData);
            Map<String, Object> wsResult = makeWSCall(json, fcProperties.getProperty(PaymentConstants.KLICKPAY_DOWNTIME_URL), 1000, 1000);
            LOGGER.info("WS response from Klickpay : " + wsResult);
            String wsStatus = (String) wsResult.get(STATUS);
            if (!StringUtils.isEmpty(wsStatus) && wsStatus.equals("1")) {
                String upiStatus = (String) wsResult.get(UPSTATUS);
                if (!StringUtils.isEmpty(upiStatus)) {
                	paymentHealthResponse.setStatusCode("200");
                	paymentHealthResponse.setStatusMsg("SUCCESS");
                    paymentHealthResponse.setPaymentHealthStatus(upiStatus);
                    paymentHealthResponse.setPaymentHealthResponseDetails(returnMap);
                    return paymentHealthResponse;
                }
            }
        } catch (Exception e) {
            paymentHealthResponse.setStatusCode("400");
			paymentHealthResponse.setStatusMsg("Caught Exception while calling klickpay");
			return paymentHealthResponse;
        }
        paymentHealthResponse.setStatusCode("400");
        paymentHealthResponse.setStatusMsg("UPI Health status from klickpay is blank");
        return paymentHealthResponse;
	}
    
    
    @Override
    public HashMap<String, Object> getNetBankingStatus(String bankCode) {
        HashMap<String, Object> returnMap = new HashMap<>();
        returnMap.put("title", bankCode);
        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("bank_code", bankCode);
        requestData.put("payment_type", "NB");
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(requestData);
            Map<String, Object> wsResult = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_DOWNTIME_URL), 1000, 1000);
            LOGGER.info("WS response from Klickpay : " + wsResult);
            String wsStatus = (String) wsResult.get("status");
            if (!StringUtils.isEmpty(wsStatus) && wsStatus.equals("1")) {
                String nbStatus = (String) wsResult.get(UPSTATUS);
                if (!StringUtils.isEmpty(nbStatus)) {
                    returnMap.put(UPSTATUS, "0");
                    if (nbStatus.equals("0")) {
                        String uiMssg = getValueFromCCS("nb." + bankCode + ".0.mssg");
                        if (StringUtils.isEmpty(uiMssg)) {
                            uiMssg = getValueFromCCS("nb.0.mssg");
                        }
                        if (!StringUtils.isEmpty(uiMssg)) {
                            returnMap.put("mssg", uiMssg);
                        }
                        returnMap.put("block", "1");
                    } else if (nbStatus.equals("1")) {
                        String uiMssg = getValueFromCCS("nb." + bankCode + ".1.mssg");
                        if (StringUtils.isEmpty(uiMssg)) {
                            uiMssg = getValueFromCCS("nb.1.mssg");
                        }
                        if (!StringUtils.isEmpty(uiMssg)) {
                            returnMap.put(MSSG, uiMssg);
                        }
                        returnMap.put("block", "0");
                    } else {
                        returnMap.put(UPSTATUS, nbStatus);
                    }
                    return returnMap;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception making call to Klickpay for NB status");
        }
        returnMap.put(UPSTATUS, "1");
        returnMap.put(MSSG, "");
        return returnMap;
    }

    @Override
    protected void validatePaymentResponse(Map<String, String> payResponseMap) {
        payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "true");

        com.klickpay.payment.model.PaymentResponse response = new com.klickpay.payment.model.PaymentResponse();
        response.setAmount(Double.parseDouble(payResponseMap.get(ResponseConstants.PARAM_AMOUNT)));
        response.setMerchantTransactionId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
        response.setReferenceId(payResponseMap.get(ResponseConstants.PARAM_REFERENCE_ID));
        response.setStatus(payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS));
        response.setTransactionTime(payResponseMap.get(ResponseConstants.PARAM_TRANSACTION_TIME));
        String orderId = paymentTransactionService
                .getMainOrderIdForMerchantId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
        String[] credentials = klickPayUtil.getKlickPayCredentials(orderIdWriteDAO.getProductName(orderId).getProductType());
        String expectedChecksum = FCPayUtil.calculateResponseChecksum(credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY], response);
        String responseChecksum = payResponseMap.get(ResponseConstants.PARAM_CHECKSUM);
        if (!expectedChecksum.equals(responseChecksum)) {
            payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "false");
        }
    }

    protected void validateStatusCheckResponse(Map<String, String> payResponseMap, String merchantKey) {
        payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "true");

        PaymentStatusResponse statusResponse = new PaymentStatusResponse();
        statusResponse.setMessage(payResponseMap.get("mssg"));
        statusResponse.setPaymentStatus(payResponseMap.get("payment_status"));
        statusResponse.setTransactionStatus(payResponseMap.get("status"));
        String expectedChecksum = FCPayUtil.calculateStatusResponseChecksum(merchantKey, statusResponse);
        String responseChecksum = payResponseMap.get(ResponseConstants.PARAM_CHECKSUM);
        LOGGER.info("Actual checksum : " + responseChecksum + ", calculated checksum : " + expectedChecksum);
        if (!expectedChecksum.equals(responseChecksum)) {
            payResponseMap.put(RequestParameters.CHECKSUM_RESULT, "false");
        }
    }

    protected PaymentResponse createPaymentResponse(Map<String, String> payResponseMap) {
        PaymentResponse paymentResponse = new PaymentResponse();

        if (payResponseMap.get(ResponseConstants.PARAM_AMOUNT) != null) {
            paymentResponse.setAmount(new Double(payResponseMap.get(RequestParameters.AMOUNT)));
        }
        paymentResponse.setIsSuccessful(isPaymentSuccessful(payResponseMap));
        paymentResponse.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        paymentResponse.setPgTransactionId(payResponseMap.get(ResponseConstants.PARAM_REFERENCE_ID));
        paymentResponse.setMtxnId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));

        String orderId = null;
        PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(
                payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));

        if (paymentTransaction != null) {
            orderId = paymentTransaction.getOrderId();
            paymentResponse.setOrderId(orderId);
        }

        // The end bank reference code.
        paymentResponse.setAuthCodeNo(payResponseMap.get(RequestParameters.BANK_REFERENCE));

        // The bank used.
        paymentResponse.setEci(payResponseMap.get(ResponseConstants.PARAM_PG_USED));
        paymentsCache.setOrderPaymentDataInCache(orderId + PaymentConstants.FAILURE_REASON_CACHE_KEY,
                payResponseMap.get(ResponseConstants.PARAM_USR_MSSG), 1);

        String paymentResponseCode = payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS);

        paymentResponse.setAvsCode(payResponseMap.get(ResponseConstants.PARAM_PG_UNDERLIER));
        paymentResponse.setResponseFromPp(PaymentUtil.getMapAsString(payResponseMap));
        paymentResponse.setPgMessage(paymentResponseCode);
        paymentResponse.setResponseData(PaymentUtil.serializedResponse(payResponseMap));
        paymentResponse.setStatus(payResponseMap.get(ResponseConstants.PARAM_STATUS));
        populateReportingData(paymentResponse, payResponseMap);
        
        LOGGER.info("Status code from Klickpay : " + paymentResponse.getStatus());
        if (paymentResponseCode != null && !paymentResponseCode.isEmpty()) {
            metricsClient.recordEvent("Payment", "Klickpay.ResponseCode", paymentResponseCode);
        }

        return paymentResponse;
    }

    private void populateReportingData(PaymentResponse paymentResponse, Map<String, String> payResponseMap) {
    	
    	ReconMetaData reconMetaData = new ReconMetaData();
    	
    	reconMetaData.setCategoryType(payResponseMap.get(ResponseConstants.PARAM_CARD_TYPE));
    	reconMetaData.setReconId(payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID));
    	reconMetaData.setCategorySubtype((null == payResponseMap.get(ResponseConstants.PARAM_PG_USED) && !("E000".equals(paymentResponse.getStatus()))) ? "NA" : payResponseMap.get(ResponseConstants.PARAM_PG_USED));
    	reconMetaData.setReferenceId(payResponseMap.get(ResponseConstants.PARAM_PG_REF));
    	paymentResponse.setReconMetaData(reconMetaData);
    } 
    public Map<String, Object> makeWSCall(String submitData, String requestURL, int connectionTimeout,
            int readTimeout) {
        return makeWSCall(submitData, requestURL, connectionTimeout, readTimeout, true);
    }

    @Override
    public Map<String, Object> makeWSCall(String submitData, String requestURL, int connectionTimeout, int readTimeout,
            boolean isJsonResponse) {
//        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectionTimeout)
//                .setSocketTimeout(readTimeout).build();
//        CloseableHttpClient client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
        CloseableHttpClient client = CustomHttpClientBuilder.getCustomHttpClient(requestURL, connectionTimeout, readTimeout);
        HttpClientContext context = HttpClientContext.create();
        FreechargeContext freechargeContext = FreechargeContextDirectory.get();
        if (freechargeContext != null) {
            FreechargeSession fs = freechargeContext.getFreechargeSession();
            if (fs != null) {
                String s2sKey = (String) fs.getSessionData().get(KP_S2S_KEY);
                if (!StringUtils.isEmpty(s2sKey) && Boolean.parseBoolean(s2sKey)) {
                    String kpCookie = (String) fs.getSessionData().get(KP_COOKIE_KEY);
                    String kpCookieDomain = (String) fs.getSessionData().get(KP_COOKIE_DOMAIN_KEY);
                    if (!StringUtils.isEmpty(kpCookie)) {
                    	LOGGER.info("GOT COOKIE FROM SESSION. COOKIE VALUE: " + kpCookie + ". COOKIE DOMAIN:" + kpCookieDomain );
                        CookieStore cookieStore = new BasicCookieStore();
                        BasicClientCookie cookie = new BasicClientCookie(KP_COOKIE_NAME, kpCookie);
                        cookie.setPath("/");
                        cookie.setDomain(kpCookieDomain);
                        cookieStore.addCookie(cookie);
                        context.setCookieStore(cookieStore);
                    }
                }
            }
        }
        HttpPost postMethod = new HttpPost(requestURL);
        postMethod.addHeader("Content-Type", "application/json");

        try {
            postMethod.setEntity(new StringEntity(submitData));

            int postStatus;
            String paymentResponse = null;
            try {
                CloseableHttpResponse response = client.execute(postMethod, context);
                postStatus = response.getStatusLine().getStatusCode();
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String responseLine = null;
                while ((responseLine = reader.readLine()) != null) {
                    if (paymentResponse == null) {
                        paymentResponse = responseLine;
                    } else {
                        paymentResponse += responseLine;
                    }
                }
                //TODO: check if this is avoidable
                CookieStore cookieStore = context.getCookieStore();
                if (cookieStore != null) {
                    for (Cookie cookie : cookieStore.getCookies()) {
                        if (cookie.getName().equals(KP_COOKIE_NAME)) {
                            String value = cookie.getValue();
                            LOGGER.info("COOKIE VALUE: " + value + ". COOKIE DOMAIN:" + cookie.getDomain() + "COOKIE PATH:" + cookie.getPath());
                            FreechargeSession freechargeSession = FreechargeContextDirectory.get()
                                    .getFreechargeSession();
                            freechargeSession.getSessionData().put(KP_COOKIE_KEY, value);
                            freechargeSession.getSessionData().put(KP_COOKIE_DOMAIN_KEY, cookie.getDomain());
                        }
                    }
                }
                LOGGER.info("postStatus:" + postStatus);
            } catch (SocketTimeoutException socketTimeoutException) {
                metricsClient.recordEvent("Payment", "Klickpay.WS", "SOTimeout." + requestURL);
                return new HashMap<>();
            } catch (ConnectTimeoutException connectTimeoutException) {
                metricsClient.recordEvent("Payment", "Klickpay.WS", "ConnectTimeout." + requestURL);
                return new HashMap<>();
            } catch (IOException e) {
                metricsClient.recordEvent("Payment", "Klickpay.WS", "Exception." + requestURL);
                return new HashMap<>();
            }

            String[] urlTokens = requestURL.split("/");
            if (urlTokens.length > 2) {
                metricsClient.recordEvent("Payment", "Klickpay.WS",
                        urlTokens[urlTokens.length - 2] + "_" + urlTokens[urlTokens.length - 1] + "." + postStatus);
            }
            Map<String, Object> wsResponse = new HashMap<String, Object>();
            if (HttpStatus.SC_OK == postStatus && isJsonResponse) {
                wsResponse = deserializer.deserialize(paymentResponse);
                LOGGER.info("isEmptyWSResponse:" + wsResponse.isEmpty());
                return wsResponse;
            } else if (!isJsonResponse) {
                wsResponse.put("fingerPrint", paymentResponse);
                return wsResponse;
            } else {
                throw new RuntimeException("Error while calling Klickpay WS end point : [" + requestURL + "]"
                        + "Got HTTP failure HTTP Code [" + postStatus + "]. Post Response [" + paymentResponse + "]");
            }
        } catch (UnsupportedEncodingException e1) {
            LOGGER.error("Exception creating request submit data", e1);
            return new HashMap<>();
        } finally {
            postMethod.releaseConnection();
        }
    }

    public String getValueFromCCS(String key) {
        ICCS ccs;
        ICCSConfig ccsConfig = new AbstractCCSConfig();

        ccsConfig.setCcsHost(fcProperties.getCCSHost());
        ccsConfig.setDefaultAttachPath(fcProperties.getCCSBasePathPayment());

        ccs = new CCS();
        ccs.init(ccsConfig);
        String value = null;
        try {
            value = ccs.getConf(key);
            if (!FCUtil.isEmpty(value)) {
                return value;
            }
        } catch (CCSException e) {
            logger.error("Caught Exception is ", e);
        } catch (Exception e) {
            logger.error("Caught Exception is ", e);
        }

        return null;
    }

    private String createRefundSubmitData(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) {

        String[] credentials = klickPayUtil.getKlickPayCredentials(
                orderIdWriteDAO.getProductName(paymentRefundBusinessDO.getOrderId()).getProductType());
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];

        PaymentTransactionBusinessDO paymentTransactionBusinessDO = paymentRefundBusinessDO
                .getPaymentTransactionBusinessDO();
        String merchantTransactionId = paymentTransactionBusinessDO.getMerchantTxnId();
        String referenceId = paymentTransactionBusinessDO.getMerchantTxnId();
        String statusCheckSubmitData = createStatusCheckSubmitData(merchantTransactionId);
        Map<String, Object> statusCheckResponse = makeWSCall(statusCheckSubmitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        /* fix for orders with old MIDs */
        if (statusCheckResponse.containsKey(ResponseConstants.PARAM_STATUS)
                && statusCheckResponse.get(ResponseConstants.PARAM_STATUS).equals("E003")) {
            credentials = klickPayUtil.getKlickPayCredentials("V");
            merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
            merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
            statusCheckSubmitData = createStatusCheckSubmitDataWithOldMID(merchantTransactionId);
            statusCheckResponse = makeWSCall(statusCheckSubmitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        }
        if (statusCheckResponse != null && statusCheckResponse.containsKey(ResponseConstants.PARAM_REFERENCE_ID)) {
            referenceId = (String) statusCheckResponse.get(ResponseConstants.PARAM_REFERENCE_ID);
            LOGGER.info("Reference Id fetched from status check API : " + referenceId);
        }
        PaymentRefundInput refundInput = new PaymentRefundInput();
        refundInput.setMerchantCode(merchantId);
        refundInput.setMerchantTransactionId(merchantTransactionId);
        refundInput.setReferenceId(referenceId);
        refundInput.setRefundAmount(paymentRefundBusinessDO.getRefundAmount());
        String walletReferenceId = "", idempotencyId = "", eventContext = "";
        if (paymentTransactionBusinessDO.getPaymentType() != null && paymentTransactionBusinessDO.getPaymentType()
                .equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET))) {
            walletReferenceId = paymentRefundBusinessDO.getOrderId();
            idempotencyId = paymentTransactionBusinessDO.getMerchantTxnId() + "_" + OneCheckTxnType.CREDIT_REFUND;
            LOGGER.info(
                    "refund type is " + refundType + "for mtxnId " + paymentTransactionBusinessDO.getMerchantTxnId());
            if (FCUtil.isEmpty(refundType)) {
                eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId();
            } else {
                if (PaymentConstants.MANUAL_REFUND.equalsIgnoreCase(refundType)) {
                    idempotencyId = paymentTransactionBusinessDO.getMerchantTxnId() + "_" + OneCheckTxnType.CS_REFUND;
                    eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId() + " CS Manual refund";
                } else if (PaymentConstants.CS_BANK_REFUND.equalsIgnoreCase(refundType)) {
                    eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId() + " CS Bank refund";
                } else {
                    eventContext = "Refund for order " + paymentRefundBusinessDO.getOrderId();
                }
            }
        }
        refundInput.setWalletBussinessEntity("FREECHARGE");
        refundInput.setWalletReferenceId(walletReferenceId);
        refundInput.setIdempotencyId(idempotencyId);
        refundInput.setEventContext(eventContext);
        String checksum = FCPayUtil.calculateRefundRequestChecksum(merchantKey, refundInput);
        refundInput.setChecksum(checksum);
        return refundInput.toString();
    }

    private String createStatusCheckSubmitData(String merchantTransactionId) {
        String[] credentials = klickPayUtil.getKlickPayCredentials(orderIdWriteDAO
                .getProductName(paymentTransactionService.getMainOrderIdForMerchantId(merchantTransactionId))
                .getProductType());
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        PaymentStatusRequest statusRequest = new PaymentStatusRequest();
        statusRequest.setMerchantCode(merchantId);
        statusRequest.setMerchantTransactionId(merchantTransactionId);
        statusRequest.setChecksum(FCPayUtil.calculateStatusRequestChecksum(merchantKey, statusRequest));
        return statusRequest.toString();
    }

    /* Temporary fix for old MID orders */
    private String createStatusCheckSubmitDataWithOldMID(String merchantTransactionId) {
        String[] credentials = klickPayUtil.getKlickPayCredentials("V");
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        PaymentStatusRequest statusRequest = new PaymentStatusRequest();
        statusRequest.setMerchantCode(merchantId);
        statusRequest.setMerchantTransactionId(merchantTransactionId);
        statusRequest.setChecksum(FCPayUtil.calculateStatusRequestChecksum(merchantKey, statusRequest));
        return statusRequest.toString();
    }

    private Boolean isPaymentSuccessful(Map<String, String> payResponseMap) {
        String checkSumResult = payResponseMap.get(RequestParameters.CHECKSUM_RESULT);
        if (checkSumResult == null || checkSumResult.trim().isEmpty() || !Boolean.parseBoolean(checkSumResult)) {
            return false;
        }

        if (Boolean.parseBoolean(payResponseMap.get(ResponseConstants.PARAM_PAYMENT_STATUS))) {
            return true;
        }
        return false;
    }

    @Override
    protected String getPaymentPortalMerchantId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getMerchantTransactionId(Map<String, String> payResponseMap) {
        return payResponseMap.get(ResponseConstants.PARAM_MERCHANT_TXN_ID);
    }

    @Override
    public List<CardDetailsDo> isDebitATMEnabled(String userId, List<CardDetailsDo> cardDetails) {
        List<CardDetailsDo> returnList = new ArrayList<CardDetailsDo>();
        
//        for(Map<String, String> each : returnList)
//            each.put("status", "false");
        
        metricsClient.recordEvent("Payment", "Debit.ATM", "Eligibility");
        JSONObject configObject = configService.getCustomProp("debit.atm");
        if (configObject == null) {
            return returnList;
        }
        String isEnabled = (String) configObject.get("enabled");
        String perc = (String) configObject.get("perc");
        if (StringUtils.isEmpty(isEnabled) || StringUtils.isEmpty(perc) || !Boolean.parseBoolean(isEnabled)) {
            return returnList;
        }
        Integer routePerc = Integer.parseInt(perc);
        int randomNumber = RandomUtils.nextInt(101);
        if (randomNumber == 0 && routePerc != 1) {
            return returnList;
        }
        if (randomNumber % routePerc != 0) {
            return returnList;
        }
        Map<String, Object> requestData = new HashMap<String, Object>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("cards_details", cardDetails);
        requestData.put("user_id", userId);
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestData);
            long startTime = System.currentTimeMillis();
            Map<String, Object> response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_DEBIT_ATM_STATUS_URL), 500, 2000);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("Payment", "Debit.ATM.Eligibility.Latency", endTime - startTime);
            
            if(response.isEmpty() || response.get(ResponseConstants.PARAM_CARDS_DEBIT_ATM_DETAILS)==null)
                return returnList;
            
            ObjectMapper mapper = new ObjectMapper();
            Object responseObject = response.get(ResponseConstants.PARAM_CARDS_DEBIT_ATM_DETAILS);
            List<CardDetailsDo> debitATMResult = mapper.readValue(ow.writeValueAsString(responseObject), new TypeReference<List<CardDetailsDo>>(){});
            return debitATMResult;
//            if (response != null && response.containsKey(ResponseConstants.PARAM_DEBIT_ATM_STATUS)) {
//                returnMap.put("status", (String) response.get(ResponseConstants.PARAM_DEBIT_ATM_STATUS));
//            }
//
//            if (response != null && response.containsKey(ResponseConstants.PARAM_DEBIT_ATM_CARD_ISSUER)) {
//                returnMap.put("issuer", (String) response.get(ResponseConstants.PARAM_DEBIT_ATM_CARD_ISSUER));
//            }

        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for Debit ATM status", e);
        }
        return returnList;
    }

    @Override
    public Map<String, Object> getStatusForITTP(Map<String, String> card, String userId, FPSParams fpsParams) {
        boolean status = false;
        metricsClient.recordEvent("Payment", "ITTP", "Eligibility");
        String cardBinHead = (String) card.get(CardStorageKey.CARD_IS_IN);
        String cardFingerprint = (String) card.get(CardStorageKey.CARD_FINGERPRINT);
        fpsParams.setCardBinHead(cardBinHead);
        fpsParams.setCardFingerprint(cardFingerprint);
        setUserAgentEntity(fpsParams);
        Map<String, Object> requestData = new HashMap<String, Object>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("fps_params", fpsParams);
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestData);

            long startTime = System.currentTimeMillis();
            Map<String, Object> response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_ITTP_STATUS_URL), 500, 20000);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("Payment", "CITI.ITTP.Status.Latency", endTime - startTime);
            LOGGER.info("Done with API call to KPay for CITI ittp status. Response : " + response == null ? "null"
                    : response.toString() + "(" + (endTime - startTime) + " msecs)");
            if (response != null) {
                status = Boolean.parseBoolean((String) response.get(ResponseConstants.PARAM_DEBIT_ATM_STATUS));
            }
        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for Debit ATM status", e);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("status", status);
        return response;
    }

    @Override
    public Map<String,Map<String,String>> getTransactionTypeStatus(List<Map<String, String>> cards,FPSParams fpsParams,String productType){
        logger.info("Inside get transaction type status ");
        List<CardDetailsDo> cardDetailsDoList=new ArrayList<>();
        for (Map<String, String> each : cards){
            String cardNumber = each.get(CardStorageKey.CARD_IS_IN);
            if (!StringUtils.isEmpty(cardNumber)) {
                CardDetailsDo cardDetailsDo=new CardDetailsDo();
                String cardBinHead = (String) each.get(CardStorageKey.CARD_IS_IN);
                String cardFingerprint = (String) each.get(CardStorageKey.CARD_FINGERPRINT);
                logger.info("cardFingerprint is  :"+cardFingerprint);

                cardDetailsDo.setCardBinHead(cardBinHead);
                cardDetailsDo.setCardFingerprint(cardFingerprint);
                cardDetailsDoList.add(cardDetailsDo);

            }
        }
        if(fpsParams.getAppVersion()!=null)
        setUserAgentEntity(fpsParams);
        BaseFPSParams baseFPSParams=new BaseFPSParams();
        convertToBaseFPSParam(fpsParams,baseFPSParams);
        Map<String, Object> requestData = new HashMap<String, Object>();
        String merchant_code=getKlickpayMerchantByProductType(productType);
        logger.info("merchant code is :"+merchant_code);
        requestData.put("merchant_code", merchant_code);
        requestData.put("fps_params", baseFPSParams);
        requestData.put("card_details_list",cardDetailsDoList);
        logger.info("After setUserAgentEntity Request Dara is "+requestData);

        Map<String, Object> response=new HashMap<>();
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestData);

            long startTime = System.currentTimeMillis();
            response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_TRANSACTIONTYPE_STATUS_URL), 500, 20000);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("Payment", "Transaction.Type.Status.Latency", endTime - startTime);
            LOGGER.info("Done with API call to KPay for Transaction Type status. Response : " + response == null ? "null"
                    : response.toString() + "(" + (endTime - startTime) + " msecs)");

        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for Transaction Type status", e);
        }
        logger.info("Response is "+response);
        logger.info("After Klickpay call transaction_status_map ="+response.get("transaction_status_map"));

        return (Map<String,Map<String,String>>)response.get("transaction_status_map");
    }

    private void convertToBaseFPSParam(FPSParams fpsParams,BaseFPSParams baseFPSParams){
        baseFPSParams.setUserId(fpsParams.getUserId());
        baseFPSParams.setAppVersion(fpsParams.getAppVersion());
        baseFPSParams.setPlatformType(fpsParams.getPlatformType());
        baseFPSParams.setUserAgentEntity(fpsParams.getUserAgentEntity());
        baseFPSParams.setDeviceName(fpsParams.getDeviceName());
        baseFPSParams.setOsVersion(fpsParams.getOsVersion());
        baseFPSParams.setClientIp(fpsParams.getClientIp());
    }


    private void setUserAgentEntity(FPSParams fpsParams) {
        String userAgentStr = fpsParams.getUserAgentStr();
        UserAgent userAgentWeb = UserAgent.parseUserAgentString(userAgentStr);
        String platformType= fpsParams.getPlatformType();
        String userAgentName = null;
        String userAgentVersion = null;
        String osVersion = null;
        if(userAgentWeb!=null) {
            if("ANDROID".equalsIgnoreCase(platformType)
                    ||"IOS".equalsIgnoreCase(platformType) || "WINDOWS".equalsIgnoreCase(platformType) || 
                    "TAB".equalsIgnoreCase(platformType)) {
                userAgentName = fpsParams.getDeviceName();
            } else if(userAgentWeb.getBrowser()!=null){
                userAgentName = userAgentWeb.getBrowser().getName();
            }
            if(userAgentWeb.getBrowserVersion()!=null && !("ANDROID".equalsIgnoreCase(platformType)
                    ||"IOS".equalsIgnoreCase(platformType) || "WINDOWS".equalsIgnoreCase(platformType) || 
                    "TAB".equalsIgnoreCase(platformType))) {
                userAgentVersion = userAgentWeb.getBrowserVersion().getVersion();
            }
            if(userAgentWeb.getOperatingSystem()!=null && !("ANDROID".equalsIgnoreCase(platformType)
                    ||"IOS".equalsIgnoreCase(platformType) || "WINDOWS".equalsIgnoreCase(platformType) || 
                    "TAB".equalsIgnoreCase(platformType)))
                osVersion = userAgentWeb.getOperatingSystem().getName();
        }
        String osVersionApp = fpsParams.getOsVersion();
        UserAgentEntity userAgentEntity = new UserAgentEntity();
        userAgentEntity.setUserAgentName(userAgentName);
        userAgentEntity.setUserAgentVersion(userAgentVersion);
        userAgentEntity.setOsVersion((osVersionApp == null || osVersionApp.isEmpty()) ? osVersion : osVersionApp );
        logger.debug("Prepared User Agent object: "+userAgentEntity);
        fpsParams.setUserAgentEntity(userAgentEntity);
    }

    @Override
    public Map<String, Object> resendOTP(Map<String, Object> requestParamsMap) {
        metricsClient.recordEvent("Payment", "ITTP", "ResendOTP");
        String status = "N";
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestParamsMap);

            long startTime = System.currentTimeMillis();
            logger.info("Passing resendOTP request for KPay: "+json);
            Map<String, Object> response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_ITTP_RESEND_OTP_URL), 1000, 90000);
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("Payment", "CITI.ITTP.ResendOTP.Latency", endTime - startTime);
            LOGGER.info("Done with API call to KPay for CITI ittp resend otp. Response : " + response == null ? "null"
                    : response.toString() + "(" + (endTime - startTime) + " msecs)");
            if (response != null) {
                status = (String) response.get(ResponseConstants.PARAM_DEBIT_ATM_STATUS);
            }
        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for Debit ATM status", e);
        }
        Map<String, Object> response = new HashMap<>();
        response.put("status", status);
        return response;

    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getSuccessfulPgRefundData(PaymentRefundBusinessDO refundBusinessDO) {
        Map<String, Object> refundStatusMap = new HashMap<>();
        refundStatusMap.put("refundAmount", new Double("0"));
        refundStatusMap.put("refundedOn", new Date());
        String merchantTxnId = refundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();
        String statusCheckSubmitData = createStatusCheckSubmitData(merchantTxnId);
        LOGGER.info("statusCheckSubmitData = " + statusCheckSubmitData + ", URL : "
                + fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL));
        Map<String, Object> statusCheckResponse = makeWSCall(statusCheckSubmitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        /* fix for orders with old MIDs */
        if (statusCheckResponse.containsKey(ResponseConstants.PARAM_STATUS)
                && statusCheckResponse.get(ResponseConstants.PARAM_STATUS).equals("E003")) {
            statusCheckSubmitData = createStatusCheckSubmitDataWithOldMID(merchantTxnId);
            statusCheckResponse = makeWSCall(statusCheckSubmitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_STATUS_CHECK_URL), 1000, 2000);
        }

        String referenceId = null;
        if (statusCheckResponse != null && statusCheckResponse.containsKey(ResponseConstants.PARAM_REFERENCE_ID)) {
            referenceId = (String) statusCheckResponse.get(ResponseConstants.PARAM_REFERENCE_ID);
            LOGGER.info("Reference Id fetched from status check API : " + referenceId);
        }
        String refundStatusSubmitData = createRefundStatusSubmitData(merchantTxnId, referenceId);
        LOGGER.info("refundStatusSubmitData = " + refundStatusSubmitData + ", URL : "
                + fcProperties.getProperty(PaymentConstants.KLICKPAY_REFUND_STATUS_URL));
        Map<String, Object> refundStatusResponse = makeWSCall(refundStatusSubmitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_REFUND_STATUS_URL), 1000, 2000);
        /* fix for orders with old MIDs */
        if (refundStatusResponse.containsKey(ResponseConstants.PARAM_STATUS)
                && refundStatusResponse.get(ResponseConstants.PARAM_STATUS).equals("E003")) {
            refundStatusSubmitData = createRefundStatusSubmitDataWithOldMID(merchantTxnId, referenceId);
            logger.info("refundStatusSubmitData = " + refundStatusSubmitData + ", URL : "
                    + fcProperties.getProperty(PaymentConstants.KLICKPAY_REFUND_STATUS_URL));
            refundStatusResponse = makeWSCall(refundStatusSubmitData,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_REFUND_STATUS_URL), 1000, 2000);
        }

        LOGGER.info("Refund status response from Klickpay : " + refundStatusResponse);
        if (refundStatusResponse != null && refundStatusResponse.containsKey("status")
                && ((String) refundStatusResponse.get("status")).equals("E000")) {
            if (refundStatusResponse.containsKey("refund_responses")) {
                List<Map<String, Object>> refundResponses = (List<Map<String, Object>>) refundStatusResponse
                        .get("refund_responses");
                if (refundResponses != null && !refundResponses.isEmpty()) {
                    for (Map<String, Object> refundResponse : refundResponses) {
                        if (refundResponse.containsKey("refund_amount")) {
                            LOGGER.info("Success Refund amount = " + refundResponse.get("refund_amount"));
                            Double refundAmount = Double.valueOf(refundResponse.get("refund_amount").toString());
                            Date refundedOn = new Date();
                            try {
                                refundedOn = formatter.parse(refundResponse.get("refunded_on").toString());
                            } catch (ParseException e) {
                            }
                            refundStatusMap.put("refundAmount", refundAmount);
                            refundStatusMap.put("refundedOn", refundedOn);
                            LOGGER.info("Success Refund amount = " + refundAmount);
                            return refundStatusMap;
                        }
                    }
                }
            }
        }
        return refundStatusMap;
    }

    private String createRefundStatusSubmitData(String merchantTxnId, String referenceId) {
        String[] credentials = klickPayUtil.getKlickPayCredentials(orderIdWriteDAO
                .getProductName(paymentTransactionService.getMainOrderIdForMerchantId(merchantTxnId)).getProductType());
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        PaymentRefundInput input = new PaymentRefundInput();
        input.setRefundAmount(new Double("0"));
        input.setMerchantCode(merchantId);
        input.setMerchantTransactionId(merchantTxnId);
        input.setRefundIdempotencyId(merchantTxnId);
        input.setReferenceId(referenceId);
        input.setChecksum(FCPayUtil.calculateRefundRequestChecksum(merchantKey, input));
        return input.toString();
    }

    /* Temporary Fix for old MID orders */
    private String createRefundStatusSubmitDataWithOldMID(String merchantTxnId, String referenceId) {
        String[] credentials = klickPayUtil.getKlickPayCredentials("V");
        String merchantId = credentials[KlickPayUtil.KLICKPAY_MERCHANT_ID];
        String merchantKey = credentials[KlickPayUtil.KLICKPAY_MERCHANT_KEY];
        PaymentRefundInput input = new PaymentRefundInput();
        input.setRefundAmount(new Double("0"));
        input.setMerchantCode(merchantId);
        input.setMerchantTransactionId(merchantTxnId);
        input.setRefundIdempotencyId(merchantTxnId);
        input.setReferenceId(referenceId);
        input.setChecksum(FCPayUtil.calculateRefundRequestChecksum(merchantKey, input));
        return input.toString();
    }

    @Override
    public Map<String, Object> isCardValid(String cardNumber) {
        boolean isEnabled = configService.isCardStatusEnabled();
        if (!isEnabled) {
            return new HashMap<>();
        }

        Map<String, String> requestData = new HashMap<String, String>();
        requestData.put("merchant_code", fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));
        requestData.put("card_number", cardNumber);
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            final String json = ow.writeValueAsString(requestData);
            long startTime = System.currentTimeMillis();
            Map<String, Object> response = makeWSCall(json,
                    fcProperties.getProperty(PaymentConstants.KLICKPAY_CARD_STATUS_URL), 500, 1000);
            long endTime = System.currentTimeMillis();
            LOGGER.info("Done with API call to KPay for Card status. Response : " + response == null ? "null"
                    : response.toString() + "(" + (endTime - startTime) + " msecs)");
            metricsClient.recordLatency("Payment", "Card.Status.API.Latency", (endTime - startTime));
            if (response != null) {
                return response;
            }
        } catch (Exception e) {
            LOGGER.error("Exception making API call to klickpay for QC status", e);
        }
        return null;
    }

    private String serializePayRequest(Map<String, Object> paymentRequest) {
        String desc = "";

        if (paymentRequest != null && !paymentRequest.isEmpty()) {
            for (String key : paymentRequest.keySet()) {
                // not storing CC related information
                if (!BLACKLISTED_KEYS.contains(key)) {
                    desc += key.toString() + " ==>>  " + MapUtils.getString(paymentRequest, key, null) + " :: ";
                }
            }
        }
        return desc;
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {
            paymentRequest = paymentTransactionService.create(paymentRequest);
            return paymentRequest;
    }

    private PaymentResponse dumpResponsetAudit(PaymentResponse paymentResponse) {
        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            LOGGER.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;
    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction paymentTransaction) {
        try {
            paymentTransaction = paymentTransactionService.create(paymentTransaction);
        } catch (Exception exp) {
            LOGGER.error(
                    "error in inserting payment transaction details for order id :" + paymentTransaction.getOrderId(),
                    exp);
        }
        return paymentTransaction;
    }

    public String getRequestChecksum(Map<String, Object> paymentRequestMap, String merchantKey) {
        String merchantTransactionId = (String) paymentRequestMap.get(RequestConstants.PARAM_MERCHANT_TXN_ID);
        Double amount = Double.parseDouble((String) paymentRequestMap.get(RequestConstants.PARAM_TXN_AMOUNT));
        String userId = (String) paymentRequestMap.get(RequestConstants.PARAM_USER_ID);
        return FCPayUtil.calculateRequestChecksum(merchantKey, merchantTransactionId, amount, userId);
    }

    public static final class RequestConstants {

        public static final String RESPONSE_URL                = "/payment/handleklickpayresponse";

        public static final String PG_RESPONSE_URL             = "/payment/handlePGResponse";

        public static final String MOBILE_PAYMENT_RESPONSE_URL = "/m/payment/response/handleklickpayresponse";

        public static final String PARAM_MERCHANT_ID           = "merchant_id";

        public static final String PARAM_MERCHANT_TXN_ID       = "merchant_txn_id";

        public static final String PARAM_TXN_AMOUNT            = "txn_amount";

        public static final String PARAM_PRODUCT_INFO          = "product_info";

        public static final String PARAM_UPI_INFO              = "upi_info";

        public static final String PARAM_CUSTOMER_NAME         = "customer_name";

        public static final String PARAM_CUSTOMER_EMAIL        = "customer_email";

        public static final String PARAM_CUSTOMER_CONTACT      = "customer_contact";

        public static final String PARAM_CALLBACK_URL          = "callback_url";

        public static final String PARAM_CALLBACK_URL1         = "callback_url_1";

        public static final String PARAM_BLACK_PAYMENTS        = "block_payment_type";

        public static final String PARAM_GATEWAY_TO_USE        = "pg";

        public static final String PARAM_PAYMENT_TYPE          = "payment_type_code";

        public static final String PARAM_CARD_TYPE             = "card_type";

        public static final String PARAM_CARD_NUMBER           = "card_num";

        public static final String PARAM_CARD_EXPIRY_YEAR      = "card_exp_yr";

        public static final String PARAM_CARD_EXPIRY_MONTH     = "card_exp_mon";

        public static final String PARAM_CARD_CVV              = "card_cvv";

        public static final String PARAM_CARD_HOLDER_NAME      = "card_holder";

        public static final String PARAM_CARD_TOKEN            = "card_token";

        public static final String PARAM_SAVE_CARD             = "save_card";

        public static final String PARAM_SAVE_CARD_NAME        = "saved_card_name";

        public static final String PARAM_USER_ID               = "user_id";

        public static final String PARAM_CHECKSUM              = "checksum";

        public static final String PARAM_CURRENCY              = "currency";

        public static final String PARAM_CHANNEL               = "req_channel";

        public static final String PARAM_METADATA              = "metadata";

        public static final String PARAM_CLIENT_REAL_IP        = "client_real_ip";

        public static final String IS_ICICI_QC_TXN             = "is_qc";
        
        public static final String S2S_RESPONSE_URL            = "/payment/s2s/kp";

        public static final String TRANSACTION_TYPE_MAP        = "transaction_type_status";

    }

    @SuppressWarnings("unused")
    public static final class ResponseConstants {

        public static final String PARAM_MERCHANT_TXN_ID       = "merchant_txn_id";

        public static final String PARAM_REFERENCE_ID          = "reference_id";

        public static final String PARAM_FCUPI_ACC_DETAILS     = "ac";

        public static final String PARAM_AMOUNT                = "amount";

        public static final String PARAM_RRN                   = "rrn";

        public static final String PARAM_STATUS                = "status";

        public static final String PARAM_PAYMENT_STATUS        = "payment_status";

        public static final String PARAM_PG_USED               = "pg_used";

        public static final String PARAM_USR_MSSG              = "usr_mssg";

        public static final String PARAM_PG_MSSG               = "pg_mssg";

        public static final String PARAM_PG_UNDERLIER          = "pg_underlier";

        public static final String PARAM_CHECKSUM              = "checksum";

        public static final String PARAM_TRANSACTION_TIME      = "transaction_time";

        public static final String PARAM_METADATA              = "metadata";

        public static final String PARAM_CARD_BIN              = "card_bin";

        public static final String ICICI_USER_QC_STATUS        = "qc_enabled";

        public static final String ICICI_MERCHANT_QC_STATUS    = "merch_qc_enabled";

        public static final String PARAM_CARD_TYPE             = "card_type";

        public static final String PARAM_CARD_SUB_TYPE         = "card_sub_type";

        public static final String PARAM_DEBIT_ATM_STATUS      = "status";
        
        public static final String PARAM_CARDS_DEBIT_ATM_DETAILS = "cards_debitatm_details";

        public static final String PARAM_DEBIT_ATM_CARD_ISSUER = "card_issuer";
        
        public static final String PARAM_PG_REF = "pg_txn_id";
    }

    @SuppressWarnings("unused")
    private static final class RefundResponseConstants {

        public static final String PARAM_REFUND_STATUS = "status";

        public static final String PARAM_REFUND_MSSG   = "mssg";

        public static final String PARAM_REFUND_TXN_ID = "refund_ref_id";

        public static final String PARAM_CHECKSUM      = "checksum";
    }

    @SuppressWarnings("unused")
    private static final class StatusResponseConstants {

        public static final String PARAM_STATUS     = "status";

        public static final String PARAM_MSSG       = "mssg";

        public static final String PARAM_PAY_STATUS = "payment_status";

        public static final String PARAM_CHECKSUM   = "checksum";
    }

    @SuppressWarnings("deprecation")
    public Map<String, String> getResponseDataMap(Map<String, String> responseMap, int channel) {
        Map<String, String> dataMap = new HashMap<String, String>();
        for (String token : responseMap.keySet()) {
            if (!token.isEmpty()) {
                String value = responseMap.get(token);
                if (token.equals("PaReq") && (channel != FCConstants.CHANNEL_ID_WEB
                        && channel != FCConstants.CHANNEL_ID_MOBILE && channel != FCConstants.CHANNEL_ID_TIZEN_PHONE)) {
                    value = URLEncoder.encode(value);
                }
                if (!token.equals("paramcount")) {
                    dataMap.put(token, value);
                }
            }
        }
        return dataMap;
    }
    
    private String getKlickpayMerchantByProductType(String productType){
        String[] utilityProducts = paymentOptionService
                .getProductListFromCCS(fcProperties.getUtilityProductsList());
        List<String> rechargeProductTypeList=Arrays.asList(PaymentConstants.RECHARGE_PRODUCT_TYPE.split(","));

        if (utilityProducts != null && ArrayUtils.contains(utilityProducts, productType)){
            return fcProperties
                    .getProperty(PaymentConstants.KLICKPAY_UTILITY_MERCHANT_ID);
        }
        else if(PaymentConstants.PAID_COUPONS_PRODUCT_TYPE.equals(productType)){
            return fcProperties.getProperty(PaymentConstants.KLICKPAY_COUPONS_MERCHANT_ID);
        }
        else if(PaymentConstants.GIFTCARD_PRODUCT_TYPE.equals(productType)){
            return fcProperties.getProperty(PaymentConstants.KLICKPAY_GIFTCARD_MERCHANT_ID);
        }
        else if(rechargeProductTypeList.contains(productType))
            return fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID);
        else
            return null;
    }


}
