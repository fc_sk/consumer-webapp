package com.freecharge.payment.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;

@Component
public class DebitCardPaymentType  extends PaymentType {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private AppConfigService configService;
    
    @Autowired
    private FCProperties fcProperties;

    public Boolean doPayment(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {

        Map<String, String> validations = validateRequest(paymentRequestBusinessDO);
        Boolean valid = Boolean.valueOf(validations.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE));
        if(valid){
            IGatewayHandler gatewayHandler = getPaymentGateway(paymentRequestBusinessDO);
            if (gatewayHandler != null) {
                if(!fcProperties.isPreProdMode() && doPaymentThroughKlickpay(paymentRequestBusinessDO)){
                    logger.info("Routing debit card payment through Klickpay for order : " + paymentRequestBusinessDO.getOrderId());
                    gatewayHandler = pgFactory.getPaymentHandler(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
                }else if (!fcProperties.isPreProdMode() && doDCPaymentThroughBD(paymentRequestBusinessDO)) {
            	    // Check whether we need to route this to billdesk.
            		logger.info("Routing debit card payment through Billdesk for order : " + paymentRequestBusinessDO.getOrderId());
            		gatewayHandler = pgFactory.getPaymentHandler("bdk");

            		// Change the card details to default Billdesk values, if it is set
            		// to PayU default values.
            		String cardExpMon = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpMonth();
            		String cardExpYear = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpYear();
            		//TODO: Find default Cvv value for PayU.
            		String cardCvv = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardCvv();
            		if (paymentRequestBusinessDO.getPaymentOption().equalsIgnoreCase(PaymentConstants.MAESTRO_SBI) &&
            				cardExpMon.equals(PaymentConstants.MAESTRO_DEFAULT_EXPIRY_MONTH) &&
            				cardExpYear.equals(PaymentConstants.MAESTRO_DEFAULT_EXPIRY_YEAR)) {
            			paymentRequestBusinessDO.getPaymentRequestCardDetails().setCardExpMonth(PaymentConstants.MAESTRO_BD_DEFAULT_EXPIRY_MONTH);
            			paymentRequestBusinessDO.getPaymentRequestCardDetails().setCardExpYear(PaymentConstants.MAESTRO_BD_DEFAULT_EXPIRY_YEAR);
            			paymentRequestBusinessDO.getPaymentRequestCardDetails().setCardCvv(PaymentConstants.MAESTRO_BD_DEFAULT_CVV);
            		}

            	}                
            	gatewayHandler.paymentRequestSales(paymentRequestBusinessDO);
            	this.recordPaymentTypeMetric("debitcard");
            	storeCardDataForOrderInCache(paymentRequestBusinessDO.getMtxnId(), paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardNumber());
            }
        }

        return valid;
    }
    
    // Check whether the String is "true".
    private boolean isConfigEnabled(String configVal) {
    	if (!StringUtils.isEmpty(configVal) && configVal.equals("true")) {
    		return true;
    	}
    	return false;
    }
    
	/*
	 * Method to determine if the deit card payment request needs to be routed through billdesk gateway
	 * For now, send all the debit cards payments to billdesk, if it is enabled in app config.
	 */
    private boolean doDCPaymentThroughBD(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        
        if (paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_WINDOWS_APP ||
        		paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_IOS_APP) {
        	return false;
        }
        
        if(paymentRequestBusinessDO.getChannel() == FCConstants.CHANNEL_ID_ANDROID_APP &&
        		paymentRequestBusinessDO.getAppVersion() < 33){
        	return false;
         }

    	JSONObject billdeskPOCCondition = configService.getCustomProp(PaymentConstants.BILLDESK_DEBIT_POC);

    	if (billdeskPOCCondition == null) {
    		return false;
    	}

    	String paymentOption = paymentRequestBusinessDO.getPaymentOption();
        String pocCondition = null;
        if (PaymentUtil.isMaestroCard(paymentOption)) {
        	String maestroEnabled = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_MAESTRO_ENABLED);
        	if (!isConfigEnabled(maestroEnabled)) {
        		return false;
        	}
        	pocCondition = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_MAESTRO_CONDITION);
       	
        } else if (paymentOption.equalsIgnoreCase(PaymentConstants.MASTER_DC)) {
        	String masterEnabled = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_MASTER_ENABLED);
        	if (!isConfigEnabled(masterEnabled)) {
        		return false;
        	}

        	pocCondition = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_MASTER_CONDITION);
        	
        } else if (paymentOption.equalsIgnoreCase(PaymentConstants.VISA_DC)) {
        	String visaEnabled = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_VISA_ENABLED);
        	if (!isConfigEnabled(visaEnabled)) {
        		return false;
        	}
            	pocCondition = (String) billdeskPOCCondition.get(PaymentConstants.BILLDESK_DEBIT_VISA_CONDITION);

        } else {
        	// Other cards
        	return false;
        }

        if (StringUtils.isEmpty(pocCondition)) {
        	return false;
        }

        Integer condition = Integer.parseInt(pocCondition);
        final int randomSampleInt = RandomUtils.nextInt(9999);
        
        if (randomSampleInt % condition != 0) {
        	return false;
        }

        return true;
    }
    
    private boolean doPaymentThroughKlickpay(PaymentRequestBusinessDO paymentRequestBusinessDO) {
//        JSONObject klickpayConfig = configService.getCustomProp(PaymentConstants.KLICKPAY);
//        if (klickpayConfig != null) {
//            String klickPayDebitEnabled = (String) klickpayConfig.get(PaymentConstants.KLICKPAY_DEBIT_ENABLED);
//            if (StringUtils.isNotEmpty(klickPayDebitEnabled) && klickPayDebitEnabled.equals("true")) {
//                String klickpayDebitPercentage = (String) klickpayConfig.get(PaymentConstants.KLICKPAY_DEBIT_PERCENTAGE);
//                if (StringUtils.isNotEmpty(klickpayDebitPercentage)) {
//                    Integer condition = Integer.parseInt(klickpayDebitPercentage);
//                    final int randomSampleInt = RandomUtils.nextInt(9999);
//                    if (randomSampleInt % condition == 0) {
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
    	return true;
    }

    public PaymentResponseBusinessDO checkStatus(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public PaymentRefundBusinessDO doRefund(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        String pg = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentGateway();
        if (pg == null){
            paymentRefundBusinessDO.setStatus(false);
            paymentRefundBusinessDO.setMsg("PG is null in DB");
            return paymentRefundBusinessDO;
        }
        IGatewayHandler gatewayHandler = null;

        if(pg != null)
            gatewayHandler = pgFactory.getPaymentHandler(pg);

            if (gatewayHandler != null)
                gatewayHandler.refundPayment(paymentRefundBusinessDO, refundType);


        return paymentRefundBusinessDO;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private Map<String, String> validateRequest(PaymentRequestBusinessDO paymentRequestBusinessDO)
    {

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

		logger.debug("Validation in Credit type payment starts for session id : "+fs.getUuid());

        Map<String, String> resultMap = new HashMap<String, String>();

		Boolean valid = true;
		List<String> errorList  = new ArrayList();

		if (StringUtils.isBlank(paymentRequestBusinessDO.getOrderId())) {
			valid = false;
			errorList.add("payment.orderid.error");
		}

		Map<String, String> amountValidationMap = PaymentUtil.validateAmount(paymentRequestBusinessDO.getAmount());

		if(valid)
		{
			valid = StringUtils.equalsIgnoreCase(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE), "true");
		}

		errorList.add(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION));
        Integer numTxnx = this.successfulTransaction(paymentRequestBusinessDO.getOrderId());
        if (numTxnx > 0){
            errorList.add("Already a successful Txn");
            valid = false;
        }


        String hashKey = paymentRequestBusinessDO.getHashKey();

        //hashkey generation format is amount+orderid+"freecharge"+key
        String f_string = paymentRequestBusinessDO.getAmount()+paymentRequestBusinessDO.getOrderId()+"freecharge"+properties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);

		if (StringUtils.isNotBlank(hashKey) && !isAmountOverriden())
		{

			String outputString = PaymentUtil.getHashKey(f_string);

			if(!StringUtils.equalsIgnoreCase(hashKey, outputString))
			{
				valid = false;
				errorList.add("Url Tampered");
			}
		}

		resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_STATE, valid.toString());
		resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION, errorList.toString());
		resultMap.put(PaymentConstants.ORDER_ID, paymentRequestBusinessDO.getOrderId());

		if (!valid) {
		    paymentRequestBusinessDO.setResultMap(resultMap);
		}

		logger.info(errorList);
		logger.info("Validation Ends here");

		return resultMap;
	}

    private Boolean isAmountOverriden()
    {
        String overrideAmount = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_AMOUNT_OVERRIDE);

        if(StringUtils.isNotBlank(overrideAmount) && !StringUtils.equalsIgnoreCase(overrideAmount, "-1"))
        {
        	return true;
        }
        return false;
    }


    private Integer successfulTransaction(String orderId){
        return paymentTransactionService.getSuccessFullTxnForOrderId(orderId);
    }
}
