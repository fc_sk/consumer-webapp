package com.freecharge.payment.services;

import com.freecharge.rest.model.FPSParams;

import java.util.List;
import java.util.Map;

public interface TransactionTypeHandler {

    Map<String,Map<String,String>> getTransactionTypeStatus(List<Map<String, String>> cards,FPSParams fpsParams,String productType);
}
