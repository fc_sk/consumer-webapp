package com.freecharge.payment.services;

import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashMapIgnoreCase;
import com.freecharge.payment.dao.KpayPaymentMetaDAO;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.dos.entity.KpayPaymentMeta;
import com.freecharge.payment.util.PaymentConstants;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("kpayPaymentMetaService")
public class KpayPaymentMetaService {
    private static final Logger logger = LoggerFactory.getLogger(KpayPaymentMetaService.class);
    @Autowired
    private KpayPaymentMetaDAO kpayPaymentMetaDAO;

    public String getPgUsed(String mtxnId){
        List<KlickpayPaymentMeta> klickpayPaymentMetas = kpayPaymentMetaDAO.getKpayPaymentMeta(mtxnId);
        if(!FCUtil.isEmpty(klickpayPaymentMetas)) {
            KlickpayPaymentMeta klickpayPaymentMeta = klickpayPaymentMetas.get(0);
            if(null != klickpayPaymentMeta) {
                return klickpayPaymentMeta.getPgUsed();
            }
        }
        logger.error("No info about pg used for mtxnId: " + mtxnId);
        return null;
    }

    public void insertKpayPaymentMeta(String mtxnId, String pgUsed, String referenceId, String merchantId) {
        KlickpayPaymentMeta klickpayPaymentMeta = new KlickpayPaymentMeta();
        klickpayPaymentMeta.setMtxnId(mtxnId);
        klickpayPaymentMeta.setPgUsed(pgUsed);
        klickpayPaymentMeta.setReferenceId(referenceId);
        Map<String, String> metadata = new HashMap<>();
        metadata.put(PaymentConstants.KLICKPAY_MID_KEY_STRING, merchantId);
        klickpayPaymentMeta.setMetadata(new Gson().toJson(metadata));
        kpayPaymentMetaDAO.insert(klickpayPaymentMeta);
    }
}
