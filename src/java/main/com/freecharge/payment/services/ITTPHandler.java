package com.freecharge.payment.services;

import java.util.Map;

import com.freecharge.rest.model.FPSParams;

public interface ITTPHandler {

    Map<String, Object> getStatusForITTP(Map<String, String> cardParams, String userId, FPSParams fpsParams);
    Map<String, Object> resendOTP(Map<String, Object> paramsMap);
}
