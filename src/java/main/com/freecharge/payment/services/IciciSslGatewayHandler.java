package com.freecharge.payment.services;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCDateUtil;
import com.freecharge.common.util.FCInfraUtils;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.opus.epg.sfa.java.Address;
import com.opus.epg.sfa.java.BillToAddress;
import com.opus.epg.sfa.java.CustomerDetails;
import com.opus.epg.sfa.java.EPGCryptLib;
import com.opus.epg.sfa.java.EPGMerchantEncryptionLib;
import com.opus.epg.sfa.java.MPIData;
import com.opus.epg.sfa.java.Merchant;
import com.opus.epg.sfa.java.PGReserveData;
import com.opus.epg.sfa.java.PGResponse;
import com.opus.epg.sfa.java.PGSearchResponse;
import com.opus.epg.sfa.java.PostLib;
import com.opus.epg.sfa.java.SFAApplicationException;
import com.opus.epg.sfa.java.SessionDetail;
import com.opus.epg.sfa.java.ShipToAddress;

@Component("iciciSslGatewayHandler")

public class IciciSslGatewayHandler implements IGatewayHandler {

    /*properties default values*/
    private static Logger logger = LoggingFactory.getLogger(IciciSslGatewayHandler.class);
    private static final Logger refundLogger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(IciciSslGatewayHandler.class.getName()));

    private static String icici_Success_Return_Value = "0";
    private static String MECODE = "00005904";
    private static String ICICI_SSL_URL = "https://payseal.icicibank.com/mpi/Ssl.jsp";
    private static String ICICI_SSL_FREECHARGE_RETUERN_URL = "/payment/handleicsresponse.htm";
    private static String MOBILE_ICICI_SSL_FREECHARGE_RETUERN_URL = "/m/payment/response/handleicsresponse.htm";

    private static HashMap<String, String> paymentTypeMap = new HashMap<String, String>();

    private static String TXN_TYPE_REFUND = "req.Refund";
    private static String TXN_TYPE_PURCHASE = "req.Sale";

    private FCProperties properties;
    private PaymentTransactionService paymentTransactionService;

    final String DUMMY_BILLING_ADDRESS = "BillingAddress";
    final String DUMMY_BILLING_CITY = "BillingCity";
    final String DUMMY_BILLING_STATE = "BillingState";
    final String DUMMY_BILLING_PIN = "560001";
    final String DUMMY_FIRST_NAME = "CustomerName";

    @Autowired
    @Qualifier(value = "walletGatewayHandler")
    private IGatewayHandler walletGatewayHandler;

    @Autowired
    public void setProperties(FCProperties properties) {
        this.properties = properties;
    }

    @Autowired
    public void setPaymentTransactionService(PaymentTransactionService paymentTransactionService) {
        this.paymentTransactionService = paymentTransactionService;
    }

    public void loadConfigurations() {
        icici_Success_Return_Value = properties.getProperty(PaymentConstants.KEY_ICICI_SSL_SUCCESS_DECISION);
        MECODE = properties.getProperty(PaymentConstants.KEY_ICICI_SSL_MECODE);
        ICICI_SSL_URL = properties.getProperty(PaymentConstants.KEY_ICICI_SSL_URL);
        ICICI_SSL_FREECHARGE_RETUERN_URL = properties.getProperty(PaymentConstants.KEY_ICICI_SSL_RETURN_URL);
    }

    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws  Exception {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String freeChargeOrderId = paymentRequestBusinessDO.getOrderId();

        String merchantOrderId = PaymentUtil.getPaymentGatewayOrderId(paymentRequestBusinessDO, paymentTransactionService.countPGRequestForOrderId(freeChargeOrderId));

        logger.debug("ICICI SSL PG payment starts for order id: " + merchantOrderId + " and session id: " + fs.getUuid());

        String pgAmount = "";
        String amount = paymentRequestBusinessDO.getAmount();

        if (StringUtils.isNotBlank(amount)) {
            pgAmount = PaymentUtil.getDoubleTillSpecificPlaces(new Double(amount), 2).toString();
        }

        logger.debug("ICICI SSL PG payment starts for order id: " + merchantOrderId + " and amount: " + pgAmount);

        Map<String, String> retMap = new LinkedHashMap<String, String>();
        String cardOption = paymentTypeMap.get(paymentRequestBusinessDO.getPaymentType().toString());

        PGResponse oPGResponse = postPaymentData(paymentRequestBusinessDO, merchantOrderId, pgAmount);

        logger.info("Response Code :" + oPGResponse.getRespCode() + ", Response Message : " + oPGResponse.getRespMessage() + "  Merchant Txn Id : " + oPGResponse.getTxnId());

        String strRedirectionURL = null;

        // MOCK PG
        if (properties.shouldMockPG()) {
        	logger.info("[MOCK] Putting /dummypg/icicissl/pay as the payment gateway submit url");
        	strRedirectionURL = properties.getProperty("hostprefix") + "/dummypg/icicissl/pay";
        } else {
        	if (oPGResponse.getRedirectionUrl() != null) {
        		strRedirectionURL = oPGResponse.getRedirectionUrl();
        	}
        }

        fs.getSessionData().put(merchantOrderId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantOrderId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantOrderId + "_error_url", paymentRequestBusinessDO.getErrorUrl());
        fs.getSessionData().put(merchantOrderId + "_amount", paymentRequestBusinessDO.getAmount());
        fs.getSessionData().put(merchantOrderId + "_payment_option", paymentRequestBusinessDO.getPaymentOption());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        if (strRedirectionURL != null) {
            resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        } else {
            resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION);
        }

        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, retMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, strRedirectionURL);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(pgAmount));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantOrderId);
        paymentRequest.setOrderId(freeChargeOrderId);
        paymentRequest.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);
        paymentRequest.setPaymentMode("pur");
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(PaymentUtil.getMapAsString(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(PaymentUtil.getMapAsString(retMap));
        paymentRequest.setFkBbanksGrpMasterId(cardOption);

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(amount));
        pt.setMerchantTxnId(merchantOrderId);
        pt.setOrderId(freeChargeOrderId);
        pt.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);
        pt.setPaymentMode("pur");
        pt.setPaymentOption(paymentRequestBusinessDO.getPaymentOption());
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        if (strRedirectionURL == null) {
            pt.setIsSuccessful(false);
            pt.setPgMessage(oPGResponse.getRespMessage());
            pt.setRecievedToPg(Calendar.getInstance().getTime());
            pt.setTransactionId(oPGResponse.getTxnId());
        }


        pt = createPaymentTransaction(pt);

        paymentRequestBusinessDO.setResultMap(resultMap);

        fs.getSessionData().put(merchantOrderId + "_txn_id", pt.getPaymentTxnId());

        logger.debug("ICICI SSL handler execution completes. Result map : " + resultMap);
        logger.debug("ICICI SSL handler execution completes. Session Id : " + fs.getUuid());

        return resultMap;
    }

    private String getResponseURL() {
        String responseUrl = null;
        String host = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME);
        responseUrl = host + ICICI_SSL_FREECHARGE_RETUERN_URL;
        return responseUrl;
    }
    
    private String getMobileResponseURL() {
        String responseUrl = null;
        String host = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME);
        responseUrl = host + MOBILE_ICICI_SSL_FREECHARGE_RETUERN_URL;
        return responseUrl;
    }

    private PGResponse postPaymentData(PaymentRequestBusinessDO paymentRequestBusinessDO, String orderId, String pgAmount) throws Exception {
        String responseUrl = getResponseURL();
        if (MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO)){
        	responseUrl = getMobileResponseURL();
        }
        ShipToAddress oSTA = new ShipToAddress();
        MPIData oMPI = new MPIData();

        oSTA.setAddressDetails(
                "CustomerBillingAddress"
                ,"x"
                ,"x"
                ,"BillingCity"
                ,"BillingState"
                ,"560001"
                ,"IND"
                , PaymentConstants.NA
                );


        PostLib oPostLib = new PostLib();
        PGReserveData oPGReserveData = new PGReserveData();

        Merchant oMerchant = createMerchant(responseUrl, orderId, pgAmount);
        BillToAddress oBTA = createBillToAddress(paymentRequestBusinessDO);
        CustomerDetails oCustomer = createCustomer(paymentRequestBusinessDO);
        SessionDetail oSessionDetail = createSessionDetails(paymentRequestBusinessDO);

        logger.info("sending ssl hit to icici for order id : " + paymentRequestBusinessDO.getOrderId());

        return oPostLib.postSSL(oBTA, oSTA, oMerchant, oMPI, null, oPGReserveData, oCustomer, oSessionDetail, null, null);
    }

    private SessionDetail createSessionDetails(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        SessionDetail oSessionDetail = new SessionDetail();

        oSessionDetail.setSessionDetails(paymentRequestBusinessDO.getRemoteIp(), (String) paymentRequestBusinessDO.getRequestMap().get(PaymentConstants.ICICI_SECURE_COOKIE_KEY),
                "US", "en", "", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20100101 Firefox/15.0.1");
        return oSessionDetail;
    }

    private CustomerDetails createCustomer(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        CustomerDetails oCustomer = new CustomerDetails();
        Address oHomeAddress = new Address();
        Address oOfficeAddress = new Address();
        oHomeAddress.setAddressDetails(
                    DUMMY_BILLING_ADDRESS
                    ,"x"
                    ,"x"
                    ,DUMMY_BILLING_CITY
                    ,DUMMY_BILLING_STATE
                    ,DUMMY_BILLING_PIN
                    ,"IND"
                    , PaymentConstants.NA
                    );

        oOfficeAddress.setAddressDetails(
                DUMMY_BILLING_ADDRESS
                ,"x"
                ,"x"
                ,DUMMY_BILLING_CITY
                ,DUMMY_BILLING_STATE
                ,DUMMY_BILLING_PIN
                ,"IND"
                , PaymentConstants.NA
                    );

        oCustomer.setCustomerDetails(DUMMY_FIRST_NAME, "", oOfficeAddress, oHomeAddress, PaymentConstants.NA, "13-06-2007", "N");
        return oCustomer;
    }

    private BillToAddress createBillToAddress(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        BillToAddress oBTA = new BillToAddress();
        oBTA.setAddressDetails("CID", DUMMY_FIRST_NAME, DUMMY_BILLING_ADDRESS, "",
                "", DUMMY_BILLING_CITY, DUMMY_BILLING_STATE, DUMMY_BILLING_PIN,
                "IND", PaymentConstants.NA
);
        return oBTA;
    }

    private Merchant createMerchant(String responseUrl, String orderId, String pgAmount) {
        Merchant oMerchant = new Merchant();
        oMerchant.setMerchantDetails(MECODE, MECODE, MECODE, FCInfraUtils.getLocalIP(), orderId
                , orderId, responseUrl, "POST", "INR", "INV123", TXN_TYPE_PURCHASE, pgAmount
                , "GMT+05:30", "Ext1", "true", "Ext3", "Ext4", "Ext5");
        return oMerchant;
    }

    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws  Exception {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        logger.info("ICICI SSL response handling starts, parameters recieved are: " + requestMap);

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String data = requestMap.get("DATA");

        Map<String, String> map = convertPaymentResponseToMap(data);

        String respcd = map.get("RespCode");
        String respmsg = map.get("Message").replace('+', ' ');
        String AuthIdCode = map.get("AuthIdCode");
        String RRN = map.get("RRN");
        String MerchantTxnId = map.get("TxnID");
        String TxnRefNo = map.get("ePGTxnID");

        Boolean isSuccessful = false;
        if (StringUtils.equalsIgnoreCase(respcd, icici_Success_Return_Value)) {
            isSuccessful = true;
        }

        String redirectUrl = null;
        String errorUrl = null;
        String mode = null;
        String productOrderId = null;
        Boolean errorExists = false;
        String trackAmount = null;

        trackAmount = FCSessionUtil.getOrderAmount(MerchantTxnId, fs);
        productOrderId = FCSessionUtil.getProductOrderId(MerchantTxnId, fs);
        redirectUrl = FCSessionUtil.getSuccessUrl(MerchantTxnId, fs);
        errorUrl = FCSessionUtil.getErrorUrl(MerchantTxnId, fs) + "&payment=fail";

        if (errorExists) {
            isSuccessful = false;
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, isSuccessful.toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, respmsg);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, respmsg);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, productOrderId);
        resultMap.put(PaymentConstants.AMOUNT, trackAmount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, TxnRefNo);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, mode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, MECODE);

        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);

        if (isSuccessful) {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);
        } else {
            finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, errorUrl);
        }

        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(MerchantTxnId);


        PaymentResponse paymentResponse = new PaymentResponse(null, isSuccessful, TxnRefNo, productOrderId, MerchantTxnId, PaymentUtil.getMapAsString(requestMap),
                new Double(trackAmount), PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE, respmsg, RRN, AuthIdCode, null, null, null, PaymentUtil.getMapAsString(resultMap), null,null);

        paymentResponse = dumpResponsetAudit(paymentResponse);

        Integer id = (Integer) fs.getSessionData().get(MerchantTxnId + "_txn_id");

        logger.debug("updating payment txn for id : " + id);

        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(id);
        
        paymentTransaction.setTransactionId(TxnRefNo);
        paymentTransaction.setPaymentTxnId(id);
        paymentTransaction.setPgTransactionTime(Calendar.getInstance().getTime());
        paymentTransaction.setIsSuccessful(paymentResponse.getIsSuccessful());
        paymentTransaction.setPaymentResponseId(paymentResponse.getPaymentRsId());
        paymentTransaction.setPgTransactionTime(paymentResponse.getPgTransactionTime());
        paymentTransaction.setRecievedToPg(Calendar.getInstance().getTime());

        updatePaymentTransaction(paymentTransaction);
        
        paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, false);

        logger.info("ICICI handling response ends here." + productOrderId);

        return finalMap;
    }

    private Map<String, String> convertPaymentResponseToMap(String data) throws Exception {
        String validatedData = validateEncryptedData(data, MECODE);

        logger.info("ICICI SSL response handling starts, parameters recieved are: " + validatedData);

        Map<String, String> map = new HashMap<String, String>();

        StringTokenizer oStringTokenizer = new StringTokenizer(validatedData, "&");
        while (oStringTokenizer.hasMoreElements()) {
            String strData = (String) oStringTokenizer.nextElement();
            StringTokenizer oObj1 = new StringTokenizer(strData, "=");
            String strKey = (String) oObj1.nextElement();
            String strValue = (String) oObj1.nextElement();
            map.put(strKey, strValue);
        }
        return map;
    }

    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String RefundType) throws  Exception {
        refundLogger.info(String.format("Start - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(), PaymentGateways.ICIC));

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();

        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));

        logger.debug("Refund processing for orderid : " + paymentRefundBusinessDO.getOrderId());
        refundLogger.info("Refund processing for orderid : " + paymentRefundBusinessDO.getOrderId());

        String amount = paymentRefundBusinessDO.getRefundAmount().toString();

        String transid = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getTransactionId();
        String authCode = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getAuthCode();
        String rrno = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getRrno();
        String merchantTxnId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId();

        com.opus.epg.sfa.java.Merchant oMerchant = new com.opus.epg.sfa.java.Merchant();

        PostLib oPostLib = new PostLib();

        oMerchant.setMerchantRelatedTxnDetails(MECODE, null, null,
                merchantTxnId, transid, rrno, authCode, null, null, "INR",
                TXN_TYPE_REFUND, amount, "GMT+05:30", "Ext1", "true", "Ext3", "Ext4", "Ext5");


        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest("");
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.REQUESTED);

        paymentTransactionService.create(paymentRefundTransaction);

        PGResponse oPgResp = oPostLib.postRelatedTxn(oMerchant);

        String responseCode = oPgResp.getRespCode();
        String desc = oPgResp.getRespMessage();
        String tno = oPgResp.getEpgTxnId();

        Boolean isSuccessful = false;

        if (StringUtils.equalsIgnoreCase(responseCode, icici_Success_Return_Value.toString())) {
            isSuccessful = true;
        }

        paymentRefundBusinessDO.setStatus(isSuccessful);
        paymentRefundBusinessDO.setMsg(desc);

        paymentRefundTransaction.setPgResponseCode(responseCode);
        paymentRefundTransaction.setPgResponseDescription(desc);
        paymentRefundTransaction.setPgStatus(isSuccessful.toString());
        paymentRefundTransaction.setPgTransactionReferenceNo(tno);
        paymentRefundTransaction.setRawPGResponse("");
        paymentRefundTransaction.setReceivedFromPG(new Date());

        if (isSuccessful) {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.INITIATED);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refunded successfully.");
        } else {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            refundLogger.info(paymentRefundBusinessDO.getOrderId() + " - refund failed.");
        }

        paymentRefundTransaction.addToWhereFields("paymentRefundTransactionId");

        paymentTransactionService.update(paymentRefundTransaction);

        logger.debug("Refund processing ends for orderid : " + paymentRefundBusinessDO.getOrderId());
        refundLogger.info(String.format("End - Processing refund for %s in %s", paymentRefundBusinessDO.getOrderId(), PaymentGateways.ICIC));
        return paymentRefundBusinessDO;
    }

    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
        String txnRefId = paymentQueryBusinessDO.getMerchantTxnId();
        String orderId = paymentQueryBusinessDO.getOrderId();

        com.opus.epg.sfa.java.Merchant oMerchant = new com.opus.epg.sfa.java.Merchant();

        PostLib oPostLib;
        try {
            oPostLib = new PostLib();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        
        oMerchant.setMerchantOnlineInquiry(MECODE, txnRefId);

        PGSearchResponse oPgSearchResp = oPostLib.postStatusInquiry(oMerchant);

        ArrayList oPgRespList = oPgSearchResp.getPGResponseObjects();

        logger.info("Received response :" + oPgSearchResp.toString());

        Boolean isSuccessful = false;
        String description = "";

        PGResponse oPgResp = null;

        if (oPgRespList != null && !oPgRespList.isEmpty()) {
            for (Object obj : oPgRespList) {
                if (obj instanceof PGResponse) {
                    oPgResp = (PGResponse) obj;
                    break;
                }
            }
        }
        String pgResponseCode = null;
        String responseDesc = null;
        Timestamp pgTransactionTime = new Timestamp(System.currentTimeMillis());

        if (oPgResp != null) {
            pgResponseCode = oPgResp.getRespCode();
            responseDesc = oPgResp.getRespMessage();
            String timeStampString = oPgResp.getTxnDateTime();
            
            if(timeStampString != null && !timeStampString.trim().isEmpty()) {
                pgTransactionTime = FCDateUtil.convertStringToTimestamp(timeStampString, "dd/MM/yyyy hh:mm:ss");
            }
            
            paymentQueryBusinessDO.setPgTransactionId(oPgResp.getEpgTxnId());
        }

        if (StringUtils.equalsIgnoreCase(pgResponseCode, icici_Success_Return_Value.toString())) {
            isSuccessful = true;
            description = responseDesc;
        } else {
            isSuccessful = false;
        }

        paymentQueryBusinessDO.setPaymentStatus(isSuccessful);
        paymentQueryBusinessDO.setResponseMsg(description);
        paymentQueryBusinessDO.setPgTransactionTime(pgTransactionTime);
        
        PaymentTransaction paymentTransaction = new PaymentTransaction();
        paymentTransaction.setIsSuccessful(isSuccessful);
        paymentTransaction.setMerchantTxnId(txnRefId);
        paymentTransaction.setOrderId(orderId);
        paymentTransaction.setAmount(paymentQueryBusinessDO.getAmount());
        
        paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, isPaymentStatusForAdminPanel);

        logger.info("ICICI SSL handling response ends here." + orderId);

        return paymentQueryBusinessDO;
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {
        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }

    private PaymentResponse dumpResponsetAudit(PaymentResponse paymentResponse) {
        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;

    }

    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
            pt = paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
        return pt;
    }

    private void updatePaymentTransaction(PaymentTransaction pt) {
        try {
            paymentTransactionService.updatePaymentTransaction(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
    }

    private String validateEncryptedData(String astrResponseData, String strMerchantId) throws Exception {
        EPGMerchantEncryptionLib oEncryptionLib = new EPGMerchantEncryptionLib();
        String astrClearData = null;
        try {
            InputStream inputStream = IciciSslGatewayHandler.class.getClassLoader().getResourceAsStream(strMerchantId + ".key");
//                 FileInputStream oFileInputStream =  new FileInputStream(new File(astrDirectoryPath + strMerchantId+".key"));
            BufferedReader oBuffRead = new BufferedReader(new InputStreamReader(inputStream));
            String strModulus = oBuffRead.readLine();
            if (strModulus == null) {
                logger.error("Invalid credentials. Transaction cannot be processed");
                throw new SFAApplicationException("Invalid credentials. Transaction cannot be processed");
            }
            strModulus = decryptMerchantKey(strModulus, strMerchantId);
            if (strModulus == null) {
                logger.error("Invalid credentials. Transaction cannot be processed");
                throw new SFAApplicationException("Invalid credentials. Transaction cannot be processed");
            }
            String strExponent = oBuffRead.readLine();
            if (strExponent == null) {
                logger.error("Invalid credentials. Transaction cannot be processed");
                throw new SFAApplicationException("Invalid credentials. Transaction cannot be processed");
            }
            strExponent = decryptMerchantKey(strExponent, strMerchantId);
            if (strExponent == null) {
                logger.error("Invalid credentials. Transaction cannot be processed");
                throw new SFAApplicationException("Invalid credentials. Transaction cannot be processed");
            }
            astrClearData = oEncryptionLib.decryptDataWithPrivateKeyContents(astrResponseData, strModulus, strExponent);

        } catch (Exception oEx) {
            logger.error("Error in validating encrypted data", oEx);
        } finally {
            return astrClearData;
        }
    }

    private String decryptMerchantKey(String astrData, String astrMerchantId) throws Exception {
        return (decryptData(astrData, (astrMerchantId + astrMerchantId).substring(0, 16)));
    }


    public String decryptData(String strData, String strKey) throws Exception {
        if (StringUtils.isEmpty(strData) || StringUtils.isEmpty(strKey)) {
            return null;
        }

        EPGCryptLib moEPGCryptLib = new EPGCryptLib();
        String strDecrypt = moEPGCryptLib.Decrypt(strKey, strData);
        return strDecrypt;
    }

    private String formatCurrency(String strNum, boolean decimal) {
        String strTemp = "";
        String[] arrNum;
        String strDec = "";
        if (decimal) {
            arrNum = strNum.split("\\.");
            strDec = (arrNum.length > 1) ? arrNum[1] : "";
            if (arrNum.length > 1) {
                strNum = arrNum[0];
            }
        }
        if (strNum.length() > 3) {
            strTemp = strNum.substring(strNum.length() - 3, strNum.length());
            strNum = strNum.substring(0, strNum.length() - 3);
            while (strNum.length() > 2) {
                strTemp = strNum.substring(strNum.length() - 2, strNum.length()) + "," + strTemp;
                strNum = strNum.substring(0, strNum.length() - 2);
            }
            strNum += "," + strTemp;
        }
        if (decimal && !strDec.equals("")) {
            strNum += "." + strDec;
        }
        return "INR" + strNum;
    }

	@Override
	public Map<String, Object> handleS2SPaymentResponse(
			PaymentResponseBusinessDO paymentResponseBusinessDO)
			throws Exception {
		throw new RuntimeException("Unimplemented method");
	}

	@Override
    @Transactional
	public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws  Exception {
        walletGatewayHandler.refundToWallet(paymentRefundBusinessDO, refundType);
	}

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        // TODO Auto-generated method stub
        return null;
    }
}
