package com.freecharge.payment.services;

import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.GSTService;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mobile.service.util.CommonUtil;
import com.freecharge.mobile.web.util.ConvenienceFeeUtil;
import com.freecharge.mongo.entity.PaymentHistoryEntry;
import com.freecharge.mongo.repos.PaymentHistoryRepository;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentRefundService.PaymentStatusTransition;
import com.freecharge.payment.services.binrange.CardBin;
import com.freecharge.payment.services.binrange.CardBinRangeService;
import com.freecharge.payment.services.binrange.CardIssuer;
import com.freecharge.payment.services.binrange.PaymentResponseEvent;
import com.freecharge.payment.util.BilldeskCardHash;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.RefundAttemptAlertSNSService;
import com.freecharge.sns.RefundStatusAlertSNSService;
import com.freecharge.sns.bean.GSTFee;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.sns.bean.RefundAttemptBean;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.web.webdo.FCUPIWebDO;
import com.freecharge.web.webdo.UPIAccountDetails;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * The base class that implements payments processing functionalities
 * and delegates any payment gateway specific functions to the 
 * appropriate payment gateway handler. All payment gateway
 * handlers should extend this class and implement only PG specific
 * functions.
 * @author arun
 *
 */
public abstract class PaymentGatewayHandler implements IGatewayHandler {
    Logger logger = Logger.getLogger(PaymentGatewayHandler.class);

    @Autowired
    @Qualifier(value = "walletGatewayHandler")
    private IGatewayHandler walletGatewayHandler;
    
    @Autowired
    protected PaymentTransactionService paymentTransactionService;

    @Autowired
    protected FCProperties fcProperties;
    
    @Autowired
    protected WalletService walletService;
    
    @Autowired
    protected CartService cartService;
    
    @Autowired
    protected UsersDAO usersDAO;
    
    @Autowired
    protected MetricsClient metricsClient;
    
    @Autowired
    private FreechargeEventPublisher freechargeEventPublisher;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    CardBinService cardBinService;

    @Autowired
    PaymentsCache paymentsCache;
    
    @Autowired
    private CardBinRangeService cardBinRangeService;
    
    @Autowired
    private PaymentSNSService paymentSNSService;
        
    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private OrderIdReadDAO orderIdReadDAO;

    @Autowired
    OrderIdWriteDAO orderIdWriteDAO;

    @Autowired
    PaymentService paymentService;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    OrderIdSlaveDAO orderIdSlaveDAO;

    @Autowired
    UserTransactionHistoryDAO userTransactionHistoryDAO;

    @Autowired
    private RefundAttemptAlertSNSService refundAttemptAlertSNSService;

    @Autowired
	private RefundStatusAlertSNSService refundStatusAlertSNSService;
  
    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    private PaymentHistoryRepository paymentHistoryRepository;
    
    @Autowired
    private PaymentOptionCache paymentOptionCache;
        
    @Autowired
    private PaymentOptionService paymentOptionService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    @Autowired
    private OneCheckWalletService oneCheckWalletService;

    @Autowired
    private PaymentRefundService paymentRefundService;
    
    @Autowired
    private ConvenienceFeeUtil convenienceFeeUtil;
    
    @Autowired
	private AppConfigService appConfig;
    
    @Autowired
    private GSTService gstService;

    private static final String CARD_DATA_CACHE_KEY = "_CARD_DATA";

	private static final String EMPTY = "";
    
	private Map<String, String> paymentModeMap = ImmutableMap.<String, String> builder()
			.put(PaymentConstants.PAYMENT_TYPE_CREDITCARD, "CC").put(PaymentConstants.PAYMENT_TYPE_DEBITCARD, "DC")
			.put(PaymentConstants.PAYMENT_TYPE_NETBANKING, "NB").put(PaymentConstants.PAYMENT_TYPE_CASHCARD, "CS")
			.put(PaymentConstants.PAYMENT_TYPE_WALLET, "WP").put(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET, "OCW")
			.build();

    
    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws FCPaymentGatewayException, Exception {
        Map<String, String> payResponseMap = paymentResponseBusinessDO.getRequestMap();

        logger.info("Payment response handling starts. RequestMap: " + PaymentUtil.getResponseDataWithoutCardData(payResponseMap));
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        String productOrderId = null;
        String successUrl = null;
        String errorUrl = null;

        String mtxnId = getMerchantTransactionId(payResponseMap);
        Integer id = (Integer) fs.getSessionData().get(mtxnId + "_txn_id");
        productOrderId = FCSessionUtil.getProductOrderId(mtxnId, fs);
        //If id is not found in session get it from DB
        if (id == null || productOrderId == null) {
            PaymentTransaction paymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderIdAndPG(mtxnId, PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
            
            
            id = paymentTxn.getPaymentTxnId();
            productOrderId = paymentTxn.getOrderId();
        }
        successUrl = FCSessionUtil.getSuccessUrl(mtxnId, fs);
        errorUrl = FCSessionUtil.getErrorUrl(mtxnId, fs) + "&payment=fail";

//        logger.info("Payment response handling ends");

        return handlePaymentResponse(paymentResponseBusinessDO, productOrderId, id, successUrl, errorUrl, false);
    }
    
    @Transactional
    protected Map<String, Object> handlePaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO,
                        String orderId, Integer paymentTxnId, String successUrl, String errorUrl,
                        Boolean isPaymentStatusForAdminPanel) {
        Map<String, String> payResponseMap = paymentResponseBusinessDO.getRequestMap();
        
        validatePaymentResponse(payResponseMap);

        return handleValidPayUResponse(paymentResponseBusinessDO, orderId, paymentTxnId, successUrl, errorUrl,
                isPaymentStatusForAdminPanel, payResponseMap);
    }

    /**
     * A common method to handle payment response from web
     * as well as status check call.
     * @param paymentResponseBusinessDO
     * @param orderId
     * @param paymentTxnId
     * @param successUrl
     * @param errorUrl
     * @param isPaymentStatusForAdminPanel
     * @param payResponseMap
     * @return
     */
    protected Map<String, Object> handleValidPayUResponse(PaymentResponseBusinessDO paymentResponseBusinessDO,
            String orderId, Integer paymentTxnId, String successUrl, String errorUrl,
            Boolean isPaymentStatusForAdminPanel, Map<String, String> payResponseMap) {
        PaymentResponse paymentResponse = createPaymentResponse(payResponseMap);
        paymentResponseBusinessDO.setmTxnId(paymentResponse.getMtxnId());
        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTxnId);
        paymentResponseBusinessDO.setPaymentType(paymentTransaction.getPaymentType());
        paymentResponse.setAmount(paymentTransaction.getAmount());
        String redirectUrl = null;

        if (paymentResponse.getIsSuccessful()) {
            redirectUrl = successUrl;
        } else {
            redirectUrl = errorUrl;
        }

        Map<String, String> resultMap = new HashMap<String, String>();

        String transactionId = paymentResponse.getPgTransactionId();
        String responseCode = paymentResponse.getPgMessage();

        String amount = paymentTransaction.getAmount().toString();
        
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, paymentResponse.getIsSuccessful().toString());
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, responseCode);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, null);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, getPaymentPortalMerchantId());
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED, Boolean.toString(false));
        resultMap.put(PaymentConstants.MERCHANT_TXN_ID, paymentResponse.getMtxnId());

        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);
        finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, redirectUrl);

        paymentResponseBusinessDO.setResultMap(finalMap);

        String binNumber = payResponseMap.get(RequestParameters.PAYU_CARD_NUMBER);
        if (binNumber != null && !binNumber.isEmpty()) {
            paymentResponseBusinessDO.setCardBinHead(StringUtils.left(binNumber, 6));
            paymentResponseBusinessDO.setCardBinTail(StringUtils.right(binNumber, 4));
        } else{
        	binNumber = payResponseMap.get(RequestParameters.PAYU_S2S_CARD_NUMBER);
        	if (binNumber != null && !binNumber.isEmpty()) {
                paymentResponseBusinessDO.setCardBinHead(StringUtils.left(binNumber, 6));
                paymentResponseBusinessDO.setCardBinTail(StringUtils.right(binNumber, 4));
            }
        }

        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
        paymentResponse = dumpResponsetAudit(paymentResponse);
        paymentResponse.setReconMetaData(new ReconMetaData(paymentResponse.getMtxnId(), transactionId, "NA", PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE, transactionId));
        Map<String, String> responseDataMap = parsePGResponseIntoMap(paymentResponse);
        
        logger.debug("updating payment txn for id : " + paymentTxnId);
        
        boolean paymentAlreadyAck= acknoledgePaymentTransactionUpdate(paymentTransaction, orderId, transactionId, paymentTxnId, paymentResponse, paymentResponseBusinessDO, isPaymentStatusForAdminPanel);
        resultMap.put(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED,
                Boolean.toString(paymentAlreadyAck));
        if (!paymentAlreadyAck) {
            Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId());
            Users users = userServiceProxy.getUserByUserId(userId);
            paymentService.updateTransactionHistoryForTransaction(paymentTransaction.getOrderId(), users.getUserId(), paymentTransaction.getIsSuccessful() ? PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS : PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE);
            paymentTransactionService.handlePostPaymentSuccessTasks(paymentTransaction, isPaymentStatusForAdminPanel);
            publishPaymentResponseEvent(paymentTransaction.getOrderId(), paymentResponse.getIsSuccessful(), payResponseMap);
			/*
			 * The payment type is validated to ensure that the final mode of
			 * payment used for payment is same as the payment mode selected on
			 * FC site. There is an option for the user to change the mode of
			 * payment once they reach PayU site.
			 */
            validatePaymentDataFromPayU(paymentTransaction, paymentResponse);
            logPaymentStatusMetric(paymentResponseBusinessDO);
            if(paymentTransaction.getIsSuccessful()){
            	storeCardHashForSuccessfulPayment(paymentResponseBusinessDO);
            } else{
            	logPaymentFailureMetrics(paymentResponse, responseDataMap);
            }
            if(responseDataMap.containsKey(PaymentConstants.PAYU_RESPONSE_GATEWAY_FIELD)){
            	metricsClient.recordEvent("Payment", "PayU.Gateway." + responseDataMap.get(PaymentConstants.PAYU_RESPONSE_GATEWAY_FIELD), paymentTransaction.getIsSuccessful().toString());
            }
        }
        logger.info("Handling payment response ends here." + paymentResponse.getMtxnId());
        return finalMap;
    }

    /**
     * Logging metric for payment success/failure per issuing bank
     */
    @SuppressWarnings("unchecked")
    protected void logPaymentStatusMetric(PaymentResponseBusinessDO paymentResponseBusinessDO) {
        if (paymentResponseBusinessDO.getCardBinHead() != null && !paymentResponseBusinessDO.getCardBinHead().isEmpty()
                && paymentResponseBusinessDO.getCardBinTail() != null
                && !paymentResponseBusinessDO.getCardBinTail().isEmpty()) {
            CardIssuer cardIssuer = cardBinRangeService.getCardIssuer(paymentResponseBusinessDO.getCardBinHead(),
                    paymentResponseBusinessDO.getCardBinTail());
            String issuerName = "NA";
            if (cardIssuer != null) {
                issuerName = cardIssuer.getIssuerName();
            }
            Map<String, String> dataMap = (Map<String, String>) paymentResponseBusinessDO.getResultMap().get(
                    PaymentConstants.DATA_MAP_FOR_WAITPAGE);
            List<PaymentTransaction> paymentTransactions = paymentTransactionService
                    .getPaymentTransactionByMerchantOrderId(paymentResponseBusinessDO.getmTxnId());
            if (paymentTransactions != null && !paymentTransactions.isEmpty()) {
                this.metricsClient.recordEvent("Payment.Status", (paymentTransactions.size() == 1 ? paymentTransactions.get(0).getPaymentType() : paymentTransactions.get(1).getPaymentType()) + "."
                        + issuerName, dataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL).toString());
            }
        }
    }
    
	/**
	 * Method validates that the payment type/option selected by the user on FC is
	 * the final mode of payment used to make the payment.<br>
	 * If not, the payment_txn entry will be updated with correct paymentType
	 * and paymentOption
	 * 
	 */
    private void validatePaymentDataFromPayU(
			PaymentTransaction paymentTransaction, PaymentResponse paymentResponse) {
    	Map<String, String> responseDataMap = parsePGResponseIntoMap(paymentResponse);
		String paymentTypeOnPayU = responseDataMap.get(PaymentConstants.PAYU_RESPONSE_PAYMENT_TYPE_FIELD); // CC,DC,NB
		boolean isPaymentTypeUpdated = false;
		boolean isPaymentOptionUpdated = false;
		if (StringUtils.isNotEmpty(paymentTypeOnPayU)) {
			isPaymentTypeUpdated = validateAndUpdateFinalPaymentType(paymentTransaction, paymentTypeOnPayU);
			if(isPaymentTypeUpdated){
				metricsClient.recordEvent("Payment", "PayU.Update", "PaymentType");
			}
		}
		
		String paymentOptionOnPayU = responseDataMap.get(PaymentConstants.PAYU_RESPONSE_PAYMENT_OPTION_FIELD);
		
			if (paymentTransaction.getPaymentType() == 1) {
				if (StringUtils.isNotEmpty(paymentOptionOnPayU)) {
				isPaymentOptionUpdated = setFinalCreditCardOption(paymentTransaction,paymentOptionOnPayU);
				}
			} else if (paymentTransaction.getPaymentType() == 2) {
				if (StringUtils.isNotEmpty(paymentOptionOnPayU)) {
					isPaymentOptionUpdated = setFinalDebitCardOption(paymentTransaction,paymentOptionOnPayU, responseDataMap.get(PaymentConstants.PAYU_RESPONSE_BANK_CODE_FIELD));
				}
			} else if (paymentTransaction.getPaymentType() == 3) {
				isPaymentOptionUpdated = setFinalNetBankingCode(paymentTransaction, paymentResponse);
			}
			if(isPaymentOptionUpdated){
				metricsClient.recordEvent("Payment", "PayU.Update", "PaymentOption");
			}
		
		
		if(isPaymentTypeUpdated || isPaymentOptionUpdated){
			paymentTransactionService.updatePaymentTransactionDetails(paymentTransaction);
		}
	}

    /**
     * Method to validate and set payment option for debit card payments
     * 
     * @param paymentTransaction
     * @param paymentOptionOnPayU
     */
	private boolean setFinalDebitCardOption(PaymentTransaction paymentTransaction, String paymentOptionOnPayU, String bankCodeOnPayU) {
		if (paymentOptionOnPayU
				.equalsIgnoreCase(PayUGatewayHandler.Constants.DEBIT_CARD_MASTER_BANK_CODE)
				&& !paymentTransaction.getPaymentOption().equals(
						PaymentConstants.MASTER_DC)) {
			paymentTransaction.setPaymentOption(PaymentConstants.MASTER_DC);
			return true;
		} else if (paymentOptionOnPayU
				.equalsIgnoreCase(PayUGatewayHandler.Constants.DEBIT_CARD_VISA_BANK_CODE)
				&& !paymentTransaction.getPaymentOption().equals(
						PaymentConstants.VISA_DC)) {
			paymentTransaction.setPaymentOption(PaymentConstants.VISA_DC);
			return true;
		} else if (paymentOptionOnPayU
				.equalsIgnoreCase(PayUGatewayHandler.Constants.DEBIT_CARD_MAESTRO_BANK_CODE)) {

			if (bankCodeOnPayU != null && bankCodeOnPayU
					.equals(PayUGatewayHandler.Constants.DEBIT_CARD_SBI_MAESTRO_BANK_CODE)
					&& !paymentTransaction.getPaymentOption().equals(
							PaymentConstants.MAESTRO_SBI)) {
				paymentTransaction
						.setPaymentOption(PaymentConstants.MAESTRO_SBI);
				return true;
			} else if (!bankCodeOnPayU
					.equals(PayUGatewayHandler.Constants.DEBIT_CARD_SBI_MAESTRO_BANK_CODE) && !paymentTransaction.getPaymentOption().equals(
					PaymentConstants.MAESTRO_CITI)) {
				paymentTransaction
						.setPaymentOption(PaymentConstants.MAESTRO_CITI);
				return true;
			}
		} else if (paymentOptionOnPayU
				.equalsIgnoreCase(PayUGatewayHandler.Constants.DEBIT_CARD_RUPAY_BANK_CODE)
				&& !paymentTransaction.getPaymentOption().equals(
						PaymentConstants.RUPAY_DC)) {
			paymentTransaction.setPaymentOption(PaymentConstants.RUPAY_DC);
			return true;
		}
		return false;
	}

	/**
	 * Method to validate and set final credit card options
	 * 
	 * @param paymentTransaction
	 * @param paymentOptionOnPayU
	 */
	private boolean setFinalCreditCardOption(
			PaymentTransaction paymentTransaction, String paymentOptionOnPayU) {
		if (paymentOptionOnPayU.equalsIgnoreCase("VISA")
				&& !paymentTransaction.getPaymentOption().equals(
						PaymentConstants.VISA_CC)) {
			paymentTransaction.setPaymentOption(PaymentConstants.VISA_CC);
			return true;
		} else if (!paymentOptionOnPayU.equalsIgnoreCase("VISA") &&
				!paymentTransaction.getPaymentOption().equals(
				PaymentConstants.MASTER_CC)) {
			paymentTransaction.setPaymentOption(PaymentConstants.MASTER_CC);
			return true;
		}
		return false;
	}

	/**
	 * Method to validate and set final netbanking options
	 * 
	 * @param paymentTransaction
	 * @param paymentResponse
	 */
	private boolean setFinalNetBankingCode(PaymentTransaction paymentTransaction,
			PaymentResponse paymentResponse) {
		String netBankingOptionOnPayU = paymentResponse.getAvsCode();
		String finalNetBankingCode = null;
		if (StringUtils.isNotEmpty(netBankingOptionOnPayU)) {
			for (PaymentConstants.PayUNBCodeMapping codeMapping : PaymentConstants.nbCodeMappings) {
				if (codeMapping.getPayUCode().equalsIgnoreCase(
						netBankingOptionOnPayU)) {
					finalNetBankingCode = codeMapping.getFcCode();
					break;
				}
			}
		}
		if(StringUtils.isNotEmpty(finalNetBankingCode) && !paymentTransaction.getPaymentOption().equalsIgnoreCase(finalNetBankingCode)){
			paymentTransaction.setPaymentOption(finalNetBankingCode);
			return true;
		}
		return false;
	}
	
	/**
	 * Method to validate and set final payment type 
	 * 
	 * @param paymentTransaction
	 * @param paymentTypeOnPayU
	 */
	private boolean validateAndUpdateFinalPaymentType(PaymentTransaction paymentTransaction,
			String paymentTypeOnPayU) {
		if (paymentTypeOnPayU.equals("CC")
				&& paymentTransaction.getPaymentType() != Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD)) {
			paymentTransaction.setPaymentType(1);
			return true;
		} else if (paymentTypeOnPayU.equals("DC")
				&& paymentTransaction.getPaymentType() != Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD)) {
			paymentTransaction.setPaymentType(2);
			return true;
		} else if (paymentTypeOnPayU.equals("NB")
				&& paymentTransaction.getPaymentType() != Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING)) {
			paymentTransaction.setPaymentType(3);
			return true;
		}
		return false;
	}

	/**
     * Log all metrics related to payment failure on PayU
     * 
     * @param paymentResponse
     * @param responseDataMap
     */
	private void logPaymentFailureMetrics(PaymentResponse paymentResponse,
			Map<String, String> responseDataMap) {
		metricsClient.recordCount("Payment", "PayU.Fail." + (paymentResponse.getRrnNo() == null ? "UNKNOWN" : paymentResponse.getRrnNo()), 1);
		if(responseDataMap.containsKey(PaymentConstants.PAYU_RESPONSE_ERROR_MSSG_FIELD)){
			if(responseDataMap.get(PaymentConstants.PAYU_RESPONSE_ERROR_MSSG_FIELD).trim().equals(PaymentConstants.PAYU_RISK_VIOLATION_RESPONSE_MSSG)){
				metricsClient.recordCount("Payment", "PayU.RiskRule.Violation", 1);
			}
		}
	}
    
    /**
     * Method to parse response data from PayU and build a map
     * @param paymentResponse
     */
    private Map<String, String> parsePGResponseIntoMap(PaymentResponse paymentResponse) {
    	Map<String, String> responseTokenMap = new HashMap<String, String>();
    	String[] responseTokens = paymentResponse.getResponseFromPp().split("::");
		for (String responseToken : responseTokens) {
			if (responseToken != null) {
				String[] responseTokenParams = responseToken.split("==>>");
				if(responseTokenParams != null && responseTokenParams.length == 2 && responseTokenParams[0] != null && responseTokenParams[1] != null){
					responseTokenMap.put(responseTokenParams[0].trim(), responseTokenParams[1].trim());
				}
			}
		}
		return responseTokenMap;	
	}

	private boolean updatePaymentTransaction(PaymentTransaction pt) {
        return (paymentTransactionService.updatePaymentTransaction(pt) == 1) ? true : false;
    }
    
    private void publishPaymentResponseEvent(String orderId, Boolean isPaySuccess, Map<String, String> payResponseMap) {
        PaymentResponseEvent payResponseEvent = new PaymentResponseEvent(this);
        
        CardBin cardBin = new CardBin();
        
        cardBin.setCardBin(payResponseMap.get(RequestParameters.PAYU_CARD_NUMBER));
        cardBin.setNameOnCard(payResponseMap.get(RequestParameters.NAME_ON_CARD));
        cardBin.setCardNature(payResponseMap.get(RequestParameters.CARD_NATURE));
        cardBin.setIssuingBank(payResponseMap.get(RequestParameters.CARD_ISSUER_BANK));
        cardBin.setCardType(payResponseMap.get(RequestParameters.CARD_TYPE));
        
        payResponseEvent.setCardBin(cardBin);
        payResponseEvent.setOrderId(orderId);
        payResponseEvent.setIsPaymentSuccessful(isPaySuccess);
        freechargeEventPublisher.publish(payResponseEvent);
    }
    
    protected void notifyPayment(PaymentTransaction paymentTransaction,
            PaymentResponse paymentResponse, PaymentResponseBusinessDO paymentResponseBusinessDO,Boolean manualStatusCheck, String transactionId) {
        logger.info("paymentSNSService publish triggered for orderId : " + paymentTransaction.getOrderId());
        PaymentAlertBean paymentAlertBean = new PaymentAlertBean();
        try {
            Map<String, String> payResponseMap = parsePGResponseIntoMap(paymentResponse);
            paymentAlertBean.setManualStatusCheck(manualStatusCheck);
            paymentAlertBean.setMtxnId(paymentTransaction.getMerchantTxnId());
            paymentAlertBean.setOrderId(paymentTransaction.getOrderId());
            paymentAlertBean.setUpiAccountDetails(getFCUpiPaymentObject(paymentResponse.getUpiAccountDetails()));
            paymentAlertBean.setRrn(paymentResponse.getRrnNo());
            paymentAlertBean.setPgMessage(paymentResponse.getPgMessage());
            paymentAlertBean.setMessageType(paymentTransaction.getIsSuccessful() ? PaymentStatus.PAYMENT_SUCCESS.getStatus()
                    : PaymentStatus.PAYMENT_FAILURE.getStatus());

            String paymentTypeName = PaymentType.PaymentTypeName.fromPaymentTypeCode(paymentTransaction.getPaymentType()).toString();
            paymentAlertBean.setPaymentType(paymentTypeName);

            if (paymentTransaction.getPaymentType() == 1 || paymentTransaction.getPaymentType() == 2) {
                String binHead = null, binTail = null;
                if(paymentResponseBusinessDO != null){
                	if(StringUtils.isNotEmpty(paymentResponseBusinessDO.getCardBinHead()) && StringUtils.isNotEmpty(paymentResponseBusinessDO.getCardBinTail())){
                		binHead = paymentResponseBusinessDO.getCardBinHead();
                		binTail = paymentResponseBusinessDO.getCardBinTail();
                	}
                }
                
                if (binHead != null && binTail != null) {
                    paymentAlertBean.setCardBin(String.format("%sXXXXXX%s", binHead, binTail));
                }
                
                setNonPCICardDetailsToBean(paymentAlertBean, paymentResponse, paymentResponseBusinessDO);

            } else if(paymentTransaction.getPaymentType() == 3){
            	paymentAlertBean.setNetBankingName(paymentTransaction.getPaymentOption());
            }

            paymentAlertBean.setChannel(FCUtil.getChannelFromChannelType(getChannelFromOrderId(paymentTransaction.getOrderId())));
            if (!StringUtils.isEmpty(paymentResponse.getPaymentGateway())) {
                logger.info("Payment Gateway for MTxn Id : " + paymentTransaction.getMerchantTxnId() + " : " + paymentResponse.getPaymentGateway());
                logger.info("ECI for MTxn Id : " + paymentTransaction.getMerchantTxnId() + " : " + paymentResponse.getEci());
                if (!paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE)) {
                    paymentAlertBean.setPaymentGateway(paymentResponse.getPaymentGateway());
                } else {
                    if (!StringUtils.isEmpty(paymentResponse.getEci())) {
                        paymentAlertBean.setPaymentGateway(paymentResponse.getEci());
                    } else {
                        paymentAlertBean.setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
                    }
                }
            }
            
            CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(paymentTransaction.getOrderId());
            
            convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, paymentAlertBean);
            
            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
            paymentAlertBean.setProduct(primaryProduct.name());
            paymentAlertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()).longValue());
            
            com.freecharge.payment.dos.entity.PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlanFromMtxnId(paymentTransaction.getMerchantTxnId(), paymentTransaction.getOrderId());
            if(fcPaymentPlan==null) {
                fcPaymentPlan = paymentPlanService.getPaymentPlan(paymentTransaction.getOrderId());
            }
            paymentAlertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
            int paymentType= paymentTransaction.getPaymentType();
            PaymentPlan paymentPlan = new PaymentPlan();
            if (fcPaymentPlan != null) {
                Double creditsAmount = fcPaymentPlan.getWalletAmount().getAmount().doubleValue();
                Double pgAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                if ((paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE) || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET) == paymentType) && creditsAmount == 0.0) {
                    creditsAmount = fcPaymentPlan.getPgAmount().getAmount().doubleValue();
                    pgAmount = 0.0;
                }
                
                paymentPlan.setPg(pgAmount);
                
                if (paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)) {
                    paymentPlan.setCredits(creditsAmount);
                } else {
                    paymentPlan.setOcw(creditsAmount);
                }
                paymentAlertBean.setAmount(fcPaymentPlan.getTotalAmount().getAmount().doubleValue());
                paymentAlertBean.setPaymentPlan(paymentPlan);
            }
            
            paymentAlertBean.setPaymentPlan(paymentPlan);
            
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
            String email = userServiceProxy.getEmailByUserId(userTransactionHistory.getFkUserId());
            String oneCheckIdentity = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email);
            String operator = userTransactionHistory.getServiceProvider();
            paymentAlertBean.setTransactionId(transactionId);
            paymentAlertBean.setImsId(oneCheckIdentity);
            //Set GST
            gstService.populateGST(paymentAlertBean, primaryProduct,operator);
        } catch (Exception e) {
            logger.error("paymentSNSService publish failure for orderId : " + paymentTransaction.getOrderId());
        } finally {
        	paymentAlertBean.setReconMetaData(paymentResponse.getReconMetaData());
        	try {
        		logger.info("PG Alert after response:"+PaymentAlertBean.getJsonString(paymentAlertBean));
        		paymentSNSService.publish(paymentTransaction.getOrderId(), "{\"message\" : " + PaymentAlertBean.getJsonString(paymentAlertBean) + "}");
        	} catch (Exception e) {
				logger.error("Exception while publishing notification for alert bean:" + paymentTransaction.getOrderId(), e);
			}
        }
    }
    
    private void setNonPCICardDetailsToBean(PaymentAlertBean paymentAlertBean, PaymentResponse paymentResponse, PaymentResponseBusinessDO paymentResponseBusinessDO) {
    	try {
    		String pg = null;
        	if(!paymentResponse.getPaymentGateway().equals(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE)) {
        		pg = paymentResponse.getPaymentGateway();
        	} else if (!StringUtils.isEmpty(paymentResponse.getEci())) {
        		pg = paymentResponse.getEci();
        	} else {
        		pg = PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE;
        	}
        	switch(pg) {
        	case "payu":
        		paymentAlertBean.setCardIssuingBank(paymentResponseBusinessDO.getRequestMap().get(RequestParameters.CARD_ISSUER_BANK));
        		paymentAlertBean.setCardType(paymentResponseBusinessDO.getRequestMap().get(RequestParameters.CARD_NATURE));
        		break;
        		
        	default:
        		paymentAlertBean.setCardIssuingBank(paymentResponseBusinessDO.getRequestMap().get(RequestParameters.CARD_ISSUER));
        		paymentAlertBean.setCardType(paymentResponseBusinessDO.getRequestMap().get(RequestParameters.CARD_TYPE));
        		break;
        	}
		} catch (Exception e) {
			logger.info("Exception while setting card details : " +e.getMessage());
		}
    	
    	
    }
    
    public boolean acknoledgePaymentTransactionUpdate(PaymentTransaction paymentTransaction, String orderId, String transactionId, Integer paymentTxnId, PaymentResponse paymentResponse, PaymentResponseBusinessDO paymentResponseBusinessDO, boolean isPaymentStatusForAdminPanel){
        //checking if the DB update has gone through by one of the responses - server to server or from UI
        /**
         * As part of the logic to update the payment txn table only once- we
         * will check the paymentTxnUpdateCount if the paymentTxnUpdateCount
         * remains 0 even after the update call - that means this is the second
         * thread calling it and first thread has already done its job
         */

        boolean paymentAlreadyAck = false;
        if (!paymentTransaction.getIsSuccessful()) {
            paymentTransaction.setTransactionId(transactionId);
            paymentTransaction.setPaymentTxnId(paymentTxnId);
            paymentTransaction.setPgTransactionTime(Calendar.getInstance().getTime());
            paymentTransaction.setIsSuccessful(paymentResponse.getIsSuccessful());
            paymentTransaction.setPaymentResponseId(paymentResponse.getPaymentRsId());
            paymentTransaction.setPgTransactionTime(paymentResponse.getPgTransactionTime());
            paymentTransaction.setRecievedToPg(Calendar.getInstance().getTime());
            // Fetches the 'is_successful' column from database. Possible values - 0,1,null
            Boolean paymentStatusBeforeUpdate = paymentTransactionService.getPaymentStatus(paymentTxnId);
            // Do update for payment transaction table only in case where there is a change in 'is_successful' column
           
            String emailId = userServiceProxy.getLoggedInUserEmailId();
            
            if(StringUtils.isEmpty(emailId)){
                emailId = userServiceProxy.getEmailIdFromOrderId(orderId);
            }
            
            if (!isPaymentStatusForAdminPanel && paymentStatusBeforeUpdate != paymentTransaction.getIsSuccessful()) {
                if (!updatePaymentTransaction(paymentTransaction)) {
                    paymentAlreadyAck = true;
                } else {
					
                	notifyPayment(paymentTransaction, paymentResponse, paymentResponseBusinessDO,
							isPaymentStatusForAdminPanel, transactionId);
                    if (paymentTransaction.getIsSuccessful()) {
						logger.info("About to save paymentOption for orderId:"+orderId);
                    	setLastPaymentOption(paymentTransaction, orderId, emailId);
                    	if(paymentStatusBeforeUpdate == null && paymentResponseBusinessDO.isStatusCheck()){
                            paymentRefundService.sendPaymentRefundNotifications(PaymentStatusTransition.NULL_TO_SUCCESS, paymentTransaction.getMerchantTxnId());
                        }
                        //saving payment success timestamp to intxndata
                        Map<String, Object> transactionInfo = new HashMap<>();
                        String lookupId = orderIdReadDAO.getLookupIdForOrderId(paymentTransaction.getOrderId());
                        transactionInfo.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, new Date());
                        try {
                            logger.info("About to Save in txn detail.");
                            campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
                        } catch(Exception e) {
                            logger.error("Exception in saving intxndata for uniqueId " + lookupId + ", orderId "
                                    + orderId + " and data " + transactionInfo, e);
                        }
                        if(!paymentResponseBusinessDO.isStatusCheck()){
                            metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(orderId) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Success");
                            metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Success");
                        }
                    } else {
                        if(paymentStatusBeforeUpdate == null && paymentResponseBusinessDO.isStatusCheck()){
                            paymentRefundService.sendPaymentRefundNotifications(PaymentStatusTransition.NULL_TO_FAILURE, paymentTransaction.getMerchantTxnId());
                        }
                        // For Klickpay gateway, E115 will be returned in case of duplicate requests. We should not mark payment as failure in such cases
                        if(!paymentResponseBusinessDO.isStatusCheck() && (StringUtils.isEmpty(paymentResponse.getStatus()) || !paymentResponse.getStatus().equals("E115"))){
                            metricsClient.recordEvent("Payment", "Channel." + getChannelFromOrderId(orderId) + "." + paymentTransaction.getPaymentType() + "." + paymentTransaction.getPaymentOption(), "Failure");
                            metricsClient.recordEvent("Payment", "PaymentGateway." + paymentTransaction.getPaymentGateway(), "Failure");
                        }
                    }
                }
                logger.debug("Payment txn update happened " + paymentAlreadyAck + " for order " + orderId);
            }
        } else {
            paymentAlreadyAck = true;
            logger.info("Payment already acknoledged is set to true for order " + orderId);
        }
        storeInPaymentHistoryRepository(paymentTransaction, orderId, paymentResponseBusinessDO);
        return paymentAlreadyAck;
    }

    
	private void setLastPaymentOption(PaymentTransaction paymentTransaction, String orderId, String emailId) {
		logger.info("Payment Type for OrderId:"+orderId+" is "+paymentTransaction.getPaymentType());		
		if (paymentTransaction.getPaymentType() != null) {
			if (paymentTransaction.getPaymentType() == Integer.valueOf(PaymentConstants.PAYMENT_TYPE_CREDITCARD)
					|| paymentTransaction.getPaymentType() == Integer
							.valueOf(PaymentConstants.PAYMENT_TYPE_DEBITCARD)) {
				String cardReference = paymentOptionCache.getCardReferenceByOrderId(orderId);
				if (!FCUtil.isEmpty(cardReference)) {
					paymentOptionService.insertToDynamoDB(emailId,
							paymentModeMap.get(String.valueOf(paymentTransaction.getPaymentType())), cardReference);
				} else {
					paymentOptionService.insertToDynamoDB(emailId,
							paymentModeMap.get(String.valueOf(paymentTransaction.getPaymentType())), EMPTY);
				}

			} else
				if (paymentTransaction.getPaymentType() == Integer.valueOf(PaymentConstants.PAYMENT_TYPE_NETBANKING)) {
				paymentOptionService.insertToDynamoDB(emailId,
						paymentModeMap.get(String.valueOf(paymentTransaction.getPaymentType())),
						paymentTransaction.getPaymentOption());
			}
		}
	}

	private void storeInPaymentHistoryRepository(PaymentTransaction paymentTransaction, String orderId,
            PaymentResponseBusinessDO paymentResponseBusinessDO) {
        try {
            logger.info("About to store in payment history.");
            List<OrderId> orderIds = orderIdWriteDAO.getByOrderId(orderId);
            String encryptedCardNo = paymentsCache.get(paymentResponseBusinessDO.getmTxnId() + CARD_DATA_CACHE_KEY);
            String cardHash = !StringUtils.isEmpty(encryptedCardNo)
                    ? BilldeskCardHash.generateHash(EPinEncrypt.getDecryptedPin(encryptedCardNo)) : null;
            int productMasterId = orderIds.get(0).getFkProductMasterId();
            Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(paymentTransaction.getOrderId());
            PaymentHistoryEntry historyEntry = new PaymentHistoryEntry();
            historyEntry.setCardHash(cardHash);
            historyEntry.setUserId(userId);
            historyEntry.setPaymentSuccessFul(paymentTransaction.getIsSuccessful());
            historyEntry.setAmount(paymentTransaction.getAmount());
            historyEntry.setProductId(productMasterId);
            paymentHistoryRepository.insertAmount(historyEntry);
        } catch (Exception ex) {
            logger.error("Error while storing in payment history repository for orderId :" + orderId, ex);
        }
    }
    
    public String getChannelFromOrderId(final String orderId) {
        try {
            if(orderId!=null && orderId.length() >= 4) {
                String fourthChar = String.valueOf(orderId.charAt(3));
                return fourthChar;
            }
        } catch (Exception e) {
            logger.error("Exception on getting channel from order id ", e);
        }
        return null;
    }
    
    private PaymentResponse dumpResponsetAudit(PaymentResponse paymentResponse) {
        try {
            paymentResponse = paymentTransactionService.create(paymentResponse);
        } catch (Exception exp) {
            logger.error("payment response not inserted in db " + paymentResponse.getOrderId());
        }
        return paymentResponse;
    }
    
    public UPIAccountDetails getFCUpiPaymentObject(String upiAccountDetails)  {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<UPIAccountDetails>(){
            }.getType();
            return gson.fromJson(upiAccountDetails, type);
        } catch (Exception e) {
            logger.error("UPIAccount object parsing Exception for info:"+upiAccountDetails);
            throw e;
        }
    }

    @Override
    @Transactional
    public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws Exception {
        walletGatewayHandler.refundToWallet(paymentRefundBusinessDO,refundType);
    }
    
    /**
	 * Method to store card hash in database for successful payments. The card
	 * details are fetched from cache. The card details are inserted into cache
	 * during payment request with an expiry of 1 day
	 * 
	 * @param paymentResponseBusinessDO
	 */
	@SuppressWarnings("unchecked")
	protected void storeCardHashForSuccessfulPayment(
			PaymentResponseBusinessDO paymentResponseBusinessDO) {
		if (paymentResponseBusinessDO.getPaymentType() == null || (!(paymentResponseBusinessDO.getPaymentType().toString().equals(PaymentConstants.PAYMENT_TYPE_CREDITCARD) 
				|| paymentResponseBusinessDO.getPaymentType().toString().equals(PaymentConstants.PAYMENT_TYPE_DEBITCARD)))) {
			return;
		}
		Map<String, String> resultDataMap = (Map<String, String>) paymentResponseBusinessDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
		if (resultDataMap != null) {
			String isPaymentSuccessful = resultDataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL);
			if (StringUtils.isNotEmpty(isPaymentSuccessful)
					&& isPaymentSuccessful.equalsIgnoreCase("true")) {
				String encryptedCardNo = paymentsCache.get(paymentResponseBusinessDO.getmTxnId() + CARD_DATA_CACHE_KEY);
				if (StringUtils.isNotEmpty(encryptedCardNo)) {
					try {
						storeCardRelatedDataForOrder(paymentResponseBusinessDO.getmTxnId(), EPinEncrypt.getDecryptedPin(encryptedCardNo));
					} catch (Exception e) {
						logger.error("Unabled to store card hash data for mtxn : " + paymentResponseBusinessDO.getmTxnId(), e);
						metricsClient.recordEvent("Payment", "CardData.DB.Add", "error");
					}
				}
			}
		}
	}
	
    public void publishRefundAttempt(PaymentRefundBusinessDO paymentRefundBusinessDO, String pgUsed) {
        try {
            RefundAttemptBean alertBean = new RefundAttemptBean();
            UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                    .findUserTransactionHistoryByOrderId(paymentRefundBusinessDO.getOrderId());

            logger.info("PG received from PaymentRefundBusinessDO for orderid:" + paymentRefundBusinessDO.getOrderId()
                    + "is " + pgUsed);
           
            alertBean.setUserId((long) userTransactionHistory.getFkUserId());
            alertBean.setOrderId(paymentRefundBusinessDO.getOrderId());
            alertBean.setAmount(paymentRefundBusinessDO.getAmount());
            alertBean.setMtxnId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
            alertBean.setPaymentGateway(pgUsed);
            alertBean.setCallerReference(
                    paymentRefundBusinessDO.getOrderId() + RechargeConstants.TRANSACTION_TYPE_DEBIT);
            logger.info("Publishing refund attempt SNS");
            refundAttemptAlertSNSService.publish(paymentRefundBusinessDO.getOrderId(),
                    "{\"message\" : " + RefundAttemptBean.getJsonString(alertBean) + "}");
        } catch (Exception e) {
            logger.error("Error while publishing the payment attempt method to SNS for order id "
                    + paymentRefundBusinessDO.getOrderId(), e);
        }
    }

    public void publishRefundAttemptStatus(PaymentRefundBusinessDO paymentRefundBusinessDO, String pgUsed) {
        try {
            RefundAttemptBean alertBean = new RefundAttemptBean();
            UserTransactionHistory userTransactionHistory = userTransactionHistoryDAO
                    .findUserTransactionHistoryByOrderId(paymentRefundBusinessDO.getOrderId());
            alertBean.setUserId((long) userTransactionHistory.getFkUserId());
            alertBean.setOrderId(paymentRefundBusinessDO.getOrderId());
            alertBean.setAmount(paymentRefundBusinessDO.getAmount());
            alertBean.setMtxnId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getMerchantTxnId());
            alertBean.setPaymentGateway(pgUsed);
            alertBean.setCallerReference(
                    paymentRefundBusinessDO.getOrderId() + RechargeConstants.TRANSACTION_TYPE_DEBIT);
            alertBean.setStatus(paymentRefundBusinessDO.getStatus());
            logger.info("Publishing refund attempt status SNS");
            refundStatusAlertSNSService.publish(paymentRefundBusinessDO.getOrderId(),
                    "{\"message\" : " + RefundAttemptBean.getJsonString(alertBean) + "}");
        } catch (Exception e) {
            logger.error("Error while publishing the payment attempt method to SNS for order id "
                    + paymentRefundBusinessDO.getOrderId(), e);
        }
    }

    
	/*
     * In case of processing CC/DC through billdesk, we need to store the card
     * bin and card hash for the transaction as it might be used for freefund
     * redemption later. This is required as billdesk does not have any API like
     * PayU which return card bin and card hash if we provide them the merchant
     * transaction id
     */
    private void storeCardRelatedDataForOrder(String mtxnId, String cardNumber) {
        String cardHash = BilldeskCardHash.generateHash(cardNumber);
        String cardBin = PaymentUtil.getCardNumberInBinFormat(cardNumber);
        PaymentCard paymentCard = new PaymentCard(cardHash, cardBin);
        if (null != cardHash) {
            try {
                PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionByMerchantOrderId(mtxnId).get(0);
                logger.info("Saving Card Hash value for order id: " + paymentTransaction.getOrderId());
                List<OrderId> orderIds = orderIdWriteDAO.getByOrderId(paymentTransaction.getOrderId());
                String lookupId = orderIds.get(0).getLookupId();
                HashMap<String, Object> paymentInfo = new HashMap<>();
                paymentInfo.put(InTxnDataConstants.CARD_HASH, cardHash);
                paymentInfo.put(InTxnDataConstants.CARD_BIN, cardNumber.substring(0, 6));
                campaignServiceClient.saveInTxnData1(lookupId, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, paymentInfo);
                logger.info("Saved Card Hash value for order id: " + paymentTransaction.getOrderId());
            } catch (Exception e) {
                logger.info("Error saving data in intxn data, card condition related campaigns might not work", e);
            }
        }
        if (!cardBinService.saveCardDetailsForOrder(mtxnId, paymentCard)) {
            throw new IllegalStateException("Error storing card related data for order : " + mtxnId);
        }
    }
    
    protected abstract void validatePaymentResponse(Map<String, String> payResponseMap);
    protected abstract PaymentResponse createPaymentResponse(Map<String, String> payResponseMap);
    protected abstract String getPaymentPortalMerchantId();
    protected abstract String getMerchantTransactionId(Map<String, String> payResponseMap);
    
    public static final class RequestParameters {
        public static final String MERCHANT_ID = "key";
        public static final String PAYMENT_REFERENCE_ID = "txnid";
        public static final String AMOUNT = "amount";
        public static final String DISCOUNT = "discount";
        public static final String OFFER = "offer";
        public static final String OFFER_AVAILED = "offer_availed";
        public static final String PRODUCT_DESCRIPTION = "productinfo";
        public static final String FIRST_NAME = "firstname";
        public static final String CUSTOMER_EMAIL = "email";
        public static final String CUSTOMER_PHONE = "phone";
        public static final String SUCCESS_URL = "surl";
        public static final String FAILURE_URL = "furl";
        public static final String CHECKSUM = "hash";
        public static final String API_VERSION = "version";
        public static final String TRANSACTION_STATUS = "status";
        public static final String CHECKSUM_RESULT = "checksumResult";
        public static final String PAYU_TRANSACTION_ID = "mihpayid";
        public static final String PG_USED= "PG_TYPE";
        public static final String BANK_REFERENCE = "bank_ref_num";
        public static final String BANK_CODE = "bankcode";
        public static final String PG_TXN_DATE = "field3";
        public static final String PAY_RESPONSE_CODE = "error";
        public static final String CARDHOLDER_NAME = "CCNAME";
        public static final String PAYU_CARD_NUMBER = "cardnum";
        public static final String PAYU_S2S_CARD_NUMBER = "card_no";
        public static final String NAME_ON_CARD = "name_on_card";
        public static final String CARD_NUMBER = "CCNUM";
        public static final String CVV = "CCVV";
        public static final String EXPIRY_MONTH = "CCEXPMON";
        public static final String EXPIRY_YEAR = "CCEXPYR";
        public static final String PAYMENT_GATEWAY = "pg";
        public static final String CC_STORAGE_KEY = "user_credentials";
        public static final String DROP_CATEGORY = "drop_category";
        public static final String CARD_NATURE = "mode";
        public static final String CARD_ISSUER_BANK = "issuing_bank";
        public static final String CARD_TYPE = "card_type";
        public static final String CARD_ISSUER= "card_issuer";
        
        public static final String OFFER_KEY = "offer_key";
        public static final String OFFER_TYPE = "offer_type";
        public static final String NA = "NA";
        
        public static final String BIN_OFFER_ID = "binOfferId";
        public static final String UDF10 = "udf10";
        public static final String UDF1 = "udf1";
        public static final String PYOP = "pyop";
        
        public static final String PAYMENT_STATUS_CODE = "unmappedstatus";
    }
    
    public static final class WebServiceParameters {
        public static final String METHOD_NAME = "command";
        public static final String MERCHANT_TXN_ID_PARAMETER = "var1";
        public static final String MERCHANT_KEY_PARAMETER = "key";
        public static final String HASH_PARAMETER = "hash";
        public static final String WS_CALL_STATUS = "status";
        public static final String REFUND_TXN_ID = "mihpayid";
        public static final String REFUND_RESPONSE_CODE = "error_code";
        public static final String REFUND_REQUEST_ID = "request_id";
        public static final String REFUND_RESPONSE_MESSAGE = "msg";
        public static final String PAYMENT_RESPONSE_PARAMETER = "transaction_details";
        
        public static final String REFUND_PG_TRANSACTION_ID = "var1";
        public static final String REFUND_REFERENCE_ID = "var2";
        public static final String REFUND_AMOUNT = "var3";
    }
    
    public static final class Constants {
        public static final String PAYMENT_RESPONSE_URL = "/payment/handlepayuresposne";
        public static final String MOBILE_PAYMENT_RESPONSE_URL = "/m/payment/response/handlepayuresponse";
        public static final String HASH_ALGORITHM = "SHA-512";
        public static final String SUCCESS_STATUS = "success";
        public static final String PENDING_STATUS = "pending";
        public static final String FAILURE_STATUS = "failure";
        public static final String PAYU_PAYMENT_GATEWAY = "payu";
        public static final String PAYMENT_STATUS_METHOD = "verify_payment";
        public static final String PAYMENT_REFUND_METHOD = "cancel_refund_transaction";
        public static final String HASH_STRING_DELIMITER = "|";
        public static final Integer WS_CALL_SUCCESS = 1;
        public static final String CREDIT_CARD_BANK_CODE = "CC";
        public static final String CREDIT_CARD_PG = "CC";
        public static final String AMEX_CARD_PG = "AMEX";
        public static final String DINERS_CARD_PG = "DINR";
        public static final String NETBANKING_PG = "NB";
        public static final String ATM_PG = "ATM";
        public static final String CASHCARD_PG = "CCD";
        public static final String DEBIT_CARD_PG = "DC";
        public static final String ONE_CHECK_WALLET_PG = "OCW";
        public static final String DEBIT_CARD_VISA_BANK_CODE = "VISA";
        public static final String DEBIT_CARD_MASTER_BANK_CODE = "MAST";
        public static final String DEBIT_CARD_MAESTRO_BANK_CODE = "MAES";
        public static final String DEBIT_CARD_CITY_MAESTRO_BANK_CODE = "CITD";
        public static final String DEBIT_CARD_SBI_MAESTRO_BANK_CODE = "SMAE";
        
        public static final String REFUND_SUFFIX = "_R";
        
        public static final String USERID_KEY = "txnuserid";
        
        public static final String EXCLUSION_LIST = "COD,EMI";
        
        public static final String UNFILLED_CARD_EXPIRY = "2012";
        
        public static final String API_VERSION_V2 = "2";
        
        public static final String OFFER_TYPE_INSTANT = "instant";
        public static final String OFFER_TYPE_CASHBACK = "cashback";
    }
}
