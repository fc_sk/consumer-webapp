package com.freecharge.payment.services;

public class PaymentStatusDetails {
    public static enum PaymentStatus {
        NotAttempted, Unknown, 
        Success, Failure
    }
    
    private String orderId;
    private PaymentStatus paymentStatus;
    
    
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }
    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
