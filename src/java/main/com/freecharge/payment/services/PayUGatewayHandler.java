package com.freecharge.payment.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.BinOffer.OfferType;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.exception.PaymentTransactionNotFoundException;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.sns.PaymentAttemptSNSService;
import com.freecharge.web.util.WebConstants;
import com.google.common.collect.ImmutableMap;

import flexjson.JSONDeserializer;


@Component("payUGatewayHandler")
public final class PayUGatewayHandler extends PaymentGatewayHandler implements CardGatewayHandler {
    private final Logger logger = Logger.getLogger(PayUGatewayHandler.class);
    private final List<String> blackListedKeys = new ArrayList<String>(Arrays.asList(
            "ccNum", "ccExpMon", "ccExpYear", "ccCvv",
            "dbNum", "dbExpMon", "dbExpYear", "dbCvv",
            "CCNUM", "CCVV", "CCEXPMON", "CCEXPYR",
            PaymentConstants.CARD_NUM_KEY, PaymentConstants.CARD_EXPYEAR_KEY,
            PaymentConstants.CARD_EXPMON_KEY, PaymentConstants.CARD_HOLDER_NAME_KEY,
            PaymentConstants.CARD_CVV_KEY
            ));
    
    JSONDeserializer<Map<String, Object>> deserializer = new JSONDeserializer<Map<String,Object>>();
        
    @Autowired
    private BinOfferService binOfferService;
    
    @Autowired
    private FreefundService freefundService;
        
    @Autowired
    private PaymentPlanService paymentPlanService;
    
    @Autowired
    private BanksStatusCache banksStatusCache;
    
    @Autowired
	 private PaymentAttemptSNSService paymentAttemptSNSService;
    
    @Autowired
    @Qualifier("payUConnectionManager")
    private MultiThreadedHttpConnectionManager payUConnectionManager;

    @Autowired
    CardBinService cardBinService;
    
   
    
	Map<String, String> foo = ImmutableMap.of("A", "B");
	
	   private Map<String, String> payuBankCodesFromUi = ImmutableMap.<String, String>builder()
	            .put("AXIS", "AXIB")
	            .put("BOI", "BOIB")
	            .put("CENB", "CBIB")
	            .put("CORB", "CRPB")
	            .put("DECB", "DCBB")
	            .put("FEDB", "FEDB")
	            .put("ICIC", "ICIB")
	            .put("HDFC", "HDFB")
	            .put("INDB", "INDB")
	            .put("IOB", "INOB")
	            .put("INIB", "INIB")
	            .put("JNKB", "JAKB")
	            .put("KARB", "KRVB")
	            .put("KRTB", "KRKB")
	            .put("SINB", "SOIB")
	            .put("UBI", "UBIB")
	            .put("YEBK", "YESB")
	            .put("SBIN", "SBIB")
	            .put("SMYB", "SBMB")
	            .put("STVB", "SBTB")
	            .put("SHYB", "SBHB")
	            .put("SBJB", "SBBJB")
	            .put("VIJB", "VJYB")
	            .put("UNIB", "UNIB")
	            .put("MAHB", "BOMB")
	            .put("IDBI", "IDBB")
	            .put("CITU","CUBB")
	            .put("CANB","CABB")
	            .put("SPTB","SBPB")
	            .put("CITA","CITNB")
	            .put("DEUB","DSHB")
	            .put("KOBK","162B")
	            .put("DHLB","DHANNB")
	            .put("INVB","INGB")
	            .build();
    
    /* Banks supported by Payu
     * AXIB AXIS Bank NetBanking
     * BOIB Bank of India
     * BOMB Bank of Maharashtra
     * CBIB Central Bank of India
     * CRPB Corporation Bank
     * DCBB Development Credit Bank
     * FEDB Federal Bank
     * ICIB ICICI NetBanking
     * HDFB HDFC Bank
     * INDB Indian Bank
     * INOB Indian Overseas Bank
     * INIB IndusInd Bank
     * IDBB Industrial Development Bank of India
     * JAKB Jammu and Kashmir Bank
     * KRVB Karur Vysya Bank
     * KRKB Karnataka Bank Ltd
     * SOIB South Indian Bank
     * UBIB Union Bank of India
     * YESB Yes Bank
     * SBIB State Bank of India
     * SBMB State Bank of Mysore
     * SBTB State Bank of Travancore
     * SBHB State Bank of Hyderabad
     * SBBJB State Bank of Jaipur and Bikaner
     * DCBB Development Credit bank 
     * UNIB United Bank of India
     * VJYB Vijaya Bank
     */
    
    public static final class RequestParameters {
        public static final String MERCHANT_ID = "key";
        public static final String PAYMENT_REFERENCE_ID = "txnid";
        public static final String AMOUNT = "amount";
        public static final String DISCOUNT = "discount";
        public static final String OFFER = "offer";
        public static final String OFFER_AVAILED = "offer_availed";
        public static final String PRODUCT_DESCRIPTION = "productinfo";
        public static final String FIRST_NAME = "firstname";
        public static final String CUSTOMER_EMAIL = "email";
        public static final String CUSTOMER_PHONE = "phone";
        public static final String SUCCESS_URL = "surl";
        public static final String FAILURE_URL = "furl";
        public static final String CHECKSUM = "hash";
        public static final String API_VERSION = "version";
        public static final String TRANSACTION_STATUS = "status";
        public static final String CHECKSUM_RESULT = "checksumResult";
        public static final String PAYU_TRANSACTION_ID = "mihpayid";
        public static final String PG_USED= "PG_TYPE";
        public static final String BANK_REFERENCE = "bank_ref_num";
        public static final String BANK_CODE = "bankcode";
        public static final String PG_TXN_DATE = "field3";
        public static final String PAY_RESPONSE_CODE = "error";
        public static final String CARDHOLDER_NAME = "CCNAME";
        public static final String PAYU_CARD_NUMBER = "cardnum";
        public static final String NAME_ON_CARD = "name_on_card";
        public static final String CARD_NUMBER = "CCNUM";
        public static final String CVV = "CCVV";
        public static final String EXPIRY_MONTH = "CCEXPMON";
        public static final String EXPIRY_YEAR = "CCEXPYR";
        public static final String PAYMENT_GATEWAY = "pg";
        public static final String CC_STORAGE_KEY = "user_credentials";
        public static final String DROP_CATEGORY = "drop_category";
        public static final String CARD_NATURE = "mode";
        public static final String CARD_ISSUER_BANK = "issuing_bank";
        public static final String CARD_TYPE = "card_type";
        
        public static final String OFFER_KEY = "offer_key";
        public static final String OFFER_TYPE = "offer_type";
        public static final String NA = "NA";
        
        public static final String BIN_OFFER_ID = "binOfferId";
        public static final String UDF10 = "udf10";
        public static final String UDF1 = "udf1";
        public static final String PYOP = "pyop";
        
        public static final String PAYMENT_STATUS_CODE = "unmappedstatus";
        
    }
    
    public static final class WebServiceParameters {
        public static final String METHOD_NAME = "command";
        public static final String MERCHANT_TXN_ID_PARAMETER = "var1";
        public static final String VAR1_DATA= "var1";
        public static final String MERCHANT_KEY_PARAMETER = "key";
        public static final String HASH_PARAMETER = "hash";
        public static final String WS_CALL_STATUS = "status";
        public static final String WS_CALL_CARD_HASH = "cardhash";
        public static final String WS_CALL_CARD_BIN = "card_no";
        public static final String REFUND_TXN_ID = "mihpayid";
        public static final String REFUND_RESPONSE_CODE = "error_code";
        public static final String REFUND_REQUEST_ID = "request_id";
        public static final String REFUND_RESPONSE_MESSAGE = "msg";
        public static final String PAYMENT_RESPONSE_PARAMETER = "transaction_details";
        
        public static final String REFUND_PG_TRANSACTION_ID = "var1";
        public static final String REFUND_REFERENCE_ID = "var2";
        public static final String REFUND_AMOUNT = "var3";
    }
    
    public static final class Constants {
        public static final String PAYMENT_RESPONSE_URL = "/payment/handlepayuresposne";
        public static final String MOBILE_PAYMENT_RESPONSE_URL = "/m/payment/response/handlepayuresponse";
        public static final String HASH_ALGORITHM = "SHA-512";
        public static final String SUCCESS_STATUS = "success";
        public static final String PENDING_STATUS = "pending";
        public static final String FAILURE_STATUS = "failure";
        public static final String PAYU_PAYMENT_GATEWAY = "payu";
		public static final String PAYMENT_STATUS_METHOD = "verify_payment";
		public static final String ISSUING_BANK_STATUS_METHOD = "getIssuingBankStatus";
		public static final String NET_BANKING_STATUS_METHOD = "getNetbankingStatus";
		public static final String PAYMENT_REFUND_METHOD = "cancel_refund_transaction";
		public static final String HASH_STRING_DELIMITER = "|";
        public static final Integer WS_CALL_SUCCESS = 1;
        public static final String CREDIT_CARD_BANK_CODE = "CC";
        public static final String CREDIT_CARD_PG = "CC";
        public static final String NETBANKING_PG="NB";
        
        public static final String DEBIT_CARD_PG = "DC";
        public static final String DEBIT_CARD_GENERIC_BANK_CODE = "Maestro";
        public static final String DEBIT_CARD_VISA_BANK_CODE = "VISA";
        public static final String DEBIT_CARD_MASTER_BANK_CODE = "MAST";
        public static final String DEBIT_CARD_MAESTRO_BANK_CODE = "MAES";
        public static final String DEBIT_CARD_CITY_MAESTRO_BANK_CODE = "CITD";
        public static final String DEBIT_CARD_SBI_MAESTRO_BANK_CODE = "SMAE";
        public static final String DEBIT_CARD_RUPAY_BANK_CODE = "RUPAY";
        
        public static final String REFUND_SUFFIX = "_R";
        
        public static final String USERID_KEY = "txnuserid";
        
        public static final String EXCLUSION_LIST = "COD,EMI";
        
        public static final String UNFILLED_CARD_EXPIRY = "2012";
        
        public static final String API_VERSION_V2 = "2";
        
        public static final String OFFER_TYPE_INSTANT = "instant";
        public static final String OFFER_TYPE_CASHBACK = "cashback";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentGatewayException, Exception {
        
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String merchantId = fcProperties.getProperty(PaymentConstants.PAYU_MERCHANT_ID);
        
        Map<String, Object> sessionData = fs.getSessionData();
        
        String userId = ((Integer) sessionData.get(WebConstants.SESSION_USER_USERID)).toString();
        
        String orderId = paymentRequestBusinessDO.getOrderId();
        Integer paymentType = paymentRequestBusinessDO.getPaymentType();
        String merchantTransactionId = paymentRequestBusinessDO.getMtxnId();
        paymentRequestBusinessDO.setMtxnId(merchantTransactionId);
        logger.info("About to process payments for PayU for order ID : [" + orderId + "], MtxnId: [" + merchantTransactionId + "]");
        
        String responseUrl = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME) + Constants.PAYMENT_RESPONSE_URL;
        if (MobileUtil.isMobilePaymentUsingOldPayments(paymentRequestBusinessDO)){
            responseUrl =  fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME) + Constants.MOBILE_PAYMENT_RESPONSE_URL;
        }
        Map<String, Object> paymentRequestMap = new HashMap<String, Object>();
        paymentRequestMap.put(RequestParameters.MERCHANT_ID, merchantId);
        paymentRequestMap.put(RequestParameters.PAYMENT_REFERENCE_ID, merchantTransactionId);
        Double chargeAmount = Double.parseDouble(paymentRequestBusinessDO.getAmount());
        paymentRequestMap.put(RequestParameters.AMOUNT, Double.toString(chargeAmount));
        paymentRequestMap.put(RequestParameters.PRODUCT_DESCRIPTION, paymentRequestBusinessDO.getProductId());
        // No special characters allowed in the first name so getting rid of space.
        paymentRequestMap.put(RequestParameters.FIRST_NAME, "NA");
        paymentRequestMap.put(RequestParameters.CUSTOMER_EMAIL, "NA");
        paymentRequestMap.put(RequestParameters.CUSTOMER_PHONE, "9999999999");
        paymentRequestMap.put(RequestParameters.SUCCESS_URL, responseUrl);
        paymentRequestMap.put(RequestParameters.FAILURE_URL, responseUrl);
        paymentRequestMap.put(RequestParameters.DROP_CATEGORY, Constants.EXCLUSION_LIST);
        paymentRequestMap.put(RequestParameters.API_VERSION, Constants.API_VERSION_V2);
        
        if (userId != null && !userId.trim().isEmpty()) {
            String cardStorageKey = merchantId + ":" + userId;
            paymentRequestMap.put(RequestParameters.CC_STORAGE_KEY, cardStorageKey);
        }

        /*
         * Below set of fields are needed for seamless integration. 
         */
        //Map<String, String> requestMap = paymentRequestBusinessDO.getRequestMap();

        String cardHolderName=null;
        String cardNumber=null;
        String cardExpMon=null;
        String cardExpYear=null;
        String cardCvv=null;
        String paymentGateWayCode=null;
        String bankCode=null;
        
        final String paymentOption = paymentRequestBusinessDO.getPaymentOption();
        
        if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_NETBANKING) == paymentType) {
            paymentGateWayCode = Constants.NETBANKING_PG;
            bankCode = payuBankCodesFromUi.get(paymentOption);
        } else if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentType || Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentType) {
            cardHolderName = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardHolderName();
            cardNumber = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardNumber();
            cardExpMon = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpMonth();
            cardExpYear = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardExpYear();
            cardCvv = paymentRequestBusinessDO.getPaymentRequestCardDetails().getCardCvv();
            
            if (Integer.parseInt(PaymentConstants.PAYMENT_TYPE_CREDITCARD) == paymentType) {
                paymentGateWayCode =  Constants.CREDIT_CARD_PG;
                bankCode = Constants.CREDIT_CARD_BANK_CODE;
            } else if(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_DEBITCARD) == paymentType) {
                paymentGateWayCode = Constants.DEBIT_CARD_PG;
                bankCode = Constants.DEBIT_CARD_MAESTRO_BANK_CODE;
                
                if (PaymentConstants.MAESTRO_SBI.equals(paymentOption) || PaymentConstants.MAESTRO_CITI.equals(paymentOption)) {
                    if (StringUtils.isBlank(cardExpYear) || StringUtils.isBlank(cardExpMon)) {
                        cardExpYear = PaymentConstants.MAESTRO_DEFAULT_EXPIRY_YEAR;
                        cardExpMon = PaymentConstants.MAESTRO_DEFAULT_EXPIRY_MONTH;
                    }
                }
            }
        }
        
        paymentRequestMap.put(RequestParameters.PAYMENT_GATEWAY, paymentGateWayCode);
        paymentRequestMap.put(RequestParameters.BANK_CODE, bankCode); 
        
        if (StringUtils.isNotEmpty(cardHolderName)) {
            paymentRequestMap.put(RequestParameters.CARDHOLDER_NAME, cardHolderName);
        } else {
            paymentRequestMap.put(RequestParameters.CARDHOLDER_NAME, "UNKNOWN");
        }
        if (StringUtils.isNotEmpty(cardNumber)) {
            paymentRequestMap.put(RequestParameters.CARD_NUMBER, cardNumber);
        }
        if (StringUtils.isNotEmpty(cardExpMon)) {
            paymentRequestMap.put(RequestParameters.EXPIRY_MONTH, PaymentUtil.getExpMonthInReqFormat(cardExpMon));
        }
        if (StringUtils.isNotEmpty(cardExpYear)) {
            paymentRequestMap.put(RequestParameters.EXPIRY_YEAR, cardExpYear);
        }
        if (StringUtils.isNotEmpty(cardCvv)) {
            paymentRequestMap.put(RequestParameters.CVV, cardCvv);
        }
        CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
        Integer binOfferId = 0;
        if (cartBusinessDo != null) {
            try {
                binOfferId = binOfferService.getEligibleOffer(cardNumber, cartBusinessDo.getLookupID());
            } catch (Exception e) {
                logger.error("Exception applying bin offer for orderId : " + orderId, e);
            }
            if (binOfferId != null && binOfferId != 0 && cartService.isEligibleForOffers(cartBusinessDo)) {
                if (binOfferId != 0) {
                    setOfferDetails(paymentRequestMap, binOfferId, orderId, cartBusinessDo);
                }

            }
        } else {
            logger.warn("cartBusinessDo is null");
        }
        paymentRequestMap.put(RequestParameters.CHECKSUM, getRequestHashV2(paymentRequestMap));
        
        logger.info("Payment request map is : " + serializePayRequest(paymentRequestMap) );
        
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(RequestParameters.BIN_OFFER_ID, binOfferId+"");
        resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "true");
        resultMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_REQUEST_VIEW);
        resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, paymentRequestMap);
        resultMap.put(PaymentConstants.PAYMENT_GATEWAY_CODE_KEY, PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        resultMap.put(PaymentConstants.MERCHANT_ORDER_ID, merchantTransactionId);
        // MOCK PG
        if (fcProperties.shouldMockPG()) {
            resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_HOST_NAME) + "/dummypg/payu/pay");
        } else {
            resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, fcProperties.getProperty(PaymentConstants.PAYU_SUBMIT_URL));
        }
        
        paymentRequestBusinessDO.setResultMap(resultMap);
        
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        paymentRequest.setAffiliateCode(paymentRequestBusinessDO.getAffiliateId().toString());
        paymentRequest.setMtxnId(merchantTransactionId);
        paymentRequest.setOrderId(orderId);
        paymentRequest.setPaymentGateway(Constants.PAYU_PAYMENT_GATEWAY);
        paymentRequest.setPaymentMode("pur");
        paymentRequest.setPaymentType(paymentRequestBusinessDO.getPaymentType().toString());
        paymentRequest.setRequestToPp(serializePayRequest(paymentRequestBusinessDO.getRequestMap()));
        paymentRequest.setRequestToGateway(serializePayRequest(paymentRequestMap));

        paymentRequest = dumpRequestAudit(paymentRequest);

        PaymentTransaction pt = new PaymentTransaction();
        pt.setAmount(new Double(paymentRequestBusinessDO.getAmount()));
        pt.setMerchantTxnId(merchantTransactionId);
        pt.setOrderId(orderId);
        pt.setPaymentGateway(Constants.PAYU_PAYMENT_GATEWAY);
        pt.setPaymentMode("pur");
        pt.setPaymentOption(paymentOption);
        pt.setPaymentRequestId(paymentRequest.getPaymentRqId());
        pt.setPaymentType(paymentRequestBusinessDO.getPaymentType());
        pt.setSetToPg(Calendar.getInstance().getTime());

        pt = createPaymentTransaction(pt);
        
        paymentRequestBusinessDO.setResultMap(resultMap);

        fs.getSessionData().put(merchantTransactionId + "_order_id", paymentRequestBusinessDO.getOrderId());
        fs.getSessionData().put(merchantTransactionId + "_success_url", paymentRequestBusinessDO.getSuccessUrl());
        fs.getSessionData().put(merchantTransactionId + "_error_url", paymentRequestBusinessDO.getErrorUrl());
        fs.getSessionData().put(merchantTransactionId + "_txn_id", pt.getPaymentTxnId());
   
        logger.info("Done processing payments for PayU for order ID : " + orderId);
        return resultMap;
    }

    private void setOfferDetails(Map<String, Object> paymentRequestMap, int binOfferId, String orderId, CartBusinessDo cartBusinessDo) {
        
        boolean isValidAmount = false;
        boolean isCartHasFreefund = false;
        
        BinOffer binOffer = binOfferService.getBinOfferById(binOfferId);
        if(binOffer != null) {
            Double rechargeAmount = cartService.getRechargeAmount(cartBusinessDo);
            if(rechargeAmount >= binOffer.getMinRechargeAmount()) {
                isValidAmount = true;
            }
            isCartHasFreefund = freefundService.containsFreeFund(cartBusinessDo);

            if (isValidAmount && !isCartHasFreefund) {
                paymentRequestMap.put(RequestParameters.OFFER_KEY, binOffer.getOfferKey());
                paymentRequestMap.put(RequestParameters.UDF1, binOffer.getId());
                paymentRequestMap.put(RequestParameters.OFFER_TYPE, "");
                paymentRequestMap.put(RequestParameters.DISCOUNT, "");
            }
        }
        
    }
    
    private String serializePayRequest(Map<String, Object> paymentRequest) {
        String desc = "";

        if (paymentRequest != null && !paymentRequest.isEmpty()) {
            for (String key : paymentRequest.keySet()) {
                // not storing CC related information
                if (!blackListedKeys.contains(key)) {
                    desc += key.toString() + " ==>>  "
                            + MapUtils.getString(paymentRequest, key, null)
                            + " :: ";
                }
            }
        }

        return desc;
    }

    private PaymentRequest dumpRequestAudit(PaymentRequest paymentRequest) {

        try {
            paymentRequest = paymentTransactionService.create(paymentRequest);
        } catch (Exception exp) {
            logger.error("payment request not inserted in db " + paymentRequest.getOrderId());
        }
        return paymentRequest;
    }
    
    private PaymentTransaction createPaymentTransaction(PaymentTransaction pt) {
        try {
            pt = paymentTransactionService.create(pt);
        } catch (Exception exp) {
            logger.error("error in inserting payment transaction details for order id :" + pt.getOrderId(), exp);
        }
        return pt;
    }

    @Override
    protected PaymentResponse createPaymentResponse(Map<String, String> payResponseMap) {
        PaymentResponse paymentResponse = new PaymentResponse();
        
        if(payResponseMap.get(RequestParameters.AMOUNT) != null) {
            paymentResponse.setAmount(new Double(payResponseMap.get(RequestParameters.AMOUNT)));
        }
        paymentResponse.setIsSuccessful(isPaymentSuccessful(payResponseMap));
        paymentResponse.setPaymentGateway(Constants.PAYU_PAYMENT_GATEWAY);
        paymentResponse.setPgTransactionId(payResponseMap.get(RequestParameters.PAYU_TRANSACTION_ID));
        paymentResponse.setMtxnId(payResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID));

		String orderId = null;
		PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(payResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID));
		
		if (paymentTransaction != null) {
			orderId = paymentTransaction.getOrderId();
			paymentResponse.setOrderId(orderId);
		}

		if (paymentTransaction != null && paymentResponse.getIsSuccessful()) {
			handleBinOffers(payResponseMap, paymentResponse, orderId, paymentTransaction);

		}
        // The end bank reference code.
        paymentResponse.setAuthCodeNo(payResponseMap.get(RequestParameters.BANK_REFERENCE));
        
        // The bank used.
        paymentResponse.setEci(payResponseMap.get(RequestParameters.PG_USED));

        String paymentResponseCode = payResponseMap.get(RequestParameters.PAY_RESPONSE_CODE);
        String paymentStatusCode = payResponseMap.get(RequestParameters.PAYMENT_STATUS_CODE);

        paymentResponse.setAvsCode(payResponseMap.get(RequestParameters.BANK_CODE));
        paymentResponse.setResponseFromPp(PaymentUtil.getMapAsString(payResponseMap));
        paymentResponse.setPgMessage(paymentResponseCode);
        paymentResponse.setRrnNo(paymentStatusCode);
        paymentResponse.setResponseData(PaymentUtil.serializedResponse(payResponseMap));
        
        if (paymentResponseCode != null && !paymentResponseCode.isEmpty()) {
            metricsClient.recordEvent("Payment", "PayU.ResponseCode", paymentResponseCode);
        }
        
        if (paymentStatusCode != null && !paymentStatusCode.isEmpty()) {
            metricsClient.recordEvent("Payment", "PayU.StatusCode", paymentStatusCode);
        }
                
        return paymentResponse;
    }

    private void handleBinOffers(Map<String, String> payResponseMap, PaymentResponse paymentResponse, String orderId,
            PaymentTransaction paymentTransaction) {
        String discountStr = payResponseMap.get(RequestParameters.DISCOUNT);
        String offerKey = payResponseMap.get(RequestParameters.OFFER);
        String offerAvailed = payResponseMap.get(RequestParameters.OFFER_AVAILED);
        String offerType = payResponseMap.get(RequestParameters.OFFER_TYPE);
        String binOfferId = payResponseMap.get(RequestParameters.UDF1);

        logger.info("Offer info for  " + orderId + " discountStr : " + discountStr + " offerKey : " + offerKey
                + " offerAvailed : " + offerAvailed);

        boolean isEligibleForBinOffer = StringUtils.isNotEmpty(discountStr) && StringUtils.isNotEmpty(offerAvailed);

        if (isEligibleForBinOffer) {
            CartBusinessDo cartBusinessDo = cartService.getCartByOrderId(orderId);
            logger.info("Giving discount to " + orderId);
            int binId = Integer.parseInt(binOfferId);
            BinOffer binOffer = binOfferService.getBinOffer(binId);
            try {
                binOffer.setOfferType(OfferType.fromPaymentResponse(offerType).toString());
                binOfferService.saveCartWithBinOffer(binOffer, cartBusinessDo, new Double(discountStr));

                switch (OfferType.valueOf(binOffer.getOfferType())) {
                    case CASHBACK:
                        paymentTransactionService.cashbackToWallet(paymentTransaction,
                                FCUtil.roundOff(new Double(discountStr)), binOffer.getOfferKey() + "-" + orderId,
                                binOffer.getId() + "");
                        logger.info("Cashback availed " + orderId + " With offer " + binOffer.getName());
                        break;

                    case INSTANT:
                        paymentResponse.setAmount(paymentResponse.getAmount() - new Double(discountStr));
                        updatePaymentTransactionAndPlan(paymentResponse, paymentTransaction);
                        logger.info("Instant doscount availed " + orderId + " With offer " + binOffer.getName());
                        break;

                    default:
                        throw new IllegalStateException("Got some unknown offer type: [" + binOffer.getOfferType()
                                + "]");
                }
            } catch (Exception e) {
                logger.error("error in using offerKey (binOfferId) " + offerKey + "(" + binOfferId + ")"
                        + " for orderId : " + orderId, e);
            }
        }
    }

    @Transactional
    private void updatePaymentTransactionAndPlan(PaymentResponse paymentResponse, PaymentTransaction paymentTransaction) {
        paymentTransaction.setAmount(paymentResponse.getAmount());
        paymentTransactionService.updatePaymentTransactionAmount(paymentTransaction);
        
        /*
         * We need to update payment plan as well.
         */
        try {
            PaymentPlan paymentPlanToUpdate = paymentPlanService.getPaymentPlanFromMtxnId(paymentTransaction.getMerchantTxnId(), paymentTransaction.getOrderId());
            paymentPlanToUpdate.setPgAmount(new Amount(paymentResponse.getAmount()));
            paymentPlanService.updatePaymentPlan(paymentPlanToUpdate);
            
        } catch (PaymentPlanNotFoundException ppnfe) {
            logger.info("Payment plan not found for order ID[" + paymentTransaction.getOrderId() + "]; not updating");
        }
    }
    
    @Override
    protected void validatePaymentResponse(Map<String, String> payResponse) {
        // MOCk PG: TBD: Remove this by calculating actual has in dummyPG
        if (fcProperties.shouldMockPG()) {
            payResponse.put(RequestParameters.CHECKSUM_RESULT, "true");
            return;
        }
        
        String expectedChecksum = getResponseHashV2(payResponse);
        String actualChecksum = payResponse.get(RequestParameters.CHECKSUM);
        String expectedCheckSumS2s = getResponseHashForS2S(payResponse);
        String orderId = payResponse.get(RequestParameters.PAYMENT_REFERENCE_ID);

        if (expectedChecksum == null || actualChecksum == null
                || expectedChecksum.trim().isEmpty()
                || actualChecksum.trim().isEmpty()) {
            logger.error("One of the checksum is empty or null failing payment. Actual: ["
                    + actualChecksum
                    + "], Expected: ["
                    + expectedChecksum
                    + "]");
            
            payResponse.put(RequestParameters.CHECKSUM_RESULT, "false");
            
            metricsClient.recordEvent("Payment", "PayU", "SignatureEmpty");
            return;
        }
        
        logger.info("Expected UI checksum: [" + expectedChecksum + "]. Expected S2S checksum: ["+expectedCheckSumS2s+"] Actual chcksum: [" + actualChecksum + "]" + "for orderId = "+orderId);
        /**
         * Added new s2s check - payu s2s checksum calculation is different
         */
        if(expectedChecksum.equals(actualChecksum)) {
            logger.info("Checksum matched!! from payu for orderId ="+orderId);
            payResponse.put(RequestParameters.CHECKSUM_RESULT, "true");
        }else if(StringUtils.isNotBlank(expectedCheckSumS2s) && expectedCheckSumS2s.equals(actualChecksum)){ 
            logger.info("Checksum matched for payu -- S2S response!! for orderId ="+orderId);
            payResponse.put(RequestParameters.CHECKSUM_RESULT, "true");
        } else {
            logger.info("Checksum mismatch from payu for orderId ="+orderId);
            metricsClient.recordEvent("Payment", "PayU", "SignatureEmpty");
            payResponse.put(RequestParameters.CHECKSUM_RESULT, "false");
        }
    }
    
    @Override
    protected String getPaymentPortalMerchantId() {
        return fcProperties.getProperty(PaymentConstants.PAYU_MERCHANT_ID);
    }
    
    @Override
    protected String getMerchantTransactionId(Map<String, String> payResponseMap) {
        return payResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID);
    }
    
    private String getRequestHashV2(Map<String, Object> paymentRequestMap) {
        /*
         * "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
         */
        
        String stringToHash = paymentRequestMap.get(RequestParameters.MERCHANT_ID) + "|"
                + paymentRequestMap.get(RequestParameters.PAYMENT_REFERENCE_ID) + "|"
                + paymentRequestMap.get(RequestParameters.AMOUNT) + "|"
                + paymentRequestMap.get(RequestParameters.PRODUCT_DESCRIPTION) + "|"
                + paymentRequestMap.get(RequestParameters.FIRST_NAME) + "|"
                + paymentRequestMap.get(RequestParameters.CUSTOMER_EMAIL) + "|"
                + (paymentRequestMap.get(RequestParameters.UDF1) == null ? "" : paymentRequestMap.get(RequestParameters.UDF1) )+"||||||||||"
                + (paymentRequestMap.get(RequestParameters.OFFER_KEY) == null ? "" : paymentRequestMap.get(RequestParameters.OFFER_KEY)) +"|"
                + (paymentRequestMap.get(RequestParameters.DISCOUNT) == null ? "" : paymentRequestMap.get(RequestParameters.DISCOUNT)) +"|"
                + (paymentRequestMap.get(RequestParameters.OFFER_TYPE) == null ? "" : paymentRequestMap.get(RequestParameters.OFFER_TYPE)) +"|"
                + fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT);
        
        //stringToHash = stringToHash + "|" + fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT);
                
        return calculateHash(stringToHash, Constants.HASH_ALGORITHM);
    }
    
    /**
     *For s2s  salt|status|udf10|udf9|udf8|udf7|udf6|udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key
     * @param paymentRequestMap
     * @return
     */
    private String getResponseHashForS2S(Map<String, String> paymentResponseMap) {
        String stringToHash =  fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT) + "|"
                + paymentResponseMap.get(RequestParameters.TRANSACTION_STATUS) + "|"
                + (paymentResponseMap.get(RequestParameters.UDF10) == null ? "" : paymentResponseMap.get(RequestParameters.UDF10)) + "|"
                +"||||||||"
                +(paymentResponseMap.get(RequestParameters.UDF1) == null ? "" : paymentResponseMap.get(RequestParameters.UDF1))+"|"
                + paymentResponseMap.get(RequestParameters.CUSTOMER_EMAIL) + "|"
                + paymentResponseMap.get(RequestParameters.FIRST_NAME) + "|"
                + paymentResponseMap.get(RequestParameters.PRODUCT_DESCRIPTION) + "|"
                + paymentResponseMap.get(RequestParameters.AMOUNT) + "|"
                + paymentResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID) + "|"
                + paymentResponseMap.get(RequestParameters.MERCHANT_ID);
                
        return calculateHash(stringToHash, Constants.HASH_ALGORITHM);
    }


    private String getResponseHashV2(Map<String, String> paymentResponseMap) {
        String stringToHash =  fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT) + "|"
                + paymentResponseMap.get(RequestParameters.TRANSACTION_STATUS) + "|"
                + (paymentResponseMap.get(RequestParameters.OFFER_TYPE) == null ? "" : paymentResponseMap.get(RequestParameters.OFFER_TYPE)) + "|"
                + paymentResponseMap.get(RequestParameters.DISCOUNT) + "|"
                + (paymentResponseMap.get(RequestParameters.OFFER) == null ? "" : paymentResponseMap.get(RequestParameters.OFFER)) + "||||||||||"
                + (paymentResponseMap.get(RequestParameters.UDF1) == null ? "" : paymentResponseMap.get(RequestParameters.UDF1))+"|"
                + paymentResponseMap.get(RequestParameters.CUSTOMER_EMAIL) + "|"
                + paymentResponseMap.get(RequestParameters.FIRST_NAME) + "|"
                + paymentResponseMap.get(RequestParameters.PRODUCT_DESCRIPTION) + "|"
                + paymentResponseMap.get(RequestParameters.AMOUNT) + "|"
                + paymentResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID) + "|"
                + paymentResponseMap.get(RequestParameters.MERCHANT_ID);
        logger.info("String to hash for orderId="+paymentResponseMap.get(RequestParameters.PAYMENT_REFERENCE_ID)+" is = "+stringToHash);        
        return calculateHash(stringToHash, Constants.HASH_ALGORITHM);
    }

    private String calculateHash(String stringToHash, String hashType) {
        byte[] hashseq = stringToHash.getBytes();
        
        StringBuffer hexString = new StringBuffer();
        
        try{
            MessageDigest algorithm = MessageDigest.getInstance(hashType);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();

            for (int i=0;i<messageDigest.length;i++) {
                String hex=Integer.toHexString(0xFF & messageDigest[i]);

                if(hex.length()==1) {
                    hexString.append("0");
                }
            
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) { 
            logger.error("Something wrong here while calculating hash for: [" + stringToHash + "] with type: [" + hashType + "]", nsae);
            throw new RuntimeException(nsae);
        }
        
        return hexString.toString();
    }
    
    
    private boolean isPaymentSuccessful(Map<String,String> payResponseMap) {
        String checkSumResult = payResponseMap.get(RequestParameters.CHECKSUM_RESULT);
        
        if(checkSumResult == null || checkSumResult.trim().isEmpty()) {
            return false;
        }
        
        Boolean didChecksumMatch  = Boolean.valueOf(checkSumResult);
        
        String payTransactionStatus = payResponseMap.get(RequestParameters.TRANSACTION_STATUS);
        
        if(payTransactionStatus == null || payTransactionStatus.trim().isEmpty()) {
            return false;
        }
        
        boolean payTransactionSuccessful = Constants.SUCCESS_STATUS.equals(payTransactionStatus);
        
        return didChecksumMatch && payTransactionSuccessful;
    }
    
    @Override
    public Map<String, Object> handleS2SPaymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws Exception {

        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        logger.info("PayU response handling starts, parameters received are: " + PaymentUtil.getResponseDataWithoutCardData(requestMap));
        String mtxnId = requestMap.get(RequestParameters.PAYMENT_REFERENCE_ID);
        PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
        paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        paymentResponseDO.setRequestMap(requestMap);

        PaymentTransaction uniquePaymentTransaction = paymentTransactionService
                .getPaymentTransactionByMerchantOrderIdAndPG(mtxnId, PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        /*
         * Don't care about success and error URLs here so null is good enough.
         */
        if (uniquePaymentTransaction == null) {
            throw new PaymentTransactionNotFoundException("MtxnID: [" + String.valueOf(mtxnId) + "]");
        }
        metricsClient.recordLatency("Payment", "S2SLatency." + uniquePaymentTransaction.getPaymentGateway(), System.currentTimeMillis()-uniquePaymentTransaction.getSetToPg().getTime());
        return handlePaymentResponse(paymentResponseDO, uniquePaymentTransaction.getOrderId(),
                uniquePaymentTransaction.getPaymentTxnId(), null, null, false);
    }

    @Override
    public PaymentRefundBusinessDO refundPayment(
            PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws FCPaymentGatewayException, Exception {
        
        String pgTransactionId = paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getTransactionId();
        
        logger.info("About to refund for PG Transaction ID: [" + pgTransactionId + "]");
        
        NameValuePair[] refundSubmitData = createRefundSubmitData(paymentRefundBusinessDO);
        logger.info(String.format("Payu refund submit data for order id %s: %s", paymentRefundBusinessDO.getOrderId(), refundSubmitData.toString()));
       
        this.publishRefundAttempt(paymentRefundBusinessDO,PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        
        Map<String, Object> refundResponse = makePayUWSCAll(refundSubmitData);
        if (refundResponse == null && paymentRefundBusinessDO != null){
            throw new Exception("Got NULL status response from PayU not doing refund for order id: " + paymentRefundBusinessDO.getOrderId());
        }
        logger.info(String.format("Payu refund response data for order id %s: %s", paymentRefundBusinessDO.getOrderId(), refundResponse));

        Integer wsCallStatus = (Integer) refundResponse.get(WebServiceParameters.WS_CALL_STATUS);

        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();

        if(Constants.WS_CALL_SUCCESS.equals(wsCallStatus)) {
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.INITIATED);
            paymentRefundBusinessDO.setStatus(true);
        } else {
            logger.error("WS call failed, doing nothing returning!");
            paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.FAILURE);
            paymentRefundBusinessDO.setStatus(false);
        }
        
        this.publishRefundAttemptStatus(paymentRefundBusinessDO,PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        
        Amount refundedAmount = new Amount(new BigDecimal(paymentRefundBusinessDO.getRefundAmount()));
        paymentRefundTransaction.setRefundedAmount(refundedAmount);
        paymentRefundTransaction.setPaymentTransactionId(paymentRefundBusinessDO.getPaymentTransactionBusinessDO().getPaymentTransactionId());
        paymentRefundTransaction.setRawPGRequest(serializeWSRequest(refundSubmitData));
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setRawPGResponse(PaymentUtil.serializedResponse(refundResponse));
        paymentRefundTransaction.setPgTransactionReferenceNo(((Integer) refundResponse.get(WebServiceParameters.REFUND_TXN_ID)).toString());

        paymentRefundTransaction.setPgResponseDescription((String) refundResponse.get(WebServiceParameters.REFUND_RESPONSE_MESSAGE));
        paymentRefundTransaction.setPgResponseCode(refundResponse.get(WebServiceParameters.REFUND_RESPONSE_CODE).toString());
        paymentRefundTransaction.setPgStatus(refundResponse.get(WebServiceParameters.REFUND_RESPONSE_CODE).toString());
        paymentRefundTransaction.setRefundTo(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentTransactionService.create(paymentRefundTransaction);
        
        paymentRefundBusinessDO.setMsg(paymentRefundTransaction.getPgResponseDescription());
        paymentRefundBusinessDO.setRawPGResponse(paymentRefundTransaction.getRawPGResponse());
        
        

        return paymentRefundBusinessDO;
    }
       

    @SuppressWarnings("unchecked")
    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
        String merchantTransactionId = paymentQueryBusinessDO.getMerchantTxnId();
        
        logger.info("About to query PayU for the status of MtxnID: [" + merchantTransactionId + "]");
        
        NameValuePair[] statusCheckSubmitData = createStatusCheckSubmitData(merchantTransactionId);

        Map<String, Object> statusCheckResponse = makePayUWSCAll(statusCheckSubmitData);
        if (statusCheckResponse == null && paymentQueryBusinessDO != null)
        {
            throw new RuntimeException("Got NULL response from PayU not doing refund for order id: " + paymentQueryBusinessDO.getOrderId());
        }
        Integer wsCallStatus = (Integer) statusCheckResponse.get(WebServiceParameters.WS_CALL_STATUS);
        
        if(!Constants.WS_CALL_SUCCESS.equals(wsCallStatus)) {
            logger.error("WS call failed, doing nothing returning!");
            paymentQueryBusinessDO.setPaymentStatus(false);
            paymentQueryBusinessDO.setResponseMsg("No Reference Id passed");
            return paymentQueryBusinessDO;
        } 

        Map<String, Object> transactionStatusResponse = (Map<String, Object>) statusCheckResponse.get(WebServiceParameters.PAYMENT_RESPONSE_PARAMETER);
        
        Map<String, String> paymentResponse = (Map<String, String>) transactionStatusResponse.get(merchantTransactionId);
        
        /*
         * A few quirky fixes
         */
        paymentResponse.put(RequestParameters.AMOUNT, paymentResponse.get("amt"));
        paymentResponse.put(RequestParameters.PAYMENT_REFERENCE_ID, merchantTransactionId);
        paymentResponse.put(RequestParameters.CHECKSUM_RESULT, "true");

        PaymentTransaction paymentTransaction = paymentTransactionService.getUniquePaymentTransactionByMerchantOrderId(merchantTransactionId);
        
        PaymentResponseBusinessDO payResponseDO = new PaymentResponseBusinessDO();
        payResponseDO.setRequestMap(paymentResponse);
        Map<String, Object> finalmap = handleValidPayUResponse(payResponseDO, paymentTransaction.getOrderId(), paymentTransaction.getPaymentTxnId(), null, null, isPaymentStatusForAdminPanel, paymentResponse);
        Map<String, String> resultMap = (Map<String, String>)finalmap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
        String paymentStatus = resultMap.get( PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL);
        if (paymentStatus.equals("true")) {
            paymentQueryBusinessDO.setPaymentStatus(true);
        } else {
            paymentQueryBusinessDO.setPaymentStatus(false);
        }
        /*
         * Refresh payment transaction. 
         */
        PaymentTransaction refreshedPaymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTransaction.getPaymentTxnId());
        
        if(refreshedPaymentTransaction.getIsSuccessful()) {
            paymentQueryBusinessDO.setPgTransactionId(refreshedPaymentTransaction.getTransactionId());
        }
        paymentQueryBusinessDO.setResponseMsg("NA");

        logger.debug("Status check complete for PayU : " + paymentQueryBusinessDO.getOrderId());

        return paymentQueryBusinessDO;
    }
    
    private String serializeWSRequest(NameValuePair[] wsRequest) {
        String serializedRequest = "";
        
        for (NameValuePair nameValuePair : wsRequest) {
            serializedRequest += nameValuePair.getName() + ":" + nameValuePair.getValue() + ",";
        }
        
        return serializedRequest;
    }

    private NameValuePair[] createStatusCheckSubmitData(
            String merchantTransactionId) {
        
        String merchantKey = fcProperties.getProperty(PaymentConstants.PAYU_MERCHANT_ID);
        String hashSalt = fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT);

        String hash = calculateHash(merchantKey
                + Constants.HASH_STRING_DELIMITER
                + Constants.PAYMENT_STATUS_METHOD
                + Constants.HASH_STRING_DELIMITER + merchantTransactionId
                + Constants.HASH_STRING_DELIMITER + hashSalt, "SHA-512");
        
        NameValuePair[] statusCheckSubmitData = {
                new NameValuePair(WebServiceParameters.METHOD_NAME, Constants.PAYMENT_STATUS_METHOD),
                new NameValuePair(WebServiceParameters.MERCHANT_KEY_PARAMETER, merchantKey),
                new NameValuePair(WebServiceParameters.MERCHANT_TXN_ID_PARAMETER, merchantTransactionId),
                new NameValuePair(WebServiceParameters.HASH_PARAMETER, hash),
            };
        
        return statusCheckSubmitData;
    }
    
    private NameValuePair[] createRefundSubmitData(PaymentRefundBusinessDO refundDo) {
        String pgTransactionId = refundDo.getPaymentTransactionBusinessDO().getTransactionId();
        String refundTransactionId = refundDo.getPaymentTransactionBusinessDO().getMerchantTxnId() + Constants.REFUND_SUFFIX;
        
        String merchantKey = fcProperties.getProperty(PaymentConstants.PAYU_MERCHANT_ID);
        String hashSalt = fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT);
        
        String hash = calculateHash(
                merchantKey
                + Constants.HASH_STRING_DELIMITER

                + Constants.PAYMENT_REFUND_METHOD
                + Constants.HASH_STRING_DELIMITER

                + pgTransactionId
                + Constants.HASH_STRING_DELIMITER 
                
                + hashSalt, 
                "SHA-512");

        NameValuePair[] refundSubmitData = {
                new NameValuePair(WebServiceParameters.METHOD_NAME, Constants.PAYMENT_REFUND_METHOD),

                new NameValuePair(WebServiceParameters.REFUND_PG_TRANSACTION_ID, pgTransactionId),
                new NameValuePair(WebServiceParameters.REFUND_REFERENCE_ID, refundTransactionId),
                new NameValuePair(WebServiceParameters.REFUND_AMOUNT, refundDo.getRefundAmount().toString()),
                
                new NameValuePair(WebServiceParameters.MERCHANT_KEY_PARAMETER, merchantKey),
                new NameValuePair(WebServiceParameters.HASH_PARAMETER, hash),
            };
        
        return refundSubmitData;
    }
    
    private Map<String, Object> makePayUWSCAll(NameValuePair[] postData) {
        String wsURL = fcProperties.getProperty(PaymentConstants.PAYU_STATUS_CHECK_URL);
        
        HttpClient client = new HttpClient();
        client.setHttpConnectionManager(payUConnectionManager);
        PostMethod postMethod = new PostMethod(wsURL);
        
        try {
            postMethod.setRequestBody(postData);

            int postStatus;
            String paymentResponse;
            try {
                postStatus = client.executeMethod(postMethod);
                paymentResponse = postMethod.getResponseBodyAsString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } 
            
            logger.info("Web service response from PayU : " + paymentResponse);

            if(HttpStatus.SC_OK == postStatus) {
                Map<String, Object> wsResponse = deserializer.deserialize(paymentResponse);
                
                return wsResponse;
            } else {
                throw new RuntimeException(
                        "Error while calling PayU WS end point : [" + wsURL + "]"
                                + "Got HTTP failure HTTP Code [" + postStatus
                                + "]. Post Response [" + paymentResponse + "]");
            }
        } finally{
            postMethod.releaseConnection();
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public HashMap<String, Object> getIssuingBankStatus(String cardFirstSixDigit) {
        Map respMap = banksStatusCache.getIssuingBankStatusByCardNo(cardFirstSixDigit);
        if (respMap != null && !respMap.isEmpty()) {
            logger.info("Cache found for issuing bank status for card");
            return new HashMap(respMap);
        } else {
            NameValuePair[] postData = createBankStatusCheckSubmitData(cardFirstSixDigit,
                    Constants.ISSUING_BANK_STATUS_METHOD);
            HashMap resp = (HashMap<String, Object>) makePayUWSCAll(postData);
            if (null == resp || resp.isEmpty()) {
                logger.info("No response from payu for issuing bank status");
                return null;
            }
            banksStatusCache.setIssuingBankStatus(cardFirstSixDigit, resp);
            return resp;
        }
    }

    private NameValuePair[] createBankStatusCheckSubmitData(String var1Data, String method) {

        String merchantKey = fcProperties.getProperty(PaymentConstants.PAYU_MERCHANT_ID);
        String hashSalt = fcProperties.getProperty(PaymentConstants.PAYU_HASH_SALT);

        String hash = calculateHash(merchantKey + Constants.HASH_STRING_DELIMITER + method
                + Constants.HASH_STRING_DELIMITER + var1Data + Constants.HASH_STRING_DELIMITER + hashSalt, "SHA-512");

        NameValuePair[] statusCheckSubmitData = { new NameValuePair(WebServiceParameters.METHOD_NAME, method),
                new NameValuePair(WebServiceParameters.MERCHANT_KEY_PARAMETER, merchantKey),
                new NameValuePair(WebServiceParameters.VAR1_DATA, var1Data),
                new NameValuePair(WebServiceParameters.HASH_PARAMETER, hash), };

        return statusCheckSubmitData;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public HashMap<String, Object> getNetBankingStatus(String bankCode) {
        String bankCodePayu = payuBankCodesFromUi.get(bankCode);
        if (bankCodePayu == null) {
            logger.info("No mapping for bank code " + bankCode + " in payuBankCodesFromUi map.");
            return null;
        }
        Map respMap = banksStatusCache.getNetBankingStatusByBankCode(bankCodePayu);

        if (respMap != null && !respMap.isEmpty()) {
            logger.info("Cache found for net banking status for " + bankCode);
            return new HashMap(respMap);
        } else {
            NameValuePair[] postData = createBankStatusCheckSubmitData(bankCodePayu,
                    Constants.NET_BANKING_STATUS_METHOD);
            HashMap resp = (HashMap<String, Object>) makePayUWSCAll(postData);
            if (null == resp || resp.isEmpty()) {
                logger.info("No response from payu for net banking status");
                return null;
            }
            banksStatusCache.setIssuingBankStatus(bankCodePayu, resp);
            return resp;
        }
    }

    @SuppressWarnings("unchecked")
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        PaymentCard paymentCard = null;
        NameValuePair[] statusCheckData = createStatusCheckSubmitData(merchantTxnId);
        Map<String, Object> statusCheckResponse = null;
        try {
        	/*
             * Since we are calculating card hash on our side and persisting it during payment processing, 
             * fetching the payment card details from DB in the first attempt;
             */
            paymentCard = cardBinService.getPaymentCardForOrderId(merchantTxnId);
            if (paymentCard == null) {
            	/*
            	 * Case where card details was not found in database (old transactions),
            	 * we need to fetch card bin and card hash from PayU 
            	 */
            	logger.error("DB lookup failed. Fetching card details from payu");
            	paymentCard = new PaymentCard();
                statusCheckResponse = makePayUWSCAll(statusCheckData);
                if (statusCheckResponse == null && !merchantTxnId.isEmpty()) {
                	logger.info("Got NULL response from PayU merchantTxnId: " + merchantTxnId);
                	return paymentCard;
                }

                Integer wsCallStatus = (Integer) statusCheckResponse.get(WebServiceParameters.WS_CALL_STATUS);

                if (!Constants.WS_CALL_SUCCESS.equals(wsCallStatus)) {
                	logger.error("WS call failed, now looking up in our database!");
                	return paymentCard;
                }
                Map<String, Object> transactionStatusResponse = (Map<String, Object>) statusCheckResponse
                		.get(WebServiceParameters.PAYMENT_RESPONSE_PARAMETER);
                Map<String, Object> transactionStatusResponseMap = (Map<String, Object>) transactionStatusResponse
                		.get(merchantTxnId);
                String payStatus = (String) transactionStatusResponseMap.get(WebServiceParameters.WS_CALL_STATUS);
                if (!Constants.SUCCESS_STATUS.equals(payStatus)) {
                	logger.error("It is a failed payment. Returning null for mtxnId : " + merchantTxnId);
                	return paymentCard;
                }
                String cardHash = (String) transactionStatusResponseMap.get(WebServiceParameters.WS_CALL_CARD_HASH);
                paymentCard.setCardHash(cardHash);
                String cardBin = (String) transactionStatusResponseMap.get(WebServiceParameters.WS_CALL_CARD_BIN);
                paymentCard.setCardBin(cardBin);
            }
        } catch(RuntimeException e) {
            logger.info("Caught RuntimeException while getting card data from payu for mtxn id: " + merchantTxnId, e);
        } catch (Exception e) {
            logger.info("Caught Exception while getting card data from payu for mtxn id: " + merchantTxnId, e);
        }
        return paymentCard;
    }

    @Override
    public Map<String, Object> getSuccessfulPgRefundData(PaymentRefundBusinessDO refundBusinessDO) {
        Map<String, Object> refundStatusMap = new HashMap<>();
        refundStatusMap.put("refundAmount", refundBusinessDO.getRefundAmount());
        refundStatusMap.put("refundedOn", new Date());
        return refundStatusMap;
    }    
    
}
