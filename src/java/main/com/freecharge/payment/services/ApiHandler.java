package com.freecharge.payment.services;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.exception.HTTPAbortException;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.gson.Gson;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.nio.charset.Charset;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Map;

@Service
public class ApiHandler {
    @Autowired
    FCProperties properties;

    private static CloseableHttpClient AuthHttpClient;
    private static String keyCertificateBundlePath;
    private static final Logger log = LoggerFactory.getLogger(ApiHandler.class);

    private SSLContext loadClientCertificate() throws KeyManagementException, UnrecoverableKeyException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {

        String keyStorePass = properties.getProperty(FCProperties.STORE_PASS);
        String privateKeyPass = properties.getProperty(FCProperties.KEY_PASS);

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(new FileInputStream(new File(properties.getProperty(FCProperties.KEYSTORE_PATH))),
                keyStorePass.toCharArray());

        SSLContext sslcontext = org.apache.http.conn.ssl.SSLContexts.custom()
                .loadKeyMaterial(keyStore, privateKeyPass.toCharArray())
                .build();
        return sslcontext;
    }

    private SSLConnectionSocketFactory getSSLSocketFactory() throws KeyManagementException, UnrecoverableKeyException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(loadClientCertificate(),
                new String[] {"TLSv1.2"}, null, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        return sslSocketFactory;
    }

    private CloseableHttpClient fetchAuthHttpClient(int readTimeout, int connectTimeout) throws KeyManagementException, UnrecoverableKeyException,
            NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {

        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectTimeout)
                .setSocketTimeout(readTimeout).build();
        HttpClientBuilder clientBuilder = HttpClients.custom().setDefaultRequestConfig(requestConfig);
        CloseableHttpClient httpClient = clientBuilder.setSSLSocketFactory(getSSLSocketFactory()).build();
        return httpClient;
    }

    private HttpRequest createHttpRequest(RequestType methodType, String url){
        HttpRequest request = null;
        switch (methodType) {
            case GET:
                request = new HttpGet(url);
                break;
            case POST:
                request = new HttpPost(url);
                break;
            default: {
                System.out.println("Incompatible HTTP request method " + methodType);
            }
        }
        return request;
    }

    private CloseableHttpResponse doAuthRequest(String url, String body, RequestType methodType, Map<String, String> headers,
                                                int connectTimeout, int readTimeout) throws  HTTPAbortException {

        HttpRequest request = createHttpRequest(methodType, url);
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        if (headers != null && headers.isEmpty() == false) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                request.setHeader(header.getKey(), header.getValue());
            }
        }

        if (request instanceof HttpPost) {
            ((HttpPost) request).setEntity(new StringEntity(body, "UTF-8"));
        } else if (request instanceof HttpPut) {
            ((HttpPut) request).setEntity(new StringEntity(body, "UTF-8"));
        }


        log.info("Request Headers: " + Arrays.asList(request.getAllHeaders()));
        CloseableHttpClient httpClient = null;
        try {
            httpClient = fetchAuthHttpClient(readTimeout + 1000, connectTimeout);
        } catch (Exception e) {
            throw new HTTPAbortException("Exception in creating http client", e);
        }
        //login request
        log.info("method is         : {}"+request.getRequestLine().getMethod());
        log.info("url is ="+request.getRequestLine().getUri());
        log.info("body is"+body);


        CloseableHttpResponse response;
        try {
            response = httpClient.execute((HttpUriRequest) request);
        } catch (IOException e) {
            log.info("http exception is :"+e);
            throw new HTTPAbortException("Exception executing HTTP request to servers", e);
        }

        return response;
    }

    private <RS> RS parseHTTPResponse(Class<RS> responseClass, CloseableHttpResponse response, StringBuilder statusIdentifier)
            throws HTTPAbortException {
        int responseCode = response.getStatusLine().getStatusCode();

        RS responseObj = null;
        StringBuilder responseString;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String responseLine = null;
            responseString = new StringBuilder();
            while ((responseLine = reader.readLine()) != null) {
                responseString.append(responseLine);
            }
        } catch (IllegalStateException | IOException e) {
            log.error("Exception while parsing HTTP response", e);
            throw new HTTPAbortException(e);
        }

        log.info("Response received from Pg" + responseString + "|StatusCode: " + responseCode);

        if(responseCode == 200) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                responseObj = mapper.readValue(responseString.toString(), responseClass);
            }
            catch(Exception e){
                throw new HTTPAbortException("Exception while mapping response");
            }
        } else {
            statusIdentifier.append(responseString);
            throw new HTTPAbortException("Status Code : " + responseCode);
        }

        return responseObj;
    }

    public <RQ, RS> RS executeHTTPPost(String url, RQ request, Map<String, String> headers, int connectTimeout, int readTimeout,
                                       Class <RS> responseClass, StringBuilder statusIdentifier) throws HTTPAbortException {

        ObjectMapper objectMapper=new ObjectMapper();
        String jsonRequest="";
        try {
            jsonRequest = objectMapper.writeValueAsString(request);
        }catch (Exception e){
            log.error("Exception while mapping the request");
            throw new HTTPAbortException(e);
        }

        CloseableHttpResponse response = doAuthRequest(url, jsonRequest, RequestType.POST, headers, connectTimeout, readTimeout);

        return parseHTTPResponse(responseClass, response, statusIdentifier);
    }

    public enum RequestType {
        GET,
        POST;
    }
}
