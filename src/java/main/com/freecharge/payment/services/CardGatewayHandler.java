package com.freecharge.payment.services;

import java.util.HashMap;
import java.util.Map;

import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;

public interface CardGatewayHandler {
	public HashMap<String, Object> getIssuingBankStatus(String cardFirstSixDigit);
	public HashMap<String, Object> getNetBankingStatus(String bankCode);
	public Map<String, Object> getSuccessfulPgRefundData(PaymentRefundBusinessDO refundBusinessDO);
}
