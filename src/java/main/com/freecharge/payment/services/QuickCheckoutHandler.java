package com.freecharge.payment.services;

public interface QuickCheckoutHandler {

    public String getQCStatusForUser(String userId);
}
