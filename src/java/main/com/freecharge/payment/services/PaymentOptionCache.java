package com.freecharge.payment.services;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.web.NetBankingOptionResponse;

@Service
public class PaymentOptionCache extends RedisCacheManager{

	private final long  TIME_TO_CACHE = 30;
	private final Logger logger = LoggingFactory.getLogger(getClass());
	
	private final String CARD_REF_KEY = "cardReference";
	private static final String NET_BANKING_OPTION_PREFIX = "NET_BANKING_OPTION_";
	
    @Autowired
    private RedisTemplate<String, Object> lastPaymentOptionCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.lastPaymentOptionCacheTemplate;
    }
    
    public String getCardReferenceByOrderId(String orderId){
    	try{
    		return (String) get(CARD_REF_KEY+"_"+orderId);
    	}catch(Exception e){
    		logger.error("Exception while setting cardReference Key for orderId"+orderId+" is ",e);
    	}
    	return null;
    }
    
    public void setCardReferenceByOrderId(String orderId,String cardReference){
    	try{
    		set(CARD_REF_KEY+"_"+orderId,cardReference,TIME_TO_CACHE,TimeUnit.MINUTES);
    	}catch(Exception e){
    		logger.error("Exception while trying to set cardRefernceKey for orderId:"+orderId+" ",e);
    	}
    }
    
    public NetBankingOptionResponse getNetBankingOptions(String merchant) {
        try {
            return (NetBankingOptionResponse) get(NET_BANKING_OPTION_PREFIX+merchant);
        } catch (Exception e) {
            logger.error("Error in getting cache data for merchant "+merchant, e);
            return null;
        }
    }

    public void setNetBankingOptions(String merchant, NetBankingOptionResponse netBankingOptions) {
        if(netBankingOptions==null){
            return;
        }
        try {
            set(NET_BANKING_OPTION_PREFIX+merchant, netBankingOptions, 48, TimeUnit.HOURS);
        } catch (Exception e) {
            logger.error("Error in setting cache data for merchant "+merchant, e);
        }
    }
}
