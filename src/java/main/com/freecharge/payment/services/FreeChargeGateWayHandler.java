package com.freecharge.payment.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.util.PaymentConstants;

/**
 * Mock gateway handler. This is meant to mimic responses and behavior
 * of various gateway handlers.
 * @author arun
 *
 */
@Component("fcMockGatewayHandler")
public class FreeChargeGateWayHandler implements IGatewayHandler {
    @Override
    public Map<String, Object> paymentRequestSales(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentGatewayException, Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, Object> paymentResponse(PaymentResponseBusinessDO paymentResponseBusinessDO) throws FCPaymentGatewayException, Exception {
        Map<String, String> requestMap = paymentResponseBusinessDO.getRequestMap();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String orderId = requestMap.get("Order_Id");
        String transactionId = requestMap.get("nb_order_no");
        String amount = requestMap.get("Amount");

        Map<String, String> resultMap = new HashMap<String, String>();

        resultMap.put(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL, "1");
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_DESCRIPTION, "success");
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_RESULT_RESPONSE_CODE, "000");
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_ORDER_ID, orderId);
        resultMap.put(PaymentConstants.AMOUNT, amount);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_TRANSACTION_ID, transactionId);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_PAYMENT_GATEWAY, PaymentConstants.PAYMENT_GATEWAY_DUMMY_FREECHARGE_CODE);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MODE, null);
        resultMap.put(PaymentConstants.PAYMENT_PORTAL_MERCHANT_ID, "");

        
        Map<String, Object> finalMap = new HashMap<String, Object>();
        finalMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, true);
        finalMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resultMap);
        finalMap.put(PaymentConstants.KEY_WAITPAGE, PaymentConstants.WAITPAGE_RESPONSE_VIEW);
        finalMap.put(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, FCSessionUtil.getSuccessUrl(orderId, fs));

        paymentResponseBusinessDO.setResultMap(finalMap);
        paymentResponseBusinessDO.setmTxnId(orderId);
        
        return finalMap;
    }

    @Override
    public PaymentRefundBusinessDO refundPayment(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws FCPaymentGatewayException, Exception {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public PaymentQueryBusinessDO paymentStatus(PaymentQueryBusinessDO paymentQueryBusinessDO, Boolean isPaymentStatusForAdminPanel) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

	@Override
	public Map<String, Object> handleS2SPaymentResponse(
			PaymentResponseBusinessDO paymentResponseBusinessDO)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void refundToWallet(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType) throws FCPaymentGatewayException, Exception {
	    // TODO Auto-generated method stub
	    
	}

    @Override
    public PaymentCard getPaymentCardData(String merchantTxnId) {
        // TODO Auto-generated method stub
        return null;
    }
}
