package com.freecharge.payment.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.BankMaster;
import com.freecharge.payment.dos.entity.PaymentGatewayMaster;
import com.freecharge.payment.dos.entity.PgRelation;
import com.freecharge.payment.services.binrange.CardBinRangeService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentsCache;
import com.freecharge.platform.metrics.MetricsClient;

public abstract class PaymentType implements IPaymentType {

    private static final Logger LOGGER = LoggingFactory.getLogger(PaymentType.class);

    @Autowired
    private CardBinRangeService cardBinRangeService;

    public static enum PaymentTypeName {
        CREDIT_CARD(1, "Credit Card"), DEBIT_CARD(2, "Debit Card"), NET_BANKING(3, "Net Banking"), CASH_CARD(4,
                "Cash Card"), ATM_CARD(5, "ATM Card"), FC_BALANCE(6, "FC Balance"), HDFC_DEBIT_CARD(7,
                "HDFC Bank Debit Card"), ZERO_PAY(8, "ZeroPay"), ONECHECK_WALLET(9, "OneCheck Wallet"), UPI(15, "UPI"), FC_UPI(16, "FCUPI");

        private final int    paymentTypeCode;
        private final String displayString;

        private PaymentTypeName(int paymentTypeCode, String displayString) {
            this.paymentTypeCode = paymentTypeCode;
            this.displayString = displayString;
        }

        public static PaymentTypeName fromPaymentTypeCode(int fromCode) {
            switch (fromCode) {
                case 1:
                    return CREDIT_CARD;
                case 2:
                    return DEBIT_CARD;
                case 3:
                    return NET_BANKING;
                case 4:
                    return CASH_CARD;
                case 5:
                    return ATM_CARD;
                case 6:
                    return FC_BALANCE;
                case 7:
                    return HDFC_DEBIT_CARD;
                case 8:
                    return ZERO_PAY;
                case 9:
                    return ONECHECK_WALLET;
                case 16:
                    return FC_UPI;
                default:
                    throw new IllegalArgumentException("Unknown payment type code : " + fromCode);
            }
        }

        public int getPaymentTypeCode() {
            return this.paymentTypeCode;
        }

        @Override
        public String toString() {
            return this.displayString;
        }
    }

    @Autowired
    protected PaymentTransactionService paymentTransactionService;
    protected FCProperties              properties;

    @Autowired
    public void setProperties(FCProperties properties) {
        this.properties = properties;
    }

    @Autowired
    protected PaymentGatewayHandlerFactory pgFactory;

    @Autowired
    private MetricsClient                  metricsClient;
    
    @Autowired
    PaymentsCache paymentsCache;
    
    @Autowired
    CardBinService cardBinService;
    
    private static final EPinEncrypt ENCRYPT = new EPinEncrypt();
    
    private static final String CARD_DATA_CACHE_KEY = "_CARD_DATA";

    public void recordPaymentTypeMetric(String type) {
        this.metricsClient.recordEvent("Payment", "Type", type);
    }

    public PaymentResponseBusinessDO handleResponse(PaymentResponseBusinessDO paymentResponseBusinessDO)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception {

        String pg = paymentResponseBusinessDO.getPg();
        IGatewayHandler gatewayHandler = null;

        if (pg != null) {
            gatewayHandler = pgFactory.getPaymentHandler(pg);
        }

        gatewayHandler.paymentResponse(paymentResponseBusinessDO);
        return paymentResponseBusinessDO;
    }
    
    @SuppressWarnings("unused")
	@Transactional(readOnly = true)
    public IGatewayHandler getPaymentGateway(PaymentRequestBusinessDO obj) {

        Boolean defaultGateway = false;
        PgRelation pgRelation = null;
        PaymentGatewayMaster paymentGatewayMaster = null;
        Integer defaultBank = properties.getIntProperty(PaymentConstants.DEFAULT_PAYMENT_BANK_CODE_KEY);

        BankMaster bankMaster = paymentTransactionService.getBankMasterForRequest(obj.getPaymentOption(),
                obj.getPaymentType());
        if (bankMaster != null) {
            pgRelation = paymentTransactionService.getPaymentGateway(obj.getProductId(), obj.getAffiliateId(),
                    bankMaster.getId());
            if (pgRelation != null) {
                paymentGatewayMaster = paymentTransactionService.getPaymentGateway(pgRelation.getPrimaryGateway());
            } else {
                defaultGateway = true;
            }
        } else {
            pgRelation = paymentTransactionService.getPaymentGateway(obj.getProductId(), obj.getAffiliateId(),
                    defaultBank);
            if (pgRelation != null) {
                paymentGatewayMaster = paymentTransactionService.getPaymentGateway(pgRelation.getPrimaryGateway());
            } else {
                defaultGateway = true;
            }
        }
        if (pgRelation != null)
            obj.setBankCode(pgRelation.getBankMasterId());
        if (paymentGatewayMaster == null) {
            LOGGER.error("No payment gateway master found for : " + obj.getPaymentOption() + "," + obj.getPaymentType());
            this.metricsClient.recordEvent("Payment", "PG.Invalid." + obj.getAppVersion(), obj.getPaymentType() + "."
                    + obj.getPaymentOption());
            paymentGatewayMaster = paymentTransactionService.getPaymentGateway(4); // Defaulting
                                                                                   // the
                                                                                   // gateway
                                                                                   // to
                                                                                   // PayU
                                                                                   // (Gateway
                                                                                   // Id
                                                                                   // :
                                                                                   // 4)
        }
        obj.setGateWayToUse(paymentGatewayMaster.getCode());
        return pgFactory.getPaymentHandler(paymentGatewayMaster.getCode());
    }
    
    /**
	 * For a given merchant transaction id and card number, this method
	 * calculates encrypted value of the card number and stores it in the cache
	 * with expiry of 1 day. <br>
	 * The key used to store in the cache is MerchantTransactionId +
	 * "_CARD_DATA"
	 * 
	 * @param merchantTransactionId
	 * @param cardNumber
	 */
    protected void storeCardDataForOrderInCache(String merchantTransactionId, String cardNumber){
    	try {
			paymentsCache.setOrderPaymentDataInCache(merchantTransactionId + CARD_DATA_CACHE_KEY, ENCRYPT.getEncryptedPin(cardNumber), 1);
		} catch (Exception e) {
			LOGGER.error("Error storing card data in cache for mtxnid : " + merchantTransactionId, e);
			metricsClient.recordEvent("Payment", "CardData.Cache.Add", "error");
		}
    }
}
