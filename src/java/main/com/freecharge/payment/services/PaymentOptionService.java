package com.freecharge.payment.services;

import com.freecharge.util.CustomHttpClientBuilder;
import java.net.URI;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.app.service.UserService;
import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.dao.jdbc.PaymentOptionConfigDAO;
import com.freecharge.payment.dos.entity.FeatureCheckInput;
import com.freecharge.payment.dos.entity.PaymentOptionMaster;
import com.freecharge.payment.dos.web.NetBankingOption;
import com.freecharge.payment.dos.web.NetBankingOptionResponse;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.payment.Merchant;
import com.freecharge.util.FCUtil;
import com.freecharge.wallet.OneCheckWalletService;
import com.google.common.collect.ImmutableMap;

@Service
public class PaymentOptionService {

	private static final String DELIMITER = ",";

	private static final String EMPTY = "";
	private static final String DEFAULT = "NA";

	@Autowired
	AmazonDynamoDBAsync dynamoClientMum;

	private final Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	MetricsClient metricsClient;

	@Autowired
	FCProperties fcProperties;

	@Autowired
	private UserService userService;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;
	
	@Autowired
    private PaymentOptionCache paymentOptionCache;

    @Autowired
	private AppConfigService appConfigService;
    
	@Autowired
	@Qualifier("klickPayGatewayHandler")
	QuickCheckoutHandler quickCheckoutHandler;

	@Autowired
    private PaymentOptionConfigDAO paymentOptionConfigDAO;

    private ICCS ccs;
	
	@PostConstruct
	public void initializePaymentOptionService() {
	    ccs = getCCSClient();
    }

	public void insertToDynamoDB(String userId, String mode, String type) {

		Map<String, AttributeValue> item = new HashMap<>();
		if (FCUtil.isEmpty(type)) {
			type = DEFAULT;
		}
		item.put("userId", new AttributeValue(userId));
		item.put("mode", new AttributeValue(mode));
		item.put("type", new AttributeValue(type));

		final long startTime = System.currentTimeMillis();
		try {
			logger.info("About to insert to dynamoDb");
			dynamoClientMum.putItem(new PutItemRequest("PaymentOptionDynamoDB", item));
			metricsClient.recordEvent("DynamoDB", "PutItem", "Success");
			logger.info("Done recording PaymentOption Data [UserId:" + userId + ", Mode: " + mode);
		} catch (AmazonServiceException e) {
			logger.error("AmazonServiceException while recording PaymentOptionData", e);
			metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonServiceException");
		} catch (AmazonClientException e) {
			logger.error("AmazonClientException while recording LastPaymentOptionData", e);
			metricsClient.recordEvent("DynamoDB", "PutItem", "AmazonClientException");
		} finally {
			metricsClient.recordLatency("DynamoDB", "PutItem", System.currentTimeMillis() - startTime);
		}

	}

	public Map<String, String> fetchFromDynamoLastPayOption(String userId) {

		GetItemRequest getItemRequest = new GetItemRequest("PaymentOptionDynamoDB",
				ImmutableMap.of("userId", new AttributeValue(userId)));
		getItemRequest.setConsistentRead(false);

		final long startTime = System.currentTimeMillis();

		Map<String, String> result = new HashMap<String, String>();

		try {
			GetItemResult getItemResult = dynamoClientMum.getItem(getItemRequest);

			metricsClient.recordLatency("DynamoDB", "GetItem", System.currentTimeMillis() - startTime);
			metricsClient.recordEvent("DynamoDB", "GetItem", "Success");

			result = createLastPayOptionMapping(getItemResult);
			logger.info("Fetched data from PaymentOptionDynamoDB " + result);
		} catch (AmazonServiceException e) {
			metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
			logger.error("AmazonServiceException while fetching PaymentOption Data", e);
		} catch (AmazonClientException e) {
			metricsClient.recordEvent("DynamoDB", "GetItem", "Failure");
			logger.error("AmazonClientException while fetching PaymentOption Data", e);
		}
		return result;

	}

	private Map<String, String> createLastPayOptionMapping(GetItemResult getItemResult) {
		Map<String, String> response = new HashMap<>();

		if (getItemResult != null && getItemResult.getItem() != null) {
			Map<String, AttributeValue> mappingItem = getItemResult.getItem();

			String mode = mappingItem.get("mode").getS();
			String type = mappingItem.get("type").getS();

			if (DEFAULT.equals(type)) {
				type = "";
			}

			response.put("mode", mode);
			response.put("type", type);
		}

		return response;
	}

	public String[] getProductListFromCCS(String key) {
	    ICCS ccs;
	    ICCSConfig ccsConfig = new AbstractCCSConfig();
	    
	    ccsConfig.setCcsHost(fcProperties.getCCSHost());
	    ccsConfig.setDefaultAttachPath(fcProperties.getCCSBasePathPayment());
	    
	    ccs = new CCS();
	    ccs.init(ccsConfig);
		String value = null;
		String[] productList = null;
		try {
			value = ccs.getConf(key);
			if (!FCUtil.isEmpty(value)) {
				productList = value.split(DELIMITER);
			}
		} catch (CCSException e) {
			logger.error("Caught Exception is ", e);
		} catch (Exception e) {
			logger.error("Caught Exception is ", e);
		}

		return productList;
	}

	public String getQCStatus(Integer userID) {
		String userId = fcProperties + "-" + userID;
		boolean isOneCheckUser = false;
		String oneCheckUserId = null;
		try {
			String email = userService.getEmail(userID);
			if ((oneCheckUserId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(email)) != null) {
				isOneCheckUser = true;
			}
			if (isOneCheckUser) {
				userId = oneCheckUserId;
			}
			logger.info("Getting qc status for ICICI NB option");
			String qcStatus = quickCheckoutHandler.getQCStatusForUser(userId);
			logger.info("Response from KPay for QC status : " + qcStatus);
			if (!StringUtils.isEmpty(qcStatus)) {
				if (qcStatus.equals("qc_enabled")) {
					return "ICICI_USER_QC";
				} else if (qcStatus.equals("merch_qc_enabled")) {
					return "ICICI_MERC_QC";
				}
			}
		} catch (Exception e) {
			logger.error("Exception caught is", e);
		}
		return EMPTY;
	}

    public NetBankingOptionResponse getNetBankingOptions(Merchant merchant) {
        NetBankingOptionResponse netBankingOptions = paymentOptionCache.getNetBankingOptions(merchant.name());
        if(netBankingOptions==null){
            boolean receivedErrorResponseFromCCS = false;
            List<String> exclusionList = new ArrayList<String>();
            try{
                exclusionList = getExclusionListFromCCS(merchant);
            }catch(Exception e){
                logger.error("Received Error response from CCS.", e);
                receivedErrorResponseFromCCS = true;
            }
            List<PaymentOptionMaster> paymentOptions = null;
            if(!FCUtil.isEmpty(exclusionList)){
                paymentOptions = paymentOptionConfigDAO.getNetBankingOptions(exclusionList);
            }else{
                paymentOptions = paymentOptionConfigDAO.getAllNetBankingOptions();
            }
            List<String> priorityList = new ArrayList<String>();
            try{
                priorityList = getPriorityListFromCCS(merchant);
            }catch(Exception e){
                logger.error("Received Error response from CCS.", e);
                receivedErrorResponseFromCCS = true;
            }
            List<PaymentOptionMaster> priorityPaymentOptions = getPriorityPaymentOptions(priorityList, paymentOptions);
            if(!FCUtil.isEmpty(paymentOptions)){
                List<NetBankingOption> priorityBanks = convertToNetBankingOptions(priorityPaymentOptions);
                paymentOptions.removeAll(priorityPaymentOptions);
                List<NetBankingOption> otherBanks = convertToNetBankingOptions(paymentOptions);
                netBankingOptions = new NetBankingOptionResponse();
                netBankingOptions.setPriorityBanks(priorityBanks);
                netBankingOptions.setBanks(otherBanks);
                if(!receivedErrorResponseFromCCS){
                    paymentOptionCache.setNetBankingOptions(merchant.name(), netBankingOptions);
                }else{
                    //Send error code to front end so that it can interpret this as temporary response and does not cache the response.
                    netBankingOptions.setErrorCode("E002");
                    netBankingOptions.setErrorDesc("CCS Error.");
                }
            }
        }
        return netBankingOptions;
    }

    private List<PaymentOptionMaster> getPriorityPaymentOptions(List<String> priorityList, List<PaymentOptionMaster> paymentOptions) {
        
        List<PaymentOptionMaster> priorityOptions = new ArrayList<PaymentOptionMaster>();
        Map<String, PaymentOptionMaster> paymentOptionsMap = new HashMap<String, PaymentOptionMaster>();
        for(PaymentOptionMaster paymentOption : paymentOptions){
            paymentOptionsMap.put(paymentOption.getPaymentOptionCode(), paymentOption);
        }
        
        for(String priorityCode : priorityList){
            if(paymentOptionsMap.containsKey(priorityCode)){
                priorityOptions.add(paymentOptionsMap.get(priorityCode));
            }
        }
        return priorityOptions;
    }

    private List<String> getPriorityListFromCCS(Merchant merchant) {
        String exclusionListString;
        try {
            exclusionListString = ccs.getConf("merchant."+merchant.name()+".priority");
        } catch (Exception | CCSException e) {
            throw new RuntimeException("Error getting Priority List from CCS for merchant "+merchant.name(), e);
        }
        List<String> exclusionList = new ArrayList<String>();
        if(!FCUtil.isEmpty(exclusionListString)){
            for(String exclusionListId : exclusionListString.split(DELIMITER)){
                if(!FCUtil.isEmpty(exclusionListId.trim())){
                    exclusionList.add(exclusionListId.trim());
                }
            }
        }
        return exclusionList;
    }

    private List<String> getExclusionListFromCCS(Merchant merchant) {
        String exclusionListString;
        try {
            exclusionListString = ccs.getConf("merchant."+merchant.name()+".exclusions");
        } catch (ClassNotFoundException | CCSException e) {
            throw new RuntimeException("Error getting Exclusion List from CCS for merchant "+merchant.name(), e);
        }
        List<String> exclusionList = new ArrayList<String>();
        if(!FCUtil.isEmpty(exclusionListString)){
            for(String exclusionListId : exclusionListString.split(DELIMITER)){
                if(!FCUtil.isEmpty(exclusionListId.trim())){
                    exclusionList.add(exclusionListId.trim());
                }
            }
        }
        return exclusionList;
    }

    private ICCS getCCSClient() {
        ICCS ccs;
        ICCSConfig ccsConfig = new AbstractCCSConfig();
        ccsConfig.setCcsHost(fcProperties.getCCSHost());
        ccsConfig.setDefaultAttachPath(fcProperties.getCCSBasePathPayment());
        ccsConfig.setConnectTimeout(1000);
        ccsConfig.setSocketTimeout(1000);
        ccs = new CCS();
        ccs.init(ccsConfig);
        return ccs;
    }

    private List<NetBankingOption> convertToNetBankingOptions(List<PaymentOptionMaster> paymentOptions) {
        List<NetBankingOption> netBankingOptions = new ArrayList<NetBankingOption>();
        for(PaymentOptionMaster paymentOption : paymentOptions){
            NetBankingOption netBankingOption = new NetBankingOption();
            netBankingOption.setBankCode(paymentOption.getPaymentOptionCode());
            netBankingOption.setBankName(paymentOption.getPaymentOptionDisplayName());
            netBankingOptions.add(netBankingOption );
        }
        return netBankingOptions;
    }

    public List<NetBankingOption> getAllNetBankingOptions() {
        List<PaymentOptionMaster> paymentOptions = paymentOptionConfigDAO.getAllNetBankingOptions();
        List<NetBankingOption> netBankingOptions = convertToNetBankingOptions(paymentOptions);
        return netBankingOptions;
    }	

    public boolean isUpiEnabledForAppVersion(String appVersion, String appType) {
    	
    	boolean enableForApp = false;
   
    	if(FCUtil.isEmpty(appVersion) || FCUtil.isEmpty(appType)) {
    		return enableForApp;
    	}
    	try {
			JSONObject appVersions = appConfigService.getCustomProp(PaymentConstants.APP_VERSIONS_DISABLED_UPI);
			if(appVersions != null) {
				String appVersionString =  (String) appVersions.get(appType);
				if(appVersionString != null) {
					JSONParser parser = new JSONParser();
					JSONObject appVersionObject = null;
					try {
						appVersionObject = (JSONObject) parser.parse(appVersionString);
					} catch (ParseException e) {
						logger.error("Exception while parsing app versions json string :" + appVersionString, e);
						return enableForApp;
					}
					logger.info("AppVersionObject:" + appVersionObject);
					if(appVersionObject != null) {
						
						String minVersion = (String) appVersionObject.get("min");
						String excludedVersions = (String) appVersionObject.get("excluded");
						
						String[] excludedVersionsArray = excludedVersions.split(",");
						if(Integer.parseInt(appVersion) > Integer.parseInt(minVersion) && !(Arrays.asList(excludedVersionsArray).contains(appVersion))) {
							enableForApp = true;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while parsing App version", e);
		}
    	logger.info("UPI enabled : " + enableForApp);
    	return enableForApp;
    }
    
	@SuppressWarnings("unchecked")
	public Map<String, Boolean> areFeaturesEligible(List<String> featureNames, String userEmail, String merchantCode) {
		
		long startTime = new Date().getTime();
		
		Map<String, Boolean> featureResponseMap = null;
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setConnectTimeout(500);
		factory.setReadTimeout(500);
		factory.setHttpClient(CustomHttpClientBuilder.getCustomHttpClient(fcProperties.getProperty(PaymentConstants.KLICKPAY_FEATURE_RELEASE_URL), 500, 500));
		
		FeatureCheckInput featureCheckInput = new FeatureCheckInput();
		featureCheckInput.setFeatureNameList(featureNames);
		featureCheckInput.setEmail(userEmail);
		featureCheckInput.setMerchantCode(merchantCode);
		
		RestTemplate restTemplate = new RestTemplate(factory);
		
//		JSONObject obj = new JSONObject();
//		obj.put(PaymentConstants.KLICKPAY_FEATURE_NAME, featureName);
//		obj.put(PaymentConstants.KLICKPAY_FEATURE_USER_EMAIL, userEmail);
//		obj.put(PaymentConstants.KLICKPAY_FEATURE_MERCHANT_CODE, fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID));

		long midTime = new Date().getTime();
		try {
			featureResponseMap = restTemplate.postForObject(new URI(fcProperties.getProperty(PaymentConstants.KLICKPAY_FEATURE_RELEASE_URL)), featureCheckInput, Map.class);
		} catch (RestClientException e) {
			logger.error("Rest Exception while calling klickpay for feature eligibility: " + featureNames, e);
		} catch (Exception e) {
			logger.error("Unexpected Exception while calling klickpay for feature eligibility: " + featureNames, e);
		}
		
		long endTime = new Date().getTime();
		logger.info("Feature Applicable " + featureResponseMap);
		logger.info("Feature Check Rest Latency: " + (endTime - midTime));
		logger.info("Feature Check Total Latency: " + (endTime - startTime));
		return featureResponseMap;
	}
}
