package com.freecharge.payment.services.binrange;

import com.freecharge.common.framework.basedo.AbstractDO;

public class CardBinRange extends AbstractDO{
    private String id;
    private String binRangeId;
    private String binHead;
    private String binTail;
    private String cardType;
    private String cardNature;
    private String cardIssuerId;
    private String status;
    private String createdOn;
    private String cardCategoryId;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getBinRangeId() {
        return binRangeId;
    }
    public void setBinRangeId(String binRangeId) {
        this.binRangeId = binRangeId;
    }
    public String getBinHead() {
        return binHead;
    }
    public void setBinHead(String binHead) {
        this.binHead = binHead;
    }
    public String getBinTail() {
        return binTail;
    }
    public void setBinTail(String binTail) {
        this.binTail = binTail;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getCardNature() {
        return cardNature;
    }
    public void setCardNature(String cardNature) {
        this.cardNature = cardNature;
    }
    public String getCardIssuerId() {
        return cardIssuerId;
    }
    public void setCardIssuerId(String cardIssuerId) {
        this.cardIssuerId = cardIssuerId;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getCreatedOn() {
        return createdOn;
    }
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    public String getCardCategoryId() {
        return cardCategoryId;
    }
    public void setCardCategoryId(String cardCategoryId) {
        this.cardCategoryId = cardCategoryId;
    }
    
    
}
