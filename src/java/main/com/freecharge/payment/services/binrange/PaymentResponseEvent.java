package com.freecharge.payment.services.binrange;

import org.springframework.context.ApplicationEvent;

public class PaymentResponseEvent extends ApplicationEvent {

    private static final long serialVersionUID = -3565658312242175292L;
    
    private CardBin cardBin;
    
    private String orderId;
    
    private Boolean isPaymentSuccessful;
    
    public PaymentResponseEvent(Object source) {
        super(source);
    }

    public CardBin getCardBin() {
        return cardBin;
    }

    public void setCardBin(CardBin cardBin) {
        this.cardBin = cardBin;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Boolean getIsPaymentSuccessful() {
        return isPaymentSuccessful;
    }

    public void setIsPaymentSuccessful(Boolean isPaymentSuccessful) {
        this.isPaymentSuccessful = isPaymentSuccessful;
    }

}
