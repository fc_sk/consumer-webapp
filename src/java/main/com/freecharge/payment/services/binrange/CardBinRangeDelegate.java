package com.freecharge.payment.services.binrange;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dao.CardBinRangeDAO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;

@Service
@EnableAsync
public class CardBinRangeDelegate {
    private static Logger   logger = LoggingFactory.getLogger(CardBinRangeDelegate.class);

    @Autowired
    private CardBinRangeDAO binRangeDao;

    @Autowired
    private MetricsClient   metricsClient;

    @Async
    public void checkCardBinRangeTable(ProductPaymentBusinessDO productPaymentBusinessDO) {
        if (PaymentUtil.isCreditCard(productPaymentBusinessDO.getPaymenttype())
                || PaymentUtil.isDebitCard(productPaymentBusinessDO.getPaymenttype())) {
            logger.info("Async method checkCardBinRangeTable for lookup id :" + productPaymentBusinessDO.getLookupID());
            String cardNum = null;
            cardNum = productPaymentBusinessDO.getPaymentRequestCardDetails().getCardNumber();
            String binHead = StringUtils.left(cardNum, 6);
            String binTail = StringUtils.right(cardNum, 4);
            List<CardBinRange> binDataInDb = binRangeDao.getCardBinRanges(binHead, binTail);
            if (binDataInDb.isEmpty()) {
                metricsClient.recordEvent("Payment", "Binlist.PayAttempt.cardBinHead." + binHead, "BinNotFound");
            } else if (binDataInDb.size() == 1) {
                metricsClient.recordEvent("Payment", "Binlist.PayAttempt.cardBinHead." + binHead, "BinFound");
                CardBinRange cardBinRange = binDataInDb.get(0);
                if (!PaymentConstants.cardStorageMasterMap.get(productPaymentBusinessDO.getPaymentoption())
                        .equalsIgnoreCase(cardBinRange.getCardType())) {
                    metricsClient.recordEvent("Payment", "Binlist.PayAttempt.cardBinHead." + binHead, "CardTypeMismatch");
                }
                if (!PaymentConstants.cardTypeMasterMap.get(productPaymentBusinessDO.getPaymenttype()).equalsIgnoreCase(
                        cardBinRange.getCardNature())) {
                    metricsClient.recordEvent("Payment", "Binlist.PayAttempt.cardBinHead." + binHead, "CardNatureMismatch");
                }
            }
        }
    }
}
