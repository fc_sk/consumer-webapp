package com.freecharge.payment.services.binrange;

public class CardIssuer {
    private int id;
    private String issuerId;
    private String issuerName;
    private String cardType;
    private String cardNature;
    
    public static final CardIssuer UNKNOWN = new CardIssuer(0, "UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN");
    
    public CardIssuer() {
    }
    
    public CardIssuer(int id, String issuerId, String issuerName, String cardType, String cardNature) {
        this.id = id;
        this.issuerId = issuerId;
        this.issuerName = issuerName;
        this.cardType = cardType;
        this.cardNature = cardNature;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIssuerId() {
        return issuerId;
    }
    public void setIssuerId(String issuerId) {
        this.issuerId = issuerId;
    }
    
    public String getIssuerName() {
        return issuerName;
    }
    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }
    
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNature() {
        return cardNature;
    }
    public void setCardNature(String cardNature) {
        this.cardNature = cardNature;
    }

    @Override
    public String toString() {
        return "CardIssuer{" +
                "id=" + id +
                ", issuerId='" + issuerId + '\'' +
                ", issuerName='" + issuerName + '\'' +
                ", cardType='" + cardType + '\'' +
                ", cardNature='" + cardNature + '\'' +
                '}';
    }
}