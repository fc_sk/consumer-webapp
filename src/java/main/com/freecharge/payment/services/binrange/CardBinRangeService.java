package com.freecharge.payment.services.binrange;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dao.CardBinRangeDAO;
import com.freecharge.payment.dao.CardIssuerDao;
import com.freecharge.platform.metrics.MetricsClient;

@Service
public class CardBinRangeService {
    private static Logger   logger = LoggingFactory.getLogger(CardBinRangeService.class);

    @Autowired
    private CardBinRangeDAO binRangeDao;
    @Autowired
    private CardIssuerDao   cardIssuerDao;
    @Autowired
    private MetricsClient   metricsClient;

    public CardIssuer getCardIssuer(String binHead, String binTail) {
        List<CardIssuer> cardIssuerList = binRangeDao.getCardIssuers(binHead, binTail);
        if (cardIssuerList.isEmpty()) {
            logger.info("No issuer found for card bin[" + binHead + ":" + binTail + "]");
            return CardIssuer.UNKNOWN;
        }
        if (cardIssuerList.size() > 1) {
            logger.error("More than one issuer found for card bin[" + binHead + ":" + binTail + "]");
        }
        CardIssuer toReturn = cardIssuerList.get(0);
        logger.info("Returning issuer for card bin[" + binHead + ":" + binTail + "]:" + toReturn.getIssuerName());
        return toReturn;
    }

    public void updateCardBinRange(String binHead, String binTail, BinList binList, List<CardBinRange> cardBinRangeList) {
        String bankName = binList.getBank();
        String cardIssuerId = cardIssuerDao.getCardIssuerId(bankName);
        if (cardIssuerId == null) {
            metricsClient.recordEvent("Payment", "Binlist.nullIsuuerId", bankName);
        }
        if (cardBinRangeList.isEmpty()) {
            logger.info("No card_bin_range found for card bin[" + binHead + ":" + binTail
                    + "]. Inserting into card_bin_range.");
            binRangeDao.insertNewCardBinRange(binHead, "*", binList, cardIssuerId);
            metricsClient.recordEvent("Payment", "Binlist", "new");
        } else if (cardBinRangeList.size() == 1) {
            CardBinRange cardBinRange = cardBinRangeList.get(0);
            if (!compareCardBinRangeAndList(cardBinRange, binList, cardIssuerId)) {
                binRangeDao.updateCardBinRange(cardBinRange.getId(), binList, cardIssuerId);
                metricsClient.recordEvent("Payment", "Binlist", "updated");
            } else {
                metricsClient.recordEvent("Payment", "Binlist", "NotUpdated");
            }
        } else if (cardBinRangeList.size() > 1) {
            logger.error("More than one issuer found for card bin[" + binHead + ":" + binTail + "]");
            metricsClient.recordEvent("Payment", "Binlist.duplicate", binHead);
        }
        logger.info("CardBinRange for card bin[" + binHead + ":" + binTail + "] completed.");
    }
    
    public CardBinRange getCardBinRange(String binHead, String binTail) {
        List<CardBinRange> cardBinRanges = binRangeDao.getCardBinRanges(binHead, binTail);
        if (FCUtil.isEmpty(cardBinRanges)) {
            return new CardBinRange();
        }
        return cardBinRanges.get(0);
    }
    
    public String getCardIssuerFromId (String cardIssuerId) {
        return binRangeDao.getCardIssuerFromId(cardIssuerId);
    }

    private boolean compareCardBinRangeAndList(CardBinRange cardBinRange, BinList binList, String cardIssuerId) {
        if (cardBinRange.getCardType().equals(binList.getBrand())
                && cardBinRange.getCardNature().equals(binList.getCardType())
                && cardBinRange.getCardIssuerId().equals(cardIssuerId)
                && cardBinRange.getCardCategoryId().equals(binList.getCardCategory())) {
            return true;
        }
        return false;
    }
}
