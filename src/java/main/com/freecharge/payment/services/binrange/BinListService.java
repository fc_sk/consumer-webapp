package com.freecharge.payment.services.binrange;

import java.io.Reader;
import java.io.StringReader;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;

@Service
public class BinListService {
    private static final Logger                LOGGER        = LoggingFactory.getLogger(BinListService.class);
    private static final String                BIN_LIST_URL  = "http://www.binlist.net/json/";

    public static final String                 BIN           = "bin";
    public static final String                 BRAND         = "brand";
    public static final String                 SUB_BRAND     = "sub_brand";
    public static final String                 COUNTRY_CODE  = "country_code";
    public static final String                 COUNTRY_NAME  = "country_name";
    public static final String                 BANK          = "bank";
    public static final String                 CARD_TYPE     = "card_type";
    public static final String                 CARD_CATEGORY = "card_category";
    public static final String                 LATITUDE      = "latitude";
    public static final String                 LONGITUDE     = "longitude";

    @Autowired
    @Qualifier("binListConnectionManager")
    private MultiThreadedHttpConnectionManager connectionManager;

    public BinList getBinListData(String binHead) throws Exception {
        String binListUrl = BIN_LIST_URL + binHead;
        String cardData = getData(binListUrl);
        if (cardData == null) {
            return null;
        }
        return parse(new StringReader(cardData));
    }

    @SuppressWarnings("unchecked")
    public BinList parse(final Reader cardData) {
        Map<String, String> binListGetDataMap = null;
        BinList binList = new BinList();
        try {
            binListGetDataMap = (Map<String, String>) new JSONParser().parse(cardData);
            if (!FCUtil.isEmpty(binListGetDataMap)) {
                binList.setBin(binListGetDataMap.get(BIN));
                binList.setBrand(binListGetDataMap.get(BRAND));
                binList.setSubBrand(binListGetDataMap.get(SUB_BRAND));
                binList.setCountryCode(binListGetDataMap.get(COUNTRY_CODE));
                binList.setCountryName(binListGetDataMap.get(COUNTRY_NAME));
                binList.setBank(binListGetDataMap.get(BANK) == null ? "" : binListGetDataMap.get(BANK));
                binList.setCardType(binListGetDataMap.get(CARD_TYPE));
                binList.setCardCategory(binListGetDataMap.get(CARD_CATEGORY));
                binList.setLatitude(binListGetDataMap.get(LATITUDE));
                binList.setLongitude(binListGetDataMap.get(LONGITUDE));
            }
        } catch (Exception e) {
            LOGGER.error("Failed to parse binlist response.", e);
        }
        return binList;
    }

    private String getData(final String binListUrl) throws Exception {
        try {
            String body = null;
            HttpClient client = new HttpClient(connectionManager);
            HttpMethod txn = new GetMethod(binListUrl);
            txn.setRequestHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
            LOGGER.info("Making request to binlist URL: " + binListUrl);
            int code = client.executeMethod(txn);
            LOGGER.info("Binlist request returned HTTP response code " + code);
            if (code == HttpServletResponse.SC_OK) {
                body = txn.getResponseBodyAsString();
            }
            return body;
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to get binlist API call : %s", binListUrl), e);
            throw e;
        }
    }
}
