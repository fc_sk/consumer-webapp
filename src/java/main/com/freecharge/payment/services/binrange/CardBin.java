package com.freecharge.payment.services.binrange;

import java.io.Serializable;

/**
 * This bean holds details for a card bin.
 * Card bin is a masked form of credit/debit card.
 * @author arun
 *
 */
public class CardBin implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String cardBin;
    
    /*
     * Visa/Master/Maestro/etc.,
     */
    private String cardType;
    
    /*
     * Credit card/Debit card/etc.,
     */
    private String cardNature;
    
    private String issuingBank;
    private String nameOnCard;
    
    public String getCardBin() {
        return cardBin;
    }
    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }
    
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public String getIssuingBank() {
        return issuingBank;
    }
    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }
    
    public String getNameOnCard() {
        return nameOnCard;
    }
    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
    
    public String getCardNature() {
        return cardNature;
    }
    public void setCardNature(String cardNature) {
        this.cardNature = cardNature;
    }

    @Override
    public String toString() {
        return "CardBin{" +
                "cardBin='" + cardBin + '\'' +
                ", cardType='" + cardType + '\'' +
                ", cardNature='" + cardNature + '\'' +
                ", issuingBank='" + issuingBank + '\'' +
                ", nameOnCard='" + nameOnCard + '\'' +
                '}';
    }
}