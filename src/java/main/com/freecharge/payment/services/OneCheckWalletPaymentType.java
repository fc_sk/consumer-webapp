package com.freecharge.payment.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCPaymentPortalException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.dos.business.PaymentRefundBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;

@Component
public class OneCheckWalletPaymentType extends PaymentType {
    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private FCProperties fcProperties;

    @Override
    public Boolean doPayment(PaymentRequestBusinessDO paymentRequestBusinessDO) throws FCPaymentPortalException,
            FCPaymentGatewayException, Exception {
        Map<String, String> validations = validateRequest(paymentRequestBusinessDO);
        Boolean valid = new Boolean(validations.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE));
        if (valid) {
            IGatewayHandler gatewayHandler = getPaymentGateway(paymentRequestBusinessDO);
            if (gatewayHandler != null) {
                paymentRequestBusinessDO.setPaymentOption(PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION);
                gatewayHandler.paymentRequestSales(paymentRequestBusinessDO);
            }
            this.recordPaymentTypeMetric("oneCheckWallet");
        }
        return valid;
    }

    @Override
    public PaymentResponseBusinessDO checkStatus(PaymentRequestBusinessDO paymentRequestBusinessDO)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentRefundBusinessDO doRefund(PaymentRefundBusinessDO paymentRefundBusinessDO, String refundType)
            throws FCPaymentPortalException, FCPaymentGatewayException, Exception {
        IGatewayHandler gatewayHandler = pgFactory.getPaymentHandler(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
        gatewayHandler.refundPayment(paymentRefundBusinessDO, refundType);
        return paymentRefundBusinessDO;
    }
    
    @Override
    @Transactional(readOnly = true)
    public IGatewayHandler getPaymentGateway(PaymentRequestBusinessDO obj) {
        return pgFactory.getPaymentHandler(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
    }
    
    private Map<String, String> validateRequest(PaymentRequestBusinessDO paymentRequestBusinessDO) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();

        logger.debug("Validation in onecheck wallet payment type starts for session id : " + fs.getUuid());

        Map<String, String> resultMap = new HashMap<String, String>();

        Boolean valid = true;
        List<String> errorList = new ArrayList();

        if (StringUtils.isBlank(paymentRequestBusinessDO.getOrderId())) {
            valid = false;
            errorList.add("payment.orderid.error");
        }

        Map<String, String> amountValidationMap = PaymentUtil.validateAmount(paymentRequestBusinessDO.getAmount());

        if (valid) {
            valid = StringUtils.equalsIgnoreCase(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_STATE), "true");
        }

        errorList.add(amountValidationMap.get(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION));
        Integer numTxnx = this.successfulTransaction(paymentRequestBusinessDO.getOrderId());
        if (numTxnx > 0) {
            errorList.add("Already a successful Txn");
            valid = false;
        }


        String hashKey = paymentRequestBusinessDO.getHashKey();

        String f_string = paymentRequestBusinessDO.getAmount() + paymentRequestBusinessDO.getOrderId() + "freecharge"
                + fcProperties.getProperty(FCConstants.PAYMENT_PORTAL_HASHKEY_KEY);

        if (StringUtils.isNotBlank(hashKey) && !isAmountOverriden()) {

            String outputString = PaymentUtil.getHashKey(f_string);

            if (!StringUtils.equalsIgnoreCase(hashKey, outputString)) {
                valid = false;
                errorList.add("Url Tampered");
            }
        }

        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_STATE, valid.toString());
        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION, errorList.toString());
        resultMap.put(PaymentConstants.ORDER_ID, paymentRequestBusinessDO.getOrderId());

        if (!valid) {
            paymentRequestBusinessDO.setResultMap(resultMap);
        }

        logger.info(errorList);

        return resultMap;
    }
    
    private Boolean isAmountOverriden() {
        String overrideAmount = fcProperties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_AMOUNT_OVERRIDE);

        if (StringUtils.isNotBlank(overrideAmount) && !StringUtils.equalsIgnoreCase(overrideAmount, "-1")) {
            return true;
        }
        return false;
    }

    @Transactional(readOnly = true)
    private Integer successfulTransaction(String orderId) {
        return paymentTransactionService.getSuccessFullTxnForOrderId(orderId);
    }

}
