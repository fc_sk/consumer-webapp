package com.freecharge.payment.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RefundSchedulerRowMapper implements RowMapper<RefundSchedulerDO> {

    @Override
    public RefundSchedulerDO mapRow(ResultSet rs, int arg1) throws SQLException {
        RefundSchedulerDO refundSchedulerDO = new RefundSchedulerDO();
        refundSchedulerDO.setCreatedOn(rs.getTimestamp("created_on"));
        refundSchedulerDO.setExpiryTime(rs.getTimestamp("expiry_time"));
        refundSchedulerDO.setLastUpdated(rs.getTimestamp("last_updated"));
        refundSchedulerDO.setMtxnId(rs.getString("mtxn_id"));
        refundSchedulerDO.setScheduleCount(rs.getInt("schedule_count"));
        refundSchedulerDO.setStatus(RefundStatus.fromCode(rs.getInt("status")));
        return refundSchedulerDO;
    }
    
}
