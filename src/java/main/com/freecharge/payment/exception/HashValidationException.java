package com.freecharge.payment.exception;

public class HashValidationException extends RuntimeException {
    public HashValidationException(Throwable cause) {
        super(cause);
    }

    public HashValidationException(String message) {
        super(message);
    }

    public HashValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
