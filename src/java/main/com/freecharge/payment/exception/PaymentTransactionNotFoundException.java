package com.freecharge.payment.exception;

public class PaymentTransactionNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public PaymentTransactionNotFoundException(String cause) {
        super(cause);
    }

}
