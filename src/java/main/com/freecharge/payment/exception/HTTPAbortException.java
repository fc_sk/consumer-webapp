package com.freecharge.payment.exception;

public class HTTPAbortException extends Exception {
    public HTTPAbortException(Throwable cause) {
        super(cause);
    }
    public HTTPAbortException(String message) {
        super(message);
    }

    public HTTPAbortException(String message, Throwable cause) {
        super(message, cause);


    }
}
