package com.freecharge.payment.exception;

public class VirtualCardStorageServiceException extends RuntimeException {
        private String errorCode;

        private String errorMessage;

    public VirtualCardStorageServiceException(Throwable cause) {
        super(cause);
    }

    public VirtualCardStorageServiceException(String message) {
        super(message);
    }

    public VirtualCardStorageServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public VirtualCardStorageServiceException(String errorCode,String errorMessage){
        this.errorCode=errorCode;
        this.errorMessage=errorMessage;
    }

    public VirtualCardStorageServiceException(String errorCode,String errorMessage,Throwable cause){
        super(cause);
        this.errorCode=errorCode;
        this.errorMessage=errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
