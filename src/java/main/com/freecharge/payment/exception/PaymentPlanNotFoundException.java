package com.freecharge.payment.exception;

public class PaymentPlanNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public PaymentPlanNotFoundException(String cause) {
        super(cause);
    }
}
