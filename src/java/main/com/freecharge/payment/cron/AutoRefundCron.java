package com.freecharge.payment.cron;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.autorefund.AutoRefund;

/**
 * A wrapper class around {@link AutoRefund} that invokes
 * {@link AutoRefund#doRefund(java.util.Date, java.util.Date)} with
 * appropriate date values.
 * @author arun
 *
 */
public class AutoRefundCron {
    private static final Logger logger = LoggingFactory.getLogger(LoggingFactory.getRefundLoggerName(AutoRefundCron.class.getName()));
	
	@Autowired
	private AutoRefund autoRefundHandler;
	
	private int scanningWindowMinutes;
	
	private int scanningDelayMinutes;
	
	public int getScanningDelayMinutes() {
		return scanningDelayMinutes;
	}
	
	public void setScanningDelayMinutes(int scanningDelayMinutes) {
		this.scanningDelayMinutes = scanningDelayMinutes;
	}
	
	public int getScanningWindowMinutes() {
		return scanningWindowMinutes;
	}
	
	public void setScanningWindowMinutes(int scanningWindowMinutes) {
		this.scanningWindowMinutes = scanningWindowMinutes;
	}
	
	public void doRefund(boolean async) throws Exception {
		Date now = new Date();
		Date to = DateUtils.addMinutes(now, -scanningDelayMinutes);
		Date from = DateUtils.addMinutes(to, -scanningWindowMinutes);

		logger.info("About to initiate AutoRefund from [" + from + "] to ["
				+ to + "]");
		autoRefundHandler.doRefund(from, to, async);
		logger.info("Successfully done with handling AutoRefund from [" + from
				+ "] to [" + to + "]");
	}
}
