package com.freecharge.payment.cron;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freefund.services.UploadCodesService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class UploadCodesCron {

    private static final Logger logger = LoggingFactory.getLogger(
            UploadCodesCron.class.getName());

    @Autowired
    private UploadCodesService uploadCodesService;

    public void doUploadCodes() throws Exception {
        logger.info("About to initiate InsertCodesIntoMongoJob");
        uploadCodesService.uploadCodes();
        logger.info("Successfully done with handling InsertCodesIntoMongoJob");
    }
}
