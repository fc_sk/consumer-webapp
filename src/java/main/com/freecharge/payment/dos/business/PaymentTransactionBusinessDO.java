package com.freecharge.payment.dos.business;

import java.util.Date;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.payment.util.PaymentConstants;

public class PaymentTransactionBusinessDO extends BaseWebDO {

    private String transactionId;
    private String orderId;
    private Date setToPg;
    private Date recievedToPg;
    private Double amount;
    private String merchantTxnId;
    private Boolean isSuccessful;
    private String paymentGateway;
    private String paymentMode;
    private String pgMessage;
    private Integer paymentRequestId;
    private Integer paymentResponseId;
    private Integer paymentType;
    private String paymentOption;
    private Integer paymentTransactionId;
    private String authCode;
    private String rrno;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getSetToPg() {
        return setToPg;
    }

    public void setSetToPg(Date setToPg) {
        this.setToPg = setToPg;
    }

    public Date getRecievedToPg() {
        return recievedToPg;
    }

    public void setRecievedToPg(Date recievedToPg) {
        this.recievedToPg = recievedToPg;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMerchantTxnId() {
        return merchantTxnId;
    }

    public void setMerchantTxnId(String merchantTxnId) {
        this.merchantTxnId = merchantTxnId;
    }

    public Boolean getSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(Boolean successful) {
        isSuccessful = successful;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPgMessage() {
        return pgMessage;
    }

    public void setPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
    }

    public Integer getPaymentRequestId() {
        return paymentRequestId;
    }

    public void setPaymentRequestId(Integer paymentRequestId) {
        this.paymentRequestId = paymentRequestId;
    }

    public Integer getPaymentResponseId() {
        return paymentResponseId;
    }

    public void setPaymentResponseId(Integer paymentResponseId) {
        this.paymentResponseId = paymentResponseId;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }


    public Integer getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public void setPaymentTransactionId(Integer paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getRrno() {
        return rrno;
    }

    public void setRrno(String rrno) {
        this.rrno = rrno;
    }
    
    public boolean isWalletPayment() {
        return paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)
                || paymentGateway.equals(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
    }

    @Override
    public String toString() {
        return "PaymentTransactionBusinessDO{" +
                "transactionId='" + transactionId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", setToPg=" + setToPg +
                ", recievedToPg=" + recievedToPg +
                ", amount=" + amount +
                ", merchantTxnId='" + merchantTxnId + '\'' +
                ", isSuccessful=" + isSuccessful +
                ", paymentGateway='" + paymentGateway + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", pgMessage='" + pgMessage + '\'' +
                ", paymentRequestId=" + paymentRequestId +
                ", paymentResponseId=" + paymentResponseId +
                ", paymentType=" + paymentType +
                ", paymentOption='" + paymentOption + '\'' +
                ", paymentTransactionId=" + paymentTransactionId +
                ", authcode=" + authCode +
                ", rrno=" + rrno+
                '}';
    }
}
