package com.freecharge.payment.dos.business;

import java.sql.Timestamp;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;

public class PaymentQueryBusinessDO extends BaseBusinessDO {

    private Integer paymentTxnId;
    private String orderId;
    private String merchantTxnId;
    private String responseMsg;
    private Boolean paymentStatus;
    private PaymentTransactionBusinessDO paymentTransactionBusinessDO;
    private Timestamp pgTransactionTime;
    private String pgTransactionId;
    private Double amount;
    private String referenceId;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPgTransactionId() {
        return pgTransactionId;
    }

    public void setPgTransactionId(String pgTransactionId) {
        this.pgTransactionId = pgTransactionId;
    }

    public Timestamp getPgTransactionTime() {
        return pgTransactionTime;
    }

    public void setPgTransactionTime(Timestamp pgTransactionTime) {
        this.pgTransactionTime = pgTransactionTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PaymentTransactionBusinessDO getPaymentTransactionBusinessDO() {
        return paymentTransactionBusinessDO;
    }

    public void setPaymentTransactionBusinessDO(PaymentTransactionBusinessDO paymentTransactionBusinessDO) {
        this.paymentTransactionBusinessDO = paymentTransactionBusinessDO;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getMerchantTxnId() {
        return merchantTxnId;
    }

    public void setMerchantTxnId(String merchantTxnId) {
        this.merchantTxnId = merchantTxnId;
    }

    public Integer getPaymentTxnId() {
        return paymentTxnId;
    }

    public void setPaymentTxnId(Integer paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
    
    public static PaymentQueryBusinessDO create(PaymentTransaction paymentTransaction) {
        PaymentQueryBusinessDO paymentQueryBusinessDO = new PaymentQueryBusinessDO();
        paymentQueryBusinessDO.setMerchantTxnId(paymentTransaction.getMerchantTxnId());
        paymentQueryBusinessDO.setOrderId(paymentTransaction.getOrderId());
        paymentQueryBusinessDO.setPaymentTransactionBusinessDO(new PaymentTransactionBusinessDO());
        paymentQueryBusinessDO.getPaymentTransactionBusinessDO().setPaymentOption(paymentTransaction.getPaymentOption());
        paymentQueryBusinessDO.getPaymentTransactionBusinessDO().setPaymentType(paymentTransaction.getPaymentType());
        paymentQueryBusinessDO.setPgTransactionId(paymentTransaction.getTransactionId());
        paymentQueryBusinessDO.setPaymentTxnId(paymentTransaction.getPaymentTxnId());
        paymentQueryBusinessDO.setAmount(paymentTransaction.getAmount());
        return paymentQueryBusinessDO;
    }

    @Override
    public String toString() {
        return "PaymentQueryBusinessDO{" + "orderId='" + orderId + '\'' + ", merchantTxnId='" + merchantTxnId + '\'' + ", responseMsg='" + responseMsg + '\'' + ", paymentStatus=" + paymentStatus
                + ", paymentTransactionBusinessDO=" + paymentTransactionBusinessDO + '}';
    }
}
