package com.freecharge.payment.dos.business;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class PaymentRefundBusinessDO extends BaseBusinessDO {

    private String orderId;
    private Double amount;
    private Boolean status;
    private Double refundAmount;
    private String msg;
    private PaymentTransactionBusinessDO paymentTransactionBusinessDO;
    private String rawPGResponse;
    private boolean refundToWallet;
    
    public String getRawPGResponse() {
		return rawPGResponse;
	}
    
    public void setRawPGResponse(String rawPGResponse) {
		this.rawPGResponse = rawPGResponse;
	}

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Double getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(Double refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PaymentTransactionBusinessDO getPaymentTransactionBusinessDO() {
        return paymentTransactionBusinessDO;
    }

    public void setPaymentTransactionBusinessDO(PaymentTransactionBusinessDO paymentTransactionBusinessDO) {
        this.paymentTransactionBusinessDO = paymentTransactionBusinessDO;
    }

    public boolean isRefundToWallet() {
        return refundToWallet;
    }

    public void setRefundToWallet(boolean refundToWallet) {
        this.refundToWallet = refundToWallet;
    }

	@Override
    public String toString() {
        return "PaymentRefundBusinessDO{" +
                "orderId='" + orderId + '\'' +
                ", amount=" + amount +
                ", status=" + status +
                ", refundAmount=" + refundAmount +
                ", msg='" + msg + '\'' +
                ", paymentTransactionBusinessDO=" + paymentTransactionBusinessDO +
                ", rawPGResponse='" + rawPGResponse + '\'' +
                ", refundToWallet=" + refundToWallet +
                '}';
    }
}
