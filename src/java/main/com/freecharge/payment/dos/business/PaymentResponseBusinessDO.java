package com.freecharge.payment.dos.business;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class PaymentResponseBusinessDO extends BaseBusinessDO {
    private Map<String, String> requestMap;
    private Map<String, Object> resultMap;
    private String              pg;
    private Integer             paymentType;
    private String              cardBinHead;
    private String              cardBinTail;
    private String              mTxnId;
    private boolean             isStatusCheck;

    public Map<String, String> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, String> requestMap) {
        this.requestMap = requestMap;
    }

    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardBinHead() {
        return cardBinHead;
    }

    public void setCardBinHead(String cardBinHead) {
        this.cardBinHead = cardBinHead;
    }

    public String getCardBinTail() {
        return cardBinTail;
    }

    public void setCardBinTail(String cardBinTail) {
        this.cardBinTail = cardBinTail;
    }

    public String getmTxnId() {
        return mTxnId;
    }

    public void setmTxnId(String mTxnId) {
        this.mTxnId = mTxnId;
    }
    
    public void setStatusCheck(boolean isStatusCheck) {
        this.isStatusCheck = isStatusCheck;
    }
    
    public boolean isStatusCheck() {
        return isStatusCheck;
    }
}
