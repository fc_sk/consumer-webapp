package com.freecharge.payment.dos.business;

import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class PaymentRequestBusinessDO extends BaseBusinessDO {
    private static final long serialVersionUID = 754806051028248568L;
    private String upiInfo;
    private String amount;
    private String billingAddress2;
    private String paymentOption;
    private String paymentCurrency;
    private String errorUrl;
    private String hashKey;
    private String paymentStatus;
    private String lastName;
    private String middleName;
    private Integer paymentType;
    private String paymentGatewayOverride;
    private String successUrl;
    private String orderId;
    private Integer affiliateId;
    private Integer productId;
    private String operatorId;
    private Integer bankCode;
    private String remoteIp;
    private Integer appVersion;
    private PaymentRequestCardDetails paymentRequestCardDetails;
    private String mtxnId;
    private int channel;
    private String gateWayToUse;
    private Users user;
    private Map<String,String> transactionTypeStatus;
    
    @SuppressWarnings("rawtypes")
    private Map resultMap;
    @SuppressWarnings("rawtypes")
    private Map requestMap;

    public Map<String, String> getTransactionTypeStatus() {
        return transactionTypeStatus;
    }

    public void setTransactionTypeStatus(Map<String, String> transactionTypeStatus) {
        this.transactionTypeStatus = transactionTypeStatus;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public String getUpiInfo() {
        return this.upiInfo;
    }

    public void setUpiInfo(String upiInfo) {
        this.upiInfo = upiInfo;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    public String getErrorUrl() {
        return errorUrl;
    }

    public void setErrorUrl(String errorUrl) {
        this.errorUrl = errorUrl;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentGatewayOverride() {
        return paymentGatewayOverride;
    }

    public void setPaymentGatewayOverride(String paymentGatewayOverride) {
        this.paymentGatewayOverride = paymentGatewayOverride;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(Integer affiliateId) {
        this.affiliateId = affiliateId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getBankCode() {
        return bankCode;
    }

    public void setBankCode(Integer bankCode) {
        this.bankCode = bankCode;
    }

    @SuppressWarnings("rawtypes")
    public Map getResultMap() {
        return resultMap;
    }

    @SuppressWarnings("rawtypes")
    public void setResultMap(Map resultMap) {
        this.resultMap = resultMap;
    }

    @SuppressWarnings("rawtypes")
    public Map getRequestMap() {
        return requestMap;
    }

    @SuppressWarnings("rawtypes")
    public void setRequestMap(Map requestMap) {
        this.requestMap = requestMap;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public Integer getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }

    public PaymentRequestCardDetails getPaymentRequestCardDetails() {
        return paymentRequestCardDetails;
    }

    public void setPaymentRequestCardDetails(PaymentRequestCardDetails paymentRequestCardDetails) {
        this.paymentRequestCardDetails = paymentRequestCardDetails;
    }

	public String getMtxnId() {
		return mtxnId;
	}

	public void setMtxnId(String mtxnId) {
		this.mtxnId = mtxnId;
	}
    
	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}

    public String getGateWayToUse() {
        return gateWayToUse;
    }

    public void setGateWayToUse(String gateWayToUse) {
        this.gateWayToUse = gateWayToUse;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

}
