package com.freecharge.payment.dos.business;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;


public class PaymentRequestCardDetails extends BaseWebDO {

    private static final long serialVersionUID = 1L;
    protected String  cardHolderName;
    protected String  cardNumber;
    protected String  cardExpMonth;
    protected String  cardExpYear;
    protected String  cardCvv;
    protected String  cardToken;
    protected String  cardType;
    protected boolean saveCard;
    protected String  saveCardName;
    protected boolean isDebitCard;

    private Map<String, String> params;
    
    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardExpMonth() {
        return cardExpMonth;
    }

    public void setCardExpMonth(String cardExpMonth) {
        this.cardExpMonth = cardExpMonth;
    }

    public String getCardExpYear() {
        return cardExpYear;
    }

    public void setCardExpYear(String cardExpYear) {
        this.cardExpYear = cardExpYear;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public boolean isDebitCard() {
        return isDebitCard;
    }

    public void setDebitCard(boolean isDebitCard) {
        this.isDebitCard = isDebitCard;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public boolean isSaveCard() {
        return saveCard;
    }

    public void setSaveCard(boolean saveCard) {
        this.saveCard = saveCard;
    }

    public String getSaveCardName() {
        return saveCardName;
    }

    public void setSaveCardName(String saveCardName) {
        this.saveCardName = saveCardName;
    }
}
