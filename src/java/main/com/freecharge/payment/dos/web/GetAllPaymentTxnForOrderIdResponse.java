package com.freecharge.payment.dos.web;

import java.util.List;

public class GetAllPaymentTxnForOrderIdResponse extends PaymentDetailResponse{

    private List<PaymentTxn> paymentTxns;
    
    public List<PaymentTxn> getPaymentTxns() {
        return paymentTxns;
    }

    public void setPaymentTxns(List<PaymentTxn> paymentTxns) {
        this.paymentTxns = paymentTxns;
    }

}
